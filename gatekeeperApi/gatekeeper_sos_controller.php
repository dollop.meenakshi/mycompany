<?php

include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {


    if ($key == $keydb) {
        $response = array();
        $sos_send_successfully = $xml->string->sos_send_successfully;
        extract(array_map("test_input", $_POST));


        $file = $_FILES['sos_image_upload']['tmp_name'];
        if(file_exists($file)) {
            $image_Arr = $_FILES['sos_image_upload'];   
            $temp = explode(".", $_FILES["sos_image_upload"]["name"]);
            $sos_image_upload = $event_name.'_'.round(microtime(true)) . '.' . end($temp);
            move_uploaded_file($_FILES["sos_image_upload"]["tmp_name"], "../img/sos/".$sos_image_upload);

            $sos_image = $base_url . 'img/sos/' . $sos_image_upload;
            $is_uploaded_image= true;
        } else{
          $sos_image=$sos_image;
          $is_uploaded_image= false;

        }

        if (isset($getPreSOSList) && $getPreSOSList == 'getPreSOSList'  && $society_id != '') {

            $qSosEvent = $d->select("sos_events_master", "society_id ='$society_id'");

            if (mysqli_num_rows($qSosEvent) > 0) {

                $response["sos_event"] = array();

                while ($data_notification = mysqli_fetch_array($qSosEvent)) {

                    $sos_event = array();
                    $sos_event["sos_duration"] = $data_notification['sos_duration'];
                    $sos_event["sos_event_id"] = $data_notification['sos_event_id'];
                    $sos_event["event_name"] = $data_notification['event_name'];
                    $sos_event["event_type"] = $data_notification['event_type'];
                    $sos_event["event_status"] = $data_notification['event_status'];
                    $sos_event["sos_for"] = $data_notification['sos_for'];
                    $sos_event["sos_image"] = $base_url . 'img/sos/' . $data_notification['sos_image'];

                    array_push($response["sos_event"], $sos_event);
                }

                $response["message"] = ''.$xml->string->getEventSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->getEventfail;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if (isset($getVisitorTypeList) && $getVisitorTypeList == 'getVisitorTypeList'  && $society_id != '') {

            $qR = $d->select("visiting_reason_master", "active_status=0 AND society_id='$society_id'");

            $qSosEvent = $d->select("visitorMainType", "active_status=0");


            if (mysqli_num_rows($qR) > 0) {
                $response["visiting_reason"] = array();
                while ($rData = mysqli_fetch_array($qR)) {
                    $visiting_reason = array();
                    $visiting_reason["visiting_reason_id"] = $rData['visiting_reason_id'];
                    $visiting_reason["visiting_reason"] = html_entity_decode($rData['visiting_reason']);

                    array_push($response["visiting_reason"],$visiting_reason); 
                }

            }

            if (mysqli_num_rows($qSosEvent) > 0) {

                $response["visitor_main_type"] = array();

                while ($data_notification = mysqli_fetch_array($qSosEvent)) {
                    $visitor_main_type = array();
                    $visitor_main_type["visitor_main_type_id"] = $data_notification['visitor_main_type_id'];
                    $language_key_name= $data_notification['language_key_name'];
                    $lngMenuName = ''.$xml->string->$language_key_name;
                    if ($lngMenuName!="") {
                        $visitor_main_type["main_type_name"]=''.$lngMenuName;
                    } else {
                        $visitor_main_type["main_type_name"]=$data_notification['main_type_name'];
                    }
                    $visitor_main_type["main_type_image"] = $base_url . 'img/visitor_company/' . $data_notification['main_type_image'];
                    $visitor_main_type["visitor_main_full_img"] = $base_url . 'img/visitor_company/' . $data_notification['visitor_main_full_img'];
                    $visitor_main_type["visitor_type"] = $data_notification['visitor_type'];

                    

                    if ($country_id!="") {
                        $qs=$d->select("visitorSubType","country_id='$country_id' AND active_status=0 AND visitor_main_type_id='$data_notification[visitor_main_type_id]' AND visitor_sub_type_name!='Other' OR country_id='0' AND active_status=0 AND visitor_main_type_id='$data_notification[visitor_main_type_id]' AND visitor_sub_type_name!='Other'");
                    } else {
                        $qs=$d->select("visitorSubType","active_status=0 AND visitor_main_type_id='$data_notification[visitor_main_type_id]' AND visitor_sub_type_name!='Other'");
                    }


                    $visitor_main_type["visitor_sub_type"] = array();
                    if(mysqli_num_rows($qs)>0){

                        while($data=mysqli_fetch_array($qs)) {
                            $visitor_sub_type = array(); 
                            $visitor_sub_type["visitor_sub_type_id"]=$data['visitor_sub_type_id'];
                            $visitor_sub_type["visitor_sub_type_name"]=$data['visitor_sub_type_name'];
                            $visitor_sub_type["visitor_sub_image"]=$base_url.'img/visitor_company/'.$data['visitor_sub_image'];
                            array_push($visitor_main_type["visitor_sub_type"],$visitor_sub_type); 
                        }
                        $temp = true;
                    } else {
                        $temp = false;
                    }

                    
                    $qOther=$d->select("visitorSubType","active_status=0 AND visitor_main_type_id='$data_notification[visitor_main_type_id]' AND visitor_sub_type_name='Other'");


                    if(mysqli_num_rows($qOther)>0){

                        while($data=mysqli_fetch_array($qOther)) {
                            $visitor_sub_type = array(); 
                            $visitor_sub_type["visitor_sub_type_id"]=$data['visitor_sub_type_id'];
                            $visitor_sub_type["visitor_sub_type_name"]=$data['visitor_sub_type_name'];
                            $visitor_sub_type["visitor_sub_image"]=$base_url.'img/visitor_company/'.$data['visitor_sub_image'];
                            array_push($visitor_main_type["visitor_sub_type"],$visitor_sub_type); 
                        }
                        $temp1 = true;
                    } else {
                         $temp1 = true;
                    }
                    if ($temp==true || $temp1 ==true ) {
                        array_push($response["visitor_main_type"],$visitor_main_type); 
                    }
                }

                
            }


            if ($qSosEvent>0) {
                $response["message"] = ''.$xml->string->visitorMaintypeSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->$getEventfail;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if (isset($addNewCustomSOSNew) && $addNewCustomSOSNew == 'addNewCustomSOSNew' && filter_var($society_id, FILTER_VALIDATE_INT) == true) {



            $m->set_data('society_id', $society_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('unit_id', $unit_id);
            $m->set_data('sos_title', $sos_title);
            $m->set_data('sos_type ', ((int) $sos_type));
            $m->set_data('sos_status', $sos_status);
            $m->set_data('sos_for', $sos_for);
            $m->set_data('sos_by', $sos_by . "(Security Guard) ");
            $m->set_data('time', $time);
            $m->set_data('otime', $otime);
            $m->set_data('sos_image', $sos_image);
            $m->set_data('minimumMin', $minimumMin);
            $m->set_data('society_name', html_entity_decode($society_name));
            $m->set_data('sos_latitude', $sos_latitude);
            $m->set_data('sos_longitude', $sos_longitude);
            $sos_unit = "Security Guard";

            $a = array(
                'society_id' => $m->get_data('society_id'),
                'sos_title' => $m->get_data('sos_title'),
                'sos_type' => ((int) $sos_type),
                'sos_status' => $m->get_data('sos_status'),
                'sos_for' => $m->get_data('sos_for'),
                'sos_by' => $m->get_data('sos_by'),
                'time' => $m->get_data('time'),
                'otime' => $m->get_data('otime'),
                'sos_image' => $m->get_data('sos_image'),
                'society_name' => $m->get_data('society_name'),
                'minimumMin' => $m->get_data('minimumMin'),
                'is_uploaded_image' => $is_uploaded_image,
            );

             

            if ($sos_for==1) {
                $fcmArray = $d->get_android_fcm("users_master", "user_token!='' AND society_id='$society_id' AND device='android' AND sos_alert=0");
                $fcmArrayIos = $d->get_android_fcm("users_master", "user_token!='' AND society_id='$society_id' AND device='ios' AND sos_alert=0");
                $nResident->noti("","", $society_id, $fcmArray, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nResident->noti_ios("","", $society_id, $fcmArrayIos, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $m->set_data('send_to', 1);

            } else if ($sos_for==2) {
                $fcmArray = $d->get_emp_fcm("employee_master", "emp_token!='' AND emp_type_id='0'  AND society_id='$society_id' AND entry_status=1");
                $nGaurd->noti_new("",$fcmArray, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $m->set_data('send_to', 2);
            }else if ($sos_for==3) {
                $fcmArray = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id'  AND device='android' AND sos_alert=0 AND sos_alert=0");
                $fcmArrayIos = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id' AND device!='android' AND sos_alert=0 AND sos_alert=0");
                $nAdmin->noti_new($society_id,"", $fcmArray, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nAdmin->noti_ios_new($society_id,"", $fcmArrayIos, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $m->set_data('send_to', 3);
            } else if ($sos_for==4) {
                $fcmArrayGuard = $d->get_emp_fcm("employee_master", "emp_token!='' AND emp_type_id='0'  AND society_id='$society_id' AND entry_status=1");
                $nGaurd->noti_new("",$fcmArrayGuard, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

                $fcmArray = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id'  AND device='android' AND sos_alert=0 AND sos_alert=0");
                $fcmArrayIos = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id' AND device!='android' AND sos_alert=0 AND sos_alert=0");
                $nAdmin->noti_new($society_id,"", $fcmArray, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nAdmin->noti_ios_new($society_id,"", $fcmArrayIos, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $m->set_data('send_to', 4);

            } else if ($sos_for==5) {
                $fcmArrayGuard = $d->get_emp_fcm("employee_master", "emp_token!='' AND emp_type_id='0'  AND society_id='$society_id' AND entry_status=1");
                $nGaurd->noti_new("",$fcmArrayGuard, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

                $fcmArray = $d->get_android_fcm("users_master", "user_token!='' AND society_id='$society_id' AND device='android' AND sos_alert=0");
                $fcmArrayIos = $d->get_android_fcm("users_master", "user_token!='' AND society_id='$society_id' AND device='ios' AND sos_alert=0");
                $nResident->noti("","", $society_id, $fcmArray, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nResident->noti_ios("","", $society_id, $fcmArrayIos, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $m->set_data('send_to', 5);
            }else if ($sos_for==0) {
                $fcmArrayGuard = $d->get_emp_fcm("employee_master", "emp_token!='' AND emp_type_id='0'  AND society_id='$society_id' AND entry_status=1");
                $nGaurd->noti_new("",$fcmArrayGuard, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

                $fcmArray = $d->get_android_fcm("users_master", "user_token!='' AND society_id='$society_id' AND device='android' AND sos_alert=0");
                $fcmArrayIos = $d->get_android_fcm("users_master", "user_token!='' AND society_id='$society_id' AND device='ios' AND sos_alert=0");
                $nResident->noti("","", $society_id, $fcmArray, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nResident->noti_ios("","", $society_id, $fcmArrayIos, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

                $fcmArrayAdmin = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id'  AND device='android' AND sos_alert=0 AND sos_alert=0");
                $fcmArrayIosAdmin = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id' AND device!='android' AND sos_alert=0 AND sos_alert=0");
                $nAdmin->noti_new($society_id,"", $fcmArrayAdmin, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nAdmin->noti_ios_new($society_id,"", $fcmArrayIosAdmin, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $m->set_data('send_to', 0);
            }else if ($sos_for==6) {
                $fq = $d->select("users_master", "unit_id='$unit_id' AND user_id='$user_id'");
                $ownerData = mysqli_fetch_array($fq);
                $userType = $ownerData['user_type'];

                $fcmArray = $d->get_android_fcm("users_master", "user_token!='' AND society_id='$society_id' AND device='android' AND unit_id='$unit_id' AND sos_alert=0 AND user_type='$userType'");
                $fcmArrayIos = $d->get_android_fcm("users_master", "user_token!='' AND society_id='$society_id' AND device='ios' AND unit_id='$unit_id' AND sos_alert=0 AND user_type='$userType'");
                $nResident->noti("","", $society_id, $fcmArray, "sos", "You have receive sos from $sos_by", $a);
                $nResident->noti_ios("","", $society_id, $fcmArrayIos, "sos", "You have receive sos from $sos_by", $a);
                $m->set_data('send_to', 6);
            }

            $aSos = array(
                'user_id' => $m->get_data('user_id'),
                'unit_id' => $m->get_data('unit_id'),
                'society_id' => $m->get_data('society_id'),
                'sos_msg ' => $m->get_data('sos_title'),
                'sos_time' => date("Y-m-d H:i:s"),
                'sos_latitude' => $m->get_data('sos_latitude'),
                'sos_longitude' => $m->get_data('sos_longitude'),
                'sos_image' => $m->get_data('sos_image'),
                'send_to' =>$m->get_data('send_to'),
            );

            $d->insert("sos_request_master", $aSos);

            $response["message"] = "SOS Send Successfully";
            $response["status"] = "200";
            echo json_encode($response);
        } else if (isset($addGlobleSOS) && $addGlobleSOS == 'addGlobleSOS' && filter_var($society_id, FILTER_VALIDATE_INT) == true) {



            $m->set_data('society_id', $society_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('unit_id', $unit_id);
            $m->set_data('sos_title', $sos_title);
            $m->set_data('sos_type ', ((int) $sos_type));
            $m->set_data('sos_status', $sos_status);
            $m->set_data('sos_for', $sos_for);
            $m->set_data('sos_by', $sos_by . "(Security Guard) ");
            $m->set_data('time', $time);
            $m->set_data('otime', $otime);
            $m->set_data('sos_image', $sos_image);
            $m->set_data('minimumMin', $minimumMin);
            $m->set_data('society_name', html_entity_decode($society_name));
            $m->set_data('sos_latitude', $sos_latitude);
            $m->set_data('sos_longitude', $sos_longitude);
            $m->set_data('gatekeeper_id', $my_id);
            $sos_unit = "Security Guard";

            $a = array(
                'society_id' => $m->get_data('society_id'),
                'sos_title' => $m->get_data('sos_title'),
                'sos_type' => ((int) $sos_type),
                'sos_status' => $m->get_data('sos_status'),
                'sos_for' => $m->get_data('sos_for'),
                'sos_by' => $m->get_data('sos_by'),
                'time' => $m->get_data('time'),
                'otime' => $m->get_data('otime'),
                'sos_image' => $m->get_data('sos_image'),
                'society_name' => $m->get_data('society_name'),
                'minimumMin' => $m->get_data('minimumMin'),
                'is_uploaded_image' => $is_uploaded_image,
            );


            if ($sos_for==3) {

                

                if ($blocks_ids!="") {
                    $blockIdAppnedBlock=" AND block_id IN($blocks_ids)";
                } 

                // my block
                $m->set_data('send_to', $sos_for);
                
                $fcmArray = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND sos_alert=0 $blockIdAppned $blockIdAppnedBlock");
                $fcmArrayIos = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND sos_alert=0 $blockIdAppned $blockIdAppnedBlock");
                $nResident->noti("","", $society_id, $fcmArray, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nResident->noti_ios("","", $society_id, $fcmArrayIos, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

            } else if ($sos_for==4) {
                // all society
                $m->set_data('send_to', $sos_for);
               
                $fcmArray = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND sos_alert=0");
                $fcmArrayIos = $d->get_android_fcm("users_master", "delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND sos_alert=0");
                $nResident->noti("","", $society_id, $fcmArray, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nResident->noti_ios("","", $society_id, $fcmArrayIos, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

            } 

            if ($sos_for_new==1) {
                  // admin only
                $m->set_data('send_to', '5');
                $fcmArrayAdmin = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id'  AND device='android' AND sos_alert=0 AND sos_alert=0");
                $fcmArrayIosAdmin = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id' AND device!='android' AND sos_alert=0 AND sos_alert=0");
                $nAdmin->noti_new($society_id,"", $fcmArrayAdmin, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nAdmin->noti_ios_new($society_id,"", $fcmArrayIosAdmin, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

            }else if ($sos_for_new==2) {
                 // gatekeeper only
                $m->set_data('send_to', '6');
                $fcmArrayGuard = $d->get_emp_fcm("employee_master", "emp_token!='' AND emp_type_id='0'  AND society_id='$society_id' AND entry_status=1");
                $nGaurd->noti_new("",$fcmArrayGuard, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

            } else if ($sos_for_new==3) {
                // gatekeepr & admin
                $m->set_data('send_to', '7');

                $fcmArrayGuard = $d->get_emp_fcm("employee_master", "emp_token!='' AND emp_type_id='0'  AND society_id='$society_id' AND entry_status=1");
                $nGaurd->noti_new("",$fcmArrayGuard, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

                $fcmArrayAdmin = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id'  AND device='android' AND sos_alert=0 AND sos_alert=0");
                $fcmArrayIosAdmin = $d->get_admin_fcm("bms_admin_master", "admin_active_status=0 AND token!='' AND society_id='$society_id' AND device!='android' AND sos_alert=0 AND sos_alert=0");
                $nAdmin->noti_new($society_id,"", $fcmArrayAdmin, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);
                $nAdmin->noti_ios_new($society_id,"", $fcmArrayIosAdmin, "sos", "You have receive sos from $sos_by ($sos_unit)", $a);

            }

            if ($sos_for>0 && $sos_for_new>0) {
                $sos_for = $sos_for.$sos_for_new;
                $m->set_data('send_to', $sos_for);
            } 

            $aSos = array(
                'user_id' => $m->get_data('user_id'),
                'unit_id' => $m->get_data('unit_id'),
                'society_id' => $m->get_data('society_id'),
                'sos_msg ' => $m->get_data('sos_title'),
                'sos_time' => date("Y-m-d H:i:s"),
                'sos_latitude' => $m->get_data('sos_latitude'),
                'sos_longitude' => $m->get_data('sos_longitude'),
                'sos_image' => $m->get_data('sos_image'),
                'gatekeeper_id' => $m->get_data('gatekeeper_id'),
                'send_to' => $m->get_data('send_to'),
            );

            $d->insert("sos_request_master", $aSos);

            $response["message"] = "$sos_send_successfully";
            $response["status"] = "200";
            echo json_encode($response);
        }else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
