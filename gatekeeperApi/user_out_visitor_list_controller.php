<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

$societyLngName=  $xml->string->society;
    if ($key == $keydb) {
   

    $response = array();
    extract(array_map("test_input" , $_POST));
    if (isset($society_id)) {
        $qss=$d->select("society_master"," society_id ='$society_id'");
        $societyData = mysqli_fetch_array($qss); 
        $visitorMobilePrivacy = $societyData['visitor_mobile_number_show_gatekeeper'];        
    }

        if (isset($getStaffList) && $getStaffList == 'getStaffList' && $society_id!='') {

            $q = $d->select("employee_master,emp_type_master,staff_visit_master", "employee_master.emp_type_id =emp_type_master.emp_type_id AND employee_master.society_id='$society_id' AND staff_visit_master.emp_id = employee_master.emp_id AND staff_visit_master.visit_status ='0' AND employee_master.entry_status = '1' AND employee_master.emp_status=1","ORDER BY staff_visit_master.staff_visit_id DESC");

            $response["list"] = array();

            if (mysqli_num_rows($q) > 0) {
                while ($data = mysqli_fetch_array($q)) {
                    $list = array();
                    // $intTime=$data['visit_entry_date_time'];
                    //      $outTime=$temDate;
                    //       $date_a = new DateTime($intTime);
                    // $date_b = new DateTime($outTime);   
                    // $interval = date_diff($date_a,$date_b);
                    // $list["duration"] = $interval->format('%h:%i:%s');

                     $intTime=$data['visit_entry_date_time'];
                         $outTime=$temDate;
                          $date_a = new DateTime($intTime);
                    $date_b = new DateTime($outTime);
                    $interval = date_diff($date_a,$date_b);
                    // $list["duration"] = $interval->format('%d %h:%i:%s');
                    $totalDays= $interval->d;
                    $totalMinutes= $interval->i;
                    $totalHours= $interval->h;
                    if ($totalDays>0) {
                    $list["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                    } else {
                        $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                    }

                    $visitDate = date("d M Y h:i A", strtotime($data['visit_entry_date_time']));


                    $list["id"] = $data['emp_id'];
                    $list["user_id"] = '';
                    $list["name"] = $data['emp_name'];
                    $list["mobile"] = $data['emp_mobile'];
                    if ($data['private_resource']==1 || $visitorMobilePrivacy==0) {
                    $list["visitor_mobile_new"] = "".substr($data['emp_mobile'], 0, 3) . '****' . substr($data['emp_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$data['visitor_mobile'];
                    }
                    $list["visit_from"] = $data2['visit_from'].'';
                    $list["des_block"] = $data['emp_type_name'];
                    $list["visitor_type"] = '0';
                    $list["qr_code"] = $data['emp_id'];
                    $list["type"] = "staff";
                    $list["visit_date_time"] = $visitDate;
                    $list["profile_img"] = $base_url . "img/emp/" . $data['emp_profile'];
                    $list["qr_code_master_id"] = "";
                    $list["late_status"] = false;
                    $list["vehicle_no"] = '';
                    $list["parking_type"] = '';
                    array_push($response["list"], $list);
                }
                $temp = true;
            } else {
                $temp = false;
            }

             
            if ($access_blocks!="") {
                $q2 = $d->select("visitors_master,unit_master,block_master", "visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND visitors_master.visitor_status = '2' AND visitors_master.block_id IN ($access_blocks)","GROUP BY visitors_master.visitor_mobile ORDER BY visitors_master.visitor_id DESC");
            }else {
                $q2 = $d->select("visitors_master,unit_master,block_master", "visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND visitors_master.visitor_status = '2'","GROUP BY visitors_master.visitor_mobile ORDER BY visitors_master.visitor_id DESC");
            }

            if (mysqli_num_rows($q2) > 0) {
                while ($data2 = mysqli_fetch_array($q2)) {

                // get Parking data
                    $pq=$d->select("parking_master","visitor='$data2[visitor_id]'");
                    $parkingData=mysqli_fetch_array($pq);

                    $late_status=false;
                    $list = array();

                    $intTime=$data2['visit_date']." ".$data2['visit_time'];
                         $outTime=$temDate;
                          $date_a = new DateTime($intTime);
                $date_b = new DateTime($outTime);
                $interval = date_diff($date_a,$date_b);
                // $list["duration"] = $interval->format('%d %h:%i:%s');
                $totalDays= $interval->d;
                $totalMinutes= $interval->i;
                $totalHours= $interval->h;
                if ($totalDays>0) {
                $list["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                } else {
                    $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                }
                 $visitDate = date("d M Y h:i A", strtotime($data2['visit_date']." ".$data2['visit_time']));

                    $list["id"] = $data2['visitor_id'];
                    $list["user_id"] = $data2['user_id'];
                    $list["name"] = html_entity_decode($data2['visitor_name']);
                    $visit_from=str_replace("&amp;","&",$data2['visit_from']);
                    $list["visit_from"] = $visit_from.'';
                    $list["mobile"] = $data2['visitor_mobile'];
                    if ($visitorMobilePrivacy==0 && $data2['visitor_type']==0  OR  $visitorMobilePrivacy==0 && $data2['visitor_type']==1) {
                    $list["visitor_mobile_new"] = "".substr($data2['visitor_mobile'], 0, 3) . '****' . substr($data2['visitor_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$data2['visitor_mobile'];
                    }
                    $list["visitor_type"] = $data2['visitor_type'];
                    $list["des_block"] = $data2['block_name'] . "-" . $data2['unit_name'];
                    $list["visit_date_time"] = $visitDate;
                    $list["qr_code"] = $data2['visitor_id'];
                    $list["type"] = "Exvisitor";
                    $list["profile_img"] = $base_url . "img/visitor/" . $data2['visitor_profile'];
                    $list["qr_code_master_id"] = $data2['qr_code_master_id'];
                    $list["vehicle_no"] = $parkingData['vehicle_no'].'';
                    $list["parking_type"] = $parkingData['parking_type'].'';
                    if ($totalDays>0) {
                        $late_status=true;
                    } else if($totalHours>0) {
                        $late_status=true;
                    } else if($totalMinutes>29) {
                        $late_status=true;
                    } else {
                        $late_status=false;
                    } 
                    if($data2['visitor_type']==2 || $data2['visitor_type']==3) {
                        $list["late_status"] = $late_status;
                    } else {
                        $list["late_status"] = false;
                    }
                    array_push($response["list"], $list);
                }
                $temp2 = true;
            } else {
                $temp2 = false;
            }
            if ($temp==true || $temp2 ==true) {
                $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = ''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if (isset($getVisitorOutListNew) && $getVisitorOutListNew == 'getVisitorOutListNew' && $society_id!='') {

            $autoExitArray = array();

            $array1 = array(); // daily visitor
            $array2 = array(); // Common Visitor
            $array3 = array(); // Normal Visitor
            $array4 = array(); // Staff


            $response["list"] = array();
            
         
            $qDV = $d->select("daily_visitors_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "daily_visitors_master.visitor_id=visitors_master.daily_visitor_id AND visitors_master.visitor_type  = '4' AND daily_visitors_master.society_id='$society_id' AND daily_visitors_master.daily_active_status =0 AND daily_visitors_master.visitor_status =1 AND visitors_master.visitor_status=2", "ORDER BY visitors_master.visitor_id DESC");

            // daily visitor
            if (mysqli_num_rows($qDV) > 0) {
                while ($data = mysqli_fetch_array($qDV)) {
                    $dailyVisitor = array();

                    
                    $block_name = "Daily Visitor";

                    $intTime=$data['visit_date']." ".$data['visit_time'];
                    $overstay_time_limit=$data['overstay_time_limit'];
                    $outTime=$temDate;
                    $date_a = new DateTime($intTime);
                    $date_b = new DateTime($outTime);
                    $interval = date_diff($date_a,$date_b);
                    // $list["duration"] = $interval->format('%d %h:%i:%s');
                    $outTime=date("Y-m-d H:i:s");
                    $diffSeconds = strtotime($outTime) - strtotime($intTime);
                    $dailyVisitor["short_time"] =  $intTime;
                    $dailyVisitor["duration_sec"] = $diffSeconds;

                    $totalDays= $interval->d;
                    $totalMinutes= $interval->i;
                    $totalHours= $interval->h;
                    if ($totalDays>0) {
                        $dailyVisitor["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                    } else {
                        $dailyVisitor["duration"] = $interval->h.' hours '. $interval->i.' min';
                    }

                    if ($overstay_time_limit!='') {
                        $diffSecondsOverstay = strtotime($overstay_time_limit) - strtotime($outTime);
                    }

                    $visitDate =date("d M Y h:i A", strtotime($data['visit_date']." ".$data['visit_time']));


                    $dailyVisitor["id"] = $data['daily_visitor_id'];
                    $dailyVisitor["parent_visitor_id"] = $data['daily_visitor_id'];
                    $dailyVisitor["user_id"] ="0";
                    $dailyVisitor["name"] =html_entity_decode($data['visitor_name']);
                    $dailyVisitor["color_status"] = "";
                    $dailyVisitor["last_in"] = "";
                    $visit_from=html_entity_decode($data['visit_from']);
                    $dailyVisitor["visit_from"] = html_entity_decode($visit_from);
                    if ($visitorMobilePrivacy==0) {
                        $dailyVisitor["visitor_mobile_new"] = "".substr($data['visitor_mobile'], 0, 3) . '****' . substr($data['visitor_mobile'],  -3);
                    } else {
                        $dailyVisitor["visitor_mobile_new"] = $data['visitor_mobile'];
                    }
                    
                    $dailyVisitor["mobile"] = $data['visitor_mobile'];
                    $dailyVisitor["country_code"] = $data['country_code'];
                    
                    // $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                    // $vistLogo=mysqli_fetch_array($fd);
                     if ($data['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$data['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $dailyVisitor["visit_logo"] = $visit_logo.'';


                    
                    $dailyVisitor["visitor_type"] = $data['visitor_type'];
                    $dailyVisitor["number_of_visitor"] = $data['number_of_visitor'];
                    $dailyVisitor["des_block"] = "".$block_name;
                    $dailyVisitor["visit_date_time"] = $visitDate;
                    $dailyVisitor["qr_code"] = $data['visitor_id'];
                    $dailyVisitor["type"] = "daily";
                    $dailyVisitor["profile_img"] = $base_url . "img/visitor/" . $data['visitor_profile'];
                    $dailyVisitor["qr_code_master_id"] = $data['qr_code_master_id'];
                    $dailyVisitor["vehicle_no"] = html_entity_decode($data['vehicle_number']);
                    $dailyVisitor["parking_type"] = '';
                    $dailyVisitor["parking_id"] = $data['parking_id'];
                    if($diffSecondsOverstay<1) {
                        $late_status=true;
                        $late_msg = "Overstay";
                    } else {
                        $late_status=false;
                        $late_msg = "";
                    }
                    $dailyVisitor["late_status"] = $late_status;
                    $dailyVisitor["late_msg"] = $late_msg;


                    $dailyVisitor["workig_units"] ='View Working Units';
                    array_push($array1, $dailyVisitor);
                   
                }
                $temp3 = true;
            } else {
                $temp3 = false;
            }

            // common visitor
            $q11 = $d->select("visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "visitor_type = '5' AND   society_id = '$society_id' AND visitor_status = '2' ", "GROUP BY parent_visitor_id ORDER BY visitor_id DESC ");


            if (mysqli_num_rows($q11) > 0) {
                while ($data = mysqli_fetch_array($q11)) {
                    $commonVisitor = array();

                    if ($data['block_id']!=0 && $data['floor_id']!=0 && $data['unit_id']!=0) {
                        $gq= $d->select("block_master,unit_master","block_master.block_id=unit_master.block_id AND block_master.block_id='$data[block_id]' AND unit_master.unit_id='$data[unit_id]'");
                        $blockData=mysqli_fetch_array($gq);
                        $block_name = $blockData['block_name'].'-'.$blockData['unit_name'].' (Empty)';
                    } else {
                        $block_name = "Common Visitor";
                    }

                    $intTime=$data['visit_date']." ".$data['visit_time'];
                    $overstay_time_limit=$data['overstay_time_limit'];
                    $outTime=$temDate;
                    $date_a = new DateTime($intTime);
                    $date_b = new DateTime($outTime);
                    $interval = date_diff($date_a,$date_b);
                    // $list["duration"] = $interval->format('%d %h:%i:%s');
                    $outTime=date("Y-m-d H:i:s");
                    $diffSeconds = strtotime($outTime) - strtotime($intTime);
                    $commonVisitor["duration_sec"] = $diffSeconds;

                    $totalDays= $interval->d;
                    $totalMinutes= $interval->i;
                    $totalHours= $interval->h;
                    $commonVisitor["short_time"] =  $intTime;
                    if ($totalDays>0) {
                        $commonVisitor["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                    } else {
                        $commonVisitor["duration"] = $interval->h.' hours '. $interval->i.' min';
                    }

                    if ($overstay_time_limit!='') {
                        $diffSecondsOverstay = strtotime($overstay_time_limit) - strtotime($outTime);
                    }

                    $visitDate =date("d M Y h:i A", strtotime($data['visit_date']." ".$data['visit_time']));


                    $commonVisitor["id"] = $data['visitor_id'];
                    $commonVisitor["parent_visitor_id"] = $data['parent_visitor_id'];
                    $commonVisitor["user_id"] ="0";
                    $commonVisitor["name"] =html_entity_decode($data['visitor_name']);
                    $commonVisitor["color_status"] = "";
                    $commonVisitor["last_in"] = "";
                    $visit_from=html_entity_decode($data['visit_from']);
                    $commonVisitor["visit_from"] = $visit_from.'';
                    $commonVisitor["mobile"] = $data['visitor_mobile'];
                    $commonVisitor["country_code"] = $data['country_code'];
                    
                    // $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                    // $vistLogo=mysqli_fetch_array($fd);
                     if ($data['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$data['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $commonVisitor["visit_logo"] = $visit_logo.'';


                    if ($visitorMobilePrivacy==0 && $data['visitor_type']==0  OR  $visitorMobilePrivacy==0 && $data['visitor_type']==1) {
                    $commonVisitor["visitor_mobile_new"] = "".substr($data['visitor_mobile'], 0, 3) . '****' . substr($data['visitor_mobile'],  -3);
                    } else {
                    $commonVisitor["visitor_mobile_new"] = "".$data['visitor_mobile'];
                    }
                    $commonVisitor["visitor_type"] = $data['visitor_type'];
                    $commonVisitor["number_of_visitor"] = $data['number_of_visitor'];
                    $commonVisitor["des_block"] = "".$block_name;
                    $commonVisitor["visit_date_time"] = $visitDate;
                    $commonVisitor["qr_code"] = $data['visitor_id'];
                    $commonVisitor["type"] = "Exvisitor";
                    $commonVisitor["profile_img"] = $base_url . "img/visitor/" . $data['visitor_profile'];
                    $commonVisitor["qr_code_master_id"] = $data['qr_code_master_id'];
                    $commonVisitor["vehicle_no"] = html_entity_decode($data['vehicle_no']);
                    $commonVisitor["parking_type"] = '';
                    $commonVisitor["parking_id"] = $data['parking_id'];
                    if($diffSecondsOverstay<1) {
                        $late_status=true;
                        $late_msg = "Overstay";
                    } else {
                        $late_status=false;
                        $late_msg = "";
                    }

                    $commonVisitor["late_status"] = $late_status;
                    $commonVisitor["late_msg"] = $late_msg;
                     $commonVisitor["workig_units"] ="";
                    if ($totalDays<3) {
                        array_push($array2, $commonVisitor);
                    } else {
                        array_push($autoExitArray, $data['parent_visitor_id']);
                    }
                }
                $temp1 = true;
            } else {
                $temp1 = false;
            }

            
            // normal visitor
            if ($access_blocks!="") {
                $q2 = $d->select("unit_master,block_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND visitors_master.visitor_status = '2' AND visitors_master.visitor_type != '5' AND visitors_master.block_id IN ($access_blocks)","GROUP BY visitors_master.parent_visitor_id ORDER BY visitors_master.visitor_id DESC");
            }else {
                $q2 = $d->select("unit_master,block_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND visitors_master.visitor_status = '2' AND visitors_master.visitor_type != '5' ","GROUP BY visitors_master.parent_visitor_id ORDER BY visitors_master.visitor_id DESC");
            }

            if (mysqli_num_rows($q2) > 0) {
                while ($data2 = mysqli_fetch_array($q2)) {


                $late_status=false;
                $normalVisitor = array();

                $intTime=$data2['visit_date']." ".$data2['visit_time'];
                $overstay_time_limit=$data2['overstay_time_limit'];
                $outTime=$temDate;
                $date_a = new DateTime($intTime);
                $date_b = new DateTime($outTime);
                $interval = date_diff($date_a,$date_b);

                $normalVisitor["short_time"] =  $intTime;

                $outTime=date("Y-m-d H:i:s");
                $diffSeconds = strtotime($outTime) - strtotime($intTime);
                $normalVisitor["duration_sec"] = $diffSeconds;

                if ($overstay_time_limit!='') {
                    $diffSecondsOverstay = strtotime($overstay_time_limit) - strtotime($outTime);
                }

                $totalDaysTemp= $interval->days;
                $totalDays= $interval->d;
                $totalMinutes= $interval->i;
                $totalHours= $interval->h;
                if ($totalDays>0) {
                $normalVisitor["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                } else {
                    $normalVisitor["duration"] = $interval->h.' hours '. $interval->i.' min';
                }
                 $visitDate = date("d M Y h:i A", strtotime($data2['visit_date']." ".$data2['visit_time']));

                    

                    $normalVisitor["id"] = $data2['visitor_id'];
                    $normalVisitor["parent_visitor_id"] = $data2['parent_visitor_id'];
                    $normalVisitor["user_id"] = $data2['user_id'];
                    $normalVisitor["name"] = html_entity_decode($data2['visitor_name']);
                    $normalVisitor["color_status"] = "";
                    $normalVisitor["last_in"] = "";
                    $visit_from=str_replace("&amp;","&",$data2['visit_from']);
                    $normalVisitor["visit_from"] = $visit_from.'';
                    $normalVisitor["mobile"] = $data2['visitor_mobile'];
                    $normalVisitor["country_code"] = $data2['country_code'];
                   
                    if ($data2['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$data2['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $normalVisitor["visit_logo"] = $visit_logo.'';


                    if ($visitorMobilePrivacy==0 && $data2['visitor_type']==0 && $data2['expected_type']!=2 && $data2['expected_type']!=3 OR  $visitorMobilePrivacy==0 && $data2['visitor_type']==1 && $data2['expected_type']!=2 && $data2['expected_type']!=3) {
                    $normalVisitor["visitor_mobile_new"] = "".substr($data2['visitor_mobile'], 0, 3) . '****' . substr($data2['visitor_mobile'],  -3);
                    } else {
                    $normalVisitor["visitor_mobile_new"] = "".$data2['visitor_mobile'];
                    }
                    
                    $normalVisitor["visitor_type"] = $data2['visitor_type'];
                    $normalVisitor["number_of_visitor"] = $data2['number_of_visitor'];
                    $normalVisitor["expected_type"] = $data2['expected_type'];
                    
                    if ($data2['visiting_apartment']!="") {
                        $normalVisitor["des_block"] = $data2['visiting_apartment'];
                    } else {
                        $normalVisitor["des_block"] = $data2['block_name'] . "-" . $data2['unit_name'];
                    }
                    $normalVisitor["visit_date_time"] = $visitDate;
                    $normalVisitor["qr_code"] = $data2['visitor_id'];
                    $normalVisitor["type"] = "Exvisitor";
                    $normalVisitor["profile_img"] = $base_url . "img/visitor/" . $data2['visitor_profile'];
                    $normalVisitor["qr_code_master_id"] = $data2['qr_code_master_id'];
                    $normalVisitor["vehicle_no"] = html_entity_decode($data2['vehicle_no']);
                    if ($data2['expected_type']==2) {
                       $normalVisitor["parking_type"] = '1';
                    } else if ($data2['expected_type']==3) {
                        $normalVisitor["parking_type"] = '0';
                    } else {
                        $normalVisitor["parking_type"] = '';
                    }
                    
                    $normalVisitor["parking_id"] = $data2['parking_id'];


                    if($diffSecondsOverstay<1 && $data2['overstay_time_limit']!="" && $data2['overstay_time_limit']!="0000-00-00 00:00:00") {
                        $late_status=true;
                    } else {
                        $late_status=false;
                    }

                    if($late_status==true) {
                        $normalVisitor["late_status"] = $late_status;
                        $normalVisitor["late_msg"] = "Overstay";
                    } else {
                        $normalVisitor["late_status"] = false;
                        $normalVisitor["late_msg"] = "";

                    }
                    $normalVisitor["workig_units"] ="";
                    $normalVisitor["visitorType"] = $data2['visitor_type'].'~'.$totalDays;
                    if ($totalDaysTemp<30 && $data2['visitor_type']==0 || $totalDaysTemp<30 && $data2['visitor_type']==1 || $totalDaysTemp<1 && $data2['visitor_type']==2 || $totalDaysTemp<1 && $data2['visitor_type']==3 || $totalDaysTemp<1 && $data2['visitor_type']==5 || $totalDaysTemp<1 && $data2['visitor_type']==6) {
                        array_push($array3, $normalVisitor);
                    } else {
                       
                         array_push($autoExitArray, $data2['parent_visitor_id']);
                       
                    }
                }
                $temp2 = true;
            } else {
                $temp2 = false;
            }

            
            $q = $d->select("employee_master,emp_type_master,staff_visit_master", "employee_master.emp_type_id =emp_type_master.emp_type_id AND employee_master.society_id='$society_id' AND staff_visit_master.emp_id = employee_master.emp_id AND staff_visit_master.visit_status ='0' AND employee_master.entry_status = '1' AND employee_master.emp_status=1","ORDER BY employee_master.emp_name ASC");

           
            // staff
            if (mysqli_num_rows($q) > 0) {
                while ($data = mysqli_fetch_array($q)) {
                    $staffArray = array();

                    $today = date("Y-m-d");
                    $last_time = date("Y-m-d", strtotime($data['in_out_time']));
                    $visitDate = date("d M Y h:i A", strtotime($data['visit_entry_date_time']));
                    $earlier = new DateTime($today);
                    $later = new DateTime($visitDate);
                    $diff = $later->diff($earlier)->format("%a");
                    $outTime=date("Y-m-d H:i:s");
                    $diffSeconds = strtotime($outTime) - strtotime($visitDate);
                    $staffArray["duration_sec"] = $diffSeconds;
                   
                    if ($diff>29) {
                        $color_status='3';
                    } else if ($diff>15) {
                        $color_status='2';
                    } else {
                         $color_status='1';
                    }

                    $intTime=$data['visit_entry_date_time'];
                    $outTime=$temDate;
                    $date_a = new DateTime($intTime);
                    $date_b = new DateTime($outTime);
                    $interval = date_diff($date_a,$date_b);
                    $totalDays= $interval->d;
                    $totalMinutes= $interval->i;
                    $totalHours= $interval->h;

                    $staffArray["short_time"] =  $intTime;

                    if ($totalDays>0) {
                    $staffArray["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                    } else {
                        $staffArray["duration"] = $interval->h.' hours '. $interval->i.' min';
                    }

                    $staffArray["id"] = $data['emp_id'];
                    $staffArray["parent_visitor_id"] = $data['staff_visit_id'];
                    $staffArray["user_id"] = '';
                    $staffArray["name"] = html_entity_decode($data['emp_name']);
                    $staffArray["color_status"] = $color_status;
                    $staffArray["last_in"] = "";
                    $staffArray["mobile"] = $data['emp_mobile'];
                    $staffArray["country_code"] = $data['country_code'];
                    if ($data['private_resource']==1 || $visitorMobilePrivacy==0) {
                    $staffArray["visitor_mobile_new"] = "".substr($data['emp_mobile'], 0, 3) . '****' . substr($data['emp_mobile'],  -3);
                    } else {
                    $staffArray["visitor_mobile_new"] = "".$data['emp_mobile'];
                    }
                    $staffArray["visit_from"] = $data2['visit_from'].'';
                    $staffArray["des_block"] = $data['emp_type_name'];
                    $staffArray["visitor_type"] = '0';
                    $staffArray["number_of_visitor"] = "1";
                    $staffArray["qr_code"] = $data['emp_id'];
                    $staffArray["type"] = "staff";
                    $staffArray["visit_date_time"] = $visitDate;
                    $staffArray["profile_img"] = $base_url . "img/emp/" . $data['emp_profile'];
                    $staffArray["qr_code_master_id"] = "";
                    $staffArray["late_status"] = false;
                    $staffArray["late_msg"] = "";
                    $staffArray["vehicle_no"] = '';
                    $staffArray["parking_type"] = '';
                    $normalVisitor["parking_id"] = "";
                    
                    $workingUnitsAry = array();
                    if ($data['emp_type']==1) {
                        // $uq=$d->selectRow("block_master.block_name,unit_master.unit_name","unit_master,block_master,users_master,employee_unit_master ","users_master.delete_status=0 AND employee_unit_master .unit_id=unit_master.unit_id  AND
                        //     employee_unit_master .user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  employee_unit_master .emp_id ='$data[emp_id]' ","GROUP BY employee_unit_master.unit_id ORDER BY unit_master.unit_id ASC");
                        // $working_units= mysqli_num_rows($uq);
                        $workiUnits = 'View Working Units';
                        $staffArray["workig_units"] =$workiUnits."";
                    } else {
                        $staffArray["workig_units"] = "";
                    }
                    array_push($array4, $staffArray);
                }
                $temp = true;
            } else {
                $temp = false;
            }

            $newArray = array_merge($array1,$array2,$array3,$array4);
            foreach ($newArray as $key => $part) {
                   $sort[$key] = strtotime($part['short_time']);
            }
            array_multisort($sort, SORT_DESC, $newArray,SORT_DESC);
            for ($i=0; $i <count($newArray) ; $i++) { 
                $list = array(); 
                $list["short_time"]=$newArray[$i]['short_time'];
                $list["duration_sec"]=$newArray[$i]['duration_sec'];
                $list["duration"]=$newArray[$i]['duration'];
                $list["id"]=$newArray[$i]['id'];
                $list["parent_visitor_id"]=$newArray[$i]['parent_visitor_id'];
                $list["user_id"]=$newArray[$i]['user_id'];
                $list["name"]=$newArray[$i]['name'];
                $list["color_status"]=$newArray[$i]['color_status'];
                $list["last_in"]=$newArray[$i]['last_in'];
                $list["visit_from"]=$newArray[$i]['visit_from'];
                $list["visitor_mobile_new"]=$newArray[$i]['visitor_mobile_new'];
                $list["mobile"]=$newArray[$i]['mobile'];
                $list["country_code"]=$newArray[$i]['country_code'];
                $list["visit_logo"]=$newArray[$i]['visit_logo'];
                $list["visitor_type"]=$newArray[$i]['visitor_type'];
                $list["number_of_visitor"]=$newArray[$i]['number_of_visitor'];
                $list["expected_type"]=$newArray[$i]['expected_type'];
                $list["des_block"]=$newArray[$i]['des_block'];
                $list["visit_date_time"]=$newArray[$i]['visit_date_time'];
                $list["qr_code"]=$newArray[$i]['qr_code'];
                $list["type"]=$newArray[$i]['type'];
                $list["profile_img"]=$newArray[$i]['profile_img'];
                $list["qr_code_master_id"]=$newArray[$i]['qr_code_master_id'];
                $list["vehicle_no"]=$newArray[$i]['vehicle_no'];
                $list["parking_type"]=$newArray[$i]['parking_type'];
                $list["parking_id"]=$newArray[$i]['parking_id'];
                $list["late_status"]=$newArray[$i]['late_status'];
                $list["late_msg"]=$newArray[$i]['late_msg'];
                $list["workig_units"]=$newArray[$i]['workig_units'];
                array_push($response["list"], $list);
            }
           
                // auto exit after limit cross
               if (count($autoExitArray)>0) {
                    $ids = join("','",$autoExitArray); 
                   $aExit = array(
                     'visitor_status' => 3
                    );
                    $d->update("visitors_master", $aExit, "parent_visitor_id IN ('$ids')");
               }

            if ($temp2==true || $temp ==true || $temp1==true || $temp3==true) {
                $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = ''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }  else if ($_POST['deleteVisitor'] == "deleteVisitor") {
            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('visitor_status', '5');

            $a = array(
                'visitor_status' => $m->get_data('visitor_status')
            );
            $qdelete = $d->update("visitors_master", $a, "user_id='$user_id' AND visitor_id='$visitor_id' AND society_id='$society_id");

            if ($qdelete == true) {

                $response["message"] = ''.$xml->string->deleteeSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->failed;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 