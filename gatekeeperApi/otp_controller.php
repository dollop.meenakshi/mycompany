<?php

include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb) {
	$response = array();
	extract(array_map("test_input" , $_POST));
    $today = date('Y-m-d');
    

    if (isset($gatekeeper_login_new) && $gatekeeper_login_new=='gatekeeper_login_new' && isset($emp_mobile)) {

        $emp_mobile=mysqli_real_escape_string($con, $emp_mobile);

        $q=$d->select("employee_master","emp_mobile ='$emp_mobile' AND emp_type_id=0 AND society_id='$society_id' AND country_code='$country_code' AND employee_master.emp_status=1");


        $user_data = mysqli_fetch_array($q);

        if($user_data==TRUE ){

            if($user_data['emp_type_id']=='0' ){

            $emp_id = $user_data['emp_id'];
            $country_code = $user_data['country_code'];

            $digits = 6;
            $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);

            $m->set_data('otp', $otp);

            $a = array(
                'otp' => $m->get_data('otp'),
            );

            $d->update("employee_master",$a,"emp_id='$emp_id'");

            

            $qSociety = $d->select("society_master","society_id ='$society_id'");

            $user_data_society = mysqli_fetch_array($qSociety);
            $society_name=$user_data_society['society_name'];
                if($user_data_society['society_status']=='0'){
                    
                    $mobile =$user_data['emp_mobile'];
                    $to = $user_data['emp_email'];

                    if ($otp_type == 1 && $country_code=="+91") {
                        $smsObj->send_voice_otp($mobile, $otp,$country_code);
                        $d->add_sms_log($mobile,"Security Gurad Voice OTP SMS",$society_id,$country_code,1);
                        $you_will_receive_your_otp_on_call = $xml->string->you_will_receive_your_otp_on_call;
                        $response["message"] = "You will receive OTP on call";
                        $response["is_firebase"] = false;
                        
                    } else if ($to!='' && $otp_type == 2) {
                        $subject= "OTP Verification for $app_name Guard App.";
                        $admin_name = $user_data['emp_name'];
                        $msg= "$otp"; 
                        include '../apAdmin/mail/adminPanelLoginOTP.php';
                        include '../apAdmin/mail.php';
                        $otp_send_successfully = $xml->string->otpSendsucess;
                        $response["message"] = "$otp_send_successfully";
                        $response["is_firebase"] = false;
                    } else {
                        $msgRespone = $smsObj->send_otp_gatekeeper($mobile, $otp,$country_code);
                        $otp_send_successfully = $xml->string->otpSendsucess;
                        $response["message"] = "$otp_send_successfully";
                        
                        if ($msgRespone==true) {
                            $d->add_sms_log($mobile,"Security Gurad App OTP SMS",$society_id,$country_code,1);
                            $response["is_firebase"] = false;
                        } else {
                            $response["is_firebase"] = true;
                        }

                    }

                   
                    if ($to!='') {
                        $response["is_email_otp"] = true;
                    } else {
                        $response["is_email_otp"] = false;
                    }
                    if ($country_code != "+91") {
                        $response["is_voice_otp"] = false;
                    } else {
                        $response["is_voice_otp"] = true;
                    }
                    
                    
                    $response["message"]=''.$xml->string->otpSendsucess;
                    $response["status"]="200";
        
                    echo json_encode($response);


                }else{
                    $response["message"]=''.$xml->string->$societyDeactive;
                    $response["status"]="204";
                    echo json_encode($response);
                }
            
            }else{
                $response["message"]=''.$xml->string->accountNotactivate;
                $response["status"]="201";
                echo json_encode($response);
            }
    
        }else{

            $response["message"]=''.$xml->string->mobileNonotregister;
            $response["status"]="201";
            echo json_encode($response);
    
        }

    } else if (isset($gatekeeper_verify) && $gatekeeper_verify=='gatekeeper_verify' && isset($emp_mobile) && isset($otp)) {

        $emp_mobile=mysqli_real_escape_string($con, $emp_mobile);
        $otp=mysqli_real_escape_string($con, $otp);

      

        if($is_firebase == "true" || $emp_mobile=='9737564998' && $society_id==2) {
            $q=$d->select("employee_master","emp_mobile ='$emp_mobile' AND emp_type_id=0 AND society_id='$society_id' AND country_code='$country_code' AND employee_master.emp_status=1");
        } else {

            $q=$d->select("employee_master","emp_mobile ='$emp_mobile' AND otp='$otp' AND emp_type_id=0 AND society_id='$society_id' AND country_code='$country_code' AND employee_master.emp_status=1");
        }


        $user_data = mysqli_fetch_array($q);

        if($user_data==TRUE && mysqli_num_rows($q) == 1){

            if($user_data['emp_type_id']=='0' ){

            $response["emp_id"]=$user_data['emp_id'];

            $emp_id = $user_data['emp_id'];
            
            // $token = $user_data['emp_token'];
            // if ($token!=$emp_token) {
            //     $nGaurd->noti_new("",$token,"Logout","Logout","Logout");
            // }

            $m->set_data('emp_token', $emp_token);

            $a = array(
                // 'emp_token' => $m->get_data('emp_token'),
                'otp' =>''
            );

            $d->update("employee_master",$a,"emp_id='$emp_id'");

            $response["society_id"]=$user_data['society_id'];
            $society_id=$user_data['society_id'];

            $response["emp_name"]=$user_data['emp_name'];

            if($user_data['emp_profile']!=""){
                $response["emp_profile"]=$base_url."img/emp/".$user_data['emp_profile'];
            }else{
                $response["emp_profile"]=$base_url."img/emp/default.png";
            }

            $response["emp_mobile"]=$user_data['emp_mobile'];
            $response["country_code"]=$user_data['country_code'];
            $response["emp_email"]=$user_data['emp_email'];
            $response["emp_address"]=$user_data['emp_address'];
            $response["emp_date_of_joing"]=$user_data['emp_date_of_joing'];
            $response["emp_sallary"]=$user_data['emp_sallary'];

            if ($user_data['mpin']!='' && strlen($user_data['mpin'])==4) {
                $response["is_lock_mpin_genrated"]= true; 
            } else {
                $response["is_lock_mpin_genrated"]= false; 
            }

            $qSociety = $d->select("society_master","society_id ='$society_id'");

            $user_data_society = mysqli_fetch_array($qSociety);

            if($user_data_society['society_status']=='0'){

                    $response["society_name"]=html_entity_decode($user_data_society['society_name']);
                    $response["country_id"]=html_entity_decode($user_data_society['country_id']);
                    $response["society_address"]=html_entity_decode($user_data_society['society_address']);
                    $response["secretary_email"]=$user_data_society['secretary_email'];
                    $response["secretary_mobile"]=$user_data_society['secretary_mobile'];
                    $response["socieaty_logo"]=$base_url."img/society/".$user_data_society['socieaty_logo'];
                    $response["resident_in_out"]=$user_data_society['resident_in_out'];  //0 for Hide 1 for Show

                   
                    $response["child_security_menu_hide"]=false;  //0 for Hide 1 for Show
                    $response["message"]=$loginSucess;
                    $response["status"]="200";
                    $response["startTime"]=$user_data['start_time'];
                    $response["endTime"]=$user_data['end_time'];
                    $response["intervalDuration"]= $user_data['duration']; // 2 Minute
                    if ($user_data['ey_attendace']==0) {
                        $response["eye_attendace"]= false; 
                    } else {
                        $response["eye_attendace"]= true; 
                    }
                    if ($user_data['qr_attendace']==0) {
                        $response["qr_attendace"]= false; 
                    } else {
                        $response["qr_attendace"]= true; 
                    }
                    if ($user_data['qr_attendace']==1 && $user_data['qr_attendace_self']==0) {
                        $response["qr_attendace_self"]= true; 
                         $response["qr_attendace"]= false; 
                    } else {
                        $response["qr_attendace_self"]= false; 
                    }
                    $response["eye_attempt"]= 2; 

                    echo json_encode($response);

            }else{
                $response["message"]=''.$xml->string->societyDeactive;
                $response["status"]="204";
                echo json_encode($response);
            }
            
            }else{
                $response["message"]=''.$xml->string->accountNotactivate;
                $response["status"]="201";
                echo json_encode($response);
            }
    
        }else{

            $response["message"]=''.$xml->string->enterWrongotp;
            $response["status"]="201";
            echo json_encode($response);
    
        }

    } else if(isset($user_logout) && $user_logout=='user_logout'){
        
        $m->set_data('emp_token','');
      
        $a = array(
			'emp_token'=>$m->get_data('emp_token')
		);

        $qdelete = $d->update("employee_master",$a,"emp_id='$emp_id'");
        
        if($qdelete==TRUE){
            $response["message"]=''.$xml->string->logout;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]=''.$xml->string->somthingWrong;
            $response["status"]="201";
            echo json_encode($response);
        }
        
    }else if(isset($setNewLockMpin) && $setNewLockMpin=='setNewLockMpin'){

        $q11 = $d->select("employee_master","emp_id!='$emp_id' AND mpin='$mpin' ");
         if (mysqli_num_rows($q11)>0) {
            $response["message"]=''.$xml->string->pleaseEnteranotherMPIN;
            $response["status"]="201";
            echo json_encode($response);
            exit();
        }
        
        $m->set_data('mpin',$mpin);
      
        $a = array(
            'mpin'=>$m->get_data('mpin')
        );

        $qdelete = $d->update("employee_master",$a,"emp_id='$emp_id'");
        
        if($qdelete==TRUE){
            $response["message"]=''.$xml->string->setMPINsucess;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]=''.$xml->string->somthingWrong;
            $response["status"]="201";
            echo json_encode($response);
        }
        
    }else if(isset($changeMpinLock) && $changeMpinLock=='changeMpinLock'){

            $q11 = $d->select("employee_master","emp_id!='$emp_id' AND mpin='$mpin' ");
            if (mysqli_num_rows($q11)>0) {
                $response["message"]=''.$xml->string->pleaseEnteranotherMPIN;
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }
        
            $q = $d->select("employee_master","emp_id='$emp_id'");
            $data = mysqli_fetch_array($q);
            if ($data["mpin"]==$old_mpin) {


            $m->set_data('mpin',$mpin);
          
            $a = array(
                'mpin'=>$m->get_data('mpin')
            );

            $qdelete = $d->update("employee_master",$a,"emp_id='$emp_id'");
            
            if($qdelete==TRUE){
                $response["message"]=''.$xml->string->setMPINsucess;
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]=''.$xml->string->somthingWrong;
                $response["status"]="201";
                echo json_encode($response);
            }
        } else {
             $response["message"]=''.$xml->string->invalidMPINtryAgain;
                $response["status"]="201";
                echo json_encode($response);
        }
        
    } else if (isset($gatekeeper_verify_mpin) && $gatekeeper_verify_mpin=='gatekeeper_verify_mpin' && isset($mpin) ) {

        $mpin=mysqli_real_escape_string($con, $mpin);

        $temDate = date("Y-m-d H:i:s");
        $temFilter = date("Y-m-d");
        $temFiltertime = date("H:i:s");


        if ($emp_mobile!="" && $emp_mobile!="0") {
            $appendQuery = " AND emp_mobile='$emp_mobile' AND country_code='$country_code'";
        }
        $q=$d->select("employee_master","mpin='$mpin' AND emp_type_id=0 AND society_id='$society_id' $appendQuery");


        $user_data = mysqli_fetch_array($q);

        if($user_data==TRUE && mysqli_num_rows($q) == 1){
            $gatekeeper_name = $user_data['emp_name'];
            if($user_data['emp_status']=='1' && $user_data['emp_type_id']=='0' ){

            $response["emp_id"]=$user_data['emp_id'];

            $emp_id = $user_data['emp_id'];
            
            $token = $user_data['emp_token'];
            if ($token!=$emp_token) {
                $nGaurd->noti_new($emp_id,$token,"LogoutLock","LogoutLock","LogoutLock");
            }

            $m->set_data('emp_token', $emp_token);
            $m->set_data('entry_status', '1');
            $m->set_data('in_out_time', date('d-m-Y h:i A'));

            $m->set_data('emp_id', $emp_id);
            $m->set_data('society_id', $society_id);
            $m->set_data('visit_entry_date_time', $temDate);
            $m->set_data('visit_status', '0');
            $m->set_data('filter_data', $temFilter);

            $a = array(
                'emp_token' => $m->get_data('emp_token'),
                'entry_status' => $m->get_data('entry_status'),
                'in_out_time' => $m->get_data('in_out_time'),
            );

            $a111 = array(
                'emp_id' => $m->get_data('emp_id'),
                'society_id' => $m->get_data('society_id'),
                'visit_entry_date_time' => $m->get_data('visit_entry_date_time'),
                'visit_status' => $m->get_data('visit_status'),
                'filter_data' => $m->get_data('filter_data'),
                'emp_gate_keeper' => 1
            );

            $d->update("employee_master",$a,"emp_id='$emp_id'");

            // $d->insert("staff_visit_master",$a111);

            $response["society_id"]=$user_data['society_id'];
            $society_id=$user_data['society_id'];

            $response["emp_name"]=$user_data['emp_name'];

            if($user_data['emp_profile']!=""){
                $response["emp_profile"]=$base_url."img/emp/".$user_data['emp_profile'];
            }else{
                $response["emp_profile"]=$base_url."img/emp/default.png";
            }

            $response["emp_mobile"]=$user_data['emp_mobile'];
            $response["country_code"]=$user_data['country_code'];
            $response["emp_email"]=$user_data['emp_email'];
            $response["emp_address"]=$user_data['emp_address'];
            $response["emp_date_of_joing"]=$user_data['emp_date_of_joing'];
            $response["emp_sallary"]=$user_data['emp_sallary'];

            if ($user_data['mpin']!='' && strlen($user_data['mpin'])==4) {
                $response["is_lock_mpin_genrated"]= true; 
            } else {
                $response["is_lock_mpin_genrated"]= false; 
            }

           
            $response["child_security_menu_hide"]=false;  //0 for Hide 1 for Show

            $qSociety = $d->select("society_master","society_id ='$society_id'");

            $user_data_society = mysqli_fetch_array($qSociety);

                if($user_data_society['society_status']=='0'){

                        $response["society_name"]=html_entity_decode($user_data_society['society_name']);
                        $response["society_address"]=html_entity_decode($user_data_society['society_address']);
                        $response["secretary_email"]=$user_data_society['secretary_email'];
                        $response["secretary_mobile"]=$user_data_society['secretary_mobile'];
                        $response["socieaty_logo"]=$base_url."img/society/".$user_data_society['socieaty_logo'];
                        $response["resident_in_out"]=$user_data_society['resident_in_out'];  //0 for Hide 1 for Show
                        $response["message"]=''.$xml->string->hiYourjobStarted;
                        $response["status"]="200";
                        $response["startTime"]=$user_data['start_time'];
                        $response["endTime"]=$user_data['end_time'];
                        $response["intervalDuration"]= $user_data['duration']; // 2 Minute
                        if ($user_data['ey_attendace']==0) {
                            $response["eye_attendace"]= false; 
                        } else {
                            $response["eye_attendace"]= true; 
                        }
                        if ($user_data['qr_attendace']==0) {
                            $response["qr_attendace"]= false; 
                        } else {
                            $response["qr_attendace"]= true; 
                        }
                        if ($user_data['qr_attendace']==1 && $user_data['qr_attendace_self']==0) {
                            $response["qr_attendace_self"]= true; 
                        } else {
                            $response["qr_attendace_self"]= false; 
                        }
                        $response["eye_attempt"]= 2; 

                        echo json_encode($response);


                   
                    $title = "$gatekeeper_name has started his duty";
                    $description = "on ".date('d-m-Y h:i A');
                    
                    $adminFcmArray= $d->selectAdminInOut($emp_id,"android",'0');
                    $adminFcmArrayIos= $d->selectAdminInOut($emp_id,"ios",'0');

                    $nAdmin->noti_new($society_id,"", $adminFcmArray, $title, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");
                    $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $title, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");

                    $d->insertGuardDuatyNotification("buildingResourcesNew.png",$title,$description,"employee?manageEmployeeUnit=yes&emp_id=$emp_id",$society_id,$emp_id);

                }else{
                    $response["message"]=''.$xml->string->societyDeactive;
                    $response["status"]="204";
                    echo json_encode($response);
                }
            
            }else{
                $response["message"]=''.$xml->string->accountNotactivate;
                $response["status"]="201";
                echo json_encode($response);
            }
    
        }else{

            $response["message"]=''.$xml->string->invalidMPINtryAgain;
            $response["status"]="201";
            echo json_encode($response);
    
        }

    } else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);

    }

    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}

?>