<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d H:i");
    $temFilter = date("Y-m-d");
    $temFiltertime = date("H:i:s");

        if ($_POST['addParcel'] == "addParcel" && $society_id!='') {


        

        

         $digits = 6;
         $pass_code= rand(pow(10, $digits-1), pow(10, $digits)-1);

            if ($parcel_photo != "") {
                $ddd = date("ymdhi");
                $profile_name = "Parcel_" . $user_id .$ddd. '.png';
                define('UPLOAD_DIR', '../img/parcel/');
                $img = $parcel_photo;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $profile_name;
                $success = file_put_contents($file, $data);

                $notyUrl= $base_url.'img/parcel/'.$profile_name;
            } else {
               $profile_name= "MyParcelNew.png";
               $notyUrl='';
            }

            $m->set_data('society_id', $society_id);
            $m->set_data('parcel_photo', $profile_name);
            $m->set_data('parcel_id', $parcel_id);
            $m->set_data('no_of_parcel', $no_of_parcel);
            $m->set_data('unit_id', $unit_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('collected_by', $collected_by);
            $m->set_data('appoval_by', $user_id);
            $m->set_data('remark', $remark);
            $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
            $m->set_data('parcel_status', 1);
            $m->set_data('delivery_boy_name', $delivery_boy_name);
            $m->set_data('delivery_company', $delivery_company);
            $m->set_data('delivery_boy_number', $delivery_boy_number);
            $m->set_data('db_country_code', $country_code);
            $m->set_data('parcel_collected_date', $temDate);
            $m->set_data('pass_code', $pass_code);


                $a = array(
                    'society_id'=>$m->get_data('society_id'),
                    'parcel_photo'=>$m->get_data('parcel_photo'),
                    'parcel_id'=>$m->get_data('parcel_id'),
                    'no_of_parcel'=>$m->get_data('no_of_parcel'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'collected_by'=>$m->get_data('collected_by'),
                    'appoval_by' => $m->get_data('appoval_by'),
                    'remark' => $m->get_data('remark'),
                    'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                    'parcel_status'=>$m->get_data('parcel_status'),
                    'delivery_boy_name'=>$m->get_data('delivery_boy_name'),
                    'delivery_company'=>$m->get_data('delivery_company'),
                    'delivery_boy_number'=>$m->get_data('delivery_boy_number'),
                    'db_country_code'=>$m->get_data('db_country_code'),
                    'parcel_collected_date'=>$m->get_data('parcel_collected_date'),
                    'pass_code'=>$m->get_data('pass_code'),
                );


                $q = $d->insert("parcel_collect_master", $a);
                $parcel_collect_master_id =  $con->insert_id;

            if ($q == true) {

                $profile_name = "delivery_boy.png";
                $visit_date = $temFilter;
                
                $visit_time  =$temFiltertime;
                $visitDate =  $visit_date.' '.$visit_time;
                $nextHours= 1 *60;
                $timestamp = strtotime($visitDate) + 60*$nextHours;
                $valid_till_date = date('Y-m-d H:i:s', $timestamp);

                $quf=$d->selectRow("floor_id,block_id","unit_master","unit_id='$unit_id'");
                $unitData=mysqli_fetch_array($quf);
                $floor_id = $unitData['floor_id'];
                $block_id = $unitData['block_id'];

                $m->set_data('society_id',$society_id);
                $m->set_data('floor_id',$floor_id);
                $m->set_data('block_id',$block_id);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('visitor_type','2');
                $m->set_data('expected_type',0);
                $m->set_data('visit_from',$delivery_company);
                $m->set_data('visitor_name',$delivery_boy_name);
                $m->set_data('visitor_mobile',$delivery_boy_number);
                $m->set_data('country_code',$country_code);
                $m->set_data('visit_date',$visit_date);
                $m->set_data('visit_time',$visit_time);
                $m->set_data('valid_till_date',$valid_till_date);
                $m->set_data('visitor_status','3');
                $m->set_data('number_of_visitor', 1);
                $m->set_data('profile_name',$profile_name);
                $m->set_data('visitor_sub_type_id',$visitor_sub_type_id);
                $m->set_data('leave_parcel_at_gate',1);
                $m->set_data('parcel_collect_master_id',$parcel_collect_master_id);

                $avary = array(
                        'society_id'=>$m->get_data('society_id'),
                        'floor_id'=>$m->get_data('floor_id'),
                        'block_id'=>$m->get_data('block_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'visitor_type'=>$m->get_data('visitor_type'),
                        'expected_type'=>$m->get_data('expected_type'),
                        'visit_from'=>$m->get_data('visit_from'),
                        'visitor_name'=>$m->get_data('visitor_name'),
                        'visitor_mobile'=>$m->get_data('visitor_mobile'),
                        'country_code'=>$m->get_data('country_code'),
                        'visitor_profile'=>$m->get_data('profile_name'),
                        'number_of_visitor'=>$m->get_data('number_of_visitor'),
                        'visit_date'=>$m->get_data('visit_date'),
                        'visit_time'=>$m->get_data('visit_time'),
                        'exit_date'=>$m->get_data('visit_date'),
                        'exit_time'=>$m->get_data('visit_time'),
                        'valid_till_date'=>$m->get_data('valid_till_date'),
                        'visitor_status'=>$m->get_data('visitor_status'),
                        'number_of_visitor'=>$m->get_data('number_of_visitor'),
                        'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                        'leave_parcel_at_gate'=>$m->get_data('leave_parcel_at_gate'),
                        'parcel_collect_master_id'=>$parcel_collect_master_id,
                        'approve_by_id'=>$user_id,
                    );


                    $quserDataUpdate = $d->insert("visitors_master",$avary);
                    $parent_visitor_id = $con->insert_id;

                    $aPrnt = array(
                        'parent_visitor_id'=>$parent_visitor_id,
                    );

                    $d->update("visitors_master",$aPrnt,"visitor_id='$parent_visitor_id'");

                $qu=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
				$data_notification=mysqli_fetch_array($qu);
				 $token=$data_notification['user_token'];
                 $device=$data_notification['device'];
                $title = "Your Parcel from $delivery_company is Collected at Gate";
                $description = "Collected by $gatekeeper_name at $temDate";

                if ($device=='android') {
                   $nResident->noti("MyParcelActivity",$notyUrl,$society_id,$token,$title,$description,'parcel');
                }  else if($device=='ios') {
                   $nResident->noti_ios("ParcelVC",$notyUrl,$society_id,$token,$title,$description,'parcel');
                }


                 $notiAry = array(
                    'society_id'=>$society_id,
                    'user_id'=>$user_id,
                    'notification_title'=>$title,
                    'notification_desc'=>$description,
                    'notification_date'=>date('Y-m-d H:i'),
                    'notification_action'=>'parcel',
                    'notification_logo'=>'MyParcelNew.png',
                  );
                  
                $d->insert("user_notification",$notiAry);

                $response["message"] =''.$xml->string->parceladdSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->somthingWrong;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if (isset($getParcelList) && $getParcelList == 'getParcelList'  && $society_id != '') {

            if ($access_blocks!="") {
                $gatekkeperBlockAccess = " AND block_master.block_id IN($access_blocks)";
            }

            $qSosEvent = $d->select("parcel_collect_master,block_master,unit_master,floors_master,users_master", "parcel_collect_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.society_id ='$society_id' AND parcel_collect_master.user_id = users_master.user_id AND users_master.floor_id = floors_master.floor_id AND parcel_collect_master.parcel_status=1 $gatekkeperBlockAccess","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 5000");
                $response["collected_parcel"] = array();
                  $response["exp_parcel"] = array();
                  $response["deliverd_parcel"] = array();

            if (mysqli_num_rows($qSosEvent) > 0) {


                while ($data = mysqli_fetch_array($qSosEvent)) {

                    $collected_parcel = array();

                    $collected_parcel["parcel_collect_master_id"] = $data['parcel_collect_master_id'];
                    $collected_parcel["society_id"] = $data['society_id'];
                    if ($data['parcel_photo']=="MyParcelNew.png") {
                    $collected_parcel["parcel_photo"] = $base_url . 'img/MyParcelNew.png';
                    } else {
                    $collected_parcel["parcel_photo"] = $base_url . 'img/parcel/' . $data['parcel_photo'];
                    }
                    $collected_parcel["parcel_id"] = 'PAR0'.$data['parcel_collect_master_id'];
                    $collected_parcel["no_of_parcel"] = $data['no_of_parcel'];
                    $collected_parcel["unit_id"] = $data['unit_id'];
                    $collected_parcel["unit_name"] = $data['block_name'].'-'.$data['floor_name'];
                    $collected_parcel["user_id"] = $data['user_id'];
                    $fdu=$d->selectRow("user_first_name","users_master","user_id='$data[user_id]' AND user_status!=0");
                    $userData=mysqli_fetch_array($fdu);
                     if ($userData['user_first_name']!='') {
                            $appoval_by = $userData['user_first_name'].'';
                        } else {
                            $appoval_by="";
                        }

                    $fdu1=$d->selectRow("emp_name,emp_mobile,country_code","employee_master","emp_id='$data[collected_by]'");
                    $gatData=mysqli_fetch_array($fdu1);
                     if ($gatData['emp_name']!='') {
                        $collected_by = $gatData['emp_name'].'';
                        $gatekeeper_mobile = $gatData['country_code'].' '.$gatData['emp_mobile'].'';
                    } else {
                        $collected_by="Not Available";
                        $gatekeeper_mobile ='';
                    }

                    $fdu2=$d->selectRow("emp_name,emp_mobile,country_code","employee_master","emp_id='$data[deliverd_by]'");
                    $gatData1=mysqli_fetch_array($fdu2);
                     if ($gatData1['emp_name']!='') {
                        $deliverd_by_gatekeeper = $gatData1['emp_name'].'';
                        $deliverd_by_gatekeeper_mobile =$gatData1['country_code'].' '. $gatData1['emp_mobile'].'';
                    } else {
                        $deliverd_by_gatekeeper="";
                        $deliverd_by_gatekeeper_mobile ='';
                    }

                    $collected_parcel["collected_by"] = $collected_by;
                    $collected_parcel["gatekeeper_mobile"] = $gatekeeper_mobile;


                    $collected_parcel["deliverd_by_gatekeeper"] = $deliverd_by_gatekeeper;
                    $collected_parcel["deliverd_by_gatekeeper_mobile"] = $deliverd_by_gatekeeper_mobile;
                    
                    $collected_parcel["appoval_by"] = $appoval_by;
                    $collected_parcel["remark"] = $data['remark'];
                    $collected_parcel["visitor_sub_type_id"] = $data['visitor_sub_type_id'];
                    $collected_parcel["parcel_status"] = $data['parcel_status'];
                    $collected_parcel["delivery_boy_name"] = html_entity_decode($data['delivery_boy_name']);
                    if ($data['delivery_boy_number']==0) {
                        # code...
                        $collected_parcel["delivery_boy_number"] ="";
                        $collected_parcel["country_code"] ="";
                    } else {
                        $collected_parcel["delivery_boy_number"] = $data['db_country_code'].' '.$data['delivery_boy_number'];
                        $collected_parcel["country_code"] = $data['db_country_code'];
                    }
                    $collected_parcel["delivery_company"] = html_entity_decode($data['delivery_company']);
                    
                    $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                    $vistLogo=mysqli_fetch_array($fd);
                     if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $collected_parcel["delivery_companylogo"] = $visit_logo.'';
                    $collected_parcel["parcel_collected_date"] =  date("d M Y h:i A", strtotime($data['parcel_collected_date']));
                    if ($data['parcel_deliverd_date']=="0000-00-00 00:00:00") {
                        $collected_parcel["parcel_deliverd_date"] =  "Yet not collected";
                        
                    } else {
                        $collected_parcel["parcel_deliverd_date"] =  date("d M Y h:i A", strtotime($data['parcel_deliverd_date']));

                    }

                    array_push($response["collected_parcel"], $collected_parcel);
                } 

                    $query1= true;
            }else {
                $query1= false;
            }


                $q2 = $d->select("parcel_collect_master,block_master,unit_master,users_master,floors_master", "parcel_collect_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.society_id ='$society_id' AND parcel_collect_master.parcel_status=0 AND users_master.user_id = parcel_collect_master.user_id AND users_master.floor_id = floors_master.floor_id","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 3000");

            if (mysqli_num_rows($q2) > 0) {

              

                while ($data = mysqli_fetch_array($q2)) {

                    $exp_parcel = array();

                    $exp_parcel["parcel_collect_master_id"] = $data['parcel_collect_master_id'];
                    $exp_parcel["society_id"] = $data['society_id'];
                    if ($data['parcel_photo']=="MyParcelNew.png") {
                    $exp_parcel["parcel_photo"] = $base_url . 'img/MyParcelNew.png';
                    } else {
                    $exp_parcel["parcel_photo"] = $base_url . 'img/parcel/' . $data['parcel_photo'];
                    }
                    $exp_parcel["parcel_id"] = 'PAR0'.$data['parcel_collect_master_id'];
                    $exp_parcel["no_of_parcel"] = $data['no_of_parcel'];
                    $exp_parcel["unit_id"] = $data['unit_id'];
                    $exp_parcel["unit_name"] = $data['block_name'].'-'.$data['floor_name'];
                    $exp_parcel["user_id"] = $data['user_id'];
                    $fdu=$d->selectRow("user_first_name","users_master","user_id='$data[user_id]'");
                    $userData=mysqli_fetch_array($fdu);
                     if ($userData['user_first_name']!='') {
                            $appoval_by = $userData['user_first_name'].'';
                        } else {
                            $appoval_by="";
                        }


                    $exp_parcel["appoval_by"] = $appoval_by;
                    $exp_parcel["remark"] = $data['remark'];
                    $exp_parcel["visitor_sub_type_id"] = $data['visitor_sub_type_id'];
                    $exp_parcel["parcel_status"] = $data['parcel_status'];
                    $exp_parcel["delivery_boy_name"] = $data['delivery_boy_name'];
                     if ($data['delivery_boy_number']==0) {
                        # code...
                        $exp_parcel["delivery_boy_number"] ="";
                        $exp_parcel["country_code"] ="";
                    } else {
                        $exp_parcel["delivery_boy_number"] = html_entity_decode($data['delivery_boy_number']);
                        $exp_parcel["country_code"] = $data['db_country_code'].' '.$data['db_country_code'];
                    }

                    $exp_parcel["collected_by"] = "";
                    $exp_parcel["gatekeeper_mobile"] = "";

                    
                    $exp_parcel["delivery_company"] = html_entity_decode($data['delivery_company']);
                    
                    $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                    $vistLogo=mysqli_fetch_array($fd);
                     if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $exp_parcel["delivery_companylogo"] = $visit_logo.'';
                    $exp_parcel["parcel_collected_date"] =  date("d M Y h:i A", strtotime($data['parcel_collected_date']));
                    if ($data['parcel_deliverd_date']=="0000-00-00 00:00:00") {
                        $exp_parcel["parcel_deliverd_date"] =  "Yet not collected";
                        
                    } else {
                        $exp_parcel["parcel_deliverd_date"] =  date("d M Y h:i A", strtotime($data['parcel_deliverd_date']));

                    }

                    $cTime = date("H:i:s");
                    $expire = strtotime($data['valid_till_date_parcel']);
                    $today = strtotime("today $cTime");
                    if($today >= $expire && $data['valid_till_date_parcel']!=''){
                        $d->delete("parcel_collect_master", "parcel_collect_master_id='$data[parcel_collect_master_id]' ");
                        $d->delete("visitors_master", "parcel_collect_master_id='$data[parcel_collect_master_id]' ");
                    } 

                    array_push($response["exp_parcel"], $exp_parcel);

                } 
                    $query2 = true;

            }else {
            $query2= false;
            }


             $q3 = $d->select("parcel_collect_master,block_master,unit_master,users_master,floors_master", "parcel_collect_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.society_id ='$society_id' AND parcel_collect_master.parcel_status=2  AND parcel_collect_master.parcel_status=0 AND users_master.user_id = parcel_collect_master.user_id AND users_master.floor_id = floors_master.floor_id","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 400");

            if (mysqli_num_rows($q3) > 0) {

              

                while ($data = mysqli_fetch_array($q3)) {

                    $deliverd_parcel = array();

                    $deliverd_parcel["parcel_collect_master_id"] = $data['parcel_collect_master_id'];
                    $deliverd_parcel["society_id"] = $data['society_id'];
                    if ($data['parcel_photo']=="MyParcelNew.png") {
                    $deliverd_parcel["parcel_photo"] = $base_url . 'img/MyParcelNew.png';
                    } else {
                    $deliverd_parcel["parcel_photo"] = $base_url . 'img/parcel/' . $data['parcel_photo'];
                    }
                    $deliverd_parcel["parcel_id"] = 'PAR0'.$data['parcel_collect_master_id'];
                    $deliverd_parcel["no_of_parcel"] = $data['no_of_parcel'];
                    $deliverd_parcel["unit_id"] = $data['unit_id'];
                    $deliverd_parcel["unit_name"] = $data['block_name'].'-'.$data['floor_name'];
                    $deliverd_parcel["user_id"] = $data['user_id'];
                    $fdu=$d->selectRow("user_first_name","users_master","user_id='$data[user_id]'");
                    $userData=mysqli_fetch_array($fdu);
                     if ($userData['user_first_name']!='') {
                            $appoval_by = $userData['user_first_name'].'';
                        } else {
                            $appoval_by="";
                        }

                    $fdu1=$d->selectRow("emp_name,emp_mobile,country_code","employee_master","emp_id='$data[collected_by]'");
                    $gatData=mysqli_fetch_array($fdu1);
                     if ($gatData['emp_name']!='') {
                        $collected_by = $gatData['emp_name'].'';
                        $gatekeeper_mobile = $gatData['country_code'].' '.$gatData['emp_mobile'].'';
                    } else {
                        $collected_by="Not Available";
                        $gatekeeper_mobile ='';
                    }


                    $fdu2=$d->selectRow("emp_name,emp_mobile,country_code","employee_master","emp_id='$data[deliverd_by]'");
                    $gatData1=mysqli_fetch_array($fdu2);
                     if ($gatData1['emp_name']!='') {
                        $deliverd_by_gatekeeper = $gatData1['emp_name'].'';
                        $deliverd_by_gatekeeper_mobile = $gatData1['country_code'].' '.$gatData1['emp_mobile'].'';
                    } else {
                        $deliverd_by_gatekeeper="";
                        $deliverd_by_gatekeeper_mobile ='';
                    }

                    $deliverd_parcel["collected_by"] = $collected_by;
                    $deliverd_parcel["gatekeeper_mobile"] = $gatekeeper_mobile;


                    $deliverd_parcel["deliverd_by_gatekeeper"] = $deliverd_by_gatekeeper;
                    $deliverd_parcel["deliverd_by_gatekeeper_mobile"] = $deliverd_by_gatekeeper_mobile;
                    
                    $deliverd_parcel["appoval_by"] = $appoval_by;
                    $deliverd_parcel["remark"] = $data['remark'];
                    $deliverd_parcel["visitor_sub_type_id"] = $data['visitor_sub_type_id'];
                    $deliverd_parcel["parcel_status"] = $data['parcel_status'];
                    $deliverd_parcel["delivery_boy_name"] = html_entity_decode($data['delivery_boy_name']);
                    if ($data['delivery_boy_number']==0) {
                        $deliverd_parcel["delivery_boy_number"] ="";
                        $deliverd_parcel["country_code"] ="";
                    } else {
                        $deliverd_parcel["delivery_boy_number"] = $data['db_country_code'].' '.$data['delivery_boy_number'];
                        $deliverd_parcel["country_code"] = $data['db_country_code'];
                    }
                    $deliverd_parcel["delivery_company"] = html_entity_decode($data['delivery_company']);
                    
                    $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                    $vistLogo=mysqli_fetch_array($fd);
                     if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $deliverd_parcel["delivery_companylogo"] = $visit_logo.'';
                    $deliverd_parcel["parcel_collected_date"] =  date("d M Y h:i A", strtotime($data['parcel_collected_date']));
                    if ($data['parcel_deliverd_date']=="0000-00-00 00:00:00") {
                        $deliverd_parcel["parcel_deliverd_date"] =  "Yet not collected";
                        
                    } else {
                        $deliverd_parcel["parcel_deliverd_date"] =  date("d M Y h:i A", strtotime($data['parcel_deliverd_date']));

                    }

                    array_push($response["deliverd_parcel"], $deliverd_parcel);
                    $query3 = true;

                } 

            }else {
            $query3= false;
            }
 
            if ($query2==true || $query1==true  || $query3==true) {

                $response["message"] =''.$xml->string->parcelList;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->noDatafound;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if (isset($getParcelDetails) && $getParcelDetails == 'getParcelDetails'  && $society_id != '') {
            $qSosEvent = $d->select("parcel_collect_master,block_master,unit_master", "parcel_collect_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.society_id ='$society_id'  AND parcel_collect_master.parcel_collect_master_id='$parcel_collect_master_id'","");
                $response["collected_parcel"] = array();
                  $response["exp_parcel"] = array();
                  $response["deliverd_parcel"] = array();

            if (mysqli_num_rows($qSosEvent) > 0) {


                while ($data = mysqli_fetch_array($qSosEvent)) {

                    $collected_parcel = array();

                    $collected_parcel["parcel_collect_master_id"] = $data['parcel_collect_master_id'];
                    $collected_parcel["society_id"] = $data['society_id'];
                    if ($data['parcel_photo']=="MyParcelNew.png") {
                    $collected_parcel["parcel_photo"] = $base_url . 'img/MyParcelNew.png';
                    } else {
                    $collected_parcel["parcel_photo"] = $base_url . 'img/parcel/' . $data['parcel_photo'];
                    }
                    $collected_parcel["parcel_id"] = 'PAR0'.$data['parcel_collect_master_id'];
                    $collected_parcel["no_of_parcel"] = $data['no_of_parcel'];
                    $collected_parcel["unit_id"] = $data['unit_id'];
                    $collected_parcel["unit_name"] = $data['block_name'].'-'.$data['unit_name'];
                    $collected_parcel["user_id"] = $data['user_id'];
                    $collected_parcel["collected_by"] = $data['collected_by'];
                    $fdu=$d->selectRow("user_first_name","users_master","user_id='$data[user_id]'");
                    $userData=mysqli_fetch_array($fdu);
                     if ($userData['user_first_name']!='') {
                            $appoval_by = $userData['user_first_name'].'';
                        } else {
                            $appoval_by="";
                        }
                    $collected_parcel["appoval_by"] = $appoval_by;
                    $collected_parcel["remark"] = $data['remark'];
                    $collected_parcel["visitor_sub_type_id"] = $data['visitor_sub_type_id'];
                    $collected_parcel["parcel_status"] = $data['parcel_status'];
                    $collected_parcel["delivery_boy_name"] = html_entity_decode($data['delivery_boy_name']);

                    if ($data['delivery_boy_number']==0) {
                        $collected_parcel["delivery_boy_number"] ="";
                        $collected_parcel["country_code"] ="";
                    } else {
                        $collected_parcel["delivery_boy_number"] = $data['delivery_boy_number'];
                        $collected_parcel["country_code"] = $data['db_country_code'];
                    }
                    
                    $collected_parcel["delivery_company"] = html_entity_decode($data['delivery_company']);
                    
                    $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                    $vistLogo=mysqli_fetch_array($fd);
                     if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $collected_parcel["delivery_companylogo"] = $visit_logo.'';
                    $collected_parcel["parcel_collected_date"] =  date("d M Y h:i A", strtotime($data['parcel_collected_date']));
                    if ($data['parcel_deliverd_date']=="0000-00-00 00:00:00") {
                        $collected_parcel["parcel_deliverd_date"] =  "Yet not collected";
                        
                    } else {
                        $collected_parcel["parcel_deliverd_date"] =  date("d M Y h:i A", strtotime($data['parcel_deliverd_date']));

                    }

                    array_push($response["collected_parcel"], $collected_parcel);
                } 

                    $response["message"] =''.$xml->string->parcelDetail;
                    $response["status"] = "200";
                    echo json_encode($response);
            }else {
                $response["message"] = ''.$xml->string->somthingWrong;
                $response["status"] = "201";
                echo json_encode($response);
            }



        }else if (isset($parcelReceivedFromDb) && $parcelReceivedFromDb == 'parcelReceivedFromDb'  && $society_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($parcel_collect_master_id, FILTER_VALIDATE_INT) == true  ) {

                 $digits = 6;
                $pass_code= rand(pow(10, $digits-1), pow(10, $digits)-1);

                if ($parcel_photo != "") {
                    $ddd = date("ymdhi");
                    $profile_name = "Parcel_" . $user_id .$ddd. '.png';
                    define('UPLOAD_DIR', '../img/parcel/');
                    $img = $parcel_photo;
                    $img = str_replace('data: img/app/png;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);
                    $file = UPLOAD_DIR . $profile_name;
                    $success = file_put_contents($file, $data);

                    $notyUrl= $base_url.'img/parcel/'.$profile_name;
                } else {
                   $profile_name= "MyParcelNew.png";
                   $notyUrl='';
                }


                $m->set_data('parcel_status', 1);
                $m->set_data('parcel_photo', $profile_name);
                $m->set_data('no_of_parcel', $no_of_parcel);
                $m->set_data('delivery_boy_name', $delivery_boy_name);
                $m->set_data('delivery_boy_number', $delivery_boy_number);
                $m->set_data('db_country_code', $country_code);
                $m->set_data('collected_by', $collected_by);
                $m->set_data('parcel_collected_date', date("Y-m-d H:i"));
                $m->set_data('collected_by', $collected_by);
                $m->set_data('remark', $remark);
                $m->set_data('pass_code', $pass_code);
                $m->set_data('out_temperature', $in_temperature);
                $m->set_data('out_with_mask', $in_with_mask);
                $m->set_data('emp_id', $collected_by);
                $m->set_data('exit_by', $collected_by);

                 $a = array(
                    'parcel_photo'=>$m->get_data('parcel_photo'),
                    'no_of_parcel'=>$m->get_data('no_of_parcel'),
                    'delivery_boy_name'=>$m->get_data('delivery_boy_name'),
                    'delivery_boy_number'=>$m->get_data('delivery_boy_number'),
                    'db_country_code'=>$m->get_data('db_country_code'),
                    'parcel_status'=>$m->get_data('parcel_status'),
                    'collected_by'=>$m->get_data('collected_by'),
                    'remark'=>$m->get_data('remark'),
                    'parcel_collected_date'=>$m->get_data('parcel_collected_date'),
                    'pass_code'=>$m->get_data('pass_code'),
                );

                $q = $d->update("parcel_collect_master", $a, "parcel_collect_master_id='$parcel_collect_master_id' AND user_id='$user_id'");
                if ($q>0) {

                    $m->set_data('visitor_status', '3');
                    $m->set_data('visit_date', $temFilter);
                    $m->set_data('visit_time', $temFiltertime);

                    $avisitor = array(
                        'visitor_name'=>$m->get_data('delivery_boy_name'),
                        'visitor_mobile'=>$m->get_data('delivery_boy_number'),
                        'country_code'=>$m->get_data('db_country_code'),
                        'visit_date' => $m->get_data('visit_date'),
                        'visit_time' => $m->get_data('visit_time'),
                        'exit_date' => $m->get_data('visit_date'),
                        'exit_time' => $m->get_data('visit_time'),
                        'visitor_status' => $m->get_data('visitor_status'),
                        'out_temperature' => $m->get_data('out_temperature'),
                        'out_with_mask' => $m->get_data('out_with_mask'),
                        'emp_id' => $m->get_data('emp_id'),
                        'exit_by' => $m->get_data('exit_by'),
                    );

                    $qdelete = $d->update("visitors_master", $avisitor, "parcel_collect_master_id='$parcel_collect_master_id' AND parcel_collect_master_id!=0");

                    $qu=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
                    $data_notification=mysqli_fetch_array($qu);
                    $token=$data_notification['user_token'];
                    $device=$data_notification['device'];
                    $title = "Your Parcel from $delivery_company is Collected at Gate";
                    $description = "Collected by $gatekeeper_name at $temDate";

                    if ($device=='android') {
                       $nResident->noti("MyParcelActivity",$notyUrl,$society_id,$token,$title,$description,'parcel');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("ParcelVC",$notyUrl,$society_id,$token,$title,$description,'parcel');
                    }


                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$user_id,
                        'notification_title'=>$title,
                        'notification_desc'=>$description,
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'parcel',
                        'notification_logo'=>'MyParcelNew.png',
                      );
                      
                    $d->insert("user_notification",$notiAry);

                    $response["message"] = ''.$xml->string->parcelRecivedsucess;
                    $response["status"] = "200";
                    echo json_encode($response);
                } else {
                    $response["message"] = ''.$xml->string->somthingWrong;
                    $response["status"] = "201";
                    echo json_encode($response);
                }


            # code...
        } else if (isset($deliverdParceltoUser) && $deliverdParceltoUser == 'deliverdParceltoUser'  && $society_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($parcel_collect_master_id, FILTER_VALIDATE_INT) == true  ) {
           
                $qqqq=$d->selectRow("pass_code","parcel_collect_master","parcel_collect_master_id='$parcel_collect_master_id' AND pass_code='$pass_code' AND pass_code !=0 AND pass_code !=''");
                if (mysqli_num_rows($qqqq)==0) {
                    $response["message"] =''.$xml->string->enterWrongacessCode;
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();

                }

                $m->set_data('parcel_status', 2);
                $m->set_data('deliverd_by', $gatekeeper_id);
                $m->set_data('parcel_deliverd_date', date("Y-m-d H:i"));

                 $a = array(
                    'parcel_status'=>$m->get_data('parcel_status'),
                    'deliverd_by'=>$m->get_data('deliverd_by'),
                    'parcel_deliverd_date'=>$m->get_data('parcel_deliverd_date'),
                );

                $q = $d->update("parcel_collect_master", $a, "parcel_collect_master_id='$parcel_collect_master_id' AND user_id='$user_id'");
                if ($q>0) {

                    $qu=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
                    $data_notification=mysqli_fetch_array($qu);
                    $token=$data_notification['user_token'];
                    $device=$data_notification['device'];
                    $title = "Your Parcel from $delivery_company is Delivered ";
                    $description = "Delivered by $gatekeeper_name at $temDate";

                    if ($device=='android') {
                       $nResident->noti("MyParcelActivity",$notyUrl,$society_id,$token,$title,$description,'parcel');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("ParcelVC",$notyUrl,$society_id,$token,$title,$description,'parcel');
                    }


                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$user_id,
                        'notification_title'=>$title,
                        'notification_desc'=>$description,
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'parcel',
                        'notification_logo'=>'MyParcelNew.png',
                      );
                      
                    $d->insert("user_notification",$notiAry);

                    $response["message"] = ''.$xml->string->parcelDelivesucess;
                    $response["status"] = "200";
                    echo json_encode($response);
                } else {
                    $response["message"] = ''.$xml->string->somthingWrong;
                    $response["status"] = "201";
                    echo json_encode($response);
                }


        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 