<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
    $date = date('Y-m-d H:i:s');

        if ($_POST['getPrvChat'] == "getPrvChat" && $society_id!='' && $user_id!='') {

            $qnotification = $d->select("chat_master", "society_id='$society_id' AND ((msg_by='$user_id' AND msg_for='$userId') OR (msg_by='$userId' AND msg_for='$user_id')) AND ((send_by='0' AND sent_to='1') OR (send_by='1' AND sent_to='0'))","ORDER BY chat_id ASC");

            if (mysqli_num_rows($qnotification)>0) {
                
                $response["chat"] = array();

                while ($data_notification = mysqli_fetch_array($qnotification)) {
                    $data_notification = array_map("html_entity_decode", $data_notification);

                    $chat = array();

                    $chat["chat_id"] = $data_notification['chat_id'];
                    $chat["society_id"] = $data_notification['society_id'];
                    $chat["msg_by"] = $data_notification['msg_by'];
                    $chat["msg_for"] = $data_notification['msg_for'];
                    $chat["msg_data"] = $data_notification['msg_data'];
                    $chat["msg_type"] = $data_notification['msg_type'];
                    if ($data_notification['msg_img']!='') {
                        $chat["msg_img"] = $base_url."img/chatImg/".$data_notification['msg_img'];
                    } else {
                        $chat["msg_img"] = "";
                    }
                    $chat["msg_status"] = $data_notification['msg_status'];
                    $chat["msg_delete"] = $data_notification['msg_delete'];
                    $chat["msg_date"] = date("d M Y h:i A", strtotime($data_notification['msg_date']));
                    
                    
                    $chat_id=$data_notification['chat_id'];

                  if($data_notification['msg_by']==$user_id && $data_notification['sent_to']==0){

                       $chat["my_msg"] = '1';

                   }else{
                       $chat["my_msg"] = '0';

                       $m->set_data('msg_status','1');

                       $a1 = array(
                           'msg_status' => $m->get_data('msg_status'));

                       $d->update('chat_master', $a1,"chat_id='$chat_id'");
                   }
                   
                    array_push($response["chat"],$chat);
                }

                if ($isRead==1) {
                   
                        $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$userId'");
                        $data_notification = mysqli_fetch_array($qsm);
                        $sos_user_token = $data_notification['user_token'];
                        $device = $data_notification['device'];
                        if ($device == 'android') {
                            $nResident->noti("chatMsg","", $society_id, $sos_user_token, "", "", 'chatMsg');
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("chatMsg","", $society_id, $sos_user_token, "", "", 'chatMsg');
                        }
                }

                $response["message"] =''.$xml->string->getChatsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noChatfound;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['addChat'] == "addChat" && $society_id!='' ) {

            $m->set_data('society_id',$society_id);
            $m->set_data('msg_by',$msg_by);
            $m->set_data('msg_for',$msg_for);
            $m->set_data('msg_data',$msg_data);
            $m->set_data('msg_status','0');
            $m->set_data('send_by','1');
            $m->set_data('sent_to',$sent_to);
            $m->set_data('msg_date',$date);
            $a1 = array(
                'society_id' => $m->get_data('society_id'),
                'msg_by' => $m->get_data('msg_by'),
                'msg_for' => $m->get_data('msg_for'),
                'msg_data' => $m->get_data('msg_data'),
                'msg_status' => $m->get_data('msg_status'),
                'send_by' => $m->get_data('send_by'),
                'sent_to' => $m->get_data('sent_to'),
                'msg_date' => $m->get_data('msg_date')
            );

            $q = $d->insert('chat_master', $a1);


            if ($q == true) {


                if($sent_to=='1'){

                    $qGaurdToken=$d->select("employee_master","emp_type_id='1' AND society_id='$society_id' AND emp_id='$msg_for'");	

					$data_Gaurd=mysqli_fetch_array($qGaurdToken);
	
					$sos_Gaurd_token=$data_Gaurd['emp_token'];

					$nGaurd->noti_new("",$sos_user_token,"$gt_name",$msg_data,'chatMsg');


                }else if($sent_to=='0'){

                    $qGaurdToken=$d->select("employee_master"," emp_id='$msg_by'");    

                    $data_Gaurd=mysqli_fetch_array($qGaurdToken);

                    $notAry = array(
                        'userType' => "gaurd",
                        'userId' => $m->get_data('msg_by'),
                        'userProfile' =>$base_url."img/emp/".$data_Gaurd['emp_profile'],
                        'userName' => $gt_name,
                        'from' => "1",
                        'sentTo' => "1",
                        'block_name' => "Guard",
                        'recidentMobile' => $phone_number,
                        'publicMobile' => "0",
                    );

                    
	               
                    $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$msg_for'");
    
                    $data_notification=mysqli_fetch_array($qUserToken);

					$sos_user_token=$data_notification['user_token'];
                    $device=$data_notification['device'];
                    if ($device=='android') {
                       $nResident->noti("chatMsg","",$society_id,$sos_user_token,"$gt_name",$msg_data,$notAry);
                    }  else if($device=='ios') {
                       $nResident->noti_ios("chatMsg","",$society_id,$sos_user_token,"$gt_name",$msg_data,$notAry);
                    }

						
                }

                $response["message"] = ''.$xml->string->msgSend;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->failToaddChat;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getdelChat'] == "getdelChat" && $chat_id!='') {

        
                $qnotification = $d->delete("chat_master", "chat_id='$chat_id'");

            
            if ($qnotification == true) {

                $response["message"] =''.$xml->string->getChatdelete;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noDeletenotification;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['readMyMessage'] == "readMyMessage" && filter_var($society_id, FILTER_VALIDATE_INT) == true  &&  filter_var($userId, FILTER_VALIDATE_INT) == true) {

             $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$userId'");
                $data_notification = mysqli_fetch_array($qsm);
                $sos_user_token = $data_notification['user_token'];
                $device = $data_notification['device'];
                if ($device == 'android') {
                    $nResident->noti("myMsgRead","", $society_id, $sos_user_token, "", "", 'myMsgRead');
                } else if ($device == 'ios') {
                    $nResident->noti_ios("myMsgRead","", $society_id, $sos_user_token, "", "", 'myMsgRead');
                }

                 $response["message"]=''.$xml->string->done;
                $response["status"]="200";
                echo json_encode($response);

        }else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 