<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
    if ($key==$keydb) { 
    $response = array();
    extract(array_map("test_input" , $_POST));

    if($_POST['getBlocks']=="getBlocks" && $society_id!=''){

        $block_data=$d->select("block_master","society_id ='$society_id'");

        if(mysqli_num_rows($block_data)>0){

            $response["block"] = array();

            
            while($data_block_list=mysqli_fetch_array($block_data)) {

                $block = array();

                $block["block_id"]=$data_block_list['block_id'];
                $block["society_id"]=$data_block_list['society_id'];
                $block["block_name"]=$data_block_list['block_name'];
                $block["block_status"]=$data_block_list['block_status'];
                
                array_push($response["block"], $block); 
            }

            $response["message"]=''.$xml->string->getBlocksucess;
            $response["status"]="200";

            echo json_encode($response);
        
        }else{

            $response["message"]=''.$xml->string->getBlockfail;
            $response["status"]="201";

            echo json_encode($response);
        
        }
    }else if($_POST['getFloors']=="getFloors" && $society_id!=''){

        $block_data=$d->select("floors_master","society_id ='$society_id'");

        if(mysqli_num_rows($block_data)>0){

            $response["floor"] = array();

            
            while($data_floor_list=mysqli_fetch_array($block_data)) {

                $block = array();

                $floor["floor_id"]=$data_floor_list['floor_id'];
                $floor["block_id"]=$data_floor_list['block_id'];
                $floor["floor_name"]=$data_floor_list['floor_name'];
                $floor["no_of_unit"]=$data_floor_list['no_of_unit'];
                $floor["unit_type"]=$data_floor_list['unit_type'];
                $floor["floor_status"]=$data_floor_list['floor_status'];
                array_push($response["floor"], $floor); 
            }

            $response["message"]=''.$xml->string->getBlocksucess;
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]=''.$xml->string->getBlockfail;
            $response["status"]="201";
            echo json_encode($response);
        
        }
    }else if ($_POST['getUnitMembers']=="getUnitMembers" && $society_id!=''){
                    $response["units"] = array();

                    if ($block_id>0) {

                       $appendBlockQuery= " AND block_master.block_id='$block_id'";
                    }

               
                    $unit_data=$d->selectRow("block_master.block_id,block_master.block_name,unit_master.unit_id,unit_master.society_id,unit_master.unit_name,unit_master.unit_status,users_master.user_id,users_master.user_profile_pic,users_master.user_full_name,users_master.user_type,users_master.user_status,unit_master.floor_id","block_master,unit_master Left join users_master on (unit_master.unit_id=users_master.unit_id AND users_master.member_status=0 and users_master.delete_status=0 and unit_master.unit_status!=4) and (unit_master.unit_status!=3 AND users_master.user_type=0 OR users_master.user_type=1)","block_master.block_id=unit_master.block_id  AND unit_master.society_id='$society_id' $appendBlockQuery","ORDER BY block_master.block_id ASC,unit_master.unit_id ASC");

                    while($data_units_list=mysqli_fetch_array($unit_data)) {

                        $units = array(); 

                        $unit_id = $data_units_list['unit_id'];
                      

                        $units["unit_id"]=$data_units_list['unit_id'];
                        $units_id=$data_units_list['unit_id'];

                        $units["society_id"]=$data_units_list['society_id'];
                         $units["block_id"]=$data_units_list['block_id'];
                       $units["block_name"]=$data_units_list['block_name'];
                           
                       
                        $units["floor_id"]=$data_units_list['floor_id'];
                        $units["unit_name"]=$data_units_list['unit_name'];

                        if ($data_units_list['unit_status']==5 || $data_units_list['unit_close_for_gatekeeper']==1) {
                            $unit_status= 5;
                        } else {
                            $unit_status= $data_units_list['unit_status'];
                        }
                        
                        $units["unit_status"]=$unit_status;
                        
                        $units["user_id"]=$data_units_list['user_id'];
                        $user_id = $data_units_list['user_id'];
                        $units["society_id"]=$data_units_list['society_id'];
                        $units["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_units_list['user_profile_pic'];
                        $units["user_full_name"]=$data_units_list['user_full_name'];
                        $units["user_type"]=$data_units_list['user_type'];
                        $units["user_status"]=$data_units_list['user_status'];
                       

                        array_push($response["units"], $units);



                    }



                    $response["message"]="Data Get".mysqli_num_rows($unit_data);
                    $response["status"]="200";
                    echo json_encode($response);
}else if($_POST['getMembers']=="getMembers" && $society_id!='' && $unit_id!=''){

    if ($is_child_member!="" && $is_child_member>0) {
        $appendChild= " AND users_master.is_child_member=1";
    }

    $block_data=$d->select("users_master,unit_master,society_master","(users_master.user_status !='0' AND users_master.society_id ='$society_id' and  users_master.unit_id='$unit_id' and unit_master.unit_id='$unit_id' and society_master.society_id=users_master.society_id and users_master.delete_status=0 $appendChild) and (society_master.owner_view_in_tenant_unit=0 AND users_master.user_type='$user_type' OR 
    society_master.owner_view_in_tenant_unit=1)");

        if(mysqli_num_rows($block_data)>0){

            $response["users"] = array();

            
            while($data_block_list=mysqli_fetch_array($block_data)) {

                if ($data_block_list['member_status']==0) {
                    if ($data_block_list['user_type']==0) {
                        $mainUser= "Owner Primary";
                    } else {
                        $mainUser="Tenant Primary ";
                    }
                    $rel= $mainUser;
                } else {
                    if ($data_block_list['user_type']==0) {
                        $fType="Owner";
                    } else {
                        $fType="Tenant";
                    }
                    $rel= $data_block_list['member_relation_name'].'-'.$fType;
                }

                $users = array();
                $users["user_id"]=$data_block_list['user_id'];
                $users["unit_id"]=$data_block_list['unit_id'];
                $users["unit_name"]=$data_block_list['unit_name'];
                $users["user_full_name"]= $data_block_list['user_full_name'].' ('.$rel  .')';
                $users["user_full_name_new"]= $data_block_list['user_full_name'];
                $users["relation_type"]= '('.$rel  .')';
                $users["society_id"]=$data_block_list['society_id'];
                $users["block_id"]=$data_block_list['block_id'];
                $users["floor_id"]=$data_block_list['floor_id'];
                $users["user_mobile"]=$data_block_list['user_mobile'];
                $users["user_email"]=$data_block_list['user_email'];
                $users["public_mobile"]=$data_block_list['public_mobile'];
                $users["visitor_approved"]=$data_block_list['visitor_approved'];
                $users["Delivery_cab_approval"]=$data_block_list['Delivery_cab_approval'];
                $users["user_mobile"]=$data_block_list['user_mobile'];
                $users["child_gate_approval"]=$data_block_list['child_gate_approval'];
                $users["country_code"]=$data_block_list['country_code'];
                
                $users["daily_visitor_approval"]='0';
                $users["resource_approval"]='0';
                
                
                
                $users["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_block_list['user_profile_pic'];

 
                
                array_push($response["users"], $users); 
            }

            $response["message"]=''.$xml->string->getBlocksucess;
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]=''.$xml->string->getBlockfail;
            $response["status"]="201";
            echo json_encode($response);
        
        }




}else if($_POST['getAllDetails']=="getAllDetails" && $society_id!=''){

        

        $block_data=$d->select("floors_master","society_id ='$society_id'  AND block_id='$block_id'");

        if(mysqli_num_rows($block_data)>0){

            $response["floor"] = array();

            
            while($data_floor_list=mysqli_fetch_array($block_data)) {

                $block = array();

                $floor["floor_id"]=$data_floor_list['floor_id'];
                $floor["block_id"]=$data_floor_list['block_id'];
                $floor["floor_name"]=$data_floor_list['floor_name'];
                $floor["no_of_unit"]=$data_floor_list['no_of_unit'];
                $floor["unit_type"]=$data_floor_list['unit_type'];
                $floor["floor_status"]=$data_floor_list['floor_status'];
                array_push($response["floor"], $floor); 
            }
        }

        $response["units"] = array();

                    if ($block_id>0) {
                       $appendBlockQuery= " AND block_master.block_id='$block_id'";
                    }

                    $unit_data=$d->selectRow("block_master.block_id,block_master.block_name,unit_master.unit_id,unit_master.society_id,unit_master.unit_name,unit_master.unit_status,users_master.user_id,users_master.user_profile_pic,users_master.user_full_name,users_master.user_designation,users_master.user_status,unit_master.floor_id,users_master.visitor_approved,users_master.Delivery_cab_approval,users_master.public_mobile,users_master.user_mobile,users_master.country_code","block_master,unit_master,users_master","users_master.block_id=block_master.block_id AND unit_master.unit_id=users_master.unit_id AND block_master.block_id=unit_master.block_id  AND unit_master.society_id='$society_id' $appendBlockQuery","ORDER BY block_master.block_id ASC,unit_master.unit_id ASC");
                   

                    while($data_units_list=mysqli_fetch_array($unit_data)) {
                        $data_units_list = array_map("html_entity_decode", $data_units_list);
                        $units = array(); 

                        $unit_id = $data_units_list['unit_id'];
                      
                        $units["unit_id"]=$data_units_list['unit_id'];
                        $units_id=$data_units_list['unit_id'];

                        $units["society_id"]=$data_units_list['society_id'];
                        $units["block_id"]=$data_units_list['block_id'];
                        $units["block_name"]=$data_units_list['block_name'];
                        $units["floor_id"]=$data_units_list['floor_id'];
                        
                        $units["user_id"]=$data_units_list['user_id'];
                        $user_id = $data_units_list['user_id'];
                        $units["society_id"]=$data_units_list['society_id'];
                        $units["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_units_list['user_profile_pic'];
                        $units["user_full_name"]=$data_units_list['user_full_name'];
                        $units["user_designation"]=$data_units_list['user_designation'];
                        $units["user_status"]=$data_units_list['user_status'];
                        $units["visitor_approved"]=$data_units_list['visitor_approved'];
                        $units["Delivery_cab_approval"]=$data_units_list['Delivery_cab_approval'];
                        $units["public_mobile"]=$data_units_list['public_mobile'];
                        $units["country_code"]=$data_units_list['country_code'];
                        $units["user_mobile"]=$data_units_list['user_mobile'];
                        $units["daily_visitor_approval"]='0';
                        $units["resource_approval"]='0';
                        

                        array_push($response["units"], $units);



                    }



             
            $user_types=explode(',',$user_types);
            $user_types_values_ios=explode(',',$user_types_values_ios);
            $user_types_colour=explode(',',$user_types_colour);
            $response["member_types"] = array();
            for ($i=0; $i <count($user_types) ; $i++) { 
                $member_types=array();
                $member_types["type_key_name"]=$user_types[$i];
                $member_types["type_key_colour"]=$user_types_colour[$i];
                $member_types["user_types_values_ios"]=$user_types_values_ios[$i];
                array_push($response["member_types"], $member_types); 

            }      
             if ($hide_user_type==1) {
                $response["hide_user_type"]=TRUE;
            } else {
                $response["hide_user_type"]=FALSE;

            }

            if ($empty_allow_visitor==1) {
                $response["empty_allow_visitor"]=true;
            } else {
                $response["empty_allow_visitor"]=false;
            }


            $response["message"]=''.$xml->string->getBlocksucess;
            $response["status"]="200";

            echo json_encode($response);
        



}else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }
}else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
?>

