<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
    if ($key==$keydb) { 
    $response = array();
    extract(array_map("test_input" , $_POST));

    if (isset($getNewVisitorHistoryVisitor) && $getNewVisitorHistoryVisitor == 'getNewVisitorHistoryVisitor' && $society_id!='') {

            $response["list_visitor"] = array();
           
            if ($blocks_id!="") {
                $appendQuery = " AND block_master.block_id IN($blocks_id)";
            }

            if ($start_date !='' && $end_date!='') {

            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));

                $q2 = $d->select("visitors_master,unit_master,block_master", "(visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND (visitors_master.visitor_status = '3' OR visitors_master.visitor_status = '5' AND visitors_master.exit_date!='') AND visitors_master.visit_date  BETWEEN '$start_date' AND '$end_date'  $appendQuery )","GROUP BY visitors_master.parent_visitor_id  ORDER BY visitors_master.exit_date DESC, visitors_master.exit_time DESC LIMIT 1000");
            } else {
                $q2 = $d->select("visitors_master,unit_master,block_master,users_master", "(visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND (visitors_master.visitor_status = '3' OR visitors_master.visitor_status = '5' AND visitors_master.exit_date!='') AND visitors_master.exit_date  >= ( CURDATE() - INTERVAL 1 DAY ) $appendQuery )","GROUP BY visitors_master.parent_visitor_id  ORDER BY visitors_master.exit_date DESC, visitors_master.exit_time DESC");
            }
            

            if (mysqli_num_rows($q2) > 0) {
                while ($data2 = mysqli_fetch_array($q2)) {
                    $data2 = array_map("html_entity_decode", $data2);
                    $list = array();
            if ($data2['exit_date']!="") {
             $intTime=$data2['visit_date']." ".$data2['visit_time'];
             $outTime=$data2['exit_date']." ".$data2['exit_time'];
             $date_a = new DateTime($intTime);
            $date_b = new DateTime($outTime);
            $interval = date_diff($date_a,$date_b);
            $totalDays= $interval->d;
                $totalMinutes= $interval->i;
                $totalHours= $interval->h;
                if ($totalDays>0) {
                $list["duration"] = $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                } else {
                    $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                }
            } else {
                 $list["duration"] = $xml->string->not_available.'';
            }
            
            $visitDate = date("d M Y h:i A", strtotime($data2['visit_date']." ".$data2['visit_time']));
            $visitDateFilter = date("d-m-Y", strtotime($data2['visit_date']));
            $exitDate = ($data2['exit_date']!="") ? date("d M Y h:i A", strtotime($data2['exit_date']." ".$data2['exit_time'])) :  "Auto Exit"; 

                // $qm=$d->select("users_master,visitors_master","users_master.user_id=visitors_master.user_id AND users_master.society_id ='$society_id' AND visitors_master.parent_visitor_id='$data2[visitor_id]'","ORDER BY users_master.user_id DESC");
                //     $userNameAry=array();

                //     while($userDataMulti=mysqli_fetch_array($qm)){
                //         array_push($userNameAry, $userDataMulti['user_first_name']);
                //     }
                //     $userNameOly= implode(",", $userNameAry);

                    $list["id"] = $data2['visitor_id'];
                    $list["name"] =$data2['visitor_name'].'';
                    $list["color_status"] = '';
                    $list["last_in"] = "";
                    $list["visit_from"] =$data2['visit_from'].'';
                    $list["mobile"] = $data2['visitor_mobile'];
                    $list["country_code"] = $data2['country_code'];
                    $list["user_id"] = $data2['user_id'];
                    $list["unit_id"] =$data2['unit_id'];;
                    $list["block_id"] =$data2['block_id'];
                    $list["floor_id"] =$data2['floor_id'];
                    $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data2[visitor_sub_type_id]'");
                    $vistLogo=mysqli_fetch_array($fd);
                     if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $list["visit_logo"] = $visit_logo.'';

                    if ($visitorMobilePrivacy==0 && $data2['visitor_type']==0  OR  $visitorMobilePrivacy==0 && $data2['visitor_type']==1) {
                    $list["visitor_mobile_new"] = "".substr($data2['visitor_mobile'], 0, 3) . '****' . substr($data2['visitor_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$data2['visitor_mobile'];
                    }
                    if ($data2['user_first_name']!='') {
                        $list["des_block"] = $data2['block_name'] . "-" .  $data2['unit_name'].' ('.$data2['user_first_name'].$data2['user_id'].')';
                    } else {
                        $list["des_block"] = $data2['block_name'] . "-" .  $data2['unit_name'];
                    }

                    if ($data2['visitor_type']==1 && $data2['expected_type']==1) {
                        $visitor_type = 0;
                    }else if ($data2['visitor_type']==1 && $data2['expected_type']==2) {
                         $visitor_type = 2;
                    }else if ($data2['visitor_type']==1 && $data2['expected_type']==3) {
                         $visitor_type = 3;
                    }else {
                        $visitor_type = $data2['visitor_type'];
                    }
                    $list["visitor_type"] =$visitor_type;
                    $list["expected_type"] =$data2['expected_type'];
                    $list["visit_date_time"] = $visitDate;
                    $list["visit_date"] = $visitDateFilter;
                    $list["visit_exit_date_time"] = $exitDate;
                    $list["qr_code"] = $data2['visitor_id'];
                    $list["type"] = "Exvisitor";
                    $list["profile_img"] = $base_url . "img/visitor/" . $data2['visitor_profile'];
                    $list["profile_img_name"] = $data2['visitor_profile'];
                    $list["visitor_sub_type_id"] = $data2['visitor_sub_type_id'];

                    if ($data2['visitor_status']==2) {
                        if ($data2['in_temperature']>0) {
                            $in_temperature=$data2['in_temperature'];
                        } else {
                            $in_temperature = "";
                        }
                        $list["temperature"]=$in_temperature.'';
                        $list["with_mask"]=$data2['in_with_mask'].'';
                    } else if ($data2['visitor_status']==3) {
                         if ($data2['out_temperature']>0) {
                            $out_temperature=$data2['out_temperature'];
                        } else {
                            $out_temperature = "";
                        }
                        $list["temperature"]=$out_temperature.'';
                        $list["with_mask"]=$data2['out_with_mask'].'';
                    } else {
                        $list["temperature"]='';
                        $list["with_mask"]='';
                    }
                    
                    $list["re_enter"] = true;
                    $list["workig_units"] ="";
                    $list["vehicle_no"] =$data2['vehicle_no'].'';
                    array_push($response["list_visitor"], $list);
                }
                $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = ''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
    }else if ($_POST['getVisitorUsersDetails']=="getVisitorUsersDetails" && $visitor_id!=''){

        $qm=$d->select("users_master,visitors_master","users_master.user_id=visitors_master.user_id AND users_master.society_id ='$society_id' AND visitors_master.parent_visitor_id='$visitor_id'","ORDER BY users_master.user_id DESC");

            $response["userNameAry"] = array();
           
            
                    while($userDataMulti=mysqli_fetch_array($qm)){
                         $userNameAry=array();
                         $userNameAry["user_id"] =$userDataMulti['user_id'];
                         $userNameAry["user_first_name"] =$userDataMulti['user_first_name'];
                         $userNameAry["user_profile_pic"]=$base_url."img/users/recident_profile/".$userDataMulti['user_profile_pic'];
                        $userNameAry["user_full_name"]=$userDataMulti['user_full_name'];
                        $userNameAry["user_type"]=$userDataMulti['user_type'];
                        $userNameAry["user_status"]=$userDataMulti['user_status'];
                       
                        array_push($response["userNameAry"],$userNameAry);
                     }
                
                $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);


    } else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
?>