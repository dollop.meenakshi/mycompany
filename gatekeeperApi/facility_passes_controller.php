<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));

    if ($_POST['passCheck'] == "passCheck" && $qrValue!='') {
          
          $today= date("Y-m-d");
          $curentTime= date("H:i:s");
          // $today = '2022-02-02';
        // $qrValue = "VEtUXzFfMTdfOTk4XzE=";
        // $qr_content = "TKT".$data['booking_id'].$data['facility_id'].$last3Digit.$data['pleyer_id'];

        $qr_content = base64_decode($qrValue);
        $base64Array = explode("_", $qr_content);
        $booking_id = $base64Array[1];
        $facility_id = $base64Array[2];
        $pleyer_id = $base64Array[4];

         $q1=$d->select("facilities_master,facility_players_master LEFT JOIN users_master on users_master.user_id=facility_players_master.user_id LEFT JOIN block_master on users_master.block_id=block_master.block_id LEFT JOIN unit_master on users_master.unit_id=unit_master.unit_id","facility_players_master.facility_id=facilities_master.facility_id  AND  facility_players_master.facility_id='$facility_id' AND facility_players_master.booking_id='$booking_id'  AND facility_players_master.pleyer_id='$pleyer_id' AND facilities_master.facility_category_id='$facility_category_id'" );
          $data=mysqli_fetch_array($q1);
        
             if ($data>0) {

                if ($data['entry_status']==0 && $data['booking_end_time']<$curentTime) {
                    $response["message"] ="Pass Expired";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit(); 
                }

                $currentDate = strtotime($data['booking_start_time']);
                $futureDate = $currentDate+(-60*15);
                $cBufferTime = date("H:i:s", $futureDate);

                if ($data['entry_status']==0 && $curentTime<$cBufferTime) {
                    $response["message"] ="Please Visit at ".$cBufferTime;
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit(); 
                } 

                if ($data['booking_date']!=$today) {
                    $booking_date = date("d M Y l", strtotime($data['booking_date']));
                    $response["message"] ="This Pass Valid On $booking_date";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            
                $facility_name = $data['facility_name'];
                $created_date = date("Y-m-d H:i:s");
                $qUserToken=$d->select("users_master","user_id='$data[user_id]'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $token=$data_notification['user_token'];
                $device=$data_notification['device'];

                

                
                // $q = $d->update("facility_players_master",$a,"pleyer_id='$pleyer_id' ");
                if ($data['entry_status']==0) {
                    $in_status = false;
                } else {
                    $in_status = true;
                }

                if ($data['user_profile_pic']!="") {
                    $response["user_profile"]=$base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                } else {
                    $response["user_profile"]= "";
                }

                if ($data['block_name']!="") {
                    $response["block_unit_name"]=$data['block_name'].'-'.$data['unit_name'];
                } else if ($data['is_guest']==1) {
                   $response["block_unit_name"]= "Guest";
                } else {
                    $response["block_unit_name"]= "";
                }
                
                $response["in_status"] =$in_status;
                $response["is_guest"] =$data['is_guest'];
                $response["pass_no"] ='TKT'.$data['user_id'].$data['pleyer_id'];;
                $response["booking_date"] =date("d M Y", strtotime($data['booking_date']));
                $response["facility_name"] =$data['facility_name'];
                $response["booking_start_time"] =date("h:i A", strtotime($data['booking_start_time']));
                $response["booking_end_time"] =date("h:i A", strtotime($data['booking_end_time']));
                $response["player_name"] =$data['player_name'];
                $response["player_mobile"] =$data['player_mobile'];
                $response["pleyer_id"] =$data['pleyer_id'];
                $response["booking_id"] =$data['booking_id'];
                $response["facility_id"] =$data['facility_id'];
                $response["message"] =''.$xml->string->passVErified;
                $response["status"] = "200";
                echo json_encode($response);
              
            } else {
                $response["message"] ='Invalid Pass';
                $response["status"] = "201";
                echo json_encode($response);
            }

        } else  if ($_POST['passAllow'] == "passAllow"  && filter_var($facility_id, FILTER_VALIDATE_INT) == true   && filter_var($booking_id, FILTER_VALIDATE_INT) == true && $pleyer_id!='') {
          
          $today= date("Y-m-d");
          // $today = '2022-02-02';
        
         $q1=$d->select("facilities_master,facility_players_master LEFT JOIN users_master on users_master.user_id=facility_players_master.user_id LEFT JOIN block_master on users_master.block_id=block_master.block_id LEFT JOIN unit_master on users_master.unit_id=unit_master.unit_id","facility_players_master.facility_id=facilities_master.facility_id  AND  facility_players_master.facility_id='$facility_id' AND facility_players_master.booking_id='$booking_id'  AND facility_players_master.pleyer_id='$pleyer_id'" );
          $data=mysqli_fetch_array($q1);
        
             if ($data>0) {

                if ($data['booking_date']!=$today) {
                    $booking_date = date("d M Y l", strtotime($data['booking_date']));
                    $response["message"] ="This Pass Valid On $booking_date";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            
                $facility_name = $data['facility_name'];
                $created_date = date("d M Y h:i A");
                
                $qUserToken=$d->select("users_master","user_id='$data[user_id]'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $token=$data_notification['user_token'];
                $device=$data_notification['device'];

                if ($in_out_status == 0) {

                    if ($data['entry_status']==1) {
                        $entry_date = date("d M Y h:i A", strtotime($data['entry_date']));
                        $response["message"] ='Entry Pass Already Verified at '.$entry_date ;
                        $response["status"] = "201";
                        echo json_encode($response);
                        exit();
                    }
                    // for Entry
                    if ($device=='android') {
                       $nResident->noti("FacilityFragment","",$society_id,$token,"$facility_name Entry Pass Verified","at ".$created_date,'facilityBooking');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("FacilityVC","",$society_id,$token,"$facility_name Entry Pass Verified","at ".$created_date,'facilityBooking');
                    }

                    $m->set_data('entry_status', 1);
                    $m->set_data('entry_date', date("Y-m-d H:i:s") );
                    $a = array(
                        'entry_status' => $m->get_data('entry_status'),
                        'entry_date' => $m->get_data('entry_date'),
                    );

                } else if ($in_out_status == 1) {
                    // for Exit
                    if ($data['exit_status']==1) {
                        $exit_date = date("d M Y h:i A", strtotime($data['exit_date']));
                        $response["message"] ='Exit Pass Already Verified at '.$exit_date;
                        $response["status"] = "201";
                        echo json_encode($response);
                        exit();
                    }

                    if ($device=='android') {
                       $nResident->noti("FacilityFragment","",$society_id,$token,"$facility_name Exit Pass Verified","at ".$created_date,'facilityBooking');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("FacilityVC","",$society_id,$token,"$facility_name Exit Pass Verified","at ".$created_date,'facilityBooking');
                    }

                    $m->set_data('exit_status', 1);
                    $m->set_data('exit_date', date("Y-m-d H:i:s") );
                    $a = array(
                        'exit_status' => $m->get_data('exit_status'),
                        'exit_date' => $m->get_data('exit_date'),
                    );
                }

                
                $q = $d->update("facility_players_master",$a,"pleyer_id='$pleyer_id' ");
                
                $response["message"] =''.$xml->string->passVErified;
                $response["status"] = "200";
                echo json_encode($response);
              
            } else {
                 $response["message"] =''.$xml->string->invalidDateorPass;
                $response["status"] = "201";
                echo json_encode($response);
            }

        } else if($_POST['checkEventPass']=="checkEventPass" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($event_id, FILTER_VALIDATE_INT) == true ){
                   $today= date("Y-m-d");
                 $eq=$d->select("event_days_master","event_id='$event_id' AND event_date='$today'");
                 $dayData=mysqli_fetch_array($eq);
                 $events_day_id = $dayData['events_day_id'];

                 if ($events_day_id!='' || $events_day_id!=0) {
                  $pendingPass= $d->count_data_direct("pass_id","event_passes_master,event_attend_list","event_passes_master.event_attend_id=event_attend_list.event_attend_id AND event_passes_master.entry_status='0' AND event_passes_master.event_id='$event_id'  AND event_passes_master.events_day_id='$events_day_id'AND event_attend_list.book_status!=3"); 
                 $VerifiedPass= $d->count_data_direct("pass_id","event_passes_master,event_attend_list","event_passes_master.event_attend_id=event_attend_list.event_attend_id AND event_passes_master.entry_status='1' AND event_passes_master.event_id='$event_id'  AND event_passes_master.events_day_id='$events_day_id'AND event_attend_list.book_status!=3"); 
                 } else {
                 $pendingPass= $d->count_data_direct("pass_id","event_passes_master,event_attend_list","event_passes_master.event_attend_id=event_attend_list.event_attend_id AND event_passes_master.entry_status='0' AND event_passes_master.event_id='$event_id' AND event_attend_list.book_status!=3"); 
                 $VerifiedPass= $d->count_data_direct("pass_id","event_passes_master,event_attend_list","event_passes_master.event_attend_id=event_attend_list.event_attend_id AND event_passes_master.entry_status='1' AND event_passes_master.event_id='$event_id' AND event_attend_list.book_status!=3"); 

                 }
                

                 
                $response["totalPass"]=$pendingPass+$VerifiedPass.'';
                $response["pendingPass"]=$pendingPass;
                $response["VerifiedPass"]=$VerifiedPass;
                $response["message"]=''.$xml->string->getPassDetai;
                $response["events_day_id"]=$events_day_id;
                $response["status"]="200";
                echo json_encode($response);


            }else if($_POST['getFacilityCategory']=="getFacilityCategory" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                    if ($user_id!='' && $user_id!=0) {
                             $quc=$d->select("users_master","user_id='$user_id' AND is_defaulter=1");
                             if (mysqli_num_rows($quc)>0) {
                                $response["message"] = "Access Denied ";
                                $response["status"] = "201";
                                echo json_encode($response);
                                exit();
                             }

                        }

                    $qfacility_data=$d->select("facility_category_master","society_id ='$society_id' AND facility_category_active_status!=1");

                    $response["facility_category"] = array();

                if(mysqli_num_rows($qfacility_data)>0){

                    while($data_facility_list=mysqli_fetch_array($qfacility_data)) {
                        
                        $facility_category = array();  
                        $fName = ucfirst($data_facility_list['facility_category_name']);

                        $facility_category["facility_category_id"]=$data_facility_list['facility_category_id'];
                        $facility_category["society_id"]=$data_facility_list['society_id'];
                        $facility_category["facility_category_name"]=html_entity_decode($fName);
                        if($data_facility_list['facility_category_name']!='') {
                          $facility_category["facility_category_description"]=html_entity_decode($data_facility_list['facility_category_description']);
                        } else {
                            $facility_category["facility_category_description"]="$not_available";
                        }
                        $facility_category["facility_category_image"]=$base_url."/img/facility/".$data_facility_list['facility_category_image'];
                        $facility_category["facility_category_terms"]=$data_facility_list['facility_category_terms'];
                        $facility_category["facility_status"]=$data_facility_list['facility_category_active_status'];

                        array_push($response["facility_category"], $facility_category); 
                    }

                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);
            
                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);
            
                }
             } else  if($_POST['getFacilitySubCategory']=="getFacilitySubCategory" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                   

                $qfacility_data=$d->select("facility_category_master,facilities_master","facilities_master.society_id ='$society_id' AND facilities_master.facility_active_status!=1 AND facilities_master.facility_category_id='$facility_category_id' AND facilities_master.facility_category_id= facility_category_master.facility_category_id");

                    $response["facility_sub_category"] = array();

                if(mysqli_num_rows($qfacility_data)>0){

                    while($data_facility_list=mysqli_fetch_array($qfacility_data)) {
                        
                        $facility_sub_category = array();  
                        $fName = ucfirst($data_facility_list['facility_name']);

                        $facility_sub_category["facility_sub_category_id"]=$data_facility_list['facility_id'];
                        $facility_sub_category["society_id"]=$data_facility_list['society_id'];
                        $facility_sub_category["balancesheet_id"]=$data_facility_list['balancesheet_id'];
                        
                        $facility_sub_category["facility_name"]=html_entity_decode($fName);
                        $facility_sub_category["facility_category_name"]=html_entity_decode($data_facility_list['facility_category_name']);

                        if($data_facility_list['facility_category_name']!='') {
                          $facility_sub_category["facility_description"]=html_entity_decode($data_facility_list['facility_description']);
                        } else {
                            $facility_sub_category["facility_category_description"]="$not_available";
                        }
                        $facility_sub_category["facility_photo"]=$base_url."/img/facility/".$data_facility_list['facility_photo'];
                        $facility_sub_category["facility_address"]=$data_facility_list['facility_address'];
                        $facility_sub_category["facility_amount"]=$data_facility_list['facility_amount'];
                        $facility_sub_category["facility_amount_tenant"]=$data_facility_list['facility_amount_tenant'];

                        $facility_sub_category["facility_amount_monthly"]=$data_facility_list['facility_amount_monthly'];
                        $facility_sub_category["facility_amount_monthly_tenant"]=$data_facility_list['facility_amount_monthly_tenant'];

                        $facility_sub_category["facility_quarterly_amount"]=$data_facility_list['facility_quarterly_amount'];
                        $facility_sub_category["facility_amount_quarterly_tenant"]=$data_facility_list['facility_amount_quarterly_tenant'];

                        $facility_sub_category["facility_halfyearly_amount"]=$data_facility_list['facility_halfyearly_amount'];
                        $facility_sub_category["facility_amount_halfyearly_tenant"]=$data_facility_list['facility_amount_halfyearly_tenant'];

                        $facility_sub_category["facility_yearly_amount"]=$data_facility_list['facility_yearly_amount'];
                        $facility_sub_category["facility_amount_yearly_tenant"]=$data_facility_list['facility_amount_yearly_tenant'];

                        $facility_sub_category["facility_type"]=$data_facility_list['facility_type'];
                        $facility_sub_category["amount_type"]=$data_facility_list['amount_type'];
                        $facility_sub_category["booking_type"]=$data_facility_list['booking_type'];
                        $facility_sub_category["person_limit"]=$data_facility_list['person_limit'];
                        
                        $facility_sub_category["facility_latitude"]=$data_facility_list['facility_latitude'];
                        $facility_sub_category["facility_longitude"]=$data_facility_list['facility_longitude'];
                        $facility_sub_category["is_paid_facility"]=$data_facility_list['is_paid_facility'];
                        $facility_sub_category["facility_terms"]=$data_facility_list['facility_terms'];
                        $facility_sub_category["person_limit"]=$data_facility_list['person_limit'];

                        array_push($response["facility_sub_category"], $facility_sub_category); 
                    }

                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);
            
                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);
            
                }
            } else  if($_POST['getFacilitySubCategoryDetails']=="getFacilitySubCategoryDetails" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            

        $qfacility_data=$d->select("facilities_master","society_id ='$society_id' AND facility_active_status!=1 AND facility_category_id='$facility_category_id' AND facility_id='$facility_sub_category_id'");

            

        if(mysqli_num_rows($qfacility_data)>0){

            $data_facility_list=mysqli_fetch_array($qfacility_data); 
                
              
                $fName = ucfirst($data_facility_list['facility_name']);

                $response["facility_sub_category_id"]=$data_facility_list['facility_id'];
                $response["society_id"]=$data_facility_list['society_id'];
                $response["balancesheet_id"]=$data_facility_list['balancesheet_id'];
                $response["facility_name"]=html_entity_decode($fName);

                if($data_facility_list['facility_category_name']!='') {
                  $response["facility_description"]=html_entity_decode($data_facility_list['facility_description']);
                } else {
                    $response["facility_category_description"]="$not_available";
                }
                if ($data_facility_list['facility_photo']!="") {
                    $response["facility_photo"]=$base_url."/img/facility/".$data_facility_list['facility_photo'];
                } else {
                    $response["facility_photo"]="";
                }
                if ($data_facility_list['facility_photo_2']!="") {
                    $response["facility_photo_2"]=$base_url."/img/facility/".$data_facility_list['facility_photo_2'];
                } else {
                    $response["facility_photo_2"]="";
                }
                if ($data_facility_list['facility_photo_3']!="") {
                    $response["facility_photo_3"]=$base_url."/img/facility/".$data_facility_list['facility_photo_3'];
                } else {
                    $response["facility_photo_3"]="";
                }
                $response["facility_address"]=$data_facility_list['facility_address'];
                
                

                
                $response["facility_type"]=$data_facility_list['facility_type'];
                $response["amount_type"]=$data_facility_list['amount_type'];
                $response["booking_type"]=$data_facility_list['booking_type'];
                $response["person_limit"]=$data_facility_list['person_limit'];
                $response["is_even_booking"]=$data_facility_list['is_even_booking'];
                $response["single_slot_booking"]=$data_facility_list['single_slot_booking'];
                $response["maximum_slot_booking"]=$data_facility_list['maximum_slot_booking'];

                $response["person_count"]=$data_facility_list['person_count'];
                $response["facility_status"]=$data_facility_list['facility_status'];
                $response["gst"]=$data_facility_list['gst'];
                $response["gst_type"]=$data_facility_list['gst_type'];
                $response["is_taxble"]=$data_facility_list['is_taxble'];
                $response["taxble_type"]=$data_facility_list['taxble_type'];
                $response["facility_latitude"]=$data_facility_list['facility_latitude'];
                $response["facility_longitude"]=$data_facility_list['facility_longitude'];
                $response["is_paid_facility"]=$data_facility_list['is_paid_facility'];
                $response["facility_terms"]=$data_facility_list['facility_terms'];
                $response["minimum_person_booking_limit"]=$data_facility_list['minimum_person_booking_limit'];
                $response["maximum_person_booking_limit"]=$data_facility_list['maximum_person_booking_limit'];
                $response["allow_other_members_booking"]=$data_facility_list['allow_other_members_booking'];
                $response["allow_guest_booking"]=$data_facility_list['allow_guest_booking'];
                $response["allow_other_facility_booking_same_time"]=$data_facility_list['allow_other_facility_booking_same_time'];
                $response["allow_other_facility_booking_family"]=$data_facility_list['allow_other_facility_booking_family'];
                $response["next_facility_booking_time"]=$data_facility_list['next_facility_booking_time'];
                $response["booking_extend_minutes_limit"]=$data_facility_list['booking_extend_minutes_limit'];
                $response["booking_cancellation_minutes"]=$data_facility_list['booking_cancellation_minutes'];
                $response["booking_cancellation_minutes"]=$data_facility_list['booking_cancellation_minutes'];
                
                
                $facilityPa=$d->select("facility_package_master","active_status!=1 AND facility_id='$data_facility_list[facility_id]'");

                $response["facility_package"] = array();

                if(mysqli_num_rows($facilityPa)>0){

                    while($data=mysqli_fetch_array($facilityPa)) {

                        $facility_package["facility_package_id"]=$data['facility_package_id'];
                        $facility_package["user_type"]=$data['user_type'];
                        $facility_package["package_name"]=$data['package_name'];
                        $facility_package["number_of_person"]=$data['number_of_person'];
                        $facility_package["package_type"]=$data['package_type'];
                        $facility_package["allow_mulitple_person_booking"]=$data['allow_mulitple_person_booking'];
                        if ($data['package_amount']>0) {
                            $facility_package["package_amount"]=$data['package_amount'];
                        } else {
                            $facility_package["package_amount"]="Free";
                        }
                        array_push($response["facility_package"], $facility_package); 

               
                    }
                }



                $qfacility_schedule_data=$d->select("facility_schedule_master","active_status!=1 AND facility_id='$facility_sub_category_id'");

                $response["facility_schedule"] = array();

                if(mysqli_num_rows($qfacility_schedule_data)>0){

                    while($data_facility_schedule_list=mysqli_fetch_array($qfacility_schedule_data)) {

                        $facility_schedule["facility_schedule_id"]=$data_facility_schedule_list['facility_schedule_id'];
                        $facility_schedule["facility_sub_category_id"]=$data_facility_schedule_list['facility_id'];
                        $facility_schedule["facility_day_id"]=$data_facility_schedule_list['facility_day_id'];
                        $facility_schedule["facility_start_time"]=date('h:i A',strtotime($data_facility_schedule_list['facility_start_time']));
                        $facility_schedule["facility_end_time"]=date('h:i A',strtotime($data_facility_schedule_list['facility_end_time']));
                        array_push($response["facility_schedule"], $facility_schedule); 

               
                    }
                }


                $response["weekly_maintainace"] = array();
                $response["date_wise_maintainace"] = array();
                
                $sq1 = $d->select("facilities_maintainace_master","facility_id='$facility_sub_category_id'  AND facilities_maintainace_type!=3 ","ORDER BY facilities_maintainace_id DESC");
                
                
                if(mysqli_num_rows($sq1)>0){

                    while($mData=mysqli_fetch_array($sq1)) {
                        if ($mData['facilities_maintainace_type']==1) {
                           $title = "Every Week";
                           $weekDays = $mData['week_days'];
                        }  else if ($mData['facilities_maintainace_type']==2) {
                           $title = "Every Day of Month";
                           $weekDays = $mData['month_number'];
                        } else if ($mData['facilities_maintainace_type']==3) {
                           $title = "This Day";
                        } else if ($mData['facilities_maintainace_type']==4) {
                           $title = "Daily Maintenance";
                        } 

                        $weekly_maintainace["title"]=$title;
                        $weekly_maintainace["week_days"]=$weekDays;
                        
                        $totalSlotArray = explode(",",$mData['facility_schedule_ids']);
                        $totalSlot =  count($totalSlotArray); 
                        $weekly_maintainace["total_timeslot"]=''.$totalSlot;

                        $weekly_maintainace["maintainace_remark"]=$mData['maintainace_remark'];

                        $weekly_maintainace['time_slot']=array();

                         $ids = join("','",$totalSlotArray); 

                        $sq=$d->select("facility_schedule_master","facility_id='$facility_sub_category_id' AND facility_schedule_id IN ('$ids')","");

                        while ($timeSlotData=mysqli_fetch_array($sq)) {
                            $time_slot["facility_start_time"]=date('h:i A',strtotime($timeSlotData['facility_start_time']));
                            $time_slot["facility_end_time"]=date('h:i A',strtotime($timeSlotData['facility_end_time']));
                            array_push($weekly_maintainace["time_slot"], $time_slot); 

                        }

                        array_push($response["weekly_maintainace"], $weekly_maintainace); 
                    }
                }

                $sq2 = $d->select("facilities_maintainace_master","facility_id='$facility_sub_category_id'  AND facilities_maintainace_type=3 ","ORDER BY facilities_maintainace_id DESC");

                if(mysqli_num_rows($sq2)>0){

                    while($mData1=mysqli_fetch_array($sq2)) {
                        

                        $date_wise_maintainace["title"]="This Date";
                        $date_wise_maintainace["week_days"]=date('d M Y',strtotime($mData1['maintainace_start_date']));
                        
                        $totalSlotArray = explode(",",$mData1['facility_schedule_ids']);
                        $totalSlot =  count($totalSlotArray); 
                        $date_wise_maintainace["total_timeslot"]=''.$totalSlot;

                        $date_wise_maintainace["maintainace_remark"]=$mData1['maintainace_remark'];

                        $date_wise_maintainace['time_slot']=array();

                         $ids = join("','",$totalSlotArray); 

                        $sq=$d->select("facility_schedule_master","facility_id='$facility_sub_category_id' AND facility_schedule_id IN ('$ids')","");

                        while ($timeSlotData=mysqli_fetch_array($sq)) {
                            $time_slot["facility_start_time"]=date('h:i A',strtotime($timeSlotData['facility_start_time']));
                            $time_slot["facility_end_time"]=date('h:i A',strtotime($timeSlotData['facility_end_time']));
                            array_push($date_wise_maintainace["time_slot"], $time_slot); 

                        }

                        array_push($response["date_wise_maintainace"], $date_wise_maintainace); 
                    }
                }

            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{

            $response["message"]="$noDatafoundMsg";
            $response["status"]="201";
            echo json_encode($response);
    
        }
    }else if($_POST['checkAvailiity']=="checkAvailiity" && filter_var($facility_id, FILTER_VALIDATE_INT) == true  && filter_var($facility_package_id, FILTER_VALIDATE_INT) == true   ){

    
      $book_date= date("Y-m-d", strtotime($book_date));
      $q=$d->select("facilities_master"," facility_id='$facility_id'","");
      $row=mysqli_fetch_array($q);
      extract($row);
     
       $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
       $weekday = date('l', strtotime($book_date)); // note: first arg to date() is lower-case L 
       $key = array_search($weekday, $dayName); // $key = 2;
      
      // find no of person 
         $sp = $d->select("facility_package_master","facility_package_id='$facility_package_id' AND facility_id='$facility_id'","");
        $packageData= mysqli_fetch_array($sp);
        // $no_of_person = $packageData['number_of_person'];
        $package_amount = $packageData['package_amount'];
        $package_type = $packageData['package_type'];
        $allow_mulitple_person_booking = $packageData['allow_mulitple_person_booking'];

        if ($allow_mulitple_person_booking==1 && $no_of_person>0) {
            // code...
            $facility_amount = $package_amount* $no_of_person;
        } else {
            $facility_amount = $package_amount;
            $no_of_person = $packageData['number_of_person'];
        }

           
        
        switch ($package_type) {
        case '0':
            // Single Booking
            $endDate = $book_date;
            break;
        case '1':
            // Monthly Booking...
            $x = strtotime($book_date);
            $endDate = date("Y-m-d",strtotime("+1 month",$x));
            break;
        case '2':
            // Quarterly Booking...
            $x = strtotime($book_date);
            $endDate = date("Y-m-d",strtotime("+3 month",$x));
            break;
        
        case '3':
            // Half Yearly Booking...
            $x = strtotime($book_date);
            $endDate = date("Y-m-d",strtotime("+6 month",$x));
            break;
        case '4':
            // Yearly Booking...
            $x = strtotime($book_date);
            $endDate = date("Y-m-d",strtotime("+12 month",$x));
            break;
        
        default:
            // code...
            break;
     }
    
      if ($package_type!=0){
          $end_date = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $endDate) ) ));
      } else {
        $end_date = $book_date;
      }

      $response["time_slot"] = array();


      

        $sq = $d->select("facility_schedule_master","facility_start_time!='00:00:00' AND facility_id='$facility_id' AND facility_status=0 AND active_status=0 ","ORDER BY facility_start_time ASC");

        if(mysqli_num_rows($sq)>0 && $package_type==0) {
            // single day Booking
            $checkDateSingle = date("d", strtotime($book_date));
            $checkDateSingle = (int)$checkDateSingle;
            $checkDateDay = date("l", strtotime($book_date));
            $maintenaceSlotIds = array();
            $sq1 = $d->select("facilities_maintainace_master","facility_id='$facility_id' AND (facilities_maintainace_type=4 OR facilities_maintainace_type=3 AND maintainace_start_date='$book_date' OR facilities_maintainace_type=2 AND FIND_IN_SET('$checkDateSingle',month_number) OR facilities_maintainace_type=1 AND FIND_IN_SET('$checkDateDay',week_days) )","ORDER BY facilities_maintainace_id DESC");
            while ($sData= mysqli_fetch_array($sq1)) {
              $arrayTimeTemp = explode(",",$sData['facility_schedule_ids']);
              for ($i2=0; $i2 <count($arrayTimeTemp) ; $i2++) { 
                array_push($maintenaceSlotIds, $arrayTimeTemp[$i2]);
              }
            }

          while ($sData= mysqli_fetch_array($sq)) {
            $time_slot=array();
            $today = date("Y-m-d");
            $cTime = date("H:i:s");
            $time1 = strtotime($sData["facility_start_time"]);
            $time2 = strtotime($sData["facility_end_time"]);
            $difference_hours = abs($time2 - $time1) / 3600;
            if($difference_hours <1) {
              $difference_hours =1;
            }
            
            $count5=$d->sum_data("no_of_person","facility_booking_months","cancel_status=0 AND facility_id='$facility_id'  AND facility_schedule_id='$sData[facility_schedule_id]'  AND booking_start='$book_date'");
            $row11=mysqli_fetch_array($count5);
            $booke_count=$row11['SUM(no_of_person)'];
            if ($booke_count=='') {
              $booke_count =0;
            }
            $avSeat = $person_limit - $booke_count;

            $time_slot["facility_schedule_id"]=$sData["facility_schedule_id"];
            $time_slot["difference_hours"]=$difference_hours.'';

            if (in_array($sData['facility_schedule_id'],$maintenaceSlotIds)) { 
               $time_slot["available_message"]='Close';
               $time_slot["available_count"]=$avSeat.'';
               $time_slot["availability"]=false;
            } else if ($avSeat<$no_of_person && $amount_type==0) { 
              $time_slot["available_message"]='Sold Out';
              $time_slot["available_count"]=$avSeat.'';
              $time_slot["availability"]=false;
            } else if ($booke_count>0 && $amount_type==1) {
              $time_slot["available_message"]='Sold Out'; 
              $time_slot["available_count"]='0';
              $time_slot["availability"]=false;
            } else if ($today==$book_date && $cTime>$sData["facility_start_time"]) {
                $time_slot["available_message"]='Time Expired';
                $time_slot["available_count"]=$avSeat.'';
                $time_slot["availability"]=false;
            }  else {
              $time_slot["available_message"]='';
              $time_slot["available_count"]=$avSeat.'';
              $time_slot["availability"]=true;
            }

            if($is_paid_facility==0){
              $time_slot["book_price"]='Free';  
            } else {
              $time_slot["book_price"]=number_format($facility_amount,2,'.','');  
            }
            $time_slot["facility_start_time"]=date('h:i A',strtotime($sData["facility_start_time"])).' to '.date('h:i A',strtotime($sData["facility_end_time"]));

            if ($today==$book_date && $cTime>$sData["facility_start_time"]) {

            } else {
                array_push($response["time_slot"], $time_slot); 
            }
          }

           $response["start_date"]=date('d M Y',strtotime($book_date));
           $response["end_date"]=date('d M Y',strtotime($end_date));
           $response["message"]="$datafoundMsg";
           $response["status"]="200";
           echo json_encode($response);
        } else if (mysqli_num_rows($sq)>0 && $package_type!=0) {
            // multiple day booking
            while ($sData= mysqli_fetch_array($sq)) {
                $time_slot=array();

                $facility_schedule_id = $sData['facility_schedule_id'];

                $time1 = strtotime($sData["facility_start_time"]);
                $time2 = strtotime($sData["facility_end_time"]);
                

                $time_slot["facility_schedule_id"]=$sData["facility_schedule_id"];
                $time_slot["difference_hours"]='';

                $isSlotAvailble = '0';
                $availblityArray = array();
                $alreadyBookedArray = array();
                $squery = $d->facility_availability($facility_id,$book_date,$end_date,$society_id,$facility_schedule_id,$no_of_person);
                while ($row=mysqli_fetch_array($squery)) {
                    if ($row['avl']<$no_of_person) {
                        $isSlotAvailble = '1';
                    }
                    array_push($availblityArray, $row['avl']);
                    array_push($alreadyBookedArray, $row);
                }

                if ($isSlotAvailble==1) { 
                  $time_slot["available_count"]='0';
                  $time_slot["availability"]=false;
                  $time_slot["available_message"]='Sold Out';
                }  else {
                  $time_slot["available_count"]='1';
                  $time_slot["availability"]=true;
                  $time_slot["available_message"]='';
                }
               
                
                $time_slot["book_price"]=number_format($facility_amount,2,'.','');  
                $time_slot["facility_start_time"]=date('h:i A',strtotime($sData["facility_start_time"])).' to '.date('h:i A',strtotime($sData["facility_end_time"]));

                array_push($response["time_slot"], $time_slot); 
              }

           $response["start_date"]=date('d M Y',strtotime($book_date));
           $response["end_date"]=date('d M Y',strtotime($end_date));
           $response["message"]="$datafoundMsg";
           $response["status"]="200";
           echo json_encode($response);

        } else {
           $no_time_slot_available_for_this_date = $xml->string->no_time_slot_available_for_this_date;
           $response["message"]="$no_time_slot_available_for_this_date";
           $response["status"]="201";
           echo json_encode($response);
        }


    }  else if($_POST['bookFacility']=="bookFacility" && $facility_id!=''  ){
           
            $sp = $d->select("facility_package_master","facility_package_id='$facility_package_id' AND facility_id='$facility_id'","");
            $packageData= mysqli_fetch_array($sp);
            // $no_of_person = $packageData['number_of_person'];
            $package_amount = $packageData['package_amount'];
            $package_type = $packageData['package_type'];

            
           
            $cq = $d->select("facilities_master","facility_id='$facility_id'");
            $cData = mysqli_fetch_array($cq);
            $facility_type=$cData['facility_type'];
            $amount_type=$cData['amount_type'];
            $person_limit = $cData['person_limit'];
            $booking_start_time_days = $bookingStartTimeDays;
            $booking_end_time_days = $bookingEndTimeDays;
            // $transection_amount= $paymentTransactionsAmount;
            $payment_for_name= $paymentForName;
            $user_name= $userName;
            $event_id = $eventId;


              $booked_date = date('Y-m-d',strtotime($facility_book_date));
              $ids = join("','",$_POST['bookingSelectedIds']); 
            if ($package_type==0) {
                $sq = $d->select("facility_schedule_master","facility_start_time!='00:00:00' AND facility_id='$facility_id' AND  facility_schedule_id IN ('$ids')","");
                while ($sData= mysqli_fetch_array($sq)) {
                  
                  $count5=$d->sum_data("no_of_person","facility_booking_months","cancel_status=0 AND facility_id='$facility_id'  AND facility_schedule_id='$sData[facility_schedule_id]' AND booking_start='$booked_date'");
                  $row11=mysqli_fetch_array($count5);
                 $booke_count=$row11['SUM(no_of_person)'];
                  if ($booke_count=='') {
                    $booke_count =0;
                  }
                  $avSeat = $person_limit - $booke_count;
                  if ($avSeat<$no_of_person && $amount_type==0 ||  $amount_type==1 && $booke_count>0) {
                      $response["message"]= "$during_trans_booking_full";
                      $response["status"]="201";
                      echo json_encode($response);
                      exit();
                    }

                }
              } else {
                  switch ($package_type) {
                    case '0':
                        // Single Booking
                        $endDate = $booked_date;
                        break;
                    case '1':
                        // Monthly Booking...
                        $x = strtotime($booked_date);
                        $endDate = date("Y-m-d",strtotime("+1 month",$x));
                        break;
                    case '2':
                        // Quarterly Booking...
                        $x = strtotime($booked_date);
                        $endDate = date("Y-m-d",strtotime("+3 month",$x));
                        break;
                    
                    case '3':
                        // Half Yearly Booking...
                        $x = strtotime($booked_date);
                        $endDate = date("Y-m-d",strtotime("+6 month",$x));
                        break;
                    case '4':
                        // Yearly Booking...
                        $x = strtotime($booked_date);
                        $endDate = date("Y-m-d",strtotime("+12 month",$x));
                        break;
                    
                    default:
                        // code...
                        break;
                 }
              
              }

                if ($package_type!=0){
                    $end_date = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $endDate) ) ));
                } else {
                  $end_date = $booked_date;
                }

              // Day Wise
              $booked_date = date('Y-m-d',strtotime($facility_book_date));
              $booking_start_time = date('H:i:s',strtotime($booking_start_time_days));
              $booking_end_time = date('H:i:s',strtotime($booking_end_time_days));
              // $effectiveDate = strtotime("+$no_of_days days", strtotime($booked_date)); // returns timestamp
              $facility_amount= $received_amount;
              $booking_expire_date = $end_date;
            
          
                
              $ref_no="REF".date('Ymd').$unit_id.$facility_id;
              $payment_status = 0;



            $bookingDate=date("Y-m-d H:i:s");

          
            $uq=$d->select("unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND unit_master.block_id=block_master.block_id AND users_master.user_id='$user_id' AND unit_master.society_id='$society_id'");
            $unitData=mysqli_fetch_array($uq);
            $unitName=  $unitData['block_name']."-".$unitData['unit_name'];
            $userType=  $unitData['user_type'];
            $unit_id_main=  $unitData['unit_id'];

            $uploadedFile=$_FILES["attachment"]["tmp_name"];
            $ext = pathinfo($_FILES['attachment']['name'], PATHINFO_EXTENSION);
                if(file_exists($uploadedFile)) {
                   
                  $sourceProperties = getimagesize($uploadedFile);
                  $newFileName = rand().$user_id;
                  $dirPath = "../img/billUnits/";
                  $imageType = $sourceProperties[2];
                  $imageHeight = $sourceProperties[1];
                  $imageWidth = $sourceProperties[0];
                  if ($imageWidth>1000) {
                    $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage /100;
                    $newImageHeight = $imageHeight * $newWidthPercentage /100;
                  } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                  }

                  switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagepng($tmp,$dirPath. $newFileName. "_bill.". $ext);
                        break;           

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagejpeg($tmp,$dirPath. $newFileName. "_bill.". $ext);
                        break;
                    
                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagegif($tmp,$dirPath. $newFileName. "_bill.". $ext);
                        break;

                    default:
                      
                        break;
                    }
                $profile_name= $newFileName."_bill.".$ext;

            } else {
             $attachment= "";
            }

              $m->set_data('facility_id',$facility_id);
              $m->set_data('balancesheet_id',$balancesheet_id);
              $m->set_data('facility_amount',$transection_amount);
              $m->set_data('payment_type',$payment_mode);
              $m->set_data('payment_received_date',$bookingDate);
              $m->set_data('no_of_person',$no_of_person); 
              $m->set_data('society_id',$society_id);
              $m->set_data('unit_id',$unit_id);
              $m->set_data('unit_name',$unit_name);
              $m->set_data('book_status','1');
              $m->set_data('receive_amount',$transection_amount);
              $m->set_data('no_of_month', $noOfMonth);
              $m->set_data('user_id', $user_id);
              $m->set_data('userType', $userType);
              $m->set_data('booking_expire_date', $end_date);
              $m->set_data('payment_status', '0');
              $m->set_data('booked_date',$booked_date);
              $m->set_data('booking_start_time',$booking_start_time);
              $m->set_data('booking_end_time',$booking_end_time);
              $m->set_data('payment_bank',$bankcode);
              $m->set_data('payment_ref_no',$payment_note);
              $m->set_data('no_of_person',$no_of_person);
              $m->set_data('no_of_month',$noOfMonth);
              $m->set_data('user_id',$user_id);
              $m->set_data('userType',$userType);
              $m->set_data('wallet_amount',$wallet_amount);
              $m->set_data('wallet_amount_type',$wallet_amount_type);
              $m->set_data('facility_package_id',$facility_package_id);
              $m->set_data('gatekeeper_id',$my_id);

              $a = array(
                  'facility_id' => $m->get_data('facility_id'),
                  'balancesheet_id'=> $m->get_data('balancesheet_id'),
                  'society_id' => $m->get_data('society_id'),
                  'booked_date' => $m->get_data('booked_date'),
                  'unit_id' => $m->get_data('unit_id'),
                  'receive_amount' => $m->get_data('receive_amount'),
                  'transaction_charges' => $m->get_data('transaction_charges'),
                  'unit_name' => $m->get_data('unit_name'),
                  'book_status' => $m->get_data('book_status'),
                  'book_request_date' => $m->get_data('book_request_date'),
                  'booking_start_time'=> $m->get_data('booking_start_time'),
                  'booking_end_time'=> $m->get_data('booking_end_time'),
                  'book_request_date'=> $booked_date,
                  'payment_type' => $m->get_data('payment_type'),
                  'payment_ref_no' => $m->get_data('payment_ref_no'),
                  'payment_status' => $m->get_data('payment_status'),
                  'payment_received_date' => $m->get_data('payment_received_date'),
                  'no_of_person' => $m->get_data('no_of_person'),
                  'booking_expire_date' => $m->get_data('booking_expire_date'),
                  'no_of_month' => $m->get_data('no_of_month'),
                  'user_id' => $m->get_data('user_id'),
                  'userType' => $m->get_data('userType'),
                  'paid_by' => $m->get_data('user_id'),
                  'wallet_amount'=> $m->get_data('wallet_amount'),
                  'wallet_amount_type'=> $m->get_data('wallet_amount_type'),
                  'facility_package_id'=> $m->get_data('facility_package_id'),
                  'gatekeeper_id'=> $m->get_data('gatekeeper_id'),
                );

            $memberPlayerArray = array();
            $totalMemberPlayers = count($_POST['bookingSelectedUserIds']);
            if ($totalMemberPlayers<1) {
                // code...
                $response["message"]="Member Name Missing";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }
            
            for ($userPlayer=0; $userPlayer <$totalMemberPlayers ; $userPlayer++) { 
              $memberData = array();

              $player_user_id = $_POST['bookingSelectedUserIds'][$userPlayer];
              $seUser= $d->selectRow("unit_id,user_token,user_mobile,user_full_name","users_master","user_id='$player_user_id'");
              $plyerData = mysqli_fetch_array($seUser);
              $unit_id_player = $plyerData['unit_id'];
              $user_mobile_player = $plyerData['user_mobile'];
              $user_token_player = $plyerData['user_token'];
              $user_full_name = $plyerData['user_full_name'];

              $memberData["user_plyer_id"]=$player_user_id;
              $memberData["unit_plyer_id"]=$unit_id_player;
              $memberData["user_token_player"]=$user_token_player;
              if ($player_user_id>0) {
                $memberData["user_full_name"]=$user_full_name;
                $memberData["user_player_mobile"]=$user_mobile_player;
                $memberData["is_guest"]='0';
              } else {
                $memberData["user_full_name"]=$_POST['bookingSelectedUserName'][$userPlayer];
                $memberData["user_player_mobile"]=$_POST['bookingSelectedUserMobile'][$userPlayer];
                $memberData["is_guest"]='1';
              }

              array_push($memberPlayerArray, $memberData);
              
            }   

            $q=$d->insert('facilitybooking_master',$a);
            $booking_id = $con->insert_id;
            if ($q==true) {

            


            if ($package_type==0) {  
               for ($i1=0; $i1 <count($_POST['bookingSelectedIds']) ; $i1++) { 
                $facility_schedule_id = $_POST['bookingSelectedIds'][$i1];
                
                $user_id_player = $_POST['bookingSelectedUserIds'][$i1];
                $is_guest = $_POST['bookingSelectedUserMemberType'][$i1];
                $player_mobile = $_POST['bookingSelectedUserMobile'][$i1];
                $player_name = $_POST['bookingSelectedUserName'][$i1];
                $player_unit_id = $_POST['bookingSelectedUnitIds'][$i1];
                
                $m->set_data('facility_schedule_id',$facility_schedule_id);
                
                $sq1 = $d->select("facility_schedule_master","facility_schedule_id='$facility_schedule_id' ","");
                $sData= mysqli_fetch_array($sq1);
                $facility_start_time = $sData['facility_start_time'];
                $facility_end_time = $sData['facility_end_time'];

                 $aMonth= array (
                  'society_id'=> $society_id,
                  'booking_id'=> $booking_id,
                  'facility_id'=> $m->get_data('facility_id'),
                  'user_id'=> $m->get_data('user_id'),
                  'unit_id'=> $m->get_data('unit_id'),
                  'no_of_person'=> $m->get_data('no_of_person'),
                  'month_name'=> 'Time Slot',
                  'booking_start_time'=> $facility_start_time,
                  'booking_end_time'=> $facility_end_time,
                  'facility_schedule_id'=> $m->get_data('facility_schedule_id'),
                  'facility_package_id'=> $m->get_data('facility_package_id'),
                  'booking_start'=> $m->get_data('booked_date'),
                  'booking_end'=> $m->get_data('booking_expire_date'),
                );
                 $d->insert("facility_booking_months",$aMonth);
                 $facility_booking_month_id = $con->insert_id;

                  for ($ij=1; $ij <=$no_of_person ; $ij++) { 

                    $ijTeamp = $ij-1;
                    $player_name = $memberPlayerArray[$ijTeamp]['user_full_name'];
                    $player_mobile = $memberPlayerArray[$ijTeamp]['user_player_mobile'];
                    $user_plyer_id = $memberPlayerArray[$ijTeamp]['user_plyer_id'];
                    $unit_plyer_id = $memberPlayerArray[$ijTeamp]['unit_plyer_id'];
                    $is_guest = $memberPlayerArray[$ijTeamp]['is_guest'];
                    if ($player_name=='') {
                      $player_name = "Player ".$ij;
                    }
                    
                    $m->set_data('is_guest',$is_guest);
                    $m->set_data('user_plyer_id',$user_plyer_id);
                    $m->set_data('unit_plyer_id',$unit_plyer_id);
                    $m->set_data('player_name',$player_name);
                    $m->set_data('player_mobile',$player_mobile);

                    $aPlayer= array (
                      'society_id'=> $society_id,
                      'booking_id'=> $booking_id,
                      'facility_id'=> $m->get_data('facility_id'),
                      'user_id'=> $m->get_data('user_plyer_id'),
                      'unit_id'=> $m->get_data('unit_plyer_id'),
                      'player_name'=> $m->get_data('player_name'),
                      'player_mobile'=> $m->get_data('player_mobile'),
                      'is_guest'=> $m->get_data('is_guest'),
                      'facility_schedule_id'=> $m->get_data('facility_schedule_id'),
                      'facility_booking_month_id'=> $facility_booking_month_id,
                      'booking_date'=> $m->get_data('booked_date'),
                      'booking_start_time'=> $facility_start_time,
                      'booking_end_time'=> $facility_end_time,
                    );

                    $d->insert("facility_players_master",$aPlayer);

                 }
              }

            } else {
              // multi days booking
              $begin = new DateTime($booked_date);
              $end = new DateTime($endDate);
              $interval = DateInterval::createFromDateString('1 day');
              $period = new DatePeriod($begin, $interval, $end);
              foreach ($period as $dt) {
                $dailyDate = $dt->format("Y-m-d");
                  for ($i1=0; $i1 <count($_POST['bookingSelectedIds']) ; $i1++) { 
                  $facility_schedule_id = $_POST['bookingSelectedIds'][$i1];
                  
                  $user_id_player = $_POST['bookingSelectedUserIds'][$i1];
                  $is_guest = $_POST['bookingSelectedUserMemberType'][$i1];
                  $player_mobile = $_POST['bookingSelectedUserMobile'][$i1];
                  $player_name = $_POST['bookingSelectedUserName'][$i1];
                  $player_unit_id = $_POST['bookingSelectedUnitIds'][$i1];
                  
                  $m->set_data('facility_schedule_id',$facility_schedule_id);
                  
                  $sq1 = $d->select("facility_schedule_master","facility_schedule_id='$facility_schedule_id' ","");
                  $sData= mysqli_fetch_array($sq1);
                  $facility_start_time = $sData['facility_start_time'];
                  $facility_end_time = $sData['facility_end_time'];

                   $aMonth= array (
                    'society_id'=> $society_id,
                    'booking_id'=> $booking_id,
                    'facility_id'=> $m->get_data('facility_id'),
                    'user_id'=> $m->get_data('user_id'),
                    'unit_id'=> $m->get_data('unit_id'),
                    'no_of_person'=> $m->get_data('no_of_person'),
                    'month_name'=> 'Time Slot',
                    'booking_start_time'=> $facility_start_time,
                    'booking_end_time'=> $facility_end_time,
                    'facility_schedule_id'=> $m->get_data('facility_schedule_id'),
                    'facility_package_id'=> $m->get_data('facility_package_id'),
                    'booking_start'=> $dailyDate,
                    'booking_end'=> $dailyDate,
                  );
                   $d->insert("facility_booking_months",$aMonth);
                   $facility_booking_month_id = $con->insert_id;

                    for ($ij=1; $ij <=$no_of_person ; $ij++) { 

                      $ijTeamp = $ij-1;
                      $player_name = $memberPlayerArray[$ijTeamp]['user_full_name'];
                      $player_mobile = $memberPlayerArray[$ijTeamp]['user_player_mobile'];
                      $user_plyer_id = $memberPlayerArray[$ijTeamp]['user_plyer_id'];
                      $unit_plyer_id = $memberPlayerArray[$ijTeamp]['unit_plyer_id'];
                      $is_guest = $memberPlayerArray[$ijTeamp]['is_guest'];
                      if ($player_name=='') {
                        $player_name = "Player ".$ij;
                      }
                      
                      $m->set_data('is_guest',$is_guest);
                      $m->set_data('user_plyer_id',$user_plyer_id);
                      $m->set_data('unit_plyer_id',$unit_plyer_id);
                      $m->set_data('player_name',$player_name);
                      $m->set_data('player_mobile',$player_mobile);

                      $aPlayer= array (
                        'society_id'=> $society_id,
                        'booking_id'=> $booking_id,
                        'facility_id'=> $m->get_data('facility_id'),
                        'user_id'=> $m->get_data('user_plyer_id'),
                        'unit_id'=> $m->get_data('unit_plyer_id'),
                        'player_name'=> $m->get_data('player_name'),
                        'player_mobile'=> $m->get_data('player_mobile'),
                        'is_guest'=> $m->get_data('is_guest'),
                        'facility_schedule_id'=> $m->get_data('facility_schedule_id'),
                        'facility_booking_month_id'=> $facility_booking_month_id,
                        'booking_date'=> $dailyDate,
                        'booking_start_time'=> $facility_start_time,
                        'booking_end_time'=> $facility_end_time,
                      );

                      $d->insert("facility_players_master",$aPlayer);

                   }
                }


              }

            }

              $facility_name = html_entity_decode($cData['facility_name']);
                $block_id=$d->getBlockid($user_id);
               $fcmArray=$d->selectAdminBlockwise("11",$block_id,"android");
               $fcmArrayIos=$d->selectAdminBlockwise("11",$block_id,"ios");
                $facility = $xml->string->facility;
                  $title = "$facility_name $facility booked by guard";
                  $description  ="Booking for $user_name ($unit_name)";

                  $nAdmin->noti_new($society_id,"",$fcmArray,$title,$description,"facilityBook?id=$facility_id");
                  $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,$title,$description,"facilityBook?id=$facility_id");
                          
                    $notiAry = array(
                        'society_id'=>$society_id,
                        'notification_tittle'=>$title,
                        'notification_description'=>$description,
                        'notifiaction_date'=>date('Y-m-d H:i'),
                        'notification_action'=>"facilityBook?id=$facility_id",
                        'admin_click_action'=>"facilityBook?id=$facility_id",
                        'notification_logo'=>'FacilitiesNew.png',
                      );
                      $d->insert("admin_notification",$notiAry);

                   $d->insert_myactivity($user_id,"$society_id","0","$user_name","Free $facility_name $facility booked","FacilitiesNew.png");
                          
                           
                 
                $paid_successfully = $xml->string->booked;

                $response["message"]="$paid_successfully";
                $response["status"]="200";
                echo json_encode($response);
                if ($user_email!='') {
                  $invoice_number= "INVFAC".$booking_id;
                  $ref_no="REF".date('Ymd').$unit_id.$facility_id;
                  $facility_name=$payment_for_name;
                  $received_amount = $transection_amount;
                  $to=$user_email;
                  $subject="$facility_name $facility Booking Payment Acknowledgement #$invoice_number";
                  if ($facility_type==0) {
                    $description=$facility_name."  Booking Date : $booked_date";
                  }else if ($facility_type==1) {
                    $description=$facility_name." Booking Date : $booked_date to $booking_expire_date for $no_of_person Person";
                  }else if ($facility_type==3) {
                    $description=$facility_name." (Booking Date : $booked_date Time: ". date('h:i A',strtotime($booking_start_time)).' - '. date('h:i A',strtotime($booking_end_time)).')';
                  } else {
                    $description=$facility_name." Booking Date : $booked_date ";
                  }
                  $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id_main&type=Fac&societyid=$society_id&id=$booking_id&facility_id=$facility_id";

                  $receive_date = $bookingDate;
                  $category = "Facility";
                 

                  // include '../apAdmin/mail/paymentReceipt.php';
                  // include '../apAdmin/mail.php';
                }

            }else{

                $response["message"]="$somethingWrong";
                $response["status"]="201";
                echo json_encode($response);
      
            }

        } else if (isset($getChatMemberList) && $getChatMemberList=='getChatMemberList' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

        $response["member"]=array();


        
        $blocks_id = $access_blocks;
        
        if ($blocks_id!="") {
            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND block_master.block_id IN($blocks_id) ","ORDER BY users_master.user_first_name ASC");
        } else {
        $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1","ORDER BY users_master.user_first_name ASC");
        }


        if (mysqli_num_rows($q3)>0) {

                while($data=mysqli_fetch_array($q3)) {
                $data = array_map("html_entity_decode", $data);
                    

                $userId= $data['user_id'];


                $member = array(); 
                $member["user_id"]=$data['user_id'];
               
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['block_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["unit_name"]=$data['unit_name'];
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["country_code"]=$data['country_code'];
                $member["public_mobile"]=$data['mobile_for_gatekeeper'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];



               
                
                     array_push($response["member"], $member); 
                }

               
            $temp=true;
         }else {
            $temp=false; 
         }

        if ($temp==true || $temp1 ==true) {
            $response["message"] =''.$xml->string->getMemberlistSucess;
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] =''.$xml->string->faildGetlist;
            $response["status"] = "201";
            echo json_encode($response);
        }


    } 


    else if($_POST['getTimeSlot']=="getTimeSlot" && filter_var($facility_id, FILTER_VALIDATE_INT) == true){

    
      $book_date= date("Y-m-d");

        $sq = $d->select("facility_schedule_master","facility_id=$facility_id AND active_status=0","ORDER BY facility_start_time ASC");

        if(mysqli_num_rows($sq)>0) {
            $response["time_slot"] = array();

          while ($sData= mysqli_fetch_array($sq)) {
            $time_slot=array();
           
            $time_slot["facility_schedule_id"]=$sData["facility_schedule_id"];
           

            $time_slot["facility_start_time"]=date('h:i A',strtotime($sData["facility_start_time"])).' to '.date('h:i A',strtotime($sData["facility_end_time"]));

            array_push($response["time_slot"], $time_slot); 
          }

          
           $response["message"]="$datafoundMsg";
           $response["status"]="200";
           echo json_encode($response);
        } 
         else {
           $no_time_slot_available_for_this_date = $xml->string->no_time_slot_available_for_this_date;
           $response["message"]="$no_time_slot_available_for_this_date";
           $response["status"]="201";
           echo json_encode($response);
        }


    } 
    else if($_POST['getPlayerInfo']=="getPlayerInfo" && filter_var($facility_schedule_id, FILTER_VALIDATE_INT) == true){

        $book_date= date("Y-m-d");
      
        $sq = $d->select("facility_players_master LEFT JOIN users_master ON users_master.user_id= facility_players_master.user_id","facility_schedule_id=$facility_schedule_id AND booking_date='$book_date'","");

        if(mysqli_num_rows($sq)>0) {
            $response["player"] = array();

          while ($sData= mysqli_fetch_array($sq)) {
            $player=array();
           
            $player["user_id"]=$sData["user_id"];
            $player["facility_schedule_id"]=$sData["facility_schedule_id"];
            $player["player_name"]=$sData["player_name"];
            $player["player_mobile"]=$sData["player_mobile"];
            $player["user_profile_pic"]= ($sData["user_profile_pic"]!="") ? $base_url."img/users/recident_profile/".$sData["user_profile_pic"] : "";
             $player["is_guest"]=$sData["is_guest"];
             $player["entry_status"]=$sData["entry_status"];
             $player["exit_status"]=$sData["exit_status"];
             $player["entry_date"]=$sData["entry_date"]."";
             $player["exit_date"]=$sData["exit_date"]."";



                $last3Digit=  substr($sData['player_mobile'], -3);
                $passes["under_maintenance"]=false;
                $qr_content = "TKT_".$sData['booking_id'].'_'.$sData['facility_id'].'_'.$last3Digit.'_'.$sData['pleyer_id'];   
                        

            $qr_content = base64_encode($qr_content);

             $player["qr_content"]=$qr_content;
            array_push($response["player"], $player); 
          }

           $response["message"]="$datafoundMsg";
           $response["status"]="200";
           echo json_encode($response);
        } 
         else {
           $no_time_slot_available_for_this_date = $xml->string->no_time_slot_available_for_this_date;
           $response["message"]="$no_time_slot_available_for_this_date";
           $response["status"]="201";
           echo json_encode($response);
        }
        } 
        else{
            $response["message"] = "wrong tagg.";
            $response["status"] = "201";
            echo json_encode($response);
        }
       

    } else {
        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
