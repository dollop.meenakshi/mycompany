<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
    if($_POST['addChildSecurity']=="addChildSecurity" && $unit_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

       
            $uploadedFile=$_FILES["child_photo"]["tmp_name"];
            $ext = pathinfo($_FILES['child_photo']['name'], PATHINFO_EXTENSION);
                if(file_exists($uploadedFile)) {
                   
                  $sourceProperties = getimagesize($uploadedFile);
                  $newFileName = rand().$user_id;
                  $dirPath = "../img/visitor/";
                  $imageType = $sourceProperties[2];
                  $imageHeight = $sourceProperties[1];
                  $imageWidth = $sourceProperties[0];
                  if ($imageWidth>1000) {
                    $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage /100;
                    $newImageHeight = $imageHeight * $newWidthPercentage /100;
                  } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                  }

                  switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagepng($tmp,$dirPath. $newFileName. "_child.". $ext);
                        break;           

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagejpeg($tmp,$dirPath. $newFileName. "_child.". $ext);
                        break;
                    
                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagegif($tmp,$dirPath. $newFileName. "_child.". $ext);
                        break;

                    default:
                      
                        break;
                    }
                $child_photo= $newFileName."_child.".$ext;

                $notyUrl= $base_url.'img/visitor/'.$child_photo;
            } else {
                if ($update==1) {
                    $child_photo=$child_photo_old;
                }else {
                    $child_photo= "visitor_default.png";
                     $notyUrl="";
                }
            }
            
            $exitTime  = date("H:i:s", strtotime($exit_time));
            $allow_for = html_entity_decode($allow_for);

            $exit_timeDate =  date("Y-m-d").' '.$exitTime;

            $visitDate =  $visit_date.' '.$visit_time;
            $valid_till = 1;
            $nextHours= $valid_till *60;
            $timestamp = strtotime($exit_timeDate) + 60*$nextHours;

            $valid_till_date = date('Y-m-d H:i:s', $timestamp);

            $qp=$d->selectRow("parent_id","users_master","user_id='$user_id'");
            $childData=mysqli_fetch_array($qp);
            $parent_id = $childData['parent_id'];

            $qche=$d->select("users_master","delete_status=0 AND member_status=0 AND unit_id='$unit_id' AND user_id='$parent_id'","");
            $userData=mysqli_fetch_array($qche);
            
            
            if ($userData['child_gate_approval']==0  && $userData['user_status']!=2) {
                $security_status=2;
            } else {
               $security_status = 1;
            }

            $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$parent_id'");
            $data_notification=mysqli_fetch_array($qUserToken);
            $visitor_approved=$data_notification['visitor_approved'];
            $deliveryApproval=$data_notification['Delivery_cab_approval'];
            $sos_user_token=$data_notification['user_token'];
            $device=$data_notification['device'];
            $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];
            $block_id=$data_notification['block_id'];
             $user_full_name=$data_notification['user_full_name'];
             $user_mobile=$data_notification['user_mobile'];

            if ($allow_for=='') {
                $allow_for ="Gate Exit";
                $notTitle= "$child_name wants to go out of building"; 
                $notDesc= "Waiting for your approval on gate ($gatekeeper_name)";
            } else if($allow_for=='Gate Exit') {
                $notTitle= "$child_name wants to go out of building"; 
                $notDesc= "Waiting for your approval on gate ($gatekeeper_name)";
            } else {
                $notTitle= "$child_name wants to enter $allow_for"; 
                $notDesc= "Waiting For Your Approval";
            }
           
            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$parent_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('emp_id',$emp_id);
            $m->set_data('child_name',$child_name);
            $m->set_data('exit_time',$exit_timeDate);
            $m->set_data('valid_till_date',$valid_till_date);
            $m->set_data('child_photo',$child_photo);
            $m->set_data('security_status',$security_status);
            $m->set_data('allow_for',$allow_for);
            $m->set_data('filter_date',date("Y-m-d"));
           
                if($update=="1"){
                     $a = array(
                        'society_id'=>$m->get_data('society_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'emp_id'=>$m->get_data('emp_id'),
                        'child_name'=>$m->get_data('child_name'),
                        'exit_time'=>$m->get_data('exit_time'),
                        'valid_till_date'=>$m->get_data('valid_till_date'),
                        'child_photo'=>$m->get_data('child_photo'),
                        'security_status'=>$m->get_data('security_status'),
                        'filter_date'=>$m->get_data('filter_date'),
                        'allow_for'=>$m->get_data('allow_for'),
                    );

                    $quserDataUpdate = $d->update("child_security_master",$a,"child_security_id='$child_security_id'");
                    $response["message"]=$childdataUpdatesucess;

                    $visit_time= date("d M Y h:i A", strtotime($exit_timeDate));
                

                    $clickAray = array(
                        'title' => "newChildApprove",
                        'child_security_id' => $child_security_id,
                        'visitor_name' => $child_name,
                        'child_photo' => $notyUrl,
                        'visit_time' => $visit_time,
                        'unit_name'=>$unit_name,
                        'user_id' => $user_id,
                        'block_id' => $block_id,
                        'user_name' => $user_full_name,
                        'base_url' => $base_url,
                        'society_id' => $society_id,
                        'user_mobile' => $user_mobile,
                        'allow_for' => $allow_for,
                    );

                }else{
                    $a = array(
                        'society_id'=>$m->get_data('society_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'emp_id'=>$m->get_data('emp_id'),
                        'child_name'=>$m->get_data('child_name'),
                        'exit_time'=>$m->get_data('exit_time'),
                        'valid_till_date'=>$m->get_data('valid_till_date'),
                        'child_photo'=>$m->get_data('child_photo'),
                        'security_status'=>$m->get_data('security_status'),
                        'filter_date'=>$m->get_data('filter_date'),
                        'allow_for'=>$m->get_data('allow_for'),
                    );

                    
                    $quserDataUpdate = $d->insert("child_security_master",$a);
                    $child_security_id= $con->insert_id;
                    if ($userData['child_gate_approval']==0  && $userData['user_status']!=2) {
                        $response["message"]=''.$xml->string->childExitwaitingApproval;
                    } else {
                        $response["message"]=''.$xml->string->childExitallow;
                    }

                     $visit_time= date("d M Y h:i A", strtotime($exit_timeDate));

                    $clickAray = array(
                        'title' => "newChildApprove",
                        'child_security_id' => $child_security_id,
                        'visitor_name' => $child_name,
                        'child_photo' => $notyUrl,
                        'visit_time' => $visit_time,
                        'unit_name'=>$unit_name,
                        'user_id' => $user_id,
                        'block_id' => $block_id,
                        'user_name' => $user_full_name,
                        'base_url' => $base_url,
                        'society_id' => $society_id,
                        'user_mobile' => $user_mobile,
                        'allow_for' => $allow_for,
                    );

                }

            if($quserDataUpdate==TRUE){

                if ($userData['child_gate_approval']==0 && $userData['user_status']!=2) {


                        
                        if ($device=='android') {
                           $nResident->noti("ChildSecurityFragment",$notyUrl,$society_id,$sos_user_token,$notTitle,$notDesc, $clickAray);
                        }  else if($device=='ios') {
                           $nResident->noti_ios("ChildSecurityVC",$notyUrl,$society_id,$sos_user_token,$notTitle,$notDesc, $clickAray);
                        }

                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$user_id,
                        'notification_title'=>$notTitle,
                        'notification_desc'=>$notDesc,
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'child',
                        'notification_logo'=>'gate.png',
                       
                      );
                      
                    $d->insert("user_notification",$notiAry);
                }

                $response["status"]="200";
                echo json_encode($response);

            }else{

                $response["message"]=$somthingWrong;
                $response["status"]="201";
                echo json_encode($response);

            }

        }  else if($_POST['getChildSecurityList']=="getChildSecurityList" && filter_var($society_id, FILTER_VALIDATE_INT) == true  ){

            if ($access_blocks>0) {
                $appedBlockAccessQuery = " AND block_master.block_id IN($access_blocks)";
            }

                $d->delete("child_security_master","security_status=2 AND DATE(exit_time) < CURDATE() ");

                if ($filter_date!='') {
                    $filter_date  = date("Y-m-d", strtotime($filter_date));

                    $qw=$d->select("users_master,child_security_master,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND  child_security_master.user_id=users_master.user_id AND  child_security_master.society_id='$society_id' AND child_security_master.filter_date ='$filter_date' ","ORDER BY child_security_master.child_security_id DESC LIMIT 100");
                } else {
                    $qw=$d->select("users_master,child_security_master,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND  child_security_master.user_id=users_master.user_id AND child_security_master.society_id='$society_id' AND emp_id=0  AND child_security_master.exit_time  >= ( CURDATE() - INTERVAL 1 DAY )  OR block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND  child_security_master.user_id=users_master.user_id AND child_security_master.society_id='$society_id' AND emp_id='$emp_id' AND child_security_master.exit_time  >= ( CURDATE() - INTERVAL 1 DAY ) OR block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND  child_security_master.user_id=users_master.user_id AND child_security_master.society_id='$society_id' AND child_security_master.security_status='3' AND child_security_master.exit_time  >= ( CURDATE() - INTERVAL 1 DAY )","ORDER BY child_security_master.child_security_id DESC LIMIT 100");
                }


                if(mysqli_num_rows($qw)>0){

                    $response["child"] = array();

                    $not_out_yet = $xml->string->not_out_yet;
                    $not_in_yet = $xml->string->not_in_yet;
                    $approved = $xml->string->approved;
                    $pending_approval = $xml->string->pending_approval;
                    $returned_home = $xml->string->returned_home;
                    $rejected = $xml->string->rejected;
                    $outside_adapter = $xml->string->outside_adapter;
                    $inside_adapter = $xml->string->inside;

                    while($data=mysqli_fetch_array($qw)) {
                        $data = array_map("html_entity_decode", $data);
                        if($data['allow_for']=='Gate Exit') {

                            switch ($data['security_status']) {
                                case '0':
                                    $security_status_view="$not_out_yet";
                                    break;
                                 case '1':
                                    $security_status_view="$approved";
                                    break;
                                 case '2':
                                    $security_status_view="$pending_approval";
                                    break;
                                 case '3':
                                    $security_status_view="$outside_adapter";
                                    break;
                                 case '4':
                                    $security_status_view="$returned_home";
                                    break;
                                 case '5':
                                    $security_status_view="$rejected";
                                    break;
                                default:
                                    $security_status_view="$approved";
                                    break;
                            }
                        } else {
                            switch ($data['security_status']) {
                                case '0':
                                    $security_status_view="$not_in_yet";
                                    break;
                                 case '1':
                                    $security_status_view="$approved";
                                    break;
                                 case '2':
                                    $security_status_view="$pending_approval";
                                    break;
                                 case '3':
                                    $security_status_view="$inside_adapter";
                                    break;
                                 case '4':
                                    $security_status_view="$returned_home";
                                    break;
                                 case '5':
                                    $security_status_view="$rejected";
                                    break;
                                default:
                                    $security_status_view="$approved";
                                    break;
                            }
                        }
                       
                        $child["child_security_id"]=$data['child_security_id'];
                        $child["society_id"]=$data['society_id'];
                        $child["mobile_for_gatekeeper"]=$data['mobile_for_gatekeeper'];
                        $child["unit_id"]=$data['unit_id'];
                        $child["user_id"]=$data['user_id'];
                        $child["allow_for"]=$data['allow_for'];
                        if ($data['mobile_for_gatekeeper']==1) {
                            $child["user_mobile"]="";
                        } else {
                            $child["user_mobile"]=$data['country_code'].' '.$data['user_mobile'];
                        }
                        $child["emp_id"]=$data['emp_id'];
                        $child["child_name"]=$data['child_name'];
                        if ($data['security_status']==5) {
                            $child["exit_time"]="";
                        } else {
                            $child["exit_time"]=date("d M Y h:i A", strtotime($data['exit_time']));
                        }
                        $time1 = strtotime($data['exit_time']);
                        $time2 = strtotime($data['valid_till_date']);
                        $difference = abs($time2 - $time1) / 3600;
                        $difference = (float)$difference;
                        if ($difference==0.25) {
                           $difference= '15 Minutes';
                        }else if ($difference==0.5) {
                           $difference= '30 Minutes';
                        }else if ($difference==1) {
                           $difference= $difference.' Hour';
                        } else {
                            if ($difference<1) {
                               $difference =1;
                            }
                           $difference= $difference.' Hours';
                        }

                        $today=date('Y-m-d');
                        $cTime= date("H:i:s");
                        $expire = strtotime($data['valid_till_date']);
                        $today = strtotime("today $cTime");

                        if($today > $expire){
                            $child["time_expire"]=true;
                        } else {
                            $child["time_expire"]=false;
                        }


                       
                        $child["valid_till"]=date("h:i A", strtotime($data['valid_till_date']));
                        if(file_exists('../img/users/recident_profile/'.$data['child_photo'])) {
                            $child["child_photo"]=$base_url."img/users/recident_profile/".$data['child_photo'];
                        }else {
                            $child["child_photo"]=$base_url."img/visitor/".$data['child_photo'];
                        }
                        $child["child_photo_old"]=$data['child_photo'];
                        $child["security_status"]=$data['security_status'].'';
                        $child["security_status_view"]=$security_status_view;
                        $child["unit_name"]=$data['block_name'].'-'.$data['unit_name'].' ('.$data['user_first_name'] .')';
                       
                        array_push($response["child"], $child); 
                    }

                    $response["message"]=''.$xml->string->hetChildsecuritySucess;
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]=''.$xml->string->noDatafound;
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['allowChildExit']=="allowChildExit" && filter_var($child_security_id, FILTER_VALIDATE_INT) == true && filter_var($emp_id, FILTER_VALIDATE_INT) == true ){

                $qch=$d->selectRow("exit_time,allow_for","child_security_master","child_security_id='$child_security_id'");
                $childData=mysqli_fetch_array($qch);
                $exitTime = $childData['exit_time'];
                $allow_for = $childData['allow_for'];

                 $exitTimeView = date("h:i A", strtotime($childData['exit_time']));

                $today=date('Y-m-d');
                $cTime= date("H:i:s");
                $expire = strtotime($exitTime);
                $today = strtotime("today $cTime");
                if($today < $expire){
                    $response["message"]=$invalidExittime.' '.$exitTimeView;
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }

                
                $m->set_data('emp_id', $emp_id);
                $m->set_data('security_status', '3');
                $m->set_data('exit_time', date("Y-m-d H:i:s"));
                $m->set_data('filter_date', date("Y-m-d"));

                $a = array(
                        'exit_time'=>$m->get_data('exit_time'),
                        'emp_id'=>$m->get_data('emp_id'),
                        'security_status'=>$m->get_data('security_status'),
                        'filter_date'=>$m->get_data('filter_date'),
                    );

                $q = $d->update("child_security_master",$a,"child_security_id='$child_security_id'");

                if ($q>0) {

                    if ($allow_for=='') {
                        $allow_for = "Gate Exit";
                        $notTitle= "$child_name Gate Exit Allowed"; 
                        $notDesc= "Allowed By Security Guard-$gatekeeper_name";
                    } else if($allow_for=='Gate Exit') {
                        $notTitle= "$child_name Gate Exit Allowed"; 
                        $notDesc= "Allowed By Security Guard-$gatekeeper_name";
                    } else {
                        $notTitle= "$child_name $allow_for Entry Allowed"; 
                        $notDesc= "Allowed By $gatekeeper_name";
                    }

                    $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");

                    $data_notification=mysqli_fetch_array($qUserToken);
                     $sos_user_token=$data_notification['user_token'];
                     $device=$data_notification['device'];
                    if ($device=='android') {
                       $nResident->noti("ChildSecurityFragment","",$society_id,$sos_user_token,$notTitle,$notDesc,'child');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("ChildSecurityVC","",$society_id,$sos_user_token,$notTitle,$notDesc,'child');
                    }


                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$user_id,
                        'notification_title'=>$notTitle,
                        'notification_desc'=>$notDesc,
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'child',
                        'notification_logo'=>"gate.png",
                      );
                      
                        $d->insert("user_notification",$notiAry);

                    $response["message"]=''.$xml->string->gateExitallow;
                    $response["status"]="200";
                    echo json_encode($response);
                } else {

                    $response["message"]=''.$xml->string->somthingWrong;
                    $response["status"]="201";
                    echo json_encode($response);
                }
                

            } else if($_POST['allowChildInside']=="allowChildInside" && filter_var($child_security_id, FILTER_VALIDATE_INT) == true && filter_var($emp_id, FILTER_VALIDATE_INT) == true ){

                $qch=$d->selectRow("allow_for","child_security_master","child_security_id='$child_security_id'");
                $childData=mysqli_fetch_array($qch);
                $allow_for = $childData['allow_for'];
                
                $m->set_data('emp_id', $emp_id);
                $m->set_data('security_status', '4');
                $m->set_data('return_date', date("Y-m-d H:i:s"));

                $a = array(
                        'return_date'=>$m->get_data('return_date'),
                        'emp_id_exit'=>$m->get_data('emp_id'),
                        'security_status'=>$m->get_data('security_status'),
                    );

                $q = $d->update("child_security_master",$a,"child_security_id='$child_security_id'");

                if ($q>0) {

                    if ($allow_for=='') {
                        $allow_for = "Gate Exit";
                        $notTitle= "$child_name Gate Entry Allowed"; 
                        $notDesc= "Allowed By Security Guard-$gatekeeper_name";
                    } else if($allow_for=='Gate Exit') {
                        $notTitle= "$child_name Gate Entry Allowed"; 
                        $notDesc= "Allowed By Security Guard-$gatekeeper_name";
                    } else {
                        $notTitle= "$child_name $allow_for Exit Allowed"; 
                        $notDesc= "Allowed By $gatekeeper_name";
                    }

                    $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");

                    $data_notification=mysqli_fetch_array($qUserToken);
                     $sos_user_token=$data_notification['user_token'];
                     $device=$data_notification['device'];
                    if ($device=='android') {
                       $nResident->noti("ChildSecurityFragment","",$society_id,$sos_user_token,$notTitle,$notDesc,'child');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("ChildSecurityVC","",$society_id,$sos_user_token,$notTitle,$notDesc,'child');
                    }


                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$user_id,
                        'notification_title'=>$notTitle,
                        'notification_desc'=>$notDesc,
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'child',
                        'notification_logo'=>"gate.png",
                      );
                      
                        $d->insert("user_notification",$notiAry);

                    $response["message"]=''.$xml->string->gateInallow;
                    $response["status"]="200";
                    echo json_encode($response);
                } else {

                    $response["message"]=''.$xml->string->$somthingWrong;
                    $response["status"]="201";
                    echo json_encode($response);
                }

                

            } else if($_POST['SendChildRemainder']=="SendChildRemainder" && filter_var($child_security_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ){
                
                $q=$d->selectRow("allow_for,security_status,child_name,child_photo,exit_time","child_security_master","child_security_id='$child_security_id' ");
                $vDdata=mysqli_fetch_array($q);
                $child_name=$vDdata['child_name'];
                $exit_time=$vDdata['exit_time'];
                $allow_for=$vDdata['allow_for'];
                $allow_for = html_entity_decode($allow_for);
                $notyUrl= $base_url.'img/visitor/'.$vDdata['child_photo'];
                if ($vDdata['security_status']==2) {

                     $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$user_id'");
                    $data_notification=mysqli_fetch_array($qUserToken);
                    $visitor_approved=$data_notification['visitor_approved'];
                    $deliveryApproval=$data_notification['Delivery_cab_approval'];
                    $sos_user_token=$data_notification['user_token'];
                    $device=$data_notification['device'];
                    $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];
                    $block_id=$data_notification['block_id'];
                    $user_full_name=$data_notification['user_full_name'];
                    $user_id=$data_notification['user_id'];
                    $user_mobile=$data_notification['user_mobile'];
                    
                     $visit_time= date("d M Y h:i A", strtotime($exit_time));

                     $clickAray = array(
                        'title' => "newChildApprove",
                        'child_security_id' => $child_security_id,
                        'visitor_name' => $child_name,
                        'child_photo' => $notyUrl,
                        'visit_time' => $visit_time,
                        'unit_name' => $unit_name,
                        'user_id' => $user_id,
                        'block_id' => $block_id,
                        'user_name' => $user_full_name,
                        'base_url' => $base_url,
                        'society_id' => $society_id,
                        'user_mobile' => $user_mobile,
                        'allow_for' => $allow_for,
                    );

                  

                    if ($allow_for=='') {
                        $allow_for = "Gate Exit";
                        $notTitle= "$child_name wants to go out of building"; 
                        $notDesc= "Waiting for your approval on gate ($gatekeeper_name)";
                    } else if($allow_for=='Gate Exit') {
                        $notTitle= "$child_name wants to go out of building"; 
                        $notDesc= "Waiting for your approval on gate ($gatekeeper_name)";
                    } else {
                        $notTitle= "$child_name wants to enter $allow_for"; 
                        $notDesc= "Waiting For Your Approval";
                    }

                    if ($device=='android') {
                       $nResident->noti("ChildSecurityFragment",$notyUrl,$society_id,$sos_user_token,$notTitle,$notDesc,$clickAray);
                    }  else if($device=='ios') {
                       $nResident->noti_ios("ChildSecurityVC",$notyUrl,$society_id,$sos_user_token,$notTitle,$notDesc,$clickAray);
                    }
                    
                    $response["message"] = ''.$xml->string->reqSuess;
                    $response["status"] = "200";
                    echo json_encode($response);
                }else if($vDdata['security_status']==5) {
                      $response["message"] =''.$xml->string->childReqRejected;
                    $response["status"] = "201";
                    echo json_encode($response);

                }else if($vDdata['security_status']==1) {
                      $response["message"] =''.$xml->string->childALreadyAccept;
                    $response["status"] = "201";
                    echo json_encode($response);

                }else {
                    $response["message"] =''.$xml->string->childPendingreq;
                    $response["status"] = "201";
                    echo json_encode($response);
                }

            } else if($_POST['reAsk']=="reAsk" && filter_var($child_security_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

                $qcheck = $d->select("child_security_master","security_status=0 AND DATE(exit_time) < CURDATE() AND child_security_id = '$child_security_id' OR security_status=2 AND DATE(exit_time) < CURDATE() AND child_security_id = '$child_security_id'");

                if (mysqli_fetch_array($qcheck)>0) {
                    
                    $response["message"] =''.$xml->string->childExitDateisExpire;
                    $response["status"] = "201";
                    echo json_encode($response);
          
                    exit();
                }

                
                $m->set_data('security_status', '2');

                $a222 = array(
                        'security_status'=>$m->get_data('security_status'),
                );

                $q = $d->update("child_security_master",$a222,"child_security_id='$child_security_id'");



                $q=$d->selectRow("security_status,child_name,child_photo,exit_time","child_security_master","child_security_id='$child_security_id' ");
                $vDdata=mysqli_fetch_array($q);
                $child_name=$vDdata['child_name'];
                $exit_time=$vDdata['exit_time'];
                $notyUrl= $base_url.'img/visitor/'.$vDdata['child_photo'];

                
                 $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$user_id'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $visitor_approved=$data_notification['visitor_approved'];
                $deliveryApproval=$data_notification['Delivery_cab_approval'];
                $sos_user_token=$data_notification['user_token'];
                $device=$data_notification['device'];
                $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];
                $block_id=$data_notification['block_id'];
                $user_full_name=$data_notification['user_full_name'];
                $user_id=$data_notification['user_id'];
                $user_mobile=$data_notification['user_mobile'];

                  $visit_time= date("d M Y h:i A", strtotime($exit_time));
                
                 $clickAray = array(
                    'title' => "newChildApprove",
                    'child_security_id' => $child_security_id,
                    'visitor_name' => $child_name,
                    'child_photo' => $notyUrl,
                    'visit_time' => $visit_time,
                    'unit_name' => $unit_name,
                    'user_id' => $user_id,
                    'block_id' => $block_id,
                    'user_name' => $user_full_name,
                    'base_url' => $base_url,
                    'society_id' => $society_id,
                    'user_mobile' => $user_mobile,
                );

                $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $visitor_approved=$data_notification['visitor_approved'];
                $deliveryApproval=$data_notification['Delivery_cab_approval'];
                $sos_user_token=$data_notification['user_token'];
                $device=$data_notification['device'];
                    
                $notTitle= "$child_name Wants To Go Out Of Building"; 
                $notDesc= "Waiting For Your Approval On Gate ";
                if ($device=='android') {
                   $nResident->noti("ChildSecurityFragment",$notyUrl,$society_id,$sos_user_token,$notTitle,$notDesc,$clickAray);
                }  else if($device=='ios') {
                   $nResident->noti_ios("ChildSecurityVC",$notyUrl,$society_id,$sos_user_token,$notTitle,$notDesc,$clickAray);
                }
                
                $response["message"] =''.$xml->string->reqSendsucess;
                $response["status"] = "200";
                echo json_encode($response);
          

            }else if($_POST['deleteChildSecurity']=="deleteChildSecurity" && filter_var($child_security_id, FILTER_VALIDATE_INT) == true  && filter_var($emp_id, FILTER_VALIDATE_INT) == true ){
               
                        $q11=$d->selectRow("security_status","child_security_master","child_security_id='$child_security_id' AND emp_id='$emp_id'");
                        $vDdata=mysqli_fetch_array($q11);


                        if ($vDdata['security_status']==0) {
                             $response["message"]=''.$xml->string->cannotDeleteExpectedentry;
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                        }else if ($vDdata['security_status']==1) {
                             $response["message"]=''.$xml->string->cannotDeletethisEntery;
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                        }else if ($vDdata['security_status']==3) {
                            $response["message"]=''.$xml->string->childReturndelete;
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                        }else if ($vDdata['security_status']==4) {
                            $response["message"]=''.$xml->string->youCannotdeleteReturn;
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                        }


                        $qdelete = $d->delete("child_security_master","child_security_id='$child_security_id'");
            
                
                        if($qdelete==TRUE){

                            // $d->insert_myactivity($user_id,"$society_id","0","$user_name","Child entry deleted","gate.png");
                            
                            $response["message"]=''.$xml->string->deleteSucess;
                            $response["status"]="200";

                            echo json_encode($response);
        
                        }else{
        
                            $response["message"]=''.$xml->string->somthingWrong;
                            $response["status"]="201";
                            echo json_encode($response);
        
                        }
        
            }else if($_POST['getChildAllowType']=="getChildAllowType" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
                
                $response["child_type"] = array();

                $gateArray = array(
                    'society_id' => "0",
                    'allow_place_name' => 'Gate Exit',
                    'place_photo' => $base_url."img/emp_icon/gate_exit.png"

                );

                array_push($response["child_type"], $gateArray); 

                

                $q=$d->select("child_security_type","society_id='$society_id'  AND active_status=0");

                if(mysqli_num_rows($q)>0){


                    while($data_employee_type=mysqli_fetch_array($q)) {
                        $data_employee_type = array_map("html_entity_decode", $data_employee_type);
                        
                        $child_type = array(); 
                        $child_type["society_id"]=$data_employee_type['society_id'];
                        $child_type["allow_place_name"]=$data_employee_type['allow_place_name'];
                        $child_type["place_photo"]=$base_url."img/emp_icon/".$data_employee_type['place_photo'];

                        array_push($response["child_type"], $child_type); 
                    }

                   

                }

                

                $response["message"]="$datafoundMsg";
                $response["status"]="200";
                echo json_encode($response);

                

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>