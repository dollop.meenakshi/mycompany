<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb) {
        
    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d h:i A");
           if($_POST['getListNew']=="getListNew" && $my_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            
                $array1 = array();
                $array2 = array();


                $q=$d->select("lost_found_master,users_master,block_master,unit_master","lost_found_master.society_id='$society_id'  AND lost_found_master.active_status='0' AND  lost_found_master.user_id=users_master.user_id  AND block_master.block_id=unit_master.block_id AND block_master.block_id=lost_found_master.block_id AND unit_master.unit_id=lost_found_master.unit_id AND lost_found_master.user_id!=0 ","ORDER BY lost_found_master.lost_found_master_id DESC" );
                
                $response["lostfound"] = array();

                if(mysqli_num_rows($q)>0){

                    while($data=mysqli_fetch_array($q)) {
                        
                        $lostfoundUser = array(); 

                        $lostfoundUser["lost_found_master_id"]=$data['lost_found_master_id'];
                        $lostfoundUser["society_id"]=$data['society_id'];
                        
                        $lostfoundUser["unit_id"]=$data['unit_id'];
                        $lostfoundUser["user_id"]=$data['user_id'];
                        $lostfoundUser["user_type"]=$data['user_type'];
                        $lostfoundUser["public_mobile"]=$data['public_mobile'];
                        $lostfoundUser["tenant_view"]=$data['tenant_view'];
                        $lostfoundUser["lost_found_title"]=html_entity_decode($data['lost_found_title']);
                        $lostfoundUser["lost_found_description"]=html_entity_decode($data['lost_found_description']);
                        $lostfoundUser["lost_found_date"]=$data['lost_found_date'];
                        $lostfoundUser["block_name"]=$data['block_name'].'-'.$data['unit_name'];
                        $lostfoundUser["user_full_name"]=$data['user_full_name'];
                        $lostfoundUser["user_mobile"]=$data['country_code'].' '.$data['user_mobile'];
                        if ($data['lost_found_image']!="") {
                            $lostfoundUser["lost_found_image"]=$base_url."img/lostFound/".$data['lost_found_image'];
                        } else {
                            $lostfoundUser["lost_found_image"]="";
                        }
                        $lostfoundUser["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                        

                        $lost = $xml->string->lost;
                        $found = $xml->string->found;
                        
                        if($data['lost_found_type']==0)  {
                            $lostfoundUser["lost_found_type"]="$found";
                        } else {
                            $lostfoundUser["lost_found_type"]="$lost";
                        }
                        $lostfoundUser["lost_found_type_int"]=$data['lost_found_type'].'';

                       
                        $lostfoundUser["is_user"]="0";
                       
                        
                        array_push($array1, $lostfoundUser); 

                    }
                }  

                $qbb=$d->select("lost_found_master,employee_master","lost_found_master.society_id='$society_id'  AND lost_found_master.active_status='0' AND  lost_found_master.gatekeeper_id=employee_master.emp_id  ","" );
               
                if(mysqli_num_rows($qbb)>0){

                    while($data=mysqli_fetch_array($qbb)) {
                        
                        $lostfoundGatekeeper = array(); 

                        $lostfoundGatekeeper["lost_found_master_id"]=$data['lost_found_master_id'];
                        $lostfoundGatekeeper["society_id"]=$data['society_id'];
                        
                        $lostfoundGatekeeper["unit_id"]="0";
                        $lostfoundGatekeeper["user_id"]=$data['emp_id'];
                        $lostfoundGatekeeper["user_type"]="0";
                        $lostfoundGatekeeper["public_mobile"]="0";
                        $lostfoundGatekeeper["tenant_view"]="0";
                        $lostfoundGatekeeper["lost_found_title"]=html_entity_decode($data['lost_found_title']);
                        $lostfoundGatekeeper["lost_found_description"]=html_entity_decode($data['lost_found_description']);
                        $lostfoundGatekeeper["lost_found_date"]=$data['lost_found_date'];
                        $security = $xml->string->gatekeeper;
                        $lostfoundGatekeeper["block_name"]=''.$security;
                        $lostfoundGatekeeper["user_full_name"]=$data['emp_name'];
                        $lostfoundGatekeeper["user_mobile"]=$data['country_code'].' '.$data['emp_mobile'];
                        if ($data['lost_found_image']!="") {
                            $lostfoundGatekeeper["lost_found_image"]=$base_url."img/lostFound/".$data['lost_found_image'];
                        } else {
                            $lostfoundGatekeeper["lost_found_image"]="";
                        }
                        $lostfoundGatekeeper["user_profile_pic"]=$base_url."img/emp/".$data['emp_profile'];
                        
                        $lost = $xml->string->lost;
                        $found = $xml->string->found;
                        
                        if($data['lost_found_type']==0)  {
                            $lostfoundGatekeeper["lost_found_type"]="$found";
                        } else {
                            $lostfoundGatekeeper["lost_found_type"]="$lost";
                        }
                        $lostfoundGatekeeper["lost_found_type_int"]=$data['lost_found_type'].'';

                       
                        $lostfoundGatekeeper["is_user"]="1";
                       
                        
                        array_push($array2, $lostfoundGatekeeper); 

                    }
                }  
                
               $newArray = array_merge($array1,$array2);
                foreach ($newArray as $key => $part) {
                       $sort[$key] = strtotime($part['lost_found_master_id']);
                }
                array_multisort($sort, SORT_DESC, $newArray,SORT_DESC);


                for ($i=0; $i <count($newArray) ; $i++) { 
                     $lostfound = array(); 

                        $lostfound["lost_found_master_id"]=$newArray[$i]['lost_found_master_id'];
                        $lostfound["society_id"]=$newArray[$i]['society_id'];
                        $lostfound["unit_id"]=$newArray[$i]['unit_id'];
                        $lostfound["user_id"]=$newArray[$i]['user_id'];
                        $lostfound["user_type"]=$newArray[$i]['user_type'];
                        $lostfound["public_mobile"]=$newArray[$i]['public_mobile'];
                        $lostfound["tenant_view"]=$newArray[$i]['tenant_view'];
                        $lostfound["lost_found_title"]=$newArray[$i]['lost_found_title'];
                        $lostfound["lost_found_description"]=$newArray[$i]['lost_found_description'];
                        $lostfound["lost_found_date"]=$newArray[$i]['lost_found_date'];
                        $lostfound["block_name"]=$newArray[$i]['block_name'];
                        $lostfound["user_full_name"]=$newArray[$i]['user_full_name'];
                        $lostfound["user_mobile"]=$newArray[$i]['user_mobile'];
                        $lostfound["public_mobile"]=$newArray[$i]['public_mobile'];
                        $lostfound["lost_found_image"]=$newArray[$i]['lost_found_image'];
                        $lostfound["user_profile_pic"]=$newArray[$i]['user_profile_pic'];
                        $lostfound["lost_found_type"]=$newArray[$i]['lost_found_type'];
                        $lostfound["lost_found_type_int"]=$newArray[$i]['lost_found_type_int'];
                        if ($newArray[$i]['is_user']==0) {
                            $lostfound["is_user"]=true;
                        } else {
                             $lostfound["is_user"]=false;
                        }
                            
                     array_push($response["lostfound"], $lostfound);

                }

                if (count($newArray)>0) {
                   
                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);

                }


            }else if ($_POST['addLostFound']=="addLostFound" && $my_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($my_id, FILTER_VALIDATE_INT) == true) {

                $uploadedFile = $_FILES['lost_found_image']['tmp_name']; 
                if ($uploadedFile != "") {
                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand().$user_id;
                    $dirPath = "../img/lostFound/";
                    $ext = pathinfo($_FILES['lost_found_image']['name'], PATHINFO_EXTENSION);
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];

                    if ($imageWidth>600) {
                        $newWidthPercentage= 600*100 / $imageWidth;  //for maximum 400 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage /100;
                        $newImageHeight = $imageHeight * $newWidthPercentage /100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }

                    switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagepng($tmp,$dirPath. $newFileName. "_lf.". $ext);
                        break;           

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagejpeg($tmp,$dirPath. $newFileName. "_lf.". $ext);
                        break;
                    
                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagegif($tmp,$dirPath. $newFileName. "_lf.". $ext);
                        break;

                    default:
                        $response["message"]=$invalidImagetype;
                        $response["status"]="201";
                        echo json_encode($response);
                        exit;
                        break;
                    }
                    $lost_found_image= $newFileName."_lf.".$ext;
                    $notiUrl= $base_url.'img/lostFound/'.$lost_found_image;

                    // $temp = explode(".", $_FILES["lost_found_image"]["name"]);
                    // $lost_found_image = 'lf_'.round(microtime(true)) . '.' . end($temp);
                    // $target = '../img/lostFound/'.$lost_found_image;
                    // move_uploaded_file($_FILES['lost_found_image']['tmp_name'], $target);
                } else {
                 $lost_found_image='';
                 $notiUrl='';
                }

                $m->set_data('society_id',$society_id);
                $m->set_data('gatekeeper_id',$my_id);
                $m->set_data('lost_found_title',$lost_found_title);
                $m->set_data('lost_found_description',$lost_found_description);
                $m->set_data('lost_found_date',date('d-m-Y'));
                $m->set_data('lost_found_image',$lost_found_image);
                $m->set_data('lost_found_type',$lost_found_type);
                $m->set_data('active_status',0);

                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'gatekeeper_id'=>$m->get_data('gatekeeper_id'),
                    'lost_found_title'=>$m->get_data('lost_found_title'),
                    'lost_found_description'=>$m->get_data('lost_found_description'),
                    'lost_found_date'=>$m->get_data('lost_found_date'),
                    'lost_found_image'=>$m->get_data('lost_found_image'),
                    'lost_found_type'=>$m->get_data('lost_found_type'),
                    'active_status'=>$m->get_data('active_status')
                );

                    $quserDataUpdate = $d->insert("lost_found_master",$a);
            
                
                if($quserDataUpdate==TRUE){
                    if ($lost_found_type==0) {

                      $notification_title=  "Found an item added by $gatekeeper_name ( Security Guard )";
                      $notification_desc= $lost_found_title;
                       $response["message"]=''.$xml->string->foundItemsucess;
                    
                    } else {
                      $notification_title=  "Lost an item added by $gatekeeper_name ( Security Guard )";
                      $notification_desc= $lost_found_title;
                       $response["message"]=''.$xml->string->lostItemsucess;
                    }

                    $title= $notification_title;
                    $description= $notification_desc;
                    $d->insertUserNotification($society_id,$title,$description,"lostfound","LostFoundNew.png","users_master.user_id!='$user_id'");


                       
                     $fcmArray=$d->get_android_fcm("users_master","user_id!='$user_id' AND user_token!='' AND society_id='$society_id' AND device='android'");
                     $fcmArrayIos=$d->get_android_fcm("users_master","user_id!='$user_id' AND user_token!='' AND society_id='$society_id' AND device='ios'");
                     $nResident->noti("LostAndFoundFragment",$notiUrl,$society_id,$fcmArray,$notification_title,$notification_desc,'lostfound');
                     $nResident->noti_ios("LostAndFoundFragment",$notiUrl,$society_id,$fcmArrayIos,$notification_title,$notification_desc,'lostfound');

                    
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]=''.$xml->string->somthingWrong;
                    $response["status"]="201";
                    echo json_encode($response);

                }

                

            }else if ($_POST['deleteLostFound']=="deleteLostFound" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($my_id, FILTER_VALIDATE_INT) == true) {
                
                $a = array('active_status' =>1);

                $qc=$d->selectRow('lost_found_type',"lost_found_master","lost_found_master_id='$lost_found_master_id' AND gatekeeper_id='$my_id'");
                $empData=mysqli_fetch_array($qc);
                $lost_found_type = $empData['lost_found_type'];
                if ($lost_found_type==0) {
                    $type = "Found";
                } else {
                    $type = "Lost";
                }

                $quserDataUpdate = $d->update("lost_found_master",$a,"lost_found_master_id='$lost_found_master_id' AND user_id='$user_id' AND gatekeeper_id='$my_id'");
            
                if($quserDataUpdate==TRUE){
                   
                    $response["message"]=''.$xml->string->deleteeSucess;
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                        $response["message"]=''.$xml->string->deletefailed;
                        $response["status"]="201";
                        echo json_encode($response);

                }

                

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>