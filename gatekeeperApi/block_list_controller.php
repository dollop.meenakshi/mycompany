    <?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb) { 
    $response = array();
    extract(array_map("test_input" , $_POST));

    if($_POST['getMembers']=="getMembers" && $society_id!=''){

        
              $response["floors"] = array();
              $floor_data=$d->select("floors_master","block_id ='$block_id'");
              if(mysqli_num_rows($floor_data)>0){

              while($data_floor_list=mysqli_fetch_array($floor_data)) {

                    $floors = array(); 

                    $floors["floor_id"]=$data_floor_list['floor_id'];
                    $floors_id=$data_floor_list['floor_id'];


                    $floors["society_id"]=$data_floor_list['society_id'];
                    $floors["block_id"]=$data_floor_list['block_id'];
                    $floors["floor_name"]=$data_floor_list['floor_name'];
                    $floors["floor_status"]=$data_floor_list['floor_status'];


                    $floors["units"] = array();

                    $unit_data=$d->select("unit_master","floor_id ='$floors_id' AND society_id ='$society_id'","");

                    while($data_units_list=mysqli_fetch_array($unit_data)) {

                        $units = array(); 

                        $unit_id = $data_units_list['unit_id'];
                        $qu=$d->select("users_master","delete_status=0 AND unit_id='$unit_id' AND member_status=0 AND society_id ='$society_id' AND users_master.user_status=1");

                        //$qu=$d->selectRow("user_id,user_full_name,user_type,user_status","users_master","unit_id='$data_units_list[unit_id]' AND member_status=0 AND society_id ='$society_id' AND users_master.user_status=1","ORDER BY user_id DESC");
                        $userData=mysqli_fetch_array($qu);

                        $units["unit_id"]=$data_units_list['unit_id'];
                        $units_id=$data_units_list['unit_id'];

                        $units["society_id"]=$data_units_list['society_id'];
                        $units["floor_id"]=$data_units_list['floor_id'];
                        $units["unit_name"]=$data_units_list['unit_name'];
                        if ($data_units_list['unit_status']==5 || $data_units_list['unit_close_for_gatekeeper']==1) {
                            $unit_status= 5;
                        } else {
                            $unit_status= $data_units_list['unit_status'];
                        }
                        
                        $units["unit_status"]=$unit_status;
                        
                        $units["unit_type"]=$data_units_list['unit_type'];

                        $units["user_id"]=$userData['user_id'];
                        $user_id = $userData['user_id'];
                        $units["society_id"]=$data_units_list['society_id'];
                        $units["user_profile_pic"]=$base_url."img/users/recident_profile/".$userData['user_profile_pic'];
                        $units["user_full_name"]=$userData['user_full_name'];
                        $units["delivery_cab_approval"]=$userData['Delivery_cab_approval'];
                        $units["user_type"]=$userData['user_type'];
                        $units["user_status"]=$userData['user_status'];
                        $units["public_mobile"]=$userData['mobile_for_gatekeeper'];


                        $qfamily = $d->select("users_master", "delete_status=0 AND user_status!=0 AND unit_id='$units_id' ","ORDER BY user_id DESC");
                        $units["member"] = array();
                        $units["family_count"]=mysqli_num_rows($qfamily)."";
                        while ($datafamily = mysqli_fetch_array($qfamily)) {

                            if ($datafamily['member_status']==0) {
                                if ($datafamily['user_type']==0) {
                                    $mainUser= "Owner Primary";
                                } else {
                                    $mainUser="Tenant Primary ";
                                }
                                $rel= $mainUser;
                            } else {
                                if ($datafamily['user_type']==0) {
                                    $fType="Owner";
                                } else {
                                    $fType="Tenant";
                                }
                                $rel= $datafamily['member_relation_name'].'-'.$fType;
                            }

                            $member = array();
                            $member["user_id"] = $datafamily['user_id'];

                            $qchatCount=$d->select("chat_master","msg_for='$my_id' AND msg_by='$user_id' AND society_id='$society_id' AND msg_status='0'");
                            $units["chat_status"]=mysqli_num_rows($qchatCount)."";
    
                            $member["member_chat"] = mysqli_num_rows($qchatCount)."";
                            $member["user_first_name"] = $datafamily['user_first_name'];
                            $member["user_last_name"] = $datafamily['user_last_name'] .' ('.$rel  .')';
                            $member["user_mobile"] = $datafamily['user_mobile'];
                            $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
                            $member["member_date_of_birth"] = $datafamily['member_date_of_birth'];
                            $member["member_age"] = "";
                            $member["member_relation_name"] = $datafamily['member_relation_name'];
                            $member["public_mobile"] = $datafamily['public_mobile'];
                            $member["user_status"] = $datafamily['user_status'];
                            $member["member_status"] = $datafamily['member_status'];

                            array_push($units["member"], $member);

                          }


                        array_push($floors["units"], $units);
                    }
                    array_push($response["floors"], $floors);
                }
            
            $response["message"]=''.$xml->string->getMembersucess;
            $response["status"]="200";
            echo json_encode($response);
        } else {

            $response["message"]=''.$xml->string->noFloorsfound;
            $response["status"]="201";
            echo json_encode($response);
        }
    

    }  else if($_POST['getMembersAll']=="getMembersAll"){

        $qe=$d->select("employee_master","emp_id='$my_id' AND society_id='$society_id'");
        $row11=mysqli_fetch_array($qe);
        $blocks_id = $row11['blocks_id'];
        
        if ($blocks_id!="") {
            $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0 AND block_id IN($blocks_id)");
        } else {
            $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0");
        }

        
        if(mysqli_num_rows($block_data)>0){
            $qss=$d->select("society_master"," society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qss); 
            $society_type = $societyData['society_type'];       
            $empty_allow_visitor = $societyData['empty_allow_visitor'];       
            $owner_view_in_tenant_unit = $societyData['owner_view_in_tenant_unit'];       


                $response["block"] = array();

            while($data_block_list=mysqli_fetch_array($block_data)) {

                $block = array(); 

            $block["block_id"]=$data_block_list['block_id'];
            $block_id=$data_block_list['block_id'];


            $block["society_id"]=$data_block_list['society_id'];
            $block["block_name"]=$data_block_list['block_name'];

              $block["block_status"]=$data_block_list['block_status'];
              

              $block["floors"] = array();

              $floor_data=$d->select("floors_master","block_id ='$block_id'");

              while($data_floor_list=mysqli_fetch_array($floor_data)) {

                    $floors = array(); 

                    $floors["floor_id"]=$data_floor_list['floor_id'];
                    $floors_id=$data_floor_list['floor_id'];


                    $floors["society_id"]=$data_floor_list['society_id'];
                    $floors["block_id"]=$data_floor_list['block_id'];
                    $floors["floor_name"]=$data_floor_list['floor_name'];
                    $floors["floor_status"]=$data_floor_list['floor_status'];


                    $floors["units"] = array();

                    $unit_data=$d->select("unit_master","floor_id ='$floors_id'");

                    while($data_units_list=mysqli_fetch_array($unit_data)) {

                        $units = array(); 

                        $units["unit_id"]=$data_units_list['unit_id'];
                        $units_id=$data_units_list['unit_id'];
                        $units["society_id"]=$data_units_list['society_id'];
                        $units["floor_id"]=$data_units_list['floor_id'];
                        $units["unit_name"]=$data_units_list['unit_name'];
                        if ($data_units_list['unit_status']==5 || $data_units_list['unit_close_for_gatekeeper']==1) {
                            $unit_status= 5;
                        } else {
                            $unit_status= $data_units_list['unit_status'];
                        }
                        
                        $units["unit_status"]=$unit_status;
                        $units["unit_type"]=$data_units_list['unit_type'].'';


                        $quser_data=$d->select("users_master","delete_status=0 AND unit_id ='$units_id' AND member_status!=1 AND user_status=1"," ORDER BY user_id DESC");

                        $user_data=mysqli_fetch_array($quser_data);
                        $userTypeTemp = $user_data['user_type'];
                        
                        $user_full_name= $user_data['user_full_name'];
                        

                        $units["user_id"]=$user_data['user_id'].'';
                        $units["about_business"]=$user_data['about_business'];
                        $units["public_mobile"]=$user_data['mobile_for_gatekeeper'];
                        $units["child_gate_approval"]=$user_data['child_gate_approval'];

                        $user_id = $user_data['user_id'];

                        if ($owner_view_in_tenant_unit==1) {
                            $qfamily = $d->select("users_master", "delete_status=0 AND user_status!=0 AND unit_id='$data_units_list[unit_id]' ","ORDER BY user_id DESC");
                        } else {
                            $qfamily = $d->select("users_master", "delete_status=0 AND user_status!=0 AND unit_id='$data_units_list[unit_id]' AND user_type='$userTypeTemp' ","ORDER BY user_id ASC");
                        }
                        $units["member"] = array();
                        $units["family_count"]=mysqli_num_rows($qfamily)."";
                        while ($datafamily = mysqli_fetch_array($qfamily)) {

                            if ($datafamily['member_status']==0) {
                                if ($datafamily['user_type']==0) {
                                    $mainUser= "Owner Primary";
                                } else {
                                    $mainUser="Tenant Primary ";
                                }
                                $rel= $mainUser;
                            } else {
                                if ($datafamily['user_type']==0) {
                                    $fType="Owner";
                                } else {
                                    $fType="Tenant";
                                }
                                $rel= $datafamily['member_relation_name'].'-'.$fType;
                            }

                            $member = array();
                            $member["user_id"] = $datafamily['user_id'];

                            $qchatCount=$d->select("chat_master","msg_for='$my_id' AND msg_by='$user_id' AND society_id='$society_id' AND msg_status='0'");
                            $units["chat_status"]=mysqli_num_rows($qchatCount)."";
    
                            $member["member_chat"] = mysqli_num_rows($qchatCount)."";
                            $member["user_first_name"] = $datafamily['user_first_name'];
                            $member["user_last_name"] = $datafamily['user_last_name'] .' ('.$rel  .')';
                            $member["only_name"] = $datafamily['user_first_name'] .' ('.$rel  .')';
                            $member["user_mobile"] = $datafamily['user_mobile'];
                            $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
                            $member["member_date_of_birth"] = $datafamily['member_date_of_birth'];
                            $member["member_age"] = "";
                            $member["member_relation_name"] = $datafamily['member_relation_name'];
                            $member["public_mobile"] = $datafamily['mobile_for_gatekeeper'];
                            if ($datafamily['user_status']==2) {
                                $member["visitor_approved"] ="0";
                                $member["delivery_cab_approval"]="0";
                                $member["child_gate_approval"]="1";
                            } else {
                                $member["visitor_approved"] = $datafamily['visitor_approved'];
                                $member["delivery_cab_approval"]=$datafamily['Delivery_cab_approval'];
                                $member["child_gate_approval"]=$datafamily['child_gate_approval'];

                            }
                            
                            $member["daily_visitor_approval"]='0';
                            $member["resource_approval"]='0';

                            $member["user_status"] = $datafamily['user_status'];
                            $member["member_status"] = $datafamily['member_status'];

                            array_push($units["member"], $member);

                          }



                        $units["society_id"]=$user_data['society_id'];
                        $units["user_profile_pic"]=$base_url."img/users/recident_profile/".$user_data['user_profile_pic'];
                        $units["user_full_name"]=$user_full_name;
                        $units["user_first_name"]=$user_data['user_first_name'];
                        $units["user_last_name"]=$user_data['user_last_name'];
                        $units["user_mobile"]=$user_data['user_mobile'];
                        $units["user_email"]=$user_data['user_email'];
                        $units["user_id_proof"]=$user_data['user_id_proof'];
                        $units["user_type"]=$user_data['user_type'];
                        $units["user_block_id"]=$user_data['block_id'];
                        $units["user_floor_id"]=$user_data['floor_id'];
                        $units["user_unit_id"]=$user_data['unit_id'];
                        $units["user_status"]=$user_data['user_status'];


                        array_push($floors["units"], $units);


                    }

                    array_push($block["floors"], $floors);

                }

            
                array_push($response["block"], $block); 
            }



           

            $response["population"]= $d->count_data_direct("user_id","users_master","delete_status=0 AND society_id='$society_id' AND user_status!=0");
            if ($empty_allow_visitor==1) {
                $response["empty_allow_visitor"]=true;
            } else {
                $response["empty_allow_visitor"]=false;
            }
            $response["message"]=''.$xml->string->getMembersucess;
            $response["status"]="200";
            echo json_encode($response);
    
        }else{

            $response["message"]=''.$xml->string->getMemberfail;
            $response["status"]="201";
            echo json_encode($response);
    
        }

    } else if($_POST['getBlocks']=="getBlocks" && $society_id!=''){

        $block_data=$d->select("block_master","society_id ='$society_id'");

        if(mysqli_num_rows($block_data)>0){

            $response["block"] = array();

            
            while($data_block_list=mysqli_fetch_array($block_data)) {

                $block = array();

                $block["block_id"]=$data_block_list['block_id'];
                $block["society_id"]=$data_block_list['society_id'];
                $block["block_name"]=$data_block_list['block_name'];
                $block["block_status"]=$data_block_list['block_status'];
                
                array_push($response["block"], $block); 
            }

            $response["message"]=''.$xml->string->getBlocksucess;
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]=''.$xml->string->getBlockfail;
            $response["status"]="201";
            echo json_encode($response);
        
    }


} else if($_POST['getFloorandUnit']=="getFloorandUnit" && $society_id!='' && $block_id!=''){

        $block_data=$d->select("floors_master","society_id ='$society_id' AND block_id ='$block_id'");


        if(mysqli_num_rows($block_data)>0){
            $response["floors"] = array();

            while($data_floor_list=mysqli_fetch_array($block_data)) {

                $floors = array(); 
                $floors["floor_id"]=$data_floor_list['floor_id'];
                $floors_id=$data_floor_list['floor_id'];
                $floors["society_id"]=$data_floor_list['society_id'];
                $floors["block_id"]=$data_floor_list['block_id'];
                $floors["floor_name"]=$data_floor_list['floor_name'];
                $floors["floor_status"]=$data_floor_list['floor_status'];
                
                $floors["units"] = array();

                $unit_data=$d->select("unit_master","floor_id ='$floors_id'");

                while($data_units_list=mysqli_fetch_array($unit_data)) {

                    $units = array(); 

                    $units["unit_id"]=$data_units_list['unit_id'];
                    $units["society_id"]=$data_units_list['society_id'];
                    $units["floor_id"]=$data_units_list['floor_id'];
                    $units["unit_name"]=$data_units_list['unit_name'];
                    $units["unit_status"]=$data_units_list['unit_status'];
                    $units["unit_type"]=$data_units_list['unit_type'];

                    array_push($floors["units"], $units);


                }

                array_push($block["floors"], $floors);
                array_push($response["floors"], $floors);

            }

            $response["message"]=''.$xml->string->getBlocksucess;
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]=''.$xml->string->getBlockfail;
            $response["status"]="201";
            echo json_encode($response);
        
    }


    }else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }
}
else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
?>