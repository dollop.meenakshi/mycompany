<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {


    if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));

        if ($_POST['addNewVisitor'] == "addNewVisitor"  && $society_id!='') {

             $visitor_mobile= (int)$visitor_mobile;
            if (strlen($visitor_mobile)<8 ) {
                $response["message"] =''.$xml->string->invalidMobileno;
                $response["status"] = "201";
                echo json_encode($response);
                exit;
            }


            $qss=$d->select("society_master"," society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qss); 
            $employee_id_required = $societyData['employee_id_required'];    

            if ($employee_id_required==0 &&  $visitor_id_proof=='' ) {
                $response["message"] =''.$xml->string->idProofReqired;
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            
            }  

           

            $cm=$d->selectRow("visitor_mobile","daily_visitors_master","visitor_mobile='$visitor_mobile' AND society_id='$society_id' AND daily_active_status=0");
            if (mysqli_num_rows($cm)>0) {
                $response["message"] =''.$xml->string->mobileNoalradyRegister;
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $cm11=$d->selectRow("visitor_mobile","daily_visitors_master","visitor_mobile='$visitor_mobile' AND society_id='$society_id' AND daily_active_status=1");
            if (mysqli_num_rows($cm11)>0) {
                $response["message"] =''.$xml->string->mobileNoalradyRegisteradminAproval;
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            if ($visitor_profile_photo != "") {
                $ddd = date("ymdhi").$visitor_mobile;
                $profile_name = "Visitor_" . $ddd . '.png';
                define('UPLOAD_DIR', '../img/visitor/');
                $img = $visitor_profile_photo;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $profile_name;
                $success = file_put_contents($file, $data);
            } else {
                $profile_name = "visitor_default.png";
            }


            if ($visitor_id_proof != "") {
                $ddd = date("ymdhi").$visitor_mobile;
                $id_proof = "Id_Visitor_" . $ddd . '.png';
                define('UPLOAD_DIR', '../img/visitor/');
                $img = $visitor_id_proof;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $id_proof;
                $success = file_put_contents($file, $data);
            } else {
                $id_proof = "";
            }

             if ($visitor_id_proof1 != "") {
                $dddd = date("ymdhi").$visitor_mobile;
                $id_proof1 = "Id_Visitor_back" . $dddd . '.png';
                define('UPLOAD_DIR', '../img/visitor/');
                $img1 = $visitor_id_proof1;
                $img1 = str_replace('data: img/app/png;base64,', '', $img1);
                $img1 = str_replace(' ', '+', $img1);
                $data1 = base64_decode($img1);
                $file1 = UPLOAD_DIR . $id_proof1;
                $success1 = file_put_contents($file1, $data1);
            } else {
                $id_proof1 = "";
            }

         
            $in_time = date("H:i:s", strtotime($in_time));
            $out_time = date("H:i:s", strtotime($out_time));
             $valid_till = date("Y-m-d", strtotime($valid_till));

            $visitor_name = html_entity_decode($visitor_name);

            $m->set_data('society_id', $society_id);
            $m->set_data('visitor_type', 4); //for daily visitor
            $m->set_data('visit_from', $visit_from);
            $m->set_data('visitor_name', $visitor_name);
            $m->set_data('visitor_mobile', $visitor_mobile);
            $m->set_data('country_code', $country_code);
            $m->set_data('profile_name', $profile_name);
            $m->set_data('visitor_id_proof', $id_proof);
            $m->set_data('visitor_id_proof_back', $id_proof1);
            $m->set_data('valid_till', $valid_till);
            $m->set_data('in_time', $in_time);
            $m->set_data('out_time', $out_time);
            $m->set_data('week_days', $week_days);
            $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
            $m->set_data('vehicle_number', $vehicle_number);
            $m->set_data('gatekeeper_id', $gatekeeper_id);
            $m->set_data('number_of_visitor',1 );
            $m->set_data('visitor_status', '0');
            $m->set_data('active_status', '0');

            

            $a = array(
                'society_id' => $m->get_data('society_id'),
                'visitor_type' => $m->get_data('visitor_type'),
                'visit_from' => $m->get_data('visit_from'),
                'visitor_name' => $m->get_data('visitor_name'),
                'visitor_mobile' => $m->get_data('visitor_mobile'),
                'country_code' => $m->get_data('country_code'),
                'visitor_profile' => $m->get_data('profile_name'),
                'number_of_visitor' => $m->get_data('number_of_visitor'),
                'valid_till' => $m->get_data('valid_till'),
                'in_time' => $m->get_data('in_time'),
                'out_time' => $m->get_data('out_time'),
                'week_days' => $m->get_data('week_days'),
                'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                'visitor_id_proof' => $m->get_data('visitor_id_proof'),
                'visitor_id_proof_back' => $m->get_data('visitor_id_proof_back'),
                'vehicle_number' => $m->get_data('vehicle_number'),
                'visitor_status' => $m->get_data('visitor_status'),
                'daily_active_status' => $m->get_data('active_status'),
                'gatekeeper_id' => $m->get_data('gatekeeper_id'),
            );

            $quserDataUpdate = $d->insert("daily_visitors_master", $a);
            $daily_visitor_id = $con->insert_id;
             $m->set_data('daily_visitor_id', $daily_visitor_id);
            if ($quserDataUpdate == true) {
                $userIdAry=explode(",",$user_id);
                for ($i=0; $i <count($userIdAry) ; $i++) { 

                    $qqq=$d->selectRow("unit_id,user_token,device","users_master","user_id='$userIdAry[$i]'");
                    $userData=mysqli_fetch_array($qqq);
                    $unit_id = $userData['unit_id'];
                    $user_token = $userData['user_token'];
                    $device = $userData['device'];


                    $m->set_data('user_id', $userIdAry[$i]);
                    $m->set_data('unit_id', $unit_id);

                     $aUnitId = array(
                        'daily_visitor_id' => $m->get_data('daily_visitor_id'),
                        'society_id' => $m->get_data('society_id'),
                        'unit_id' => $m->get_data('unit_id'),
                        'user_id' => $m->get_data('user_id'),
                        'valid_till_user' => $m->get_data('valid_till'),
                        'in_time_user' => $m->get_data('in_time'),
                        'out_time_user' => $m->get_data('out_time'),   
                        'week_days_user' => $m->get_data('week_days')
                    );

                    $d->insert("daily_visitor_unit_master", $aUnitId);

                    $title ="Daily visitor $visitor_name added in your unit";
     
                      $notiAry = array(
                      'society_id'=>$society_id,
                      'user_id'=>$userIdAry[$i],
                      'notification_title'=>$title,
                      'notification_desc'=>"By Security Guard ".$gatekeeper_name,    
                      'notification_date'=>date('Y-m-d H:i'),
                      'notification_action'=>'visitor',
                      'notification_logo'=>'visitorsNew.png',
                      );
                      $d->insert("user_notification",$notiAry);
             
                    if ($device=='android') {
                        $nResident->noti("VisitorFragment","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'VisitorFragment');
                     } else {
                        $nResident->noti_ios("VisitorVC","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'VisitorVC');
                     }

                }

              

                $response["message"] =''.$xml->string->dailyVisitoraddSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->novisitoradd;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['deleteDailyVisitorMain']=="deleteDailyVisitorMain" && filter_var($visitor_id, FILTER_VALIDATE_INT) == true && filter_var($gatekeeper_id, FILTER_VALIDATE_INT) == true ){
                        
                        $qca=$d->select("daily_visitors_master","society_id='$society_id' AND visitor_id='$visitor_id' AND gatekeeper_id='$gatekeeper_id' AND gatekeeper_id!=0");
                        if (mysqli_num_rows($qca)<1) {
                            $response["message"]=''.$xml->string->acessDenied;
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                        } 
                           $qdelete= $d->delete("daily_visitors_master","society_id='$society_id' AND visitor_id='$visitor_id'");
                           $qdelete= $d->delete("daily_visitor_unit_master","society_id='$society_id' AND unit_id='$unit_id' AND daily_visitor_id='$visitor_id'");
                
                        if($qdelete==TRUE){

                            $response["message"]=''.$xml->string->dailyVisitordelete;
                            $response["status"]="200";

                            echo json_encode($response);
        
                        }else{
        
                            $response["message"]=''.$xml->string->somthingWrong;
                            $response["status"]="201";
                            echo json_encode($response);
        
                        }
        
            }else if ($_POST['editNewVisitor'] == "editNewVisitor"  && $society_id!='') {
           

            if ($visitor_id_proof_old && $employee_id_required==0 &&  $visitor_id_proof=='' ) {
                $response["message"] = ''.$xml->string->idProofReqired;
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            
            }  


            $cm=$d->selectRow("visitor_mobile","daily_visitors_master","visitor_mobile='$visitor_mobile' AND society_id='$society_id' AND daily_active_status=0 AND visitor_id!='$visitor_id'");
            if (mysqli_num_rows($cm)>0) {
                $response["message"] =''.$xml->string->mobileAlradyRegister;
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            if ($visitor_profile_photo != "") {
                $ddd = date("ymdhi");
                $profile_name = "Visitor_" . $ddd . '.png';
                define('UPLOAD_DIR', '../img/visitor/');
                $img = $visitor_profile_photo;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $profile_name;
                $success = file_put_contents($file, $data);
            } else {
                $profile_name =$visitor_profile_old;
            }

            if ($visitor_id_proof != "") {
                $ddd = date("ymdhi");
                $id_proof = "Id_Visitor_" . $ddd . '.png';
                define('UPLOAD_DIR', '../img/visitor/');
                $img = $visitor_profile_photo;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $id_proof;
                $success = file_put_contents($file, $data);
            } else {
                $id_proof = $visitor_id_proof_old;
            }

             if ($visitor_id_proof1 != "") {
                $dddd = date("ymdhi");
                $id_proof1 = "Id_Visitor_back" . $dddd . '.png';
                define('UPLOAD_DIR', '../img/visitor/');
                $img1 = $visitor_id_proof1;
                $img1 = str_replace('data: img/app/png;base64,', '', $img1);
                $img1 = str_replace(' ', '+', $img1);
                $data1 = base64_decode($img1);
                $file1 = UPLOAD_DIR . $id_proof1;
                $success1 = file_put_contents($file1, $data1);
            } else {
                $id_proof1 = $visitor_id_proof_old1;
            }
            
            $in_time = date("H:i:s", strtotime($in_time));
            $out_time = date("H:i:s", strtotime($out_time));
            $valid_till = date("Y-m-d", strtotime($valid_till));
            
            $visitor_name = html_entity_decode($visitor_name);

            $m->set_data('daily_visitor_id', $daily_visitor_id);
            $m->set_data('society_id', $society_id);
            $m->set_data('visitor_type', 4); //for daily visitor
            $m->set_data('visit_from', $visit_from);
            $m->set_data('visitor_name', $visitor_name);
            $m->set_data('visitor_mobile', $visitor_mobile);
            $m->set_data('visitor_profile', $visitor_profile);
            $m->set_data('valid_till', $valid_till);
            $m->set_data('in_time', $in_time);
            $m->set_data('out_time', $out_time);
            $m->set_data('week_days', $week_days);
            $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
            $m->set_data('visitor_id_proof', $id_proo);
            $m->set_data('visitor_id_proof_back', $id_proof1);
            $m->set_data('vehicle_number', $vehicle_number);
            $m->set_data('number_of_visitor',1 );
            $m->set_data('visitor_status', '0');

            

            $a = array(
                'society_id' => $m->get_data('society_id'),
                'visitor_type' => $m->get_data('visitor_type'),
                'visit_from' => $m->get_data('visit_from'),
                'visitor_name' => $m->get_data('visitor_name'),
                'visitor_mobile' => $m->get_data('visitor_mobile'),
                'visitor_profile' => $m->get_data('visitor_profile'),
                'number_of_visitor' => $m->get_data('number_of_visitor'),
                'valid_till' => $m->get_data('valid_till'),
                'in_time' => $m->get_data('in_time'),
                'out_time' => $m->get_data('out_time'),
                'week_days' => $m->get_data('week_days'),
                'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                'visitor_id_proof' => $m->get_data('visitor_id_proof'),
                'visitor_id_proof_back' => $m->get_data('visitor_id_proof_back'),
                'vehicle_number' => $m->get_data('vehicle_number'),
                'visitor_status' => $m->get_data('visitor_status'),
            );

            $quserDataUpdate = $d->update("daily_visitors_master", $a, "visitor_id='$visitor_id'");

            if ($quserDataUpdate == true) {
                $d->delete("daily_visitor_unit_master","daily_visitor_id='$visitor_id'");

                $userIdAry=explode(",",$user_id);
                for ($i=0; $i <count($userIdAry) ; $i++) { 

                    $qqq=$d->selectRow("unit_id","users_master","user_id='$userIdAry[$i]'");
                    $userData=mysqli_fetch_array($qqq);
                    $unit_id = $userData['unit_id'];

                    $m->set_data('user_id', $userIdAry[$i]);
                    $m->set_data('unit_id', $unit_id);
                    $m->set_data('visitor_id', $visitor_id);

                     $aUnitId = array(
                        'daily_visitor_id' => $m->get_data('visitor_id'),
                        'society_id' => $m->get_data('society_id'),
                        'unit_id' => $m->get_data('unit_id'),
                        'user_id' => $m->get_data('user_id'),
                    );

                    $d->insert("daily_visitor_unit_master", $aUnitId);

                }


                $response["message"] =''.$xml->string->dailyVisitordataUpdate;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noDataadd;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getDailyVisitorUnitList'] == "getDailyVisitorUnitList" && filter_var($daily_visitor_id, FILTER_VALIDATE_INT) == true ) {

           
            $uq=$d->select("unit_master,block_master,users_master,daily_visitor_unit_master","daily_visitor_unit_master.unit_id=unit_master.unit_id AND   
                       daily_visitor_unit_master.user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  daily_visitor_unit_master.daily_visitor_id ='$daily_visitor_id' ","GROUP BY daily_visitor_unit_master.unit_id ORDER BY users_master.user_id DESC");

            $response["units"] = array();

            if (mysqli_num_rows($uq) > 0) {

                while ($data = mysqli_fetch_array($uq)) {

                    $units = array();

                    $qqname= $d->selectRow("user_full_name","users_master,daily_visitor_unit_master","daily_visitor_unit_master.user_id=users_master.user_id AND daily_visitor_unit_master.unit_id='$data[unit_id]' AND users_master.member_status=0","ORDER BY users_master.user_id DESC");
                    $userData=mysqli_fetch_array($qqname);
                    $primaryName= $userData['user_full_name'];
                    if ($primaryName!="") {
                        $user_name_view= $primaryName;
                      } else {
                        $user_name_view= $data['user_full_name']; 
                      } 

                    $units["visitor_id"] = $data['daily_visitor_id'];
                    $units["society_id"] = $data['society_id'];
                    $units["unit_name"] = $data['block_name'].'-'.$data['unit_name'];
                    $units["user_name"] = $user_name_view;
                    $units["unit_id"] = $data['unit_id'];
                    $units["user_id"] = $data['user_id'];
                    $units["valid_till_user"] =  date("d-m-Y", strtotime($data['valid_till_user']));
                    $units["in_time_user"] = date("h:i A", strtotime($data['in_time_user']));
                    $units["out_time_user"] =date("h:i A", strtotime($data['out_time_user']));
                    $units["week_days_user"] ='('. $data['week_days_user'].')';

                     $uqcoiunt=$d->select("unit_master,block_master,users_master,daily_visitor_unit_master","daily_visitor_unit_master.unit_id=unit_master.unit_id AND   
                    daily_visitor_unit_master.user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  daily_visitor_unit_master.daily_visitor_id ='$data[visitor_id]' ","GROUP BY daily_visitor_unit_master.unit_id ORDER BY users_master.user_id DESC");
                     $working_units= mysqli_num_rows($uqcoiunt);

                    $units["working_units"] ='Working Units : '. $working_units;

                    
                    array_push($response["units"], $units);
                }

                $response["message"] =''.$xml->string->getUnit;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noUnitfound;
                $response["status"] = "200";
                echo json_encode($response);
            }


        }else if ($_POST['getDailyVisitorListNew'] == "getDailyVisitorListNew") {

            if (isset($society_id)) {
                $qss=$d->selectRow("visitor_mobile_number_show_gatekeeper","society_master"," society_id ='$society_id'");
                $societyData = mysqli_fetch_array($qss); 
                $visitorMobilePrivacy = $societyData['visitor_mobile_number_show_gatekeeper'];        
            }

            $qnotification = $d->select("daily_visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=daily_visitors_master.visitor_sub_type_id", "daily_visitors_master.visitor_type  = '4' AND daily_visitors_master.society_id='$society_id' AND daily_visitors_master.daily_active_status =0 AND daily_visitors_master.visitor_status =0", "ORDER BY daily_visitors_master.visitor_name ASC");

            $response["visitor"] = array();

            if (mysqli_num_rows($qnotification) > 0) {

                while ($data_notification = mysqli_fetch_array($qnotification)) {

                    $visitor = array();
                    
                    if ($data_notification['visitor_sub_image']!='') {
                        $visit_logo = $base_url.'img/visitor_company/'.$data_notification['visitor_sub_image'].'';
                        $visit_company = $data_notification['visitor_sub_type_name'].'';
                    } else {
                        $visit_logo="";
                    }



                    $visitor["visitor_id"] = $data_notification['visitor_id'];
                    $visitor["society_id"] = $data_notification['society_id'];
                    $visitor["visitor_name"] = $data_notification['visitor_name'];
                    $visitor["visit_logo"] = $visit_logo;
                    $visitor["visit_from"] = $data_notification['visit_from'];
                    $visitor["visitor_type"] = $data_notification['visitor_type'];
                    if ($visitorMobilePrivacy==0) {
                        $visitor["visitor_mobile_view"] = "".substr($data_notification['visitor_mobile'], 0, 3) . '****' . substr($data_notification['visitor_mobile'],  -3);
                    } else {
                        $visitor["visitor_mobile_view"] = $data_notification['visitor_mobile'].'';
                    }
                    $visitor["visitor_mobile"] = $data_notification['visitor_mobile'].'';
                    $visitor["country_code"] = $data_notification['country_code'];
                    $visitor["visitor_profile"] = $base_url."img/visitor/".$data_notification['visitor_profile'];
                    $visitor["visitor_profile_old"] = $data_notification['visitor_profile'];
                    $visitor["visitor_id_proof_old"] = $data_notification['visitor_id_proof'];
                    $visitor["visitor_id_proof_old1"] = $data_notification['visitor_id_proof_back'];
                    $visitor["visitor_status"] = $data_notification['visitor_status'];
                    $visitor["vehicle_number"] = $data_notification['vehicle_number'];
                    $visitor["visitor_sub_type_id"] = $data_notification['visitor_sub_type_id'];
                    $visitor["gatekeeper_id"] = $data_notification['gatekeeper_id'];
                    

                   
                    $visitor["visit_time"] = "No Time Available";
                    $visitor["duration"] = "";
                    

                    //  $uq=$d->selectRow("daily_visitor_unit_master.unit_id","unit_master,block_master,users_master,daily_visitor_unit_master","daily_visitor_unit_master.unit_id=unit_master.unit_id AND   
                    // daily_visitor_unit_master.user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  daily_visitor_unit_master.daily_visitor_id ='$data_notification[visitor_id]' ","GROUP BY daily_visitor_unit_master.unit_id ORDER BY users_master.user_id DESC");

                    $visitor["user_id"] = "";
                    $visitor["unit_name"] = "";
                    $visitor["working_units"] ='View Working Units';


                    array_push($response["visitor"], $visitor);
                }

                $response["message"] =''.$xml->string->getDailyvisitorSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noDailyvisitorFound;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['addUnitinDailyVisitor'] == "addUnitinDailyVisitor" && filter_var($daily_visitor_id, FILTER_VALIDATE_INT) == true ) {

                $valid_till = date("Y-m-d", strtotime($valid_till));
                $in_time = date("H:i:s", strtotime($in_time));
                $out_time = date("H:i:s", strtotime($out_time));
                $userIdAry=explode(",",$user_id);
                // for ($i=0; $i <count($userIdAry) ; $i++) { 

                //     $qqq=$d->selectRow("unit_id","users_master","user_id='$userIdAry[$i]'");
                //     $userData=mysqli_fetch_array($qqq);
                //     $unit_id = $userData['unit_id'];

                $m->set_data('daily_visitor_id', $daily_visitor_id);
                $m->set_data('society_id', $society_id);
                $m->set_data('user_id', $user_id);
                $m->set_data('unit_id', $unit_id);
                $m->set_data('valid_till', $valid_till);
                $m->set_data('in_time', $in_time);
                $m->set_data('out_time', $out_time);
                $m->set_data('week_days', $week_days);

                 $aUnitId = array(
                    'daily_visitor_id' => $m->get_data('daily_visitor_id'),
                    'society_id' => $m->get_data('society_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'user_id' => $m->get_data('user_id'),
                    'valid_till_user' => $m->get_data('valid_till'),
                    'in_time_user' => $m->get_data('in_time'),
                    'out_time_user' => $m->get_data('out_time'),   
                    'week_days_user' => $m->get_data('week_days'),   
                );

                $qca=$d->select("daily_visitor_unit_master","society_id='$society_id' AND unit_id='$unit_id' AND daily_visitor_id='$daily_visitor_id'");
                if (mysqli_num_rows($qca)>0) {
                    $response["message"] =''.$xml->string->visitorAlreadyadded;
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                 }  else {
                    $d->insert("daily_visitor_unit_master", $aUnitId);
                    $response["message"] =''.$xml->string->unitAddSucess;
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();
                 }

                $response["message"] = ''.$xml->string->unitAddSucess;
                $response["status"] = "201";
                echo json_encode($response);
                exit();


        } else if ($_POST['addUnitinDailyVisitorMulti'] == "addUnitinDailyVisitorMulti" && filter_var($daily_visitor_id, FILTER_VALIDATE_INT) == true ) {

                $valid_till = date("Y-m-d", strtotime($valid_till));
                $in_time = date("H:i:s", strtotime($in_time));
                $out_time = date("H:i:s", strtotime($out_time));
                $userIdAry=explode(",",$user_id);
                for ($i=0; $i <count($userIdAry) ; $i++) { 

                    $qqq=$d->selectRow("unit_id,user_token,device","users_master","user_id='$userIdAry[$i]'");
                    $userData=mysqli_fetch_array($qqq);
                    $unit_id = $userData['unit_id'];
                    $user_token = $userData['user_token'];
                    $device = $userData['device'];
                
                    $m->set_data('daily_visitor_id', $daily_visitor_id);
                    $m->set_data('society_id', $society_id);
                    $m->set_data('user_id', $userIdAry[$i]);
                    $m->set_data('unit_id', $unit_id);
                    $m->set_data('valid_till', $valid_till);
                    $m->set_data('in_time', $in_time);
                    $m->set_data('out_time', $out_time);
                    $m->set_data('week_days', $week_days);

                     $aUnitId = array(
                        'daily_visitor_id' => $m->get_data('daily_visitor_id'),
                        'society_id' => $m->get_data('society_id'),
                        'unit_id' => $m->get_data('unit_id'),
                        'user_id' => $m->get_data('user_id'),
                        'valid_till_user' => $m->get_data('valid_till'),
                        'in_time_user' => $m->get_data('in_time'),
                        'out_time_user' => $m->get_data('out_time'),   
                        'week_days_user' => $m->get_data('week_days'),   
                    );

                    $qca=$d->select("daily_visitor_unit_master","society_id='$society_id' AND unit_id='$unit_id' AND daily_visitor_id='$daily_visitor_id' AND user_id=$userIdAry[$i]");
                    if (mysqli_num_rows($qca)>0) {
                     
                     }  else {
                        $d->insert("daily_visitor_unit_master", $aUnitId);

                        $title ="Daily visitor $visitor_name added in your unit";
     
                          $notiAry = array(
                          'society_id'=>$society_id,
                          'user_id'=>$userIdAry[$i],
                          'notification_title'=>$title,
                          'notification_desc'=>"By Security Guard ".$gatekeeper_name,    
                          'notification_date'=>date('Y-m-d H:i'),
                          'notification_action'=>'visitor',
                          'notification_logo'=>'visitorsNew.png',
                          );
                          $d->insert("user_notification",$notiAry);
                 
                         if ($device=='android') {
                            $nResident->noti("VisitorFragment","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'VisitorFragment');
                         } else {
                            $nResident->noti_ios("VisitorVC","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'VisitorVC');
                         }

                        
                     }
                }


                $response["message"] = ''.$xml->string->unitAddSucess;
                $response["status"] = "200";
                echo json_encode($response);
                exit();


        } else if ($_POST['getDailyVisitorList'] == "getDailyVisitorList") {

            $qnotification = $d->select("daily_visitors_master", "visitor_type  = '4' AND society_id='$society_id' AND daily_active_status =0", "ORDER BY visitor_name ASC");

            $response["visitor"] = array();

            if (mysqli_num_rows($qnotification) > 0) {

                while ($data_notification = mysqli_fetch_array($qnotification)) {

                    $visitor = array();
                     $fd=$d->selectRow("visitor_sub_image,visitor_sub_type_name","visitorSubType","visitor_sub_type_id='$data_notification[visitor_sub_type_id]'");
                        $vistLogo=mysqli_fetch_array($fd);
                        if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                            $visit_company = $vistLogo['visitor_sub_type_name'].'';
                        } else {
                            $visit_logo="";
                        }



                    $visitor["visitor_id"] = $data_notification['visitor_id'];
                    $visitor["society_id"] = $data_notification['society_id'];
                    $visitor["visitor_name"] = $data_notification['visitor_name'];
                    $visitor["visit_logo"] = $visit_logo;
                    $visitor["visit_from"] = $visit_company;
                    $visitor["visitor_type"] = $data_notification['visitor_type'];
                    $visitor["visitor_mobile"] = $data_notification['visitor_mobile'].'';
                    $visitor["country_code"] = $data_notification['country_code'];
                    $visitor["visitor_profile"] = $base_url."img/visitor/".$data_notification['visitor_profile'];
                    $visitor["visitor_profile_old"] = $data_notification['visitor_profile'];
                    $visitor["visitor_id_proof_old"] = $data_notification['visitor_id_proof'];
                    $visitor["visitor_id_proof_old1"] = $data_notification['visitor_id_proof_back'];
                    $visitor["visitor_status"] = $data_notification['visitor_status'];
                    $visitor["vehicle_number"] = $data_notification['vehicle_number'];
                    $visitor["visitor_sub_type_id"] = $data_notification['visitor_sub_type_id'];
                    $visitor["gatekeeper_id"] = $data_notification['gatekeeper_id'];
                    

                    $qv=$d->select("visitors_master","daily_visitor_id='$data_notification[visitor_id]' AND visitor_type=4","ORDER BY visitor_id DESC");
                    $visitData=mysqli_fetch_array($qv);
                    if ($data_notification['visitor_status']==0 && $visitData['exit_date']!='') {
                        $visitor["visit_time"] =  date("d M Y h:i A", strtotime($visitData['exit_date'].' '.$visitData['exit_time']));
                       $visitor["duration"] = "";
                    } else if($data_notification['visitor_status']==1 && $visitData['visit_date']!='') {
                        $intTime=$visitData['visit_date']." ".$visitData['visit_time'];
                        $outTime=$temDate;
                        $date_a = new DateTime($intTime);
                        $date_b = new DateTime($outTime);
                        $interval = date_diff($date_a,$date_b);
                        // $list["duration"] = $interval->format('%d %h:%i:%s');
                        $totalDays= $interval->d;
                        $totalMinutes= $interval->i;
                        $totalHours= $interval->h;
                        if ($totalDays>0) {
                        $visitor["duration"] = $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                        } else {
                            $visitor["duration"] = $interval->h.' hours '. $interval->i.' min';
                        }

                        $visitor["visit_time"] =  date("d M Y h:i A", strtotime($visitData['visit_date'].' '.$visitData['visit_time']));
                    } else {
                        $visitor["visit_time"] = "No Time Available";
                        $visitor["duration"] = "";
                    }
                    $qqq=$d->select("daily_visitor_unit_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=daily_visitor_unit_master.unit_id AND daily_visitor_unit_master.daily_visitor_id='$data_notification[visitor_id]'");
                    $userIdAry= array();
                    $blockNameAry= array();
                    while ($unitData=mysqli_fetch_array($qqq)) {
                        $unit_name= $unitData['block_name'].'-'.$unitData['unit_name'];
                        $user_id= $unitData['user_id'];
                        array_push($userIdAry, $user_id);
                        array_push($blockNameAry, $unit_name);
                    }

                    $blockNameAry = array_unique($blockNameAry);

                     $uq=$d->select("unit_master,block_master,users_master,daily_visitor_unit_master","daily_visitor_unit_master.unit_id=unit_master.unit_id AND   
           daily_visitor_unit_master.user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  daily_visitor_unit_master.daily_visitor_id ='$data_notification[visitor_id]' ","GROUP BY daily_visitor_unit_master.unit_id ORDER BY users_master.user_id DESC");

                    $visitor["user_id"] = implode(",", $userIdAry);
                    $visitor["unit_name"] = implode(",", $blockNameAry);
                    $visitor["working_units"] ='Working Units : '.mysqli_num_rows($uq);


                    array_push($response["visitor"], $visitor);
                }

                $response["message"] =''.$xml->string->getDailyvisitorSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noDailyvisitorFound;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['allowInOut'] == "allowInOut" && $visitor_mobile !='') {
            $visit_date = date("Y-m-d");
            $visit_time = date("H:i:s");

            $gd=$d->selectRow("valid_till,country_code,visitor_profile","daily_visitors_master","visitor_mobile='$visitor_mobile'");
            $visitorData= mysqli_fetch_array($gd);
            $valid_till = $visitorData['valid_till'];
             $country_code = $visitorData['country_code'];
             $visitor_profile = $visitorData['visitor_profile'];
            $expire = strtotime($valid_till);
            $today = strtotime("today midnight");
            if($today > $expire){
               $response["message"] =''.$xml->string->validDateExprie;
               $response["status"] = "201";
               echo json_encode($response);
               exit();
            }
            
            $visitor_name = html_entity_decode($visitor_name);
            $visit_from = html_entity_decode($visit_from);

            $os= $d->selectRow("overstay_alert_minutes","visitorMainType","visitor_main_type_id='4' AND overstay_alert_minutes>0");
            $overStayData = mysqli_fetch_array($os);
            $overstay_alert_minutes = $overStayData['overstay_alert_minutes'];
            if ($overstay_alert_minutes>0) {
                // code...
                $overstay_time_limit = date("Y-m-d H:i:s",strtotime($temDate." +$overstay_alert_minutes minutes"));
            } 

            $m->set_data('society_id', $society_id);
            $m->set_data('visitor_type', 4);
            $m->set_data('visit_from', $visit_from);
            $m->set_data('visitor_name', $visitor_name);
            $m->set_data('visitor_mobile', $visitor_mobile);
            $m->set_data('country_code', $country_code);
            $m->set_data('visitor_profile', $visitor_profile);
            $m->set_data('number_of_visitor', 1);
            $m->set_data('visit_date', $visit_date);
            $m->set_data('visit_time', $visit_time);
            $m->set_data('daily_visitor_id', $daily_visitor_id);
            $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);

            $m->set_data('in_temperature', $in_temperature);
            $m->set_data('in_with_mask', $in_with_mask);

            $m->set_data('out_temperature', $out_temperature);
            $m->set_data('out_with_mask', $out_with_mask);
            $m->set_data('overstay_time_limit', $overstay_time_limit);



            $a1 = array(
                'visitor_status'=>$visitor_status,
            );

            $userIdAry= array();
            $qc = $d->select("daily_visitor_unit_master", "active_status=0 AND daily_visitor_id  = '$daily_visitor_id' AND society_id='$society_id' AND active_status=0", "");
            while($data=mysqli_fetch_array($qc)){
                array_push($userIdAry, $data['user_id']);
            }

            $ids = join("','",$userIdAry);


              $blockAry = array();
              $duCheck=$d->select("employee_block_master","emp_id='$gatekeeper_id' AND emp_id!=0");
              while ($oldBlock=mysqli_fetch_array($duCheck)) {
                  array_push($blockAry , $oldBlock['block_id']);
              }
              $ids11 = join("','",$blockAry);   
              if ($ids11!='') {
                  $appendQuery = " AND block_id IN ('$ids11')";
                  $appendQuer11 = " AND users_master.block_id IN ('$ids11')";
              }
                 


            $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$ids') $appendQuery");
            $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$ids') $appendQuery");
            
            $append_query = "users_master.user_id IN ('$ids') $appendQuer11";

                if ($visitor_status==1) {

                    $title= "$visitor_name ($visit_from) has Arrived at Gate";
                    $desc= "Entry allowed by Security Guard-$gatekeeper_name";
                    // for out
                     $a = array(
                        'society_id'=>$m->get_data('society_id'),
                        'visitor_type'=>$m->get_data('visitor_type'),
                        'visit_from'=>$m->get_data('visit_from'),
                        'visitor_name'=>$m->get_data('visitor_name'),
                        'country_code'=>$m->get_data('country_code'),
                        'visitor_mobile'=>$m->get_data('visitor_mobile'),
                        'visitor_profile'=>$m->get_data('visitor_profile'),
                        'number_of_visitor'=>$m->get_data('number_of_visitor'),
                        'visit_date' => $m->get_data('visit_date'),
                        'visit_time' => $m->get_data('visit_time'),
                        'visitor_status' => 2,
                        'daily_visitor_id' => $m->get_data('daily_visitor_id'),
                        'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                        'in_temperature' => $m->get_data('in_temperature'),
                        'in_with_mask' => $m->get_data('in_with_mask'),
                        'overstay_time_limit' => $m->get_data('overstay_time_limit'),
                    );

                    $q = $d->insert("visitors_master", $a);
                    $response["message"] =''.$xml->string->dailyVisitorIn;

                    $adminFcmArray= $d->selectAdminInOut($daily_visitor_id,"android",'1');
                    $adminFcmArrayIos= $d->selectAdminInOut($daily_visitor_id,"ios",'1');

                    $nAdmin->noti_new($society_id,"", $adminFcmArray, $title, $desc, "dailyVisitors?manageVisitor=yes&visitor_id=$daily_visitor_id");
                    $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $title, $desc, "dailyVisitors?manageVisitor=yes&visitor_id=$daily_visitor_id");

                } else {
                    $fq=$d->selectRow("visitor_id","visitors_master","daily_visitor_id='$daily_visitor_id'","ORDER BY visitor_id DESC");
                    $visitData=mysqli_fetch_array($fq);
                    $visitor_id = $visitData['visitor_id'];

                    $title= "$visitor_name ($visit_from) Exit Allowed";
                    $desc= "Exit allowed by Security Guard-$gatekeeper_name";
                    // for in  0
                    $a = array(
                        'exit_date' => $m->get_data('visit_date'),
                        'exit_time' => $m->get_data('visit_time'),
                        'visitor_status' => "3",
                        'out_temperature' => $m->get_data('out_temperature'),
                        'out_with_mask' => $m->get_data('out_with_mask'),
                    );
                    $q = $d->update("visitors_master", $a,"visitor_id='$visitor_id'  ORDER BY visitor_id DESC ");
                    $response["message"] =''.$xml->string->dailyVisitorout;
                    
                    $adminFcmArray= $d->selectAdminInOut($daily_visitor_id,"android",'1');
                    $adminFcmArrayIos= $d->selectAdminInOut($daily_visitor_id,"ios",'1');

                    $nAdmin->noti_new($society_id,"", $adminFcmArray, $title, $desc, "dailyVisitors?manageVisitor=yes&visitor_id=$daily_visitor_id");
                    $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $title, $desc, "dailyVisitors?manageVisitor=yes&visitor_id=$daily_visitor_id");
                    
                }

            if ($q == true) {

                $d->insertUserNotification($society_id,$title,$desc,"visitor","serviceProviderNew.png",$append_query);

                $nResident->noti("VisitorFragment","",$society_id,$fcmArray,$title,$desc,'visitor');
                $nResident->noti_ios("VisitorVC","",$society_id,$fcmArrayIos,$title,$desc,'visitor');

                $d->update("daily_visitors_master",$a1,"visitor_mobile='$visitor_mobile'");
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->failed;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }  else if ($_POST['deleteDailyVisitor'] == "deleteDailyVisitor" && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true) {
          
           
            $qdelete=$d->delete("daily_visitor_unit_master","society_id='$society_id' AND unit_id='$unit_id' AND daily_visitor_id='$daily_visitor_id'");
    
            if($qdelete==TRUE){

                $response["message"]=''.$xml->string->dailyVisitordeletesucess;
                $response["status"]="200";

                echo json_encode($response);

            }else{

                $response["message"]=''.$xml->string->somthingWrong;
                $response["status"]="201";
                echo json_encode($response);

            }
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 