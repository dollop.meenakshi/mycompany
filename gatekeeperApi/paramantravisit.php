<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    $CURRENT_TIMESTAMP=date('Y-m-d H:i:s');
    $today = date('Y-m-d');

    $response = array();
    extract(array_map("test_input" , $_POST));
    if (isset($society_id)) {
        $qss=$d->selectRow("visitor_mobile_number_show_gatekeeper,auto_reject_vistor_minutes,visitor_otp_verify","society_master"," society_id ='$society_id'");
        $societyData = mysqli_fetch_array($qss); 
        $visitorMobilePrivacy = $societyData['visitor_mobile_number_show_gatekeeper'];  
        $auto_reject_vistor_minutes= $societyData['auto_reject_vistor_minutes'];      
        $visitor_otp_verify= $societyData['visitor_otp_verify'];      
    }

        if ($society_id!='' && $addCommonEntry=='addCommonEntry') {


            $appendQuery = " AND country_code='$country_code'";
            
            $qq=$d->select("visitors_master","visitor_mobile='$visitor_mobile' AND visitor_status=0 AND society_id='$society_id' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=1 AND society_id='$society_id' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=2 AND society_id='$society_id'  $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=6 AND society_id='$society_id'  ");
            if (mysqli_num_rows($qq)>0) {
                $oldData=mysqli_fetch_array($qq);
                if ($oldData['visitor_type']==1 && $oldData['visitor_status']==0) {
                    $response["message"] = 'Mobile number alreay added in expected visitor';
                } else if ($oldData['visitor_type']==4) {
                    $response["message"] ='Visitor Already Added in Daily Visitor List';
                } else if ($oldData['visitor_status']==2) {
                    $response["message"] ='Visitor Aleready Entered in Society';
                } else {
                    $response["message"] = 'Mobile Number Already Register';
                }
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            }

           
            $profile_name = "visitor_default.png";
            $notyUrl='';
        
           
            $digits = 4;
            $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
            $m->set_data('otp', $otp);

            $visitor_name = html_entity_decode($visitor_name);
            if ($number_of_visitor=="") {
                $number_of_visitor =1;
            }

                $m->set_data('parent_visitor_id', $visitor_id);
                $m->set_data('society_id', $society_id);
                $m->set_data('floor_id', 0);
                $m->set_data('block_id',0);
                $m->set_data('unit_id', 0);
                $m->set_data('user_id', 0);
                $m->set_data('emp_id', 0);
                $m->set_data('visitor_type', 5);
                $m->set_data('visit_from', "Paramantra Site Visit");
                $m->set_data('visitor_name', $visitor_name);
                $m->set_data('visitor_mobile', $visitor_mobile);
                $m->set_data('country_code', $country_code);
                $m->set_data('number_of_visitor',$number_of_visitor);
                $m->set_data('visit_date', $visit_date);
                $m->set_data('visit_time', $visit_time);
                $m->set_data('visitor_status', '1');
                $m->set_data('visiting_reason', $visiting_reason);
                $m->set_data('vehicle_no', $vehicle_no);
                $m->set_data('visitor_vehicle_type', $vehicle_type);
                $m->set_data('parking_id', $parking_id);
                $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
                $m->set_data('otp', $otp);
                $m->set_data('approve_button_status', 1);

                

                $a = array(
                    'society_id' => $m->get_data('society_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'block_id' => $m->get_data('block_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'user_id' => $m->get_data('user_id'),
                    'emp_id' => $m->get_data('emp_id'),
                    'visitor_type' => $m->get_data('visitor_type'),
                    'visit_from' => $m->get_data('visit_from'),
                    'visitor_name' => $m->get_data('visitor_name'),
                    'visitor_mobile' => $m->get_data('visitor_mobile'),
                    'country_code' => $m->get_data('country_code'),
                    'visitor_profile' => $profile_name,
                    'number_of_visitor' => $m->get_data('number_of_visitor'),
                    'visit_date' => $m->get_data('visit_date'),
                    'visit_time' => $m->get_data('visit_time'),
                    'visitor_status' => $m->get_data('visitor_status'),
                    'visiting_reason' => $m->get_data('visiting_reason'),
                    'vehicle_no'=>$m->get_data('vehicle_no'),
                    'visitor_vehicle_type'=>$m->get_data('visitor_vehicle_type'),
                    'parking_id'=>$m->get_data('parking_id'),
                    'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                    'otp'=>$m->get_data('otp'),
                    'approve_button_status'=>$m->get_data('approve_button_status'),
                    'entry_time'=>date("Y-m-d H:i:s"),
                );


                $q = $d->insert("visitors_master", $a);
                $parent_visitor_id = $con->insert_id;

                if ($q>0) {
                     $aPrnt = array(
                    'parent_visitor_id' => $parent_visitor_id,
                    );
                    
                    $d->update("visitors_master", $aPrnt,"visitor_id='$parent_visitor_id'");
                    $visitDate = date("d-m-Y", strtotime($visit_date));
                    $visitTime = date("h:i A", strtotime($visit_time));

                    
                    $title= "Expected visitor added by Paramantra";
                    $description= "$visitor_name will be arriving on $visitDate $visitTime";

                    
                    $fcmToken=$d->get_emp_fcm("employee_master,employee_block_master","employee_master.entry_status=1 AND employee_master.emp_id=employee_block_master.emp_id AND employee_master.society_id='$society_id'  AND employee_master.emp_token!=''");

                    $nGaurd->noti_new("visitor",$fcmToken,$title,$description,"in~1");


                    $response["message"] ='Visit Added Successfully';
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();

                } else {
                    $response["message"] ='Something went wrong on society server';
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();
                }


        } else {
            $response["message"] = "Soemthing went wrong";
            $response["status"] = "201";
            echo json_encode($response);
        }
    
} 
