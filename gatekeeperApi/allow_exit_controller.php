<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb) {

    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d H:i:s");
    $temFilter = date("Y-m-d");
    $temFiltertime = date("H:i:s");

        if ($_POST['allowExitExVisitor'] == "allowExitExVisitor" && $visitor_id!='') {

            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('visitor_status', '3');
            $m->set_data('visit_date', $temFilter);
            $m->set_data('visit_time', $temFiltertime);
            $m->set_data('exit_by', $gatekeeper_id);
            $m->set_data('out_temperature', $out_temperature);
            $m->set_data('out_with_mask', $out_with_mask);


            $a = array(
                'exit_by' => $m->get_data('exit_by'),
                'exit_date' => $m->get_data('visit_date'),
                'exit_time' => $m->get_data('visit_time'),
                'visitor_status' => $m->get_data('visitor_status'),
                'out_temperature' => $m->get_data('out_temperature'),
                'out_with_mask' => $m->get_data('out_with_mask'),
            );

            $qdelete = $d->update("visitors_master", $a, "visitor_id='$visitor_id'");

            if ($qdelete == true) {

                $response["message"] = ''.$xml->string->exitAllow;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->failed;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['allowExitNewVisitor'] == "allowExitNewVisitor" && $visitor_id!='') {

            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('visitor_status', '3');
            $m->set_data('visit_date', $temFilter);
            $m->set_data('visit_time', $temFiltertime);
            $m->set_data('out_temperature', $out_temperature);
            $m->set_data('out_with_mask', $out_with_mask);

            $a = array(
                'exit_date' => $m->get_data('visit_date'),
                'exit_time' => $m->get_data('visit_time'),
                'visitor_status' => $m->get_data('visitor_status'),
                'out_temperature' => $m->get_data('out_temperature'),
                'out_with_mask' => $m->get_data('out_with_mask'),
            );

            $qdelete = $d->update("visitors_master", $a, "visitor_mobile='$visitor_mobile'");

            if ($qdelete == true) {


               $qUserToken=$d->select("users_master,visitors_master","users_master.society_id='$society_id' AND visitors_master.visitor_id='$visitor_id' AND users_master.user_id=visitors_master.user_id");
	
                while($data_notification=mysqli_fetch_array($qUserToken)){

                  $sos_user_token=$data_notification['user_token'];
                  $device=$data_notification['device'];


                    if ($device=='android') {
                       $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,"$visitor_name Exit allowed","by Security Guard-$gatekeeper_name",'newVisitor');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token,"$visitor_name Exit allowed","by Security Guard-$gatekeeper_name",'newVisitor');
                    }

                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$data_notification['user_id'],
                        'notification_title'=>"$visitor_name Exit allowed",
                        'notification_desc'=>"by Security Guard-$gatekeeper_name",
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'Visitor',
                        'notification_logo'=>'visitorsNew.png',
                      );
                  
                    $d->insert("user_notification",$notiAry);
                }

                $m->set_data('vehicle_no', "");
                $m->set_data('parking_status', '0');
                $m->set_data('visitor', "0");
                $m->set_data('vehicle_type', "0");

                $a2 = array(
                // 'parking_status' => $m->get_data('parking_status'),
                'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                'visitor' => $m->get_data('visitor'));
                
                $d->update("parking_master",$a2,"visitor='$visitor_id'");

                $response["message"] =''.$xml->string->exitAllowsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->exitFail;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['allowExitNewVisitorMulti'] == "allowExitNewVisitorMulti" && $visitor_id!='') {

            $m->set_data('exit_by', $gatekeeper_id);
            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('visitor_status', '3');
            $m->set_data('visit_date', $temFilter);
            $m->set_data('visit_time', $temFiltertime);
            $m->set_data('out_temperature', $out_temperature);
            $m->set_data('out_with_mask', $out_with_mask);

            $a = array(
                'exit_by' => $m->get_data('exit_by'),
                'exit_date' => $m->get_data('visit_date'),
                'exit_time' => $m->get_data('visit_time'),
                'visitor_status' => $m->get_data('visitor_status'),
                'out_temperature' => $m->get_data('out_temperature'),
                'out_with_mask' => $m->get_data('out_with_mask'),
            );

            $qdelete = $d->update("visitors_master", $a, "parent_visitor_id='$parent_visitor_id' AND parent_visitor_id!=0");

            if ($qdelete == true) {


               $qUserToken=$d->select("users_master,visitors_master","users_master.society_id='$society_id' AND visitors_master.parent_visitor_id='$visitor_id' AND users_master.user_id=visitors_master.user_id");
    
                while($data_notification=mysqli_fetch_array($qUserToken)){

                  $sos_user_token=$data_notification['user_token'];
                  $device=$data_notification['device'];
                   $visitor_type=$data_notification['visitor_type'];
                    $visitor_name=$data_notification['visitor_name'];
                    $vehicle_no=$data_notification['vehicle_no'];
                    $visit_from=$data_notification['visit_from'];
                    $expected_type=$data_notification['expected_type'];
                    if ($visitor_type==2) {
                        $title= "Delivery boy from $visit_from left the gate";
                        $description = "Exit allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "delivery_boy.png";
                    } else  if ($visitor_type==3) {
                        $title= "Cab $vehicle_no from $visit_from left the gate";
                        $description = "Exit allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "taxi.png";
                    }else if ($visitor_type==1 && $expected_type==3) {
                         $title= "Cab $vehicle_no from $visit_from left the gate";
                        $description = "Exit allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "taxi.png";
                    }else if ($visitor_type==1 && $expected_type==2) {
                         $title= "Delivery boy from $visit_from left the gate";
                        $description = "Exit allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "delivery_boy.png";
                    }else if ($visitor_type==1 && $expected_type==6) {
                         $title= "Vendor from $visit_from left the gate";
                        $description = "Exit allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "delivery_boy.png";
                    } else {
                        $title= "$visitor_name Visitor left the gate";
                        $description = "Exit allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "visitorsNew.png";
                    }

                    if ($device=='android') {
                       $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,$title,$description,'newVisitor');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token, $title,$description,'newVisitor');
                    }

                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$data_notification['user_id'],
                        'notification_title'=> $title,
                        'notification_desc'=>$description,
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'Visitor',
                        'notification_logo'=>$notification_logo,
                      );
                  
                    $d->insert("user_notification",$notiAry);
                }

                if ($parking_id>0) {
                
                    $m->set_data('visitor_vehicle_no', "");
                    $m->set_data('visitor', "0");

                    $aParking = array(
                    'visitor_vehicle_status' => 0,
                    'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                    'visitor' => $m->get_data('visitor'),
                    );
                    
                    $d->update("parking_master",$aParking,"parking_id='$parking_id'");

                    $ParkingLog= array (
                       'exit_by'=>$gatekeeper_id,
                      'out_time'=> $temDate);

                    $q1=$d->update("parking_logs_master",$ParkingLog,"parking_visitor_id='$visitor_id'");

                }

                $response["message"] = ''.$xml->string->exitAllowsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->exitFail;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['allowExitStaff'] == "allowExitStaff" && $emp_id!='') {

            $m->set_data('emp_id', $emp_id);
            $m->set_data('society_id', $society_id);
             $temDate = date("Y-m-d H:i:s");
            $m->set_data('visit_exit_date_time', $temDate);
            $m->set_data('visit_status', '1');
             $m->set_data('out_temperature', $out_temperature);
            $m->set_data('out_with_mask', $out_with_mask);

            $a = array(
                'visit_exit_date_time' => $m->get_data('visit_exit_date_time'),
                'visit_status' => $m->get_data('visit_status'),
                'out_temperature' => $m->get_data('out_temperature'),
                'out_with_mask' => $m->get_data('out_with_mask'),
                'exit_by' => $gatekeeper_name,
            );

            $qselect=$d->select("staff_visit_master","emp_id = '$emp_id'","ORDER BY staff_visit_id DESC");

            $data = mysqli_fetch_array($qselect);


            $qdelete = $d->update("staff_visit_master",$a,"emp_id = '$emp_id' AND staff_visit_id = '$data[staff_visit_id]' OR emp_id = '$emp_id' AND visit_status=0");

            if ($qdelete == true) {

               $m->set_data('entry_status', '2');
               $m->set_data('in_out_time', date('d-m-Y h:i A'));

            $a2 = array(
                'entry_status' => $m->get_data('entry_status'),
                'in_out_time' => $m->get_data('in_out_time'),
            );

            $d->update("employee_master",$a2,"emp_id = '$emp_id'");


                $qq=$d->select("employee_master,emp_type_master","employee_master.emp_type_id=emp_type_master.emp_type_id AND employee_master.emp_id='$emp_id'");
                $row=mysqli_fetch_array($qq);
                $emp_name= $row['emp_name'].' ('.$row['emp_type_name'].') Exit Allowed';

                if ($row['emp_type_id']==0) {
                    $title = $gatekeeper_name." has ended his duty";
                    $description = "on ".date('d-m-Y h:i A');

                    $adminFcmArray= $d->selectAdminInOut($emp_id,"android",'0');
                    $adminFcmArrayIos= $d->selectAdminInOut($emp_id,"ios",'0');

                    $nAdmin->noti_new($society_id,"", $adminFcmArray, $title, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");
                    $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $title, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");

                    $d->insertGuardDuatyNotification("buildingResourcesNew.png",$title,$description,"employee?manageEmployeeUnit=yes&emp_id=$emp_id",$society_id,$emp_id);

                } else {


                     $blockAry = array();
                      $duCheck=$d->select("employee_block_master","emp_id='$gatekeeper_id'  AND emp_id!=0");
                      while ($oldBlock=mysqli_fetch_array($duCheck)) {
                          array_push($blockAry , $oldBlock['block_id']);
                      }
                      $ids = join("','",$blockAry);   
                      if ($ids!='') {
                          $appendQuery = " AND users_master.block_id IN ('$ids')";
                      }
                    
                    $qUserToken=$d->select("users_master,unit_master,employee_unit_master","employee_unit_master.active_status=0 AND employee_unit_master.unit_id=users_master.unit_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND employee_unit_master.emp_id='$emp_id' AND employee_unit_master.user_id!=0 AND employee_unit_master.user_id=users_master.user_id $appendQuery");
                    // $unitIdAry = explode(",", $row['unit_id']);
                    // $ids = join("','",$unitIdAry);   
                    $description = "Exit Allowed by Security Guard-$gatekeeper_name";
                    // $qUserToken=$d->select("users_master","society_id='$society_id' AND user_token!='' AND unit_id IN ('$ids')");
                    while($data_notification=mysqli_fetch_array($qUserToken)) {

                      $sos_user_token=$data_notification['user_token'];
                      
                      $notiAry = array(
                      'society_id'=>$society_id,
                      'user_id'=>$data_notification['user_id'],
                      'notification_title'=>$emp_name,
                      'notification_desc'=>$description,    
                      'notification_date'=>date('Y-m-d H:i'),
                      'notification_action'=>'resource',
                      'notification_logo'=>'buildingResourcesNew.png',
                      );

                      $device=$data_notification['device'];
                        if ($device=='android') {
                           $nResident->noti("StaffFragment","",$society_id,$sos_user_token,$emp_name,$description,'resource');
                        }  else if($device=='ios') {
                           $nResident->noti_ios("ResourcesVC","",$society_id,$sos_user_token,$emp_name,$description,'resource');
                        }
                      $d->insert("user_notification",$notiAry);
                    }


                    $adminFcmArray= $d->selectAdminInOut($emp_id,"android",'0');
                    $adminFcmArrayIos= $d->selectAdminInOut($emp_id,"ios",'0');

                    $nAdmin->noti_new($society_id,"", $adminFcmArray, $emp_name, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");
                    $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $emp_name, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");

                }
                $response["message"] = ''.$xml->string->exitAllowsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->exitFail;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['deleteVisitor'] == "deleteVisitor" && $visitor_id!='') {
            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('visitor_status', '5');

            $a = array(
                'visitor_status' => $m->get_data('visitor_status')
            );

            $qdelete = $d->update("visitors_master", $a, "user_id='$user_id' AND visitor_id='$visitor_id'");

            if ($qdelete == true) {

                $response["message"] = ''.$xml->string->deleteeSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->failed;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 