<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d H:i:s");
    $temFilter = date("Y-m-d");
    $temFiltertime = date("H:i:s");

        if ($_POST['allowEntryExVisitor'] == "allowEntryExVisitor" && $visitor_id!='') {

           
            $qv = $d->select("visitors_master", "society_id = '$society_id' AND visitor_id='$visitor_id'", "");
            $vdata=mysqli_fetch_array($qv);
            $parking_id  = $vdata['parking_id'];
            $vehicle_no  = $vdata['vehicle_no'];
            $visitor_vehicle_type  = $vdata['visitor_vehicle_type'];


            $expected_type=$vdata['expected_type'];
            if ($expected_type==1) {
                $visitor_mobile = $vdata['visitor_mobile'];
            }

            if ($_POST['vehicle_no']=='') {
                $vehicle_no=$vdata['vehicle_no'];
            }

            $os= $d->selectRow("overstay_alert_minutes","visitorMainType","visitor_main_type_id='$expected_type' AND overstay_alert_minutes>0");
            $overStayData = mysqli_fetch_array($os);
            $overstay_alert_minutes = $overStayData['overstay_alert_minutes'];

            if ($overstay_alert_minutes>0) {
                // code...
                $overstay_time_limit = date("Y-m-d H:i:s",strtotime($temDate." +$overstay_alert_minutes minutes"));
            } 
            
         


            $cTime = date("H:i:s");
            $expire = strtotime($vdata['valid_till_date']);
            $today = strtotime("today $cTime");

            if($today >= $expire){
               $response["message"] = ''.$xml->string->validTimeisOver;
               $response["status"] = "201";
               echo json_encode($response);
               exit();
            } 


            if($vdata['visit_date']!=$temFilter) {
                $visit_date= date("d M Y h:i A", strtotime($vdata['visit_date']));
                $response["message"] = ''.$xml->string->visitDateMsg.' '.$visit_date;
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            } else if ($vdata['visitor_status']==2) {
                $response["message"] = ''.$xml->string->alreadyEnter;
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            }

            if ($visitor_profile != "") {
                $ddd = date("ymdhi");
                $profile_name = "Visitor_" .$ddd. $visitor_mobile . '.png';
                define('UPLOAD_DIR', '../img/visitor/');
                $img = $visitor_profile;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $profile_name;
                $success = file_put_contents($file, $data);

                $notyUrl= $base_url.'img/visitor/'.$profile_name;
            } else {
              if($visitor_type==2) {
                $profile_name = "delivery_boy.png";
              } elseif ($visitor_type==3) {
                $notification_logo="taxi.png";
                $profile_name = "taxi.png";
              } else {
                $profile_name = "visitor_default.png";
                $notification_logo="visitorsNew.png";
              }
                $notyUrl='';
            }

            $visitor_name = html_entity_decode($visitor_name);


            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('visitor_name', $visitor_name);
            $m->set_data('visitor_status', '2');
            $m->set_data('visit_date', $temFilter);
            $m->set_data('visit_time', $temFiltertime);
            $m->set_data('visit_time', $temFiltertime);
            $m->set_data('profile_name', $profile_name);
            $m->set_data('vehicle_no', $vehicle_no);
            $m->set_data('visitor_mobile', $visitor_mobile);
            $m->set_data('country_code', $country_code);
            $m->set_data('in_temperature', $in_temperature);
            $m->set_data('in_with_mask', $in_with_mask);
            $m->set_data('overstay_time_limit', $overstay_time_limit);
            $m->set_data('emp_id', $gatekeeper_id);



            if($flg=="1"){


            $m->set_data('visitor_mobile',$visitor_mobile);
            $m->set_data('number_of_visitor',$number_of_visitor);
            $m->set_data('visiting_reason',$visiting_reason);

                $a = array(
                    'visitor_name'=>$m->get_data('visitor_name'),
                    'visitor_mobile'=>$m->get_data('visitor_mobile'),
                    'country_code'=>$m->get_data('country_code'),
                    'visitor_profile'=>$profile_name,
                    'number_of_visitor'=>$m->get_data('number_of_visitor'),
                    'visit_date' => $m->get_data('visit_date'),
                    'visit_time' => $m->get_data('visit_time'),
                    'visitor_status' => $m->get_data('visitor_status'),
                    'visiting_reason'=>$m->get_data('visiting_reason'),
                    'vehicle_no' =>$m->get_data('vehicle_no'),
                    'in_temperature' =>$m->get_data('in_temperature'),
                    'in_with_mask' =>$m->get_data('in_with_mask'),
                    'overstay_time_limit' =>$m->get_data('overstay_time_limit'),
                    'emp_id' =>$m->get_data('emp_id'),
                );

            }else{

                $a = array(
                    'visitor_name'=>$m->get_data('visitor_name'),
                    'visitor_mobile'=>$m->get_data('visitor_mobile'),
                    'country_code'=>$m->get_data('country_code'),
                    'visit_time' => $m->get_data('visit_time'),
                    'visit_date' => $m->get_data('visit_date'),
                    'visitor_status' => $m->get_data('visitor_status'),
                    'visitor_profile' =>$m->get_data('profile_name'),
                    'vehicle_no' =>$m->get_data('vehicle_no'),
                    'in_temperature' =>$m->get_data('in_temperature'),
                    'in_with_mask' =>$m->get_data('in_with_mask'),
                    'overstay_time_limit' =>$m->get_data('overstay_time_limit'),
                    'emp_id' =>$m->get_data('emp_id'),
                );

            }

            $qdelete = $d->update("visitors_master", $a, "visitor_id='$visitor_id'");

            if ($qdelete == true) {


                if($flag == true){

                    $m->set_data('parking_id', $parking_id);
                    $m->set_data('vehicle_no', $vehicle_no);
                    $m->set_data('parking_status', '1');
                    $m->set_data('visitor', $visitor_id);
                    $m->set_data('vehicle_type', $vehicle_type);
    
                    $a2 = array('parking_type' => $m->get_data('vehicle_type'),
                    // 'parking_status' => $m->get_data('parking_status'),
                    // 'vehicle_no' => $m->get_data('vehicle_no'),
                    'visitor' => $m->get_data('visitor'));
    
                    $d->update("parking_master",$a2,"parking_id='$parking_id'");
    
                    }

                        $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$vdata[user_id]'");

                        $data_notification=mysqli_fetch_array($qUserToken);
                        $sos_user_token=$data_notification['user_token'];
                        $device=$data_notification['device'];
                        $user_id=$data_notification['user_id'];
                        $parent_id=$data_notification['parent_id'];
                        $user_full_name=$data_notification['user_full_name'];
                        $meetName = $user_full_name;

                        $leave_parcel_at_gate=$vdata['leave_parcel_at_gate'];
                        $parcel_collect_master_id=$vdata['parcel_collect_master_id'];

                        if ($leave_parcel_at_gate==1 && $parcel_collect_master_id!=0) {
                             $title="Parcel on gate converted to expected delivery boy";
                             $desc="Entry allowed by Security Guard-$gatekeeper_name";
                             
                             $app = array(
                                'leave_parcel_at_gate'=>0,
                                'parcel_collect_master_id'=>0,
                                
                            );
                          $d->update("visitors_master", $app, "visitor_id='$visitor_id'");
                          $d->delete("parcel_collect_master", "parcel_collect_master_id='$parcel_collect_master_id'");

                        } else if ($expected_type==2) {
                            $title="Delivery Boy Entered";
                            $desc="Entry allowed by Security Guard-$gatekeeper_name";
                            $titleParent = "Delivery Boy has arrived to deliver $meetName parcel";
                            $descParent = "Entry allowed by Security Guard-$gatekeeper_name";
                        } else if ($expected_type==3) {
                             $title="Cab $vehicle_no Entered";
                             $desc="Entry allowed by Security Guard-$gatekeeper_name";
                             $titleParent = "Cab $vehicle_no Entered for pickup $meetName";
                             $descParent = "Entry allowed by Security Guard-$gatekeeper_name";
                        } else  {
                             $title="$visitor_name Visitor Entered";
                             $desc="Entry allowed by Security Guard-$gatekeeper_name";
                             $titleParent = "Visitor $visitor_name has arrived to meet $meetName";
                             $descParent = "Entry allowed by Security Guard-$gatekeeper_name";
                        }



                        if ($device=='android') {
                           $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,$title,$desc,'newVisitor');
                        }  else if($device=='ios') {
                           $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token,$title,$desc,'newVisitor');
                        }


                         $notiAry = array(
                            'society_id'=>$society_id,
                            'user_id'=>$user_id,
                            'notification_title'=>$title,
                            'notification_desc'=>$desc,
                            'notification_date'=>date('Y-m-d H:i'),
                            'notification_action'=>'Visitor',
                            'notification_logo'=>"visitorsNew.png",
                          );
                          
                        $d->insert("user_notification",$notiAry);


                       
                        if ($parent_id>0) {
                            
                           
                            $qPrentToken=$d->select("users_master","society_id='$society_id' AND user_id='$parent_id' AND family_visitor_notification=1");
                            $data_notification_parent=mysqli_fetch_array($qPrentToken);
                            $parent_token=$data_notification_parent['user_token'];
                            $parent_device=$data_notification_parent['device'];
                            if ($parent_token!="" && $titleParent!="") {
                             
                                
                                $clickAray = array(
                                    'title' => $titleParent,
                                    'description' => $descParent,
                                    'img_url' =>  '',
                                    'notification_time' => date("d M Y h:i A"),
                                ); 

                                if ($parent_device=='android') {
                                   $nResident->noti("VisitorFragmentFamily","",$society_id,$parent_token,$titleParent,$descParent,'newVisitorFamily');
                                }  else if($parent_device=='ios') {
                                   $nResident->noti_ios("VisitorVCFamily","",$society_id,$parent_token,$titleParent,$descParent,'newVisitorFamily');
                                }

                                 $notiAryPart = array(
                                    'society_id'=>$society_id,
                                    'user_id'=>$parent_id,
                                    'notification_title'=>"$titleParent",
                                    'notification_desc'=>"$descParent",
                                    'notification_date'=>date('Y-m-d H:i'),
                                    'notification_action'=>'VisitorFamily',
                                    'notification_logo'=>'visitor.png',
                                  );
                                  
                                $d->insert("user_notification",$notiAryPart);
                            }
                        }
                if ($parking_id>0) {
                    $aParking = array(
                            'visitor_vehicle_status' => 1
                    );

                    $m->set_data('parking_id', $parking_id);
                    $m->set_data('vehicle_no', $vehicle_no);
                    $m->set_data('visitor_vehicle_status', '1');
                    $m->set_data('visitor', $visitor_id);
                    $m->set_data('vehicle_type', $vehicle_type);

                    $aParking = array(
                        'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                        'visitor' => $m->get_data('visitor'),
                        'visitor_vehicle_status' => $m->get_data('visitor_vehicle_status'),
                    );

                    $d->update("parking_master",$aParking,"parking_id='$parking_id'");

                    $vehicleLog= array (
                          'society_id'=> $society_id,
                          'parking_id'=> $parking_id,
                          'guest_name'=> $visitor_name,
                          'guest_mobile'=> $visitor_mobile,
                          'vehicle_no'=> $vehicle_no,
                          'vehicle_type'=> $visitor_vehicle_type,
                          'in_time'=> $temDate,
                          'entry_by'=>$gatekeeper_id,
                          'parking_visitor_id'=>$visitor_id,
                      
                    );

                    $d->insert("parking_logs_master",$vehicleLog);


                }

                $response["message"] = ''.$xml->string->entryAloowdMsg;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->allowedFailMsg;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['allowEntryNewVisitorMultiNew'] == "allowEntryNewVisitorMultiNew"  && $visitor_id!='') {

            $vDataQuery= $d->selectRow("visitor_id,visitor_type,parking_id,visitor_vehicle_type,vehicle_no","visitors_master","visitor_id='$visitor_id' OR parent_visitor_id='$parent_visitor_id' AND parent_visitor_id!=0","ORDER BY visitor_id DESC LIMIT 1");
            $vdata = mysqli_fetch_array($vDataQuery);
            $parking_id  = $vdata['parking_id'];
            $vehicle_no  = $vdata['vehicle_no'];
            $visitor_vehicle_type  = $vdata['visitor_vehicle_type'];

            if ($vdata['visitor_status']==2) {
                $response["message"] = ''.$xml->string->alreadyEnter;
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            }
            
            $visitor_name = html_entity_decode($visitor_name);
            $userIdsArray  = explode(",", $user_id);
            $usersids = join("','",$userIdsArray); 

            $userIdsArrayNotApprovd  = explode(",", $uv_user_id);
            $usersidsNotApprovd = join("','",$userIdsArrayNotApprovd); 

            switch ($vdata['visitor_type']) {
                 case '0':
                     // guest...
                  $os= $d->selectRow("overstay_alert_minutes","visitorMainType","visitor_main_type_id='1' AND overstay_alert_minutes>0");
                     break;
                 case '2':
                     // delivery...
                    $os= $d->selectRow("overstay_alert_minutes","visitorMainType","visitor_main_type_id='2' AND overstay_alert_minutes>0");
                     break;
                 case '3':
                     // cab...
                    $os= $d->selectRow("overstay_alert_minutes","visitorMainType","visitor_main_type_id='3' AND overstay_alert_minutes>0");
                     break;
                 case '4':
                     // daily...
                    $os= $d->selectRow("overstay_alert_minutes","visitorMainType","visitor_main_type_id='4' AND overstay_alert_minutes>0");
                     break;
                 case '5':
                     // common...
                    $os= $d->selectRow("overstay_alert_minutes","visitorMainType","visitor_main_type_id='5' AND overstay_alert_minutes>0");
                     break;
                 case '6':
                     // common...
                    $os= $d->selectRow("overstay_alert_minutes","visitorMainType","visitor_main_type_id='6' AND overstay_alert_minutes>0");
                     break;
                 default:
                     // code...
                     break;
             } 
           
            $overStayData = mysqli_fetch_array($os);
            $overstay_alert_minutes = $overStayData['overstay_alert_minutes'];

            if ($overstay_alert_minutes>0) {
                // code...
                $overstay_time_limit = date("Y-m-d H:i:s",strtotime($temDate." +$overstay_alert_minutes minutes"));
            } 

            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('visitor_status', '2');
            $m->set_data('visit_date', $temFilter);
            $m->set_data('visit_time', $temFiltertime);
            $m->set_data('in_temperature', $in_temperature);
            $m->set_data('in_with_mask', $in_with_mask);
            $m->set_data('overstay_time_limit', $overstay_time_limit);
            $m->set_data('emp_id', $gatekeeper_id);

            $a = array(

                'visit_date' => $m->get_data('visit_date'),
                'visit_time' => $m->get_data('visit_time'),
                'visitor_status' => $m->get_data('visitor_status'),
                'in_temperature' => $m->get_data('in_temperature'),
                'in_with_mask' => $m->get_data('in_with_mask'),
                'overstay_time_limit' => $m->get_data('overstay_time_limit'),
                'emp_id' => $m->get_data('emp_id'),
            );

            $qdelete = $d->update("visitors_master", $a, "visitor_id='$visitor_id' OR parent_visitor_id='$parent_visitor_id' AND parent_visitor_id!=0");

            if ($qdelete == true) {
                 

                $qUserToken=$d->select("users_master,visitors_master","users_master.society_id='$society_id' AND visitors_master.parent_visitor_id='$parent_visitor_id' AND users_master.user_id=visitors_master.user_id AND users_master.user_id IN ('$usersids')");
    
                while($data_notification=mysqli_fetch_array($qUserToken)){
                    $sos_user_token=$data_notification['user_token'];
                    $device=$data_notification['device'];
                    $visitor_type=$data_notification['visitor_type'];
                    $expected_type=$data_notification['expected_type'];
                    $visitor_name=$data_notification['visitor_name'];
                    $vehicle_no=$data_notification['vehicle_no'];
                    if ($visitor_type==2  || $expected_type==2) {
                        $title= "Delivery boy $visitor_name Entered";
                        $description = "Entry allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "delivery_boy.png";
                    } else  if ($visitor_type==3  || $expected_type==3) {
                        $title= "Cab $vehicle_no $visitor_name Entered";
                        $description = "Entry allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "taxi.png";

                    }else  if ($visitor_type==6 || $expected_type==6) {
                        $title= "$visitor_name Vendor Entered";
                        $description = "Entry allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "vendor.png";

                    } else {
                        $title= "$visitor_name Visitor Arrived";
                        $description = "Entry allowed by Security Guard-$gatekeeper_name";
                        $notification_logo= "visitorsNew.png";
                    }

                    if ($device=='android') {
                       $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,$title,$description,'newVisitor');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token,$title,$description,'newVisitor');
                    }


                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$data_notification['user_id'],
                        'notification_title'=>"$title",
                        'notification_desc'=>"$description",
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'Visitor',
                        'notification_logo'=>$notification_logo,
                      );
                      
                    $d->insert("user_notification",$notiAry);

                    // send notification to parent
                    if ($data_notification['member_status']==1) {
                        $parent_id = $data_notification['parent_id'];
                        $meetName = $data_notification['user_full_name'];

                        $qPrentToken=$d->selectRow("user_token,device","users_master","society_id='$society_id' AND user_id='$parent_id' AND family_visitor_notification=1 AND user_token!=''");
                        $data_notification_parent=mysqli_fetch_array($qPrentToken);
                        $parent_token=$data_notification_parent['user_token'];
                        $parent_device=$data_notification_parent['device'];
                         
                        $titleParent = "Visitor $visitor_name has arrived to meet $meetName";
                        $descParent = "Entry allowed by Security Guard-$gatekeeper_name";
                        

                        if ($parent_device=='android') {
                           $nResident->noti("VisitorFragmentFamily","",$society_id,$parent_token,$titleParent,$descParent,'newVisitorFamily');
                        }  else if($parent_device=='ios') {
                           $nResident->noti_ios("VisitorVCFamily","",$society_id,$parent_token,$titleParent,$descParent,'newVisitorFamily');
                        }

                         $notiAryPart = array(
                            'society_id'=>$society_id,
                            'user_id'=>$parent_id,
                            'notification_title'=>"$titleParent",
                            'notification_desc'=>"$descParent",
                            'notification_date'=>date('Y-m-d H:i'),
                            'notification_action'=>'VisitorFamily',
                            'notification_logo'=>'visitor.png',
                          );
                          
                        $d->insert("user_notification",$notiAryPart);
                    }
                    // send notification to parent end
                }

                // while end

                $qUserTokenNon=$d->select("users_master,visitors_master","users_master.society_id='$society_id' AND visitors_master.parent_visitor_id='$parent_visitor_id' AND users_master.user_id=visitors_master.user_id AND users_master.user_id IN ('$usersidsNotApprovd')");
    
                while($data_notification_non=mysqli_fetch_array($qUserTokenNon)){
                    $sos_user_token=$data_notification_non['user_token'];
                    $device=$data_notification_non['device'];
                    $visitor_type=$data_notification_non['visitor_type'];
                    $visitor_name=$data_notification_non['visitor_name'];
                    $vehicle_no=$data_notification_non['vehicle_no'];

                    if ($visitor_type==2) {
                        $title= "Delivery boy $visitor_name Entered";
                        $description = "Entry allowed by Security Guard-$gatekeeper_name (Approved by other member)";
                        $notification_logo= "delivery_boy.png";
                    } else  if ($visitor_type==3) {
                        $title= "Cab $vehicle_no $visitor_name Entered";
                        $description = "Entry allowed by Security Guard-$gatekeeper_name (Approved by other member)";
                        $notification_logo= "taxi.png";

                    } else {
                        $title= "$visitor_name Visitor Arrived";
                        $description = "Entry allowed by Security Guard-$gatekeeper_name (Approved by other member)";
                        $notification_logo= "visitorsNew.png";
                    }

                    if ($device=='android') {
                       $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,$title,$description,'newVisitor');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token,$title,$description,'newVisitor');
                    }


                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$data_notification_non['user_id'],
                        'notification_title'=>"$title",
                        'notification_desc'=>"$description",
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'Visitor',
                        'notification_logo'=>$notification_logo,
                      );
                      
                    $d->insert("user_notification",$notiAry);

                   
                }

                if ($parking_id>0) {
                    $aParking = array(
                            'visitor_vehicle_status' => 1
                    );

                     $m->set_data('parking_id', $parking_id);
                     $m->set_data('vehicle_no', $vehicle_no);
                    $m->set_data('visitor_vehicle_status', '1');
                    $m->set_data('visitor', $visitor_id);
                    $m->set_data('vehicle_type', $vehicle_type);

                    $aParking = array(
                        'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                        'visitor' => $m->get_data('visitor'),
                        'visitor_vehicle_status' => $m->get_data('visitor_vehicle_status'),
                    );

                    $d->update("parking_master",$aParking,"parking_id='$parking_id'");

                    $vehicleLog= array (
                          'society_id'=> $society_id,
                          'parking_id'=> $parking_id,
                          'guest_name'=> $visitor_name,
                          'guest_mobile'=> $visitor_mobile,
                          'vehicle_no'=> $vehicle_no,
                          'vehicle_type'=> $visitor_vehicle_type,
                          'in_time'=> $temDate,
                          'entry_by'=>$gatekeeper_id,
                          'parking_visitor_id'=>$visitor_id,
                      
                    );

                    $d->insert("parking_logs_master",$vehicleLog);


                }

                $response["message"] = ''.$xml->string->allowSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->allowedFailMsg;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['allowEntryStaff'] == "allowEntryStaff"  && $emp_id!='') {

            $m->set_data('emp_id', $emp_id);
            $m->set_data('society_id', $society_id);
            $m->set_data('visit_entry_date_time', $temDate);
            $m->set_data('visit_status', '0');
            $m->set_data('filter_data', $temFilter);
            $m->set_data('in_temperature', $in_temperature);
            $m->set_data('in_with_mask', $in_with_mask);


            if($flg=='1'){

                $a = array(
                    'emp_id' => $m->get_data('emp_id'),
                    'society_id' => $m->get_data('society_id'),
                    'visit_entry_date_time' => $m->get_data('visit_entry_date_time'),
                    'visit_status' => $m->get_data('visit_status'),
                    'filter_data' => $m->get_data('filter_data'),
                    'emp_gate_keeper' => 1,
                    'entry_by' => $gatekeeper_name,
                );


            }else{

                $a = array(
                    'emp_id' => $m->get_data('emp_id'),
                    'society_id' => $m->get_data('society_id'),
                    'visit_entry_date_time' => $m->get_data('visit_entry_date_time'),
                    'visit_status' => $m->get_data('visit_status'),
                    'filter_data' => $m->get_data('filter_data'),
                    'in_temperature' => $m->get_data('in_temperature'),
                    'in_with_mask' => $m->get_data('in_with_mask'),
                    'entry_by' => $gatekeeper_name,
                );

                $alredyInCheck= $d->selectRow("emp_id","employee_master","entry_status=1 AND emp_id='$emp_id'","");
                if (mysqli_num_rows($alredyInCheck)>0) {
                    $response["message"] = ''.$xml->string->alreadyEnter;
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();
                }

            }
            $qdelete = $d->insert("staff_visit_master",$a);

            if ($qdelete == true) {

                $m->set_data('entry_status', '1');
                $m->set_data('in_out_time', date('d-m-Y h:i A'));

                $a2 = array(
                    'entry_status' => $m->get_data('entry_status'),
                    'in_out_time' => $m->get_data('in_out_time'),
                );

                $d->update("employee_master",$a2,"emp_id = '$emp_id'");
                $qq=$d->select("employee_master,emp_type_master","employee_master.emp_type_id=emp_type_master.emp_type_id AND employee_master.emp_id='$emp_id'");
                $row=mysqli_fetch_array($qq);
                $emp_name= $row['emp_name'].' ('.$row['emp_type_name'].') has Arrived';
                $description = "Entry allowed by Security Guard-$gatekeeper_name";
                  
                  $blockAry = array();
                  $duCheck=$d->select("employee_block_master","emp_id='$gatekeeper_id'  AND emp_id!=0");
                  while ($oldBlock=mysqli_fetch_array($duCheck)) {
                      array_push($blockAry , $oldBlock['block_id']);
                  }
                  $ids = join("','",$blockAry);   
                  if ($ids!='') {
                      $appendQuery = " AND users_master.block_id IN ('$ids')";
                  }

                $qUserToken=$d->select("users_master,unit_master,employee_unit_master","employee_unit_master.active_status=0 AND employee_unit_master.unit_id=users_master.unit_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND employee_unit_master.emp_id='$emp_id' AND employee_unit_master.user_id!=0 AND employee_unit_master.user_id=users_master.user_id $appendQuery");
                // $ids = join("','",$unitIdAry);   
                // $qUserToken=$d->select("users_master","society_id='$society_id' AND user_token!='' AND unit_id IN ('$ids')");
                while($data_notification=mysqli_fetch_array($qUserToken)) {

                  $sos_user_token=$data_notification['user_token'];
                  
                  $notiAry = array(
                  'society_id'=>$society_id,
                  'user_id'=>$data_notification['user_id'],
                  'notification_title'=>$emp_name,
                  'notification_desc'=>$description,    
                  'notification_date'=>date('Y-m-d H:i'),
                  'notification_action'=>'resource',
                  'notification_logo'=>'buildingResourcesNew.png',
                  );

                  $device=$data_notification['device'];
                    if ($device=='android') {
                       $nResident->noti("StaffFragment","",$society_id,$sos_user_token,$emp_name,$description,'resource');
                    }  else if($device=='ios') {
                       $nResident->noti_ios("ResourcesVC","",$society_id,$sos_user_token,$emp_name,$description,'resource');
                    }
                  $d->insert("user_notification",$notiAry);
                }
                
                $adminFcmArray= $d->selectAdminInOut($emp_id,"android",'0');
                $adminFcmArrayIos= $d->selectAdminInOut($emp_id,"ios",'0');

                $nAdmin->noti_new($society_id,"", $adminFcmArray, $emp_name, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");
                $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $emp_name, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");

                $empType = $row['emp_type_name'];
                if ($empType=='Technician') {
                     // assing new complain
                    $technician_id_old = $emp_id;
                    $openComplain = $d->count_data_direct("complain_id","complains_master,unit_master","complains_master.unit_id=unit_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.flag_delete!=2 AND complains_master.complain_status!='1' AND complains_master.flag_delete=0 AND complains_master.technician_id='$technician_id_old'");

                    if ($technician_id_old>0 && $openComplain<1) {
                        $compCategoryArrayEmp = array();
                        $cuCHeck=$d->select("employee_master,employee_complaint_category","employee_master.emp_id=employee_complaint_category.emp_id AND employee_master.emp_id='$technician_id_old' AND employee_master.emp_status=1");
                        while ($oldCat=mysqli_fetch_array($cuCHeck)) {
                            array_push($compCategoryArrayEmp , $oldCat['complaint_category_id']);
                        }

                        $ids = join("','",$compCategoryArrayEmp); 
                        $openComplainArray = array();
                        $openComQuery=$d->selectRow("complains_master.complain_id,complains_master.complaint_category","complains_master,unit_master","complains_master.unit_id=unit_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.flag_delete!=2 AND complains_master.complain_status!='1' AND complains_master.flag_delete=0 AND complains_master.technician_id=0 AND complains_master.complaint_category IN ('$ids')","ORDER BY complains_master.complain_id ASC");
                        $compData=mysqli_fetch_array($openComQuery);
                        $complain_id_next_assing = $compData['complain_id'];
                        
                        if($complain_id_next_assing >0) {
                            $anew= array (
                                'technician_id'=> $technician_id_old,
                            );

                            $d->update("complains_master",$anew,"complain_id='$complain_id_next_assing'");

                            $gt=$d->selectRow("compalain_title,complain_assing_to","complains_master","complain_id='$complain_id_next_assing'");
                            $getCompData = mysqli_fetch_array($gt);
                            $complain_assing_to = $getCompData['complain_assing_to'];
                            $compalain_title = $getCompData['compalain_title'];

                            $qToken=$d->selectRow("technician_token","employee_master","emp_id='$technician_id_old' AND technician_token!=''");
                            $techData=mysqli_fetch_array($qToken);
                            $technician_token = $techData['technician_token'];
                            $title = "New Complaint Assigned For $complain_assing_to";
                            if ($technician_token!="") {
                                $nGaurd->noti_new("",$technician_token,$title,$compalain_title,"0");

                                $notiAryTech = array(
                                    'society_id' => $society_id,
                                    'guard_notification_title' => $title,
                                    'guard_notification_desc' => $compalain_title,
                                    'employee_id' => $technician_id_old,
                                    'guard_notification_date' => date('Y-m-d H:i'),
                                    'click_action' => '0',
                                    'notification_logo'=>'ComplaintsNew.png',
                                );
                                $d->insert("guard_notification_master", $notiAryTech);
                            }
                        }   
                          
                      } 

                }

                $response["message"] = ''.$xml->string->allowSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->allowedFailMsg;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['deleteVisitor'] == "deleteVisitor" && $visitor_id!='') {
            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('visitor_status', '5');

            $a = array(
                'visitor_status' => $m->get_data('visitor_status')
            );

            $qdelete = $d->update("visitors_master", $a, "user_id='$user_id' AND visitor_id='$visitor_id'");

            if ($qdelete == true) {

                $response["message"] = ''.$xml->string->deleteeSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->failed;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 