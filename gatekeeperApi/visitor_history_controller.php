<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {


    if ($key == $keydb) {

    


    $response = array();
    extract(array_map("test_input" , $_POST));

    if (isset($society_id)) {
        $qss=$d->selectRow("visitor_mobile_number_show_gatekeeper","society_master"," society_id ='$society_id'");
        $societyData = mysqli_fetch_array($qss); 
        $visitorMobilePrivacy = $societyData['visitor_mobile_number_show_gatekeeper'];        
    }

    $temFilter = date("Y-m-d");
    $privDate = date('Y-m-d', strtotime($temFilter .' -1 day'));

    if (isset($getNewVisitorHistoryVisitor) && $getNewVisitorHistoryVisitor == 'getNewVisitorHistoryVisitor' && $society_id!='') {

            $response["list_visitor"] = array();
           
            if ($access_blocks!="") {
                $appendQuery = " AND block_master.block_id IN($access_blocks)";
            }

            if ($start_date !='' && $end_date!='') {

            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));

                $q2 = $d->select("unit_master,block_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "(visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND (visitors_master.visitor_status = '3' OR visitors_master.visitor_status = '5' AND visitors_master.exit_date!='') AND visitors_master.visit_date  BETWEEN '$start_date' AND '$end_date'  $appendQuery )","GROUP BY visitors_master.parent_visitor_id  ORDER BY visitors_master.exit_date DESC, visitors_master.exit_time DESC LIMIT 1000");
            } else {
                $q2 = $d->select("unit_master,block_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "(visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND (visitors_master.visitor_status = '3' OR visitors_master.visitor_status = '5' AND visitors_master.exit_date!='') AND visitors_master.exit_date  >= ( CURDATE() - INTERVAL 1 DAY ) $appendQuery )","GROUP BY visitors_master.parent_visitor_id  ORDER BY visitors_master.exit_date DESC, visitors_master.exit_time DESC");
            }
             

            if (mysqli_num_rows($q2) > 0) {
                while ($data2 = mysqli_fetch_array($q2)) {
                    $data2 = array_map("html_entity_decode", $data2);
                    $list = array();
                if ($data2['exit_date']!="") {
                $intTime=$data2['visit_date']." ".$data2['visit_time'];
                $outTime=$data2['exit_date']." ".$data2['exit_time'];
                $date_a = new DateTime($intTime);
                $date_b = new DateTime($outTime);
                $interval = date_diff($date_a,$date_b);
                $totalDays= $interval->d;
                $totalMinutes= $interval->i;
                $totalHours= $interval->h;
                if ($totalDays>0) {
                $list["duration"] = $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                } else {
                    $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                }
                } else {
                     $list["duration"] = $xml->string->not_available.'';
                }
           
                $visitDate = date("d M Y h:i A", strtotime($data2['visit_date']." ".$data2['visit_time']));
                $visitDateFilter = date("d-m-Y", strtotime($data2['visit_date']));
                $exitDate = ($data2['exit_date']!="") ? date("d M Y h:i A", strtotime($data2['exit_date']." ".$data2['exit_time'])) :  "Auto Exit"; 

                // $qm=$d->select("users_master,visitors_master","users_master.user_id=visitors_master.user_id AND users_master.society_id ='$society_id' AND visitors_master.parent_visitor_id='$data2[visitor_id]'","ORDER BY users_master.user_id DESC");
                //     $userNameAry=array();

                //     while($userDataMulti=mysqli_fetch_array($qm)){
                //         array_push($userNameAry, $userDataMulti['user_first_name']);
                //     }
                //     $userNameOly= implode(",", $userNameAry);

                    $list["id"] = $data2['visitor_id'];
                    $list["name"] =$data2['visitor_name'].'';
                    $list["color_status"] = '';
                    $list["last_in"] = "";
                    $list["visit_from"] =$data2['visit_from'].'';
                    $list["mobile"] = $data2['visitor_mobile'];
                    $list["country_code"] = $data2['country_code'];
                    $list["user_id"] = $data2['user_id'];
                    $list["unit_id"] =$data2['unit_id'];;
                    $list["block_id"] =$data2['block_id'];
                    $list["floor_id"] =$data2['floor_id'];
                   
                    if ($data2['visitor_sub_image']!='') {
                        $list["visit_logo"]  = $base_url.'img/visitor_company/'.$data2['visitor_sub_image'].'';
                    } else {
                        $list["visit_logo"] ="";
                    }

                    if ($visitorMobilePrivacy==0 && $data2['visitor_type']==0  OR  $visitorMobilePrivacy==0 && $data2['visitor_type']==1) {
                    $list["visitor_mobile_new"] = "".substr($data2['visitor_mobile'], 0, 3) . '****' . substr($data2['visitor_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$data2['visitor_mobile'];
                    }
                    if ($data2['user_id']=='0') {
                        $list["des_block"] = $data2['block_name'] . "-" .  $data2['unit_name'] .' (Empty)';
                    } else  if ($data2['visiting_apartment']!="") {
                        $list["des_block"] = $data2['visiting_apartment'];
                    }else {
                        $list["des_block"] = $data2['block_name'] . "-" .  $data2['unit_name'];
                    }

                    if ($data2['visitor_type']==1 && $data2['expected_type']==1) {
                        $visitor_type = 0;
                    }else if ($data2['visitor_type']==1 && $data2['expected_type']==2) {
                         $visitor_type = 2;
                    }else if ($data2['visitor_type']==1 && $data2['expected_type']==3) {
                         $visitor_type = 3;
                    }else {
                        $visitor_type = $data2['visitor_type'];
                    }
                    $list["visitor_type"] =$visitor_type;
                    $list["expected_type"] =$data2['expected_type'];
                    $list["visit_date_time"] = $visitDate;
                    $list["visit_date"] = $visitDateFilter;
                    $list["visit_exit_date_time"] = $exitDate;
                    $list["qr_code"] = $data2['visitor_id'];
                    $list["type"] = "Exvisitor";
                    $list["profile_img"] = $base_url . "img/visitor/" . $data2['visitor_profile'];
                    $list["profile_img_name"] = $data2['visitor_profile'];
                    $list["visitor_sub_type_id"] = $data2['visitor_sub_type_id'];

                    if ($data2['visitor_status']==2) {
                        if ($data2['in_temperature']>0) {
                            $in_temperature=$data2['in_temperature'];
                        } else {
                            $in_temperature = "";
                        }
                        $list["temperature"]=$in_temperature.'';
                        $list["with_mask"]=$data2['in_with_mask'].'';
                    } else if ($data2['visitor_status']==3) {
                         if ($data2['out_temperature']>0) {
                            $out_temperature=$data2['out_temperature'];
                        } else {
                            $out_temperature = "";
                        }
                        $list["temperature"]=$out_temperature.'';
                        $list["with_mask"]=$data2['out_with_mask'].'';
                    } else {
                        $list["temperature"]='';
                        $list["with_mask"]='';
                    }
                    
                    $list["re_enter"] = true;
                    
                    $list["workig_units"] ="";
                    $list["vehicle_no"] =$data2['vehicle_no'].'';

                    array_push($response["list_visitor"], $list);
                }
                $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = ''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if (isset($getNewVisitorHistoryDaily) && $getNewVisitorHistoryDaily == 'getNewVisitorHistoryDaily' && $society_id!='') {

            $response["list_daily"] = array();
           
              $qDaily = $d->selectRow("visitors_master.visitor_id,visitors_master.parent_visitor_id,visitors_master.society_id,visitors_master.floor_id,visitors_master.block_id,visitors_master.block_id,visitors_master.unit_id,visitors_master.visitor_type,visitors_master.expected_type,visitors_master.visit_from,visitors_master.visitor_mobile,visitors_master.country_code,visitors_master.user_id,visitors_master.visitor_name,visitors_master.number_of_visitor,visitors_master.visit_date,visitors_master.visit_time,visitors_master.exit_date,visitors_master.exit_time,visitors_master.in_temperature,visitors_master.out_temperature,visitors_master.in_with_mask,visitors_master.out_with_mask,visitors_master.visitor_sub_type_id,daily_visitors_master.visitor_profile,visitors_master.daily_visitor_id,visitorSubType.visitor_sub_image","daily_visitors_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "daily_visitors_master.visitor_id=visitors_master.daily_visitor_id AND visitors_master.visitor_type = '4' AND   visitors_master.society_id = '$society_id' AND visitors_master.visitor_status = '3' AND visitors_master.exit_date  >= ( CURDATE() - INTERVAL 1 DAY ) ", "ORDER BY visitors_master.exit_date DESC, visitors_master.exit_time DESC");

            if (mysqli_num_rows($qDaily) > 0) {
                while ($dataDaily = mysqli_fetch_array($qDaily)) {
                    $dataDaily = array_map("html_entity_decode", $dataDaily);
                    $list = array();

                    // $fq=$d->selectRow("visitor_profile","daily_visitors_master","visitor_id='$dataDaily[daily_visitor_id]'");
                    // $dData=mysqli_fetch_array($fq);

             $intTime=$dataDaily['visit_date']." ".$dataDaily['visit_time'];
             $outTime=$dataDaily['exit_date']." ".$dataDaily['exit_time'];
             $date_a = new DateTime($intTime);
             $date_b = new DateTime($outTime);
             $interval = date_diff($date_a,$date_b);
             $totalDays= $interval->d;
                $totalMinutes= $interval->i;
                $totalHours= $interval->h;
                if ($totalDays>0) {
                $list["duration"] = $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                } else {
                    $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                }
          
            
                $visitDate = date("d M Y h:i A", strtotime($dataDaily['visit_date']." ".$dataDaily['visit_time']));
                $exitDate = date("d M Y h:i A", strtotime($dataDaily['exit_date']." ".$dataDaily['exit_time']));

               
                    $list["id"] = $dataDaily['daily_visitor_id'];
                    $list["name"] =$dataDaily['visitor_name'].'';
                    $list["color_status"] = '';
                    $list["last_in"] = "";
                    $list["visit_from"] =$dataDaily['visit_from'].'';
                    $list["mobile"] = $dataDaily['visitor_mobile'];
                    $list["country_code"] = $dataDaily['country_code'];
                    $list["user_id"] ="";
                    $list["unit_id"] ="";
                    $list["block_id"] ="";
                    $list["floor_id"] ="";

                    //  $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$dataDaily[visitor_sub_type_id]'");
                    // $vistLogo=mysqli_fetch_array($fd);
                     if ($dataDaily['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$dataDaily['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $list["visit_logo"] = $visit_logo.'';

                    if ($visitorMobilePrivacy==0 && $dataDaily['visitor_type']==0  OR  $visitorMobilePrivacy==0 && $dataDaily['visitor_type']==1) {
                    $list["visitor_mobile_new"] = "".substr($dataDaily['visitor_mobile'], 0, 3) . '****' . substr($dataDaily['visitor_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$dataDaily['visitor_mobile'];
                    }

                     $uqcoiunt=$d->selectRow("daily_visitor_unit_master.daily_visitor_unit_id","unit_master,block_master,users_master,daily_visitor_unit_master","daily_visitor_unit_master.unit_id=unit_master.unit_id AND   
                    daily_visitor_unit_master.user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  daily_visitor_unit_master.daily_visitor_id ='$dataDaily[daily_visitor_id]' ","GROUP BY daily_visitor_unit_master.unit_id ORDER BY users_master.user_id DESC");
                     $working_units= mysqli_num_rows($uqcoiunt);

                    $list["des_block"] = 'Working Units : '. $working_units;
                    
                    $list["visitor_type"] =$dataDaily['visitor_type'];
                    $list["expected_type"] ="0";
                    $list["visit_date_time"] = $visitDate;
                    $list["visit_exit_date_time"] = $exitDate;
                    $list["qr_code"] = $dataDaily['visitor_id'];
                    $list["type"] = "Daily Visitor";
                    $list["profile_img"] = $base_url . "/img/visitor/" . $dataDaily['visitor_profile'];
                    if ($dataDaily['visitor_status']==2) {
                        if ($dataDaily['in_temperature']>0) {
                            $in_temperature=$dataDaily['in_temperature'];
                        } else {
                            $in_temperature = "";
                        }
                        $list["temperature"]=$in_temperature.'';
                        $list["with_mask"]=$dataDaily['in_with_mask'].'';
                    } else if ($dataDaily['visitor_status']==3) {
                         if ($dataDaily['out_temperature']>0) {
                            $out_temperature=$dataDaily['out_temperature'];
                        } else {
                            $out_temperature = "";
                        }
                        $list["temperature"]=$out_temperature.'';
                        $list["with_mask"]=$dataDaily['out_with_mask'].'';
                    } else {
                        $list["temperature"]='';
                        $list["with_mask"]='';
                    }
                    $list["re_enter"] = false;
                    $list["visitor_sub_type_id"] = $dataDaily['visitor_sub_type_id'];
                    $list["workig_units"] ="";
                    $list["vehicle_no"] ="";
                    
                    array_push($response["list_daily"], $list);
                }
                $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {
              
                $response["message"] = ''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if (isset($getNewVisitorHistoryCommon) && $getNewVisitorHistoryCommon == 'getNewVisitorHistoryCommon' && $society_id!='') {
             $response["list_common"] = array();
                
            $q11 = $d->select("visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "unit_id=0 AND visitor_type = '5' AND   society_id = '$society_id' AND visitor_status = '3' AND exit_date  >= ( CURDATE() - INTERVAL 5 DAY ) ", "GROUP BY parent_visitor_id ORDER BY visitor_id DESC ");

            if (mysqli_num_rows($q11) > 0) {
                while ($data3 = mysqli_fetch_array($q11)) {

                    $data3 = array_map("html_entity_decode", $data3);
                    $list = array();

                $intTime=$data3['visit_date']." ".$data3['visit_time'];
                $outTime=$data3['exit_date']." ".$data3['exit_time'];
                $date_a = new DateTime($intTime);
                $date_b = new DateTime($outTime);
                $interval = date_diff($date_a,$date_b);
                $totalDays= $interval->d;
                $totalMinutes= $interval->i;
                $totalHours= $interval->h;
                if ($totalDays>0) {
                $list["duration"] = $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                } else {
                    $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                }
          
            
                $visitDate = date("d M Y h:i A", strtotime($data3['visit_date']." ".$data3['visit_time']));
                $exitDate = date("d M Y h:i A", strtotime($data3['exit_date']." ".$data3['exit_time']));

               
                    $list["id"] = $data3['visitor_id'];
                    $list["name"] =$data3['visitor_name'].'';
                    $list["color_status"] = '';
                    $list["last_in"] = "";
                    $list["visit_from"] =$data3['visit_from'].'';
                    $list["mobile"] = $data3['visitor_mobile'];
                    $list["country_code"] = $data3['country_code'];
                    $list["user_id"] = $data3['user_id'];
                    $list["unit_id"] =$data3['unit_id'];;
                    $list["block_id"] =$data3['block_id'];
                    $list["floor_id"] =$data3['floor_id'];
                    //  $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data3[visitor_sub_type_id]'");
                    // $vistLogo=mysqli_fetch_array($fd);
                     if ($data3['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$data3['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $list["visit_logo"] = $visit_logo.'';

                    if ($visitorMobilePrivacy==0 && $data3['visitor_type']==0  OR  $visitorMobilePrivacy==0 && $data3['visitor_type']==1) {
                    $list["visitor_mobile_new"] = "".substr($data3['visitor_mobile'], 0, 3) . '****' . substr($data3['visitor_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$data3['visitor_mobile'];
                    }
                    $list["des_block"] = $data3['visit_from'].'';
                    $list["visitor_type"] =$data3['visitor_type'];
                    $list["expected_type"] =$data3['expected_type'];
                    $list["visit_date_time"] = $visitDate;
                    $list["visit_exit_date_time"] = $exitDate;
                    $list["qr_code"] = $data3['visitor_id'];
                    $list["type"] = "Exvisitor";
                    $list["profile_img"] = $base_url . "img/visitor/" . $data3['visitor_profile'];
                    $list["profile_img_name"] = $data3['visitor_profile'];
                    if ($data3['visitor_status']==2) {
                        if ($data3['in_temperature']>0) {
                            $in_temperature=$data3['in_temperature'];
                        } else {
                            $in_temperature = "";
                        }
                        $list["temperature"]=$in_temperature.'';
                        $list["with_mask"]=$data3['in_with_mask'].'';
                    } else if ($data3['visitor_status']==3) {
                         if ($data3['out_temperature']>0) {
                            $out_temperature=$data3['out_temperature'];
                        } else {
                            $out_temperature = "";
                        }
                        $list["temperature"]=$out_temperature.'';
                        $list["with_mask"]=$data3['out_with_mask'].'';
                    } else {
                        $list["temperature"]='';
                        $list["with_mask"]='';
                    }
                    $list["re_enter"] = true;
                    $list["visitor_sub_type_id"] = $data3['visitor_sub_type_id'];
                    $list["workig_units"] ="";
                    array_push($response["list_common"], $list);
                }
                 $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {
             
                $response["message"] = ''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if (isset($getNewVisitorHistoryStaff) && $getNewVisitorHistoryStaff == 'getNewVisitorHistoryStaff' && $society_id!='') {
             $response["list_staff"] = array();
            
           

            $q = $d->select("employee_master,emp_type_master,staff_visit_master", "staff_visit_master.visit_exit_date_time  >= ( CURDATE() - INTERVAL 1 DAY ) AND employee_master.emp_type_id != '0' AND employee_master.emp_type_id =emp_type_master.emp_type_id AND employee_master.society_id='$society_id' AND staff_visit_master.emp_id = employee_master.emp_id AND staff_visit_master.visit_status ='1' ","ORDER BY staff_visit_master.visit_exit_date_time DESC");
           

           

            if (mysqli_num_rows($q) > 0) {
                while ($data = mysqli_fetch_array($q)) {
                    $data = array_map("html_entity_decode", $data);

                    $list = array();

                    $today = date("Y-m-d");
                    $last_time = date("Y-m-d", strtotime($data['in_out_time']));

                    $earlier = new DateTime($today);
                    $later = new DateTime($last_time);
                    $diff = $later->diff($earlier)->format("%a");

                    if ($diff>29) {
                        $color_status='3';
                    } else if ($diff>15) {
                        $color_status='2';
                    } else {
                         $color_status='1';
                    }

                    $intTime=$data['visit_entry_date_time'];
                    $outTime=$data['visit_exit_date_time'];
                    
                    $date_a = new DateTime($intTime);
                    $date_b = new DateTime($outTime);
                    
                    $interval = date_diff($date_a,$date_b);
                    $totalDays= $interval->d;
                    $totalMinutes= $interval->i;
                    $totalHours= $interval->h;
                    if ($totalDays>0) {
                    $list["duration"] = $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                    } else {
                        $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                    }

                    $visitDate = date("d M Y h:i A", strtotime($data['visit_entry_date_time']));
                    $exitDate = date("d M Y h:i A", strtotime($data['visit_exit_date_time']));


                    $list["id"] = $data['emp_id'];
                    $list["name"] = $data['emp_name'];
                    $list["color_status"] = $color_status;
                    $list["last_in"] = time_elapsed_string($data['in_out_time']);
                    $list["visit_from"] ='';
                    $list["mobile"] = $data['emp_mobile'];
                    $list["user_id"] = '';
                    $list["unit_id"] ='';
                    $list["block_id"] ='';
                    $list["floor_id"] ='';
                     $list["visit_logo"] = '';
                    if ($data['private_resource'] || $visitorMobilePrivacy==0) {
                    $list["visitor_mobile_new"] = "".substr($data['emp_mobile'], 0, 3) . '****' . substr($data['emp_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$data['emp_mobile'];
                    }
                    $list["country_code"] = $data['country_code'];
                    $list["des_block"] = $data['emp_type_name'];
                    $list["qr_code"] = "qr" . $data['emp_id'];
                    $list["type"] = "staff";
                    $list["visit_date_time"] = $visitDate;
                    $list["visit_exit_date_time"] = $exitDate;
                    $list["profile_img"] = $base_url . "/img/emp/" . $data['emp_profile'];

                    if ($data['entry_status']==1) {
                        if ($data['in_temperature']>0) {
                          $in_temperature= $data['in_temperature'];
                        } else {
                            $in_temperature="";
                        }
                        $list["temperature"]=$in_temperature.'';
                        $list["with_mask"]=$data['in_with_mask'].'';
                    } else if ($data['entry_status']==2) {
                        if ($data['out_temperature']>0) {
                          $out_temperature= $data['out_temperature'];
                        } else {
                             $out_temperature= "";
                        }
                        $list["temperature"]=$out_temperature.'';
                        $list["with_mask"]=$data['out_with_mask'].'';
                    }  else {
                        $list["temperature"]='';
                        $list["with_mask"]='';
                    }
                    $list["re_enter"] = false;
                    $list["visitor_sub_type_id"] ="";
                    if ($data['emp_type']==1) {
                        $uq=$d->selectRow("employee_unit_master.employee_unit_id","unit_master,block_master,users_master,employee_unit_master ","users_master.delete_status=0 AND employee_unit_master .unit_id=unit_master.unit_id  AND
                            employee_unit_master .user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  employee_unit_master .emp_id ='$data[emp_id]' ","GROUP BY employee_unit_master.unit_id ORDER BY unit_master.unit_id ASC");
                         $working_units= mysqli_num_rows($uq);

                    $workiUnits = 'Working Units : '. $working_units;
                    } else {
                    $workiUnits = "";
                    }

                    $list["workig_units"] =$workiUnits."";
                    $list["vehicle_no"] ="";

                    array_push($response["list_staff"], $list);
                }
                 $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            }   else {
                $response["message"] = ''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if (isset($getNewVisitorHistoryGroupWiseSearch) && $getNewVisitorHistoryGroupWiseSearch == 'getNewVisitorHistoryGroupWiseSearch' && $society_id!='') {

            
            

            $response["list_common"] = array();
            $response["list_visitor"] = array();


            if ($search_value=='') {
               $response["message"] = ''.$xml->string->faildGetlist;
               $response["status"] = "201";
               echo json_encode($response);
               exit();
            }

            $q11 = $d->select("visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "visitor_type = '5' AND   society_id = '$society_id' AND visitor_status = '3' AND  visitor_name LIKE '%$search_value%' OR visitor_type = '5' AND   society_id = '$society_id' AND visitor_status = '3' AND  visitor_mobile LIKE '%$search_value%' OR visitor_type = '5' AND   society_id = '$society_id' AND visitor_status = '3' AND  vehicle_no LIKE '%$search_value%'", "GROUP BY parent_visitor_id ORDER BY visitor_id DESC ");

            if (mysqli_num_rows($q11) > 0) {
                while ($data3 = mysqli_fetch_array($q11)) {
                    $data3 = array_map("html_entity_decode", $data3);

                    $list = array();

             $intTime=$data3['visit_date']." ".$data3['visit_time'];
             $outTime=$data3['exit_date']." ".$data3['exit_time'];
             $date_a = new DateTime($intTime);
             $date_b = new DateTime($outTime);
             $interval = date_diff($date_a,$date_b);
             $totalDays= $interval->d;
                $totalMinutes= $interval->i;
                $totalHours= $interval->h;
                if ($totalDays>0) {
                $list["duration"] = $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                } else {
                    $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                }
          
            
                $visitDate = date("d M Y h:i A", strtotime($data3['visit_date']." ".$data3['visit_time']));
                $exitDate = date("d M Y h:i A", strtotime($data3['exit_date']." ".$data3['exit_time']));

               
                    $list["id"] = $data3['visitor_id'];
                    $list["name"] =$data3['visitor_name'].'';
                    $list["last_in"] = "";
                    $list["visit_from"] ='';
                    $list["visit_from"] =$data3['visit_from'].'';
                    $list["mobile"] = $data3['visitor_mobile'];
                    $list["country_code"] = $data3['country_code'];
                    $list["user_id"] = $data3['user_id'];
                    $list["unit_id"] =$data3['unit_id'];;
                    $list["block_id"] =$data3['block_id'];
                    $list["floor_id"] =$data3['floor_id'];

                    //  $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data3[visitor_sub_type_id]'");
                    // $vistLogo=mysqli_fetch_array($fd);
                     if ($data3['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$data3['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $list["visit_logo"] = $visit_logo.'';

                    if ($visitorMobilePrivacy==0 && $data3['visitor_type']==0  OR  $visitorMobilePrivacy==0 && $data3['visitor_type']==1) {
                    $list["visitor_mobile_new"] = "".substr($data3['visitor_mobile'], 0, 3) . '****' . substr($data3['visitor_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$data3['visitor_mobile'];
                    }
                    $list["des_block"] = $data3['visit_from'].'';
                    $list["visitor_type"] =$data3['visitor_type'];
                    $list["expected_type"] =$data3['expected_type'];
                    $list["user_id"] =$data3['user_id'];
                    $list["visit_date_time"] = $visitDate;
                    $list["visit_exit_date_time"] = $exitDate;
                    $list["qr_code"] = $data3['visitor_id'];
                    $list["type"] = "Exvisitor";
                    $list["profile_img"] = $base_url . "img/visitor/" . $data3['visitor_profile'];
                     $list["profile_img_name"] = $data3['visitor_profile'];
                    if ($data3['visitor_status']==2) {
                        if ($data3['in_temperature']>0) {
                            $in_temperature=$data3['in_temperature'];
                        } else {
                            $in_temperature = "";
                        }
                        $list["temperature"]=$in_temperature.'';
                        $list["with_mask"]=$data3['in_with_mask'].'';
                    } else if ($data3['visitor_status']==3) {
                         if ($data3['out_temperature']>0) {
                            $out_temperature=$data3['out_temperature'];
                        } else {
                            $out_temperature = "";
                        }
                        $list["temperature"]=$out_temperature.'';
                        $list["with_mask"]=$data3['out_with_mask'].'';
                    } else {
                        $list["temperature"]='';
                        $list["with_mask"]='';
                    }
                    $list["re_enter"] = true;
                    $list["visitor_sub_type_id"] = $data3['visitor_sub_type_id'];
                    $list["workig_units"] ="";
                    $list["vehicle_no"] =$data3['vehicle_no'].'';
                    array_push($response["list_common"], $list);
                }
                $temp1 = true;
            } else {
                $temp1 = false;
            }

         if ($access_blocks!="") {
            $q2 = $d->select("unit_master,block_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "( visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND (visitors_master.visitor_status = '3' OR visitors_master.visitor_status = '5' AND visitors_master.exit_date!='' )  AND block_master.block_id IN($access_blocks) AND (visitors_master.visitor_name LIKE '%$search_value%' OR visitors_master.visitor_mobile LIKE '%$search_value%' OR visitors_master.vehicle_no LIKE '%$search_value%') )","GROUP BY visitors_master.parent_visitor_id  ORDER BY visitors_master.exit_date DESC, visitors_master.exit_time DESC");
         } else {

            $q2 = $d->select("unit_master,block_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "(visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND (visitors_master.visitor_status = '3' OR visitors_master.visitor_status = '5' AND visitors_master.exit_date!=''  ) AND  (visitors_master.visitor_name LIKE '%$search_value%' OR visitors_master.visitor_mobile LIKE '%$search_value%' OR visitors_master.vehicle_no LIKE '%$search_value%' ))","GROUP BY visitors_master.parent_visitor_id ORDER BY visitors_master.exit_date DESC, visitors_master.exit_time DESC");
         }

            if (mysqli_num_rows($q2) > 0) {
                while ($data2 = mysqli_fetch_array($q2)) {
                    $data2 = array_map("html_entity_decode", $data2);

                    $list = array();

                $intTime=$data2['visit_date']." ".$data2['visit_time'];
                $outTime=$data2['exit_date']." ".$data2['exit_time'];
                $date_a = new DateTime($intTime);
                $date_b = new DateTime($outTime);
                $interval = date_diff($date_a,$date_b);
                $totalDays= $interval->d;
                $totalMinutes= $interval->i;
                $totalHours= $interval->h;
                if ($totalDays>0) {
                $list["duration"] = $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                } else {
                    $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                }
          
                $visitDate = date("d M Y h:i A", strtotime($data2['visit_date']." ".$data2['visit_time']));
                $exitDate = date("d M Y h:i A", strtotime($data2['exit_date']." ".$data2['exit_time']));

                // $qm=$d->select("users_master,visitors_master","users_master.user_id=visitors_master.user_id AND users_master.society_id ='$society_id' AND visitors_master.parent_visitor_id='$data2[visitor_id]'","ORDER BY users_master.user_id DESC");
                //     $userNameAry=array();

                //     while($userDataMulti=mysqli_fetch_array($qm)){
                //         array_push($userNameAry, $userDataMulti['user_first_name']);
                //     }
                //     $userNameOly= implode(",", $userNameAry);

                    $list["id"] = $data2['visitor_id'];
                    $list["name"] =$data2['visitor_name'].'';
                    $list["last_in"] = "";
                    $list["visit_from"] ='';
                    $list["visit_from"] =$data2['visit_from'].'';
                    $list["mobile"] = $data2['visitor_mobile'];
                    $list["user_id"] = $data2['user_id'];
                    $list["unit_id"] =$data2['unit_id'];;
                    $list["block_id"] =$data2['block_id'];
                    $list["floor_id"] =$data2['floor_id'];
                    
                    // $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data2[visitor_sub_type_id]'");
                    // $vistLogo=mysqli_fetch_array($fd);
                     if ($data2['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$data2['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $list["visit_logo"] = $visit_logo.'';

                    if ($visitorMobilePrivacy==0 && $data2['visitor_type']==0  OR  $visitorMobilePrivacy==0 && $data2['visitor_type']==1) {
                    $list["visitor_mobile_new"] = "".substr($data2['visitor_mobile'], 0, 3) . '****' . substr($data2['visitor_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$data2['visitor_mobile'];
                    }
                    if ($data2['user_id']=='0') {
                        $list["des_block"] = $data2['block_name'] . "-" .  $data2['unit_name'] .' (Empty)';
                    } else  if ($data2['visiting_apartment']!="") {
                        $list["des_block"] = $data2['visiting_apartment'];
                    }else {
                        $list["des_block"] = $data2['block_name'] . "-" .  $data2['unit_name'];
                    }
                    
                    if ($data2['visitor_type']==1 && $data2['expected_type']==1) {
                        $visitor_type = 0;
                    }else if ($data2['visitor_type']==1 && $data2['expected_type']==2) {
                         $visitor_type = 2;
                    }else if ($data2['visitor_type']==1 && $data2['expected_type']==3) {
                         $visitor_type = 3;
                    }else {
                        $visitor_type = $data2['visitor_type'];
                    }
                    $list["visitor_type"] =$visitor_type;
                    $list["user_id"] =$data2['user_id'];
                    $list["expected_type"] =$data2['expected_type'];
                    $list["visit_date_time"] = $visitDate;
                    $list["visit_exit_date_time"] = $exitDate;
                    $list["qr_code"] = $data2['visitor_id'];
                    $list["type"] = "Exvisitor";
                    $list["profile_img"] = $base_url . "img/visitor/" . $data2['visitor_profile'];
                    $list["profile_img_name"] = $data2['visitor_profile'];
                    $list["visitor_sub_type_id"] = $data2['visitor_sub_type_id'];

                    if ($data2['visitor_status']==2) {
                        if ($data2['in_temperature']>0) {
                            $in_temperature=$data2['in_temperature'];
                        } else {
                            $in_temperature = "";
                        }
                        $list["temperature"]=$in_temperature.'';
                        $list["with_mask"]=$data2['in_with_mask'].'';
                    } else if ($data2['visitor_status']==3) {
                         if ($data2['out_temperature']>0) {
                            $out_temperature=$data2['out_temperature'];
                        } else {
                            $out_temperature = "";
                        }
                        $list["temperature"]=$out_temperature.'';
                        $list["with_mask"]=$data2['out_with_mask'].'';
                    } else {
                        $list["temperature"]='';
                        $list["with_mask"]='';
                    }
                        $list["re_enter"] = true;
                   
                    $list["workig_units"] ="";
                    $list["vehicle_no"] =$data2['vehicle_no'].'';

                    array_push($response["list_visitor"], $list);
                }
                $temp2 = true;
            } else {
                $temp2 = false;
            }


            


            if ($temp2 ==true || $temp1 ==true ) {
                $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = ''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getVisitorUsersDetails']=="getVisitorUsersDetails" && $visitor_id!=''){

        $qm=$d->select("users_master,visitors_master,block_master,unit_master","unit_master.block_id=block_master.block_id AND users_master.block_id=block_master.block_id AND unit_master.unit_id=users_master.unit_id AND users_master.user_id=visitors_master.user_id AND users_master.society_id ='$society_id' AND visitors_master.parent_visitor_id='$visitor_id'","ORDER BY users_master.user_id DESC");

            $response["userNameAry"] = array();
           
            
                    while($userDataMulti=mysqli_fetch_array($qm)){
                         $userNameAry=array();
                         $userNameAry["user_id"] =$userDataMulti['user_id'];
                         $userNameAry["user_first_name"] =$userDataMulti['user_first_name'];
                         $userNameAry["unit_name"] =$userDataMulti['block_name'].'-'.$userDataMulti['unit_name'];
                         $userNameAry["user_profile_pic"]=$base_url."img/users/recident_profile/".$userDataMulti['user_profile_pic'];
                        $userNameAry["user_full_name"]=$userDataMulti['user_full_name'];
                        $userNameAry["user_type"]=$userDataMulti['user_type'];
                        $userNameAry["user_status"]=$userDataMulti['user_status'];
                        $userNameAry["unit_status"]=$userDataMulti['unit_status'];
                        $userNameAry["visitor_status"]=$userDataMulti['visitor_status'];
                        $userNameAry["reject_by_id"]=$userDataMulti['reject_by_id'];
                        $userNameAry["approve_by_id"]=$userDataMulti['approve_by_id'];
                        $userNameAry["hold_by_id"]=$userDataMulti['hold_by_id'];
                       
                        array_push($response["userNameAry"],$userNameAry);
                     }
                
                $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);


    } else  if (isset($getVehicleHistory) && $getVehicleHistory == 'getVehicleHistory' && $society_id!='') {

            $response["list_visitor"] = array();
           

                
        $q2 = $d->selectRow("society_parking_master.socieaty_parking_name,parking_master.*,parking_logs_master.*,visitors_master.visiting_apartment","society_parking_master,parking_master,parking_logs_master LEFT JOIN visitors_master on visitors_master.visitor_id=parking_logs_master.parking_visitor_id", "society_parking_master.society_parking_id=parking_master.society_parking_id AND parking_master.parking_id=parking_logs_master.parking_id AND  parking_logs_master.society_id='$society_id' AND parking_logs_master.in_time  >= ( CURDATE() - INTERVAL 10 DAY ) ","ORDER BY parking_logs_master.parking_log_id DESC");
            
             

            if (mysqli_num_rows($q2) > 0) {
                while ($data2 = mysqli_fetch_array($q2)) {
                    $data2 = array_map("html_entity_decode", $data2);
                    $list = array();
                    
                    if ($data2['out_time']!="" && $data2['out_time']!="0000-00-00 00:00:00") {
                    $intTime=$data2['in_time'];
                    $outTime=$data2['out_time'];
                    $date_a = new DateTime($intTime);
                    $date_b = new DateTime($outTime);
                    $interval = date_diff($date_a,$date_b);
                    $totalDays= $interval->d;
                    $totalMinutes= $interval->i;
                    $totalHours= $interval->h;
                    if ($totalDays>0) {
                    $list["duration"] = $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                    } else {
                        $list["duration"] = $interval->h.' hours '. $interval->i.' min';
                    }
                    } else {
                         $list["duration"] = $xml->string->not_available.'';
                    }
               
                    $visitDate = date("d M Y h:i A", strtotime($data2['in_time']));
                    $visitDateFilter = date("d-m-Y", strtotime($data2['in_time']));
                    $exitDate = ($data2['out_time']!=""  && $data2['out_time']!="0000-00-00 00:00:00") ? date("d M Y h:i A", strtotime($data2['out_time'])) :  "Auto Exit"; 


                    $list["parking_log_id"] = $data2['parking_log_id'];
                    $list["guest_name"] =$data2['guest_name'].'';
                    $list["unit_name"] =$data2['visiting_apartment'];
                    $list["entry_type"] ='Guest';
                    $list["vehicle_no"] =$data2['vehicle_no'].'';
                    $list["vehicle_type"] = $data2['vehicle_type'];
                    $list["unit_id"] =$data2['unit_id'];;
                    $list["parking_name"] =$data2['parking_name'].' ('.$data2['socieaty_parking_name'].')';
                   

                    $list["visit_date_time"] = $visitDate;
                    $list["visit_date"] = $visitDateFilter;
                    $list["visit_exit_date_time"] = $exitDate;
                    

                    array_push($response["list_visitor"], $list);
                }
                $response["message"] = ''.$xml->string->getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = ''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 