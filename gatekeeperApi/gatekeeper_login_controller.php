<?php

include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb) {
    
	$response = array();
	extract(array_map("test_input" , $_POST));
    $today = date('Y-m-d');

        if (isset($gatekeeper_login) && $gatekeeper_login=='gatekeeper_login' && isset($emp_mobile) && isset($emp_password)) {

        $emp_mobile=mysqli_real_escape_string($con, $emp_mobile);
        $emp_password=mysqli_real_escape_string($con, $emp_password);

        $q=$d->select("employee_master","emp_mobile ='$emp_mobile' AND emp_password='$emp_password' AND emp_type_id=0 AND society_id='$society_id'");



        $user_data = mysqli_fetch_array($q);

        if($user_data==TRUE && mysqli_num_rows($q) == 1){

            if($user_data['emp_status']=='1' && $user_data['emp_type_id']=='0' ){

            $response["emp_id"]=$user_data['emp_id'];

            $emp_id = $user_data['emp_id'];
            
             $token = $user_data['emp_token'];

            $nGaurd->noti_new("",$token,"Logout","Logout","Logout");

            $m->set_data('emp_token', $emp_token);

            $a = array(
                'emp_token' => $m->get_data('emp_token')
            );

            $d->update("employee_master",$a,"emp_id='$emp_id'");

            $response["society_id"]=$user_data['society_id'];
            $society_id=$user_data['society_id'];

            $response["emp_name"]=$user_data['emp_name'];

            if($user_data['emp_profile']!=""){
                $response["emp_profile"]=$base_url."img/emp/".$user_data['emp_profile'];
            }else{
                $response["emp_profile"]=$base_url."img/emp/default.png";
            }

            $response["emp_mobile"]=$user_data['emp_mobile'];
            $response["emp_email"]=$user_data['emp_email'];
            $response["emp_address"]=$user_data['emp_address'];
            $response["emp_date_of_joing"]=$user_data['emp_date_of_joing'];
            $response["emp_sallary"]=$user_data['emp_sallary'];

            $qSociety = $d->select("society_master","society_id ='$society_id'");

            $user_data_society = mysqli_fetch_array($qSociety);

            if($user_data_society['society_status']=='0'){


                if($user_data_society['plan_expire_date']>$today){

                    $response["society_name"]=html_entity_decode($user_data_society['society_name']);
                    $response["society_address"]=html_entity_decode($user_data_society['society_address']);
                    $response["secretary_email"]=$user_data_society['secretary_email'];
                    $response["secretary_mobile"]=$user_data_society['secretary_mobile'];
                    $response["socieaty_logo"]=$base_url."img/society/".$user_data_society['socieaty_logo'];

                    $response["resident_in_out"]="1";  //0 for Hide 1 for Show
                    $response["message"]=''.$xml->string->loginSucess;
                    $response["status"]="200";
        
                    echo json_encode($response);

                }else{
                    $response["message"]=''.$xml->string->socityPlanExpire;
                    $response["status"]="203";
                    echo json_encode($response);
                }


            }else{
                $response["message"]=''.$xml->string->societyDeactive;
                $response["status"]="204";
                echo json_encode($response);
            }
            
            }else{
                $response["message"]=''.$xml->string->accountNotactivate;
                $response["status"]="201";
                echo json_encode($response);
            }
    
        }else{

            $response["message"]=''.$xml->string->loginFaildwrongIdpass;
            $response["status"]="201";
            echo json_encode($response);
    
        }

    }else if(isset($user_logout) && $user_logout=='user_logout'){

        $response["message"]=''.$xml->string->pleaseContactbuildAdmin;
        $response["status"]="201";
        echo json_encode($response);
        exit();
        
        $m->set_data('emp_token','');
      
        $a = array(
			'emp_token'=>$m->get_data('emp_token')
		);

        $qdelete = $d->update("employee_master",$a,"emp_id='$emp_id'");
        
        if($qdelete==TRUE){
            $response["message"]=''.$xml->string->logout;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]=''.$xml->string->somthingWrong;
            $response["status"]="201";
            echo json_encode($response);
        }
        
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);

    }

    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}

?>