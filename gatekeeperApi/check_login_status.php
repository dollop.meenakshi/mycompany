<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
    $today = date('Y-m-d');
    
        if (isset($check_login_new) && $check_login_new=='check_login_new' && $user_id!='' && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

        $q=$d->select("employee_master,society_master","employee_master.emp_id='$user_id' AND society_master.society_id =employee_master.society_id AND employee_master.emp_status=1");

        if(mysqli_num_rows($q)>0){

            $user_data = mysqli_fetch_array($q);
            
            if($user_data['society_status']=='0'){

                if ($user_data['chat_aceess_for_society']==1) {
                    $chat_aceess_for_society = true;
                 } else {
                    $chat_aceess_for_society = false;
                 }

                if ($user_data['whole_unit_selection']==1) {
                    $whole_unit_selection = true;
                 } else {
                    $whole_unit_selection = false;
                 }

                if ($user_data['direct_parcel_collect']==0) {
                    $direct_parcel_collect = true;
                 } else {
                    $direct_parcel_collect = false;
                 }

                 if ($user_data['direct_child_exit']==0) {
                    $direct_child_exit = true;
                 } else {
                    $direct_child_exit = false;
                 }

                    if($user_token!="" ){

                    $a = array(
                        'device_mac' => $device_mac,
                        'phone_model' => $phone_model,
                        'phone_brand' => $phone_brand,
                        'version_code' => $version_code,
                    );

                    $d->update("employee_master",$a,"emp_id='$user_id'");
                        
                    $response["event_scan"]=$user_data['event_scanner_for_gatekeeper'];  //0 for Hide 1 for Show
                    $response["resident_in_out"]=$user_data['resident_in_out'];  //0 for Hide 1 for Show
                    $response["device_lock"]=$user_data['device_lock'];  //0 for Lock 1 For UnLock
                    $response["access_blocks"]=$user_data['blocks_id']; 
                    $response["mpin"]="9997";
                   
                    $response["startTime"]=$user_data['start_time'];
                    $response["endTime"]=$user_data['end_time'];
                    $response["intervalDuration"]= $user_data['duration']; // 2 Minute
                    if ($user_data['ey_attendace']==0) {
                        $response["eye_attendace"]= false; 
                    } else {
                        $response["eye_attendace"]= true; 
                    }
                    if ($user_data['qr_attendace']==0) {
                        $response["qr_attendace"]= false; 
                    } else {
                        $response["qr_attendace"]= true; 
                    }
                    
                    if ($user_data['add_resource_by_gatekeeper']==1) {
                        $response["add_resource_by_gatekeeper"]= false; 
                    } else {
                        $response["add_resource_by_gatekeeper"]= true; 
                    }


                    if ($user_data['qr_attendace']==1 && $user_data['qr_attendace_self']==0) {
                        $response["qr_attendace_self"]= true; 
                        $response["qr_attendace"]= false; 
                    } else {
                        $response["qr_attendace_self"]= false; 
                    }

                    $response["eye_attempt"]= 1;

                    if ($user_data['mpin']!='' && strlen($user_data['mpin'])==4) {
                        $response["is_lock_mpin_genrated"]= true; 
                    } else {
                        $response["is_lock_mpin_genrated"]= false; 
                    }
                   
                     $response["child_security_menu_hide"]=false;  //0 for Hide 1 for Show

                    // $qnotificationCount=$d->select("guard_notification_master","employee_id='$user_id' AND read_status='0'");
                    $response["read_status"]="0";

                    $toIn=$d->select("visitors_master", "society_id='$society_id' AND `visit_date` >= now() - INTERVAL 30 DAY AND visitor_status=2 AND ( visitor_type=0 OR  visitor_type=1 OR visitor_type=2 OR  visitor_type=3 OR  visitor_type=4 OR visitor_type=5)","GROUP BY visitor_mobile"); 

                    $totalInVisitor=mysqli_num_rows($toIn);

                    $staffIn = $d->select("employee_master,emp_type_master,staff_visit_master", "employee_master.emp_type_id =emp_type_master.emp_type_id AND employee_master.society_id='$society_id' AND staff_visit_master.emp_id = employee_master.emp_id AND staff_visit_master.visit_status ='0' AND employee_master.entry_status = '1'","ORDER BY staff_visit_master.staff_visit_id DESC");
                   
                    $totalInStaff = mysqli_num_rows($staffIn);

                    $staffInToday = $d->select("employee_master,emp_type_master,staff_visit_master", "employee_master.emp_type_id =emp_type_master.emp_type_id AND employee_master.society_id='$society_id' AND staff_visit_master.emp_id = employee_master.emp_id AND staff_visit_master.visit_status ='0' AND employee_master.entry_status = '1' AND staff_visit_master.filter_data='$today'","ORDER BY staff_visit_master.staff_visit_id DESC");
                   
                    $totalInStaffToday = mysqli_num_rows($staffInToday);
         
                    $tOut=$d->select("visitors_master","society_id='$society_id' AND exit_date='$today' AND visitor_status=3","GROUP BY visitor_mobile");
                    
                    $totalInOut=mysqli_num_rows($tOut);

                    $todayIn=$d->select("visitors_master", "visit_date='$today' AND society_id='$society_id' AND visitor_status=2 AND ( visitor_type=0 OR  visitor_type=1 OR visitor_type=2 OR visitor_type=3 OR visitor_type=4 OR  visitor_type=5)","GROUP BY visitor_mobile"); 

                    $totalInVisitorToday=mysqli_num_rows($todayIn);



                    $totalOuttotalInStaff=$d->count_data_direct("staff_visit_id","staff_visit_master","society_id='$society_id' AND DATE(visit_exit_date_time) = CURDATE() AND visit_status=1 AND emp_gate_keeper='0'");
                    if ($access_blocks>0) {
                        $appedBlockAccessQuery = " AND block_master.block_id IN($access_blocks)";
                    }

                    $expectedParcel=$d->select("parcel_collect_master,unit_master,block_master,users_master,visitors_master","
                          visitors_master.parcel_collect_master_id =parcel_collect_master.parcel_collect_master_id and  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.unit_id=unit_master.unit_id AND  parcel_collect_master.society_id='$society_id' AND parcel_collect_master.parcel_status=0  AND   parcel_collect_master.user_id=users_master.user_id $appedBlockAccessQuery ","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 500");

                    $collectedParcel=$d->select("parcel_collect_master,unit_master,block_master,users_master ,employee_master","
                          employee_master.emp_id=parcel_collect_master.collected_by AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.unit_id=unit_master.unit_id AND  parcel_collect_master.society_id='$society_id' AND parcel_collect_master.parcel_status=1  AND   parcel_collect_master.user_id=users_master.user_id $appedBlockAccessQuery ","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 500");

                    $deliverdParcel=$d->select("parcel_collect_master,unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.unit_id=unit_master.unit_id AND  parcel_collect_master.society_id='$society_id' AND parcel_collect_master.parcel_status=2  AND   parcel_collect_master.user_id=users_master.user_id $appedBlockAccessQuery ","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 500");

                    $totalExpectedParcel =  mysqli_num_rows($expectedParcel).'';
                    $collectedParcel =  mysqli_num_rows($collectedParcel).'';
                    $deliverdParcel =  mysqli_num_rows($deliverdParcel).'';

                    $response["visitor_in"]=$totalInVisitor+$totalInStaff."";
                    $response["visitor_in_today"]=$totalInVisitorToday+$totalInStaffToday."";
                    $response["visitor_out"]=$totalInOut+$totalOuttotalInStaff."";
                    $response["hide_chat"]=$chat_aceess_for_society;
                    $response["expected_parcel"]=$totalExpectedParcel;
                    $response["collected_parcel"]=$collectedParcel;
                    $response["deliverd_parcel"]=$deliverdParcel;
                    $response["direct_parcel_collect"]=$direct_parcel_collect;
                    $response["direct_child_exit"]=$direct_child_exit;
                    $response["whole_unit_selection"]=$whole_unit_selection;

                    $qemployee_type=$d->selectRow("emp_type_id,emp_type_name,emp_type_icon","emp_type_master","society_id='$society_id' ","LIMIT 3");


                    $response["employee_Type"] = array();

                    while($data_employee_type=mysqli_fetch_array($qemployee_type)) {

                        $employee_Type = array(); 

                        $employee_Type["emp_type_id"]=$data_employee_type['emp_type_id'];
                        $employee_Type["emp_type_name"]=$data_employee_type['emp_type_name'];
                        $employee_Type["emp_type_icon"]=$base_url."/img/emp_icon/".$data_employee_type['emp_type_icon'];

                        array_push($response["employee_Type"], $employee_Type); 
                    }


                    $appMenu=$d->select("gatekeeper_menu_master","","");


                    $response["app_menu"] = array();

                    while($menuData=mysqli_fetch_array($appMenu)) {

                        $app_menu = array(); 

                        $app_menu["menu_id"]=$menuData['menu_id'];
                        $app_menu["menu_name"]=$menuData['menu_name'];
                        $app_menu["active_status"]=$menuData['active_status'];

                        array_push($response["app_menu"], $app_menu); 
                    }


                     $response["message"]=''.$xml->string->loginSucess;
                    $response["status"]="200";
                    echo json_encode($response);
                        
                    }else{
                        
                    $response["message"]=''.$xml->string->loginAgain;
                    $response["status"]="201";
                    echo json_encode($response);
                    }

            }else{
                $response["message"]=''.$xml->string->$societyDeactive;
                $response["status"]="201";
                echo json_encode($response);
            }

    
        }else{

            $response["message"]=''.$xml->string->noUsermatchFound;
            $response["status"]="201";
            echo json_encode($response);
    
        }

    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);

    }

    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}

?>