    <?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb) { 
    $response = array();
    extract(array_map("test_input" , $_POST));

     if($_POST['getMembersBlockWise']=="getMembersBlockWise" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

           
            $qss=$d->selectRow("hide_user_type,society_type,owner_tenant_view,empty_allow_visitor,owner_unit_colour,tenant_unit_colour,defaulter_unit_colour,closed_unit_colour,empty_unit_colour","society_master"," society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qss); 
            $hide_user_type = $societyData['hide_user_type'];        
            $society_type = $societyData['society_type'];        
            $owner_tenant_view = $societyData['owner_tenant_view'];
            $empty_allow_visitor = $societyData['empty_allow_visitor'];
            $owner_unit_colour = $societyData['owner_unit_colour'];
            $tenant_unit_colour = $societyData['tenant_unit_colour'];
            $defaulter_unit_colour = $societyData['defaulter_unit_colour'];
            $closed_unit_colour = $societyData['closed_unit_colour'];
            $empty_unit_colour = $societyData['empty_unit_colour'];

            if ($owner_tenant_view==1) {
                $ownerUnitCoour = "$owner_unit_colour";
                $closedUnitCoour = "$closed_unit_colour";
                $emptyUnitCoour = "$empty_unit_colour";
                $user_types="occupied,closed,empty";
                $user_types_colour="$owner_unit_colour,$closed_unit_colour,$empty_unit_colour";
                $user_types_values_ios="Occupied,Closed,Empty";
            } else {
                $ownerUnitCoour = "$owner_unit_colour";
                $tenantUnitCoour = "$tenant_unit_colour";
                $defaulterUnitCoour = "$defaulter_unit_colour";
                $closedUnitCoour = "$closed_unit_colour";
                $emptyUnitCoour = "$empty_unit_colour";
                $user_types="owner,tenant,defaulter,closed,empty";
                $user_types_colour="$owner_unit_colour,$tenant_unit_colour,$defaulter_unit_colour,$closed_unit_colour,$empty_unit_colour";
                $user_types_values_ios="Owner,Tenant,Defaulter,Closed,Empty";
            }             

          
            $response["floors"] = array();

            $floor_data=$d->select("floors_master","block_id ='$block_id' ");

              while($data_floor_list=mysqli_fetch_array($floor_data)) {

                    $floors = array(); 

                    $floors["floor_id"]=$data_floor_list['floor_id'];
                    $floors_id=$data_floor_list['floor_id'];


                    $floors["society_id"]=$data_floor_list['society_id'];
                    $floors["block_id"]=$data_floor_list['block_id'];
                    $floors["floor_name"]=$data_floor_list['floor_name'];
                    $floors["floor_status"]=$data_floor_list['floor_status'];


                    $floors["units"] = array();

                    $unit_data=$d->select("unit_master","floor_id ='$floors_id' AND society_id ='$society_id' ");

                    while($data_units_list=mysqli_fetch_array($unit_data)) {
                         
                        $units = array(); 

                        

                        $qu=$d->selectRow("user_type,user_id,user_full_name,user_type,user_status,tenant_view,public_mobile,is_defaulter,delete_status,user_profile_pic,user_mobile","users_master","users_master.delete_status=0 AND unit_id='$data_units_list[unit_id]' AND member_status=0 AND society_id ='$society_id' AND users_master.user_status=1","ORDER BY user_id DESC");
                        $userData=mysqli_fetch_array($qu);
                        $userTypeTemp = $userData['user_type'];
                        
                        $user_full_name= $userData['user_full_name'];

                        $units["unit_id"]=$data_units_list['unit_id'];
                        $units_id=$data_units_list['unit_id'];

                        $units["user_id"]=$userData['user_id'].'';
                        $units["user_full_name"]=$user_full_name;
                        $units["user_type"]=$userData['user_type'].'';
                        $units["user_status"]=$userData['user_status'].'';
                        $units["society_id"]=$data_units_list['society_id'];
                        $units["floor_id"]=$data_units_list['floor_id'];
                        $units["unit_name"]=$data_units_list['unit_name'];
                        $units["unit_status"]=$data_units_list['unit_status'];
                        $units["society_id"]=$data_units_list['society_id'];
                        $units["user_block_id"]=$data_units_list['block_id'];
                        $units["user_floor_id"]=$data_units_list['floor_id'];
                        $units["user_unit_id"]=$data_units_list['unit_id'];
                        $units["tenant_view"]=$userData['tenant_view'].'';
                        $units["public_mobile"]=$userData['public_mobile'].'';
                        $units["user_mobile"] = $userData['user_mobile'];
                        if ($userData['user_profile_pic'] != '') {
                            $units["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $userData['user_profile_pic'];
                        } else {
                            $units["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                        }
                        

                        if ($userData['is_defaulter']==1 && $owner_tenant_view==0) {
                            $unit_colour = $defaulterUnitCoour;
                        } else if ($data_units_list['unit_status']==1 && $userData['is_defaulter']==0) {
                            $unit_colour = $ownerUnitCoour;
                        } else if ($data_units_list['unit_status']==3 && $owner_tenant_view==0 && $userData['is_defaulter']==0) {
                            $unit_colour = $tenantUnitCoour;
                        }else if ($data_units_list['unit_status']==5) {
                            $unit_colour = $closedUnitCoour;
                        }else if ($data_units_list['unit_status']==0 || $data_units_list['unit_status']==4) {
                            $unit_colour = $emptyUnitCoour;
                        } else {
                            $unit_colour = $ownerUnitCoour;
                        }
                               
                               
                        $units["unit_colour"]=$unit_colour;
                        if ($owner_tenant_view==1) {
                            $units["is_defaulter"]='0';
                        } else {
                            $units["is_defaulter"]=$userData['is_defaulter'].'';
                        }

                        $user_id = $userData['user_id'];

                        if ($owner_view_in_tenant_unit==1) {
                            $qfamily = $d->select("users_master", "delete_status=0 AND user_status!=0 AND unit_id='$data_units_list[unit_id]' ","ORDER BY user_type DESC");
                        } else {
                            $qfamily = $d->select("users_master", "delete_status=0 AND user_status!=0 AND unit_id='$data_units_list[unit_id]' AND user_type='$userTypeTemp' ","ORDER BY user_type DESC");
                        }
                        $units["member"] = array();
                        $units["family_count"]=mysqli_num_rows($qfamily)."";
                        while ($datafamily = mysqli_fetch_array($qfamily)) {

                            if ($datafamily['member_status']==0) {
                                if ($datafamily['user_type']==0) {
                                    $mainUser= "Owner Primary";
                                } else {
                                    $mainUser="Tenant Primary ";
                                }
                                $rel= $mainUser;
                            } else {
                                if ($datafamily['user_type']==0) {
                                    $fType="Owner";
                                } else {
                                    $fType="Tenant";
                                }
                                $rel= $datafamily['member_relation_name'].'-'.$fType;
                            }

                            $member = array();
                            $member["user_id"] = $datafamily['user_id'];

                            $qchatCount=$d->select("chat_master","msg_for='$my_id' AND msg_by='$user_id' AND society_id='$society_id' AND msg_status='0'");
                            $units["chat_status"]=mysqli_num_rows($qchatCount)."";
    
                            $member["member_chat"] = mysqli_num_rows($qchatCount)."";
                            $member["user_first_name"] = $datafamily['user_first_name'];
                            $member["user_last_name"] = $datafamily['user_last_name'] .' ('.$rel  .')';
                            $member["only_name"] = $datafamily['user_first_name'] .' ('.$rel  .')';
                            $member["user_mobile"] = $datafamily['user_mobile'];
                            $member["country_code"] = $datafamily['country_code'];
                            $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
                            $member["member_date_of_birth"] = $datafamily['member_date_of_birth'];
                            $member["member_age"] = "";
                            $member["member_relation_name"] = $datafamily['member_relation_name'];
                            $member["public_mobile"] = $datafamily['mobile_for_gatekeeper'];
                            if ($datafamily['user_status']==2) {
                                $member["visitor_approved"] ="0";
                                $member["delivery_cab_approval"]="0";
                                $member["child_gate_approval"]="1";
                            } else {
                                $member["visitor_approved"] = $datafamily['visitor_approved'];
                                $member["delivery_cab_approval"]=$datafamily['Delivery_cab_approval'];
                                $member["child_gate_approval"]=$datafamily['child_gate_approval'];

                            }
                            
                            $member["daily_visitor_approval"]='0';
                            $member["resource_approval"]='0';

                            $member["user_status"] = $datafamily['user_status'];
                            $member["member_status"] = $datafamily['member_status'];

                            array_push($units["member"], $member);

                          }


                        array_push($floors["units"], $units);
                    }
                    array_push($response["floors"], $floors);
                }


          
            
            $response["population_key_name"]= "population";
            $response["population_key_ios_value"]= "Population";

            $response["blocks_key_name"]= "units";
            $response["blocks_key_ios_value"]= "Units";
            
            $response["population"]=  $d->count_data_direct("user_id","users_master","delete_status=0 AND society_id='$society_id' AND user_status!=0");

            $qUnits=$d->select("unit_master,floors_master,block_master",
            "block_master.block_id=floors_master.block_id AND  unit_master.floor_id=floors_master.floor_id
            AND unit_master.society_id='$society_id'");

            $response["block_view"]= mysqli_num_rows($qUnits).'';
            
            
            if ($hide_user_type==1) {
                $response["hide_user_type"]=TRUE;
            } else {
                $response["hide_user_type"]=FALSE;

            }
            

            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);
    
        
    } else if($_POST['getBlocks']=="getBlocks" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

          if ($access_blocks!="") {
                $gatekkeperBlockAccess = " AND block_id IN($access_blocks)";
            }

        $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0 $gatekkeperBlockAccess");

        if(mysqli_num_rows($block_data)>0){

                    

            $user_types=explode(',',$user_types);
            $user_types_values_ios=explode(',',$user_types_values_ios);
            $user_types_colour=explode(',',$user_types_colour);
            $response["member_types"] = array();
            for ($i=0; $i <count($user_types) ; $i++) { 
                $member_types=array();
                $member_types["type_key_name"]=$user_types[$i];
                $member_types["type_key_colour"]=$user_types_colour[$i];
                $member_types["user_types_values_ios"]=$user_types_values_ios[$i];
                array_push($response["member_types"], $member_types); 

            }


            $response["block"] = array();

            
            while($data_block_list=mysqli_fetch_array($block_data)) {

                $block = array();

                $block["block_id"]=$data_block_list['block_id'];
                $block["society_id"]=$data_block_list['society_id'];
                $block["block_name"]=$data_block_list['block_name'];
                $block["block_status"]=$data_block_list['block_status'];
                
                array_push($response["block"], $block); 
            }

            $response["population_key_name"]= "population";
            $response["population_key_ios_value"]= "Population";

            $response["blocks_key_name"]= "units";
            $response["blocks_key_ios_value"]= "Units";
            
            $response["population"]=  $d->count_data_direct("user_id","users_master","delete_status=0 AND society_id='$society_id' AND user_status!=0");

            $qUnits=$d->select("unit_master,floors_master,block_master",
            "block_master.block_id=floors_master.block_id AND  unit_master.floor_id=floors_master.floor_id
            AND unit_master.society_id='$society_id'");

            $response["block_view"]= mysqli_num_rows($qUnits).'';
            
            
            if ($hide_user_type==1) {
                $response["hide_user_type"]=TRUE;
            } else {
                $response["hide_user_type"]=FALSE;

            }

            if ($empty_allow_visitor==1) {
                $response["empty_allow_visitor"]=true;
            } else {
                $response["empty_allow_visitor"]=false;
            }

            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]="$noDatafoundMsg";
            $response["status"]="201";
            echo json_encode($response);
        
    }


}else if($_POST['getFloorandUnit']=="getFloorandUnit" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($block_id, FILTER_VALIDATE_INT) == true ){

        $block_data=$d->select("floors_master","society_id ='$society_id' AND society_id!=0 AND block_id ='$block_id' AND block_id!=0");


        if(mysqli_num_rows($block_data)>0){
            $response["floors"] = array();

            while($data_floor_list=mysqli_fetch_array($block_data)) {

                $floors = array(); 
                $floors["floor_id"]=$data_floor_list['floor_id'];
                $floors_id=$data_floor_list['floor_id'];
                $floors["society_id"]=$data_floor_list['society_id'];
                $floors["block_id"]=$data_floor_list['block_id'];
                $floors["floor_name"]=$data_floor_list['floor_name'];
                $floors["floor_status"]=$data_floor_list['floor_status'];
                
                $floors["units"] = array();

                $unit_data=$d->select("unit_master","floor_id ='$floors_id'");

                while($data_units_list=mysqli_fetch_array($unit_data)) {

                    $units = array(); 

                    $units["unit_id"]=$data_units_list['unit_id'];
                    $unit_id=$data_units_list['unit_id'];

                    $units["society_id"]=$data_units_list['society_id'];
                    $units["floor_id"]=$data_units_list['floor_id'];
                    $units["unit_name"]=$data_units_list['unit_name'];
                    $units["unit_status"]=$data_units_list['unit_status'];
                    $units["unit_type"]=$data_units_list['unit_type'];

                    array_push($floors["units"], $units);


                }

                array_push($block["floors"], $floors);
                array_push($response["floors"], $floors);

            }

            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]="$noDatafoundMsg";
            $response["status"]="201";
            echo json_encode($response);
        
    }


    }else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }
}
else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
?>