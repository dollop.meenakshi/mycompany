
<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb) { 
    $response = array();
    extract(array_map("test_input" , $_POST));

    if($_POST['getViewParkingList']=="getViewParkingList" 
        && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

        $sParking = $d->select("society_parking_master", "society_id='$society_id' and society_parking_status='0'");

            if (mysqli_num_rows($sParking) > 0) {

                $response["sBlocks"] = array();

                while ($s_partking_list = mysqli_fetch_array($sParking)) {

                    $sBlocks = array();


                    $sBlocks["society_parking_id"] = $s_partking_list['society_parking_id'];
                    $sBlocks["socieaty_parking_name"] = $s_partking_list['socieaty_parking_name'];
                    $sBlocks["society_parking_status"] = $s_partking_list['society_parking_status'];

                    $temp_society_parking_id=$s_partking_list[society_parking_id];

                    $qParking = $d->select("parking_master", "society_id='$society_id' and society_parking_id='$temp_society_parking_id'");

                            if (mysqli_num_rows($qParking) > 0) {

                                $sBlocks["sParkingList"] = array();

                                 while ($q_partking_list = mysqli_fetch_array($qParking)) {

                                     $sParkingList = array();
                                     $sParkingList["parking_id"] = $q_partking_list['parking_id'];

                                      if ($q_partking_list['unit_id']!=0) {

                                        $sParkingList["is_unit_parking"]=true;

                                        $temp_unit_id= $q_partking_list[unit_id];


                                        $cqqq=$d->select("unit_master,block_master","unit_master.unit_id='$temp_unit_id' and unit_master.block_id=block_master.block_id");
                                         $unitData=mysqli_fetch_array($cqqq);
                                         $unit_name=$unitData['unit_name'];
                                         $block_name=$unitData['block_name'];
                                         $sParkingList["unit_name"]=$block_name."-".$unit_name;

                                      }else{
                                        $sParkingList["is_unit_parking"]=false;
                                        $sParkingList["unit_name"]="";
                                      }

                                      $sParkingList["society_parking_id"] = $q_partking_list['society_parking_id'];
                                      $sParkingList["parking_id"] = $q_partking_list['parking_id'];
                                      $sParkingList["parking_name"] = $q_partking_list['parking_name'];
                                      $sParkingList["parking_type"] = $q_partking_list['parking_type'];

                                      if ($q_partking_list['parking_type']==0) {
                                        $sParkingList["parking_type_name"]="Car";
                                      }else{
                                        $sParkingList["parking_type_name"]="Bike";
                                      }

                                      
                                      if ($q_partking_list['parking_has_electric_charge_point']==0) {
                                        $sParkingList["has_charge_point"]=false;
                                      }else{
                                        $sParkingList["has_charge_point"]=true;
                                      }

                                      $sParkingList["parking_status"] = $q_partking_list['parking_status'];


                                      if ($q_partking_list['parking_status']==0) {
                                        $sParkingList["parking_status_name"]="Not Allocated";
                                      }else if($q_partking_list['parking_status']==1){
                                        $sParkingList["parking_status_name"]="Allocated";
                                      }else{
                                        $sParkingList["parking_status_name"]="Pending";
                                      }


                                    $sParkingList['member_vehicle_status']=$q_partking_list['member_vehicle_status'];
                                    $sParkingList['visitor_vehicle_status']=$q_partking_list['visitor_vehicle_status'];
                                    $sParkingList['park_to_other']=$q_partking_list['member_to_member_vehicle_status'];


                                     
                                      if ($q_partking_list['member_vehicle_status']==0) {
                                        $sParkingList["member_vehicle_status_name"]="Not parked";
                                      }else{
                                        $sParkingList["member_vehicle_status_name"]="Parked";
                                      }

                                      if ($q_partking_list['visitor_vehicle_status']==0) {
                                        $sParkingList["visitor_vehicle_status_name"]="Not parked";
                                      }else{
                                        $sParkingList["visitor_vehicle_status_name"]="Parked by guest";
                                      }


                                      if ($q_partking_list['member_to_member_vehicle_status']==0) {
                                        $sParkingList["park_to_other_status_name"]="Not parked";
                                      }else{
                                        $sParkingList["park_to_other_status_name"]="Parked by Other Member";
                                      }


                                      $sParkingList["vehicle_no"] = $q_partking_list['vehicle_no'];
                                      $sParkingList["visitor_vehicle_no"] = $q_partking_list['visitor_vehicle_no'];
                                      $sParkingList["visitor"] = $q_partking_list['visitor'];

                                     array_push($sBlocks["sParkingList"], $sParkingList);              
                                 }
                            }

                    array_push($response["sBlocks"], $sBlocks);
                }

                $response["message"] = "$datafoundMsg";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "$noDatafoundMsg";
                $response["status"] = "201";
                echo json_encode($response);
            }


    } else if($_POST['getViewParkingListVisitor']=="getViewParkingListVisitor" 
        && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

        $sParking = $d->select("society_parking_master", "society_id='$society_id' and society_parking_status='0'");

            if (mysqli_num_rows($sParking) > 0) {

                $response["sBlocks"] = array();

                while ($s_partking_list = mysqli_fetch_array($sParking)) {

                    $sBlocks = array();


                    $sBlocks["society_parking_id"] = $s_partking_list['society_parking_id'];
                    $sBlocks["socieaty_parking_name"] = $s_partking_list['socieaty_parking_name'];
                    $sBlocks["society_parking_status"] = $s_partking_list['society_parking_status'];

                    $temp_society_parking_id=$s_partking_list[society_parking_id];

                    $qParking = $d->select("parking_master", "society_id='$society_id' and society_parking_id='$temp_society_parking_id' AND is_visitor_parking=1");

                            if (mysqli_num_rows($qParking) > 0) {

                                $sBlocks["sParkingList"] = array();

                                 while ($q_partking_list = mysqli_fetch_array($qParking)) {

                                     $sParkingList = array();
                                     $sParkingList["parking_id"] = $q_partking_list['parking_id'];

                                      
                                      $sParkingList["is_unit_parking"]=false;
                                      $sParkingList["unit_name"]="";

                                      $sParkingList["society_parking_id"] = $q_partking_list['society_parking_id'];
                                      $sParkingList["parking_id"] = $q_partking_list['parking_id'];
                                      $sParkingList["parking_name"] = $q_partking_list['parking_name'];
                                      $sParkingList["parking_type"] = $q_partking_list['parking_type'];

                                      if ($q_partking_list['parking_type']==0) {
                                        $sParkingList["parking_type_name"]="Car";
                                      }else{
                                        $sParkingList["parking_type_name"]="Bike";
                                      }

                                      
                                      if ($q_partking_list['parking_has_electric_charge_point']==0) {
                                        $sParkingList["has_charge_point"]=false;
                                      }else{
                                        $sParkingList["has_charge_point"]=true;
                                      }

                                      $sParkingList["parking_status"] = $q_partking_list['parking_status'];


                                      if ($q_partking_list['parking_status']==0) {
                                        $sParkingList["parking_status_name"]="Not Allocated";
                                      }else if($q_partking_list['parking_status']==1){
                                        $sParkingList["parking_status_name"]="Allocated";
                                      }else{
                                        $sParkingList["parking_status_name"]="Pending";
                                      }


                                      $sParkingList['member_vehicle_status']=$q_partking_list['member_vehicle_status'];
                                      $sParkingList['visitor_vehicle_status']=$q_partking_list['visitor_vehicle_status'];
                                      $sParkingList['park_to_other']=$q_partking_list['member_to_member_vehicle_status'];

                                     
                                      if ($q_partking_list['member_vehicle_status']==0) {
                                        $sParkingList["member_vehicle_status_name"]="Not parked";
                                      }else{
                                        $sParkingList["member_vehicle_status_name"]="Parked";
                                      }

                                      if ($q_partking_list['visitor_vehicle_status']==0) {
                                        $sParkingList["visitor_vehicle_status_name"]="Not parked";
                                      }else{
                                        $sParkingList["visitor_vehicle_status_name"]="Parked by guest";
                                      }


                                      if ($q_partking_list['member_to_member_vehicle_status']==0) {
                                        $sParkingList["park_to_other_status_name"]="Not parked";
                                      }else{
                                        $sParkingList["park_to_other_status_name"]="Parked by Other Member";
                                      }


                                      $sParkingList["vehicle_no"] = $q_partking_list['vehicle_no'];
                                      $sParkingList["visitor_vehicle_no"] = $q_partking_list['visitor_vehicle_no'];
                                      $sParkingList["visitor"] = $q_partking_list['visitor'];

                                     array_push($sBlocks["sParkingList"], $sParkingList);              
                                 }
                            }

                    array_push($response["sBlocks"], $sBlocks);
                }

                $response["message"] = "$datafoundMsg";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "$noDatafoundMsg";
                $response["status"] = "201";
                echo json_encode($response);
            }


    }else if($_POST['checkParkingAvailablity']=="checkParkingAvailablity" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true ){

        $q=$d->select("parking_settings_master","society_id='$society_id'");
        $psData = mysqli_fetch_array($q);
        $maximum_bike_allow_per_car_slot = $psData['maximum_bike_allow_per_car_slot'];
        if ($maximum_bike_allow_per_car_slot=="") {
          $maximum_bike_allow_per_car_slot =2;
        }
        

        $qnotification=$d->selectRow("parking_master.*,society_parking_master.socieaty_parking_name","society_parking_master,parking_master","society_parking_master.society_parking_id=parking_master.society_parking_id AND parking_master.unit_id!='0' AND parking_master.parking_status=1 AND (parking_master.unit_id='$unit_id' AND parking_on_rent=0 OR parking_master.renter_unit_id='$unit_id' AND renter_unit_id!=0 AND parking_on_rent=1)");

        $totalCarParking = 0;
        $totalCar = 0;
        $totalBikeParking = 0;
        $totalBike = 0;

        if(mysqli_num_rows($qnotification)>0){
            $response["myParking"] = array();

            while($data_notification=mysqli_fetch_array($qnotification)) {

                $myParking = array(); 

                if ($data_notification['parking_type']==0) {
                    $totalCarParking += 1;
                } else {
                    $totalBikeParking += 1;
                }

                $myParking["parking_id"]=$data_notification['parking_id'];
                $myParking["sociaty_id"]=$data_notification['society_id'];
                $myParking["parking_name"]=html_entity_decode($data_notification['socieaty_parking_name'].' - '.$data_notification['parking_name']);
                $myParking["parking_type"]=$data_notification['parking_type'];
                $myParking["parking_status"]=$data_notification['parking_status'];

                array_push($response["myParking"], $myParking); 
            }

            

        }


        $qVehi=$d->select("users_vehicle_master,users_master","users_vehicle_master.active_status=0 AND users_vehicle_master.unit_id='$unit_id'  AND users_vehicle_master.society_id='$society_id'  AND users_master.user_id=users_vehicle_master.user_id AND users_master.active_status=0 AND users_vehicle_master.in_out_status=1");

        if (mysqli_num_rows($qVehi)>0) {
            $response["vechiles"] = array();

            while ($datafamily = mysqli_fetch_array($qVehi)) {
                
                if ($datafamily['vehicle_type']==0) {
                    $totalCar += 1;
                } else {
                    $totalBike += 1;
                }

                $vechiles = array();
                $vechiles["users_vehicle_id"] = $datafamily['users_vehicle_id'];
                $vechiles["user_id"] = $datafamily['user_id'];
                $vechiles["unit_id"] = $datafamily['unit_id'];
                $vechiles["user_type"] = $datafamily['user_type'];
                $vechiles["vehicle_type"] = $datafamily['vehicle_type'];
                $vechiles["vehicle_number"] = $datafamily['vehicle_number'];
                $vechiles["vehicle_name"] = $datafamily['vehicle_name'];
                $vechiles["in_out_status"] = $datafamily['in_out_status'];
                
                if ($datafamily['vehicle_photo']!="") {
                    $vechiles["vehicle_photo"] = $base_url . "img/society/".$datafamily['vehicle_photo'];
                } else {
                    $vechiles["vehicle_photo"] = "";
                }

                array_push($response["vechiles"], $vechiles);
            }
            
        }

        if (mysqli_num_rows($qnotification)>0 || mysqli_num_rows($qVehi)>0) {
            
            $allow_entry = true;

            if ($vehicle_type==0) {
                $totalTempBikeIn = ($totalBikeParking-$totalBike)+$totalCarParking;
                if ($totalCarParking<=$totalCar || $totalTempBikeIn<1) {
                  $allow_entry = false;
                }
            } else if ($vehicle_type==1) {
                $totalSlots = ($totalCarParking*$maximum_bike_allow_per_car_slot)+$totalBikeParking;
                // check in bike pakring
                if ($totalBikeParking<=$totalBike && $totalSlots<=$totalBike ) {
                  $allow_entry = false;
                } 
            } 
            
            
            $response["allow_entry"]=$allow_entry;
            $response["totalCar"]=$totalCar;
            $response["totalBike"]=$totalBike;
            $response["totalCarParking"]=$totalCarParking;
            $response["totalBikeParking"]=$totalBikeParking;
            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);
        } else{

            $response["message"]="$noDatafoundMsg";
            $response["status"]="201";
            echo json_encode($response);
        }


    }else if($_POST['getNotParkedVehicleList']=="getNotParkedVehicleList" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                         
                    $qParking = $d->selectRow("unit_master.unit_id,block_master.block_id,unit_master.floor_id,block_master.society_id,unit_master.unit_status,block_master.block_name,unit_master.unit_name,users_vehicle_master.vehicle_photo,users_vehicle_master.vehicle_name,users_vehicle_master.users_vehicle_id,users_vehicle_master.vehicle_number,users_vehicle_master.vehicle_type","unit_master,block_master,users_vehicle_master","unit_master.block_id=block_master.block_id AND  users_vehicle_master.vehicle_number!='' AND users_vehicle_master.in_out_status=0 AND users_vehicle_master.active_status=0 AND unit_master.unit_id=users_vehicle_master.unit_id");

            
                            if (mysqli_num_rows($qParking) > 0) {

                                  $response["sParkingList"] = array();

                                 while ($q_partking_list = mysqli_fetch_array($qParking)) {

                                 
                                     $sParkingList = array();
                                     $sParkingList["society_id"] = $q_partking_list['society_id'];
                                     $sParkingList["block_id"] = $q_partking_list['block_id'];
                                     $sParkingList["floor_id"] = $q_partking_list['floor_id'];
                                     $sParkingList["unit_id"] = $q_partking_list['unit_id'];

                                      if ($q_partking_list['vehicle_photo']!="") {
                                        $sParkingList["vehicle_photo"] = $base_url . "img/society/".$q_partking_list['vehicle_photo'];
                                      } else {
                                        $sParkingList["vehicle_photo"] = "";
                                      }

                                        
                                       $unit_name=$q_partking_list['unit_name'];
                                       $block_name=$q_partking_list['block_name'];
                                       $sParkingList["unit_name"]=$block_name."-".$unit_name;

                                       if ($q_partking_list["unit_status"]==1) {
                                       $sParkingList["UnitType"]="Owner";
                                       }else if ($q_partking_list["unit_status"]==3) {
                                           
                                       $sParkingList["UnitType"]="Tenant";
                                       }else{
                                            $sParkingList["UnitType"]="";
                                       }

                                      $sParkingList["society_parking_id"] = $q_partking_list['society_parking_id'];
                                      $sParkingList["parking_id"] = $q_partking_list['parking_id'];
                                      $sParkingList["parking_name"] = $q_partking_list['socieaty_parking_name']." | ".$q_partking_list['parking_name'];
                                      
                                      if ($q_partking_list['vehicle_type']==0) {
                                        $sParkingList["parking_type"]= $q_partking_list['vehicle_type'];
                                        $sParkingList["parking_type_name"]="Car";
                                      }else{
                                        $sParkingList["parking_type_name"]="Bike";
                                        $sParkingList["parking_type"]= $q_partking_list['vehicle_type'];
                                      }

                                      $sParkingList["parking_status"] = $q_partking_list['parking_status'];

                                      $sParkingList["member_vehicle_status_name"]="Park";
                                      $sParkingList["vehicle_no"] = $q_partking_list['vehicle_number'];
                                      $sParkingList["vehicle_name"] = $q_partking_list['vehicle_name'];

                                     array_push($response["sParkingList"], $sParkingList);              
                                 }

                                    $response["message"] = "$noDatafoundMsg";
                                    $response["status"] = "200";
                                    echo json_encode($response);

                            }else{

                                    $response["message"] = "$noDatafoundMsg";
                                    $response["status"] = "201";
                                    echo json_encode($response);
                            }
    }else if($_POST['getParkedVehicleList']=="getParkedVehicleList" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
                      

                       $qParking = $d->selectRow("parking_logs_master.parking_log_id,parking_logs_master.in_time,unit_master.unit_id,block_master.block_id,unit_master.floor_id,block_master.society_id,unit_master.unit_status,block_master.block_name,unit_master.unit_name,users_vehicle_master.vehicle_photo,users_vehicle_master.vehicle_name,users_vehicle_master.users_vehicle_id,users_vehicle_master.vehicle_number,users_vehicle_master.vehicle_type","unit_master,block_master,users_vehicle_master,parking_logs_master","parking_logs_master.vehicle_no=users_vehicle_master.vehicle_number AND unit_master.block_id=block_master.block_id AND  users_vehicle_master.vehicle_number!='' AND users_vehicle_master.in_out_status=1 AND users_vehicle_master.active_status=0 AND unit_master.unit_id=users_vehicle_master.unit_id","GROUP BY parking_logs_master.vehicle_no ORDER BY parking_logs_master.parking_log_id DESC");


                   

            
                            if (mysqli_num_rows($qParking) > 0) {

                                  $response["sParkingList"] = array();

                                 while ($q_partking_list = mysqli_fetch_array($qParking)) {

                                 
                                     $sParkingList = array();
                                     $sParkingList["parking_log_id"]=$q_partking_list['parking_log_id'];

                                      

                                        $sParkingList["is_unit_parking"]=true;

                                        $temp_unit_id= $q_partking_list[unit_id];


                                        $cqqq=$d->select("unit_master,block_master","unit_master.unit_id='$temp_unit_id' and unit_master.block_id=block_master.block_id");
                                         $unitData=mysqli_fetch_array($cqqq);
                                         $unit_name=$unitData['unit_name'];
                                         $block_name=$unitData['block_name'];
                                         $sParkingList["unit_name"]=$block_name."-".$unit_name;
                                         if ($unitData["unit_status"]==1) {
                                         $sParkingList["UnitType"]="Owner";
                                         }else if ($unitData["unit_status"]==3) {
                                             
                                         $sParkingList["UnitType"]="Tenant";
                                         }else{
                                              $sParkingList["UnitType"]="";
                                         }

                                     

                                      if ($q_partking_list['vehicle_photo']!="") {
                                        $sParkingList["vehicle_photo"] = $base_url . "img/society/".$q_partking_list['vehicle_photo'];
                                      } else {
                                        $sParkingList["vehicle_photo"] = "";
                                      }


                                      if ($q_partking_list['vehicle_type']==0) {
                                        $sParkingList["parking_type"]= $q_partking_list['vehicle_type'];
                                        $sParkingList["parking_type_name"]="Car";
                                      }else{
                                        $sParkingList["parking_type_name"]="Bike";
                                        $sParkingList["parking_type"]= $q_partking_list['vehicle_type'];
                                      }



                                      $sParkingList["vehicle_no"] = $q_partking_list['vehicle_number'];
                                      
                                      $sParkingList["in_time"] =date("d M Y h:i A", strtotime($q_partking_list['in_time']));

                                       
                                        $intTime=$q_partking_list['in_time'];
                                        $outTime=date("Y-m-d h:i:s a");
                                        
                                        $date_a = new DateTime($intTime);
                                        $date_b = new DateTime($outTime);
                                        
                                        $interval = date_diff($date_a,$date_b);

                                        $totalDays= $interval->d;
                                        $totalMinutes= $interval->i;
                                        $totalHours= $interval->h;

                                        if ($totalDays>0) {
                                        $sParkingList["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                                        } else {
                                        $sParkingList["duration"] = $interval->h.' hours '. $interval->i.' min';
                                        }
                                        
                                        $diffSeconds = strtotime($outTime) - strtotime($intTime);
                                        $sParkingList["duration_sec"] = $diffSeconds;


                                         $sParkingList["vehicle_name"] = $q_partking_list['vehicle_name'];
                                        
                                      
                                     array_push($response["sParkingList"], $sParkingList);              
                                 }

                                    $response["message"] = "$noDatafoundMsg";
                                    $response["status"] = "200";
                                    echo json_encode($response);

                            }else{

                                    $response["message"] = "$noDatafoundMsg";
                                    $response["status"] = "201";
                                    echo json_encode($response);
                            }
    }else if($_POST['assignParkingToVisitor']=="assignParkingToVisitor" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
 


             $today=date("Y-m-d H:i:s");
             
             $vehicle_no=strtoupper($vehicle_no);
      
             $m->set_data('society_parking_id',$society_parking_id);
             $m->set_data('society_id',$society_id);
             $m->set_data('parking_id',$parking_id);
             $m->set_data('block_id',0);
             $m->set_data('floor_id',0);
             $m->set_data('unit_id',0);
             $m->set_data('entry_by',$entry_by);
             $m->set_data('guest_name',$unit_name);
             $m->set_data('guest_mobile',$guest_mobile);
             $m->set_data('is_valet_parked', $is_valet_parked);
             $m->set_data('vehicle_no', $vehicle_no);
             $m->set_data('vehicle_type',$vehicle_type);
             $m->set_data('in_time',$today);
             $m->set_data('visitor_vehicle_status','1');
             $m->set_data('visitor_vehicle_no',$vehicle_no);


              $a2= array (
               'society_id'=>$m->get_data('society_id'),
              'visitor_vehicle_status'=> $m->get_data('visitor_vehicle_status'),
              'is_valet_parking'=> $m->get_data('is_valet_parking'),
              'visitor_vehicle_no'=> $m->get_data('visitor_vehicle_no'));

            $q1=$d->update("parking_master",$a2,"parking_id='$parking_id'");



        
             $a1= array (
              'society_id'=> $m->get_data('society_id'),
              'parking_id'=> $m->get_data('parking_id'),
              'unit_id'=> $m->get_data('unit_id'),
              'guest_name'=> $m->get_data('guest_name'),
              'guest_mobile'=> $m->get_data('guest_mobile'),
              'is_valet_parked'=> $m->get_data('is_valet_parked'),
              'vehicle_no'=> $m->get_data('vehicle_no'),
              'vehicle_type'=> $m->get_data('vehicle_type'),
              'in_time'=> $m->get_data('in_time'),
              'entry_by'=>$m->get_data('entry_by'),
          
          );


            $q=$d->insert("parking_logs_master",$a1);
             
            

       if ($q1==true && $q==true) {

             $response["message"] = "Done";
             $response["status"] = "200";
             echo json_encode($response);
         }else{
            $response["message"]="Something went wrong.!";
        $response["status"]="201";
        echo json_encode($response);

         }

}else if($_POST['assignParkingToMember']=="assignParkingToMember" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
 


            $today=date("Y-m-d H:i:s");
             
      
             $m->set_data('society_parking_id',$society_parking_id);
             $m->set_data('society_id',$society_id);
             $m->set_data('block_id',$block_id);
             $m->set_data('floor_id',$floor_id);
             $m->set_data('unit_id',$unit_id);
             $m->set_data('entry_by',$entry_by);
             $m->set_data('vehicle_no', $vehicle_no);
             $m->set_data('in_time',$today);
             $m->set_data('in_out_status','1');



            
             $a2= array ( 
               'in_out_status'=>$m->get_data('in_out_status')
             );
                                        
              
            $q1=$d->update("users_vehicle_master",$a2,"vehicle_number='$vehicle_no'");



        
             $a1= array (
              'society_id'=> $m->get_data('society_id'),
              'unit_id'=> $m->get_data('unit_id'),
              'vehicle_no'=> $m->get_data('vehicle_no'),
              'vehicle_type'=> $m->get_data('vehicle_type'),
              'in_time'=> $m->get_data('in_time'),
              'entry_by'=>$m->get_data('entry_by'),
          );


            $q=$d->insert("parking_logs_master",$a1);
             
            

       if ($q1==true && $q==true) {

             $response["message"] = "Enter Successfully";
             $response["status"] = "200";
             echo json_encode($response);
         }else{
            $response["message"]="Something went wrong.!";
        $response["status"]="201";
        echo json_encode($response);

         }

  } else if($_POST['exitVehicle']=="exitVehicle" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
 

        $today=date("Y-m-d H:i:s");
             
    
             $m->set_data('exit_by',$exit_by);
             $m->set_data('out_time',$today);
             $m->set_data('park_vehicle_status',"0");
             $m->set_data('parked_vehicle_no',"");
             $m->set_data('member_vehicle_status',$member_vehicle_status);
             $m->set_data('visitor_vehicle_status',$visitor_vehicle_status);
             


              $a2= array (
               'exit_by'=>$m->get_data('exit_by'),
              'out_time'=> $m->get_data('out_time')
            );

            $q1=$d->update("parking_logs_master",$a2,"parking_log_id='$parking_log_id'");

        
             $a1= array ( 
               'in_out_status'=>0
             );

            $q=$d->update("users_vehicle_master",$a1,"vehicle_number='$vehicle_no'");
             

       if ($q1==true && $q==true) {

             $response["message"] = "Exit Successfully";
             $response["status"] = "200";
             echo json_encode($response);
         }else{
            $response["message"]="Something went wrong.!";
        $response["status"]="201";
        echo json_encode($response);

         }

}else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }
}
else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
?>
