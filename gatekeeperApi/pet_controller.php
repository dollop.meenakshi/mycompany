<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

	if ($key == $keydb) {
		$please_enter_valid_mobile_number = $xml->string->please_enter_valid_mobile_number;
		$response = array();
		extract(array_map("test_input", $_POST));

		$tempDate = date('Y-m-d');

		if (isset($getPet) && $getPet == 'getPet' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($emp_id, FILTER_VALIDATE_INT) == true  ) {

			
			
			$qfamily=$d->select("users_master,block_master,unit_master,pet_master","users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND users_master.user_id=pet_master.user_id AND users_master.delete_status=0 AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=pet_master.unit_id AND pet_master.active_status=0 AND pet_master.society_id='$society_id' ","ORDER BY pet_master.pet_name ASC");

			if (mysqli_num_rows($qfamily)) {
				$response["pet"] = array();

				while ($datafamily = mysqli_fetch_array($qfamily)) {
					

					$pet = array();
					$pet["pet_id"] = $datafamily['pet_id'];
					$pet["user_id"] = $datafamily['user_id'];
					$pet["user_full_name"] = $datafamily['user_first_name'].' '.$datafamily['user_last_name'];
					$pet["user_mobile"] = $datafamily['country_code'].' '.$datafamily['user_mobile'];
					$pet["public_mobile"] = $datafamily['mobile_for_gatekeeper'];
					if ($datafamily['mobile_for_gatekeeper']==1) {
						$pet["user_mobile_view"] = $datafamily['country_code'].' '.substr($datafamily['user_mobile'], 0, 3) . '****' . substr($datafamily['user_mobile'],  -3);
					} else {
						$pet["user_mobile_view"] = $datafamily['country_code'].' '.$datafamily['user_mobile'];
					}
					
					if ($datafamily['user_profile_pic']!="") {
						$pet["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
					} else {
						$pet["user_profile_pic"] = "";
					}

					if ($datafamily['user_type']==0) {
                        $fType="Owner";
                    } else {
                        $fType="Tenant";
                    }

					$pet["user_type"] = $fType;
					$pet["pet_name"] = $datafamily['pet_name'];
					$pet["unit_name"] = $datafamily['block_name'].'-'.$datafamily['unit_name'];
					$pet["pet_gender"] = $datafamily['pet_gender'];
					if ($datafamily['pet_vactionation_date']!="0000-00-00") {
						$pet["pet_vactionation_date"] = $datafamily['pet_vactionation_date'].'';
					} else {
						$pet["pet_vactionation_date"] = '';
					}
					
					if ($datafamily['pet_vactionation_doc']!="" ) {
						$pet["pet_vactionation_doc"] = $base_url . "img/documents/".$datafamily['pet_vactionation_doc'];
					} else {
						$pet["pet_vactionation_doc"] = "";
					}

					$pet["pet_type"] = $datafamily['pet_type'];
					$pet["created_date"] = $datafamily['created_date'];
					$pet["pent_photo"] = $base_url . "img/users/recident_profile/" . $datafamily['pent_photo'];
					$today= date('Y-m-d');
			        if ($today>$datafamily['pet_vactionation_date'] && $datafamily['pet_vactionation_date']!="0000-00-00" && $datafamily['pet_vactionation_doc']!="") {
			            $vactionation_msg = "Vaccination Expried";
			        } else if ($datafamily['pet_vactionation_doc']!="") {
			        	$vactionation_msg = "Vaccinated";
			        } else {
			        	 $vactionation_msg = "";
			        }
			        $pet["vactionation_msg"] = $vactionation_msg;

					array_push($response["pet"], $pet);
				}
				$response["message"] = "$datafoundMsg";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "$noDatafoundMsg";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else {
			$response["message"] = "wrong tag. !";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {

		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}
