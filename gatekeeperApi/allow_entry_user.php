<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {


    if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d H:i:s");
    $temFilter = date("Y-m-d");
    $temFiltertime = date("H:i:s");

        if ($_POST['allowOut'] == "allowOut" && $user_id!='') {

            $m->set_data('society_id', $society_id);
            $m->set_data('block_id', $block_id);
            $m->set_data('unit_id', $unit_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('out_date', $temFilter);
            $m->set_data('out_time', $temFiltertime);
              $m->set_data('in_temperature', $in_temperature);
            $m->set_data('in_with_mask', $in_with_mask);

            $m->set_data('out_temperature', $out_temperature);
            $m->set_data('out_with_mask', $out_with_mask);
           

            $a1 = array(
                'in_out_status'=>$in_out_status,
            );
                if ($in_out_status==1) {
                    // for out
                     $a = array(
                        'society_id'=>$m->get_data('society_id'),
                        'block_id'=>$m->get_data('block_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'out_date' => $m->get_data('out_date'),
                        'out_time' => $m->get_data('out_time'),
                        'out_temperature' => $m->get_data('out_temperature'),
                        'out_with_mask' => $m->get_data('out_with_mask'),
                    );
                    $q = $d->insert("user_in_out_master", $a);
                    $response["message"] =''.$xml->string->userOutsucess;
                } else {
                    // for in  0
                    $a = array(
                        'society_id'=>$m->get_data('society_id'),
                        'block_id'=>$m->get_data('block_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'in_date' => $m->get_data('out_date'),
                        'in_time' => $m->get_data('out_time'),
                        'in_temperature' => $m->get_data('in_temperature'),
                        'in_with_mask' => $m->get_data('in_with_mask'),
                    );
                    $q = $d->update("user_in_out_master", $a,"user_id='$user_id'  ORDER BY visitor_id DESC ");
                    $response["message"] = ''.$xml->string->userInsucess;
                }

            if ($q == true) {
                $d->update("users_master",$a1,"user_id='$user_id'");
                // $response["message"] = "User Out Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->failed;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getUsersAll'] == "getUsersAll" && $society_id!='') {

            $qe=$d->select("employee_master","emp_id='$my_id' AND society_id='$society_id'");
            $row11=mysqli_fetch_array($qe);
            $blocks_id = $row11['blocks_id'];
            
            if ($blocks_id!="") {
                // $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0 AND block_id IN($blocks_id)");
                $qfamily = $d->select("users_master,block_master,unit_master", "users_master.delete_status=0 AND block_master.block_id=users_master.block_id AND unit_master.unit_id=users_master.unit_id AND users_master.society_id='$society_id' AND block_master.block_id IN($blocks_id) AND users_master.user_status!=0","ORDER BY users_master.user_first_name ASC");
            } else {
                $qfamily = $d->select("users_master,block_master,unit_master", "users_master.delete_status=0 AND block_master.block_id=users_master.block_id AND unit_master.unit_id=users_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_status!=0","ORDER BY users_master.user_first_name ASC");
            }

            if (mysqli_num_rows($qfamily) > 0) {
                $response["member"] = array();
                while ($datafamily = mysqli_fetch_array($qfamily)) {

                    $member = array();
                    $member["user_id"] = $datafamily['user_id'];
                    $member["block_id"] = $datafamily['block_id'];
                    $member["unit_id"] = $datafamily['unit_id'];
                    $member["user_first_name"] = $datafamily['user_first_name'];
                    $member["user_last_name"] = $datafamily['user_last_name'];
                    $member["user_mobile"] = $datafamily['user_mobile'];
                    $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
                    $member["block_name"] = $datafamily['block_name'].'-'.$datafamily['unit_name'];
                    $member["user_status"] = $datafamily['user_status'];
                    $member["member_status"] = $datafamily['member_status'];
                    $member["in_out_status"] = $datafamily['in_out_status'];

                    $qv=$d->select("user_in_out_master","user_id='$datafamily[user_id]' ","ORDER BY visitor_id DESC");
                    $visitData=mysqli_fetch_array($qv);
                    if ($datafamily['in_out_status']=='1') {
                        $member["visit_time"] =  date("d M Y h:i A", strtotime($visitData['out_date'].' '.$visitData['out_time']));
                        $intTime=$visitData['out_date']." ".$visitData['out_time'];
                        $outTime=$temDate;
                        $date_a = new DateTime($intTime);
                        $date_b = new DateTime($outTime);
                        $interval = date_diff($date_a,$date_b);
                        // $list["duration"] = $interval->format('%d %h:%i:%s');
                        $totalDays= $interval->d;
                        $totalMinutes= $interval->i;
                        $totalHours= $interval->h;
                        if ($totalDays>0) {
                        $member["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                        } else {
                            $member["duration"] = $interval->h.' hours '. $interval->i.' min';
                        }
                    } else if ($datafamily['in_out_status']=='0' && $visitData['in_date']!='') {
                        $member["visit_time"] =date("d M Y h:i A", strtotime($visitData['in_date'].' '.$visitData['in_time']));
                         $intTime=$visitData['in_date']." ".$visitData['in_time'];
                        $outTime=$temDate;
                        $date_a = new DateTime($intTime);
                        $date_b = new DateTime($outTime);
                        $interval = date_diff($date_a,$date_b);
                        // $list["duration"] = $interval->format('%d %h:%i:%s');
                        $totalDays= $interval->d;
                        $totalMinutes= $interval->i;
                        $totalHours= $interval->h;
                        if ($totalDays>0) {
                        $member["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                        } else {
                            $member["duration"] = $interval->h.' hours '. $interval->i.' min';
                        }
                    }else {
                        $member["visit_time"] ="No Data Available";
                        $member["duration"] = "";
                    }



                    array_push($response["member"], $member);

                  }
                 $response["message"] = ''.$xml->string->getMembersucess;
                 $response["status"] = "200";
                 echo json_encode($response);

            } else {

                $response["message"] = ''.$xml->string->noMemberfound;
                $response["status"] = "201";
                echo json_encode($response);
            }    

        } else if ($_POST['getUsersAllUnitWsie'] == "getUsersAllUnitWsie" && $society_id!='') {

           
            
            if ($access_blocks!="") {

                // $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0 AND block_id IN($blocks_id)");
                $qC = $d->select("block_master,unit_master", "unit_master.society_id='$society_id' AND block_master.block_id IN($access_blocks) AND block_master.block_id=unit_master.block_id AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  ","ORDER BY unit_master.unit_id ASC");
            } else {
                $qC = $d->select("block_master,unit_master", "unit_master.society_id='$society_id' AND block_master.block_id=unit_master.block_id AND unit_master.unit_status!=0 AND unit_master.unit_status!=4","ORDER BY unit_master.unit_id ASC");
            }

            if (mysqli_num_rows($qC) > 0) {
                    $response["units"] = array();

                    while($data_units_list=mysqli_fetch_array($qC)) {

                        $units = array(); 

                        $units["unit_id"]=$data_units_list['unit_id'];
                        $units_id=$data_units_list['unit_id'];
                        $units["society_id"]=$data_units_list['society_id'];
                        $units["floor_id"]=$data_units_list['floor_id'];
                        $units["unit_name"]=$data_units_list['block_name'].'-'.$data_units_list['unit_name'];
                        if ($data_units_list['unit_status']==5 || $data_units_list['unit_close_for_gatekeeper']==1) {
                            $unit_status= 5;
                        } else {
                            $unit_status= $data_units_list['unit_status'];
                        }
                        
                        $units["unit_status"]=$unit_status;
                        $units["unit_type"]=$data_units_list['unit_type'].'';


                        $quser_data=$d->select("users_master","delete_status=0 AND unit_id ='$units_id' AND member_status!=1 AND user_status=1"," ORDER BY user_id DESC");

                        $user_data=mysqli_fetch_array($quser_data);
                        $user_full_name= $user_data['user_full_name'];

                        $units["user_id"]=$user_data['user_id'].'';
                        $units["about_business"]=$user_data['about_business'];
                        $units["public_mobile"]=$user_data['mobile_for_gatekeeper'];
                        $units["child_gate_approval"]=$user_data['child_gate_approval'];

                        $user_id = $user_data['user_id'];


                        $qfamily = $d->select("users_master", "delete_status=0 AND user_status!=0 AND unit_id='$data_units_list[unit_id]' ","ORDER BY user_id DESC");
                        $units["member"] = array();
                        $units["family_count"]=mysqli_num_rows($qfamily)."";
                        while ($datafamily = mysqli_fetch_array($qfamily)) {

                            if ($datafamily['member_status']==0) {
                                if ($datafamily['user_type']==0) {
                                    $mainUser= "Owner Primary";
                                } else {
                                    $mainUser="Tenant Primary ";
                                }
                                $rel= $mainUser;
                            } else {
                                if ($datafamily['user_type']==0) {
                                    $fType="Owner";
                                } else {
                                    $fType="Tenant";
                                }
                                $rel= $datafamily['member_relation_name'].'-'.$fType;
                            }

                            $member = array();
                            $member["user_id"] = $datafamily['user_id'];

                            $qchatCount=$d->select("chat_master","msg_for='$my_id' AND msg_by='$user_id' AND society_id='$society_id' AND msg_status='0'");
                            $units["chat_status"]=mysqli_num_rows($qchatCount)."";
    
                            $member["member_chat"] = mysqli_num_rows($qchatCount)."";
                            $member["user_first_name"] = $datafamily['user_first_name'];
                            $member["user_last_name"] = $datafamily['user_last_name'] .' ('.$rel  .')';
                            $member["only_name"] = $datafamily['user_first_name'] .' ('.$rel  .')';
                            $member["user_mobile"] = $datafamily['user_mobile'];
                            $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
                            $member["member_date_of_birth"] = $datafamily['member_date_of_birth'];
                            $member["member_age"] = "";
                            $member["member_relation_name"] = $datafamily['member_relation_name'];
                            $member["public_mobile"] = $datafamily['mobile_for_gatekeeper'];
                            if ($datafamily['user_status']==2) {
                                $member["visitor_approved"] ="0";
                                $member["delivery_cab_approval"]="0";
                                $member["child_gate_approval"]="1";
                            } else {
                                $member["visitor_approved"] = $datafamily['visitor_approved'];
                                $member["delivery_cab_approval"]=$datafamily['Delivery_cab_approval'];
                                $member["child_gate_approval"]=$datafamily['child_gate_approval'];

                            }
                            
                            $member["daily_visitor_approval"]='0';
                            $member["resource_approval"]='0';

                            $member["user_status"] = $datafamily['user_status'];
                            $member["member_status"] = $datafamily['member_status'];
                            $member["in_out_status"] = $datafamily['in_out_status'];

                            $qv=$d->select("user_in_out_master","user_id='$datafamily[user_id]' ","ORDER BY visitor_id DESC");
                            $visitData=mysqli_fetch_array($qv);
                            if ($datafamily['in_out_status']=='1') {
                                $member["visit_time"] =  date("d M Y h:i A", strtotime($visitData['out_date'].' '.$visitData['out_time']));
                                $intTime=$visitData['out_date']." ".$visitData['out_time'];
                                $outTime=$temDate;
                                $date_a = new DateTime($intTime);
                                $date_b = new DateTime($outTime);
                                $interval = date_diff($date_a,$date_b);
                                // $list["duration"] = $interval->format('%d %h:%i:%s');
                                $totalDays= $interval->d;
                                $totalMinutes= $interval->i;
                                $totalHours= $interval->h;
                                if ($totalDays>0) {
                                $member["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                                } else {
                                    $member["duration"] = $interval->h.' hours '. $interval->i.' min';
                                }
                            } else if ($datafamily['in_out_status']=='0' && $visitData['in_date']!='') {
                                $member["visit_time"] =date("d M Y h:i A", strtotime($visitData['in_date'].' '.$visitData['in_time']));
                                 $intTime=$visitData['in_date']." ".$visitData['in_time'];
                                $outTime=$temDate;
                                $date_a = new DateTime($intTime);
                                $date_b = new DateTime($outTime);
                                $interval = date_diff($date_a,$date_b);
                                // $list["duration"] = $interval->format('%d %h:%i:%s');
                                $totalDays= $interval->d;
                                $totalMinutes= $interval->i;
                                $totalHours= $interval->h;
                                if ($totalDays>0) {
                                $member["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                                } else {
                                    $member["duration"] = $interval->h.' hours '. $interval->i.' min';
                                }
                            }else {
                                $member["visit_time"] ="No Data Available";
                                $member["duration"] = "";
                            }

                            array_push($units["member"], $member);

                          }



                        $units["society_id"]=$user_data['society_id'];
                        $units["user_profile_pic"]=$base_url."img/users/recident_profile/".$user_data['user_profile_pic'];
                        $units["user_full_name"]=$user_full_name;
                        $units["user_first_name"]=$user_data['user_first_name'];
                        $units["user_last_name"]=$user_data['user_last_name'];
                        $units["user_mobile"]=$user_data['user_mobile'];
                        $units["user_email"]=$user_data['user_email'];
                        $units["user_id_proof"]=$user_data['user_id_proof'];
                        $units["user_type"]=$user_data['user_type'];
                        $units["user_block_id"]=$user_data['block_id'];
                        $units["user_floor_id"]=$user_data['floor_id'];
                        $units["user_unit_id"]=$user_data['unit_id'];
                        $units["user_status"]=$user_data['user_status'];


                        array_push($response["units"], $units);


                    }

                 $response["message"] = ''.$xml->string->getMembersucess;
                 $response["status"] = "200";
                 echo json_encode($response);

            } else {

                $response["message"] = ''.$xml->string->noMemberfound;
                $response["status"] = "201";
                echo json_encode($response);
            }    

        }  else if ($_POST['getUsersAllInOutNew'] == "getUsersAllInOutNew" && $society_id!='') {

            $response["member"]=array();
            
            if ($block_id!="" && $block_id>0) {
                $appendBlockQuery = " AND users_master.block_id='$block_id'";
            }

            if ($user_id!="" && $user_id>0) {
                $singleUserQuery = " AND users_master.user_id='$user_id'";
            }

            if ($user_mobile!="" && $user_mobile>0) {
                $singleUserQueryMobile = " AND users_master.user_mobile='$user_mobile'";
            }

            if ($in_out_status!="") {
                 $inOutFilterQuery = " AND users_master.in_out_status='$in_out_status'";
            }

           

            if ($access_blocks!="") {
                $gatekkeperBlockAccess = " AND block_master.block_id IN($access_blocks)";
            } 

            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status!=0 $gatekkeperBlockAccess $appendBlockQuery $singleUserQuery $inOutFilterQuery $singleUserQueryMobile","ORDER BY users_master.user_first_name ASC");
            


            if (mysqli_num_rows($q3)>0) {

                    while($data=mysqli_fetch_array($q3)) {

                    
                    $userId= $data['user_id'];

                    if ($data['member_status']==0) {
                        if ($data['user_type']==0) {
                            $mainUser= "Owner";
                        } else {
                            $mainUser="Tenant ";
                        }
                        $rel= $mainUser;
                    } else {
                        if ($data['user_type']==0) {
                            $fType="Owner";
                        } else {
                            $fType="Tenant";
                        }
                        $rel= $data['member_relation_name'].'-'.$fType;
                    }

                    $member = array(); 
                    $member["user_id"]=$data['user_id'];
                   
                    $member["user_full_name"]=$data['user_full_name'];
                    $member["user_first_name"]=$data['user_first_name'];
                    $member["relation"]=$rel;
                    $member["gender"]=$data['gender'];
                    $member["gender"]=$data['gender'];
                    $member["user_type"]=$data['user_type'];
                    $member["block_name"]=$data['block_name'];
                    $member["floor_name"]=$data['floor_name'];
                    $member["unit_name"]=$data['unit_name'];
                    $member["block_id"]=$data['block_id'];
                    $member["floor_id"]=$data['floor_id'];
                    $member["unit_id"]=$data['unit_id'];
                    $member["unit_status"]=$data['unit_status'];
                    $member["user_status"]=$data['user_status'];
                    $member["member_status"]=$data['member_status'];
                    $member["token"] = $data['user_token'];
                    $member["in_out_status"] = $data['in_out_status'];

                    $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                    
                    if ($data['in_out_status']=='1') {
                        $qv=$d->select("user_in_out_master","user_id='$userId' ","ORDER BY visitor_id DESC");
                        $visitData=mysqli_fetch_array($qv);
                        $member["visit_time"] =  date("d M Y h:i A", strtotime($visitData['out_date'].' '.$visitData['out_time']));
                        $intTime=$visitData['out_date']." ".$visitData['out_time'];
                        $outTime=$temDate;
                        $date_a = new DateTime($intTime);
                        $date_b = new DateTime($outTime);
                        $interval = date_diff($date_a,$date_b);
                        // $list["duration"] = $interval->format('%d %h:%i:%s');
                        $totalDays= $interval->d;
                        $totalMinutes= $interval->i;
                        $totalHours= $interval->h;
                        if ($totalDays>0) {
                        $member["duration"] = $interval->days.' days '. $interval->h.' hours '. $interval->i.' min';
                        } else {
                            $member["duration"] = $interval->h.' hours '. $interval->i.' min';
                        }
                    } else {
                        $member["visit_time"] ="No Data Available";
                        $member["duration"] = "";
                    }
                    
                         array_push($response["member"], $member); 
                    }

                   
                $response["message"] =''.$xml->string->getMemberlistSucess;
                $response["status"] = "200";
                echo json_encode($response);
             } else {
                $response["message"] =''.$xml->string->noDatafound;
                $response["status"] = "201";
                echo json_encode($response);
            }


        }else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }  else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 