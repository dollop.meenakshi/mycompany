<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {



    if ($key == $keydb) {
    $CURRENT_TIMESTAMP=date('Y-m-d H:i:s');
    $today = date('Y-m-d');

    $response = array();
    extract(array_map("test_input" , $_POST));
    if (isset($society_id)) {
        $qss=$d->selectRow("visitor_mobile_number_show_gatekeeper,auto_reject_vistor_minutes,visitor_otp_verify","society_master"," society_id ='$society_id'");
        $societyData = mysqli_fetch_array($qss); 
        $visitorMobilePrivacy = $societyData['visitor_mobile_number_show_gatekeeper'];  
        $auto_reject_vistor_minutes= $societyData['auto_reject_vistor_minutes'];      
        $visitor_otp_verify= $societyData['visitor_otp_verify'];      
    }

        if ($_POST['addNewVisitorCommonEntry'] == "addNewVisitorCommonEntry"  && $society_id!='') {

            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }
            
            $qq=$d->select("visitors_master","visitor_mobile='$visitor_mobile' AND visitor_status=0 AND society_id='$society_id' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=1 AND society_id='$society_id' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=2 AND society_id='$society_id'  $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=6 AND society_id='$society_id' $appendQuery ");
            if (mysqli_num_rows($qq)>0) {
                $oldData=mysqli_fetch_array($qq);
                if ($oldData['visitor_type']==1 && $oldData['visitor_status']==0) {
                    $response["message"] = ''.$xml->string->mobileNumberaddedExpectedvisitorList;
                } else if ($oldData['visitor_type']==4) {
                    $response["message"] =''.$xml->string->mobileNumberisAlradyaddDailyvisitorList;
                } else if ($oldData['visitor_status']==2) {
                    $response["message"] =''.$xml->string->mobileNumberisEnterinBuilding;
                } else {
                    $response["message"] = ''.$xml->string->mobileAlradyRegister;
                }
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $visit_date = date("Y-m-d");
            $visit_time = date("H:i:s");
            if ($visitor_profile_photo != "") {
                $ddd = date("ymdhi");
                $profile_name = "Visitor_" . $visitor_mobile .$ddd. '.png';
                define('UPLOAD_DIR', '../img/visitor/');
                $img = $visitor_profile_photo;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $profile_name;
                $success = file_put_contents($file, $data);

                $notyUrl= $base_url.'img/visitor/'.$profile_name;
            } else {
                $profile_name = "visitor_default.png";
                $notyUrl='';
            }
            
            $qSociety = $d->select("society_master","society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qSociety);
            $society_name = $societyData['society_name'];
            $digits = 4;
            $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
            $m->set_data('otp', $otp);

            $visitor_name = html_entity_decode($visitor_name);

                $m->set_data('parent_visitor_id', $visitor_id);
                $m->set_data('society_id', $society_id);
                $m->set_data('floor_id', 0);
                $m->set_data('block_id',0);
                $m->set_data('unit_id', 0);
                $m->set_data('user_id', 0);
                $m->set_data('emp_id', 0);
                $m->set_data('visitor_type', 5);
                $m->set_data('visit_from', $visit_from);
                $m->set_data('visitor_name', $visitor_name);
                $m->set_data('visitor_mobile', $visitor_mobile);
                $m->set_data('country_code', $country_code);
                $m->set_data('number_of_visitor',1);
                $m->set_data('visit_date', $visit_date);
                $m->set_data('visit_time', $visit_time);
                $m->set_data('visitor_status', '1');
                $m->set_data('visiting_reason', $visiting_reason);
                $m->set_data('vehicle_no', $vehicle_no);
                $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
                $m->set_data('otp', $otp);

                

                $a = array(
                    'society_id' => $m->get_data('society_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'block_id' => $m->get_data('block_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'user_id' => $m->get_data('user_id'),
                    'emp_id' => $m->get_data('emp_id'),
                    'visitor_type' => $m->get_data('visitor_type'),
                    'visit_from' => $m->get_data('visit_from'),
                    'visitor_name' => $m->get_data('visitor_name'),
                    'visitor_mobile' => $m->get_data('visitor_mobile'),
                    'country_code' => $m->get_data('country_code'),
                    'visitor_profile' => $profile_name,
                    'number_of_visitor' => $m->get_data('number_of_visitor'),
                    'visit_date' => $m->get_data('visit_date'),
                    'visit_time' => $m->get_data('visit_time'),
                    'visitor_status' => $m->get_data('visitor_status'),
                    'visiting_reason' => $m->get_data('visiting_reason'),
                    'vehicle_no'=>$m->get_data('vehicle_no'),
                    'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                    'otp'=>$m->get_data('otp'),
                    'entry_time'=>date("Y-m-d H:i:s"),
                );


                $q = $d->insert("visitors_master", $a);
                $parent_visitor_id = $con->insert_id;

                if ($q>0) {
                     $aPrnt = array(
                    'parent_visitor_id' => $parent_visitor_id,
                    );
                     $d->update("visitors_master", $aPrnt,"visitor_id='$parent_visitor_id'");
                     if ($societyData['visitor_otp_verify']==1) {
                        
                        if ($country_code=='') {
                              $country_code = "+91";
                          }
                        $smsObj->send_ongate_visitor_otp($society_id,$visitor_mobile,$visitor_name,$society_name,$otp,$country_code);
                        $d->add_sms_log($visitor_mobile,"On Gate Visitor OTP SMS",$society_id,$country_code,1);  
                    }

                    $response["parent_visitor_id"] =''.$xml->string->addVisitorSucess;
                    $response["message"] =''.$xml->string->addVisitorSucess;
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();

                } else {
                    $response["message"] =''.$xml->string->somthingWrong;
                    $response["status"] = "2001";
                    echo json_encode($response);
                    exit();
                }


        } else if ($_POST['addNewVisitorGuest'] == "addNewVisitorGuest"  && $society_id!='') {

            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }


            $visit_date = date("Y-m-d");
            $visit_time = date("H:i:s");
             $visitor_name = html_entity_decode($visitor_name);

            $qSociety = $d->select("society_master","society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qSociety);
            $society_name = $societyData['society_name'];
            $digits = 4;
            $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
            $m->set_data('otp', $otp);
            
            $qq=$d->select("visitors_master","visitor_mobile='$visitor_mobile' AND visitor_status=0 AND society_id='$society_id'  $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=1 AND society_id='$society_id' AND visitors_master.visit_date='$visit_date'AND visitor_status!='3'  $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=2 AND society_id='$society_id'  AND visitor_status!='3'  $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=6 AND society_id='$society_id' AND visitor_status!='3'  $appendQuery");
            if (mysqli_num_rows($qq)>0) {
                $oldData=mysqli_fetch_array($qq);
                if ($oldData['visitor_type']==1 && $oldData['visitor_status']==1) {
                    $response["message"] = ''.$xml->string->mobileNumberaddedExpectedvisitorList;
                } else if ($oldData['visitor_type']==4) {
                    $response["message"] = ''.$xml->string->mobileNumberisAlradyaddDailyvisitorList;
                } else if ($oldData['visitor_status']==2) {
                    $response["message"] = ''.$xml->string->mobileNumberisEnterinBuilding;
                } else {
                    $response["message"] = ''.$xml->string->mobileAlradyRegister;
                }
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            if ($isReinvite=='true' && $visitor_profile_photo=='') {
                $profile_name = $visitor_profile_photo_name;
                $notyUrl= $base_url.'img/visitor/'.$profile_name;

            } else {
                if ($visitor_profile_photo != "") {
                    $ddd = date("ymdhi");
                    $profile_name = "Visitor_" . $visitor_mobile .$ddd. '.jpeg';
                    define('UPLOAD_DIR', '../img/visitor/');
                    $img = $visitor_profile_photo;
                    $img = str_replace('data: img/app/png;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);
                    $file = UPLOAD_DIR . $profile_name;
                    $success = file_put_contents($file, $data);

                    $notyUrl= $base_url.'img/visitor/'.$profile_name;
                } else {
                    $profile_name = "visitor_default.png";
                    $notyUrl= $base_url.'img/visitor/visitor_default.png';
                }
            }

            $userIdAry=explode(",",$user_id);

            $userIdNonApporvalReq= array();
            $userIdApporvalReq= array();
            $unitIdApporvalReq= array();
            for ($i=0; $i <count($userIdAry) ; $i++) { 
                $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$userIdAry[$i]'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $deliveryApproval=$data_notification['visitor_approved'];
                if ($deliveryApproval==0) {
                    array_push($userIdNonApporvalReq, $data_notification['user_id']);
                } else {
                    array_push($userIdApporvalReq, $data_notification['user_id']);
                    array_push($unitIdApporvalReq, $data_notification['unit_id']);
                }

            }

            $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$visitor_sub_type_id'");
            $vistLogo=mysqli_fetch_array($fd);
             if ($vistLogo['visitor_sub_image']!='') {
                    $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                } else {
                    $visit_logo="";
                }



           

            if ($number_of_visitor==0) {
                $number_of_visitor =1;
            }
            $globleVariblePrntId = "";

            for ($i=0; $i <count($userIdNonApporvalReq) ; $i++) { 
                $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$userIdNonApporvalReq[$i]'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $visitor_approved=$data_notification['visitor_approved'];
                $deliveryApproval=$data_notification['Delivery_cab_approval'];
                $sos_user_token=$data_notification['user_token'];
                $device=$data_notification['device'];
                $user_mobile=$data_notification['user_mobile'];

                $visit_time = date("H:i:s");

                $m->set_data('parent_visitor_id', $visitor_id);
                $m->set_data('society_id', $society_id);
                if ($data_notification['floor_id']=='') {
                    $m->set_data('floor_id', $floor_id);
                    $m->set_data('block_id', $block_id);
                    $m->set_data('unit_id', $unit_id);
                    $m->set_data('visitor_type', 5);
                } else {
                    $m->set_data('floor_id', $data_notification['floor_id']);
                    $m->set_data('block_id', $data_notification['block_id']);
                    $m->set_data('unit_id', $data_notification['unit_id']);
                    $m->set_data('visitor_type', $visitor_type);
                }
                $m->set_data('user_id', $userIdNonApporvalReq[$i]);
                $m->set_data('emp_id', $emp_id);
                $m->set_data('visit_from', $visit_from);
                $m->set_data('visitor_name', $visitor_name);
                $m->set_data('visitor_mobile', $visitor_mobile);
                $m->set_data('country_code', $country_code);
                $m->set_data('number_of_visitor', $number_of_visitor);
                $m->set_data('visit_date', $visit_date);
                $m->set_data('visit_time', $visit_time);
                
                $m->set_data('visitor_status', '1');
                $m->set_data('visiting_reason', $visiting_reason);
                $m->set_data('qr_code_master_id', $qrCode);
                $m->set_data('vehicle_no', $vehicle_no);
                $m->set_data('parking_id', $parking_id);
                $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
                $m->set_data('visiting_apartment', $block_unit_name);
                $m->set_data('approve_by_id', $userIdNonApporvalReq[$i]);

                

                $a = array(
                    'parent_visitor_id' => $m->get_data('parent_visitor_id'),
                    'society_id' => $m->get_data('society_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'block_id' => $m->get_data('block_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'user_id' => $m->get_data('user_id'),
                    'emp_id' => $m->get_data('emp_id'),
                    'visitor_type' => $m->get_data('visitor_type'),
                    'visit_from' => $m->get_data('visit_from'),
                    'visitor_name' => $m->get_data('visitor_name'),
                    'visitor_mobile' => $m->get_data('visitor_mobile'),
                    'country_code' => $m->get_data('country_code'),
                    'visitor_profile' => $profile_name,
                    'number_of_visitor' => $m->get_data('number_of_visitor'),
                    'visit_date' => $m->get_data('visit_date'),
                    'visit_time' => $m->get_data('visit_time'),
                    'visitor_status' => $m->get_data('visitor_status'),
                    'otp' => $m->get_data('otp'),
                    'visiting_reason' => $m->get_data('visiting_reason'),
                    'qr_code_master_id'=>$m->get_data('qr_code_master_id'),
                    'vehicle_no'=>$m->get_data('vehicle_no'),
                    'parking_id'=>$m->get_data('parking_id'),
                    'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                    'entry_time'=>date("Y-m-d H:i:s"),
                    'visiting_apartment'=>$m->get_data('visiting_apartment'),
                    'approve_by_id'=>$m->get_data('approve_by_id'),
                );


                $quserDataUpdate = $d->insert("visitors_master", $a);
                $parent_visitor_id = $con->insert_id;
                $visitor_id = $parent_visitor_id;
                if ($i==0) {
                    $aPrt = array(
                        'parent_visitor_id' => $parent_visitor_id,
                    );
                    $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                   $globleVariblePrntId=$parent_visitor_id;
                } else {
                    $aPrt = array(
                        'parent_visitor_id' => $globleVariblePrntId,
                    );
                    $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                }

                if ($quserDataUpdate == true) {

                    if($flag == true){

                        $m->set_data('parking_id', $parking_id);
                        $m->set_data('vehicle_no', $vehicle_no);
                        // $m->set_data('parking_status', '1');
                        $m->set_data('visitor', $visitor_id);
                        $m->set_data('vehicle_type', $vehicle_type);

                        $a2 = array('parking_type' => $m->get_data('vehicle_type'),
                        // 'parking_status' => $m->get_data('parking_status'),
                        'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                        'visitor' => $m->get_data('visitor'));

                        $d->update("parking_master",$a2,"parking_id='$parking_id'");

                    }

                    if ($visitor_approved==0 && $visitor_type==0) {
                        $notTitle= "New Visitor $visitor_name Arrived at Gate";
                        $notDesc= "Will meet you very soon."; 
                        if ($device=='android') {
                           $nResident->noti("VisitorFragment","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                        }  else if($device=='ios') {
                           $nResident->noti_ios("VisitorVC","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                        }
                   } else {
                        $notTitle= "New Visitor $visitor_name Arrived at Gate"; 
                        $notDesc= "$visitor_name visitor is waiting for your approval.";
                        if ($device=='android') {
                           $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,$notTitle,$notDesc,"newVisitorApprove~$visitor_id~$visitor_name");
                        }  else if($device=='ios') {
                           $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token,$notTitle,$notDesc,"newVisitorApprove~$visitor_id~$visitor_name");
                        }
                   }

                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$userIdNonApporvalReq[$i],
                        'notification_title'=>$notTitle,
                        'notification_desc'=>$notDesc,
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'Visitor',
                        'notification_logo'=>'visitorsNew.png',
                      );
                      
                    $d->insert("user_notification",$notiAry);
                } 
            } //for loop end

            //  Apporval  req

            $tempPrntIdAry=array();
            $tempUnitIdAry=array();

            for ($i=0; $i <count($userIdApporvalReq) ; $i++) { 

               
                $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$userIdApporvalReq[$i]'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $visitor_approved=$data_notification['visitor_approved'];
                $deliveryApproval=$data_notification['Delivery_cab_approval'];
                $sos_user_token=$data_notification['user_token'];
                $block_id=$data_notification['block_id'];
                $user_full_name=$data_notification['user_full_name'];
                $device=$data_notification['device'];
                $user_mobile=$data_notification['user_mobile'];
                $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];


               
                    $visitor_name= $visitor_name.'';

                    $visit_time = date("H:i:s");

                    $m->set_data('parent_visitor_id', $visitorId);
                    $m->set_data('society_id', $society_id);
                    $m->set_data('floor_id', $data_notification['floor_id']);
                    $m->set_data('block_id', $data_notification['block_id']);
                    $m->set_data('unit_id', $data_notification['unit_id']);
                    $m->set_data('user_id', $userIdApporvalReq[$i]);
                    $m->set_data('emp_id', $emp_id);
                    $m->set_data('visitor_type', $visitor_type);
                    $m->set_data('visit_from', $visit_from);
                    $m->set_data('visitor_name', $visitor_name);
                    $m->set_data('visitor_mobile', $visitor_mobile);
                    $m->set_data('country_code', $country_code);
                    $m->set_data('number_of_visitor', $number_of_visitor);
                    $m->set_data('visit_date', $visit_date);
                    $m->set_data('visit_time', $visit_time);
                    $m->set_data('visitor_status', '0');
                    $m->set_data('visiting_reason', $visiting_reason);
                    $m->set_data('qr_code_master_id', $qrCode);
                    $m->set_data('vehicle_no', $vehicle_no);
                    $m->set_data('parking_id', $parking_id);
                    $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
                    $m->set_data('visiting_apartment', $block_unit_name);
                    

                    $a = array(
                        // 'parent_visitor_id' => $m->get_data('parent_visitor_id'),
                        'society_id' => $m->get_data('society_id'),
                        'floor_id' => $m->get_data('floor_id'),
                        'block_id' => $m->get_data('block_id'),
                        'unit_id' => $m->get_data('unit_id'),
                        'user_id' => $m->get_data('user_id'),
                        'emp_id' => $m->get_data('emp_id'),
                        'visitor_type' => $m->get_data('visitor_type'),
                        'visit_from' => $m->get_data('visit_from'),
                        'visitor_name' => $m->get_data('visitor_name'),
                        'visitor_mobile' => $m->get_data('visitor_mobile'),
                        'country_code' => $m->get_data('country_code'),
                        'visitor_profile' => $profile_name,
                        'number_of_visitor' => $m->get_data('number_of_visitor'),
                        'visit_date' => $m->get_data('visit_date'),
                        'visit_time' => $m->get_data('visit_time'),
                        'visitor_status' => $m->get_data('visitor_status'),
                        'otp' => $m->get_data('otp'),
                        'visiting_reason' => $m->get_data('visiting_reason'),
                        'qr_code_master_id'=>$m->get_data('qr_code_master_id'),
                        'vehicle_no'=>$m->get_data('vehicle_no'),
                        'parking_id'=>$m->get_data('parking_id'),
                        'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                        'entry_time'=>date("Y-m-d H:i:s"),
                        'visiting_apartment'=>$m->get_data('visiting_apartment'),
                    );


                    $quserDataUpdate = $d->insert("visitors_master", $a);
                    $parent_visitor_id = $con->insert_id;
                    $visitorId = $parent_visitor_id;
                    if ($i==0) {
                        $aPrt = array(
                            'parent_visitor_id' => $parent_visitor_id,
                        );
                        $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                       $globleVariblePrntId=$parent_visitor_id;
                    } else {
                        $aPrt = array(
                            'parent_visitor_id' => $globleVariblePrntId,
                        );
                        $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                    }

                    if ($quserDataUpdate == true) {

                        if($flag == true){

                            $m->set_data('parking_id', $parking_id);
                            $m->set_data('vehicle_no', $vehicle_no);
                            // $m->set_data('parking_status', '1');
                            $m->set_data('visitor', $visitorId);
                            $m->set_data('vehicle_type', $vehicle_type);

                            $a2 = array('parking_type' => $m->get_data('vehicle_type'),
                            // 'parking_status' => $m->get_data('parking_status'),
                            'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                            'visitor' => $m->get_data('visitor'));

                            $d->update("parking_master",$a2,"parking_id='$parking_id'");

                        }

                        $visit_time11= date("d M Y h:i A", strtotime($visit_date.' '.$visit_time));

                         $clickAray = array(
                            'title' => "newVisitorApprove",
                            'visitor_id' => "$visitorId",
                            'visitor_name' => $visitor_name,
                            'visitor_profile' => $notyUrl,
                            'visitor_type' => $visitor_type,
                            'visitor_mobile' => $visitor_mobile,
                            'visit_from' => $visit_from,
                            'visit_logo' => $visit_logo,
                            'visit_time' => $visit_time11,
                            'unit_name' => $unit_name,
                            'user_id' => $userIdApporvalReq[$i],
                            'user_mobile'=>$user_mobile,
                            'block_id' => $block_id,
                            'user_name' => $user_full_name,
                            'base_url' => $base_url,
                        );

                       if ($visitor_approved==0 && $visitor_type==0) {
                            $notTitle= "Visitor $unit_name Arrived at Gate";
                            $notDesc= "$visitor_name will meet you very soon."; 
                            if ($device=='android') {
                               $nResident->noti("VisitorFragment","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                            }  else if($device=='ios') {
                               $nResident->noti_ios("VisitorVC","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                            }
                       } else {
                            $notTitle= "Visitor $unit_name Arrived at Gate"; 
                            $notDesc= "$visitor_name is waiting for your approval.";
                            if ($device=='android') {
                               $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,$notTitle,$notDesc,$clickAray);
                            }  else if($device=='ios') {
                               $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token,$notTitle,$notDesc,$clickAray);
                            }
                       }
                   
                         $notiAry = array(
                            'society_id'=>$society_id,
                            'user_id'=>$userIdApporvalReq[$i],
                            'notification_title'=>$notTitle,
                            'notification_desc'=>$notDesc,
                            'notification_date'=>date('Y-m-d H:i'),
                            'notification_action'=>'Visitor',
                            'notification_logo'=>'visitorsNew.png',
                          );
                          
                        $d->insert("user_notification",$notiAry);
                    } 
                

                
            } //for loop end

            if ($societyData['visitor_otp_verify']==1) {
                
                if ($country_code=='') {
                              $country_code = "+91";
                          }
                $smsObj->send_ongate_visitor_otp($society_id,$visitor_mobile,$visitor_name,$society_name,$otp,$country_code);
                $d->add_sms_log($visitor_mobile,"On Gate Visitor OTP SMS",$society_id,$country_code,1);
            }

            $response["message"] =''.$xml->string->addVisitorSucess ;
            $response["status"] = "200";
            echo json_encode($response);



        } else if ($_POST['addNewVisitorCabDelivery'] == "addNewVisitorCabDelivery"  && $society_id!='') {

            if ($country_code!="") {
                $appendQuery = " AND country_code='$country_code'";
            }
            
            
            $visitor_name = html_entity_decode($visitor_name);
            $qq=$d->select("visitors_master","visitor_mobile='$visitor_mobile' AND visitor_status=0 AND society_id='$society_id' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=1 AND society_id='$society_id' AND visitors_master.visit_date='$visit_date'AND visitor_status!='3' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=2 AND society_id='$society_id'  AND visitor_status!='3' $appendQuery OR visitor_mobile='$visitor_mobile' AND visitor_status=6 AND society_id='$society_id' AND visitor_status!='3' $appendQuery");
            if (mysqli_num_rows($qq)>0) {
                $oldData=mysqli_fetch_array($qq);
                if ($oldData['visitor_type']==1 && $oldData['visitor_status']==1) {
                    $response["message"] = ''.$xml->string->mobileNumberaddedExpectedvisitorList;
                } else if ($oldData['visitor_type']==4) {
                    $response["message"] = ''.$xml->string->mobileNumberisAlradyaddDailyvisitorList;
                } else if ($oldData['visitor_status']==2) {
                    $response["message"] = ''.$xml->string->mobileNumberisEnterinBuilding;
                } else {
                    $response["message"] = ''.$xml->string->mobileAlradyRegister;
                }
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }


            $qSociety = $d->selectRow("society_name,visitor_otp_verify","society_master","society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qSociety);
            $society_name = $societyData['society_name'];
            $digits = 4;
            $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
            $m->set_data('otp', $otp);


            $visit_date = date("Y-m-d");
            $visit_time = date("H:i:s");
            if ($visitor_profile_photo != "") {
                $ddd = date("ymdhi");
                $profile_name = "Visitor_" . $visitor_mobile.$ddd . '.png';
                define('UPLOAD_DIR', '../img/visitor/');
                $img = $visitor_profile_photo;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $profile_name;
                $success = file_put_contents($file, $data);

                $notyUrl= $base_url.'img/visitor/'.$profile_name;
            } else {
              if ($profile_img_name!="") {
                  $profile_name = $profile_img_name;
              } else if($visitor_type==2) {
                $profile_name = "delivery_boy.png";
              } elseif ($visitor_type==3) {
                $profile_name = "taxi.png";
              } else {
                $profile_name = "visitor_default.png";
              }
                $notyUrl=$base_url.'img/visitor/'.$profile_name;
            }


              $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$visitor_sub_type_id'");
             $vistLogo=mysqli_fetch_array($fd);
             if ($vistLogo['visitor_sub_image']!='') {
                    $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                } else {
                    $visit_logo="";
                }

            $userIdAry=explode(",",$user_id);

            $userIdNonApporvalReq= array();
            $userIdApporvalReq= array();
            $unitIdApporvalReq= array();
            for ($i=0; $i <count($userIdAry) ; $i++) { 
                $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$userIdAry[$i]'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $deliveryApproval=$data_notification['Delivery_cab_approval'];
                if ($deliveryApproval==0) {
                    array_push($userIdNonApporvalReq, $data_notification['user_id']);
                } else {
                    array_push($userIdApporvalReq, $data_notification['user_id']);
                    array_push($unitIdApporvalReq, $data_notification['unit_id']);
                }

            }

            $visit_from = $visit_from.'';

            // approval not req

            $globleVariblePrntId = "";

            for ($i=0; $i <count($userIdNonApporvalReq) ; $i++) { 
                $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$userIdNonApporvalReq[$i]'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $visitor_approved=$data_notification['visitor_approved'];
                $deliveryApproval=$data_notification['Delivery_cab_approval'];
                $sos_user_token=$data_notification['user_token'];
                $device=$data_notification['device'];
                $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];
                // $block_id=$data_notification['block_id'];
                $user_full_name=$data_notification['user_full_name'];
                $user_mobile=$data_notification['user_mobile'];
               

                $m->set_data('parent_visitor_id', $visitor_id);
                $m->set_data('society_id', $society_id);

                if ($data_notification['floor_id']=='') {
                    $m->set_data('floor_id', $floor_id);
                    $m->set_data('block_id', $block_id);
                    $m->set_data('unit_id', $unit_id);
                    $m->set_data('visitor_type', 5);
                } else if ($data_notification['floor_id']!="") {
                    $m->set_data('floor_id', $data_notification['floor_id']);
                    $m->set_data('block_id', $data_notification['block_id']);
                    $m->set_data('unit_id', $data_notification['unit_id']);
                    $m->set_data('visitor_type', $visitor_type);
                } else {
                    $m->set_data('floor_id', $floor_id);
                    $m->set_data('block_id', $block_id);
                    $m->set_data('unit_id', $unit_id);
                    $m->set_data('visitor_type', 5);
                }
                
                $m->set_data('user_id', $userIdNonApporvalReq[$i]);
                $m->set_data('emp_id', $emp_id);
                $m->set_data('visit_from', $visit_from);
                $m->set_data('visitor_name', $visitor_name);
                $m->set_data('visitor_mobile', $visitor_mobile);
                $m->set_data('country_code', $country_code);
                $m->set_data('number_of_visitor', $number_of_visitor);
                $m->set_data('visit_date', $visit_date);
                $m->set_data('visit_time', $visit_time);
                $m->set_data('visitor_status', '1');
                $m->set_data('number_of_visitor', 1);
                $m->set_data('visiting_reason', $visiting_reason);
                $m->set_data('qr_code_master_id', $qrCode);
                $m->set_data('vehicle_no', $vehicle_no);
                $m->set_data('parking_id', $parking_id);
                $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
                $m->set_data('visiting_apartment', $block_unit_name);
                $m->set_data('approve_by_id', $userIdNonApporvalReq[$i]);

                $a = array(
                    // 'parent_visitor_id' => $m->get_data('parent_visitor_id'),
                    'society_id' => $m->get_data('society_id'),
                    'floor_id' => $m->get_data('floor_id'),
                    'block_id' => $m->get_data('block_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'user_id' => $m->get_data('user_id'),
                    'emp_id' => $m->get_data('emp_id'),
                    'visitor_type' => $m->get_data('visitor_type'),
                    'visit_from' => $m->get_data('visit_from'),
                    'visitor_name' => $m->get_data('visitor_name'),
                    'visitor_mobile' => $m->get_data('visitor_mobile'),
                    'country_code' => $m->get_data('country_code'),
                    'visitor_profile' => $profile_name,
                    'number_of_visitor' => $m->get_data('number_of_visitor'),
                    'visit_date' => $m->get_data('visit_date'),
                    'visit_time' => $m->get_data('visit_time'),
                    'visitor_status' => $m->get_data('visitor_status'),
                    'otp' => $m->get_data('otp'),
                    'visiting_reason' => $m->get_data('visiting_reason'),
                    'qr_code_master_id'=>$m->get_data('qr_code_master_id'),
                    'vehicle_no'=>$m->get_data('vehicle_no'),
                    'parking_id'=>$m->get_data('parking_id'),
                    'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                    'entry_time'=>date("Y-m-d H:i:s"),
                    'visiting_apartment'=>$m->get_data('visiting_apartment'),
                    'approve_by_id'=>$m->get_data('approve_by_id'),
                );


                $quserDataUpdate = $d->insert("visitors_master", $a);
                $parent_visitor_id = $con->insert_id;
                $visitor_id = $parent_visitor_id;
                if ($i==0) {
                    $aPrt = array(
                        'parent_visitor_id' => $parent_visitor_id,
                    );
                    $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                   $globleVariblePrntId=$parent_visitor_id;
                } else {
                    $aPrt = array(
                        'parent_visitor_id' => $globleVariblePrntId,
                    );
                    $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                }
                if ($quserDataUpdate == true) {

                    if($flag == true){

                        $m->set_data('parking_id', $parking_id);
                        $m->set_data('vehicle_no', $vehicle_no);
                        // $m->set_data('parking_status', '1');
                        $m->set_data('visitor', $visitor_id);
                        $m->set_data('vehicle_type', $vehicle_type);

                        $a2 = array('parking_type' => $m->get_data('vehicle_type'),
                        // 'parking_status' => $m->get_data('parking_status'),
                        'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                        'visitor' => $m->get_data('visitor'));

                        $d->update("parking_master",$a2,"parking_id='$parking_id'");

                    }

                    if($visitor_type==2) {
                            $notification_logo= "delivery_boy.png";
                            $notTitle= "Delivery boy $unit_name Arrived at Gate"; 
                            $notDesc= "$visitor_name arrived at your doorstep very soon"; 
                            if ($device=='android') {
                               $nResident->noti("VisitorFragment","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                            }  else if($device=='ios') {
                               $nResident->noti_ios("VisitorVC","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                            }
                       
                    } else if($visitor_type==3) {
                        $notification_logo= "taxi.png";
                            $notTitle= "Cab $unit_name Arrived at Gate"; 
                            $notDesc= "Entered in building soon."; 
                            if ($device=='android') {
                               $nResident->noti("VisitorFragment","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                            }  else if($device=='ios') {
                               $nResident->noti_ios("VisitorVC","$notyUrl",$society_id,$sos_user_token,$notTitle,$notDesc,'newVisitor');
                            }
                    }
                     $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$userIdNonApporvalReq[$i],
                        'notification_title'=>$notTitle,
                        'notification_desc'=>$notDesc,
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'Visitor',
                        'notification_logo'=>$notification_logo,
                      );
                      
                    $d->insert("user_notification",$notiAry);
                } 
            } //for loop end


            //  Apporval  req  for cab & delivry

            $tempPrntIdAry=array();
            $tempUnitIdAry=array();

            for ($i=0; $i <count($userIdApporvalReq) ; $i++) { 

               
                $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$userIdApporvalReq[$i]'");
                $data_notification=mysqli_fetch_array($qUserToken);
                $visitor_approved=$data_notification['visitor_approved'];
                $deliveryApproval=$data_notification['Delivery_cab_approval'];
                $sos_user_token=$data_notification['user_token'];
                $device=$data_notification['device'];
                $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];
                $user_full_name=$data_notification['user_full_name'];
                $user_mobile=$data_notification['user_mobile'];

                if ($data_notification['floor_id']=='') {
                    $m->set_data('floor_id', $floor_id);
                    $m->set_data('block_id', $block_id);
                    $m->set_data('unit_id', $unit_id);
                    $m->set_data('visitor_type', 5);
                } else if ($data_notification['floor_id']!="") {
                    $m->set_data('floor_id', $data_notification['floor_id']);
                    $m->set_data('block_id', $data_notification['block_id']);
                    $m->set_data('unit_id', $data_notification['unit_id']);
                    $m->set_data('visitor_type', $visitor_type);
                } else {
                    $m->set_data('floor_id', $floor_id);
                    $m->set_data('block_id', $block_id);
                    $m->set_data('unit_id', $unit_id);
                    $m->set_data('visitor_type', 5);
                }


                    $m->set_data('parent_visitor_id', $visitorId);
                    $m->set_data('society_id', $society_id);
                    // $m->set_data('floor_id', $data_notification['floor_id']);
                    // $m->set_data('block_id', $data_notification['block_id']);
                    // $m->set_data('unit_id', $data_notification['unit_id']);
                    $m->set_data('user_id', $userIdApporvalReq[$i]);
                    $m->set_data('emp_id', $emp_id);
                    $m->set_data('visitor_type', $visitor_type);
                    $m->set_data('visit_from', $visit_from);
                    $m->set_data('visitor_name', $visitor_name);
                    $m->set_data('visitor_mobile', $visitor_mobile);
                    $m->set_data('country_code', $country_code);
                    $m->set_data('number_of_visitor', $number_of_visitor);
                    $m->set_data('visit_date', $visit_date);
                    $m->set_data('visit_time', $visit_time);
                    $m->set_data('visitor_status', '0');
                    
                    $m->set_data('number_of_visitor', 1);
                    $m->set_data('visiting_reason', $visiting_reason);
                    $m->set_data('qr_code_master_id', $qrCode);
                    $m->set_data('vehicle_no', $vehicle_no);
                    $m->set_data('parking_id', $parking_id);
                    $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
                    $m->set_data('visiting_apartment', $block_unit_name);

                    $a = array(
                        'parent_visitor_id' => $m->get_data('parent_visitor_id'),
                        'society_id' => $m->get_data('society_id'),
                        'floor_id' => $m->get_data('floor_id'),
                        'block_id' => $m->get_data('block_id'),
                        'unit_id' => $m->get_data('unit_id'),
                        'user_id' => $m->get_data('user_id'),
                        'emp_id' => $m->get_data('emp_id'),
                        'visitor_type' => $m->get_data('visitor_type'),
                        'visit_from' => $m->get_data('visit_from'),
                        'visitor_name' => $m->get_data('visitor_name'),
                        'visitor_mobile' => $m->get_data('visitor_mobile'),
                        'country_code' => $m->get_data('country_code'),
                        'visitor_profile' => $profile_name,
                        'number_of_visitor' => $m->get_data('number_of_visitor'),
                        'visit_date' => $m->get_data('visit_date'),
                        'visit_time' => $m->get_data('visit_time'),
                        'visitor_status' => $m->get_data('visitor_status'),
                        'otp' => $m->get_data('otp'),
                        'visiting_reason' => $m->get_data('visiting_reason'),
                        'qr_code_master_id'=>$m->get_data('qr_code_master_id'),
                        'vehicle_no'=>$m->get_data('vehicle_no'),
                        'parking_id'=>$m->get_data('parking_id'),
                        'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                        'entry_time'=>date("Y-m-d H:i:s"),
                        'visiting_apartment'=>$m->get_data('visiting_apartment'),
                    );


                    $quserDataUpdate = $d->insert("visitors_master", $a);
                    $parent_visitor_id = $con->insert_id;
                    $visitorId = $parent_visitor_id;
                    if ($i==0) {
                        $aPrt = array(
                            'parent_visitor_id' => $parent_visitor_id,
                        );
                        $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                       $globleVariblePrntId=$parent_visitor_id;
                    } else {
                        $aPrt = array(
                            'parent_visitor_id' => $globleVariblePrntId,
                        );
                        $d->update("visitors_master", $aPrt,"visitor_id='$parent_visitor_id'");
                    }
                    if ($quserDataUpdate == true) {

                        if($flag == true){

                            $m->set_data('parking_id', $parking_id);
                            $m->set_data('vehicle_no', $vehicle_no);
                            // $m->set_data('parking_status', '1');
                            $m->set_data('visitor', $visitorId);
                            $m->set_data('vehicle_type', $vehicle_type);

                            $a2 = array('parking_type' => $m->get_data('vehicle_type'),
                            // 'parking_status' => $m->get_data('parking_status'),
                            'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                            'visitor' => $m->get_data('visitor'));

                            $d->update("parking_master",$a2,"parking_id='$parking_id'");

                        }

                         $visit_timeNotificatin= date("d M Y h:i A", strtotime($visit_date.' '.$visit_time));

                         $clickAray = array(
                            'title' => "newVisitorApprove",
                            'visitor_id' => "$visitorId",
                            'parent_visitor_id' => "$visitorId",
                            'unique_visitor_id' => "$parent_visitor_id",
                            'visitor_name' => $visitor_name,
                            'visitor_profile' => $notyUrl,
                            'visitor_type' => $visitor_type,
                            'visitor_mobile' => $visitor_mobile,
                            'visit_from' => $visit_from,
                            'visit_logo' => $visit_logo,
                            'visit_time' => $visit_timeNotificatin,
                            'unit_name' => $unit_name,
                            'user_id' => $userIdApporvalReq[$i],
                            'block_id' => $block_id,
                            'user_name' => $user_full_name,
                            'base_url' => $base_url,
                            'user_mobile' => $user_mobile,
                        );

                        if($visitor_type==2) {
                                $notification_logo= "delivery_boy.png";
                                $notTitle= "Delivery boy $unit_name Arrived at Gate"; 
                                $notDesc= "$visitor_name from $visit_from is waiting for your approval.";
                                if ($device=='android') {
                                   $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,$notTitle,$notDesc, $clickAray);
                                }  else if($device=='ios') {
                                   $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token,$notTitle,$notDesc, $clickAray);
                                }
                           
                        } else if($visitor_type==3) {
                                $notification_logo= "taxi.png";
                                $notTitle= "Cab $unit_name Arrived at Gate ($user_name)"; 
                                $notDesc= "Cab driver from $visit_from is waiting for your approval";
                                if ($device=='android') {
                                   $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,$notTitle,$notDesc, $clickAray);
                                }  else if($device=='ios') {
                                   $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token,$notTitle,$notDesc, $clickAray);
                                }
                        }
                         $notiAry = array(
                            'society_id'=>$society_id,
                            'user_id'=>$userIdApporvalReq[$i],
                            'notification_title'=>$notTitle,
                            'notification_desc'=>$notDesc,
                            'notification_date'=>date('Y-m-d H:i'),
                            'notification_action'=>"Visitor",
                            'notification_logo'=>$notification_logo
                          );
                          
                        $d->insert("user_notification",$notiAry);
                    } 
                

                
            } //for loop end

                if ($societyData['visitor_otp_verify']==1) {
                    
                    if ($country_code=='') {
                      $country_code = "+91";
                    }
                    $smsObj->send_ongate_visitor_otp($society_id,$visitor_mobile,$visitor_name,$society_name,$otp,$country_code);
                    $d->add_sms_log($visitor_mobile,"On Gate Visitor OTP SMS",$society_id,$country_code,1);

                }

                $response["message"] =''.$xml->string->addVisitorSucess;
                $response["status"] = "200";
                echo json_encode($response);


        }  else if ($_POST['SendVisitorRemainder'] == "SendVisitorRemainder"  && $society_id!='') {
            
            $qq=$d->select("visitors_master","parent_visitor_id='$parent_visitor_id' AND user_id='$user_id'");
            while ($visitorData=mysqli_fetch_array($qq)) {
                if ($visitorData['visitor_status']==0 || $visitorData['visitor_status']==6) {
                
                    $unique_visitor_id = $visitorData['visitor_id'];
                    $visitor_id = $visitorData['parent_visitor_id'];
                    $parent_visitor_id = $visitorData['parent_visitor_id'];
                    $visitor_status = $visitorData['visitor_status'];
                    $visitor_name = $visitorData['visitor_name'];
                    $visit_from = $visitorData['visit_from'];
                    $visitor_type = $visitorData['visitor_type'];
                    $visitor_mobile = $visitorData['visitor_mobile'];
                    $visit_date = $visitorData['visit_date'];
                    $visit_time = $visitorData['visit_time'];
                    $user_id = $visitorData['user_id'];


                    $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$user_id' ");
                    $data_notification=mysqli_fetch_array($qUserToken);
                    $sos_user_token=$data_notification['user_token'];
                    $device=$data_notification['device'];
                    $notyUrl = $base_url.'img/visitor/'.$visitorData['visitor_profile'];
                    $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];
                    $block_id=$data_notification['block_id'];
                    $user_full_name=$data_notification['user_full_name'];
                    $user_mobile=$data_notification['user_mobile'];

                    $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$visitorData[visitor_sub_type_id]'");
                    $vistLogo=mysqli_fetch_array($fd);
                     if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $visit_time= date("d M Y h:i A", strtotime($visit_date.' '.$visit_time));


                    $clickAray = array(
                        'title' => "newVisitorApprove",
                        'visitor_id' => $visitor_id,
                        'parent_visitor_id' => $parent_visitor_id,
                        'unique_visitor_id' => $unique_visitor_id,
                        'visitor_name' => $visitor_name,
                        'visitor_profile' => $notyUrl,
                        'visitor_type' => $visitor_type,
                        'visitor_mobile' => $visitor_mobile,
                        'visit_from' => $visit_from,
                        'visit_logo' => $visit_logo,
                        'visit_time' => $visit_time,
                        'visitor_status' => $visitor_status,
                        'unit_name' => $unit_name,
                        'user_id' => $user_id,
                        'block_id' => $block_id,
                        'user_name' => $user_full_name,
                        'base_url' => $base_url,
                        'user_mobile' => $user_mobile,
                    );

                     if($visitor_type==2) {
                                $notTitle= "Delviery boy $unit_name Arrived at Gate"; 
                                $notDesc= "$visitor_name from $visit_from is waiting for your approval.";
                           
                        } else if($visitor_type==3) {
                                $notTitle= "Cab $unit_name has Arrived at Gate"; 
                                $notDesc= "Cab driver from $visit_from is waiting for your approval";
                        } else  {
                                $notTitle= "Visitor $unit_name has Arrived at Gate"; 
                                $notDesc= "$visitor_nameis waiting for your approval";
                        }

                    if ($device=='android') {
                       $nResident->noti("VisitorFragment","",$society_id,$sos_user_token,$notTitle,$notDesc, $clickAray);
                    }  else if($device=='ios') {
                       $nResident->noti_ios("VisitorVC","",$society_id,$sos_user_token,$notTitle,$notDesc, $clickAray);
                    }
                    $response["message"] =''.$xml->string->approvalreqSucess;
                    $response["status"] = "200";
                    echo json_encode($response);
                } else {
                     $response["message"] =''.$xml->string->visitorNotreqApproval;
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                } 
            }
            


        } else  if ($_POST['resendOtpExptVisitor'] == "resendOtpExptVisitor"  && $society_id!='') {

            $q = $d->select("visitors_master,unit_master,block_master", "visitors_master.block_id = block_master.block_id AND visitors_master.unit_id = unit_master.unit_id AND  visitors_master.visitor_id = '$visitor_id' AND visitors_master.society_id = '$society_id'", "");
            $data=mysqli_fetch_array($q);
            $otp=$data['otp'];
            $visitor_name=$data['visitor_name'];
            $visitor_mobile=$data['visitor_mobile'];
            $visit_date=$data['visit_date'];
            $visit_time=$data['visit_time'];
            $block_name=$data['block_name'].'-'.$data['unit_name'];
            
            $qSociety = $d->select("society_master","society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qSociety);
            $society_name = $societyData['society_name'];


                if ($country_code=='') {
                    $country_code = "+91";
                }
                $smsObj->send_ongate_visitor_otp($society_id,$visitor_mobile,$visitor_name,$society_name,$otp,$country_code);
                $d->add_sms_log($visitor_mobile,"On Gate Visitor OTP SMS",$society_id,$country_code,1);


            if($q) {
                $response["message"] = ''.$xml->string->otpSendtoVisitor;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->novisitoradd;
                $response["status"] = "201";
                echo json_encode($response);
            }


        } else if ($_POST['getNewVisitorList'] == "getNewVisitorList") {

            $qnotification = $d->select("visitors_master,unit_master,block_master", "visitors_master.visitor_status != '5' AND visitors_master.block_id = block_master.block_id AND visitors_master.unit_id = unit_master.unit_id AND visitors_master.visitor_type != '1' AND visitors_master.society_id = '$society_id' AND visitors_master.visitor_status != '2' AND visitors_master.block_id = block_master.block_id AND visitors_master.unit_id = unit_master.unit_id AND visitors_master.visitor_type != '1' AND visitors_master.society_id = '$society_id' AND visitors_master.visitor_status != '3' AND visitors_master.block_id = block_master.block_id AND visitors_master.unit_id = unit_master.unit_id AND visitors_master.visitor_type != '1' AND visitors_master.society_id = '$society_id' ", "GROUP BY visitors_master.visitor_mobile ORDER BY visitors_master.visitor_id DESC ");

            $response["visitor"] = array();

            if (mysqli_num_rows($qnotification) > 0) {

                while ($data_notification = mysqli_fetch_array($qnotification)) {
                    $userId= $data_notification['user_id'];
                    $qu=$d->selectRow("user_id,user_full_name,user_type,user_status,visitor_approved","users_master","user_id='$userId' AND society_id ='$society_id'","ORDER BY user_id DESC");
                    
                    $userData=mysqli_fetch_array($qu);
                    $visitor = array();

                    $visitor["visitor_id"] = $data_notification['visitor_id'];
                    $visitor["society_id"] = $data_notification['society_id'];
                    $unit_id = $data_notification['unit_id'];
                    $visitor["visitor_name"] = $data_notification['visitor_name'];
                    $visitor["visitor_type"] = $data_notification['visitor_type'];
                    $visitor["block"] = $data_notification['block_name'] . "-" . $data_notification['unit_name'] .' '. $userData['user_full_name'];
                    $visitor["visitor_mobile"] = "" . $data_notification['visitor_mobile'];

                    if ($visitorMobilePrivacy==0) {
                    $visitor["visitor_mobile_new"] = "".substr($data_notification['visitor_mobile'], 0, 3) . '****' . substr($data_notification['visitor_mobile'],  -3);
                    } else {
                    $visitor["visitor_mobile_new"] = "".$data_notification['visitor_mobile'];
                    }

                    $visitor["visitor_profile"] = $base_url . "img/visitor/" . $data_notification['visitor_profile'];
                    $visitor["visitor_status"] = $data_notification['visitor_status'];
                    $visitor["visitor_approved"] = $userData['visitor_approved'];
                    $visitor["qr_code_master_id"] = $data_notification['qr_code_master_id'];


                    $qUserToken=$d->select("users_master","unit_id='$unit_id'");
    
                    $data_notification=mysqli_fetch_array($qUserToken);
    
                    $visitor["user_id"] =$userData['user_id'];

                    array_push($response["visitor"], $visitor);
                }

                $response["message"] =''.$xml->string->getVisitorList;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noVisitorfound;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getNewVisitorListMulti'] == "getNewVisitorListMulti") {

            $array1 = array();
            $array2 = array();

            $response["visitor"] = array();
            // common visitor
            $qnotification = $d->select("visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "visitor_type = '5' AND   society_id = '$society_id' AND visitor_status != '3' AND visitor_status != '5'  AND  visitor_status != '2' ", "GROUP BY parent_visitor_id ORDER BY visitor_id DESC ");


            if (mysqli_num_rows($qnotification) > 0) {

                while ($data_notification = mysqli_fetch_array($qnotification)) {
                    $data_notification = array_map("html_entity_decode", $data_notification);
                    $parent_visitor_id = $data_notification['parent_visitor_id'];
                    if ($data_notification['block_id']!=0 && $data_notification['floor_id']!=0 && $data_notification['unit_id']!=0) {
                        $gq= $d->select("block_master,unit_master","block_master.block_id=unit_master.block_id AND block_master.block_id='$data_notification[block_id]' AND unit_master.unit_id='$data_notification[unit_id]'");
                        $blockData=mysqli_fetch_array($gq);
                        $block_name = $blockData['block_name'].'-'.$blockData['unit_name'].' (Empty)';
                    } else {
                        $block_name = "Common Visitor";
                    }

                    $visitorCommon = array();

                    $visitorCommon["visitor_id"] = $data_notification['visitor_id'];
                    $visitorCommon["parent_visitor_id"] = $data_notification['parent_visitor_id'];
                    $visitorCommon["society_id"] = $data_notification['society_id'];
                    $visitorCommon["visitor_name"] = html_entity_decode($data_notification['visitor_name']);
                    $visitorCommon["visit_from"] = $data_notification['visit_from'];

    
                     if ($data_notification['visitor_sub_image']!='') {
                        $visit_logo = $base_url.'img/visitor_company/'.$data_notification['visitor_sub_image'].'';
                    } else {
                        $visit_logo="";
                    }
                    $visitorCommon["visit_logo"] = $visit_logo.'';
                    
                    $visitorCommon["visitor_type"] = $data_notification['visitor_type'];
                    $visitorCommon["block"] = "".$block_name;
                    $visitorCommon["visitor_mobile"] = "" . $data_notification['visitor_mobile'];
                    $visitorCommon["country_code"] = $data_notification['country_code'];

                    if ($visitorMobilePrivacy==0) {
                    $visitorCommon["visitor_mobile_new"] = "".substr($data_notification['visitor_mobile'], 0, 3) . '****' . substr($data_notification['visitor_mobile'],  -3);
                    } else {
                    $visitorCommon["visitor_mobile_new"] = "".$data_notification['visitor_mobile'];
                    }

                    $visitorCommon["visitor_profile"] = $base_url . "img/visitor/" . $data_notification['visitor_profile'];
                    $visitorCommon["visitor_status"] = $data_notification['visitor_status'];
                    $visitorCommon["reject_by_id"] = $data_notification['reject_by_id'];
                    $visitorCommon["visitor_approved"] = "0";
                    $visitorCommon["delivery_cab_approval"] = "0";
                    $visitorCommon["qr_code_master_id"] = $data_notification['qr_code_master_id'];
                    $visitorCommon["otp"] = $data_notification['otp'];
                    $visitorCommon["vehicle_no"] = $data_notification['vehicle_no'];
                    $visitorCommon["added_time"] =  time_elapsed_string(date("d M Y h:i A", strtotime($data_notification['visit_date']." ".$data_notification['visit_time'])));
                    $visitorCommon["user_id"] ="0";

                    $intTime = $data_notification['visit_date'].' '.$data_notification['visit_time'];
                    $outTime=date("Y-m-d H:i:s");
                    $diffSeconds = strtotime($outTime) - strtotime($intTime);
                    if (($auto_reject_vistor_minutes*60)-$diffSeconds>0) {
                        $visitorCommon["duration_sec"] = ($auto_reject_vistor_minutes*60)-$diffSeconds;
                    }else {
                        $visitorCommon["duration_sec"] = "0";
                    }


                    if ( $diffSeconds/60 >$auto_reject_vistor_minutes && $data_notification['visitor_status']==1) {
                        $visitorCommon["visitor_status"] = '4';
                        $aReject = array(
                            // 'visit_date' => $m->get_data('visit_date'),
                            // 'visit_time' => $m->get_data('visit_time'),
                            'visitor_status' => 4
                        );
                        $d->update("visitors_master", $aReject, "parent_visitor_id='$parent_visitor_id' AND $parent_visitor_id!=0");

                    } 

        
                    if ($diffSeconds<86400) {
                        // not show if visitor rejected 24 hours a go
                        array_push($array1, $visitorCommon);
                    }

                    // array_push($response["visitor"], $visitor);
                }

                 $temp2 = true;
             } else {
                $temp2 = false;
             }

            // other visitor
            if ($access_blocks>0) {
                $appedBlockAccessQuery = " AND block_master.block_id IN($access_blocks)";
            }

            $qnotification = $d->select("unit_master,block_master,users_master,visitorSubType,visitors_master", "visitors_master.visitor_sub_type_id=visitorSubType.visitor_sub_type_id AND  visitors_master.user_id=users_master.user_id AND visitors_master.visitor_status != '5' AND visitors_master.block_id = block_master.block_id AND visitors_master.unit_id = unit_master.unit_id AND visitors_master.visitor_type != '1' AND visitors_master.society_id = '$society_id' AND visitors_master.visitor_status != '2' AND visitors_master.block_id = block_master.block_id AND visitors_master.unit_id = unit_master.unit_id AND visitors_master.visitor_type != '1' AND visitors_master.society_id = '$society_id' AND visitors_master.visitor_status != '3' AND visitors_master.block_id = block_master.block_id AND visitors_master.unit_id = unit_master.unit_id AND visitors_master.visitor_type != '1' AND visitors_master.society_id = '$society_id' $appedBlockAccessQuery", "GROUP BY visitors_master.parent_visitor_id ORDER BY visitors_master.visitor_id DESC ");


            if (mysqli_num_rows($qnotification) > 0) {

                while ($data_notification = mysqli_fetch_array($qnotification)) {
                    $data_notification = array_map("html_entity_decode", $data_notification);
                    
                    $visitorNormal = array();
                    $parent_visitor_id =  $data_notification['parent_visitor_id'];;
                    $visitorNormal["visitor_id"] = $data_notification['visitor_id'];
                    $visitorNormal["parent_visitor_id"] = $data_notification['parent_visitor_id'];
                    $visitorNormal["society_id"] = $data_notification['society_id'];
                    $unit_id = $data_notification['unit_id'];
                    $visitorNormal["visitor_name"] = html_entity_decode($data_notification['visitor_name']);
                    $visitorNormal["visit_from"] = $data_notification['visit_from'];
                    
                     if ($data_notification['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$data_notification['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $visitorNormal["visit_logo"] = $visit_logo.'';
                    
                    $visitorNormal["visitor_type"] = $data_notification['visitor_type'];
                    if ($data_notification['visiting_apartment']!="") {
                        $visitorNormal["block"] = $data_notification['visiting_apartment'];
                    } else {
                        $visitorNormal["block"] = $data_notification['block_name'] . "-" . $data_notification['unit_name'];
                    }
                    $visitorNormal["visitor_mobile"] = "" . $data_notification['visitor_mobile'];
                    $visitorNormal["country_code"] = $data_notification['country_code'];

                    if ($visitorMobilePrivacy==0) {
                    $visitorNormal["visitor_mobile_new"] = "".substr($data_notification['visitor_mobile'], 0, 3) . '****' . substr($data_notification['visitor_mobile'],  -3);
                    } else {
                    $visitorNormal["visitor_mobile_new"] = "".$data_notification['visitor_mobile'];
                    }

                    $visitorNormal["visitor_profile"] = $base_url . "img/visitor/" . $data_notification['visitor_profile'];
                    $visitorNormal["visitor_status"] = $data_notification['visitor_status'];
                    $visitorNormal["reject_by_id"] = $data_notification['reject_by_id'];
                    $visitorNormal["visitor_approved"] = $data_notification['visitor_approved'];
                    $visitorNormal["delivery_cab_approval"] = $data_notification['Delivery_cab_approval'];
                    $visitorNormal["qr_code_master_id"] = $data_notification['qr_code_master_id'];
                    $visitorNormal["otp"] = $data_notification['otp'];
                    $visitorNormal["vehicle_no"] = $data_notification['vehicle_no'];
                    $visitorNormal["added_time"] =  time_elapsed_string(date("d M Y h:i A", strtotime($data_notification['visit_date']." ".$data_notification['visit_time'])));
                    $visitorNormal["user_id"] =$data_notification['user_id'];

                    $intTime = $data_notification['visit_date'].' '.$data_notification['visit_time'];
                    $outTime=date("Y-m-d H:i:s");
                    $diffSeconds = strtotime($outTime) - strtotime($intTime);
                    if (($auto_reject_vistor_minutes*60)-$diffSeconds>0) {
                        $visitorNormal["duration_sec"] = ($auto_reject_vistor_minutes*60)-$diffSeconds;
                    }else {
                        $visitorNormal["duration_sec"] = "0";
                    }


                    if ($diffSeconds/60 >$auto_reject_vistor_minutes && $data_notification['visitor_status']==0 || $diffSeconds/60 >$auto_reject_vistor_minutes && $data_notification['visitor_status']==1 || $diffSeconds/60 >$auto_reject_vistor_minutes && $data_notification['visitor_status']==6) {
                        $visitorNormal["visitor_status"] = '4';
                        $aReject = array(
                            // 'visit_date' => $m->get_data('visit_date'),
                            // 'visit_time' => $m->get_data('visit_time'),
                            'visitor_status' => 4
                        );
                        $d->update("visitors_master", $aReject, "parent_visitor_id='$parent_visitor_id' ");

                    } 

                   if ($diffSeconds<86400) {
                        // not show if visitor rejected 24 hours a go
                        array_push($array2, $visitorNormal);
                    }
                }

               $temp = true;
            } else {
                $temp = false;
            }

            $newArray = array_merge($array1,$array2);
            foreach ($newArray as $key => $part) {
                   $sort[$key] = strtotime($part['visitor_id']);
            }
            array_multisort($sort, SORT_DESC, $newArray,SORT_DESC);

            for ($i=0; $i <count($newArray) ; $i++) { 
                $visitor = array(); 
                $visitor["visitor_id"]=$newArray[$i]['visitor_id'];
                $visitor["parent_visitor_id"]=$newArray[$i]['parent_visitor_id'];
                $visitor["society_id"]=$newArray[$i]['society_id'];
                $visitor["visitor_name"]=$newArray[$i]['visitor_name'];
                $visitor["visit_from"]=$newArray[$i]['visit_from'];
                $visitor["visit_logo"]=$newArray[$i]['visit_logo'];
                $visitor["visitor_type"]=$newArray[$i]['visitor_type'];
                $visitor["block"]=$newArray[$i]['block'];
                $visitor["visitor_mobile"]=$newArray[$i]['visitor_mobile'];
                $visitor["country_code"]=$newArray[$i]['country_code'];
                $visitor["visitor_mobile_new"]=$newArray[$i]['visitor_mobile_new'];
                $visitor["visitor_profile"]=$newArray[$i]['visitor_profile'];
                $visitor["visitor_status"]=$newArray[$i]['visitor_status'];
                $visitor["reject_by_id"]=$newArray[$i]['reject_by_id'];
                $visitor["visitor_approved"]=$newArray[$i]['visitor_approved'];
                $visitor["delivery_cab_approval"]=$newArray[$i]['delivery_cab_approval'];
                $visitor["qr_code_master_id"]=$newArray[$i]['qr_code_master_id'];
                $visitor["otp"]=$newArray[$i]['otp'];
                $visitor["vehicle_no"]=$newArray[$i]['vehicle_no'];
                $visitor["added_time"]=$newArray[$i]['added_time'];
                $visitor["user_id"]=$newArray[$i]['user_id'];
                $visitor["duration_sec"]=$newArray[$i]['duration_sec'];
                array_push($response["visitor"], $visitor);
            }


            if ($temp2==true || $temp ==true) {
                $response["visitor_otp_verify"] = $visitor_otp_verify;
                $response["message"] =''.$xml->string->getVisitorList;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["visitor_otp_verify"] = $visitor_otp_verify;
                $response["message"] = ''.$xml->string->noVisitorfound;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getNewVisitorListSingle'] == "getNewVisitorListSingle") {
            
            $qnotification = $d->select("visitors_master,unit_master,block_master,users_master", "users_master.user_id=visitors_master.user_id AND block_master.block_id=users_master.block_id AND unit_master.unit_id=users_master.unit_id AND visitors_master.society_id = '$society_id' AND visitors_master.parent_visitor_id='$visitor_id'", "");

            $response["users"] = array();
            $aloowIn = false;
            if (mysqli_num_rows($qnotification) > 0) {

                while ($data_notification = mysqli_fetch_array($qnotification)) {
                    $userId= $data_notification['user_id'];
                    


                    $users = array();

                    $users["user_id"] = $userId;
                     if ($data_notification['member_status']==1) {
                      $member_relation_name = '('.$data_notification['member_relation_name'].')';
                    } else {
                      $member_relation_name= "";
                    }
                    $users["user_first_name"] = $data_notification['user_full_name'].' '.$member_relation_name;
                    $users["visitor_id"] = $data_notification['visitor_id'];
                    $users["parent_visitor_id"] = $data_notification['parent_visitor_id'];
                    $users["society_id"] = $data_notification['society_id'];
                    $users["visitor_name"] = html_entity_decode($data_notification['visitor_name']);
                    $users["user_mobile"] = $data_notification['country_code'].' '.$data_notification['user_mobile'];
                    $users["public_mobile"] = $data_notification['public_mobile'];                    
                    if ($data_notification['visitor_status']==0 || $data_notification['visitor_status']==6) {
                        $users["visitor_approved"] = $data_notification['visitor_approved'];   
                        $users["delivery_cab_approval"] = $data_notification['Delivery_cab_approval'];                    
                    } else {
                        $users["visitor_approved"] ="0";   
                        $users["delivery_cab_approval"] = "0";                   
                    }                
                    $users["mobile_for_gatekeeper"] = $data_notification['mobile_for_gatekeeper'];                    
                    $users["user_status"] = $data_notification['user_status'];
                    if ($data_notification['visitor_status']==1) {
                        $aloowIn = true;                    
                    }                    
                    $users["visitor_type"] = $data_notification['visitor_type'];
                    $users["visitor_status"] = $data_notification['visitor_status'];
                    $users["reject_by_id"] = $data_notification['reject_by_id'];
                    $users["block"] = $data_notification['block_name'].'-'.$data_notification['unit_name'] ;
                   
                    $users["user_profile"] = $base_url . "img/users/recident_profile/" . $data_notification['user_profile_pic'];


                    

                    array_push($response["users"], $users);
                }

                $response["aloowIn"] =$aloowIn;
                $response["message"] =''.$xml->string->getVisitorList;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noVisitorfound;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if (isset($getStaffList) && $getStaffList == 'getStaffList') {

            $d->delete("visitors_master","visitor_status=0 AND DATE(visit_date) < CURDATE() OR visitor_status=6 AND DATE(visit_date) < CURDATE()");

            $q = $d->select("employee_master,emp_type_master", "employee_master.emp_type_id =emp_type_master.emp_type_id AND employee_master.society_id='$society_id' AND employee_master.entry_status != '1' AND employee_master.emp_status=1","ORDER BY emp_name ASC");

            $response["list"] = array();

            if (mysqli_num_rows($q) > 0) {
                while ($data = mysqli_fetch_array($q)) {
                    $list = array();
                    $today = date("Y-m-d");
                    $last_time = date("Y-m-d", strtotime($data['in_out_time']));

                    $earlier = new DateTime($today);
                    $later = new DateTime($last_time);
                    $diff = $later->diff($earlier)->format("%a");

                    if ($diff>29) {
                        $color_status='3';
                    } else if ($diff>15) {
                        $color_status='2';
                    } else {
                         $color_status='1';
                    }

                    $list["id"] = $data['emp_id'];
                    $list["name"] = $data['emp_name'];
                    $list["color_status"] = $color_status;
                    if ($data['in_out_time']!="") {
                    $list["last_in"] = time_elapsed_string($data['in_out_time']);
                    } else {
                    $list["last_in"] = "";
                    }
                    $list["mobile"] = $data['emp_mobile'];
                    $list["visit_from"] = '';
                    $list["visit_logo"] = '';
                    if ($data['private_resource']==1 || $visitorMobilePrivacy==0) {
                    $list["visitor_mobile_new"] = "".substr($data['emp_mobile'], 0, 3) . '****' . substr($data['emp_mobile'],  -3);
                    } else {
                    $list["visitor_mobile_new"] = "".$data['emp_mobile'];
                    }
                    $list["country_code"] = $data['country_code'];
                    $list["visitor_type"] = "0";
                    $list["des_block"] = $data['emp_type_name'];
                    $list["qr_code"] = $data['emp_id'];
                    $list["type"] = "staff";
                    $list["user_id"] = "";
                    $list["number_of_visitor"] = "";
                    $list["visiting_reason"] = "";
                    $list["profile_img"] = $base_url . "img/emp/" . $data['emp_profile'];
                    $list["qr_code_master_id"] = "";
                    $list["visit_date"] = "";
                    $list["visit_time"] = "";
                    $list["otp"] = '';
                    $list["vehicle_no"] ='';
                    $list["visitor_sub_type_id"] ='';
                    $list["leave_parcel_at_gate"] ='0';
                    $list["parcel_collect_master_id"] ="0";
                    $workingUnitsAry = array();
                    if ($data['emp_type']==1) {
                        $uq=$d->selectRow("employee_unit_master.employee_unit_id","unit_master,block_master,users_master,employee_unit_master ","users_master.delete_status=0 AND employee_unit_master .unit_id=unit_master.unit_id  AND
                            employee_unit_master .user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  employee_unit_master .emp_id ='$data[emp_id]' ","GROUP BY employee_unit_master.unit_id ORDER BY unit_master.unit_id ASC");
                         $working_units= mysqli_num_rows($uq);

                        $workiUnits = 'Working Units : '. $working_units;
                        $list["workig_units"] =$workiUnits."";
                    } else {
                        $list["workig_units"] = "";
                    }
                    

                    array_push($response["list"], $list);
                }
                $response["message"] = ''.$xml->string->$getListsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] =''.$xml->string->faildGetlist;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getExVisitorList'] == "getExVisitorList") {
            
           
            if ($access_blocks!="") {
                $qnotification = $d->select("visitors_master", "user_id = '$user_id' AND visitor_status != '5' AND visitor_status != '2' AND visitor_status != '3'AND visitor_status != '4' AND visitor_type = '1' AND society_id='$society_id' AND block_id IN($access_blocks) AND DATE(visit_date) >= CURDATE()");
            } else {
                $qnotification = $d->select("visitors_master", "user_id = '$user_id' AND visitor_status != '5' AND visitor_status != '2' AND visitor_status != '3'AND visitor_status != '4' AND visitor_type = '1' AND society_id='$society_id' AND DATE(visit_date) >= CURDATE()");

            }



            if (mysqli_num_rows($qnotification) > 0) {

                $response["visitor"] = array();

                while ($data_notification = mysqli_fetch_array($qnotification)) {

                    $visitor = array();

                    $visitor["visitor_id"] = $data_notification['visitor_id'];
                    $visitor["society_id"] = $data_notification['society_id'];
                    $visitor["unit_id"] = $data_notification['unit_id'];
                    $visitor["user_id"] = $data_notification['user_id'];
                    $visitor["visitor_type"] = $data_notification['visitor_type'];
                    $visitor["expected_type"] = $data_notification['expected_type'];
                    $visitor["visitor_name"] = html_entity_decode($data_notification['visitor_name']);
                    $visitor["visitor_mobile"] = "" . $data_notification['visitor_mobile'];
                    if ($visitorMobilePrivacy==0) {
                    $visitor["visitor_mobile_new"] = "".substr($data_notification['visitor_mobile'], 0, 3) . '****' . substr($data_notification['visitor_mobile'],  -3);
                    } else {
                    $visitor["visitor_mobile_new"] = "".$data_notification['visitor_mobile'];
                    }
                    $visitor["visitor_profile"] = $base_url . "img/visitor/" . $data_notification['visitor_profile'];
                    $visitor["vistor_number"] = "" . $data_notification['vistor_number'];
                    $visitor["visit_date"] = date("d M Y", strtotime($data_notification['visit_date']));
                    $visitor["visit_time"] = $data_notification['visit_time'];
                    $visitor["exit_date"] = "" . $data_notification['exit_date'];
                    $visitor["exit_time"] = "" . $data_notification['exit_time'];
                    $visitor["visitor_status"] = $data_notification['visitor_status'];
                    $visitor["otp"] = $data_notification['otp'].'';
                    $visitor["vehicle_no"] = $data_notification['vehicle_no'].'';
                    $visitor["visitor_sub_type_id"]= $data_notification['visitor_sub_type_id'].'';

                    array_push($response["visitor"], $visitor);
                }

                $response["message"] =''.$xml->string->getVistorsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noVisitorfound;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getExVisitorListNew'] == "getExVisitorListNew") {
            
        
            
            if ($access_blocks!="") {

            $q2 = $d->select("users_master,unit_master,block_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "users_master.user_id=visitors_master.user_id AND visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND visitors_master.visitor_type='1' AND visitors_master.visitor_status != '2' AND visitors_master.visitor_status != '3' AND visitors_master.visitor_status != '5' AND visitors_master.block_id IN ($access_blocks)  AND visitors_master.visit_date='$today'","ORDER BY visitor_id DESC");
            } else {
            $q2 = $d->select("users_master,unit_master,block_master,visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "users_master.user_id=visitors_master.user_id AND visitors_master.block_id =block_master.block_id AND visitors_master.unit_id =unit_master.unit_id AND visitors_master.society_id='$society_id' AND visitors_master.visitor_type='1' AND visitors_master.visitor_status != '2' AND visitors_master.visitor_status != '3' AND visitors_master.visitor_status != '5' AND visitors_master.visit_date='$today'","ORDER BY visitor_id DESC");
            }



            if (mysqli_num_rows($q2) > 0) {

                $response["list"] = array();

                while ($data2 = mysqli_fetch_array($q2)) {
                    
                    $vDate=$data2['visit_date'];
                    $visitor_id=$data2['visitor_id'];
                    $today= date('Y-m-d');
                    $date11 = new DateTime($today);
                    $date22 = new DateTime($vDate);
                    $interval = $date11->diff($date22);
                    $difference_days= $interval->days;
                    if ($difference_days>1) {
                        $d->delete("visitors_master","visitor_id='$visitor_id' AND society_id='$society_id'");
                    }else{
                    $list = array();
                    // $qu=$d->selectRow("user_first_name","users_master","user_id='$data2[user_id]'");
                    // $data3=mysqli_fetch_array($qu);

                    $visitDate = date("d M Y h:i A", strtotime($data2['visit_date'].' '.$data2['visit_time']));
                    $valid_till_date = date("d M Y h:i A", strtotime($data2['valid_till_date']));
                    $valid_till_dateTimeOnly = date("h:i A", strtotime($data2['valid_till_date']));

                    $list["id"] = $data2['visitor_id'];
                    $list["name"] =html_entity_decode($data2['visitor_name']);
                    $list["visit_from"] = html_entity_decode($data2['visit_from']).'';
                    // $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data2[visitor_sub_type_id]'");
                    // $vistLogo=mysqli_fetch_array($fd);
                     if ($data2['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$data2['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $list["visit_logo"] = $visit_logo.'';
                    if($data2['visitor_mobile']==0){
                        $list["mobile"] ="";
                    }else {
                        $list["mobile"] = $data2['visitor_mobile'];

                    }
                    $list["country_code"] = $data2['country_code'];

                    if ($visitorMobilePrivacy==0 && $data2['visitor_mobile']!=0) {
                    $list["visitor_mobile_new"] = "".substr($data2['visitor_mobile'], 0, 3) . '****' . substr($data2['visitor_mobile'],  -3);
                    } else if($data2['visitor_mobile']==0){
                    $list["visitor_mobile_new"] = "Not Available";
                    } else {
                    $list["visitor_mobile_new"] = "".$data2['visitor_mobile'];
                    }
                    $list["visitor_type"] = $data2['expected_type'];
                    $list["des_block"] = $data2['block_name'] . "-" . $data2['unit_name']. " (" . $data2['user_first_name'].')';
                    $list["visit_date_time"] = $visitDate.' to '.$valid_till_dateTimeOnly;
                    $list["valid_till_date"] = $valid_till_date;
                    $list["qr_code"] = $data2['visitor_id'];
                    $list["number_of_visitor"] = $data2['number_of_visitor'];
                    $list["visiting_reason"] = $data2['visiting_reason'];
                    $list["type"] = "Exvisitor";
                    $list["profile_img"] = $base_url . "img/visitor/" . $data2['visitor_profile'];
                    $list["qr_code_master_id"] = $data2['qr_code_master_id'];
                    $list["visit_date"] = $data2['visit_date'];
                    $list["visit_time"] = $data2['visit_time'];
                    $list["otp"] = $data2['otp'].'';
                    $list["vehicle_no"] =html_entity_decode($data2['vehicle_no']);
                    $list["visitor_sub_type_id"]= $data2['visitor_sub_type_id'].'';
                    $list["leave_parcel_at_gate"] = $data2['leave_parcel_at_gate'].'';
                    $list["parcel_collect_master_id"] = $data2['parcel_collect_master_id'].'';
                    $list["visitor_status"] = $data2['visitor_status'];
                    $list["color_status"] ="";
                    $list["last_in"] = "";
                    $unit_id = $data2['unit_id'];
                    
                   
                    $list["user_id"] =$data2['user_id'];
                    $cTime = date("H:i:s");

                    $expire = strtotime($data2['valid_till_date']);
                    $today = strtotime("today $cTime");

                    if($today >= $expire){
                        $visitor["visitor_status"] = '4';
                        $aReject = array(
                            'visitor_status' => 4
                        );
                        $d->update("visitors_master", $aReject, "parent_visitor_id='$data2[parent_visitor_id]' ");
                    } 

                    array_push($response["list"], $list);
                    
                    }
                }

                $response["message"] =''.$xml->string->getVistorsucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noVisitorfound;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['deleteVisitor'] == "deleteVisitor") {
            $q=$d->selectRow("visitor_status,leave_parcel_at_gate,parcel_collect_master_id","visitors_master","parent_visitor_id='$visitor_id' OR visitor_id = '$visitor_id' ");
            $vDdata=mysqli_fetch_array($q);
            if ($vDdata['leave_parcel_at_gate']==1) {
                $d->delete("parcel_collect_master","parcel_collect_master_id='$vDdata[parcel_collect_master_id]'");
            }

            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('visitor_status', '5');

            $a = array(
                'visitor_status' => $m->get_data('visitor_status')
            );

            if ($vDdata['visitor_status']==2) {
                 $response["message"]=$afterVisitoreExityoucanDelete;
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }else  if ($vDdata['visitor_status']==0 || $vDdata['visitor_status']==1)  {
                $qdelete = $d->delete("visitors_master","parent_visitor_id='$visitor_id' OR visitor_id = '$visitor_id' AND visitor_status!='2' ");
            } else {
                // $qdelete = $d->update("visitors_master",$a,"parent_visitor_id='$visitor_id' OR visitor_id = '$visitor_id' AND visitor_status!='2' ");
                $qdelete = $d->update("visitors_master", $a, "parent_visitor_id='$visitor_id' OR user_id='$user_id' AND visitor_id='$visitor_id' AND society_id='$society_id");
            }

            if ($qdelete == true) {

                $m->set_data('vehicle_no', "");
                $m->set_data('parking_status', '0');
                $m->set_data('visitor', "0");
                $m->set_data('vehicle_type', "0");

                $a2 = array(
                // 'parking_status' => $m->get_data('parking_status'),
                'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                'visitor' => $m->get_data('visitor'));
                
                $d->update("parking_master",$a2,"visitor='$visitor_id'");

                $response["message"] = ''.$xml->string->deleteSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->failed;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['deleteVisitorPermanet'] == "deleteVisitorPermanet") {
           

            $q=$d->selectRow("visitor_status,leave_parcel_at_gate,parcel_collect_master_id","visitors_master","parent_visitor_id='$visitor_id' OR visitor_id = '$visitor_id' ");
            $vDdata=mysqli_fetch_array($q);
            if ($vDdata['leave_parcel_at_gate']==1) {
                $d->delete("parcel_collect_master","parcel_collect_master_id='$vDdata[parcel_collect_master_id]'");
            }

            $m->set_data('visitor_id', $visitor_id);
            $m->set_data('user_id', $user_id);
            $m->set_data('deleted_by', $gatekeeper_id);
            $m->set_data('visitor_status', '5');
            $m->set_data('deleted_time', date("Y-m-d H:i:s"));

            $a = array(
                'visitor_status' => $m->get_data('visitor_status'),
                'deleted_time' => $m->get_data('deleted_time'),
                'deleted_by' => $m->get_data('deleted_by'),
            );


            if ($vDdata['visitor_status']==2) {
                $response["message"]=''.$xml->string->afterVisitoreExityoucanDelete;
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }else {
                // $qdelete = $d->update("visitors_master",$a,"parent_visitor_id='$visitor_id' OR visitor_id = '$visitor_id' AND visitor_status!='2' ");
                $dq = $d->update("visitors_master", $a, "parent_visitor_id='$visitor_id' OR   visitor_id = '$visitor_id'");
            }

            if ($dq == true) {

                $m->set_data('vehicle_no', "");
                $m->set_data('parking_status', '0');
                $m->set_data('visitor', "0");
                $m->set_data('vehicle_type', "0");

                $a2 = array(
                // 'parking_status' => $m->get_data('parking_status'),
                'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                'visitor' => $m->get_data('visitor'));
                
                $d->update("parking_master",$a2,"visitor='$visitor_id'");

                $response["message"] = ''.$xml->string->deleteSucess;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->failed;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getPrvData'] == "getPrvData" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($visitor_mobile, FILTER_VALIDATE_INT) == true) {

            $qA = $d->selectRow("visitors_master.*,visitorSubType.visitor_sub_type_name,visitorSubType.visitor_sub_image","visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=visitors_master.visitor_sub_type_id", "visitor_mobile='$visitor_mobile' AND society_id='$society_id' AND visitor_type='$visitor_type'","ORDER BY visitor_id DESC");

            if (mysqli_num_rows($qA) > 0) {
                $data = mysqli_fetch_array($qA);
                $response["visitor_name"] = html_entity_decode($data['visitor_name']);
                $response["visit_from"] = $data['visit_from'];
                $response["vehicle_no"] = $data['vehicle_no'];
                $response["visitor_sub_type_id"] = $data['visitor_sub_type_id'];
                $response["visit_from"] = html_entity_decode($data['visitor_sub_type_name']);
                
                if ($data['visitor_profile']!='' && $data['visitor_profile']!='delivery_boy.png' && $data['visitor_profile']!='taxi.png' && $data['visitor_profile']!='vendor.png' && $data['visitor_profile']!='visitor_default.png') {
                    $response["visitor_profile"] = $base_url . "img/visitor/" . $data['visitor_profile'];
                } else {
                    $response["visitor_profile"] = '';
                }

               
                if ($data['visitor_sub_image']!='') {
                    $visit_logo = $base_url.'img/visitor_company/'.$data['visitor_sub_image'].'';
                } else {
                    $visit_logo="";
                }
                $response["visit_logo"] = $visit_logo.'';

                $response["message"] = "Get Visitor success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No Visitor Found";
                $response["status"] = "201";
                echo json_encode($response);
            }



        } else if (isset($getMemberSeach) && $getMemberSeach=='getMemberSeach' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

        $response["member"]=array();

         

        $ids = join("','",$recentGpAry); 

        if ($access_blocks!="") {
            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND block_master.block_id IN($access_blocks) AND users_master.user_id NOT IN ('$ids')","ORDER BY users_master.user_first_name ASC");
        } else {
        $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id NOT IN ('$ids')","ORDER BY users_master.user_first_name ASC");
        }


        if (mysqli_num_rows($q3)>0) {

                while($data=mysqli_fetch_array($q3)) {
                $data = array_map("html_entity_decode", $data);

                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['block_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["unit_name"]=$data['unit_name'];
                $member["block_id"]=$data['block_id'];
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['country_code'].' '.$data['user_mobile'];
                $member["public_mobile"]=$data['mobile_for_gatekeeper'];
                $member["visitor_approved"]=$data['visitor_approved'];
                $member["Delivery_cab_approval"]=$data['Delivery_cab_approval'];
                $member["child_gate_approval"]=$data['child_gate_approval'];
                $member["daily_visitor_approval"]="0";
                $member["resource_approval"]="0";

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                
                     array_push($response["member"], $member); 
                }

            $response["message"] =''.$xml->string->getMemberlistSucess;
            $response["status"] = "200";
            echo json_encode($response);
            
         } else {
            $response["message"] =''.$xml->string->faildGetlist;
            $response["status"] = "201";
            echo json_encode($response);
        }


    } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
