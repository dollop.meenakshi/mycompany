<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
            if($_POST['getBuildingResource']=="getBuildingResource" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
                    if ($emp_type_id!="" && $emp_type_id!=0) {
                        $appendQuery = " AND employee_master.emp_type_id='$emp_type_id'";
                    }
               
                
                $qemployee=$d->selectRow("employee_master.*,emp_type_master.emp_type_icon,emp_type_master.emp_type_name,(SELECT COUNT(*) FROM employee_rating_master WHERE employee_rating_master.emp_id = employee_master.emp_id ) AS total_rating ","employee_master LEFT JOIN emp_type_master ON employee_master.emp_type_id=emp_type_master.emp_type_id","employee_master.private_resource=0 AND employee_master.society_id='$society_id' AND employee_master.emp_type_id!=0 AND employee_master.emp_status=1  $appendQuery " ,"ORDER BY employee_master.emp_name ASC LIMIT 1000");


                if(mysqli_num_rows($qemployee)>0){
                    $response["building_resource"] = array();

                while($data_employee_list=mysqli_fetch_array($qemployee)) {

                        $today = date("Y-m-d");
                        $last_time = date("Y-m-d", strtotime($data_employee_list['in_out_time']));

                        $earlier = new DateTime($today);
                        $later = new DateTime($last_time);
                        $diff = $later->diff($earlier)->format("%a");

                        if ($diff>29) {
                            $color_status='3';
                        } else if ($diff>15) {
                            $color_status='2';
                        } else {
                             $color_status='1';
                        }

                   
                        $building_resource = array(); 

                       
                        

                       
                        $building_resource["emp_id"]=$data_employee_list['emp_id'];
                        $building_resource["society_id"]=$data_employee_list['society_id'];
                        $building_resource["emp_type_id"]=$data_employee_list['emp_type_id'];
                        $building_resource["emp_type"]=$data_employee_list['emp_type'];
                        $building_resource["emp_type_name"]=$data_employee_list['emp_type_name'];
                        $building_resource["color_status"] = $color_status;
                        $building_resource["last_in"] = time_elapsed_string($data_employee_list['in_out_time']);

                        $building_resource["emp_type_icon"]=$base_url."/img/emp_icon/".$data_employee_list['emp_type_icon'];
                        $building_resource["emp_name"]=$data_employee_list['emp_name'];
                        $building_resource["emp_mobile"]=$data_employee_list['emp_mobile'];
                        $building_resource["country_code"]=$data_employee_list['country_code'];
                        $building_resource["emp_address"]=$data_employee_list['emp_address'];
                        $building_resource["emp_id_proof"]=$data_employee_list['emp_id_proof'];
                        $building_resource["emp_id_proof1"]=$data_employee_list['emp_id_proof1'];
                        if ($data_employee_list['emp_id_proof']!='') {
                        $building_resource["emp_id_proof_view"]=$base_url."img/emp_id/".$data_employee_list['emp_id_proof'];
                        }else {
                            $building_resource["emp_id_proof_view"]="";
                        }
                        if ($data_employee_list['emp_id_proof1']!='') {
                        $building_resource["emp_id_proof_view1"]=$base_url."img/emp_id/".$data_employee_list['emp_id_proof1'];
                        }else {
                            $building_resource["emp_id_proof_view1"]="";
                        }
                        $building_resource["emp_date_of_joing"]=$data_employee_list['emp_date_of_joing'];
                        $building_resource["emp_profile"]=$base_url."img/emp/".$data_employee_list['emp_profile'];
                        $building_resource["emp_profile_old"]=$data_employee_list['emp_profile'];
                        $building_resource["emp_id_proof_old"]=$data_employee_list['emp_id_proof'];
                        $building_resource["emp_id_proof_old1"]=$data_employee_list['emp_id_proof1'];
                        $building_resource["created_by"]=$data_employee_list['gatekeeper_id'];

                        $uq=$d->select("unit_master,block_master,users_master,employee_unit_master","employee_unit_master.unit_id=unit_master.unit_id AND   
                       employee_unit_master.user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  employee_unit_master.emp_id ='$data_employee_list[emp_id]' ","GROUP BY employee_unit_master.unit_id ORDER BY users_master.user_id DESC");
                        if ($data_employee_list['emp_type']==1) {
                        $building_resource["working_units_count"]='Working Units : '. mysqli_num_rows($uq);
                        } else {

                        $building_resource["working_units_count"]='';
                        }


                        $qr_size = "300x300";
                        $qr_content =$data_employee_list['emp_mobile'];
                        $qr_correction = strtoupper('H');
                        $qr_encoding = 'UTF-8';
                        $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                        $building_resource['qr_code'] = $qrImageUrl;
                        $building_resource['qr_code_ios'] = $qrImageUrl . '.png';

                        $unitNameAray = array();
                        $userNameAray = array();
                        $workingUnitIds = array();
                        
                        $qu=$d->select("unit_master,block_master,employee_unit_master","unit_master.block_id=block_master.block_id AND unit_master.unit_id=employee_unit_master.unit_id AND employee_unit_master.emp_id='$data_employee_list[emp_id]'");  

                        while ($unitData=mysqli_fetch_array($qu)) {
                            array_push($userNameAray,$unitData['user_id']);
                        }   

                         $qu1=$d->select("unit_master,block_master,employee_unit_master","unit_master.block_id=block_master.block_id AND unit_master.unit_id=employee_unit_master.unit_id AND employee_unit_master.emp_id='$data_employee_list[emp_id]'","");
                        while ($unitData11=mysqli_fetch_array($qu1)) {
                            array_push($unitNameAray,$unitData11['block_name'].'-'.$unitData11['unit_name']);
                            array_push($workingUnitIds,$unitData11['unit_id']);
                        }   

                        $building_resource["working_units_ids"]=implode(",", $workingUnitIds);
                        $building_resource["working_units"]=implode(",", array_unique($unitNameAray));
                        $building_resource["working_user_id"]=implode(",", $userNameAray);


                        array_push($response["building_resource"], $building_resource);

                     // }

                }
                $response["message"]=''.$xml->string->employeelist;
                $response["status"]="200";
                echo json_encode($response);

            }else{

                $response["message"]=''.$xml->string->noEmployeefound;
                $response["status"]="201";
                echo json_encode($response);

            }
                

        } else  if($_POST['getEmployeeType']=="getEmployeeType" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                $qemployee_type=$d->select("emp_type_master","society_id='$society_id' ");

                

                if(mysqli_num_rows($qemployee_type)>0){

                    $response["employee_Type"] = array();

                    while($data_employee_type=mysqli_fetch_array($qemployee_type)) {

                        $employee_Type = array(); 

                        $employee_Type["emp_type_id"]=$data_employee_type['emp_type_id'];
                        $emp_type_id=$data_employee_type['emp_type_id'];

                        $employee_Type["society_id"]=$society_id;
                        $employee_Type["emp_type_name"]=$data_employee_type['emp_type_name'];
                        $employee_Type["emp_type_status"]=$data_employee_type['emp_type_status'];
                        $employee_Type["emp_type_icon"]=$base_url."/img/emp_icon/".$data_employee_type['emp_type_icon'];

                        array_push($response["employee_Type"], $employee_Type); 
                    }

                    $response["message"]= ''.$xml->string->EmptypeSucess;
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]=''.$xml->string->noEmptypeFound;
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else  if($_POST['addNewResource']=="addNewResource" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                $qss=$d->select("society_master"," society_id ='$society_id'");
                $societyData = mysqli_fetch_array($qss); 
                $employee_id_required = $societyData['employee_id_required'];    

                if ($employee_id_required==0 && !file_exists($_FILES["emp_id_proof"]["tmp_name"])) {
                    $response["message"] = ''.$xml->string->idProofReqired;
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                
                }   

                if ($country_code!="") {
                    $appendQuery = " AND country_code='$country_code'";
                }


                    $qselect=$d->select("employee_master","country_code='$country_code' AND emp_mobile='$emp_mobile' AND society_id='$society_id' AND emp_type_id='$emp_type_id' $appendQuery");
                    $user_data = mysqli_fetch_array($qselect);
                    if($user_data==true){
                    $emp_type_id= $user_data['emp_type_id'];
                        if ($emp_type_id==0) {
                            $empTypeName = "Security Guard";
                        } else  {
                            $qc=$d->selectRow("emp_type_name","emp_type_master","emp_type_id='$emp_type_id'");
                            $empData=mysqli_fetch_array($qc);
                            $empTypeName= $empData['emp_type_name'];

                        }

                   
                    if ($empTypeName=="Security Guard") {
                        $response["message"]=''.$xml->string->categoryNotavilableforYou;
                    } else {
                        $response["message"]=''.$xml->string->categoryUnitviewDetail;
                    }

                    $response["status"]="203";
                    echo json_encode($response);
                    exit();

                    
                  }


                    $qc=$d->select("emp_type_master","emp_type_id='$emp_type_id'");
                    $empData=mysqli_fetch_array($qc);
                    $empTypeName= $empData['emp_type_name'];

                    $file11=$_FILES["emp_id_proof"]["tmp_name"];
                    if(file_exists($file11)) {
                        $image_Arr = $_FILES['emp_id_proof'];   
                        $temp = explode(".", $_FILES["emp_id_proof"]["name"]);
                        $emp_id_proof = $emp_mobile.'_id_'.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["emp_id_proof"]["tmp_name"], "../img/emp_id/".$emp_id_proof);
                    } else {
                     $emp_id_proof='';
                    }

                    $file111=$_FILES["emp_id_proof1"]["tmp_name"];
                    if(file_exists($file111)) {
                        $image_Arr = $_FILES['emp_id_proof1'];   
                        $temp = explode(".", $_FILES["emp_id_proof1"]["name"]);
                        $emp_id_proof1 = $emp_mobile.'_id1_'.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["emp_id_proof1"]["tmp_name"], "../img/emp_id/".$emp_id_proof1);
                    } else {
                     $emp_id_proof1='';
                    }

                    $file22=$_FILES["emp_profile"]["tmp_name"];
                    if(file_exists($file22)) {
                        $image_Arr = $_FILES['emp_profile'];   
                        $temp = explode(".", $_FILES["emp_profile"]["name"]);
                        $emp_profile = $emp_mobile.'_profile_'.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["emp_profile"]["tmp_name"], "../img/emp/".$emp_profile);
                    } else {
                     $emp_profile='user.png';
                    }

                  
                    $emp_name = html_entity_decode($emp_name);
                    

                    $m->set_data('society_id',$society_id);
                    $m->set_data('emp_type_id',$emp_type_id);
                    $m->set_data('emp_name',$emp_name);
                    $m->set_data('emp_mobile',$emp_mobile);
                    $m->set_data('country_code',$country_code);
                    $m->set_data('emp_address',$emp_address);
                    $m->set_data('emp_profile',$emp_profile);
                    $m->set_data('emp_id_proof',$emp_id_proof);
                    $m->set_data('emp_id_proof1',$emp_id_proof1);
                    $m->set_data('emp_date_of_joing',date('Y-m-d'));
                    $m->set_data('emp_type',$emp_type);
                    $m->set_data('unit_id',$unit_id);
                     $m->set_data('gatekeeper_id',$my_id);


                   
                   
                      $a1= array (
                        'society_id'=> $m->get_data('society_id'),
                        'emp_type_id'=> $m->get_data('emp_type_id'),
                        'emp_name'=> $m->get_data('emp_name'),
                        'emp_mobile'=> $m->get_data('emp_mobile'),
                        'country_code'=> $m->get_data('country_code'),
                        'emp_address'=> $m->get_data('emp_address'),
                        'emp_profile'=> $m->get_data('emp_profile'),
                        'emp_id_proof'=> $m->get_data('emp_id_proof'),
                        'emp_id_proof1'=> $m->get_data('emp_id_proof1'),
                        'emp_date_of_joing'=> $m->get_data('emp_date_of_joing'),
                        'emp_type'=> $m->get_data('emp_type'),
                        'unit_id'=> $m->get_data('unit_id'),
                        'gatekeeper_id'=> $m->get_data('gatekeeper_id'),
                    );
                    $q=$d->insert("employee_master",$a1);
                    $emp_id=$con->insert_id;


                if ($q>0) {
                    if($emp_type==1) {

                       
                        $userIdAry=explode(",",$user_id);
                        for ($i=0; $i <count($userIdAry) ; $i++) { 

                            $qqq=$d->selectRow("unit_id,user_token,device","users_master","user_id='$userIdAry[$i]' ","ORDER BY user_id DESC");
                            $userData=mysqli_fetch_array($qqq);
                            $unit_id = $userData['unit_id'];
                            $user_token = $userData['user_token'];
                            $device = $userData['device'];

                             $m->set_data('unit_id',$unit_id);
                             $m->set_data('user_id',$userIdAry[$i]);
                             $m->set_data('society_id',$society_id);
                             $m->set_data('emp_id', $emp_id);

                              $a = array(
                                'unit_id'=>$m->get_data('unit_id'),
                                'user_id'=>$m->get_data('user_id') ,
                                'society_id'=>$m->get_data('society_id') ,
                                'emp_id'=>$m->get_data('emp_id') 
                              );
                            $qchek = $d->selectRow("unit_id","employee_unit_master","unit_id='$unit_id' AND user_id='$userIdAry[$i]' AND emp_id='$emp_id'");
                            if (mysqli_num_rows($qchek)<1 && $userIdAry[$i]!=0) {
                                $q=$d->insert("employee_unit_master",$a);
                                $title =$emp_name." ($empTypeName) resource added in your resources";
     
                                  $notiAry = array(
                                  'society_id'=>$society_id,
                                  'user_id'=>$userIdAry[$i],
                                  'notification_title'=>$title,
                                  'notification_desc'=>"By Security Guard ".$gatekeeper_name,    
                                  'notification_date'=>date('Y-m-d H:i'),
                                  'notification_action'=>'employees',
                                  'notification_logo'=>'buildingResourcesNew.png',
                                  );
                                  $d->insert("user_notification",$notiAry);
                         
                                if ($device=='android') {
                                    $nResident->noti("StaffFragment","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'StaffFragment');
                                 } else {
                                    $nResident->noti_ios("ResourcesVC","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'ResourcesVC');
                                 }
                            }
                        }

                    }
                
                

                    $response["message"]=''.$xml->string->newResourceAdd;
                    $response["status"]="200";
                    echo json_encode($response);
                }else{
                    $response["message"]=''.$xml->string->somthingWrong;
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else  if($_POST['editResource']=="editResource" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                $qss=$d->select("society_master"," society_id ='$society_id'");
                $societyData = mysqli_fetch_array($qss); 
                $employee_id_required = $societyData['employee_id_required']; 

                $qe=$d->selectRow("emp_id_proof","employee_master","emp_id='$emp_id'");
                $oldDataEmp = mysqli_fetch_array($qe);
                $emp_id_proof_old = $oldDataEmp['emp_id_proof'];

                if ($employee_id_required==0 && $_FILES["emp_id_proof"]["tmp_name"]=="" && $emp_id_proof_old=="") {
                    
                    $response["message"] = ''.$xml->string->idProofReqired;
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                
                }  

                if ($country_code!="") {
                    $appendQuery = " AND country_code='$country_code'";
                }

                $qselect=$d->select("employee_master","country_code='$country_code' AND emp_mobile='$emp_mobile' AND society_id='$society_id' AND emp_type_id='$emp_type_id'  AND emp_id!='$emp_id' $appendQuery");
                   $user_data = mysqli_fetch_array($qselect);
                    if($user_data==true){
                    $emp_type_id= $user_data['emp_type_id'];
                        if ($emp_type_id==0) {
                            $empTypeName = "Security Guard";
                        } else  {
                            $qc=$d->selectRow("emp_type_name","emp_type_master","emp_type_id='$emp_type_id'");
                            $empData=mysqli_fetch_array($qc);
                            $empTypeName= $empData['emp_type_name'];

                        }


                    $response["message"]=''.$xml->string->categoryUnitviewDetail;
                    $response["status"]="203";
                    echo json_encode($response);
                    exit();

                    
                  }

                    $file11=$_FILES["emp_id_proof"]["tmp_name"];
                    if(file_exists($file11)) {
                        $image_Arr = $_FILES['emp_id_proof'];   
                        $temp = explode(".", $_FILES["emp_id_proof"]["name"]);
                        $emp_id_proof = $emp_mobile.'_id_'.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["emp_id_proof"]["tmp_name"], "../img/emp_id/".$emp_id_proof);
                    } else {
                     $emp_id_proof=$emp_id_proof_old;
                    }

                    $file111=$_FILES["emp_id_proof1"]["tmp_name"];
                    if(file_exists($file111)) {
                        $image_Arr = $_FILES['emp_id_proof1'];   
                        $temp = explode(".", $_FILES["emp_id_proof1"]["name"]);
                        $emp_id_proof1 = $emp_mobile.'_id1_'.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["emp_id_proof1"]["tmp_name"], "../img/emp_id/".$emp_id_proof1);
                    } else {
                     $emp_id_proof1=$emp_id_proof_old1;
                    }

                    $file22=$_FILES["emp_profile"]["tmp_name"];
                    if(file_exists($file22)) {
                        $image_Arr = $_FILES['emp_profile'];   
                        $temp = explode(".", $_FILES["emp_profile"]["name"]);
                        $emp_profile = $emp_mobile.'_profile_'.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["emp_profile"]["tmp_name"], "../img/emp/".$emp_profile);
                    } else {
                     $emp_profile=$emp_profile_old;
                    }
                    
                    $qc=$d->select("emp_type_master","emp_type_id='$emp_type_id'");
                    $empData=mysqli_fetch_array($qc);
                    $empTypeName= $empData['emp_type_name'];

                    $emp_name = html_entity_decode($emp_name);
                    
                    
                    $m->set_data('society_id',$society_id);
                    $m->set_data('emp_type_id',$emp_type_id);
                    $m->set_data('emp_name',$emp_name);
                    $m->set_data('emp_address',$emp_address);
                    $m->set_data('emp_profile',$emp_profile);
                    $m->set_data('emp_id_proof',$emp_id_proof);
                    $m->set_data('emp_id_proof1',$emp_id_proof1);
                    $m->set_data('emp_type',$emp_type);
                    $m->set_data('emp_mobile',$emp_mobile);
                    $m->set_data('country_code',$country_code);
                    $m->set_data('gatekeeper_id',$my_id);

                   
                     $a1= array (
                        'society_id'=> $m->get_data('society_id'),
                        'emp_type_id'=> $m->get_data('emp_type_id'),
                        'emp_name'=> $m->get_data('emp_name'),
                        'emp_address'=> $m->get_data('emp_address'),
                        'emp_mobile'=> $m->get_data('emp_mobile'),
                        'country_code'=> $m->get_data('country_code'),
                        'emp_profile'=> $m->get_data('emp_profile'),
                        'emp_id_proof'=> $m->get_data('emp_id_proof'),
                        'emp_id_proof1'=> $m->get_data('emp_id_proof1'),
                        'emp_type'=> $m->get_data('emp_type'),
                        'gatekeeper_id'=> $m->get_data('gatekeeper_id'),
                    );

                    $q=$d->update("employee_master",$a1,"emp_id='$emp_id' AND society_id='$society_id' AND gatekeeper_id='$my_id'");

                if ($q>0) {

                    if ($emp_type==0) {
                        $d->delete("employee_unit_master","emp_id='$emp_id'");
                        $d->delete("employee_schedule","emp_id='$emp_id' ");
                    } else if($emp_type==1) {
                        // $d->delete("employee_unit_master","emp_id='$emp_id'");

                        $userIdAryRemove=explode(",",$user_id_remove);
                        for ($i11=0; $i11 <count($userIdAryRemove) ; $i11++) { 

                            $qqq1=$d->selectRow("unit_id,user_token,device","users_master","user_id='$userIdAryRemove[$i11]' ","ORDER BY user_id DESC");
                            $userData=mysqli_fetch_array($qqq1);
                            $unit_id = $userData['unit_id'];
                            $user_token = $userData['user_token'];
                            $device = $userData['device'];

                            $d->delete("employee_unit_master","unit_id='$unit_id' AND user_id='$userIdAryRemove[$i11]' AND emp_id='$emp_id'");

                            $title =$emp_name."($empTypeName) resource removed from your resources";
     
                              $notiAry = array(
                              'society_id'=>$society_id,
                              'user_id'=>$userIdAryRemove[$i11],
                              'notification_title'=>$title,
                              'notification_desc'=>"By Security Guard ".$gatekeeper_name,    
                              'notification_date'=>date('Y-m-d H:i'),
                              'notification_action'=>'employees',
                              'notification_logo'=>'buildingResourcesNew.png',
                              );
                              $d->insert("user_notification",$notiAry);
                     
                            if ($device=='android') {
                                $nResident->noti("StaffFragment","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'StaffFragment');
                             } else {
                                $nResident->noti_ios("ResourcesVC","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'ResourcesVC');
                             }
                        }

                        
                        $userIdAry=explode(",",$user_id);
                        for ($i=0; $i <count($userIdAry) ; $i++) { 

                            $qqq=$d->selectRow("unit_id,user_token,device","users_master","user_id='$userIdAry[$i]' ","ORDER BY user_id DESC");
                            $userData=mysqli_fetch_array($qqq);
                            $unit_id = $userData['unit_id'];
                            $user_token = $userData['user_token'];
                            $device = $userData['device'];

                             $m->set_data('unit_id',$unit_id);
                             $m->set_data('user_id',$userIdAry[$i]);
                             $m->set_data('society_id',$society_id);
                             $m->set_data('emp_id', $emp_id);

                              $a = array(
                                'unit_id'=>$m->get_data('unit_id'),
                                'user_id'=>$m->get_data('user_id') ,
                                'society_id'=>$m->get_data('society_id') ,
                                'emp_id'=>$m->get_data('emp_id') 
                              );
                               $qchek = $d->selectRow("unit_id","employee_unit_master","unit_id='$unit_id' AND user_id='$userIdAry[$i]' AND emp_id='$emp_id'");
                            if (mysqli_num_rows($qchek)<1 && $userIdAry[$i]!=0) {
                                $q=$d->insert("employee_unit_master",$a);
                                 $title =$emp_name." ($empTypeName) resource added in your resources";
     
                                  $notiAry = array(
                                  'society_id'=>$society_id,
                                  'user_id'=>$userIdAry[$i],
                                  'notification_title'=>$title,
                                  'notification_desc'=>"By Security Guard ".$gatekeeper_name,    
                                  'notification_date'=>date('Y-m-d H:i'),
                                  'notification_action'=>'employees',
                                  'notification_logo'=>'buildingResourcesNew.png',
                                  );
                                  $d->insert("user_notification",$notiAry);
                         
                                if ($device=='android') {
                                    $nResident->noti("StaffFragment","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'StaffFragment');
                                 } else {
                                    $nResident->noti_ios("ResourcesVC","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'ResourcesVC');
                                 }
                                
                            }
                        }

                    }

                   

                    $response["message"]=''.$xml->string->empDataupdate;
                    $response["status"]="200";
                    echo json_encode($response);
                }else{
                    $response["message"]=''.$xml->string->somthingWrong;
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else  if($_POST['deleteResource']=="deleteResource" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true   && filter_var($emp_id, FILTER_VALIDATE_INT) == true && filter_var($my_id, FILTER_VALIDATE_INT) == true ){



                $qc=$d->selectRow('emp_name,emp_id',"employee_master","emp_id='$emp_id' and gatekeeper_id='$my_id'");
                $empData=mysqli_fetch_array($qc);
                $emp_name = $empData['emp_name'];
                $empId = $empData['emp_id'];


                
                if ($empData>0) {

                    $qca=$d->select("employee_unit_master","society_id='$society_id' AND emp_id='$emp_id' ");
                     
                    if (mysqli_num_rows($qca)>0 ) {
                       
                      while ($unitD=mysqli_fetch_array($qca)) {
                         $qqq=$d->selectRow("unit_id,user_token,device","users_master","user_id='$unitD[user_id]' ","ORDER BY user_id DESC");
                        $userData=mysqli_fetch_array($qqq);
                        $unit_id = $userData['unit_id'];
                        $user_token = $userData['user_token'];
                        $device = $userData['device'];
                            
                        $title =$emp_name." resource deleted";
                        $description= "By Security Guard ".$gatekeeper_name;

                          $notiAry = array(
                          'society_id'=>$society_id,
                          'user_id'=>$unitD['user_id'],
                          'notification_title'=>$title,
                          'notification_desc'=>$description,    
                          'notification_date'=>date('Y-m-d H:i'),
                          'notification_action'=>'employees',
                          'notification_logo'=>'buildingResourcesNew.png',
                          );
                          $d->insert("user_notification",$notiAry);
                 
                        if ($device=='android') {
                            $nResident->noti("StaffFragment","",$society_id,$user_token,$title,$description,'StaffFragment');
                         } else {
                            $nResident->noti_ios("ResourcesVC","",$society_id,$user_token,$title,$description,'ResourcesVC');
                         }

                      }
                    }  



                    $q=$d->delete("employee_unit_master","emp_id='$emp_id' AND unit_id='$unit_id'");
                    $d->delete("employee_schedule","emp_id='$emp_id' AND unit_id='$unit_id'");
                    $d->delete("employee_rating_master","emp_id='$emp_id' AND unit_id='$unit_id'");

                    // $d->delete("employee_master","emp_id='$emp_id'");
                    
                    $a22 =array(
                            'emp_status'=> 0,
                            'private_resource'=> 0,
                          'entry_status'=>0,
                    );
                    $d->update("employee_master",$a22,"emp_id='$emp_id'");

                    $m->set_data('emp_id', $emp_id);
                    $m->set_data('society_id', $society_id);
                     $temDate = date("Y-m-d H:i:s");
                    $m->set_data('visit_exit_date_time', $temDate);
                    $m->set_data('visit_status', '1');
                    $aDe = array(
                        'visit_exit_date_time' => $m->get_data('visit_exit_date_time'),
                        'visit_status' => $m->get_data('visit_status'),
                        'exit_by' => "",
                    );

                    $qselect=$d->select("staff_visit_master","emp_id = '$emp_id'","ORDER BY staff_visit_id DESC");

                    $data = mysqli_fetch_array($qselect);

                    $qdelete = $d->update("staff_visit_master",$aDe,"emp_id = '$emp_id' AND staff_visit_id = '$data[staff_visit_id]' OR emp_id = '$emp_id' AND visit_status=0");

                    $response["message"]=''.$xml->string->empDeleted;
                    $response["status"]="200";
                    echo json_encode($response);
                }else{
                    $response["message"]=''.$xml->string->somthingWrong;
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else if ($_POST['getResourceUnitList'] == "getResourceUnitList" && filter_var($emp_id, FILTER_VALIDATE_INT) == true ) {



            $uq=$d->select("unit_master,block_master,users_master,employee_unit_master","employee_unit_master.unit_id=unit_master.unit_id AND  
                       employee_unit_master.user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  employee_unit_master.emp_id ='$emp_id' ","GROUP BY employee_unit_master.unit_id ORDER BY users_master.user_id DESC");

            $response["working_units"] = array();

            if (mysqli_num_rows($uq) > 0) {

                while ($data = mysqli_fetch_array($uq)) {

                    

                    $working_units = array();

                    $working_units["employee_unit_id"] = $data['unit_id'];
                    $working_units["emp_id"] = $data['emp_id'];
                    $working_units["society_id"] = $data['society_id'];
                    $working_units["unit_name"] = $data['block_name'].'-'.$data['unit_name'];
                    $working_units["user_name"] = $data['user_full_name'];
                    $working_units["unit_id"] = $data['unit_id'];
                    $working_units["user_id"] = $data['user_id'];

                    
                    array_push($response["working_units"], $working_units);
                }

                $response["message"] = ''.$xml->string->getUnit;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] =''.$xml->string->noUnitfound;
                $response["status"] = "200";
                echo json_encode($response);
            }


        }else if ($_POST['deleteSingleUnit']=="deleteSingleUnit" && filter_var($emp_id, FILTER_VALIDATE_INT) == true && filter_var($employee_unit_id, FILTER_VALIDATE_INT) == true ){

                    $qc=$d->selectRow('emp_name',"employee_master","emp_id='$emp_id' ");
                    $empData=mysqli_fetch_array($qc);
                    $emp_name = $empData['emp_name'];
                        
                    $qqq=$d->selectRow("unit_id","employee_unit_master","employee_unit_id='$employee_unit_id' AND emp_id='$emp_id'");
                    $unitData = mysqli_fetch_array($qqq);

                    $qca=$d->select("employee_unit_master","society_id='$society_id' AND emp_id='$emp_id' AND unit_id='$employee_unit_id'");
                    if (mysqli_num_rows($qca)>0 && $employee_unit_id!=0) {
                          
                      while ($unitD=mysqli_fetch_array($qca)) {

                        $qqq=$d->selectRow("unit_id,user_token,device","users_master","user_id='$unitD[user_id]' ","ORDER BY user_id DESC");
                        $userData=mysqli_fetch_array($qqq);
                        $unit_id = $userData['unit_id'];
                        $user_token = $userData['user_token'];
                        $device = $userData['device'];

                            # code...
                        $title =$emp_name." resource deleted";
                        $description= "By Security Guard ".$gatekeeper_name;

                          $notiAry = array(
                          'society_id'=>$society_id,
                          'user_id'=>$unitD['user_id'],
                          'notification_title'=>$title,
                          'notification_desc'=>$description,    
                          'notification_date'=>date('Y-m-d H:i'),
                          'notification_action'=>'employees',
                          'notification_logo'=>'buildingResourcesNew.png',
                          );
                          $d->insert("user_notification",$notiAry);
                 
                        if ($device=='android') {
                            $nResident->noti("StaffFragment","",$society_id,$user_token,$title,$description,'StaffFragment');
                         } else {
                            $nResident->noti_ios("ResourcesVC","",$society_id,$user_token,$title,$description,'ResourcesVC');
                         }

                      }
                    }  
                       
                     $qdelete= $d->delete("employee_unit_master","society_id='$society_id' AND unit_id='$employee_unit_id' AND emp_id='$emp_id'");
                
                        if($qdelete==TRUE){

                            $unit_id = $unitData['unit_id'];

                            $d->delete("employee_schedule","emp_id='$emp_id' AND unit_id='$unit_id'");
                            $d->delete("employee_rating_master","emp_id='$emp_id' AND unit_id='$unit_id'");

                            $response["message"]=''.$xml->string->unitDeleted;
                            $response["status"]="200";

                            echo json_encode($response);
        
                        }else{
        
                            $response["message"]=''.$xml->string->$somthingWrong;
                            $response["status"]="201";
                            echo json_encode($response);
        
                        }
        
            }else if ($_POST['addSingleUnit'] == "addSingleUnit" && filter_var($emp_id, FILTER_VALIDATE_INT) == true ) {

                         $qc=$d->select("emp_type_master,employee_master","emp_type_master.emp_type_id=employee_master.emp_type_id AND employee_master.emp_id='$emp_id'");
                        $empData=mysqli_fetch_array($qc);
                        $empTypeName= $empData['emp_type_name'];

                        $userIdAry=explode(",",$user_id);
                        for ($i=0; $i <count($userIdAry) ; $i++) { 

                            $qqq=$d->selectRow("unit_id,user_token,device","users_master","user_id='$userIdAry[$i]' ","ORDER BY user_id DESC");
                            $userData=mysqli_fetch_array($qqq);
                            $unit_id = $userData['unit_id'];
                            $user_token = $userData['user_token'];
                            $device = $userData['device'];


                             $m->set_data('unit_id',$unit_id);
                             $m->set_data('user_id',$userIdAry[$i]);
                             $m->set_data('society_id',$society_id);
                             $m->set_data('emp_id', $emp_id);

                              $a = array(
                                'unit_id'=>$m->get_data('unit_id'),
                                'user_id'=>$m->get_data('user_id') ,
                                'society_id'=>$m->get_data('society_id') ,
                                'emp_id'=>$m->get_data('emp_id') 
                              );

                             $qca=$d->select("employee_unit_master","society_id='$society_id' AND unit_id='$unit_id' AND user_id='$userIdAry[$i]' AND emp_id='$emp_id'");
                              if (mysqli_num_rows($qca)==0 && $unit_id!=0) {
                                $q=$d->insert("employee_unit_master",$a);

                                $title =$emp_name." ($empTypeName) resource added in your resources !";
     
                                  $notiAry = array(
                                  'society_id'=>$society_id,
                                  'user_id'=>$userIdAry[$i],
                                  'notification_title'=>$title,
                                  'notification_desc'=>"By Security Guard ".$gatekeeper_name,    
                                  'notification_date'=>date('Y-m-d H:i'),
                                  'notification_action'=>'employees',
                                  'notification_logo'=>'buildingResourcesNew.png',
                                  );
                                  $d->insert("user_notification",$notiAry);
                         
                                if ($device=='android') {
                                    $nResident->noti("StaffFragment","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'StaffFragment');
                                 } else {
                                    $nResident->noti_ios("ResourcesVC","",$society_id,$user_token,$title,"By Security Guard ".$gatekeeper_name,'ResourcesVC');
                                 }

                              }
                        }



                $response["message"] =''.$xml->string->unitAdded;
                $response["status"] = "200";
                echo json_encode($response);
                exit();


        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);

        }

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>