<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


   
    if ($key==$keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
            if($_POST['addEyeEmployeeData']=="addEyeEmployeeData" && $employee_id!=''){




                $scannedTime=date('Y-m-d H:i:s');
                $day=date("dHi");

                $uploadedFile=$_FILES["eye_photo"]["tmp_name"];
                $ext = pathinfo($_FILES['eye_photo']['name'], PATHINFO_EXTENSION);
                    if(file_exists($uploadedFile)) {
                       
                      $sourceProperties = getimagesize($uploadedFile);
                      $newFileName = $day.$employee_id;
                      $dirPath = "../img/gatekeeper_eye/";
                      $imageType = $sourceProperties[2];
                      $imageHeight = $sourceProperties[1];
                      $imageWidth = $sourceProperties[0];
                      if ($imageWidth>1000) {
                        $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage /100;
                        $newImageHeight = $imageHeight * $newWidthPercentage /100;
                      } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                      }

                      switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagepng($tmp,$dirPath. $newFileName. "_eye.". $ext);
                            break;           

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagejpeg($tmp,$dirPath. $newFileName. "_eye.". $ext);
                            break;
                        
                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagegif($tmp,$dirPath. $newFileName. "_eye.". $ext);
                            break;

                        default:
                          
                            break;
                        }
                    $eye_photo= $newFileName."_eye.".$ext;

                }  else {
                    $eye_photo="";
                }

                $m->set_data('emp_id', $employee_id);
                $m->set_data('scannedTime', $scannedTime);
                $m->set_data('society_id', $society_id);
                $m->set_data('eye_photo', $eye_photo);
                 $m->set_data('place_latitude', $place_latitude);
                $m->set_data('place_longitue', $place_longitue);



                $a = array(
                    'society_id'=>$m->get_data('society_id'),
                    'emp_id'=>$m->get_data('emp_id'),
                    'scannedTime'=>$m->get_data('scannedTime'),
                    'eye_photo'=>$m->get_data('eye_photo'),
                    'place_latitude'=>$m->get_data('place_latitude'),
                    'place_longitue'=>$m->get_data('place_longitue'),
                );


                $q = $d->insert("gatekeeper_eye_report", $a);


                $response["message"]=$reportSucess;
                $response["status"]="200";
                echo json_encode($response);

            }else if($_POST['addQrEmployeeData']=="addQrEmployeeData" && $employee_id!=''){

                if ($qr_data=='') {
                    $response["message"]=''.$xml->string->invalidQr;
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }   


                $qrArray = explode("-",$qr_data);
                
                $orgQr = $qrArray[1];

                $qt=$d->select("qr_code_master","society_id='$society_id' AND qr_code_master_id='$orgQr'");
                if (mysqli_num_rows($qt)==0) {
                   $response["message"]=''.$xml->string->invalidQr;
                   $response["status"]="201";
                   echo json_encode($response);
                   exit();
                }


               

                $scannedTime=date('Y-m-d H:i:s');
                $day=date("dHi");

                $m->set_data('emp_id', $employee_id);
                $m->set_data('scannedTime', $scannedTime);
                $m->set_data('society_id', $society_id);
                $m->set_data('qr_code_id', $orgQr);
                $m->set_data('report_type', 1);
                $m->set_data('place_latitude', $place_latitude);
                $m->set_data('place_longitue', $place_longitue);


                $a = array(
                    'society_id'=>$m->get_data('society_id'),
                    'emp_id'=>$m->get_data('emp_id'),
                    'report_type'=>$m->get_data('report_type'),
                    'scannedTime'=>$m->get_data('scannedTime'),
                    'qr_code_id'=>$m->get_data('qr_code_id'),
                    'place_latitude'=>$m->get_data('place_latitude'),
                    'place_longitue'=>$m->get_data('place_longitue'),
                );


                $q = $d->insert("gatekeeper_eye_report", $a);


                $response["message"]=''.$xml->string->reportSucess;
                $response["status"]="200";
                echo json_encode($response);

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>