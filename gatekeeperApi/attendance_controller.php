<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
    $societyLngName=  $xml->string->society;

    if ($key==$keydb ) { 
    
        $response = array();
        extract(array_map("test_input" , $_POST));

        $currentDate = date('Y-m-d');

        if($_POST['getEmployeeList']=="getEmployeeList" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $qemployee=$d->selectRow("employee_master.*,emp_type_master.emp_type_icon,emp_type_master.emp_type_name,employee_face_data_master.face_id,employee_face_data_master.face_data_image,employee_face_data_master.face_added_date","employee_master LEFT JOIN emp_type_master ON employee_master.emp_type_id=emp_type_master.emp_type_id LEFT JOIN employee_face_data_master ON employee_face_data_master.emp_id=employee_master.emp_id AND employee_face_data_master.emp_type=1","employee_master.private_resource=0 AND employee_master.society_id='$society_id' AND employee_master.emp_type_id!=0 AND employee_master.emp_status=1  " ,"ORDER BY emp_name ASC LIMIT 5000");


                if(mysqli_num_rows($qemployee)>0){
                    $response["building_resource"] = array();

                while($data_employee_list=mysqli_fetch_array($qemployee)) {

                        
                        $building_resource = array(); 

                        $building_resource["emp_id"]=$data_employee_list['emp_id'];
                        $building_resource["emp_type"]='1';
                        $building_resource["emp_type_name"]=$data_employee_list['emp_type_name'];
                        $building_resource["emp_type_icon"]=$base_url."/img/emp_icon/".$data_employee_list['emp_type_icon'];
                        $building_resource["emp_name"]=$data_employee_list['emp_name'];
                        $building_resource["emp_mobile"]=$data_employee_list['emp_mobile'];
                        $building_resource["country_code"]=$data_employee_list['country_code'];
                        if ($data_employee_list['emp_profile']!="") {
                            $building_resource["emp_profile"] = $base_url."img/emp/".$data_employee_list['emp_profile'];
                        } else {
                            $building_resource["emp_profile"] = "";
                        }
                        if ($data_employee_list['face_id']>0) {
                           $building_resource["face_added"] = true;
                           $building_resource["face_added_date"] = $data_employee_list['face_added_date'];
                           $building_resource["face_image"] = $base_url."img/emp/".$data_employee_list['face_data_image'];
                        } else {
                            $building_resource["face_added"] = false;
                            $building_resource["face_added_date"] = "";
                            $building_resource["face_image"] = "";
                        }
                        

                        array_push($response["building_resource"], $building_resource);

                     // }

                }

            }

            $qd = $d->selectRow("daily_visitors_master.*,visitorSubType.visitor_sub_image,employee_face_data_master.face_id,employee_face_data_master.face_data_image,employee_face_data_master.face_added_date","daily_visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=daily_visitors_master.visitor_sub_type_id LEFT JOIN employee_face_data_master ON employee_face_data_master.emp_id=daily_visitors_master.visitor_id AND employee_face_data_master.emp_type=2", "daily_visitors_master.visitor_type  = '4' AND daily_visitors_master.society_id='$society_id' AND daily_visitors_master.daily_active_status =0 ", "ORDER BY daily_visitors_master.visitor_name ASC");


                if(mysqli_num_rows($qd)>0){
                    $response["daily_visitor"] = array();

                while($data_employee_list=mysqli_fetch_array($qd)) {

                        
                        $daily_visitor = array(); 

                        $daily_visitor["emp_id"]=$data_employee_list['visitor_id'];
                        $daily_visitor["emp_type"]='2';
                        $daily_visitor["emp_type_name"]=$data_employee_list['visit_from'];
                        $daily_visitor["emp_type_icon"]=$base_url."/img/visitor_company/".$data_employee_list['visitor_sub_image'];
                        $daily_visitor["emp_name"]=$data_employee_list['visitor_name'];
                        $daily_visitor["emp_mobile"]=$data_employee_list['visitor_mobile'];
                        $daily_visitor["country_code"]=$data_employee_list['country_code'];

                        if ($data_employee_list['visitor_profile']!="") {
                            $daily_visitor["emp_profile"] = $base_url."img/visitor/".$data_employee_list['visitor_profile'];
                        } else {
                            $daily_visitor["emp_profile"] = "";
                        }

                         if ($data_employee_list['face_id']>0) {
                           $daily_visitor["face_added"] = true;
                           $daily_visitor["face_added_date"] = $data_employee_list['face_added_date'];
                           $daily_visitor["face_image"] = $base_url."img/emp/".$data_employee_list['face_data_image'];
                        } else {
                            $daily_visitor["face_added"] = false;
                            $daily_visitor["face_added_date"] = "";
                            $daily_visitor["face_image"] = "";
                        }
                        

                        array_push($response["daily_visitor"], $daily_visitor);

                     // }

                }

            }

            $response["message"] = "Employee Get Successfully";
            $response["status"] = "200";
            echo json_encode($response);
            exit();

            
        } else if($_POST['addEmployeeFace']=="addEmployeeFace" && $emp_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $face_data_image = "";

            if ($_FILES["face_data_image"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["face_data_image"]["tmp_name"];
                $extId = pathinfo($_FILES['face_data_image']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["face_data_image"]["name"]);
                    $face_data_image = "face_".$emp_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["face_data_image"]["tmp_name"], "../img/emp/" . $face_data_image);
                } else {
                
                    $response["message"] = "Invalid Photo. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $face_data_image_two = "";

            if ($_FILES["face_data_image_two"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["face_data_image_two"]["tmp_name"];
                $extId = pathinfo($_FILES['face_data_image_two']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["face_data_image_two"]["name"]);
                    $face_data_image_two = "face_two_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["face_data_image_two"]["tmp_name"], "../img/emp/" . $face_data_image_two);
                } else {
                
                    $response["message"] = "Invalid Photo. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $m->set_data('emp_id',$emp_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('emp_type',$emp_type);
            $m->set_data('user_face_data',$user_face_data);
            $m->set_data('face_data_image',$face_data_image);
            $m->set_data('face_data_image_two',$face_data_image_two);
            $m->set_data('face_added_by',$gatekeeper_name);
            $m->set_data('face_added_date',date('Y-m-d H:i:s'));

            $a = array(
                'emp_id'=>$m->get_data('emp_id'),
                'society_id'=>$m->get_data('society_id'),
                'emp_type'=>$m->get_data('emp_type'),
                'user_face_data'=>$m->get_data('user_face_data'),
                'face_data_image'=>$m->get_data('face_data_image'),
                'face_data_image_two'=>$m->get_data('face_data_image_two'),
                'face_added_date'=>$m->get_data('face_added_date'),
                'face_added_by'=>$m->get_data('face_added_by'),
            );

            $qd=$d->selectRow("face_id","employee_face_data_master","emp_id = '$emp_id' AND emp_type='$emp_type'");
            if (mysqli_num_rows($qd)>0) {
                $qry = $d->update("employee_face_data_master",$a,"emp_id = '$emp_id' AND emp_type='$emp_type'");
            } else {
                $qry = $d->insert("employee_face_data_master",$a);
            }

            if ($qry == true) {
                
                $response["message"] = "Employee face added";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        } else if($_POST['removeEmployeeFace']=="removeEmployeeFace" && $emp_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

          

            $qry = $d->delete("employee_face_data_master","emp_id = '$emp_id' AND emp_type='$emp_type'");

            if ($qry == true) {
                $response["message"] = "Employee face removed";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        } else if($_POST['getFaceData']=="getFaceData" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            

            $q1=$d->selectRow("employee_face_data_master.*,employee_master.emp_name,employee_master.emp_profile,employee_master.entry_status,emp_type_master.emp_type_icon,emp_type_master.emp_type_name,employee_master.emp_mobile","employee_face_data_master,employee_master LEFT JOIN emp_type_master ON employee_master.emp_type_id=emp_type_master.emp_type_id ","employee_master.emp_id=employee_face_data_master.emp_id AND employee_face_data_master.society_id ='$society_id' AND employee_face_data_master.emp_type=1");
           
            $q2=$d->selectRow("employee_face_data_master.*,daily_visitors_master.visitor_profile,daily_visitors_master.visitor_name,daily_visitors_master.visitor_status,visitorSubType.visitor_sub_image,visitorSubType.visitor_sub_type_name,daily_visitors_master.visitor_sub_type_id,daily_visitors_master.visitor_mobile","employee_face_data_master,daily_visitors_master LEFT JOIN visitorSubType on visitorSubType.visitor_sub_type_id=daily_visitors_master.visitor_sub_type_id","daily_visitors_master.daily_active_status=0 AND daily_visitors_master.visitor_id=employee_face_data_master.emp_id AND employee_face_data_master.society_id ='$society_id' AND employee_face_data_master.emp_type=2");
          
           // $unit_data=$d->selectRow("employee_face_data_master.*,employee_master.emp_name,employee_master.emp_profile,daily_visitors_master.visitor_profile,daily_visitors_master.visitor_name,daily_visitors_master.visitor_status,employee_master.entry_status","employee_face_data_master LEFT JOIN employee_master ON employee_master.emp_id=employee_face_data_master.emp_id AND employee_face_data_master.emp_type=1 LEFT JOIN daily_visitors_master ON daily_visitors_master.visitor_id=employee_face_data_master.emp_id AND employee_face_data_master.emp_type=2","employee_face_data_master.society_id ='$society_id'");
            $response["employees"] =array();

            if (mysqli_num_rows($q1) > 0) {


                while($data_units_list=mysqli_fetch_array($q1)) {
                     
                    $employees = array(); 

                    $data_units_list = array_map("html_entity_decode", $data_units_list);
                    $employees["emp_id"]=$data_units_list['emp_id'].'';
                    $employees["society_id"]=$data_units_list['society_id'].'';
                    $employees["emp_type"]='1';
                    $employees["face_id"]=$data_units_list['face_id'].'';
                    $employees["user_face_data"]=$data_units_list['user_face_data'].'';
                    $employees["visitor_sub_type_id"]=$data_units_list['visitor_sub_type_id'].'';
                    $employees["emp_mobile"]=$data_units_list['emp_mobile'].'';
                    $employees["face_added_date"]=date("d M Y h:i A", strtotime($data_units_list['face_added_date'].''));

                    $employees["emp_type_name"]=$data_units_list['emp_type_name'];
                    $employees["emp_type_icon"]=$base_url."/img/emp_icon/".$data_units_list['emp_type_icon'];

                    if ($data_units_list['emp_type']==1) {
                        if ($data_units_list['emp_profile']!='') {
                            $employees["emp_profile"] = $base_url."img/emp/".$data_units_list['emp_profile'];
                        } else {
                            $employees["emp_profile"]= "";
                        }
                        $employees["emp_name"]=$data_units_list['emp_name'];
                        if ($data_units_list['entry_status']==1) {
                            $employees["in_status"] = true;
                        } else {
                            $employees["in_status"] = false;
                        }

                    } else if($data_units_list['emp_type']==2)  {
                        if ($data_units_list['visitor_profile']!="") {
                            $employees["emp_profile"] = $base_url."img/visitor/".$data_units_list['visitor_profile'];
                        } else {
                             $employees["emp_profile"]= "";
                        }
                        $employees["emp_name"]=$data_units_list['visitor_name'];

                        if ($data_units_list['visitor_status']==1) {
                            $employees["in_status"] = true;
                        } else {
                            $employees["in_status"] = false;
                        }
                    }
                    if ($data_units_list['face_data_image'] != '') {
                        $employees["face_data_image"] = $base_url . "img/emp/" . $data_units_list['face_data_image'];
                    } else {
                        $employees["face_data_image"] = "";
                    }

                    if ($data_units_list['face_data_image_two'] != '' && file_exists("../img/emp/$data_units_list[face_data_image_two]")) {
                        $employees["face_data_image_two"] = $base_url . "img/emp/" . $data_units_list['face_data_image_two'];
                        array_push($response["employees"], $employees);
                    } 

                }
        
            }

            if (mysqli_num_rows($q2) > 0) {

                

                while($data_units_list=mysqli_fetch_array($q2)) {
                     
                    $employees = array(); 

                    $data_units_list = array_map("html_entity_decode", $data_units_list);
                    $employees["emp_id"]=$data_units_list['emp_id'].'';
                    $employees["society_id"]=$data_units_list['society_id'].'';
                    $employees["emp_type"]=2;
                    $employees["face_id"]=$data_units_list['face_id'].'';
                    $employees["user_face_data"]=$data_units_list['user_face_data'].'';
                    $employees["face_added_date"]=date("d M Y h:i A", strtotime($data_units_list['face_added_date'].''));

                    $employees["emp_mobile"]=$data_units_list['visitor_mobile'];
                    $employees["visitor_sub_type_id"]=$data_units_list['visitor_sub_type_id'];
                    $employees["emp_type_name"]=$data_units_list['visitor_sub_type_name'];
                    $employees["emp_type_icon"]=$base_url."/img/visitor_company/".$data_units_list['visitor_sub_image'];

                    if ($data_units_list['emp_type']==1) {
                        if ($data_units_list['emp_profile']!='') {
                            $employees["emp_profile"] = $base_url."img/emp/".$data_units_list['emp_profile'];
                        } else {
                            $employees["emp_profile"]= "";
                        }
                        $employees["emp_name"]=$data_units_list['emp_name'];
                        if ($data_units_list['entry_status']==1) {
                            $employees["in_status"] = true;
                        } else {
                            $employees["in_status"] = false;
                        }

                    } else if($data_units_list['emp_type']==2)  {
                        if ($data_units_list['visitor_profile']!="") {
                            $employees["emp_profile"] = $base_url."img/visitor/".$data_units_list['visitor_profile'];
                        } else {
                             $employees["emp_profile"]= "";
                        }
                        $employees["emp_name"]=$data_units_list['visitor_name'];

                        if ($data_units_list['visitor_status']==1) {
                            $employees["in_status"] = true;
                        } else {
                            $employees["in_status"] = false;
                        }
                    }
                    if ($data_units_list['face_data_image'] != '') {
                        $employees["face_data_image"] = $base_url . "img/emp/" . $data_units_list['face_data_image'];
                    } else {
                        $employees["face_data_image"] = "";
                    }

                    if ($data_units_list['face_data_image_two'] != '' && file_exists("../img/emp/$data_units_list[face_data_image_two]")) {
                        $employees["face_data_image_two"] = $base_url . "img/emp/" . $data_units_list['face_data_image_two'];
                        array_push($response["employees"], $employees);
                    } 

                }
        
            }

            if (mysqli_num_rows($q1) > 0 || mysqli_num_rows($q2) > 0) {
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }

            else{

                $response["message"]="Get Member Fail.";
                $response["status"]="201";
                echo json_encode($response);
            }
        } else if ($_POST['allowEntryExitFace'] == "allowEntryExitFace"  && $emp_id!='') {

                $temDate = date("Y-m-d H:i:s");
                $filter_data = date("Y-m-d");

            if ($emp_type==1) {
                // employee...
                
                $m->set_data('emp_id', $emp_id);
                $m->set_data('society_id', $society_id);
                $m->set_data('visit_entry_date_time', $temDate);
                $m->set_data('filter_data', $temFilter);
                $m->set_data('in_temperature', $in_temperature);
                $m->set_data('in_with_mask', $in_with_mask);
                $m->set_data('visit_exit_date_time', $temDate);
                $m->set_data('out_temperature', $out_temperature);
                $m->set_data('out_with_mask', $out_with_mask);
                $m->set_data('filter_data', $filter_data);

                $qselect=$d->select("staff_visit_master","emp_id = '$emp_id' AND visit_status=0","ORDER BY staff_visit_id DESC");

                if (mysqli_num_rows($qselect)>0) {
                    // Exit Allow
                    $data = mysqli_fetch_array($qselect);
                    $datePunchIN = $data['visit_entry_date_time'];

                    $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
                    $datePunchOUT = date('Y-m-d H:i:s');

                    if ($datePunchOUT < $datePunchIN) {
                        $response["message"] = "Immediate exit is restricted, please exit after 1 minute.";
                        $response["status"] = "201";
                        echo json_encode($response);
                        exit();
                    }

                    $a = array(
                        'visit_exit_date_time' => $m->get_data('visit_exit_date_time'),
                        'visit_status' => 1,
                        'out_temperature' => $m->get_data('out_temperature'),
                        'out_with_mask' => $m->get_data('out_with_mask'),
                        'exit_by' => $gatekeeper_name,
                    );

                    $qdelete = $d->update("staff_visit_master",$a,"emp_id = '$emp_id' AND staff_visit_id = '$data[staff_visit_id]' OR emp_id = '$emp_id' AND visit_status=0");

                    $m->set_data('entry_status', '2');
                    $m->set_data('in_out_time', date('d-m-Y h:i A'));

                    $a2 = array(
                        'entry_status' => $m->get_data('entry_status'),
                        'in_out_time' => $m->get_data('in_out_time'),
                    );

                    $d->update("employee_master",$a2,"emp_id = '$emp_id'");

                   

                    $title= $emp_name.' ('.$emp_type_name.') Exit Allowed';
                    $description = "Exit Allowed by Security Guard-$gatekeeper_name";
                    
                    $qUserToken=$d->select("users_master,unit_master,employee_unit_master","employee_unit_master.active_status=0 AND employee_unit_master.unit_id=users_master.unit_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND employee_unit_master.emp_id='$emp_id' AND employee_unit_master.user_id!=0 AND employee_unit_master.user_id=users_master.user_id ");
                  
                   
                    while($data_notification=mysqli_fetch_array($qUserToken)) {

                      $sos_user_token=$data_notification['user_token'];
                      
                      $notiAry = array(
                      'society_id'=>$society_id,
                      'user_id'=>$data_notification['user_id'],
                      'notification_title'=>$title,
                      'notification_desc'=>$description,    
                      'notification_date'=>date('Y-m-d H:i'),
                      'notification_action'=>'resource',
                      'notification_logo'=>'buildingResourcesNew.png',
                      );

                      $device=$data_notification['device'];
                        if ($device=='android') {
                           $nResident->noti("StaffFragment","",$society_id,$sos_user_token,$title,$description,'resource');
                        }  else if($device=='ios') {
                           $nResident->noti_ios("ResourcesVC","",$society_id,$sos_user_token,$title,$description,'resource');
                        }
                      $d->insert("user_notification",$notiAry);
                    }


                    $adminFcmArray= $d->selectAdminInOut($emp_id,"android",'0');
                    $adminFcmArrayIos= $d->selectAdminInOut($emp_id,"ios",'0');

                    $nAdmin->noti_new($society_id,"", $adminFcmArray, $title, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");
                    $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $title, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");

                    $response["emp_id"] = $emp_id;
                    $response["emp_type"] = $emp_type;
                    $response["in_out_status"] = "1";
                    $response["attendance_id"] = $data['staff_visit_id'];
                    $response["message"] = 'Goodbye '.$emp_name;

                } else {
                    // Entry Allow
                    $a = array(
                        'emp_id' => $m->get_data('emp_id'),
                        'society_id' => $m->get_data('society_id'),
                        'visit_entry_date_time' => $m->get_data('visit_entry_date_time'),
                        'visit_status' => 0,
                        'filter_data' => $m->get_data('filter_data'),
                        'in_temperature' => $m->get_data('in_temperature'),
                        'in_with_mask' => $m->get_data('in_with_mask'),
                        'entry_by' => $gatekeeper_name,
                    );
                   
                    $qdelete = $d->insert("staff_visit_master",$a);
                    $staff_visit_id = $con->insert_id;

                    $m->set_data('entry_status', '1');
                    $m->set_data('in_out_time', date('d-m-Y h:i A'));

                    $a2 = array(
                        'entry_status' => $m->get_data('entry_status'),
                        'in_out_time' => $m->get_data('in_out_time'),
                    );

                    $d->update("employee_master",$a2,"emp_id = '$emp_id'");

                    $title= $title.' ('.$emp_type_name.') has Arrived';
                    $description = "Entry allowed by Security Guard-$gatekeeper_name";
                 
                    $qUserToken=$d->select("users_master,unit_master,employee_unit_master","employee_unit_master.active_status=0 AND employee_unit_master.unit_id=users_master.unit_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND employee_unit_master.emp_id='$emp_id' AND employee_unit_master.user_id!=0 AND employee_unit_master.user_id=users_master.user_id ");
                    // $ids = join("','",$unitIdAry);   
                    // $qUserToken=$d->select("users_master","society_id='$society_id' AND user_token!='' AND unit_id IN ('$ids')");
                    while($data_notification=mysqli_fetch_array($qUserToken)) {

                      $sos_user_token=$data_notification['user_token'];
                      
                      $notiAry = array(
                      'society_id'=>$society_id,
                      'user_id'=>$data_notification['user_id'],
                      'notification_title'=>$title,
                      'notification_desc'=>$description,    
                      'notification_date'=>date('Y-m-d H:i'),
                      'notification_action'=>'resource',
                      'notification_logo'=>'buildingResourcesNew.png',
                      );

                      $device=$data_notification['device'];
                        if ($device=='android') {
                           $nResident->noti("StaffFragment","",$society_id,$sos_user_token,$title,$description,'resource');
                        }  else if($device=='ios') {
                           $nResident->noti_ios("ResourcesVC","",$society_id,$sos_user_token,$title,$description,'resource');
                        }
                      $d->insert("user_notification",$notiAry);
                    }
                    
                    $adminFcmArray= $d->selectAdminInOut($emp_id,"android",'0');
                    $adminFcmArrayIos= $d->selectAdminInOut($emp_id,"ios",'0');

                    $nAdmin->noti_new($society_id,"", $adminFcmArray, $title, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");
                    $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $title, $description, "employee?manageEmployeeUnit=yes&emp_id=$emp_id");

                    $empType = $row['emp_type_name'];
                    if ($empType=='Technician') {
                         // assing new complain
                        $technician_id_old = $emp_id;
                        $openComplain = $d->count_data_direct("complain_id","complains_master,unit_master","complains_master.unit_id=unit_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.flag_delete!=2 AND complains_master.complain_status!='1' AND complains_master.flag_delete=0 AND complains_master.technician_id='$technician_id_old'");

                        if ($technician_id_old>0 && $openComplain<1) {
                            $compCategoryArrayEmp = array();
                            $cuCHeck=$d->select("employee_master,employee_complaint_category","employee_master.emp_id=employee_complaint_category.emp_id AND employee_master.emp_id='$technician_id_old' AND employee_master.emp_status=1");
                            while ($oldCat=mysqli_fetch_array($cuCHeck)) {
                                array_push($compCategoryArrayEmp , $oldCat['complaint_category_id']);
                            }

                            $ids = join("','",$compCategoryArrayEmp); 
                            $openComplainArray = array();
                            $openComQuery=$d->selectRow("complains_master.complain_id,complains_master.complaint_category","complains_master,unit_master","complains_master.unit_id=unit_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.flag_delete!=2 AND complains_master.complain_status!='1' AND complains_master.flag_delete=0 AND complains_master.technician_id=0 AND complains_master.complaint_category IN ('$ids')","ORDER BY complains_master.complain_id ASC");
                            $compData=mysqli_fetch_array($openComQuery);
                            $complain_id_next_assing = $compData['complain_id'];
                            
                            if($complain_id_next_assing >0) {
                                $anew= array (
                                    'technician_id'=> $technician_id_old,
                                );

                                $d->update("complains_master",$anew,"complain_id='$complain_id_next_assing'");

                                $gt=$d->selectRow("compalain_title,complain_assing_to","complains_master","complain_id='$complain_id_next_assing'");
                                $getCompData = mysqli_fetch_array($gt);
                                $complain_assing_to = $getCompData['complain_assing_to'];
                                $compalain_title = $getCompData['compalain_title'];

                                $qToken=$d->selectRow("technician_token","employee_master","emp_id='$technician_id_old' AND technician_token!=''");
                                $techData=mysqli_fetch_array($qToken);
                                $technician_token = $techData['technician_token'];
                                $title = "New Complaint Assigned For $complain_assing_to";
                                if ($technician_token!="") {
                                    $nGaurd->noti_new("",$technician_token,$title,$compalain_title,"0");

                                    $notiAryTech = array(
                                        'society_id' => $society_id,
                                        'guard_notification_title' => $title,
                                        'guard_notification_desc' => $compalain_title,
                                        'employee_id' => $technician_id_old,
                                        'guard_notification_date' => date('Y-m-d H:i'),
                                        'click_action' => '0',
                                        'notification_logo'=>'ComplaintsNew.png',
                                    );
                                    $d->insert("guard_notification_master", $notiAryTech);
                                }
                            }   
                              
                          } 

                    }

                    $response["emp_id"] = $emp_id;
                    $response["emp_type"] = $emp_type;
                    $response["in_out_status"] = "0";
                    $response["attendance_id"] = $staff_visit_id;
                    $response["message"] = 'Welcome '.$emp_name;
                }

            } else if ($emp_type==2) {
                // daily visitor...
                $visit_date = date("Y-m-d");
                $visit_time = date("H:i:s");
                $daily_visitor_id = $emp_id;
                $gd=$d->selectRow("valid_till,country_code,visitor_profile,visitor_status","daily_visitors_master","visitor_id='$daily_visitor_id'");
                $visitorData= mysqli_fetch_array($gd);
                $valid_till = $visitorData['valid_till'];
                $visitor_status = $visitorData['visitor_status'];
                $country_code = $visitorData['country_code'];
                $visitor_profile = $visitorData['visitor_profile'];
                $expire = strtotime($valid_till);
                $today = strtotime("today midnight");
                if($today > $expire){
                   $response["message"] =''.$xml->string->validDateExprie;
                   $response["status"] = "201";
                   echo json_encode($response);
                   exit();
                }
                
                $visitor_name = html_entity_decode($visitor_name);
                $visit_from = html_entity_decode($visit_from);

                $os= $d->selectRow("overstay_alert_minutes","visitorMainType","visitor_main_type_id='4' AND overstay_alert_minutes>0");
                $overStayData = mysqli_fetch_array($os);
                $overstay_alert_minutes = $overStayData['overstay_alert_minutes'];
                if ($overstay_alert_minutes>0) {
                    // code...
                    $overstay_time_limit = date("Y-m-d H:i:s",strtotime($temDate." +$overstay_alert_minutes minutes"));
                } 

                $m->set_data('society_id', $society_id);
                $m->set_data('visitor_type', 4);
                $m->set_data('visit_from', $emp_type_name);
                $m->set_data('visitor_name', $emp_name);
                $m->set_data('visitor_mobile', $visitor_mobile);
                $m->set_data('country_code', $country_code);
                $m->set_data('visitor_profile', $visitor_profile);
                $m->set_data('number_of_visitor', 1);
                $m->set_data('visit_date', $visit_date);
                $m->set_data('visit_time', $visit_time);
                $m->set_data('daily_visitor_id', $daily_visitor_id);
                $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);

                $m->set_data('in_temperature', $in_temperature);
                $m->set_data('in_with_mask', $in_with_mask);

                $m->set_data('out_temperature', $out_temperature);
                $m->set_data('out_with_mask', $out_with_mask);
                $m->set_data('overstay_time_limit', $overstay_time_limit);


                

                $userIdAry= array();
                $qc = $d->select("daily_visitor_unit_master", "active_status=0 AND daily_visitor_id  = '$daily_visitor_id' AND society_id='$society_id' AND active_status=0", "");
                while($data=mysqli_fetch_array($qc)){
                    array_push($userIdAry, $data['user_id']);
                }

                $ids = join("','",$userIdAry);

                $blockAry = array();
                $duCheck=$d->select("employee_block_master","emp_id='$gatekeeper_id' AND emp_id!=0");
                while ($oldBlock=mysqli_fetch_array($duCheck)) {
                  array_push($blockAry , $oldBlock['block_id']);
                }
                $ids11 = join("','",$blockAry);   
                if ($ids11!='') {
                  $appendQuery = " AND block_id IN ('$ids11')";
                  $appendQuer11 = " AND users_master.block_id IN ('$ids11')";
                }
                     

                $fcmArray=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$ids') $appendQuery");
                $fcmArrayIos=$d->get_android_fcm("users_master","user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$ids') $appendQuery");
                
                $append_query = "users_master.user_id IN ('$ids') $appendQuer11";

                    if ($visitor_status==0) {

                        $a1 = array(
                            'visitor_status'=>1,
                        );

                        $title= "$emp_name ($emp_type_name) has Arrived at Gate";
                        $desc= "Entry allowed by Security Guard-$gatekeeper_name";
                        // for out
                         $a = array(
                            'society_id'=>$m->get_data('society_id'),
                            'visitor_type'=>$m->get_data('visitor_type'),
                            'visit_from'=>$m->get_data('visit_from'),
                            'visitor_name'=>$m->get_data('visitor_name'),
                            'country_code'=>$m->get_data('country_code'),
                            'visitor_mobile'=>$m->get_data('visitor_mobile'),
                            'visitor_profile'=>$m->get_data('visitor_profile'),
                            'number_of_visitor'=>$m->get_data('number_of_visitor'),
                            'visit_date' => $m->get_data('visit_date'),
                            'visit_time' => $m->get_data('visit_time'),
                            'visitor_status' => 2,
                            'daily_visitor_id' => $m->get_data('daily_visitor_id'),
                            'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                            'in_temperature' => $m->get_data('in_temperature'),
                            'in_with_mask' => $m->get_data('in_with_mask'),
                            'overstay_time_limit' => $m->get_data('overstay_time_limit'),
                        );

                        $q = $d->insert("visitors_master", $a);
                        $attendance_id = $con->insert_id;

                        $adminFcmArray= $d->selectAdminInOut($daily_visitor_id,"android",'1');
                        $adminFcmArrayIos= $d->selectAdminInOut($daily_visitor_id,"ios",'1');

                        $nAdmin->noti_new($society_id,"", $adminFcmArray, $title, $desc, "dailyVisitors?manageVisitor=yes&visitor_id=$daily_visitor_id");
                        $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $title, $desc, "dailyVisitors?manageVisitor=yes&visitor_id=$daily_visitor_id");

                         $d->insertUserNotification($society_id,$title,$desc,"visitor","serviceProviderNew.png",$append_query);

                        $nResident->noti("VisitorFragment","",$society_id,$fcmArray,$title,$desc,'visitor');
                        $nResident->noti_ios("VisitorVC","",$society_id,$fcmArrayIos,$title,$desc,'visitor');

                        $d->update("daily_visitors_master",$a1,"visitor_mobile='$visitor_mobile'");

                        $response["emp_id"] = $emp_id;
                        $response["emp_type"] = $emp_type;
                        $response["in_out_status"] = "0";
                        $response["attendance_id"] = $attendance_id;
                        $response["message"] ='Welcome '.$emp_name;
                        $response["status"] = "200";
                        echo json_encode($response);
                        exit();

                    } else if ($visitor_status==1) { 

                        $a1 = array(
                            'visitor_status'=>0,
                        );

                        $fq=$d->selectRow("visitor_id,visit_date,visit_time","visitors_master","daily_visitor_id='$emp_id'","ORDER BY visitor_id DESC");
                        $visitData=mysqli_fetch_array($fq);
                        $visitor_id = $visitData['visitor_id'];

                        $datePunchIN = $visitData['visit_date'].' '.$visitData['visit_time'];

                        $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
                        $datePunchOUT = date('Y-m-d H:i:s');

                        if ($datePunchOUT < $datePunchIN) {
                            $response["message"] = "Immediate exit is restricted, please exit after 1 minute.";
                            $response["status"] = "201";
                            echo json_encode($response);
                            exit();
                        }

                        $title= "$emp_name ($emp_type_name) Exit Allowed";
                        $desc= "Exit allowed by Security Guard-$gatekeeper_name";
                        // for in  0
                        $a = array(
                            'exit_date' => $m->get_data('visit_date'),
                            'exit_time' => $m->get_data('visit_time'),
                            'visitor_status' => "3",
                            'out_temperature' => $m->get_data('out_temperature'),
                            'out_with_mask' => $m->get_data('out_with_mask'),
                        );
                        $q = $d->update("visitors_master", $a,"visitor_id='$visitor_id'  ORDER BY visitor_id DESC ");
                       
                        
                        $adminFcmArray= $d->selectAdminInOut($daily_visitor_id,"android",'1');
                        $adminFcmArrayIos= $d->selectAdminInOut($daily_visitor_id,"ios",'1');

                        $nAdmin->noti_new($society_id,"", $adminFcmArray, $title, $desc, "dailyVisitors?manageVisitor=yes&visitor_id=$daily_visitor_id");
                        $nAdmin->noti_ios_new($society_id,"", $adminFcmArrayIos, $title, $desc, "dailyVisitors?manageVisitor=yes&visitor_id=$daily_visitor_id");

                         $d->insertUserNotification($society_id,$title,$desc,"visitor","serviceProviderNew.png",$append_query);

                        $nResident->noti("VisitorFragment","",$society_id,$fcmArray,$title,$desc,'visitor');
                        $nResident->noti_ios("VisitorVC","",$society_id,$fcmArrayIos,$title,$desc,'visitor');

                        $d->update("daily_visitors_master",$a1,"visitor_mobile='$visitor_mobile'");

                        $response["emp_id"] = $emp_id;
                        $response["emp_type"] = $emp_type;
                        $response["in_out_status"] = "1";
                        $response["attendance_id"] = $visitor_id;
                        $response["message"] ='Goodbye '.$emp_name;
                        $response["status"] = "200";
                        echo json_encode($response);
                        exit();
                        
                    } else {
                       $response["message"] ='Something went wrong';
                       $response["status"] = "201";
                       echo json_encode($response);
                       exit(); 
                    }


            } else {
                $response["message"] = ''.$xml->string->allowedFailMsg;
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
                

            if ($qdelete == true) {
                
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = ''.$xml->string->allowedFailMsg;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if($_POST['cancelAttendance']=="cancelAttendance" && $attendance_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            if ($in_out_status == "0" && $emp_type==1) {

                $a2 = array(
                    'entry_status' => 0,
                    'in_out_time' => "",
                );

                $d->update("employee_master",$a2,"emp_id = '$emp_id' AND emp_id!=0");


                $qryDelete = $d->delete("staff_visit_master","staff_visit_id = '$attendance_id'");              
                $response["message"]="Entry Cancelled Successfully";
                $response["status"]="200";
                echo json_encode($response);

            }else if ($in_out_status == "1" && $emp_type==1) {

                $a = array(
                    'visit_exit_date_time' => "",
                    'visit_status' => 0,
                    'out_temperature' => "0.00",
                    'out_with_mask' => "",
                    'exit_by' => "",
                );


                $punchOutQryUpdate = $d->update("staff_visit_master",$a,"staff_visit_id = '$attendance_id'");

                $a2 = array(
                    'entry_status' => 1,
                    'in_out_time' => "",
                );

                $d->update("employee_master",$a2,"emp_id = '$emp_id' AND emp_id!=0");

                $response["message"]="Attendance Cancelled";
                $response["status"]="200";
                echo json_encode($response);

            } else if ($in_out_status == "0" && $emp_type==2) {

                $a2 = array(
                    'visitor_status' => 0,
                );
                
                $d->update("daily_visitors_master",$a2,"visitor_id='$emp_id'");


                $qryDelete = $d->delete("visitors_master","visitor_id = '$attendance_id'");  

                $response["message"]="Entry Cancelled Successfully";
                $response["status"]="200";
                echo json_encode($response);

            }else if ($in_out_status == "1" && $emp_type==2) {

                        $a = array(
                            'exit_date' => "",
                            'exit_time' => "",
                            'visitor_status' => "2",
                            'out_temperature' =>"",
                            'out_with_mask' => "",
                        );

                $q = $d->update("visitors_master", $a,"visitor_id='$attendance_id'");

                $a2 = array(
                    'visitor_status' => 1,
                );
                
                $d->update("daily_visitors_master",$a2,"visitor_id='$emp_id'");

               

                $response["message"]="Attendance Cancelled";
                $response["status"]="200";
                echo json_encode($response);

            }else{
                $response["message"]="Something went wrong, try again!";
                $response["status"]="201";
                echo json_encode($response);
            }
        } else {
          $response["message"]="wrong tagg";
            $response["status"]="201";
            echo json_encode($response);  
        }
    }else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}
?>