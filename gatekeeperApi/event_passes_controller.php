<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));

    if ($_POST['passAllow'] == "passAllow" && $pass_id!='') {
                   $today= date("Y-m-d");

          $q1=$d->select("event_passes_master,event_master,event_days_master","event_days_master.event_id=event_master.event_id AND  event_passes_master.event_id=event_master.event_id AND event_passes_master.pass_id='$pass_id' AND entry_status=0  AND event_days_master.event_date='$today' AND event_days_master.events_day_id=event_passes_master.events_day_id" );
          $data=mysqli_fetch_array($q1);
        
             if ($data>0) {
            

                $created_date = date("Y-m-d H:i:s");
                 $qUserToken=$d->select("users_master","user_id='$data[user_id]'");
                 $data_notification=mysqli_fetch_array($qUserToken);
                 $token=$data_notification['user_token'];
                 $device=$data_notification['device'];

                if ($device=='android') {
                   $nResident->noti("","",$society_id,$token,"Pass Verified","at ".$created_date,'scannedQR');
                }  else if($device=='ios') {
                   $nResident->noti_ios("","",$society_id,$token,"Pass Verified","at ".$created_date,'scannedQR');
                }

                $m->set_data('entry_status', 1);
                $a = array('entry_status' => $m->get_data('entry_status'));
                $q = $d->update("event_passes_master",$a,"pass_id='$pass_id'");

                $response["message"] =''.$xml->string->passVErified;
                $response["status"] = "200";
                echo json_encode($response);
              
            } else {
                 $response["message"] =''.$xml->string->invalidDateorPass;
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['getEventList']=="getEventList" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
            $today= date("Y-m-d");
                $qnotification=$d->select("event_master,event_days_master","event_master.event_id=event_days_master.event_id AND event_days_master.event_date='$today' AND event_master.society_id = '$society_id' AND event_master.event_status=1","order by event_master.event_id DESC LIMIT 100");

                if(mysqli_num_rows($qnotification)>0){

                    $response["event"] = array();

                    while($data_notification=mysqli_fetch_array($qnotification)) {
                         
                        $expire = strtotime($data_notification['event_end_date']);
                        $today = strtotime("today midnight");
                         
                        $event_id=$data_notification['event_id'];

                        $event = array(); 
                        $today=date('Y-m-d');
                        $cTime= date("H:i:s");
                        $expire = strtotime($data_notification['event_end_date']);
                        $today = strtotime("today $cTime");
                        if($today > $expire){
                            $eventEnd=0;
                            $event["hide_status"]="0";
                          } else {
                            $eventEnd=1;
                            $event["hide_status"]="1";
                          }
                        $event["event_id"]=$data_notification['event_id'];
                        $id=$data_notification['event_id'];
                        $event["event_title"]=html_entity_decode($data_notification['event_title'].'-'.$data_notification['event_day_name']);
                        $event["event_image"]=$base_url."img/event_image/".$data_notification['event_image'];
                        $event["event_start_date"]=$data_notification['event_start_date'];
                        $event["event_start_date_view"]=date("d M Y h:i A", strtotime($data_notification['event_start_date'].' '.$data_notification['event_time']));
                        $event["event_end_date"]=date("d M Y", strtotime($data_notification['event_end_date']));
                        $event["event_location"]=$data_notification['eventMom'];
                        $event["event_type"]=$data_notification['event_type'];
                        
                        $aq = $d->selectRow("SUM(going_person) as sum","event_attend_list","event_id = '$event_id' AND society_id='$society_id' AND events_day_id='$data_notification[events_day_id]' AND book_status=1");
                        $aData = mysqli_fetch_array($aq);
                        $bookAdult= $aData['sum'];

                        $qc = $d->selectRow("SUM(going_child) as sumchild","event_attend_list","event_id = '$event_id' AND society_id='$society_id' AND events_day_id='$data_notification[events_day_id]' AND book_status=1");
                        $cData = mysqli_fetch_array($qc);
                        $bookChild=  $cData['sumchild'];

                        $qg = $d->selectRow("SUM(going_guest) as sumguest","event_attend_list","event_id = '$event_id' AND society_id='$society_id' AND events_day_id='$data_notification[events_day_id]' AND book_status=1");
                        $gData = mysqli_fetch_array($qg);
                        $bookGuest= $gData['sumguest'];
                        $TotalBooked= $bookAdult+ $bookChild+$bookGuest;

                        $event["TotalBooked"]=$TotalBooked;

                        $event["users"] = array();

                        $qfamily=$d->select("event_attend_list,users_master","event_attend_list.user_id=users_master.user_id AND event_attend_list.event_id='$event_id' AND event_attend_list.society_id='$society_id' and event_attend_list.events_day_id='$data_notification[events_day_id]' ","ORDER BY RAND() LIMIT 5");
                        while ($datafamily = mysqli_fetch_array($qfamily)) {
                            $users = array();
                            $users["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
                            array_push($event["users"], $users);
                        }
                        

                       array_push($response["event"], $event);
                         
                    }

                    $response["message"]=''.$xml->string->getEventSucess;
                    $response["status"]="200";
                    echo json_encode($response);


                }else{

                    $response["message"]=''.$xml->string->noEventfound;
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else if($_POST['checkEventPass']=="checkEventPass" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($event_id, FILTER_VALIDATE_INT) == true ){
                   $today= date("Y-m-d");
                 $eq=$d->select("event_days_master","event_id='$event_id' AND event_date='$today'");
                 $dayData=mysqli_fetch_array($eq);
                 $events_day_id = $dayData['events_day_id'];

                 if ($events_day_id!='' || $events_day_id!=0) {
                  $pendingPass= $d->count_data_direct("pass_id","event_passes_master,event_attend_list","event_passes_master.event_attend_id=event_attend_list.event_attend_id AND event_passes_master.entry_status='0' AND event_passes_master.event_id='$event_id'  AND event_passes_master.events_day_id='$events_day_id'AND event_attend_list.book_status!=3"); 
                 $VerifiedPass= $d->count_data_direct("pass_id","event_passes_master,event_attend_list","event_passes_master.event_attend_id=event_attend_list.event_attend_id AND event_passes_master.entry_status='1' AND event_passes_master.event_id='$event_id'  AND event_passes_master.events_day_id='$events_day_id'AND event_attend_list.book_status!=3"); 
                 } else {
                 $pendingPass= $d->count_data_direct("pass_id","event_passes_master,event_attend_list","event_passes_master.event_attend_id=event_attend_list.event_attend_id AND event_passes_master.entry_status='0' AND event_passes_master.event_id='$event_id' AND event_attend_list.book_status!=3"); 
                 $VerifiedPass= $d->count_data_direct("pass_id","event_passes_master,event_attend_list","event_passes_master.event_attend_id=event_attend_list.event_attend_id AND event_passes_master.entry_status='1' AND event_passes_master.event_id='$event_id' AND event_attend_list.book_status!=3"); 

                 }
                

                 
                $response["totalPass"]=$pendingPass+$VerifiedPass.'';
                $response["pendingPass"]=$pendingPass;
                $response["VerifiedPass"]=$VerifiedPass;
                $response["message"]=''.$xml->string->getPassDetai;
                $response["events_day_id"]=$events_day_id;
                $response["status"]="200";
                echo json_encode($response);


            }else {
             $response["message"] = "wrong tagg.";
            $response["status"] = "201";
            echo json_encode($response);
        }

       


    } else {
        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
