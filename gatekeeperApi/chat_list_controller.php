    <?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb) { 
	$response = array();
	extract(array_map("test_input" , $_POST));

    if($_POST['getMembers']=="getMembers" && $society_id!=''){

        $qe=$d->select("employee_master","emp_id='$my_id' AND society_id='$society_id'");
        $row11=mysqli_fetch_array($qe);
        $blocks_id = $row11['blocks_id'];
        
        if ($blocks_id!="") {
            $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0 AND block_id IN($blocks_id)");
        } else {
            $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0");
        }
        
        if(mysqli_num_rows($block_data)>0){

            $qss=$d->select("society_master"," society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qss); 
            $society_type = $societyData['society_type'];       


            $response["block"] = array();

            while($data_block_list=mysqli_fetch_array($block_data)) {

                $block = array(); 

            $block["block_id"]=$data_block_list['block_id'];
            $block_id=$data_block_list['block_id'];


	      	$block["society_id"]=$data_block_list['society_id'];
	      	$block["block_name"]=$data_block_list['block_name'];

              $block["block_status"]=$data_block_list['block_status'];
              

              $block["floors"] = array();

              $floor_data=$d->select("floors_master","block_id ='$block_id'");

              while($data_floor_list=mysqli_fetch_array($floor_data)) {

                    $floors = array(); 

                    $floors["floor_id"]=$data_floor_list['floor_id'];
                    $floors_id=$data_floor_list['floor_id'];


                    $floors["society_id"]=$data_floor_list['society_id'];
                    $floors["block_id"]=$data_floor_list['block_id'];
                    $floors["floor_name"]=$data_floor_list['floor_name'];
                    $floors["floor_status"]=$data_floor_list['floor_status'];


                    $floors["units"] = array();

                    $unit_data=$d->select("unit_master","floor_id ='$floors_id'");

                    while($data_units_list=mysqli_fetch_array($unit_data)) {

                        $units = array(); 

                        $units["unit_id"]=$data_units_list['unit_id'];
                        $units_id=$data_units_list['unit_id'];


                        $units["society_id"]=$data_units_list['society_id'];
                        $units["floor_id"]=$data_units_list['floor_id'];
                        $units["unit_name"]=$data_units_list['unit_name'];
                        if ($data_units_list['unit_status']==5 || $data_units_list['unit_close_for_gatekeeper']==1) {
                            $unit_status= 5;
                        } else {
                            $unit_status= $data_units_list['unit_status'];
                        }
                        
                        $units["unit_status"]=$unit_status;
                        $units["unit_type"]=$data_units_list['unit_type'].'';


                        $quser_data=$d->select("users_master","delete_status=0 AND user_status!=0 AND unit_id ='$units_id' AND member_status=0","ORDER BY user_id DESC");

                        $user_data=mysqli_fetch_array($quser_data);
                        
                        $user_full_name= $user_data['user_full_name'];
                            

                        $units["user_id"]=$user_data['user_id'].'';
                        $user_id = $user_data['user_id'];

                        $units["family_count"]=$d->count_data_direct("user_id","users_master","delete_status=0 AND unit_id='$units_id' AND society_id='$society_id' AND member_status=1 AND user_status!=0 AND user_type='$user_data[user_type]'");

                        // $qchatCount=$d->select("chat_master","msg_for='$my_id' AND msg_by='$user_id' AND society_id='$society_id' AND msg_status='0'");
                        $units["chat_status"]="0";

                       
                        $units["society_id"]=$data_units_list['society_id'];
                        if ($user_data['user_profile_pic']!="") {
                        $units["user_profile_pic"]=$base_url."img/users/recident_profile/".$user_data['user_profile_pic'];
                        } else {
                        $units["user_profile_pic"]='';
                        }
                        $units["user_full_name"]=$user_full_name.'';
                        $units["user_first_name"]=$user_data['user_first_name'].'';
                        $units["user_last_name"]=$user_data['user_last_name'].'';
                        $units["user_mobile"]=$user_data['user_mobile'].'';
                        $units["country_code"]=$user_data['country_code'].'';
                        $units["user_email"]=$user_data['user_email'].'';
                        $units["user_id_proof"]=$user_data['user_id_proof'].'';
                        $units["user_type"]=$user_data['user_type'].'';
                        $units["user_block_id"]=$user_data['block_id'].'';
                        $units["user_floor_id"]=$user_data['floor_id'].'';
                        $units["user_unit_id"]=$user_data['unit_id'].'';
                        $units["user_status"]=$user_data['user_status'].'';
                        $units["public_mobile"]=$user_data['mobile_for_gatekeeper'].'';
                        $units["child_gate_approval"]=$user_data['child_gate_approval'].'';


                        $qfamily = $d->select("users_master", "delete_status=0 AND user_status!=0 AND unit_id='$units_id'","ORDER BY user_id DESC");
                        $units["member"] = array();
                        $units["family_count"]=mysqli_num_rows($qfamily)."";
                        while ($datafamily = mysqli_fetch_array($qfamily)) {

                            $member = array();
                            if ($datafamily['member_status']==0) {
                                if ($datafamily['user_type']==0) {
                                    $mainUser= "Owner Primary";
                                } else {
                                    $mainUser="Tenant Primary ";
                                }
                                $rel= $mainUser;
                            } else {
                                if ($datafamily['user_type']==0) {
                                    $fType="Owner";
                                } else {
                                    $fType="Tenant";
                                }
                                $rel= $datafamily['member_relation_name'].'-'.$fType;
                            }

                            $member["user_id"] = $datafamily['user_id'];

                            // $qchatCount=$d->select("chat_master","msg_for='$my_id' AND msg_by='$datafamily[user_id]'  AND society_id='$society_id' AND msg_status='0'");
                            // $chatCount=mysqli_num_rows($qchatCount)."";
                            $member["chat_status"]=$chatCount.'';
    
                            $member["member_chat"] = "0";
                            $member["user_first_name"] = $datafamily['user_first_name'];
                            if ($chatCount>0) {
                            $member["user_last_name"] = $datafamily['user_last_name'] .' ('.$rel  .') '.$chatCount.' Message';
                            $units["chat_status"]=$chatCount."";
                            } else {
                            $member["user_last_name"] = $datafamily['user_last_name'] .' ('.$rel  .')';
                            }
                            $member["only_name"] = $datafamily['user_first_name'] .' ('.$rel  .')';
                            $member["user_mobile"] = $datafamily['user_mobile'];
                            $member["country_code"] = $datafamily['country_code'];
                            $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
                            $member["member_date_of_birth"] = $datafamily['member_date_of_birth'];
                            $member["member_age"] = "";
                            $member["member_relation_name"] = $datafamily['member_relation_name'];
                            $member["public_mobile"] = $datafamily['mobile_for_gatekeeper'];
                            $member["user_status"] = $datafamily['user_status'];
                            $member["member_status"] = $datafamily['member_status'];
                            $member["visitor_approved"] = $datafamily['visitor_approved'];
                            $member["delivery_cab_approval"] = $datafamily['Delivery_cab_approval'];

                            array_push($units["member"], $member);

                          }


                        array_push($floors["units"], $units);


                    }

                    array_push($block["floors"], $floors);

                }

	      	
                array_push($response["block"], $block); 
            }



            $qPop=$d->select("unit_master","society_id ='$society_id' AND unit_status='1' OR
            society_id ='$society_id' AND unit_status='3'");

            $response["population"]=mysqli_num_rows($qPop);

            $response["message"]=''.$xml->string->getMembersucess;
            $response["status"]="200";
            echo json_encode($response);
    
        }else{

            $response["message"]=''.$xml->string->getMemberfail;
            $response["status"]="201";
            echo json_encode($response);
    
        }

    }else if($_POST['getBlocks']=="getBlocks" && $society_id!=''){

        $block_data=$d->select("block_master","society_id ='$society_id'");

        if(mysqli_num_rows($block_data)>0){

            $response["block"] = array();

            
            while($data_block_list=mysqli_fetch_array($block_data)) {

                $block = array();

                $block["block_id"]=$data_block_list['block_id'];
                $block["society_id"]=$data_block_list['society_id'];
                $block["block_name"]=$data_block_list['block_name'];
                $block["block_status"]=$data_block_list['block_status'];
                
                array_push($response["block"], $block); 
            }

            $response["message"]=''.$xml->string->$getBlocksucess;
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]=''.$xml->string->getBlockfail;
            $response["status"]="201";
            echo json_encode($response);
        
    }


}else if($_POST['getFloorandUnit']=="getFloorandUnit" && $society_id!='' && $block_id!=''){

        $block_data=$d->select("floors_master","society_id ='$society_id' AND block_id ='$block_id'");


        if(mysqli_num_rows($block_data)>0){
            $response["floors"] = array();

            while($data_floor_list=mysqli_fetch_array($block_data)) {

                $floors = array(); 
                $floors["floor_id"]=$data_floor_list['floor_id'];
                $floors_id=$data_floor_list['floor_id'];
                $floors["society_id"]=$data_floor_list['society_id'];
                $floors["block_id"]=$data_floor_list['block_id'];
                $floors["floor_name"]=$data_floor_list['floor_name'];
                $floors["floor_status"]=$data_floor_list['floor_status'];
                
                $floors["units"] = array();

                $unit_data=$d->select("unit_master","floor_id ='$floors_id'");

                while($data_units_list=mysqli_fetch_array($unit_data)) {

                    $units = array(); 

                    $units["unit_id"]=$data_units_list['unit_id'];
                    $units["society_id"]=$data_units_list['society_id'];
                    $units["floor_id"]=$data_units_list['floor_id'];
                    $units["unit_name"]=$data_units_list['unit_name'];
                    $units["unit_status"]=$data_units_list['unit_status'];
                    $units["unit_type"]=$data_units_list['unit_type'];

                    array_push($floors["units"], $units);


                }

                array_push($block["floors"], $floors);
                array_push($response["floors"], $floors);

            }

            $response["message"]=''.$xml->string->getBlocksucess;
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]=''.$xml->string->getBlockfail;
            $response["status"]="201";
            echo json_encode($response);
        
    }


    } else if (isset($getChatMemberList) && $getChatMemberList=='getChatMemberList' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

        $response["member"]=array();


        
        $blocks_id = $access_blocks;
        
        
       
        $q=$d->getRecentChatGatekeeperToUser("chat_master",$my_id);

        if (mysqli_num_rows($q)>0) {
             $recentGpAry=array();
            while($data=mysqli_fetch_array($q)) {
                $data = array_map("html_entity_decode", $data);
                
               if ($data['msg_for']==$my_id) {
                    $recentUser= $data['msg_by'];
                    
                }else if ($data['msg_by']==$my_id) {
                    $recentUser= $data['msg_for'];
                }

                array_push($recentGpAry, $recentUser);

                $userId= $recentUser;


                $member = array(); 
                $member["user_id"]=$userId;
                if ($data['msg_data']=='' && $data['msg_img']!='') {
                    $member["msg_data"]="📸 Image";
                 }else {
                    $member["msg_data"]=$data['msg_data'].'';
                 }
                $member["msg_date"]=$data['msg_date'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['block_name'];
                $member["floor_name"]=$data['floor_name'].'';
                $member["unit_name"]=$data['unit_name'];
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['country_code'].' '.$data['user_mobile'];
                $member["public_mobile"]=$data['mobile_for_gatekeeper'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'].'';

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";

                $qchatCount=$d->select("chat_master","msg_for='$my_id' AND msg_by='$userId' AND society_id='$society_id' AND msg_status='0'");
                $member["chat_status"]=mysqli_num_rows($qchatCount)."";
                
                if (mysqli_num_rows($cq)>0) {
                    $member["block_status"] = true;
                } else {
                    $member["block_status"] = false;
                }
                
                     array_push($response["member"], $member); 
                }
             $temp=true;
        } else {
             $temp=false;
        }

        $ids = join("','",$recentGpAry); 

        if ($blocks_id!="") {
            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND block_master.block_id IN($blocks_id) AND users_master.user_id NOT IN ('$ids')","ORDER BY users_master.user_first_name ASC");
        } else {
        $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id NOT IN ('$ids')","ORDER BY users_master.user_first_name ASC");
        }


        if (mysqli_num_rows($q3)>0) {

                while($data=mysqli_fetch_array($q3)) {
                $data = array_map("html_entity_decode", $data);
                    
                $cq=$d->select("chat_block_master","society_id='$society_id' AND block_for='$user_id' AND block_by='$data[user_id]' OR society_id='$society_id' AND  block_by='$user_id' AND block_for='$data[user_id]'");

                $userId= $data['user_id'];


                $member = array(); 
                $member["user_id"]=$data['user_id'];
                if ($unData11['msg_data']=='' && $unData11['msg_img']!='') {
                    $member["msg_data"]="📸 Image";
                 }else {
                    $member["msg_data"]=$unData11['msg_data'].'';
                 }
                $member["msg_date"]='1970-01-01 00:01:00';
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['block_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["unit_name"]=$data['unit_name'];
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['country_code'].' '.$data['user_mobile'];
                $member["public_mobile"]=$data['mobile_for_gatekeeper'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";


                $units["chat_status"]=mysqli_num_rows($qchatCount)."";

                $member["member_chat"] = mysqli_num_rows($qchatCount)."";
                if (mysqli_num_rows($cq)>0) {
                    $member["block_status"] = true;
                } else {
                    $member["block_status"] = false;
                }
                
                     array_push($response["member"], $member); 
                }

               
            $temp=true;
         }else {
            $temp=false; 
         }

        if ($temp==true || $temp1 ==true) {
            $response["message"] =''.$xml->string->getMemberlistSucess;
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] =''.$xml->string->faildGetlist;
            $response["status"] = "201";
            echo json_encode($response);
        }


    } else if (isset($getChatMemberListNew) && $getChatMemberListNew=='getChatMemberListNew' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

        $response["member"]=array();
        

       
        $blocks_id = $access_blocks;

        if ($blocks_id!="") {
            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND block_master.block_id IN($blocks_id) ","ORDER BY users_master.user_first_name ASC");
        } else {
        $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 ","ORDER BY users_master.user_first_name ASC");
        }


        if (mysqli_num_rows($q3)>0) {

                while($data=mysqli_fetch_array($q3)) {

                $cq=$d->select("chat_block_master","society_id='$society_id' AND block_for='$user_id' AND block_by='$data[user_id]' OR society_id='$society_id' AND  block_by='$user_id' AND block_for='$data[user_id]'");

                $userId= $data['user_id'];


                $member = array(); 
                $member["user_id"]=$data['user_id'];
               
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['block_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["unit_name"]=$data['unit_name'];
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["public_mobile"]=$data['mobile_for_gatekeeper'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];
                $member["token"] = $data['user_token'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";


                $member["block_status"] = false;
                
                     array_push($response["member"], $member); 
                }

               
            $response["message"] =''.$xml->string->getMemberlistSucess;
            $response["status"] = "200";
            echo json_encode($response);
         } else {
            $response["message"] =''.$xml->string->faildGetlist;
            $response["status"] = "201";
            echo json_encode($response);
        }


    } else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }
}
else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
?>