<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d h:i A");
    if($_POST['getList']=="getList" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){

                $q=$d->select("penalty_master,unit_master,users_master","penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id' AND penalty_master.user_id='$user_id'","ORDER BY penalty_master.penalty_id DESC");

                if(mysqli_num_rows($q)>0){
                        $response["penalty"] = array();

                    while($data=mysqli_fetch_array($q)) {
                        
                            $penalty = array(); 

                            $q3=$d->select("balancesheet_master,society_payment_getway","society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data[balancesheet_id]' AND balancesheet_master.society_payment_getway_id!=0 OR society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data[balancesheet_id]' AND balancesheet_master.society_payment_getway_id_upi!=0");


                            $penalty["penalty_id"]=$data['penalty_id'];
                            $penalty["society_id"]=$data['society_id'];
                            $penalty["balancesheet_id"]=$data['balancesheet_id'];
                            $penalty["unit_id"]=$data['unit_id'];
                            $penalty["user_id"]=$data['user_id'];
                            $penalty["penalty_name"]=html_entity_decode($data['penalty_name']);
                            $penalty["penalty_date"]=date("d M Y h:i A", strtotime($data['penalty_date']));;
                            $penalty["penalty_amount"]=$data['penalty_amount'];
                            $penalty["penalty_amount_view"]=number_format($data['penalty_amount'],2);
                            $penalty["paid_status"]=$data['paid_status'];
                            $penalty["penalty_photo"]=$base_url."img/billReceipt/".$data['penalty_photo'];

                             $photoAryy= array();

                            if ($data['penalty_photo']!="") {
                                 array_push($photoAryy, $data['penalty_photo']);
                            }

                            if ($data['penalty_photo_2']!='') {
                               array_push($photoAryy, $data['penalty_photo_2']);
                            }

                            if ($data['penalty_photo_3']!='') {
                              array_push($photoAryy, $data['penalty_photo_3']);
                            }

                            $penalty["penalty_img"]=array();

                             for ($iFeed=0; $iFeed < count($photoAryy) ; $iFeed++) { 
                                  $penalty_img=array();
                                  $penalty_img["penalty_photo"]=$base_url."img/billReceipt/".$photoAryy[$iFeed];
                                  array_push($penalty["penalty_img"], $penalty_img); 

                              }


                            $penalty["bill_type"]=$data['bill_type'];
                            $penalty["gst_type"]=$data['gst_type'];
                            $penalty["gst_slab"]=$data['tax_slab'];  

                            $penalty["is_taxble"]=$data['is_taxble'];
                            $penalty["taxble_type"]=$data['taxble_type'];
                            $penalty["tax_slab"]=$data['tax_slab']; 

                            $penalty["gst"]=$data['gst'];  
                            
                            if ($data['taxble_type']=='1') {
                                $gstValue = $data['penalty_amount'] - ($data['penalty_amount']  * (100/(100+$data['tax_slab']))); 

                                $penalty["igst_amount"]=sprintf("%.2f",$gstValue);
                                $penalty["igst_lbl_view"]=$igstName.' ('.$data['tax_slab'].' %)';
                                $penalty["cgst_amount"]="";
                                $penalty["sgst_amount"]="";
                                $penalty["cgst_lbl_view"]="";
                                $penalty["sgst_lbl_view"]="";
                                   
                            } else {
                                $gstValue = $data['penalty_amount'] - ($data['penalty_amount']  * (100/(100+$data['tax_slab']))); 
                                $sepGst = $gstValue/2;
                                $tax_slab = number_format($data['tax_slab']/2,2,'.','');
                                $penalty["igst_amount"]="";
                                $penalty["cgst_amount"]=number_format($sepGst,2);
                                $penalty["sgst_amount"]=number_format($sepGst,2);
                                $penalty["igst_lbl_view"]="";
                                $penalty["cgst_lbl_view"]=$cgstName.' ('.$tax_slab.' %)';
                                $penalty["sgst_lbl_view"]=$sgstName.' ('.$tax_slab.' %)';
                             }   
                            if ($data['is_taxble']=="1") {
                             $penalty["amount_without_gst"] =number_format($data['amount_without_gst'],2);
                            } else {
                             $penalty["amount_without_gst"]=number_format($data['penalty_amount'],2);
                            }
                             $penalty["bill_no"]='INVPN'.$data['penalty_id']; 
                             $penalty['invoice_url']= $base_url."apAdmin/invoice.php?user_id=$data[user_id]&unit_id=$data[unit_id]&type=P&societyid=$society_id&id=$data[penalty_id]";

                            if (mysqli_num_rows($q3)>0) {
                                $penalty["isPay"]=true;   
                            } else {
                                $penalty["isPay"]=false;   
                            }

                            $qPaid= $d->selectRow("payment_request_id","payment_paid_request","unit_id='$data[unit_id]' AND user_id='$user_id' AND penalty_id='$data[penalty_id]'");
                            if (mysqli_num_rows($qPaid)>0) {
                                $reqData=mysqli_fetch_array($qPaid);
                                $penalty["already_request"]=true;   
                                $penalty["payment_request_id"]=$reqData['payment_request_id'];   
                            } else {
                                $penalty["already_request"]=false;  
                                $penalty["payment_request_id"]=''; 
                            }
                           
                            array_push($response["penalty"], $penalty);


                    }
                    $response["message"]="Penalty List";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No Penalty Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>