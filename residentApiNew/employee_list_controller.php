<?php
include_once 'lib.php';
$employeeLngName = $xml->string->employee;
if(isset($_POST) && !empty($_POST)){
$in_time = $xml->string->in_time;
$out_time = $xml->string->out_time;
$owner = $xml->string->owner;
$tenant = $xml->string->tenant;
    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
            if($_POST['getEmployee']=="getEmployee" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                 if ($emp_type_id==1) {
                    # code...
                    $qemployee=$d->select("employee_master","emp_status=1 AND society_id='$society_id' AND emp_type_id='0'" ,"ORDER BY emp_name ASC");
                } else {
                    $qemployee=$d->select("employee_master","emp_status=1 AND society_id='$society_id' AND emp_type_id='$emp_type_id' AND private_resource=0" ,"ORDER BY emp_name ASC");

                }

                if(mysqli_num_rows($qemployee)>0){
                    $response["employee"] = array();

                while($data_employee_list=mysqli_fetch_array($qemployee)) {
                   
                        $employee = array(); 

                        $count5=$d->sum_data("rating_star","employee_rating_master","emp_id='$data_employee_list[emp_id]'");
                        while($row=mysqli_fetch_array($count5))
                       {
                            $asif=$row['SUM(rating_star)'];
                          $totalStar=number_format($asif,2,'.','');
                               
                        }
                        $totalUser=$d->count_data_direct("rating_id","employee_rating_master","emp_id='$data_employee_list[emp_id]'");
                        if ($totalStar>0) {
                            $average_rating= $totalStar/$totalUser;
                            $average_rating= number_format($average_rating,1,'.','');
                        } else {
                            $average_rating='0.0';

                        }

                        $EW=$d->select("emp_type_master","emp_type_id='$data_employee_list[emp_type_id]'");
                        $empTypeData=mysqli_fetch_array($EW);

                        $employee["emp_id"]=$data_employee_list['emp_id'];
                        $employee["society_id"]=$data_employee_list['society_id'];
                        $employee["emp_type_id"]=$data_employee_list['emp_type_id'];
                        $employee["emp_type"]=$data_employee_list['emp_type'];
                        $employee["emp_name"]=$data_employee_list['emp_name'];
                        $employee["emp_mobile"]=$data_employee_list['emp_mobile'];
                        $employee["country_code"]=$data_employee_list['country_code'];
                        $employee["emp_email"]=$data_employee_list['emp_email'];
                        $employee["emp_address"]=$data_employee_list['emp_address'];
                        $employee["emp_id_proof"]=$data_employee_list['emp_id_proof'];
                        $employee["emp_date_of_joing"]=$data_employee_list['emp_date_of_joing'];
                        $employee["emp_sallary"]=$data_employee_list['emp_sallary'];
                        $employee["emp_status"]=$data_employee_list['emp_status'];
                        $employee["entry_status"]=$data_employee_list['entry_status'];
                        $employee["average_rating"]=$average_rating.'';
                        if ($data_employee_list['emp_type_id']==0) {
                            $employee["gate_number"]=$data_employee_list['gate_number'];
                        } else {
                            $employee["gate_number"]="";
                        }

                        if ($data_employee_list['emp_type_id']==0) {
                         $employee["emp_type_name"]="Security Guard";
                        }else {
                         $employee["emp_type_name"]=$empTypeData['emp_type_name'];
                        }

                        if ($data_employee_list['entry_status']==1 && $data_employee_list['in_out_time']!="") {
                            $employee["in_out_time"]="$in_time : ".$data_employee_list['in_out_time'].'';
                        } else if($data_employee_list['in_out_time']!="")  {
                            $employee["in_out_time"]="$out_time : ".$data_employee_list['in_out_time'].'';
                        }  else {
                            $employee["in_out_time"]= "";
                        }

                        $employee["emp_profile"]=$base_url."img/emp/".$data_employee_list['emp_profile'];
                        $employee["emp_profile_old"]=$data_employee_list['emp_profile'];
                        $fiq= $d->select("employee_rating_master","unit_id='$unit_id' AND user_id='$user_id' AND emp_id='$data_employee_list[emp_id]'");
                        $comData=mysqli_fetch_array($fiq);
                        if ($comData>0) {
                            $employee["rating_msg"]=$comData['rating_msg'];
                            $employee["rating_star"]=''.$comData['rating_star'];
                        } else {
                            $employee["rating_msg"]="";
                            $employee["rating_star"]="";
                        }

                        array_push($response["employee"], $employee);

                     // }

                }
                $response["message"]="$datafoundMsg";
                $response["status"]="200";
                echo json_encode($response);

            }else{

                $response["message"]="$noDatafoundMsg";
                $response["status"]="201";
                echo json_encode($response);

            }
                

        } else if($_POST['getEmployeeByFilter']=="getEmployeeByFilter" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
           // $qemployee=$d->select("employee_master,employee_schedule","employee_master.emp_id = employee_schedule.emp_id AND employee_master.society_id='$society_id' AND employee_master.emp_type_id='0' AND employee_schedule.schedule_day='$schedule_day' AND employee_schedule.time_slot_id IN($time_slot_id)","GROUP BY employee_schedule.emp_id");
           // $qemployee=$d->select("employee_master,employee_schedule","employee_master.emp_id = employee_schedule.emp_id AND employee_master.society_id='$society_id' AND employee_master.emp_type_id='$emp_type_id' AND employee_schedule.schedule_day='$schedule_day' AND employee_schedule.time_slot_id IN($time_slot_id)" ,"GROUP BY employee_schedule.emp_id");

            if ($emp_type_id==1) {
                $qemployee=$d->select("employee_master","emp_status=1 AND emp_type=1 AND  emp_type_id='0' AND emp_id  NOT IN (Select emp_id from employee_schedule where schedule_day='$schedule_day' AND time_slot_id IN($time_slot_id))","ORDER BY emp_name ASC");
            } else {
                $qemployee=$d->select("employee_master","emp_status=1 AND private_resource=0 AND emp_type=1 AND emp_type_id='$emp_type_id' AND emp_id  NOT IN (Select emp_id from employee_schedule where schedule_day='$schedule_day' AND  time_slot_id IN($time_slot_id))","ORDER BY emp_name ASC");
            }

            if(mysqli_num_rows($qemployee)>0){
                $response["employee"] = array();

            while($data_employee_list=mysqli_fetch_array($qemployee)) {
               
                    $employee = array(); 

                    $count5=$d->sum_data("rating_star","employee_rating_master","emp_id='$data_employee_list[emp_id]'");
                    while($row=mysqli_fetch_array($count5))
                   {
                        $asif=$row['SUM(rating_star)'];
                      $totalStar=number_format($asif,2,'.','');
                           
                    }
                    $totalUser=$d->count_data_direct("rating_id","employee_rating_master","emp_id='$data_employee_list[emp_id]'");
                    if ($totalStar>0) {
                        $average_rating= $totalStar/$totalUser;
                        $average_rating= number_format($average_rating,1,'.','');
                    } else {
                        $average_rating='0.0';
                    }

                    $EW=$d->select("emp_type_master","emp_type_id='$data_employee_list[emp_type_id]'");
                    $empTypeData=mysqli_fetch_array($EW);


                    $employee["emp_id"]=$data_employee_list['emp_id'];
                    $employee["society_id"]=$data_employee_list['society_id'];
                    $employee["emp_type_id"]=$data_employee_list['emp_type_id'];
                    $employee["emp_type"]=$data_employee_list['emp_type'];
                    $employee["emp_name"]=$data_employee_list['emp_name'];
                    $employee["emp_mobile"]=$data_employee_list['emp_mobile'];
                    $employee["country_code"]=$data_employee_list['country_code'];
                    $employee["emp_email"]=$data_employee_list['emp_email'];
                    $employee["emp_address"]=$data_employee_list['emp_address'];
                    $employee["emp_id_proof"]=$data_employee_list['emp_id_proof'];
                    $employee["emp_date_of_joing"]=$data_employee_list['emp_date_of_joing'];
                    $employee["emp_sallary"]=$data_employee_list['emp_sallary'];
                    $employee["emp_status"]=$data_employee_list['emp_status'];
                    $employee["entry_status"]=$data_employee_list['entry_status'];
                    $employee["average_rating"]=$average_rating.'';
                    if ($data_employee_list['emp_type_id']==0) {
                        $employee["gate_number"]=$data_employee_list['gate_number'];
                    } else {
                        $employee["gate_number"]="";
                    }
                        
                     $fiq= $d->select("employee_rating_master","unit_id='$unit_id' AND user_id='$user_id' AND emp_id='$data_employee_list[emp_id]'");
                    $comData=mysqli_fetch_array($fiq);
                    if ($comData>0) {
                        $employee["rating_msg"]=$comData['rating_msg'];
                        $employee["rating_star"]=''.$comData['rating_star'];
                    } else {
                        $employee["rating_msg"]="";
                        $employee["rating_star"]="";
                    }
                    if ($data_employee_list['emp_type_id']==0) {
                         $employee["emp_type_name"]="Security Guard";
                        }else {
                         $employee["emp_type_name"]=$empTypeData['emp_type_name'];
                        }
                    if ($data_employee_list['entry_status']==1) {
                        $employee["in_out_time"]=$in_time.' : '.$data_employee_list['in_out_time'].'';
                    } else {
                        $employee["in_out_time"]=$out_time.' : '.$data_employee_list['in_out_time'].'';
                    }
                    $employee["emp_profile"]=$base_url."img/emp/".$data_employee_list['emp_profile'];
                    $employee["emp_profile_old"]=$data_employee_list['emp_profile'];

                    array_push($response["employee"], $employee);

                 // }

            }
            $response["message"]="$datafoundMsg" ;
            $response["status"]="200";
            echo json_encode($response);

        }else{

            $response["message"]="$noDatafoundMsg";
            $response["status"]="201";
            echo json_encode($response);

        }
            

    }else if($_POST['getMyEmployee']=="getMyEmployee" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                $q=$d->select("employee_master,employee_unit_master","employee_master.emp_status=1 AND employee_master.emp_id=employee_unit_master.emp_id AND employee_unit_master.unit_id='$unit_id' AND employee_master.society_id='$society_id' AND employee_master.emp_type='1' AND employee_unit_master.user_id='$user_id'","ORDER BY employee_master.emp_name ASC");
                
                if(mysqli_num_rows($q)>0){
                    $response["employee"] = array();

                while($data_employee_list=mysqli_fetch_array($q)) {
                    //old
                       
                        $employee = array(); 
                        $EW=$d->select("emp_type_master","emp_type_id='$data_employee_list[emp_type_id]'");
                        $empTypeData=mysqli_fetch_array($EW);

                         $count5=$d->sum_data("rating_star","employee_rating_master","emp_id='$data_employee_list[emp_id]'");
                        while($row=mysqli_fetch_array($count5))
                       {
                            $asif=$row['SUM(rating_star)'];
                          $totalStar=number_format($asif,2,'.','');
                               
                        }
                        $totalUser=$d->count_data_direct("rating_id","employee_rating_master","emp_id='$data_employee_list[emp_id]'");
                        if ($totalStar>0) {
                            $average_rating= $totalStar/$totalUser;
                            $average_rating= number_format($average_rating,1,'.','');
                        } else {
                          $average_rating='0.0';

                        }

                        $employee["emp_id"]=$data_employee_list['emp_id'];
                        $employee["private_resource"]=$data_employee_list['private_resource'];
                        $employee["active_status"]=$data_employee_list['active_status'];
                        $employee["society_id"]=$data_employee_list['society_id'];
                        $employee["emp_type_id"]=$data_employee_list['emp_type_id'];
                        $employee["emp_name"]=$data_employee_list['emp_name'];
                        $employee["emp_mobile"]=$data_employee_list['emp_mobile'];
                        $employee["country_code"]=$data_employee_list['country_code'];
                        $employee["emp_email"]=$data_employee_list['emp_email'];
                        $employee["emp_address"]=$data_employee_list['emp_address'];
                        $employee["emp_id_proof"]=$data_employee_list['emp_id_proof'];
                        if ($data_employee_list['emp_id_proof']!='') {
                        $employee["emp_id_proof_view"]=$base_url."img/emp_id/".$data_employee_list['emp_id_proof'];
                        }else {
                        $employee["emp_id_proof_view"]="";
                        }
                        if ($data_employee_list['emp_id_proof1']!='') {
                        $employee["emp_id_proof_view1"]=$base_url."img/emp_id/".$data_employee_list['emp_id_proof1'];
                        }else {
                        $employee["emp_id_proof_view1"]="";
                        }
                        $employee["emp_date_of_joing"]=$data_employee_list['emp_date_of_joing'];
                        $employee["emp_sallary"]=$data_employee_list['emp_sallary'];
                        $employee["emp_status"]=$data_employee_list['emp_status'];
                        $employee["entry_status"]=$data_employee_list['entry_status'];
                        if ($data_employee_list['entry_status']==1) {
                            $employee["in_out_time"]=$in_time.' : '.$data_employee_list['in_out_time'].'';
                        } else {
                            $employee["in_out_time"]=$out_time.' : '.$data_employee_list['in_out_time'].'';
                        }
                        $employee["emp_profile"]=$base_url."img/emp/".$data_employee_list['emp_profile'];
                        $employee["emp_profile_old"]=$data_employee_list['emp_profile'];
                        $employee["emp_type_name"]=$empTypeData['emp_type_name'];
                        $employee["average_rating"]=$average_rating.'';

                        $fiq= $d->select("employee_rating_master","unit_id='$unit_id' AND user_id='$user_id' AND emp_id='$data_employee_list[emp_id]'");
                        $comData=mysqli_fetch_array($fiq);
                        if ($comData>0) {
                            $employee["rating_msg"]=$comData['rating_msg'];
                            $employee["rating_star"]=''.$comData['rating_star'];
                        } else {
                            $employee["rating_msg"]="";
                            $employee["rating_star"]="";
                        }

                        $qc=$d->select("staff_visit_master","emp_id='$data_employee_list[emp_id]' ","ORDER BY staff_visit_id DESC LIMIT 1");
                        $visitData=mysqli_fetch_array($qc);
                        
                        
                        if ($data_employee_list['entry_status']==1) {
                            if ($visitData['in_temperature']>0) {
                              $in_temperature= $visitData['in_temperature'];
                            } else {
                                $in_temperature="";
                            }
                            $employee["temperature"]=$in_temperature.'';
                            $employee["with_mask"]=$visitData['in_with_mask'].'';
                        } else if ($data_employee_list['entry_status']==2) {
                            if ($visitData['out_temperature']>0) {
                              $out_temperature= $visitData['out_temperature'];
                            } else {
                                 $out_temperature= "";
                            }
                            $employee["temperature"]=$out_temperature.'';
                            $employee["with_mask"]=$visitData['out_with_mask'].'';
                        }  else {
                            $employee["temperature"]='';
                            $employee["with_mask"]='';
                        }

                        $employee["family_member"] = array();
                        $q3=$d->select("unit_master,block_master,users_master","users_master.user_status=1 AND users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0  AND unit_master.unit_status!=4 AND users_master.unit_id='$unit_id' AND users_master.parent_id='$user_id'","ORDER BY unit_master.unit_id ASC");
                          while ($unitData=mysqli_fetch_array($q3)) { 
                                // check add or not
                            $qqq=$d->select("employee_unit_master","user_id='$unitData[user_id]' AND unit_id='$unit_id' AND emp_id='$data_employee_list[emp_id]'");

                              if($unitData['user_type']=="0" && $unitData['member_status']=="0" ){
                                  $relation= "$owner";
                              } else  if($unitData['user_type']=="0" && $unitData['member_status']=="1" ){
                                  $relation= $unitData['member_relation_name'];
                              } else  if($unitData['user_type']=="1" && $unitData['member_status']=="0"  ){
                                  $relation= "$tenant";
                              }else  if($unitData['user_type']=="1" && $unitData['member_status']=="1"   ){
                                  $relation= $unitData['member_relation_name'];
                              } else   {
                                  $relation= "$owner";
                              } 
                            $family_member = array();
                            $family_member["user_id"]=$unitData["user_id"];
                            if ($unitData['user_profile_pic'] != '') {
                                $family_member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $unitData['user_profile_pic'];
                            } else {
                                $family_member["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                            }
                            $family_member["user_full_name"]=$unitData["user_full_name"].'-'.$relation;
                            if (mysqli_num_rows($qqq)>0) {
                                $family_member["is_employee_added"]=true;
                                $notData =mysqli_fetch_array($qqq);
                                if ($notData['active_status']==0) {
                                $family_member["is_notification"]=true;
                                } else {
                                $family_member["is_notification"]=false;
                                }

                            } else {
                                 $family_member["is_employee_added"]=false;
                                 $family_member["is_notification"]=false;
                            }

                            array_push($employee["family_member"], $family_member); 
                         }

                        array_push($response["employee"], $employee);

                }
                $response["message"]="$datafoundMsg";
                $response["status"]="200";
                echo json_encode($response);

            }else{

                $response["message"]="$noDatafoundMsg";
                $response["status"]="201";
                echo json_encode($response);

            }
                

        }else if($_POST['addMoreEmployee']=="addMoreEmployee" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

             $m->set_data('unit_id',$unit_id);
             $m->set_data('user_id',$user_id);
             $m->set_data('society_id', $society_id);
             $m->set_data('emp_id', $emp_id);

              $a = array(
                'unit_id'=>$m->get_data('unit_id'),
                'user_id'=>$m->get_data('user_id') ,
                'society_id'=>$m->get_data('society_id') ,
                'emp_id'=>$m->get_data('emp_id') 
              );


              $dc= $d->selectRow("user_id","employee_unit_master","unit_id='$unit_id' AND user_id='$user_id' AND emp_id ='$emp_id' AND user_id='$user_id'");
              if (mysqli_num_rows($dc)) {
                $already_added = $xml->string->already_added;
                $response["message"]="$already_added";
                $response["status"]="201";
                echo json_encode($response);
                exit();
              } else {
                $q=$d->insert("employee_unit_master",$a);

                 $title ="$emp_name - Employee Added in your resources";
               
                  $notiAry = array(
                    'society_id'=>$society_id,
                    'user_id'=>$user_id,
                    'notification_title'=>$title,
                    'notification_desc'=>"By ".$parent_name,    
                    'notification_date'=>date('Y-m-d H:i'),
                    'notification_action'=>'employees',
                    'notification_logo'=>'Resourcesxxxhdpi.png',
                    );
                    $d->insert("user_notification",$notiAry);
           
                  $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android'  and user_id='$user_id' ");
                  $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' and user_id='$user_id'  ");
                    $nResident->noti("StaffFragment","",$society_id,$fcmArray,$title,"By Admin ".$_COOKIE['admin_name'],'StaffFragment');
                  $nResident->noti_ios("ResourcesVC","",$society_id,$fcmArrayIos,$title,"By Admin ".$_COOKIE['admin_name'],'ResourcesVC');

                $response["message"]="$addMsg";
                $response["status"]="200";
                echo json_encode($response);

              }


        } else  if($_POST['getEmployeeType']=="getEmployeeType" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                $qemployee_type=$d->select("emp_type_master","society_id='$society_id' ");

                

                if(mysqli_num_rows($qemployee_type)>0){

                    $response["employee_Type"] = array();

                    while($data_employee_type=mysqli_fetch_array($qemployee_type)) {

                        $employee_Type = array(); 

                        $employee_Type["emp_type_id"]=$data_employee_type['emp_type_id'];
                        $emp_type_id=$data_employee_type['emp_type_id'];

                        $employee_Type["society_id"]=$society_id;
                        $employee_Type["emp_type_name"]=$data_employee_type['emp_type_name'];
                        $employee_Type["emp_type_status"]=$data_employee_type['emp_type_status'];
                        $employee_Type["emp_type_icon"]=$base_url."/img/emp_icon/".$data_employee_type['emp_type_icon'];

                        array_push($response["employee_Type"], $employee_Type); 
                    }

                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else  if($_POST['addNewEmp']=="addNewEmp" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                if ($country_code!="") {
                    $appendQuery = " AND country_code='$country_code'";
                }

                    $qselect=$d->select("employee_master","emp_mobile='$emp_mobile' AND society_id='$society_id' AND emp_type_id='$emp_type_id' $appendQuery");
                    $user_data = mysqli_fetch_array($qselect);
                    if($user_data==true && $user_data['emp_type_id']!=0){
                        $emp_type_id= $user_data['emp_type_id'];
                            if ($emp_type_id==0) {
                                $empTypeName = "Security Guard";
                            } else  {
                                $qc=$d->selectRow("emp_type_name","emp_type_master","emp_type_id='$emp_type_id'");
                                $empData=mysqli_fetch_array($qc);
                                $empTypeName= $empData['emp_type_name'];

                            }

                        $qu=$d->select("employee_unit_master","emp_id='$emp_id' AND unit_id='$unit_id'");

                        if(mysqli_num_rows($qu)>0){
                            $already_register_in_your_unit = $xml->string->already_register_in_your_unit;
                            $response["message"]="$already_register_in_your_unit";
                            $response["status"]="203";
                            echo json_encode($response);
                            exit();
                        }

                        if ($user_data['private_resource']==1) {
                            $employee_is_private = $xml->string->employee_is_private;
                            $response["message"]="$employee_is_private";
                            $response["status"]="203";
                            echo json_encode($response);
                            exit();
                        }

                        if ($empTypeName=="Security Guard") {
                            $employeeLngName_category_not_available = $xml->string->employeeLngName_category_not_available;
                            $employeeLngName_category_not_available = str_replace('{employeeLngName}', $employeeLngName, $employeeLngName_category_not_available);
                            $employeeLngName_category_not_available = str_replace('{empTypeName}', $empTypeName, $employeeLngName_category_not_available);
                            $response["message"]="$employeeLngName_category_not_available";
                        } else {

                            $employeelngname_is_booked_your_time_slot_from_all_resources = $xml->string->employeelngname_is_booked_your_time_slot_from_all_resources;
                            $employeelngname_is_booked_your_time_slot_from_all_resources = str_replace('{employeeLngName}', $employeeLngName, $employeelngname_is_booked_your_time_slot_from_all_resources);
                            $employeelngname_is_booked_your_time_slot_from_all_resources = str_replace('{empTypeName}', $empTypeName, $employeelngname_is_booked_your_time_slot_from_all_resources);
                            $response["message"]="$employeelngname_is_booked_your_time_slot_from_all_resources";

                        }

                        $response["status"]="203";
                        echo json_encode($response);
                        exit();

                    
                  }     
                  
                    $file11=$_FILES["emp_id_proof"]["tmp_name"];
                    if(file_exists($file11)) {
                        $image_Arr = $_FILES['emp_id_proof'];   
                        $temp = explode(".", $_FILES["emp_id_proof"]["name"]);
                        $emp_id_proof = $emp_mobile.'_id_'.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["emp_id_proof"]["tmp_name"], "../img/emp_id/".$emp_id_proof);
                    } else {
                     $emp_id_proof='';
                    }

                  $extension=array("jpeg","jpg","png","gif","JPG","jpeg","JPEG","PNG");
                  $uploadedFile = $_FILES['emp_profile']['tmp_name'];
                  $ext = pathinfo($_FILES['emp_profile']['name'], PATHINFO_EXTENSION);

                  if(file_exists($uploadedFile)) {
                    if(in_array($ext,$extension)) {
                        $sourceProperties = getimagesize($uploadedFile);
                      $newFileName = rand().$emp_name;
                      $dirPath = "../img/emp/";
                      $imageType = $sourceProperties[2];
                      $imageHeight = $sourceProperties[1];
                      $imageWidth = $sourceProperties[0];
                      
                      // less 30 % size 
                      $newImageWidth = $imageWidth * 30 /100;
                      $newImageHeight = $imageHeight * 30 /100;

                      switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagepng($tmp,$dirPath. $newFileName. "_emp.". $ext);
                            break;           

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagejpeg($tmp,$dirPath. $newFileName. "_emp.". $ext);
                            break;
                        
                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagegif($tmp,$dirPath. $newFileName. "_emp.". $ext);
                            break;

                        default:
                           $invalid_document = $xml->string->invalid_document;
                           $response["message"]="$invalid_document";
                            $response["status"]="201";
                            exit;
                            break;
                        }
                        $emp_profile= $newFileName."_emp.".$ext;

                      } else{
                        $invalid_document = $xml->string->invalid_document;
                        $response["message"]="$invalid_document";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit();
                      }
                    } else {
                      $emp_profile="user.png";
                    }


                    $m->set_data('society_id',$society_id);
                    $m->set_data('emp_type_id',$emp_type_id);
                    $m->set_data('emp_name',$emp_name);
                    $m->set_data('emp_mobile',$emp_mobile);
                    $m->set_data('country_code',$country_code);
                    $m->set_data('emp_address',$emp_address);
                    $m->set_data('emp_profile',$emp_profile);
                    $m->set_data('emp_id_proof',$emp_id_proof);
                    $m->set_data('emp_date_of_joing',date('Y-m-d'));
                    $m->set_data('emp_type',1);
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('private_resource',$private_resource);

                      $a1= array (
                        'society_id'=> $m->get_data('society_id'),
                        'emp_type_id'=> $m->get_data('emp_type_id'),
                        'emp_name'=> $m->get_data('emp_name'),
                        'emp_mobile'=> $m->get_data('emp_mobile'),
                        'country_code'=> $m->get_data('country_code'),
                        'emp_address'=> $m->get_data('emp_address'),
                        'emp_profile'=> $m->get_data('emp_profile'),
                        'emp_id_proof'=> $m->get_data('emp_id_proof'),
                        'emp_date_of_joing'=> $m->get_data('emp_date_of_joing'),
                        'emp_type'=> $m->get_data('emp_type'),
                        'unit_id'=> $m->get_data('unit_id'),
                        'private_resource'=> $m->get_data('private_resource'),
                    );
                    $q=$d->insert("employee_master",$a1);
                    $emp_id = $con->insert_id;


                if ($q>0) {
                     $m->set_data('unit_id',$unit_id);
                     $m->set_data('user_id',$user_id);
                     $m->set_data('society_id',$society_id);
                     $m->set_data('emp_id', $emp_id);

                      $a = array(
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id') ,
                        'society_id'=>$m->get_data('society_id') ,
                        'emp_id'=>$m->get_data('emp_id') 
                      );

                    $q=$d->insert("employee_unit_master",$a);
                
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","New resource $emp_name added","Resourcesxxxhdpi.png");


                    $response["message"]="$addMsg";
                    $response["status"]="200";
                    echo json_encode($response);
                }else{
                    $response["message"]="$somethingWrong";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else  if($_POST['editMyEmp']=="editMyEmp" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                if ($country_code!="") {
                    $appendQuery = " AND country_code='$country_code'";
                }

                // getUnitId
                $qqqqqqq=$d->selectRow("unit_id","users_master","user_id='$user_id'");
                $unitData=mysqli_fetch_array($qqqqqqq);
                $unit_id= $unitData['unit_id'];

                $qselect=$d->select("employee_master","emp_mobile='$emp_mobile' AND society_id='$society_id' AND emp_type_id='$emp_type_id'  AND emp_id!='$emp_id' $appendQuery OR emp_mobile='$emp_mobile' AND society_id='$society_id' AND emp_type_id='0' $appendQuery");
                   $user_data = mysqli_fetch_array($qselect);
                    if($user_data==true){
                        $emp_type_id= $user_data['emp_type_id'];
                            if ($emp_type_id==0) {
                                $empTypeName = "Security Guard";
                            } else  {
                                $qc=$d->selectRow("emp_type_name","emp_type_master","emp_type_id='$emp_type_id'");
                                $empData=mysqli_fetch_array($qc);
                                $empTypeName= $empData['emp_type_name'];

                            }

                        $qce = $d->select("employee_unit_master","emp_id ='$emp_id' AND unit_id='$unit_id'");
                        if(mysqli_num_rows($qce)>0){
                            $already_register_in_your_unit = $xml->string->already_register_in_your_unit;
                            $response["message"]="$already_register_in_your_unit";
                            $response["status"]="203";
                            echo json_encode($response);
                            exit();
                        }

                        if ($user_data['private_resource']==1) {
                            $employee_is_private = $xml->string->employee_is_private;
                            $response["message"]="$employee_is_private";
                            $response["status"]="203";
                            echo json_encode($response);
                            exit();
                        }


                        $$employeelngname_is_booked_your_time_slot_from_all_resources = $xml->string->employeelngname_is_booked_your_time_slot_from_all_resources;
                        $employeelngname_is_booked_your_time_slot_from_all_resources = str_replace('{employeeLngName}', $employeeLngName, $employeelngname_is_booked_your_time_slot_from_all_resources);
                        $employeelngname_is_booked_your_time_slot_from_all_resources = str_replace('{empTypeName}', $empTypeName, $employeelngname_is_booked_your_time_slot_from_all_resources);
                        $response["message"]="$employeelngname_is_booked_your_time_slot_from_all_resources";
                        $response["status"]="203";
                        echo json_encode($response);
                        exit();

                    
                  }

                    $file11=$_FILES["emp_id_proof"]["tmp_name"];
                    if(file_exists($file11)) {
                        $image_Arr = $_FILES['emp_id_proof'];   
                        $temp = explode(".", $_FILES["emp_id_proof"]["name"]);
                        $emp_id_proof = $emp_mobile.'_id_'.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["emp_id_proof"]["tmp_name"], "../img/emp_id/".$emp_id_proof);
                    } else {
                     $emp_id_proof=$emp_id_proof_old;
                    }

                  $extension=array("jpeg","jpg","png","gif","JPG","jpeg","JPEG","PNG");
                  $uploadedFile = $_FILES['emp_profile']['tmp_name'];
                  $ext = pathinfo($_FILES['emp_profile']['name'], PATHINFO_EXTENSION);

                  if(file_exists($uploadedFile)) {
                    if(in_array($ext,$extension)) {
                        $sourceProperties = getimagesize($uploadedFile);
                      $newFileName = rand().$emp_name;
                      $dirPath = "../img/emp/";
                      $imageType = $sourceProperties[2];
                      $imageHeight = $sourceProperties[1];
                      $imageWidth = $sourceProperties[0];
                      
                      // less 30 % size 
                      $newImageWidth = $imageWidth * 30 /100;
                      $newImageHeight = $imageHeight * 30 /100;

                      switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagepng($tmp,$dirPath. $newFileName. "_emp.". $ext);
                            break;           

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagejpeg($tmp,$dirPath. $newFileName. "_emp.". $ext);
                            break;
                        
                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagegif($tmp,$dirPath. $newFileName. "_emp.". $ext);
                            break;

                        default:
                           $invalid_document = $xml->string->invalid_document;
                            $response["message"]="$invalid_document";
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                            exit;
                            break;
                        }
                        $emp_profile= $newFileName."_emp.".$ext;

                      } else{
                        $invalid_document = $xml->string->invalid_document;
                        $response["message"]="$invalid_document";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit();
                      }
                    } else {
                      $emp_profile=$emp_profile_old;
                    }

                    $qce11 = $d->select("employee_unit_master","emp_id ='$emp_id' AND unit_id!='$unit_id'  AND unit_id!=0");
                    if (mysqli_num_rows($qce11)>0 && $private_resource==1) {
                        $emp_multiple_units = $xml->string->emp_multiple_units;
                        $emp_multiple_units = str_replace("{employeeLngName}", $employeeLngName, $emp_multiple_units);
                            $response["message"]="$emp_multiple_units";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit();
                    }
                    

                    $m->set_data('society_id',$society_id);
                    $m->set_data('emp_type_id',$emp_type_id);
                    $m->set_data('emp_name',$emp_name);
                    $m->set_data('emp_address',$emp_address);
                    $m->set_data('emp_profile',$emp_profile);
                    $m->set_data('emp_id_proof',$emp_id_proof);
                    $m->set_data('emp_type',1);
                    $m->set_data('private_resource',$private_resource);

                   
                     $a1= array (
                        'society_id'=> $m->get_data('society_id'),
                        'emp_type_id'=> $m->get_data('emp_type_id'),
                        'emp_name'=> $m->get_data('emp_name'),
                        'emp_address'=> $m->get_data('emp_address'),
                        'emp_profile'=> $m->get_data('emp_profile'),
                        'emp_id_proof'=> $m->get_data('emp_id_proof'),
                        'emp_date_of_joing'=> $m->get_data('emp_date_of_joing'),
                        'emp_type'=> $m->get_data('emp_type'),
                        'private_resource'=> $m->get_data('private_resource'),
                    );

                    $q=$d->update("employee_master",$a1,"emp_id='$emp_id' AND society_id='$society_id'");

                if ($q>0) {

                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","$employeeLngName $emp_name data updated","Resourcesxxxhdpi.png");


                    $response["message"]="$updateMsg";
                    $response["status"]="200";
                    echo json_encode($response);
                }else{
                    $response["message"]="$somethingWrong";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else  if($_POST['deleteEmployee']=="deleteEmployee" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true   && filter_var($emp_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true ){
                $myUnit_id= $unit_id;

                $qc=$d->selectRow('emp_name',"employee_master","emp_id='$emp_id'");
                $empData=mysqli_fetch_array($qc);
                $emp_name = $empData['emp_name'];

                $checkUnits = $d->select("employee_unit_master","emp_id='$emp_id'  AND user_id!='$user_id'");


                    $q=$d->delete("employee_unit_master","emp_id='$emp_id' AND unit_id='$unit_id' AND user_id='$user_id'");
                    $d->delete("employee_rating_master","emp_id='$emp_id' AND unit_id='$unit_id' AND user_id='$user_id'");

                if ($q>0) {
                     $checkUnitsSame = $d->select("employee_unit_master","emp_id='$emp_id' AND unit_id='$unit_id'");
                    if (mysqli_num_rows($checkUnitsSame)==0) {
                        $d->delete("employee_schedule","emp_id='$emp_id' AND unit_id='$unit_id'");
                    }

                    if (mysqli_num_rows($checkUnits)==0) {
                        $d->delete("employee_master","emp_id='$emp_id'");
                        $d->delete("employee_schedule","emp_id='$emp_id' AND unit_id='$unit_id'");
                    }
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","Resource  $emp_name deleted","Resourcesxxxhdpi.png");

                    $response["message"]="$deleteMsg";
                    $response["status"]="200";
                    echo json_encode($response);
                }else{
                    $response["message"]="$somethingWrong";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else if(isset($muteResourceNotification) &&  filter_var($emp_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
                $notification_on = $xml->string->notification_on;
                $notification_off = $xml->string->notification_off;

                if ($active_status==0) {
                    $active_status = 1;
                    $response["message"] = "$notification_off";
                } else {
                    $active_status = 0;
                    $response["message"] = "$notification_on";
                }

                $a = array(
                  'active_status'=>$active_status,
                  );

              $q=$d->update("employee_unit_master",$a,"unit_id='$unit_id' AND emp_id='$emp_id' AND user_id='$user_id'");
              if($q==TRUE) {
                
                $response["status"] = "200";
                echo json_encode($response);
                exit();
              } else { 
                $response["message"] = "$somethingWrong";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
              }
        } else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);

        }

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>