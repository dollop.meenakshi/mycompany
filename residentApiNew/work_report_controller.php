<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();

        $dateTime = date("Y-m-d H:i:s");

        if($_POST['checkAccessWorkReport']=="checkAccessWorkReport" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract( $_POST);
            $access_type = $work_report_Access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['addWorkReport']=="addWorkReport" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            
            extract( $_POST);

            $qry1 = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND attendance_date_start = '$work_report_date' AND society_id = '$society_id'");

            if ($qry1 <= 0) {
                $response["message"] = "You were not present for the day";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $q1 = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND work_report_date = '$work_report_date' AND society_id = '$society_id' AND floor_id = '$floor_id' AND work_report_type = '0'");

            if ($q1 > 0) {
                $response["message"] = "Work report already added for the same date";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }
            
            $sq = $d->selectRow("shift_time_id,block_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
                $block_id = $sdata['block_id'];
            }

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $shift_type = $shiftData['shift_type'];

            if ($work_report_date=='') {
                $work_report_date = date('Y-m-d');
            }

            $dateTime = date('Y-m-d H:i:s');

            $m->set_data('society_id',$society_id);
            $m->set_data('floor_id',$floor_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('block_id',$block_id);
            $m->set_data('work_report',$work_report);
            $m->set_data('work_report_date',$work_report_date);
            $m->set_data('work_report_type',$work_report_type);            
            $m->set_data('work_report_created_date',$dateTime);

            //if ($template_ids != '') {
                
                $m->set_data('template_ids',$template_id);

                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'floor_id'=>$m->get_data('floor_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'block_id'=>$m->get_data('block_id'),
                    'work_report_date'=>$m->get_data('work_report_date'),
                    'work_report_type'=>$m->get_data('work_report_type'),
                    'work_report'=>$m->get_data('work_report'),
                    'template_ids'=>$m->get_data('template_ids'),
                    'work_report_created_date'=>$m->get_data('work_report_created_date'),
                );
            /*}else{
                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'floor_id'=>$m->get_data('floor_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'block_id'=>$m->get_data('block_id'),
                    'work_report_date'=>$m->get_data('work_report_date'),
                    'work_report_type'=>$m->get_data('work_report_type'),
                    'work_report'=>$m->get_data('work_report'),
                    'work_report_created_date'=>$m->get_data('work_report_created_date'),
                );
            }*/


            //if ($work_report_id == '') {
                $qry = $d->insert("work_report_master",$a);

                $work_report_id = $con->insert_id;
            /*}else{

                if ($template_ids != '') {
                
                    $m->set_data('template_ids',$template_ids);

                    $a = array(
                        'template_ids'=>$m->get_data('template_ids'),
                    );

                    $qry = $d->update("work_report_master",$a,"work_report_id = '$work_report_id' AND user_id = '$user_id'");
                }

                $qq1 = $d->count_data_direct("
                    work_report_template_id
                    ","
                    work_report_employee_ans,
                    work_report_master
                    ","
                    work_report_employee_ans.work_report_template_id = '$template_id' 
                    AND work_report_master.work_report_date = '$work_report_date' 
                    AND work_report_master.user_id = '$user_id' 
                    AND work_report_employee_ans.user_id = '$user_id' 
                    AND work_report_master.work_report_id = '$work_report_id' 
                    AND work_report_master.work_report_id = work_report_employee_ans.work_report_id");

                if ($qq1 > 0) {
                    $response["message"] = "Work report already added for this template";
                    $response["work_report_id"] = $work_report_id;
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }

                $qry = true;
            }*/

            $message = "Work report submitted";
        
            if ($qry == true) {

                $response['work_report_added'] = false;
                    
                $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND work_report_date = '$work_report_date' AND society_id = '$society_id'");

                if ($workReportQry > 0) {
                    $response['work_report_added'] = true;
                } 

                if ($work_report_type == 1) {

                    $work_report_employee_qa = explode("~",$work_report_employee_ans);
                    $template_question = explode("~",$template_question);
                    $question_type = explode("~",$question_type);
                    $question_type_value = explode("~",$question_type_value);
                    $work_report_is_required = explode("~",$work_report_is_required);

                    $countFile = 0;
                    $multiFileCount = 0;

                    for ($i=0; $i < count($template_question); $i++) { 

                        $has_multi_attachment = 0;                       

                        if ($work_report_employee_qa[$i] == "image") {

                            $work_report_file = "";

                            $file11=$_FILES["work_report_file"]["tmp_name"][$countFile];

                            if ($file11 != "" || $file11 != null) {                    

                                $extId = pathinfo($_FILES['work_report_file']['name'][$countFile], PATHINFO_EXTENSION);
                                $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");

                                if(in_array($extId,$extAllow)) {
                                    $temp = explode(".", $_FILES["work_report_file"]["name"][$countFile]);
                                    $work_report_file = "wr_".$user_id.round(microtime(true)).$countFile . '.' . end($temp);
                                    move_uploaded_file($_FILES["work_report_file"]["tmp_name"][$countFile], "../img/work_report/" . $work_report_file);
                                } 
                            }

                            $countFile++;                            

                            $m->set_data('work_report_employee_ans',$work_report_file);
                        }else if (preg_match('/imagemulti/i', $work_report_employee_qa[$i])) {


                            $imagemulti = explode("_", $work_report_employee_qa[$i]);
                            $imageMultiCountSize = $imagemulti[1];

                            $aryMultiImage= array();

                            $newCount = $multiFileCount + $imageMultiCountSize;

                            for ($p=$multiFileCount; $p < $newCount; $p++) {
                                $work_report_file_multi = "";

                                $file11=$_FILES["work_report_file_multi"]["tmp_name"][$p];

                                if ($file11 != "" || $file11 != null) {                    

                                    $extId = pathinfo($_FILES['work_report_file_multi']['name'][$p], PATHINFO_EXTENSION);
                                    $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");

                                    if(in_array($extId,$extAllow)) {
                                        $temp = explode(".", $_FILES["work_report_file_multi"]["name"][$p]);
                                        $work_report_file_multi = "wr_".$user_id.round(microtime(true)).$p . '.' . end($temp);
                                        move_uploaded_file($_FILES["work_report_file_multi"]["tmp_name"][$p], "../img/work_report/" . $work_report_file_multi);
                                        $aryF = array();
                                        $aryF['file'] = $work_report_file_multi;
                                        array_push($aryMultiImage,$aryF);
                                    } 
                                }
                            }

                            $multiFileCount = $newCount;

                            $multiFileJsonData = json_encode($aryMultiImage);

                            $m->set_data('work_report_employee_ans',$multiFileJsonData);
                            $has_multi_attachment = "1";
                        }else{

                            if ($work_report_employee_qa[$i] == "NA" || $work_report_employee_qa[$i] == "na") {
                                $m->set_data('work_report_employee_ans',"");
                            }else{                                
                                $m->set_data('work_report_employee_ans',$work_report_employee_qa[$i]);
                            }
                        }

                        if ($question_type_value[$i] == "NA" || $question_type_value[$i] == "na" 
                            || $question_type_value[$i] == '') {
                            $qtValue = "";
                        }else{
                            $qtValue = $question_type_value[$i];
                        }

                        $m->set_data('work_report_template_id',$template_id);
                        $m->set_data('template_question',$template_question[$i]);
                        $m->set_data('question_type',$question_type[$i]);
                        $m->set_data('work_report_date',$work_report_date);
                        $m->set_data('work_report_is_required',$work_report_is_required[$i]);
                        $m->set_data('question_type_value',$qtValue);
                        $m->set_data('work_report_id',$work_report_id);
                        $m->set_data('work_report_template_name',$template_name);
                        $m->set_data('has_multi_attachment',$has_multi_attachment);
                        $m->set_data('work_report_created_date',date('Y-m-d H:i:s'));

                        $a = array(
                            'society_id' =>$m->get_data('society_id'),
                            'work_report_template_id' =>$m->get_data('work_report_template_id'),
                            'work_report_id' =>$m->get_data('work_report_id'),
                            'work_report_template_name' =>$m->get_data('work_report_template_name'),
                            'user_id' =>$m->get_data('user_id'),
                            'work_report_date' =>$m->get_data('work_report_date'),
                            'work_report_template_question' =>$m->get_data('template_question'),
                            'work_report_question_type'=>$m->get_data('question_type'),
                            'work_report_is_required'=>$m->get_data('work_report_is_required'),
                            'work_report_question_type_value'=>$m->get_data('question_type_value'),
                            'work_report_employee_ans'=>$m->get_data('work_report_employee_ans'),
                            'has_multi_attachment'=>$m->get_data('has_multi_attachment'),
                            'work_report_created_date'=>$m->get_data('work_report_created_date'),
                        );

                        $d->insert("work_report_employee_ans",$a);
                    }                    
                }   

                $response["message"] = $message;
                $response["work_report_id"] = $work_report_id;
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['addWorkReportNew']=="addWorkReportNew" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            
            extract( $_POST);

            $qry1 = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND attendance_date_start = '$work_report_date' AND society_id = '$society_id'");

            if ($qry1 <= 0) {
                $response["message"] = "You were not present for the day";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $q1 = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND work_report_date = '$work_report_date' AND society_id = '$society_id' AND floor_id = '$floor_id' AND work_report_type = '0'");

            if ($q1 > 0) {
                $response["message"] = "Work report already added for the same date";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }
            
            $sq = $d->selectRow("shift_time_id,block_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
                $block_id = $sdata['block_id'];
            }

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $shift_type = $shiftData['shift_type'];

            if ($work_report_date=='') {
                $work_report_date = date('Y-m-d');
            }

            $dateTime = date('Y-m-d H:i:s');

            $work_report_file = "";

            $totalFiles = count($_FILES['work_report_file_new']['tmp_name']);
            $work_report_file_list = array();

            for ($i = 0; $i < $totalFiles; $i++) {

                $workReportFile = $_FILES['work_report_file_new']['tmp_name'][$i];

                if(file_exists($workReportFile)) {

                    $file11=$_FILES["work_report_file"]["tmp_name"][$i];
                    $extId = pathinfo($_FILES['work_report_file_new']['name'][$i], PATHINFO_EXTENSION);
                    $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF","CSV","csv","xls","xlsx","XLS","XLSX","doc","docx");

                    if(in_array($extId,$extAllow)) {
                        $temp = explode(".", $_FILES["work_report_file_new"]["name"][$i]);
                        $work_report_file = "workReport".$user_id.round(microtime(true)).rand(10,10000) . '.' . end($temp);
                        $work_report_file_list["document_".$i] = $work_report_file;
                        move_uploaded_file($_FILES["work_report_file_new"]["tmp_name"][$i], "../img/work_report/" . $work_report_file);
                    }
                }
            }

            $work_report_file = !empty($work_report_file_list)?json_encode($work_report_file_list):'';

            $m->set_data('society_id',$society_id);
            $m->set_data('floor_id',$floor_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('block_id',$block_id);
            $m->set_data('work_report',$work_report);
            $m->set_data('work_report_file',$work_report_file);
            $m->set_data('work_report_date',$work_report_date);
            $m->set_data('work_report_type',$work_report_type);            
            $m->set_data('work_report_created_date',$dateTime);
            $m->set_data('template_ids',$template_id);

            $a = array(
                'society_id' =>$m->get_data('society_id'),
                'floor_id'=>$m->get_data('floor_id'),
                'user_id'=>$m->get_data('user_id'),
                'block_id'=>$m->get_data('block_id'),
                'work_report_date'=>$m->get_data('work_report_date'),
                'work_report_type'=>$m->get_data('work_report_type'),
                'work_report'=>$m->get_data('work_report'),
                'work_report_file'=>$m->get_data('work_report_file'),
                'template_ids'=>$m->get_data('template_ids'),
                'work_report_created_date'=>$m->get_data('work_report_created_date'),
            );
        
            $qry = $d->insert("work_report_master",$a);
            $work_report_id = $con->insert_id;

            $message = "Work report submitted";
        
            if ($qry == true) {

                $response['work_report_added'] = false;
                    
                $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND work_report_date = '$work_report_date' AND society_id = '$society_id'");

                if ($workReportQry > 0) {
                    $response['work_report_added'] = true;
                } 
                

                if ($work_report_type == 1) {

                    $work_report_employee_qa = explode("~",$work_report_employee_ans);
                    $template_question = explode("~",$template_question);
                    $question_type = explode("~",$question_type);
                    $question_type_value = explode("~",$question_type_value);
                    $work_report_is_required = explode("~",$work_report_is_required);

                    $countFile = 0;

                    $multiFileCount = 0;

                    for ($i=0; $i < count($template_question); $i++) { 
                        
                        $has_multi_attachment = 0;

                        if ($work_report_employee_qa[$i] == "image") {

                            $work_report_file = "";

                            $file11=$_FILES["work_report_file"]["tmp_name"][$countFile];

                            if ($file11 != "" || $file11 != null) {                    

                                $extId = pathinfo($_FILES['work_report_file']['name'][$countFile], PATHINFO_EXTENSION);
                                $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");

                                if(in_array($extId,$extAllow)) {
                                    $temp = explode(".", $_FILES["work_report_file"]["name"][$countFile]);
                                    $work_report_file = "wr_".$user_id.round(microtime(true)).$countFile . '.' . end($temp);
                                    move_uploaded_file($_FILES["work_report_file"]["tmp_name"][$countFile], "../img/work_report/" . $work_report_file);
                                } 
                            }

                            $countFile++;                            

                            $m->set_data('work_report_employee_ans',$work_report_file);
                        }else if (preg_match('/imagemulti/i', $work_report_employee_qa[$i])) {


                            $imagemulti = explode("_", $work_report_employee_qa[$i]);
                            $imageMultiCountSize = $imagemulti[1];

                            $aryMultiImage= array();

                            $newCount = $multiFileCount + $imageMultiCountSize;


                            for ($p=$multiFileCount; $p < $newCount; $p++) {
                                $work_report_file_multi = "";

                                $file11=$_FILES["work_report_file_multi"]["tmp_name"][$p];

                                if ($file11 != "" || $file11 != null) {                    

                                    $extId = pathinfo($_FILES['work_report_file_multi']['name'][$p], PATHINFO_EXTENSION);
                                    $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");

                                    if(in_array($extId,$extAllow)) {
                                        $temp = explode(".", $_FILES["work_report_file_multi"]["name"][$p]);
                                        $work_report_file_multi = "wr_".$user_id.round(microtime(true)).$p . '.' . end($temp);
                                        move_uploaded_file($_FILES["work_report_file_multi"]["tmp_name"][$p], "../img/work_report/" . $work_report_file_multi);
                                        $aryF = array();
                                        $aryF['file'] = $work_report_file_multi;
                                        array_push($aryMultiImage,$aryF);
                                    } 
                                }
                            }

                            $multiFileCount = $newCount;

                            $multiFileJsonData = json_encode($aryMultiImage);

                            $m->set_data('work_report_employee_ans',$multiFileJsonData);
                            $has_multi_attachment = "1";
                        }else{

                            if ($work_report_employee_qa[$i] == "NA" || $work_report_employee_qa[$i] == "na") {
                                $m->set_data('work_report_employee_ans',"");
                            }else{                                
                                $m->set_data('work_report_employee_ans',$work_report_employee_qa[$i]);
                            }
                        }

                        if ($question_type_value[$i] == "NA" || $question_type_value[$i] == "na" 
                            || $question_type_value[$i] == '') {
                            $qtValue = "";
                        }else{
                            $qtValue = $question_type_value[$i];
                        }

                        $m->set_data('work_report_template_id',$template_id);
                        $m->set_data('template_question',$template_question[$i]);
                        $m->set_data('question_type',$question_type[$i]);
                        $m->set_data('work_report_date',$work_report_date);
                        $m->set_data('work_report_is_required',$work_report_is_required[$i]);
                        $m->set_data('question_type_value',$qtValue);
                        $m->set_data('work_report_id',$work_report_id);
                        $m->set_data('work_report_template_name',$template_name);
                        $m->set_data('has_multi_attachment',$has_multi_attachment);
                        $m->set_data('work_report_created_date',date('Y-m-d H:i:s'));

                        $a = array(
                            'society_id' =>$m->get_data('society_id'),
                            'work_report_template_id' =>$m->get_data('work_report_template_id'),
                            'work_report_id' =>$m->get_data('work_report_id'),
                            'work_report_template_name' =>$m->get_data('work_report_template_name'),
                            'user_id' =>$m->get_data('user_id'),
                            'work_report_date' =>$m->get_data('work_report_date'),
                            'work_report_template_question' =>$m->get_data('template_question'),
                            'work_report_question_type'=>$m->get_data('question_type'),
                            'work_report_is_required'=>$m->get_data('work_report_is_required'),
                            'work_report_question_type_value'=>$m->get_data('question_type_value'),
                            'work_report_employee_ans'=>$m->get_data('work_report_employee_ans'),
                            'work_report_created_date'=>$m->get_data('work_report_created_date'),
                            'has_multi_attachment'=>$m->get_data('has_multi_attachment'),
                        );

                        $d->insert("work_report_employee_ans",$a);
                    }                    
                }   

                $response["message"] = $message;
                $response["work_report_id"] = $work_report_id;
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['editWorkReport']=="editWorkReport" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            
            extract( $_POST);

            $work_report_employee_qa = explode("~",$work_report_employee_ans);
            $work_report_employee_ans_id = explode("~",$work_report_employee_ans_id);

            $countFile = 0;

            for ($i=0; $i < count($work_report_employee_ans_id); $i++) { 
                $updateId = $work_report_employee_ans_id[$i];

                $work_report_file = "";

                if ($work_report_employee_qa[$i] == "image") {


                    $file11=$_FILES["work_report_file"]["tmp_name"][$countFile];

                    if ($file11 != "" || $file11 != null) {

                        $extId = pathinfo($_FILES['work_report_file']['name'][$countFile], PATHINFO_EXTENSION);
                        $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");

                        if(in_array($extId,$extAllow)) {
                            $temp = explode(".", $_FILES["work_report_file"]["name"][$countFile]);
                            $work_report_file = "wr_".$user_id.round(microtime(true)).$countFile . '.' . end($temp);
                            move_uploaded_file($_FILES["work_report_file"]["tmp_name"][$countFile], "../img/work_report/" . $work_report_file);

                            $countFile = $countFile+1;
                        } 

                        $m->set_data('work_report_employee_ans',$work_report_file);
                    }
                }else{
                    if ($work_report_employee_qa[$i] == "NA" || $work_report_employee_qa[$i] == "na") {
                        $m->set_data('work_report_employee_ans',"");
                    }else{                                
                        $m->set_data('work_report_employee_ans',$work_report_employee_qa[$i]);
                    }
                }

                $m->set_data('work_report_modified_date',date('Y-m-d H:i:s'));

                $a = array(
                    'work_report_employee_ans'=>$m->get_data('work_report_employee_ans'),
                    'work_report_modified_date'=>$m->get_data('work_report_modified_date'),
                );

                $qry = $d->update("work_report_employee_ans",$a,"work_report_employee_ans_id = '$updateId' AND work_report_id = '$work_report_id' AND user_id = '$user_id'");

            }

            if ($qry == true) {                    

                $response["message"] = "Report Updated";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['editWorkReportNew']=="editWorkReportNew" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            
            extract( $_POST);

            $work_report_employee_qa = explode("~",$work_report_employee_ans);
            $work_report_employee_ans_id = explode("~",$work_report_employee_ans_id);

            $countFile = 0;

            for ($i=0; $i < count($work_report_employee_ans_id); $i++) { 
                $updateId = $work_report_employee_ans_id[$i];

                $work_report_file = "";

                $has_multi_attachment = 0;
                $multiFileCount = 0;

                if ($work_report_employee_qa[$i] == "image") {


                    $file11=$_FILES["work_report_file"]["tmp_name"][$countFile];

                    if ($file11 != "" || $file11 != null) {

                        $extId = pathinfo($_FILES['work_report_file']['name'][$countFile], PATHINFO_EXTENSION);
                        $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");

                        if(in_array($extId,$extAllow)) {
                            $temp = explode(".", $_FILES["work_report_file"]["name"][$countFile]);
                            $work_report_file = "wr_".$user_id.round(microtime(true)).$countFile . '.' . end($temp);
                            move_uploaded_file($_FILES["work_report_file"]["tmp_name"][$countFile], "../img/work_report/" . $work_report_file);

                            $countFile = $countFile+1;
                        } 

                        $m->set_data('work_report_employee_ans',$work_report_file);
                    }
                }else if (preg_match('/imagemulti/i', $work_report_employee_qa[$i])) {

                    $imagemulti = explode("_", $work_report_employee_qa[$i]);
                    $imageMultiCountSize = $imagemulti[1];

                    $aryMultiImage= array();

                    $newCount = $multiFileCount + $imageMultiCountSize;

                    for ($p=$multiFileCount; $p < $newCount; $p++) {
                        $work_report_file_multi = "";

                        $file11=$_FILES["work_report_file_multi"]["tmp_name"][$p];

                        if ($file11 != "" || $file11 != null) {                    

                            $extId = pathinfo($_FILES['work_report_file_multi']['name'][$p], PATHINFO_EXTENSION);
                            $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");

                            if(in_array($extId,$extAllow)) {
                                $temp = explode(".", $_FILES["work_report_file_multi"]["name"][$p]);
                                $work_report_file_multi = "wr_".$user_id.round(microtime(true)).$p . '.' . end($temp);
                                move_uploaded_file($_FILES["work_report_file_multi"]["tmp_name"][$p], "../img/work_report/" . $work_report_file_multi);
                                $aryF = array();
                                $aryF['file'] = $work_report_file_multi;
                                array_push($aryMultiImage,$aryF);
                            } 
                        }
                    }

                    $multiFileCount = $newCount;

                    $multiFileJsonData = json_encode($aryMultiImage);

                    $m->set_data('work_report_employee_ans',$multiFileJsonData);
                    $has_multi_attachment = "1";
                }else{
                    if ($work_report_employee_qa[$i] == "NA" || $work_report_employee_qa[$i] == "na") {
                        $m->set_data('work_report_employee_ans',"");
                    }else{                                
                        $m->set_data('work_report_employee_ans',$work_report_employee_qa[$i]);
                    }
                }

                $m->set_data('work_report_modified_date',date('Y-m-d H:i:s'));
                $m->set_data('has_multi_attachment',$has_multi_attachment);

                if ($has_multi_attachment == 1) {
                 
                    $a = array(
                        'work_report_employee_ans'=>$m->get_data('work_report_employee_ans'),
                        'work_report_modified_date'=>$m->get_data('work_report_modified_date'),
                        'has_multi_attachment'=>$m->get_data('has_multi_attachment'),
                    );
                }else{
                    $a = array(
                        'work_report_employee_ans'=>$m->get_data('work_report_employee_ans'),
                        'work_report_modified_date'=>$m->get_data('work_report_modified_date'),
                    );
                }

                $qry = $d->update("work_report_employee_ans",$a,"work_report_employee_ans_id = '$updateId' AND work_report_id = '$work_report_id' AND user_id = '$user_id'");

            }

            if ($qry == true) {                    

                $response["message"] = "Report Updated";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getWorkReport']=="getWorkReport" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract(array_map("test_input" , $_POST));
            $query = $d->select("work_report_master","user_id = '$user_id' AND society_id = '$society_id' AND MONTH(work_report_date) = '$month' AND YEAR(work_report_date) = '$year'","ORDER BY work_report_date DESC");

            if(mysqli_num_rows($query)>0){
                
                $response["work_report"] = array();

                while($data=mysqli_fetch_array($query)) {

                    $data = array_map("html_entity_decode", $data);

                    $workReport = array(); 

                    $work_report_date = $data['work_report_date'];

                    $workReport["work_report_id"] = $data['work_report_id'];
                    $workReport["work_report_date"] = $work_report_date;
                    $workReport["work_report_date_view"] = date('d F Y', strtotime($work_report_date));
                    $workReport["work_report"] = $data['work_report'];

                    if ($work_report_date == date('Y-m-d')) {
                        $workReport["is_today"] = true;
                    }else{
                        $workReport["is_today"] = false;                    
                    }

                    if ($data['work_report_modified_date'] != null) {
                        $workReport["work_report_modified_date"] = date('d F Y, h:i A', strtotime($work_report_date));
                    }else{
                        $workReport["work_report_modified_date"] = "";
                    }

                    $workReport['template_data'] = array();

                    if ($data['work_report_type'] == 1) {

                        $workReport['is_template_report'] = true;

                        $qry = $d->select("work_report_employee_ans","work_report_id = '$data[work_report_id]' AND user_id = '$data[user_id]'","GROUP BY work_report_template_id");

                        if (mysqli_num_rows($qry) > 0) {
                            while($data2 = mysqli_fetch_array($qry)){
                                $templateData = array();

                                $templateData['work_report_template_id'] = $data2['work_report_template_id'];
                                $templateData['work_report_template_name'] = $data2['work_report_template_name'];

                                $qryqq = $d->select("work_report_employee_ans","work_report_id = '$data[work_report_id]' AND user_id = '$data[user_id]' AND work_report_template_id = '$data2[work_report_template_id]'");

                                if (mysqli_num_rows($qryqq) > 0) {

                                    $templateData['report'] = array();

                                    while($data3 = mysqli_fetch_array($qryqq)){
                                        $templateData2 = array();

                                        $templateData2['work_report_employee_ans_id'] = $data3['work_report_employee_ans_id'];
                                        $templateData2['work_report_template_name'] = $data3['work_report_template_name'];
                                        $templateData2['work_report_template_question'] = $data3['work_report_template_question'];
                                        $templateData2['work_report_question_type'] = $data3['work_report_question_type'];
                                        $templateData2['work_report_question_type_value'] = $data3['work_report_question_type_value'];

                                        if ($data3['work_report_is_required'] == 0) {
                                            $templateData2['is_required'] = true;
                                        }else{
                                            $templateData2['is_required'] = false;                            
                                        }

                                        if ($data3['work_report_question_type'] == 4) {
                                            if ($data3['work_report_employee_ans'] == '') {
                                                $templateData2['work_report_employee_ans'] = "";
                                            }else{                                                
                                                $templateData2['work_report_employee_ans'] = $base_url.'img/work_report/'. $data3['work_report_employee_ans'];
                                            }

                                            $templateData2['work_report_file_name'] =  $data3['work_report_employee_ans'].'';
                                        }else{

                                            $templateData2['work_report_employee_ans'] = $data3['work_report_employee_ans'];
                                            $templateData2['work_report_file_name'] =  "";
                                        }

                                        array_push($templateData['report'],$templateData2);
                                    }
                                }

                                array_push($workReport['template_data'],$templateData);
                            }
                        }
                    }else{
                        $workReport['is_template_report'] = false;
                    }

                    array_push($response["work_report"], $workReport);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Work Report";
                $response["status"] = "201";
                echo json_encode($response);
            }           
        }else if($_POST['getWorkReportNew']=="getWorkReportNew" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract(array_map("test_input" , $_POST));
            $query = $d->select("work_report_master","user_id = '$user_id' AND society_id = '$society_id' AND MONTH(work_report_date) = '$month' AND YEAR(work_report_date) = '$year'","ORDER BY work_report_created_date DESC");

            if(mysqli_num_rows($query)>0){
                
                $response["work_report"] = array();

                while($data=mysqli_fetch_array($query)) {

                    $data = array_map("html_entity_decode", $data);

                    $workReport = array(); 

                    $work_report_date = $data['work_report_date'];

                    $workReport["work_report_id"] = $data['work_report_id'];
                    $workReport["work_report_date"] = $work_report_date;
                    $workReport["work_report_date_view"] = date('d F Y', strtotime($work_report_date));
                    $workReport["work_submited_date_view"] = date('d M Y h:i A', strtotime($data['work_report_created_date']));
                    $workReport["work_report"] = $data['work_report'];

                    $workReport['documents'] = array();

                    $jsonArray = json_decode($data['work_report_file'],true);

                    if (is_array($jsonArray)) {
                        for ($i=0; $i < count($jsonArray); $i++) {
                            $document = $jsonArray["document_".$i];
                            $docs['name'] = $document;
                            $docs['document'] = $base_url."img/work_report/" .$document;
                            array_push($workReport['documents'],$docs);
                        }
                    }
                

                    if ($work_report_date == date('Y-m-d')) {
                        $workReport["is_today"] = true;
                    }else{
                        $workReport["is_today"] = false;                    
                    }

                    if ($data['work_report_modified_date'] != null) {
                        $workReport["work_report_modified_date"] = date('d F Y, h:i A', strtotime($work_report_date));
                    }else{
                        $workReport["work_report_modified_date"] = "";
                    }

                    $workReport['template_data'] = array();

                    if ($data['work_report_type'] == 1) {

                        $workReport['is_template_report'] = true;

                        $qry = $d->select("work_report_employee_ans","work_report_id = '$data[work_report_id]' AND user_id = '$data[user_id]'","GROUP BY work_report_template_id");

                        if (mysqli_num_rows($qry) > 0) {
                            while($data2 = mysqli_fetch_array($qry)){
                                $templateData = array();

                                $templateData['work_report_template_id'] = $data2['work_report_template_id'];
                                $templateData['work_report_template_name'] = $data2['work_report_template_name'];

                                $qryqq = $d->select("work_report_employee_ans","work_report_id = '$data[work_report_id]' AND user_id = '$data[user_id]' AND work_report_template_id = '$data2[work_report_template_id]'");

                                if (mysqli_num_rows($qryqq) > 0) {

                                    $templateData['report'] = array();

                                    while($data3 = mysqli_fetch_array($qryqq)){
                                        $templateData2 = array();

                                        $templateData2['work_report_employee_ans_id'] = $data3['work_report_employee_ans_id'];
                                        $templateData2['work_report_template_name'] = $data3['work_report_template_name'];
                                        $templateData2['work_report_template_question'] = $data3['work_report_template_question'];
                                        $templateData2['work_report_question_type'] = $data3['work_report_question_type'];
                                        $templateData2['work_report_question_type_value'] = $data3['work_report_question_type_value'];

                                        if ($data3['work_report_is_required'] == 0) {
                                            $templateData2['is_required'] = true;
                                        }else{
                                            $templateData2['is_required'] = false;                            
                                        }

                                        if ($data3['has_multi_attachment'] == 1) {
                                            $templateData2['has_multi_attachment'] = true;
                                        }else{
                                            $templateData2['has_multi_attachment'] = false;                            
                                        }

                                        $templateData2['base_url'] =$base_url."img/work_report/";

                                        if ($data3['work_report_question_type'] == 4) {
                                            if ($data3['work_report_employee_ans'] == '') {
                                                $templateData2['work_report_employee_ans'] = "";
                                            }else{
                                                if ($data3['has_multi_attachment'] == 1) {
                                                    $templateData2['work_report_employee_ans'] = $data3['work_report_employee_ans'];
                                                }else{                                                    
                                                    $templateData2['work_report_employee_ans'] = $base_url.'img/work_report/'. $data3['work_report_employee_ans'];
                                                }                                      
                                            }

                                            $templateData2['work_report_file_name'] =  $data3['work_report_employee_ans'].'';
                                        }else{

                                            $templateData2['work_report_employee_ans'] = $data3['work_report_employee_ans'];
                                            $templateData2['work_report_file_name'] =  "";
                                        }

                                        array_push($templateData['report'],$templateData2);
                                    }
                                }

                                array_push($workReport['template_data'],$templateData);
                            }
                        }
                    }else{
                        $workReport['is_template_report'] = false;
                    }

                    array_push($response["work_report"], $workReport);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Work Report";
                $response["status"] = "201";
                echo json_encode($response);
            }           
        }else if($_POST['getWorkReportOtherEmployee']=="getWorkReportOtherEmployee" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract(array_map("test_input" , $_POST));

            $query = $d->selectRow("
                work_report_master.*,
                users_master.user_full_name,
                users_master.user_designation,
                users_master.user_profile_pic,
                block_master.block_name,
                floors_master.floor_name
                ","
                work_report_master,
                users_master,
                floors_master,
                block_master
                ","
                work_report_master.society_id = '$society_id' 
                AND work_report_master.user_id = '$user_id'
                AND users_master.user_id = work_report_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id
                AND MONTH(work_report_master.work_report_date) = '$month' 
                AND YEAR(work_report_master.work_report_date) = '$year'
                $appendQuery","ORDER BY work_report_master.work_report_date DESC");

            if(mysqli_num_rows($query)>0){
                
                $response["work_report"] = array();

                while($data=mysqli_fetch_array($query)) {

                    $data = array_map("html_entity_decode", $data);

                    $workReport = array(); 

                    $work_report_date = $data['work_report_date'];

                    $workReport["work_report_id"] = $data['work_report_id'];
                    $workReport["user_full_name"] = $data['user_full_name'];
                    $workReport["user_designation"] = $data['user_designation'];
                    $workReport["block_name"] = $data['block_name'];
                    $workReport["floor_name"] = $data['floor_name'];
                    $workReport["work_report_date"] = $work_report_date;
                    $workReport["work_report_date_view"] = date('d F Y', strtotime($work_report_date));
                    $workReport["work_submited_date_view"] = date('d M Y h:i A', strtotime($data['work_report_created_date']));
                    $workReport["work_report"] = $data['work_report'];

                    if ($data['user_profile_pic'] != '') {
                        $workReport["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $workReport["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $workReport["is_today"] = false;                    

                    if ($data['work_report_modified_date'] != null) {
                        $workReport["work_report_modified_date"] = date('d F Y, h:i A', strtotime($work_report_date));
                    }else{
                        $workReport["work_report_modified_date"] = "";
                    }

                    $workReport['template_data'] = array();

                    if ($data['work_report_type'] == 1) {

                        $workReport['is_template_report'] = true;

                        $qry = $d->select("work_report_employee_ans","work_report_id = '$data[work_report_id]' AND user_id = '$data[user_id]'","GROUP BY work_report_template_id");

                        if (mysqli_num_rows($qry) > 0) {
                            while($data2 = mysqli_fetch_array($qry)){
                                $templateData = array();

                                $templateData['work_report_template_id'] = $data2['work_report_template_id'];
                                $templateData['work_report_template_name'] = $data2['work_report_template_name'];

                                $qryqq = $d->select("work_report_employee_ans","work_report_id = '$data[work_report_id]' AND user_id = '$data[user_id]' AND work_report_template_id = '$data2[work_report_template_id]'");

                                if (mysqli_num_rows($qryqq) > 0) {

                                    $templateData['report'] = array();

                                    while($data3 = mysqli_fetch_array($qryqq)){
                                        $templateData2 = array();

                                        $templateData2['work_report_employee_ans_id'] = $data3['work_report_employee_ans_id'];
                                        $templateData2['work_report_template_name'] = $data3['work_report_template_name'];
                                        $templateData2['work_report_template_question'] = $data3['work_report_template_question'];
                                        $templateData2['work_report_question_type'] = $data3['work_report_question_type'];
                                        $templateData2['work_report_question_type_value'] = $data3['work_report_question_type_value'];

                                        if ($data3['work_report_is_required'] == 0) {
                                            $templateData2['is_required'] = true;
                                        }else{
                                            $templateData2['is_required'] = false;                            
                                        }

                                        if ($data3['has_multi_attachment'] == 1) {
                                            $templateData2['has_multi_attachment'] = true;
                                        }else{
                                            $templateData2['has_multi_attachment'] = false;                            
                                        }

                                        $templateData2['base_url'] =$base_url."img/work_report/";

                                        if ($data3['work_report_question_type'] == 4) {
                                            if ($data3['work_report_employee_ans'] == '') {
                                                $templateData2['work_report_employee_ans'] = "";
                                            }else{
                                                if ($data3['has_multi_attachment'] == 1) {
                                                    $templateData2['work_report_employee_ans'] = $data3['work_report_employee_ans'];
                                                }else{                                                    
                                                    $templateData2['work_report_employee_ans'] = $base_url.'img/work_report/'. $data3['work_report_employee_ans'];
                                                }                                      
                                            }

                                            $templateData2['work_report_file_name'] =  $data3['work_report_employee_ans'].'';
                                        }else{

                                            $templateData2['work_report_employee_ans'] = $data3['work_report_employee_ans'];
                                            $templateData2['work_report_file_name'] =  "";
                                        }

                                        array_push($templateData['report'],$templateData2);
                                    }
                                }

                                array_push($workReport['template_data'],$templateData);
                            }
                        }
                    }else{
                        $workReport['is_template_report'] = false;
                    }

                    array_push($response["work_report"], $workReport);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Work Report";
                $response["status"] = "201";
                echo json_encode($response);
            }           
        }else if($_POST['getWorkReportOtherEmployeeNew']=="getWorkReportOtherEmployeeNew" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract(array_map("test_input" , $_POST));

            $query = $d->selectRow("
                work_report_master.*,
                users_master.user_full_name,
                users_master.user_designation,
                users_master.user_profile_pic,
                block_master.block_name,
                floors_master.floor_name
                ","
                work_report_master,
                users_master,
                floors_master,
                block_master
                ","
                work_report_master.society_id = '$society_id' 
                AND work_report_master.user_id = '$user_id'
                AND users_master.user_id = work_report_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id
                AND MONTH(work_report_master.work_report_date) = '$month' 
                AND YEAR(work_report_master.work_report_date) = '$year'
                $appendQuery","ORDER BY work_report_master.work_report_date DESC");

            if(mysqli_num_rows($query)>0){
                
                $response["work_report"] = array();

                while($data=mysqli_fetch_array($query)) {

                    $data = array_map("html_entity_decode", $data);

                    $workReport = array(); 

                    $work_report_date = $data['work_report_date'];

                    $workReport["work_report_id"] = $data['work_report_id'];
                    $workReport["user_full_name"] = $data['user_full_name'];
                    $workReport["user_designation"] = $data['user_designation'];
                    $workReport["block_name"] = $data['block_name'];
                    $workReport["floor_name"] = $data['floor_name'];
                    $workReport["work_report_date"] = $work_report_date;
                    $workReport["work_report_date_view"] = date('d F Y', strtotime($work_report_date));
                    $workReport["work_submited_date_view"] = date('d M Y h:i A', strtotime($data['work_report_created_date']));
                    $workReport["work_report"] = $data['work_report'];

                    $workReport['documents'] = array();

                    $jsonArray = json_decode($data['work_report_file'],true);

                    if (is_array($jsonArray)) {
                        for ($i=0; $i < count($jsonArray); $i++) {
                            $document = $jsonArray["document_".$i];
                            $docs['name'] = $document;
                            $docs['document'] = $base_url."img/work_report/" .$document;
                            array_push($workReport['documents'],$docs);
                        }
                    }

                    if ($data['user_profile_pic'] != '') {
                        $workReport["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $workReport["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $workReport["is_today"] = false;                    

                    if ($data['work_report_modified_date'] != null) {
                        $workReport["work_report_modified_date"] = date('d F Y, h:i A', strtotime($work_report_date));
                    }else{
                        $workReport["work_report_modified_date"] = "";
                    }

                    $workReport['template_data'] = array();

                    if ($data['work_report_type'] == 1) {

                        $workReport['is_template_report'] = true;

                        $qry = $d->select("work_report_employee_ans","work_report_id = '$data[work_report_id]' AND user_id = '$data[user_id]'","GROUP BY work_report_template_id");

                        if (mysqli_num_rows($qry) > 0) {
                            while($data2 = mysqli_fetch_array($qry)){
                                $templateData = array();

                                $templateData['work_report_template_id'] = $data2['work_report_template_id'];
                                $templateData['work_report_template_name'] = $data2['work_report_template_name'];

                                $qryqq = $d->select("work_report_employee_ans","work_report_id = '$data[work_report_id]' AND user_id = '$data[user_id]' AND work_report_template_id = '$data2[work_report_template_id]'");

                                if (mysqli_num_rows($qryqq) > 0) {

                                    $templateData['report'] = array();

                                    while($data3 = mysqli_fetch_array($qryqq)){
                                        $templateData2 = array();

                                        $templateData2['work_report_employee_ans_id'] = $data3['work_report_employee_ans_id'];
                                        $templateData2['work_report_template_name'] = $data3['work_report_template_name'];
                                        $templateData2['work_report_template_question'] = $data3['work_report_template_question'];
                                        $templateData2['work_report_question_type'] = $data3['work_report_question_type'];
                                        $templateData2['work_report_question_type_value'] = $data3['work_report_question_type_value'];

                                        if ($data3['work_report_is_required'] == 0) {
                                            $templateData2['is_required'] = true;
                                        }else{
                                            $templateData2['is_required'] = false;                            
                                        }

                                        if ($data3['has_multi_attachment'] == 1) {
                                            $templateData2['has_multi_attachment'] = true;
                                        }else{
                                            $templateData2['has_multi_attachment'] = false;                            
                                        }

                                        $templateData2['base_url'] =$base_url."img/work_report/";

                                        if ($data3['work_report_question_type'] == 4) {
                                            if ($data3['work_report_employee_ans'] == '') {
                                                $templateData2['work_report_employee_ans'] = "";
                                            }else{
                                                if ($data3['has_multi_attachment'] == 1) {
                                                    $templateData2['work_report_employee_ans'] = $data3['work_report_employee_ans'];
                                                }else{                                                    
                                                    $templateData2['work_report_employee_ans'] = $base_url.'img/work_report/'. $data3['work_report_employee_ans'];
                                                }                                      
                                            }

                                            $templateData2['work_report_file_name'] =  $data3['work_report_employee_ans'].'';
                                        }else{

                                            $templateData2['work_report_employee_ans'] = $data3['work_report_employee_ans'];
                                            $templateData2['work_report_file_name'] =  "";
                                        }

                                        array_push($templateData['report'],$templateData2);
                                    }
                                }

                                array_push($workReport['template_data'],$templateData);
                            }
                        }
                    }else{
                        $workReport['is_template_report'] = false;
                    }

                    array_push($response["work_report"], $workReport);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Work Report";
                $response["status"] = "201";
                echo json_encode($response);
            }           
        }else if($_POST['getWorkReportTemplate']=="getWorkReportTemplate" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract(array_map("test_input" , $_POST));

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
            }

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $shift_type = $shiftData['shift_type'];


            $reportIdQry = $d->selectRow("work_report_id","work_report_master","user_id = '$user_id' AND society_id = '$society_id' AND work_report_date = '$report_date'");

            $dataWR = mysqli_fetch_array($reportIdQry);

            if ($dataWR['work_report_id'] != null && $dataWR['work_report_id'] != '') {
                $response['work_report_id'] = $dataWR['work_report_id'];
            }else{
                $response['work_report_id'] = "0";
            }


            $query = $d->selectRow("
                template_assign_master.*,
                template_master.*, 
                (SELECT count(work_report_employee_ans.work_report_employee_ans_id) FROM work_report_employee_ans,work_report_master WHERE work_report_employee_ans.work_report_template_id = template_assign_master.template_id AND work_report_employee_ans.user_id = '$user_id' AND DATE_FORMAT(work_report_master.work_report_date,'%Y-%m-%d') = '$report_date' AND work_report_master.work_report_id = work_report_employee_ans.work_report_id) AS reportAdded
                ","
                template_assign_master LEFT JOIN template_master ON template_master.template_id = template_assign_master.template_id
                ","
                ((template_assign_master.society_id = '$society_id' AND template_assign_master.template_assign_for = '0') 
                OR (template_assign_master.society_id = '$society_id' AND template_assign_master.template_assign_for = '1' AND FIND_IN_SET('$block_id',template_assign_master.template_assign_ids)) 
                OR (template_assign_master.society_id = '$society_id' AND template_assign_master.template_assign_for = '2' AND FIND_IN_SET('$floor_id',template_assign_master.template_assign_ids))
                OR (template_assign_master.society_id = '$society_id' AND template_assign_master.template_assign_for = '3' AND FIND_IN_SET('$user_id',template_assign_master.template_assign_ids))) 
                AND template_master.template_id = template_assign_master.template_id AND template_master.template_status = '0'","GROUP BY template_master.template_id");


            if(mysqli_num_rows($query)>0){
                
                $response["template"] = array();

                while($data=mysqli_fetch_array($query)) {

                    $data = array_map("html_entity_decode", $data);

                    $template = array(); 

                    if ($data['reportAdded'] > 0) {
                        $template["reportAdded"] = true;
                    }else{
                        $template["reportAdded"] = false;                        
                    }

                    if ($data['allow_muliple_time']==1) {
                       $allow_muliple_time = true;
                    } else {
                       $allow_muliple_time = false;
                    }

                   
                    $template["template_id"] = $data['template_id'];
                    $template["allow_muliple_time"] = $allow_muliple_time;
                    $template["template_name"] = $data['template_name'];
                    $template["template_description"] = $data['template_description'];

                    $template['template_questions'] = array();

                    $qry = $d->select("template_question_master","template_id = '$data[template_id]' AND society_id = '$society_id' AND template_question_status = '0'");

                    if (mysqli_num_rows($qry) > 0) {
                        

                        while($queData = mysqli_fetch_array($qry)){
                            $que = array();

                            $queData = array_map("html_entity_decode", $queData);

                            $que['template_question_id'] = $queData['template_question_id'];
                            $que['template_question'] = $queData['template_question'];
                            $que['question_type'] = $queData['question_type'];
                            $que['question_type_value'] = $queData['question_type_value'];
                            $que['question_value_count'] = $queData['question_value_count'];
                            $que['template_multiple_file'] = $queData['template_multiple_file'];

                            if ($queData['is_required'] == 0) {
                                $que['is_required'] = true;
                            }else{
                                $que['is_required'] = false;                            
                            }

                            array_push($template['template_questions'],$que);
                        }
                    }

                    array_push($response["template"], $template);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No templates";
                $response["status"] = "201";
                echo json_encode($response);
            }           
        }else if($_POST['getTemplateForSummary']=="getTemplateForSummary" && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract(array_map("test_input" , $_POST));

            $query = $d->select("template_master","society_id = '$society_id' AND template_status = 0");

            if(mysqli_num_rows($query)>0){
                
                $response["template"] = array();

                while($data=mysqli_fetch_array($query)) {

                    $data = array_map("html_entity_decode", $data);

                    $template = array(); 

                    $template["template_id"] = $data['template_id'];
                    $template["template_name"] = $data['template_name'];
                    $template["template_description"] = $data['template_description'];
                    array_push($response["template"], $template);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No templates";
                $response["status"] = "201";
                echo json_encode($response);
            }           
        }else if($_POST['getTemplateSummary']=="getTemplateSummary" && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract(array_map("test_input" , $_POST));

            if ($is_filter == "true") {

                if ($is_first_call == "true") {
                    $access_type = $work_report_Access;

                    include "check_access_data.php";

                    switch ($access_for) {
                        case '0':
                        case '1':
                            $appendQry = " AND work_report_master.block_id IN ('$accessBranchIds')";
                            break;
                        case '2':
                            $appendQry = " AND work_report_master.floor_id IN ('$accessDepIds')";
                            break;
                        case '3':
                            $appendQry = " AND work_report_master.user_id IN ('$accessUserIds')";
                            break;
                        
                        default:                    
                            break;
                    }

                    $appendQry = $appendQry . " AND DATE_FORMAT(work_report_employee_ans.work_report_created_date,'%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'";

                }else{
                    if ($block_id != '') {
                        $appendQry = " AND work_report_master.block_id = '$block_id'";
                    }

                    if ($floor_id != '') {
                        $appendQry = $appendQry . " AND work_report_master.floor_id = '$floor_id'";
                    }

                    if ($user_id != '') {
                        $appendQry = $appendQry . " AND work_report_master.user_id = '$user_id'";
                    }

                    if ($start_date != '' && $end_date != '') {
                        $appendQry = $appendQry . " AND DATE_FORMAT(work_report_employee_ans.work_report_created_date,'%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'";
                    }
                } 
            }
            

            $query = $d->select("template_question_master","society_id = '$society_id' AND template_id = '$template_id' AND question_type IN ('1','2','3','8','9','10','11')");

            if(mysqli_num_rows($query)>0){
                
                $response["template_questions"] = array();

                while($data=mysqli_fetch_array($query)) {
                    $template_question = str_replace("'", "\'", $data['template_question']);


                    $data = array_map("html_entity_decode", $data);

                    $template = array(); 

                    $questionType = $data['question_type'];
                    $questionTypeValue = $data['question_type_value'];

                    $template["template_question_id"] = $data['template_question_id'];
                    $template["template_id"] = $data['template_id'];
                    $template["template_question"] = $data['template_question'];
                    $template["template_multiple_file"] = $data['template_multiple_file'];
                    $template["question_type"] = $questionType;
                    $template["question_type_value"] = $questionTypeValue;
                    $template["total_numbers"] = "";
                    $template["total_percentage"] = "";
                    $template["question_type_value_summary"] = array();

                    if ($questionType == 1 || $questionType == 2 || $questionType == 3) {
                        $jsonArray = json_decode($questionTypeValue,true);


                        for ($i=0; $i < count($jsonArray); $i++) { 

                            $summaryArray = array();
                            $queValue = $jsonArray["option_$i"];

                            if ($is_filter == "true") {

                                $countQry = $d->selectRow("
                                    work_report_employee_ans.work_report_employee_ans_id
                                    ","
                                    work_report_employee_ans,
                                    work_report_master
                                    ","work_report_employee_ans.work_report_template_question = '$template_question' AND 
                                    work_report_employee_ans.work_report_id = work_report_master.work_report_id                                    
                                    AND work_report_employee_ans.work_report_employee_ans LIKE '%$queValue%' 
                                    AND work_report_employee_ans.work_report_question_type = '$questionType' 
                                    AND work_report_employee_ans.work_report_template_id = '$template_id' $appendQry");

                                $countQry = mysqli_num_rows($countQry);

                            }else{

                                $countQry = $d->count_data_direct("work_report_employee_ans_id","work_report_employee_ans.work_report_template_question = '$template_question' AND work_report_employee_ans","work_report_employee_ans LIKE '%$queValue%' AND work_report_question_type = '$questionType' AND work_report_template_id = '$template_id'");
                            }

                            $summaryArray["option_name"] = $queValue;   
                            $summaryArray["option_value"] = $countQry.'';  

                            array_push($template["question_type_value_summary"],$summaryArray);
                        }
                    }else if ($questionType == 10) {

                        if ($is_filter == "true") {

                            $qry1 = $d->selectRow("
                                work_report_employee_ans.work_report_employee_ans
                                ","
                                work_report_employee_ans,
                                work_report_master
                                ","work_report_employee_ans.work_report_template_question = '$template_question' AND 
                                work_report_employee_ans.work_report_id = work_report_master.work_report_id 
                                AND work_report_employee_ans.work_report_question_type = '$questionType' 
                                AND work_report_employee_ans.work_report_template_id = '$template_id' $appendQry");
                        }else{

                            $qry1 = $d->selectRow("work_report_employee_ans","work_report_employee_ans","work_report_template_question = '$template_question' AND work_report_template_id = '$template_id' AND work_report_question_type = '$questionType'");
                        }

                        if(mysqli_num_rows($qry1)>0){
                
                            while($data=mysqli_fetch_array($qry1)) {

                                $data = array_map("html_entity_decode", $data);

                                $templateAns = array(); 

                                $workReportEmployeeAns = $data['work_report_employee_ans'];

                                $jsonArray = json_decode($workReportEmployeeAns,true);

                                if (is_array($jsonArray)) {
                                    for ($i=0; $i < count($jsonArray); $i++) {
                                        $topicTime = $jsonArray[$i]["time"];
                                        if ($topicTime != '' && 
                                            filter_var($topicTime, FILTER_VALIDATE_INT) == true) {
                                            $topicTimeCount = $topicTimeCount + $topicTime;
                                        }
                                    }
                                }
                            }
                        }

                        if ($topicTimeCount > 0) {
                            $template["total_numbers"] = $topicTimeCount.' Minutes';
                        }else{
                            $template["total_numbers"] = '0 Minute';                            
                        }

                    }else if ($questionType == 11) {

                        if ($is_filter == "true") {

                            $qry1 = $d->selectRow("
                                work_report_employee_ans.work_report_employee_ans
                                ","
                                work_report_employee_ans,
                                work_report_master
                                ","work_report_employee_ans.work_report_template_question = '$template_question' AND 
                                work_report_employee_ans.work_report_id = work_report_master.work_report_id 
                                AND work_report_employee_ans.work_report_question_type = '$questionType' 
                                AND work_report_employee_ans.work_report_template_id = '$template_id' $appendQry");
                        }else{

                            $qry1 = $d->selectRow("work_report_employee_ans","work_report_employee_ans","work_report_template_question = '$template_question' AND work_report_template_id = '$template_id' AND work_report_question_type = '$questionType'");
                        }

                        if(mysqli_num_rows($qry1)>0){

                            while($data=mysqli_fetch_array($qry1)) {

                                $data = array_map("html_entity_decode", $data);

                                $templateAns = array(); 

                                $workReportEmployeeAns = $data['work_report_employee_ans'];

                                $jsonArray1 = json_decode($workReportEmployeeAns,true);

                                if (is_array($jsonArray1)) {
                                    $topicTime1 = $jsonArray1["time"];
                                    if ($topicTime1 != '' && 
                                        filter_var($topicTime1, FILTER_VALIDATE_INT) == true) {
                                        $topicTimeCount1 = $topicTimeCount1 + $topicTime1;
                                    }
                                }
                            }
                        }

                        if ($topicTimeCount1 > 0) {
                            $template["total_numbers"] = $topicTimeCount1.' Minutes';
                        }else{
                            $template["total_numbers"] = '0 Minute';                            
                        }

                    }else if($questionType == 8){
                        if ($is_filter == "true") {

                            $sumQry = $d->sum_data("work_report_employee_ans.work_report_employee_ans","
                                work_report_employee_ans,
                                work_report_master
                                ","work_report_employee_ans.work_report_template_question = '$template_question' AND 
                                work_report_employee_ans.work_report_id = work_report_master.work_report_id
                                AND work_report_employee_ans.work_report_question_type = '$questionType' 
                                AND work_report_employee_ans.work_report_template_id = '$template_id' $appendQry");

                            $sumData=mysqli_fetch_array($sumQry);
                            $totalNumberSum = $sumData['SUM(work_report_employee_ans.work_report_employee_ans)'];

                        }else{
                            $sumQry = $d->sum_data("work_report_employee_ans","work_report_employee_ans","work_report_employee_ans.work_report_template_question = '$template_question' AND work_report_question_type = '$questionType' AND work_report_template_id = '$template_id'");

                            $sumData=mysqli_fetch_array($sumQry);
                            $totalNumberSum = $sumData['SUM(work_report_employee_ans)'];
                        }   

                        
                        $template["total_numbers"] = number_format($totalNumberSum,2,'.','').'';
                    }else if($questionType == 9){
                        if ($is_filter == "true") {

                            $totalPercentageQry = $d->sum_data("work_report_employee_ans.work_report_employee_ans","
                                work_report_employee_ans,
                                work_report_master
                                ","work_report_employee_ans.work_report_template_question = '$template_question' AND 
                                work_report_employee_ans.work_report_id = work_report_master.work_report_id
                                AND work_report_employee_ans.work_report_question_type = '$questionType' 
                                AND work_report_employee_ans.work_report_template_id = '$template_id' $appendQry");

                            $sumPercentageData=mysqli_fetch_array($totalPercentageQry);
                            $totalPercentageSum = $sumPercentageData['SUM(work_report_employee_ans.work_report_employee_ans)'];

                            $totalPercentageEntry = $d->selectRow("work_report_employee_ans.work_report_employee_ans","
                                work_report_employee_ans,
                                work_report_master
                                ","work_report_employee_ans.work_report_template_question = '$template_question' AND 
                                work_report_employee_ans.work_report_id = work_report_master.work_report_id
                                AND work_report_employee_ans.work_report_question_type = '$questionType' 
                                AND work_report_employee_ans.work_report_template_id = '$template_id' $appendQry");

                            $totalPercentageEntry = mysqli_num_rows($totalPercentageEntry);                           

                        }else{
                            $totalPercentageQry = $d->sum_data("work_report_employee_ans","work_report_template_question = '$template_question' AND work_report_employee_ans","work_report_question_type = '$questionType' AND work_report_template_id = '$template_id'");

                            $totalPercentageEntry = $d->count_data_direct("work_report_employee_ans_id","work_report_template_question = '$template_question' AND work_report_employee_ans","work_report_question_type = '$questionType' AND work_report_template_id = '$template_id'");

                            $sumPercentageData=mysqli_fetch_array($totalPercentageQry);
                            $totalPercentageSum = $sumPercentageData['SUM(work_report_employee_ans)'];
                        }

                       
                        $averagePecentage = $totalPercentageSum / $totalPercentageEntry;
                        if ($averagePecentage > 0) {
                            $template["total_percentage"] = number_format($averagePecentage,2,'.','').'';
                        }else{
                            $template["total_percentage"] = "0";
                        }
                    }


                    array_push($response["template_questions"], $template);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No templates";
                $response["status"] = "201";
                echo json_encode($response);
            }           
        }else if($_POST['getTemplateSummaryFilterData']=="getTemplateSummaryFilterData" && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract(array_map("test_input" , $_POST));

            $access_type = $work_report_Access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);        
        }else if($_POST['workReportCalendar']=="workReportCalendar" && $_POST['user_id']!='' && filter_var($_POST['society_id'], FILTER_VALIDATE_INT) == true){
            extract(array_map("test_input" , $_POST));
            $response['data'] = array();

            $datesArrayOfMonth = range_date($month_start_date,$month_end_date);

            $yesterdayDate = date('Y-m-d',strtotime('-1 days'));
            $previousDayOfYesterdayDate = date('Y-m-d',strtotime('-2 days'));
 
            for ($j = 0; $j < count($datesArrayOfMonth); $j++) {
                
                $wrAry = array();

                $monthDate = $datesArrayOfMonth[$j];

                $wrAry['monthDate'] = $monthDate;
                $wrAry['has_work_report'] = false;
                $wrAry['present'] = false;

                if ($monthDate < date('Y-m-d')) {
                    $wrAry['is_date_gone'] = true;
                }else{
                    $wrAry['is_date_gone'] = false;
                }

                if ($monthDate == date('Y-m-d')) {
                    $wrAry['is_today'] = true;
                }else{
                    $wrAry['is_today'] = false;
                }

                $wrAry['allow_report'] = false;

                /*$qry = $d->select("attendance_master AS am LEFT JOIN work_report_master AS wr ON am.attendance_date_start = wr.work_report_date","am.society_id = '$society_id'
                    AND am.user_id = '$user_id'
                    AND am.user_id = wr.user_id
                    AND am.attendance_date_start = '$monthDate'");*/

                $qry = $d->select("attendance_master","society_id = '$society_id'
                    AND user_id = '$user_id'
                    AND attendance_date_start = '$monthDate'");

                if (mysqli_num_rows($qry) > 0) {

                    while($data = mysqli_fetch_array($qry)){

                        $wrAry['present'] = true;

                        if ($monthDate == $yesterdayDate) {
                            $wrAry['allow_report'] = true;
                        }

                        if ($monthDate == $previousDayOfYesterdayDate) {
                            $wrAry['allow_report'] = true;
                        }

                        if ($monthDate == date('Y-m-d')) {
                            $wrAry['allow_report'] = true;
                        }

                        $qry1 = $d->select("work_report_master","society_id = '$society_id'
                            AND user_id = '$user_id'
                            AND work_report_date = '$monthDate'");

                        if (mysqli_num_rows($qry1) > 0) {

                            $data1 = mysqli_fetch_array($qry1);

                            if (($data1['work_report_id'] != null || 
                                $data1['work_report_id'] > 0 || 
                                $data1['work_report_id'] != '') && $data1['work_report_type'] == 0) {
                                $wrAry['has_work_report'] = true;
                            }
                        }
                    }
                }
                array_push($response['data'],$wrAry);
            }

            echo json_encode($response);
        
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function createRange($start, $end, $format = 'Y-m-d') {
    $start  = new DateTime($start);
    $end    = new DateTime($end);
    $invert = $start > $end;

    $dates = array();
    $dates[] = $start->format($format);
    while ($start != $end) {
        $start->modify(($invert ? '-' : '+') . '1 day');
        $dates[] = $start->format($format);
    }
    return $dates;
}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}


?>