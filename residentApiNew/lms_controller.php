<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d H:i:s");

    if($_POST['getCourses']=="getCourses" && $society_id !=""){

        $q=$d->selectRow("course_master.*,lms_submit_master.completed_percentage,(SELECT count(course_chapter_id) FROM course_chapter_master WHERE course_chapter_master.course_id = course_master.course_id) AS totalLessons","course_master LEFT JOIN lms_submit_master ON lms_submit_master.course_id = course_master.course_id AND lms_submit_master.user_id = '$user_id'","course_master.society_id = '$society_id' AND course_master.course_is_active = 0 AND course_master.course_is_deleted = 0","ORDER BY course_master.course_id DESC");

        if(mysqli_num_rows($q)>0){
            
            $response["course"] = array();

            while($data=mysqli_fetch_array($q)) {  

                $data = array_map("html_entity_decode", $data);

                $course = array(); 

                $course_id = $data['course_id'];
                
                $course["course_id"]=$data['course_id'];
                $course["course_title"]=$data['course_title'];
                $course["course_description"]=$data['course_description'];
                if ($data['course_image']!="") {
                    $course["course_image"]= $base_url."img/course/".$data['course_image'];     
                }else{
                    $course["course_image"]= "";
                }
            
                $course['total_chapters'] = $data['totalLessons'].'';

                if ($data['completed_percentage'] > 0) {
                    $course['complete_percentage'] = (int)$data['completed_percentage'].'';
                }else{
                    $course['complete_percentage'] = '0';
                }

                
                array_push($response["course"], $course);
            }
            $response["message"]="Course List";
            $response["status"]="200";
            echo json_encode($response);
        }else{

            $response["message"]="No Course Found.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if ($_POST['getCourseDetails'] == "getCourseDetails" && $course_id != '' && $user_id !="" && $society_id !="") {


        $q=$d->selectRow("course_master.*,lms_submit_master.completed_percentage,(SELECT count(course_chapter_id) FROM course_chapter_master WHERE course_chapter_master.course_id = course_master.course_id) AS totalLessons","course_master  LEFT JOIN lms_submit_master ON lms_submit_master.course_id = course_master.course_id AND lms_submit_master.user_id = '$user_id'","course_master.society_id = '$society_id' AND course_master.course_is_active = 0 AND course_master.course_is_deleted = 0 AND course_master.course_id = '$course_id'","ORDER BY course_id DESC");

        if(mysqli_num_rows($q)>0){
            
            $data=mysqli_fetch_array($q);
            $data = array_map("html_entity_decode", $data);
            $course_id = $data['course_id'];
            
            $response["course_id"]=$data['course_id'];
            $response["course_title"]=$data['course_title'];
            $response["course_description"]=$data['course_description'];

            if ($data['course_image']!="") {
                $response["course_image"]= $base_url."img/course/".$data['course_image'];     
            }else{
                $response["course_image"]= "";
            }

            $response['total_chapters'] = $data['totalLessons'].'';

            if ($data['completed_percentage'] > 0) {
                $response['complete_percentage'] = (int)$data['completed_percentage'].'';
            }else{
                $response['complete_percentage'] = '0';
            }
        }

        $chaptersQry = $d->selectRow("course_chapter_master.*,(SELECT count(lessons_id) FROM course_lessons_master WHERE course_lessons_master.course_chapter_id = course_chapter_master.course_id) AS totalLession","course_chapter_master","course_chapter_master.society_id='$society_id' AND course_chapter_master.course_chapter_is_deleted=0 AND course_chapter_master.course_id='$course_id'","ORDER BY course_chapter_master.course_chapter_id ASC");

        if (mysqli_num_rows($chaptersQry) > 0) {

            $response['chapters'] = array();

            while($data = mysqli_fetch_assoc($chaptersQry)) { 

                $data = array_map("html_entity_decode", $data);
            
                $chapters = array(); 

                $chapterId = $data['course_chapter_id'];

                $chapters['course_chapter_id'] = $chapterId;
                $chapters['course_id'] = $data['course_id'];
                $chapters['chapter_title'] = $data['chapter_title'];
                $chapters['chapter_description'] = $data['chapter_description'];

                $chapters['total_lessons'] = $datap['totalLession'].'';
                
                $lessonQry = $d->selectRow("course_lessons_master.*,lesson_type_master.*,(SELECT count(user_course_master.user_course_id) FROM user_course_master WHERE user_course_master.course_chapter_id = '$chapterId' AND user_course_master.user_id = '$user_id' AND course_lessons_master.lessons_id = user_course_master.lessons_id) AS totalLessionWatched","course_lessons_master,lesson_type_master","course_lessons_master.lesson_is_deleted = 0 AND course_lessons_master.lesson_is_active = 0 AND course_lessons_master.course_chapter_id='$chapterId' AND lesson_type_master.lesson_type_id = course_lessons_master.lesson_type_id");


                if (mysqli_num_rows($lessonQry) > 0) {

                    $chapters['lessons'] = array();

                    while($lessonData = mysqli_fetch_assoc($lessonQry)) {  

                        $lessonData = array_map("html_entity_decode", $lessonData);

                        $lessons = array();

                        $lessons_id = $lessonData['lessons_id'];

                        $lessons['lessons_id'] = $lessons_id;
                        $lessons['lesson_type_id'] = $lessonData['lesson_type_id'];
                        $lessons['lesson_type'] = $lessonData['lesson_type'];
                        $lessons['lesson_title'] = $lessonData['lesson_title'];
                        $lessons['lesson_is_file_or_link'] = $lessonData['lesson_is_file_or_link'];
                        $lessons['total_video_duration'] = $lessonData['total_video_duration'];
                        $lessons['video_watch_percentage'] = $lessonData['video_watch_percentage'];

                        if ($lessonData['lesson_video'] != '') {
                            $lessons['lesson_file'] = $base_url.'img/course/'.$lessonData['lesson_video'];
                        }else{
                            $lessons['lesson_file'] = "";
                        }

                        $lessons['lesson_video_link'] = $lessonData['lesson_video_link'];
                        $lessons['lesson_text'] = $lessonData['lesson_text'];

                        if ($lessonData['totalLessionWatched'] > 0) {
                            $lessons['lessons_watched'] = true;
                        }else{
                            $lessons['lessons_watched'] = false;
                        }

                        array_push($chapters['lessons'],$lessons);
                    }
                }


                array_push($response['chapters'],$chapters);
            }

            $response["message"]="Course Details";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"] = "Failed";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }else if ($_POST['addToWatchedList'] == "addToWatchedList" && $course_id != '' && $course_chapter_id != '' && $lessons_id  != '' && $user_id !="" && $society_id !="") {

        $coundTotalLessons = $d->count_data_direct("lessons_id","course_lessons_master","course_id = '$course_id' AND lesson_is_deleted = '0'");

        $q1 = $d->selectRow("lms_submit_id","lms_submit_master","course_id = '$course_id'  AND user_id= '$user_id' AND society_id = '$society_id'");

        $q1Data = mysqli_fetch_array($q1);

        if (mysqli_num_rows($q1) == 0) {
            $value1 = array(
                'society_id'=>$society_id,
                'course_id'=>$course_id,
                'user_id'=>$user_id,
                'submitted_date'=>date('Y-m-d H:i:s'),
                'block_id'=>$block_id,
                'floor_id'=>$floor_id
            );

            $result1 =  $d->insert('lms_submit_master',$value1);
            $lms_submit_id = $con->insert_id;
        }else{
            $lms_submit_id = $q1Data['lms_submit_id'];            
        }

        $q = $d->selectRow("user_course_id","user_course_master","course_id = '$course_id' AND course_chapter_id = '$course_chapter_id' AND lessons_id = '$lessons_id' AND user_id= '$user_id' AND society_id = '$society_id'");

        if (mysqli_num_rows($q) > 0) {
            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
            exit();
        }

        $value = array(
            'course_id'=>$course_id,
            'course_chapter_id'=>$course_chapter_id,
            'lessons_id '=>$lessons_id,
            'user_id'=>$user_id,
            'society_id'=>$society_id,
            'lms_submit_id'=>$lms_submit_id,
            'completed_percentage'=>"100",
            'resume_seconds'=>$resume_seconds,
            'user_course_created_at'=>$temDate
        );

        $result =  $d->insert('user_course_master',$value);
        
        if ($result) { 

            $countUserWatched = $d->count_data_direct("user_course_id","user_course_master","course_id = '$course_id'  AND user_id = '$user_id' AND completed_percentage = '100'");

            $count1 = $countUserWatched / $coundTotalLessons;
            $count2 = $count1 * 100;
            $completed_percentage = number_format($count2, 0);

            if ($completed_percentage < 100) {
                $value1 = array(
                    'completed_percentage'=>$completed_percentage
                );
            }else{
                $value1 = array(
                    'completed_percentage'=>$completed_percentage,
                    'completed_date'=>date('Y-m-d H:i:s')
                );
            }

            $result1 =  $d->update('lms_submit_master',$value1,"course_id = '$course_id'  AND user_id= '$user_id' AND society_id = '$society_id'");

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Failed";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
}
?>


