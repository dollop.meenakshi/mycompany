<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d h:i A");

    ///////////////get course
    if($_POST['action']=="get_course"){

       if($society_id !="" ){
                $q=$d->select("course_master","society_id='$society_id' AND course_is_active=0 AND course_is_deleted=0","ORDER BY course_id DESC");
                if(mysqli_num_rows($q)>0){
                        $response["course"] = array();

                    while($data=mysqli_fetch_array($q)) {                        
                            $course = array();    
                            $course["course_id"]=$data['course_id'];
                            $course["course_title"]=$data['course_title'];
                            $course["course_description"]=$data['course_description'];
                            $course["course_image"]=$data['course_image'];     
                            $course["course_created_date"]=$data['course_created_date'];     
                            array_push($response["course"], $course);
                    }
                    $response["message"]="Course List";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No Course Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }
   
   
///////////////////get course by id
    if ($_POST['action'] == "get_course_by_id") {
        if($course_id != '' && $user_id !="" && $society_id !="")
        {
            $mainData=array();
           
            $q = $d->selectRow('*',"course_master", "course_id='$course_id'");  
            $data = mysqli_fetch_assoc($q);          
            if ($data) {   


                $q2=$d->select("course_chapter_master","course_chapter_master.society_id='$society_id' AND course_chapter_master.course_chapter_is_deleted=0 AND course_chapter_master.course_id='$course_id'");
          
                while($d1=mysqli_fetch_assoc($q2)) { 
                 $lessondata=array(); 
                    $q3=$d->select("course_lessons_master","course_lessons_master.society_id='$society_id' AND course_lessons_master.lesson_is_deleted=0 AND course_lessons_master.course_id='$course_id' AND course_chapter_id='$d1[course_chapter_id]");

                     while($lessondata2=mysqli_fetch_assoc($q3)) {  
                            array_push($lessondata,$lessondata2);
                     }
                     $d1['lesson']=$lessondata;
                    array_push($mainData,$d1);
                }
                $data['lesson']=$mainData;
                $response["course"] = $data;
                $response["message"] = "Course";
                $response["status"] = "200";
                echo json_encode($response);
            } else {    
                $response["course"] = "";
                $response["message"] = "Course";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }
        else {    
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

////////////////////////add user watched video

if ($_POST['action'] == "add_user_watched_course") {
    $value = array(
        'course_id'=>$course_id,
        'lessons_id'=>$lessons_id,
        'user_id'=>$user_id,
        'society_id'=>$society_id,
        'user_course_created_at'=>$temDate

    );
    if( $course_id != '' && $lessons_id != '' && $user_id !="" && $society_id !="" )
    {       
       $result =  $d->insert('user_course_master',$value);
        if ($result) {    
            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Failed";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "data required";
        $response["status"] = "201";
        $response["data"] = $value;
        echo json_encode($response);
    }
}

}
?>


