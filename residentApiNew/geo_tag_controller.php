<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

  if ($key==$keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));

    if($_POST['getgeoTag']=="getgeoTag"){

      $limitQuery ="";

      $queryAry=array();
      if ($block_id!=0 && $block_id!='' && $block_id>0) {
        $append_query="users_master.block_id='$block_id'";
        array_push($queryAry, $append_query);
      } 
      if ($floor_id!=0 && $floor_id!='' && $floor_id>0) {
        $append_query1 ="users_master.floor_id='$floor_id'";
        array_push($queryAry, $append_query1);
      }

      if ($other_user_id!=0 && $other_user_id!='') {
        $append_query2 ="users_master.user_id='$other_user_id'";
        array_push($queryAry, $append_query2);
      }

      if ($my_team==1 && $my_team!='') {
        $append_query3 ="users_master.parent_id='$user_id'";
        array_push($queryAry, $append_query3);
        $append_query4 ="users_master.member_status='1'";
        array_push($queryAry, $append_query4);
      } else {
        $append_query4 ="users_master.member_status='0'";
        array_push($queryAry, $append_query4);
      }

      if ($get_new_member==1 ) {
        $limitQuery ="LIMIT 10";
      }

      $appendQuery= implode(" AND ", $queryAry);
      if ($appendQuery!='') {
        $appendQuery= " AND ".$appendQuery;
      }

      
        $q1= $d->select("unit_master,users_master,block_master,floors_master,society_master", "users_master.delete_status=0 AND  society_master.society_id=users_master.society_id AND unit_master.unit_status ='1' AND  unit_master.unit_id = users_master.unit_id  AND block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND users_master.user_latitude!='' AND users_master.user_longitude!='' 
        $appendQuery","ORDER BY user_id DESC $limitQuery");


      if(mysqli_num_rows($q1)>0){

        $response["geoTagList"] = array();


        while($data_app=mysqli_fetch_array($q1)) {

          $geoTagList=array();


          $geoTagList["plot_name"]=$data_app["floor_name"]."-".$data_app["block_name"];



          $geoTagList["user_latitude"]=$data_app["user_latitude"];
          $geoTagList["user_longitude"]=$data_app["user_longitude"];
          $geoTagList["user_full_name"]=$data_app["user_full_name"];
          $geoTagList["user_email"]=$data_app["user_email"];
          
          $geoTagList["company_name"] = html_entity_decode($data_app["floor_name"].'-'.$data_app["block_name"]);
          $geoTagList["plot_address"] = $data_app["user_designation"];

          if ($data_app["company_email"]=="") {
            $geoTagList["company_email"]="Not Available";
          }else{
            $geoTagList["company_email"]=$data_app["company_email"];
          }

          if ($data_app["company_contact_number"]=="" || $data_app["company_contact_number"]=="0" ) {
            $geoTagList["company_contact_number"]="Not Available";
          }else{
            $geoTagList["company_contact_number"]=$data_app["company_contact_number"];

          }

          if ($data_app["company_website"]=="") {
            $geoTagList["company_website"]="Not Available";
          }else{
            $geoTagList["company_website"]=$data_app["company_website"];

          }


          $geoTagList["user_id"]=$data_app["user_id"];
          $geoTagList["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_app['user_profile_pic'];
          $geoTagList["public_mobile"] = $data_app['public_mobile'] . '';




          if ($data_app['public_mobile']=='1') {

            $geoTagList["user_mobile"] = "".substr($data_app['user_mobile'], 0, 3) . '****' . substr($data_app['user_mobile'],  -3);
          }else{
            $geoTagList["user_mobile"]=$data_app["user_mobile"];

          }

          if ($user_latitude!="0.0" || $user_latitude!="") {
            $radiusInMeeter=  $d->haversineGreatCircleDistance($user_latitude,$user_longitude,$data_app["user_latitude"], $data_app["user_longitude"]);
            $totalKm= number_format($radiusInMeeter/1000,2,'.','');
            $geoTagList["distance"]=$totalKm. ' KM';
            $geoTagList["radius_distance"]="$totalKm";
          } else {
            $geoTagList["distance"]='';
            $geoTagList["radius_distance"]="";
          }

          array_push($response["geoTagList"], $geoTagList); 

        }

        $response["message"]="Member Found";
        $response["status"]="200";
        echo json_encode($response);
      }else{
        $response["message"]="No Member Found";
        $response["status"]="201";
        echo json_encode($response); 
      }

    }else {
      $response["message"]="wrong tag.";
      $response["status"]="201";
      echo json_encode($response);                  
    }
  }else{

    $response["message"]="wrong api key.";
    $response["status"]="201";
    echo json_encode($response);
  }
}
