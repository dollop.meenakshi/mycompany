<?php
include_once 'lib.php';


/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST) && !empty($_POST)) {

	if ($key == $keydb && $auth_check=='true') {

		$response = array();
		extract(array_map("test_input", $_POST));

		if ($_POST['getPraiseTypes']=="getPraiseTypes"  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$qry = $d->select("employee_praise_type_master","employee_praise_active_status = '0'");


			if (mysqli_num_rows($qry) > 0) {
			
				$response["praise_types"] = array();			

				while($data = mysqli_fetch_array($qry)){
					$praise = array();

					$praise['employee_praise_type_id'] = $data['employee_praise_type_id'];
					$praise['employee_praise_name'] = $data['employee_praise_name'];
					$praise['employee_praise_content'] = $data['employee_praise_content'];
					$praise['employee_praise_name_color_code'] = $data['employee_praise_name_color_code'];
					$praise['employee_praise_image'] = $base_url.'img/praise_icon/'.$data['employee_praise_image'];

					array_push($response["praise_types"],$praise);
				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data found";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if ($_POST['getMyPraisedList']=="getMyPraisedList" &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$qry = $d->selectRow("employee_praise_type_master.*,employee_praise_master.praised_by_id,employee_praise_master.praised_content,(SELECT count(*) FROM employee_praise_master WHERE employee_praise_master.employee_id = '$user_id' AND employee_praise_master.employee_praise_type_id = employee_praise_type_master.employee_praise_type_id) AS totalPraise","employee_praise_type_master,employee_praise_master","employee_praise_master.employee_id = '$user_id' AND employee_praise_master.employee_praise_type_id = employee_praise_type_master.employee_praise_type_id","GROUP BY employee_praise_master.employee_praise_type_id");
 
			if (mysqli_num_rows($qry) > 0) {
			
				$response["praise_types"] = array();			

				while($data = mysqli_fetch_array($qry)){

					$praise = array();

					$praise['employee_praise_type_id'] = $data['employee_praise_type_id'];
					$praise['employee_praise_name'] = $data['employee_praise_name'];
					$praise['employee_praise_content'] = $data['employee_praise_content'];
					$praise['employee_praise_name_color_code'] = $data['employee_praise_name_color_code'];
					$praise['employee_praise_image'] = $base_url.'img/praise_icon/'.$data['employee_praise_image'];

					$praise['total'] = $data['totalPraise'];

					array_push($response["praise_types"],$praise);
				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data found";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if ($_POST['getPraiseComments']=="getPraiseComments" &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$qry = $d->selectRow("
				employee_praise_master.praised_content,
				users_master.user_id,
                users_master.user_full_name,
                users_master.user_designation,
                users_master.user_profile_pic,
                block_master.block_name,
                floors_master.floor_name
                ","
                employee_praise_master,
                users_master,
                block_master,
                floors_master
                ","employee_praise_master.praised_by_id = users_master.user_id 
                AND employee_praise_master.employee_id = '$user_id' 
                AND employee_praise_master.praised_content != ''
                AND employee_praise_master.employee_praise_type_id = '$employee_praise_type_id'
                AND users_master.block_id = block_master.block_id 
                AND users_master.floor_id = floors_master.floor_id
                ","ORDER BY employee_praise_master.praised_date DESC");
 
			if (mysqli_num_rows($qry) > 0) {
			
				$response["praise_content"] = array();			

				while($data = mysqli_fetch_array($qry)){

					$data = array_map("html_entity_decode", $data);

					$praise = array();

					$praise['praised_content'] = $data['praised_content'];

                    $praise['user_full_name'] = $data['user_full_name'];
                    $praise['branch_name'] = $data['block_name'];
                    $praise['department_name'] = $data['floor_name'];
                    $praise['user_designation'] = $data['user_designation'];
                    
                    if ($data['user_profile_pic'] != '') {
                        $praise["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $praise["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

					array_push($response["praise_content"],$praise);
				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data found";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if($_POST['addPraised']=="addPraised" && $praised_by_id!='' && $employee_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			/*$currentMonth = date('m');

			$hasPraise = $d->count_data_direct("employee_praise_id","employee_praise_master","praised_by_id = '$praised_by_id' AND employee_id = '$employee_id' AND employee_praise_type_id = '$employee_praise_type_id' AND DATE_FORMAT(praised_date,'%m') = '$currentMonth'");

			if ($hasPraise > 0) {
				$response["message"] = "You have already praised ".$praise_to_user_name." as a ".$employee_praise_name." for this month";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}*/

			$m->set_data('society_id',$society_id);
			$m->set_data('employee_praise_type_id',$employee_praise_type_id);
			$m->set_data('employee_id',$employee_id);
			$m->set_data('praised_by_id',$praised_by_id);
			$m->set_data('praised_content',$praised_content);
			$m->set_data('praised_date',date('Y-m-d H:i:s'));

            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'employee_praise_type_id'=>$m->get_data('employee_praise_type_id'),
                'employee_id'=>$m->get_data('employee_id'),
                'praised_by_id'=>$m->get_data('praised_by_id'),
                'praised_content'=>$m->get_data('praised_content'),
                'praised_date'=>$m->get_data('praised_date'),
            );

            $qry = $d->insert("employee_praise_master",$a);

            if ($qry == true) {

	            $title = "Praised as: $employee_praise_name";
                $description = "By $user_name";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$employee_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $icon=array();

                $icon['userProfile'] = $employee_praise_image;

                if ($device == 'android') {
                    $nResident->noti("praised","",$society_id,$user_token,$title,$description,$icon);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("praised","",$society_id,$user_token,$title,$description,$icon);
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"praised","","user_id = '$user_id'",$employee_id);

	            $response["message"] = "Praised sent";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "Something went wrong!";
				$response["status"] = "201";
				echo json_encode($response);
            }
                      
        }else {
			$response["message"] = "wrong tag.";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {
		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}
?>