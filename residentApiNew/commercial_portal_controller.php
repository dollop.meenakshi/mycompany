<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));


    if (isset($addcommercialUser) && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

          $m->set_data("name",$name);
          $m->set_data("unit_id",$unit_id);
          $m->set_data("society_id",$society_id);
          $m->set_data("phone",$phone);
          $m->set_data("password",$password);
          $m->set_data("active_status",0);
          $m->set_data("login_created_date",date('Y-m-d H:i:s'));

        $qq= $d->select("users_entry_login","login_mobile='$phone' AND unit_id='$unit_id'");
        $oldData=mysqli_fetch_array($qq);
        if ($oldData['active_status']==1) {

             $entry = array('login_name' => $m->get_data("name"),
                'unit_id' => $m->get_data("unit_id"),
                'society_id' => $m->get_data("society_id"),
                'login_mobile' => $m->get_data("phone"),
                'login_password' => $m->get_data("password"),
                'login_created_date' => $m->get_data("login_created_date"),
                'active_status' => $m->get_data("active_status"),
              );

            $query=$d->update("users_entry_login",$entry,"login_mobile='$phone' AND unit_id='$unit_id'");
            $response["message"] = "User Activated Successfully ";
            $response["status"] = "200";
            echo json_encode($response);
            exit();
        } else  if ($oldData>0 && $oldData['active_status']==0) {
            $response["message"] = "User Already Register.";
            $response["status"] = "201";
            echo json_encode($response);
            exit();
        }

          $entry = array('login_name' => $m->get_data("name"),
                'unit_id' => $m->get_data("unit_id"),
                'society_id' => $m->get_data("society_id"),
                'login_mobile' => $m->get_data("phone"),
                'login_password' => $m->get_data("password"),
                'login_created_date' => $m->get_data("login_created_date"),
              );

          $query=$d->insert("users_entry_login",$entry);
          if ($query === true) {
             $response["message"] = "Commercial User Added Successfully ";
            $response["status"] = "200";
            echo json_encode($response);
          }else{
            $response["message"] = "Something Wrong";
            $response["status"] = "201";
            echo json_encode($response);
          }

        } else  if (isset($deleteCommercialUser) && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_entry_id, FILTER_VALIDATE_INT) == true) {
     

            $entry = array(
                'active_status' => 1,
              );

          $query=$d->update("users_entry_login",$entry,"user_entry_id='$user_entry_id' AND unit_id='$unit_id'");
          if ($query === true) {
             $response["message"] = "Commercial User Deleted Successfully ";
            $response["status"] = "200";
            echo json_encode($response);
          }else{
            $response["message"] = "Something Wrong";
            $response["status"] = "201";
            echo json_encode($response);
          }

        } else  if($_POST['getCommercialUserList']=="getCommercialUserList" && $unit_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true){

                $q=$d->select("users_entry_login","society_id='$society_id' AND unit_id='$unit_id' AND active_status=0","ORDER BY user_entry_id DESC");

                if(mysqli_num_rows($q)>0){
                        $response["commercial_users"] = array();

                    while($data=mysqli_fetch_array($q)) {
                        
                            $commercial_users = array(); 

                            
                            $commercial_users["user_entry_id"]=$data['user_entry_id'];
                            $commercial_users["society_id"]=$data['society_id'];
                            $commercial_users["phone"]=$data['login_mobile'];
                            $commercial_users["name"]=$data['login_name'];
                            $commercial_users["unit_id"]=$data['unit_id'];
                           
                            array_push($response["commercial_users"], $commercial_users);

                    }
                    $response["message"]="Commercial Users List";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No Commercial User Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }  else {
             $response["message"] = "wrong tagg.";
            $response["status"] = "201";
            echo json_encode($response);
        }

    }else {
        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 