<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {

    $response = array();
    extract(array_map("test_input" , $_POST));
        
            if($_POST['getFacility']=="getFacility" && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ){
                $todayDate=date("Y-m-d");
                if ($user_id!='' && $user_id!=0) {
                     $quc=$d->select("users_master","user_id='$user_id' AND is_defaulter=1");
                     if (mysqli_num_rows($quc)>0) {
                        $response["message"] = "Access Denied ";
                        $response["status"] = "201";
                        echo json_encode($response);
                        exit();
                     }

                }

                $fq=$d->select("users_master","unit_id='$unit_id' AND user_id='$user_id'");
                $ownerData=mysqli_fetch_array($fq);
                $userType=$ownerData['user_type'];


                $mybooking=$d->select("facilities_master,facilitybooking_master","facilitybooking_master.society_id='$society_id' AND facilitybooking_master.society_id!='0' AND facilitybooking_master.unit_id='$unit_id' AND facilitybooking_master.unit_id!='0' AND facilitybooking_master.facility_id=facilities_master.facility_id  AND facilitybooking_master.userType='$userType' AND facilitybooking_master.book_status = '1'","ORDER BY booking_id DESC LIMIT 100");
                if(mysqli_num_rows($mybooking)>0){
                        $response["booking"] = array();

                    while($data_booking_list=mysqli_fetch_array($mybooking)) {

                            $booking = array(); 
                            $mq =$d->select("facility_booking_months","booking_id='$data_booking_list[booking_id]'");
                            $monthData=mysqli_fetch_array($mq);

                            $booking["booking_id"]=$data_booking_list['booking_id'];
                            $booking["facility_id"]=$data_booking_list['facility_id'];
                            $booking["balancesheet_id"]=$data_booking_list['balancesheet_id'];
                            $booking["society_id"]=$data_booking_list['society_id'];
                            $booking["booked_date"]=date('d M Y',strtotime($data_booking_list['booked_date']));
                            $booking["booking_expire_date"]=date('d M Y',strtotime($data_booking_list['booking_expire_date']));
                            if ($monthData['booking_start_time']!='' && $monthData['booking_start_time']!='00:00:00') {
                                $booking["booking_start_time"]=date('h:i A',strtotime($monthData['booking_start_time']));
                                $booking["booking_end_time"]=date('h:i A',strtotime($monthData['booking_end_time']));
                            } else {
                                $booking["booking_start_time"]='';
                                $booking["booking_end_time"]='';
                            }
                            $booking["unit_id"]=$data_booking_list['unit_id'];
                            $booking["unit_name"]=$data_booking_list['unit_name'];
                            $booking["book_status"]=$data_booking_list['book_status'];
                            $booking["book_request_date"]=$data_booking_list['book_request_date'];
                            $booking["payment_type"]=$data_booking_list['payment_type'];
                            $booking["payment_status"]=$data_booking_list['payment_status'];
                            $booking["receive_amount"]=number_format($data_booking_list['receive_amount']-$data_booking_list['transaction_charges'],2); 
                            $booking["payment_received_date"]=date('d M Y, h:i A',strtotime($data_booking_list['payment_received_date']));
                            $booking["facility_name"]=html_entity_decode($data_booking_list['facility_name']);
                            $booking["facility_photo"]=$base_url."/img/facility/".$data_booking_list['facility_photo'];
                            $booking["facility_amount"]=$data_booking_list['facility_amount'];

                            $booking["no_of_person"]=$data_booking_list['no_of_person'];
                            $booking["no_of_month"]=$data_booking_list['no_of_month'];
                            $booking["invoice_url"]=$base_url."apAdmin/invoice.php?user_id=$data_booking_list[user_id]&unit_id=$data_booking_list[unit_id]&type=Fac&societyid=$data_booking_list[society_id]&id=$data_booking_list[booking_id]&facility_id=$data_booking_list[facility_id]&language_id=$language_id";


                            array_push($response["booking"], $booking);


                    }
                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>