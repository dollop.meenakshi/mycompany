<?php
include_once 'lib.php';
if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb && $auth_check=='true') {

        $response = array();
        extract(array_map("test_input", $_POST));

        if ($_POST['getEmpAttendNew'] == "getEmpAttendNew" && filter_var($emp_id, FILTER_VALIDATE_INT) == true) {
            $statrDate =  date("Y-m-1", strtotime("-3 month"));
            $startMonth =  date("Y-m", strtotime("-3 month"));
            $today = date("Y-m-d");
            $todayOne = date("Y-m-d", strtotime("1 days"));

            $response["empAttend"] = array();
            $begin = new DateTime($statrDate);
            $end = new DateTime($todayOne);

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            foreach ($period as $dt) {
                $attDate =$dt->format("Y-m-d");
                $empAttend["emp_id"] = $emp_id;
                $empAttend["society_id"] = $society_id;
                $empAttend["att_date"] =$attDate;
                $qt = $d->select("staff_visit_master","emp_id='$emp_id' AND filter_data='$attDate'","");
                if (mysqli_num_rows($qt)>0) {
                    $isPresent = true;
                } else {
                    $isPresent = false;
                }
                $empAttend["isPresent"] =$isPresent;
                $empAttend["in_out_time"] = array();
                while ($subData=mysqli_fetch_array($qt)) {
                    $in_out_time = array();
                    $in_out_time["visit_date"]= date("d-m-Y", strtotime($subData["filter_data"]));
                    $in_out_time["int_time"]= date("h:i A", strtotime($subData["visit_entry_date_time"]));
                    if ($subData["visit_exit_date_time"]!='' && $subData["visit_exit_date_time"]!='0000-00-00 00:00:00' && date("Y-m-d", strtotime($subData['visit_exit_date_time'])) != $attDate) {

                        $in_out_time["out_time"]=  date("h:i A", strtotime($subData["visit_exit_date_time"]))." (". date("d-M", strtotime($subData['visit_exit_date_time'])).')';
                    } else if($subData["visit_exit_date_time"]!='' && $subData["visit_exit_date_time"]!='0000-00-00 00:00:00' &&  date("Y-m-d", strtotime($subData['visit_exit_date_time'])) == $attDate) {
                        $in_out_time["out_time"]=  date("h:i A", strtotime($subData["visit_exit_date_time"])).' '.$addNExtData;
                    } else {
                        $in_out_time["out_time"]= "-";
                    }
                    array_push($empAttend["in_out_time"], $in_out_time); 
                }

                array_push($response["empAttend"], $empAttend);
            }

            
            $month = strtotime($statrDate);
            $response["summary"] = array();
            $curnetMonthName = date("F-Y");
            for($i=1; $i<=8 ; $i++){ 
                $summary = array();
                $month_name = date('F-Y', $month);
                if ($month_name==$curnetMonthName) {
                    $totalDaysMonth =   date('d');
                } else {
                    $totalDaysMonth =   date('t', strtotime($month_name));
                }
                $firstDate = date('Y-m-01', strtotime($month_name));
                $lastDate = date('Y-m-t', strtotime($month_name));
                $vCount = $d->select("staff_visit_master","emp_id='$emp_id' AND filter_data BETWEEN '$firstDate' AND '$lastDate' ","GROUP BY filter_data ORDER BY staff_visit_id DESC ");
                $totalWorkingdays=  mysqli_num_rows($vCount).'';

                $summary["month_name"]=$month_name; 
                $summary["total_working_days"]=$totalWorkingdays; 
                $summary["absunt_days"]=$totalDaysMonth-$totalWorkingdays.''; 
                $summary["totalDaysMonth"]=$totalDaysMonth; 

                array_push($response["summary"], $summary); 

                $month = strtotime('+1 month', $month);
                  $i++; 
                // It will print: January,February,.............December,
            }

          

            $response["message"] = "$datafoundMsg";
            $response["status"] = "200";
            echo json_encode($response);


        } else if ($_POST['getEmpAttendNewDaily'] == "getEmpAttendNewDaily" && filter_var($visitor_id, FILTER_VALIDATE_INT) == true) {
            $statrDate =  date("Y-m-1", strtotime("-3 month"));
            $startMonth =  date("Y-m", strtotime("-3 month"));
            $today = date("Y-m-d");
            $todayOne = date("Y-m-d", strtotime("1 days"));

            $response["empAttend"] = array();
            $begin = new DateTime($statrDate);
            $end = new DateTime($todayOne);

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            foreach ($period as $dt) {
                $attDate =$dt->format("Y-m-d");
                $empAttend["visitor_id"] = $visitor_id;
                $empAttend["society_id"] = $society_id;
                $empAttend["att_date"] =$attDate;
                $qt = $d->select("visitors_master","daily_visitor_id='$visitor_id' AND visit_date='$attDate'","ORDER BY visitor_id ASC");
                if (mysqli_num_rows($qt)>0) {
                    $isPresent = true;
                } else {
                    $isPresent = false;
                }
                $empAttend["isPresent"] =$isPresent;
                $empAttend["in_out_time"] = array();
                while ($subData=mysqli_fetch_array($qt)) {
                    $in_out_time = array();
                    $in_out_time["visit_date"]= date("d-m-Y", strtotime($subData["visit_date"]));
                    $in_out_time["int_time"]= date("h:i A", strtotime($subData["visit_time"]));
                    if ($subData["exit_time"]!='' && date("Y-m-d", strtotime($subData['exit_date'])) != $attDate) {

                        $in_out_time["out_time"]=  date("h:i A", strtotime($subData["exit_time"]))." (". date("d-M", strtotime($subData['exit_date'])).')';
                    } else if($subData["exit_time"]!='' && date("Y-m-d", strtotime($subData['exit_date'])) == $attDate) {
                        $in_out_time["out_time"]=  date("h:i A", strtotime($subData["exit_time"])).' '.$addNExtData;
                    } else {
                        $in_out_time["out_time"]= "-";
                    }
                    array_push($empAttend["in_out_time"], $in_out_time); 
                }

                array_push($response["empAttend"], $empAttend);
            }

            
            $month = strtotime($statrDate);
            $response["summary"] = array();
            $curnetMonthName = date("F-Y");
            for($i=1; $i<=8 ; $i++){ 
                $summary = array();
                $month_name = date('F-Y', $month);
                $month_name = date('F-Y', $month);
                if ($month_name==$curnetMonthName) {
                    $totalDaysMonth =   date('d');
                } else {
                    $totalDaysMonth =   date('t', strtotime($month_name));
                }
                $firstDate = date('Y-m-01', strtotime($month_name));
                $lastDate = date('Y-m-t', strtotime($month_name));
                $vCount = $d->select("visitors_master","daily_visitor_id='$visitor_id' AND visit_date BETWEEN '$firstDate' AND '$lastDate' ","GROUP BY visit_date ORDER BY visitor_id DESC ");
                $totalWorkingdays=  mysqli_num_rows($vCount).'';

                $summary["month_name"]=$month_name; 
                $summary["total_working_days"]=$totalWorkingdays; 
                $summary["absunt_days"]=$totalDaysMonth-$totalWorkingdays.''; 
                $summary["totalDaysMonth"]=$totalDaysMonth; 

                array_push($response["summary"], $summary); 

                $month = strtotime('+1 month', $month);
                  $i++; 
                // It will print: January,February,.............December,
            }

          

            $response["message"] = "$datafoundMsg";
            $response["status"] = "200";
            echo json_encode($response);


        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
