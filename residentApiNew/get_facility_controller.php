<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

$today=date("Y-m-d");
$cTime=date("H:i:s");
    if ($key==$keydb && $auth_check=='true') { 

	$response = array();
	extract(array_map("test_input" , $_POST));
    if (isset($society_id)) {
        $sq=$d->selectRow("currency","society_master","society_id='$society_id'");
        $societyData = mysqli_fetch_array($sq);
        $currency = $societyData['currency'];
    } else {
        $currency = '';
    }
    
        if($_POST['getFacility']=="getFacility" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            if ($user_id!='' && $user_id!=0) {
                     $quc=$d->select("users_master","user_id='$user_id' AND is_defaulter=1");
                     if (mysqli_num_rows($quc)>0) {
                        $response["message"] = "Access Denied ";
                        $response["status"] = "201";
                        echo json_encode($response);
                        exit();
                     }

                }

            $qfacility_data=$d->select("facilities_master","society_id ='$society_id' AND society_id!=0");

            $response["facility"] = array();

        if(mysqli_num_rows($qfacility_data)>0){

            while($data_facility_list=mysqli_fetch_array($qfacility_data)) {
                
                $facility = array();  
                $fName = ucfirst($data_facility_list['facility_name']);

                $facility["facility_id"]=$data_facility_list['facility_id'];
	      	    $facility["society_id"]=$data_facility_list['society_id'];
                $facility["facility_name"]=html_entity_decode($fName);
                if($data_facility_list['facility_description']!='') {
                  $facility["facility_description"]=html_entity_decode($data_facility_list['facility_description']);
                } else {
    	      	    $facility["facility_description"]="$not_available";
                }
                $facility["facility_photo"]=$base_url."/img/facility/".$data_facility_list['facility_photo'];
                $facility["facility_type"]=$data_facility_list['facility_type'];
                $facility["facility_status"]=$data_facility_list['facility_status'];

                array_push($response["facility"], $facility); 
            }

            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{

            $response["message"]="$noDatafoundMsg";
            $response["status"]="201";
            echo json_encode($response);
    
        }

    }else if($_POST['getFullFacilityDetailsNew']=="getFullFacilityDetailsNew" && filter_var($facility_id, FILTER_VALIDATE_INT) == true ){


        $qfacility_data=$d->select("facilities_master","facility_id ='$facility_id'");

        if(mysqli_num_rows($qfacility_data)>0){

                $data_facility_list=mysqli_fetch_array($qfacility_data);
                if ($data_facility_list['amount_type']==0) {
                  $personText = " / Per Person";
                }

                $q3=$d->select("balancesheet_master,society_payment_getway","society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data_facility_list[balancesheet_id]' AND balancesheet_master.society_payment_getway_id!=0 OR society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data_facility_list[balancesheet_id]' AND balancesheet_master.society_payment_getway_id_upi!=0");


                if ($user_type!='' && $user_type==0) {
                  $facAmount = $data_facility_list['facility_amount'];
                  
                } else if ($user_type!='' && $user_type==1 && $data_facility_list['facility_amount_tenant']!=0) {
                  $facAmount = $data_facility_list['facility_amount_tenant'];
                  
                } else {
                  $facAmount = $data_facility_list['facility_amount'];
                }


                $todayDate=date("Y-m-d");

                $count5=$d->sum_data("no_of_person","facilitybooking_master","facility_id='$data_facility_list[facility_id]' AND booking_expire_date>='$todayDate' AND book_status = '1'");
                $row11=mysqli_fetch_array($count5);
                $person_count=$row11['SUM(no_of_person)'];

                if (is_null($person_count)) {
        
                    $person_count="0";                    
                }

                 if ($data_facility_list['facility_type']==2) {
                    $type= "Free";
                  } else  if ($data_facility_list['facility_type']==0)  {
                    $type=  "/ Per Day".$personText;
                  } else  if ($data_facility_list['facility_type']==1)  {
                    $type=  "/ Per Person Monthly";
                  } else  if ($data_facility_list['facility_type']==3)  {
                    $type=  "/ Per Hour".$personText;
                  } else if ($data_facility_list['facility_type']==4)  {
                    $type=  "/ Per Time Slot".$personText;
                  }
            
                $response["facility_id"]=$data_facility_list['facility_id'];
                $response["facility_active_status"]=$data_facility_list['facility_active_status'];
                $response["balancesheet_id"]=$data_facility_list['balancesheet_id'];
                $response["society_id"]=$data_facility_list['society_id'];
                $response["facility_name"]=html_entity_decode($data_facility_list['facility_name']);
                $response["facility_photo"]=$base_url."img/facility/".$data_facility_list['facility_photo'];

                $photoAryy= array();

                if ($data_facility_list['facility_photo']!="") {
                     array_push($photoAryy, $data_facility_list['facility_photo']);
                }

                if ($data_facility_list['facility_photo_2']!='') {
                   array_push($photoAryy, $data_facility_list['facility_photo_2']);
                }

                if ($data_facility_list['facility_photo_3']!='') {
                  array_push($photoAryy, $data_facility_list['facility_photo_3']);
                }

                $response["facility_img"]=array();

                 for ($iFeed=0; $iFeed < count($photoAryy) ; $iFeed++) { 
                      $facility_img=array();
                      $facility_img["facility_photo"]=$base_url."img/facility/".$photoAryy[$iFeed];
                      array_push($response["facility_img"], $facility_img); 

                  }

                
                $response["facility_type"]=$data_facility_list['facility_type'];
                $response["person_limit"]=$data_facility_list['person_limit'];
                $response["bill_type"]=$data_facility_list['bill_type'];
                $response["gst"]=$data_facility_list['gst'];
                $response["gst_type"]=$data_facility_list['gst_type'];
                $response["gst_slab"]=$data_facility_list['tax_slab'];
                 if ($data_facility_list['facility_type']==2) {
                  $response["facility_amount_view"]=$type;
                } else {
                   if ($data_facility_list['is_taxble']=="1") {
                    
                      $facAmount11 = number_format($facAmount,2,'.','');
                      $response["facility_amount_view"]="$currency ". number_format($facAmount11,2) .' '.$type.' (included gst/tax)';
                      $response["facility_amount_included_tax"]= $facAmount.'';

                      $gstAmount = $facAmount - ( $facAmount  * (100/(100+$data_facility_list['tax_slab'])));

                      $response["gst_amount"]=number_format($gstAmount,2,'.','');
                  } else {
                     $facAmount11 = number_format($facAmount,2,'.','');
                    $response["facility_amount_view"]="$currency ". number_format($facAmount11,2) .' '.$type;
                    $response["facility_amount_included_tax"]= $facAmount.'';
                    $response["gst_amount"]="";
                  }
                }

                $gstValue =   $facAmount - ( $facAmount  * (100/(100+$data_facility_list['tax_slab']))); 
               
                    $response["facility_amount"]= $facAmount.'';

                
               

               $response["schedule"] = array();
               $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
                for ($i=0; $i < 7; $i++) { 
                $qt=$d->select("facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$i'","");
                    $timData=mysqli_fetch_array($qt);
                    $schedule = array();
                    $schedule["day_name"]=$dayName[$i];
                    if ($timData['facility_start_time']=="00:00:00" || $timData['facility_status']==1) {
                    $schedule["opening_time"]="Close";
                        $schedule["is_open"]=false;
                    }else {
                    $schedule["opening_time"]=date('h:i A',strtotime($timData['facility_start_time'])) .' to '.date('h:i A',strtotime($timData['facility_end_time']));
                        $schedule["is_open"]=true;
                    }

                    array_push($response["schedule"], $schedule); 
                }

               $response["schedule_new"] = array();
               $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
                for ($i=0; $i < 7; $i++) { 
                    $schedule_new = array();
                    $schedule_new["day_name"]=$dayName[$i];
                    $qt=$d->select("facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$i'","");
                    $schedule_new["time_slot"]=array();
                    while($timData=mysqli_fetch_array($qt)){
                      $time_slot = array();
                      if ($timData['facility_start_time']=="00:00:00" || $timData['facility_status']==1) {
                        $time_slot["opening_time"]="Close";
                        $time_slot["is_open"]=false;
                      }else {
                      $time_slot["opening_time"]=date('h:i A',strtotime($timData['facility_start_time'])) .' to '.date('h:i A',strtotime($timData['facility_end_time']));
                          $time_slot["is_open"]=true;
                      }
                       array_push($schedule_new["time_slot"], $time_slot); 
                    }
                    array_push($response["schedule_new"], $schedule_new); 
                }
              
              
                
              if (mysqli_num_rows($q3)>0) {
                  $response["isPay"]=true;   
              } else {
                  $response["isPay"]=false;   
              }

            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{

            $response["message"]="$noDatafoundMsg";
            $response["status"]="201";
            echo json_encode($response);
    
        }

    }else if($_POST['getFullFacilityDetailsNewTax']=="getFullFacilityDetailsNewTax" && filter_var($facility_id, FILTER_VALIDATE_INT) == true ){


        $qfacility_data=$d->select("facilities_master","facility_id ='$facility_id'");

        if(mysqli_num_rows($qfacility_data)>0){

                $data_facility_list=mysqli_fetch_array($qfacility_data);
                if ($data_facility_list['amount_type']==0) {
                  $personText = " / Per Person";
                }

                $q3=$d->select("balancesheet_master,society_payment_getway","society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data_facility_list[balancesheet_id]' AND balancesheet_master.society_payment_getway_id!=0 OR society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data_facility_list[balancesheet_id]' AND balancesheet_master.society_payment_getway_id_upi!=0");


                if ($user_type!='' && $user_type==0) {
                  $facAmount = $data_facility_list['facility_amount'];
                  
                } else if ($user_type!='' && $user_type==1 && $data_facility_list['facility_amount_tenant']!=0) {
                  $facAmount = $data_facility_list['facility_amount_tenant'];
                  
                } else {
                  $facAmount = $data_facility_list['facility_amount'];
                }


                $todayDate=date("Y-m-d");

                $count5=$d->sum_data("no_of_person","facilitybooking_master","facility_id='$data_facility_list[facility_id]' AND booking_expire_date>='$todayDate' AND book_status = '1'");
                $row11=mysqli_fetch_array($count5);
                $person_count=$row11['SUM(no_of_person)'];

                if (is_null($person_count)) {
        
                    $person_count="0";                    
                }

                 if ($data_facility_list['facility_type']==2) {
                    $type= "Free";
                  } else  if ($data_facility_list['facility_type']==0)  {
                    $type=  "/ Per Day".$personText;
                  } else  if ($data_facility_list['facility_type']==1)  {
                    $type=  "/ Per Person Monthly";
                  } else  if ($data_facility_list['facility_type']==3)  {
                    $type=  "/ Per Hour".$personText;
                  } else if ($data_facility_list['facility_type']==4)  {
                    $type=  "/ Per Time Slot".$personText;
                  }
            
                $response["facility_id"]=$data_facility_list['facility_id'];
                $response["facility_active_status"]=$data_facility_list['facility_active_status'];
                $response["balancesheet_id"]=$data_facility_list['balancesheet_id'];
                $response["society_id"]=$data_facility_list['society_id'];
                $response["facility_name"]=html_entity_decode($data_facility_list['facility_name']);
                $response["facility_address"]=html_entity_decode($data_facility_list['facility_address']);
                $response["facility_photo"]=$base_url."img/facility/".$data_facility_list['facility_photo'];

                $photoAryy= array();

                if ($data_facility_list['facility_photo']!="") {
                     array_push($photoAryy, $data_facility_list['facility_photo']);
                }

                if ($data_facility_list['facility_photo_2']!='') {
                   array_push($photoAryy, $data_facility_list['facility_photo_2']);
                }

                if ($data_facility_list['facility_photo_3']!='') {
                  array_push($photoAryy, $data_facility_list['facility_photo_3']);
                }

                $response["facility_img"]=array();

                 for ($iFeed=0; $iFeed < count($photoAryy) ; $iFeed++) { 
                      $facility_img=array();
                      $facility_img["facility_photo"]=$base_url."img/facility/".$photoAryy[$iFeed];
                      array_push($response["facility_img"], $facility_img); 

                  }

                
                $response["facility_type"]=$data_facility_list['facility_type'];
                $response["person_limit"]=$data_facility_list['person_limit'];
                $response["is_taxble"]=$data_facility_list['is_taxble'];
                $response["gst"]=$data_facility_list['gst'];
                $response["taxble_type"]=$data_facility_list['taxble_type'];
                $response["tax_slab"]=$data_facility_list['tax_slab'];
                 if ($data_facility_list['facility_type']==2) {
                  $response["facility_amount_view"]=$type;
                } else {
                   if ($data_facility_list['is_taxble']=="1") {
                    
                      $facAmount11 = number_format($facAmount,2,'.','');
                      $response["facility_amount_view"]="$currency ". number_format($facAmount11,2) .' '.$type.' ('.$xml->string->included.' '.$xml->string->tax.')';
                      $response["facility_amount_included_tax"]= $facAmount.'';

                      $gstAmount = $facAmount - ( $facAmount  * (100/(100+$data_facility_list['tax_slab'])));

                      $response["gst_amount"]=number_format($gstAmount,2,'.','');
                  } else {
                     $facAmount11 = number_format($facAmount,2,'.','');
                    $response["facility_amount_view"]="$currency ". number_format($facAmount11,2) .' '.$type;
                    $response["facility_amount_included_tax"]= $facAmount.'';
                    $response["gst_amount"]="";
                  }
                }

                $gstValue =   $facAmount - ( $facAmount  * (100/(100+$data_facility_list['tax_slab']))); 
               
                    $response["facility_amount"]= $facAmount.'';

                
               

               $response["schedule"] = array();
               $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
                for ($i=0; $i < 7; $i++) { 
                $qt=$d->select("facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$i'","");
                    $timData=mysqli_fetch_array($qt);
                    $schedule = array();
                    $schedule["day_name"]=$dayName[$i];
                    if ($timData['facility_start_time']=="00:00:00" || $timData['facility_status']==1) {
                    $schedule["opening_time"]="Close";
                        $schedule["is_open"]=false;
                    }else {
                    $schedule["opening_time"]=date('h:i A',strtotime($timData['facility_start_time'])) .' to '.date('h:i A',strtotime($timData['facility_end_time']));
                        $schedule["is_open"]=true;
                    }

                    array_push($response["schedule"], $schedule); 
                }

               $response["schedule_new"] = array();
               $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
                for ($i=0; $i < 7; $i++) { 
                    $schedule_new = array();
                    $schedule_new["day_name"]=$dayName[$i];
                    $qt=$d->select("facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$i'","");
                    $schedule_new["time_slot"]=array();
                    while($timData=mysqli_fetch_array($qt)){
                      $time_slot = array();
                      if ($timData['facility_start_time']=="00:00:00" || $timData['facility_status']==1) {
                        $time_slot["opening_time"]="Close";
                        $time_slot["is_open"]=false;
                      }else {
                      $time_slot["opening_time"]=date('h:i A',strtotime($timData['facility_start_time'])) .' to '.date('h:i A',strtotime($timData['facility_end_time']));
                          $time_slot["is_open"]=true;
                      }
                       array_push($schedule_new["time_slot"], $time_slot); 
                    }
                    array_push($response["schedule_new"], $schedule_new); 
                }
              
              
                
              if (mysqli_num_rows($q3)>0) {
                  $response["isPay"]=true;   
              } else {
                  $response["isPay"]=false;   
              }

            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{

            $response["message"]="$noDatafoundMsg";
            $response["status"]="201";
            echo json_encode($response);
    
        }

    }else if($_POST['checkFacilityDate']=="checkFacilityDate" && filter_var($facility_id, FILTER_VALIDATE_INT) == true  ){


        $book_date= date('Y-m-d',strtotime($book_date));
        $q=$d->select("facilities_master"," facility_id='$facility_id'","");
        $row=mysqli_fetch_array($q);
        extract($row);
         if ($facility_type==2) {
            $response["message"]="You Can't Book Free Facility !";
            $response["status"]="201";
            echo json_encode($response);
            exit();
          }
         $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
         $weekday = date('l', strtotime($book_date)); // note: first arg to date() is lower-case L 
         $key = array_search($weekday, $dayName); // $key = 2;
         $qt=$d->select("facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$key'","");
         $timData=mysqli_fetch_array($qt);
         $startTimeHourOnly= date('H',strtotime($timData['facility_start_time']));
         $startTimeMinuteOnly= date('i',strtotime($timData['facility_start_time']));
         $endTimeHourOnly= date('H',strtotime($timData['facility_end_time']));
         $endTimeMinOnly= date('i',strtotime($timData['facility_end_time']));
         if ($timData['facility_status']=="1"  && $facility_type!=1) {

            $faciliyt_close_msg = $xml->string->faciliyt_close_msg;
             $faciliyt_close_msg = str_replace('{weekday}', $weekday, $faciliyt_close_msg);
            $response["message"]="$faciliyt_close_msg";
            $response["status"]="201";
            echo json_encode($response);
            exit();

        } else {
             $bookedAray = array();
             $qcheck=$d->select("facilitybooking_master","facility_id='$facility_id' AND booked_date='$book_date' and book_status!=3");
                 while($bookData=mysqli_fetch_array($qcheck)){
                     $otherBookStartTime= date('H:i',strtotime($bookData['booking_start_time']));
                     $otherBookEndTime= date('H:i',strtotime($bookData['booking_end_time']));
                     $startTimeHourOnlyBook= date('H',strtotime($bookData['booking_start_time']));
                     $startTimeMinuteOnlyBook= date('i',strtotime($bookData['booking_start_time']));
                     $endTimeHourOnlyBook= date('H',strtotime($bookData['booking_end_time']));
                     $endTimeMinOnlyBook= date('i',strtotime($bookData['booking_end_time']));

                      $hourFormat = 'H:i';
                      $minutesInterval = new DateInterval('PT15M');
                      $tNow = new DateTime();
                      $tStart = new DateTime();
                      $tStart->setTime($startTimeHourOnlyBook, $startTimeMinuteOnlyBook);
                      $tEnd = new DateTime();
                      $tEnd->setTime($endTimeHourOnlyBook, $endTimeMinOnlyBook);
                      $tCount = clone $tStart;
                      while($tCount <= $tEnd){
                        $attribute = "";
                        $dateFormatted = $tCount->format($hourFormat);
                        array_push($bookedAray, $dateFormatted);
                        $tCount->add($minutesInterval);
                      }
                 }
        // print_r($bookedAray);
            $response["start_time"] = array();

              $hourFormat = 'H:i';
              $minutesInterval = new DateInterval('PT15M');
              $tNow = new DateTime();
              $tStart = new DateTime();
              $tStart->setTime($startTimeHourOnly, $startTimeMinuteOnly);
              $tEnd = new DateTime();
              $tEnd->setTime($endTimeHourOnly, $endTimeMinOnly);
              $tCount = clone $tStart;
              while($tCount <= $tEnd){
                $start_time = array();
                $attribute = "";
                $dateFormatted = $tCount->format($hourFormat);
                $start_time["time_slot"]=date("h:i A", strtotime($dateFormatted));
                if(in_array($dateFormatted, $bookedAray) && $facility_type!=1 || $cTime>$dateFormatted && $today==$book_date &&  $facility_type!=1 ) { 
                    $start_time["is_booked"]=true;
                }else {
                    $start_time["is_booked"]=false;
                    array_push($response["start_time"], $start_time); 
                }

                $tCount->add($minutesInterval);
              }

            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);

        }

    }else if($_POST['checkFacilityTime']=="checkFacilityTime" && filter_var($facility_id, FILTER_VALIDATE_INT) == true  ){
        $book_date= date('Y-m-d',strtotime($book_date));
        $booking_start_time_days= date("H:i", strtotime($booking_start_time_days));
     
        $q=$d->select("facilities_master"," facility_id='$facility_id'","");
        $row=mysqli_fetch_array($q);
        extract($row);
         $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
         $weekday = date('l', strtotime($book_date)); // note: first arg to date() is lower-case L 
         $key = array_search($weekday, $dayName); // $key = 2;
         $qt=$d->select("facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$key' ","");
         $timData=mysqli_fetch_array($qt);
         $startTimeHourOnly= date('H',strtotime($booking_start_time_days. "+15 minutes"));
         $startTimeMinuteOnly= date('i',strtotime($booking_start_time_days. "+15 minutes"));
         
         $bookedAray = array();
         $qcheck=$d->select("facilitybooking_master","facility_id='$facility_id' AND booked_date='$book_date' and book_status!=3");
         while($bookData=mysqli_fetch_array($qcheck)){
         $otherBookStartTime= date('H:i',strtotime($bookData['booking_start_time']));
         $otherBookEndTime= date('H:i',strtotime($bookData['booking_end_time']));
          $startTimeHourOnlyBook= date('H',strtotime($bookData['booking_start_time']));
         $startTimeMinuteOnlyBook= date('i',strtotime($bookData['booking_start_time']));
         $endTimeHourOnlyBook= date('H',strtotime($bookData['booking_end_time']));
         $endTimeMinOnlyBook= date('i',strtotime($bookData['booking_end_time']));
                  $hourFormat = 'H:i';
                  $minutesInterval = new DateInterval('PT15M');
                  $tNow = new DateTime();
                  $tStart = new DateTime();
                  $tStart->setTime($startTimeHourOnlyBook, $startTimeMinuteOnlyBook);
                  $tEnd = new DateTime();
                  $tEnd->setTime($endTimeHourOnlyBook, $endTimeMinOnlyBook);
                  $tCount = clone $tStart;
                  while($tCount <= $tEnd){
                    $attribute = "";
                    $dateFormatted = $tCount->format($hourFormat);
                    array_push($bookedAray, $dateFormatted);
                    $tCount->add($minutesInterval);
                  }
         }
         $nextBookAray =array();
        for ($i=0; $i <count($bookedAray) ; $i++) { 
            if ($booking_start_time_days<$bookedAray[$i]) {
                     $NextBookValue=  $bookedAray[$i];
                array_push($nextBookAray, $NextBookValue);
                # code...
            }
        };
        if ($nextBookAray[0]!='' && $facility_type!=1) {
            $nextBookTimeMax=date("H:i", strtotime($nextBookAray[0] . "-15 minutes"));
            $endTimeHourOnly= date('H',strtotime($nextBookTimeMax));
            $endTimeMinOnly= date('i',strtotime($nextBookTimeMax));
            
        }else {
            $endTimeHourOnly= date('H',strtotime($timData['facility_end_time']));
            $endTimeMinOnly= date('i',strtotime($timData['facility_end_time']));

        }

        if ($nextBookTimeMax==$booking_start_time_days && $facility_type!=1) {
          $no_time_slot_available_for_this_date = $xml->string->no_time_slot_available_for_this_date;

            $response["message"]="$no_time_slot_available_for_this_date";
            $response["status"]="201";
            echo json_encode($response);
            exit();
        } else {
            $response["end_time"] = array();
           $hourFormat = 'H:i';
           $minutesInterval = new DateInterval('PT15M');
           $tNow = new DateTime();
           $tStart = new DateTime();
           $tStart->setTime($startTimeHourOnly, $startTimeMinuteOnly);
           $tEnd = new DateTime();
           $tEnd->setTime($endTimeHourOnly, $endTimeMinOnly);
           $tCount = clone $tStart;
           while($tCount <= $tEnd){
            $end_time = array();
            $attribute = "";
            $dateFormatted = $tCount->format($hourFormat);
            
            $end_time["is_booked"]=false;
            $end_time["time_slot"]=date("h:i A", strtotime($dateFormatted));
            array_push($response["end_time"], $end_time); 

              $tCount->add($minutesInterval);
           }

            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);


        }



    }else if($_POST['checkAvailiity']=="checkAvailiity" && filter_var($facility_id, FILTER_VALIDATE_INT) == true  && filter_var($no_of_person, FILTER_VALIDATE_INT) == true   ){

    
      $book_date= date("Y-m-d", strtotime($book_date));
      $q=$d->select("facilities_master"," facility_id='$facility_id'","");
      $row=mysqli_fetch_array($q);
      extract($row);
      if ($facility_type==2) {
        $response["message"]="You Can't Book Free Facility !";
        $response["status"]="201";
        echo json_encode($response);
        exit();
      }
       $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
       $weekday = date('l', strtotime($book_date)); // note: first arg to date() is lower-case L 
       $key = array_search($weekday, $dayName); // $key = 2;
      
      if($user_type==1 && $facility_amount_tenant>0) {
        $facility_amount = $facility_amount_tenant;
      }

      if($amount_type==0) {
         $facility_amount = ($facility_amount * $no_of_person);
      } 
       

      $response["time_slot"] = array();
      if($facility_type==1) { 
        $month = strtotime($book_date);
        $start_month = date('F-Y', $month);
        $temp_booked_end_date = $no_of_months-1; 
        $end_date = date("Y-m-t", strtotime("+$temp_booked_end_date month", $month));
        $end = strtotime($end_date);

       // exit(); 
        for ($i=0; $i <$no_of_months ; $i++) { 
            $month_name = date('F-Y', $month);
            $fullName= $month_name.'-'.$year_name;
            $time_slot=array();
            $monthName =  date('F-Y', $month);  $month = strtotime("+1 month", $month);
            
            $count5=$d->sum_data("no_of_person","facility_booking_months","(cancel_status=0 AND facility_id='$facility_id' AND month_name='$monthName')");
            $row11=mysqli_fetch_array($count5);
            $booke_count=$row11['SUM(no_of_person)'];
            if ($booke_count=='') {
                $booke_count =0;
              }
              $avSeat = $person_limit - $booke_count;

            $time_slot["facility_schedule_id"]=$monthName;
            $time_slot["difference_hours"]='0';
            $time_slot["available_count"]=$avSeat.'';
            if ($avSeat<$no_of_person && $amount_type==0) { 
              $time_slot["availability"]=false;
            } else if ($booke_count>0 && $amount_type==1) { 
              $time_slot["availability"]=false;
            }  else {
              $time_slot["availability"]=true;
            }
            if($facility_type==3){
              $time_slot["book_price"]=number_format($facility_amount*$difference_hours,2,'.','');  
            } else {
              $time_slot["book_price"]=number_format($facility_amount,2,'.','');  
            }
            $time_slot["facility_start_time"]=$month_name;

            array_push($response["time_slot"], $time_slot); 
          }
           $response["message"]="$datafoundMsg";
           $response["status"]="200";
           echo json_encode($response);
        

      } else {

        $sq = $d->select("facility_schedule_master","facility_start_time!='00:00:00' AND facility_id='$facility_id' AND facility_status=0  AND facility_day_id='$key'","");

        if(mysqli_num_rows($sq)>0) {
          while ($sData= mysqli_fetch_array($sq)) {
            $time_slot=array();

            $time1 = strtotime($sData["facility_start_time"]);
            $time2 = strtotime($sData["facility_end_time"]);
            $difference_hours = abs($time2 - $time1) / 3600;
            if($difference_hours <1) {
              $difference_hours =1;
            }
            
            $count5=$d->sum_data("no_of_person","facility_booking_months","cancel_status=0 AND facility_id='$facility_id'  AND facility_schedule_id='$sData[facility_schedule_id]'  AND booking_start='$book_date'");
            $row11=mysqli_fetch_array($count5);
            $booke_count=$row11['SUM(no_of_person)'];
            if ($booke_count=='') {
              $booke_count =0;
            }
            $avSeat = $person_limit - $booke_count;

            $time_slot["facility_schedule_id"]=$sData["facility_schedule_id"];
            $time_slot["difference_hours"]=$difference_hours.'';


            if ($avSeat<$no_of_person && $amount_type==0) { 
              $time_slot["available_count"]=$avSeat.'';
              $time_slot["availability"]=false;
            } else if ($booke_count>0 && $amount_type==1) { 
              $time_slot["available_count"]='0';
              $time_slot["availability"]=false;
            }  else {
              $time_slot["available_count"]=$avSeat.'';
              $time_slot["availability"]=true;
            }
            if($facility_type==3){
              $time_slot["book_price"]=number_format($facility_amount*$difference_hours,2,'.','');  
            } else {
              $time_slot["book_price"]=number_format($facility_amount,2,'.','');  
            }
            $time_slot["facility_start_time"]=date('h:i A',strtotime($sData["facility_start_time"])).' to '.date('h:i A',strtotime($sData["facility_end_time"]));

            array_push($response["time_slot"], $time_slot); 
          }
           $response["message"]="$datafoundMsg";
           $response["status"]="200";
           echo json_encode($response);
        } else {
           $no_time_slot_available_for_this_date = $xml->string->no_time_slot_available_for_this_date;
           $response["message"]="$no_time_slot_available_for_this_date";
           $response["status"]="201";
           echo json_encode($response);
        }
      }


    }else if($_POST['checkFacilityMonth']=="checkFacilityMonth" && filter_var($facility_id, FILTER_VALIDATE_INT) == true  && filter_var($no_of_person, FILTER_VALIDATE_INT) == true  ){
        $q=$d->select("facilities_master"," facility_id='$facility_id'","");
        $row=mysqli_fetch_array($q);
        extract($row);

        if ($user_type!='' && $user_type==0) {
          $facility_amount = $row['facility_amount'];
          
        } else if ($user_type!='' && $user_type==1 && $row['facility_amount_tenant']!=0) {
          $facility_amount = $row['facility_amount_tenant'];
          
        } else {
          $facility_amount = $row['facility_amount'];
        }


        // if ($gst=='1') {
        //   $newAmount= $facility_amount*$tax_slab/100;
        //   $amountwithGst= $facility_amount+$newAmount;
        //   $facility_amount= number_format($amountwithGst,2,'.','');
        // }else {
           $facility_amount=$facility_amount;
        // }

        $book_date= date('Y-m-d',strtotime($book_date));
        $booking_start_time_days= date("H:i", strtotime($booking_start_time_days));
        $booking_end_time_days= date("H:i", strtotime($booking_end_time_days));
        if ($no_of_person=='' || $no_of_person==0) {
           $please_select_number_of_person = $xml->string->please_select_number_of_person;
            $response["message"]="$please_select_number_of_person";
            $response["status"]="201";
            echo json_encode($response);
            exit();
        }
        $month = strtotime($book_date);

         $response["booking_month"] = array();
        for ($i=1; $i <12 ; $i++) { 

            $month_name = date('F', $month);
            $year_name = date('Y', $month);
            $fullName= $month_name.'-'.$year_name;
            $startTime= date('H:i:s',strtotime($booking_start_time_days));
            $endTime= date('H:i:s',strtotime($booking_end_time_days));

            $count5=$d->sum_data("no_of_person","facility_booking_months","(cancel_status=0 AND facility_id='$facility_id' AND month_name='$fullName' AND ('$startTime' BETWEEN booking_start_time AND booking_end_time OR '$endTime' BETWEEN booking_start_time AND booking_end_time))");
            $row11=mysqli_fetch_array($count5);
            $booke_count=$row11['SUM(no_of_person)'];
            if ($booke_count=='') {
              $booke_count=0;
            }
             $person_limit;
            $availble= $person_limit-$booke_count;
            if ($availble>=$no_of_person) {
               $toatlAmount = $facility_amount*$no_of_person;

               $toatlAmountwithPerson = $toatlAmount*$i;
               
               $booking_month = array();
               $booking_month["month_name"]=$fullName;
               $booking_month["month_number"]=$i.'';
               $booking_month["person_limit"]=$person_limit.'';
               $booking_month["booked_count"]=$booke_count.'';
               $booking_month["booking_amount"]=$toatlAmountwithPerson.'';
               if ($is_taxble=='1') {
                  // if ($gst=="0") {
                       $gstAmountNew = $toatlAmountwithPerson - ( $toatlAmountwithPerson  * (100/(100+$tax_slab)));
                      $booking_amount_without_gst =    $toatlAmountwithPerson-$gstAmountNew;
                  // } else {
                  //     $gstAmountNew=  $toatlAmountwithPerson*$data_facility_list['tax_slab']/100;
                  //    $booking_amount_without_gst =  $toatlAmountwithPerson;
                  // }

                 $booking_month["gst_amount"]=number_format($gstAmountNew,2,'.','');
                 $booking_month["booking_amount_without_gst"]=number_format($booking_amount_without_gst,2,'.','');
               } else {
                $booking_month["gst_amount"]='0';
                $booking_month["booking_amount_without_gst"]='0';
               }
               
               array_push($response["booking_month"], $booking_month); 
              // echo "<input  type='checkbox' name='booking_month[]' value='$fullName'><b> ".$fullName.' ('.$booke_count.'/'.$person_limit.')</b>';
              // echo "<option value='$fullName' > ".$fullName.' ('.$booke_count.'/'.$person_limit.')</b>'." </option>";
            }else {
             $no_booking_available = $xml->string->no_booking_available;
              $response["message"]="$no_booking_available";
              $response["status"]="201";
              echo json_encode($response);
              exit();
            }
              $month = strtotime('+1 month', $month);
              // $i++;


        }

        $response["message"]="$datafoundMsg";
        $response["status"]="200";
        echo json_encode($response);
          

    }else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }

    }else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }

}
