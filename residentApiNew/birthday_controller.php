<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST) && !empty($_POST)) {

	if ($key == $keydb && $auth_check=='true') {

		$response = array();
		extract(array_map("test_input", $_POST));

		if (isset($getBirthdays) &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$ends = array('th','st','nd','rd','th','th','th','th','th','th');

			$startDate = date('Y-m-d');

			$birthdayQry = $d->selectRow("
				users_master.member_date_of_birth,
				users_master.user_id,
				users_master.user_full_name,
				users_master.user_designation,
				users_master.user_profile_pic,
				block_master.block_name,
				floors_master.floor_name,
				wishes_master.wish_reply,
				wishes_master.wish_id
				","
				block_master,
				floors_master,
				users_master LEFT JOIN wishes_master ON wishes_master.wish_by_id = '$user_id' AND wishes_master.wish_to_id = users_master.user_id AND DATE_FORMAT(wishes_master.wish_date,'%m-%d') = DATE_FORMAT(users_master.member_date_of_birth,'%m-%d')
				","
				users_master.user_status = '1' 
				AND users_master.active_status = '0' 
				AND users_master.delete_status = '0'  
				AND users_master.floor_id = floors_master.floor_id
				AND users_master.block_id = block_master.block_id
				AND users_master.member_date_of_birth != '0000-00-00'
				AND DATE_ADD(users_master.member_date_of_birth, INTERVAL YEAR(CURDATE())-YEAR(users_master.member_date_of_birth) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(users_master.member_date_of_birth),1,0) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 30 DAY)","");

			$anniversaryQry = $d->selectRow("
				users_master.wedding_anniversary_date,
				users_master.user_id,
				users_master.user_full_name,
				users_master.user_designation,
				users_master.user_profile_pic,
				block_master.block_name,
				floors_master.floor_name,
				wishes_master.wish_reply,
				wishes_master.wish_id
				","
				block_master,
				floors_master,
				users_master LEFT JOIN wishes_master ON wishes_master.wish_by_id = '$user_id' AND wishes_master.wish_to_id = users_master.user_id AND DATE_FORMAT(wishes_master.wish_date,'%m-%d') = DATE_FORMAT(users_master.wedding_anniversary_date,'%m-%d')
				","
				users_master.user_status = '1' 
				AND users_master.active_status = '0' 
				AND users_master.delete_status = '0'  
				AND users_master.floor_id = floors_master.floor_id
				AND users_master.block_id = block_master.block_id
				AND users_master.wedding_anniversary_date != '0000-00-00'
				AND DATE_ADD(users_master.wedding_anniversary_date, INTERVAL YEAR(CURDATE())-YEAR(users_master.wedding_anniversary_date) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(users_master.wedding_anniversary_date),1,0) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 30 DAY)","");

			$workAnniversaryQry = $d->selectRow("
				user_employment_details.joining_date,
				users_master.user_id,
				users_master.user_full_name,
				users_master.user_designation,
				users_master.user_profile_pic,
				block_master.block_name,
				floors_master.floor_name,
				wishes_master.wish_reply,
				wishes_master.wish_id
				","
				users_master,
				block_master,
				floors_master,
				user_employment_details LEFT JOIN wishes_master ON wishes_master.wish_by_id = '$user_id' AND wishes_master.wish_to_id = user_employment_details.user_id AND DATE_FORMAT(wishes_master.wish_date,'%m-%d') = DATE_FORMAT(user_employment_details.joining_date,'%m-%d')
				","
				users_master.user_status = '1' 
				AND users_master.active_status = '0' 
				AND users_master.delete_status = '0' 				 
				AND users_master.floor_id = floors_master.floor_id
				AND users_master.block_id = block_master.block_id
				AND users_master.user_id = user_employment_details.user_id
				AND user_employment_details.joining_date != '0000-00-00'
				AND DATE_ADD(user_employment_details.joining_date, INTERVAL YEAR(CURDATE())-YEAR(user_employment_details.joining_date) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(user_employment_details.joining_date),1,0) YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 30 DAY)","");

			$response["today_birth_days"] = array();
			$array1 = array();
			$array2 = array();
			$array3 = array();

			if (mysqli_num_rows($birthdayQry) > 0) {
				while($birthday_data = mysqli_fetch_array($birthdayQry)){
					array_push($array1,$birthday_data);
				}
			}

			if (mysqli_num_rows($anniversaryQry) > 0) {
				while($birthday_data = mysqli_fetch_array($anniversaryQry)){
					array_push($array2,$birthday_data);
				}
			}

			if (mysqli_num_rows($workAnniversaryQry) > 0) {
				while($birthday_data = mysqli_fetch_array($workAnniversaryQry)){
					array_push($array3,$birthday_data);
				}
			}

			$newArray = array_merge($array1,$array2,$array3);
            foreach ($newArray as $key => $part) {
            
            	if ($part['member_date_of_birth']!=null && $part['member_date_of_birth'] != '0000-00-00' && $part['member_date_of_birth'] != '') {
            		$sort[$key] = date('m-d',strtotime($part['member_date_of_birth']));
            	}else if ($part['wedding_anniversary_date']!=null && $part['wedding_anniversary_date'] != '0000-00-00' && $part['wedding_anniversary_date'] != '') {
            		$sort[$key] = date('m-d',strtotime($part['wedding_anniversary_date']));
            	}else if ($part['joining_date']!=null && $part['joining_date'] != '0000-00-00' &&$part['joining_date'] != '') {
            		$sort[$key] = date('m-d',strtotime($part['joining_date']));
            	}
            }

            array_multisort($sort, SORT_ASC, $newArray);

            for ($i=0; $i <count($newArray) ; $i++) { 
                $birthDay = array(); 

                $birthDay['user_id'] = $newArray[$i]['user_id'];
				$birthDay['user_full_name'] = $newArray[$i]['user_full_name'];
				$birthDay['user_designation'] = $newArray[$i]['user_designation'];
				$birthDay['block_name'] = $newArray[$i]['block_name'];
				$birthDay['floor_name'] = $newArray[$i]['floor_name'];
				$birthDay['wish_id'] = $newArray[$i]['wish_id'].'';
				$birthDay['wish_reply'] = $newArray[$i]['wish_reply'].'';
				$birthDay['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $newArray[$i]['user_profile_pic'];

				if ($newArray[$i]['member_date_of_birth']!=null &&  $newArray[$i]['member_date_of_birth'] != '0000-00-00' &&  $newArray[$i]['member_date_of_birth'] != '') {
					$birthDayDate = $newArray[$i]['member_date_of_birth'];
					$birthDay['member_date_of_birth'] = date('d-m-Y',strtotime($birthDayDate));

					$birthDay['isBirthDay'] = true;
					$birthDay['isAnniversary'] = false;
					$birthDay['isWorkAnniversary'] = false;

					$dateOfbirth = $newArray[$i]['member_date_of_birth'];
					
					$diff = date_diff(date_create($dateOfbirth), date_create(date("Y-m-d")));

					$month = $diff->format('%m');

					if($month > 0){
						$number = $diff->format('%y') + 1;
					}else{
						$number = $diff->format('%y');
					}

					if (($number %100) >= 11 && ($number%100) <= 13){
					 	$abbreviation = $number. 'th';
					}else{
					 	$abbreviation = $number. $ends[$number % 10];
					}

					$bDate = date("d-m",strtotime($dateOfbirth));					

					$birthDay['total_year_view'] = $abbreviation." Birthday";

				}else if ($newArray[$i]['wedding_anniversary_date']!=null &&  $newArray[$i]['wedding_anniversary_date'] != '0000-00-00' &&  $newArray[$i]['wedding_anniversary_date'] != '') {
					$birthDayDate = $newArray[$i]['wedding_anniversary_date'];
					$birthDay['wedding_anniversary_date'] = date('d-m-Y',strtotime($birthDayDate));
					
					$birthDay['isBirthDay'] = false;
					$birthDay['isAnniversary'] = true;
					$birthDay['isWorkAnniversary'] = false;

					$dateOfbirth = $newArray[$i]['wedding_anniversary_date'];
					
					$diff = date_diff(date_create($dateOfbirth), date_create(date("Y-m-d")));

					$month = $diff->format('%m');

					if($month > 0){
						$number = $diff->format('%y') + 1;
					}else{
						$number = $diff->format('%y');
					}

					if (($number %100) >= 11 && ($number%100) <= 13){
					 	$abbreviation = $number. 'th';
					}else{
					 	$abbreviation = $number. $ends[$number % 10];
					}

					$birthDay['total_year_view'] = $abbreviation." Anniversary";

				}else if ($newArray[$i]['joining_date']!=null &&  $newArray[$i]['joining_date'] != '0000-00-00' && $newArray[$i]['joining_date'] != '') {
					$birthDayDate = $newArray[$i]['joining_date'];
					$birthDay['joining_date'] = date('d-m-Y',strtotime($birthDayDate));
					
					$birthDay['isBirthDay'] = false;
					$birthDay['isAnniversary'] = false;
					$birthDay['isWorkAnniversary'] = true;

					$dateOfbirth = $newArray[$i]['joining_date'];
					$diff = date_diff(date_create($dateOfbirth), date_create(date("Y-m-d")));

					$month = $diff->format('%m');

					if($month > 0){
						$number = $diff->format('%y') + 1;
					}else{
						$number = $diff->format('%y');
					}

					if (($number %100) >= 11 && ($number%100) <= 13){
					 	$abbreviation = $number. 'th';
					}else{
					 	$abbreviation = $number. $ends[$number % 10];
					}

					$birthDay['total_year_view'] = $abbreviation." Work Anniversary";

				}

				$bdatet = date('d-m',strtotime($birthDayDate));

				if ($bdatet == date('d-m')) {
					$birthDay["is_today"] = true;
					$birthDay['member_date_of_birth_view'] = date('dS F',strtotime($birthDayDate));
				}else{
					$birthDay["is_today"] = false;
					$birthDay['member_date_of_birth_view'] = date('dS F',strtotime($birthDayDate));
				}	

				if ($number > 0) {
                	array_push($response['today_birth_days'],$birthDay);
				}	

            }

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else if($_POST['changeRequestForBirthday']=="changeRequestForBirthday" && $user_id !=0 && $user_id!='' &&  filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 

			$hasReq = $d->selectRow("birthdate_change_request_id","birthdate_change_request","user_id = '$user_id' AND birthdate_status = '0'");

			if (mysqli_num_rows($hasReq) > 0) {
				$response["message"] = "You have already applied to change the date of birth, wait until the admin approves.";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
			}


            $m->set_data('user_id',$user_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('new_birth_date',$new_birth_date);
            $m->set_data('created_datetime',date("Y-m-d H:i:s"));

            $a1= array (
                'user_id'=> $m->get_data('user_id'),
                'society_id'=> $m->get_data('society_id'),
                'new_birth_date'=> $m->get_data('new_birth_date'),
                'created_datetime'=> $m->get_data('created_datetime'),
            );
           
            $q=$d->insert("birthdate_change_request",$a1);
                
            if ($q>0) {

            	$title = "New Birthday Date Change Request";
                $description = "Requested by, ".$user_name;

                $access_type = $birthday_change_request_Access;

                include 'check_access_data_user.php';

                // To Employee App
                if (count($userIDArray) > 0) {

                    $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

              
                    $nResident->noti("new_birthday_request","",$society_id,$fcmArrayPA,$title,$description,"");
                    $nResident->noti_ios("new_birthday_request","",$society_id,$fcmArrayIosPA,$title,$description,"");

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"new_birthday_request","birthday_date.png","","users_master.user_id IN ('$userFcmIds')");
                }

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("21",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("21",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"birthdateRequest");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"birthdateRequest");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"birthdateRequest",
                  'admin_click_action '=>"birthdateRequest",
                  'notification_logo'=>'birthday_date.png',
                );
                            
                $d->insert("admin_notification",$notiAry);
               
                $response["message"] = "A request for a new date to change the date of birth has been sent to the admin.";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong! please try again";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getBirthdayRequest']=="getBirthdayRequest"  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

        	$access_type = $birthday_change_request_Access;

	        include "check_access_data.php";

	        if ($userIds=='') {
	            switch ($access_for) {
	                case '0':
	                case '1':
	                    $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
	                    $LIMIT_DATA = " LIMIT 100";
	                    break;
	                case '2':
	                    $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
	                    $LIMIT_DATA = " LIMIT 100";
	                    break;
	                case '3':
	                    $appendQuery = " AND birthdate_change_request.user_id IN ('$accessUserIds')";
	                    $LIMIT_DATA = " LIMIT 100";
	                    break;
	                
	                default:
	                    // code...
	                    break;
	            }
	        }else{
	            $appendQuery = " AND birthdate_change_request.user_id IN ($userIds)";
	        }

	        $response['modification_access'] = $modification_access;

			$birthdayQry = $d->selectRow("birthdate_change_request.*,users_master.user_id,users_master.user_full_name,users_master.user_designation,users_master.user_profile_pic,block_master.block_name,floors_master.floor_name","birthdate_change_request,users_master,block_master,floors_master","birthdate_change_request.society_id = '$society_id' AND birthdate_change_request.birthdate_status = '0' AND users_master.user_id = birthdate_change_request.user_id AND users_master.floor_id = floors_master.floor_id AND users_master.block_id = block_master.block_id $appendQuery");


			if (mysqli_num_rows($birthdayQry) > 0) {
			
				$response["birthday_requests"] = array();			

				while($birthday_data = mysqli_fetch_array($birthdayQry)){
					$birthDay = array();

					$birthDay['birthdate_change_request_id'] = $birthday_data['birthdate_change_request_id'];
					$birthDay['user_id'] = $birthday_data['user_id'];
					$birthDay['user_full_name'] = $birthday_data['user_full_name'];
					$birthDay['branch_name'] = $birthday_data['block_name'];
					$birthDay['department_name'] = $birthday_data['floor_name'];
					$birthDay['user_designation'] = $birthday_data['user_designation'];
					$birthDay['new_birth_date_view'] = date('dS F Y',strtotime($birthday_data['new_birth_date']));
					$birthDay['new_birth_date'] = $birthday_data['new_birth_date'];
					$birthDay['new_birth_date_indian'] = date('d-m-Y',strtotime($birthday_data['new_birth_date']));
					$birthDay['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $birthday_data['user_profile_pic'];

					array_push($response["birthday_requests"],$birthDay);
				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data found";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if($_POST['checkAccessBirthday']=="checkAccessBirthday" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $birthday_change_request_Access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['approveBirthDate']=="approveBirthDate" && $my_user_id!='' && $birthdate_change_request_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			$m->set_data('approved_by_id',$my_user_id);
			$m->set_data('approved_by_type',"0");
			$m->set_data('birthdate_status',"1");
			$m->set_data('modified_datetime',date('Y-m-d H:i:s'));

            $a = array(
                'approved_by_id'=>$m->get_data('approved_by_id'),
                'approved_by_type'=>$m->get_data('approved_by_type'),
                'birthdate_status'=>$m->get_data('birthdate_status'),
                'modified_datetime'=>$m->get_data('modified_datetime'),
            );

            $qry = $d->update("birthdate_change_request",$a,"user_id = '$user_id' AND birthdate_change_request_id = '$birthdate_change_request_id'");

            if ($qry == true) {
            	$m->set_data('member_date_of_birth',$new_birth_date);

	            $a1 = array(
	                'member_date_of_birth'=>$m->get_data('member_date_of_birth'),
	            );

	            $qry = $d->update("users_master",$a1,"user_id = '$user_id'");

	            $title = "Birthdate Request Approved";
                $description = "By $user_name";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("birthdate_change","",$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("birthdate_change","",$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"birthdate_change","birthday_date.png","user_id = '$user_id'",$user_id);

	            $response["message"] = "Request approved";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "Something went wrong!";
				$response["status"] = "201";
				echo json_encode($response);
            }
                      
        }else if($_POST['rejectBirthDate']=="rejectBirthDate" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			$m->set_data('reject_by_id',$my_user_id);
			$m->set_data('reject_reason',$reject_reason);
			$m->set_data('reject_by_type',"0");
			$m->set_data('birthdate_status',"2");
			$m->set_data('modified_datetime',date('Y-m-d H:i:s'));

            $a = array(
                'reject_by_id'=>$m->get_data('reject_by_id'),
                'reject_reason'=>$m->get_data('reject_reason'),
                'reject_by_type'=>$m->get_data('reject_by_type'),
                'birthdate_status'=>$m->get_data('birthdate_status'),
                'modified_datetime'=>$m->get_data('modified_datetime'),
            );

            $qry = $d->update("birthdate_change_request",$a,"user_id = '$user_id' AND birthdate_change_request_id = '$birthdate_change_request_id'");

            if ($qry == true) {
            	
	            $title = "Birthdate Request Rejected";
                $description = "By $user_name";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];



                if ($device == 'android') {
                    $nResident->noti("birthdate_change","",$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("birthdate_change","",$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"birthdate_change","birthday_date.png","user_id = '$user_id'",$user_id);

	            $response["message"] = "Request Rejected";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "Something went wrong!";
				$response["status"] = "201";
				echo json_encode($response);
            }
                      
        }else if($_POST['getBirthdayTemplate']=="getBirthdayTemplate" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			$birthdayQry = $d->selectRow("wish_template_id,wish_template,wish_template_type","wish_template_status = '0' AND wish_template_type = '$templateType'");


			if (mysqli_num_rows($birthdayQry) > 0) {
			
				$response["birthday_template"] = array();			

				while($birthday_data = mysqli_fetch_array($birthdayQry)){
					$birthDay = array();

					$birthDay['wish_template_id'] = $birthday_data['wish_template_id'];
					$birthDay['wish_template_type'] = $birthday_data['wish_template_type'];

					if ($birthday_data['wish_template'] != '') {
						$birthDay['wish_template'] = $base_url . "img/birthday_template/" .  $birthday_data['wish_template'];
					}else{
						$birthDay['wish_template'] = "";
					}
				
					array_push($response["birthday_template"],$birthDay);
				}

	            $response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "No Templates Found";
				$response["status"] = "201";
				echo json_encode($response);
            }
                      
        }else if($_POST['addWish']=="addWish" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        	$today = date('Y-m-d');

        	$qryCheck = $d->count_data_direct("wish_id","wishes_master","wish_by_id = '$user_id' AND wish_to_id = '$wish_to_id' AND DATE_FORMAT(wish_date,'%Y-%m-%d') = '$today'");

        	if ($qryCheck > 0) {
        		$response["message"] = "You have already sent your wishes to $to_user_name";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
        	}

			$m->set_data('society_id',$society_id);
			$m->set_data('wish_template_url',$wish_template);
			$m->set_data('wish_content',$wish_content);
			$m->set_data('wish_type',$wish_type);
			$m->set_data('wish_by_id',$user_id);
			$m->set_data('wish_to_id',$wish_to_id);
			$m->set_data('wish_date',date('Y-m-d H:i:s'));

            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'wish_template_url'=>$m->get_data('wish_template_url'),
                'wish_content'=>$m->get_data('wish_content'),
                'wish_type'=>$m->get_data('wish_type'),
                'wish_by_id'=>$m->get_data('wish_by_id'),
                'wish_to_id'=>$m->get_data('wish_to_id'),
                'wish_date'=>$m->get_data('wish_date'),
            );

            $qry = $d->insert("wishes_master",$a);

            if ($qry == true) {

            	$qq12 = $d->selectRow("birthday_notification_type","society_master","society_id = '$society_id'");

            	$dataSoc = mysqli_fetch_array($qq12);

            	$birthday_notification_type = $dataSoc['birthday_notification_type'];

            	if ($birthday_notification_type != 0) {

	            	$todayDate = date('Y-m-d');

	            	$qqq2 = $d->selectRow("log_id","log_master","user_id = '1' AND log_name = 'Birthday cron run' AND DATE_FORMAT(log_time,'%Y-%m-%d') = '$todayDate'");

	            	if (mysqli_num_rows($qqq2) <= 0) {

	            		$startDate = date('Y-m-d');
						$startDate1 = date('m-d',strtotime($startDate));

	            		$birthdayQry = $d->selectRow("
							user_full_name,
							block_id,
							floor_id,
							user_designation
							","
							users_master
							","
							user_status = '1' 
							AND active_status = '0' 
							AND delete_status = '0'  
							AND user_id != '$user_id'
							AND DATE_FORMAT(member_date_of_birth,'%m-%d') = '$startDate1' ","");

	            		$totalBirthDay = mysqli_num_rows($birthdayQry);

	            		$brithdayBlockIdAry = array();
	            		$brithdayFloorIdAry = array();

	            		if ($totalBirthDay >0) {

	            			$birthdayDescription = "";
		            		
		            		if ($totalBirthDay == 1) {
		            			$dataBirthday = mysqli_fetch_array($birthdayQry);	
			            		$title = "Today's Birthday 🎂";		            				            		
		            			$description = $dataBirthday['user_full_name'] . " (".$dataBirthday['user_designation'].")";
		            			array_push($brithdayBlockIdAry,$dataBirthday['block_id']);
		            			array_push($brithdayFloorIdAry,$dataBirthday['floor_id']);
		            		}else if ($totalBirthDay == 2) {		
		            			while($dataBirthday = mysqli_fetch_array($birthdayQry)){
			            			$birthdayDescription = $birthdayDescription."".$dataBirthday['user_full_name'] . " (".$dataBirthday['user_designation'].") and ";
			            			array_push($brithdayBlockIdAry,$dataBirthday['block_id']);
		            				array_push($brithdayFloorIdAry,$dataBirthday['floor_id']);
				            	}
			            		$title = "Today's Birthdays 🎂";				            	
			            		$description = substr_replace($birthdayDescription ,"",-4);

		            		}else if ($totalBirthDay > 2) {
		            			$dataBirthday = mysqli_fetch_array($birthdayQry);
		            			while($dataBirthday1 = mysqli_fetch_array($birthdayQry)){
			            			array_push($brithdayBlockIdAry,$dataBirthday1['block_id']);
		            				array_push($brithdayFloorIdAry,$dataBirthday1['floor_id']);
				            	}		
			            		$title = "Today's Birthdays 🎂";				            	
		            			$description = $dataBirthday['user_full_name'] . " (".$dataBirthday['user_designation'].") and ".($totalBirthDay - 1)." others";
		            		}

		            		$brithdayBlockIdAry = join("','",$brithdayBlockIdAry); 
		            		$brithdayFloorIdAry = join("','",$brithdayFloorIdAry); 

		                    if ($birthday_notification_type == 1) {
		                    	$fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND block_id IN ('$brithdayBlockIdAry')");
		                    	$fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND block_id IN ('$brithdayBlockIdAry')");
		                    }else if($birthday_notification_type == 2){
		                    	$fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND floor_id IN ('$brithdayFloorIdAry')");
		                    	$fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND floor_id IN ('$brithdayFloorIdAry')");
		                    }else if($birthday_notification_type == 3){
		                    	$fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android'");
		                    	$fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios'");
		                    }

		                    		                
		                    $nResident->noti("birthday","",$society_id,$fcmArrayPA,$title,$description,$feedId);
		                    $nResident->noti_ios("birthday","",$society_id,$fcmArrayIosPA,$title,$description,$feedId);
	            		}


						$anniversaryQry = $d->selectRow("
							wedding_anniversary_date,
							user_id,
							user_full_name,
							block_id,
							floor_id,
							user_designation
							","
							users_master
							","
							user_status = '1' 
							AND active_status = '0' 
							AND delete_status = '0'
							AND user_id != '$user_id'  
							AND DATE_FORMAT(wedding_anniversary_date,'%m-%d') = '$startDate1'  ","");

						$totalAnniversary = mysqli_num_rows($anniversaryQry);

						$anniversaryBlockIdAry = array();
	            		$anniversaryFloorIdAry = array();

	            		if ($totalAnniversary >0) {

	            			$anniversaryDescription = "";
		            		
		            		if ($totalAnniversary == 1) {
		            			$dataAnniversary = mysqli_fetch_array($anniversaryQry);		            			
			            		$title = "Today's Anniversary 💝";		            			
		            			$description = $dataAnniversary['user_full_name'] . " (".$dataAnniversary['user_designation'].")";
		            			array_push($anniversaryBlockIdAry,$dataAnniversary['block_id']);
		            			array_push($anniversaryFloorIdAry,$dataAnniversary['floor_id']);
		            		}else if ($totalAnniversary == 2) {	

		            			while($dataAnniversary = mysqli_fetch_array($anniversaryQry)){
			            			$anniversaryDescription = $anniversaryDescription."".$dataAnniversary['user_full_name'] . " (".$dataAnniversary['user_designation'].") and ";
			            			array_push($anniversaryBlockIdAry,$dataAnniversary['block_id']);
		            				array_push($anniversaryFloorIdAry,$dataAnniversary['floor_id']);
				            	}
				            	$title = "Today's Anniversaries 💝";	
			            		$description = substr_replace($anniversaryDescription ,"",-4);
				            	
		            		}else if ($totalAnniversary > 2) {
		            			$dataAnniversary = mysqli_fetch_array($anniversaryQry);
		            			while($dataAnniversary1 = mysqli_fetch_array($birthdayQry)){
			            			array_push($anniversaryBlockIdAry,$dataAnniversary1['block_id']);
		            				array_push($anniversaryFloorIdAry,$dataAnniversary1['floor_id']);
				            	}			
				            	$title = "Today's Anniversaries 💝";	
		            			$description = $dataAnniversary['user_full_name'] . " (".$dataAnniversary['user_designation'].") and ".($totalAnniversary - 1)." others";
		            		}

		            		$anniversaryBlockIdAry = join("','",$anniversaryBlockIdAry); 
		            		$anniversaryFloorIdAry = join("','",$anniversaryFloorIdAry);


		                    if ($birthday_notification_type == 1) {
		                    	$fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND block_id IN ('$anniversaryBlockIdAry')");
		                    	$fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND block_id IN ('$anniversaryBlockIdAry')");
		                    }else if($birthday_notification_type == 2){
		                    	$fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND floor_id IN ('$anniversaryFloorIdAry')");
		                    	$fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND floor_id IN ('$anniversaryFloorIdAry')");
		                    }else if($birthday_notification_type == 3){
		                    	$fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android'");
		                    	$fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios'");
		                    }

		                    		                
		                    $nResident->noti("birthday","",$society_id,$fcmArrayPA,$title,$description,$feedId);
		                    $nResident->noti_ios("birthday","",$society_id,$fcmArrayIosPA,$title,$description,$feedId);
	            		}

						$workAnniversaryQry = $d->selectRow("
							user_employment_details.joining_date,
							users_master.user_full_name,
							users_master.user_designation,
							users_master.block_id,
							users_master.floor_id
							","
							users_master,
							user_employment_details
							","
							users_master.user_status = '1' 
							AND users_master.active_status = '0' 
							AND users_master.delete_status = '0' 
							AND users_master.user_id != '$user_id'				 
							AND users_master.user_id = user_employment_details.user_id  
							AND DATE_FORMAT(user_employment_details.joining_date,'%m-%d') = '$startDate1'","");

	            		$totalWorkAnniversary = mysqli_num_rows($workAnniversaryQry);

	            		$workAnniversaryBlockIdAry = array();
	            		$workAnniversaryFloorIdAry = array();

	            		if ($totalWorkAnniversary >0) {

	            			$workAnniversaryDescription = "";
		            		
		            		if ($totalWorkAnniversary == 1) {
		            			$dataWorkAnniversary = mysqli_fetch_array($workAnniversaryQry);		            			
			            		$title = "Today's Work Anniversary 💼";		            			
		            			$description = $dataWorkAnniversary['user_full_name'] . " (".$dataWorkAnniversary['user_designation'].")";

		            			array_push($workAnniversaryBlockIdAry,$dataWorkAnniversary['block_id']);
		            			array_push($workAnniversaryFloorIdAry,$dataWorkAnniversary['floor_id']);
		            		}else if ($totalWorkAnniversary == 2) {	

		            			while($dataWorkAnniversary = mysqli_fetch_array($workAnniversaryQry)){
			            			$workAnniversaryDescription = $workAnniversaryDescription."".$dataWorkAnniversary['user_full_name'] . " (".$dataWorkAnniversary['user_designation'].") and ";
			            			array_push($workAnniversaryBlockIdAry,$dataWorkAnniversary['block_id']);
		            				array_push($workAnniversaryFloorIdAry,$dataWorkAnniversary['floor_id']);
				            	}
				            	$title = "Today's Work Anniversaries 💼";	
			            		$description = substr_replace($workAnniversaryDescription ,"",-4);
				            	
		            		}else if ($totalWorkAnniversary > 2) {
		            			$dataWorkAnniversary = mysqli_fetch_array($workAnniversaryQry);		
		            			while($dataWorkAnniversary1 = mysqli_fetch_array($birthdayQry)){
			            			array_push($workAnniversaryBlockIdAry,$dataWorkAnniversary1['block_id']);
		            				array_push($workAnniversaryFloorIdAry,$dataWorkAnniversary1['floor_id']);
				            	}
				            	$title = "Today's Work Anniversaries 💼";	
		            			$description = $dataWorkAnniversary['user_full_name'] . " (".$dataWorkAnniversary['user_designation'].") and ".($totalWorkAnniversary - 1)." others";
		            		}

		            		$workAnniversaryBlockIdAry = join("','",$workAnniversaryBlockIdAry); 
		            		$workAnniversaryFloorIdAry = join("','",$workAnniversaryFloorIdAry);


		                    if ($birthday_notification_type == 1) {
		                    	$fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND block_id IN ('$workAnniversaryBlockIdAry')");
		                    	$fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND block_id IN ('$workAnniversaryBlockIdAry')");
		                    }else if($birthday_notification_type == 2){
		                    	$fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND floor_id IN ('$workAnniversaryFloorIdAry')");
		                    	$fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND floor_id IN ('$workAnniversaryFloorIdAry')");
		                    }else if($birthday_notification_type == 3){
		                    	$fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android'");
		                    	$fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios'");
		                    }

		                    		                
		                    $nResident->noti("birthday","",$society_id,$fcmArrayPA,$title,$description,$feedId);
		                    $nResident->noti_ios("birthday","",$society_id,$fcmArrayIosPA,$title,$description,$feedId);
	            		}
	                    
	                    $d->insert_log("", "$society_id", "1", "Super Admin", "Birthday cron run");
	            	}
	            }
            	
	            $title = "$user_name Wishing You";
                $description = "$wish_content";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$wish_to_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $icon=array();

                $icon['userProfile'] = $wish_template;


                if ($device == 'android') {
                    $nResident->noti("employee_wish",$wish_template,$society_id,$user_token,$title,$description,$icon);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("employee_wish",$wish_template,$society_id,$user_token,$title,$description,$icon);
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"employee_wish","birthday_date.png","user_id = '$wish_to_id'",$wish_to_id);

	            $response["message"] = "Wish sent successfully";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "Something went wrong!";
				$response["status"] = "201";
				echo json_encode($response);
            }
                      
        }else if($_POST['addWishReply']=="addWishReply" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			$m->set_data('wish_reply',$wish_reply);

            $a = array(
                'wish_reply'=>$m->get_data('wish_reply'),
            );

            $qry = $d->update("wishes_master",$a,"wish_id = '$wish_id'");

            if ($qry == true) {
            	
	            $title = "Reply on your wish";
                $description = "By $user_name,\n$wish_reply";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$wish_by_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];


                if ($device == 'android') {
                    $nResident->noti("wish_reply",$wish_template,$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("wish_reply",$wish_template,$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"wish_reply","birthday_date.png","user_id = '$wish_by_id'",$wish_by_id);

	            $response["message"] = "Successfully replied on wish";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "Something went wrong!";
				$response["status"] = "201";
				echo json_encode($response);
            }
                      
        }else if($_POST['getMyWishes']=="getMyWishes" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			$birthdayQry = $d->selectRow("users_master.user_full_name,users_master.user_designation,users_master.user_profile_pic,wishes_master.*","users_master,wishes_master","wishes_master.wish_to_id = '$user_id' AND users_master.user_id = wishes_master.wish_by_id","ORDER BY wishes_master.wish_date DESC");

			if (mysqli_num_rows($birthdayQry) > 0) {
			
				$response["my_wishes"] = array();			

				while($data = mysqli_fetch_array($birthdayQry)){
					$wishes = array();

					$wishes['wish_id'] = $data['wish_id'];
					$wishes['wish_content'] = $data['wish_content'];
					$wishes['wish_by_id'] = $data['wish_by_id'];
					$wishes['wish_to_id'] = $data['wish_to_id'];
					$wishes['wish_reply'] = $data['wish_reply'];

					$wishes["user_full_name"] = $data['user_full_name'];
                    $wishes["user_designation"] = $data['user_designation'];
                    if ($data['user_profile_pic'] != '') {
                        $wishes["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $wishes["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }
                    
					$wishes['wish_template'] = $data['wish_template_url'].'';
				
					array_push($response["my_wishes"],$wishes);
				}

	            $response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "No Templates Found";
				$response["status"] = "201";
				echo json_encode($response);
            }
                      
        }else {
			$response["message"] = "wrong tag.";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {

		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}
?>