<?php 
if ($topic_name!='') {

	if (!is_array($registrationIds)) {
         $registrationIds = array($registrationIds);
    }
	// code...
	$fields = [
	    'to' => "/topics/".$topic_name,
	    'registration_tokens' => $registrationIds,
	];

	$fields = json_encode($fields);

	$headers = array(
	   'Authorization: '.$server_key,
	   'project_id: '.$project_id,
	   'Content-Type: application/json'
	);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://iid.googleapis.com/iid/v1:batchAdd');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

	$result = curl_exec($ch); 
} 
// //Response of above Request: 
?>
