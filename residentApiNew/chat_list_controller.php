<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    $response = array();
    extract(array_map("test_input" , $_POST));

    
    if ($key==$keydb && $auth_check=='true') {
    

       if (isset($getekeeperList) && $getekeeperList=='getekeeperList' && filter_var($society_id, FILTER_VALIDATE_INT) == true ) {

              $q=$d->getRecentChatGatekeeper("chat_master",$user_id);
            $response["gatekeeper"]=array();
        
        // $q=$d->select("employee_master","society_id ='$society_id' AND society_id!=0 and emp_type_id='0'");

            if (mysqli_num_rows($q)>0) {
                $recentGpAry=array();

            while($gatekeeper_data=mysqli_fetch_array($q)) {
                 $gatekeeper_data = array_map("html_entity_decode", $gatekeeper_data);
                 $recentUser= $gatekeeper_data['msg_for'];
                 array_push($recentGpAry, $recentUser);

                $qf2=$d->select("chat_master","msg_for='$recentUser' AND msg_by='$user_id' AND society_id='$society_id' AND sent_to='1' OR msg_by='$recentUser' AND msg_for='$user_id' AND society_id='$society_id' AND send_by='1'","ORDER BY chat_id DESC ");
                $unData11=mysqli_fetch_array($qf2);
                 $unData11 = array_map("html_entity_decode", $unData11);

                $qd=$d->select("employee_master","society_id ='$society_id' AND society_id!=0 and emp_type_id='0' AND emp_id='$recentUser' AND emp_status = '1'");
                $empData=mysqli_fetch_array($qd);

                if ($empData>0) {

                $gatekeeper_record = array(); 

                $gatekeeper_record["emp_id"]=$empData['emp_id'];

                if ($unData11['msg_type']=='1') {
                   $gatekeeper_record["msg_data"]="📸 Image";
                }else if ($unData11['msg_type']=='2') {
                    $gatekeeper_record["msg_data"]="📃 Document";
                }else if ($unData11['msg_type']=='3') {
                   $gatekeeper_record["msg_data"]="🎧 Audio";
                }else if ($unData11['msg_type']=='4') {
                   $gatekeeper_record["msg_data"]="📌 Location";
                }else if ($unData11['msg_type']=='5') {
                   $gatekeeper_record["msg_data"]="👤 Contact";
                }else if ($unData11=='6') {
                    $gatekeeper_record["msg_data"]="🎞️ Video";
                }else {
                     $gatekeeper_record["msg_data"]=$unData11['msg_data'];
                }
                
                 // if ($unData11['msg_data']=='' && $unData11['msg_img']!='') {
                 //    $gatekeeper_record["msg_data"]="📸 Image";
                 // }else {
                 //    $gatekeeper_record["msg_data"]=$unData11['msg_data'].'';
                 // }
                $gatekeeper_record["msg_date"]=$unData11['msg_date'].'';
                $emp_id = $empData['emp_id'];
                $gatekeeper_record["emp_name"]=$empData['emp_name'];
                $gatekeeper_record["emp_mobile"]=$empData['country_code'].' '.$empData['emp_mobile'];
                $gatekeeper_record["emp_profile"]=$base_url."img/emp/".$empData['emp_profile'];

                $qChatMsg = $d->select("chat_master","send_by='1' AND msg_by='$recentUser' AND msg_for='$user_id' AND msg_status='0'");

                $gatekeeper_record["chat_status"]=mysqli_num_rows($qChatMsg)."";
              
                array_push($response["gatekeeper"], $gatekeeper_record); 
                }
            }
                
                 $temp = true;
            }else{
                 $temp = false;
            }


            $ids = join("','",$recentGpAry); 
            $q111=$d->select("employee_master","emp_id NOT IN ('$ids') AND society_id ='$society_id' AND society_id!=0 and emp_type_id='0' AND emp_status = '1'");

            if (mysqli_num_rows($q111)>0) {
            

            while($gatekeeper_data=mysqli_fetch_array($q111)) {
                 $recentUser= $gatekeeper_data['emp_id'];


                $gatekeeper_record = array(); 

                $gatekeeper_record["emp_id"]=$gatekeeper_data['emp_id'];
               
                 $gatekeeper_record["msg_data"]='';
                $gatekeeper_record["msg_date"]='1970-01-01 00:01:00';
                $emp_id = $gatekeeper_data['emp_id'];
                $gatekeeper_record["emp_name"]=$gatekeeper_data['emp_name'];
                $gatekeeper_record["emp_mobile"]=$gatekeeper_data['country_code'].' '.$gatekeeper_data['emp_mobile'];
                $gatekeeper_record["emp_profile"]=$base_url."img/emp/".$gatekeeper_data['emp_profile'];

                $qChatMsg = $d->select("chat_master","send_by='1' AND msg_by='$recentUser' AND msg_for='$user_id' AND msg_status='0'");

                $gatekeeper_record["chat_status"]=mysqli_num_rows($qChatMsg)."";
              
                array_push($response["gatekeeper"], $gatekeeper_record); 
            }
                
                 $temp1 = true;
            }else{
                 $temp1 = false;
            }


            if ($temp==true || $temp1 ==true) {
                $response["message"] = "$datafoundMsg";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "$noDatafoundMsg";
                $response["status"] = "201";
                echo json_encode($response);
            }

    }else if (isset($getekeeperListNew) && $getekeeperListNew=='getekeeperListNew' && filter_var($society_id, FILTER_VALIDATE_INT) == true ) {

              // $q=$d->getRecentChatGatekeeper("chat_master",$user_id);
            $response["member"]=array();
        
        $q=$d->select("employee_master","society_id ='$society_id' AND society_id!=0 and emp_type_id='0' AND emp_status = '1'");

            if (mysqli_num_rows($q)>0) {

            while($data=mysqli_fetch_array($q)) {
                
                $member = array(); 
                $member["user_id"]=$data['emp_id'];
                $member["user_full_name"]=$data['emp_name'];
                $member["user_first_name"]=$data['emp_name'];
                $member["user_last_name"]=$data['emp_name'];
                $member["gender"]="";
                $member["user_type"]='0';
                $member["block_name"]="Security Guard";
                $member["floor_name"]=$data['gate_number'];
                $member["unit_name"]="";
                $member["floor_id"]="";
                $member["unit_id"]="";
                $member["unit_status"]="";
                $member["user_status"]="";
                $member["member_status"]="";
                $member["user_mobile"]=$data['emp_mobile'];
                $member["public_mobile"]="0";
                $member["member_date_of_birth"]="";
                $member["alt_mobile"] ="";
                $member["user_profile_pic"]=$base_url."img/emp/".$data['emp_profile'];
                $member["owner_name"]="";
                $member["owner_email"]="";
                $member["owner_mobile"]="";
                $member["block_status"] = false;
                $member["token"] = $data['emp_token'];
           

                array_push($response["member"], $member); 

            }


            
                $response["message"] = "$datafoundMsg";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "$noDatafoundMsg";
                $response["status"] = "201";
                echo json_encode($response);
            }

    } else if (isset($getMemberList) && $getMemberList=='getMemberList' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }
         
        if ($user_type==1 ) {
            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id' AND users_master.tenant_view=0","ORDER BY unit_master.unit_id ASC");
        } else {

            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id'","ORDER BY unit_master.unit_id ASC");
        }

         if (mysqli_num_rows($q3)>0) {
                $response["member"]=array();

                while($data=mysqli_fetch_array($q3)) {

                $cq=$d->select("chat_block_master","society_id='$society_id' AND block_for='$user_id' AND block_by='$data[user_id]' OR society_id='$society_id' AND  block_by='$user_id' AND block_for='$data[user_id]'");

                if ($data['member_status']==0) {
                    $uType = "(Primary)";
                } else {
                    $uType= "(Team)";
                }

                $q1=$d->select("user_employment_details","user_id='$data[user_id]'");
                $proData=mysqli_fetch_array($q1);

                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["company_name"]=html_entity_decode($data['user_designation']).'';
                $member["user_designation"]=$proData['designation'].'';
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['floor_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["unit_name"]=$data['block_name'].' '.$uType;
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["public_mobile"]=$data['public_mobile'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];
                $member["token"] = $data['user_token'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";
                    if (mysqli_num_rows($cq)>0) {
                        $member["block_status"] = true;
                    } else {
                        $member["block_status"] = false;
                    }
           

                    array_push($response["member"], $member); 
                }

                $response["message"]="Member List";
                $response["status"]="200";
                 
                echo json_encode($response);

         }else{
            $response["message"]="No Member Found!";
            $response["status"]="201";
            echo json_encode($response);
        }


    } else if (isset($getMemberListNew) && $getMemberListNew=='getMemberListNew' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }
         
       
            $q3=$d->selectRow("unit_master.unit_name,
                block_master.block_name,
                floors_master.floor_name,
                users_master.user_id,
                users_master.user_full_name,
                users_master.user_first_name,
                users_master.user_last_name,
                users_master.country_code,
                unit_master.company_name,
                users_master.user_designation,
                users_master.gender,
                users_master.user_type,
                users_master.floor_id,
                users_master.block_id,
                users_master.unit_id,
                unit_master.unit_status,
                users_master.user_status,
                users_master.member_status,
                users_master.user_mobile,
                users_master.public_mobile,
                users_master.member_date_of_birth,
                users_master.alt_mobile,
                users_master.user_token,
                users_master.user_profile_pic,
                users_master.user_designation,
                block_master.block_status
                ","unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id'  AND users_master.floor_id = '$floor_id'","ORDER BY unit_master.unit_id ASC");

         if (mysqli_num_rows($q3)>0) {
                $response["member"]=array();

                while($data=mysqli_fetch_array($q3)) {

                // $cq=$d->selectRow("chat_block_id","chat_block_master","society_id='$society_id' AND block_for='$user_id' AND block_by='$data[user_id]' OR society_id='$society_id' AND  block_by='$user_id' AND block_for='$data[user_id]'");

                $uType = "(Primary)";

              
                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["country_code"]=$data['country_code'];
                $member["company_name"]=html_entity_decode($data['user_designation']).'';
                $member["user_designation"]=html_entity_decode($data['user_designation']);
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['floor_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["unit_name"]=$data['block_name'].' '.$uType;
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["public_mobile"]=$data['public_mobile'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];
                $member["token"] = $data['user_token'];
                if ($data['user_profile_pic']!="") {
                    $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                } else {
                    $member["user_profile_pic"]="";
                }
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";
                    // if (mysqli_num_rows($cq)>0) {
                    //     $member["block_status"] = true;
                    // } else {
                        $member["block_status"] = false;
                    // }
           

                    array_push($response["member"], $member); 
                }

                $response["message"]="Member List";
                $response["status"]="200";
                 
                echo json_encode($response);

         }else{
            $response["message"]="No Member Found!";
            $response["status"]="201";
            echo json_encode($response);
        }


    } else if (isset($getMemberList) && $getMemberList=='getMemberListGroup' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }
         

        $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id'","ORDER BY block_master.block_sort ASC");

         if (mysqli_num_rows($q3)>0) {
                $response["member"]=array();

                while($data=mysqli_fetch_array($q3)) {

                $cq=$d->select("chat_block_master","society_id='$society_id' AND block_for='$user_id' AND block_by='$data[user_id]' OR society_id='$society_id' AND  block_by='$user_id' AND block_for='$data[user_id]'");

                $userId= $data['user_id'];
                $qqq=$d->select("chat_group_member_master","user_id='$userId' AND society_id='$society_id' AND group_id='$group_id'");


                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['floor_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["company_name"]=html_entity_decode($data['user_designation']).'';
                $member["unit_name"]=$data['floor_name'];
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["public_mobile"]=$data['public_mobile'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];
                $member["token"] = $data['user_token'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";
                  if (mysqli_num_rows($cq)>0) {
                        $member["block_status"] = true;
                    } else {
                        $member["block_status"] = false;
                    }
                if (mysqli_num_rows($qqq)>0) {
                    $member["join_status"]=true;
                    array_push($response["member"], $member); 
                } else {
                    $member["join_status"]=false;
                }

                }

                $response["message"]="Member List";
                $response["status"]="200";
                 
                echo json_encode($response);

         }else{
            $response["message"]="No Member Found!";
            $response["status"]="201";
            echo json_encode($response);
        }


    } else if (isset($getMemberList) && $getMemberList=='getMemberListGroupNew' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }
         

        $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id' AND users_master.floor_id = '$floor_id'","ORDER BY block_master.block_sort ASC");

         if (mysqli_num_rows($q3)>0) {
                $response["member"]=array();

                while($data=mysqli_fetch_array($q3)) {

                $cq=$d->select("chat_block_master","society_id='$society_id' AND block_for='$user_id' AND block_by='$data[user_id]' OR society_id='$society_id' AND  block_by='$user_id' AND block_for='$data[user_id]'");

                $userId= $data['user_id'];
                $qqq=$d->select("chat_group_member_master","user_id='$userId' AND society_id='$society_id' AND group_id='$group_id'");


                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['floor_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["company_name"]=html_entity_decode($data['user_designation']).'';
                $member["unit_name"]=$data['floor_name'];
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["public_mobile"]=$data['public_mobile'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];
                $member["token"] = $data['user_token'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";
                  if (mysqli_num_rows($cq)>0) {
                        $member["block_status"] = true;
                    } else {
                        $member["block_status"] = false;
                    }
                if (mysqli_num_rows($qqq)>0) {
                    $member["join_status"]=true;
                    array_push($response["member"], $member); 
                } else {
                    $member["join_status"]=false;
                }

                }

                $response["message"]="Member List";
                $response["status"]="200";
                 
                echo json_encode($response);

         }else{
            $response["message"]="No Member Found!";
            $response["status"]="201";
            echo json_encode($response);
        }


    }else if (isset($getMemberListGroupNew) && $getMemberListGroupNew=='getMemberListGroupNew' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }


        $q3=$d->select("unit_master,block_master,users_master,floors_master,chat_group_member_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id' AND chat_group_member_master.user_id = users_master.user_id AND chat_group_member_master.group_id = '$group_id' AND chat_group_member_master.chat_group_member_delete=0","ORDER BY floors_master.floor_sort ASC");

         if (mysqli_num_rows($q3)>0) {
                $response["member"]=array();

                while($data=mysqli_fetch_array($q3)) {

                $cq=$d->select("chat_block_master","society_id='$society_id' AND block_for='$user_id' AND block_by='$data[user_id]' OR society_id='$society_id' AND  block_by='$user_id' AND block_for='$data[user_id]'");

                $userId= $data['user_id'];
                $qqq=$d->select("chat_group_member_master","user_id='$userId' AND society_id='$society_id' AND group_id='$group_id'");


                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['floor_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["company_name"]=html_entity_decode($data['user_designation']).'';
                $member["unit_name"]=$data['floor_name'];
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["public_mobile"]=$data['public_mobile'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];
                $member["token"] = $data['user_token'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";
                  if (mysqli_num_rows($cq)>0) {
                        $member["block_status"] = true;
                    } else {
                        $member["block_status"] = false;
                    }
                if (mysqli_num_rows($qqq)>0) {
                    $member["join_status"]=true;
                    array_push($response["member"], $member); 
                } else {
                    $member["join_status"]=false;
                }

                }

                $response["message"]="Member List";
                $response["status"]="200";
                 
                echo json_encode($response);

         }else{
            $response["message"]="No Member Found!";
            $response["status"]="201";
            echo json_encode($response);
        }


    }else if (isset($getMemberListGroupNew2) && $getMemberListGroupNew2=='getMemberListGroupNew2' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }

        if ($group_type == 1) {
            $q3=$d->select("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND users_master.user_status!=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_id!='$user_id'  AND users_master.block_id = '$block_id' AND chat_group_master.group_id = '$group_id' AND block_master.block_id = chat_group_master.group_common_id");
          
        }else if ($group_type == 2) {
            $q3=$d->select("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$user_id'  AND users_master.floor_id = '$floor_id' AND chat_group_master.group_id = '$group_id' AND floors_master.floor_id = chat_group_master.group_common_id");
        }else if ($group_type == 3) {
            $q3=$d->select("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$user_id'  AND users_master.zone_id = '$zone_id' AND chat_group_master.group_id = '$group_id' AND zone_master.zone_id = chat_group_master.group_common_id");
        }else if ($group_type == 4) {
            $q3=$d->select("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$user_id'  AND users_master.level_id = '$level_id' AND chat_group_master.group_id = '$group_id' AND employee_level_master.level_id = chat_group_master.group_common_id");
        }else{
            $q3=$d->select("unit_master,block_master,users_master,floors_master,chat_group_member_master","users_master.delete_status=0 AND users_master.active_status = 0 AND users_master.user_status!=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id' AND chat_group_member_master.user_id = users_master.user_id AND chat_group_member_master.group_id = '$group_id' AND chat_group_member_master.chat_group_member_delete=0","ORDER BY floors_master.floor_sort ASC");
        }


         if (mysqli_num_rows($q3)>0) {
                $response["member"]=array();

                while($data=mysqli_fetch_array($q3)) {


                $userId= $data['user_id'];
               


                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['block_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["company_name"]=html_entity_decode($data['user_designation']).'';
                $member["unit_name"]=$data['floor_name'];
                $member["floor_id"]=$data['floor_id'];
                $member["block_id"]=$data['block_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["country_code"]=$data['country_code'];
                $member["public_mobile"]=$data['public_mobile'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];
                $member["token"] = $data['user_token'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";
                $member["total_employee"]=mysqli_num_rows($q3)."";
                    $member["block_status"] = false;
                $member["join_status"]=true;
                array_push($response["member"], $member); 

                }

                $response["message"]="Member List";
                $response["status"]="200";
                 
                echo json_encode($response);

         }else{
            $response["message"]="No Member Found!";
            $response["status"]="201";
            echo json_encode($response);
        }


    } else if (isset($getMemberList) && $getMemberList=='getMemberListGroupAddMember' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

        if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }
         

        $q3=$d->select("unit_master,block_master,users_master,floors_master","block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id'","ORDER BY block_master.block_sort ASC");

         if (mysqli_num_rows($q3)>0) {
                $response["member"]=array();

                while($data=mysqli_fetch_array($q3)) {

                $cq=$d->select("chat_block_master","society_id='$society_id' AND block_for='$user_id' AND block_by='$data[user_id]' OR society_id='$society_id' AND  block_by='$user_id' AND block_for='$data[user_id]'");

                $userId= $data['user_id'];
                $qqq=$d->select("chat_group_member_master","user_id='$userId' AND society_id='$society_id' AND group_id='$group_id'");

                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['floor_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["unit_name"]=$data['block_name'];
                $member["company_name"]=html_entity_decode($data['user_designation']);
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["public_mobile"]=$data['public_mobile'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];
                $member["token"] = $data['user_token'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";
                if (mysqli_num_rows($cq)>0) {
                    $member["block_status"] = true;
                } else {
                    $member["block_status"] = false;
                }
                
                if (mysqli_num_rows($qqq)>0) {
                    $member["join_status"]=true;
                } else {
                    $member["join_status"]=false;
                    array_push($response["member"], $member); 
                }
                

                }

                $response["message"]="Member List";
                $response["status"]="200";
                 
                echo json_encode($response);

         }else{
            $response["message"]="No Member Found!";
            $response["status"]="201";
            echo json_encode($response);
        }


    }  else if (isset($getMemberListNew) && $getMemberListNew=='getMemberListGroupAddMember' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

        if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }
         

        $q3=$d->select("unit_master,block_master,users_master,floors_master","block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id' AND users_master.floor_id = '$floor_id' AND users_master.user_id NOT IN ('$ids')","ORDER BY block_master.block_sort ASC");

         if (mysqli_num_rows($q3)>0) {
                $response["member"]=array();

                while($data=mysqli_fetch_array($q3)) {

                $cq=$d->select("chat_block_master","society_id='$society_id' AND block_for='$user_id' AND block_by='$data[user_id]' OR society_id='$society_id' AND  block_by='$user_id' AND block_for='$data[user_id]'");

                $userId= $data['user_id'];
                $qqq=$d->select("chat_group_member_master","user_id='$userId' AND society_id='$society_id' AND group_id='$group_id'");

                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["gender"]=$data['gender'];
                $member["user_type"]=$data['user_type'];
                $member["block_name"]=$data['floor_name'];
                $member["floor_name"]=$data['floor_name'];
                $member["unit_name"]=$data['block_name'];
                $member["company_name"]=html_entity_decode($data['user_designation']);
                $member["floor_id"]=$data['floor_id'];
                $member["unit_id"]=$data['unit_id'];
                $member["unit_status"]=$data['unit_status'];
                $member["user_status"]=$data['user_status'];
                $member["member_status"]=$data['member_status'];
                $member["user_mobile"]=$data['user_mobile'];
                $member["public_mobile"]=$data['public_mobile'];
                $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                $member["alt_mobile"] = $data['alt_mobile'];
                $member["token"] = $data['user_token'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                $member["owner_name"]=$data['owner_name']."";
                $member["owner_email"]=$data['owner_email']."";
                $member["owner_mobile"]=$data['owner_mobile']."";
                if (mysqli_num_rows($cq)>0) {
                    $member["block_status"] = true;
                } else {
                    $member["block_status"] = false;
                }
                
                if (mysqli_num_rows($qqq)>0) {
                    $member["join_status"]=true;
                } else {
                    $member["join_status"]=false;
                    array_push($response["member"], $member); 
                }
                

                }

                $response["message"]="Member List";
                $response["status"]="200";
                 
                echo json_encode($response);

         }else{
            $response["message"]="No Member Found!";
            $response["status"]="201";
            echo json_encode($response);
        }


    }  else if (isset($getMemberListRecent) && $getMemberListRecent=='getMemberListRecent' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }


        $qSociety = $d->select("society_master","society_id ='$society_id'");
        $user_data_society = mysqli_fetch_array($qSociety);
        $group_chat_status =  $user_data_society['group_chat_status'];

        $q3=$d->select("chat_master","msg_by='$user_id' AND society_id='$society_id' AND sent_to=0","ORDER BY chat_id DESC");
        $q4=$d->select("chat_master","msg_for='$user_id' AND society_id='$society_id' AND send_by=0","ORDER BY chat_id DESC");
        $q5=$d->select("chat_group_master,chat_group_member_master","chat_group_master.group_id = chat_group_member_master.group_id AND
        chat_group_member_master.user_id='$user_id' AND chat_group_member_master.society_id='$society_id'","");


        $response["member"]=array();

         

         if (mysqli_num_rows($q4)>0) {
               

                while($data1=mysqli_fetch_array($q4)) {
                    $qs1=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  AND users_master.user_status=1 AND  users_master.user_id='$data1[msg_by]'");
                    $userData1=mysqli_fetch_array($qs1);
                $userIdCheck11=$userData1['user_id'];
                if ($userIdCheck11!='') {
                  
                    $member = array(); 
                    $member["chat_id"]=$data1['chat_id'];
                    // $member["msg_data"]=$data1['msg_data'];
                     if ($data1['msg_data']=='' && $data1['msg_img']!='') {
                        $member["msg_data"]="📸 Image";
                    }else {
                        $member["msg_data"]=$data1['msg_data'];
                    }
                    $member["msg_date"]=$data1['msg_date'];
                    $member["flag"]="1";
                    $member["member_size"]="1";
                    $member["user_id"]=$userData1['user_id'];
                    $member["user_full_name"]=$userData1['user_full_name'];
                    $member["user_first_name"]=$userData1['user_first_name'];
                    $member["user_last_name"]=$userData1['user_last_name'];
                    $member["gender"]=$data['gender'];
                    $member["user_type"]=$userData1['user_type'];
                    $member["block_name"]=$userData1['floor_name'];
                    $member["floor_name"]=$userData1['floor_name'];
                    $member["unit_name"]=$userData1['block_name'];
                    $member["company_name"]=html_entity_decode($data['user_designation']).' '.$uType;
                    $member["floor_id"]=$userData1['floor_id'];
                    $member["unit_id"]=$userData1['unit_id'];
                    $member["unit_status"]=$userData1['unit_status'];
                    $member["user_status"]=$userData1['user_status'];
                    $member["member_status"]=$userData1['member_status'];
                    $member["user_mobile"]=$userData1['user_mobile'];
                    $member["public_mobile"]=$userData1['public_mobile'];
                    $member["member_date_of_birth"]="".$userData1['member_date_of_birth'];
                    $member["alt_mobile"] = $userData1['alt_mobile'];

                    $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$userData1['user_profile_pic'];

                    $chatCount=$d->count_data_direct("chat_id","chat_master","msg_for='$user_id' AND msg_by='$userData1[user_id]'  AND society_id='$society_id' AND msg_status='0'");

                    $member["chatCount"] = $chatCount.'';

                        array_push($response["member"], $member); 
                    }
                }

         }



        if (mysqli_num_rows($q5)>0) {

            while($data2=mysqli_fetch_array($q5)) {
              
            $userIdCheck =$data2['user_id'];
            if ($userIdCheck!='') {

                $qf1=$d->select("chat_group_member_master","group_id='$data2[group_id]' AND society_id='$society_id' AND  user_id='$user_id'");
                $unData=mysqli_fetch_array($qf1);

                $qf2=$d->select("chat_master_group","group_id='$data2[group_id]' AND society_id='$society_id' ","ORDER BY chat_id DESC ");
                $unData11=mysqli_fetch_array($qf2);
              
                $member = array(); 
                $member["chat_id"]=$data2['group_id'];
                $member["msg_data"]=$unData11['msg_data'].'';
                 if ($unData11['msg_data']=='' && $unData11['msg_img']!='') {
                        $member["msg_data"]="📸 Image";
                    }else {
                        $member["msg_data"]=$unData11['msg_data'].'';
                    }
                $member["msg_date"]=$unData11['msg_date'].'';
                $member["flag"]="2";
                $member["member_size"]=$d->count_data_direct("chat_group_member_id","chat_group_member_master,users_master","chat_group_member_master.user_id=users_master.user_id  AND chat_group_member_master.group_id='$data2[group_id]'").'';
                $member["user_id"]=$data2['created_by'];
                $member["user_full_name"]=$data2['group_name'];
                $member["user_first_name"]=$data2['group_name'];
                $member["user_last_name"]=$data2['group_name'];
                $member["gender"]="";
                $member["user_type"]="group";
                $member["user_profile_pic"]=$base_url."img/users/".$data2['group_icon'];

                $member["block_name"]="group";
                $member["floor_name"]="";
                $member["unit_name"]="";
                $member["floor_id"]="";
                $member["unit_id"]="";
                $member["unit_status"]="";
                $member["user_status"]="";
                $member["member_status"]="";
                $member["user_mobile"]="";
                $member["public_mobile"]="1";
                $member["member_date_of_birth"]="";
                $member["alt_mobile"] = "";
                $chatCount=$d->count_data_direct("chat_id","chat_master_group","group_id='$data2[group_id]' AND society_id='$society_id'")-$unData['unread_count'].'';
                if ($chatCount>0) {
                    $member["chatCount"] = $chatCount;
                } else {
                    $member["chatCount"] = "0";

                }

                
                    array_push($response["member"], $member); 
                }
            }

        }


       if (mysqli_num_rows($q3)>0) {

            while($data=mysqli_fetch_array($q3)) {
                $qs=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  AND users_master.user_status=1 AND  users_master.user_id='$data[msg_for]'");
                $userData=mysqli_fetch_array($qs);
            $userIdCheck =$userData['user_id'];
            if ($userIdCheck!='') {
              
                $member = array(); 
                $member["chat_id"]=$data['chat_id'];
                // $member["msg_data"]=$data['msg_data'];
                 if ($data['msg_data']=='' && $data['msg_img']!='') {
                        $member["msg_data"]="📸 Image";
                    }else {
                        $member["msg_data"]=$data['msg_data'];
                    }
                $member["msg_date"]=$data['msg_date'];
                $member["flag"]="1";
                $member["member_size"]="1";
                $member["user_id"]=$userData['user_id'];
                $member["user_full_name"]=$userData['user_full_name'];
                $member["user_first_name"]=$userData['user_first_name'];
                $member["user_last_name"]=$userData['user_last_name'];
                $member["gender"]="";
                $member["user_type"]=$userData['user_type'];
                $member["block_name"]=$userData['floor_name'];
                $member["floor_name"]=$userData['floor_name'];
                $member["unit_name"]=$userData['block_name'];
                $member["floor_id"]=$userData['floor_id'];
                $member["unit_id"]=$userData['unit_id'];
                $member["unit_status"]=$userData['unit_status'];
                $member["user_status"]=$userData['user_status'];
                $member["member_status"]=$userData['member_status'];
                $member["user_mobile"]=$userData['user_mobile'];
                $member["public_mobile"]=$userData['public_mobile'];
                $member["member_date_of_birth"]="".$userData['member_date_of_birth'];
                $member["alt_mobile"] = $userData['alt_mobile'];

                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$userData['user_profile_pic'];

                $chatCount=$d->count_data_direct("chat_id","chat_master","msg_by='$userData[user_id]' AND msg_for='$user_id'  AND society_id='$society_id' AND msg_status='0'");

                $member["chatCount"] = $chatCount.'';
                
                    array_push($response["member"], $member); 
                }
            }

      

         }


            $response["message"]="Member List";
            $response["group_chat_status"] = $group_chat_status;
            $response["status"]="200";
            echo json_encode($response);

    }  else if (isset($getRecentChatMember) && $getRecentChatMember=='getRecentChatMember' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }


        $qSociety = $d->select("society_master","society_id ='$society_id'");
        $user_data_society = mysqli_fetch_array($qSociety);
        $group_chat_status =  $user_data_society['group_chat_status'];

        // if ($user_type==1 && $user_type!='') {
        //     $q4=$d->getRecentChatMemberNewTenant("chat_master",$user_id);
        // } else {
            $q4=$d->getRecentChatMemberNew("chat_master",$user_id);
        // }
        
        $response["member"]=array();

         

         if (mysqli_num_rows($q4)>0) {
               

                while($data1=mysqli_fetch_array($q4)) {
                    if ($data1['msg_for']==$user_id) {
                        $recentUser= $data1['msg_by'];
                        
                    }else if ($data1['msg_by']==$user_id) {
                        $recentUser= $data1['msg_for'];
                    }
                    
                
                if ($recentUser!='') {
                    
                    

                    $member = array(); 
                    $member["chat_id"]=$data1['chat_id'];
                    // $member["msg_data"]=$data1['msg_data'];
                        if ($data1['msg_type']=='1') {
                            $member["msg_data"]="📸 Image";
                        }else if ($data1['msg_type']=='2') {
                            $member["msg_data"]="📃 Document";
                        }else if ($data1['msg_type']=='3') {
                           $member["msg_data"]="🎧 Audio";
                        }else if ($data1['msg_type']=='4') {
                           $member["msg_data"]="📌 Location";
                        }else if ($data1['msg_type']=='5') {
                           $member["msg_data"]="👤 Contact";
                        }else if ($data1['msg_type']=='6') {
                            $member["msg_data"]="🎞️ Video";
                        }else {
                             $member["msg_data"]=$data1['msg_data'];
                        }
                    // }else {
                    //     $member["msg_data"]=$data1['msg_data'];
                    // }
                    $member["msg_date"]=$data1['msg_date'];
                    $member["flag"]="1";
                    $member["member_size"]="1";
                    $member["user_id"]=$recentUser;
                    $member["user_full_name"]=$data1['user_full_name'];
                    $member["user_first_name"]=$data1['user_first_name'];
                    $member["user_last_name"]=$data1['user_last_name'];
                    $member["gender"]=$data1['gender'].'';
                    $member["user_type"]=$data1['user_type'];
                    $member["company_name"]=html_entity_decode($data1['user_designation']);
                    $member["block_name"]=$data1['block_name'];
                    $member["floor_name"]=$data1['floor_name'].'';
                    $member["unit_name"]=$data1['unit_name'];
                    $member["floor_id"]=$data1['floor_id'];
                    $member["unit_id"]=$data1['unit_id'];
                    $member["unit_status"]=$data1['unit_status'];
                    $member["user_status"]=$data1['user_status'];
                    $member["member_status"]=$data1['member_status'];
                    $member["user_mobile"]=$data1['user_mobile'];
                    $member["country_code"]=$data1['country_code'];
                    $member["public_mobile"]=$data1['public_mobile'];
                    $member["member_date_of_birth"]="".$data1['member_date_of_birth'];
                    $member["alt_mobile"] = "";

                    $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data1['user_profile_pic'];

                    $chatCount=$d->count_data_direct("chat_id","chat_master","msg_for='$user_id' AND msg_by='$recentUser'  AND society_id='$society_id' AND msg_status='0'");

                    $member["chatCount"] = $chatCount.'';

                        array_push($response["member"], $member); 
                    }
                }

         }

     

            $response["message"]="Member List";
            $response["group_chat_status"] = $group_chat_status;
            $response["status"]="200";
            echo json_encode($response);

    }   else if (isset($getGroupListRecent) && $getGroupListRecent=='getGroupListRecent' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }


        $qSociety = $d->select("society_master","society_id ='$society_id'");
        $user_data_society = mysqli_fetch_array($qSociety);
        $group_chat_status =  $user_data_society['group_chat_status'];

        $q1=$d->select("chat_group_master,chat_group_member_master","chat_group_master.group_id = chat_group_member_master.group_id AND chat_group_member_master.user_id='$user_id' AND chat_group_member_master.society_id='$society_id' AND group_type = 0","");

        /*$q2=$d->select("chat_group_master,block_master,users_master","users_master.block_id = block_master.block_id AND chat_group_master.group_common_id = users_master.block_id AND users_master.delete_status = 0 AND block_master.block_id = chat_group_master.group_common_id AND users_master.user_status != 0 AND  chat_group_master.society_id='$society_id' AND group_type = 1","");*/



        $response["member"]=array();

        

        if (mysqli_num_rows($q1)>0) {

            while($data2=mysqli_fetch_array($q1)) {
              
            $userIdCheck =$data2['user_id'];
            if ($userIdCheck!='') {

                $qf1=$d->select("chat_group_member_master","group_id='$data2[group_id]' AND society_id='$society_id' AND  user_id='$user_id'");
                $unData=mysqli_fetch_array($qf1);

                $qf2=$d->select("chat_master_group","group_id='$data2[group_id]' AND society_id='$society_id' ","ORDER BY chat_id DESC ");
                $unData11=mysqli_fetch_array($qf2);
              
                $member = array(); 
                $member["chat_id"]=$data2['group_id'];
                $member["msg_data"]=$unData11['msg_data'].'';
                 // if ($unData11['msg_data']=='' && $unData11['msg_img']!='') {
                        if ($unData11['msg_type']==1) {
                            $member["msg_data"]="📸 Image";
                        } else if ($unData11['msg_type']==2) {
                            $member["msg_data"]="📃 Document";
                        }else if ($unData11['msg_type']==3) {
                           $member["msg_data"]="🎧 Audio";
                        }else if ($unData11['msg_type']==4) {
                           $member["msg_data"]="📌 Location";
                        }else if ($unData11['msg_type']==5) {
                           $member["msg_data"]="👤 Contact";
                        }else if ($unData11['msg_type']=='6') {
                            $member["msg_data"]="🎞️ Video";
                        }else {
                             $member["msg_data"]=$unData11['msg_data'].'';
                        }
                    // }else {
                    //     $member["msg_data"]=$unData11['msg_data'].'';
                    // }
                $member["msg_date"]=$unData11['msg_date'].'';
                $member["flag"]="2";
                $member["member_size"]=$d->count_data_direct("chat_group_member_id","chat_group_member_master,users_master","chat_group_member_master.user_id=users_master.user_id AND users_master.active_status = 0 AND users_master.delete_status = 0 AND users_master.user_status=1  AND chat_group_member_master.group_id='$data2[group_id]'").'';
                $member["user_id"]=$data2['created_by'];
                $member["user_full_name"]=$data2['group_name'];
                $member["user_first_name"]=$data2['group_name'];
                $member["user_last_name"]=$data2['group_name'];
                $member["gender"]="";
                $member["user_type"]="group";
                $member["user_profile_pic"]=$base_url."img/users/".$data2['group_icon'];

                $member["block_name"]="group";
                $member["floor_name"]="";
                $member["unit_name"]="";
                $member["floor_id"]="";
                $member["unit_id"]="";
                $member["unit_status"]="";
                $member["user_status"]="";
                $member["member_status"]="";
                $member["user_mobile"]="";
                $member["public_mobile"]="1";
                $member["member_date_of_birth"]="";
                $member["alt_mobile"] = "";
                $chatCount=$d->count_data_direct("chat_id","chat_master_group","group_id='$data2[group_id]' AND society_id='$society_id'")-$unData['unread_count'].'';
                if ($chatCount>0) {
                    $member["chatCount"] = $chatCount;
                } else {
                    $member["chatCount"] = "0";

                }

                
                    array_push($response["member"], $member); 
                }
            }

        }


     


            

            $response["message"]="Chat List";
            $response["group_chat_status"] = $group_chat_status;
            $response["status"]="200";
            echo json_encode($response);

    }else if (isset($getGroupListRecentNew) && $getGroupListRecentNew=='getGroupListRecentNew' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) { 

         if ($unit_id!='' && $unit_id!=0) {
             $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
             if (mysqli_num_rows($quc)>0) {
                $response["message"] = "Access Denied..!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
             }

          }


        $qSociety = $d->select("society_master","society_id ='$society_id'");
        $user_data_society = mysqli_fetch_array($qSociety);
        $group_chat_status =  $user_data_society['group_chat_status'];

    
 /*$chatCount=$d->count_data_direct("chat_id","chat_master_group","group_id='$data2[group_id]' AND society_id='$society_id'")-$unData['unread_count'].'';*/
        $q1=$d->selectRow("chat_group_master.*,chat_group_member_master.*,
            (SELECT msg_data FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg,
            (SELECT msg_type FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_type,
            (SELECT msg_date FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_date,
            (SELECT count(chat_id) FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id AND chat_master_group.msg_by_name != '') AS total_msg,
            (SELECT unread_count FROM chat_group_unread_message WHERE chat_group_master.group_id = chat_group_unread_message.group_id AND chat_group_unread_message.user_id = '$user_id') AS my_read_msg","chat_group_master,chat_group_member_master","chat_group_master.group_id = chat_group_member_master.group_id AND chat_group_member_master.user_id='$user_id' AND chat_group_member_master.society_id='$society_id' AND group_type = 0 AND chat_group_master.active_status = 0","");


        // Blockwise group

        $q2=$d->selectRow("block_master.*,chat_group_master.*,(SELECT msg_data FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg,(SELECT msg_type FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_type,(SELECT msg_date FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_date,
            (SELECT count(chat_id) FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id AND chat_master_group.msg_by_name != '') AS total_msg,
            (SELECT unread_count FROM chat_group_unread_message WHERE chat_group_master.group_id = chat_group_unread_message.group_id AND chat_group_unread_message.user_id = '$user_id') AS my_read_msg","block_master,chat_group_master","block_master.block_id = chat_group_master.group_common_id AND chat_group_master.group_common_id = '$block_id' AND  chat_group_master.society_id='$society_id' AND chat_group_master.group_type = 1 AND chat_group_master.active_status = 0","");

        // Floorwise group

        $q3=$d->selectRow("floors_master.*,chat_group_master.*,(SELECT msg_data FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg,(SELECT msg_type FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_type,(SELECT msg_date FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_date,
            (SELECT count(chat_id) FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id AND chat_master_group.msg_by_name != '') AS total_msg,
            (SELECT unread_count FROM chat_group_unread_message WHERE chat_group_master.group_id = chat_group_unread_message.group_id AND chat_group_unread_message.user_id = '$user_id') AS my_read_msg","floors_master,chat_group_master","chat_group_master.society_id='$society_id' AND chat_group_master.group_type = 2 AND floors_master.floor_id = chat_group_master.group_common_id AND floors_master.floor_id = '$floor_id' AND chat_group_master.active_status = 0","");

        // Zone group

        $q4=$d->selectRow("zone_master.*,users_master.*,chat_group_master.*,(SELECT msg_data FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg,(SELECT msg_type FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_type,(SELECT msg_date FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_date,
            (SELECT count(chat_id) FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id AND chat_master_group.msg_by_name != '') AS total_msg,
            (SELECT unread_count FROM chat_group_unread_message WHERE chat_group_master.group_id = chat_group_unread_message.group_id AND chat_group_unread_message.user_id = '$user_id') AS my_read_msg","zone_master,users_master,chat_group_master","chat_group_master.society_id='$society_id' AND chat_group_master.group_type = 3 AND zone_master.zone_id = chat_group_master.group_common_id AND zone_master.zone_id = '$zone_id' AND users_master.zone_id = zone_master.zone_id AND users_master.zone_id = '$zone_id' AND users_master.user_id = '$user_id' AND chat_group_master.active_status = 0","");

        // Level group

        $q5=$d->selectRow("employee_level_master.*,users_master.*,chat_group_master.*,(SELECT msg_data FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg,(SELECT msg_type FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_type,(SELECT msg_date FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id ORDER BY chat_master_group.chat_id DESC LIMIT 1) AS last_msg_date,
            (SELECT count(chat_id) FROM chat_master_group WHERE chat_group_master.group_id = chat_master_group.group_id AND chat_master_group.msg_by_name != '') AS total_msg,
            (SELECT unread_count FROM chat_group_unread_message WHERE chat_group_master.group_id = chat_group_unread_message.group_id AND chat_group_unread_message.user_id = '$user_id') AS my_read_msg","employee_level_master,users_master,chat_group_master","chat_group_master.society_id='$society_id' AND chat_group_master.group_type = 4 AND employee_level_master.level_id = chat_group_master.group_common_id AND employee_level_master.level_id = '$level_id' AND users_master.level_id = employee_level_master.level_id AND users_master.level_id = '$level_id' AND users_master.user_id = '$user_id' AND chat_group_master.active_status = 0","");

        $array1 = array();
        $array2 = array();
        $array3 = array();
        $array4 = array();
        $array5 = array();

        if(mysqli_num_rows($q1)>0){

            while($data1=mysqli_fetch_array($q1)) {
                array_push($array1, $data1);
            }
        }

        if(mysqli_num_rows($q2)>0){

            while($data2=mysqli_fetch_array($q2)) {
                array_push($array2, $data2);
            }
        }

        if(mysqli_num_rows($q3)>0){

            while($data3=mysqli_fetch_array($q3)) {
                array_push($array3, $data3);
            }
        }

        if(mysqli_num_rows($q4)>0){

            while($data4=mysqli_fetch_array($q4)) {
                array_push($array4, $data4);
            }
        }

        if(mysqli_num_rows($q5)>0){

            while($data5=mysqli_fetch_array($q5)) {
                array_push($array5, $data5);
            }
        }

        $newArray = array_merge($array1,$array2,$array3,$array4,$array5);
        foreach ($newArray as $key => $part) {
            $sort[$key] = $part['group_id'];
        }
        
        array_multisort($sort, SORT_DESC, $newArray);

        /* print_r($newArray);
         exit();
*/
        $response["member"]=array();


        if (count($newArray)>0) {

            for ($i=0; $i <count($newArray) ; $i++) { 
              
            /*$userIdCheck =$newArray[$i]['user_id'];
            if ($userIdCheck!='') {*/

               /* $qf1=$d->select("chat_group_member_master","group_id='$newArray[$i][group_id]' AND society_id='$society_id' AND  user_id='$user_id'");
                $unData=mysqli_fetch_array($qf1);

                $qf2=$d->select("chat_master_group","group_id='$newArray[$i][group_id]' AND society_id='$society_id' ","ORDER BY chat_id DESC ");
                $unData11=mysqli_fetch_array($qf2);*/


                $member = array(); 
                $member["chat_id"]=$newArray[$i]['group_id'];
                $member["society_id"]=$newArray[$i]['society_id'];
                $member["topic_name"]=$society_id.'group'.$newArray[$i]['group_id'];
                $member["msg_data"]=$newArray[$i]['last_msg'].'';
                $member["group_type"]=$newArray[$i]['group_type'].'';
                 // if ($unData11['msg_data']=='' && $unData11['msg_img']!='') {
                        if ($newArray[$i]['last_msg_type']==1) {
                            $member["msg_data"]="📸 Image";
                        } else if ($newArray[$i]['last_msg_type']==2) {
                            $member["msg_data"]="📃 Document";
                        }else if ($newArray[$i]['last_msg_type']==3) {
                           $member["msg_data"]="🎧 Audio";
                        }else if ($newArray[$i]['last_msg_type']==4) {
                           $member["msg_data"]="📌 Location";
                        }else if ($newArray[$i]['last_msg_type']==5) {
                           $member["msg_data"]="👤 Contact";
                        }else if ($newArray[$i]['last_msg_type']=='6') {
                            $member["msg_data"]="🎞️ Video";
                        }else {
                             $member["msg_data"]=$newArray[$i]['last_msg'].'';
                        }
                    // }else {
                    //     $member["msg_data"]=$unData11['msg_data'].'';
                    // }
                $group_id = $newArray[$i]['group_id'];
                $group_type = $newArray[$i]['group_type'];
                $total_msg = $newArray[$i]['total_msg'];
                $my_read_msg = $newArray[$i]['my_read_msg'];
                $chatCount = $total_msg - $my_read_msg;

                if ($chatCount < 0) {
                    $chatCount = "0";
                }

                $member["msg_date"]=$newArray[$i]['last_msg_date'].'';
                $member["flag"]="2";
                $member["member_size"]='0';
                $member["user_id"]=$newArray[$i]['created_by'];
                if ($group_type == 1) {
                    $member["user_full_name"]=$newArray[$i]['block_name'].' Branch';
                    $member["user_first_name"]=$newArray[$i]['block_name'].' Branch';
                    $member["user_last_name"]=$newArray[$i]['block_name'].' Branch';
                }else if ($group_type == 2) {
                    $member["user_full_name"]=$newArray[$i]['floor_name'].' Department';
                    $member["user_first_name"]=$newArray[$i]['floor_name'].' Department';
                    $member["user_last_name"]=$newArray[$i]['floor_name'].' Department';
                }else if ($group_type == 3) {
                    $member["user_full_name"]=$newArray[$i]['zone_name'].' Zone';
                    $member["user_first_name"]=$newArray[$i]['zone_name'].' Zone';
                    $member["user_last_name"]=$newArray[$i]['zone_name'].' Zone';
                }else if ($group_type == 4) {
                    $member["user_full_name"]=$newArray[$i]['level_name'].' Level';
                    $member["user_first_name"]=$newArray[$i]['level_name'].' Level';
                    $member["user_last_name"]=$newArray[$i]['level_name'].' Level';
                }else{
                    $member["user_full_name"]=$newArray[$i]['group_name'];
                    $member["user_first_name"]=$newArray[$i]['group_name'];
                    $member["user_last_name"]=$newArray[$i]['group_name'];
                }
                
                $member["gender"]="";
                $member["user_type"]="group";
                $member["user_profile_pic"]=$base_url."img/users/".$newArray[$i]['group_icon'];

                $member["block_name"]="group";
                $member["floor_name"]="";
                $member["unit_name"]="";
                $member["floor_id"]="";
                $member["unit_id"]="";
                $member["unit_status"]="";
                $member["user_status"]="";
                $member["member_status"]="";
                $member["user_mobile"]="";
                $member["public_mobile"]="1";
                $member["member_date_of_birth"]="";
                $member["alt_mobile"] = "";
               
                $member["chatCount"] = $chatCount.'';

                

                
                    array_push($response["member"], $member); 
                }
            //}

        }


     


            $response["message"]="Chat List";
            $response["group_chat_status"] = $group_chat_status;
            $response["status"]="200";
            echo json_encode($response);

    }else{
        $response["message"]="wrong tag.";
        $response["group_chat_status"] = $group_chat_status;
        $response["status"]="201";
        echo json_encode($response);

    }

    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}
