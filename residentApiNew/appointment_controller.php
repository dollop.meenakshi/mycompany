<?php
include_once 'lib.php';


/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST) && !empty($_POST)) {

	if ($key == $keydb && $auth_check=='true') {

		$response = array();
		extract(array_map("test_input", $_POST));

		if($_POST['addAppointment']=="addAppointment" &&  filter_var($society_id, FILTER_VALIDATE_INT) == true ){ 

			
            $m->set_data('society_id',$society_id);
            $m->set_data('appointment_by_user_id',$appointment_by_user_id);
            $m->set_data('appointment_for_user_id',$appointment_for_user_id);
            $m->set_data('appointment_datetime',$appointment_datetime);
            $m->set_data('appointment_place',$appointment_place);
            $m->set_data('appointment_agenda',$appointment_agenda);
            $m->set_data('appointment_created_date',date('Y-m-d H:i:s'));

            $a1= array (
                'society_id'=> $m->get_data('society_id'),
                'appointment_by_user_id'=> $m->get_data('appointment_by_user_id'),
                'appointment_for_user_id'=> $m->get_data('appointment_for_user_id'),
                'appointment_datetime'=> $m->get_data('appointment_datetime'),
                'appointment_place'=> $m->get_data('appointment_place'),
                'appointment_agenda'=> $m->get_data('appointment_agenda'),
                'appointment_created_date'=> $m->get_data('appointment_created_date'),
            );
           
            $q=$d->insert("appointment_master",$a1);

            $appointmentId = $con->insert_id;
                
            if ($q>0) {

            	$title = "New Appointment Request";
                $description = "By $user_name\nAt ".date('h:i A, d M Y',strtotime($appointment_datetime))."\n".
                "Place: $appointment_place\nAgenda: $appointment_agenda";


            	$qsm = $d->selectRow("user_designation,device,user_token,user_profile_pic,user_full_name","users_master", "society_id='$society_id' AND user_id='$appointment_for_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $qsmReq = $d->selectRow("user_profile_pic","users_master", "society_id='$society_id' AND user_id='$appointment_by_user_id'");
                $data_notification1 = mysqli_fetch_array($qsmReq);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $profile = $base_url . "img/users/recident_profile/". $data_notification1['user_profile_pic'];
                $dateTime = date('h:i A, dS M Y',strtotime($appointment_datetime));

                $dataAry = array(
                    "appointmentId"=>$appointmentId,
                    "name"=>$user_name,
                    "time"=>$dateTime,
                    "profile"=>$profile,
                    "designation"=>$user_designation,
                    "place"=>$appointment_place,
                    "reason"=>$appointment_agenda,
                    "appointment_by_user_id"=>$appointment_by_user_id,
                    "appointment_for_user_id"=>$appointment_for_user_id,
                    
                );

                $dataJson = json_encode($dataAry);

                if ($device == 'android') {
                    $nResident->noti("new_appointment","",$society_id,$user_token,$title,$description,$dataJson);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("new_appointment","",$society_id,$user_token,$title,$description,$dataJson);
                }

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"new_appointment","quick_appointment.png",$dataJson,"users_master.user_id IN ('$appointment_for_user_id')");


                
                $response["message"] = "Appointment request sent";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong! please try again";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
        }else if ($_POST['getAppointments']=="getAppointments"  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$appointmentQry = $d->selectRow("appointment_master.*,users_master.user_id,users_master.country_code,users_master.user_mobile,users_master.user_full_name,users_master.user_designation,users_master.user_profile_pic,block_master.block_name,floors_master.floor_name","appointment_master ,users_master,block_master,floors_master","
				appointment_master.society_id = '$society_id' 
				AND appointment_master.appointment_for_user_id = '$user_id'
                AND appointment_master.appointment_status = '0'
				AND appointment_master.appointment_by_user_id = users_master.user_id
				AND users_master.block_id = block_master.block_id 
				AND users_master.floor_id = floors_master.floor_id
				AND block_master.block_id = floors_master.block_id","ORDER BY appointment_master.appointment_datetime DESC");


			if (mysqli_num_rows($appointmentQry) > 0) {
			
				$response["my_appointments"] = array();			

				while($data = mysqli_fetch_array($appointmentQry)){
					$appointment = array();

					$appointment['appointment_id'] = $data['appointment_id'];
					$appointment['user_id'] = $data['user_id'];
					$appointment['user_full_name'] = $data['user_full_name'];
					$appointment['user_mobile'] =$data['country_code']. ' ' . $data['user_mobile'];
					$appointment['branch_name'] = $data['block_name'];
					$appointment['department_name'] = $data['floor_name'];
					$appointment['user_designation'] = $data['user_designation'];
					$appointment['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $data['user_profile_pic'];
					$appointment['appointment_place'] = $data['appointment_place'];
					$appointment['appointment_agenda'] = $data['appointment_agenda'];
					$appointment['appointment_datetime'] = date('h:i A, dS M Y',strtotime($data['appointment_datetime']));

                    $todayDate = date('Y-m-d');
                    $appointmentDate = date('Y-m-d',strtotime($data['appointment_datetime']));

                    if ($todayDate > $appointmentDate && $data["appointment_status"] != 2) {
                        $m->set_data('appointment_status',"2");
                        $m->set_data('appointment_reject_reason',"Auto Rejected");

                        $a = array(
                            'appointment_status'=>$m->get_data('appointment_status'),
                            'appointment_reject_reason'=>$m->get_data('appointment_reject_reason'),
                        );

                        $qry = $d->update("appointment_master",$a,"appointment_id = '$data[appointment_id]'");
                    }else{                        
    					array_push($response["my_appointments"],$appointment);
                    }

				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data found";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if ($_POST['getMyAppointments']=="getMyAppointments"  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            $appointmentQry = $d->selectRow("appointment_master.*,users_master.user_id,users_master.country_code,users_master.user_mobile,users_master.user_full_name,users_master.user_designation,users_master.user_profile_pic,block_master.block_name,floors_master.floor_name","appointment_master ,users_master,block_master,floors_master","
                appointment_master.society_id = '$society_id' 
                AND appointment_master.appointment_by_user_id = '$user_id'
                AND appointment_master.appointment_for_user_id = users_master.user_id
                AND users_master.block_id = block_master.block_id 
                AND users_master.floor_id = floors_master.floor_id
                AND block_master.block_id = floors_master.block_id","ORDER BY appointment_master.appointment_status ASC,appointment_master.appointment_datetime ASC");


            if (mysqli_num_rows($appointmentQry) > 0) {
            
                $response["my_appointments"] = array();         

                while($data = mysqli_fetch_array($appointmentQry)){
                    $appointment = array();

                    $appointment['appointment_id'] = $data['appointment_id'];
                    $appointment['user_id'] = $data['user_id'];
                    $appointment['user_full_name'] = $data['user_full_name'];
                    $appointment['user_mobile'] =$data['country_code']. ' ' . $data['user_mobile'];
                    $appointment['branch_name'] = $data['block_name'];
                    $appointment['department_name'] = $data['floor_name'];
                    $appointment['user_designation'] = $data['user_designation'];
                    $appointment['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $data['user_profile_pic'];
                    $appointment['appointment_place'] = $data['appointment_place'];
                    $appointment['appointment_agenda'] = $data['appointment_agenda'];
                    $appointment['appointment_reject_reason'] = $data['appointment_reject_reason'];
                    $appointment['appointment_status'] = $data['appointment_status'];
                    $appointment['appointment_datetime'] = date('h:i A, dS M Y',strtotime($data['appointment_datetime']));
                    $appointment['appointment_created_date'] = date('h:i A, dS M Y',strtotime($data['appointment_created_date']));

                    if ($data["appointment_status"] == 0) {
                        $appointment["appointment_status_view"] = "Pending";
                    }else if ($data["appointment_status"] == 1) {
                        $appointment["appointment_status_view"] = "Approved";
                    }else if ($data["appointment_status"] == 2) {
                        $appointment["appointment_status_view"] = "Rejected";
                    }

                    $todayDate = date('Y-m-d');
                    $appointmentDate = date('Y-m-d',strtotime($data['appointment_datetime']));

                    if ($todayDate > $appointmentDate && $data["appointment_status"] != 2) {
                        $m->set_data('appointment_status',"2");
                        $m->set_data('appointment_reject_reason',"Auto Rejected");

                        $a = array(
                            'appointment_status'=>$m->get_data('appointment_status'),
                            'appointment_reject_reason'=>$m->get_data('appointment_reject_reason'),
                        );

                        $qry = $d->update("appointment_master",$a,"appointment_id = '$data[appointment_id]'");
                    }
                    
                    array_push($response["my_appointments"],$appointment);
                    
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['approveAppointment']=="approveAppointment" && $user_id!='' && $appointment_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			$m->set_data('appointment_status',"1");

            $a = array(
                'appointment_status'=>$m->get_data('appointment_status'),
            );

            $qry = $d->update("appointment_master",$a,"appointment_id = '$appointment_id' AND appointment_for_user_id = '$user_id'");

            if ($qry == true) {

	            $title = "Appointment has been approved";
                $description = "By $user_name";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$appointment_by_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("appointment","",$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("appointment","",$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"appointment","quick_appointment.png","user_id = '$appointment_by_user_id'",$appointment_by_user_id);

	            $response["message"] = "Appointment has been approved";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "Something went wrong!";
				$response["status"] = "201";
				echo json_encode($response);
            }
                      
        }else if($_POST['rejectAppointment']=="rejectAppointment" && $user_id!='' && $appointment_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			$m->set_data('appointment_status',"2");
            $m->set_data('appointment_reject_reason',$appointment_reject_reason);

            $a = array(
                'appointment_status'=>$m->get_data('appointment_status'),
                'appointment_reject_reason'=>$m->get_data('appointment_reject_reason'),
            );

            $qry = $d->update("appointment_master",$a,"appointment_id = '$appointment_id' AND appointment_for_user_id = '$user_id'");

            if ($qry == true) {

	            $title = "Appointment has been rejected";
                $description = "By $user_name\nReason: $appointment_reject_reason";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$appointment_by_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("appointment","",$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("appointment","",$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"appointment","quick_appointment.png","user_id = '$appointment_by_user_id'",$appointment_by_user_id);

	            $response["message"] = "Appointment has been rejected";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "Something went wrong!";
				$response["status"] = "201";
				echo json_encode($response);
            }
                      
        }else if($_POST['sendAppointmentReminder']=="sendAppointmentReminder" && $appointment_for_user_id!='' && $appointment_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $title = "Appointment Reminder";
            $description = "By $user_name\nTime: $appointment_time";

            $qsm = $d->selectRow("user_designation,device,user_token,user_profile_pic,user_full_name","users_master", "society_id='$society_id' AND user_id='$appointment_for_user_id'");
            $data_notification = mysqli_fetch_array($qsm);

            $qsmReq = $d->selectRow("user_profile_pic","users_master", "society_id='$society_id' AND user_id='$user_id'");
            $data_notification1 = mysqli_fetch_array($qsmReq);

            $user_token = $data_notification['user_token'];
            $device = $data_notification['device'];
            $user_designation = $data_notification['user_designation'];

            $profile = $base_url . "img/users/recident_profile/". $data_notification1['user_profile_pic'];

            $dataAry = array(
                "appointmentId"=>$appointment_id,
                "name"=>$user_name,
                "time"=>$appointment_time,
                "profile"=>$profile,
                "designation"=>$user_designation,
                "place"=>htmlentities(str_replace("'","",$appointment_place)),
                "reason"=>htmlentities(str_replace("'","",$appointment_agenda)),
                "appointment_by_user_id"=>$user_id,
                "appointment_for_user_id"=>$appointment_for_user_id,
                
            );

            $dataJson = json_encode($dataAry);

            if ($device == 'android') {
                $nResident->noti("new_appointment","",$society_id,$user_token,$title,$description,$dataJson);
            } else if ($device == 'ios') {
                $nResident->noti_ios("new_appointment","",$society_id,$user_token,$title,$description,$dataJson);
            }

            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"new_appointment","quick_appointment.png",$dataJson,"users_master.user_id IN ('$appointment_for_user_id')");

            $response["message"] = "Appointment Reminder Sent";
            $response["status"] = "200";
            echo json_encode($response);
                 
        }else if($_POST['deleteAppointment']=="deleteAppointment" && $appointment_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $qry = $d->delete("appointment_master","appointment_id = '$appointment_id'");

            if ($qry == true) {
                $response["message"] = "Appointment has been removed";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }
                      
        }else {
			$response["message"] = "wrong tag.";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {

		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}
?>