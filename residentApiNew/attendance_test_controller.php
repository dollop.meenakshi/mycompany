<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        $startDate=date("Y-m-01");
        $currentDate=date("Y-m-d");
        $lastDate=date("Y-m-t");

        if($_POST['getAttendanceType']=="getAttendanceType" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $attendance_date_start = date('Y-m-d');
            $dateTimeCurrent = date('Y-m-d H:i:s');

            $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND shift_time_id = '$shift_time_id' AND attendance_date_start = '$attendance_date_start' AND attendance_date_end = '0000-00-00' AND punch_out_time = '00:00:00'","ORDER BY attendance_id DESC LIMIT 1");

            if (mysqli_num_rows($getTodayAttendace) > 0) {

                $attData = mysqli_fetch_array($getTodayAttendace);

                if ($attendance_id == null || $attendance_id=="" || $attendance_id == "0") {
                    $attendance_id = $attData["attendance_id"];
                }


                $attStartDate = $attData["attendance_date_start"];
                $punchInTime = $attData["punch_in_time"];

                $pDate = $attStartDate." ".$punchInTime;

                $inTimeAMPM = date('Y-m-d H:i:s',strtotime($pDate));
                $dateTimeCurrent = date('Y-m-d H:i:s',strtotime($dateTimeCurrent));

                $date_a = new DateTime($inTimeAMPM);
                $date_b = new DateTime($dateTimeCurrent);

                $interval = $date_a->diff($date_b);
               
                $hours = $interval->format('%h');
                $minutes = $interval->format('%i');
                $sec = $interval->format('%s');

                if ($sec < 45) {
                   $sec += 5;
                }

                $finalTime =  $attStartDate." ".sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

                $time_diff = sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);
                
                $response['attendance_id'] =$attData["attendance_id"];
                $response['punch_in_time'] =$attData["punch_in_time"];
                $response['date_time'] =$finalTime;
                $response['time_diff'] =$time_diff;
            }else{
                $response['attendance_id'] ="0";
            }

            $getBreakTime = $d->select("attendance_break_history_master","attendance_id = '$attendance_id' AND break_start_date='$attendance_date_start' AND break_out_time = '00:00:00' AND break_end_date = '0000-00-00'","ORDER BY attendance_break_history_id DESC LIMIT 1");

            if (mysqli_num_rows($getBreakTime) > 0) {

                $breakData = mysqli_fetch_array($getBreakTime);

                $breakStartDate = $breakData["break_start_date"];
                $breakInTime = $breakData["break_in_time"];

                $date_a1 = new DateTime($breakStartDate." ".$breakInTime);
                $date_b1 = new DateTime($dateTimeCurrent);

                $interval1 = $date_a1->diff($date_b1);

                $hours1 = $interval1->format('%h');
                $minutes1 = $interval1->format('%i');
                $sec1 = $interval1->format('%s');

                if ($sec1 < 45) {
                    $sec1 += 5;
                }

                $break_date_time =  $breakStartDate." ".sprintf('%02d:%02d:%02d', $hours1, $minutes1,$sec1);

                $break_time_diff = sprintf('%02d:%02d:%02d', $hours1, $minutes1,$sec1);
                
                $response['attendance_break_history_id'] =$breakData["attendance_break_history_id"];
                $response['break_in_time'] =$breakData["break_in_time"];
                $response['attendance_type_id'] =$breakData["attendance_type_id"];
                $response['break_date_time'] =$break_date_time;
                $response['break_time_diff'] =$break_time_diff;
            }else{
                $response['attendance_break_history_id'] ="0";
            }

            $getBreakTimeTypeIds = $d->select("attendance_break_history_master","attendance_id = '$attendance_id' AND break_start_date = '$attendance_date_start' AND break_out_time != '00:00:00' AND break_end_date != '0000-00-00'");

            $attTypeIdArray="";

            if (mysqli_num_rows($getBreakTimeTypeIds)) {
                while($typeData = mysqli_fetch_array($getBreakTimeTypeIds)){

                    $qry = $d->select("attendance_type_master","society_id='$society_id' AND attendance_type_id = '$typeData[attendance_type_id]' AND is_multipletime_use = '0'");

                    if (mysqli_num_rows($qry) > 0) {
                        $attTypeIdArray = $attTypeIdArray."'".$typeData["attendance_type_id"]."',";
                    }
                }
            }

            $attTypeIdArray = rtrim($attTypeIdArray, ",");

            if ($attTypeIdArray!="") {
                $attendanceTypeQry = $d->select("attendance_type_master","society_id='$society_id'  AND attendance_type_active_status = '0' AND attendance_type_delete = '0' AND attendance_type_id NOT IN ($attTypeIdArray)");
            }else{
                $attendanceTypeQry = $d->select("attendance_type_master","society_id='$society_id' AND attendance_type_active_status = '0' AND attendance_type_delete = '0'");
            }            

            if(mysqli_num_rows($attendanceTypeQry)>0){
                
                $response["attendance_types"] = array();

                while($leaveTypeData=mysqli_fetch_array($attendanceTypeQry)) {

                    $attendance_type = array(); 

                    $attendance_type["attendance_type_id"] = $leaveTypeData['attendance_type_id'];
                    $attendance_type["attendance_type_name"] = $leaveTypeData['attendance_type_name'];

                    array_push($response["attendance_types"], $attendance_type);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['attendancePunchIn']=="attendancePunchIn" && $user_id!='' && $shift_time_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND unit_id = '$unit_id' AND shift_time_id = '$shift_time_id' AND attendance_date_start = '$currentDate'");

            if (mysqli_num_rows($isPunchIn) > 0) {
                $response["message"] = "You already Punched In for today";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $m->set_data('shift_time_id',$shift_time_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('attendance_date_start',date('Y-m-d'));
            $m->set_data('punch_in_time',date('H:i:s'));
            $m->set_data('punch_in_latitude',$punch_in_latitude);
            $m->set_data('punch_in_longitude',$punch_in_longitude);
            $m->set_data('attendance_reason',$attendance_reason);
            $m->set_data('attendance_status',$attendance_status);

            $a = array(
                'shift_time_id'=>$m->get_data('shift_time_id'),
                'society_id' =>$m->get_data('society_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'user_id'=>$m->get_data('user_id'),
                'attendance_date_start'=>$m->get_data('attendance_date_start'),
                'punch_in_time'=>$m->get_data('punch_in_time'),
                'punch_in_latitude'=>$m->get_data('punch_in_latitude'),
                'punch_in_longitude'=>$m->get_data('punch_in_longitude'),
                'attendance_reason'=>$m->get_data('attendance_reason'),
                'attendance_status'=>$m->get_data('attendance_status'),
            );

            $punchInQry = $d->insert("attendance_master",$a);

            $attendance_id = $con->insert_id;

            if ($punchInQry == true) {
                $response["message"] = "Success";
                $response["attendance_id"] = $attendance_id."";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['attendancePunchOut']=="attendancePunchOut" && $attendance_id!='' && $user_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_date_end',date('Y-m-d'));
            $m->set_data('punch_out_time',date('H:i:s'));
            $m->set_data('work_report_title',$work_report_title);
            $m->set_data('work_report',$work_report);
            $m->set_data('punch_out_latitude',$punch_out_latitude);
            $m->set_data('punch_out_longitude',$punch_out_longitude);

            $a = array(
                'attendance_date_end'=>$m->get_data('attendance_date_end'),
                'punch_out_time'=>$m->get_data('punch_out_time'),
                'work_report_title'=>$m->get_data('work_report_title'),
                'work_report'=>$m->get_data('work_report'),
                'punch_out_latitude'=>$m->get_data('punch_out_latitude'),
                'punch_out_longitude'=>$m->get_data('punch_out_longitude'),
            );

            $punchOutQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

            if ($punchOutQry == true) {

                $attHisQry = $d->selectRow("attendance_break_history_id,break_end_date,break_out_time","attendance_break_history_master","attendance_id = '$attendance_id' AND break_end_date = '0000-00-00' AND break_out_time = '00:00:00'");

                if (mysqli_num_rows($attHisQry) > 0) {
                    while($attData = mysqli_fetch_array($attHisQry)){
                        $attendance_break_history_id = $attData['attendance_break_history_id'];
                        $break_end_date = date('Y-m-d');
                        $break_out_time = date('H:i:s');

                        $m->set_data('break_end_date',$break_end_date);
                        $m->set_data('break_out_time',$break_out_time);

                        $a = array(
                            'break_end_date'=>$m->get_data('break_end_date'),
                            'break_out_time'=>$m->get_data('break_out_time'),
                        );

                        $qry = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id' AND attendance_id = '$attendance_id'");
                    }
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['breakIn']=="breakIn" && $attendance_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_id',$attendance_id);
            $m->set_data('attendance_type_id',$attendance_type_id);
            $m->set_data('break_start_date',date('Y-m-d'));
            $m->set_data('break_in_time',date('H:i:s'));
            $m->set_data('break_in_latitude',$break_in_latitude);
            $m->set_data('break_in_longitude',$break_in_longitude);

            $a = array(
                'attendance_id'=>$m->get_data('attendance_id'),
                'attendance_type_id' =>$m->get_data('attendance_type_id'),
                'break_start_date' =>$m->get_data('break_start_date'),
                'break_in_time'=>$m->get_data('break_in_time'),
                'break_in_latitude'=>$m->get_data('break_in_latitude'),
                'break_in_longitude'=>$m->get_data('break_in_longitude')
            );

            $punchInQry = $d->insert("attendance_break_history_master",$a);

            $attendance_break_history_id = $con->insert_id;

            if ($punchInQry == true) {
                $response["message"] = "Success";
                $response["attendance_break_history_id"] = $attendance_break_history_id."";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['breakOut']=="breakOut" && $attendance_break_history_id!='' && $user_id!='' && filter_var($attendance_break_history_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('break_end_date',date('Y-m-d'));
            $m->set_data('break_out_time',date('H:i:s'));
            $m->set_data('break_out_latitude',$break_out_latitude);
            $m->set_data('break_out_longitude',$break_out_longitude);

            $a = array(
                'break_end_date'=>$m->get_data('break_end_date'),
                'break_out_time'=>$m->get_data('break_out_time'),
                'break_out_latitude'=>$m->get_data('break_out_latitude'),
                'break_out_longitude'=>$m->get_data('break_out_longitude'),
            );

            $breakOut = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id'");

            if ($breakOut == true) {
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['checkRadius']=="checkRadius" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $isRadiusON = false;

            $userQry = $d->select("users_master","user_id = '$user_id' AND society_id = '$society_id' AND user_geofence_latitude != '' AND user_geofence_longitude != '' AND user_geofence_range != ''");

            if (mysqli_num_rows($userQry) == 0) {

                $blockQry = $d->select("block_master","society_id = '$society_id' AND block_geofence_latitude != '' AND block_geofence_longitude != '' AND block_geofence_longitude != ''");

                if (mysqli_num_rows($blockQry) > 0) {

                    $isRadiusON = true;

                    $data = mysqli_fetch_array($blockQry);

                    $geofence_latitude = $data['block_geofence_latitude'];
                    $geofence_longitude = $data['block_geofence_longitude'];
                    $geofence_range = $data['block_geofence_range'];
                }else{
                    $isRadiusON = false;
                }
            }else{

                $isRadiusON = true;

                $data = mysqli_fetch_array($userQry);

                $geofence_latitude = $data['user_geofence_latitude'];
                $geofence_longitude = $data['user_geofence_longitude'];
                $geofence_range = $data['user_geofence_range'];
            }

            if ($isRadiusON) {

                $radiusInMeeter=  $d->haversineGreatCircleDistance($user_latitude,$user_longitude,$geofence_latitude, $geofence_longitude);

                $totalKm= number_format($radiusInMeeter,2,'.','');

                if ($user_latitude=="0.0" || $user_latitude=="") {
                    $response['take_punch_in_reason'] = true;
                    $response["message"] = "Invalid Location";
                    $response["status"] = "201";
                    echo json_encode($response);
                }else if($totalKm <= $geofence_range){
                    $response['take_punch_in_reason'] = false;
                    $response["message"] = "You are in range";
                    $response["status"] = "200";
                    echo json_encode($response);
                }else{
                    $response['take_punch_in_reason'] = true;
                    $response["message"] = "You are out of range";
                    $response["status"] = "201";
                    echo json_encode($response);
                }
            }else{
                $response['take_punch_in_reason'] = false;
                $response["message"] = "No Radius Checking Required";
                $response["status"] = "201";
                echo json_encode($response);
            }             
        }else if($_POST['getWeeklyAttendanceHistory']=="getWeeklyAttendanceHistory" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shift_time_id = $sdata['shift_time_id'];
            }

            // Total Week of Month

            $textdt=date("01-m-Y");
            $currdt= strtotime($textdt);
            $nextmonth=strtotime($textdt."+1 month");
            $i=0;
            $flag=true;

            $response["weekly_histrory"] = array();
            $response["week_position"] = "0";

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];

            $parts = explode(':', $perdayHours);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);

                     
            // Week
        
            for ($x = 0; $x < count($startarr); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";
                $weekArray["total_spend_time"] = "0";

                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);

                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $days = array();
                    $days['day'] = $date->format('l');
                    $days['date'] = $dayDate = $date->format('Y-m-d');
                    $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate)) ;

                    if ($dayDate == date('Y-m-d')) {
                        $response["week_position"] = $x."";
                    }
                    
                    $holidayQry = $d->selectRow("holiday_start_date","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' ");

                    $x1 = $x+1;
                    // holiday off
                    if (mysqli_num_rows($holidayQry)>0) {
                        $minusHoliday += $perdayHours;
                    } else {
                        // full day off
                        if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                            $minusWeekOffHR += $perdayHours;
                        }
                        // alternate week day off
                        if ($shiftData['has_altenate_week_off'] == "1") {  
                            if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                in_array($x1,$alternateWeekOff)) {
                                $minusAlternateWeekOffDays += $perdayHours;
                            }
                        }
                    }
                    array_push($daysArray['days'],$days);
                }

                $totalWeekDay = count($daysArray['days']);
                
                $totalWeekHour = $perdayHours * $totalWeekDay;
                $totalWeekHour1 = ($totalWeekHour - $minusWeekOffHR) - $minusAlternateWeekOffDays;
                $totalWeekHour = $totalWeekHour1 - $minusHoliday;
                $weekArray['total_hours'] = $totalWeekHour."";
                
                $weekArray['week'] = date('d',strtotime($sDate))."-".date('d',strtotime($eDate))." ".date('F',strtotime($sDate))." ".date('Y',strtotime($sDate));

                // Week History

                $tdate = date('Y-m-d');

                $attendanceQry = $d->select("attendance_master,users_master","(attendance_master.society_id='$society_id' 
                    AND attendance_master.user_id = users_master.user_id 
                    AND attendance_master.user_id = '$user_id'
                    AND attendance_master.attendance_date_start != '$tdate'
                    AND attendance_master.attendance_date_start BETWEEN '$sDate' AND '$eDate') OR (
                    attendance_master.society_id='$society_id' 
                    AND attendance_master.user_id = users_master.user_id 
                    AND attendance_master.user_id = '$user_id'
                    AND attendance_master.attendance_date_start = '$tdate'
                    AND attendance_master.punch_out_time != '00:00:00'
                    AND attendance_master.attendance_date_start BETWEEN '$sDate' AND '$eDate')","ORDER BY attendance_master.attendance_id ASC");

                $weekArray["history"] = array();

                if(mysqli_num_rows($attendanceQry)>0){

                    $times = array();

                    while($data=mysqli_fetch_array($attendanceQry)) {

                        $attendanceHistory = array(); 

                        $attendanceHistory["attendance_id"] = $data['attendance_id'];
                        $attendanceHistory["day_name"] = date('l', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_start"] = date('d F Y', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_end"] = date('d F Y', strtotime($data['attendance_date_end']));

                        $attendanceDateStart = $data['attendance_date_start'];
                        $attendanceDateEnd = $data['attendance_date_end'];
                        
                        $punchInTime = $data['punch_in_time'];
                        $punchOutTime = $data['punch_out_time'];
                        
                        $attendanceHistory["punch_in_time"] = date("h:i A",strtotime($punchInTime));
                        $attendanceHistory["punch_out_time"] = date("h:i A",strtotime($punchOutTime));

                        if ($attendanceDateEnd == "0000-00-00") {
                            $attendanceHistory["attendance_date_end"] = "";
                            $attendanceHistory["punch_out_time"] = "";
                            $attendanceDateEnd = date('Y-m-d');
                            $punchOutTime = date('H:i:s');
                        }

                        if ($data['punch_out_time'] != "00:00:00" && $data['attendance_date_end'] != "0000-00-00") {
                        
                            $totalHours = getTotalHours($attendanceDateStart,$attendanceDateEnd,$punchInTime,$punchOutTime);
                            
                            $totalHoursName = getTotalHoursWithNames($attendanceDateStart,$attendanceDateEnd,$punchInTime,$punchOutTime);

                            $times[] = $totalHours;

                            $attendanceHistory["total_hours"] = $totalHoursName;
                            $attendanceHistory["is_punch_out_missing"] = false;
                            $attendanceHistory["punch_out_missing_message"] = "";

                            $weekArray['total_spend_time'] = getTotalWeekHours($times);
                            $weekArray['total_spend_time_name'] = getTotalWeekHoursName($times);
                        }else{
                            $attendanceHistory["is_punch_out_missing"] = true;
                            $attendanceHistory["total_hours"] = "";
                            $attendanceHistory["punch_out_missing_message"] = "Punch Out Missing";
                        }

                        array_push($weekArray["history"], $attendanceHistory);
                    }
                }

                array_push($response['weekly_histrory'],$weekArray);
            } 
            
            echo json_encode($response);
        
        }else if($_POST['getMonthlyAttendanceHistory']=="getMonthlyAttendanceHistory" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shift_time_id = $sdata['shift_time_id'];
            }

            $response['monthly_history'] = array();
            //$response['week'] = array();

            $datesArrayOfMonth = range_date($month_start_date,$month_end_date);

            $response['month'] = date('F Y',strtotime($month_start_date));

            $holidayAry = array();
            $weekAry = array();


            $currdt= strtotime($month_start_date);
            $nextmonth=strtotime($month_start_date."+1 month");
            $i=0;
            $flag=true;

            $totalMonthHours = 0;

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shift_start_time = $shiftData['shift_start_time'];
            $shift_end_time = $shiftData['shift_end_time'];
            $lunch_break_start_time = $shiftData['lunch_break_start_time'];
            $lunch_break_end_time = $shiftData['lunch_break_end_time'];
            $tea_break_start_time = $shiftData['tea_break_start_time'];
            $tea_break_end_time = $shiftData['tea_break_end_time'];
            $half_day_time_start = $shiftData['half_day_time_start'];
            $late_time_start = $shiftData['late_time_start'];
            $early_out_time = $shiftData['early_out_time'];

            $parts = explode(':', $perdayHours);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);


            // Week
        
            for ($x = 0; $x < count($startarr); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";


                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);


                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $dayN= $date->format('l');
                    $dayDate = $date->format('Y-m-d');

                        // code...
                        $days = array();
                        $hDates = array();
                        $days['day'] = $dayN;
                        $days['date'] = $dayDate;
                        $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate)) ;
                        
                        $holidayQry = $d->selectRow("holiday_start_date,holiday_name","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' ");

                        $holidayData = mysqli_fetch_array($holidayQry);

                        $x1 = $x+1;
                        // holiday off
                        if (mysqli_num_rows($holidayQry)>0) {
                            array_push($holidayAry,$dayDate);
                            $minusHoliday += $perdayHours;
                        } else {
                            // full day off
                            if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                                array_push($weekAry,$dayDate);
                                $minusWeekOffHR += $perdayHours;
                            }

                            // alternate week day off
                            if ($shiftData['has_altenate_week_off'] == "1") {  
                                if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                    in_array($x1,$alternateWeekOff)) {
                                    array_push($weekAry,$dayDate);
                                    $minusAlternateWeekOffDays += $perdayHours;
                                }
                            }
                        }
                        array_push($daysArray['days'],$days);
                }

                $totalWeekDay = count($daysArray['days']);
                
                $totalWeekHour = $perdayHours * $totalWeekDay;
                $totalWeekHour1 = ($totalWeekHour - $minusWeekOffHR) - $minusAlternateWeekOffDays;
                $totalWeekHour = $totalWeekHour1 - $minusHoliday;
                $weekArray['total_hours'] = $totalWeekHour."";

                $totalMonthHours += $totalWeekHour."";
                $weekArray['totalMonthHours'] = $totalMonthHours."";

                //array_push($response['week'],$weekArray);
            }

            $response['total_month_hours'] = $totalMonthHours."";

            // Leave Data

            $leaveQry = $d->select("leave_master","society_id = '$society_id'
                    AND user_id = '$user_id'
                    AND unit_id = '$unit_id'
                    AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'
                    AND leave_status = '1'");

            $leaveArray = array();
            $halfHeaveArray = array();

            if (mysqli_num_rows($leaveQry) > 0) {

                while($leaveData = mysqli_fetch_array($leaveQry)){

                    if ($leaveData['leave_day_type'] == "0") {
                        $lary = range_date($leaveData['leave_start_date'],
                            $leaveData['leave_end_date']);

                        for ($a = 0; $a < count($lary); $a++) {
                            array_push($leaveArray,$lary[$a]);
                        }
                    }else{
                        $laryHalf = range_date($leaveData['leave_start_date'],
                            $leaveData['leave_end_date']);

                        for ($b = 0; $b < count($laryHalf); $b++) {
                            array_push($halfHeaveArray,$laryHalf[$b]);
                        }
                    }

                }
            }

            // Month

            $totalLatePunchIN = 0;
            $totalEarlyPunchOUT = 0;
            $totalPunchOUTMissing = 0;
            $totalPresent = 0;
            $totalWeekOff = 0;
            $totalHolidays = 0;
            $totalLeaves = 0;
            $totalHalfDays = 0;
            $totalAbsent = count($datesArrayOfMonth);
            $totalAbsentDays = 0;

            $totalMonthHourSpent = array();
        
            for ($j = 0; $j < count($datesArrayOfMonth); $j++) {
                
                $dates = array();

                $monthDate = $datesArrayOfMonth[$j];
                $month = date('Y-m',strtotime($monthDate));
                $dates['date'] = $monthDate;
                $day_pos_new =  date('w', strtotime($monthDate));
                $dates['date_name'] = date('d F Y',strtotime($monthDate));
                $dates['day_name'] = date('l',strtotime($monthDate));

                $dates["total_spend_time"] = "0";

                $dates['holiday'] = false;
                $dates['week_off'] = false;
                $dates['work_report'] = false;
                $dates['leave'] = false;
                $dates['present'] = false;
                $dates['half_day'] = false;
                $dates['is_punch_out_missing'] = false;

                $isDateGone = "false";

                $dates["punch_out_missing_message"] = "";

                if ($monthDate < date('Y-m-d')) {
                    $isDateGone = "true";
                    $dates['is_date_gone'] = true;
                }else{
                    $isDateGone = "false";
                    $dates['is_date_gone'] = false;
                }

                if (in_array($monthDate,$holidayAry)) {
                    $dates['holiday'] = true;
                    $totalHolidays += 1;
                    $isDateGone = "false";
                }else{
                    if (in_array($day_pos_new,$weekOffDays) && !in_array($day_pos_new,$alternate_weekoff_days_array)) {
                        $dates['week_off'] = true;
                        $totalWeekOff += 1;
                        $isDateGone = "false";
                    }

                    $x1 = weekOfMonth(strtotime($monthDate));

                    if ($shiftData['has_altenate_week_off'] == "1") {  
                        if (in_array($day_pos_new,$alternate_weekoff_days_array) && 
                            in_array($x1,$alternateWeekOff)) {
                            $dates['week_off'] = true;
                            $totalWeekOff += 1;
                            $isDateGone = "false";
                        }
                    }
                }

                $dates["leave_reason"] = "";
                $dates["leave_type_name"] = "";

                if (in_array($monthDate,$leaveArray)) {
                    $dates['leave'] = true;
                    $totalLeaves += 1;
                    $isDateGone = "false";

                    $leaveDataQry = $d->selectRow("leave_type_name,leave_reason","leave_master,leave_type_master","leave_master.society_id = '$society_id' AND leave_master.user_id = '$user_id' AND leave_master.unit_id = '$unit_id' AND leave_master.leave_status = '1' AND leave_type_master.leave_type_id = leave_master.leave_type_id AND ('$monthDate' BETWEEN leave_master.leave_start_date AND leave_master.leave_end_date)");

                    if (mysqli_num_rows($leaveDataQry) > 0) {

                        $leaveDataTrue = mysqli_fetch_array($leaveDataQry);

                        $dates["leave_reason"] = $leaveDataTrue['leave_reason'];
                        $dates["leave_type_name"] = $leaveDataTrue['leave_type_name'];
                    }
                }

                if (in_array($monthDate,$halfHeaveArray)) {
                    $dates['half_day'] = true;
                    $totalHalfDays += 1;
                    $isDateGone = "false";
                }

                $holidayNameQry = $d->selectRow("holiday_name,holiday_description","holiday_master","society_id = '$society_id' AND holiday_start_date = '$monthDate'");

                $dates["holiday_name"] = "";
                $dates["holiday_description"] = "";

                if (mysqli_num_rows($holidayNameQry) > 0) {

                    $holidayNameData = mysqli_fetch_array($holidayNameQry);

                    $dates["holiday_name"] = $holidayNameData['holiday_name'];
                    $dates["holiday_description"] = $holidayNameData['holiday_description'];
                }

                $tdt = date('Y-m-d');

                $attendanceQry = $d->select("attendance_master,users_master","
                    (attendance_master.society_id='$society_id' 
                AND attendance_master.user_id = users_master.user_id 
                AND attendance_master.user_id = '$user_id'
                AND attendance_master.attendance_date_start = '$monthDate'
                AND attendance_master.attendance_date_start != '$tdt') OR 
                (attendance_master.society_id='$society_id' 
                AND attendance_master.user_id = users_master.user_id 
                AND attendance_master.user_id = '$user_id'
                AND attendance_master.attendance_date_start = '$monthDate'
                AND attendance_master.attendance_date_start = '$tdt'
                AND attendance_master.punch_out_time != '00:00:00')");


                if(mysqli_num_rows($attendanceQry)>0){

                    $totalPresent += 1;

                   
                    $dates['present'] = true;

                    $times = array();

                    $data = mysqli_fetch_array($attendanceQry);

                    if ($data['work_report'] !=null || $data['work_report'] != "") {
                        $dates["work_report"] = true;
                    }

                    $attendanceDateStart = $data['attendance_date_start'];
                    $attendanceDateEnd = $data['attendance_date_end'];
                    
                    $punchInTime = $data['punch_in_time'];
                    $punchOutTime = $data['punch_out_time'];

                    $dates['attendance_id'] = $data['attendance_id'];
                    $dates['work_report_title'] = $data['work_report_title'];
                    $dates['work_report_description'] = $data['work_report'];
                    $dates['punch_in_time'] = date("h:i A",strtotime($punchInTime));
                    $dates['punch_out_time'] = date("h:i A",strtotime($punchOutTime));

                    // Late Punch IN

                    $late_in_minutes = date('i',strtotime($late_time_start));
                    $late_in_minutes = $late_in_minutes * 60;
                    $shift_late_time = date("H:i:s",(strtotime($shift_start_time)+$late_in_minutes));

                    if ($punchInTime > $shift_late_time) {
                        $totalLatePunchIN += 1;
                    }
                    

                    if ($attendanceDateEnd == "0000-00-00") {
                        $dates['punch_out_time']= "";
                        $attendanceDateEnd = date('Y-m-d');
                        $punchOutTime = date('H:i:s');
                    }

                    if ($data['punch_out_time'] != "00:00:00") {

                        $totalHours = getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],
                            $data['punch_in_time'],$data['punch_out_time']);

                        $totalHoursName = getTotalHoursWithNames($data['attendance_date_start'],$data['attendance_date_end'],
                            $data['punch_in_time'],$data['punch_out_time']);

                        $times[] = $totalHours;

                        $dates['total_hours_spend'] = $totalHoursName;

                        // Early Punch OUT

                        if($punchOutTime !="00:00:00" && $monthDate != date('Y-m-d')){

                            $time = explode(':', $totalHoursName.":00");
                            $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                            $phtime = explode(':', $perdayHours);
                            $total_minutes_pday = ($phtime[0]*60) + ($phtime[1]) + ($phtime[2]/60);

                            $defaultearlyout = explode(':', $early_out_time);
                            $total_early_out_minutes = ($defaultearlyout[0]*60) + ($defaultearlyout[1]) + ($defaultearlyout[2]/60);

                            $fTime = $total_minutes_pday - $total_early_out_minutes;

                            if ($total_minutes < $fTime) {
                                $totalEarlyPunchOUT += 1;
                                //$dates['early_out'] = true;
                            }
                        }

                        $totalMonthHourSpent[] = $totalHours;

                        $dates['total_spend_time'] = getTotalWeekHours($times);
                        $dates["punch_out_missing_message"] = "";
                        $dates['is_punch_out_missing'] = false;
                    }else{
                        $dates['total_spend_time'] = "00:00";
                        $dates['total_hours_spend'] = "00:00";
                        $dates["punch_out_missing_message"] = "Punch Out Missing";
                        $dates['is_punch_out_missing'] = true;
                        $totalPunchOUTMissing += 1;
                    }

                    $attendanceHistoryQry = $d->select("attendance_master,attendance_break_history_master,
                        attendance_type_master","attendance_master.attendance_id = attendance_break_history_master.attendance_id
                        AND attendance_master.attendance_id = '$dates[attendance_id]'
                        AND attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id AND attendance_break_history_master.break_out_time != '00:00:00' AND attendance_break_history_master.break_end_date != '0000-00-00' ");

                    $dates["attendance_history"] = array();
                    
                    if(mysqli_num_rows($attendanceHistoryQry)>0){

                        $totalBreakTimeAtt = array();

                        while($historyData = mysqli_fetch_array($attendanceHistoryQry)){
                            $historyArray = array();

                            $breakStartDate = $historyData['break_start_date'];
                            $breakEndDate = $historyData['break_end_date'];
                            
                            $breakInTime = $historyData['break_in_time'];
                            $breakOutTime = $historyData['break_out_time'];

                            $historyArray['attendance_break_history_id'] = $historyData['attendance_break_history_id'];
                            $historyArray['attendance_type_name'] = $historyData['attendance_type_name'];
                            $historyArray['break_start_date'] = date('d F Y', strtotime($breakStartDate));
                            $historyArray['break_end_date'] = date('d F Y', strtotime($breakEndDate));
                            $historyArray['break_in_time'] = date('h:i A', strtotime($breakInTime));
                            $historyArray['break_out_time'] = date('h:i A', strtotime($breakOutTime));

                            $totalBreak = getTotalHours($breakStartDate,$breakEndDate,$breakInTime,$breakOutTime);

                            $totalBreakTimeAtt[] = $totalBreak;

                            $dates['total_spend_time_break'] = getTotalWeekHours($totalBreakTimeAtt);

                            $totalBreakHoursName = getTotalHoursWithNames($breakStartDate,$breakEndDate,$breakInTime,$breakOutTime);

                            $historyArray['total_break_hours_spend'] =$totalBreakHoursName;

                            array_push($dates['attendance_history'],$historyArray);
                        }
                    }
                }else{
                    if ($isDateGone == "true") {
                        $totalAbsentDays = ($totalAbsentDays + 1);
                    }
                }

                $response['total_month_hour_spent'] = getTotalWeekHours($totalMonthHourSpent);

                array_push($response['monthly_history'],$dates);
            }

            $response['late_punch_in'] = ($totalLatePunchIN-$totalHalfDays)."";
            $response['early_punch_out'] = $totalEarlyPunchOUT."";
            $response['total_present'] = $totalPresent."";
            $response['total_leave'] = $totalLeaves."";
            $response['total_half_day'] = $totalHalfDays."";
            $response['total_punch_out_missing'] = $totalPunchOUTMissing."";
            $response['total_week_off'] = $totalWeekOff."";
            $response['total_holidays'] = $totalHolidays."";

            $totalCData = $totalPresent + $totalWeekOff + $totalHolidays;
            $totalWorkingDays = $totalAbsent - ($totalWeekOff + $totalHolidays);

            $response['total_working_days'] = $totalWorkingDays."";
            //$response['total_absent'] = $totalAbsent - $totalCData."";
            $response['total_absent'] = $totalAbsentDays."";
            
            echo json_encode($response);
        
        }else{
            $response["message"]="Wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="Wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function getTotalWeekHours($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d.%02d', $hours, $minutes);
}

function getTotalWeekHoursName($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d:%02d', $hours, $minutes);
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {
    /*$date1 = strtotime("$startDate $startTime"); 
    $date2 = strtotime("$endDate $endTime"); 

    $diff = abs($date2 - $date1); 
    $years = floor($diff / (365*60*60*24)); 
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
    $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60)); 
    $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 
    $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

    return sprintf('%02d:%02d', $hours, $minutes);*/

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}


function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
    /*$date1 = strtotime("$startDate $startTime"); 
    $date2 = strtotime("$endDate $endTime"); 

    $diff = abs($date2 - $date1); 
    $years = floor($diff / (365*60*60*24)); 
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
    $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60)); 
    $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 
    $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));*/

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');


    return sprintf('%02d:%02d', $hours, $minutes);
}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}
?>