<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getPurchase']=="getPurchase" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("purchase_master,site_master,local_service_provider_users,bms_admin_master","site_master.site_id=purchase_master.site_id AND local_service_provider_users.service_provider_users_id=purchase_master.vendor_id AND 
            purchase_master.created_by = bms_admin_master.admin_id AND purchase_master.society_id='$society_id'","ORDER BY purchase_master.purchase_date DESC");

        if(mysqli_num_rows($qry)>0){
            
            $response["purchase_orders"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $purchase_orders = array();

                $purchase_orders["purchase_id"] = $data['purchase_id'];
                $purchase_orders["puchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
                $purchase_orders["po_number"] = $data['po_number'];
                $purchase_orders["site_id"] = $data['site_id'];
                $purchase_orders["site_name"] = $data['site_name'];
                $purchase_orders["purchase_descripion"] = $data['purchase_descripion'];
                $purchase_orders["service_provider_name"] = $data['service_provider_name'];
                $purchase_orders["admin_name"] = $data['admin_name'];
                $purchase_orders["admin_mobile"] = $data['admin_mobile'];
                $purchase_orders["admin_profile"] =  $base_url."img/society/".  $data['admin_profile'];

                if($data['purchase_status'] == '0'){ 
                    $status = "Pending";
                } else if($data['purchase_status'] == '1'){
                    $status = "Accepted";
                }else{
                    $status = "Rejected";
                }

                $totalItemCount = $d->count_data_direct("puchase_product_id","purchase_product_master","purchase_id = '$data[purchase_id]'");

                $purchase_orders["purchase_status"] = $data['purchase_status'];
                $purchase_orders["quntity"] = $totalItemCount.'';
                $purchase_orders["purchase_status_view"] = $status;

                     
                array_push($response["purchase_orders"], $purchase_orders);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getPurchaseDetails']=="getPurchaseDetails" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("purchase_master,site_master,local_service_provider_users,bms_admin_master","site_master.site_id=purchase_master.site_id AND local_service_provider_users.service_provider_users_id=purchase_master.vendor_id AND 
            purchase_master.created_by = bms_admin_master.admin_id AND purchase_master.society_id='$society_id'  AND purchase_id = '$purchase_id'");

        if(mysqli_num_rows($qry)>0){

            $data = mysqli_fetch_array($qry);

            $data = array_map("html_entity_decode", $data);

            $response["purchase_id"] = $data['purchase_id'];
            $response["puchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
            $response["po_number"] = $data['po_number'];
            $response["site_id"] = $data['site_id'];
            $response["site_name"] = $data['site_name'];
            $response["purchase_descripion"] = $data['purchase_descripion'];
            $response["service_provider_name"] = $data['service_provider_name'];
            $response["admin_name"] = $data['admin_name'];
            $response["admin_mobile"] = $data['admin_mobile'];
            $response["admin_profile"] =  $base_url."img/society/".  $data['admin_profile'];

            if ($data['purchase_invoice'] != '') {
                $response["purchase_invoice"] =  $base_url."img/purchase_invoice/".  $data['purchase_invoice'];
            }else{
                $response["purchase_invoice"] =  "";
            }

            if($data['purchase_status'] == '0'){ 
                $status = "Pending";
            } else if($data['purchase_status'] == '1'){
                $status = "Accepted";
            }else{
                $status = "Rejected";
            }

            $response["purchase_status"] = $data['purchase_status'];
            $response["purchase_status_view"] = $status;

            if ($purchase_id!='') {
                $q = $d->select("purchase_product_master,product_master,product_category_master,product_sub_category_master,unit_measurement_master","purchase_product_master.product_id=product_master.product_id AND product_master.product_category_id=product_category_master.product_category_id AND product_master.product_sub_category_id=product_sub_category_master.product_sub_category_id AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id AND purchase_product_master.purchase_id='$purchase_id'");

                $response['products'] = array();

                if (mysqli_num_rows($q) > 0) {
                    while($purchaseData = mysqli_fetch_array($q)){

                        $purchaseData = array_map("html_entity_decode", $purchaseData);

                        $products = array();

                        $products['puchase_product_id'] = $purchaseData['puchase_product_id'];
                        $products['product_code'] = $purchaseData['product_code'];
                        $products['product_name'] = $purchaseData['product_name'];
                        $products['category_name'] = $purchaseData['category_name'];
                        $products['sub_category_name'] = $purchaseData['sub_category_name'];
                        $products['unit_measurement_name'] = $purchaseData['unit_measurement_name'];
                        $products['price'] = $purchaseData['price'];
                        $products['quantity'] = $purchaseData['quantity'];
                        $products['availablity_status'] = $purchaseData['availablity_status'];

                        array_push($response['products'],$products);
                    }
                }
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['sendPurchaseInvoice']=="sendPurchaseInvoice" && $purchase_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $purchase_invoice = "";

        if ($_FILES["purchase_invoice"]["tmp_name"] != null) {
            // code...
            $file11=$_FILES["purchase_invoice"]["tmp_name"];
            $extId = pathinfo($_FILES['purchase_invoice']['name'], PATHINFO_EXTENSION);
            $extAllow=array("pdf","doc","docx","png","jpg","jpeg","JPEG","PNG","JPG","PDF");

            if(in_array($extId,$extAllow)) {
                $temp = explode(".", $_FILES["purchase_invoice"]["name"]);
                $purchase_invoice = "p_invoice_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["purchase_invoice"]["tmp_name"], "../img/purchase_invoice/" . $purchase_invoice);
            } else {
            
                $response["message"] = "Invalid Document. Only JPG,PNG, JPEG, Doc & PDF files are allowed.";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
        }

        $m->set_data('purchase_invoice',$purchase_invoice);

        $a = array(
            'purchase_invoice'=>$m->get_data('purchase_invoice'),
        );

        $qry = $d->update("purchase_master",$a,"purchase_id = '$purchase_id'");

        if ($qry == true) {
            $response["message"]="Invoice Uploaded";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updateProduct']=="updateProduct" && $productIds != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $puchaseProductIds = explode(",", $productIds);
        $puchaseProductPrice = explode(",", $productPrice);

        $totalIds = COUNT($puchaseProductIds);

        for ($i=0; $i < $totalIds; $i++) { 
            $m->set_data('price',$puchaseProductPrice[$i]);

            $a = array(
                'price'=>$m->get_data('price'),
            );

            $purchaseProductId = $puchaseProductIds[$i];

            $qry = $d->update("purchase_product_master",$a,"puchase_product_id = '$purchaseProductId'");
        }

        if ($qry == true) {
            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['markAsNotAvailable']=="markAsNotAvailable" && $puchase_product_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('availablity_status',$availablity_status);

        $a = array(
            'availablity_status'=>$m->get_data('availablity_status'),
        );

        $qry = $d->update("purchase_product_master",$a,"puchase_product_id = '$puchase_product_id'");

        if ($qry == true) {
            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['changePurchaseStatus']=="changePurchaseStatus" && $purchase_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('purchase_status',$purchase_status);
        $m->set_data('purchase_reject_reason',$purchase_reject_reason);

        $a = array(
            'purchase_status'=>$m->get_data('purchase_status'),
            'purchase_reject_reason'=>$m->get_data('purchase_reject_reason'),
        );

        $qry = $d->update("purchase_master",$a,"purchase_id = '$purchase_id'");

        if ($qry == true) {
            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getSiteList']=="getSiteList" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("site_master","society_id='$society_id' AND site_active_status=0","ORDER BY site_name ASC");

        $local_service_providerq=$d->select("local_service_provider_users,local_service_provider_users_category","local_service_provider_users_category.service_provider_users_id=local_service_provider_users.service_provider_users_id  AND  local_service_provider_users.service_provider_status=0 ","");

        $response["products_category"] = array();
        $response["sites"] = array();
        $response["local_service_provider"] = array();
        if(mysqli_num_rows($qry)>0){

            while($data = mysqli_fetch_array($qry)) {

                $sites = array();

                $sites["site_id"] = $data['site_id'];
                $sites["society_id"] = $data['society_id'];
                $sites["site_name"] = $data['site_name'].' ('.$data['site_address'].')';
                     
                array_push($response["sites"], $sites);
            }
        }

        if(mysqli_num_rows($local_service_providerq)>0){

          while($data_local_service_provider=mysqli_fetch_array($local_service_providerq)) {
            $local_service_provider = array();
            $local_service_provider["service_provider_users_id"]=$data_local_service_provider['service_provider_users_id'];
            $local_service_provider["service_provider_name"]=html_entity_decode($data_local_service_provider['service_provider_name']);

            array_push($response["local_service_provider"], $local_service_provider);

          }
        }

        $pq=$d->select("product_category_master","society_id='$society_id' ");  
        while ($floorData=mysqli_fetch_array($pq)) {
            $products_category = array();

            $products_category["product_category_id"]=$floorData['product_category_id'];
            $products_category["category_name"]=html_entity_decode($floorData['category_name']);

            $products_category["products_sub_category"] = array();

            $pqsub=$d->select("product_sub_category_master","society_id='$society_id' AND  product_category_id='$floorData[product_category_id]'"); 
            while ($subData=mysqli_fetch_array($pqsub)) {
                $products_sub_category = array();
                $products_sub_category["product_sub_category_id"]=$subData['product_sub_category_id'];
                $products_sub_category["product_category_id"]=$subData['product_category_id'];
                $products_sub_category["sub_category_name"]=html_entity_decode($subData['sub_category_name']);

                array_push($products_category["products_sub_category"], $products_sub_category);

            }

            array_push($response["products_category"], $products_category);
        }

        if (mysqli_num_rows($qry)>0) {
            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        } else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getProductList']=="getProductList" && filter_var($society_id, FILTER_VALIDATE_INT) == true){
        if ($product_sub_category_id>0) {
            $appendQuery = " AND product_master.product_sub_category_id='$product_sub_category_id'";
        }

        $q = $d->select("product_master,unit_measurement_master", "product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id AND product_master.society_id='$society_id' AND product_master.product_category_id = '$product_category_id' AND product_master.product_status=0  $appendQuery","ORDER BY product_master.product_name ASC");
        if (mysqli_num_rows($q)>0) {
            $response["products"] = array();
            while ($data = mysqli_fetch_assoc($q)) {
                $products = array();
                $products["product_id"]=$data['product_id'];
                $products["product_category_id"]=$data['product_category_id'];
                $products["product_sub_category_id"]=$data['product_sub_category_id'];
                $products["product_name"]=$data['product_name'];
                $products["product_code"]=html_entity_decode($data['product_code']);
                $products["product_short_name"]=html_entity_decode($data['product_short_name']);
                $products["product_brand"]=html_entity_decode($data['product_brand']);
                $products["unit_measurement_name"]=html_entity_decode($data['unit_measurement_name']);
                array_push($response["products"], $products);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);

        } else {
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response); 
        }
    }else if($_POST['addPurchase']=="addPurchase" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        /*$purchase_invoice = "";

        if ($_FILES["purchase_invoice"]["tmp_name"] != null) {
            // code...
            $file11=$_FILES["purchase_invoice"]["tmp_name"];
            $extId = pathinfo($_FILES['purchase_invoice']['name'], PATHINFO_EXTENSION);
            $extAllow=array("pdf","doc","docx","png","jpg","jpeg","JPEG","PNG","JPG","PDF");

            if(in_array($extId,$extAllow)) {
                $temp = explode(".", $_FILES["purchase_invoice"]["name"]);
                $purchase_invoice = "p_invoice_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["purchase_invoice"]["tmp_name"], "../img/purchase_invoice/" . $purchase_invoice);
            } else {
            
                $response["message"] = "Invalid Document. Only JPG,PNG, JPEG, Doc & PDF files are allowed.";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
        }*/

        $m->set_data('society_id',$society_id);
        $m->set_data('purchase_date',$dateTime);
        $m->set_data('site_id',$site_id);
        $m->set_data('vendor_id',$vendor_id);
        $m->set_data('purchase_descripion',$purchase_descripion);
        $m->set_data('po_number',$po_number);
        $m->set_data('created_by',$user_id);
        $m->set_data('created_date',$dateTime);

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'purchase_date'=>$m->get_data('purchase_date'),
            'site_id'=>$m->get_data('site_id'),
            'vendor_id'=>$m->get_data('vendor_id'),
            'purchase_descripion'=>$m->get_data('purchase_descripion'),
            'po_number'=>$m->get_data('po_number'),
            'created_by'=>$m->get_data('created_by'),
            'created_date'=>$m->get_data('created_date'),
        );

        $qry = $d->insert("purchase_master",$a);

        $purchase_id = $con->insert_id;

        if ($qry == true) {

            $productIdAry = explode(",",$productIds);
            $quantitiesAry = explode(",",$quantities);

            for ($i=0; $i < count($productIdAry) ; $i++) { 
                $m->set_data('society_id',$society_id);
                $m->set_data('purchase_id',$purchase_id);
                $m->set_data('product_id',$productIdAry[$i]);
                $m->set_data('quantity',$quantitiesAry[$i]);

                $a1 = array(
                    'society_id'=>$m->get_data('society_id'),
                    'purchase_id'=>$m->get_data('purchase_id'),
                    'product_id'=>$m->get_data('product_id'),
                    'quantity'=>$m->get_data('quantity'),
                );

                $qry = $d->insert("purchase_product_master",$a1);
            }

            $response["message"]="Purchase added";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updatePurchase']=="updatePurchase" && $purchase_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


        $m->set_data('society_id',$society_id);
        $m->set_data('purchase_date',$dateTime);
        $m->set_data('site_id',$site_id);
        $m->set_data('vendor_id',$vendor_id);
        $m->set_data('purchase_descripion',$purchase_descripion);
        $m->set_data('po_number',$po_number);

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'purchase_date'=>$m->get_data('purchase_date'),
            'site_id'=>$m->get_data('site_id'),
            'vendor_id'=>$m->get_data('vendor_id'),
            'purchase_descripion'=>$m->get_data('purchase_descripion'),
            'po_number'=>$m->get_data('po_number'),
        );

        $qry = $d->update("purchase_master",$a,"purchase_id = '$purchase_id'");


        if ($qry == true) {
            $response["message"]="Purchase Data Updated";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['addUpdatePurchaseProduct']=="addUpdatePurchaseProduct" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('society_id',$society_id);
        $m->set_data('purchase_id',$purchase_id);
        $m->set_data('product_id',$product_id);
        $m->set_data('quantity',$quantity);

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'purchase_id'=>$m->get_data('purchase_id'),
            'product_id'=>$m->get_data('product_id'),
            'quantity'=>$m->get_data('quantity'),
        );

        if ($puchase_product_id != null && $puchase_product_id !='') {
            $qry = $d->update("purchase_product_master",$a,"puchase_product_id = '$puchase_product_id'");
            $msg = "Product Updated";
        }else{
            $qry = $d->insert("purchase_product_master",$a);
            $msg = "Product Added";
        }


        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['deletePurchase']=="deletePurchase" && $purchase_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->delete("purchase_master","purchase_id = '$purchase_id' AND purchase_status = '0'");
        
        $msg = "Purchase Deleted";
    
        if ($qry == true) {

            $qry = $d->delete("purchase_product_master","purchase_id = '$purchase_id'");

            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['deleteProduct']=="deleteProduct" && $puchase_product_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->delete("purchase_product_master","puchase_product_id = '$puchase_product_id'");
        
        $msg = "Product Deleted";
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>