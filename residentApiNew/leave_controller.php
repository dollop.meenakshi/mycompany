<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        if($_POST['checkAccess']=="checkAccess" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $leave_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['getLeaveTypes']=="getLeaveTypes" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $leaveTyprQry = $d->select("leave_type_master","society_id= '$society_id' AND leave_type_active_status='0' AND leave_type_delete = '0' ");

            if(mysqli_num_rows($leaveTyprQry)>0){
                
                $response["leave_types"] = array();

                while($leaveTypeData=mysqli_fetch_array($leaveTyprQry)) {

                    $leaveTypeData = array_map("html_entity_decode", $leaveTypeData);

                    $leave_types = array(); 

                    $leave_types["leave_type_id"] = $leaveTypeData['leave_type_id'];
                    $leave_types["leave_type_name"] = $leaveTypeData['leave_type_name'];

                    array_push($response["leave_types"], $leave_types);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['getLeaveTypesWithData']=="getLeaveTypesWithData" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $leaveTyprQry = $d->select("leave_type_master,leave_assign_master","leave_type_master.leave_type_id = leave_assign_master.leave_type_id 
                AND leave_assign_master.assign_leave_year = '$currentYear' 
                AND leave_assign_master.society_id = '$society_id' 
                AND leave_assign_master.user_id = '$user_id'
                AND leave_type_master.leave_type_active_status='0' 
                AND leave_type_master.leave_type_delete = '0' ");

            if(mysqli_num_rows($leaveTyprQry)>0){
                
                $response["leave_types"] = array();

                while($assignedLeaveData=mysqli_fetch_array($leaveTyprQry)) {

                    $leave_types = array();

                    $userTotalTypeLeave = $assignedLeaveData['user_total_leave'];

                    $leave_types["leave_type_id"] = $assignedLeaveData['leave_type_id'];
                    $leave_types["leave_type_name"] = $assignedLeaveData['leave_type_name'];
                    $leave_types["applicable_leaves_in_month"] = $assignedLeaveData['applicable_leaves_in_month'];
                    $leave_types["user_total_leave"] = $userTotalTypeLeave."";

                    $totalUsedLeaveByType = $d->count_data_direct("leave_id","leave_master",
                        "society_id = '$society_id' 
                        AND user_id = '$user_id' 
                        AND leave_type_id = '$assignedLeaveData[leave_type_id]' 
                        AND leave_status = '1'");

                    $leave_types["user_total_used_leave"] = $totalUsedLeaveByType."";
                    $leave_types["remaining_leave"] = $userTotalTypeLeave - $totalUsedLeaveByType."";

                    if ($assignedLeaveData['leave_apply_on_date'] == 1) {
                        $leave_types['leave_apply_on_date'] = true;
                    }else{
                        $leave_types['leave_apply_on_date'] = false;
                    }

                    array_push($response["leave_types"], $leave_types);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Leaves not assigned";
                $response["status"] = "201";
                echo json_encode($response);
            }             
        }else if($_POST['applyForLeave']=="applyForLeave" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $startTimeStamp = strtotime($leave_start_date);
            $endTimeStamp = strtotime($leave_end_date);

            $timeDiff = abs($endTimeStamp - $startTimeStamp);

            $numberDays = $timeDiff/86400;  // 86400 seconds in one day

            $numberDays = intval($numberDays);
            
            $leaveTotalDays = $numberDays + 1;

            if ($leaveTotalDays > 31) {
                $response["message"] = "You are applying leave more than 1 month, please contact to HR team for your leave application.";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $leave_attachment = "";

            if ($_FILES["leave_attachment"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["leave_attachment"]["tmp_name"];
                $extId = pathinfo($_FILES['leave_attachment']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","png","jpg","jpeg","PDF","JPEG","PNG","JPG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["leave_attachment"]["name"]);
                    $leave_attachment = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["leave_attachment"]["tmp_name"], "../img/leave_attchment/" . $leave_attachment);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $m->set_data('leave_type_id',$leave_type_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('leave_reason',$leave_reason);
            $m->set_data('leave_alternate_number',$leave_alternate_number);
            $m->set_data('leave_start_date',$leave_start_date);
            $m->set_data('leave_end_date',$leave_end_date);
            $m->set_data('leave_total_days',$leave_total_days);
            $m->set_data('leave_attachment',$leave_attachment);
            $m->set_data('leave_day_type',$leave_day_type);
            $m->set_data('leave_task_dependency',$leave_task_dependency);
            $m->set_data('leave_handle_dependency',$leave_handle_dependency);
            $m->set_data('leave_created_date',$dateTime);

            $a = array(
                'leave_type_id'=>$m->get_data('leave_type_id'),
                'society_id' =>$m->get_data('society_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'user_id'=>$m->get_data('user_id'),
                'leave_reason'=>$m->get_data('leave_reason'),
                'leave_alternate_number'=>$m->get_data('leave_alternate_number'),
                'leave_start_date'=>$m->get_data('leave_start_date'),
                'leave_end_date'=>$m->get_data('leave_end_date'),
                'leave_total_days'=>$m->get_data('leave_total_days'),
                'leave_attachment'=>$m->get_data('leave_attachment'),
                'leave_day_type'=>$m->get_data('leave_day_type'),
                'leave_task_dependency'=>$m->get_data('leave_task_dependency'),
                'leave_handle_dependency'=>$m->get_data('leave_handle_dependency'),
                'leave_created_date'=>$m->get_data('leave_created_date'),
            );

            $applyLeaveQry = $d->insert("leave_master",$a);

            if ($applyLeaveQry == true) {

                // To App Access

                $title = "Leave Request";
                $description = "Requested by, ".$user_name;

                $access_type = $leave_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmAccessAry=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmAccessAryiOS=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $tabPosition = "1"; // 1 for all leaves, 0 for my leaves

                    $nResident->noti("leave_tracker","",$society_id,$fcmAccessAry,$title,$description,$tabPosition);
                    $nResident->noti_ios("leave_tracker","",$society_id,$fcmAccessAryiOS,$title,$description,$tabPosition);

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"users_master.user_id IN ('$userFcmIds')");
                }

                // To Admin App

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("15",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("15",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"leaves");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"leaves");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"leaves",
                  'admin_click_action '=>"leaves",
                  'notification_logo'=>'leave_tracker.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Leave Applied";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['applyForLeaveNew']=="applyForLeaveNew" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $leave_dates = explode(",",$leave_dates);
            $leave_types = explode(",",$leave_types);
            $leave_day_types = explode(",",$leave_day_types);
            $half_day_sessions = explode(",",$half_day_sessions);
            $leave_type_id = explode(",",$leave_type_id);

            if (COUNT($leave_dates) > 15) {
                $response["message"] = "You are applying leave for more than 15 days, please contact to HR for your leave application.";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $leave_attachment = "";

            if ($_FILES["leave_attachment"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["leave_attachment"]["tmp_name"];
                $extId = pathinfo($_FILES['leave_attachment']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","png","jpg","jpeg","PDF","JPEG","PNG","JPG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["leave_attachment"]["name"]);
                    $leave_attachment = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["leave_attachment"]["tmp_name"], "../img/leave_attchment/" . $leave_attachment);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $leave_total_days = 1;

            // $leaveLastDate  = $leave_dates[array_key_last($leave_dates)];
            for ($i=0; $i < COUNT($leave_dates); $i++) { 
                $m->set_data('leave_type_id',$leave_type_id[$i]);
                $m->set_data('society_id',$society_id);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('paid_unpaid',$leave_types[$i]);
                $m->set_data('leave_reason',$leave_reason);
                $m->set_data('leave_alternate_number',$leave_alternate_number);
                $m->set_data('leave_start_date',$leave_dates[$i]);
                $m->set_data('leave_end_date',$leave_dates[$i]);
                $m->set_data('leave_total_days',$leave_total_days);
                $m->set_data('leave_attachment',$leave_attachment);
                $m->set_data('leave_day_type',$leave_day_types[$i]);
                $m->set_data('half_day_session',$half_day_sessions[$i]);
                $m->set_data('leave_task_dependency',$leave_task_dependency);
                $m->set_data('leave_handle_dependency',$leave_handle_dependency);
                $m->set_data('leave_created_date',$dateTime);

                $a = array(
                    'leave_type_id'=>$m->get_data('leave_type_id'),
                    'society_id' =>$m->get_data('society_id'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'paid_unpaid'=>$m->get_data('paid_unpaid'),
                    'leave_reason'=>$m->get_data('leave_reason'),
                    'leave_alternate_number'=>$m->get_data('leave_alternate_number'),
                    'leave_start_date'=>$m->get_data('leave_start_date'),
                    'leave_end_date'=>$m->get_data('leave_end_date'),
                    'leave_total_days'=>$m->get_data('leave_total_days'),
                    'leave_attachment'=>$m->get_data('leave_attachment'),
                    'leave_day_type'=>$m->get_data('leave_day_type'),
                    'half_day_session'=>$m->get_data('half_day_session'),
                    'leave_task_dependency'=>$m->get_data('leave_task_dependency'),
                    'leave_handle_dependency'=>$m->get_data('leave_handle_dependency'),
                    'leave_created_date'=>$m->get_data('leave_created_date'),
                );

                $applyLeaveQry = $d->insert("leave_master",$a);
            }

            if ($applyLeaveQry == true) {

                // To App Access

                $title = "Leave Request";
                $description = "Requested by, ".$user_name;

                $access_type = $leave_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmAccessAry=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmAccessAryiOS=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $tabPosition = "1"; // 1 for all leaves, 0 for my leaves
                    
                    $nResident->noti("leave_tracker","",$society_id,$fcmAccessAry,$title,$description,$tabPosition);
                    $nResident->noti_ios("leave_tracker","",$society_id,$fcmAccessAryiOS,$title,$description,$tabPosition);

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"users_master.user_id IN ('$userFcmIds')");
                }

                $d->insert_myactivity($user_id, $society_id, "0", $user_name, "New leave applied", "leave_tracker.png");

                // To Admin App

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("15",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("15",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"leaves");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"leaves");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"leaves",
                  'admin_click_action '=>"leaves",
                  'notification_logo'=>'leave_tracker.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Leave Applied";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['changeLeaveStatus']=="changeLeaveStatus" && $user_id!='' && $leave_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $lq = $d->selectRow("leave_type_id,leave_start_date","leave_master","leave_id = '$leave_id' AND user_id = '$user_id'");
            $lqData = mysqli_fetch_array($lq);

            $leave_type_id = $lqData['leave_type_id'];
            $leave_start_date = $lqData['leave_start_date'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"$base_url/residentApiNew/commonController.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                      "action=getLeaveBalanceForAutoLeave&leave_type_id=$leave_type_id&leave_date=$leave_start_date&user_id=$user_id&society_id=$society_id");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'key: '.$keydb
            ));

            $server_output = curl_exec($ch);

            curl_close ($ch);
            $server_output=json_decode($server_output,true);


            $m->set_data('leave_status',$leave_status);
            $m->set_data('leave_day_type',$leave_day_type);
            $m->set_data('leavePaidUnpaid',$leavePaidUnpaid);
            $m->set_data('leave_admin_reason',$leave_admin_reason);

            if ($leave_status == 1) {


                if ($server_output!='' && $server_output['leave']['available_paid_leave'] <= 0 && $leavePaidUnpaid == "0") {
                    $response["message"] = "Paid Leave not available";                
                    $response["status"] = "201";
                    echo json_encode($response);

                    $d->insert_myactivity($leave_approved_by, $society_id, "0", $user_name, "Paid leave not available", "leave_tracker.png");
                    
                    exit();
                }

                $m->set_data('leave_approved_by',$my_user_id);
                $m->set_data('leave_approved_date',date('Y-m-d H:i:s'));
                $m->set_data('leave_declined_by',"0");
            }else{
                $m->set_data('leave_approved_by',"0");
                $m->set_data('leave_declined_by',$my_user_id);
            }

            $a = array(
                'leave_status'=>$m->get_data('leave_status'),
                'leave_day_type'=>$m->get_data('leave_day_type'),
                'paid_unpaid'=>$m->get_data('leavePaidUnpaid'),
                'leave_admin_reason'=>$m->get_data('leave_admin_reason'),
                'leave_approved_by'=>$m->get_data('leave_approved_by'),
                'leave_approved_date'=>$m->get_data('leave_approved_date'),
                'leave_declined_by'=>$m->get_data('leave_declined_by'),
            );

            $changeLeaveStatusQry = $d->update("leave_master",$a,"leave_id = '$leave_id' AND user_id = '$user_id'");

            if ($changeLeaveStatusQry == true) {

                if ($leave_status == 1) {
                    if ($leave_day_type == 0) {
                        $title = "Leave Approved - Full Day";
                    }else{
                        $title = "Leave Approved - Half Day";                        
                    }
                    $msg_view = "Approved";
                }else if($leave_status == 2){
                    $title = "Leave Rejected";
                    $msg_view = "Rejected";


                }

                $d->insert_myactivity($my_user_id, $society_id, "0", $user_name, "$title", "leave_tracker.png");


                $qry = $d->selectRow("leave_start_date","leave_master","leave_id = '$leave_id' AND user_id = '$user_id'");

                $qryData = mysqli_fetch_array($qry);

                if($leave_status == 2){

                    $m->set_data('auto_leave',"0");
                    $m->set_data('is_leave',"0");
                    $m->set_data('extra_day_leave_type',"0");

                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'is_leave'=>$m->get_data('is_leave'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                    );

                    $d->update("attendance_master",$a1,"attendance_date_start = '$qryData[leave_start_date]' AND user_id = '$user_id'");
                }

                $description ="For Date : ".date('d F Y',strtotime($qryData['leave_start_date']))."\nBy, ".$user_name;

                $response["message"] = $title;
                $response["leave_status_view"] = $msg_view;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
                $data_notification = mysqli_fetch_array($qsm);
                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "0"; // 1 for all leaves, 0 for my leaves

                if ($device == 'android') {
                    $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");

                $response["leave_status"] = $leave_status;
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['deleteLeaveRequest']=="deleteLeaveRequest" && $user_id!='' && $leave_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $leaveQry = $d->selectRow("leave_start_date","leave_master","leave_id = '$leave_id' AND user_id = '$user_id'");

            $leaveData = mysqli_fetch_array($leaveQry);

            $leaveDate = date('m-Y',strtotime($leaveData['leave_start_date']));

            $qry = $d->count_data_direct("salary_slip_id","salary_slip_master","user_id = '$user_id' AND salary_month_name = '$leaveDate'");

            if ($qry > 0) {
                $response["message"] = "This month salary already generated. Therefor, leave can not be deleted";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $deleteQry = $d->delete("leave_master","leave_id = '$leave_id' AND user_id = '$user_id'");

            if ($deleteQry == true) {

                $d->insert_myactivity($my_user_id, $society_id, "0", $user_name, "Leave Request Deleted", "leave_tracker.png");

                $m->set_data('auto_leave',"0");
                $m->set_data('is_leave',"0");
                $m->set_data('extra_day_leave_type',"0");

                $a1 = array(
                    'auto_leave'=>$m->get_data('auto_leave'),
                    'is_leave'=>$m->get_data('is_leave'),
                    'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                );

                $d->update("attendance_master",$a1,"attendance_date_start = '$leaveData[leave_start_date]' AND user_id = '$user_id'");

                $response["message"] = "Leave request deleted";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed to delete leave request";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getLeaveHistory']=="getLeaveHistory" && $user_id!='' && $user_id!='' && $floor_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true){

            $teamQry = $d->selectRow("
                employee_level_master.level_id,
                employee_level_master.parent_level_id,
                employee_level_master.level_name,
                users_master.user_id,
                users_master.user_full_name,
                users_master.user_designation,
                users_master.user_profile_pic
                ","
                users_master,
                employee_level_master
                ","
                users_master.level_id = employee_level_master.level_id 
                AND employee_level_master.parent_level_id = '$level_id' AND  employee_level_master.parent_level_id != ''");

            $userIDArray = array();


            if (mysqli_num_rows($teamQry) > 0) {
                while($teamData = mysqli_fetch_array($teamQry)){

                    $team_member_id = $teamData['user_id'];

                    array_push($userIDArray,$team_member_id);
                }
            }

            $userIDArray = join("','",$userIDArray);

            if (isset($userIDArray) && count($userIDArray) > 0) {

                // Current Week

                $monday = strtotime('next Monday -1 week');
                $monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
                $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
                $this_week_sd = date("Y-m-d",$monday);
                $this_week_ed = date("Y-m-d",$sunday);

                // Next Week

                $monday = strtotime('next Monday');
                $monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
                $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
                $next_week_sd = date("Y-m-d",$monday);
                $next_week_ed = date("Y-m-d",$sunday);

                $leaveCurrentWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$this_week_sd' AND '$this_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC");

                $leaveNextWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$next_week_sd' AND '$next_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC");

                if (mysqli_num_rows($leaveCurrentWeekQry) > 0) {
                    $response['team_leave'] = true;
                }
                
                if (mysqli_num_rows($leaveNextWeekQry) > 0) {
                    $response['team_leave'] = true;
                }

                if (mysqli_num_rows($leaveNextWeekQry) <=  0 && mysqli_num_rows($leaveCurrentWeekQry) <= 0) {
                    $response['team_leave'] = false;
                }
            }else{
                $response['team_leave'] = false;
            }


            $access_type = $leave_access;

            include "check_access_data.php";

            $response["current_month"] = date('m');

            if ($access_for == 0 || $access_for == 1) {

                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM leave_master,leave_type_master,users_master WHERE leave_master.leave_status = '0' AND leave_master.leave_type_id = leave_type_master.leave_type_id 
                        AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND users_master.block_id IN ('$accessBranchIds')) AS total_pending,
                    (SELECT COUNT(*) FROM leave_master,leave_type_master,users_master WHERE leave_master.leave_status = '1' AND leave_master.leave_type_id = leave_type_master.leave_type_id 
                        AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND users_master.block_id IN ('$accessBranchIds')) AS total_approved,
                    (SELECT COUNT(*) FROM leave_master,leave_type_master,users_master WHERE leave_master.leave_status = '2' AND leave_master.leave_type_id = leave_type_master.leave_type_id 
                        AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND users_master.block_id IN ('$accessBranchIds')) AS total_rejected,
                    (SELECT COUNT(*) FROM leave_master,users_master WHERE leave_master.leave_status = '1' AND leave_master.leave_type_id = 0 AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND users_master.block_id IN ('$accessBranchIds')) AS total_auto");
            }else if($access_for == 2){

                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM leave_master,leave_type_master,users_master WHERE leave_master.leave_status = '0' AND leave_master.leave_type_id = leave_type_master.leave_type_id 
                        AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND users_master.floor_id IN ('$accessDepIds')) AS total_pending,
                    (SELECT COUNT(*) FROM leave_master,leave_type_master,users_master WHERE leave_master.leave_status = '1' AND leave_master.leave_type_id = leave_type_master.leave_type_id 
                        AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND users_master.floor_id IN ('$accessDepIds')) AS total_approved,
                    (SELECT COUNT(*) FROM leave_master,leave_type_master,users_master WHERE leave_master.leave_status = '2' AND leave_master.leave_type_id = leave_type_master.leave_type_id 
                        AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND users_master.floor_id IN ('$accessDepIds')) AS total_rejected,
                    (SELECT COUNT(*) FROM leave_master,users_master WHERE leave_master.leave_status = '1' AND leave_master.leave_type_id = 0 AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND users_master.floor_id IN ('$accessDepIds')) AS total_auto");

            }else if($access_for == 3){

                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM leave_master,leave_type_master,users_master WHERE leave_master.leave_status = '0' AND leave_master.leave_type_id = leave_type_master.leave_type_id 
                        AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND leave_master.user_id IN ('$accessUserIds')) AS total_pending,
                    (SELECT COUNT(*) FROM leave_master,leave_type_master,users_master WHERE leave_master.leave_status = '1' AND leave_master.leave_type_id = leave_type_master.leave_type_id 
                        AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND leave_master.user_id IN ('$accessUserIds')) AS total_approved,
                    (SELECT COUNT(*) FROM leave_master,leave_type_master,users_master WHERE leave_master.leave_status = '2' AND leave_master.leave_type_id = leave_type_master.leave_type_id 
                        AND leave_master.user_id = users_master.user_id AND users_master.delete_status = 0 AND leave_master.user_id IN ('$accessUserIds')) AS total_rejected,
                    (SELECT COUNT(*) FROM leave_master,users_master WHERE leave_master.leave_status = '1' AND leave_master.leave_type_id = 0 AND users_master.delete_status = 0 AND leave_master.user_id = users_master.user_id AND leave_master.user_id IN ('$accessUserIds')) AS total_auto");
            }

            $countData = mysqli_fetch_array($totalCounts);

            $response['total_pending_leaves'] = $countData['total_pending']."";
            $response['total_approved_leaves'] = $countData['total_approved']."";
            $response['total_rejected_leaves'] = $countData['total_rejected']."";
            $response['total_auto'] = $countData['total_auto']."";

            $leaveHistoryQry = $d->selectRow("
                leave_master.*,
                leave_type_master.*,
                holiday_master.holiday_id
                ","
                leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id LEFT JOIN holiday_master ON holiday_master.holiday_start_date = leave_master.leave_start_date
                ","
                leave_master.society_id = '$society_id' 
                AND leave_master.user_id = '$user_id' 
                ","ORDER BY leave_master.leave_start_date DESC");

           /* echo "SELECT  leave_master.*,
                leave_type_master.*,
                holiday_master.holiday_id FROM leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id LEFT JOIN holiday_master ON holiday_master.holiday_start_date = leave_master.leave_start_date WHERE leave_master.society_id = '$society_id' 
                AND leave_master.user_id = '$user_id' ORDER BY leave_master.leave_start_date DESC ";*/

            if(mysqli_num_rows($leaveHistoryQry)>0){
                
                $response["leave_history"] = array();

                while($leaveHistoryData = mysqli_fetch_array($leaveHistoryQry)) {

                    $leaveHistoryData = array_map("html_entity_decode", $leaveHistoryData);

                    $leave_history = array();

                    $leave_history["holiday_id"] = $leaveHistoryData['holiday_id'];
                    $leave_history["leave_id"] = $leaveHistoryData['leave_id'];
                    $leave_history["user_id"] = $leaveHistoryData['user_id'];
                    $leave_history["unit_id"] = $leaveHistoryData['unit_id'];

                    if ($leaveHistoryData['leave_type_id'] == null || $leaveHistoryData['leave_type_id'] == 0) {
                        $leave_history["leave_type_name"] = "Auto Leave";
                        $leave_history['auto_leave'] = true;
                    }else{                        
                        $leave_history['auto_leave'] = false;
                        $leave_history["leave_type_name"] = $leaveHistoryData['leave_type_name'];
                    }

                    $leave_history["leave_reason"] = $leaveHistoryData['leave_reason'];
                    if ($leaveHistoryData['leave_reason']=='') {
                        $leave_history["auto_leave_reason"] = $leaveHistoryData['auto_leave_reason'];
                    }else{
                        $leave_history["auto_leave_reason"] = "";                        
                    }
                    $leave_history["leave_alternate_number"] = $leaveHistoryData['leave_alternate_number'];
                    if ($leaveHistoryData['leave_attachment']!="") {
                        $leave_history["leave_attachment"] = $base_url."img/leave_attchment/". $leaveHistoryData['leave_attachment'];
                    }else{
                        $leave_history["leave_attachment"] = "";
                    }

                    if ($leaveHistoryData['leave_task_dependency'] == 1) {
                        $leave_history["leave_task_dependency"] = true;
                    }else{
                        $leave_history["leave_task_dependency"] = false;
                    }

                    if ($leaveHistoryData['leave_alternate_number'] == "0") {
                        $leave_history["leave_alternate_number"] = "";
                    }else{
                        $leave_history["leave_alternate_number"] = $leaveHistoryData['leave_alternate_number'];
                    }
                    
                    $leave_history["leave_handle_dependency"] = $leaveHistoryData['leave_handle_dependency'];
                    $leave_history["leave_admin_reason"] = $leaveHistoryData['leave_admin_reason'];
                    $leave_history["leave_status"] = $leaveHistoryData['leave_status'];

                    if ($leaveHistoryData["leave_status"] == 0) {
                        $leave_history["leave_status_view"] = "Pending";
                    }else if ($leaveHistoryData["leave_status"] == 1) {
                        $leave_history["leave_status_view"] = "Approved";
                    }else if ($leaveHistoryData["leave_status"] == 2) {
                        $leave_history["leave_status_view"] = "Rejected";
                    }

                    if ($leaveHistoryData["paid_unpaid"] == 0) {
                        $leave_history["paid_unpaid"] = "Paid Leave";
                    }else if ($leaveHistoryData["paid_unpaid"] == 1) {
                        $leave_history["paid_unpaid"] = "Unpaid Leave";
                    }

                    if ($leaveOnData['is_salary_generated'] == '1') { 
                        $leave_history["is_salary_generated"] = true;
                    }else{
                        $leave_history["is_salary_generated"] = false;
                    }

                    if ($leaveHistoryData["half_day_session"] != null) {                    
                        if ($leaveHistoryData["half_day_session"] == 1) {
                            $leave_history["half_day_session"] = "(First Half)";
                        }else if ($leaveHistoryData["half_day_session"] == 2) {
                            $leave_history["half_day_session"] = "(Second Half)";
                        }else{
                            $leave_history["half_day_session"] = "";
                        }
                    }else{
                        $leave_history["half_day_session"] = "";
                    }

                    if ($leaveHistoryData["leave_total_days"] == 1 && $leaveHistoryData["leave_day_type"] == 0) {
                        $leave_history["leave_day_view"] = "Full Day Leave";
                    }else if ($leaveHistoryData["leave_total_days"] == 1 && $leaveHistoryData["leave_day_type"] == 1) {
                        $leave_history["leave_day_view"] = "Half Day Leave";
                    }else{
                        $leave_history["leave_day_view"] = $leaveHistoryData['leave_total_days']." Days Full Leave";
                    }

                    $leave_history["leave_date"] = $leaveHistoryData["leave_start_date"];
                    $leave_history["year"] = date("Y",strtotime($leaveHistoryData["leave_start_date"]));
                    $leave_history["month"] = date("m",strtotime($leaveHistoryData["leave_start_date"]));

                    if ($leaveHistoryData["leave_start_date"] == $leaveHistoryData["leave_end_date"]) {
                        $leave_history["leave_date_view"] = date("D, dS M Y",strtotime($leaveHistoryData["leave_start_date"]));
                    }else{
                       $leave_history["leave_date_view"] = date("D, dS M Y",strtotime($leaveHistoryData["leave_start_date"]))." - ". date("D, dS M Y",strtotime($leaveHistoryData["leave_end_date"]));
                    }

                    $leave_history['approved_by_name'] = "";

                    if ($leaveHistoryData["leave_status"] == 1) {
                        if ($leaveHistoryData['leave_approved_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$leaveHistoryData[leave_approved_by]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $leave_history['approved_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $leave_history['approved_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$leaveHistoryData[leave_approved_by]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $leave_history['approved_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $leave_history['approved_by_name'] = "";
                            }
                        }
                    }


                    $leave_history['rejected_by_name'] = "";

                    if ($leaveHistoryData["leave_status"] == 2) {
                        if ($leaveHistoryData['leave_declined_by_type'] == 0) {
                            $rejectedByQry = $d->selectRow("user_full_name","users_master","user_id = '$leaveHistoryData[leave_declined_by]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                            if ($dataRejectedName['user_full_name']!=null) {
                                $leave_history['rejected_by_name'] = "By, ".$dataRejectedName['user_full_name']."";
                            }else{
                                $leave_history['rejected_by_name'] = "";
                            }

                        }else{
                            $rejectedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$leaveHistoryData[leave_declined_by]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                             if ($dataRejectedName['admin_name']!=null) {
                                $leave_history['rejected_by_name'] = "By, ".$dataRejectedName['admin_name']."";
                            }else{
                                $leave_history['rejected_by_name'] = "";
                            }
                        }
                    }

                    if ($leaveHistoryData['holiday_id'] == null || $leaveHistoryData['holiday_id'] == '') {
                        array_push($response["leave_history"], $leave_history);
                    }

                }
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);

        }else if($_POST['getUsedLeave']=="getUsedLeave" && $user_id!='' && $floor_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $leaveHistoryQry = $d->selectRow("
                leave_master.leave_id,
                leave_master.leave_day_type,
                leave_master.leave_start_date,
                leave_type_master.leave_type_name
                ","
                leave_type_master,
                leave_master
                ","
                leave_type_master.leave_type_id = leave_master.leave_type_id 
                AND leave_type_master.society_id = leave_master.society_id 
                AND leave_master.society_id = '$society_id' 
                AND leave_master.leave_status = '1'
                AND leave_master.user_id = '$user_id' 
                AND leave_master.paid_unpaid = '0'
                AND leave_type_master.leave_type_id = '$leave_type_id'
                ","
                ORDER BY leave_master.leave_start_date ASC");

            $response["leave_history"] = array();            

            if(mysqli_num_rows($leaveHistoryQry)>0){
                
                while($leaveHistoryData = mysqli_fetch_array($leaveHistoryQry)) {

                    $leaveHistoryData = array_map("html_entity_decode", $leaveHistoryData);

                    $leave_history = array();

                    $leave_history["leave_id"] = $leaveHistoryData['leave_id'];
                    $leave_history["leave_type_name"] = $leaveHistoryData['leave_type_name'];

                    if ($leaveHistoryData["leave_day_type"] == 0) {
                        $leave_history["leave_day_view"] = "Full Day";
                    }else if ($leaveHistoryData["leave_day_type"] == 1) {
                        $leave_history["leave_day_view"] = "Half Day";
                    }

                    $leave_history["leave_date"] = date("D, dS F Y",strtotime($leaveHistoryData["leave_start_date"]));

                    array_push($response["leave_history"], $leave_history);
                }
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);

        }else if($_POST['getAllLeaveHistoryNew']=="getAllLeaveHistoryNew" && $user_id!='' && $floor_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true){

            $access_type = $leave_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND leave_master.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND leave_master.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $currentYear = date('Y');

            if ($isAutoLeave == 1) {
                $leaveQry1 = $d->selectRow("
                    leave_master.*,
                    floors_master.floor_id as f_id,
                    floors_master.floor_name,
                    block_master.block_name,
                    users_master.user_id as u_id,
                    users_master.user_profile_pic,
                    users_master.user_designation,
                    users_master.society_id as s_id,
                    users_master.user_full_name,
                    lam.user_total_leave,
                    lam.applicable_leaves_in_month,
                    attendance_master.attendance_id,
                    attendance_master.attendance_date_start,
                    attendance_master.attendance_date_end,
                    attendance_master.punch_in_time,
                    attendance_master.punch_out_time
                    ","
                    users_master,
                    floors_master,
                    block_master,
                    leave_master LEFT JOIN leave_assign_master AS lam ON leave_master.leave_type_id = lam.leave_type_id AND leave_master.user_id = lam.user_id AND lam.assign_leave_year = '$currentYear' LEFT JOIN attendance_master ON attendance_master.user_id = leave_master.user_id AND attendance_master.attendance_date_start = leave_master.leave_start_date AND attendance_master.auto_leave = '1'
                     ","
                    users_master.user_id = leave_master.user_id 
                    AND leave_master.leave_type_id = '0' 
                    AND users_master.delete_status = 0
                    AND users_master.floor_id = floors_master.floor_id 
                    AND users_master.block_id = block_master.block_id 
                    AND leave_master.society_id = '$society_id' 
                    AND leave_master.leave_status = '$leave_status' $appendQuery","ORDER BY leave_master.leave_id DESC $LIMIT_DATA");
            }else{
                $leaveQry1 = $d->selectRow("
                    leave_master.*,
                    block_master.block_name,
                    leave_type_master.*,
                    users_master.user_profile_pic,
                    users_master.user_designation,
                    floors_master.floor_id as f_id,
                    floors_master.floor_name,
                    users_master.user_id as u_id,
                    users_master.society_id as s_id,
                    users_master.user_full_name,
                    lam.user_total_leave,
                    lam.applicable_leaves_in_month
                    ","
                    users_master,
                    block_master,
                    leave_type_master,
                    floors_master,
                    leave_master LEFT JOIN leave_assign_master AS lam ON leave_master.leave_type_id = lam.leave_type_id 
                    AND leave_master.user_id = lam.user_id 
                    AND lam.assign_leave_year = '$currentYear'
                    ","
                    users_master.user_id = leave_master.user_id 
                    AND leave_type_master.leave_type_id = leave_master.leave_type_id
                    AND users_master.floor_id = floors_master.floor_id 
                    AND leave_master.society_id = '$society_id' 
                    AND users_master.delete_status = 0
                    AND users_master.block_id = block_master.block_id 
                    AND leave_master.leave_status = '$leave_status' $appendQuery","ORDER BY leave_master.leave_id DESC $LIMIT_DATA");
            }

            if(mysqli_num_rows($leaveQry1)>0){

                $response["all_leaves"] = array();

                while($leaveOnData = mysqli_fetch_array($leaveQry1)) {

                    $leaveOnData = array_map("html_entity_decode", $leaveOnData);

                    $allLeavehistory = array();

                    $allLeavehistory["leave_id"] = $leaveOnData['leave_id'];

                    if ($leaveOnData['leave_type_id'] == 0) {
                        $allLeavehistory['auto_leave'] = true;
                        $allLeavehistory["leave_type_name"] = "Auto Leave";
                    }else{
                        $allLeavehistory['auto_leave'] = false;
                        $allLeavehistory["leave_type_name"] = $leaveOnData['leave_type_name'];                        
                    }

                    if ($leaveOnData['user_profile_pic'] != '') {
                        $allLeavehistory["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $leaveOnData['user_profile_pic'];
                    } else {
                        $allLeavehistory["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $allLeavehistory["leave_type_id"] = $leaveOnData['leave_type_id'];
                    $allLeavehistory["society_id"] = $leaveOnData['society_id'];
                    $allLeavehistory["user_id"] = $leaveOnData['u_id'];
                    $allLeavehistory["unit_id"] = $leaveOnData['unit_id'];
                    $allLeavehistory["floor_id"] = $leaveOnData['f_id'];
                    $allLeavehistory["user_full_name"] = $leaveOnData['user_full_name'];
                    $allLeavehistory["user_designation"] = $leaveOnData['user_designation'];
                    $allLeavehistory["floor_name"] = $leaveOnData['floor_name'];
                    $allLeavehistory["block_name"] = $leaveOnData['block_name'];
                    $allLeavehistory["leave_reason"] = $leaveOnData['leave_reason'];
                    $allLeavehistory["auto_leave_reason"] = $leaveOnData['auto_leave_reason'];                 
                    $allLeavehistory["leave_admin_reason"] = $leaveOnData['leave_admin_reason'];
                    $allLeavehistory["leave_date"] = $leaveOnData['leave_start_date'];

                    if ($leaveOnData['leave_alternate_number'] == "0") {
                        $allLeavehistory["leave_alternate_number"] = "";
                    }else{
                        $allLeavehistory["leave_alternate_number"] = $leaveOnData['leave_alternate_number'];
                    }


                    $allLeavehistory["attendance_id"] = $leaveOnData['attendance_id'].'';
                    $allLeavehistory["attendance_date_start"] = $leaveOnData['attendance_date_start'].'';
                    $allLeavehistory["attendance_date_end"] = $leaveOnData['attendance_date_end'].'';

                    if ($leaveOnData['punch_in_time'] != '' && $leaveOnData['punch_in_time'] != '00:00:00') {
                        $allLeavehistory["punch_in_time"] = date('h:i A',strtotime($leaveOnData['punch_in_time'])).'';
                    }else{
                        $allLeavehistory["punch_in_time"] = "";
                    }

                    if ($leaveOnData['punch_out_time'] != '' && $leaveOnData['punch_out_time'] != '00:00:00') {
                        $allLeavehistory["punch_out_time"] = date('h:i A',strtotime($leaveOnData['punch_out_time'])).'';
                    }else{
                        $allLeavehistory["punch_out_time"] = "";
                    }

                    if ($leaveOnData['leave_start_date'] < date('Y-m-d')) {
                        $allLeavehistory["is_date_gone"] = true;
                    }else{
                        $allLeavehistory["is_date_gone"] = false;
                    }


                    if ($leaveOnData['leave_task_dependency'] == 1) {
                        $allLeavehistory["leave_task_dependency"] = true;
                    }else{
                        $allLeavehistory["leave_task_dependency"] = false;
                    }

                    $allLeavehistory["year"] = date("Y",strtotime($leaveOnData["leave_start_date"]));
                    $allLeavehistory["month"] = date("m",strtotime($leaveOnData["leave_start_date"]));

                    if ($leaveOnData['leave_attachment']!="") {
                        $allLeavehistory["leave_attachment"] = $base_url."img/leave_attchment/". $leaveOnData['leave_attachment'];
                    }else{
                        $allLeavehistory["leave_attachment"] = "";
                    }
                    $allLeavehistory["leave_handle_dependency"] = $leaveOnData['leave_handle_dependency'];
                    $allLeavehistory["leave_admin_reason"] = $leaveOnData['leave_admin_reason'];
                    $allLeavehistory["leave_status"] = $leaveOnData['leave_status'];

                    $allLeavehistory["paid_unpaid_status"] = $leaveOnData['paid_unpaid'];

                    if ($leaveOnData["paid_unpaid"] == 0) {
                        $allLeavehistory["paid_unpaid"] = "Paid Leave";
                    }else if ($leaveOnData["paid_unpaid"] == 1) {
                        $allLeavehistory["paid_unpaid"] = "Unpaid Leave";
                    }

                    if ($leaveOnData["half_day_session"] != null) {                    
                        if ($leaveOnData["half_day_session"] == 1) {
                            $allLeavehistory["half_day_session"] = "(First Half)";
                        }else if ($leaveOnData["half_day_session"] == 2) {
                            $allLeavehistory["half_day_session"] = "(Second Half)";
                        }else{
                            $allLeavehistory["half_day_session"] = "";
                        }
                    }else{
                        $allLeavehistory["half_day_session"] = "";
                    }

                    if ($leaveOnData["leave_status"] == 0) {
                        $allLeavehistory["leave_status_view"] = "Pending";
                    }else if ($leaveOnData["leave_status"] == 1) {
                        $allLeavehistory["leave_status_view"] = "Approved";
                    }else if ($leaveOnData["leave_status"] == 2) {
                        $allLeavehistory["leave_status_view"] = "Rejected";
                    }

                    if ($leaveOnData["leave_total_days"] == 1 && $leaveOnData["leave_day_type"] == 0) {
                        $allLeavehistory["leave_day_view"] = "Full Day Leave";
                    }else if ($leaveOnData["leave_total_days"] == 1 && $leaveOnData["leave_day_type"] == 1) {
                        $allLeavehistory["leave_day_view"] = "Half Day Leave";
                    }else{
                        $allLeavehistory["leave_day_view"] = $leaveOnData['leave_total_days']." Days Full Leave";
                    }

                    $allLeavehistory['leave_day_type_status'] = $leaveOnData['leave_day_type'];

                    if ($leaveOnData["leave_start_date"] == $leaveOnData["leave_end_date"]) {
                        $allLeavehistory["leave_date_view"] = date("D, dS M Y",strtotime($leaveOnData["leave_start_date"]));
                    }else{
                       $allLeavehistory["leave_date_view"] = date("D, dS M Y",strtotime($leaveOnData["leave_start_date"]))." - ". date("D, dS M Y",strtotime($leaveOnData["leave_end_date"]));
                    }

                    if ($leaveOnData["leave_created_date"] == $leaveOnData["leave_created_date"]) {
                        $allLeavehistory["leave_requested_date"] = date("D, dS M Y",strtotime($leaveOnData["leave_created_date"]));
                    }else{
                       $allLeavehistory["leave_requested_date"] = "";
                    }




                    $allLeavehistory['approved_by_name'] = "";

                    if ($leaveOnData["leave_status"] == 1) {
                        if ($leaveOnData['leave_approved_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$leaveOnData[leave_approved_by]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $allLeavehistory['approved_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $allLeavehistory['approved_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$leaveOnData[leave_approved_by]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $allLeavehistory['approved_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $allLeavehistory['approved_by_name'] = "";
                            }
                        }
                    }


                    $allLeavehistory['rejected_by_name'] = "";

                    if ($leaveOnData["leave_status"] == 2) {
                        if ($leaveOnData['leave_declined_by_type'] == 0) {
                            $rejectedByQry = $d->selectRow("user_full_name","users_master","user_id = '$leaveOnData[leave_declined_by]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                            if ($dataRejectedName['user_full_name']!=null) {
                                $allLeavehistory['rejected_by_name'] = "By, ".$dataRejectedName['user_full_name']."";
                            }else{
                                $allLeavehistory['rejected_by_name'] = "";
                            }

                        }else{
                            $rejectedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$leaveOnData[leave_declined_by]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                             if ($dataRejectedName['admin_name']!=null) {
                                $allLeavehistory['rejected_by_name'] = "By, ".$dataRejectedName['admin_name']."";
                            }else{
                                $allLeavehistory['rejected_by_name'] = "";
                            }
                        }
                    }

                   /* $assignedLeaveQry = $d->select("leave_type_master,leave_assign_master","leave_type_master.leave_type_id = leave_assign_master.leave_type_id 
                        AND leave_type_master.society_id = leave_assign_master.society_id 
                        AND leave_assign_master.society_id = '$society_id' 
                        AND leave_assign_master.user_id = '$user_id'
                        AND leave_assign_master.assign_leave_year = '$currentYear'");*/

                    $totalUsedLeaveByTypeFullDay = $d->count_data_direct("leave_id","leave_master",
                        "society_id = '$leaveOnData[society_id]' 
                        AND user_id = '$leaveOnData[user_id]' 
                        AND leave_type_id = '$leaveOnData[leave_type_id]' 
                        AND leave_status = '1' 
                        AND paid_unpaid = '0' 
                        AND leave_day_type = '0'");

                    $totalUsedLeaveByTypeHalfDay = $d->count_data_direct("leave_id","leave_master",
                        "society_id = '$leaveOnData[society_id]' 
                        AND user_id = '$leaveOnData[user_id]' 
                        AND leave_type_id = '$leaveOnData[leave_type_id]' 
                        AND leave_status = '1' 
                        AND paid_unpaid = '0'  
                        AND leave_day_type = '1'");

                    $totalPayout = $d->sum_data("no_of_payout_leaves","leave_payout_master",
                        "user_id = '$leaveOnData[user_id]' 
                        AND leave_type_id = '$leaveOnData[leave_type_id]'
                        AND leave_payout_year = '$currentYear' 
                        AND is_leave_carry_forward = '0'");

                    while($row=mysqli_fetch_array($totalPayout)){
                        $data = $row['SUM(no_of_payout_leaves)'];

                        if ($data == "") {
                            $data = 0;
                        }

                        $totalPayout = $data;
                    }

                    if ($leaveOnData['user_total_leave'] !=null && $leaveOnData['user_total_leave'] > 0) {
                        $userTotalTypeLeave = $leaveOnData['user_total_leave'];
                        $totalHalfDay = $totalUsedLeaveByTypeHalfDay/2;
                        $totalLeaves = $totalUsedLeaveByTypeFullDay + $totalHalfDay + $totalPayout;
                    }


                    $allLeavehistory["user_total_leave"] = $userTotalTypeLeave."";
                    $allLeavehistory["user_total_used_leave"] = $totalLeaves."";

                    $totalRemLeave = $userTotalTypeLeave - $totalLeaves;

                    if($totalRemLeave < 0){
                        $allLeavehistory["remaining_leave"] = "0";
                    }else{
                        $allLeavehistory["remaining_leave"] = $totalRemLeave."";
                    }

                    if ($leaveOnData['is_salary_generated'] == '1') {
                        $allLeavehistory['is_salary_generated'] = true;
                    }else{
                        $allLeavehistory['is_salary_generated'] = false;                    
                    }

                    array_push($response["all_leaves"], $allLeavehistory);                    
                }   
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="No data found";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getLeaveTypeList']=="getLeaveTypeList" && $user_id!='' && $floor_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true){  

            if ($currentYear == null || $currentYear == "") {
                $currentYear = date('Y');
            } 


            $assignedLeaveQry = $d->select("leave_type_master,leave_assign_master","leave_type_master.leave_type_id = leave_assign_master.leave_type_id 
                AND leave_type_master.society_id = leave_assign_master.society_id 
                AND leave_assign_master.society_id = '$society_id' 
                AND leave_assign_master.user_id = '$user_id'
                AND leave_assign_master.assign_leave_year = '$currentYear' 
                AND leave_type_master.leave_type_active_status='0' 
                AND leave_type_master.leave_type_delete = '0'");

            if(mysqli_num_rows($assignedLeaveQry)>0){

                $totalLeavesQry = $d->sum_data("user_total_leave","leave_assign_master",
                "society_id = '$society_id' 
                AND user_id = '$user_id'
                AND leave_assign_active_status = '0'");

                while($totalData = mysqli_fetch_array($totalLeavesQry)){
                    $totalLeaves=$totalData['SUM(user_total_leave)'];
                }

                $response['total_leaves'] = $totalLeaves."";

                $totalUsedLeaveFullDay = $d->count_data_direct("leave_id","leave_master",
                            "society_id = '$society_id' 
                            AND user_id = '$user_id' 
                            AND leave_status = '1' 
                            AND paid_unpaid = '0' 
                            AND leave_day_type = '0'");

                $totalUsedLeaveHalfDay = $d->count_data_direct("leave_id","leave_master",
                            "society_id = '$society_id' 
                            AND user_id = '$user_id' 
                            AND leave_status = '1' 
                            AND paid_unpaid = '0' 
                            AND leave_day_type = '1'");

                $totalHalfDay = $totalUsedLeaveHalfDay/2;

                $totalUsedLeave = $totalUsedLeaveFullDay + $totalHalfDay;

                $response['total_used_leaves'] = $totalUsedLeave."";

                $response['total_remaining_leaves'] = $totalLeaves - $totalUsedLeave."";
                
                $response["leave_types"] = array();

                while($assignedLeaveData=mysqli_fetch_array($assignedLeaveQry)) {

                    $assignedLeaveData = array_map("html_entity_decode", $assignedLeaveData);

                    $leave_types = array();

                    $userTotalTypeLeave = $assignedLeaveData['user_total_leave'];

                    $leave_types["leave_type_id"] = $assignedLeaveData['leave_type_id'];
                    $leave_types["leave_type_name"] = $assignedLeaveData['leave_type_name'];
                    $leave_types["applicable_leaves_in_month"] = $assignedLeaveData['applicable_leaves_in_month'];
                    $leave_types["user_total_leave"] = $userTotalTypeLeave."";

                    $totalUsedLeaveByTypeFullDay = $d->count_data_direct("leave_id","leave_master",
                        "society_id = '$society_id' 
                        AND user_id = '$user_id' 
                        AND leave_type_id = '$assignedLeaveData[leave_type_id]' 
                        AND leave_status = '1' 
                        AND paid_unpaid = '0' 
                        AND leave_day_type = '0'");

                    $totalUsedLeaveByTypeHalfDay = $d->count_data_direct("leave_id","leave_master",
                        "society_id = '$society_id' 
                        AND user_id = '$user_id' 
                        AND leave_type_id = '$assignedLeaveData[leave_type_id]' 
                        AND leave_status = '1' 
                        AND paid_unpaid = '0'  
                        AND leave_day_type = '1'");

                    $totalPayout = $d->sum_data("no_of_payout_leaves","leave_payout_master",
                        "user_id = '$user_id' 
                        AND leave_type_id = '$assignedLeaveData[leave_type_id]' 
                        AND leave_payout_year = '$currentYear' 
                        AND is_leave_carry_forward = '0'");

                    while($row=mysqli_fetch_array($totalPayout)){
                        $data = $row['SUM(no_of_payout_leaves)'];

                        if ($data == "") {
                            $data = 0;
                        }

                        $totalPayout = $data;
                    }

                    $totalCarryForward = $d->sum_data("no_of_payout_leaves","leave_payout_master",
                        "user_id = '$user_id' 
                        AND leave_type_id = '$assignedLeaveData[leave_type_id]' 
                        AND leave_payout_year = '$currentYear' 
                        AND is_leave_carry_forward = '1'");

                    while($row=mysqli_fetch_array($totalCarryForward)){
                        $data = $row['SUM(no_of_payout_leaves)'];

                        if ($data == "") {
                            $data = 0;
                        }

                        $totalCarryForward = $data;
                    }

                    $totalHalfDay = $totalUsedLeaveByTypeHalfDay/2;

                    $totalLeaves = $totalUsedLeaveByTypeFullDay + $totalHalfDay;

                    $finalRemainingLeave = $userTotalTypeLeave - ($totalLeaves + $totalPayout + $totalCarryForward);

                    $leave_types["user_total_used_leave"] = $totalLeaves."";
                    $leave_types["remaining_leave"] = $finalRemainingLeave."";
                    $leave_types["totalPayout"] = $totalPayout."";
                    $leave_types["totalCarryForward"] = $totalCarryForward."";

                    array_push($response["leave_types"], $leave_types);
                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Something went wrong";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getLeaveBalanceForAutoLeave']=="getLeaveBalanceForAutoLeave" && $user_id!=''   && $leave_date!=''  && $leave_type_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $selectedLeaveDateMonth = date('m-Y',strtotime($leave_date));

            $salaryQ = $d->count_data_direct("salary_slip_id","salary_slip_master","user_id = '$user_id' AND society_id = '$society_id' AND salary_month_name = '$selectedLeaveDateMonth'");

            if ($salaryQ > 0) {
                $response['is_salary_generated'] = true;
            }else{
                $response['is_salary_generated'] = false;                
            }

            $attendanceQ = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start = '$leave_date' AND attendance_status != '2'");

            if ($attendanceQ > 0) {
                $response['has_attendance'] = true;
            }else{
                $response['has_attendance'] = false;                
            }
            
            $user=$d->selectRow("users_master.shift_time_id,users_master.floor_id,user_employment_details.joining_date","users_master,user_employment_details","users_master.user_id=user_employment_details.user_id AND users_master.user_id='$user_id'");
            
            $userData = mysqli_fetch_array($user);
            $floor_id = $userData['floor_id'];
        
            $i=1;
            $j=1;
            $total_month = '12';
            $currentYear = date('Y');

            $calQ = $d->selectRow("leave_calculation","leave_default_count_master","leave_default_count_master.floor_id = '$floor_id' AND leave_default_count_master.leave_type_id = '$leave_type_id' AND leave_default_count_master.leave_year=DATE_FORMAT('$leave_date', '%Y')");
            $calData = mysqli_fetch_array($calQ);
            $leave_calculation = $calData['leave_calculation'];
            if($currentYear == date('Y', strtotime($userData['joining_date']))){
                $i=date('m', strtotime($userData['joining_date']));
                $j=date('m', strtotime($userData['joining_date']));
                $total_month = 12 - date('m', strtotime($userData['joining_date'])) + 1;
            }

            $lq = $d->selectRow("leave_start_date,
            (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id='$leave_type_id' AND leave_payout_year=DATE_FORMAT('$leave_date', '%Y')) AS pay_and_carry_forward_leave,
            (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
            AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND leave_type_id='$leave_type_id') AS total_half_day_leave,
            (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
            AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND leave_type_id='$leave_type_id') AS total_full_day_leave,(SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
            AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_start_date, '%Y-%m')=DATE_FORMAT('$leave_date', '%Y-%m') AND leave_type_id='$leave_type_id') AS use_half_day_leave,
            (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
            AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_start_date, '%Y-%m')=DATE_FORMAT('$leave_date', '%Y-%m') AND leave_type_id='$leave_type_id') AS use_full_day_leave", 
            "leave_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_status=1 AND paid_unpaid=0 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y')","ORDER BY leave_master.leave_start_date DESC LIMIT 1");
            $leaveData = mysqli_fetch_assoc($lq);
        
            $use_leave = $leaveData['use_full_day_leave'] + ($leaveData['use_half_day_leave']/2);  

            if($leave_calculation == 1){

                $max_leave_date = date("m", strtotime($leaveData['leave_start_date']));
                $leave_month = date("m", strtotime($leave_date));

                if($max_leave_date>$leave_month){
                    $loop_count = $max_leave_date;
                }else{
                    $loop_count = $leave_month;
                }

                $monthly_leave_array = array();
                $total_remaining = 0; 
                $negative_remaining = 0;

                for ($i; $i <= $loop_count; $i++) { 
                    $userLeaveq = $d->selectRow("user_total_leave,
                    applicable_leaves_in_month,
                    (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id='$leave_type_id' AND leave_payout_year=DATE_FORMAT('$leave_date', '%Y')) AS pay_and_carry_forward_leave,
                    (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                    AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND MONTH(leave_start_date)='$i' AND leave_type_id='$leave_type_id') AS total_half_day_leave, 
                    (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                    AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND MONTH(leave_start_date)='$i' AND leave_type_id='$leave_type_id') AS total_full_day_leave", 
                    "leave_assign_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND assign_leave_year=DATE_FORMAT('$leave_date', '%Y')");

                    $data = mysqli_fetch_assoc($userLeaveq);
                    $applicable_leaves_in_month = $data['applicable_leaves_in_month'];
                    $user_total_leave = $data['user_total_leave'];
                    $leave_avg = ($data['user_total_leave'] - $data['pay_and_carry_forward_leave'])/$total_month;
                    $avg_leave_str = explode(".",$leave_avg);
                    $explode01 = $avg_leave_str[1][0];

                    if($explode01=='' || $explode01<=4){
                        $explode02 = 0;
                    }elseif($explode01<=9 || $explode01>=5){
                        $explode02 = 5;
                    }

                    $apply_leave = $avg_leave_str[0].'.'.$explode02;

                    $used_in_month = $data['total_full_day_leave'] + ($data['total_half_day_leave']/2);
                    $remaining = $leave_avg - $used_in_month;

                    if($remaining<0){
                        $negative_remaining += $remaining;
                    }

                    $leave_array = array('avg_leave' => $leave_avg,
                                        'used_in_month' => $used_in_month,
                                        'remaining' => $remaining);

                    $monthly_leave_array[$i] = $leave_array;
                }

                $final_remaining = 0; 

                for ($j; $j <= $loop_count; $j++) { 

                    $result = $monthly_leave_array[$j]['remaining'];

                    if($monthly_leave_array[$j]['remaining']>0 && $negative_remaining<0){
                        $result = $monthly_leave_array[$j]['remaining']+($negative_remaining);
                        $negative_remaining = $result;
                        if($negative_remaining>=0){
                            $negative_remaining = 0;
                        }
                        if($result<0){
                            $result = 0;
                        }
                    }else if($monthly_leave_array[$j]['remaining']<0){
                        $result = $negative_remaining;
                        if($result<0){
                            $result = 0;
                        }
                    }
                    
                    $monthly_leave_array[$j]['remaining'] = $result;
                    $final_remaining += $monthly_leave_array[$j]['remaining'];
                    $total_remaining = $final_remaining;
                }
                $available_paid_leave = $total_remaining;

                if($use_leave>=$applicable_leaves_in_month){
                    $available_paid_leave = 0;
                }else{
                    if($available_paid_leave > $applicable_leaves_in_month){
                        $available_paid_leave = $applicable_leaves_in_month;
                    }
                    if($available_paid_leave<0){
                        $available_paid_leave = 0;
                    }
                }
            }else{
                $leave_month = date("m", strtotime($leave_date));
                $userLeaveq = $d->selectRow("user_total_leave,
                    applicable_leaves_in_month,
                    (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id='$leave_type_id' AND leave_payout_year=DATE_FORMAT('$leave_date', '%Y')) AS pay_and_carry_forward_leave,
                    (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                    AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND leave_type_id='$leave_type_id') AS total_half_day_leave, 
                    (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                    AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND leave_type_id='$leave_type_id') AS total_full_day_leave", 
                    "leave_assign_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND assign_leave_year=DATE_FORMAT('$leave_date', '%Y')");

                $data = mysqli_fetch_assoc($userLeaveq);
                
                $applicable_leaves_in_month = $data['applicable_leaves_in_month'];
                $user_total_leave = $data['user_total_leave'];
                $available_paid_leave = $user_total_leave;

                $use_leave = $data['total_full_day_leave'] + ($data['total_half_day_leave']/2); 
                
                if($use_leave>=$available_paid_leave){
                    $available_paid_leave = 0;
                }else{
                    if($available_paid_leave > $applicable_leaves_in_month){
                        $available_paid_leave = $applicable_leaves_in_month;
                    }
                    if($available_paid_leave<0){
                        $available_paid_leave = 0;
                    }
                }
            }        

            $remaining_leave = $user_total_leave - ($leaveData['total_full_day_leave'] + ($leaveData['total_half_day_leave']/2))- $leaveData['pay_and_carry_forward_leave'];

            $available_paid_leave_str = explode(".",$available_paid_leave);
            $explode01 = $available_paid_leave_str[1][0];
            if($explode01=='' || $explode01<=4){
                $explode02 = 0;
            }elseif($explode01<=9 || $explode01>=5){
                $explode02 = 5;
            }
            $available_paid_leave = $available_paid_leave_str[0].'.'.$explode02;
            $main_data['available_paid_leave'] = $available_paid_leave;
            $main_data['remaining_leave'] = $remaining_leave;

            if ($main_data) {
                $response["leave"] = $main_data;
                $response["message"] = "Leave Data";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Leave Data";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['leaveCalendar']=="leaveCalendar" && $user_id!=''  && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shift_time_id = $sdata['shift_time_id'];
            }

            $qrySalary = $d->selectRow("salary_slip_id,salary_start_date,salary_end_date","salary_slip_master","user_id = '$user_id' AND society_id = '$society_id' AND salary_start_date = '$month_start_date' AND salary_end_date = '$month_end_date'");

            if (mysqli_num_rows($qrySalary) > 0) {
                $salaryData = mysqli_fetch_array($qrySalary);
                $salary_slip_id = $salaryData['salary_slip_id'];
                $response['is_salary_generated'] = true;
                $response['salary_slip_id'] = $salaryData['salary_slip_id'];
                $response['salary_start_date'] = $salaryData['salary_start_date'];
                $response['salary_end_date'] = $salaryData['salary_end_date'];
            }else{
                $response['is_salary_generated'] = false;
                $response['salary_slip_id'] = "";
                $response['salary_start_date'] = "";
                $response['salary_end_date'] = "";
            }

            $response['monthly_history'] = array();
            //$response['week'] = array();

            $datesArrayOfMonth = range_date($month_start_date,$month_end_date);

            $holidayAry = array();
            $weekAry = array();

            $currdt= strtotime($month_start_date);
            $nextmonth=strtotime($month_start_date."+1 month");
            $i=0;
            $flag=true;

            $totalMonthHours = 0;

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);

            // Week
        
            for ($x = 0; $x < count($startarr); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";

                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);

                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $dayN= $date->format('l');
                    $dayDate = $date->format('Y-m-d');

                        // code...
                        $days = array();
                        $hDates = array();
                        $days['day'] = $dayN;
                        $days['date'] = $dayDate;
                        $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate)) ;
                        
                        $holidayQry = $d->selectRow("holiday_start_date,holiday_name","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' ");

                        $holidayData = mysqli_fetch_array($holidayQry);

                        $x1 = $x+1;
                        // holiday off
                        if (mysqli_num_rows($holidayQry)>0) {
                            array_push($holidayAry,$dayDate);
                            $minusHoliday += $perdayHours;
                        } else {
                            // full day off
                            if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                                array_push($weekAry,$dayDate);
                                $minusWeekOffHR += $perdayHours;
                            }

                            // alternate week day off
                            if ($shiftData['has_altenate_week_off'] == "1") {  
                                if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                    in_array($x1,$alternateWeekOff)) {
                                    array_push($weekAry,$dayDate);
                                    $minusAlternateWeekOffDays += $perdayHours;
                                }
                            }
                        }
                        array_push($daysArray['days'],$days);
                }
            }

            // Month

            $totalWeekOff = 0;
            $totalHolidays = 0;
 
            for ($j = 0; $j < count($datesArrayOfMonth); $j++) {
                
                $dates = array();

                $monthDate = $datesArrayOfMonth[$j];
                $month = date('Y-m',strtotime($monthDate));
                $dates['date'] = $monthDate;
                $day_pos_new =  date('w', strtotime($monthDate));

                $dates['holiday'] = false;
                $dates['week_off'] = false;
                $dates['leave_applied'] = false;

                if (in_array($monthDate,$holidayAry)) {
                    $dates['holiday'] = true;
                    $totalHolidays += 1;
                }else{
                    if (in_array($monthDate,$weekAry)) {
                        $dates['week_off'] = true;
                        $totalWeekOff += 1;
                        $isDateGone = "false";
                    }
                }

                $holidayNameQry = $d->selectRow("holiday_name,holiday_description","holiday_master","society_id = '$society_id' AND holiday_start_date = '$monthDate'");

                $dates["holiday_name"] = "";

                if (mysqli_num_rows($holidayNameQry) > 0) {

                    $holidayNameData = mysqli_fetch_array($holidayNameQry);

                    $dates["holiday_name"] = $holidayNameData['holiday_name'];
                }

                $leaveAppliedQry = $d->selectRow("leave_master.*,leave_type_master.leave_type_name","leave_master,leave_type_master","leave_master.society_id = '$society_id' AND leave_master.leave_start_date = '$monthDate' AND leave_master.user_id = '$user_id' AND leave_master.leave_type_id = leave_type_master.leave_type_id AND (leave_master.leave_status = '1' OR leave_master.leave_status = '0')");


                if (mysqli_num_rows($leaveAppliedQry) > 0) {

                    $leaveAppliedData = mysqli_fetch_array($leaveAppliedQry);

                    $dates['leave_applied'] = true;

                    $dates["leave_type_name"] = $leaveAppliedData['leave_type_name'];
                    $dates["leave_reason"] = $leaveAppliedData['leave_reason'];
                    $dates["leave_start_date"] = $leaveAppliedData['leave_start_date'];

                    $paidUnpaid = $leaveAppliedData['paid_unpaid'];
                    $leaveDay = $leaveAppliedData['leave_day_type'];
                    $leaveStatus = $leaveAppliedData['leave_status'];

                    if ($paidUnpaid == '0') {
                        $dates["paid_unpaid"] = "Paid";
                    }else{
                        $dates["paid_unpaid"] = "Unpaid";
                    }

                    
                    if ($leaveDay == '0') {
                        $dates["leave_day_type"] = "Full Day";
                    }else{
                        $dates["leave_day_type"] = "Half Day";
                    }

                    
                    if ($leaveStatus == '0') {
                        $dates["leave_status"] = "Pending";
                    }else{
                        $dates["leave_status"] = "Approved";
                    }
                }

                array_push($response['monthly_history'],$dates);
            }

            echo json_encode($response);
        
        }else if($_POST['changeAutoLeave']=="changeAutoLeave" && $user_id!='' && $leave_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('leave_type_id',$leave_type_id);
            $m->set_data('paid_unpaid',$paid_unpaid);

            $a = array(
                'leave_type_id'=>$m->get_data('leave_type_id'),
                'paid_unpaid'=>$m->get_data('paid_unpaid'),
            );

            $changeLeaveStatusQry = $d->update("leave_master",$a,"leave_id = '$leave_id' AND user_id = '$user_id'");

            if ($changeLeaveStatusQry == true) {

                if (!isset($leave_date)) {
                    $qq = $d->selectRow("leave_start_date","leave_master","leave_id = '$leave_id' AND user_id = '$user_id'");
                    $qqData = mysqli_fetch_array($qq);

                    $leave_date = $qqData['leave_start_date'];
                }

                $d->insert_myactivity($user_id, $society_id, "0", $user_name, "Auto Leave Edited", "leave_tracker.png");

                $m->set_data('auto_leave',"0");
                $m->set_data('is_leave',"0");
                $m->set_data('extra_day_leave_type',"0");

                $a1 = array(
                    'auto_leave'=>$m->get_data('auto_leave'),
                    'is_leave'=>$m->get_data('is_leave'),
                    'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                );

                $d->update("attendance_master",$a1,"attendance_date_start = '$leave_date' AND user_id = '$user_id'");

                $response["message"] = "Auto Leave Data Changed";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function createRange($start, $end, $format = 'Y-m-d') {
    $start  = new DateTime($start);
    $end    = new DateTime($end);
    $invert = $start > $end;

    $dates = array();
    $dates[] = $start->format($format);
    while ($start != $end) {
        $start->modify(($invert ? '-' : '+') . '1 day');
        $dates[] = $start->format($format);
    }
    return $dates;
}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}
?>