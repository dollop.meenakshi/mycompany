<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
            if($_POST['getFeed']=="getFeed" && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true){

                if ($unit_id!='' && $unit_id!=0) {
                     $quc=$d->select("unit_master","unit_id='$unit_id' AND timline_access_denied=1");
                     if (mysqli_num_rows($quc)>0) {
                        $response["message"] = "Access Denied..!";
                        $response["status"] = "202";
                        echo json_encode($response);
                        exit();
                     }

                  }

                $timeline_user_save_master = $d->selectRow("feed_id","news_feed_saved", "user_id='$user_id'", "");
                $saved_timeline_array = array('0');
                while ($timeline_user_save_master_data = mysqli_fetch_array($timeline_user_save_master)) {
                    $saved_timeline_array[] = $timeline_user_save_master_data['feed_id'];
                }
                
                // find type
                $fq=$d->select("users_master","user_id='$user_id' AND society_id='$society_id'");
                $ownerData=mysqli_fetch_array($fq);
                $userType= $ownerData['user_type'];

                $totalFeed=$d->count_data_direct("feed_id","news_feed","society_id='$society_id' AND status = 0 ");
                

                $qnotification=$d->select("news_feed","society_id='$society_id' AND status = 0 ","ORDER BY feed_id DESC LIMIT $limit_feed,10");

                $totalSocietyFeedLimit= $d->count_data_direct("feed_id","news_feed","society_id='$society_id' AND status = 0 ");


                if(mysqli_num_rows($qnotification)>0){

                    $response["feed"] = array();
                    while($data_notification=mysqli_fetch_array($qnotification)) {
                      
                        if ($data_notification['user_id']!=0) {
                            $qu=$d->selectRow("user_profile_pic,tenant_view,user_full_name,user_designation,user_mobile,public_mobile,country_code","users_master","user_id='$data_notification[user_id]' AND society_id='$society_id'");
                            $userData=mysqli_fetch_array($qu);
                            $userProfile=$base_url."img/users/recident_profile/".$userData['user_profile_pic'];
                            $user_full_name=$userData['user_full_name'];
                            $company_name = html_entity_decode($userData['user_designation']);
                            if ($userData['public_mobile'] == 0 && $userData['user_mobile'] != 0) {
                                $user_mobile_view = $userData['country_code'] .' '.$userData['user_mobile'];
                            } else {
                                $user_mobile_view = "";
                            } 
                        } else {
                            $userProfile=$base_url."img/newFav.png";
                            $user_full_name="MyCo";
                            $company_name = "Admin";
                            $user_mobile_view = "";
                        }

                        $feed = array(); 
                        $feed["feed_id"]=$data_notification['feed_id'];
                        $feed["society_id"]=$data_notification['society_id'];
                        $feed["feed_msg"]=html_entity_decode($data_notification['feed_msg']);
                        $feed["user_name"]=$user_full_name;
                        $feed["block_name"]=$company_name;
                        $feed["user_id"]=$data_notification['user_id'];
                        $feed["user_profile_pic"]=$userProfile;
                        $feed["user_mobile"]=$user_mobile_view;
                        $feed_id = $data_notification['feed_id'];


                        $feed_img=explode('~',$data_notification['feed_img']);
                        $feed_img_height=explode('~',$data_notification['feed_img_height']);
                        $feed_img_width=explode('~',$data_notification['feed_img_width']);

                        $unique=array_unique($feed_img);

                        $feed["feed_img"]=array();
                        
                        for ($iFeed=0; $iFeed < count($unique) ; $iFeed++) { 
                            $feed_img=array();
                            $feed_img["feed_img"]=$base_url."img/users/recident_feed/".$unique[$iFeed];
                            $feed_img["feed_height"]=intval($feed_img_height[$iFeed]).'';
                            $feed_img["feed_width"]=intval($feed_img_width[$iFeed]).'';
                            array_push($feed["feed_img"], $feed_img); 

                        }


                        $feed["feed_type"]=$data_notification['feed_type'];
                        $feed["feed_video"]=$base_url."img/users/recident_feed/".$data_notification['feed_video'];
                        $feed["video_thumb"]=$base_url."img/users/recident_feed/".$data_notification['feed_img'];
                        $feed["modify_date"]=date("h:i A, dS M Y", strtotime($data_notification['modify_date']));
                        if(in_array($data_notification['feed_id'], $saved_timeline_array)){
                            $feed["is_saved"] = '1';
                        }else {
                            $feed["is_saved"] = '0';
                        }
                        


                       $qlike=$d->select("news_like,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_like.feed_id='$feed_id' AND news_like.user_id=users_master.user_id AND news_like.active_status=0");
                       $totalLikes = mysqli_num_rows($qlike);
                       $feed["totalLikes"]=$totalLikes."";

                        if(mysqli_num_rows($qlike)>0){
                            $feed["like"] = array();
                            $feed["like_status"]="0";  

                            while($data_like=mysqli_fetch_array($qlike)) {

                                $like = array(); 

                                $like["like_id"]=$data_like['like_id'];
                                $like["feed_id"]=$data_like['feed_id'];
                                $like["user_id"]=$data_like['user_id'];

                               
                                if ($user_id==$data_like['user_id']) {

                                    $feed["like_status"]="1";

                                }else{
                                    if ($feed["like_status"]=="1") {
                                  
                                    }else{

                                   $feed["like_status"]="0"; 
                                    }  
                                }
                                $like["user_name"]=$data_like['user_full_name'];
                                $like["block_name"]=html_entity_decode($data_like['user_designation']);
                                $like["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_like['user_profile_pic'];
                                $like["modify_date"]=$data_like['modify_date'].'';

                                array_push($feed["like"], $like); 
                            }
                        }

                        else{

                             $feed["like_status"]="0";  
                        }

                        $commentCount= $d->count_data_direct("comments_id","news_comments","feed_id='$feed_id'  AND society_id!=0 AND user_id='$user_id'");

                        $feed['commentCount'] = $commentCount;


                        $qcomment=$d->select("news_comments,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_comments.feed_id='$feed_id' AND news_comments.user_id=users_master.user_id AND news_comments.parent_comments_id=0","ORDER BY news_comments.comments_id DESC");

                        $feed["comment"] = array();
                        if(mysqli_num_rows($qcomment)>0){

                            while($data_comment=mysqli_fetch_array($qcomment)) {

                                $comment = array(); 

                                $comment["comments_id"]=$data_comment['comments_id'];
                                $comment["feed_id"]=$data_comment['feed_id'];
                                $comment["msg"]=html_entity_decode($data_comment['msg']);
                                $comment["user_name"]=$data_comment['user_full_name'];
                                $comment["block_name"]=html_entity_decode($data_comment['user_designation']);
                                $comment["user_id"]=$data_comment['user_id'];
                                $comment["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_comment['user_profile_pic'];
                                if ($user_id==$data_comment['user_id']) {
                                    $feed["comment_status"]="1";
                                }else{
                                   $feed["comment_status"]="0";   
                                }
                                $comment["modify_date"]=time_elapsed_string($data_comment['modify_date']);


                                $comment["sub_comment"] = array();
                                $q4=$d->select("news_comments,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_comments.feed_id='$feed_id' AND news_comments.user_id=users_master.user_id AND news_comments.parent_comments_id='$data_comment[comments_id]'","ORDER BY news_comments.comments_id DESC");
                                    while ($subCommentData=mysqli_fetch_array($q4)) {
                                        $sub_comment = array();
                                        $sub_comment["comments_id"]=$subCommentData['comments_id'];
                                        $sub_comment["feed_id"]=$subCommentData['feed_id'];
                                        $sub_comment["msg"]=html_entity_decode($subCommentData['msg']);
                                        $sub_comment["user_name"]=$subCommentData['user_full_name'];
                                        $sub_comment["block_name"]=$subCommentData['user_designation'];
                                        $sub_comment["user_id"]=$subCommentData['user_id'];
                                        $sub_comment["user_profile_pic"]=$base_url."img/users/recident_profile/".$subCommentData['user_profile_pic'];
                                        $sub_comment["modify_date"]=time_elapsed_string($subCommentData['modify_date']);

                                        array_push($comment["sub_comment"], $sub_comment); 
                                  }
                                array_push($feed["comment"], $comment); 
                            }
                        }
                        
                        if($commentCount>0) {
                            $feed["comment_status"]="1";   
                        } else {
                            $feed["comment_status"]="0";   
                        }


                        if ($userType==1 && $userData['tenant_view']==1) {
               
                        } else {
                            array_push($response["feed"], $feed); 
                        } 
                    }

                    
                     $q2222 = $d->select("user_notification", "user_id='$user_id' AND  notification_type=1 AND read_status=0");


                    $response["unread_notification"]=mysqli_num_rows($q2222);
                    $response["pos1"]=$pos1+0;
                    $response["totalSocietyFeedLimit"]=''.$totalSocietyFeedLimit;
                    $response["message"]="Get Feeds success.";
                    $response["totalFeed"]=$totalFeed;
                    $response["video_duration"]=30;
                    $response["status"]="200";
                    echo json_encode($response);

                } else{
                    $response["message"]="No Feeds Found";
                    $response["video_duration"]=30;
                    $response["status"]="201";
                    echo json_encode($response);
                }
            }else if($_POST['getFeedNew']=="getFeedNew" && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true){

                if ($unit_id!='' && $unit_id!=0) {
                     $quc=$d->select("unit_master","unit_id='$unit_id' AND timline_access_denied=1");
                     if (mysqli_num_rows($quc)>0) {
                        $response["message"] = "Access Denied..!";
                        $response["status"] = "202";
                        echo json_encode($response);
                        exit();
                     }

                  }

                $timeline_user_save_master = $d->selectRow("feed_id","news_feed_saved", "user_id='$user_id'", "");
                $saved_timeline_array = array('0');
                while ($timeline_user_save_master_data = mysqli_fetch_array($timeline_user_save_master)) {
                    $saved_timeline_array[] = $timeline_user_save_master_data['feed_id'];
                }
                
                // find type
                $fq=$d->select("users_master","user_id='$user_id' AND society_id='$society_id'");
                $ownerData=mysqli_fetch_array($fq);
                $userType= $ownerData['user_type'];

                $totalFeed=$d->count_data_direct("feed_id","news_feed","society_id='$society_id' AND status = 0 ");
                

                $qnotification=$d->select("news_feed","society_id='$society_id' AND status = 0 ","ORDER BY feed_id DESC LIMIT $limit_feed,10");

                $totalSocietyFeedLimit= $d->count_data_direct("feed_id","news_feed","society_id='$society_id' AND status = 0 ");


                if(mysqli_num_rows($qnotification)>0){

                    $response["feed"] = array();
                    while($data_notification=mysqli_fetch_array($qnotification)) {
                      
                        if ($data_notification['user_id']!=0) {
                            $qu=$d->selectRow("user_profile_pic,tenant_view,user_full_name,user_designation,user_mobile,public_mobile,country_code","users_master","user_id='$data_notification[user_id]' AND society_id='$society_id'");
                            $userData=mysqli_fetch_array($qu);
                            $userProfile=$base_url."img/users/recident_profile/".$userData['user_profile_pic'];
                            $user_full_name=$userData['user_full_name'];
                            $company_name = html_entity_decode($userData['user_designation']);
                            if ($userData['public_mobile'] == 0 && $userData['user_mobile'] != 0) {
                                $user_mobile_view = $userData['country_code'] .' '.$userData['user_mobile'];
                            } else {
                                $user_mobile_view = "";
                            } 
                        } else {
                            $userProfile=$base_url."img/newFav.png";
                            $user_full_name="MyCo";
                            $company_name = "Admin";
                            $user_mobile_view = "";
                        }

                        $feed = array(); 
                        $feed["feed_id"]=$data_notification['feed_id'];
                        $feed["society_id"]=$data_notification['society_id'];
                        $feed["feed_msg"]=html_entity_decode($data_notification['feed_msg']);
                        $feed["user_name"]=$user_full_name;
                        $feed["block_name"]=$company_name;
                        $feed["user_id"]=$data_notification['user_id'];
                        $feed["user_profile_pic"]=$userProfile;
                        $feed["user_mobile"]=$user_mobile_view;
                        $feed_id = $data_notification['feed_id'];


                        $feed_img=explode('~',$data_notification['feed_img']);
                        $feed_img_height=explode('~',$data_notification['feed_img_height']);
                        $feed_img_width=explode('~',$data_notification['feed_img_width']);

                        $unique=array_unique($feed_img);

                        $feed["feed_img"]=array();
                        
                        for ($iFeed=0; $iFeed < count($unique) ; $iFeed++) { 
                            $feed_img=array();
                            $feed_img["feed_img"]=$base_url."img/users/recident_feed/".$unique[$iFeed];
                            $feed_img["feed_height"]=intval($feed_img_height[$iFeed]).'';
                            $feed_img["feed_width"]=intval($feed_img_width[$iFeed]).'';
                            array_push($feed["feed_img"], $feed_img); 

                        }


                        $feed["feed_type"]=$data_notification['feed_type'];
                        $feed["feed_video"]=$base_url."img/users/recident_feed/".$data_notification['feed_video'];
                        $feed["video_thumb"]=$base_url."img/users/recident_feed/".$data_notification['feed_img'];
                        $feed["modify_date"]=date("h:i A, dS M Y", strtotime($data_notification['modify_date']));
                        if(in_array($data_notification['feed_id'], $saved_timeline_array)){
                            $feed["is_saved"] = '1';
                        }else {
                            $feed["is_saved"] = '0';
                        }
                        

                       $qTotalLike=$d->count_data_direct("like_id","news_like","feed_id = '$feed_id' AND active_status = '0'");

                       $feed["totalLikes"]=$qTotalLike."";

                       $qMylike=$d->select("news_like,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_like.feed_id='$feed_id' AND news_like.user_id=users_master.user_id AND news_like.active_status=0 AND news_like.user_id = '$user_id'");

                       if (mysqli_num_rows($qMylike) > 0) {
                            $feed["like_status"]="1";
                       }else{
                            $feed["like_status"]="0";
                       }

                       $qlike=$d->select("news_like,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_like.feed_id='$feed_id' AND news_like.user_id=users_master.user_id AND news_like.active_status=0","LIMIT 3");

                        if(mysqli_num_rows($qlike)>0){
                            $feed["like"] = array();

                            while($data_like=mysqli_fetch_array($qlike)) {

                                $like = array(); 

                                $like["like_id"]=$data_like['like_id'];
                                $like["feed_id"]=$data_like['feed_id'];
                                $like["user_id"]=$data_like['user_id'];
                                            
                                $like["user_name"]=$data_like['user_full_name'];
                                $like["block_name"]=html_entity_decode($data_like['user_designation']);
                                $like["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_like['user_profile_pic'];
                                $like["modify_date"]=$data_like['modify_date'];

                                array_push($feed["like"], $like); 
                            }
                        }

                        $commentCount= $d->count_data_direct("comments_id","news_comments","feed_id='$feed_id'  AND society_id!=0");

                        $feed['commentCount'] = $commentCount;

                        $commentCountMy= $d->count_data_direct("comments_id","news_comments","feed_id='$feed_id'  AND society_id!=0 AND user_id='$user_id'");


                        if($commentCountMy>0) {
                            $feed["comment_status"]="1";   
                        } else {
                            $feed["comment_status"]="0";   
                        }


                        if ($userType==1 && $userData['tenant_view']==1) {
               
                        } else {
                            array_push($response["feed"], $feed); 
                        } 
                    }

                    
                     $q2222 = $d->select("user_notification", "user_id='$user_id' AND  notification_type=1 AND read_status=0");


                    $response["unread_notification"]=mysqli_num_rows($q2222);
                    $response["pos1"]=$pos1+0;
                    $response["totalSocietyFeedLimit"]=''.$totalSocietyFeedLimit;
                    $response["message"]="Get Feeds success.";
                    $response["totalFeed"]=$totalFeed;
                    $response["video_duration"]=30;
                    $response["status"]="200";
                    echo json_encode($response);

                } else{
                    $response["message"]="No Feeds Found";
                    $response["video_duration"]=30;
                    $response["status"]="201";
                    echo json_encode($response);
                }
            }else if($_POST['getFeedLikeList']=="getFeedLikeList" && $user_id != '' && $feed_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

               
                $qlike=$d->select("news_like,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_like.feed_id='$feed_id' AND news_like.user_id=users_master.user_id AND news_like.active_status=0");
                           
                $totalLikes = mysqli_num_rows($qlike);

                    $response["like"] = array();

                
                if(mysqli_num_rows($qlike)>0){
                    

                    while($data_like=mysqli_fetch_array($qlike)) {

                        $like = array(); 

                        $like["like_id"]=$data_like['like_id'];
                        $like["feed_id"]=$data_like['feed_id'];
                        $like["user_id"]=$data_like['user_id'];
                        $like["user_name"]=$data_like['user_full_name'];
                        $like["block_name"]=html_entity_decode($data_like['user_designation']);
                        $like["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_like['user_profile_pic'];
                        $like["modify_date"]=$data_like['modify_date'];

                        array_push($response["like"], $like); 
                    }

                    $response["message"]="Success";
                    $response["status"]="200";
                    echo json_encode($response);
                } else{
                    $response["message"]="No likes";
                    $response["status"]="201";
                    echo json_encode($response);
                }
            } else if($_POST['saveFeed']=="saveFeed" && filter_var($feed_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true){

                $m->set_data('society_id',$society_id);
                $m->set_data('user_id',$user_id); 
                $m->set_data('feed_id',$feed_id);

                $a1 = array(
                  'user_id'=>$m->get_data('user_id'),
                  'feed_id'=>$m->get_data('feed_id'),
                  'society_id'=>$m->get_data('society_id'),
                );
                
                if($status=='0') { 
                    $qd=$d->select("news_feed_saved","feed_id='$feed_id' AND user_id='$user_id'");
                    if(mysqli_num_rows($qd)>0) {

                    } else {
                        $q= $d->insert("news_feed_saved",$a1);
                    }

                    $response["message"]="Feed Saved Successfully";
                    $response["status"]="200";
                    echo json_encode($response);
                    exit();
                } else {
                   $q=  $d->delete("news_feed_saved","feed_id='$feed_id' AND user_id='$user_id'");
                   $response["message"]="Saved Feed Deleted";
                   $response["status"]="200";
                   echo json_encode($response);
                    exit();
                }



            }else if($_POST['getSavedFeed']=="getSavedFeed" && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true){

                if ($unit_id!='' && $unit_id!=0) {
                     $quc=$d->select("unit_master","unit_id='$unit_id' AND timline_access_denied=1");
                     if (mysqli_num_rows($quc)>0) {
                        $response["message"] = "Access Denied..!";
                        $response["status"] = "202";
                        echo json_encode($response);
                        exit();
                     }

                  }
                
                // find type
                $fq=$d->select("users_master","user_id='$user_id' AND society_id='$society_id'");
                $ownerData=mysqli_fetch_array($fq);
                $userType= $ownerData['user_type'];

                $totalFeed=$d->count_data_direct("feed_id","news_feed,news_feed_saved","news_feed_saved.feed_id=news_feed.feed_id AND news_feed_saved.user_id='$user_id' AND news_feed_saved.society_id='$society_id' AND news_feed.status = 0 ");
                

                $qnotification=$d->select("news_feed_saved,news_feed","news_feed_saved.feed_id=news_feed.feed_id AND news_feed_saved.user_id='$user_id' AND news_feed.society_id='$society_id' AND news_feed.status = 0 ","ORDER BY news_feed.feed_id DESC LIMIT $limit_feed,10");

                $totalSocietyFeedLimit= $d->count_data_direct("feed_id","news_feed_saved","society_id='$society_id' AND user_id='$user_id'");
                
                if(mysqli_num_rows($qnotification)>0){

                    $response["feed"] = array();
                    while($data_notification=mysqli_fetch_array($qnotification)) {
                      
                        if ($data_notification['user_id']!=0) {
                            $qu=$d->selectRow("user_profile_pic,tenant_view,user_full_name,user_designation,user_mobile,public_mobile","users_master","user_id='$data_notification[user_id]' AND society_id='$society_id'");
                            $userData=mysqli_fetch_array($qu);
                            $userProfile=$base_url."img/users/recident_profile/".$userData['user_profile_pic'];
                            $user_full_name=$userData['user_full_name'];
                            $company_name = html_entity_decode($userData['user_designation']);
                            if ($userData['public_mobile'] == 0 && $userData['user_mobile'] != 0) {
                                $user_mobile_view = $userData['user_mobile'];
                            }
                        } else {
                            $userProfile=$base_url."img/newFav.png";
                            $user_full_name="MyCo";
                            $company_name = "Admin";
                             $user_mobile_view= "";
                        }

                        $feed = array(); 
                        $feed["feed_id"]=$data_notification['feed_id'];
                        $feed["society_id"]=$data_notification['society_id'];
                        $feed["feed_msg"]=html_entity_decode($data_notification['feed_msg']);
                        $feed["user_name"]=$user_full_name;
                        $feed["block_name"]=$company_name;
                        $feed["user_id"]=$data_notification['user_id'];
                        $feed["user_profile_pic"]=$userProfile;
                        $feed["user_mobile"]=$user_mobile_view;
                        $feed_id = $data_notification['feed_id'];


                        $feed_img=explode('~',$data_notification['feed_img']);
                        $feed_img_height=explode('~',$data_notification['feed_img_height']);
                        $feed_img_width=explode('~',$data_notification['feed_img_width']);

                        $unique=array_unique($feed_img);

                        $feed["feed_img"]=array();
                        
                        for ($iFeed=0; $iFeed < count($unique) ; $iFeed++) { 
                            $feed_img=array();
                            $feed_img["feed_img"]=$base_url."img/users/recident_feed/".$unique[$iFeed];
                            $feed_img["feed_height"]=$feed_img_height[$iFeed];
                            $feed_img["feed_width"]=$feed_img_width[$iFeed];
                            array_push($feed["feed_img"], $feed_img); 

                        }


                        $feed["feed_type"]=$data_notification['feed_type'];
                        $feed["feed_video"]=$base_url."img/users/recident_feed/".$data_notification['feed_video'];
                        $feed["video_thumb"]=$base_url."img/users/recident_feed/".$data_notification['feed_img'];
                        $feed["modify_date"]=date("h:i A, dS M Y", strtotime($data_notification['modify_date']));
                        $feed["is_saved"] = '1';
                      
                       $qlike=$d->select("news_like,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_like.feed_id='$feed_id' AND news_like.user_id=users_master.user_id AND news_like.active_status=0");
                       $totalLikes = mysqli_num_rows($qlike);
                       $feed["totalLikes"]="$totalLikes";

                        if(mysqli_num_rows($qlike)>0){
                            $feed["like"] = array();
                            $feed["like_status"]="0";  

                            while($data_like=mysqli_fetch_array($qlike)) {

                                $like = array(); 

                                $like["like_id"]=$data_like['like_id'];
                                $like["feed_id"]=$data_like['feed_id'];
                                $like["user_id"]=$data_like['user_id'];

                               
                                if ($user_id==$data_like['user_id']) {

                                    $feed["like_status"]="1";

                                }else{
                                    if ($feed["like_status"]=="1") {
                                  
                                    }else{

                                   $feed["like_status"]="0"; 
                                    }  
                                }
                                $like["user_name"]=$data_like['user_full_name'];
                                $like["block_name"]=html_entity_decode($data_like['user_designation']);
                                $like["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_like['user_profile_pic'];
                                $like["modify_date"]=$data_like['modify_date'];

                                array_push($feed["like"], $like); 
                            }
                        }

                        else{

                             $feed["like_status"]="0";  
                        }

                        $commentCount= $d->count_data_direct("comments_id","news_comments","feed_id='$feed_id'  AND society_id!=0 AND user_id='$user_id'");


                        $qcomment=$d->select("news_comments,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_comments.feed_id='$feed_id' AND news_comments.user_id=users_master.user_id AND news_comments.parent_comments_id=0","ORDER BY news_comments.comments_id DESC");

                        if(mysqli_num_rows($qcomment)>0){
                            $feed["comment"] = array();

                            while($data_comment=mysqli_fetch_array($qcomment)) {

                                $comment = array(); 

                                $comment["comments_id"]=$data_comment['comments_id'];
                                $comment["feed_id"]=$data_comment['feed_id'];
                                $comment["msg"]=html_entity_decode($data_comment['msg']);
                                $comment["user_name"]=$data_comment['user_full_name'];
                                $comment["block_name"]=html_entity_decode($data_comment['user_designation']);
                                $comment["user_id"]=$data_comment['user_id'];
                                $comment["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_comment['user_profile_pic'];
                                if ($user_id==$data_comment['user_id']) {
                                    $feed["comment_status"]="1";
                                }else{
                                   $feed["comment_status"]="0";   
                                }
                                $comment["modify_date"]=time_elapsed_string($data_comment['modify_date']);


                                $comment["sub_comment"] = array();
                                $q4=$d->select("news_comments,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_comments.feed_id='$feed_id' AND news_comments.user_id=users_master.user_id AND news_comments.parent_comments_id='$data_comment[comments_id]'","ORDER BY news_comments.comments_id DESC");
                                    while ($subCommentData=mysqli_fetch_array($q4)) {
                                        $sub_comment = array();
                                        $sub_comment["comments_id"]=$subCommentData['comments_id'];
                                        $sub_comment["feed_id"]=$subCommentData['feed_id'];
                                        $sub_comment["msg"]=html_entity_decode($subCommentData['msg']);
                                        $sub_comment["user_name"]=$subCommentData['user_full_name'];
                                        $sub_comment["block_name"]=html_entity_decode($subCommentData['user_designation']);
                                        $sub_comment["user_id"]=$subCommentData['user_id'];
                                        $sub_comment["user_profile_pic"]=$base_url."img/users/recident_profile/".$subCommentData['user_profile_pic'];
                                        $sub_comment["modify_date"]=time_elapsed_string($subCommentData['modify_date']);

                                        array_push($comment["sub_comment"], $sub_comment); 
                                  }
                                array_push($feed["comment"], $comment); 
                            }
                        }
                        
                        if($commentCount>0) {
                            $feed["comment_status"]="1";   
                        } else {
                            $feed["comment_status"]="0";   
                        }


                        if ($userType==1 && $userData['tenant_view']==1) {
               
                        } else {
                            array_push($response["feed"], $feed); 
                        } 
                    }
                    
                    $response["pos1"]=$pos1+0;
                    $response["totalFeed"]=$totalFeed;
                    $response["totalSocietyFeedLimit"]=$totalSocietyFeedLimit.'';
                    $response["message"]="Get Saved Feeds success.";
                    $response["status"]="200";
                    echo json_encode($response);

                } else{
                    $response["message"]="No Feeds Found";
                    $response["video_duration"]=30;
                    $response["status"]="201";
                    echo json_encode($response);
                }
            } else if($_POST['getSavedFeedNew']=="getSavedFeedNew" && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true){

                if ($unit_id!='' && $unit_id!=0) {
                     $quc=$d->select("unit_master","unit_id='$unit_id' AND timline_access_denied=1");
                     if (mysqli_num_rows($quc)>0) {
                        $response["message"] = "Access Denied..!";
                        $response["status"] = "202";
                        echo json_encode($response);
                        exit();
                     }

                  }
                
                // find type
                $fq=$d->select("users_master","user_id='$user_id' AND society_id='$society_id'");
                $ownerData=mysqli_fetch_array($fq);
                $userType= $ownerData['user_type'];

                $totalFeed=$d->count_data_direct("feed_id","news_feed,news_feed_saved","news_feed_saved.feed_id=news_feed.feed_id AND news_feed_saved.user_id='$user_id' AND news_feed_saved.society_id='$society_id' AND news_feed.status = 0 ");
                

                $qnotification=$d->select("news_feed_saved,news_feed","news_feed_saved.feed_id=news_feed.feed_id AND news_feed_saved.user_id='$user_id' AND news_feed.society_id='$society_id' AND news_feed.status = 0 ","ORDER BY news_feed.feed_id DESC LIMIT $limit_feed,10");

                $totalSocietyFeedLimit= $d->count_data_direct("feed_id","news_feed_saved","society_id='$society_id' AND user_id='$user_id'");
                
                if(mysqli_num_rows($qnotification)>0){

                    $response["feed"] = array();
                    while($data_notification=mysqli_fetch_array($qnotification)) {
                      
                        if ($data_notification['user_id']!=0) {
                            $qu=$d->selectRow("user_profile_pic,tenant_view,user_full_name,user_designation,user_mobile,public_mobile","users_master","user_id='$data_notification[user_id]' AND society_id='$society_id'");
                            $userData=mysqli_fetch_array($qu);
                            $userProfile=$base_url."img/users/recident_profile/".$userData['user_profile_pic'];
                            $user_full_name=$userData['user_full_name'];
                            $company_name = html_entity_decode($userData['user_designation']);
                            if ($userData['public_mobile'] == 0 && $userData['user_mobile'] != 0) {
                                $user_mobile_view = $userData['user_mobile'];
                            }
                        } else {
                            $userProfile=$base_url."img/newFav.png";
                            $user_full_name="MyCo";
                            $company_name = "Admin";
                             $user_mobile_view= "";
                        }

                        $feed = array(); 
                        $feed["feed_id"]=$data_notification['feed_id'];
                        $feed["society_id"]=$data_notification['society_id'];
                        $feed["feed_msg"]=html_entity_decode($data_notification['feed_msg']);
                        $feed["user_name"]=$user_full_name;
                        $feed["block_name"]=$company_name;
                        $feed["user_id"]=$data_notification['user_id'];
                        $feed["user_profile_pic"]=$userProfile;
                        $feed["user_mobile"]=$user_mobile_view;
                        $feed_id = $data_notification['feed_id'];


                        $feed_img=explode('~',$data_notification['feed_img']);
                        $feed_img_height=explode('~',$data_notification['feed_img_height']);
                        $feed_img_width=explode('~',$data_notification['feed_img_width']);

                        $unique=array_unique($feed_img);

                        $feed["feed_img"]=array();
                        
                        for ($iFeed=0; $iFeed < count($unique) ; $iFeed++) { 
                            $feed_img=array();
                            $feed_img["feed_img"]=$base_url."img/users/recident_feed/".$unique[$iFeed];
                            $feed_img["feed_height"]=$feed_img_height[$iFeed];
                            $feed_img["feed_width"]=$feed_img_width[$iFeed];
                            array_push($feed["feed_img"], $feed_img); 

                        }


                        $feed["feed_type"]=$data_notification['feed_type'];
                        $feed["feed_video"]=$base_url."img/users/recident_feed/".$data_notification['feed_video'];
                        $feed["video_thumb"]=$base_url."img/users/recident_feed/".$data_notification['feed_img'];
                        $feed["modify_date"]=date("h:i A, dS M Y", strtotime($data_notification['modify_date']));
                        $feed["is_saved"] = '1';
                      
                       $qTotalLike=$d->count_data_direct("like_id","news_like","feed_id = '$feed_id' AND active_status = '0'");

                       $feed["totalLikes"]=$qTotalLike."";

                       $qMylike=$d->select("news_like,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_like.feed_id='$feed_id' AND news_like.user_id=users_master.user_id AND news_like.active_status=0 AND news_like.user_id = '$user_id'");

                       if (mysqli_num_rows($qMylike) > 0) {
                            $feed["like_status"]="1";
                       }else{
                            $feed["like_status"]="0";
                       }

                       $qlike=$d->select("news_like,users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND news_like.feed_id='$feed_id' AND news_like.user_id=users_master.user_id AND news_like.active_status=0","LIMIT 3");

                        if(mysqli_num_rows($qlike)>0){
                            $feed["like"] = array();

                            while($data_like=mysqli_fetch_array($qlike)) {

                                $like = array(); 

                                $like["like_id"]=$data_like['like_id'];
                                $like["feed_id"]=$data_like['feed_id'];
                                $like["user_id"]=$data_like['user_id'];

                               
                                $like["user_name"]=$data_like['user_full_name'];
                                $like["block_name"]=html_entity_decode($data_like['user_designation']);
                                $like["user_profile_pic"]=$base_url."img/users/recident_profile/".$data_like['user_profile_pic'];
                                $like["modify_date"]=$data_like['modify_date'].'';

                                array_push($feed["like"], $like); 
                            }
                        }



                        $commentCount= $d->count_data_direct("comments_id","news_comments","feed_id='$feed_id'  AND society_id!=0");

                        $feed['commentCount'] = $commentCount;

                        $commentCountMy= $d->count_data_direct("comments_id","news_comments","feed_id='$feed_id'  AND society_id!=0 AND user_id='$user_id'");


                        if($commentCountMy>0) {
                            $feed["comment_status"]="1";   
                        } else {
                            $feed["comment_status"]="0";   
                        }


                        if ($userType==1 && $userData['tenant_view']==1) {
               
                        } else {
                            array_push($response["feed"], $feed); 
                        } 
                    }
                    
                    $response["pos1"]=$pos1+0;
                    $response["totalFeed"]=$totalFeed;
                    $response["totalSocietyFeedLimit"]=$totalSocietyFeedLimit.'';
                    $response["message"]="Get Saved Feeds success.";
                    $response["status"]="200";
                    echo json_encode($response);

                } else{
                    $response["message"]="No Feeds Found";
                    $response["video_duration"]=30;
                    $response["status"]="201";
                    echo json_encode($response);
                }
            }  else {
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);                  
        }
    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>