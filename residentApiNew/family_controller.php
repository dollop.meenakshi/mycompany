<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

	if ($key == $keydb && $auth_check=='true') {

		$response = array();
		extract(array_map("test_input", $_POST));

		if (isset($addFamilyMember) && $addFamilyMember == 'addFamilyMember' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($parent_id, FILTER_VALIDATE_INT) == true && filter_var($block_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true ) {

			if ($member_status == 1) {
				$response["message"] = "Access Denied For Sub Member!";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}


			$qSociety = $d->select("society_master", "society_id ='$society_id'");
			$societyData = mysqli_fetch_array($qSociety);
			$family_member_approval = $societyData['family_member_approval'];
			$society_name = $societyData['society_name'];

			$qselect = $d->select("users_master", "unit_id='$unit_id' AND user_mobile='$user_mobile' AND user_mobile != '' AND user_mobile != '0'  ");
			$user_data = mysqli_fetch_array($qselect);
			if ($user_data>0 && $user_data['delete_status']==0) {
				$response["message"] = "Mobile number is already register in this unit.";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}


			$qc = $d->select("users_master,unit_master,block_master", "unit_master.unit_id='$unit_id' AND block_master.block_id='$block_id' AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=users_master.block_id AND users_master.user_id='$parent_id'");
			$userTypeData = mysqli_fetch_array($qc);
			$user_type = $userTypeData['user_type'];
			$tenant_view = $userTypeData['tenant_view'];
			$parent_name = $userTypeData['user_full_name'];
			$blockName = $userTypeData['block_name'];
			$unitName = $userTypeData['unit_name'];


			$extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");
			$uploadedFile = $_FILES["family_profile_pic"]["tmp_name"];
			$ext = pathinfo($_FILES['family_profile_pic']['name'], PATHINFO_EXTENSION);
			if (file_exists($uploadedFile)) {

				$sourceProperties = getimagesize($uploadedFile);
				$newFileName = rand() . $user_id;
				$dirPath = "../img/users/recident_profile/";
				$imageType = $sourceProperties[2];
				$imageHeight = $sourceProperties[1];
				$imageWidth = $sourceProperties[0];
				if ($imageWidth > 500) {
					$newWidthPercentage = 500 * 100 / $imageWidth; //for maximum 500 widht
					$newImageWidth = $imageWidth * $newWidthPercentage / 100;
					$newImageHeight = $imageHeight * $newWidthPercentage / 100;
				} else {
					$newImageWidth = $imageWidth;
					$newImageHeight = $imageHeight;
				}
				switch ($imageType) {

				case IMAGETYPE_PNG:
					$imageSrc = imagecreatefrompng($uploadedFile);
					$tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
					imagepng($tmp, $dirPath . $newFileName . "_profile." . $ext);
					break;

				case IMAGETYPE_JPEG:
					$imageSrc = imagecreatefromjpeg($uploadedFile);
					$tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
					imagejpeg($tmp, $dirPath . $newFileName . "_profile." . $ext);
					break;

				case IMAGETYPE_GIF:
					$imageSrc = imagecreatefromgif($uploadedFile);
					$tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
					imagegif($tmp, $dirPath . $newFileName . "_profile." . $ext);
					break;

				default:

					break;
				}
				$family_profile_pic = $newFileName . "_profile." . $ext;

			} else {

				$family_profile_pic  = "";
			}

			$m->set_data('user_profile_pic', $family_profile_pic);

			
			$m->set_data('parent_id', $parent_id);
			$m->set_data('society_id', $society_id);
			$m->set_data('block_id', $block_id);
			$m->set_data('floor_id', $floor_id);
			$m->set_data('unit_id', $unit_id);

			$m->set_data('member_status', '1');

			$m->set_data('user_password', $password);
			$m->set_data('user_first_name', $user_first_name);
			$m->set_data('user_last_name', $user_last_name);
			$m->set_data('country_code', $country_code);
			$m->set_data('user_full_name', $user_first_name . " " . $user_last_name);

			if ($user_status == 2) {
				$m->set_data('user_status', $user_status);
				$m->set_data('user_mobile', $user_mobile);
			} else {
				if ($family_member_approval == "1") {
					if ($user_id != 0 && $oldUserStatus == 1) {
						$m->set_data('user_status', 1);
					} else {
						$m->set_data('user_status', 0);
					}
					$adminNoti = true;
				} else {
					$adminNoti = false;
					$m->set_data('user_status', $user_status);
				}
				$m->set_data('user_mobile', $user_mobile);
			}

			
			// user status    =>  0 pending approval 1 approved 2 for login deny.
			$m->set_data('user_type', $user_type);

			$m->set_data('member_relation_name', $member_relation);
			$m->set_data('gender', $gender);
			if($member_date_of_birth!="" && $member_date_of_birth!="null") {
				$m->set_data('member_date_of_birth', date('Y-m-d', strtotime($member_date_of_birth)));
			} else {
				$m->set_data('member_date_of_birth',"");
			}
			$m->set_data('tenant_view', $tenant_view);
			$m->set_data('register_date', date('Y-m-d H:i:s'));
      		$m->set_data('designation', $designation);


			$a1 = array(
				'parent_id' => $m->get_data('parent_id'),
				'society_id' => $m->get_data('society_id'),
				'block_id' => $m->get_data('block_id'),
				'floor_id' => $m->get_data('floor_id'),
				'unit_id' => $m->get_data('unit_id'),
				'user_first_name' => $m->get_data('user_first_name'),
				'user_last_name' => $m->get_data('user_last_name'),
				'user_full_name' => $m->get_data('user_full_name'),
				'gender' => $m->get_data('gender'),
				'user_mobile' => $m->get_data('user_mobile'),
				'user_type' => $m->get_data('user_type'),
				'user_status' => $m->get_data('user_status'),
				'user_profile_pic' => $m->get_data('user_profile_pic'),
				'member_status' => $m->get_data('member_status'),
				'member_relation_name' => "Team",
				'member_date_of_birth' => $m->get_data('member_date_of_birth'),
				'register_date' => $m->get_data('register_date'),
				'tenant_view' => $m->get_data('tenant_view'),
				'country_code' => $m->get_data('country_code'),
				'delete_status'=> 0,
			);

				if ($user_data['delete_status']==1) {

					$q = $d->update("users_master", $a1,"unit_id='$unit_id' AND user_mobile='$user_mobile' AND user_mobile != '' AND user_mobile != '0' ");
					$user_id = $user_data['user_id'];
		          $d->delete("user_employment_details","user_id='$user_id'");
				} else {

					$q = $d->insert("users_master", $a1);
					$user_id = $con->insert_id;
			        $m->set_data('user_id',$user_id);
				}

				if ($user_status != 2 && $adminNoti == false) {

					$smsObj->send_welcome_message($society_id,$user_mobile,$user_first_name,$society_name,$country_code);
			        $d->add_sms_log($user_mobile,"User Welcome Message",$society_id,$country_code,4);

				}
					$d->insert_myactivity($parent_id, "$society_id", "0", "$user_name", "Team member added by $parent_name", "My-profilexxxhdpi.png");
					
					if ($adminNoti == true) {

		                // $block_id=$d->getBlockid($user_id);
                		$fcmArray=$d->selectAdminBlockwise("7",$block_id,"android");
                		$fcmArrayIos=$d->selectAdminBlockwise("7",$block_id,"ios");

						$nAdmin->noti_new($society_id,$notiImg, $fcmArray, "New Team Member Request by $parent_name", "for $user_first_name ($blockName-$unitName)", 'pendingUser?pendingUser=yes');
						$nAdmin->noti_ios_new($society_id,$notiImg, $fcmArrayIos, "New Team Member Request by $parent_name", "for $user_first_name ($blockName-$unitName)", 'pendingUser?pendingUser=yes');

						$notiAry = array(
							'society_id' => $society_id,
							'notification_tittle' => "New Team Member Request by $parent_name",
							'notification_description' =>"for $user_first_name ($blockName-$unitName)",
							'notifiaction_date' => date('Y-m-d H:i'),
							'notification_action' => "pendingUser?pendingUser=yes",
							'admin_click_action ' => "pendingUser?pendingUser=yes",
							'notification_logo'=>'Members_1xxxhdpi.png',
						);

						$d->insert("admin_notification", $notiAry);
					}

			if ($q == true) {

		          $compArray =array(
		            'user_id'=> $m->get_data('user_id'),
		            'society_id'=> $m->get_data('society_id'),
		            'unit_id'=> $m->get_data('unit_id'),
		            'designation'=> $m->get_data('designation'),
		          );
		          $d->insert("user_employment_details",$compArray);

				$response["message"] = "Team Member Added ";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "Something went wrong";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else  if (isset($updateFamilyMember) && $updateFamilyMember == 'updateFamilyMember' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($parent_id, FILTER_VALIDATE_INT) == true && filter_var($block_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true ) {

			if ($member_status == 1) {
				$response["message"] = "Access Denied For Sub Member!";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}

			$qSociety = $d->select("society_master", "society_id ='$society_id'");
			$societyData = mysqli_fetch_array($qSociety);
			$family_member_approval = $societyData['family_member_approval'];

			$qselect = $d->select("users_master", "unit_id='$unit_id' AND user_mobile='$user_mobile' AND user_mobile != '' AND user_mobile != '0' AND user_status=1 AND user_id!='$user_id'");
			$user_data = mysqli_fetch_array($qselect);
			if ($user_data == true ) {
				$response["message"] = "Mobile number is already register.";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}


			$qc = $d->select("users_master,unit_master,block_master", "unit_master.unit_id='$unit_id' AND block_master.block_id='$block_id' AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=users_master.block_id AND users_master.user_id='$parent_id'");
			$userTypeData = mysqli_fetch_array($qc);
			$user_type = $userTypeData['user_type'];
			$tenant_view = $userTypeData['tenant_view'];
			$parent_name = $userTypeData['user_full_name'];
			$blockName = $userTypeData['block_name'];
			$unitName = $userTypeData['unit_name'];

			$qold=$d->select("users_master","user_id='$user_id'","" );
            $olddata=mysqli_fetch_array($qold);
            $old_mobile= $olddata['user_mobile'];
            $user_fcm = $olddata['user_token'];
			$device = $olddata['device'];
			$oldUserStatus = $olddata['user_status'];

			$extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");
			$uploadedFile = $_FILES["family_profile_pic"]["tmp_name"];
			$ext = pathinfo($_FILES['family_profile_pic']['name'], PATHINFO_EXTENSION);
			if (file_exists($uploadedFile)) {

				$sourceProperties = getimagesize($uploadedFile);
				$newFileName = rand() . $user_id;
				$dirPath = "../img/users/recident_profile/";
				$imageType = $sourceProperties[2];
				$imageHeight = $sourceProperties[1];
				$imageWidth = $sourceProperties[0];
				if ($imageWidth > 500) {
					$newWidthPercentage = 500 * 100 / $imageWidth; //for maximum 500 widht
					$newImageWidth = $imageWidth * $newWidthPercentage / 100;
					$newImageHeight = $imageHeight * $newWidthPercentage / 100;
				} else {
					$newImageWidth = $imageWidth;
					$newImageHeight = $imageHeight;
				}
				switch ($imageType) {

				case IMAGETYPE_PNG:
					$imageSrc = imagecreatefrompng($uploadedFile);
					$tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
					imagepng($tmp, $dirPath . $newFileName . "_profile." . $ext);
					break;

				case IMAGETYPE_JPEG:
					$imageSrc = imagecreatefromjpeg($uploadedFile);
					$tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
					imagejpeg($tmp, $dirPath . $newFileName . "_profile." . $ext);
					break;

				case IMAGETYPE_GIF:
					$imageSrc = imagecreatefromgif($uploadedFile);
					$tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
					imagegif($tmp, $dirPath . $newFileName . "_profile." . $ext);
					break;

				default:

					break;
				}
				$family_profile_pic = $newFileName . "_profile." . $ext;

			} else {

				
				$family_profile_pic  = $olddata['user_profile_pic'];
			}


			
			$m->set_data('parent_id', $parent_id);
			$m->set_data('society_id', $society_id);
			$m->set_data('block_id', $block_id);
			$m->set_data('floor_id', $floor_id);
			$m->set_data('unit_id', $unit_id);

			$m->set_data('member_status', '1');

			$m->set_data('user_password', $password);
			$m->set_data('user_first_name', $user_first_name);
			$m->set_data('user_last_name', $user_last_name);
			$m->set_data('country_code', $country_code);
			$m->set_data('user_full_name', $user_first_name . " " . $user_last_name);
			
			if ($user_status == 2) {
				$m->set_data('user_status', $user_status);
				$m->set_data('user_mobile', $user_mobile);
			} else {
				if ($family_member_approval == "1") {
					if ($user_id != 0 && $oldUserStatus == 1) {
						$m->set_data('user_status', 1);
						$adminNoti = false;
					} else {
						$m->set_data('user_status', 0);
						$adminNoti = true;
					}
				} else {
					$adminNoti = false;
					$m->set_data('user_status', $user_status);
				}
				$m->set_data('user_mobile', $user_mobile);
			}

			
			// user status    =>  0 pending approval 1 approved 2 for login deny.
			$m->set_data('user_profile_pic', $family_profile_pic);
			$m->set_data('user_type', $user_type);

			$m->set_data('member_relation_name', "Team");
			$m->set_data('gender', $gender);
			$m->set_data('member_date_of_birth', $member_date_of_birth);
			$m->set_data('tenant_view', $tenant_view);
			$m->set_data('register_date', date('Y-m-d H:i:s'));
			$m->set_data('designation', $designation);

			$a1 = array(
				'parent_id' => $m->get_data('parent_id'),
				'society_id' => $m->get_data('society_id'),
				'block_id' => $m->get_data('block_id'),
				'floor_id' => $m->get_data('floor_id'),
				'unit_id' => $m->get_data('unit_id'),
				'user_first_name' => $m->get_data('user_first_name'),
				'user_last_name' => $m->get_data('user_last_name'),
				'user_full_name' => $m->get_data('user_full_name'),
				'gender' => $m->get_data('gender'),
				'user_mobile' => $m->get_data('user_mobile'),
				'user_type' => $m->get_data('user_type'),
				'user_status' => $m->get_data('user_status'),
				'user_profile_pic' => $m->get_data('user_profile_pic'),
				'member_status' => $m->get_data('member_status'),
				'member_relation_name' => "Team",
				'member_date_of_birth' => $m->get_data('member_date_of_birth'),
				'register_date' => $m->get_data('register_date'),
				'tenant_view' => $m->get_data('tenant_view'),
				'country_code' => $m->get_data('country_code'),
			);


				if ($user_status == 2 || $old_mobile!=$user_mobile) {
					
					if ($device == 'android') {
						$nResident->noti("", "", $society_id, $user_fcm, "logout", "$parent_name Parent Removed Your Account", 'logout');
					} else if ($device == 'ios') {
						$nResident->noti_ios("", "", $society_id, $user_fcm, "logout", "$parent_name Parent Removed Your Account", 'logout');
					}
					$q = $d->update("users_master", $a1, "user_id='$user_id'");
					$a14 = array(
						'user_token' => "",
					);
					$d->update("users_master", $a14, "user_id='$user_id'");
					$d->insert_myactivity($parent_id, "$society_id", "0", "$user_name", "Team member data updated", "My-profilexxxhdpi.png");
				} else {

						$q = $d->update("users_master", $a1, "user_id='$user_id'");
						$d->insert_myactivity($parent_id, "$society_id", "0", "$user_name", "Team member data updated", "My-profilexxxhdpi.png");
						
						$q = $d->update("users_master", $a1, "user_id='$user_id'");

						
						$d->insert_myactivity($parent_id, "$society_id", "0", "$user_name", "Team member data updated", "My-profilexxxhdpi.png");

						if ($adminNoti == true) {
							$qunitName = $d->select("unit_master,block_master", "unit_master.unit_id='$unit_id' AND block_master.block_id='$block_id'");
							$data_unit = mysqli_fetch_array($qunitName);
							$blockName = $data_unit['block_name'];
							$unitName = $data_unit['unit_name'];

			                $fcmArray=$d->selectAdminBlockwise("7",$block_id,"android");
                			$fcmArrayIos=$d->selectAdminBlockwise("7",$block_id,"ios");

							$nAdmin->noti_new($society_id,$notiImg, $fcmArray, "New Team Member Request by", "$user_first_name  for $blockName-$unitName", 'pendingUser?pendingUser=yes');
							$nAdmin->noti_ios_new($society_id,$notiImg, $fcmArrayIos, "New Team Member Request by", "$user_first_name  for $blockName-$unitName", 'pendingUser?pendingUser=yes');

							$notiAry = array(
								'society_id' => $society_id,
								'notification_tittle' => "New Team Member Request by",
								'notification_description' => $user_first_name . " for " . $data_unit['block_name'] . "-" . $data_unit['unit_name'],
								'notifiaction_date' => date('Y-m-d H:i'),
								'notification_action' => "pendingUser?pendingUser=yes",
								'admin_click_action ' => "pendingUser?pendingUser=yes",
								'notification_logo'=>'Members_1xxxhdpi.png',
							);

							$d->insert("admin_notification", $notiAry);

						}

				}

			if ($q == true) {
				
				$compArray =array(
		            'designation'=> $m->get_data('designation'),
		          );
		         $d->update("user_employment_details",$compArray,"user_id='$user_id'");

				$response["message"] = "Team Member Data Updated";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "Something went wrong ";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else  if (isset($approveFamilyMember) && $approveFamilyMember == 'approveFamilyMember' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($parent_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true ) {


			$q=$d->select("users_master","society_id='$society_id' AND user_id='$user_id' AND parent_id='$parent_id' AND user_status=0");

			if(mysqli_num_rows($q)>0) {

				$a33 =array(
			      'user_status'=> 1,
			      'register_date'=> date("Y-m-d H:i:s"),
			    );

			    $q=$d->update("users_master",$a33,"user_id='$user_id'");

			    if($q>0) {
			      $userData=mysqli_fetch_array($q);
			      $user_full_name = $userData['user_full_name'];
			      $user_mobile = $userData['user_mobile'];
			      $country_code = $userData['country_code'];
			      // $d->delete("users_master","unit_id='$unit_id' AND user_id!='$approve_user_id' AND user_status=0");
			    	$qSociety = $d->selectRow("society_name","society_master", "society_id ='$society_id'");
				    $societyData = mysqli_fetch_array($qSociety);
			      
			      $societyName = $societyData['society_name'];
			      
			      	$smsObj->send_welcome_message($society_id,$user_mobile,$user_full_name,$society_name,$country_code);
			        $d->add_sms_log($user_mobile,"User Welcome Message",$society_id,$country_code,4);

			      $d->insert_log("","$society_id","$parent_id","$created_by","Member Request Approved for $unit_name-$user_full_name by $parent_name");
				    $response["message"] = "Team request approved";
					$response["status"] = "200";
					echo json_encode($response);
			    } else {
			     	$response["message"] = "Something went wrong";
					$response["status"] = "201";
					echo json_encode($response);
			    }
			} else {
				$response["message"] = "Something went wrong";
				$response["status"] = "201";
				echo json_encode($response);
			}


		}else  if (isset($getOtherFamilyMmeber) && $getOtherFamilyMmeber == 'getOtherFamilyMmeber' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($new_primary_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true ) {

			$qn=$d->select("users_master","unit_id='$unit_id' AND user_id='$new_primary_id' AND delete_status=0");
			$newData=mysqli_fetch_array($qn);
			if (strlen($newData['user_mobile']<8)) {
				$response["message"] = "Selected users have no mobile number, Please update from my profile";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			} else if ($newData['user_status']==2){
				$response["message"] = "Selected users have no App access, Please update from my profile";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}

			$qfamily=$d->select("users_master","delete_status=0 AND user_status!=0 AND unit_id='$unit_id'  AND society_id='$society_id'  AND users_master.user_id!='$new_primary_id' AND user_type='$user_type'");



			if ($qfamily == true) {
				$response["member"] = array();

				while ($datafamily = mysqli_fetch_array($qfamily)) {

					$member = array();
					$member["user_id"] = $datafamily['user_id'];
					$member["user_first_name"] = $datafamily['user_first_name'];
					$member["user_last_name"] = $datafamily['user_last_name'];
					$member["country_code"] = $datafamily['country_code'].'';
					if ($datafamily['user_mobile'] != 0) {
						$member["user_mobile"] = $datafamily['user_mobile'];
					} else {
						$member["user_mobile"] = "";
					}

					$member["gender"] = $datafamily['gender'];
					
					$member["user_status"] = $datafamily['user_status'];
					$member["member_status"] = $datafamily['member_status'];
					$member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];



					array_push($response["member"], $member);
				}

				$response["message"] = "Get Team Members Successfully";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "No Team details found.";
				$response["status"] = "201";
				echo json_encode($response);
			}

		}else  if (isset($setNewPrimaryMember) && $setNewPrimaryMember == 'setNewPrimaryMember' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($new_primary_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true ) {

			$qu= $d->select("users_master","unit_id='$unit_id' AND delete_status=0 AND user_id!='$new_primary_id'");

			while ($famData=mysqli_fetch_array($qu)) {

	         $m->set_data('parent_id',$new_primary_id);
	         $m->set_data('member_status',1);
	         $m->set_data('member_relation',"Team");

	         $a1 = array(
	          'parent_id'=>$m->get_data('parent_id'),
	          'member_status'=>$m->get_data('member_status'),
	          'member_relation_name'=>$m->get_data('member_relation'),
	        );
	         $family_user_id = $famData['user_id'];
	         $d->update("users_master",$a1,"user_id='$family_user_id'");
	      }

	       $aPrimary = array(
	          'parent_id'=>0,
	          'member_status'=>0,
	          'member_relation_name'=>"",
	        );
	     
	       $q = $d->update("users_master",$aPrimary,"user_id='$new_primary_id'");
	       if ($q>0) {

			$npr=$d->select("users_master","user_id='$new_primary_id'");
			$newData=mysqli_fetch_array($npr);
			$newFcm = $newData['user_token'];
			$newDevice = $newData['device'];

			if ($newDevice=='android') {
	            $nResident->noti("","",$society_id,$newFcm,"Primary Account Changed","By $user_name",'approved');
	          }  else if($device=='ios') {
	            $nResident->noti_ios("","",$society_id,$newFcm,"Primary Account Changed","By $user_name",'approved');
	          }

	       	$response["message"] = "Primary Member Changed Successfully";
			$response["status"] = "200";
			echo json_encode($response);
	       } else {
	       	$response["message"] = "Something Went Wrong !";
			$response["status"] = "201";
			echo json_encode($response);
	       }

		}else {
			$response["message"] = "wrong tag. !";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {

		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}
