<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    $registration_req_send = $xml->string->registration_req_send;
    $please_enter_valid_mobile_number = $xml->string->please_enter_valid_mobile_number;

    if ($key==$keydb ) {
    $response = array();
    extract(array_map("test_input" , $_POST));

        if(isset($_POST['addPrimaryUser']) && $_POST['addPrimaryUser']=="addPrimaryUser" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($block_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true)
        {   



            $user_mobile=mysqli_real_escape_string($con, $user_mobile);
            $user_email=mysqli_real_escape_string($con, $user_email);
            $user_mobile= (int)$user_mobile;
            if (strlen($user_mobile)<8) {
                $response["message"]="$please_enter_valid_mobile_number";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }

            $qqq = $d->selectRow("user_mobile,user_status,active_status","users_master","user_mobile = '$user_mobile' AND delete_status != '1'");

            if (mysqli_num_rows($qqq) > 0) {
                $qqqData = mysqli_fetch_array($qqq);

                if ($qqqData['user_status'] == 0) {
                    $response["message"]="You have already applied for registration and your account is in process of approval";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }else if($qqqData['user_status'] == 1) {
                    $response["message"]="Mobile number is already registered";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }           
            }


            if($user_profile_pic!="") 
            {
                $ddd=date("ymdhi");
                $profile_name= "user_".$user_mobile.round(microtime(true)) .'.png';
                define('UPLOAD_DIR', '../img/users/recident_profile/');
                $img = $user_profile_pic;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $profile_name;
                $success = file_put_contents($file, $data);
                $notiImg=$base_url.'img/users/recident_profile/'.$profile_name;
            }else{
                $profile_name= "user_default.png";
                $notiImg="";
            }

            $get_sort = $d->selectRow("user_sort","users_master","floor_id = '$floor_id' AND block_id = '$block_id'","ORDER BY user_sort DESC LIMIT 1");
            $sort = $get_sort->fetch_assoc();
            $sort_value = $sort['user_sort'] + 1;

            $m->set_data('user_sort',$sort_value);
            $m->set_data('user_profile_pic',$profile_name);
            $m->set_data('society_id',$society_id);
            $m->set_data('user_password',$user_password);
            $m->set_data('user_full_name',$user_full_name);
            $m->set_data('user_first_name',$user_first_name);
            $m->set_data('user_last_name',$user_last_name);
            $m->set_data('designation',$designation);
            $m->set_data('gender',$gender);
            $m->set_data('last_address',$last_address);
            $m->set_data('user_mobile',$user_mobile);
            $m->set_data('user_email',$user_email);
            $m->set_data('user_id_proof',$user_id_proof);
            $m->set_data('user_type',0);
            $m->set_data('block_id',$block_id);
            $m->set_data('floor_id',$floor_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('shift_time_id',$shift_time_id);
            $m->set_data('user_token',$user_token);
            if ($newUserByAdmin == "1") {
                $m->set_data('user_status','1');
                $u_status = "1";

            }else{
                $u_status = "4";

                $m->set_data('user_status','0');
            }
            $m->set_data('owner_name',$owner_first_name.' '.$owner_last_name);
            $m->set_data('owner_email',$owner_email);
            $m->set_data('owner_mobile',$owner_mobile);
            $m->set_data('owner_country_code',$owner_country_code);
            $m->set_data('country_code',$country_code);
            $m->set_data('designation',$designation);
            $m->set_data('prv_doc', $prv_doc);
            $m->set_data('joining_date', $joining_date);
            $m->set_data('register_date',date('Y-m-d H:i:s'));

            
          
            $au =array(
                'society_id'=>$m->get_data('society_id'),
                'block_id'=> $m->get_data('block_id'),
                'floor_id'=> $m->get_data('floor_id'),
                'unit_name'=>$unit_name,
                'unit_status'=>$u_status,
              );

            $qunit=$d->insert("unit_master",$au);
            $unit_id = $con->insert_id;
            $m->set_data('unit_id',$unit_id);


            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'user_profile_pic'=>$m->get_data('user_profile_pic'),
                'user_password'=>$m->get_data('user_password'),
                'user_full_name'=>$m->get_data('user_full_name'),
                'user_first_name'=>$m->get_data('user_first_name'),
                'user_last_name'=>$m->get_data('user_last_name'),
                'user_designation'=>$m->get_data('designation'),
                'gender'=>$m->get_data('gender'),
                'user_mobile'=>$m->get_data('user_mobile'),
                'user_email'=>$m->get_data('user_email'),
                'user_id_proof'=>$m->get_data('user_id_proof'),
                'last_address'=>$m->get_data('last_address'),
                'user_type'=>$m->get_data('user_type'),
                'block_id'=>$m->get_data('block_id'),
                'floor_id'=>$m->get_data('floor_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'shift_time_id'=>$m->get_data('shift_time_id'),
                'user_status'=>$m->get_data('user_status'),
                'register_date'=>$m->get_data('register_date'),
                'country_code'=>$m->get_data('country_code'),
                'user_token'=>$m->get_data('user_token'),
                'user_sort'=>$m->get_data('user_sort'),
            );
     
                
                
                $quserDataUpdate = $d->insert("users_master",$a);
                $user_id = $con->insert_id;

                $m->set_data('user_id',$user_id);
               
                
                
                 $qunitName = $d->select("unit_master,block_master,floors_master","unit_master.floor_id=floors_master.floor_id AND unit_master.unit_id='$unit_id' AND block_master.block_id='$block_id'");
                 $data_unit=mysqli_fetch_array($qunitName);
                 $blockName= $data_unit['block_name'];
                 $unitName= $data_unit['unit_name'];
                 $areaName= $data_unit['floor_name'];

                 if ($newUserByAdmin != "1") {
                 
                    $block_id=$d->getBlockid($user_id);
                     $fcmArray=$d->selectAdminBlockwise("7",$block_id,"android");
                     $fcmArrayIos=$d->selectAdminBlockwise("7",$block_id,"ios");
              
                    $nAdmin->noti_new($society_id,$notiImg,$fcmArray,"New Member Request by ".$user_first_name." ".$user_last_name, "For $areaName-$blockName","pendingUser?id=$user_id");
                    $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,"New Member Request by ".$user_full_name, "For $areaName-$blockName","pendingUser?id=$user_id");
                 }

                    $notiAry = array(
                      'society_id'=>$society_id,
                      'notification_tittle'=>"New Member Request by ".$user_first_name." ".$user_last_name,
                      'notification_description'=>"For $areaName-$blockName",
                      'notifiaction_date'=>date('Y-m-d H:i'),
                      'notification_action'=>"pendingUser?id=$user_id",
                      'admin_click_action '=>"pendingUser?id=$user_id",
                      'notification_logo'=>'Members_1xxxhdpi.png',
                    );
                            
                $d->insert("admin_notification",$notiAry);

                // To app Access

                if ($newUserByAdmin != "1") {

                    $title = "New Member Request by ".$user_first_name." ".$user_last_name;
                    $description = "For ".$data_unit['block_name']." (".$areaName.")";

                    $access_type = $approve_employee_access;

                    include 'check_access_data_user.php';

                    if (count($userIDArray) > 0) {

                        $fcmAccessAry=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                        $fcmAccessAryiOS=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                        $nResident->noti("new_employee","",$society_id,$fcmAccessAry,$title,$description,"");
                        $nResident->noti_ios("new_employee","",$society_id,$fcmAccessAryiOS,$title,$description,"");

                        $d->insertUserNotification($society_id,$title,$description,"new_employee","Members_1xxxhdpi.png","users_master.user_id IN ('$userFcmIds')");
                    }
                }

        
            
            if($quserDataUpdate==TRUE){

              
              

                    $compArray =array(
                      'user_id'=> $m->get_data('user_id'),
                      'society_id'=> $m->get_data('society_id'),
                      'unit_id'=> $m->get_data('unit_id'),
                      'designation'=> $m->get_data('designation'),
                      'joining_date'=> $m->get_data('joining_date'),
                    );
                     $d->insert("user_employment_details",$compArray);
                
                $response["user_id"]="$user_id";
                if ($newUserByAdmin == "1") {
                    $response["message"]="Member Registered";
                }else{
                    $response["message"]="$registration_req_send";
                }
                $response["status"]="200";
                echo json_encode($response);
        
            }else{
    
                $response["message"]="Registration faild.";
                $response["status"]="201";
                echo json_encode($response);
        
            }
        
    } else if(isset($_POST['addUser']) && $_POST['addUser']=="addMoreUser" && $society_id!='' && $unit_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true)
        {   

           if(in_array($user_mobile, $_POST['family_mobile'])){
                $response["message"]="Primary member and other member mobile should be unique.";
                $response["status"]="201";
                echo json_encode($response);
                exit();

            }
            $user_mobile= (int)$user_mobile;
            if (strlen($user_mobile)<8) {
                $response["message"]="$please_enter_valid_mobile_number";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }

            if(count(array_unique($_POST['family_mobile']))<count($_POST['family_mobile']))
            {
                $response["message"]="Member mobile should be unique.";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }

            


            if($user_profile_pic!="") 
            {
                $ddd=date("ymdhi");
                $profile_name= "user_".$ddd.$user_first_name.'.png';
                define('UPLOAD_DIR', '../img/users/recident_profile/');
                $img = $user_profile_pic;
                $img = str_replace('data: img/app/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $file = UPLOAD_DIR . $profile_name;
                $success = file_put_contents($file, $data);
            }else{
            $profile_name= "user_default.png";
            }

            // get old seting
            $oq=$d->select("users_master","delete_status=0 AND user_mobile='$user_mobile'");
            $oldData=mysqli_fetch_array($oq);
            $public_mobile= $oldData['public_mobile'];
            $visitor_approved= $oldData['visitor_approved'];
            $Delivery_cab_approval= $oldData['Delivery_cab_approval'];
            $tenant_view= $oldData['tenant_view'];
            $sos_alert= $oldData['sos_alert'];
            $dob_view= $oldData['dob_view'];
            $mobile_for_gatekeeper= $oldData['mobile_for_gatekeeper'];
            $discussion_mute= $oldData['discussion_mute'];

            $m->set_data('user_profile_pic',$profile_name);
            $m->set_data('society_id',$society_id);
            $m->set_data('user_password',$user_password);
            $m->set_data('user_full_name',$user_full_name);
            $m->set_data('user_first_name',$user_first_name);
            $m->set_data('user_last_name',$user_last_name);
            $m->set_data('gender',$gender);
            $m->set_data('last_address',$last_address);
            $m->set_data('user_mobile',$user_mobile);
            $m->set_data('user_email',$user_email);
            $m->set_data('user_id_proof',$user_id_proof);
            $m->set_data('user_type',$user_type);
            $m->set_data('block_id',$block_id);
            $m->set_data('floor_id',$floor_id);
            $m->set_data('user_status','0');
            $m->set_data('owner_name',$owner_first_name.' '.$owner_last_name);
            $m->set_data('owner_email',$owner_email);
            $m->set_data('owner_mobile',$owner_mobile);
            $m->set_data('user_token',$user_token);
            $m->set_data('device',$device);
            $m->set_data('country_code',$country_code);
            $m->set_data('register_date',date('Y-m-d H:i:s'));
            $m->set_data('designation',$designation);
    
              $au =array(
                'society_id'=>$m->get_data('society_id'),
                'block_id'=> $m->get_data('block_id'),
                'floor_id'=> $m->get_data('floor_id'),
                'unit_name'=>$unit_name,
                'unit_status'=>4,
              );

            $qunit=$d->insert("unit_master",$au);
            $unit_id = $con->insert_id;
            $m->set_data('unit_id',$unit_id);
           
            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'user_profile_pic'=>$m->get_data('user_profile_pic'),
                'user_password'=>$m->get_data('user_password'),
                'user_full_name'=>$m->get_data('user_full_name'),
                'user_first_name'=>$m->get_data('user_first_name'),
                'user_last_name'=>$m->get_data('user_last_name'),
                'gender'=>$m->get_data('gender'),
                'user_mobile'=>$m->get_data('user_mobile'),
                'user_email'=>$m->get_data('user_email'),
                'user_id_proof'=>$m->get_data('user_id_proof'),
                'last_address'=>$m->get_data('last_address'),
                'user_type'=>$m->get_data('user_type'),
                'block_id'=>$m->get_data('block_id'),
                'floor_id'=>$m->get_data('floor_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'user_status'=>$m->get_data('user_status'),
                'user_token'=>$m->get_data('user_token'),
                'device'=>$m->get_data('device'),
                'register_date'=>$m->get_data('register_date'),
                'country_code'=>$m->get_data('country_code'),
                'public_mobile'=>$public_mobile,
                'visitor_approved'=>$visitor_approved,
                'Delivery_cab_approval'=>$Delivery_cab_approval,
                'tenant_view'=>$tenant_view,
                'sos_alert'=>$sos_alert,
                'dob_view'=>$dob_view,
                'mobile_for_gatekeeper'=>$mobile_for_gatekeeper,
                'discussion_mute'=>$discussion_mute,
                'user_designation'=>$designation,
            );
      
             

                
                $quserDataUpdate = $d->insert("users_master",$a);
                $user_id = $con->insert_id;

                $m->set_data('user_id',$user_id);
          
                $aUnit = array('unit_status'=>'4');
                 $qunitName = $d->select("unit_master,block_master","unit_master.unit_id='$unit_id' AND block_master.block_id='$block_id'");
                 $data_unit=mysqli_fetch_array($qunitName);

                $block_id=$d->getBlockid($user_id);
               $fcmArray=$d->selectAdminBlockwise("7",$block_id,"android");
               $fcmArrayIos=$d->selectAdminBlockwise("7",$block_id,"ios");

          
                  $nAdmin->noti_new($society_id,"",$fcmArray,"New Member Request by $user_full_name","for ".$data_unit['block_name']."-".$data_unit['unit_name'],"pendingUser?id=$user_id");
                  $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,"New Member Request by $user_full_name","for ".$data_unit['block_name']."-".$data_unit['unit_name'],"pendingUser?id=$user_id");
                  
                    $notiAry = array(
                      'society_id'=>$society_id,
                      'notification_tittle'=>"New Member Request by $user_full_name",
                      'notification_description'=>"for ".$data_unit['block_name']."-".$data_unit['unit_name'],
                      'notifiaction_date'=>date('Y-m-d H:i'),
                      'notification_action'=>"pendingUser?id=$user_id",
                      'admin_click_action '=>"pendingUser?id=$user_id",
                      'notification_logo'=>'Members_1xxxhdpi.png',
                    );
                            
                $d->insert("admin_notification",$notiAry);

                $qUnit = $d->update("unit_master",$aUnit,"unit_id ='$unit_id'");

             
                    $compArray =array(
                      'user_id'=> $m->get_data('user_id'),
                      'society_id'=> $m->get_data('society_id'),
                      'unit_id'=> $m->get_data('unit_id'),
                      'designation'=> $m->get_data('designation'),
                    );
                     $d->insert("user_employment_details",$compArray);

                   
                     
            if($quserDataUpdate==TRUE){

             
                $response["user_id"]="$user_id";
                $response["message"]="$registration_req_send";
                $response["status"]="200";
                echo json_encode($response);
        
            }else{
    
                $response["message"]="Registration faild.";
                $response["status"]="201";
                echo json_encode($response);
        
            }
        
    }else if(isset($_POST['deleteEmergencyContact']) && filter_var($emergencyContact_id, FILTER_VALIDATE_INT) == true){

        $d->delete("user_emergencycontact_master","emergencyContact_id ='$emergencyContact_id'");

        if($d==TRUE){

            $response["message"]="Emergency Number Deleted Successfully.";
            $response["status"]="200";
            echo json_encode($response);

            $d->insert_myactivity($parent_id,"$society_id","0","$user_name","Emergency Number Deleted","Emergencyxxxhdpi.png");

        }else{
            $response["message"]="Something went wrong";
            $response["status"]="201";
            echo json_encode($response);
        }
    } else if(isset($updateBusinessDetails) && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

        $m->set_data('business_categories',html_entity_decode($business_categories));
        $m->set_data('business_categories_sub',html_entity_decode($business_categories_sub));
        $m->set_data('business_categories_other',$business_categories_other);
        $m->set_data('professional_other',$professional_other);
        $m->set_data('employment_description',$employment_description);
        $m->set_data('company_name',$company_name);
        $m->set_data('designation',$designation);
        $m->set_data('company_address',$company_address);
        $m->set_data('plot_lattitude',$plot_lattitude);
        $m->set_data('plot_longitude',$plot_longitude);
        $m->set_data('user_id',$user_id);
        $m->set_data('society_id',$society_id);
        $m->set_data('unit_id',$unit_id);

        $a =array(
          'user_id'=> $m->get_data('user_id'),
          'society_id'=> $m->get_data('society_id'),
          'unit_id'=> $m->get_data('unit_id'),
          'business_categories'=> $m->get_data('business_categories'),
          'business_categories_sub'=> $m->get_data('business_categories_sub'),
          'business_categories_other'=> $m->get_data('business_categories_other'),
          'professional_other'=> $m->get_data('professional_other'),
          'designation'=> $m->get_data('designation'),
        );

        $a2 =array(
            'plot_lattitude'=> $m->get_data('plot_lattitude'),
            'plot_longitude'=> $m->get_data('plot_longitude'),
            // 'company_name'=> $m->get_data('company_name'),
            'company_address'=> $m->get_data('company_address'),
        );
        $q=$d->update("unit_master",$a2,"unit_id='$unit_id'");

        $q1=$d->select("user_employment_details","user_id='$user_id'");
    
        if (mysqli_num_rows($q1)>0) {
          $q=$d->update("user_employment_details",$a,"user_id='$user_id'");
        } else {
          $q=$d->insert("user_employment_details",$a);
        }

        if ($q>0) {
            $response["message"]="Updated Successfully";
            $response["status"]="200";
            echo json_encode($response);
        } else {
             $response["message"]="Something went wrong";
            $response["status"]="201";
            echo json_encode($response);
        }   

    }else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }
  }
    else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}

?>