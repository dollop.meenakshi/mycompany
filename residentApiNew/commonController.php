<?php
include_once 'lib.php';
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
// include_once '../fcm_file/admin_fcm.php';
/* include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd(); */
$created_by = $_COOKIE['admin_name'];
$bms_admin_id = $_COOKIE['bms_admin_id'];
$society_id = $_COOKIE['society_id'];
$blockAryAccess = array();
$aq=$d->select("bms_admin_master","admin_id='$bms_admin_id' ");
$adminData= mysqli_fetch_array($aq);
$access_branchs=$adminData['access_branchs'];
$access_departments=$adminData['access_departments'];
$blockAryAccess = array();
$departmentAry = array();
$blockAryAccess = explode(",", $access_branchs);
$departmentAry = explode(",", $access_departments);

if ($access_branchs!='') {
    $blockids = join("','",$blockAryAccess); 
    if ($access_departments!='') {
      $departmentIds = join("','",$departmentAry); 
      $restictDepartment = " AND floors_master.floor_id IN ('$departmentIds')";
      $restictDepartmentUser = " AND users_master.floor_id IN ('$departmentIds')";
    } else {
      $restictDepartment = "";
      $restictDepartmentUser = "";
    }

    $blockAppendQuery = " AND block_master.block_id IN ('$blockids')";
    $blockAppendQueryUnit = " AND unit_master.block_id IN ('$blockids')";
    $blockAppendQueryFloor = " AND floors_master.block_id IN ('$blockids') $restictDepartment";
    $blockAppendQueryUser = " AND users_master.block_id IN ('$blockids') $restictDepartmentUser";
    $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

} else {
    $blockAppendQuery = "";
    $blockAppendQueryUnit = "";
    $blockAppendQueryUser = "";
    $blockAppendQueryOnly="";
}
if (isset($_POST) && !empty($_POST)) {

    $response = array();
    extract($_POST);
    $temDate = date("Y-m-d h:i A");
    /////////////////////////////get hr category by id
    if ($_POST['action'] == "getHrDocCategory") {
        if ($hr_document_category_id != '') {
            $q = $d->selectRow('*', "hr_document_category_master", "hr_document_category_id='$hr_document_category_id'");
            $data = mysqli_fetch_assoc($q);
            if ($data) {
                $response["hr_document_category"] = $data;
                $response["message"] = "Holiday List";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Hr Doc Category";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "Category Id Required";
            $response["status"] = "201";
            echo json_encode($response);
        }

    }

    /////////////////////////////get getsalary
    if ($_POST['action'] == "getsalaryById") {
        if ($salary_id != '') {
            $arrayEarning = array();
            $qsd=$d->select("salary_earning_deduction_type_master","society_id='$society_id' AND earn_deduct_is_delete=0");
            while ($earn_data=mysqli_fetch_array($qsd)) {
                array_push($arrayEarning, $earn_data);
            }

            // print_r($arrayEarning);

            $q = $d->selectRow('salary.*,users_master.user_full_name,users_master.user_designation,users_master.user_id', "salary,users_master", "salary.salary_id='$salary_id' AND salary.user_id = users_master.user_id");
            $data = mysqli_fetch_assoc($q);
            if ($data) {
                $mainData=array();
                $q2 = $d->select('salary,salary_master,salary_earning_deduction_type_master,salary_common_value_master',"salary_common_value_master.salary_group_id=salary.salary_group_id AND salary_common_value_master.salary_earning_deduction_id=salary_master.salary_earning_deduction_id AND  salary.salary_id=salary_master.salary_id AND salary_master.salary_id='$data[salary_id]' AND salary_earning_deduction_type_master.salary_earning_deduction_id=salary_master.salary_earning_deduction_id");
                while ($data2 = mysqli_fetch_assoc($q2)) {

                    if ($data2['earning_deduction_type']==1 && $data2['amount_type']==0) {
                         $salary_common_value_earn_deduction =$data2['salary_common_value_earn_deduction'];
                         $salaryCvED = explode(',',$salary_common_value_earn_deduction);
                         $tempValueDedcution = 0;
                         $showNameForApply = array();

                         for ($iD11=0; $iD11 <count($arrayEarning) ; $iD11++) { 
                            
                            $ks = array_search($salaryCvED[$iD11], array_column($arrayEarning, 'salary_earning_deduction_id'));

                            if ($ks === 0 || $ks > 0) {
                              
                               $tempValueDedcutionValue = $arrayEarning[$ks]['salary_earning_deduction_value'];
                               array_push($showNameForApply,$arrayEarning[$ks]['earning_deduction_name']);
                            }
                        }

                        $data2['deduct_view'] = "(". implode('+',$showNameForApply).")";
                    } else {
                        $data2['deduct_view'] = "";
                    }

                    array_push($mainData, $data2);
                }
                
                $data['earn_deduct']=$mainData;
                $response["salary"] = $data;
                $response["message"] = "salary";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "salary";
                $response["status"] = "201";
                echo json_encode($response);

            }
        } else {
            $response["message"] = "salary Id";
            $response["status"] = "201";
            echo json_encode($response);

        }

    }

    /////////////////////////////get getsalary
    if ($_POST['action'] == "getUserCurrentMonthsalary") {
        if ($user_id != '') {
            $crntYear = date('Y-m');
            $q = $d->selectRow('*', "salary,users_master,shift_timing_master", "shift_timing_master.shift_time_id =users_master.shift_time_id AND salary.user_id='$user_id' AND salary.user_id = users_master.user_id");
            $data = mysqli_fetch_assoc($q);
            if ($data) {
                $q2 = $d->select("leave_master", "user_id='$user_id'");
                while ($data2 = mysqli_fetch_assoc($q2)) {

                    array_push($mainData, $data2);
                }
                $q3 = $d->select("attendance_master", "user_id='$user_id' AND DATE_FORMAT(attendance_date_start,'%Y-%m') = '$crntYear'");
                while ($data3 = mysqli_fetch_assoc($q3)) {

                    $totalUserWorked = COUNT($data3);
                }
                $response["salary"] = $data;
                $response["message"] = "salary";
                $response["status"] = "200";
                echo json_encode($response);

            } else {
                $response["message"] = "salary";
                $response["status"] = "201";
                echo json_encode($response);

            }
        } else {
            $response["message"] = "salary Id";
            $response["status"] = "201";
            echo json_encode($response);

        }

    }

    ////////////////////////get doc
    if ($_POST['action'] == "getHrDoc") {
        if ($hr_document_id != '') {
            $q = $d->selectRow('*', "hr_document_master", "hr_document_id='$hr_document_id'");
            $data = mysqli_fetch_assoc($q);
            if ($data) {
                $response["hr_document"] = $data;
                $response["message"] = "HR Doc List";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Hr Doc Category";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "Hr DocId Required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ///////////get leave default count
    if ($_POST['action'] == "getLeaveDefaultCount") {
        if ($leave_default_count_id != '') {
            $q = $d->selectRow('*', "leave_default_count_master", "leave_default_count_id='$leave_default_count_id'");
            $data = mysqli_fetch_assoc($q);
            if ($data) {
                $response["leave_count"] = $data;
                $response["message"] = "Leave Default Count List";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Failure";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID Required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ////////////////////////get doc
    if ($_POST['action'] == "getWorkTIme") {
        if ($working_day_id != '') {
            $q = $d->selectRow('*', "working_days_master", "working_day_id='$working_day_id'");
            $data = mysqli_fetch_assoc($q);
            if ($data) {
                $response["work_time"] = $data;
                $response["message"] = "Work Time List";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Failure";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "Work Time id required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ///////////get leave timing
    if ($_POST['action'] == "getShiftTiming") {
        if ($shift_time_id != '') {
            $week_days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            $weeks = array('Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5');

            $q = $d->selectRow('*', "shift_timing_master", "shift_timing_master.is_deleted =0  AND shift_timing_master.shift_time_id='$shift_time_id'");
            $data = mysqli_fetch_assoc($q);
            $shift_timing = array();
            if($data['tea_break_start_time'] !=""){
                $shift_timing['tea_break_start_time'] = date('h:i A',strtotime($data['tea_break_start_time']));
            }
            else
            {
                $shift_timing['tea_break_start_time'] = "";
            }
            if($data['tea_break_end_time'] !=""){
                $shift_timing['tea_break_end_time'] = date('h:i A',strtotime($data['tea_break_end_time']));
            }
            else
            {
                $shift_timing['tea_break_end_time'] = "";
            }
            $shift_timing['floor_name'] = $data['shift_name'];
            $shift_timing['block_name'] = "";
            $shift_timing['shift_type'] = $data['shift_type'];
            $shift_timing['half_day_time_start'] = date('h:i A ',strtotime($data['half_day_time_start']));
            $shift_timing['halfday_before_time'] = date('h:i A',strtotime($data['halfday_before_time']));
            $shift_timing['shift_start_time'] = date('h:i A',strtotime($data['shift_start_time']));
            $shift_timing['shift_end_time'] = date('h:i A',strtotime($data['shift_end_time']));
            // $shift_timing['lunch_break_start_time'] = date('h:i A',strtotime($data['lunch_break_start_time']));
            // $shift_timing['lunch_break_end_time'] = date('h:i A',strtotime($data['lunch_break_end_time']));
           
            if($data['lunch_break_start_time'] !=""){
                $shift_timing['lunch_break_start_time'] = date('h:i A',strtotime($data['lunch_break_start_time']));
            }
            else
            {
                $shift_timing['lunch_break_start_time'] = "";
            }
            if($data['lunch_break_end_time'] !=""){
                $shift_timing['lunch_break_end_time'] = date('h:i A',strtotime($data['lunch_break_end_time']));
            }
            else
            {
                $shift_timing['lunch_break_end_time'] = "";
            }
            
            $shift_timing['minimum_hours_for_full_day'] = date('h:i',strtotime($data['minimum_hours_for_full_day']));
            $shift_timing['maximum_halfday_hours'] = date('h:i',strtotime($data['maximum_halfday_hours']));
            $shift_timing['per_day_hour'] = date('h:i',strtotime($data['per_day_hour']));
            $shift_timing['late_time_start'] = date('i',strtotime($data['late_time_start']));
            $shift_timing['early_out_time'] = date('i',strtotime($data['early_out_time']));
            $shift_timing['has_altenate_week_off'] = $data['has_altenate_week_off'];
            $week_off_days = explode(',', $data['week_off_days']);
            $selected_off_days = array();
            for ($i = 0; $i < count($week_off_days); $i++) {
               
                $dow_numeric = $week_off_days[$i];
               /*  $get_name = date('D', strtotime("Sunday +{$dow_numeric} days")); //get week day
                print_r($get_name); */
              //  $selected_off_days[] = strtotime('D', strtotime("Sunday +{$dow_numeric} days"));
                $selected_off_days[] = date('D', strtotime("Sunday +{$dow_numeric} days"));
               
            }
            $selected_off_days = implode(', ', $selected_off_days);

            $alternate_weekoff_days = explode(',', $data['alternate_weekoff_days']);
            $selected_alternate_days = array();
            for ($i = 0; $i < count($alternate_weekoff_days); $i++) {
                $dow_numeric = $alternate_weekoff_days[$i];
              //  $selected_alternate_days[] = strtotime('%A', strtotime("Sunday +{$dow_numeric} days"));
                $selected_alternate_days[] = date('D', strtotime("Sunday +{$dow_numeric} days"));;
            }
            $selected_alternate_days = implode(', ', $selected_alternate_days);

            $alternate_weekoff_days = explode(',', $data['alternate_weekoff_days']);
            $shift_timing['week_off_days'] = $selected_off_days;
            $shift_timing['alternate_week_off'] = $data['alternate_week_off'];
            $shift_timing['alternate_weekoff_days'] = $selected_alternate_days;
            if ($data) {
                $response["shift_timing"] = $shift_timing;
                $response["message"] = "Shift Timing List";
                $response["status"] = "200";

                echo json_encode($response);
            } else {
                $response["message"] = "Failure";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID Required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ////////////////////////get doc
    if ($_POST['action'] == "getLeaveType") {
        if ($leave_type_id != '') {
            $q = $d->selectRow('*', "leave_type_master", "leave_type_id='$leave_type_id'");
            $data = mysqli_fetch_assoc($q);
            $data = array_map("html_entity_decode", $data);
            if ($data) {
                $response["leave_type"] = $data;
                $response["message"] = "Leave Type";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Leave Type";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    ////////////////////////get doc
    if ($_POST['action'] == "getLeaveAssign") {
        if ($user_leave_id != '') {
            $q = $d->selectRow("*,
            (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = leave_assign_master.user_id 
            AND leave_status=1 AND paid_unpaid=0 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=0 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')=leave_assign_master.assign_leave_year) AS total_full_day_leave, 
            (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = leave_assign_master.user_id 
            AND leave_status=1 AND paid_unpaid=0 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=1 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')=leave_assign_master.assign_leave_year) AS total_half_day_leave, 
            (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = leave_assign_master.user_id AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year=leave_assign_master.assign_leave_year AND is_leave_carry_forward=0) AS pay_out_leave,
            (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = leave_assign_master.user_id AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year=leave_assign_master.assign_leave_year AND is_leave_carry_forward=1) AS carry_forward_leave
            ", "leave_assign_master", "user_leave_id='$user_leave_id'");
            $data = mysqli_fetch_assoc($q);
            if ($data) {
                $response["leave_assign"] = $data;
                $response["message"] = "Leave Assign";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Leave Assign";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ////////////////////////get Idea Category
    if ($_POST['action'] == "getIdeaCategory") {
        if ($idea_category_id != '') {
            $q = $d->selectRow('*', "idea_category_master", "idea_category_id='$idea_category_id'");
            $data = mysqli_fetch_assoc($q);
            $data = array_map("html_entity_decode", $data);
            if ($data) {
                $response["idea_category"] = $data;
                $response["message"] = "Idea Category";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Idea Category";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    ////////////////////////get course
    if ($_POST['action'] == "getCourseById") {
        if ($course_id != '') {
            $q = $d->selectRow('*', "course_master", "course_id='$course_id'");
            $data = mysqli_fetch_assoc($q);
            $data = array_map("html_entity_decode", $data);  
            if ($data) {
                $response["course"] = $data;
                $response["message"] = "Course";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["course"] = "";
                $response["message"] = "Course";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    if ($_POST['action'] == "getLessonTypeById") {
        if ($lesson_type_id != '') {
            $q = $d->selectRow('*', "lesson_type_master", "lesson_type_id='$lesson_type_id'");
            $data = mysqli_fetch_assoc($q);
            $data = array_map("html_entity_decode", $data); 
            if ($data) {
                $response["lesson_type"] = $data;
                $response["message"] = "lesson_type";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["lesson_type"] = "";
                $response["message"] = "lesson_type";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ////////////////////////get getHrDocByCategory
    if ($_POST['action'] == "getHrDocByCategory") {
        $mainData = array();
        if ($hr_document_categroy_id != '') {
            $q = $d->select("hr_document_master", "hr_document_category_id='$hr_document_categroy_id'");
            while ($data = mysqli_fetch_assoc($q)) {
                array_push($mainData, $data);
            }
            if ($mainData) {
                $response["hr_document"] = $mainData;
                $response["message"] = "Hr Document";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Hr Document";
                $response["hr_document"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    ////////////////////////get user by floor id
    if ($_POST['action'] == "getUserByFloor") {
        $mainData = array();
        if ($floor_id != '') {
            $q = $d->selectRow('users_master.*,user_employment_details.joining_date',"users_master LEFT JOIN user_employment_details ON user_employment_details.user_id =users_master.user_id", "users_master.floor_id='$floor_id' AND users_master.delete_status=0 AND users_master.user_status=1 $blockAppendQueryUser","ORDER BY users_master.user_full_name");
            while ($data = mysqli_fetch_assoc($q)) {
                array_push($mainData, $data);
            }
            if ($mainData) {
                $response["users"] = $mainData;
                $response["message"] = "users";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "users";
                $response["users"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    if ($_POST['action'] == "getUserByFloorForSalarySlip") {
        $mainData = array();
        if ($floor_id != '') {
            
            $startDate = date("$year-$month-01");
            $month_end_date = date("Y-m-t", strtotime($startDate));

            $q = $d->selectRow("block_master.block_name,floors_master.floor_name,user_employment_details.joining_date,users_master.*,salary.*","block_master,floors_master,user_employment_details,users_master LEFT JOIN salary ON salary.user_id = users_master.user_id AND salary.is_preivous_salary=0 AND salary.gross_salary>0 AND salary.is_delete=0 ", "user_employment_details.user_id = users_master.user_id  AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.society_id='$society_id' AND users_master.user_status=1 AND users_master.floor_id='$floor_id'  AND  (users_master.delete_status=0  AND users_master.register_date<='$month_end_date' OR users_master.register_date<='$month_end_date' AND users_master.delete_status=1 AND users_master.deleted_date>='$startDate')
            ", "ORDER BY users_master.user_full_name ASC");


            // $q = $d->select("users_master", "floor_id='$floor_id' AND delete_status=0  ");
            while ($data = mysqli_fetch_assoc($q)) {
                $checkSalary = $d->selectRow('*', "salary", "is_delete=0 AND is_active=0 AND user_id = '$data[user_id]'");
                $checkSalaryData = mysqli_fetch_assoc($checkSalary);
               if($checkSalaryData) {
                   array_push($mainData, $data);
               }
            }
            if ($mainData) {
                $response["users"] = $mainData;
                $response["message"] = "users $startDate";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "users";
                $response["users"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    if ($_POST['action'] == "getUserByFloorForBank") {
        $mainData = array();
        if ($floor_id != '') {
            $q = $d->select("users_master", "floor_id='$floor_id' AND delete_status=0 AND active_status=0 ");
            while ($data = mysqli_fetch_assoc($q)) {
                $user_id= $data['user_id'];
                $q2 = $d->selectRow('*',"user_bank_master", "user_id='$user_id'");
                $data2= mysqli_fetch_assoc($q2); 
              ///  print_R($data2);
                if(!isset($data2))
                {
                    array_push($mainData, $data);
                }
               
            }
            if ($mainData) {
                $response["users"] = $mainData;
                $response["message"] = "users";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "users";
                $response["users"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    ////////////////////////get attendance by user id
    if ($_POST['action'] == "get_attendance_by_user") {
        $mainData = array();
        $mainData2 = array();
        $mainData3 = array();
        $totalHoursArray = array();
        $totalMinArray = array();
        if ($user_id != '') {

            $q = $d->select("attendance_master,shift_timing_master", "attendance_master.user_id='$user_id' AND attendance_master.shift_time_id = shift_timing_master.shift_time_id");
            while ($data = mysqli_fetch_assoc($q)) {
              
                $time3 = new DateTime($data['punch_in_time']);
                $time4 = new DateTime($data['punch_out_time']);
                $workingHours1 = $time4->diff($time3);
                $workingHours = $workingHours1->format('%H');
                if ($workingHours < 1) {
                    $workingMin = $workingHours1->format('%i');
                }
                $data['wokring_hour']=$workingHours;
                $data['wokring_min']=$workingMin;
                array_push($mainData, $data);
                array_push($totalHoursArray, $data['wokring_hour']);
                array_push($totalMinArray, $data['wokring_min']);
            }
            $q2 = $d->select("holiday_master", "holiday_master.society_id='$society_id' AND holiday_status=0");
            while ($data2 = mysqli_fetch_assoc($q2)) {
                array_push($mainData2, $data2);
            }
            $q3 = $d->select("leave_master,leave_type_master", "leave_master.society_id='$society_id' AND leave_master.leave_status =1 AND leave_master.user_id = '$user_id' AND leave_master.leave_type_id = leave_type_master.leave_type_id ");
            while ($data3 = mysqli_fetch_assoc($q3)) {
                array_push($mainData3, $data3);
            }

            if ($mainData) {
                $response["attendance"] = $mainData;
                $response["holiday"] = $mainData2;
                $response["leave"] = $mainData3;
                $response["total_hours"] = array_sum($totalHoursArray);
                $response["total_min"] = floor(array_sum($totalMinArray) / 60);
                $response["message"] = "attendance";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "attendance";
                $response["attendance"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ////////////////////////get getHrDocByCategory
    if ($_POST['action'] == "getShiftByDeptId") {
        $mainData = array();
        if ($floor_id != '') {
            $q = $d->select("shift_timing_master", "floor_id='$floor_id' AND  is_deleted=0");

            while ($data = mysqli_fetch_assoc($q)) {
                array_push($mainData, $data);
            }
            if ($mainData) {
                $response["shift"] = $mainData;
                $response["message"] = "Shift Timing";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Shift Timing";
                $response["hr_document"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ////////////////////////get course lesson
    if ($_POST['action'] == "getCourseLesson") {
        if ($lessons_id != '') {
            $q = $d->selectRow('course_lessons_master.*,lesson_type_master.lesson_type', "course_lessons_master LEFT JOIN lesson_type_master ON lesson_type_master.lesson_type_id = course_lessons_master.lesson_type_id","course_lessons_master.lessons_id='$lessons_id'");
            $data = mysqli_fetch_assoc($q);
            $data = array_map("html_entity_decode", $data); 
            $data['lesson_created_date'] = date("d M Y h:i A", strtotime($data['lesson_created_date']));
           /*  $course_lesson = array();
            $course_lesson['course_chapter_id'] = $data['course_chapter_id'];
            $course_lesson['course_id'] = $data['course_id'];
            $course_lesson['course_title'] = $data['course_title'];
            $course_lesson['lesson_title'] = $data['lesson_title'];
            $course_lesson['lesson_type_id'] = $data['lesson_type_id'];
            $course_lesson['lessons_id'] = $data['lessons_id'];
            $course_lesson['chapter_description'] = $data['chapter_description'];
            $course_lesson['chapter_title'] = $data['chapter_title'];
            $course_lesson['lesson_created_date'] = date("d M Y h:i A", strtotime($data['lesson_created_date']));
            $course_lesson['lesson_video'] = $data['lesson_video']; */
            if ($data) {
                $response["course_lesson"] = $data;
                $response["message"] = "Course Lesson";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["course"] = "";
                $response["message"] = "Course Lesson";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ////////////////////////Attendance Type/////////////////
    if ($_POST['action'] == "getAttendanceType") {
        if ($attendance_type_id != '') {
            $q = $d->selectRow('*', "attendance_type_master", "attendance_type_id='$attendance_type_id'");
            $data = mysqli_fetch_assoc($q);
            $data = array_map("html_entity_decode", $data);
            if ($data) {
                $response["attendance_type"] = $data;
                $response["message"] = "Attendance Type";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Attendance Type";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    ////////////////////////Attendance Type/////////////////
    if ($_POST['action'] == "getbankById") {
        if ($bank_id != '') {
            $q = $d->selectRow('user_bank_master.*,users_master.user_id,users_master.floor_id,users_master.floor_id,users_master.block_id,users_master.user_full_name,floors_master.floor_id,floors_master.floor_name,block_master.block_id,block_master.block_name', "user_bank_master LEFT JOIN users_master ON users_master.user_id=user_bank_master.user_id LEFT JOIN floors_master ON floors_master.floor_id=users_master.floor_id LEFT JOIN block_master ON block_master.block_id=users_master.block_id","user_bank_master.bank_id='$bank_id'");
            $data = mysqli_fetch_assoc($q);
            if ($data) {
                $response["bank"] = $data;
                $response["message"] = "Bank";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Bank";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    ////////////////////////Attendance Type/////////////////
    if ($_POST['action'] == "getCourseChapterByCourse") {
        if ($course_id != '') {
            $mainData = array();
            $q = $d->Select("course_chapter_master", "course_id=$course_id");
            while ($data = mysqli_fetch_assoc($q)) {
                array_push($mainData, $data);
            }
            if ($mainData) {
                $response["course_chapter"] = $mainData;
                $response["message"] = "Attendance Type";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Attendance Type";
                $response["course_chapter"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

/////////////////////////////////get attendance by id

    if ($_POST['action'] == "get_attendace_by_id") {

        if ($attendance_id != "") {
            $attendaceArray = array();
            $q2 = $d->selectRow('leave_master.leave_type_id,attendance_master.*,DATE_FORMAT(attendance_master.attendance_date_start,"%d %M %Y") AS attendance_date_start,DATE_FORMAT(attendance_master.attendance_date_end,"%d %M %Y") AS attendance_date_end,DATE_FORMAT(attendance_master.punch_in_time,"%h:%i %p") AS punch_in_time,CASE  WHEN attendance_master.punch_out_time !="00:00:00" THEN DATE_FORMAT(attendance_master.punch_out_time,"%h:%i %p")  ELSE punch_out_time END  punch_out_time,work_report_master.work_report AS work_report_data', "attendance_master LEFT JOIN leave_master ON leave_master.user_id = attendance_master.user_id AND leave_master.leave_start_date=attendance_master.attendance_date_start
            LEFT JOIN work_report_master ON work_report_master.work_report_date=attendance_master.attendance_date_start AND  work_report_master.user_id = attendance_master.user_id", "attendance_master.attendance_id='$attendance_id'");
            $data2 = mysqli_fetch_assoc($q2);
            $attendace = array();
            
            if($data2['attendance_date_end'] !=null)
            {
                $data2['attendance_date_end'] = $data2['attendance_date_end'];

            }
            else
            {
                $data2['attendance_date_end'] = "";
            }
            
            if($data2['attendance_in_from'] !=null)
            {
                if($data2['attendance_in_from'] ==0){
                    $data2['attendance_in_from']="Face App";
                }else
                {
                    $data2['attendance_in_from']="User App";
                }
            }
            else
            {
                $data2['attendance_in_from'] = "";
            }
            if($data2['attendance_out_from'] !=null)
            {
                if($data2['attendance_out_from'] ==0){
                    $data2['attendance_out_from']="Face App";
                }else
                {
                    $data2['attendance_out_from']="User App";
                }
            }
            else
            {
                $data2['attendance_out_from'] = "";
            }
            if($data2['punch_in_time'] !="00:00:00")
            {
                $data2['in_time'] = date('h:i A', strtotime($data2['punch_in_time']));
            }
            else
            {
                $data2['in_time'] = "";
            }
            if($data2['punch_out_time'] !="00:00:00")
            {
                if ($data2['attendance_start_date']!=$data2['attendance_date_end'] && $data2['attendance_date_end']!="0000-00-00" ) {
                    $nextDayMsg = '('.date('d M', strtotime($data2['attendance_date_end'])).')';
                }
                $data2['out_time'] = date('h:i A ', strtotime($data2['punch_out_time'])).' '.$nextDayMsg;
            }
            else
            {
                $data2['out_time'] = "";
            }
            if($data2['attendance_status_change_by'] !=null)
            {

                if($data2['attendance_status_change_by_type']==1){
                    /// Admin
                    $q5 = $d->selectRow('*','bms_admin_master',"admin_id='$data2[attendance_status_change_by]'");
                    $bms_admin_master = mysqli_fetch_assoc($q5);
                    if( $bms_admin_master ){
        
                        $data2['changed_by'] = $bms_admin_master['admin_name'];
                    }
                    else
                    {
                        $data2['changed_by'] = "";
                    }
        
                }
                else
                {
                    $q5 = $d->selectRow('*','users_master',"user_id='$data2[attendance_status_change_by]'");
                    $users_master = mysqli_fetch_assoc($q5);
                    if( $users_master ){
    
                        $data2['changed_by'] = $users_master['user_full_name'];
                    }
                    else
                    {
                        $data2['changed_by'] = "";
                    }
        
                    
                }
            }
            else
            {
                $data2['changed_by'] = "";

            }
           
            $q = $d->select("attendance_break_history_master,attendance_type_master", "attendance_type_master.attendance_type_id = attendance_break_history_master.attendance_type_id AND attendance_break_history_master.attendance_id='$attendance_id'");
            if (mysqli_num_rows($q2) > 0) {
             
                while ($data = mysqli_fetch_array($q)) {
                   
                    
                    if($data['break_out_time'] !="00:00:00"){

                        $time1 = new DateTime($data['break_in_time']);
                        $time2 = new DateTime($data['break_out_time']);
                        $dtime = "";
                        $interval1 = $time1->diff($time2);
                        $hr = $interval1->format('%h');
                        $interval = $interval1->format('%i');
                        if($hr>0){
                          $dtime =   $interval1->format('%h Hour %i Min');
                        }
                        else
                        {
                            if($interval>0){
                                $dtime = $interval1->format('%i Min');
                            }else
                            {
                                $dtime = $interval1->format('%s seconds');
                            }
                        }
                       
                        $attendace["total_break"] = $dtime;
                    }else
                    {
                        $time1 = new DateTime($data['break_in_time']);
                        $time2 = new DateTime(date('H:i:s'));
                        $dtime = "";
                        $interval1 = $time1->diff($time2);
                        $hr = $interval1->format('%h');
                        $interval = $interval1->format('%i');
                        if($hr>0){
                          $dtime =   $interval1->format('%h Hour %i Min');
                        }
                        else
                        {
                            if($interval>0){
                                $dtime = $interval1->format('%i Min');
                            }else
                            {
                                $dtime = $interval1->format('%s seconds');
                            }
                        }
                       
                        $attendace["total_break"] = $dtime;
                    }
                    $attendace["attendance_break_history_id"] = $data['attendance_break_history_id'];
                    $attendace["attendance_id"] = $data['attendance_id'];
                    $attendace["attendance_type_id"] = $data['attendance_type_id'];
                    $attendace["break_start_date"] = $data['break_start_date'];
                    if($data['break_out_time'] !="00:00:00"){

                        $attendace["break_out_time"] = date('h:i A',strtotime($data['break_out_time']));
                    }else
                    {
                        $attendace["break_out_time"] = "Not Out";
                    }
                    $attendace["break_in_time"] = date('h:i A',strtotime($data['break_in_time']));
                  //  $attendace["break_out_time"] = $data['break_out_time'];
                    $attendace["break_in_latitude"] = $data['break_in_latitude'];
                    $attendace["break_in_longitude"] = $data['break_in_longitude'];
                    $attendace["break_out_latitude"] = $data['break_out_latitude'];
                    $attendace["break_out_longitude"] = $data['break_out_longitude'];
                    $attendace["attendance_type_name"] = $data['attendance_type_name'];

                    array_push($attendaceArray, $attendace);
                }

                if ($data2['auto_leave']==1 && $data2['leave_type_id']==0 && $data2['attendance_status']!=2) {
                   $attendance_date_start_leave = date('Y-m-d', strtotime($data2['attendance_date_start']));
                    $ql=$d->selectRow("auto_leave_reason","leave_master","leave_type_id=0 AND user_id='$data2[user_id]' AND leave_start_date='$attendance_date_start_leave'");
                    $autoLeaveData=mysqli_fetch_array($ql);
                    $data2['auto_leave_reason'] = '('.$autoLeaveData['auto_leave_reason'].')';
                    $data2['auto_leave'] = "1";
                } else {
                    $data2['auto_leave_reason'] = "";
                    $data2['auto_leave'] = "0";
                }

                $multiple_punch_in_out_data = $data2['multiple_punch_in_out_data'];
                $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

                $data2['punch_in_data'] = array();

                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                    for ($i=0; $i < count($multiplePunchDataArray); $i++) { 
                        $punchInData = array();

                        $punchInData['punch_in_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_in_date"])).' ('.date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_in_time"])).')';

                        if ($multiplePunchDataArray[$i]["punch_out_date"] != '' && $multiplePunchDataArray[$i]["punch_out_date"] != "00:00:00") {
                            $punchInData['punch_out_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_out_date"])).' ('.date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_out_time"])).')';
                        }else{
                            $punchInData['punch_out_date'] = "";
                        }

                       

                        if ($multiplePunchDataArray[$i]["working_hour"] != '' && $multiplePunchDataArray[$i]["working_hour"] != "00:00") {
                            $punchInData['working_hour'] = $multiplePunchDataArray[$i]["working_hour"];
                        }else{
                            $punchInData['working_hour'] = "";
                        }
                        $punchInData['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"];
                        $punchInData['punch_in_branch'] = $multiplePunchDataArray[$i]["location_name_in"];
                        $punchInData['punch_out_branch'] = $multiplePunchDataArray[$i]["location_name_out"];

                        array_push($data2['punch_in_data'],$punchInData);
                    }
                }
                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray)>0) {
                    $data2['total_punch']= count($multiplePunchDataArray);
                }else {
                    $data2['total_punch']= "0";
                }
                $data2['leave_type_id'] = $data2['leave_type_id'];
                // $data2['multiplePunchDataArray'] = $multiplePunchDataArray;
                $data2['attedance'] = $attendaceArray;
                $response["message"] = "attendace List";
                $response["status"] = "200";
                $response["data"] = $data2;
                echo json_encode($response);

            } else {

                $response["message"] = "No data Found.";
                $response["status"] = "201";
                echo json_encode($response);

            }

        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);

        }
    }

    /////////////////////////////////////////////get attendance by user id with date
    if ($_POST['action'] == "get_attendance_today_attendance_data") {

        if ($user_id != "") {
            $attendaceArray = array();
            $q2 = $d->selectRow('*', "attendance_master LEFT JOIN work_report_master ON work_report_master.user_id = attendance_master.user_id AND work_report_master.user_id='$user_id' AND work_report_master.work_report_date = '$date'", "attendance_master.user_id='$user_id' AND attendance_master.attendance_date_start ='$date'");
            $data2 = mysqli_fetch_assoc($q2);
            $q = $d->select("attendance_break_history_master,attendance_type_master", "attendance_type_master.attendance_type_id = attendance_break_history_master.attendance_type_id AND attendance_break_history_master.attendance_id='$data2[attendance_id]'");
            if (mysqli_num_rows($q2) > 0) {
                $work = $d->selectRow('work_report', "work_report_master", "work_report_date='$date' AND user_id ='$user_id'");
                $workReport = mysqli_fetch_assoc($work);
                if($workReport)
                {
                    $workReport = $workReport['work_report'];
                }
                else
                {
                    $workReport = "";
                }
                $day = date('l', strtotime($date));
                if ($data2['total_working_hours']=='') {
                    $workingHours = $d->getTotalHoursWithNames($data2['attendance_date_start'],$data2['attendance_date_end'],$data2['punch_in_time'],$data2['punch_out_time']);
                } else {
                    $workingHours = $data2['total_working_hours'];
                }
                
                while ($data = mysqli_fetch_array($q)) {
                    $attendace = array();
                    if($data['break_out_time'] !="00:00:00"){

                        $time1 = new DateTime($data['break_in_time']);
                        $time2 = new DateTime($data['break_out_time']);
                        $dtime = "";
                        $interval1 = $time1->diff($time2);
                        $hr = $interval1->format('%h');
                        $interval = $interval1->format('%i');
                        if($hr>0){
                          $dtime =   $interval1->format('%h Hour %i Min');
                        }
                        else
                        {
                            if($interval>0){
                                $dtime = $interval1->format('%i Min');
                            }else
                            {
                                $dtime = $interval1->format('%s seconds');
                            }
                        }
                       
                        $attendace["total_break"] = $dtime;
                    }else
                    {
                        $time1 = new DateTime($data['break_in_time']);
                        $time2 = new DateTime(date('H:i:s'));
                        $dtime = "";
                        $interval1 = $time1->diff($time2);
                        $hr = $interval1->format('%h');
                        $interval = $interval1->format('%i');
                        if($hr>0){
                          $dtime =   $interval1->format('%h Hour %i Min');
                        }
                        else
                        {
                            if($interval>0){
                                $dtime = $interval1->format('%i Min');
                            }else
                            {
                                $dtime = $interval1->format('%s seconds');
                            }
                        }
                       
                        $attendace["total_break"] = $dtime;
                        
                    }
                   
                    $attendace["attendance_break_history_id"] = $data['attendance_break_history_id'];
                    $attendace["attendance_id"] = $data['attendance_id'];
                    $attendace["attendance_type_id"] = $data['attendance_type_id'];
                    $attendace["break_start_date"] = $data['break_start_date'];
                    $attendace["break_end_date"] = $data['break_end_date'];
                    $attendace["break_in_time"] = date('h:i A',strtotime($data['break_in_time']));
                    if($data['break_out_time'] !="00:00:00"){ 
                        $attendace["break_out_time"] =   date('h:i A',strtotime($data['break_out_time']));
                     }else{
                        $attendace["break_out_time"] = "";
                     }
                    $attendace["break_in_latitude"] = $data['break_in_latitude'];
                    $attendace["break_in_longitude"] = $data['break_in_longitude'];
                    $attendace["break_out_latitude"] = $data['break_out_latitude'];
                    $attendace["break_out_longitude"] = $data['break_out_longitude'];
                    $attendace["attendance_type_name"] = $data['attendance_type_name'];
                    array_push($attendaceArray, $attendace);
                }
                $data2['punch_in_time'] = date('h:i A',strtotime($data2['punch_in_time']));
                $data2['day'] = $day;
                if($data2['punch_out_time'] !="00:00:00" && $data2['attendance_start_date']!=$data2['attendance_date_end']){
                    $data2['punch_out_time'] = date('h:i A',strtotime($data2['punch_out_time'])).' ('.date('d M Y',strtotime($data2['attendance_date_end'])).')';
                } else if($data2['punch_out_time'] !="00:00:00" ){
                    $data2['punch_out_time'] = date('h:i A',strtotime($data2['punch_out_time']));
                }
                else
                {
                    if($date<date('Y-m-d')){
                        $data2['punch_out_time'] = "Punch Out Missing";
                    }
                    else
                    {
                        $data2['punch_out_time'] = "Not Punch Out";
                    }
                }


                $multiple_punch_in_out_data = $data2['multiple_punch_in_out_data'];
                $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

                $data2['punch_in_data'] = array();

                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                    for ($i=0; $i < count($multiplePunchDataArray); $i++) { 
                        $punchInData = array();

                        $punchInData['punch_in_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_in_date"]));
                        $punchInData['punch_in_time'] = date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_in_time"]));

                        if ($multiplePunchDataArray[$i]["punch_out_date"] != '' && $multiplePunchDataArray[$i]["punch_out_date"] != "00:00:00") {
                            $punchInData['punch_out_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_out_date"]));
                        }else{
                            $punchInData['punch_out_date'] = "";
                        }

                        if ($multiplePunchDataArray[$i]["punch_out_time"] != '' && $multiplePunchDataArray[$i]["punch_out_time"] != "00:00:00") {
                            $punchInData['punch_out_time'] = date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_out_time"]));
                        }else{
                            $punchInData['punch_out_time'] = "";
                        }

                        if ($multiplePunchDataArray[$i]["working_hour"] != '' && $multiplePunchDataArray[$i]["working_hour"] != "00:00") {
                            $punchInData['working_hour'] = $multiplePunchDataArray[$i]["working_hour"];
                        }else{
                            $punchInData['working_hour'] = "";
                        }
                        $punchInData['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"];

                        array_push($data2['punch_in_data'],$punchInData);
                    }
                }

                $data2["total_punch"] = count($multiplePunchDataArray);
                $data2["workReport"] = $workReport.'';
                $data2['total_working'] = $workingHours;
                $data2['attedance'] = $attendaceArray;
                $response["message"] = "attendace List";
                $response["status"] = "200";
                $response["data"] = $data2;
                echo json_encode($response);

            } else {

                $response["message"] = "No Data Found.";
                $response["status"] = "201";
                echo json_encode($response);
            }

        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);

        }
    }

    ////////////////////////get course Chapter/////////////////
    if ($_POST['action'] == "getcourseChapterByChapterId") {
        if ($course_chapter_id != '') {
            $mainData = array();
            $q = $d->Select("course_chapter_master,course_master", "course_chapter_master.course_id=course_master.course_id AND course_chapter_master.course_chapter_id=$course_chapter_id");
            $data = mysqli_fetch_assoc($q);
            $data = array_map("html_entity_decode", $data); 
            $course_chapter = array();
            $course_chapter['course_chapter_id'] = $data['course_chapter_id'];
            $course_chapter['course_id'] = $data['course_id'];
            $course_chapter['course_title'] = $data['course_title'];
            $course_chapter['chapter_title'] = $data['chapter_title'];
            $course_chapter['chapter_description'] = $data['chapter_description'];
            $course_chapter['course_chapter_created_date'] = date("d M Y h:i A", strtotime($data['course_chapter_created_date']));
            if ($data) {
                $response["course_chapter"] = $course_chapter;
                $response["message"] = "Course Chapter";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Course Chapter";
                $response["course_chapter"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

/* Dollop Infotech date 22-oct-2021 -   */
    if ($_POST['action'] == "getFloorById") {
        if ($floor_id != '') {
            $q = $d->selectRow('*', "floors_master", "floor_id='$floor_id'");
            $data = mysqli_fetch_assoc($q);
            if ($data) {
                $response["floor"] = $data;
                $response["message"] = "Floor Data";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Floor Data";
                $response["floor"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    if ($_POST['action'] == "getemployeeExpenses") {
        if ($user_expense_id != '') {
            $q = $d->selectRow('user_expenses.*,floors_master.*,users_master.*,approved_user.user_full_name AS approved_user_name,approved_admin.admin_name AS approved_admin_name,reject_admin.admin_name AS reject_admin_name,reject_user.user_full_name AS reject_user_name,paid_by_admin.admin_name AS paid_by_admin_name,paid_by_user.user_full_name AS paid_by_user_name',"user_expenses LEFT JOIN users_master AS approved_user ON approved_user.user_id=user_expenses.expense_approved_by_id LEFT JOIN bms_admin_master AS approved_admin ON approved_admin.admin_id=user_expenses.expense_approved_by_id
            LEFT JOIN bms_admin_master AS reject_admin ON reject_admin.admin_id=user_expenses.expense_reject_by_id LEFT JOIN users_master AS reject_user ON reject_user.user_id=user_expenses.expense_reject_by_id LEFT JOIN users_master AS paid_by_user ON paid_by_user.user_id=user_expenses.expense_paid_by_id LEFT JOIN bms_admin_master AS paid_by_admin ON paid_by_admin.admin_id=user_expenses.expense_paid_by_id,floors_master,users_master ", "users_master.user_id=user_expenses.user_id AND user_expenses.floor_id=floors_master.floor_id AND user_expenses.user_expense_id='$user_expense_id'");
            $data = mysqli_fetch_assoc($q);
            $expenses = array();
            $expenses['reject_user_name'] = $data['reject_user_name'];
            $expenses['paid_by_admin_name'] = $data['paid_by_admin_name'];
            $expenses['paid_by_user_name'] = $data['paid_by_user_name'];
            $expenses['expense_title'] = $data['expense_title'];
            $expenses['approved_user_name'] = $data['approved_user_name'];
            $expenses['reject_admin_name'] = $data['reject_admin_name'];
            $expenses['expense_paid_by_id'] = $data['expense_paid_by_id'];
            $expenses['expense_approved_by_id'] = $data['expense_approved_by_id'];
            $expenses['expense_reject_by_id'] = $data['expense_reject_by_id'];
            $expenses['expense_approved_by_type'] = $data['expense_approved_by_type'];
            $expenses['expense_reject_by_type'] = $data['expense_reject_by_type'];
            $expenses['expense_paid_by_type'] = $data['expense_paid_by_type'];
            $expenses['approved_admin_name'] = $data['approved_admin_name'];
            $expenses['description'] = $data['description'];
            $expenses['date'] = date("d M Y", strtotime($data['date']));
            $expenses['amount'] = $data['amount'];
            $expenses['floor_name'] = $data['floor_name'];
            $expenses['user_full_name'] = $data['user_full_name'];
            $expenses['user_designation'] = $data['user_designation'];
           

            $docArray = array();
            
            $jsonArray = json_decode($data['expense_document'],true);

            if (is_array($jsonArray)) {
                for ($i=0; $i < count($jsonArray); $i++) {
                    $i1 = $i+1;
                    $document = $jsonArray["document_".$i];
                    // $docs['name'] = $document;
                    // $docs['document'] = $base_url."img/expense_document/" .$document;
                    array_push($docArray,"<a href=".$base_url."img/expense_document/".$document.">$i1. $document</a>");
                }
            }else if($data['expense_document']!=''){
                 $document = $data['expense_document'];
                array_push($docArray,"<a href=".$base_url."img/expense_document/".$document.">$document</a>");
            }

            $expenses['expense_document'] = implode(", ", $docArray);

            $expenses['expense_reject_reason'] = $data['expense_reject_reason'];
            if ($data) {
                $response["expenses"] = $expenses;
                $response["message"] = "expenses Data";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "expenses Data";
                $response["expenses"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

    //////for generate salary slip
    if ($_POST['action'] == "getUserSalarySlip") {
        if ($user_id != '') {
            if($month !="")
            {
                $current_month = $month;
            }
            else
            {
                $current_month = date('m');
            }
            if($year !="")
            {
                $current_year = $year;
            }
            else
            {
                $current_year = date('Y');
            }
            $days = cal_days_in_month(CAL_GREGORIAN, $current_month, date('Y'));

            $curent_month_year =  date('m-Y');

            $q = $d->selectRow('*', "floors_master,users_master", "users_master.user_id='$user_id' AND users_master.floor_id=floors_master.floor_id");
            $data = mysqli_fetch_assoc($q);
            if($data){
                $user_shift = $d->selectRow('*', "shift_timing_master", "shift_time_id='$data[shift_time_id]'"); 
                $user_shift_data = mysqli_fetch_assoc($user_shift);
                $shift_weekoff = explode(',',$user_shift_data['week_off_days']);
                for ($i = 1; $i <= $days; $i++) {

                    $date = $current_year.'/'.$current_month.'/'.$i; //format date
                    $get_name = date('N', strtotime($date)); //get week day
                    //$day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
                   
                    //if not a weekend add day to array
                    if (in_array($get_name, $shift_weekoff)){
                        $workdays[] = $i;
                    }
                }
                ////print_r($workdays);die; 4 days
                $totalworkingdays = $days - count($workdays);
                $holiday = $d->selectRow('*', "holiday_master", "DATE_FORMAT(holiday_start_date,'%m-%Y')='$current_month-$current_year'"); 
                $holiday_data = mysqli_fetch_assoc($holiday);
               
                if($holiday_data){
                    $totalworkingdays = $totalworkingdays - count($holiday_data);
                }
                $user_salary = $d->selectRow('*', "salary", "user_id='$user_id'"); 
                $user_salary_data = mysqli_fetch_assoc($user_salary);
                
                if($user_salary_data){
                    if($user_salary_data['basic_salary'] !="")
                    {
                        $basic = ($user_salary_data['pf'] / 100) * $user_salary_data['basic_salary'];
                        $user_salary_data['basic_salary']=$user_salary_data['basic_salary']-$basic;
                    }
                   
                    if($user_salary_data['salary_type']=='working_days_salary'){
                        $perday_salary = ($user_salary_data['basic_salary'] + $user_salary_data['hra']+$user_salary_data['conveyance'] )/ $totalworkingdays;
                    }
                    else
                    {
                        $perday_salary = ($user_salary_data['basic_salary'] + $user_salary_data['hra']+$user_salary_data['conveyance'] )/ $totalworkingdays;

                    }
                   
                    $leave = $d->Select("leave_master","DATE_FORMAT(leave_start_date,'%m-%Y')='$current_month-$current_year'");
                    $total_leave = array();
                    while ($leaveData = mysqli_fetch_array($leave)) {
                    if($leaveData)
                    {
                        $start_month =  date('%m-%Y', strtotime($leaveData['leave_start_date']));
                        $end_month =  date('%m-%Y', strtotime($leaveData['leave_end_date']));
                        if($start_month == $end_month){
                            if($leaveData['leave_status'] != 1){
                                array_push($total_leave, $leaveData['leave_total_days']);
                            }
                        }else{
                            $leave_start_date = strtotime($leaveData['leave_start_date']);
                            $leave_end_date = strtotime($leaveData['leave_end_date']);
                            $stepVal = '+1 day';
                            while( $leave_start_date <= $leave_end_date ) {
                                $dates[] = date('%m-%Y', $leave_start_date);
                                $leave_start_date = strtotime($stepVal, $leave_start_date);
                            }
                            
                            foreach($dates as $single_date){
                                if($curent_month_year == $single_date){
                                    array_push($total_leave,1);
                                }
                            }
                        }
                    }}
                    $total_leave_count = array_sum($total_leave);
                  //  $totalUserworkingdays = $totalworkingdays-$total_leave_count;
                    
                    $is_checked = 1;
                    $user_expanse_amount = array();
                    if($is_checked == 1){
                        $user_expanse = $d->select("user_expenses", "user_id='$user_id' AND expense_paid_status = 0"); 
                        while ($user_expanse_data = mysqli_fetch_array($user_expanse)) {
                            if($user_expanse_data ){
                                array_push($user_expanse_amount,$user_expanse_data['amount']);
                            }
                        }
                    }
                    
                    $user_expanse_amount_val= array_sum($user_expanse_amount);
                   
                   
                    $net_salary = $net_salary- ($user_salary_data['others_deduction']-$user_salary_data['others_deduction']-$user_salary_data['professional_tex']);
                    $attendaceArrayData = array();
                   
                    $attendace = $d->select("attendance_master", "user_id='$user_id' AND DATE_FORMAT(attendance_date_start,'%m-%Y')='$curent_month_year'"); 
                        while ($attendace_data = mysqli_fetch_array($attendace)) {
                            
                            array_push($attendaceArrayData,$attendace_data['attendance_id']);
                        }

                    if(!empty($attendaceArrayData))
                    {
                        
                        $net_salary = $user_expanse_amount_val+(COUNT($attendaceArrayData)*$perday_salary);
                    }
                    else
                    {
                        $net_salary = 0;
                    }
                       
                    

                  $final_arr =  array('basic' =>  $user_salary_data['basic_salary'],
                                        'HRA' => $user_salary_data['hra'],
                                        'Medical' => $user_salary_data['medical'],
                                        'conveyance' => $user_salary_data['conveyance'],
                                        'Others' => $user_salary_data['others_deduction'],
                                        'PF' => $user_salary_data['pf'],
                                        'esic' => $user_salary_data['esic'],
                                        'professional_tax' => $user_salary_data['professional_tex'],
                                        'totalworkingdays' => $totalUserworkingdays,
                                        'month_wokring_days' => $totalworkingdays,
                                        'leaves' => $total_leave_count,
                                        'holiday' => count($holiday_data),
                                        'net_salary'=>$net_salary,
                                        'pf_amount'=>$basic,
                                        'user_expanse' => $user_expanse_amount_val,
                                        'user_full_name' =>$data['user_full_name']
                    );
               
                    $response["salary"] = $final_arr;
                    $response["message"] = "Salary Data";
                    $response["status"] = "200";
                    echo json_encode($response);
                }
                else
                { $response["message"] = "salary Data";
                    $response["salary"] = "";
                    $response["status"] = "201";
                    echo json_encode($response);

                }
               
            } else {
                $response["message"] = "salary Data";
                $response["salary"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }


    /////////////getAssignAndRemainingLeave
    if ($_POST['action'] == "getAssignAndRemainingLeave") {
        if ($user_id != '' && $leave_type_id != '' && $year != '') {
            $assignLeave = $d->selectRow('leave_assign_master.user_total_leave,leave_type_master.leave_type_name', "leave_assign_master,leave_type_master", "leave_type_master.leave_type_id=leave_assign_master.leave_type_id AND leave_assign_master.user_id='$user_id' AND leave_assign_master.leave_type_id='$leave_type_id' AND leave_assign_master.assign_leave_year='$year' AND leave_assign_master.leave_assign_active_status=0");
            $assignLeaveData = mysqli_fetch_assoc($assignLeave);
            /* $approvedLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_status=1 AND paid_unpaid=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
            $approvedLeaveData = mysqli_fetch_assoc($approvedLeave); */
            $approvedFullDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
            $approvedFullDayLeaveData = mysqli_fetch_assoc($approvedFullDayLeave);
            $approvedHalfDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
            $approvedHalfDayLeaveData = mysqli_fetch_assoc($approvedHalfDayLeave);
            $noOfHalfDayLeave = $approvedHalfDayLeaveData['approved_leave']/2;
            $payOutLeave = $d->selectRow('SUM(no_of_payout_leaves) AS no_of_payout_leaves', "leave_payout_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_payout_year = '$year'");
			$payOutLeaveData = mysqli_fetch_assoc($payOutLeave);
            $data = array();
            $data['user_total_leave'] = $assignLeaveData['user_total_leave'];
            $data['leave_type_name'] = $assignLeaveData['leave_type_name'];
            $data['approved_leave'] = $approvedFullDayLeaveData['approved_leave'] + $noOfHalfDayLeave;
            $data['remaining_leave'] = $assignLeaveData['user_total_leave'] - ($approvedFullDayLeaveData['approved_leave'] + $noOfHalfDayLeave) - $payOutLeaveData['no_of_payout_leaves'];
            if ($data) {
                $response["leave"] = $data;
                $response["message"] = "Leave Data";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "leave Data";
                $response["leave"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }

/////////
if ($_POST['action'] == "getSiteData") {
    if($site_id != '')
    {
        $q = $d->selectRow('*',"site_master"," site_master.site_id ='$site_id' ");
        $data = mysqli_fetch_assoc($q);  
        $data = array_map("html_entity_decode", $data);
        if ($data) {   
            $q2 = $d->selectRow('site_manager_master.*,users_master.user_id,users_master.user_full_name',"site_manager_master,users_master"," site_manager_master.site_id ='$site_id' AND users_master.user_id = site_manager_master.site_manager_id");
            $finance = array();
            $site_manager = array();
            $procurement = array();
            while ($data2 = mysqli_fetch_assoc($q2)) {
                    if($data2){
                    if($data2['manager_type']==0){
                        array_push($site_manager,$data2);
                    }
                    if($data2['manager_type']==1){
                        array_push($procurement,$data2);
                    }
                    if($data2['manager_type']==2){
                        array_push($finance,$data2);
                    }
                }
            }
            $data['site_created_date']  =  date("d M Y h:i A", strtotime($data['site_created_date']));
            $response["site"] = $data;
            $response["procurement"] = $procurement;
            $response["finance"] = $finance;
            $response["site_manager"] = $site_manager;
            $response["message"] = "site";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "site";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "400";
        echo json_encode($response);
    }
} 

///////getUnitMeasurementData
if ($_POST['action'] == "getUnitMeasurementData") {
    if( $unit_measurement_id != '')
    {
        $q = $d->selectRow('*',"unit_measurement_master","unit_measurement_id='$unit_measurement_id '");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {   
            $data['unit_measurement_created_date']  =  date("d M Y h:i A", strtotime($data['unit_measurement_created_date']));
            $data = array_map("html_entity_decode", $data);
            $response["unit"] = $data;
            $response["message"] = "unit";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "unit";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

////////////getCategorytData
if ($_POST['action'] == "getCategorytData") {
    if( $product_category_id != '')
    {
        $q = $d->selectRow('*',"product_category_master","product_category_id='$product_category_id '");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {  
            $data['product_category_created_date']  =  date("d M Y h:i A", strtotime($data['product_category_created_date']));
  
            $response["category"] = $data;
            $response["message"] = "category";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "category";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

///////////getSubCategoryData
if ($_POST['action'] == "getSubCategoryData") {
    if( $product_sub_category_id != '')
    {
        $q = $d->selectRow('*',"product_sub_category_master,product_category_master","product_sub_category_master.product_category_id = product_category_master.product_category_id AND product_sub_category_master.product_sub_category_id ='$product_sub_category_id'");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {    
            $data['product_sub_category_created_date']  =  date("d M Y h:i A", strtotime($data['product_sub_category_created_date']));
            
            $response["subCategory"] = $data;
            $response["message"] = "subCategory";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "subCategory";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 


if ($_POST['action'] == "addUpdatedIuserGenaratedSalary") {
    if( $user_id01 != '')
    {
        $q = $d->selectRow('*',"user_generated_salary","user_id='$user_id01' AND month='$month' AND year='$year'");
        $data = mysqli_fetch_assoc($q); 
        $user_salary=array(
            'society_id'=>$society_id,
            'user_id'=>$user_id01,
            'basic_amount'=>$basic_salary,
            'pf_amount'=>$pf_amount,
            'professional_tex'=>$professional_tax,
            'conveyance'=>$conveyance,
            'hra'=>$hra,
            'esic'=>$esic,
            'medical'=>$medical,
            'others_deduction'=>$others_deduction,
            'expence_amount'=>$user_expanse,
            'net_salary'=>$net_salary,
            'month'=>$month,
            'year'=>$year,
            'month_working_days'=>$totalworkingdays,
            'user_working_days'=>$totalworkingdays,
            'user_leave_days'=>$leaves,
             'holidays'=>$holiday,
            'created_date'=>$created_date,
        );                
        if ($data) {  
           

            $q = $d->update("user_generated_salary", $user_salary, "user_generated_salary_id ='$data[user_generated_salary_id]'");
            $_SESSION['msg'] = "user Genarated Salary Successfully Updated";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "user Genarated Salary Successfully Updated");  
            $response["unit"] = $data;
            $response["message"] = "user salary";
            $response["status"] = "200";
            echo json_encode($response);
        } else {  
            $q = $d->insert("user_generated_salary", $user_salary);
            $_SESSION['msg'] = "user Genarated Salary Added Successfully";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "user Genarated Salary Added Successfully");
  
            $response["message"] = "user salary";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "400";
        echo json_encode($response);
    }
}

////////////getCategorytData
if ($_POST['action'] == "getSubCategory") {
    if( $product_category_id != '')
    {
        $mainData = array();
        $products = array();
        $q = $d->select("product_sub_category_master", "product_category_id='$product_category_id' AND product_sub_category_delete=0 AND product_sub_category_status=0");
        while ($data = mysqli_fetch_assoc($q)) {
           
            $q2 = $d->select("product_master", "product_category_id = '$product_category_id'");
            while ($data2 = mysqli_fetch_assoc($q2)) {
                array_push($products, $data2);
            }
            
            array_push($mainData, $data);
        }             
        if ($mainData) {    
            $response["sub_category"] = $mainData;
            $response["products"] = $products;
            $response["message"] = "Sub Category";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Sub Category";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


////////////getCategorytData
if ($_POST['action'] == "getProductByCategoryAndSubCategoryId") {
    if($product_sub_category_id != '')
    {
        $mainData = array();
        $q = $d->select("product_master", "product_sub_category_id = '$product_sub_category_id'");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }             
        if ($mainData) {    
            $response["product"] = $mainData;
            $response["message"] = "Product Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Product Data";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getServiceProviderDetails") {
    if( $service_provider_bank_id != '')
    {
        $mainData = array();
        $q = $d->selectRow('*',"service_provider_bank_detail", "service_provider_bank_id='$service_provider_bank_id'");
       $data = mysqli_fetch_assoc($q);
                    
        if ($data) {    
           $response["bank_details"] = $data;
            $response["message"] = "Bank Details";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Bank Details";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getProduct") {
    if( $product_id != '')
    {
        $mainData = array();
        $q = $d->selectRow('*',"product_master,product_category_master,product_sub_category_master,unit_measurement_master", "product_master.product_category_id=product_category_master.product_category_id AND product_master.product_sub_category_id=product_sub_category_master.product_sub_category_id AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id AND product_master.product_id='$product_id'");
        $data = mysqli_fetch_assoc($q);         
        if ($data) {    
            $response["product"] = $data;
            $response["message"] = "Product Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Product Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getProductByIds") {
    $mainData = array();
  //  print_r($_POST);die;
    if(COUNT($_POST['product_ids'])>0)
    { 
       foreach($_POST['product_ids'] as $key => $val)
       {
        if($val !="")
        {
            $q = $d->selectRow('*',"product_master,product_category_master,product_sub_category_master,unit_measurement_master", "product_master.product_category_id=product_category_master.product_category_id AND product_master.product_sub_category_id=product_sub_category_master.product_sub_category_id AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id AND product_master.product_id='$key'");
            $data = mysqli_fetch_assoc($q);  
            $data['quantity'] =  $val;  
             array_push($mainData,$data);
        }
       
       }
        if ($mainData) {    
            $response["product"] = $mainData;
            $response["message"] = "Product Detail";
            $response["status"] = "200";
            echo json_encode($response);
            } else {    
                $response["message"] = "Product Detail";
                $response["status"] = "201";
                echo json_encode($response);
            }
        
        
    }
    else {    
        $response["message"] = "";
        $response["status"] = "400";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getXeroxPaperSizeData") {
    if( $xerox_paper_size_id != '')
    {
        $q = $d->selectRow('*',"xerox_paper_size","xerox_paper_size_id='$xerox_paper_size_id'");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {    
            $response["xerox"] = $data;
            $response["message"] = "Paper Size Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Paper Size Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getXeroxCategoryData") {
    if( $xerox_type_id != '')
    {
        $q = $d->selectRow('*',"xerox_type_category","xerox_type_id='$xerox_type_id'");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {    
            $response["category"] = $data;
            $response["message"] = "Xerox Category Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Xerox Category Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getXeroxDocData") {
    if( $xerox_doc_id != '')
    {
        $q = $d->selectRow('*',"xerox_doc_master,users_master,xerox_type_category,xerox_paper_size","xerox_doc_master.user_id = users_master.user_id AND xerox_doc_master.xerox_type_id = xerox_type_category.xerox_type_id AND xerox_doc_master.xerox_paper_size_id = xerox_paper_size.xerox_paper_size_id AND xerox_doc_master.xerox_doc_id=".$xerox_doc_id."");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {    
            $response["doc"] = $data;
            $response["message"] = "Xerox Document Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Xerox Document Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

/* Dollop Infotech Change Date 15-Nov-2021  */
if ($_POST['action'] == "getVendorCategoryData") {
    if( $vendor_id != '')
    {
        $q = $d->selectRow('*',"vendor_master,vendor_category_master","vendor_master.vendor_category_id = vendor_category_master.vendor_category_id AND vendor_master.vendor_id='$vendor_id'");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {    
            $response["vendor"] = $data;
            $response["message"] = "Vendor Product Category Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Vendor Product Category Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
/* Dollop Infotech Change Date 15-Nov-2021  */

/* Dollop Infotech date 22-oct-2021 -   */

/* Dollop Infotech Date 15-Nov-2021 -  */
if ($_POST['action'] == "getVendorProductCategoryData") {
    if( $vendor_product_category_id != '')
    {
        $q = $d->selectRow('*',"vendor_product_category_master,vendor_master,vendor_category_master","vendor_product_category_master.vendor_id = vendor_master.vendor_id AND vendor_product_category_master.vendor_category_id = vendor_category_master.vendor_category_id AND vendor_product_category_master.vendor_product_category_id='$vendor_product_category_id'");
        $data = mysqli_fetch_assoc($q); 
        $data = array_map("html_entity_decode", $data);                
        if ($data) {    
            $response["product"] = $data;
            $response["message"] = "Vendor Product Category Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Vendor Product Category Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


if ($_POST['action'] == "updatePrchaseProductById") {
    if($purchase_product_id != '')
    {
        $user_purchase_product = array();
        
        if($product_id !="")
        {
            $user_purchase_product['product_id'] = $product_id;
        }
        if($quantity !="")
        {
            $user_purchase_product['quantity'] = $quantity;
        }
       
        $q = $d->update("purchase_product_master", $user_purchase_product, "puchase_product_id ='$purchase_product_id'");
        $_SESSION['msg'] = "purchase product Successfully Updated";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "purchase product Successfully Updated ");  
       if($q)
       {
        $response["product_master"] = $q;
        $response["message"] = "product_master";
        $response["status"] = "200";
        echo json_encode($response);
       }
       else
       {
        $response["message"] = "Failed";
        $response["status"] = "400";
        echo json_encode($response);
       }
       
    }
    else {    
        $response["message"] = "Id Required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getPurhaseProductById") {
    if($purchase_id != '')
    {
        $q = $d->selectRow('*',"purchase_product_master,product_master,product_category_master,product_sub_category_master","purchase_product_master.puchase_product_id = $purchase_id AND product_master.product_id = purchase_product_master.product_id AND product_sub_category_master.product_sub_category_id = product_master.product_sub_category_id AND product_category_master.product_category_id = product_master.product_category_id");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {    
            $response["PurchaseProduct"] = $data;
            $response["message"] = "Purchase Product  Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Purchase Product  Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getCanteenAndStationaryProductData") {
    if( $vendor_product_id != '')
    {
        $q = $d->selectRow('*',"vendor_product_master","vendor_product_id='$vendor_product_id'");
        $data = mysqli_fetch_assoc($q);     
        $data = array_map("html_entity_decode", $data);            
        if ($data) {    
            $response["product"] = $data;
            $response["message"] = "Vendor Product Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Vendor Product Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getBlockById") {
    if( $block_id != '')
    {
        $q = $d->selectRow('*',"block_master","block_id='$block_id'");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {    
            $response["block"] = $data;
            $response["message"] = "block_id Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "block_id Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "deletepurchaseProduct") {
    $q = $d->delete("purchase_product_master", "puchase_product_id='$puchase_product_id'");
    if ($q > 0) {
     
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Purchase Product Deleted");
      //  $_SESSION['msg'] = "Purchase Product Deleted.";
        $response["product"] = $data;
        $response["message"] = "Purchase Product Deleted";
        $response["status"] = "200";
        echo json_encode($response);
    } else {
        $response["product"] = $data;
        $response["message"] = "Purchase Product Deleted Failed";
        $response["status"] = "200";
        echo json_encode($response);
      
    }
}
if ($_POST['action'] == "getUserByFloorForGeo") {
    $mainData = array();
    if ($floor_id != '') {
        $q = $d->select("users_master", "floor_id='$floor_id' AND user_geofence_latitude ='' AND user_geofence_longitude ='' AND delete_status=0 AND active_status=0");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }
        if ($mainData) {
            $response["users"] = $mainData;
            $response["message"] = "users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}



/* Dollop Infotech Date 15-Nov-2021 - */


/* Dollop Infotech Date 17-Nov-2021 - */

if ($_POST['action'] == "getDeleteEmployeedata") {
    if( $user_id != '')
    {
        $q = $d->selectRow('*',"users_master","user_id='$user_id'");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {    
            $response["employee"] = $data;
            $response["message"] = " Employee Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = " Employee Detail ";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getProductVendorDetail") {
    if( $vendor_id != '')
    {
        $q = $d->selectRow('*',"vendor_master","vendor_id='$vendor_id'");
        $data = mysqli_fetch_assoc($q);                 
        if ($data) {    
            $response["vendor"] = $data;
            $response["message"] = "Vendor Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Vendor Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

////////////getCategorytData
if ($_POST['action'] == "getLikedIdeaBoxUsers") {
    if($idea_id != '')
    {
        $mainData = array();
        $q = $d->select("idea_like_master, users_master", "idea_like_master.user_id=users_master.user_id AND idea_like_master.idea_id='$idea_id'");
        while ($data = mysqli_fetch_assoc($q)) {
            $userData['user_full_name'] = $data['user_full_name'];
            $userData['user_profile_pic'] = $data['user_profile_pic'];
            $userData['idea_like_id'] = $data['idea_like_id'];
            $userData['idea_id'] = $data['idea_id'];
            $userData['user_id'] = $data['user_id'];
            array_push($mainData, $userData);
        }             
        if ($mainData) {    
            $response["like_users"] = $mainData;
            $response["message"] = "Liked Users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Liked Users";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getCommentIdeaBoxUsers") {
    if($idea_id != '')
    {
        $mainData = array();
        $q = $d->select("idea_comment_master, users_master", "idea_comment_master.user_id=users_master.user_id AND idea_comment_master.idea_id='$idea_id' AND idea_comment_master.idea_parent_comment_id=0");
        while ($data = mysqli_fetch_assoc($q)) {
            $commentData['user_full_name'] = $data['user_full_name'];
            $commentData['user_profile_pic'] = $data['user_profile_pic'];
            $commentData['idea_comment'] = $data['idea_comment'];
            $commentData['idea_comment_id'] = $data['idea_comment_id'];
            $commentData['idea_id'] = $data['idea_id'];
            $commentData['user_id'] = $data['user_id'];
            $subCommentData2 = array();
            $subCommentData = array();
            $q1 = $d->select("idea_comment_master, users_master", "idea_comment_master.user_id=users_master.user_id AND  idea_comment_master.idea_parent_comment_id='$data[idea_comment_id]' ");
            while ($subData = mysqli_fetch_assoc($q1)) {
                $subCommentData2['user_full_name'] = $subData['user_full_name'];
                $subCommentData2['user_profile_pic'] = $subData['user_profile_pic'];
                $subCommentData2['idea_comment'] = $subData['idea_comment'];
                $subCommentData2['idea_comment_id'] = $subData['idea_comment_id'];
                $subCommentData2['idea_id'] = $subData['idea_id'];
                $subCommentData2['user_id'] = $subData['user_id'];
               array_push($subCommentData,$subCommentData2);
            }
            $commentData['sub_comment'] = $subCommentData;
            array_push($mainData, $commentData);
        }             
        if ($mainData) {    
            $response["like_users"] = $mainData;
            $response["message"] = "Liked Users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Liked Users";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


/* Dollop Infotech Date 17-Nov-2021 - */


/*  Dollop infotech 25-nov-2021  */
if ($_POST['action'] == "getShiftByFloorId") {
    if($floor_id != '')
    {
        $mainData = array();
        $q = $d->select("shift_timing_master", "is_deleted=0");
        $week_days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

        while ($data = mysqli_fetch_assoc($q)) {
            $arData = array();
            for($i=0; $i<count($week_days); $i++){
            if($data['week_off_days'] !="")
            {
                $week_off_days = explode(',',$data['week_off_days']);
                if(in_array($i, $week_off_days)){
                array_push($arData,$week_days[$i]);
                } 
            }
            }
            if(!empty($arData)){
            $showOff = implode(',',$arData);
            }
            else
            {
            $showOff = "";
            }
            $data['shift_time_view']='S'.$data['shift_time_id'];
            $data['show_off_name']=$showOff;
            array_push($mainData, $data);
        }             
        if ($mainData) {    
            $response["shift"] = $mainData;
            $response["message"] = "Shift";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Shift";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "updateUserById") {
    if($user_id != '' && $shift_time_id !="")
    {
        
        $user_shift =array(
            'shift_time_id'=>$shift_time_id
        );
        $q = $d->update("users_master", $user_shift, "user_id ='$user_id'");
        if($_POST['apply_from'] !=""){
            if($_POST['apply_from']==1){
               $month = date('m');
               $CrmonthDate = date('Y-m-d');
               $sDate = date('Y-m-01');
               $q = $d->update("attendance_master", $user_shift, "attendance_date_start  BETWEEN '$sDate' AND '$CrmonthDate'");
            }
        }
        $_SESSION['msg'] = "user shift  Successfully Updated";
        $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Employee Shift Updated");  
        if($q)
       {
        $response["user"] = $q;
        $response["message"] = "user update";
        $response["status"] = "200";
        echo json_encode($response);
       }
       else
       {
        $response["message"] = "Failed";
        $response["status"] = "400";
        echo json_encode($response);
       }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
/* Dollop infotech 25-nov-2021  */

if ($_POST['action'] == "updateUsersShiftMulti")
{
    if($user_id != '' && $shift_time_id !="")
    {
        $today_date = date("Y-m-d");
        foreach($user_id AS $key => $value)
        {
            $user_shift =array(
                'shift_time_id' => $shift_time_id
            );
            $q = $d->update("users_master", $user_shift, "user_id = '$value'");
            if($q)
            {
                $q2 = $d->update("attendance_master", $user_shift, "attendance_date_start = '$today_date' AND attendance_date_end = '0000-00-00' AND punch_out_time = '00:00:00' AND user_id = '$value'");
            }
            $d->insert_log("","$society_id","$admin_id","$admin_name","User's shift timing successfully updated");
        }
        if($q)
        {
            $response["message"] = "Users shift updated";
            $response["status"] = "200";
            echo json_encode($response);
        }
        else
        {
            $response["message"] = "Failed";
            $response["status"] = "400";
            echo json_encode($response);
        }
    }
    else
    {
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}



/* Dollop  Infotech 03-Nov-2021 */
if ($_POST['action'] == "getFloorByBlockId") {
    if ($block_id != '') {
        $mainData = array();
        $q = $d->select("floors_master,block_master", "block_master.block_id=floors_master.block_id AND floors_master.block_id='$block_id' $blockAppendQuery $blockAppendQueryFloor");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }  
        if ($mainData) {
            $response["floor"] = $mainData;
            $response["message"] = "Floor Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Floor Data";
            $response["floor"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


if ($_POST['action'] == "updateAttendanceById") {

   
    if ($attendance_id != '' && $punch_out_time!='') {
        
        $m->set_data('punch_out_time',$punch_out_time);
        $m->set_data('punch_out_date',$punch_out_date);
        $attendance_date = $punch_out_date;
        $attendance_time = $punch_out_time;



        $sq=$d->selectRow("*","attendance_master","attendance_id ='$attendance_id'");
        $attData=mysqli_fetch_array($sq);
        extract($attData);
        $shiftTimeId = $shift_time_id;
        $attendance_user_id = $attData['user_id'];
        $datePunchIN = $attendance_date_start ." ".$punch_in_time;
        
        $qsm = $d->selectRow("user_token,device,unit_id,user_full_name","users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
        $data_notification = mysqli_fetch_array($qsm);
        $user_token = $data_notification['user_token'];
        $device = $data_notification['device'];
        $unit_id = $data_notification['unit_id'];
        
        $tabPosition = "0"; 

        if ($attendance_date != '0000-00-00' && $attendance_date != 'null') {
            $day = date("w", strtotime($attendance_date));
        }
        $shiftQry = $d->selectRow("shift_day_master.*, shift_timing_master.is_multiple_punch_in, shift_timing_master.maximum_in_out, shift_timing_master.max_punch_out_time, shift_timing_master.max_shift_hour","shift_day_master,shift_timing_master","shift_day_master.shift_time_id=$shiftTimeId AND shift_day_master.shift_day='$day' AND shift_timing_master.shift_time_id=shift_day_master.shift_time_id");
        $shiftData = mysqli_fetch_array($shiftQry);
        $perdayHours = $shiftData['per_day_hour'];
        $early_out_time = $shiftData['early_out_time'];
        $half_day_time_start = $shiftData['half_day_time_start'];
        $shift_end_time = $shiftData['shift_end_time'];
        $halfday_before_time = $shiftData['halfday_before_time'];
        $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
        $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
        $maximum_in_out = $shiftData['maximum_in_out'];
        $is_multiple_punch_in = $shiftData['is_multiple_punch_in'];


        $multiple_punch_in_out_data = $attData['multiple_punch_in_out_data'];
        $is_extra_day = $attData['is_extra_day'];
        $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

        $finalTotalMinute = 0;

        $dataArray = array();

        if (isset($multiplePunchDataArray) && $is_multiple_punch_in == 1 && count($multiplePunchDataArray) > 0) {

            for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                if($multiplePunchDataArray[$i]["punch_out_date"] == '' || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){
                    $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$attendance_date, $multiplePunchDataArray[$i]["punch_in_time"],$attendance_time);
                    $timeHr = explode(':', $totalHours.":00");
                    $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                    $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                    $oldAry = array(
                        "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"],
                        "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"],
                        "punch_out_date" => $attendance_date,
                        "punch_out_time" =>$attendance_time,
                        "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"],
                        "punch_out_image" => "",
                        "working_hour" => $totalHours.'',
                        "working_hour_minute" => $totalMinutes.'',
                    );

                    array_push($dataArray,$oldAry);
                }else{
                    $oldAry = array(
                        "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"],
                        "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"],
                        "punch_out_date" => $multiplePunchDataArray[$i]["punch_out_date"],
                        "punch_out_time" =>$multiplePunchDataArray[$i]["punch_out_time"],
                        "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"],
                        "punch_out_image" => $multiplePunchDataArray[$i]["punch_out_image"],
                        "working_hour" => $multiplePunchDataArray[$i]["working_hour"],
                        "working_hour_minute" => $multiplePunchDataArray[$i]["working_hour_minute"],
                    );
                    $finalTotalMinute = $finalTotalMinute + $multiplePunchDataArray[$i]["working_hour_minute"];
                    array_push($dataArray,$oldAry);
                }
            }  

            $multPunchDataJson = json_encode($dataArray).'';
        }else{
            if (isset($multiplePunchDataArray) &&  count($multiplePunchDataArray) > 0) {
                for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                    if($multiplePunchDataArray[$i]["punch_out_date"] == '' || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){
                        $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$attendance_date, $multiplePunchDataArray[$i]["punch_in_time"],$attendance_time);
                        $timeHr = explode(':', $totalHours.":00");
                        $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                        $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                        $oldAry = array(
                            "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"],
                            "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"],
                            "punch_out_date" => $attendance_date,
                            "punch_out_time" =>$attendance_time,
                            "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"],
                            "punch_out_image" => "",
                            "working_hour" => $totalHours,
                            "working_hour_minute" => $finalTotalMinute,
                        );
                        array_push($dataArray,$oldAry);
                    }
                }

                $multPunchDataJson = json_encode($dataArray).'';
            }
        }          
        $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type","leave_master","user_id = '$attendance_user_id' AND leave_start_date = '$attendance_date_start'");

        $hasLeave = false;

        if (mysqli_num_rows($qryLeave) > 0) {

            $hasLeave = true;

            $leaveData = mysqli_fetch_array($qryLeave);

            $leave_id = $leaveData['leave_id'];
            $leave_type_id = $leaveData['leave_type_id'];
            $paid_unpaid = $leaveData['paid_unpaid'];
            $leave_reason = $leaveData['leave_reason'];
            $leave_day_type = $leaveData['leave_day_type'];
            $half_day_session = $leaveData['half_day_session'];
        }

        $parts = explode(':', $perdayHours);
        $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
        $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

        $datePunchOUT = $attendance_date ." ".$attendance_time;
        $datePunchIN = $attendance_date_start ." ".$punch_in_time;

        if ($is_multiple_punch_in == 0) {
            $partEO = explode(':', $early_out_time);
            $min = ($partEO[0]*60) + ($partEO[1]) + ($partEO[2]/60);
            $defaultOutTime = $attendance_date." ".$shift_end_time;
            $strMin = "-".$min." minutes";
            $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

            if ($datePunchOUT >= $defaultOutTime) {
                $early_out = "0";
            }else{
                $early_out = "1";              
            }

            if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                $early_out = "0";
            }

            $totalHours = getTotalHoursWithNames($attendance_date_start,$attendance_date,$punch_in_time,$attendance_time);

            $totalHR = getTotalHours($attendance_date_start,$attendance_date,$punch_in_time,$attendance_time);


            $time = explode(':', $totalHR.":00");
            $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);
        }else{
            $total_minutes =  $finalTotalMinute;

            $hoursPM  = floor($total_minutes/60);
            $minutesPM = $total_minutes % 60;

            $totalHR = sprintf('%02d:%02d', $hoursPM, $minutesPM);
            $times[] = $totalHR;

            if ($hoursPM > 0 && $minutesPM) {
                $totalHoursNames = sprintf('%02d hr %02d min', $hoursPM, $minutesPM);
            }else if ($hoursPM > 0 && $minutesPM <= 0) {
                $totalHoursNames = sprintf('%02d hr', $hoursPM);
            }else if ($hoursPM <= 0 && $minutesPM > 0) {
                $totalHoursNames = sprintf('%02d min', $minutesPM);
            }else{
                $totalHoursNames = "No Data";
            }
        }

        $avgWorkingDays = $total_minutes/$perDayHourMinute;

        $avg_working_days = round($avgWorkingDays * 2) / 2;

        $extra_working_hours_minutes = 0;

        if ($total_minutes > $perDayHourMinute) {
            $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
            $hours  = floor($extra_working_hours_minutes/60);
            $minutes = $extra_working_hours_minutes % 60;

            $extra_working_hours = sprintf('%02d:%02d', $hours, $minutes);
        }

        $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

        $qryBreakData = mysqli_fetch_array($qryBreak);

        $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

        if ($total_minutes > $totalBreakinutes) {
            $productive_working_hours_minutes = $total_minutes - $totalBreakinutes;

            $hoursPM  = floor($productive_working_hours_minutes/60);
            $minutesPM = $productive_working_hours_minutes % 60;

            $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
        }else{
            $productive_working_hours_minutes = $total_minutes;

            $hoursPM  = floor($productive_working_hours_minutes/60);
            $minutesPM = $productive_working_hours_minutes % 60;

            $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
        }

        $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND gps_accuracy <= 100  AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT'");

        $query11Data=mysqli_fetch_array($query11);

        $total_travel_meter = "0";
        
        if ($query11Data['SUM(last_distance)'] > 0) {
            $total_travel_meter = $query11Data['SUM(last_distance)']."";
        }

        $m->set_data('total_working_hours',$totalHR);
        $m->set_data('extra_working_hours',$extra_working_hours);
        $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
        $m->set_data('avg_working_days',$avg_working_days);
        $m->set_data('productive_working_hours',$productive_working_hours);
        $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
        $m->set_data('total_working_minutes',$total_minutes);
        $m->set_data('total_travel_meter',$total_travel_meter);
        $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
        $m->set_data('early_out',$early_out);
        $m->set_data('attendance_status_change_by',$bms_admin_id);


        $attendance_status_change_by_type =1;


        $a1 = array(
            'punch_out_time'=>$m->get_data('punch_out_time'),
            'attendance_date_end'=>$m->get_data('punch_out_date'),
            'attendance_status'=>1,
            'early_out'=>$m->get_data('early_out'),
            'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
            'attendance_status_change_by_type'=>$attendance_status_change_by_type,
            'extra_working_hours'=>$m->get_data('extra_working_hours'),
            'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
            'avg_working_days'=>$m->get_data('avg_working_days'),
            'productive_working_hours'=>$m->get_data('productive_working_hours'),                
            'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),                
            'total_working_hours'=>$m->get_data('total_working_hours'),                
            'total_working_minutes'=>$m->get_data('total_working_minutes'),                    
            'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),                    
            'total_travel_meter'=>$m->get_data('total_travel_meter')
            
        );  
        $max_punch_out_time= $attendance_date." ".date('H:i:s', strtotime($shiftData['max_punch_out_time']));
        $max_shift_hour= date('H:i:s', strtotime($shiftData['max_shift_hour']));
        $time=explode(":",$max_shift_hour);
        $convertedTime = date('H:i:s',strtotime(+$time[0].' hour '.+$time[1].' minutes '.+$time[2].' seconds',strtotime($punch_in_time)));

        if($max_punch_out_time > $datePunchOUT || $convertedTime > $attendance_time){
            if(isset($attendance_id) && $attendance_id>0 )
            {
                
                    $_SESSION['msg']="Punch Out Time Updated Successfully ";
                    $q=$d->update("attendance_master",$a1,"attendance_id ='$attendance_id'");
                    $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Punch Out Time Updated ($attendance_id)");
            }  
            if ($q) {

                $leaveValue = false;
    
                if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                    $leaveValue = true;
                } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $attendance_time) {
                    $leaveValue = true;
                }
                $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);
            
    
                if ($total_minutes >= $minimum_hours_for_full_day_min) {
                    $noLeave = true;
                }else{
                    $noLeave = false;
                }
    
                $alreadyLeaveQryCheck = false; 
    
                if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {
    
    
                    $leave_total_days = 1;
    
                    // check half day and full day
    
                    if ($total_minutes < $maximum_halfday_hours_min) {
                        $avgCount = "0";
                        $leaveTypeName = "Full Day";
                        $leaveType = "0";
                        $is_leave = "1";
                        $extra_day_leave_type = "1";
                        $half_day_session = "0";                
                    }else{
                        $avgCount = "0.5";
                        $leaveType = "1";
                        $is_leave = "2";
                        $extra_day_leave_type = "2";
                        $leaveTypeName = "Half Day";
                    }
    
                    if ($is_extra_day == 1) {
                        $is_leave = "0";
                    }else{
                        $extra_day_leave_type = "0";
                    }
    
                    if ($hasLeave == true) {
    
                        // Change already applied leave
    
                        $title = "Auto Leave Applied - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed ($totalHours)";
                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;
    
                        $m->set_data('leave_type_id',$leave_type_id);
                        $m->set_data('paid_unpaid',$paid_unpaid);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('half_day_session',$half_day_session);
                        $m->set_data('auto_leave_reason',$titleMessage);
    
                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'half_day_session'=>$m->get_data('half_day_session'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                        );
    
                        $leaveQry = $d->update("leave_master",$a,"user_id = '$attendance_user_id' AND leave_id = '$leave_id'");
                        
    
                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }
    
                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                        
                    }else{
    
                        // Auto Leave
    
                        $title = "Auto Leave Applied"." - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed ($totalHR)";
                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;
    
                        $m->set_data('leave_type_id',"0");
                        $m->set_data('society_id',$society_id);
                        $m->set_data('paid_unpaid',"1");
                        $m->set_data('unit_id',$unit_id);
                        $m->set_data('user_id',$attendance_user_id);
                        $m->set_data('auto_leave_reason',$titleMessage);
                        $m->set_data('leave_start_date',$attendance_date_start);
                        $m->set_data('leave_end_date',$attendance_date_start);
                        $m->set_data('leave_total_days',$leave_total_days);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('leave_status',"1");
                        $m->set_data('leave_created_date',date('Y-m-d H:i:s'));
    
                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'society_id' =>$m->get_data('society_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                            'leave_start_date'=>$m->get_data('leave_start_date'),
                            'leave_end_date'=>$m->get_data('leave_end_date'),
                            'leave_total_days'=>$m->get_data('leave_total_days'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'leave_status'=>$m->get_data('leave_status'),
                            'leave_created_date'=>$m->get_data('leave_created_date'),
                        );
    
                        $leaveQry = $d->insert("leave_master",$a);
    
                        $alreadyLeaveQryCheck = true;
    
                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }
    
                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                    }
    
                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',$avgCount);
                    $m->set_data('is_leave',$is_leave);
                    $m->set_data('extra_day_leave_type',$extra_day_leave_type);
    
    
                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'is_leave'=>$m->get_data('is_leave'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
    
                    );
    
                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                    
                }else if($noLeave == true && $hasLeave == true){
                    // Remove leave if present and full time
    
                    $leaveQry = $d->delete("leave_master","user_id = '$attendance_user_id' AND leave_id = '$leave_id'");
    
                    $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");
    
                    $title = "Applied Leave Cancelled";
                    $description = "For Date : ".$attendance_date_start;
    
                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }
    
                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                }
    
                // Code - Maximum Late IN & Early OUT Limit Check -> Apply Leave
    
                $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND MONTH(attendance_date_start) = MONTH(NOW()) AND late_in = '1'");
    
                $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND MONTH(attendance_date_start) = MONTH(NOW()) AND early_out = '1'");
    
                $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;
    
                if ($hasLeave = false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($late_in == "1" || $early_out == "1")) {
    
                    $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";
    
                    $m->set_data('leave_type_id',"0");
                    $m->set_data('society_id',$society_id);
                    $m->set_data('paid_unpaid',"1");
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('user_id',$attendance_user_id);
                    $m->set_data('auto_leave_reason',$titleMessage);
                    $m->set_data('leave_start_date',$attendance_date_start);
                    $m->set_data('leave_end_date',$attendance_date);
                    $m->set_data('leave_total_days',"1");
                    $m->set_data('leave_day_type',"1");
                    $m->set_data('leave_status',"1");
                    $m->set_data('leave_created_date',date('Y-m-d H:i:s'));
    
                    $a = array(
                        'leave_type_id'=>$m->get_data('leave_type_id'),
                        'society_id' =>$m->get_data('society_id'),
                        'paid_unpaid' =>$m->get_data('paid_unpaid'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'), 
                        'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                        'leave_start_date'=>$m->get_data('leave_start_date'),
                        'leave_end_date'=>$m->get_data('leave_end_date'),
                        'leave_total_days'=>$m->get_data('leave_total_days'),
                        'leave_day_type'=>$m->get_data('leave_day_type'),
                        'leave_status'=>$m->get_data('leave_status'),
                        'leave_created_date'=>$m->get_data('leave_created_date'),
                    );
    
                    $leaveQry = $d->insert("leave_master",$a);
    
                    $title = "Auto Leave Applied - Half Day";
    
                    $description = "For Date : ".$attendance_date_start."\n".$titleMessage;
    
                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }
    
                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
    
                    if ($is_extra_day == 1) {
                        $is_leave = "0";
                    }else{
                        $extra_day_leave_type = "0";
                    }
    
                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',"0.5");
                    $m->set_data('is_leave',$is_leave);
                    $m->set_data('extra_day_leave_type',$extra_day_leave_type);
    
                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'is_leave'=>$m->get_data('is_leave'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                    );
    
                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                }
    
                $response["floor"] = $mainData;
                $response["message"] = "Attendance Update";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Attendance Update";
                $response["floor"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else{
            $_SESSION['msg1']="Punch out time exceeds maximum punch out time!";
            $response["message"] = "Punch out time exceeds maximum punch out time!";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "Pucnh Out Time required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


if ($_POST['action'] == "getZone") {
    if ($zone_id != '') {
        $q = $d->selectRow('*', "zone_master", "zone_id='$zone_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["zone"] = $data;
            $response["message"] = "Zone";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Zone";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getEmployeeLevel") {
    if ($level_id != '') {
        $q = $d->selectRow('*', "employee_level_master", "level_id='$level_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["level"] = $data;
            $response["message"] = "Employee Level";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Employee Level";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getSubDepartmentByFloorId") {
    if($floor_id != '')
    {
        $mainData = array();
        $q = $d->select("sub_department", "floor_id='$floor_id' AND sub_department_status=0");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }             
        if ($mainData) {    
            $response["sub_department"] = $mainData;
            $response["message"] = "Sub Department";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Sub Department";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getVendorProductCategoryByVendorId") {
    if($vendor_id != '')
    {
        $mainData = array();
        $q = $d->select("vendor_product_category_master", "vendor_id='$vendor_id' AND vendor_product_category_status = 0 AND is_delete=0");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }             
        if ($mainData) {    
            $response["product_category"] = $mainData;
            $response["message"] = "Product Category";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Product Category";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


if ($_POST['action'] == "getVendorProductByVendorId") {
    if($vendor_id != '')
    {
        $mainData = array();
        $q = $d->select("vendor_product_master", "vendor_id='$vendor_id' AND vendor_product_active_status=0 AND vendor_product_delete =0");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }             
        if ($mainData) {    
            $response["vendor_product"] = $mainData;
            $response["message"] = "Vendor Product";
            $response["status"] = "200";
            echo json_encode($response);
        } else {    
            $response["message"] = "Vendor Product";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {    
        $response["message"] = "";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "acceptUserPunchOutRequest") {
    if ($attendance_id != '') {
        $a1 = array(
            'punch_out_time'=>$punch_out_time,
            'attendance_date_end'=>$attendance_date_end,
             
          );  
           if(isset($attendance_id) && $attendance_id>0 )
           {
           $q=$d->update("attendance_master",$a1,"attendance_id ='$attendance_id'");
            $_SESSION['msg']="Attendance Update Successfully";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Attendance Update Successfully");
            $a2 = array('attendance_punch_out_missing_status'=>1);
            $q1=$d->update("attendance_punch_out_missing_request",$a2,"attendance_punch_out_missing_id ='$attendance_punch_out_missing_id'");
           }  
        if ($q) {
            $response["floor"] = $mainData;
            $response["message"] = "Attendance Update";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Attendance Update";
            $response["floor"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getWorkReport") {
    if ($work_report_id != '') {
       
        $q = $d->selectRow('*', "work_report_master,users_master,floors_master", "work_report_master.user_id=users_master.user_id AND work_report_master.floor_id=floors_master.floor_id AND work_report_master.work_report_id='$work_report_id'");
        $data = mysqli_fetch_assoc($q);
        $work_report = array();
        $work_report['user_full_name'] = $data['user_full_name'];
        $work_report['user_designation'] = $data['user_designation'];
        $work_report['floor_name'] = $data['floor_name'];
        $work_report['work_report_date'] = date("d M Y", strtotime($data['work_report_date']));
        $work_report['work_report'] = $data['work_report'];
        if ($data) {
            $response["work_report"] = $work_report;
            $response["message"] = "Work Report";
            $response["status"] = "200";

            echo json_encode($response);
        } else {
            $response["message"] = "Failure";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID Required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getPunchOutMissingRequest") {
    if ($attendance_punch_out_missing_id != '') {
       
        $q = $d->selectRow('*', "attendance_punch_out_missing_request,attendance_master,users_master", "attendance_punch_out_missing_request.attendance_id=attendance_master.attendance_id AND attendance_punch_out_missing_request.user_id=users_master.user_id AND attendance_punch_out_missing_request.attendance_punch_out_missing_id='$attendance_punch_out_missing_id'");
        $data = mysqli_fetch_assoc($q);
        if( $data){
            $missing_request = array();
            if($data['attendance_status_change_by_type']==1){
            /// Admin
            $q2 = $d->selectRow('*','bms_admin_master',"admin_id='$data[attendance_punch_out_missing_status_changed_by]'");
            $data3 = mysqli_fetch_assoc($q2);
            if( $data3 ){

                $missing_request['changed_by'] = $data3['admin_name'];
            }
            else
            {
                $missing_request['changed_by'] = "";
            }

            }
            else
            {
                $q2 = $d->selectRow('*','users_master',"user_id='$data[attendance_punch_out_missing_status_changed_by]'");
                $data4 = mysqli_fetch_assoc($q2);
                if( $data4 ){

                    $missing_request['changed_by'] = $data4['admin_name'];
                }
                else
                {
                    $missing_request['changed_by'] = "";
                }
    
                
            }
            
            $missing_request['user_full_name'] = $data['user_full_name'];
            $missing_request['punch_in_time'] = date("h:i A", strtotime($data['punch_in_time']));
            $missing_request['attendance_date_start'] = date("d M Y", strtotime($data['attendance_date_start']));
            $missing_request['attendance_time'] = date("h:i A", strtotime($data['attendance_time']));
            $missing_request['attendance_date'] = date("d M Y", strtotime($data['attendance_date'])); 
            $missing_request['punch_out_missing_reason'] = $data['punch_out_missing_reason'];

          
            $sDTime = $data['attendance_date_start']." ".$data['punch_in_time'];
            $eDTime = $data['attendance_date']." ".$data['attendance_time'];

            $pTime = date('Y-m-d h:i A',strtotime($sDTime));
            $eTime = date('Y-m-d h:i A',strtotime($eDTime));

            $date_a = new DateTime($pTime);
            $date_b = new DateTime($eTime);

            $interval = $date_a->diff($date_b);
           
            $days = $interval->format('%d')*24;
            $hours = $interval->format('%h');
            $hours = $hours+$days;
            $minutes = $interval->format('%i');
            $sec = $interval->format('%s');

            if ($hours > 0 && $minutes) {
                $totalHours = sprintf('%02d hr %02d min', $hours, $minutes);
            }else if ($hours > 0 && $minutes <= 0) {
                $totalHours = sprintf('%02d hr', $hours);
            }else if ($hours <= 0 && $minutes > 0) {
                $totalHours = sprintf('%02d min', $minutes);
            }else{
                $totalHours = "No Data";
            }


            
            $time = explode(':', $totalHours.":00");
            $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

            $missing_request['total_hours'] = $totalHours;
            $missing_request['total_minutes'] = $total_minutes.'';

            if ($data) {
                $response["missing_request"] = $missing_request;
                $response["message"] = "Missing Request";
                $response["status"] = "200";
    
                echo json_encode($response);
            } else {
                $response["message"] = "Failure";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }
        else
        {
            $response["message"] = "Failure";
            $response["status"] = "201";
            echo json_encode($response);

        }
    } else {
        $response["message"] = "ID Required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getUserLeaveById") {
    if ($leave_id != '') {
       
        $q = $d->selectRow('*', "leave_master", "leave_id='$leave_id'");
        $data = mysqli_fetch_assoc($q);
        
        if ($data) {
            $response["leave"] = $data;
            $response["message"] = "User Leave";
            $response["status"] = "200";

            echo json_encode($response);
        } else {
            $response["message"] = "Failure";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID Required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

  if($_POST['action']=="getEscalationById") {
   
    $q=$d->selectRow('escalation_master.*,DATE_FORMAT(escalation_master.escalation_created_date,"%d %b %Y %h:%i %p") AS escalation_created_date,DATE_FORMAT(escalation_master.escalation_replay_date,"%d %b %Y %h:%i %p") AS escalation_replay_date,reciver.user_full_name AS reciver_name,sender.user_full_name AS sender_name',"escalation_master LEFT JOIN users_master AS reciver ON reciver.user_id=escalation_master.escalation_to_id LEFT JOIN users_master AS sender ON sender.user_id=escalation_master.escalation_by_id","escalation_id='$escaltion_id'");
      if($q>0) {
       // echo 1;
        $data = mysqli_fetch_assoc($q);
        $response["message"] = "Success";
        $response["status"] = "200";
        $response["data"] = $data;
        echo json_encode($response);
      } else {
        echo 0;
        $_SESSION['msg1']="Something Wrong";
        $response["message"] = "ID Required";
        $response["status"] = "201";
        echo json_encode($response);
      }
  }
  if ($_POST['action'] == "getCompanyHome") {
    if ($company_home_id != '') {
        $q = $d->selectRow('*', "company_home_master", "company_home_id='$company_home_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["company_home"] = $data;
            $response["message"] = "CompanyHome";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "CompanyHome";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
  }

//////////////////17-01-2021///////////////////////////////////////////////
  if ($_POST['action'] == "getCompanyService") {
    if ($company_service_id != '') {
        $q = $d->selectRow('*', "company_service_master", "company_service_id='$company_service_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["company_service"] = $data;
            $response["message"] = "Company Service";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Company Service";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getCompanyServiceDetail") {
    if ($company_service_details_id != '') {
        $q = $d->selectRow('company_service_details_master.*, company_service_master.company_service_name', "company_service_details_master,company_service_master", "company_service_details_master.company_service_id=company_service_master.company_service_id AND company_service_details_master.company_service_details_id='$company_service_details_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["company_service_detail"] = $data;
            $response["message"] = "Company Service Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Company Service Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getEarningDeductionById") {
    if ($salary_earning_deduction_type_id != '') {
        $q = $d->selectRow('salary_earning_deduction_type_master.*', "salary_earning_deduction_type_master","salary_earning_deduction_id=$salary_earning_deduction_type_id");
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            $response["salary_earning_deduction_type_master"] = $data;
            $response["message"] = "salary_earning_deduction_type_master";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "salary_earning_deduction_type_master";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getDepartmentByFloorIdForSalaryValue") {
    if ($block_id != '') {
        $mainData = array();
        $mainArray = array();
        
        $q1 = $d->selectRow("floors_master.*,block_master.*,(SELECT COUNT(*) FROM salary_common_value_master WHERE salary_common_value_master.floor_id=floors_master.floor_id ) AS Total","floors_master,block_master", "block_master.block_id=floors_master.block_id AND floors_master.block_id='$block_id'");
        while ($data1 = mysqli_fetch_assoc($q1)) {
            //print_r($data1);
            if((int)$data1['Total']<=0){

                array_push($mainArray, $data1);
            }
        }  
        if ($mainArray) {
            $response["floor"] = $mainArray;
            $response["message"] = "Floor Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Floor Data";
            $response["floor"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getCompanyCurrentOpening") {
    if ($company_current_opening_id != '') {
        $q = $d->selectRow('*', "company_current_opening_master", "company_current_opening_id='$company_current_opening_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        
        if ($data) {
            $response["current_opening"] = $data;
            $response["message"] = "Company Current Opening";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Company Current Opening";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


if ($_POST['action'] == "getUserByFloorForSalary") {
    $mainData = array();
    if ($floor_id != '') {
        $q = $d->select("users_master", "floor_id='$floor_id' AND delete_status=0 AND active_status=0 ");
        while ($data = mysqli_fetch_assoc($q)) {
            $checkSalary = $d->selectRow('*', "salary", "is_delete=0 AND is_active=0 AND user_id = '$data[user_id]'");
            $checkSalaryData = mysqli_fetch_assoc($checkSalary);
            if(!isset($checkSalaryData)) {
               array_push($mainData, $data);
           }
        }
        if ($mainData) {
            $response["users"] = $mainData;
            $response["message"] = "users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getSalaryCommonValueById") {
    $mainData = array();
    if ($salary_common_value_id != '') {
        $checkSalaryCommonDeductionData=array();
            $checkSalary = $d->selectRow('*', "salary_common_value_master 
            LEFT JOIN block_master On block_master.block_id = salary_common_value_master.block_id 
            LEFT JOIN salary_earning_deduction_type_master On salary_earning_deduction_type_master.salary_earning_deduction_id = salary_common_value_master.salary_earning_deduction_id 
            LEFT JOIN floors_master On floors_master.floor_id = salary_common_value_master.floor_id", 
            "salary_common_value_master.salary_common_value_id = '$salary_common_value_id'");
            $checkSalaryData = mysqli_fetch_assoc($checkSalary);
        
        if ($checkSalaryData) {
            $checkSalaryData['salary_common_created_date'] = date("d M Y", strtotime($checkSalaryData['salary_common_created_date']));
           $earnName = array();
          
            if($checkSalaryData['salary_common_value_earn_deduction'] !=""){
                
               $explodIds = explode(',',$checkSalaryData['salary_common_value_earn_deduction']);
              
               for($k=0;$k<COUNT($explodIds);$k++){
                  
                $checkSalaryCommonDeduction = $d->selectRow( "*", "salary_earning_deduction_type_master","salary_earning_deduction_id = $explodIds[$k] ");
                $checkSalaryCommonDeductionData = mysqli_fetch_assoc($checkSalaryCommonDeduction);

                array_push($earnName,$checkSalaryCommonDeductionData['earning_deduction_name']);
            }
           }
            
            if(!empty($earnName))
            {
                
                $checkSalaryData['ern_deduct_name']=implode(',',$earnName);
            }
            else
            {
                $checkSalaryData['ern_deduct_name']='';
            }
            $response["salary"] = $checkSalaryData;
            $response["message"] = "users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "userLeaveHistory") {
    if ($user_id != '' && $leave_type_id != '' && $year) {
        $mainArray = array();
        
        $q1 = $d->select("leave_master", "user_id=$user_id AND leave_type_id='$leave_type_id' AND paid_unpaid=0 AND leave_status=1 AND DATE_FORMAT(leave_start_date,'%Y') = '$year'");
        while ($data = mysqli_fetch_assoc($q1)) {
            $mainData['paid_unpaid'] = $data['paid_unpaid']==0?'Paid':'Unpaid';
            $mainData['leave_day_type'] = $data['leave_day_type']==0?'Full Day Leave':'Half Day Leave';
            $mainData['leave_date'] = date("d M Y", strtotime($data['leave_start_date']));
            $mainData['leave_day'] = date("l", strtotime($data['leave_start_date']));
            array_push($mainArray, $mainData);
        }  
        if ($mainArray) {
            $response["leave"] = $mainArray;
            $response["message"] = "Leave History";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Leave History";
            $response["floor"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getSalarySlipById") {
    if ($salary_slip_id != '') {
        $q = $d->selectRow('salary_slip_master.*,users_master.user_id,users_master.user_full_name,chekced.admin_name AS checked_user_by,gen.admin_name AS generated_user_by,published.admin_name AS published_user_by', "salary_slip_master 
        LEFT JOIN bms_admin_master AS chekced ON chekced.admin_id = salary_slip_master.checked_by 
        LEFT JOIN bms_admin_master AS gen ON gen.admin_id = salary_slip_master.prepared_by
        LEFT JOIN bms_admin_master AS published ON published.admin_id = salary_slip_master.authorised_by ,users_master ", "salary_slip_master.salary_slip_id='$salary_slip_id' AND salary_slip_master.user_id = users_master.user_id");
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            $mainData=array();
            $contriAr=array();
            $q2 = $d->select('salary_slip_sub_master',"salary_slip_sub_master.salary_slip_id='$salary_slip_id'");
            while ($data2 = mysqli_fetch_assoc($q2)) {
                if((float)$data2['employer_contribution_amount']>0){
                    array_push($contriAr, $data2);
                }
                array_push($mainData, $data2);
            }
            
            $data['earn_deduct']=$mainData;
            $data['contribution']=$contriAr;
            $data['month'] = date('M-Y',strtotime($data['salary_start_date']));
            $response["salary"] = $data;
            $response["message"] = "salary";
            $response["status"] = "200";
            echo json_encode($response);

        } else {
            $response["message"] = "salary";
            $response["status"] = "201";
            echo json_encode($response);

        }
    } else {
        $response["message"] = "salary Id";
        $response["status"] = "201";
        echo json_encode($response);

    }

}
if ($_POST['action'] == "ChangeSalarySlipStatus") {
    if ($salary_slip_id != '') {
        $salarySlipSubValueData = array('salary_slip_status'=>$status);
        if($status==1)
        {
            $salarySlipSubValueData['checked_by'] = $bms_id;
        }
        if($status==2)
        {
            $salarySlipSubValueData['authorised_by']= $bms_id;

        }
        $q2 = $d->update("salary_slip_master",$salarySlipSubValueData, "salary_slip_id='$salary_slip_id'" );

        if ($q2) {
            $_SESSION['msg'] = "Salary Slip Status Successfully Updated";
            $d->insert_log("", "$society_id", "$_COOKIE[bms_admin_id]", "$created_by", "Salary Slip Status Successfully Updated");  
            $response["message"] = "salary";
            $response["status"] = "200";
            echo json_encode($response);
            if($is_notify==1)
            {
                    $a1 = array('share_with_user' => 1);
                     $q = $d->update('salary_slip_master', $a1, "salary_slip_id=$salary_slip_id");
    
                $q3 = $d->selectRow('*',"salary_slip_master","salary_slip_id='$salary_slip_id'");
                $data3 = mysqli_fetch_assoc($q3);
               
                $q4 = $d->selectRow('user_token,device',"users_master","user_id='$data3[user_id]'");
                    $data2 = mysqli_fetch_assoc($q4);
                   
                    $title = "Salary Slip Share With User ";
                    $description = "By Admin $created_by";
                    $menuClick = "";
                    $image = "";
                    $activity = "Salary_Slip";
                    $user_token = $data2['user_token'];
                    $device = $data2['device'];
                    if ($device=='android') {
                        $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                    } else {
                        $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                    }
                   // print_r($nResident);die;
            }
            
           

        } else {
            $response["message"] = "salary";
            $response["status"] = "201";
            echo json_encode($response);

        }
    } else {
        $response["message"] = "salary Id";
        $response["status"] = "201";
        echo json_encode($response);

    }

}

if ($_POST['action'] == "getUserFaceRequestById") {
    if ($user_face_data_request_id != '') {
        $q = $d->selectRow('user_face_data_request.*,users_master.user_full_name,users_master.user_profile_pic', "user_face_data_request,users_master", "user_face_data_request.user_face_data_request_id=$user_face_data_request_id AND user_face_data_request.user_id=users_master.user_id");
        $data = mysqli_fetch_assoc($q);
       
        if ($data) {
            $response["user_face_data_request"] = $data;
            $response["message"] = "user_face_data_request Detail";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "user_face_data_request Detail";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['updateUserAttendance'] == "updateUserAttendance") {
  //
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    if ($_POST['attendance_id'] != '' ) {
        $attendance_date = $attendance_date_end;
        $attendance_time = $punch_out_time;
       
        $sq=$d->selectRow("*","attendance_master","attendance_id ='$attendance_id'");
        $attData=mysqli_fetch_array($sq);
        extract($attData);
        $shiftTimeId = $shift_time_id;
        $attendance_user_id = $attData['user_id'];
        $attendance_start_date = $attData['attendance_date_start'];
        $punch_in_time = $attData['punch_in_time'];
        $punch_out_time = $_REQUEST['punch_out_time'];
        $datePunchIN = $attendance_start_date ." ".$punch_in_time;
        $multiple_punch_in_out_data = $multiple_punch_in_out_data;
        $is_extra_day = $is_extra_day;

        
        if($_POST['attendance_punch_out_missing_status'] == 1){
            
            $qsm = $d->selectRow("user_token,device,shift_time_id,unit_id,user_full_name","users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
            $data_notification = mysqli_fetch_array($qsm);
            $user_token = $data_notification['user_token'];
            $device = $data_notification['device'];
            $unit_id = $data_notification['unit_id'];
            $tabPosition = "0"; 

            if ($attendance_start_date != '0000-00-00' && $attendance_start_date != 'null') {
                $day = date("w", strtotime($attendance_start_date));
            }

            $shiftQry = $d->selectRow("shift_day_master.*, shift_timing_master.is_multiple_punch_in, shift_timing_master.maximum_in_out, shift_timing_master.max_punch_out_time, shift_timing_master.max_shift_hour","shift_day_master,shift_timing_master","shift_day_master.shift_time_id=$shiftTimeId AND shift_day_master.shift_day='$day' AND shift_timing_master.shift_time_id=shift_day_master.shift_time_id");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $early_out_time = $shiftData['early_out_time'];
            $half_day_time_start = $shiftData['half_day_time_start'];
            $shift_end_time = $shiftData['shift_end_time'];
            $halfday_before_time = $shiftData['halfday_before_time'];
            $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
            $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
            $maximum_in_out = $shiftData['maximum_in_out'];
            $is_multiple_punch_in = $shiftData['is_multiple_punch_in'];


            $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

            $finalTotalMinute = 0;

            $dataArray = array();

            if (isset($multiplePunchDataArray) && $is_multiple_punch_in == 1 && count($multiplePunchDataArray) > 0) {

                for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                    if($multiplePunchDataArray[$i]["punch_out_date"] == '' 
                        || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){

                        $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$attendance_date,
                                $multiplePunchDataArray[$i]["punch_in_time"],$attendance_time);

                        $timeHr = explode(':', $totalHours.":00");
                        $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                        $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                        $oldAry = array(
                            "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"],
                            "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"],
                            "punch_out_date" => $attendance_date,
                            "punch_out_time" =>$attendance_time,
                            "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"],
                            "punch_out_image" => "",
                            "working_hour" => $totalHours.'',
                            "working_hour_minute" => $totalMinutes.'',
                        );

                        array_push($dataArray,$oldAry);
                    }else{
                        $oldAry = array(
                            "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"],
                            "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"],
                            "punch_out_date" => $multiplePunchDataArray[$i]["punch_out_date"],
                            "punch_out_time" =>$multiplePunchDataArray[$i]["punch_out_time"],
                            "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"],
                            "punch_out_image" => $multiplePunchDataArray[$i]["punch_out_image"],
                            "working_hour" => $multiplePunchDataArray[$i]["working_hour"],
                            "working_hour_minute" => $multiplePunchDataArray[$i]["working_hour_minute"],
                        );
                        $finalTotalMinute = $finalTotalMinute + $multiplePunchDataArray[$i]["working_hour_minute"];
                        array_push($dataArray,$oldAry);
                    }
                }  

                $multPunchDataJson = json_encode($dataArray).'';
            }else{
                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                    for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                        if($multiplePunchDataArray[$i]["punch_out_date"] == '' || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){
                            $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$attendance_date,
                                    $multiplePunchDataArray[$i]["punch_in_time"],$attendance_time);

                            $timeHr = explode(':', $totalHours.":00");
                            $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                            $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                            $oldAry = array(
                                "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"],
                                "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"],
                                "punch_out_date" => $attendance_date,
                                "punch_out_time" =>$attendance_time,
                                "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"],
                                "punch_out_image" => "",
                                "working_hour" => $totalHours,
                                "working_hour_minute" => $finalTotalMinute,
                            );
                            array_push($dataArray,$oldAry);
                        }
                    }

                    $multPunchDataJson = json_encode($dataArray).'';
                }
            }

            $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type","leave_master","user_id = '$attendance_user_id' AND leave_start_date = '$attendance_date_start'");

            $hasLeave = false;

            if (mysqli_num_rows($qryLeave) > 0) {

                $hasLeave = true;

                $leaveData = mysqli_fetch_array($qryLeave);

                $leave_id = $leaveData['leave_id'];
                $leave_type_id = $leaveData['leave_type_id'];
                $paid_unpaid = $leaveData['paid_unpaid'];
                $leave_reason = $leaveData['leave_reason'];
                $leave_day_type = $leaveData['leave_day_type'];
                $half_day_session = $leaveData['half_day_session'];
            }

            $parts = explode(':', $perdayHours);
            $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $datePunchOUT = $attendance_date ." ".$attendance_time;
            $datePunchIN = $attendance_date_start ." ".$punch_in_time;
            if ($early_out_time!="") {
                // code...
            $partEO = explode(':', $early_out_time);
            $min = ($partEO[0]*60) + ($partEO[1]) + ($partEO[2]/60);
            $defaultOutTime = $attendance_date." ".$shift_end_time;
            $strMin = "-".$min." minutes";
            $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

                if ($datePunchOUT >= $defaultOutTime) {
                    $early_out = "0";
                }else{
                    $early_out = "1";              
                }
            } else {
                $early_out = "0";
            }

            if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                $early_out = "0";
            }

            if ($is_multiple_punch_in == 0) {

                $totalHours = getTotalHoursWithNames($attendance_date_start,$attendance_date,$punch_in_time,$attendance_time);

                $totalHR = getTotalHours($attendance_date_start,$attendance_date,$punch_in_time,$attendance_time);


                $time = explode(':', $totalHR.":00");
                $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);
            }else{
                $total_minutes =  $finalTotalMinute;

                $hoursPM  = floor($total_minutes/60);
                $minutesPM = $total_minutes % 60;

                $totalHR = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                $times[] = $totalHR;

                if ($hoursPM > 0 && $minutesPM) {
                    $totalHoursNames = sprintf('%02d hr %02d min', $hoursPM, $minutesPM);
                }else if ($hoursPM > 0 && $minutesPM <= 0) {
                    $totalHoursNames = sprintf('%02d hr', $hoursPM);
                }else if ($hoursPM <= 0 && $minutesPM > 0) {
                    $totalHoursNames = sprintf('%02d min', $minutesPM);
                }else{
                    $totalHoursNames = "No Data";
                }
            }


            $avgWorkingDays = $total_minutes/$perDayHourMinute;

            $avg_working_days = round($avgWorkingDays * 2) / 2;

            $extra_working_hours_minutes = 0;

            if ($total_minutes > $perDayHourMinute) {
                $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
                $hours  = floor($extra_working_hours_minutes/60);
                $minutes = $extra_working_hours_minutes % 60;

                $extra_working_hours = sprintf('%02d:%02d', $hours, $minutes);
            }

            $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

            $qryBreakData = mysqli_fetch_array($qryBreak);

            $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

            if ($total_minutes > $totalBreakinutes) {
                $productive_working_hours_minutes = $total_minutes - $totalBreakinutes;

                $hoursPM  = floor($productive_working_hours_minutes/60);
                $minutesPM = $productive_working_hours_minutes % 60;

                $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
            }else{
                $productive_working_hours_minutes = $total_minutes;

                $hoursPM  = floor($productive_working_hours_minutes/60);
                $minutesPM = $productive_working_hours_minutes % 60;

                $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
            }

            $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND gps_accuracy <= 100  AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT'");

            $query11Data=mysqli_fetch_array($query11);

            if ($query11Data['SUM(last_distance)'] > 0) {
                $total_travel_meter = $query11Data['SUM(last_distance)'].'';
            }else{
                $total_travel_meter = '0';
            }

                $m->set_data('total_working_hours',$totalHR);
                $m->set_data('extra_working_hours',$extra_working_hours);
                $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
                $m->set_data('avg_working_days',$avg_working_days);
                $m->set_data('productive_working_hours',$productive_working_hours);
                $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
                $m->set_data('total_working_minutes',$total_minutes);
                $m->set_data('total_travel_meter',$total_travel_meter);
                $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);

            $a1 = array(
                'punch_out_time'=>$_POST['punch_out_time'],
                'attendance_date_end'=>$_POST['attendance_date_end'], 
                'attendance_status_change_by'=>$_COOKIE['bms_admin_id'], 
                'attendance_status_change_by_type'=>1, 
                'early_out'=>$early_out, 
                'avg_working_days'=>$avg_working_days, 
                'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
                'avg_working_days'=>$m->get_data('avg_working_days'),
                'productive_working_hours'=>$m->get_data('productive_working_hours'),                
                'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),                
                'total_working_hours'=>$m->get_data('total_working_hours'),                
                'total_working_minutes'=>$m->get_data('total_working_minutes'),                    
                'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'), 
                'total_travel_meter'=>$total_travel_meter, 
            );
            $max_punch_out_time= $attendance_date." ".date('H:i:s', strtotime($shiftData['max_punch_out_time']));
            $max_shift_hour= date('H:i:s', strtotime($shiftData['max_shift_hour']));
            $time=explode(":",$max_shift_hour);
            $convertedTime = date('H:i:s',strtotime(+$time[0].' hour '.+$time[1].' minutes '.+$time[2].' seconds',strtotime($punch_in_time)));
            
            if($max_punch_out_time > $datePunchOUT || $convertedTime > $attendance_time){
                $q1=$d->update("attendance_master",$a1,"attendance_id ='$_POST[attendance_id]'");
                
                $a2 = array('attendance_punch_out_missing_status'=> $_POST['attendance_punch_out_missing_status'] ,
                            'attendance_punch_out_missing_status_changed_by' => $_COOKIE['bms_admin_id'],
                            'updated_attendance_date' => $_POST['attendance_date_end'],
                            'updated_attendance_time' => $_POST['punch_out_time'],
                            'status_change_by_type' =>1 ,
                        );
                $q=$d->update("attendance_punch_out_missing_request",$a2,"attendance_punch_out_missing_id ='$attendance_punch_out_missing_id'");



                $leaveValue = false;

                if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                    $leaveValue = true;
                } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $attendance_time) {
                    $leaveValue = true;
                }
                $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);
            

                if ($total_minutes >= $minimum_hours_for_full_day_min) {
                    $noLeave = true;
                }else{
                    $noLeave = false;
                }

                $alreadyLeaveQryCheck = false; 

                if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {


                    $leave_total_days = 1;

                    // check half day and full day


                    if ($total_minutes < $maximum_halfday_hours_min) {
                        $avgCount = "0";
                        $leaveTypeName = "Full Day";
                        $leaveType = "0";
                        $is_leave = "1";
                        $extra_day_leave_type = "1";
                        $half_day_session = "0";                
                    }else{
                        $avgCount = "0.5";
                        $leaveType = "1";
                        $is_leave = "2";
                        $extra_day_leave_type = "2";
                        $leaveTypeName = "Half Day";
                    }

                    if ($is_extra_day == 1) {
                        $is_leave = "0";
                    }else{
                        $extra_day_leave_type = "0";
                    }

                    if ($hasLeave == true) {

                        // Change already applied leave

                        $title = "Auto Leave Applied - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed ($totalHR)";
                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                        $m->set_data('leave_type_id',$leave_type_id);
                        $m->set_data('paid_unpaid',$paid_unpaid);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('half_day_session',$half_day_session);
                        $m->set_data('auto_leave_reason',$titleMessage);

                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'half_day_session'=>$m->get_data('half_day_session'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                        );

                        $leaveQry = $d->update("leave_master",$a,"user_id = '$attendance_user_id' AND leave_id = '$leave_id'");
                        

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                        
                    }else{

                        // Auto Leave

                        $title = "Auto Leave Applied"." - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed ($totalHours)";
                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                        $m->set_data('leave_type_id',"0");
                        $m->set_data('society_id',$society_id);
                        $m->set_data('paid_unpaid',"1");
                        $m->set_data('unit_id',$unit_id);
                        $m->set_data('user_id',$attendance_user_id);
                        $m->set_data('auto_leave_reason',$titleMessage);
                        $m->set_data('leave_start_date',$attendance_date_start);
                        $m->set_data('leave_end_date',$attendance_date_start);
                        $m->set_data('leave_total_days',$leave_total_days);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('leave_status',"1");
                        $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'society_id' =>$m->get_data('society_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                            'leave_start_date'=>$m->get_data('leave_start_date'),
                            'leave_end_date'=>$m->get_data('leave_end_date'),
                            'leave_total_days'=>$m->get_data('leave_total_days'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'leave_status'=>$m->get_data('leave_status'),
                            'leave_created_date'=>$m->get_data('leave_created_date'),
                        );

                        $leaveQry = $d->insert("leave_master",$a);

                        $alreadyLeaveQryCheck = true;

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                    }

                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',$avgCount);
                    $m->set_data('is_leave',$is_leave);
                    $m->set_data('extra_day_leave_type',$extra_day_leave_type);


                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'is_leave'=>$m->get_data('is_leave'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),

                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                    
                }else if($noLeave == true && $hasLeave == true){
                    // Remove leave if present and full time

                    $leaveQry = $d->delete("leave_master","user_id = '$attendance_user_id' AND leave_id = '$leave_id'");

                    $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                    $title = "Applied Leave Cancelled";
                    $description = "For Date : ".$attendance_date_start;

                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                }

                // Code - Maximum Late IN & Early OUT Limit Check -> Apply Leave

                $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND MONTH(attendance_date_start) = MONTH(NOW()) AND late_in = '1'");

                $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND MONTH(attendance_date_start) = MONTH(NOW()) AND early_out = '1'");

                $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                if ($hasLeave = false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($late_in == "1" || $early_out == "1")) {

                    $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";

                    $m->set_data('leave_type_id',"0");
                    $m->set_data('society_id',$society_id);
                    $m->set_data('paid_unpaid',"1");
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('user_id',$attendance_user_id);
                    $m->set_data('auto_leave_reason',$titleMessage);
                    $m->set_data('leave_start_date',$attendance_date_start);
                    $m->set_data('leave_end_date',$attendance_date);
                    $m->set_data('leave_total_days',"1");
                    $m->set_data('leave_day_type',"1");
                    $m->set_data('leave_status',"1");
                    $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                    $a = array(
                        'leave_type_id'=>$m->get_data('leave_type_id'),
                        'society_id' =>$m->get_data('society_id'),
                        'paid_unpaid' =>$m->get_data('paid_unpaid'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'), 
                        'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                        'leave_start_date'=>$m->get_data('leave_start_date'),
                        'leave_end_date'=>$m->get_data('leave_end_date'),
                        'leave_total_days'=>$m->get_data('leave_total_days'),
                        'leave_day_type'=>$m->get_data('leave_day_type'),
                        'leave_status'=>$m->get_data('leave_status'),
                        'leave_created_date'=>$m->get_data('leave_created_date'),
                    );

                    $leaveQry = $d->insert("leave_master",$a);

                    $title = "Auto Leave Applied - Half Day";

                    $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");

                    if ($is_extra_day == 1) {
                        $is_leave = "0";
                    }else{
                        $extra_day_leave_type = "0";
                    }

                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',"0.5");
                    $m->set_data('is_leave',$is_leave);
                    $m->set_data('extra_day_leave_type',$extra_day_leave_type);

                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'is_leave'=>$m->get_data('is_leave'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                }
            
            }else{
                $_SESSION['msg1'] = "Punch out time exceeds maximum punch out time!";
                header("Location: ../apAdmin/punchOutMissingRequest?dId=".$redirect_floor_id."&bId=".$redirect_block_id."&status=".$redirect_status);
                exit();
            }
           
        }else if($_POST['attendance_punch_out_missing_status']  == 2){
            
            $a2 = array('attendance_punch_out_missing_status'=> $_POST['attendance_punch_out_missing_status'] ,
                        'attendance_punch_out_missing_status_changed_by' => $_COOKIE['bms_admin_id'],
                        'status_change_by_type' => 1,
                        'punch_out_missing_reject_reason' => $_POST['punch_out_missing_reject_reason']);
            $q=$d->update("attendance_punch_out_missing_request",$a2,"attendance_punch_out_missing_id ='$attendance_punch_out_missing_id'");
        }
       
             
        if ($q == true) {
            $_SESSION['msg']="Punch Out Missing Request Status Update Successfully";
            $d->insert_log("","$society_id","$_COOKIE[bms_admin_id]","$created_by","Punch Out Missing Request Status Update Successfully");
            // header("Location: ../apAdmin/punchOutMissingRequest?dId=".$redirect_floor_id."&bId=".$redirect_block_id."&status=".$redirect_status);
        } else {
            $_SESSION['msg1'] = "Something Wrong";
            // header("Location: ../apAdmin/punchOutMissingRequest?dId=".$redirect_floor_id."&bId=".$redirect_block_id."&status=".$redirect_status);
        }
    } else {
        $_SESSION['msg1'] = "Something Wrong";
        // header("Location: ../apAdmin/punchOutMissingRequest?dId=".$redirect_floor_id."&bId=".$redirect_block_id."&status=".$redirect_status);
    }
}

if ($_POST['action'] == "getUserTaskPauseHistoryDetail") {
    if ($task_pause_history_id != '') {
        $mainArray = array();
        
        $q1 = $d->selectRow("*","task_pause_history,users_master","task_pause_history.user_id=users_master.user_id AND task_pause_history.task_pause_history_id ='$task_pause_history_id'");
        $data = mysqli_fetch_assoc($q1);
        $mainData['user_full_name'] = $data['user_full_name'];
        $mainData['task_pause_date'] = date("d M Y h:i A", strtotime($data['task_pause_date']));
        $mainData['task_resume_date'] = $data['leave_day_type']=='0000-00-00 00:00:00' && $data['leave_day_type']==null?'':date("d M Y h:i A", strtotime($data['task_resume_date']));
        $mainData['task_pause_reason'] = $data['task_pause_reason'];
        if($data['task_resume_date'] != '0000-00-00 00:00:00' && $data['task_resume_date'] != null){
            $pause_time = new DateTime($data['task_pause_date']);
            $resume_time = new DateTime($data['task_resume_date']);
            $interval = $pause_time->diff($resume_time);
            $mainData['pause_time'] = $interval->format('%H:%I'); 
          }else{
            $mainData['pause_time'] = '';
          }
        
        if ($mainData) {
            $response["pause_history"] = $mainData;
            $response["message"] = "Pause History";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Pause History";
            $response["pause_history"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getWFHDetailById") {
    if ($wfh_id != '') {
       
        $q = $d->selectRow('wfh_master.*,users_master.user_full_name,users_master.user_designation', "wfh_master,users_master", "wfh_master.society_id='$society_id' AND wfh_master.user_id=users_master.user_id AND wfh_master.wfh_id='$wfh_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        $wfh_data = array();
        $wfh_data['user_full_name'] = $data['user_full_name'];
        $wfh_data['user_designation'] = $data['user_designation'];
        $wfh_data['wfh_start_date'] = date("d M Y", strtotime($data['wfh_start_date']));
        $wfh_data['wfh_end_date'] = date("d M Y", strtotime($data['wfh_end_date']));
        $wfh_data['wfh_attachment'] = $data['wfh_attachment'];
        $wfh_data['wfh_reason'] = $data['wfh_reason'];
        // $wfh_data['wfh_take_selfie'] = $data['wfh_take_selfie'];
        if ($data['wfh_day_type']==0) {
            $wfh_data['wfh_day_type'] = "Full Day";
        } else {
            $wfh_data['wfh_day_type'] = "Half Day";
        }
        $wfh_data['wfh_latitude'] = $data['wfh_latitude'];
        $wfh_data['wfh_longitude'] = $data['wfh_longitude'];
        $wfh_data['wfh_declined_reason'] = $data['wfh_declined_reason'];
        if ($data['wfh_take_selfie']==0) {
            $wfh_data['wfh_take_selfie'] = "No";
        } else {
            $wfh_data['wfh_take_selfie'] = "Yes";
        }
        $wfh_data['wfh_attendance_range'] = $data['wfh_attendance_range'];
        if ($data) {
            $response["wfh"] = $wfh_data;
            $response["message"] = "WFH Data";
            $response["status"] = "200";

            echo json_encode($response);
        } else {
            $response["message"] = "Failure";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID Required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getMonthDataByYear") {
    if ($month != '' && $year !="") {
        $mainArray = array();
        $attendaceArrayData = array();
        $HalfDayArrayData = array();
        $earlyOutArrayData = array();
        $LateCHeckData = array();
        $extraHours = array();
        $LeaveArrayData = array();
        $holiday_data = array();
        $PunchOUtMissingData = array();
        $user = $d->selectRow("shift_time_id","users_master", "user_id='$user_id'"); 
        $userData = mysqli_fetch_assoc($user);
        $shift = $d->selectRow("*","shift_timing_master", "shift_time_id='$userData[shift_time_id]'"); 
        $shiftData = mysqli_fetch_assoc($shift);
        $days = cal_days_in_month(CAL_GREGORIAN, $month, date('Y'));
        $start_date = date("$year-$month-01"); // hard-coded '01' for first day
        $end_date =   date('Y-m-t', strtotime($start_date));
        $shift_weekoff = explode(',',$shiftData['week_off_days']);
                   
        for ($i = 0; $i < $days; $i++) {

            $date = $year.'/'.$month.'/'.$i; //format date
            $get_name = date('N', strtotime($date)); //get week day
                if($get_name ==7){
                $get_name = 0;
                }
                else
                {
                $get_name = $get_name-1;
                }
            if (in_array($get_name, $shift_weekoff)){
                $workdays[] = $i;
            }
        }
        // print_r($workdays);die;
        $totalworkingdays = $days - count($workdays);
        
        $holiday = $d->selectRow('*', "holiday_master", "holiday_start_date BETWEEN  '$start_date' AND '$end_date' AND holiday_status=0","GROUP BY holiday_start_date"); 
        while ($holidayData = mysqli_fetch_array($holiday)) {
            array_push($holiday_data,$holidayData);
        }
        if($holiday_data){
            $totalworkingdays = $totalworkingdays - count($holiday_data);
        }
        
        $o = date('00:00:00');
        $attendace = $d->select("attendance_master", "user_id='$user_id' AND attendance_date_start BETWEEN  '$start_date' AND '$end_date' "); 
        while ($attendace_data = mysqli_fetch_array($attendace)) {
            $half_day_time_start = strtotime($shiftData['half_day_time_start']);
            $shift_start_time = strtotime($shiftData['shift_start_time']);
            $shift_out_time = strtotime($shiftData['shift_end_time']);
            $punch_out_time = strtotime($attendace_data['punch_out_time']);
            $punch_in_time = strtotime($attendace_data['punch_in_time']);
            if($punch_out_time>$half_day_time_start)
            {
                array_push($HalfDayArrayData,$attendace_data);
            }
            else
            {
                array_push($attendaceArrayData,$attendace_data);
            }
            if($punch_in_time > $shift_start_time)
            {
                array_push($LateCHeckData,$attendace_data);
            }
            if($punch_out_time < $shift_out_time)
            {
                
                array_push($earlyOutArrayData,$attendace_data);
            }
            if($punch_out_time > $shift_out_time)
            {
               
                $pause_time = new DateTime($attendace_data['punch_out_time']);
                $resume_time = new DateTime($shiftData['shift_end_time']);
                $interval = $pause_time->diff($resume_time);
                $interval =  $interval->format('%H hours %I minutes'); 
               // print_r("+ $interval");
                $o =  date(' H:i',strtotime("+ $interval",strtotime($o)));
            }
            
            if($shiftData['shift_type']=="Day"){
                if(($attendace_data['attendance_date_start']<date('Y-m-d')) && $attendace_data['punch_out_time']=="00:00:00")
                {
                    array_push($PunchOUtMissingData,$attendace_data);

                }
            }
            
        }
        $leaves = $d->select("leave_master", "user_id='$user_id' AND leave_start_date BETWEEN  '$start_date' AND '$end_date' "); 
        while ($leave_data = mysqli_fetch_array($leaves)) {
            array_push($LeaveArrayData,$leave_data);
        }
        $mainArray['working'] = COUNT($attendaceArrayData);
        $mainArray['leave'] = COUNT($LeaveArrayData);
        $mainArray['extraHours'] = $o;
        $mainArray['punchOutMiss'] = COUNT($PunchOUtMissingData);
        $mainArray['earlyOut'] = COUNT($earlyOutArrayData);
        $mainArray['lateCheckIn'] = COUNT($LateCHeckData);
        $mainArray['halfDAy'] = COUNT($HalfDayArrayData);
        $mainArray['holiday'] = count($holiday_data);
        $mainArray['totalWorkDays'] = $totalworkingdays;
        $mainArray['weekOff'] = count($workdays);
        if(COUNT($attendaceArrayData)>$totalworkingdays)
        {
            $mainArray['extra_days'] = COUNT($attendaceArrayData) - $totalworkingdays;
            $mainArray['absent'] = 0;
        }
        else
        {
            $mainArray['extra_days'] = 0;
            $mainArray['absent'] =$totalworkingdays - COUNT($attendaceArrayData);
        }
        if ($mainArray) {
            $response["data"] = $mainArray;
            $response["message"] = "Month History";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Month History";
            $response["pause_history"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getTaskStepById") {
    if ($task_step_id != '') {
        $q = $d->selectRow('*', "task_step_master", "task_step_id='$task_step_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["task_step"] = $data;
            $response["message"] = "Task Step";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Task Step";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getTaskStepDetailById") {
    if ($task_step_id != '') {
       
        $q = $d->selectRow('*', "task_step_master", "task_step_id='$task_step_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        $step_data = array();
        $step_data['task_step'] = $data['task_step'];
        $step_data['task_step_note'] = $data['task_step_note'];
        $step_data['task_step_due_date'] = date("d M Y", strtotime($data['task_step_due_date']));
        $step_data['task_step_created_date'] = date("d M Y h:i A", strtotime($data['task_step_created_date']));
        $step_data['task_step_complete_date'] = $data['task_step_complete_date']==null?'':date("d M Y h:i A", strtotime($data['task_step_complete_date']));
        if($data['task_step_complete'] == 0){ 
            $step_data['task_step_complete'] = "Pending";
        }
        else if($data['task_step_complete'] == 1){
            $step_data['task_step_complete'] = "In Progress";
        }else{
            $step_data['task_step_complete'] = "Completed";
        }
        if ($data) {
            $response["step"] = $step_data;
            $response["message"] = "Step Data";
            $response["status"] = "200";

            echo json_encode($response);
        } else {
            $response["message"] = "Failure";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID Required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getUserLeaveCountByDate") {
    if ($user_id != '' && $leave_type_id != '' && $date != '') {
        $useHalfLeaveq = $d->selectRow("COUNT(*) AS use_leave","leave_master","user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_day_type=1 AND DATE_FORMAT(leave_start_date, '%Y-%m')=DATE_FORMAT('$date', '%Y-%m')");
        $useHalfLeaveData = mysqli_fetch_assoc($useHalfLeaveq);

        $useFullLeaveq = $d->selectRow("COUNT(*) AS use_leave","leave_master","user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_day_type=0 AND DATE_FORMAT(leave_start_date, '%Y-%m')=DATE_FORMAT('$date', '%Y-%m')");
        $useFullLeaveData = mysqli_fetch_assoc($useFullLeaveq);

        $use_leave = $useFullLeaveData['use_leave'] + ($useHalfLeaveData['use_leave']/2);

        $data['use_leave'] = $use_leave;
        if ($data) {
            $response["leave"] = $data;
            $response["message"] = "Leave Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Leave Data";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


if ($_POST['action'] == "autoCalculateCommonEarnDeductForSalarySlip") {
    if ($block_id != '' && $floor_id!="" && $salary_group_id!="") {
      
        $mainData2 = array();
        $exrtaValid = '0';
        $flatAmount = 0;
        $q1 = $d->select("salary_common_value_master LEFT JOIN salary_earning_deduction_type_master ON salary_earning_deduction_type_master.salary_earning_deduction_id = salary_common_value_master.salary_earning_deduction_id","salary_group_id='$salary_group_id' ");
        while ($data = mysqli_fetch_assoc($q1)) {
              
                if($data['amount_type']==1 && $data['earning_deduction_type']==0){
                   
                    if((int)$data['salary_earning_deduction_id']==(int)$salary_earning_deduction_id){
                        $amount_value = $earnFixed;
                    }else{
                        $amount_value = $data['amount_value'];
                    }
                   
                    $flatAmount  = $flatAmount +$amount_value;
                }
            if($salary_earning_deduction_id !=""){
                if($data['salary_common_value_earn_deduction'] !=""){
                   
                    if(($salary_earning_deduction_id==$data['salary_earning_deduction_id']) && $data['salary_common_value_earn_deduction'] !=""){
                        $exrtaValid=1;
                    }
                }
            }
            array_push($mainData2, $data);
        }
        if (!empty($mainData2)) {
          ///  $data['deduct_for']=$mainData2;
            $response["earn_deduct_calculation"] = $mainData2;
            $response["flatAmount"] = $flatAmount;
            $response["exrtaValid"] = $exrtaValid;
            $response["message"] = "earn_deduct_calculation";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "earn_deduct_calculation";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getUserMACAddressDetailById") {
    if ($user_mac_address_id != '') {
       
        $q = $d->selectRow('user_mac_address_master.*, users_master.user_full_name,users_master.user_mac_address,u.user_full_name AS status_change_by,bms_admin_master.admin_name', "users_master,user_mac_address_master LEFT JOIN users_master AS u ON u.user_id=user_mac_address_master.mac_address_status_change_by LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=user_mac_address_master.mac_address_status_change_by", "user_mac_address_master.user_id=users_master.user_id AND user_mac_address_master.user_mac_address_id='$user_mac_address_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        $step_data = array();
        $step_data['mac_address'] = $data['mac_address'];
        $step_data['user_mac_address'] = $data['user_mac_address'];
        $step_data['user_full_name'] = $data['user_full_name'];
        $step_data['mac_address_change_reason'] = $data['mac_address_change_reason'];
        $step_data['mac_address_reject_reason'] = $data['mac_address_reject_reason'];
        $step_data['mac_address_status_change_by_type'] = $data['mac_address_status_change_by_type'];
        if($data['mac_address_status_change_by_type']==0){
            $step_data['status_change_by'] = $data['status_change_by'];
        }
        else
        {
            $step_data['status_change_by'] = $data['admin_name'];
        }
        $step_data['mac_address_status_change_date'] = date("d M Y", strtotime($data['mac_address_status_change_date']));
        $step_data['mac_address_request_date'] = date("d M Y h:i A", strtotime($data['mac_address_request_date']));
       /*  $step_data['task_step_complete_date'] = $data['task_step_complete_date']==null?'':date("d M Y h:i A", strtotime($data['task_step_complete_date'])); */
        if($data['mac_address_change_status'] == 0){ 
            $step_data['mac_address_change_status'] = "Pending";
        }
        else if($data['mac_address_change_status'] == 1){
            $step_data['mac_address_change_status'] = "Approved";
        }else{
            $step_data['mac_address_change_status'] = "Reject";
        }
        if ($data) {
            $response["step"] = $step_data;
            $response["message"] = "Step Data";
            $response["status"] = "200";

            echo json_encode($response);
        } else {
            $response["message"] = "Failure";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID Required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getLeaveBalanceForAutoLeave") {
    if ($user_id != '' && $leave_type_id != '' && $leave_date != '') {
        
        $user=$d->selectRow("users_master.shift_time_id,users_master.floor_id,user_employment_details.joining_date","users_master,user_employment_details","users_master.user_id=user_employment_details.user_id AND users_master.user_id='$user_id'");
        $userData = mysqli_fetch_array($user);
        $floor_id = $userData['floor_id'];
        $i=1;
        $j=1;
        $total_month = '12';
        $currentYear = date('Y');
        $calQ = $d->selectRow("leave_calculation","leave_assign_master","leave_assign_master.user_id = '$user_id' AND leave_assign_master.leave_type_id = '$leave_type_id' AND leave_assign_master.assign_leave_year=DATE_FORMAT('$leave_date', '%Y')");
        $calData = mysqli_fetch_array($calQ);
        $leave_calculation = $calData['leave_calculation'];
        if($currentYear == date('Y', strtotime($userData['joining_date']))){
            $i=date('m', strtotime($userData['joining_date']));
            $j=date('m', strtotime($userData['joining_date']));
            $total_month = 12 - date('m', strtotime($userData['joining_date'])) + 1;
        }
        $lq = $d->selectRow("leave_start_date,
        (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id='$leave_type_id' AND leave_payout_year=DATE_FORMAT('$leave_date', '%Y')) AS pay_and_carry_forward_leave,
        (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
        AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND leave_type_id='$leave_type_id') AS total_half_day_leave,
        (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
        AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND leave_type_id='$leave_type_id') AS total_full_day_leave,(SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
        AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_start_date, '%Y-%m')=DATE_FORMAT('$leave_date', '%Y-%m') AND leave_type_id='$leave_type_id') AS use_half_day_leave,
        (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
        AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_start_date, '%Y-%m')=DATE_FORMAT('$leave_date', '%Y-%m') AND leave_type_id='$leave_type_id') AS use_full_day_leave", 
        "leave_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND leave_status=1 AND paid_unpaid=0 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y')","ORDER BY leave_master.leave_start_date DESC LIMIT 1");
        $leaveData = mysqli_fetch_assoc($lq);
        
        $use_leave = $leaveData['use_full_day_leave'] + ($leaveData['use_half_day_leave']/2);  
        if($leave_calculation == 1){
            $max_leave_date = date("m", strtotime($leaveData['leave_start_date']));
            $leave_month = date("m", strtotime($leave_date));
            if($max_leave_date>$leave_month){
                $loop_count = $max_leave_date;
            }else{
                $loop_count = $leave_month;
            }
            $monthly_leave_array = array();
            $total_remaining = 0; 
            $negative_remaining = 0;
            for ($i; $i <= $loop_count; $i++) { 
                $userLeaveq = $d->selectRow("user_total_leave,
                applicable_leaves_in_month,
                (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id='$leave_type_id' AND leave_payout_year=DATE_FORMAT('$leave_date', '%Y')) AS pay_and_carry_forward_leave,
                (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND MONTH(leave_start_date)='$i' AND leave_type_id='$leave_type_id') AS total_half_day_leave, 
                (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND MONTH(leave_start_date)='$i' AND leave_type_id='$leave_type_id') AS total_full_day_leave", 
                "leave_assign_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND assign_leave_year=DATE_FORMAT('$leave_date', '%Y')");
                $data = mysqli_fetch_assoc($userLeaveq);
                $applicable_leaves_in_month = $data['applicable_leaves_in_month'];
                $user_total_leave = $data['user_total_leave'];
                $leave_avg = ($data['user_total_leave'] - $data['pay_and_carry_forward_leave'])/$total_month;
                $avg_leave_str = explode(".",$leave_avg);
                $explode01 = $avg_leave_str[1][0];
                if($explode01=='' || $explode01<=4){
                    $explode02 = 0;
                }elseif($explode01<=9 || $explode01>=5){
                    $explode02 = 5;
                }
                $apply_leave = $avg_leave_str[0].'.'.$explode02;

                $used_in_month = $data['total_full_day_leave'] + ($data['total_half_day_leave']/2);
                $remaining = $leave_avg - $used_in_month;

                if($remaining<0){
                    $negative_remaining += $remaining;
                }

                $leave_array = array('avg_leave' => $leave_avg,
                                    'used_in_month' => $used_in_month,
                                    'remaining' => $remaining);

                $monthly_leave_array[$i] = $leave_array;
            }
            $final_remaining = 0;  
            for ($j; $j <= $loop_count; $j++) { 
                $result = $monthly_leave_array[$j]['remaining'];
                if($monthly_leave_array[$j]['remaining']>0 && $negative_remaining<0){
                    $result = $monthly_leave_array[$j]['remaining']+($negative_remaining);
                    $negative_remaining = $result;
                    if($negative_remaining>=0){
                        $negative_remaining = 0;
                    }
                    if($result<0){
                        $result = 0;
                    }
                }elseif($monthly_leave_array[$j]['remaining']<0){
                    $result = $negative_remaining;
                    if($result<0){
                        $result = 0;
                    }
                }
                
                $monthly_leave_array[$j]['remaining'] = $result;
                $final_remaining += $monthly_leave_array[$j]['remaining'];
                $total_remaining = $final_remaining;
            }
            $available_paid_leave = $total_remaining;

            if($use_leave>=$applicable_leaves_in_month){
                $available_paid_leave = 0;
            }else{
                if($available_paid_leave > $applicable_leaves_in_month){
                    $available_paid_leave = $applicable_leaves_in_month;
                }
                if($available_paid_leave<0){
                    $available_paid_leave = 0;
                }
            }
        }else{
            $leave_month = date("m", strtotime($leave_date));
            $userLeaveq = $d->selectRow("user_total_leave,
                applicable_leaves_in_month,
                (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id='$leave_type_id' AND leave_payout_year=DATE_FORMAT('$leave_date', '%Y')) AS pay_and_carry_forward_leave,
                (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND leave_type_id='$leave_type_id') AS total_half_day_leave, 
                (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_start_date,'%Y') = DATE_FORMAT('$leave_date', '%Y') AND leave_type_id='$leave_type_id') AS total_full_day_leave", 
                "leave_assign_master", "user_id='$user_id' AND leave_type_id='$leave_type_id' AND assign_leave_year=DATE_FORMAT('$leave_date', '%Y')");
                $data = mysqli_fetch_assoc($userLeaveq);
                
                $applicable_leaves_in_month = $data['applicable_leaves_in_month'];
                $user_total_leave = $data['user_total_leave'];
                $available_paid_leave = $user_total_leave;

                $use_leave = $data['total_full_day_leave'] + ($data['total_half_day_leave']/2); 

                if($use_leave>=$available_paid_leave){
                    $available_paid_leave = 0;
                }else{
                    if($available_paid_leave > $applicable_leaves_in_month){
                        $available_paid_leave = $applicable_leaves_in_month;
                    }
                    if($available_paid_leave<0){
                        $available_paid_leave = 0;
                    }
                }
        }
        
        $remaining_leave = $user_total_leave - ($leaveData['total_full_day_leave'] + ($leaveData['total_half_day_leave']/2))- $leaveData['pay_and_carry_forward_leave'];
        
        $available_paid_leave_str = explode(".",$available_paid_leave);
        $explode01 = $available_paid_leave_str[1][0];
        if($explode01=='' || $explode01<=4){
            $explode02 = 0;
        }elseif($explode01<=9 || $explode01>=5){
            $explode02 = 5;
        }
        $available_paid_leave = $available_paid_leave_str[0].'.'.$explode02;
        $main_data['available_paid_leave'] = $available_paid_leave;
        $main_data['remaining_leave'] = $remaining_leave;
        
        if ($main_data) {
            $response["leave"] = $main_data;
            $response["message"] = "Leave Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Leave Data";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getWorkfolioEmployeesById") {
    $mainData = array();
    if ($workfolio_team_id != '') {
        $q = $d->select("workfolio_employees,workfolio_team", "workfolio_employees.teamId=workfolio_team.teamId AND workfolio_team.workfolio_team_id='$workfolio_team_id'");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }
        if ($mainData) {
            $response["team_employee"] = $mainData;
            $response["message"] = "workfolio Team Employee";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "workfolio Team Employee";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getTaskPriority") {
    if ($task_priority_id != '') {
        $q = $d->selectRow('*', "task_priority_master", "task_priority_id='$task_priority_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["task_priority"] = $data;
            $response["message"] = "Task Priority";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Task Priority";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getTaskUserHistory") {
    if ($task_id != '') {
        $mainArray = array();
        
        $q=$d->select("task_user_history,users_master,block_master,floors_master","task_user_history.user_id=users_master.user_id AND block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND task_user_history.society_id='$society_id' AND task_user_history.task_id='$task_id'","ORDER BY task_user_history.task_user_history_id DESC");
        while ($data = mysqli_fetch_assoc($q)) {
            $mainData['task_id'] = $data['task_id'];
            $mainData['block_name'] = $data['block_name'];
            $mainData['floor_name'] = $data['floor_name'];
            $mainData['user_full_name'] = $data['user_full_name'];
            $mainData['task_pull_back_date'] = $data['task_pull_back_date'];
            $mainData['task_user_history_id'] = $data['task_user_history_id'];
            $mainData['task_assign_date'] = date("d M Y h:i A", strtotime($data['task_assign_date']));
            $mainData['status_change_date'] = $data['status_change_date']=='0000-00-00 00:00:00' || $data['status_change_date'] == null?'':date("d M Y h:i A", strtotime($data['status_change_date']));
            $totalPause = $d->count_data_direct("task_pause_history_id","task_pause_history","task_user_history_id='$data[task_user_history_id]' AND user_id='$data[user_id]'");
            $mainData['total_pause'] = $totalPause;
            array_push($mainArray, $mainData);
        }  
        if ($mainArray) {
            $response["task_history"] = $mainArray;
            $response["message"] = "Task User History";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Task User History";
            $response["floor"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getLeaveTypeByUserId") {
    $mainData = array();
    if ($user_id != '' && $year != '') {
        $q=$d->select("leave_type_master,leave_assign_master","leave_type_master.leave_type_id=leave_assign_master.leave_type_id AND leave_type_master.society_id='$society_id' AND leave_assign_master.user_id='$user_id' AND leave_type_master.leave_type_delete=0 AND leave_assign_master.assign_leave_year='$year' AND leave_type_master.leave_type_active_status=0");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }
        if ($mainData) {
            $response["leave_type"] = $mainData;
            $response["message"] = "Leave Type";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getAttendanceSummary") {
   
    $mainData = array();
    $month = date("Y-m",strtotime($month));
    if ($user_id != '' && $month != '' && $key != '') {
       
        if($key =='total_full_leave')
        {
            $q=$d->select("leave_master","leave_master.user_id =  $user_id
                        AND DATE_FORMAT(leave_master.leave_start_date,'%Y-%m')='$month' AND leave_day_type=0 AND leave_status=1");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                $date = $Leavedata['leave_start_date']." (".date('l',strtotime($Leavedata['leave_start_date'])).") ".$Leavedata['leave_type_name']."";
                
                array_push($mainData,  $date);
            }
        }
         else if($key =='total_half_leave')
        {
            $q=$d->select("leave_master","leave_master.user_id =  $user_id
                        AND DATE_FORMAT(leave_master.leave_start_date,'%Y-%m')='$month' AND leave_day_type=1 AND leave_status=1");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                $date = $Leavedata['leave_start_date']." (".date('l',strtotime($Leavedata['leave_start_date'])).") ".$Leavedata['leave_type_name']."";
                
                array_push($mainData,  $date);
            }
        }else if($key =='total_paid_leave_full')
        {
            $q=$d->select("leave_master","leave_master.user_id =  $user_id
                        AND DATE_FORMAT(leave_master.leave_start_date,'%Y-%m')='$month' AND leave_day_type=0 AND leave_status=1 AND paid_unpaid=0");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                $date = $Leavedata['leave_start_date']." (".date('l',strtotime($Leavedata['leave_start_date'])).") ".$Leavedata['leave_type_name']."";
                
                array_push($mainData,  $date);
            }
        }else if($key =='total_paid_leave_half')
        {
            $q=$d->select("leave_master","leave_master.user_id =  $user_id
                        AND DATE_FORMAT(leave_master.leave_start_date,'%Y-%m')='$month' AND leave_day_type=1 AND leave_status=1 AND paid_unpaid=0");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                $date = $Leavedata['leave_start_date']." (".date('l',strtotime($Leavedata['leave_start_date'])).") ".$Leavedata['leave_type_name']."";
                
                array_push($mainData,  $date);
            }
        }
        else if($key =='ealry_out'){
            $q=$d->select("attendance_master","user_id =  $user_id
                        AND DATE_FORMAT(attendance_date_start,'%Y-%m')='$month' AND early_out=1");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
               
                 $date = $Leavedata['attendance_date_start']."(".date('l',strtotime($Leavedata['attendance_date_start'])).")";
                
                array_push($mainData,  $date);
            }
        }
        else if($key =='late_in'){
           
            $q=$d->select("attendance_master","user_id =  $user_id
                        AND DATE_FORMAT(attendance_date_start,'%Y-%m')='$month' AND late_in=1");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
              
                 $date = $Leavedata['attendance_date_start']." (".date('l',strtotime($Leavedata['attendance_date_start'])).")"; 
                array_push($mainData,  $date);
            }
        }
        else if($key =='total_out_missing'){
            $q=$d->select("attendance_master","user_id =  $user_id
            AND DATE_FORMAT(attendance_date_start,'%Y-%m')='$month' AND punch_in_request=1");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                 $date = $Leavedata['attendance_date_start']." (".date('l',strtotime($Leavedata['attendance_date_start'])).")";
                array_push($mainData,  $date);
            }
        }
        else if($key =='total_out_of_range'){
            $q=$d->select("attendance_master","user_id =  $user_id
            AND DATE_FORMAT(attendance_date_start,'%Y-%m')='$month' AND punch_in_in_range=1");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                 $date = $Leavedata['attendance_date_start']." (".date('l',strtotime($Leavedata['attendance_date_start'])).")";
                array_push($mainData,  $date);
            }
        }
        else if($key =='total_out_of_range_out'){
            $q=$d->select("attendance_master","user_id =  $user_id
            AND DATE_FORMAT(attendance_date_start,'%Y-%m')='$month' AND punch_out_in_range=1");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                 $date = $Leavedata['attendance_date_start']." (".date('l',strtotime($Leavedata['attendance_date_start'])).")";
                array_push($mainData,  $date);
            }
        }
        else if($key =='out_miss'){
            $q=$d->select("attendance_punch_out_missing_request","user_id =  $user_id
            AND DATE_FORMAT(attendance_date,'%Y-%m')='$month'");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                 $date = $Leavedata['attendance_date']." (".date('l',strtotime($Leavedata['attendance_date'])).")";
                array_push($mainData,  $date);
            }
        }
        else if($key =='out_miss_pending'){
            $q=$d->select("attendance_master","user_id =  $user_id
            AND DATE_FORMAT(attendance_date_start,'%Y-%m')='$month' AND attendance_date_end='0000-00-00'");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                 $date = $Leavedata['attendance_date_start']." (".date('l',strtotime($Leavedata['attendance_date_start'])).")";
                array_push($mainData,  $date);
            }
        }
        else if($key =='present'){
            $q=$d->select("attendance_master LEFT JOIN leave_master ON leave_master.leave_start_date=attendance_master.attendance_date_start AND leave_master.user_id=attendance_master.user_id AND leave_status=1","attendance_master.user_id =  $user_id
            AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y-%m')='$month' AND attendance_master.attendance_date_end!='0000-00-00'","ORDER BY attendance_master.attendance_date_start ASC");
            while ($Leavedata = mysqli_fetch_assoc($q)) {
                if($Leavedata['leave_id']>0) {
                    if ($Leavedata['leave_day_type']==0) {
                        $leaveAppend = " <span class='badge badge-danger'>Full Day Leave</span>";
                    } else {
                        $leaveAppend = " <span class='badge badge-danger'>Half Day Leave</span>";
                    }
                    $date = $Leavedata['attendance_date_start']." (".date('l',strtotime($Leavedata['attendance_date_start'])).") ".$leaveAppend;
                } else {
                    $date = $Leavedata['attendance_date_start']." (".date('l',strtotime($Leavedata['attendance_date_start'])).") ";
                }
               
                array_push($mainData,  $date);
            }
        }
        if ($mainData) {
            $response["attendance_summary"] = $mainData;
            $response["message"] = "Attendance Summary";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Attendance Summary ";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if($_POST['action'] == "bulkSalaryChecked"){

    if ($status_action != '' && $salary_slip_id !="") {
      $a = array();
      if($status_action==1){
          $a['checked_by'] = $bms_admin_id;
          $a['salary_slip_status'] =1;
      }else if($status_action==2){
          $a['authorised_by'] = $bms_admin_id;
          $a['salary_slip_status'] =2;
          if($shareWtUser==1){
            $a['share_with_user'] =1;
          }

      }
      else
      {
          $a['share_with_user'] =1;
      }
      if(!empty($a)){
          $q = $d->update("salary_slip_master", $a, "salary_slip_id ='$salary_slip_id'");
          if ($q) {
              if($shareWtUser==1){
                  $q3 = $d->selectRow('*',"salary_slip_master","salary_slip_id='$salary_slip_id'");
                  $data3 = mysqli_fetch_assoc($q3);
                  $salary_start_date = $data3['salary_start_date'];
                  $q2 = $d->selectRow('user_token,device',"users_master","user_id='$data3[user_id]'");
                  $data2 = mysqli_fetch_assoc($q2);

                    $title = "Salary Slip Published";
                    $salary_month_name = date('F', strtotime($salary_start_date));
                    $description = "For Month of $salary_month_name, $salary_year";
                    $menuClick = "Salary_Slip";
                    $image = "";
                    $activity = "Salary_Slip";

                  $user_token = $data2['user_token'];
                  if ($user_token!='') {
                      // code...
                  $device = $data2['device'];
                      if ($device=='android') {
                        
                        $v =   $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                      } else {
                          $v =  $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                      }
                  }
                      
              }
            if($status_action==1){
                 $response["message"] = "Checked";
            } else {
                $response["message"] = "Published";
            }
            $response["users"] = "";
            $response["status"] = "200";
            echo json_encode($response);
          } else {
              $response["message"] = "Failed";
              $response["status"] = "201";
              echo json_encode($response);
          }
      }else {
          $response["message"] = "All Required";
          $response["status"] = "201";
          echo json_encode($response);
      }

  } else {
      $response["message"] = "All Required";
      $response["status"] = "201";
      echo json_encode($response);
  }

}

 if($_POST['action'] == "bulkSalary"){
    // error_reporting(E_ALL);
  // ini_set('display_errors', '1');

    $expIdsAr  = array();
    

    $startDate = date("$salary_year-$salary_month-1");
    $month_end_date = date("Y-m-t", strtotime($startDate)); 
    $q1 = $d->selectRow('*', "block_master,floors_master,users_master LEFT JOIN salary ON salary.user_id = users_master.user_id AND is_delete=0 AND is_preivous_salary=0 AND gross_salary>0", "block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND block_master.block_id=floors_master.block_id AND users_master.user_id='$user_id' AND users_master.floor_id=floors_master.floor_id ");
    $data = mysqli_fetch_assoc($q1);
    $block_id = $data['block_id'];
    $floor_id = $data['floor_id'];
    $present_designation_name = $data['user_designation'];
    $present_branch_name = $data['block_name'];
    $present_department_name = $data['floor_name'];
    $salary_group_id = $data['salary_group_id']; 

   
    if($data){
       
        if($data['salary_id'] !=""){
            $salary_id = $data['salary_id'];
            if($data['salary_type']==0){
               $salary_type = "Fixed";
            }elseif ($data['salary_type']==1) {
               $salary_type = "Per-Day";
            }
            else
            {
               $salary_type = "Per-Hour";
            }
            if($data['salary_type']==0){
                $salary_type = "Fixed";
             }elseif ($data['salary_type']==1) {
                $salary_type = "Per-Day";
             } else {
                $salary_type = "Per-Hour";
             }
             $days = cal_days_in_month(CAL_GREGORIAN, $_GET['month'], $_GET['year']);
          
          
             $ch = curl_init();
                   curl_setopt($ch, CURLOPT_URL,$base_url."residentApiNew/monthAttendance.php");
                   curl_setopt($ch, CURLOPT_POST, 1);
                   curl_setopt($ch, CURLOPT_POSTFIELDS,
                               "getMonthlyAttendanceHistoryNew=getMonthlyAttendanceHistoryNew&society_id=$society_id&user_id=$user_id&month_end_date=$month_end_date&month_start_date=$startDate");
          
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          
             $server_output = curl_exec($ch);
            
                   curl_close ($ch);
          
             $server_output=json_decode($server_output,true);
            // print_r('$server_output');
             $totalPresent = $server_output['total_present_days'];
            // print_r('$totalPresent');
             // print_r($server_output);
            $toatlWorkingHours = $server_output['total_month_hours'];
            $unpaid_leave_hours = $server_output['unpaid_leave_hours'];
            //$totalworkingdays = $server_output['total_working_days'];
            $totalworkingdays = $days;
            $tHours = $server_output['total_month_hour_spent'];
            $totalExtraPresent = $server_output['total_extra_days'];
            $extra_days = $server_output['total_extra_days'];
            $holidayWeekDate_temp = $server_output['total_holidays'];
            $total_holiday_extra_hrs = $server_output['total_holiday_extra_hrs'];
            $total_holiday_hrs = $server_output['total_holiday_hrs'];
            $week_off_hours = $server_output['week_off_hours'];
            $week_off_extra_hours = $server_output['week_off_extra_hours'];
            $total_leave = $server_output['total_leave'];
            $extraWorkingHoursTemp = ($server_output['total_extra_minutes']/60);
            $shift_weekoff_date_temp = $server_output['total_week_off'];
            $paid_half_day_leave = $server_output['paid_half_day_leave'];
            $total_paid_leave_count = $server_output['paid_leave'];
            $absent= $server_output['total_absent'];
            $un_paid_leave= $server_output['un_paid_leave'];
            $un_paid_half_day_leave= $server_output['un_paid_half_day_leave'];
            $total_holiday_hrs = $total_holiday_hrs- $total_holiday_extra_hrs;
            $week_off_hours = $week_off_hours- $week_off_extra_hours;
            $weekAry = array();
            $currdt= strtotime($startDate);
            $nextmonth=strtotime($startDate."+1 month");
            $i=0;
            $totalMonthHours = 0;
            $days = cal_days_in_month(CAL_GREGORIAN, $salary_month, $salary_year);

           
                  /////////////////emp working days calculation
                  $arrayEarning= array();
                  $arraDedicution= array();
                  if(isset($user_id) && isset($salary_month))
                  {
                  
                    /* $qs = $d->selectRow('salary.*,salary_group_master.working_day_calculation,salary_group_master.hourly_salary_extra_hours_payout','salary,salary_group_master',"salary.user_id ='$user_id' AND salary.is_preivous_salary=0 AND salary.is_delete=0 AND salary.gross_salary>0 AND salary_group_master.salary_group_id=salary.salary_group_id"); */
                    $qs = $d->selectRow('salary.*,salary_group_master.working_day_calculation,salary_group_master.hourly_salary_extra_hours_payout,salary_group_master.extra_day_payout_type_per_day,salary_group_master.allow_extra_day_payout_per_day,salary_group_master.fixed_amount_for_per_day,salary_group_master.allow_extra_day_payout_per_hour,salary_group_master.extra_day_payout_type_per_hour,salary_group_master.fixed_amount_for_per_hour,salary_group_master.allow_extra_day_payout_fixed,salary_group_master.extra_day_payout_type_fixed,salary_group_master.fixed_amount_for_fixed,salary_group_master.extra_hour_calculation_minutes,salary_group_master.extra_hour_calculation','salary,salary_group_master',"salary.user_id ='$user_id' AND  salary.gross_salary>0 AND salary_start_date<='$startDate' AND salary_group_master.salary_group_id=salary.salary_group_id","ORDER BY salary_id DESC LIMIT 1");
                    $salaryData =mysqli_fetch_array($qs);
             
                    if ($salaryData['working_day_calculation']==1) {
                        // 1 for (- weekoff & + holidays)
                        $totalworkingdays = $days - $shift_weekoff_date_temp;
                        $totalworkingdays = $totalworkingdays;
                        $total_paid_holiday = $holidayWeekDate_temp; 
                        $total_paid_week_off = "0";
                    } else if ($salaryData['working_day_calculation']==2) {
                        
                        // 2 for (+ weekoff & + holidays) 
                        $totalworkingdays = $days;
                        $total_paid_holiday = $holidayWeekDate_temp; 
                        $total_paid_week_off = $shift_weekoff_date_temp;
                    } else {
                        // 0 for (- weekoff & - holidays),
                        $totalworkingdays = $days - $shift_weekoff_date_temp;
                        $totalworkingdays = $totalworkingdays - $holidayWeekDate_temp;
                        $total_paid_holiday = "0"; 
                        $total_paid_week_off = "0";
                    }

                    /* salary calculation for per day and monthlly */
                    $flatAmount = $salaryData['total_fixed_amount'];////10000
                    $gross_salary = $salaryData['gross_salary']-$flatAmount;////10000
                    $perDaySalary = (float)$gross_salary/((float)$totalworkingdays);///  526.31578947368
                    $salary_start_date = $salaryData['salary_start_date']; 
                    $salary_group_id = $salaryData['salary_group_id']; 
                    $perHourSalary = (float)$gross_salary/((float)$toatlWorkingHours);
              
              
                    if($total_paid_leave_count !="" && $total_paid_leave_count >0){
                        
                        $total_paid_leave_sal = $total_paid_leave_count*$perDaySalary;
                    }
                    $total_unpaid_leave_count =  $absent+COUNT($un_paid_leave)+COUNT($un_paid_half_day_leave);  

                    if ($total_unpaid_leave_count <0) {
                        $total_unpaid_leave_count  =0;
                    }

                    if($data['salary_type']==0){
                        // $MonthNetSalary  = (float)$salaryData['gross_salary'];
                        $MonthNetSalary  = (float)$salaryData['gross_salary']-(float)$salaryData['total_fixed_amount'];
                        if($totalExtraPresent>0){
                            // $totalPresent  = $totalPresent -$totalExtraPresent;
                            if($salaryData){
                                if($salaryData['allow_extra_day_payout_fixed']=="1" && $salaryData['extra_day_payout_type_fixed']=="1"){
                                    
                                    if($salaryData['hourly_salary_extra_hours_payout']>0){
            
                                    $extraPerDaySalary=((float)$salaryData['fixed_amount_for_fixed'])*$salaryData['hourly_salary_extra_hours_payout'];
                                }else{
                                    $extraPerDaySalary=$salaryData['fixed_amount_for_fixed'];
                                }
                                }else if($salaryData['allow_extra_day_payout_fixed']=="1"){
        
                                    if($salaryData['hourly_salary_extra_hours_payout']>0){
            
                                    $extraPerDaySalary=((float)$perDaySalary)*$salaryData['hourly_salary_extra_hours_payout'];
                                    }
                                }
                                $extrDaysSalary = $extraPerDaySalary*$totalExtraPresent;
                            ///  $MonthNetSalary = ((float)$perDaySalary)*((float)($totalPresent)-$totalExtraPresent);
                                // $MonthNetSalary = $extrDaysSalary+$MonthNetSalary;
                                
                            }else
                            {
                                /// $MonthNetSalary = ((float)$perDaySalary)*(float)($totalPresent);
                            }
                        }

                    } else if($data['salary_type']==1) {
                        // per day salay
                        if($totalExtraPresent>0){
                            // $totalPresent  = $totalPresent -$totalExtraPresent;
                            if($salaryData){
                                if($salaryData['allow_extra_day_payout_per_day']=="1" && $salaryData['extra_day_payout_type_per_day']=="1"){
                                if($salaryData['hourly_salary_extra_hours_payout']>0){
            
                                    $extraPerDaySalary=((float)$salaryData['fixed_amount_for_per_day'])*$salaryData['hourly_salary_extra_hours_payout'];
                                }else{

                                    $extraPerDaySalary=$salaryData['fixed_amount_for_per_day'];
                                }
                                }else if($salaryData['allow_extra_day_payout_per_day']=="1"){

                                if($salaryData['hourly_salary_extra_hours_payout']>0){
        
                                    $extraPerDaySalary=((float)$perDaySalary)*$salaryData['hourly_salary_extra_hours_payout'];
                                }
                                }
                                $extrDaysSalary = $extraPerDaySalary*$totalExtraPresent;
                                $MonthNetSalary = ((float)$perDaySalary)*((float)($totalPresent));
                            
                            }else
                            {
                                
                                $MonthNetSalary = ((float)$perDaySalary)*(float)($totalPresent);
                            }
                        }else
                        {
                            $MonthNetSalary = ((float)$perDaySalary)*(float)($totalPresent);
                        
                        }
                        $MonthNetSalary  = (float)$MonthNetSalary;
                    } else {
                        // per hour salary
                        $extraWorkingHoursAvgTemp = 0;
                        $extraWorkingHoursDayByDayTemp = 0;

                        if($tHours>$toatlWorkingHours){
                            $extraWorkingHoursAvgTemp =  $tHours-$toatlWorkingHours;
                        }
                        $extraWorkingHoursDayByDayTemp = $extraWorkingHoursTemp;
                        
                        
                        if($extraWorkingHoursDayByDayTemp>0 || $extraWorkingHoursAvgTemp>0 ){
                            if($salaryData){
                                if($salaryData['extra_hour_calculation']=="1"){
                                if($salaryData['extra_hour_calculation_minutes']>0){
                                    if($salaryData['extra_hour_calculation_minutes']>=$extraWorkingHoursDayByDayTemp){
                                        $extraWorkingHoursTemp =0;
                                    }
                                }else{

                                    $extraWorkingHoursTemp = $extraWorkingHoursDayByDayTemp;
                                }
                                }else{
                                $extraWorkingHoursTemp = $extraWorkingHoursAvgTemp;    
                                }
                            
                                if($salaryData['allow_extra_day_payout_per_hour']=="1" && $salaryData['extra_day_payout_type_per_hour']=="1"){
                                if($salaryData['hourly_salary_extra_hours_payout']>0){
                                    // if($salaryData['extra_hour_calculation'])
                                    $extra_hour_day_salary=((float)$salaryData['fixed_amount_for_per_hour'])*$salaryData['hourly_salary_extra_hours_payout'];
                                }else{
                                    $extra_hour_day_salary=$salaryData['fixed_amount_for_per_hour'];
                                }

                                }else if($salaryData['allow_extra_day_payout_per_hour']=="1"){

                                if($salaryData['hourly_salary_extra_hours_payout']>0){
                                    $extra_hour_day_salary=((float)$perHourSalary)*$salaryData['hourly_salary_extra_hours_payout'];
                                }
                                }
                               
                                $extraWorkingHoursTemp =$extraWorkingHoursTemp+$total_holiday_extra_hrs+$week_off_extra_hours;
                                $extrDaysSalary = $extra_hour_day_salary*$extraWorkingHoursTemp;
                                $MonthNetSalary = ((float)$perHourSalary)*((float)($tHours));
                                
                            // echo $tHours;
                            /// $MonthNetSalary = $extrDaysSalary+$MonthNetSalary;
                                
                            }else
                            {
                                $MonthNetSalary = ((float)$perHourSalary)*(float)($tHours);
                            }
                        }else
                        {
                            $MonthNetSalary = ((float)$perHourSalary)*(float)($tHours);
                        }
                        $MonthNetSalary  = (float)$MonthNetSalary;
                    }
              
              

                    if ($salaryData['working_day_calculation']==1 && $totalPresent>0) {
                        // 1 for (- weekoff & + holidays)
                        $totalPaidWeekOff = "0";
                        $totalPaidHoliday = $holidayWeekDate_temp*$perDaySalary;
                    } else if ($salaryData['working_day_calculation']==2 && $totalPresent>0) {
                        // 2 for (+ weekoff & + holidays) 
                        $totalPaidWeekOff = $shift_weekoff_date_temp*$perDaySalary;
                        $totalPaidHoliday = $holidayWeekDate_temp*$perDaySalary;
                    } else {
                        $totalPaidWeekOff = "0";
                        $totalPaidHoliday = "0";
                    }

                      /* salary calculation */
                     if($salaryData)
                     {
        
                                   
                        $q=$d->selectRow("salary_master.*,salary_earning_deduction_type_master.earning_deduction_type,salary_earning_deduction_type_master.earning_deduction_name","salary_master LEFT JOIN salary_earning_deduction_type_master ON salary_master.salary_earning_deduction_id =salary_earning_deduction_type_master.salary_earning_deduction_id","salary_master.salary_id='$salaryData[salary_id]'","GROUP BY salary_master.salary_master_id");                   
                    }
        
                  }
                  

                  $counter = 1;
                  $totalEarns = 0;
                  $totalContribution = 0;
                  $totalEarn = 0;
                  $totalDeductAmnt = 0;
                  $totalDAr = array();
                  $totalDeduct = array();
                  $salaryDeductForPrecent = array();
                  $MonthNetSalary +=$total_paid_leave_sal;
                  $MonthNetSalary +=$totalPaidWeekOff;
                  $MonthNetSalary +=$totalPaidHoliday;
                  $earningArray["earning"] = array();
                  $earningArray["deduction"] = array();
                  
                  if (isset($q) && mysqli_num_rows($q)>0) {
            
                    while ($data3=mysqli_fetch_assoc($q)) {
                       
                      
                       $asalComn = $d->selectRow('salary_common_value_master.salary_common_value_id,salary_common_value_master.salary_earning_deduction_id,salary_common_value_master.salary_common_value_remark,salary_common_value_master.amount_value,salary_common_value_master.slab_json,salary_common_value_master.salary_common_value_earn_deduction,salary_common_value_master.percent_deduction_max_amount,salary_common_value_master.percent_min_max'," salary_common_value_master", "salary_common_value_master.salary_earning_deduction_id = '$data3[salary_earning_deduction_id]' AND salary_common_value_master.salary_group_id='$salary_group_id'");
                       $commnVal = mysqli_fetch_assoc($asalComn);
                       $data3['salary_common_value_id'] =$commnVal['salary_common_value_id'];
                       $data3['percent_min_max'] =$commnVal['percent_min_max'];
                       $data3['percent_deduction_max_amount'] =$commnVal['percent_deduction_max_amount'];
                       $data3['salary_earning_deduction_id'] =$commnVal['salary_earning_deduction_id'];
                       $data3['salary_common_value_remark'] =$commnVal['salary_common_value_remark'];
                       $data3['slab_json'] =$commnVal['slab_json'];
                       $data3['amount_value'] =$commnVal['amount_value'];
                       $data3['salary_common_value_earn_deduction'] =$commnVal['salary_common_value_earn_deduction'];
                       // if($data3['earning_deduction_type']=='0' && $data3['amount_type']=="1"){
                          
                       //    $flatAmount = $flatAmount + $data3['amount_value'];
                       //    $MonthNetSalary = $MonthNetSalary-$flatAmount;
                       // }
                      ////////////////Earning calculation
                       if($data3['earning_deduction_type']==0)
                       { 
                         
                          $earning = array();
                          if ($data3['amount_type']==1) {
                             $Deamnt = $data3['salary_earning_deduction_value'];
                          } else {
                            // $Deamnt =((float)$MonthNetSalary*(float)$data3['amount_percentage'])/100;
                            if($data3['salary_common_value_earn_deduction'] !=""){
                             $ernOnErnIdsAr = explode(',',$data3['salary_common_value_earn_deduction']);
                             $baseAmount = 0;
                             foreach ($ernOnErnIdsAr as $u => $n) {
                               
                                $getSub = $d->selectRow('salary_master.*,salary.salary_group_id,salary_common_value_master.amount_value AS salary_common_amount_value','salary_master LEFT JOIN salary ON  salary.salary_id = salary_master.salary_id
                                LEFT JOIN salary_common_value_master ON  salary_common_value_master.salary_group_id = salary.salary_group_id',"salary_master.salary_id=$data3[salary_id] AND salary_master.salary_earning_deduction_id =$n AND salary_common_value_master.salary_earning_deduction_id=$n");
                                $getSubData =mysqli_fetch_assoc($getSub);
                                $baseAmount = (float)$baseAmount+((float)$MonthNetSalary*(float)$getSubData['salary_common_amount_value'])/100;
                             }
                             $Deamnt = ((float)$baseAmount*(float)$data3['amount_value'])/100;
                            }else{
     
                               $Deamnt =((float)$MonthNetSalary*(float)$data3['amount_value'])/100;
                            }
                          }
                          $basicArray = array('salary_earning_deduction_id'=>$data3['salary_earning_deduction_id'],'amount'=>$Deamnt);
                          $salaryDeductForPrecent[$data3['salary_earning_deduction_id']]= $Deamnt;
                          $data3['amount']=$Deamnt;
                          $totalEarn +=(float)$Deamnt;
     
                          $earning["amount"] = $Deamnt;
                          $earning["salary_earning_deduction_id"] = $data3['salary_earning_deduction_id'];
                          $earning["earning_percentage"] = number_format((float)$data3['amount_percentage'], 2, '.', '');;
                          $earning["earning_percentage_show"] = number_format((float)$data3['amount_value'], 2, '.', '');
                          $earning["earning_deduction_name"] = $data3['earning_deduction_name'];
                          $earning["salary_common_value_remark"] = $data3['salary_common_value_remark'];
                          $earning["amount_value"] = $data3['amount_value'];
                          $earning["amount_type"] = $data3['amount_type'];
                          if($data3['salary_common_value_earn_deduction'] !=""){
                             $subEarnValue = $d->selectRow('GROUP_CONCAT(earning_deduction_name) AS earning_deduction_name','salary_earning_deduction_type_master',"salary_earning_deduction_id IN ($data3[salary_common_value_earn_deduction])");
     
                             $subEarnValueData =  mysqli_fetch_assoc($subEarnValue);
                            $earning["earn_show_name"] = $subEarnValueData['earning_deduction_name'];
                          }else{
                             $earning["earn_show_name"] = '';
                          }
     
                          array_push($earningArray["earning"], $earning);
                           array_push($arrayEarning,$data3);
                       }
                       else
                       {
                          $deduction = array();
                          //////////////amount_type =1 flat,0 percent
                          $totalDeducts=0;
                          if($data3['amount_type']==1)
                          {
                             $totalDeducts = $data3['salary_earning_deduction_value'];
                             $data3['amount']=$totalDeducts;
                             $data3['contribution_amount']=$data3['contribution_amount'];
                             $contribution_amount=$data3['contribution_amount'];
                             $data3['contribution_precent']=$data3['contribution_precent'];
                             $showNameForApply = "";
                             $totalContribution = $totalContribution +$contribution_amount;
                             $contri[$data3['salary_earning_deduction_id']]=$contribution_amount;
                             array_push($totalDeduct,(float)$totalDeducts);
                          } else if($data3['amount_type']==0) {
                             
                             $tempMultiDeductionArray = array();
                             $dedecution_precent = $data3['amount_percentage'];
                             $salary_common_value_id=$data3['salary_common_value_id'];
                             $salary_common_value_earn_deduction=$data3['salary_common_value_earn_deduction'];
                            
                            if($data3['salary_common_value_earn_deduction'] !=""){
                             $ernOnErnIdsArNew = explode(',',$data3['salary_common_value_earn_deduction']);
                             $contributionEarning = 0;
                             
                                $totalEarnContri = 0;
                                for ($newCOntri=0; $newCOntri <COUNT($earningArray['earning']) ; $newCOntri++) { 
                                   $kContri = array_search($ernOnErnIdsArNew[$newCOntri], array_column($earningArray['earning'], 'salary_earning_deduction_id'));
                                  
                                   if ($kContri === 0 || $kContri > 0) {
     
                                      $totalEarnContri += $earningArray['earning'][$kContri]['amount'];
                                   }
                                }
                                $contributionEarning = (float)$contributionEarning+$totalEarnContri;
                                $contribution_amount = ((float)$contributionEarning*(float)$data3['contribution_precent'])/100;
                            }else{
                               $contribution_amount = $MonthNetSalary*$data3['contribution_precent']/100;
                            }
                           
                            $totalContribution = $totalContribution +$contribution_amount;
                            
                             $salaryCvED = explode(',',$salary_common_value_earn_deduction);
                             $tempValueDedcution = 0;
                            
                                $showNameForApply = array();
                                for ($iD=0; $iD <count($earningArray['earning']) ; $iD++) { 
                                  
                                   $ks = array_search($salaryCvED[$iD], array_column($earningArray['earning'], 'salary_earning_deduction_id'));
                                  
                                   if ($ks === 0 || $ks > 0) {
                                     
                                      $tempValueDedcution += $earningArray['earning'][$ks]['amount'];
                                      array_push($tempMultiDeductionArray,$salaryCvED[$iD]);
                                      array_push($showNameForApply,$earningArray['earning'][$ks]['earning_deduction_name']);
     
                                   }
                                }
                               
                            
                                $totalCombineDeduction = $tempValueDedcution;
                                $perstageDedcutionValue = $totalCombineDeduction *  $dedecution_precent /100;
                               
                             $x = 1;
                            
                          if($data3['percent_min_max'] !=""){
                             $percent_min_max = json_decode($data3['percent_min_max'],true);
                             if($gross_salary>=$percent_min_max['percent_min'] && $gross_salary<=$percent_min_max['percent_max']){
                                if($data3['percent_deduction_max_amount']>0){
                                   if($perstageDedcutionValue<= $data3['percent_deduction_max_amount'])
                                   {
                                      $totalDeducts = $perstageDedcutionValue;
                                   }else
                                   {
                                      $totalDeducts=$data3['percent_deduction_max_amount'];
                                   }
                                 }
                                   else
                                   {
                                      $totalDeducts=$perstageDedcutionValue;
                                   }
                                 if($data3['percent_deduction_max_amount']>0){
                                   if($contribution_amount<= $data3['percent_deduction_max_amount'])
                                   {
                                      $contribution_amount = $contribution_amount;
                                   }else
                                   {
                                      $contribution_amount=$data3['percent_deduction_max_amount'];
                                   }
                                 }
                                 else
                                {
                                   $contribution_amount=$contribution_amount;
                                }
                                $contri[$data3['salary_earning_deduction_id']]=$contribution_amount;
                             }else{
                                $totalDeducts = 0;
                                $contribution_amount = 0;
                             }
                          }else{
                            
                             if($data3['percent_deduction_max_amount']>0){
                               if($perstageDedcutionValue<= $data3['percent_deduction_max_amount'])
                               {
                                  $totalDeducts = $perstageDedcutionValue;
                               }else
                               {
                                  $totalDeducts=$data3['percent_deduction_max_amount'];
                               }
                             }
                               else
                               {
                                  $totalDeducts=$perstageDedcutionValue;
                               }
                             if($data3['percent_deduction_max_amount']>0){
                               if($contribution_amount<= $data3['percent_deduction_max_amount'])
                               {
                                  $contribution_amount = $contribution_amount;
                               }else
                               {
                                  $contribution_amount=$data3['percent_deduction_max_amount'];
                               }
                             }
                             else
                            {
                               $contribution_amount=$contribution_amount;
                            }
                          }
                          $contri[$data3['salary_earning_deduction_id']]=$contribution_amount;
                          $data3['contribution_amount']=$contribution_amount;
                             array_push($totalDeduct,(float)$totalDeducts);
                           
                          }
                          else
                          {
                             /////////////slab calculation (deduction)
                            
                             $slabArray = explode('~',$data3['slab_json']);
     
     
                             $tempMultiDeductionArray = array();
                             $dedecution_precent = $data3['amount_percentage'];
                             $salary_common_value_id=$data3['salary_common_value_id'];
                             $salary_common_value_earn_deduction=$data3['salary_common_value_earn_deduction'];
                             $salaryCvED = explode(',',$salary_common_value_earn_deduction);
                            
                             $tempValueDedcution = 0;
                                $showNameForApply = array();
                                for ($iD=0; $iD <count($earningArray['earning']) ; $iD++) { 
                                   
                                   $ks = array_search($salaryCvED[$iD], array_column($earningArray['earning'], 'salary_earning_deduction_id'));
                                   if ($ks === 0 || $ks > 0) {
                                  
                                      $tempValueDedcution += $earningArray['earning'][$ks]['amount'];
                                      array_push($tempMultiDeductionArray,$salaryCvED[$iD]);
                                      array_push($showNameForApply,$earningArray['earning'][$ks]['earning_deduction_name']);
     
                                   }
                                }
                               
                            $totalCombineDeduction = $tempValueDedcution;
                             
                            $perstageDedcutionValue = $totalCombineDeduction;
                             $x = 1;
                             $totalDeducts = $perstageDedcutionValue;
     
                            for ($k=0; $k <count($slabArray) ; $k++) { 
                               
                                $slabDecodedJson = json_decode($slabArray[$k],true);
                              
                               if((float)$slabDecodedJson['min']<=$totalDeducts &&(float)$slabDecodedJson['max']>=$totalDeducts){
     
                                  $totalDeducts = $slabDecodedJson['value'];
                                  
                                  array_push($totalDeduct,(float)$totalDeducts);
                                  
                               } else {
                                  $totalDeducts = 0;
                               }
                             }
                             
                          }
                         
                          $deduction["amount"] = $totalDeducts;
                          $deduction["show_name_apply_form"] = $showNameForApply;
                          $deduction["contribution_precent"] = $data3['contribution_precent'];
                          $deduction["salary_earning_deduction_id"] = $data3['salary_earning_deduction_id'];
                          $deduction["contribution_amount"] = $data3['contribution_amount'];
                          $deduction["earning_deduction_name"] = $data3['earning_deduction_name'];
                          $deduction["deduction_percentage"] = number_format((float)$data3['amount_percentage'], 2, '.', '');
                          $deduction["salary_common_value_remark"] = $data3['salary_common_value_remark'];
                          $deduction["amount_value"] = $data3['amount_value'];
                          $deduction["amount_type"] = $data3['amount_type'];
                          array_push($earningArray["deduction"], $deduction);
                          array_push($arraDedicution,$data3);
                       }
                       
                    }
                 }
                
                $earnDedArray = array_merge($earningArray["deduction"],$earningArray["earning"]);
              
              if($data['salary_type']==0){
               $final_emp_salary =  $totalEarn -array_sum($totalDeduct);
              }
              else if($data['salary_type']==2)
              {
               $final_emp_salary =$totalEarn- array_sum($totalDeduct);
              }
              else
              {
               $final_emp_salary = $totalEarn -array_sum($totalDeduct);
              }
            
          
        
            $salary_start_date = date("$salary_year-$salary_month-01"); // hard-coded '01' for first day
            $salary_end_date =   date('Y-m-t', strtotime($salary_start_date));
           
            $user = $d->selectRow("unit_id", "users_master", "user_id='$user_id'");
            $maData = mysqli_fetch_array($user);
            $checkSalary = $d->selectRow('*', "salary", " salary_id = '$salary_id' ");
            $checkSalaryData = mysqli_fetch_assoc($checkSalary);
            $attendaceArrayData = array();
            $totalDe=  array_sum($totalDeduct);
             // print_r($earnDedArray);die;
            // $tHours = $tHours-$tHoursExtra;
            foreach ($earnDedArray as $key => $val) {
                $valTemp =  number_format($val['amount'],2,'.','');
                $keyTemp = $val['salary_earning_deduction_id'];
                $questionTypeValue[$keyTemp] = $valTemp;
            }
            if(isset($contri) && !empty($contri)){

                foreach ($contri as $keys => $vals) {
                    $contriValue[$keys] = $vals;
                }
            }
            $salary_json = !empty($questionTypeValue)?json_encode($questionTypeValue):'';
            $contri_json = !empty($contriValue)?json_encode($contriValue):'';

           /// print_R( $contriValue);die;
            if ($reimbursement==1) {
                $expenseAmount = array();
                $expQ = $d->selectRow('user_expenses.*','user_expenses',"user_expenses.user_id='$user_id' AND user_expenses.expense_status=1 AND user_expenses.expense_paid_status=0 AND user_expenses.date  BETWEEN '$salary_start_date' AND '$salary_end_date'");
                    while ($expanceData= mysqli_fetch_array($expQ)) {
                        array_push($expIdsAr,$expanceData['user_expense_id']);
                        array_push($expenseAmount,$expanceData['amount']);
                    }

                    $expense_amount = array_sum($expenseAmount);

                    $expData = implode(",",$expIdsAr);
                    $salary_month_name = date('F-Y', strtotime($salary_start_date));
                    $expaneHistryData = array('expense_id'=>$expData,
                                                'user_id'=>$user_id,
                                                'amount'=>$expense_amount,
                                                'expense_payment_mode'=>"Settled in $salary_month_name Salary",
                                                'society_id'=>$society_id,
                                                'remark'=> "Reimbursed Settlement in salary of $salary_month_name" 
                                             );
                        
                    $expaneHistryData['paid_by_type']=0;
                    $expaneHistryData['paid_by']=$bms_admin_id;       
                    
                    
                    
                    $final_emp_salary = $final_emp_salary+$expense_amount;
                    
            }

            if ($advance_salary==1) {

                    $advanceQ = $d->selectRow("advance_salary.*,(SELECT SUM(advance_salary_amount) FROM `advance_salary` WHERE advance_salary_date BETWEEN '$startDate' AND '$month_end_date' AND user_id = $user_id AND is_paid=0) AS advance_amount",'advance_salary',"advance_salary.user_id=$user_id AND advance_salary.is_paid=0 AND  advance_salary.advance_salary_date  BETWEEN '$startDate' AND '$month_end_date'");

                    $advanceData = mysqli_fetch_assoc($advanceQ);
              
                    $advanceAmount  = $advanceData['advance_amount'];
                    if($final_emp_salary>$advanceAmount){
                      $advanceAmount = $advanceAmount;
                    }else{
                      $advanceAmount = 0;
                    }

                    $crMonthYr =date('Y-m',strtotime($salary_start_date));
                    $advanceAmount;

                        
                    if ($advanceAmount<$final_emp_salary) {
                        $final_emp_salary = $final_emp_salary-$advanceAmount;
                        $totalDe = $totalDe + $advanceAmount;
                    } else {
                        $advanceAmount = 0;
                    }

            }

            if ($loan_emi==1) {

                $loan_emi_ids = array();
                $loadAmount = array();
                $expQ = $d->selectRow('loan_emi_master.*','loan_emi_master',"loan_emi_master.user_id='$user_id'  AND loan_emi_master.is_emi_paid=0 AND loan_emi_master.emi_created_at  BETWEEN '$salary_start_date' AND '$salary_end_date'");
                    while ($loanData= mysqli_fetch_array($expQ)) {
                        array_push($loan_emi_ids,$loanData['loan_emi_id']);
                        array_push($loadAmount,$loanData['emi_amount']);
                    }

                    $emi_deduction = array_sum($loadAmount);

                    if ($emi_deduction<$final_emp_salary) {
                        $final_emp_salary = $final_emp_salary-$emi_deduction;
                        $totalDe = $totalDe + $emi_deduction;
                    } else {
                        $emi_deduction = 0;
                    }

            }
        
            $m->set_data('present_branch_name', $present_branch_name);
            $m->set_data('present_department_name', $present_department_name);
            $m->set_data('present_designation_name', $present_designation_name);

            $a1 = array(
                'society_id' => $society_id,
                'salary_id' => $salary_id,
                'salary_type_current' => $checkSalaryData['salary_type'],
                'user_id' => $user_id,
                'floor_id' => $floor_id,
                'block_id' => $block_id,
                'present_branch_name'=>$m->get_data('present_branch_name'),
                'present_department_name'=>$m->get_data('present_department_name'),
                'present_designation_name'=>$m->get_data('present_designation_name'),
                'salary_start_date' => $salary_start_date,
                'extra_days_per_day_salary' => $extra_days_per_day_salary,
                'extra_per_hour_salary' => $extra_hour_day_salary,
                'expense_amount' => $expense_amount,
                'emi_deduction' => $emi_deduction,
                'advance_salary_paid_amount' => $advanceAmount,
                'other_deduction' => 0,
                'salary_end_date' => $salary_end_date,
                'salary_month_name' => ($salary_month . "-" . $salary_year),
                'total_deduction_salary' => $totalDe,
                'total_earning_salary' => $totalEarn,
                'month_net_salary' => $totalEarn-$flatAmount,
                'total_net_salary' => $final_emp_salary,
                'total_ctc_cost' => $final_emp_salary+$totalContribution,
                'other_earning' => 0,
                'salary_mode_description' => "",
                'salary_mode' =>$salary_mode,
                'total_month_days' => $totalworkingdays,
                'leave_days' => $total_paid_leave_count+$total_unpaid_leave_count,
                'unpaid_leave_days' => $total_unpaid_leave_count,
                'paid_leave_days' => $total_paid_leave_count,
                'paid_week_off' => $total_paid_week_off,
                'paid_holidays' => $total_paid_holiday,
                'total_working_days' => $totalPresent,
                'extra_days' => $extra_days,
                'prepared_by' => $bms_admin_id,
                'created_date' => date('Y-m-d H:i:s'),
                'total_working_hours'=>$tHours,
                'total_month_hours'=>$toatlWorkingHours,
                'per_hour_salary'=>$perHourSalary,
                'per_day_salary'=>$perDaySalary,
                'extra_hours'=>$extraWorkingHoursTemp,
                'is_bulk_generated'=>1,
                'salary_json'=>$salary_json,
                'contribution_json'=>$contri_json,
            );

          //// print_r($a1);die;
            if($share_with_user==1){
                $a1['share_with_user'] = 1; 
            }
            if($status_action==0){
                $a1['prepared_by'] = $bms_admin_id;
            }else if($status_action==1){
                
                $a1['checked_by'] = $bms_admin_id;
                $a1['salary_slip_status'] = 1;
                $a1['prepared_by'] = $bms_admin_id;
            }else{
                $a1['checked_by'] = $bms_admin_id;
                $a1['prepared_by'] = $bms_admin_id;
                $a1['salary_slip_status'] = 2;
                $a1['authorised_by'] = $bms_admin_id;
            }
            
            if($final_emp_salary>0){

                $checkSalarySlip = $d->selectRow('*', 'salary_slip_master', "salary_start_date = '$salary_start_date' AND salary_end_date='$salary_end_date' AND user_id = '$user_id'");
                $checkSalarySlipData = mysqli_fetch_assoc($checkSalarySlip);
                
                if ($checkSalarySlipData) {
                    $response["message"] = "Salary Already Generated";
                    $response["users"] = "";
                    $response["status"] = "201";
                    echo json_encode($response);
                } else {
                    $q = $d->insert("salary_slip_master", $a1);
                    $salary_slip_id = $con->insert_id;

                    if ($reimbursement==1 && $q == true && $expense_amount>0) {

                        $UsrExpHistory = $d->insert("user_expense_history", $expaneHistryData);
                        $expiDs = join("','",$expIdsAr);

                        $q2 = $d->update("user_expenses",array('expense_paid_status'=>1), "user_expense_id IN ('$expiDs')");
                    }
                    if ($advance_salary==1  && $q == true) {
                        
                        $crMonthYr =date('Y-m',strtotime($salary_start_date));
                        $salary_month_name =date('M Y',strtotime($salary_start_date));
                        $advance_salary_paid_remark = "deducted from $salary_month_name month salary";
                        
                        $q2 = $d->update("advance_salary",array('is_paid'=>1,'paid_date'=>date('Y-m-d H:i'),'amount_receive_by'=>$bms_admin_id,'advance_salary_paid_remark'=>$advance_salary_paid_remark), "advance_salary_date BETWEEN '$salary_start_date' AND '$salary_end_date' AND user_id = $user_id");
                        
                        $q2 = $d->update("monthly_advance_salary",array('is_complete_paid'=>1,'paid_by'=>$bms_admin_id,'total_paid_amount'=>$advance_salary_paid_amount,'single_paid_date'=>date('Y-m-d H:i'),'single_paid_date_remark'=>$advance_salary_paid_remark), " month='$crMonthYr' AND user_id=$user_id");
                       
                    }

                    if ($loan_emi==1 && $emi_deduction>0) {
                        $emi_paid_remark = "deducted from $salary_month_name month salary";
                        $emiDs = join("','",$loan_emi_ids);
                        $q2 = $d->update("loan_emi_master",array('is_emi_paid'=>1,'emi_paid_by'=>$bms_admin_id,'emi_paid_remark'=>$emi_paid_remark,'received_date'=>date('Y-m-d H:i')), "loan_emi_id IN ('$emiDs') AND user_id='$user_id'");


                    }

                    if ($q == true) {
                        $salary_bulk_array=array(
                    
                        'salay_id'=> $salary_id ,
                        'user_id'=>$a1['user_id'],
                        'society_id'=>$society_id,
                        'salary_slip_id'=>$salary_slip_id,
                        'month'=>$salary_month,
                        'year'=>$salary_year,
                        'net_salary'=>$final_emp_salary,
                        'gross_Salary'=>$MonthNetSalary,
                        'salary_response'=>"Salary Addedd Success",
                        'created_date   '=>date('Y-m-d H:i:s'),);
        
                      /// $q4 = $d->insert("salary_bulk_generate_report", $salary_bulk_array);
                          
                            foreach ($earnDedArray as $key => $val) {
                             
                                
                                // if($val['amount_type']==0){
                                //     $amntPrcnt = $val['amount_value'];
                                // }else
                                // {
                                    $amntPrcnt  = $val['amount_value'];
                                //}
                                $employer_contribution_amount = 0;
                                if(isset($val['contribution_amount'])){
                                    $employer_contribution_amount =$val['contribution_amount']; 
                                }
                                $salarySlipSubValueData = array(
                                    'salary_earning_deduction_id' => $val['salary_earning_deduction_id'],
                                    'earning_deduction_name_current' => $val['earning_deduction_name'],
                                    'earning_deduction_type_current' => $val['amount_type'],
                                    'earning_deduction_amount' => $val['amount'],
                                    'employer_contribution_amount' => $employer_contribution_amount,
                                    'earning_deduction_parcent' => $amntPrcnt,
                                    'created_date' => date('Y-m-d H:i:s'),
                                    'salary_slip_id' => $salary_slip_id,
                                    'society_id' => $society_id,
                                    'user_id' => $a1['user_id'],
                                );
                                $checkSalarySub = $d->selectRow('*', "salary_slip_sub_master", " salary_slip_id = '$salary_slip_id' AND salary_earning_deduction_id = '$val[salary_earning_deduction_id]'");
                                $checkSalaryData = mysqli_fetch_assoc($checkSalarySub);
                                if ($checkSalaryData) {
                                    $q2 = $d->update("salary_slip_sub_master", $salarySlipSubValueData, "salary_slip_sub_id='$checkSalaryData[salary_slip_sub_id]'");
                                } else {
                                    $q2 = $d->insert("salary_slip_sub_master", $salarySlipSubValueData);
                                }
                            }
                        $q3 = $d->update("leave_master", array('is_salary_generated'=>1), "leave_master.user_id='$a1[user_id])'  AND DATE_FORMAT(leave_master.leave_start_date,'%c-%Y')='$a1[salary_month_name]' ");

                            $d->insert_log("", "$society_id", "$bms_admin_id", "$created_by", "Salary Slip ($salary_month - $salary_year) Generated for $data[user_full_name] - $data[floor_name] ($data[block_name])");

                            if($share_with_user==1 && $data['user_token']){
                                $title = "Salary Slip Published";
                                $salary_month_name = date('F', strtotime($salary_start_date));
                                $description = "For Month of $salary_month_name, $salary_year";
                                $menuClick = "Salary_Slip";
                                $image = "";
                                $activity = "Salary_Slip";
                                $user_token = $data['user_token'];
                                $device = $data['device'];
                                if ($device=='android') {
                                $v =   $nResident->noti($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                                } else {
                                  $v =  $nResident->noti_ios($menuClick,$image,$society_id,$user_token,$title,$description,$activity);
                                }

                                $notiAry = array(
                                    'society_id'=>$society_id,
                                    'user_id'=>$data['user_id'],
                                    'notification_title'=>$title,
                                    'notification_desc'=>$description,    
                                    'notification_date'=>date('Y-m-d H:i'),
                                    'notification_action'=>'Salary_Slip',
                                    'notification_logo'=>'payslip.png',
                                    'notification_type'=>'0',
                                );
                                
                                $d->insert("user_notification",$notiAry);

                            }

                            $response["message"] = "Salary Generated";
                            $response["users"] = "";
                            $response["status"] = "200";
                            echo json_encode($response);
                    
                    } else {
                        $salary_bulk_array=array(
                    
                            'salay_id'=> $salary_id ,
                            'user_id'=>$a1['user_id'],
                            'society_id'=>$society_id,
                            'salary_slip_id'=>$salary_slip_id,
                            'month'=>$salary_month,
                            'year'=>$salary_year,
                            'net_salary'=>0,
                            'gross_Salary'=>0,
                            'salary_response'=>"Salary Not Added",
                            'created_date   '=>date('Y-m-d H:i:s'),);
            
                        $q4 = $d->insert("salary_bulk_generate_report", $salary_bulk_array);
                        $response["message"] = "Salary Not Added ";
                        $response["users"] = "";
                        $response["status"] = "201";
                        echo json_encode($response);
                    }
                }
        
             }
            else
            {
                $salary_bulk_array=array(
                    
                    'salay_id'=> $salary_id ,
                    'user_id'=>$a1['user_id'],
                    'society_id'=>$society_id,
                    'salary_slip_id'=>0,
                    'month'=>$salary_month,
                    'year'=>$salary_year,
                    'net_salary'=>0,
                    'gross_Salary'=>0,
                    'salary_response'=>"Attendance Not Available",
                    'created_date   '=>date('Y-m-d H:i:s'),);
    
                $q4 = $d->insert("salary_bulk_generate_report", $salary_bulk_array);
                $response["message"] = "Attendance Not Available";
                $response["users"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }
        else
        {
            $salary_bulk_array=array(
                    
                'salay_id'=> 0 ,
                'user_id'=>$user_id,
                'society_id'=>$society_id,
                'salary_slip_id'=>0,
                'month'=>$salary_month,
                'year'=>$salary_year,
                'net_salary'=>0,
                'gross_Salary'=>0,
                'salary_response'=>"Defualt Salary Not Set",
                'created_date   '=>date('Y-m-d H:i:s'),);

            $q4 = $d->insert("salary_bulk_generate_report", $salary_bulk_array);
            ///////////////////////salary and salary rules not added for this person
            $response["message"] = "Defualt Salary Not Set ";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else {
        
            $salary_bulk_array=array(
                        
                'salay_id'=> 0 ,
                'user_id'=>$user_id,
                'society_id'=>$society_id,
                'salary_slip_id'=>0,
                'month'=>$salary_month,
                'year'=>$salary_year,
                'net_salary'=>0,
                'gross_Salary'=>0,
                'salary_response'=>"User Not Exist",
                'created_date   '=>date('Y-m-d H:i:s'),);

            $q4 = $d->insert("salary_bulk_generate_report", $salary_bulk_array);
            $response["message"] = "User Not Exist ";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
    }
}

  if ($_POST['action'] == "getCateogryByServiceProvider") {
    
    if ($vendor_id != '') {
        $q=$d->select('product_category_master',"vendor_id='$vendor_id' AND product_category_delete=0 AND product_category_status=0");
        $mainData =array();
        while ($data = mysqli_fetch_assoc($q)) {
          
            array_push($mainData, $data);
        }
        if ($mainData) {
            $response["category"] = $mainData;
            $response["message"] = "Category Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Category Data";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getHrDocSubCategory") {
    if ($hr_document_sub_category_id != '') {
        $q = $d->selectRow('*', "hr_document_sub_category_master","hr_document_sub_category_id='$hr_document_sub_category_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["hr_document_sub_category"] = $data;
            $response["message"] = "Hr Doc Sub Category";
            $response["status"] = "200";
            echo json_encode($response);

        } else {
            $response["message"] = "Hr Doc Sub Category";
            $response["status"] = "201";
            echo json_encode($response);

        }
    } else {
        $response["message"] = "Category Id Required";
        $response["status"] = "201";
        echo json_encode($response);

    }

}

if ($_POST['action'] == "getHRDocSubCategoryById") {
    $mainData = array();
    if ($hr_document_category_id != '') {
        $q = $d->select("hr_document_sub_category_master","hr_document_category_id='$hr_document_category_id'");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }
        if ($mainData) {
            $response["sub_category"] = $mainData;
            $response["message"] = "HR Doc Sub Category";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "HR Doc Sub Category";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getAccessTypeById") {
    if ($access_type_id != '') {
        $q = $d->selectRow('*', "access_type_master", "access_type_id='$access_type_id'");
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            $response["access_type"] = $data;
            $response["message"] = "Access Type";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Access Type";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "All Required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


if ($_POST['action'] == "getEmployeeAccessTypeById") {
    $mainData = array();
    if ($user_id != '') {
        $accessTypeIds = array();
        $q = $d->selectRow("access_by_id,access_type","app_access_master","access_by_id='$user_id'");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($accessTypeIds, $data['access_type']);
        }
        $accessTypeIds = join("','",$accessTypeIds); 
        $q1 = $d->select("access_type_master","access_type_id NOT IN ('$accessTypeIds')");
        while ($data01 = mysqli_fetch_assoc($q1)) {
            array_push($mainData, $data01);
        }
        if ($mainData) {
            $response["access_type"] = $mainData;
            $response["message"] = "Access Type";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Access Type";
            $response["access_type"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if($_POST['action']=="getLeaveDetails"){

    if ($leave_id != '') {
        $user = $d->selectRow("users_master.user_full_name,leave_master.*,attendance_master.attendance_id,attendance_master.attendance_date_start,attendance_master.attendance_date_end,attendance_master.punch_in_time,attendance_master.punch_out_time","users_master,leave_master LEFT JOIN attendance_master ON attendance_master.user_id=leave_master.user_id AND attendance_master.attendance_date_start=leave_master.leave_start_date","users_master.user_id=leave_master.user_id  AND leave_master.leave_id='$leave_id'");
        $maData = mysqli_fetch_assoc($user);
      
                $response["leave"] = "";
                $response["leave"] = $maData;

            
            $response["message"]="Leave";
            $response["status"]="200";
            echo json_encode($response);


    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }

}
/* Dollop  Infotech 03-Nov-2021 */

if ($_POST['action'] == "getFloorByBlockIdForAssignMultiDptBranch") {
    if ($block_id != '' && $user_id !="") {
        $mainData = array();
        $q = $d->select("floors_master,block_master", "block_master.block_id=floors_master.block_id AND floors_master.block_id='$block_id' $blockAppendQuery $blockAppendQueryFloor");
        while ($data = mysqli_fetch_assoc($q)) {
           $q2 = $d->select("employee_multiple_department_branch", "employee_multiple_department_branch.user_id=$user_id AND floor_id = '$data[floor_id]'");
           if(mysqli_num_rows($q2)<=0){
               array_push($mainData, $data);
           }                    
        }  
        if ($mainData) {
            $response["floor"] = $mainData;
            $response["message"] = "Floor Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Floor Data";
            $response["floor"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getBlockByUserForAssignMultiDptBranch") {
    if ( $user_id !="") {
        $mainData = array();
        $q = $d->select("block_master", "$blockAppendQuery");
        while ($data = mysqli_fetch_assoc($q)) {
           $q2 = $d->select("employee_multiple_department_branch", "employee_multiple_department_branch.user_id=$user_id AND employee_multiple_department_branch.branch_id= '$data[block_id]'");
           if( mysqli_num_rows($q2) >0){
               while ($data2 = mysqli_fetch_assoc($q2)) {
                   if(($data2['floor_id']>0)){
                       array_push($mainData, $data);
                   }
               }  
           }else
           {
            array_push($mainData, $data);
           }
        }  
        if ($mainData) {
            $response["floor"] = $mainData;
            $response["message"] = "Floor Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Floor Data";
            $response["floor"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if($_POST['action']=="getCategoryAndCheckVendorId"){
    if ($vendor_id !="") {
        $getVendorCat = $d->selectRow('product_category_id','product_category_vendor_master',"vendor_id=$vendor_id");
        $filter = "";
        $vIds = array();
        if(mysqli_num_rows($getVendorCat)>0){
            while($data=mysqli_fetch_assoc($getVendorCat)){
                if($data){
                    $vId  =$data['product_category_id'];
                    array_push($vIds,$vId);
                }
            }
            if(!empty($vIds)){
                $vendorIds = join("','",$vIds); 
                $filter = " AND product_category_id NOT IN ('$vendorIds');";
            }
        }
        $cat = $d->selectRow('product_category_master.*',"product_category_master","product_category_master.product_category_delete=0 AND product_category_master.product_category_delete=0 $filter");
    }
    else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if($_POST['action']=="getJoiningDateByUserId"){
    if ($user_id !="") {
        $q = $d->selectRow('*','user_employment_details',"user_id=$user_id");
        $data = mysqli_fetch_assoc($q);
        if($data){
            if($data['joining_date'] !="0000-00-00" && $data['joining_date'] !=""){
                $data['joining_date'] = $data['joining_date'];
            }else
            {
                $data['joining_date'] = "";
            }
            $response["message"] = "Success";
            $response["data"] = $data;
            $response["status"] = "200";
            echo json_encode($response);
        }
        else
        {
            $response["message"] = "Failed";
            $response["data"] = $data;
            $response["status"] = "200";
            echo json_encode($response);
        }
    }
    else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getAssetsItemByCategoryId") {
    $mainData = array();
    if ($assets_category_id != '') {
        $accessTypeIds = array();
        $q1 = $d->select("assets_item_detail_master","assets_category_id = '$assets_category_id'");
        while ($data01 = mysqli_fetch_assoc($q1)) {
            array_push($mainData, $data01);
        }
        if ($mainData) {
            $response["assets_item"] = $mainData;
            $response["message"] = "Assets Item";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Assets Item";
            $response["access_type"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getProductVariantById") {
    $mainData = array();
    if ($product_variant_id != '') {
        $accessTypeIds = array();
        $q1 = $d->selectRow("product_variant_master.*","product_variant_master","product_variant_id = '$product_variant_id'");
       $data01 = mysqli_fetch_assoc($q1) ;
        if ($data01) {
            $response["product_variant"] = $data01;
            $response["message"] = "Product Variant";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Product Variant";
            $response["access_type"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "addCommentOnPost") {
    $mainData = array();
    if ($msg != '' && $feed_id !="") {
        $cmnAr = array('feed_id'=>$feed_id,'msg'=>$msg,'user_id'=>$bms_admin_id,'society_id'=>$society_id);
        $uSql = $d->selectRow('bms_admin_master.*', "bms_admin_master", "admin_id=$bms_admin_id");
        $user = mysqli_fetch_assoc($uSql);
        if ($user) {
            $cmnAr['block_name'] = 'Admin';
            $cmnAr['user_type'] = '1';
            $cmnAr['user_name'] = $user['admin_name'];
        }
        $q = $d->insert("news_comments", $cmnAr);
        if ($q) {
            $response["message"] = " Post On Comment";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = " Post On Comment";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "addSubComment") {
    $mainData = array();
    if ($msg != '' && $comment_id !="" && $feed_id !="") {
        $cmnAr = array('feed_id'=>$feed_id,'msg'=>$msg,'user_id'=>$bms_admin_id,'society_id'=>$society_id,'parent_comments_id'=>$comment_id);
        $uSql = $d->selectRow('bms_admin_master.*', "bms_admin_master", "admin_id=$bms_admin_id");
        $user = mysqli_fetch_assoc($uSql);
        if ($user) {
            $cmnAr['block_name'] = 'Admin';
            $cmnAr['user_type'] = '1';
            $cmnAr['user_name'] = $user['admin_name'];
        }
        $q = $d->insert("news_comments", $cmnAr);
        if ($q) {
            $response["message"] = " Post On Comment";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = " Post On Comment";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getProductVariant") {
    $mainData = array();
   // print_r()
    if ( $product_id !="" ) {
       
        $pSql = $d->selectRow('product_variant_master.*', "product_variant_master", "product_id=$product_id AND variant_delete_status=0 AND variant_active_status=0");
      
        while ($pd = mysqli_fetch_assoc($pSql)) {
           array_push($mainData,$pd);
        }
        if (mysqli_num_rows($pSql)>0) {
            $response["message"] = " Variant Data";
            $response["status"] = "200";
            $response["data"] = $mainData;
            echo json_encode($response);
        } else {
            $response["message"] = " No Variant";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getProductPriceData") {
    $mainData = array();
   // print_r()
    if ($product_price_id !="" ) {
       
        $pSql = $d->selectRow("product_price_master.*,product_master.*,product_variant_master.product_variant_name,product_sub_category_master.sub_category_name,product_category_master.*","product_price_master LEFT JOIN product_variant_master ON product_variant_master.product_variant_id=product_price_master.product_variant_id,product_master LEFT JOIN product_sub_category_master ON product_sub_category_master.product_sub_category_id=product_master.product_sub_category_id ,product_category_master","product_master.product_id=product_price_master.product_id AND product_category_master.product_category_id=product_master.product_category_id AND product_price_id=$product_price_id ");
        $pd = mysqli_fetch_assoc($pSql);
        if (mysqli_num_rows($pSql)>0) {
            $response["message"] = " Variant Data";
            $response["status"] = "200";
            $response["data"] = $pd;
            echo json_encode($response);
        } else {
            $response["message"] = " No Variant";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if($_POST['action'] == "advanceSalaryById"){
   
    if ($advance_salary_id !="" ) {
        $q = $d->selectRow('paid_by_admin.admin_name AS paid_by_admin_name,advance_salary.*,users_master.*,floors_master.floor_id,floors_master.floor_name,block_master.block_id,block_master.block_name',"advance_salary LEFT JOIN bms_admin_master AS paid_by_admin ON paid_by_admin.admin_id= advance_salary.amount_receive_by,users_master LEFT JOIN floors_master ON floors_master.floor_id=users_master.floor_id LEFT JOIN block_master ON block_master.block_id=users_master.block_id"," users_master.user_id=advance_salary.user_id AND advance_salary.advance_salary_id=$advance_salary_id" );
        $data = mysqli_fetch_assoc($q);
        if (mysqli_num_rows($q)>0) {
            $data['cr_date']=date('d M Y',strtotime($data['created_date']));
            if ($data['is_paid']==1) {
                $data['paid_date']=date('d M Y h:i A',strtotime($data['paid_date']));
            }
            $response["message"] = "Advance Salary Data";
            $response["status"] = "200";
            $response["data"] = $data;
            echo json_encode($response);
        } else {
            $response["message"] = " Advance Salary";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else{
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if($_POST['action'] == "employee_history"){
   
    if ($history_id !="" ) {
        $q = $d->selectRow("employee_history.*,users_master.*,floors_master.floor_id,floors_master.floor_name,block_master.block_name,block_master.block_id","employee_history LEFT JOIN users_master ON users_master.user_id=employee_history.user_id LEFT JOIN floors_master ON  floors_master.floor_id=users_master.floor_id LEFT JOIN  block_master ON block_master.block_id=users_master.block_id","employee_history.history_id=$history_id" );
        $data = mysqli_fetch_assoc($q);
        if (mysqli_num_rows($q)>0) {
            $data['cr_date']=date('d-M-Y',strtotime($data['created_date']));
            $data['joining_date']=date('d-M-Y',strtotime($data['joining_date']));
            $data['end_date']=date('d-M-Y',strtotime($data['end_date']));
            $response["message"] = "Employee History Data";
            $response["status"] = "200";
            $response["data"] = $data;
            echo json_encode($response);
        } else {
            $response["message"] = " Employee History Data";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }
    else{
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getFloorByBlockIdForHr") {
   
    if (count($_POST['block_id'])>0) {
        if (is_array($_POST['block_id'])) {
            $block_id = join("','",$_POST['block_id']);
            $queryAppend = "  AND floors_master.block_id IN('$block_id')";
        } else {
            $queryAppend = "  AND floors_master.block_id='$block_id'";
        }
        $mainData = array();
        $q = $d->select("floors_master,block_master", "block_master.block_id=floors_master.block_id $queryAppend $blockAppendQuery $blockAppendQueryFloor");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }  
        if ($mainData) {
            $response["floor"] = $mainData;
            $response["message"] = "Floor Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Floor Data";
            $response["floor"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getUserByFloorForHrdoc") {
    $mainData = array();
    if (count($_POST['floor_id'])>0) {
        $floor_id = join("','",$_POST['floor_id']);
        $q = $d->select("users_master", " floor_id IN('$floor_id') AND delete_status=0 AND user_status=1 AND active_status=0 $blockAppendQueryUser");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }
        if ($mainData) {
            $response["users"] = $mainData;
            $response["message"] = "users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getUserByFloorForTrack") {
    $mainData = array();
    if ($floor_id != '') {
        $q = $d->select("users_master", "floor_id='$floor_id' AND delete_status=0 AND user_status=1 AND active_status=0  $blockAppendQueryUser AND track_user_location=0");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }
        if ($mainData) {
            $response["users"] = $mainData;
            $response["message"] = "users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getUserById") {
    $mainData = array();
    if ($user_id != '') {
        $q = $d->select("users_master", "user_id='$user_id'");
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            $data['track_user_time']=(int) (($data['track_user_time'] / (1000*60))); 
            $response["user"] = $data;
            $response["message"] = "user";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "user";
            $response["user"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "GetUserTrackDataById") {
    $mainData = array();
    if ($user_track_id != '') {
        $q = $d->selectRow("user_track_master.*,users_master.*,block_master.block_id,block_master.block_name,floors_master.floor_id,floors_master.floor_name","user_track_master LEFT JOIN users_master ON users_master.user_id=user_track_master.user_id LEFT JOIN floors_master ON users_master.floor_id=floors_master.floor_id LEFT JOIN block_master ON users_master.block_id=block_master.block_id","user_track_master.user_track_id=$user_track_id");
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            if($data['user_track_date'] !="0000-00-00 00:00:00"){
                $data['user_track_date'] = date('d-M-Y H:I A',strtotime($data['user_track_date'])); }
            $response["user"] = $data;
            $response["message"] = "user";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "user";
            $response["user"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "GetUserbackTrackDataByIdLastData") {
    $mainData = array();
    if ($user_id != '') {
        $q = $d->selectRow("users_master.*,block_master.block_id,block_master.block_name,floors_master.floor_id,floors_master.floor_name","users_master,floors_master,block_master","users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.user_id=$user_id");
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            if($data['last_tracking_location_time'] !="0000-00-00 00:00:00"){
                $data['last_tracking_location_time'] = date('d M Y h:i A',strtotime($data['last_tracking_location_time'])); }
            $response["user"] = $data;
            $response["message"] = "user";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "user";
            $response["user"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getGpsDataUser") {
    $mainData = array();
    if ($user_gps_on_off_id != '') {
         $q = $d->selectRow("user_gps_on_off.*","user_gps_on_off","user_gps_on_off_id='$user_gps_on_off_id'");
       
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            if($data['created_date'] !="0000-00-00 00:00:00"){
                $data['created_date'] = date('d M Y h:i A',strtotime($data['created_date'])); }
            $response["user"] = $data;
            $response["message"] = "user";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "user";
            $response["user"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "GetUserbackTrackDataById") {
    $mainData = array();
    if ($user_back_track_id != '') {
        $q = $d->selectRow("user_back_track_master.*,users_master.*,block_master.block_id,block_master.block_name,floors_master.floor_id,floors_master.floor_name","user_back_track_master LEFT JOIN users_master ON users_master.user_id=user_back_track_master.user_id LEFT JOIN floors_master ON users_master.floor_id=floors_master.floor_id LEFT JOIN block_master ON users_master.block_id=block_master.block_id","user_back_track_master.user_back_track_id=$user_back_track_id");
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            if($data['user_back_track_date'] !="0000-00-00 00:00:00"){
                $data['user_back_track_date'] = date('d M Y h:i A',strtotime($data['user_back_track_date'])); }
            $response["user"] = $data;
            $response["message"] = "user";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "user";
            $response["user"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "untrackeUser") {
    if ($track_user_location==1) {
       $track_user_time = "300000";
    } else {
       $track_user_time = "";
    }
    $mainData = array();
    if ($user_id != '') {
        $q = $d->update("users_master",array('track_user_time'=>$track_user_time,'track_user_location'=>$track_user_location) ,"user_id='$user_id'");
        if ($q) {
            
            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Falied";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getUserForSiteManager") {
    $mainData = array();
    $mainData2 = array();
    $procurementData = array();
    $financeData = array();
    if (count($_POST['floor_id'])>0) {
        $floor_ids = join("','",$_POST['floor_id']); 
       if($site_mn_ids !=""){
            $site_mn_idsAr = explode(',',$site_mn_ids);
            if(count($site_mn_idsAr)>0){
                for ($i=0; $i <count($site_mn_idsAr) ; $i++) { 
                    if (!in_array($site_mn_idsAr[$i], $mainData2))
                    {
                        array_push($mainData2,$site_mn_idsAr[$i]);
                    }
                }
            }
            $site_mn_ids =  join("','",$site_mn_idsAr);
            $qSim = $d->selectRow('users_master.*',"users_master ", "users_master.delete_status=0 AND users_master.user_status=1 AND users_master.user_id IN ('$site_mn_ids') $blockAppendQueryUser");
            while ($data2 = mysqli_fetch_assoc($qSim)) {
                array_push($mainData, $data2);
            }

            $ssql = " AND users_master.user_id NOT IN ('$site_mn_ids')";
        }
        if($procurement_ids !=""){
            $procurement_idsAr = explode(',',$procurement_ids);
            if(count($procurement_idsAr)>0){
                for ($i=0; $i <count($procurement_idsAr) ; $i++) { 
                    if (!in_array($procurement_idsAr[$i], $procurementData))
                    {
                        array_push($procurementData,$procurement_idsAr[$i]);
                    }
                }
            }
            $procurement_ids =  join("','",$procurement_idsAr);
            $qSim = $d->selectRow('users_master.*',"users_master ", "users_master.delete_status=0 AND users_master.user_status=1 AND users_master.user_id IN ('$procurement_ids') $blockAppendQueryUser");
            while ($data3 = mysqli_fetch_assoc($qSim)) {
                array_push($mainData, $data3);
            }
            $psql = " AND users_master.user_id NOT IN ('$site_mn_ids')";
        }
        if($finance_manager_ids !=""){
            $finance_manager_idsAr = explode(',',$finance_manager_ids);
            if(count($finance_manager_idsAr)>0){
                for ($i=0; $i <count($finance_manager_idsAr) ; $i++) { 
                    if (!in_array($finance_manager_idsAr[$i], $financeData))
                    {
                        array_push($financeData,$finance_manager_idsAr[$i]);
                    }
                }
            }
            $$finance_manager_ids =  join("','",$finance_manager_idsAr);
            $qSim = $d->selectRow('users_master.*',"users_master ", "users_master.delete_status=0 AND users_master.user_status=1  AND users_master.user_id IN ('$finance_manager_ids')   $blockAppendQueryUser ");
            while ($data3 = mysqli_fetch_assoc($qSim)) {
                array_push($mainData, $data3);
            }
            $fsql = " AND users_master.user_id NOT IN ('$finance_manager_ids')";
        }
        $q = $d->selectRow('users_master.*',"users_master ", "users_master.delete_status=0 AND users_master.user_status=1  AND users_master.floor_id IN ('$floor_ids') $ssql $psql  $fsql $blockAppendQueryUser");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }
        if ($mainData) {
            $mainData  = array_map("unserialize", array_unique(array_map("serialize", $mainData)));
            $response["users"] = $mainData;
            $response["site_mn_idss"] = $mainData2;
            $response["procurement_ids"] = $procurementData;
            $response["finance_manager_ids"] = $financeData;
            $response["message"] = "users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


if ($_POST['action'] == "paidAdvanceSalary") {
    $mainData = array();
    //print_r($advance_salary_id);
        if($advance_salary_id>0){
            
            $q = $d->update("advance_salary", array('is_paid'=>1,'paid_date'=>date('Y-m-d H:i'),'amount_receive_by'=>$bms_admin_id), "advance_salary_id ='$advance_salary_id'");            
            if ($data) {
            
                $response["message"] = "users";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "users";
                $response["users"] = "";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else {
            $response["message"] = "ID required";
            $response["status"] = "201";
            echo json_encode($response);
        }
   
}
if ($_POST['action'] == "getLoanById") {
    $mainData = array();
    $getLoanEmis = array();
   // 
    if ($loan_id>0) {
        $q = $d->selectRow('employee_loan_master.*,users_master.*,floors_master.floor_id,floors_master.floor_name,block_master.block_id,block_master.block_name,loan_created_by_admin.admin_name',"employee_loan_master LEFT JOIN floors_master ON floors_master.floor_id = employee_loan_master.floor_id LEFT JOIN block_master ON block_master.block_id = employee_loan_master.block_id LEFT JOIN users_master ON users_master.user_id = employee_loan_master.user_id  LEFT JOIN bms_admin_master AS loan_created_by_admin ON loan_created_by_admin.admin_id = employee_loan_master.loan_created_by","employee_loan_master.loan_id = '$loan_id'");
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            $LEs = $q = $d->selectRow('loan_emi_master.*','loan_emi_master',"loan_emi_master.loan_id=$loan_id");
            while ($data2 = mysqli_fetch_assoc($LEs)) {
                array_push($getLoanEmis, $data2);
            }
            $data['loan_emis']=$getLoanEmis;
            $response["data"] = $data;
            $response["message"] = "users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getFaceAppDeviceById") {
    $mainData = array();
    $getLoanEmis = array();
   // 
    if ($face_app_device_id>0) {
        $q = $d->selectRow('face_app_device_master.*,bms_admin_master.admin_id,bms_admin_master.admin_name','face_app_device_master LEFT JOIN bms_admin_master ON bms_admin_master.admin_id = face_app_device_master.updated_by',"face_app_device_master.face_app_device_id=$face_app_device_id");
        $data = mysqli_fetch_assoc($q);
        if ($data) {
            $data['last_syc_date'] =date('Y-m-d',strtotime($data['last_syc_date']));
            $data['updated_date'] =date('Y-m-d',strtotime($data['updated_date']));
            if($data['admin_name'] !=null){
                $data['admin_name'] = $data['admin_name'];
            }else
            {
                $data['admin_name'] = "";
            }
            $response["data"] = $data;
            $response["message"] = "users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "removeSalaryStamp") {
    $mainData = array();
    $getLoanEmis = array();
   // 
    if ($salary_setting_id>0) {
       $a = array('salary_setting_sign_stamp'=>"");
       $q = $d->update("salary_setting", $a, "salary_setting_id ='$salary_setting_id'");            
        if ($q) {
            $response["message"] = "setting";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getPendingDues") {
    $mainData = array();
    $getLoanEmis = array();
   // 
    if ($user_id>0) {
        /* $q = $d->selectRow('penalty_master.*',"penalty_master","penalty_master.user_id=$user_id");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }  */      
        $LoanSql = $d->selectRow("employee_loan_master.*,(SELECT COUNT(*) FROM loan_emi_master WHERE loan_id = employee_loan_master.loan_id AND is_emi_paid = 0 AND user_id = $user_id) AS emi_count,(SELECT SUM(emi_amount) FROM loan_emi_master WHERE loan_id = employee_loan_master.loan_id AND is_emi_paid = 0 AND user_id = $user_id) AS emi_amount,(SELECT COUNT(*) FROM advance_salary WHERE  is_paid = 0 AND user_id = $user_id) AS advance_salary_count,(SELECT SUM(advance_salary_amount) FROM advance_salary WHERE  is_paid = 0 AND user_id = $user_id) AS advance_salary_amount,(SELECT SUM(penalty_amount) FROM penalty_master WHERE  paid_status = 0 AND user_id = $user_id) AS penalty_amount,(SELECT COUNT(*) FROM penalty_master WHERE  paid_status = 0 AND user_id = $user_id) AS penalty_count","employee_loan_master","employee_loan_master.user_id=$user_id");
        $getLoan = mysqli_fetch_assoc($LoanSql);      
        if ($getLoan) {
           // $response["data"] = $mainData;
            $response["getLoan"] = $getLoan;
            $response["message"] = "loan_data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["data"] = $mainData;
            $response["getLoan"] = "";
            $response["users"] = "loan_data";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getAachievementsEducation") {
    $mainData = array();
    if ($user_id>0) {
        /* $q = $d->selectRow('penalty_master.*',"penalty_master","penalty_master.user_id=$user_id");
        while ($data = mysqli_fetch_assoc($q)) {
            array_push($mainData, $data);
        }  */      
        $eaec=$d->selectRow("employee_achievements_education.*,DATE_FORMAT(employee_achievements_education.achievement_date,'%d %M %Y') AS achievement_date","employee_achievements_education","user_id='$user_id'");
        while ($data2 = mysqli_fetch_assoc($eaec)) {
            array_push($mainData, $data2);
        }     
        if ($mainData) {
           // $response["data"] = $mainData;
            $response["education"] = $mainData;
            $response["message"] = "education";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["education"] = $mainData;
         
            $response["users"] = "education";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getUserNots") {
    $mainData = array();
    if ($user_id>0) {
        $fq11=$d->selectRow("user_notes.*,DATE_FORMAT(created_date,'%d %M %Y') AS created_date","user_notes","user_id='$user_id' AND society_id='$society_id' AND share_with_admin=1");
        while ($data2 = mysqli_fetch_assoc($fq11)) {
            array_push($mainData, $data2);
        }     
        if ($mainData) {
           // $response["data"] = $mainData;
            $response["user_notes"] = $mainData;
            $response["message"] = "user_notes";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["user_notes"] = $mainData;
         
            $response["users"] = "user_notes";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "UserAssetsData") {
    $mainData = array();
    if ($user_id>0) {
        $iaq=$d->select("assets_item_detail_master,assets_category_master,assets_detail_inventory_master","assets_item_detail_master.assets_id=assets_detail_inventory_master.assets_id AND assets_item_detail_master.assets_category_id=assets_category_master.assets_category_id AND assets_detail_inventory_master.user_id='$user_id' AND assets_detail_inventory_master.society_id='$society_id' AND assets_detail_inventory_master.end_date='0000-00-00'");
        while ($data2 = mysqli_fetch_assoc($iaq)) {
            array_push($mainData, $data2);
        }     
        if ($mainData) {
           // $response["data"] = $mainData;
            $response["user_assets"] = $mainData;
            $response["message"] = "user_notes";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["user_assets"] = $mainData;
         
            $response["users"] = "user_notes";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getEmployeeBankData") {
    $mainData = array();
    if ($user_id>0) {
        $bd=$d->selectRow("user_bank_master.*,DATE_FORMAT(bank_created_at,'%d %M %Y') AS bank_created_at","user_bank_master","user_id='$user_id' AND society_id='$society_id' AND is_delete=0");
        while ($data2 = mysqli_fetch_assoc($bd)) {
            array_push($mainData, $data2);
        }     
        if ($mainData) {
           // $response["data"] = $mainData;
            $response["user_bank"] = $mainData;
            $response["message"] = "user_notes";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["user_bank"] = $mainData;
         
            $response["users"] = "user_notes";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getEmployeeVisitingCard") {
    $mainData = array();
    if ($user_id>0) {
        $bd=$d->selectRow("user_visiting_card.*","user_visiting_card,users_master","user_visiting_card.user_id='$user_id' AND user_visiting_card.user_id=users_master.user_id ");
        while ($data2 = mysqli_fetch_assoc($bd)) {
            array_push($mainData, $data2);
        }     
        if ($mainData) {
           // $response["data"] = $mainData;
            $response["visiting_card"] = $mainData;
            $response["message"] = "visting_card";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["visiting_card"] = $mainData;
         
            $response["users"] = "visting_card";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "No Data Found";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "deleteBankAccuount") {
        
    $mainData = $d->delete("user_bank_master","user_id='$user_id' AND bank_id ='$bank_id'"); 
    if ($mainData) {
           // $response["data"] = $mainData;
            $response["message"] = "bank details deleted";
            $response["status"] = "200";
            echo json_encode($response);
        
    } else {
        $response["message"] = "Something went wrong";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "getEmployeeHistory") {
    $mainData = array();
    $mainData2 = array();
    if ($user_id>0) {
        $empe_exp_history=$d->select("employee_history","user_id='$user_id' AND society_id='$society_id'");
        while ($data2 = mysqli_fetch_assoc($empe_exp_history)) {
             if($data2['end_date'] !="0000-00-00"){ 
                $data2['end_date'] =  date("d M Y", strtotime($data2['end_date']));
                } else{
                    $data2['end_date'] = "";
                }
             if($data2['joining_date'] !="0000-00-00"){ 
                $data2['joining_date'] =  date("d M Y", strtotime($data2['joining_date']));
                } else{
                    $data2['joining_date'] = "";
                }
            array_push($mainData, $data2);
        }     
        $empe_exp=$d->select("employee_experience","user_id='$user_id' AND society_id='$society_id'");
        while ($data3 = mysqli_fetch_assoc($empe_exp)) {
            if($data2['work_from'] !="0000-00-00"){ 
                $data2['work_from'] =  date("d M Y", strtotime($data2['work_from']));
                } else{
                    $data2['work_from'] = "";
            }
            if($data2['work_to'] !="0000-00-00"){ 
                $data2['work_to'] =  date("d M Y", strtotime($data2['work_to']));
                } else{
                    $data2['work_to'] = "";
            }
            array_push($mainData2, $data3);
        }     
        if ($mainData) {
           // $response["data"] = $mainData;
            $response["employee_history"] = $mainData;
            $response["employee_experience"] = $mainData2;
            $response["message"] = "user_notes";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["employee_history"] = $mainData;
            $response["employee_experience"] = $mainData2;
            $response["users"] = "user_notes";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
if ($_POST['action'] == "payOutLeave") {
    $mainData = array();
    $mainData2 = array();
    if ($leave_payout_id>0) {
       $q = $d->update('leave_payout_master',array('leave_payout_status'=>1),"leave_payout_id=$leave_payout_id");   
        if ($mainData) {
            $response["message"] = "leave_payout";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["users"] = "leave_payout";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


if(isset($_POST['updateBirthdaySetting'])){

    
      $m->set_data('birthday_notification_type',$birthday_notification_type);
      

      $a =array(
        'birthday_notification_type'=> $m->get_data('birthday_notification_type'),
      );
      
      $q=$d->update("society_master",$a,"society_id='$society_id'");
    
      if($q>0) {
        echo 1;
      } else {
        echo 0;
      }

}



}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}

function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "No Data";
    }

}

/* PMS Module 07/07/2022 */

if ($_POST['action'] == "getDimensional") {
    if ($dimensional_id != '') {
        $q = $d->selectRow('*', "dimensional_master", "dimensional_id='$dimensional_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["dimensional"] = $data;
            $response["message"] = "Dimensional Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Dimensional Data";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getAttribute") {
    if ($attribute_id != '') {
        $q = $d->selectRow('*', "attribute_master", "attribute_id='$attribute_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["attribute"] = $data;
            $response["message"] = "Attribute Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Attribute Data";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}


/* DAR Module 11/07/2022 */
if ($_POST['action'] == "getTemplate") {
    if ($template_id != '') {
        $q = $d->selectRow('*', "template_master", "template_id='$template_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["template"] = $data;
            $response["message"] = "Template Data";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Template Data";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

/* 2022 July 29 */
if ($_POST['action'] == "UserTeamMembers") {
    $mainData = array();
    if ($user_id>0 && $level_id>0) {
        $iaq=$d->selectRow("users_master.*,block_master.block_name,floors_master.floor_name,employee_level_master.level_name","users_master,block_master,floors_master,employee_level_master","users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.level_id=employee_level_master.level_id AND users_master.level_id IN (SELECT level_id FROM `employee_level_master` WHERE parent_level_id = '$level_id') AND users_master.society_id='$society_id'");
        while ($data2 = mysqli_fetch_assoc($iaq)) {
            array_push($mainData, $data2);
        }     
        if ($mainData) {
            $response["team_members"] = $mainData;
            $response["message"] = "user_notes";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["team_members"] = $mainData;
         
            $response["users"] = "user_notes";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["team_members"] = $mainData;
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "getIdProof") {
    if ($id_proof_id != '') {
        $q = $d->selectRow('*', "id_proof_master", "id_proof_id='$id_proof_id'");
        $data = mysqli_fetch_assoc($q);
        $data = array_map("html_entity_decode", $data);
        if ($data) {
            $response["id_proof"] = $data;
            $response["message"] = "ID Proof";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "ID Proof";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

if ($_POST['action'] == "UserIdProof") {
    $mainData = array();
    if ($user_id>0) {
        $q=$d->selectRow("id_proof_master.id_proof_name,user_id_proofs.user_id, user_id_proofs.id_proof_id, user_id_proofs.created_date,id_proof,id_proof_back","user_id_proofs,id_proof_master","user_id_proofs.id_proof_id=id_proof_master.id_proof_id AND user_id_proofs.society_id='$society_id' AND user_id_proofs.user_id='$user_id'","");
        while ($data = mysqli_fetch_assoc($q)) {
           
            $data['id_proof_id'] = $data['id_proof_id'];
            $ext = pathinfo($data['id_proof'], PATHINFO_EXTENSION);
            $extBack = pathinfo($data['id_proof_back'], PATHINFO_EXTENSION);
            $data['id_proof_ext'] = $ext;
            $data['id_proof_ext_back'] = $extBack;
            $data['id_proof'] = $data['id_proof'];
            $data['id_proof_back'] = $data['id_proof_back'];
            array_push($mainData, $data);
        }     
        if ($mainData) {
            $response["id_proof"] = $mainData;
            $response["message"] = "ID Proof";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["id_proof"] = $mainData;
            $response["message"] = "ID Proof";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["id_proof"] = $mainData;
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}

/* 01 DEC 2022 SHUBHAM */
if(isset($_POST['action']) && $_POST['action'] == 'menuAccessTypeSetting'){

    
    $m->set_data('access_type',$access_type);
    

    $a =array(
      'access_type'=> $m->get_data('access_type'),
    );
    
    $q=$d->update("employee_profile_menu_access",$a,"profile_menu_id='$profile_menu_id'");
  
    if($q>0) {
      echo 1;
    } else {
      echo 0;
    }

}

if ($_POST['action'] == "getLeaveDefaultCountById") {
    $mainData = array();
    if ($leave_default_count_id != '') {
            $checkLeave = $d->selectRow('*', "leave_default_count_master 
            LEFT JOIN leave_group_master On leave_group_master.leave_group_id = leave_default_count_master.leave_group_id 
            LEFT JOIN leave_type_master On leave_type_master.leave_type_id = leave_default_count_master.leave_type_id", 
            "leave_default_count_master.leave_default_count_id = '$leave_default_count_id'");
            $checkLeaveData = mysqli_fetch_assoc($checkLeave);
        
        if ($checkLeaveData) {
            $response["Leave"] = $checkLeaveData;
            $response["message"] = "users";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "users";
            $response["users"] = "";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "ID required";
        $response["status"] = "201";
        echo json_encode($response);
    }
}