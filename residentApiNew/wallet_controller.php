<?php
include_once 'lib.php';


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
	$response = array();
	extract(array_map("test_input" , $_POST));

     if($_POST['getTransaction']=="getTransaction" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_mobile, FILTER_VALIDATE_INT) == true){

        if ($country_code!='') {
            $appendQuery = " AND users_master.country_code='$country_code'";
        }
           
        $q=$d->select("user_wallet_master,users_master","users_master.delete_status=0 AND users_master.unit_id=user_wallet_master.unit_id AND  users_master.user_mobile=user_wallet_master.user_mobile AND user_wallet_master.user_mobile='$user_mobile' $appendQuery ","ORDER BY user_wallet_master.wallet_id DESC");
          
         
        if(mysqli_num_rows($q)>0){

            $response["tansaction"] = array();
            while($data_bill_list=mysqli_fetch_array($q)) {
                     $tansaction = array(); 
                     $tansaction["wallet_id"]=$data_bill_list['wallet_id'];
                     $tansaction["society_id"]=$data_bill_list['society_id'];
                     $tansaction["user_mobile"]=$data_bill_list['user_mobile'];
                     $tansaction["credit_amount"]=$data_bill_list['credit_amount'];
                     $tansaction["debit_amount"]=$data_bill_list['debit_amount'];
                     $tansaction["credit_amount_view"]=number_format($data_bill_list['credit_amount'],2);
                     $tansaction["debit_amount_view"]=number_format($data_bill_list['debit_amount'],2);
                     $tansaction["avl_balance"]=$data_bill_list['avl_balance'];
                     $tansaction["avl_balance_view"]=number_format($data_bill_list['avl_balance'],2);
                     $tansaction["remark"]=html_entity_decode($data_bill_list['remark']);
                    
                     $tansaction["active_status"]=$data_bill_list['active_status'];
                     $tansaction["created_date"]=date("d M Y h:i A", strtotime($data_bill_list['created_date']));
                    
                   
                     array_push($response["tansaction"], $tansaction);
            
            }

            $count5=$d->sum_data("debit_amount","user_wallet_master","user_mobile='$user_mobile' AND active_status=0");
            $row11=mysqli_fetch_array($count5);
            $totalDebitAmount=$row11['SUM(debit_amount)'];

            $cq=$d->sum_data("credit_amount","user_wallet_master","user_mobile='$user_mobile' AND active_status=0");
            $row22=mysqli_fetch_array($cq);
            $totalCreditAmount=$row22['SUM(credit_amount)'];

            $availableBalance= $totalCreditAmount-$totalDebitAmount;
            
           
             $response["availableBalance"]=number_format($availableBalance,2);
             $response["till_msg"]=date("d M Y h:i A");
             $response["message"]="Customer Transaction Successfully !";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["availableBalance"]="0.00";
            $response["message"]="No Transaction Found.";
            $response["status"]="201";
            echo json_encode($response);

        }
    } else if($_POST['getOnlineTransaction']=="getOnlineTransaction" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_mobile, FILTER_VALIDATE_INT) == true){

        if ($country_code!='') {
            $appendQuery = " AND users_master.country_code='$country_code'";
        }
          
           
        $q=$d->select("society_transection_master,users_master","society_transection_master.unit_id=users_master.unit_id AND users_master.user_mobile=society_transection_master.payment_phone AND  society_transection_master.payment_phone='$user_mobile' $appendQuery ","ORDER BY society_transection_master.transection_id DESC");
          
         
        if(mysqli_num_rows($q)>0){

            $response["tansaction"] = array();
            while($data_bill_list=mysqli_fetch_array($q)) {
                     $tansaction = array(); 
                     $tansaction["transection_id"]=$data_bill_list['transection_id'];
                     $tansaction["society_id"]=$data_bill_list['society_id'];
                     $tansaction["user_mobile"]=$data_bill_list['payment_phone'];
                     $tansaction["payment_for_name"]=$data_bill_list['payment_for_name'];
                     if ($data_bill_list['wallet_balance_used']>0 && $data_bill_list['transection_amount']>0) {
                        $tansaction["payment_mode"]=$data_bill_list['payment_mode'].' & Wallet';
                     } else {
                        $tansaction["payment_mode"]=$data_bill_list['payment_mode'];
                     }
                     $tansaction["transection_amount"]=number_format($data_bill_list['transection_amount']+$data_bill_list['wallet_balance_used'],2);
                     $tansaction["transection_date"]=html_entity_decode($data_bill_list['transection_date']);
                     $tansaction["transection_date"]=date("d M Y h:i A", strtotime($data_bill_list['transection_date']));
                    
                   
                     array_push($response["tansaction"], $tansaction);
            
            }

          
             $response["message"]="Online Transaction Successfully !";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["availableBalance"]="0.00";
            $response["message"]="No Transaction Found.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else{
      $response["message"]="wrong tag.";
      $response["status"]="201";
      echo json_encode($response);
    }
  }
    else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
