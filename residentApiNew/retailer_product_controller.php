<?php
include_once 'lib.php';

if (isset($_POST)){
	extract(array_map("test_input", $_POST));
	$response = array();

	if($_POST['getProductCategory']=="getProductCategory" && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	

		$qry = $d->select("product_category_master","society_id = '$society_id' AND product_category_status = '0' AND product_category_delete = '0'");

		$response["product_categories"] = array();

		if (mysqli_num_rows($qry) > 0) {
		

			while($data = mysqli_fetch_array($qry)){
				$category = array();

				$product_category_id = $data['product_category_id'];

				$category['product_category_id'] = $product_category_id.'';
				$category['category_name'] = $data['category_name'];

				$qry1 = $d->select("
					retailer_product_master,
					retailer_product_variant_master,
					product_category_master,
					unit_measurement_master
					","retailer_product_variant_master.product_id = retailer_product_master.product_id 
					AND retailer_product_variant_master.variant_active_status = '0'
					AND retailer_product_variant_master.variant_delete_status = '0' 
					AND unit_measurement_master.unit_measurement_id = retailer_product_variant_master.unit_measurement_id  
					AND product_category_master.product_category_id = retailer_product_master.product_category_id 
					AND retailer_product_master.product_category_id = '$product_category_id'");

				$category["products"] = array();

				if (mysqli_num_rows($qry1) > 0) {
				

					while($data = mysqli_fetch_array($qry1)){
						$products = array();

						$products['product_id'] = $data['product_id'];
						$products['product_variant_id'] = $data['product_variant_id'];
						$products['product_name'] = $data['product_name'];
						$products['product_variant_name'] = $data['product_variant_name'].'';

						if ($data['variant_photo'] != '') {
							$products['variant_photo'] = $base_url.'img/vendor_product/'.$data['variant_photo'].'';
						}else{
							$products['variant_photo'] = "";
						}

						$products['variant_packing_type'] = $data['variant_packing_type'].'';
						$products['variant_bulk_type'] = $data['variant_bulk_type'].'';

						if ($data['variant_bulk_type'] == 0) {
							$products['variant_bulk_type_view'] = "Piece Wise";
						}else{
							$products['variant_bulk_type_view'] = "Box Wise";					
						}
						
						$products['sku'] = $data['sku'].'';
						$products['variant_per_box_piece'] = $data['variant_per_box_piece'].'';
						$products['retailer_selling_price'] = $data['retailer_selling_price'].'';
						$products['maximum_retail_price'] = $data['maximum_retail_price'].'';
						$products['menufacturing_cost'] = $data['menufacturing_cost'].'';
						$products['unit_measurement_id'] = $data['unit_measurement_id'].'';
						$products['unit_measurement_name'] = $data['unit_measurement_name'].'';
						$products['variant_description'] = $data['variant_description'].'';
						$products['weight_in_kg'] = $data['weight_in_kg'].'';

						array_push($category["products"],$products);
					}
				}

				if (count($category["products"]) > 0) {
					array_push($response["product_categories"],$category);
				}
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getProducts']=="getProducts" && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	

		$qry = $d->selectRow(
			"retailer_product_master.*,
			product_category_master.category_name,
			product_sub_category_master.sub_category_name
			","
			retailer_product_master,product_category_master,product_sub_category_master
			","
			retailer_product_master.society_id = '$society_id' 
			AND retailer_product_master.product_status = '0' 
			AND retailer_product_master.product_delete= '0'
			AND retailer_product_master.product_category_id = product_category_master.product_category_id
			AND retailer_product_master.product_sub_category_id = product_sub_category_master.product_sub_category_id");

		if (mysqli_num_rows($qry) > 0) {
		
			$response["products"] = array();

			while($data = mysqli_fetch_array($qry)){
				$products = array();

				$products['product_id'] = $data['product_id'];
				$products['product_code'] = $data['product_code'];
				$products['product_name'] = $data['product_name'];
				$products['product_alias_name'] = $data['product_alias_name'].'';
				$products['product_short_name'] = $data['product_short_name'].'';
				$products['product_category_id'] = $data['product_category_id'];
				$products['category_name'] = $data['category_name'];			
				$products['product_sub_category_id'] = $data['product_sub_category_id'];
				$products['sub_category_name'] = $data['sub_category_name'];			
				$products['product_brand'] = $data['product_brand'].'';

				if ($data['product_image'] != '') {
					$products['product_image'] = $base_url.'img/vendor_product/'.$data['product_image'].'';
				}else{
					$products['product_image'] = "";
				}

				$products['product_description'] = $data['product_description'].'';
				
				array_push($response["products"],$products);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getProductVariants']=="getProductVariants" && $product_category_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	

		$qry = $d->select("
			retailer_product_master,
			retailer_product_variant_master,
			product_category_master
			","retailer_product_variant_master.product_id = retailer_product_master.product_id 
			AND retailer_product_variant_master.variant_active_status = '0'
			AND retailer_product_variant_master.variant_delete_status = '0' 
			AND product_category_master.product_category_id = retailer_product_master.product_category_id 
			AND retailer_product_master.product_category_id = '$product_category_id'");

		if (mysqli_num_rows($qry) > 0) {
		
			$response["products"] = array();

			while($data = mysqli_fetch_array($qry)){
				$products = array();

				$products['product_id'] = $data['product_id'];
				$products['product_variant_id'] = $data['product_variant_id'];
				$products['product_name'] = $data['product_name'];
				$products['product_variant_name'] = $data['product_variant_name'].'';

				if ($data['variant_photo'] != '') {
					$products['variant_photo'] = $base_url.'img/vendor_product/'.$data['variant_photo'].'';
				}else{
					$products['variant_photo'] = "";
				}

				$products['variant_packing_type'] = $data['variant_packing_type'].'';
				$products['variant_bulk_type'] = $data['variant_bulk_type'].'';

				if ($data['variant_bulk_type'] == 0) {
					$products['variant_bulk_type_view'] = "Piece Wise";
				}else{
					$products['variant_bulk_type_view'] = "Box Wise";					
				}
				
				$products['sku'] = $data['sku'].'';
				$products['variant_per_box_piece'] = $data['variant_per_box_piece'].'';
				$products['retailer_selling_price'] = $data['retailer_selling_price'].'';
				$products['maximum_retail_price'] = $data['maximum_retail_price'].'';
				$products['menufacturing_cost'] = $data['menufacturing_cost'].'';
				$products['unit_measurement_id'] = $data['unit_measurement_id'].'';
				$products['variant_description'] = $data['variant_description'].'';

				array_push($response["products"],$products);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getFrequentProducts']=="getFrequentProducts" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

		$qry = $d->selectRow("
			retailer_product_master.*,
			retailer_product_variant_master.*,
			product_category_master.*,
			retailer_order_product_master.*,
			unit_measurement_master.unit_measurement_name,
			count(retailer_order_product_master.product_variant_id)
			","
			retailer_product_master,
			retailer_product_variant_master,
			product_category_master,
			retailer_order_product_master,
			retailer_order_master,
			unit_measurement_master
			","retailer_product_variant_master.product_id = retailer_product_master.product_id 
			AND retailer_product_variant_master.variant_active_status = '0'
			AND retailer_product_variant_master.variant_delete_status = '0' 
			AND product_category_master.product_category_id = retailer_product_master.product_category_id 
			AND retailer_order_product_master.product_variant_id = retailer_product_variant_master.product_variant_id 
			AND retailer_order_product_master.product_id = retailer_product_variant_master.product_id 
			AND retailer_order_product_master.retailer_order_id = retailer_order_master.retailer_order_id 
			AND retailer_order_master.retailer_id = '$retailer_id' 
			AND unit_measurement_master.unit_measurement_id = retailer_product_variant_master.unit_measurement_id  
			","
			Group BY retailer_order_product_master.product_variant_id ORDER BY count(retailer_order_product_master.product_variant_id) DESC LIMIT 10 
			");

		if (mysqli_num_rows($qry) > 0) {
		
			$response["products"] = array();

			while($data = mysqli_fetch_array($qry)){

				$products = array();

				$products['product_id'] = $data['product_id'];				
				$products['product_variant_id'] = $data['product_variant_id'];
				$products['product_category_id'] = $data['product_category_id'];
				$products['product_name'] = $data['product_name'];
				$products['product_variant_name'] = $data['product_variant_name'].'';
				$products['frequent_count'] = $data['count(retailer_order_product_master.product_variant_id)'];

				if ($data['variant_photo'] != '') {
					$products['variant_photo'] = $base_url.'img/vendor_product/'.$data['variant_photo'].'';
				}else{
					$products['variant_photo'] = "";
				}

				$products['variant_packing_type'] = $data['variant_packing_type'].'';
				$products['variant_bulk_type'] = $data['variant_bulk_type'].'';

				if ($data['variant_bulk_type'] == 0) {
					$products['variant_bulk_type_view'] = "Piece Wise";
				}else{
					$products['variant_bulk_type_view'] = "Box Wise";					
				}
				
				$products['sku'] = $data['sku'].'';
				$products['variant_per_box_piece'] = $data['variant_per_box_piece'].'';
				$products['retailer_selling_price'] = $data['retailer_selling_price'].'';
				$products['maximum_retail_price'] = $data['maximum_retail_price'].'';
				$products['menufacturing_cost'] = $data['menufacturing_cost'].'';
				$products['unit_measurement_id'] = $data['unit_measurement_id'].'';
				$products['unit_measurement_name'] = $data['unit_measurement_name'].'';
				$products['variant_description'] = $data['variant_description'].'';
				$products['weight_in_kg'] = $data['weight_in_kg'].'';
				
				array_push($response["products"],$products);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else{
		$response["success"] = 201;
		$response["message"] = "Invalid Tag !";
		echo json_encode($response);
	}

} else {
	$response["success"] = 201;
	$response["message"] = "Invalid API Key !";
	echo json_encode($response);
}

?>