<?php
include_once 'lib.php';


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
	$response = array();
	extract(array_map("test_input" , $_POST));
    if (isset($society_id)) {
        $sq=$d->selectRow("currency","society_master","society_id='$society_id'");
        $societyData = mysqli_fetch_array($sq);
        $currency = $societyData['currency'];
    } else {
        $currency = '';
    }
    
    if($_POST['getCustomer']=="getCustomer" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

           
        $q=$d->select("finbook_customer","society_id='$society_id' AND user_id='$user_id'","ORDER BY customer_name ASC");
          
         
        if(mysqli_num_rows($q)>0){

            $response["customer"] = array();

            $sum =0;
            $sum1=0;

            while($data_bill_list=mysqli_fetch_array($q)) {
                    $q111=$d->selectRow("user_full_name","users_master","user_id='$user_id'","");
                    $userdataComment=mysqli_fetch_array($q111);
                    $user_full_name=$userdataComment['user_full_name'];

                     $customer = array(); 
                     $customer["finbook_customer_id"]=$data_bill_list['finbook_customer_id'];
                     $customer["society_id"]=$data_bill_list['society_id'];
                     $customer["user_id"]=$data_bill_list['user_id'];
                     $customer["customer_name"]=$data_bill_list['customer_name'];
                     $customer["customer_mobile"]=$data_bill_list['customer_mobile'];
                     $customer["customer_address"]=$data_bill_list['customer_address']."";

                    

                    $count5=$d->sum_data("debit_amount","finbook_passbook","user_id='$data_bill_list[user_id]' AND finbook_customer_id='$data_bill_list[finbook_customer_id]' AND active_status=0");
                    $row11=mysqli_fetch_array($count5);
                    $totalDebitAmount=$row11['SUM(debit_amount)'];

                    $cq=$d->sum_data("credit_amount","finbook_passbook","user_id='$data_bill_list[user_id]' AND finbook_customer_id='$data_bill_list[finbook_customer_id]' AND active_status=0");
                    $row22=mysqli_fetch_array($cq);
                    $totalCreditAmount=$row22['SUM(credit_amount)'];

                    $dueBlanace= $totalDebitAmount-$totalCreditAmount;
                    if ($dueBlanace<0) {
                        $dueBlanace= abs($dueBlanace);
                        $dueBlanace=number_format($dueBlanace,2,'.','');
                        $viewMsg= "Due";
                        $is_due= true;
                        if ($data_bill_list['due_date']!='') {
                            $due_date=date('d M Y',strtotime($data_bill_list['due_date']));
                        }
                        $sum= $sum+=$dueBlanace;
                        $paymentReminderMsg= "Payment of $currency $dueBlanace to $user_full_name is pending. Please make payment as soon as possible.\n\nThank You\nMyCo";
                    } else {
                        $dueBlanace= number_format($dueBlanace,2,'.','');
                        $viewMsg= "Advance";
                        $is_due=false;
                        $due_date = "";
                        $sum1= $sumy+=$dueBlanace;
                        $paymentReminderMsg= "";
                    }

                     $customer["viewMsg"]=$viewMsg;
                     $customer["is_due"]=$is_due;
                     $customer["due_amount"]="$currency ".number_format($dueBlanace,2);
                     $customer["paymentReminderMsg"]=$paymentReminderMsg;
                     $customer["due_date"]=$due_date.'';

                     array_push($response["customer"], $customer);

            
            }  

            
           
             $response["totalDue"]="$currency ".number_format($sum,2);
             $response["totalAdvance"]="$currency ".number_format($sum1,2);
             $response["message"]="Customer Get successfully...";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Customer Found.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else if($_POST['addCustomer']=="addCustomer" && filter_var($user_id, FILTER_VALIDATE_INT) == true){

              $customer_mobile= (int)$customer_mobile;
                if (strlen($customer_mobile)<8 ) {
                    $response["message"]="Invalid Mobile Number";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }

            $qc=$d->selectRow("customer_mobile","finbook_customer","user_id='$user_id' AND customer_mobile='$customer_mobile'");
            if (mysqli_num_rows($qc)>0) {
                $response["message"]="This Number Already Added In Your Customer List !";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }

            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('customer_name',$customer_name);
            $m->set_data('customer_mobile',$customer_mobile);
            $m->set_data('customer_address',$customer_address);



               $a1 = array(
                    'society_id'=>$m->get_data('society_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'customer_name'=>$m->get_data('customer_name'),
                    'customer_mobile'=>$m->get_data('customer_mobile'),
                    'customer_address'=>$m->get_data('customer_address')
                );


              $q1=$d->insert("finbook_customer",$a1);

            if ($q1==TRUE) {
                $d->insert_myactivity($user_id,"$society_id","0","$user_name","$customer_name hisab customer added","Hisab-book_1xxxhdpi.png");

                 $response["message"]="Customer Added !";
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }

    }else if($_POST['editCustomer']=="editCustomer"  && filter_var($finbook_customer_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

              $customer_mobile= (int)$customer_mobile;
                if (strlen($customer_mobile)<8 ) {
                    $response["message"]="Invalid Mobile Number";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }

            $qc=$d->selectRow("customer_mobile","finbook_customer","user_id='$user_id' AND customer_mobile='$customer_mobile' AND finbook_customer_id!='$finbook_customer_id'");
            if (mysqli_num_rows($qc)>0) {
                $response["message"]="This Number Already Added In Your Customer List !";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }

            $m->set_data('customer_name',$customer_name);
            $m->set_data('customer_mobile',$customer_mobile);
            $m->set_data('customer_address',$customer_address);



               $a1 = array(
                    'customer_name'=>$m->get_data('customer_name'),
                    'customer_mobile'=>$m->get_data('customer_mobile'),
                    'customer_address'=>$m->get_data('customer_address')
                );


              $q1=$d->update("finbook_customer",$a1,"finbook_customer_id='$finbook_customer_id' AND user_id='$user_id'");

            if ($q1==TRUE) {

                 $response["message"]="Customer Data Updated !";
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }

    }else if($_POST['setDueDate']=="setDueDate" && filter_var($finbook_customer_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

             $due_date= date('Y-m-d',strtotime($due_date));

               $m->set_data('due_date',$due_date);

               $a1 = array(
                    'due_date'=>$m->get_data('due_date'),
                );


              $q1=$d->update("finbook_customer",$a1,"finbook_customer_id='$finbook_customer_id' AND user_id='$user_id'");

            if ($q1==TRUE) {

                 $response["message"]="Due Date Set Successfully !";
                 $response["due_date"]= date('d M Y',strtotime($due_date));
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }

    }else if($_POST['addTransaction']=="addTransaction" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($finbook_customer_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){
            if ($amount<1) {
                $response["message"]="Invalid Amount !";
                $response["status"]="201";
                echo json_encode($response);
                exit();

            }

            $con -> autocommit(FALSE);

           

                
                $m->set_data('finbook_customer_id',$finbook_customer_id);
                $m->set_data('society_id',$society_id);
                $m->set_data('user_id',$user_id);

               
            $count5=$d->sum_data("debit_amount","finbook_passbook","user_id='$user_id' AND finbook_customer_id='$finbook_customer_id' AND active_status=0");
            $row11=mysqli_fetch_array($count5);
            $totalDebitAmount=$row11['SUM(debit_amount)'];

            $cq=$d->sum_data("credit_amount","finbook_passbook","user_id='$user_id' AND finbook_customer_id='$finbook_customer_id' AND active_status=0");
            $row22=mysqli_fetch_array($cq);
            $totalCreditAmount=$row22['SUM(credit_amount)'];

            $dueBlanace= $totalDebitAmount-$totalCreditAmount;
            if ($dueBlanace<0) {
                $dueBlanace= $dueBlanace;
                // $dueBlanace= abs($dueBlanace);
            } else {
                $dueBlanace= $dueBlanace;
            }

            if ($amount_type==0) {
               $newAmount = $dueBlanace-$amount;
            } else {
               $newAmount = $dueBlanace+$amount;
            }

             if ($newAmount<0) {
                    $dueType="Due";
                 } else {
                    $dueType="Advance";
                      $aData = array(
                        'due_date'=>"",
                        );
                    $d->update("finbook_customer",$aData,"finbook_customer_id='$finbook_customer_id' AND user_id='$user_id'");
                 }

            
            if ($amount_type==0) {
                
                $dueBlanace=abs($newAmount);
                $m->set_data('credit_amount',$amount);
                $m->set_data('debit_amount',0);
                $m->set_data('total_due_advance_amount',number_format($dueBlanace,2,'.',''));
            } else {

               

                $dueBlanace=abs($newAmount);
                $m->set_data('debit_amount',$amount);
                $m->set_data('credit_amount',0);
                $m->set_data('total_due_advance_amount',number_format($dueBlanace,2,'.',''));
            }
            $m->set_data('created_date',date("Y-m-d H:i:s"));
            $m->set_data('remark',$remark);
            $m->set_data('total_due_advance_amount_msg',$dueType);

               $a1 = array(
                    'finbook_customer_id'=>$m->get_data('finbook_customer_id'),
                    'society_id'=>$m->get_data('society_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'credit_amount'=>$m->get_data('credit_amount'),
                    'debit_amount'=>$m->get_data('debit_amount'),
                    'created_date'=>$m->get_data('created_date'),
                    'remark'=>$m->get_data('remark'),
                    'total_due_advance_amount'=>$m->get_data('total_due_advance_amount'),
                    'total_due_advance_amount_msg'=>$m->get_data('total_due_advance_amount_msg'),
                );


              $q1=$d->insert("finbook_passbook",$a1);
              $finbook_passbook_id = $con->insert_id;
              $m->set_data('finbook_passbook_id',$finbook_passbook_id);
              
               $total = count($_FILES['bill_photo']['tmp_name']);
                $pPhoto="";
                for ($i = 0; $i < $total; $i++) {
                    $uploadedFile = $_FILES['bill_photo']['tmp_name'][$i]; 
                    if ($uploadedFile != "") {

                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand().$finbook_customer_id;
                    $dirPath = "../img/fincredit/";
                    $ext = pathinfo($_FILES['bill_photo']['name'][$i], PATHINFO_EXTENSION);
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];
                    
                    if ($imageWidth>1000) {
                        $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 400 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage /100;
                        $newImageHeight = $imageHeight * $newWidthPercentage /100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }

                    switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagepng($tmp,$dirPath. $newFileName. "_finbook.". $ext);
                            break;           

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagejpeg($tmp,$dirPath. $newFileName. "_finbook.". $ext);
                            break;
                        
                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagegif($tmp,$dirPath. $newFileName. "_finbook.". $ext);
                            break;

                        default:
                            $response["message"]="Invalid Image type.";
                            $response["status"]="201";
                            echo json_encode($response);
                            exit;
                            break;
                        }
                        $bill_photo= $newFileName."_finbook.".$ext;
                        
                        $a1poto = array(
                            'finbook_passbook_id'=>$m->get_data('finbook_passbook_id'),
                            'society_id'=>$m->get_data('society_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'finbook_customer_id'=>$m->get_data('finbook_customer_id'),
                            'bill_photo_name'=>$bill_photo,
                        );
                     //Make sure we have a file path
                        $photoQuery= $d->insert("finbook_bill_photos",$a1poto);
                    }else{
                        $response["message"]="faild.";
                        $response["status"]="201";
                        echo json_encode($response);

                    }
                }


            if ($q1>0) {
                 $con -> commit();
                 $response["message"]="Added Successfully";
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 mysqli_query("ROLLBACK");
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }
    }else if($_POST['updateBillPhoto']=="updateBillPhoto" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($finbook_customer_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($finbook_passbook_id, FILTER_VALIDATE_INT) == true){
            
             $con -> autocommit(FALSE);


                $m->set_data('finbook_passbook_id',$finbook_passbook_id);
                $m->set_data('finbook_customer_id',$finbook_customer_id);
                $m->set_data('society_id',$society_id);
                $m->set_data('user_id',$user_id);
                if ($device=='ios') {
                    $d->delete("finbook_bill_photos","finbook_passbook_id='$finbook_passbook_id'");
                }

                $total = count($_FILES['bill_photo']['tmp_name']);
                $pPhoto="";
                for ($i = 0; $i < $total; $i++) {
                    $uploadedFile = $_FILES['bill_photo']['tmp_name'][$i]; 
                    if ($uploadedFile != "") {

                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand().$finbook_customer_id;
                    $dirPath = "../img/fincredit/";
                    $ext = pathinfo($_FILES['bill_photo']['name'][$i], PATHINFO_EXTENSION);
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];
                    
                    if ($imageWidth>1000) {
                        $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 400 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage /100;
                        $newImageHeight = $imageHeight * $newWidthPercentage /100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }

                    switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagepng($tmp,$dirPath. $newFileName. "_finbook.". $ext);
                            break;           

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagejpeg($tmp,$dirPath. $newFileName. "_finbook.". $ext);
                            break;
                        
                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagegif($tmp,$dirPath. $newFileName. "_finbook.". $ext);
                            break;

                        default:
                            $response["message"]="Invalid Image type.";
                            $response["status"]="201";
                            echo json_encode($response);
                            exit;
                            break;
                        }
                        $bill_photo= $newFileName."_finbook.".$ext;
                        
                        $a1poto = array(
                            'finbook_passbook_id'=>$m->get_data('finbook_passbook_id'),
                            'society_id'=>$m->get_data('society_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'finbook_customer_id'=>$m->get_data('finbook_customer_id'),
                            'bill_photo_name'=>$bill_photo,
                        );
                     //Make sure we have a file path
                        $photoQuery= $d->insert("finbook_bill_photos",$a1poto);
                    }else{
                        $response["message"]="faild.";
                        $response["status"]="201";
                        echo json_encode($response);

                    }
                }

            
            $m->set_data('remark',$remark);

               $a1 = array(
                    'remark'=>$m->get_data('remark'),
                );

              $q1=$d->update("finbook_passbook",$a1,"user_id='$user_id' AND finbook_passbook_id='$finbook_passbook_id' AND finbook_customer_id='$finbook_customer_id'");

            if ($q1>0) {
                 $con -> commit();
                 $response["message"]="Details Updated Successfully !";
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 mysqli_query("ROLLBACK");
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }
    }else if($_POST['getTransaction']=="getTransaction" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($finbook_customer_id, FILTER_VALIDATE_INT) == true){

        $q111=$d->selectRow("user_full_name","users_master","user_id='$user_id'","");
        $userdataComment=mysqli_fetch_array($q111);
        $user_full_name=$userdataComment['user_full_name'];
           
        $q=$d->select("finbook_customer,finbook_passbook","finbook_customer.finbook_customer_id=finbook_passbook.finbook_customer_id AND finbook_passbook.society_id='$society_id' AND finbook_passbook.finbook_customer_id='$finbook_customer_id' AND finbook_passbook.user_id='$user_id'","ORDER BY finbook_passbook_id ASC");
          
         
        if(mysqli_num_rows($q)>0){

            $response["tansaction"] = array();
            $tempPrvDate = "";
            while($data_bill_list=mysqli_fetch_array($q)) {
                     $tansaction = array(); 
                     $tansaction["finbook_passbook_id"]=$data_bill_list['finbook_passbook_id'];
                     $tansaction["finbook_customer_id"]=$data_bill_list['finbook_customer_id'];
                     $tansaction["society_id"]=$data_bill_list['society_id'];
                     $tansaction["user_id"]=$data_bill_list['user_id'];
                     $tansaction["credit_amount"]=$data_bill_list['credit_amount'];
                     $tansaction["debit_amount"]=$data_bill_list['debit_amount'];
                     $tansaction["credit_amount_view"]=number_format($data_bill_list['credit_amount'],2);
                     $tansaction["debit_amount_view"]=number_format($data_bill_list['debit_amount'],2);
                       
                     $tansaction["remaning_amount"]=number_format($data_bill_list['total_due_advance_amount'],2)." ".$data_bill_list['total_due_advance_amount_msg'];
                     $tansaction["created_date"]=date("h:i A", strtotime($data_bill_list['created_date']));
                     $chatDate=date("d M Y", strtotime($data_bill_list['created_date']));
                     $tansaction["isDate"] = false;
                     $tansaction["msg_date_view"] = $chatDate;
                     $tansaction["added_on"] = $chatDate.', '.date("h:i A", strtotime($data_bill_list['created_date']));

                     $tansaction["remark"]=$data_bill_list['remark'];
                    
                     $tansaction["active_status"]=$data_bill_list['active_status'];
                     if ($data_bill_list['deleted_date']!='0000-00-00 00:00:00') {
                         $tansaction["deleted_date"]=date("d M Y, h:i A", strtotime($data_bill_list['deleted_date']));
                     } else {
                         $tansaction["deleted_date"]="";
                     }
                   

                     if($tempPrvDate!=""){

                        if($tempPrvDate != $chatDate){
                            $tempPrvDate = $chatDate;
                            $chatD = array();
                            $chatD["msg_date"] = $tempPrvDate;
                            $chatD["isDate"] = true;
                            $chatD["msg_date_view"] = $chatDate;

                            array_push($response["tansaction"], $chatD);
                        }

                    }else{
                        $tempPrvDate = $chatDate;
                        $chatD = array();
                        $chatD["msg_date"] = $tempPrvDate;
                        $chatD["isDate"] = true;
                        $chatD["msg_date_view"] = $chatDate;
                        array_push($response["tansaction"], $chatD);
                    }

                    $tansaction["bill_photo"] = array();
                    $qs=$d->select("finbook_bill_photos","finbook_passbook_id='$data_bill_list[finbook_passbook_id]'");
                    while ($subData=mysqli_fetch_array($qs)) {
                        $bill_photo = array();
                        $bill_photo["finbook_bill_photo_id"]=$subData["finbook_bill_photo_id"];
                        $bill_photo["finbook_passbook_id"]=$subData["finbook_passbook_id"];
                        $bill_photo["tutorial_video"]=$subData["tutorial_video"].'';
                        $bill_photo["bill_photo"]=$base_url.'img/fincredit/'.$subData['bill_photo_name'];
                        array_push($tansaction["bill_photo"], $bill_photo); 

                    }

                    
                     array_push($response["tansaction"], $tansaction);
            
            }

            $count5=$d->sum_data("debit_amount","finbook_passbook","user_id='$user_id' AND finbook_customer_id='$finbook_customer_id' AND active_status=0");
            $row11=mysqli_fetch_array($count5);
            $totalDebitAmount=$row11['SUM(debit_amount)'];

            $cq=$d->sum_data("credit_amount","finbook_passbook","user_id='$user_id' AND finbook_customer_id='$finbook_customer_id' AND active_status=0");
            $row22=mysqli_fetch_array($cq);
            $totalCreditAmount=$row22['SUM(credit_amount)'];

            $dueBlanace= $totalDebitAmount-$totalCreditAmount;
            if ($dueBlanace<0) {
                $dueBlanace= abs($dueBlanace);
                $viewMsg= "Total Due";
                $is_due= true;
                $qs=$d->selectRow("due_date","finbook_customer","finbook_customer_id='$finbook_customer_id' ");
                $cusData=mysqli_fetch_array($qs);
                if ($cusData['due_date']!='') {
                    $due_date=date('d M Y',strtotime($cusData['due_date']));
                }
                 $paymentReminderMsg= "Payment of $currency $dueBlanace to $user_full_name is pending. Please make payment as soon as possible.\n\nThank You\nMyCo";
            } else {
                $dueBlanace= $dueBlanace;
                $viewMsg= "Advance Balance";
                $is_due=false;
                $due_date="";
                $paymentReminderMsg="";
            }
           
             $response["is_due"]=$is_due;
             $response["dueBlanace"]=number_format($dueBlanace,2);
             $response["viewMsg"]=$viewMsg;
             $response["message"]="Customer Transaction Successfully !";
             $response["paymentReminderMsg"]=$paymentReminderMsg;
             $response["due_date"]=$due_date.'';
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Transaction Found.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else if($_POST['getTransactionAll']=="getTransactionAll" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

           
        $q=$d->select("finbook_customer,finbook_passbook","finbook_customer.finbook_customer_id=finbook_passbook.finbook_customer_id AND finbook_passbook.society_id='$society_id' AND finbook_passbook.user_id='$user_id' AND finbook_passbook.active_status=0","ORDER BY finbook_passbook_id ASC");
          
         
        if(mysqli_num_rows($q)>0){

            $response["tansaction"] = array();
            while($data_bill_list=mysqli_fetch_array($q)) {
                     $tansaction = array(); 
                     $tansaction["finbook_passbook_id"]=$data_bill_list['finbook_passbook_id'];
                     $tansaction["finbook_customer_id"]=$data_bill_list['finbook_customer_id'];
                     $tansaction["society_id"]=$data_bill_list['society_id'];
                     $tansaction["user_id"]=$data_bill_list['user_id'];
                     $tansaction["customer_name"]=$data_bill_list['customer_name'];
                     $tansaction["credit_amount"]=$data_bill_list['credit_amount'];
                     $tansaction["debit_amount"]=$data_bill_list['debit_amount'];
                     $tansaction["credit_amount_view"]=number_format($data_bill_list['credit_amount'],2);
                     $tansaction["debit_amount_view"]=number_format($data_bill_list['debit_amount'],2);
                       
                     $tansaction["remaning_amount"]=number_format($data_bill_list['total_due_advance_amount'],2)." ".$data_bill_list['total_due_advance_amount_msg'];
                     $tansaction["created_date"]=date("d M Y h:i A", strtotime($data_bill_list['created_date']));
                     $tansaction["added_on"] = $chatDate.', '.date("h:i A", strtotime($data_bill_list['created_date']));

                     $tansaction["remark"]=$data_bill_list['remark'];
                    
                     $tansaction["active_status"]=$data_bill_list['active_status'];
                     if ($data_bill_list['deleted_date']!='0000-00-00 00:00:00') {
                         $tansaction["deleted_date"]=date("d M Y h:i A", strtotime($data_bill_list['deleted_date']));
                     } else {
                         $tansaction["deleted_date"]="";
                     }

                    

                    $tansaction["bill_photo"] = array();
                    $qs=$d->select("finbook_bill_photos","finbook_passbook_id='$data_bill_list[finbook_passbook_id]'");
                    while ($subData=mysqli_fetch_array($qs)) {
                        $bill_photo = array();
                        $bill_photo["finbook_bill_photo_id"]=$subData["finbook_bill_photo_id"];
                        $bill_photo["finbook_passbook_id"]=$subData["finbook_passbook_id"];
                        $bill_photo["tutorial_video"]=$subData["tutorial_video"].'';
                        $bill_photo["bill_photo"]=$base_url.'img/fincredit/'.$subData['bill_photo_name'];
                        array_push($tansaction["bill_photo"], $bill_photo); 

                    }

                    
                     array_push($response["tansaction"], $tansaction);
            
            }

            $count5=$d->sum_data("debit_amount","finbook_passbook","user_id='$user_id' AND active_status=0");
            $row11=mysqli_fetch_array($count5);
            $totalDebitAmount=$row11['SUM(debit_amount)'];

            $cq=$d->sum_data("credit_amount","finbook_passbook","user_id='$user_id' AND active_status=0");
            $row22=mysqli_fetch_array($cq);
            $totalCreditAmount=$row22['SUM(credit_amount)'];

            $dueBlanace= $totalDebitAmount-$totalCreditAmount;
            if ($dueBlanace<0) {
                $dueBlanace= abs($dueBlanace);
                $viewMsg= "Total Due";
                $is_due= true;
            } else {
                $dueBlanace= $dueBlanace;
                $viewMsg= "Advance Balance";
                $is_due=false;
            }
           
             $response["is_due"]=$is_due;
             $response["dueBlanace"]=number_format($dueBlanace,2);
             $response["totalDebitAmount"]=number_format($totalDebitAmount,2);
             $response["totalCreditAmount"]=number_format($totalCreditAmount,2);
             $response["viewMsg"]=$viewMsg;
             $response["message"]="Customer Transaction Successfully !";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Transaction Found.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else if($_POST['deleteTransaction']=="deleteTransaction"  && filter_var($finbook_passbook_id, FILTER_VALIDATE_INT) == true && filter_var($finbook_customer_id, FILTER_VALIDATE_INT) == true){
        
         $m->set_data('active_status',1);
         $m->set_data('deleted_date',date("Y-m-d H:i:s"));

           $a1 = array(
                'active_status'=>$m->get_data('active_status'),
                'deleted_date'=>$m->get_data('deleted_date'),
            );


        $q=$d->update("finbook_passbook",$a1,"finbook_passbook_id='$finbook_passbook_id' AND finbook_customer_id='$finbook_customer_id'");
        if ($q>0) {
             $response["message"]="Transaction Deleted !";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Transaction Found.";
            $response["status"]="201";
            echo json_encode($response);

        }


   }else if($_POST['deleteBillPhoto']=="deleteBillPhoto"  && filter_var($finbook_bill_photo_id, FILTER_VALIDATE_INT) == true && filter_var($finbook_customer_id, FILTER_VALIDATE_INT) == true){
        
        $q=$d->delete("finbook_bill_photos","finbook_bill_photo_id='$finbook_bill_photo_id' AND finbook_customer_id='$finbook_customer_id'");
        if ($q>0) {
             $response["message"]="Bill Photo Removed !";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Transaction Found.";
            $response["status"]="201";
            echo json_encode($response);

        }


   }else if($_POST['deleteCustomer']=="deleteCustomer"  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($finbook_customer_id, FILTER_VALIDATE_INT) == true){
        
        
        $q=$d->delete("finbook_customer","finbook_customer_id='$finbook_customer_id' AND user_id='$user_id'");
        if ($q>0) {
              $d->delete("finbook_passbook","finbook_customer_id='$finbook_customer_id' AND user_id='$user_id'");
              $d->delete("finbook_bill_photos","finbook_customer_id='$finbook_customer_id' AND user_id='$user_id'");

              $d->insert_myactivity($user_id,"$society_id","0","$user_name","Hisab customer deleted","Hisab-book_1xxxhdpi.png");

             $response["message"]="Customer Deleted !";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Cusetomer Found.";
            $response["status"]="201";
            echo json_encode($response);

        }


   }else{
      $response["message"]="wrong tag";
      $response["status"]="201";
      echo json_encode($response);
    }
  }
    else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
