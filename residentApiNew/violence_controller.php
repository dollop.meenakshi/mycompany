<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
$societyLngName=  $xml->string->society;

    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
    if($_POST['addReport']=="addReport" && $unit_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ){


           
           
            $m->set_data('society_id',$society_id);
            $m->set_data('reported_by',$user_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('common_id',$common_id);
            $m->set_data('violence_type',$violence_type);
            $m->set_data('violence_for',$violence_for);
            $m->set_data('violence_message',$violence_message);
            $m->set_data('reported_time',date("Y-m-d H:i:s"));
           
               
            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'reported_by'=>$m->get_data('reported_by'),
                'unit_id'=>$m->get_data('unit_id'),
                'common_id'=>$m->get_data('common_id'),
                'violence_type'=>$m->get_data('violence_type'),
                'violence_for'=>$m->get_data('violence_for'),
                'violence_message'=>$m->get_data('violence_message'),
                'reported_time'=>$m->get_data('reported_time'),
            );


            $quserDataUpdate = $d->insert("violence_report_master",$a);
            

            if($quserDataUpdate==TRUE){

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("7",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("7",$block_id,"ios");

                switch ($violence_type) {
                    case '1':
                        $title = "Post report for $violence_for";
                        $redirectUrl = "newsFeed?id=$common_id";
                        $notification_logo = "Timeline_1xxxhdpi.png";
                        break;
                    case '2':
                        $title = "Discussion forum report for $violence_for";
                        $redirectUrl = "discussionHistory?id=$common_id";
                        $notification_logo = "Discussion_1xxxhdpi.png";
                        break;
                    
                    case '3':
                        $title = "Profile report for $violence_for";
                        $redirectUrl = "employeeDetails?id=$common_id";
                        $notification_logo = "My-profilexxxhdpi.png";
                        break;
                    
                    default:
                        // code...
                        break;
                }
                
                $description = "Reported by $user_name";
                

                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,$redirectUrl);
                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,$redirectUrl);

                    $notiAry = array(
                      'society_id'=>$society_id,
                      'notification_tittle'=>$title,
                      'notification_description'=>$description,
                      'notifiaction_date'=>date('Y-m-d H:i'),
                      'notification_action'=>$redirectUrl,
                      'admin_click_action '=>$redirectUrl,
                      'notification_logo'=>$notification_logo,
                    );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"]="$addMsg";
                $response["status"]="200";
                echo json_encode($response);

            }else{

                $response["message"]="$somethingWrong";
                $response["status"]="201";
                echo json_encode($response);

            }

        }  else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>