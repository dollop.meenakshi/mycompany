<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

  if ($key==$keydb && $auth_check=='true') {
	$response = array();
	extract(array_map("test_input" , $_POST));
  $today  = date("Y-m-d");
  if(isset($_POST['addRequest']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true ){
      $today = strtotime("$payment_date midnight");
      $checS=$d->selectRow("virtual_wallet,currency","society_master","society_id='$society_id'");
      $sData=mysqli_fetch_array($checS);
      $virtual_wallet= $sData['virtual_wallet'];
      $currency= $sData['currency'];

      $block_id=$d->getBlockid($user_id);
      $fcmArray=$d->selectAdminBlockwise($notiType,$block_id,"android");
      $fcmArrayIos=$d->selectAdminBlockwise($notiType,$block_id,"ios");


      
        $q=$d->selectRow("penalty_amount","penalty_master","penalty_id='$penalty_id'","");
        $pData=mysqli_fetch_array($q);
        $penalty_amount=$pData['penalty_amount'];
        if ($penalty_amount>$payment_amount) {
            $response["message"]="Minimum $currency $penalty_amount amount required !";
            $response["status"]='201';
            echo json_encode($response);
            exit();
        }
        $notiType= 12;
        $notiAdminUrl= "penalties?type=1";
        $qcq= $d->selectRow("payment_request_id","payment_paid_request","unit_id='$unit_id' AND user_id='$user_id' AND penalty_id='$penalty_id'");
        $title= "Penalty Payment Paid Request";



     
      if (mysqli_num_rows($qcq)>0) {
        $response["message"]="Your payment request is already sent !";
        $response["status"]='201';
        echo json_encode($response);
        exit();
      }


      $uploadedFile = $_FILES["payment_photo"]["tmp_name"];
      $ext = pathinfo($_FILES['payment_photo']['name'], PATHINFO_EXTENSION);
      if (file_exists($uploadedFile)) {

          $sourceProperties = getimagesize($uploadedFile);
          $newFileName = rand() . $user_id;
          $dirPath = "../img/request/";
          $imageType = $sourceProperties[2];
          $imageHeight = $sourceProperties[1];
          $imageWidth = $sourceProperties[0];
          if ($imageWidth > 1000) {
              $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
              $newImageWidth = $imageWidth * $newWidthPercentage / 100;
              $newImageHeight = $imageHeight * $newWidthPercentage / 100;
          } else {
              $newImageWidth = $imageWidth;
              $newImageHeight = $imageHeight;
          }
          switch ($imageType) {

              case IMAGETYPE_PNG:
                  $imageSrc = imagecreatefrompng($uploadedFile);
                  $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                  imagepng($tmp, $dirPath . $newFileName . "_payment_request." . $ext);
                  break;

              case IMAGETYPE_JPEG:
                  $imageSrc = imagecreatefromjpeg($uploadedFile);
                  $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                  imagejpeg($tmp, $dirPath . $newFileName . "_payment_request." . $ext);
                  break;

              case IMAGETYPE_GIF:
                  $imageSrc = imagecreatefromgif($uploadedFile);
                  $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                  imagegif($tmp, $dirPath . $newFileName . "_payment_request." . $ext);
                  break;

              default:

                  break;
          }
          $payment_photo = $newFileName . "_payment_request." . $ext;

          $notiUrl = $base_url . 'img/request/' . $payment_photo;
      } else {
          $payment_photo = '';
          $notiUrl = "";
      }

     $payment_date=  date('Y-m-d',strtotime($payment_date));
    
     $m->set_data('society_id',$society_id);
     $m->set_data('unit_id',$unit_id);
     $m->set_data('user_id',$user_id);
     $m->set_data('user_type',$user_type);
     $m->set_data('payment_amount',$payment_amount);
     $m->set_data('payment_photo',$payment_photo);
     $m->set_data('maintenance_id',$maintenance_id);
     $m->set_data('receive_maintenance_id',$receive_maintenance_id);
     $m->set_data('bill_master_id',$bill_master_id);
     $m->set_data('receive_bill_id',$receive_bill_id);
     $m->set_data('payment_date',$payment_date);
     $m->set_data('penalty_id',$penalty_id);
     $m->set_data('payment_type',$payment_type);
     $m->set_data('bank_name',$bank_name);
     $m->set_data('cheque_number',$cheque_number);
     $m->set_data('remark',$remark);
     $m->set_data('request_type',$request_type);
     $m->set_data('created_date',date("Y-m-d H:i:s"));

     $a = array(
      'society_id'=>$m->get_data('society_id'),
      'unit_id'=>$m->get_data('unit_id'),
      'user_id'=>$m->get_data('user_id'),
      'user_type'=>$m->get_data('user_type'),
      'payment_amount'=>$m->get_data('payment_amount'),
      'payment_photo'=>$m->get_data('payment_photo'),
      'maintenance_id'=>$m->get_data('maintenance_id'),
      'receive_maintenance_id'=>$m->get_data('receive_maintenance_id'),
      'bill_master_id'=>$m->get_data('bill_master_id'),
      'receive_bill_id'=>$m->get_data('receive_bill_id'),
      'payment_date'=>$m->get_data('payment_date'),
      'penalty_id'=>$m->get_data('penalty_id'),
      'payment_type'=>$m->get_data('payment_type'),
      'bank_name'=>$m->get_data('bank_name'),
      'cheque_number'=>$m->get_data('cheque_number'),
      'remark'=>$m->get_data('remark'),
      'created_date'=>$m->get_data('created_date'),
    );

    $q1=$d->insert("payment_paid_request",$a);

      if ($q1>0) {
         
          
          $description= "for $paid_for by $user_name";

        


        $nAdmin->noti_new($society_id,"",$fcmArray,$title,$description,$notiAdminUrl);
        $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,$title,$description,$notiAdminUrl);
              
          $notiAry = array(
              'society_id'=>$society_id,
              'notification_tittle'=>$title,
              'notification_description'=>$description,
              'notifiaction_date'=>date('Y-m-d H:i'),
              'notification_action'=>$notiAdminUrl,
              'admin_click_action'=>$notiAdminUrl,
               'notification_logo'=>'MyBillsNew.png',
            );
            
            $d->insert("admin_notification",$notiAry);

         $response["message"]="Payment Request Sent Successfully!";
         $response["status"]='200';
         echo json_encode($response);
      }else{
        $response["message"]="Soenthing Wrong.";
        $response["status"]='201';
        echo json_encode($response);
      }


  }else if(isset($_POST['deleteRequest']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($payment_request_id, FILTER_VALIDATE_INT) == true){

      $q = $d->delete("payment_paid_request","payment_request_id='$payment_request_id' ");
      if ($q>0) {
        $response["message"]="Payment Request Cancelled Successfully!";
         $response["status"]='200';
         echo json_encode($response);
      } else {
        $response["message"]="Soenthing Wrong.";
        $response["status"]='201';
        echo json_encode($response);
      }

  }else{
      $response["message"]="wrong tag";
      $response["status"]="201";
      echo json_encode($response);
    }
  }
    else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
