<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));

        if ($_POST['getAbout'] == "getAbout" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {

           $qA = $d->select("users_master", "user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id' AND about_business!=''","");

            if (mysqli_num_rows($qA) > 0) {
                  
                  
                $data = mysqli_fetch_array($qA);

                $response["about_business"] = $data['about_business'];
                $response["message"] = "Get About success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No About us Found";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } 


        if ($_POST['addAbout'] == "addAbout" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
            
            $m->set_data('about_business', $about_business);

            $a = array(
                'about_business' => $m->get_data('about_business'),
            );

            $q = $d->update("users_master",$a,"user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'");

            if ($q == true) {
                $response["message"] = "Profile Updated";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }

        if ($_POST['changePrivacy'] == "changePrivacy" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ) {
            
            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }

            $m->set_data('public_mobile', $public_mobile);
            $a = array('public_mobile' => $m->get_data('public_mobile'));
            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' OR  user_id='$user_id' AND society_id='$society_id' $appendQuery");

            if ($q == true) {
                if ($public_mobile==1) {
                    $response["message"] = "Mobile number is private";
                } else {
                    $response["message"] = "Mobile number is public";
                }
                // $response["message"] = "Mobile Privacy Changed ";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }

        if ($_POST['changePrivacyGatekeeper'] == "changePrivacyGatekeeper" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ) {

            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }
            
            $m->set_data('mobile_for_gatekeeper', $mobile_for_gatekeeper);
            $a = array('mobile_for_gatekeeper' => $m->get_data('mobile_for_gatekeeper'));
            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' OR user_id='$user_id' AND society_id='$society_id' $appendQuery");

            if ($q == true) {
                if ($mobile_for_gatekeeper==1) {
                    $response["message"] = "Mobile number is private for gatekeeper";
                } else {
                    $response["message"] = "Mobile number is public for gatekeeper";
                }
                // $response["message"] = "Mobile Privacy Changed ";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }

        if ($_POST['changePrivacyVisitor'] == "changePrivacyVisitor" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
            
            $m->set_data('visitor_approved', $visitor_approved);

            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }

            $a = array('visitor_approved' => $m->get_data('visitor_approved'));
             if ($visitor_approved!=1) {
                $qnotification=$d->select("visitors_master,users_master","(visitors_master.visitor_status=0 AND users_master.user_id=visitors_master.user_id AND  (visitors_master.user_id='$user_id' AND visitors_master.visitor_status != 5 AND visitors_master.society_id='$society_id') AND  (visitors_master.visitor_type = 0 OR visitors_master.visitor_type =2 or visitors_master.visitor_type =3))","ORDER BY visitors_master.visitor_id DESC LIMIT 200");
                $totalVisitor=mysqli_num_rows($qnotification);
                while ($viData=mysqli_fetch_array($qnotification)) {

                    $visitor_id = $viData['visitor_id'];

                    $m->set_data('visitor_status','1');
               
                    $a11 = array(
                        'visitor_status'=>$m->get_data('visitor_status')
                    );
        
                    $d->update("visitors_master",$a11,"parent_visitor_id='$visitor_id' OR visitor_id='$visitor_id'");
                }

            }
            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' OR  user_id='$user_id' AND society_id='$society_id' $appendQuery");

            if ($q == true) {
                if ($visitor_approved==1) {
                    $response["message"] = "Visitor approval on";
                } else {

                    $response["message"] = "Visitor approval off ";
                }
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }

        if ($_POST['changePrivacyVisitorGroup'] == "changePrivacyVisitorGroup" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {


            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }
            
            $m->set_data('group_visitor_approval', $visitor_approved);

            $a = array('Delivery_cab_approval' => $m->get_data('group_visitor_approval'));

            if ($visitor_approved!=1) {
                $qnotification=$d->select("visitors_master,users_master","(visitors_master.visitor_status=0 AND users_master.user_id=visitors_master.user_id AND  (visitors_master.user_id='$user_id' AND visitors_master.visitor_status != 5 AND visitors_master.society_id='$society_id') AND  (visitors_master.visitor_type = 0 OR visitors_master.visitor_type =2 or visitors_master.visitor_type =3))","ORDER BY visitors_master.visitor_id DESC LIMIT 200");
                $totalVisitor=mysqli_num_rows($qnotification);
                while ($viData=mysqli_fetch_array($qnotification)) {

                    $visitor_id = $viData['visitor_id'];

                    $m->set_data('visitor_status','1');
               
                    $a11 = array(
                        'visitor_status'=>$m->get_data('visitor_status')
                    );
        
                    $d->update("visitors_master",$a11,"parent_visitor_id='$visitor_id' OR visitor_id='$visitor_id'");
                }

            }

            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' OR  user_id='$user_id' AND society_id='$society_id' $appendQuery");

            if ($q == true) {
                if ($visitor_approved==1) {
                    $response["message"] = "Visitor group approval on";
                } else {
                    $response["message"] = "Visitor group approval off";
                }
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }

        if ($_POST['changePrivacySos'] == "changePrivacySos" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ) {
            
            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }

            $m->set_data('sos_alert', $sos_alert);
            $a = array('sos_alert' => $m->get_data('sos_alert'));
            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' OR  user_id='$user_id' AND society_id='$society_id' $appendQuery");

            if ($q == true) {
                if ($sos_alert==1) {
                    $response["message"] = "SOS alerts is off";
                } else {
                    $response["message"] = "SOS alerts is on";
                }
                // $response["message"] = "SOS Alert Setting Changed ";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }

         

        if ($_POST['changeTimelineNotification'] == "changeTimelineNotification" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ) {
            
            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }

            $m->set_data('timeline_notification_on', $timeline_notification_on);
            $a = array('timeline_notification_on' => $m->get_data('timeline_notification_on'));
            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' OR  user_id='$user_id' AND society_id='$society_id' $appendQuery");

            if ($q == true) {
                if ($timeline_notification_on==1) {
                    $response["message"] = "Timeline notification is OFF";
                } else {
                    $response["message"] = "Timeline notification is ON";
                }
                // $response["message"] = "SOS Alert Setting Changed ";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }

         if ($_POST['changePrivacyTenant'] == "changePrivacyTenant" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ) {
            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }


            $m->set_data('tenant_view', $tenant_view);
            $a = array('tenant_view' => $m->get_data('tenant_view'));
            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' OR  user_id='$user_id' AND society_id='$society_id' $appendQuery");

            if ($q == true) {
                if ($tenant_view==1) {
                    $response["message"] = "Your profile is private for tenant";
                } else {
                    $response["message"] = "Your profile is public for tenant";
                }
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }

        if ($_POST['changeDOBPrivacy'] == "changeDOBPrivacy" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ) {
            
            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }

            $m->set_data('dob_view', $dob_view);
            $a = array('dob_view' => $m->get_data('dob_view'));
            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' OR  user_id='$user_id' AND society_id='$society_id' $appendQuery");

            if ($q == true) {
                if ($dob_view==1) {
                    $response["message"] = "Your birth date is private";
                } else {
                    $response["message"] = "Your birth date is public";
                }
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }


        if ($_POST['changeChildSecurityPrivacy'] == "changeChildSecurityPrivacy" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }
            
            $m->set_data('child_gate_approval', $child_gate_approval);

            $a = array('child_gate_approval' => $m->get_data('child_gate_approval'));

            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' OR  user_id='$user_id' AND society_id='$society_id' $appendQuery");

            if ($q == true) {
                if ($child_gate_approval==0) {
                    $response["message"] = "Child gate exit approval on";
                } else {
                    $response["message"] = "Child gate exit approval off";
                }
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }

    } else {
        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 