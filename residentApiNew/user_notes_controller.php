<?php
include_once 'lib.php';


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
	$response = array();
	extract(array_map("test_input" , $_POST));

    if($_POST['getNotes']=="getNotes" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

           
        $q=$d->select("user_notes","society_id='$society_id' AND user_id='$user_id'","ORDER BY note_id ASC");
          
         
        if(mysqli_num_rows($q)>0){

            $response["notes"] = array();


            while($data_bill_list=mysqli_fetch_array($q)) {
                  
                     $notes = array(); 
                     $notes["note_id"]=$data_bill_list['note_id'];
                     $notes["user_id"]=$data_bill_list['user_id'];
                     $notes["admin_id"]=$data_bill_list['admin_id'];
                     $notes["society_id"]=$data_bill_list['society_id'];
                     $notes["note_title"]=html_entity_decode($data_bill_list['note_title']);
                     $notes["note_description"]=html_entity_decode($data_bill_list['note_description']);
                     $notes["created_date"]=date("d M Y", strtotime($data_bill_list['created_date']));
                     if ($data_bill_list['share_with_admin']==1) {
                     $notes["share_with_admin"]=true;
                     } else {
                     $notes["share_with_admin"]=false;
                     }
                     array_push($response["notes"], $notes);

            
            }  

           
             $response["message"]="Get  Notes Successfully...";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Notes Found.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else if($_POST['addNote']=="addNote" && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('note_title',$note_title);
            $m->set_data('note_description',$note_description);
            $m->set_data('share_with_admin',$share_with_admin);
            $m->set_data('created_date',date("Y-m-d H:i:s"));



               $a1 = array(
                    'society_id'=>$m->get_data('society_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'note_title'=>$m->get_data('note_title'),
                    'note_description'=>$m->get_data('note_description'),
                    'share_with_admin'=>$m->get_data('share_with_admin'),
                    'created_date'=>$m->get_data('created_date')
                );


              $q1=$d->insert("user_notes",$a1);

            if ($q1==TRUE) {

                 $response["message"]="Note Added Successfully !";
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }

    }else if($_POST['editNote']=="editNote"  && filter_var($note_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($society_id, FILTER_VALIDATE_INT) == true){

             
            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('note_title',$note_title);
            $m->set_data('note_description',$note_description);
            $m->set_data('share_with_admin',$share_with_admin);
            $m->set_data('created_date',date("Y-m-d H:i:s"));



           $a1 = array(
                'society_id'=>$m->get_data('society_id'),
                'user_id'=>$m->get_data('user_id'),
                'note_title'=>$m->get_data('note_title'),
                'note_description'=>$m->get_data('note_description'),
                'share_with_admin'=>$m->get_data('share_with_admin'),
                'created_date'=>$m->get_data('created_date')
            );



              $q1=$d->update("user_notes",$a1,"note_id='$note_id' AND user_id='$user_id'");

            if ($q1==TRUE) {

                 $response["message"]="Note Updated Successfully!";
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }

    }else if($_POST['deleteNote']=="deleteNote" && filter_var($note_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){


            $q1=$d->delete("user_notes","note_id='$note_id' AND user_id='$user_id'");

            if ($q1==TRUE) {

                 $response["message"]="Note Deleted Successfully !";
                 $response["due_date"]= date('d M Y',strtotime($due_date));
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }

    }else{
      $response["message"]="wrong tag";
      $response["status"]="201";
      echo json_encode($response);
    }
  }
    else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
