<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST) && !empty($_POST)) {

	if ($key == $keydb && $auth_check=='true') {

		$response = array();
		extract(array_map("test_input", $_POST));
		$todayDate = date('Y-m-d');

		if ($_POST['getDimensionalList']=="getDimensionalList" && $user_id != ''  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$qry = $d->selectRow("
				dimensional_master.*,
				pms_schedule_master.schedule_date,
				pms_schedule_master.pms_schedule_id,
				pms_schedule_master.schedule_status,
				pms_schedule_master.pms_type_on_schedule,
				performance_dimensional_assign.day,				
				(SELECT count(attribute_id) FROM attribute_master WHERE attribute_master.dimensional_id = dimensional_master.dimensional_id) AS totalAttributes
				","
				dimensional_master,
				pms_schedule_master,
				performance_dimensional_assign
				","
				pms_schedule_master.user_id = '$user_id' 
				AND pms_schedule_master.society_id = '$society_id' 
				AND performance_dimensional_assign.dimensional_id = dimensional_master.dimensional_id
				AND performance_dimensional_assign.dimensional_id = pms_schedule_master.dimensional_id
				AND dimensional_master.dimensional_id = pms_schedule_master.dimensional_id");

			$response["dimensional_upcoming_list"] = array();			
			$response["dimensional_complete_list"] = array();	

			$totalPendingReview =	$d->selectRow("
				dimensional_master.*,
				pms_schedule_master.schedule_date,
				pms_schedule_master.pms_schedule_id,
				pms_schedule_master.schedule_status,
				pms_schedule_master.user_id,
				users_master.user_full_name,
				users_master.user_profile_pic,
				users_master.user_designation,
				(SELECT count(attribute_id) FROM attribute_master WHERE attribute_master.dimensional_id = dimensional_master.dimensional_id) AS totalAttributes
				","
				dimensional_master,
				pms_schedule_master,
				users_master,
				employee_level_master
				","
				pms_schedule_master.society_id = '$society_id' 
				AND dimensional_master.dimensional_id = pms_schedule_master.dimensional_id
				AND pms_schedule_master.schedule_status = 1
				AND pms_schedule_master.reviewer_id = 0
				AND pms_schedule_master.user_id = users_master.user_id
				AND users_master.level_id = employee_level_master.level_id
				AND pms_schedule_master.user_level_id = users_master.level_id
				AND employee_level_master.parent_level_id = '$level_id'");

			$response['pending_review'] = mysqli_num_rows($totalPendingReview).'';

			if (mysqli_num_rows($qry) > 0) {
								
				while($data = mysqli_fetch_array($qry)){

					$data = array_map("html_entity_decode", $data);
					
					$dimensional = array();

					$dimensional['dimensional_id'] = $data['dimensional_id'];
					$dimensional['pms_schedule_id'] = $data['pms_schedule_id'];
					$dimensional['dimensional_name'] = $data['dimensional_name'];
					$dimensional['dimensional_weightage'] = $data['dimensional_weightage'];
					$dimensional['total_attributes'] = $data['totalAttributes'];
					$dimensional['schedule_status'] = $data['schedule_status'];
					$dimensional['pms_type'] = $data['pms_type_on_schedule'];
					$dimensional['day'] = $data['day'];

					if ($data['pms_type_on_schedule'] == 0) {
						$dimensional['pms_type_view'] = "Weekly";
					}else if ($data['pms_type_on_schedule'] == 1) {
						$dimensional['pms_type_view'] = "Monthly";
					}else if ($data['pms_type_on_schedule'] == 2) {
						$dimensional['pms_type_view'] = "Quarterly";
					}else if ($data['pms_type_on_schedule'] == 3) {
						$dimensional['pms_type_view'] = "Half Yearly";
					}else if ($data['pms_type_on_schedule'] == 4) {
						$dimensional['pms_type_view'] = "Yearly";
					}
					
					$dimensional['dimensional_created_date'] = date('d-m-Y',strtotime($data['dimensional_created_date']));

					$scheduleDate = $data['schedule_date'];
					$dimensional['schedule_date'] = date('d-m-Y',strtotime($scheduleDate));
					$dimensional['schedule_date_view'] = date('D, dS M Y',strtotime($scheduleDate));
					$dimensional['is_today'] = ($scheduleDate == $todayDate) ? true : false;
					$dimensional['is_date_gone'] = ($scheduleDate < $todayDate) ? true : false;
					$dimensional['dimensional_image'] = ($data['dimensional_image'] != '') ? $base_url . "img/dimensionals/" .  $data['dimensional_image'] : "";

					if ($dimensional['schedule_status'] == "0") {
						array_push($response["dimensional_upcoming_list"],$dimensional);
					}else{
						array_push($response["dimensional_complete_list"],$dimensional);						
					}
				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data found";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if ($_POST['getDimensionalListForReview']=="getDimensionalListForReview" && $user_id != ''  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$qry = $d->selectRow("
				dimensional_master.*,
				pms_schedule_master.schedule_date,
				pms_schedule_master.pms_schedule_id,
				pms_schedule_master.schedule_status,
				pms_schedule_master.user_id,
				users_master.user_full_name,
				users_master.user_profile_pic,
				users_master.user_designation,
				(SELECT count(attribute_id) FROM attribute_master WHERE attribute_master.dimensional_id = dimensional_master.dimensional_id) AS totalAttributes
				","
				dimensional_master,
				pms_schedule_master,
				users_master,
				employee_level_master
				","
				pms_schedule_master.society_id = '$society_id' 
				AND dimensional_master.dimensional_id = pms_schedule_master.dimensional_id
				AND pms_schedule_master.schedule_status = 1
				AND pms_schedule_master.reviewer_id = 0
				AND pms_schedule_master.user_id = users_master.user_id
				AND users_master.level_id = employee_level_master.level_id
				AND pms_schedule_master.user_level_id = users_master.level_id
				AND employee_level_master.parent_level_id = '$level_id'");

			if (mysqli_num_rows($qry) > 0) {

				$response["pms_review_list"] = array();
								
				while($data = mysqli_fetch_array($qry)){

					$data = array_map("html_entity_decode", $data);

					$dimensional = array();

					$dimensional['dimensional_id'] = $data['dimensional_id'];
					$dimensional['pms_schedule_id'] = $data['pms_schedule_id'];
					$dimensional['dimensional_name'] = $data['dimensional_name'];
					$dimensional['dimensional_weightage'] = $data['dimensional_weightage'];
					$dimensional['total_attributes'] = $data['totalAttributes'];
					$dimensional['schedule_status'] = $data['schedule_status'];
					$dimensional['user_id'] = $data['user_id'];
					$dimensional['user_full_name'] = $data['user_full_name'];
					$dimensional['user_designation'] = $data['user_designation'];

					if ($data['user_profile_pic'] != '') {
                        $dimensional["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $dimensional["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }
					
					$scheduleDate = $data['schedule_date'];
					$dimensional['schedule_date'] = date('d-m-Y',strtotime($scheduleDate));
					$dimensional['schedule_date_view'] = date('D, dS M Y',strtotime($scheduleDate));
					$dimensional['dimensional_image'] = ($data['dimensional_image'] != '') ? $base_url . "img/dimensionals/" .  $data['dimensional_image'] : "";

					array_push($response["pms_review_list"],$dimensional);
					
				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data found";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if ($_POST['getPMSAttributes']=="getPMSAttributes" && $user_id != ''  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$qry = $d->select("attribute_master","society_id = '$society_id' AND dimensional_id = '$dimensional_id' AND attribute_status = '0'");

			if (mysqli_num_rows($qry) > 0) {
				$response["attributes"] = array();			
							
				while($data = mysqli_fetch_array($qry)){

					$data = array_map("html_entity_decode", $data);

					$attributes = array();

					$attributes['attribute_id'] = $data['attribute_id'];
					$attributes['attribute_name'] = $data['attribute_name'];
					$attributes['attribute_type'] = $data['attribute_type'];

					$attributes['attribute_created_date'] = date('d-m-Y',strtotime($data['attribute_created_date']));
					array_push($response["attributes"],$attributes);
				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data found";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if ($_POST['getSubmittedPMS']=="getSubmittedPMS" && $user_id != ''  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$qry=$d->selectRow("
				pms_schedule_master.*,
				users_master.user_id,
				users_master.user_full_name,
				employee_level_master.level_name,
				dimensional_master.dimensional_name
				","
				pms_schedule_master,
				users_master,
				employee_level_master,
				dimensional_master
				","
				pms_schedule_master.dimensional_id=dimensional_master.dimensional_id 
				AND pms_schedule_master.user_id = users_master.user_id 
				AND pms_schedule_master.user_level_id = employee_level_master.level_id 
				AND pms_schedule_master.society_id='$society_id' 
				AND pms_schedule_master.schedule_status='1' 
				AND users_master.user_id='$user_id'");

			if (mysqli_num_rows($qry) > 0) {

				$response["my_pms"] = array();			
								
				while($data = mysqli_fetch_array($qry)){

					$data = array_map("html_entity_decode", $data);

					$myPMS = array();

					$myPMS['dimensional_name'] = $data['dimensional_name'];
					$myPMS['schedule_date'] = date('dS M Y',strtotime($data['schedule_date']));

					if($data['pms_type'] == 0){
						$myPMS['report_type'] = 'Weekly';
					}else if($data['pms_type'] == 1){
						$myPMS['report_type'] =  'Monthly';
					}else if($data['pms_type'] == 2){
						$myPMS['report_type'] =  'Quarterly';
					}else if($data['pms_type'] == 3){
						$myPMS['report_type'] =  'Half Yearly';
					}else if($data['pms_type'] == 4){
						$myPMS['report_type'] =  'Yearly';
					}
					array_push($response["my_pms"],$myPMS);

				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data found";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if ($_POST['getSubmittedPMSAnswers']=="getSubmittedPMSAnswers" && $user_id != ''  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$q=$d->select("pms_answer_master","pms_answer_master.society_id='$society_id' AND pms_answer_master.pms_schedule_id='$pms_schedule_id'");

			if(mysqli_num_rows($q) > 0){

				$qry= $d->selectRow("
					pms_schedule_master.*,
					users_master.user_id,
					users_master.user_full_name,
					employee_level_master.level_name,
					dimensional_master.dimensional_name,
					dimensional_master.dimensional_weightage,
					ru.user_full_name AS reviewer_name,
					rl.level_name AS reviewer_level
					","
					pms_schedule_master LEFT JOIN users_master ru ON ru.user_id = pms_schedule_master.reviewer_id 
					LEFT JOIN employee_level_master rl ON rl.level_id = pms_schedule_master.reviewer_level_id,
					users_master, 
					employee_level_master, 
					dimensional_master
					","
					pms_schedule_master.dimensional_id = dimensional_master.dimensional_id 
					AND pms_schedule_master.user_id = users_master.user_id 
					AND pms_schedule_master.user_level_id = employee_level_master.level_id 
					AND pms_schedule_master.society_id = '$society_id'
					AND pms_schedule_master.schedule_status = '1' 
					AND pms_schedule_master.pms_schedule_id='$pms_schedule_id'");


				$row=mysqli_fetch_array($qry);

				$row = array_map("html_entity_decode", $row);

				$response['dimensional_name'] = $row['dimensional_name'];
				$response['dimensional_weightage'] = $row['dimensional_weightage'];
				$response['schedule_date'] = $row['schedule_date'];
				$response['schedule_submit_date'] = date('dS M Y h:i A', strtotime($row['schedule_submit_date']));

				if ($row['reviewer_name'] != '') {
					$response['reviewer_name'] = $row['reviewer_name'].' ('.$row['reviewer_level'].')';
				}else{
					$response['reviewer_name'] = "";					
				}

				if ($row['review_date'] != '') {
					$response['review_date'] = date('d M Y h:i A', strtotime($row['review_date']));
				}else{
					$response['review_date'] = "";					
				}

				if (mysqli_num_rows($q) > 0) {

					$response["attributes"] = array();			
									
					while($data = mysqli_fetch_array($q)){

						$data = array_map("html_entity_decode", $data);

						$attributes = array();

						$attributes['pms_answer_id'] = $data['pms_answer_id'];
						$attributes['attribute_name'] = $data['attribute_name'];
						$attributes['attribute_type'] = $data['attribute_type'];
						$attributes['attribute_employee_answer'] = $data['attribute_employee_answer'];
						$attributes['attribute_senior_answer'] = $data['attribute_senior_answer'];

						if ($data['attribute_employee_answer_date'] != '' && $data['attribute_employee_answer_date'] != "0000-00-00 00:00:00") {
							$attributes['attribute_employee_answer_date'] = date('d M Y, h:i A', strtotime($data['attribute_employee_answer_date']));
						}else{
							$attributes['attribute_employee_answer_date'] = "";					
						}

						if ($data['attribute_senior_answer_date'] != '' && $data['attribute_senior_answer_date'] != "0000-00-00 00:00:00") {
							$attributes['attribute_senior_answer_date'] = date('d M Y, h:i A', strtotime($data['attribute_senior_answer_date']));
						}else{
							$attributes['attribute_senior_answer_date'] = "";					
						}			
				
						array_push($response["attributes"],$attributes);
					}

					$response["message"] = "Success";
					$response["status"] = "200";
					echo json_encode($response);
				}else{
					$response["message"] = "No data found";
					$response["status"] = "201";
					echo json_encode($response);
				}
			}
		}else if ($_POST['submitPMS']=="submitPMS" && $user_id != ''  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			if ($level_id == 0 || $level_id == '') {
				$response["message"] = "Level not assigned";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}

			$scheduled_date = date('Y-m-d',strtotime($scheduled_date));

			if ($pmsType == 0) {
				$next_scheduled_date = date('Y-m-d', strtotime('+7 days',strtotime($scheduled_date)));
			}else if ($pmsType == 1) {
				$next_scheduled_date = date('Y-m-d', strtotime('+1 month',strtotime($scheduled_date)));
			}else if ($pmsType == 2) {
				$next_scheduled_date = date('Y-m-d', strtotime('+3 month',strtotime($scheduled_date)));
			}else if ($pmsType == 3) {
				$next_scheduled_date = date('Y-m-d', strtotime('+6 month',strtotime($scheduled_date)));
			}else if ($pmsType == 4) {
				$next_scheduled_date = date('Y-m-d', strtotime('+1 year',strtotime($scheduled_date)));
			}

			// Add Next Schedule Date

			$m->set_data('society_id',$society_id);
			$m->set_data('user_id',$user_id);
			$m->set_data('schedule_date',$next_scheduled_date);
			$m->set_data('dimensional_id',$dimensional_id);
			$m->set_data('schedule_status',"0");
			$m->set_data('user_level_id',$level_id);
			$m->set_data('pms_type_on_schedule',$pmsType);
			$m->set_data('schedule_created_date',date('Y-m-d H:i:s'));

            $a2 = array(
                'society_id' =>$m->get_data('society_id'),
                'user_id' =>$m->get_data('user_id'),
                'schedule_date' =>$m->get_data('schedule_date'),
                'dimensional_id' =>$m->get_data('dimensional_id'),
                'schedule_status' =>$m->get_data('schedule_status'),
                'user_level_id' =>$m->get_data('user_level_id'),
                'pms_type_on_schedule' =>$m->get_data('pms_type_on_schedule'),
                'schedule_created_date' =>$m->get_data('schedule_created_date'),
            );

            $qry = $d->insert("pms_schedule_master",$a2);

            // Update Schedule

            $m->set_data('schedule_status',"1");
			$m->set_data('schedule_submit_date',date('Y-m-d H:i:s'));

            $a1 = array(
                'schedule_status' =>$m->get_data('schedule_status'),
                'schedule_submit_date' =>$m->get_data('schedule_submit_date'),
            );

            $qry = $d->update("pms_schedule_master",$a1,"pms_schedule_id = '$pms_schedule_id'");

            // Add PMS Answer

			$attribute_name = explode("~",$attribute_name);
            $attribute_type = explode("~",$attribute_type);
            $attribute_employee_answer = explode("~",$attribute_employee_answer);

            for ($i=0; $i < count($attribute_name); $i++) {

            
                $m->set_data('society_id',$society_id);
                $m->set_data('pms_schedule_id',$pms_schedule_id);
                $m->set_data('dimensional_name',$dimensional_name);
                $m->set_data('attribute_name',$attribute_name[$i]);
                $m->set_data('attribute_type',$attribute_type[$i]);
                $m->set_data('attribute_employee_answer',$attribute_employee_answer[$i]);
                $m->set_data('attribute_employee_id',$user_id);
                $m->set_data('scheduled_date',$scheduled_date);
                $m->set_data('attribute_employee_answer_date',date('Y-m-d H:i:s'));

                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'pms_schedule_id' =>$m->get_data('pms_schedule_id'),
                    'dimensional_name' =>$m->get_data('dimensional_name'),
                    'attribute_name' =>$m->get_data('attribute_name'),
                    'attribute_type' =>$m->get_data('attribute_type'),
                    'attribute_employee_id' =>$m->get_data('attribute_employee_id'),
                    'attribute_employee_answer' =>$m->get_data('attribute_employee_answer'),
                    'scheduled_date' =>$m->get_data('scheduled_date'),
                    'attribute_employee_answer_date' =>$m->get_data('attribute_employee_answer_date'),
                );

                $qry = $d->insert("pms_answer_master",$a);
            }

			$response["message"] = "Successfully submitted";
			$response["status"] = "200";
			echo json_encode($response);
		}else if ($_POST['submitReviewerPMS']=="submitReviewerPMS" && $reviewer_id != ''  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {
			
            // Update Schedule

            $m->set_data('reviewer_id',$reviewer_id);
            $m->set_data('reviewer_level_id',$reviewer_level_id);
            $m->set_data('dimensional_weightage',$dimensional_weightage);
            $m->set_data('reviewer_single_weightage',$reviewer_single_weightage);
            $m->set_data('reviewer_total_weightage',$reviewer_total_weightage);
			$m->set_data('review_date',date('Y-m-d H:i:s'));

            $a1 = array(
                'reviewer_id' =>$m->get_data('reviewer_id'),
                'reviewer_level_id' =>$m->get_data('reviewer_level_id'),
                'dimensional_weightage' =>$m->get_data('dimensional_weightage'),
                'reviewer_single_weightage' =>$m->get_data('reviewer_single_weightage'),
                'reviewer_total_weightage' =>$m->get_data('reviewer_total_weightage'),
                'review_date' =>$m->get_data('review_date'),
            );

            $qry = $d->update("pms_schedule_master",$a1,"pms_schedule_id = '$pms_schedule_id'");

            // Add PMS Answer

            $attribute_senior_answer = explode("~",$attribute_senior_answer);
            $pms_answer_id = explode("~",$pms_answer_id);

            for ($i=0; $i < count($pms_answer_id); $i++) {

            	$answerId = $pms_answer_id[$i];
            
                $m->set_data('attribute_senior_id',$reviewer_id);
                $m->set_data('attribute_senior_answer',$attribute_senior_answer[$i]);
                $m->set_data('attribute_senior_answer_date',date('Y-m-d H:i:s'));

                $a = array(
                    'attribute_senior_id' =>$m->get_data('attribute_senior_id'),
                    'attribute_senior_answer' =>$m->get_data('attribute_senior_answer'),
                    'attribute_senior_answer_date' =>$m->get_data('attribute_senior_answer_date'),
                );

                $qry = $d->update("pms_answer_master",$a,"pms_answer_id = '$answerId'");
            }

			$response["message"] = "Review submitted";
			$response["status"] = "200";
			echo json_encode($response);
		}else {
			$response["message"] = "wrong tag.";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {

		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}
?>