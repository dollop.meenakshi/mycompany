<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {


	if ($key == $keydb && $auth_check=='true') {
	$response = array();
	extract(array_map("test_input", $_POST));

		if ($_POST['getBlockList'] == "getBlockList" && $society_id != '') {

			$sParking = $d->select("society_parking_master", "society_id='$society_id'");

			if (mysqli_num_rows($sParking) > 0) {

				$response["sBlocks"] = array();

				while ($s_partking_list = mysqli_fetch_array($sParking)) {

					$sBlocks = array();

					$sBlocks["society_parking_id"] = $s_partking_list['society_parking_id'];
					$sBlocks["socieaty_parking_name"] = $s_partking_list['socieaty_parking_name'];
					$sBlocks["society_parking_status"] = $s_partking_list['society_parking_status'];

					$allocatedCars = $d->count_data_direct("parking_id", "parking_master", "society_parking_id='$s_partking_list[society_parking_id]' AND parking_type=0 AND parking_status=1");

					$allocatedBikess = $d->count_data_direct("parking_id", "parking_master", "society_parking_id='$s_partking_list[society_parking_id]' AND parking_type=1 AND parking_status=1");

					$notAllocatedCars = $d->count_data_direct("parking_id", "parking_master", "society_parking_id='$s_partking_list[society_parking_id]' AND parking_type=0 AND parking_status=0");

					$notAllocatedBikess = $d->count_data_direct("parking_id", "parking_master", "society_parking_id='$s_partking_list[society_parking_id]' AND parking_type=1 AND parking_status=0");

					$sBlocks["total_car_parking"] = $s_partking_list['total_car_parking'];
					$sBlocks["allocate_cars"] = $allocatedCars;
					$sBlocks["not_allocate_cars"] = $notAllocatedCars;
					$sBlocks["total_bike_parking"] = $s_partking_list['total_bike_parking'];
					$sBlocks["allocate_bikes"] = $allocatedBikess;
					$sBlocks["not_allocate_bikes"] = $notAllocatedBikess;

					array_push($response["sBlocks"], $sBlocks);
				}

				$response["message"] = "$datafoundMsg";
				$response["status"] = "200";
				echo json_encode($response);
			} else {

				$response["message"] = "$noDatafoundMsg";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else if ($_POST['getParkingList'] == "getParkingList" && $society_id != '') {

			$sParking = $d->select("parking_master", "society_id='$society_id' AND society_parking_id='$society_parking_id'", "");

			if (mysqli_num_rows($sParking) > 0) {

				$response["cars"] = array();
				$response["bikes"] = array();

				while ($s_partking_list = mysqli_fetch_array($sParking)) {

					$car_bike = array();

					$car_bike["parking_id"] = $s_partking_list['parking_id'];
					$car_bike["parking_name"] = $s_partking_list['parking_name'];
					$car_bike["vehicle_no"] = $s_partking_list['vehicle_no'];
					$car_bike["unit_id"] = $s_partking_list['unit_id'];
					$car_bike["parking_status"] = $s_partking_list['parking_status'];

					

					if ($s_partking_list['parking_type'] == "0") {
						array_push($response["cars"], $car_bike);
					} else {
						array_push($response["bikes"], $car_bike);
					}

				}

				$response["message"] = "$datafoundMsg";
				$response["status"] = "200";
				echo json_encode($response);
			} else {

				$response["message"] = "$noDatafoundMsg";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else if ($_POST['parkingDelete'] == "parkingDelete" && $society_id != '') {

			$m->set_data('block_id', 0);
			$m->set_data('floor_id', 0);
			$m->set_data('unit_id', 0);
			$m->set_data('vehicle_no', "");
			$m->set_data('parking_status', 0);

			$a1 = array(
				'block_id' => $m->get_data('block_id'),
				'floor_id' => $m->get_data('floor_id'),
				'unit_id' => $m->get_data('unit_id'),
				'parking_status' => $m->get_data('parking_status'),
				'vehicle_no' => $m->get_data('vehicle_no'),
			);

			$q = $d->update("parking_master", $a1, "parking_id='$parking_id' AND society_id='$society_id'");

			if ($q == TRUE) {
				
				/*$qUserToken=$d->select("users_master","society_id='$society_id' AND unit_id='$unit_id'");
	            $data_notification=mysqli_fetch_array($qUserToken);
	            $sos_user_token=$data_notification['user_token'];
	            $device=$data_notification['device'];
	            if ($device=='android') {
	              $nResident->noti("","",$society_id,$sos_user_token,"Parking Un Allocated","by Admin",'myparking');
	            }  else if($device=='ios') {
	              $nResident->noti_ios("","",$society_id,$sos_user_token,"Parking Un Allocated","by Admin",'myparking');
	            }
	            
	          if ($user_id!=0) {
	            $notiAry = array(
	            'society_id'=>$society_id,
	            'user_id'=>$user_id,
	            'notification_title'=>'Parking Un Allocated',
	            'notification_desc'=>"by you",    
	            'notification_date'=>date('Y-m-d H:i'),
	            'notification_action'=>'myparking',
	            'notification_logo'=>'Parkingxxxhdpi.png',
	            );
	            $d->insert("user_notification",$notiAry);
	          }*/

	            $d->insert_myactivity($user_id,"$society_id","0","$user_name","My parking deleted","Parkingxxxhdpi.png");
                    
				$response["message"] = "$deleteMsg";
				$response["status"] = "200";
				echo json_encode($response);
			} else {

				$response["message"] = "$somethingWrong";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else if ($_POST['parkingAdd'] == "parkingAdd" && $society_id != '') {

			$m->set_data('block_id',$block_id);
	        $m->set_data('floor_id',$floor_id);
	        $m->set_data('unit_id',$unit_id);
	        $m->set_data('parking_status',2);
	        $m->set_data('vehicle_no',$vehicle_no);

	       $a1 =array(
	          'block_id'=> $m->get_data('block_id'),
	          'floor_id'=> $m->get_data('floor_id'),
	          'unit_id'=> $m->get_data('unit_id'),
	          'parking_status'=> $m->get_data('parking_status'),
	          'vehicle_no'=> $m->get_data('vehicle_no'),
	        );

	       $q=$d->update("parking_master",$a1,"parking_id='$parking_id' AND society_id='$society_id'");

			if ($q == TRUE) {

               $block_id=$d->getBlockid($user_id);
               $fcmArray=$d->selectAdminBlockwise("4",$block_id,"android");
               $fcmArrayIos=$d->selectAdminBlockwise("4",$block_id,"ios");

          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,"New Parking Request by $user_full_name", "for $parking_name Parking","parkings?pendingParking=yes");
                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,"New Resident Request by $user_full_name", "for $parking_name Parking","parkings?pendingParking=yes");

                    $notiAry = array(
                      'society_id'=>$society_id,
                      'notification_tittle'=>"New Parking Request by $user_full_name",
                      'notification_description'=>"For $parking_name Parking",
                      'notifiaction_date'=>date('Y-m-d H:i'),
                      'notification_action'=>"parkings?pendingParking=yes",
                      'admin_click_action '=>"parkings?pendingParking=yes",
                       'notification_logo'=>'Parkingxxxhdpi.png',
                    );

                 $d->insert("admin_notification",$notiAry);

                 $d->insert_myactivity($user_id,"$society_id","0","$user_name","New parking request send ","Parkingxxxhdpi.png");
                 
				$request_sent_successfully = $xml->string->request_sent_successfully;
                 
				$response["message"] = "$request_sent_successfully";
				$response["status"] = "200";
				echo json_encode($response);
			} else {

				$response["message"] = "Failed";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else {

			$response["message"] = "wrong tag.";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {
		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}?>