<?php

include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    //if ($key==$keydb && $auth_check=='true') {

        $response = array();
        extract(array_map("test_input" , $_POST));
        
        if($_POST['getDashboardData']=="getDashboardData" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            // ---------- Slider ----------

            $response["slider"] = array();

            $sliderQry = $d->select("company_home_slider_master","society_id='$society_id'");
            
            if (mysqli_num_rows($sliderQry) > 0) {
                
                while ($data = mysqli_fetch_array($sliderQry)) {

                    $slider = array();

                    $slider["company_home_slider_id"] = $data['company_home_slider_id'];

                    if ($data['company_home_slider_image'] != '') {
                        $slider["company_home_slider_image"] = $base_url.'img/front_slider/'.$data['company_home_slider_image'];
                    }else{
                        $slider["company_home_slider_image"]="";
                    }
                    
                    $slider["company_home_slider_title"] = $data['company_home_slider_title'];
                    $slider["company_home_slider_description"] = $data['company_home_slider_description'];
                    $slider["company_home_slider_url"] = $data['company_home_slider_url'];
                    $slider["company_home_slider_mobile"] = $data['company_home_slider_mobile'];

                    array_push($response["slider"], $slider);
                }
            } 

            // ---------- Home Details ----------

            $homeDetailQry = $d->selectRow("*","company_home_master","society_id='$society_id'");

            if (mysqli_num_rows($homeDetailQry) > 0) {
                $homeDetailsData = mysqli_fetch_array($homeDetailQry);

                $homeDetails = array(
                    "company_home_id" => $homeDetailsData['company_home_id'],
                    "company_home_title" => $homeDetailsData['company_home_title'],
                    "company_home_description" => $homeDetailsData['company_home_description']
                );

                $response['about_company'] = $homeDetails;
            }

            // ---------- Our Services ----------

            $response["our_services"] = array();

            $ourServiceQry = $d->select("company_service_master","society_id='$society_id'","LIMIT 5");

            if (mysqli_num_rows($ourServiceQry) > 0) {
                
                while ($serviceData = mysqli_fetch_array($ourServiceQry)) {

                    $service = array();

                    $service["company_service_id"] = $serviceData['company_service_id'];

                    if ($serviceData['company_service_image'] != '') {
                        $service["company_service_image"] = $base_url.'img/company_services/'.$serviceData['company_service_image'];
                    }else{
                        $service["company_service_image"]="";
                    }
                    
                    $service["company_service_name"] = $serviceData['company_service_name'];

                    $service['service_list'] = array();

                    $serviceListQry = $d->select("company_service_master,company_service_details_master,company_service_detail_list_master","company_service_details_master.company_service_id = company_service_master.company_service_id AND company_service_detail_list_master.company_service_details_id = company_service_details_master.company_service_details_id AND company_service_details_master.company_service_id = '$serviceData[company_service_id]'");

                    if (mysqli_num_rows($serviceListQry) > 0) {
                        while ($serviceListData = mysqli_fetch_array($serviceListQry)) {
                            $serviceList = array();

                            $serviceList['company_service_detail_list_id'] = $serviceListData['company_service_detail_list_id'];
                            $serviceList['company_service_detail_list_name'] = $serviceListData['company_service_detail_list_name'];

                            array_push($service['service_list'],$serviceList);
                        }
                    }

                    array_push($response["our_services"], $service);
                }
            }

            // ---------- Current Opening ----------

            $response["current_opening"] = array();

            $openingQry = $d->select("company_current_opening_master","society_id='$society_id' AND company_current_opening_status = '0'","LIMIT 5");
            
            if (mysqli_num_rows($openingQry) > 0) {
                
                while ($openingData = mysqli_fetch_array($openingQry)) {

                    $opening = array();

                    $opening["company_current_opening_id"] = $openingData['company_current_opening_id'];
                    $opening["company_current_opening_title"] = $openingData['company_current_opening_title'];
                    $opening["company_current_opening_position"] = $openingData['company_current_opening_position'];
                    $opening["company_current_opening_timing"] = $openingData['company_current_opening_timing'];
                    $opening["company_current_opening_address"] = $openingData['company_current_opening_address'];

                    array_push($response["current_opening"], $opening);
                }
            } 

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);

        }else{
            $response["message"]="wrong tag";
            $response["status"]="201";
            echo json_encode($response);
        }
    /*}else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }*/
}
?>