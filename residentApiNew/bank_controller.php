<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
        if($_POST['getBankList']=="getBankList" && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

            $response["list"] = array();

            $q = $d->select("user_bank_master","user_id='$user_id' AND floor_id = '$floor_id' AND block_id = '$block_id' AND is_delete = '0'");

            if (mysqli_num_rows($q) > 0) {

                while ($data = mysqli_fetch_array($q)) {
                    $list = array();
                    $list["bank_id"] = $data['bank_id'];
                    $list["account_no"] = $data['account_no'];
                    $list["ifsc_code"] = $data['ifsc_code'];
                    $list["bank_name"] = $data['bank_name'];
                    $list["bank_branch_name"] = $data['bank_branch_name'];
                    $list["account_type"] = $data['account_type'];
                    $list["crn_no"] = $data['crn_no'];
                    $list["pan_card_no"] = $data['pan_card_no'];
                    $list["esic_no"] = $data['esic_no'];    
                    $list["is_active"] = $data['is_active'];  
                    if ($data['is_active'] == 0) {
                        $list['is_pending'] = false;      
                    }else{
                        $list['is_pending'] = true;      
                    }
                    if ($data['is_primary'] == 0) {
                        $list['is_primary'] = false;      
                    }else{
                        $list['is_primary'] = true;      
                    }
                    array_push($response["list"], $list);
                }
                
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Faild to get List...!!";
                $response["status"] = "201";
                echo json_encode($response);
            }

           

        }else if($_POST['addBank']=="addBank" && $user_id !=0 && $user_id!='' &&  filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 

            $q1 = $d->count_data_direct("bank_id","user_bank_master","user_id='$user_id' AND floor_id = '$floor_id' AND block_id = '$block_id' AND is_delete = 0 AND is_primary = 1");

            if ($q1 > 0) {
                $is_primary = "0";
            }else{
                $is_primary = "1";                
            }

            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('floor_id',$floor_id);
            $m->set_data('block_id',$block_id);
            $m->set_data('account_no',$account_no);
            $m->set_data('account_type',$account_type);
            $m->set_data('ifsc_code',$ifsc_code);
            $m->set_data('bank_name', $bank_name);
            $m->set_data('bank_branch_name', $bank_branch_name);
            $m->set_data('crn_no', $crn_no);
            $m->set_data('pan_card_no', $pan_card_no);
            $m->set_data('esic_no', $esic_no);
            $m->set_data('is_primary', $is_primary);


            $a1= array(
                'society_id'=> $m->get_data('society_id'),
                'user_id'=> $m->get_data('user_id'),
                'unit_id'=> $m->get_data('unit_id'),
                'floor_id'=> $m->get_data('floor_id'),
                'block_id'=> $m->get_data('block_id'),
                'account_no'=> $m->get_data('account_no'),
                'account_type'=> $m->get_data('account_type'),
                'ifsc_code'=> $m->get_data('ifsc_code'),
                'bank_name'=> $m->get_data('bank_name'),
                'bank_branch_name'=> $m->get_data('bank_branch_name'),
                'crn_no'=> $m->get_data('crn_no'),
                'pan_card_no'=> $m->get_data('pan_card_no'),
                'esic_no'=> $m->get_data('esic_no'),
                'is_primary'=> $m->get_data('is_primary'),
            );
           
            $q=$d->insert("user_bank_master",$a1);
                
            if ($q>0) {
                               
                $response["message"] = "Added Successfully";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
               
            } else {
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

        }else if($_POST['updateBank']=="updateBank" && $user_id !=0 && $user_id!='' && $bank_id !=0 && $bank_id!='' && filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 


            if ($isPrimaryEdit == 1) {
                $m->set_data('new_bank_name', $bank_name);
                $m->set_data('new_bank_branch_name', $bank_branch_name);
                $m->set_data('new_account_type',$account_type);
                $m->set_data('new_account_no',$account_no);
                $m->set_data('new_ifsc_code',$ifsc_code);
                $m->set_data('new_crn_no', $crn_no);
                $m->set_data('new_pan_card_no', $pan_card_no);
                $m->set_data('new_esic_no', $esic_no);
                $m->set_data('is_active', "1");
                $m->set_data('new_change_request_datetime', date('Y-m-d H:i:s'));

                $a= array(
                    'new_bank_name'=> $m->get_data('new_bank_name'),
                    'new_bank_branch_name'=> $m->get_data('new_bank_branch_name'),
                    'new_account_type'=> $m->get_data('new_account_type'),
                    'new_account_no'=> $m->get_data('new_account_no'),
                    'new_ifsc_code'=> $m->get_data('new_ifsc_code'),
                    'new_crn_no'=> $m->get_data('new_crn_no'),
                    'new_pan_card_no'=> $m->get_data('new_pan_card_no'),
                    'new_esic_no'=> $m->get_data('new_esic_no'),
                    'is_active'=> $m->get_data('is_active'),
                    'new_change_request_datetime'=> $m->get_data('new_change_request_datetime'),
                );
            }else{
                $m->set_data('bank_name', $bank_name);
                $m->set_data('bank_branch_name', $bank_branch_name);
                $m->set_data('account_type',$account_type);
                $m->set_data('account_no',$account_no);
                $m->set_data('ifsc_code',$ifsc_code);
                $m->set_data('crn_no', $crn_no);
                $m->set_data('pan_card_no', $pan_card_no);
                $m->set_data('esic_no', $esic_no);

                $a= array(
                    'bank_name'=> $m->get_data('bank_name'),
                    'bank_branch_name'=> $m->get_data('bank_branch_name'),
                    'account_type'=> $m->get_data('account_type'),
                    'account_no'=> $m->get_data('account_no'),
                    'ifsc_code'=> $m->get_data('ifsc_code'),
                    'crn_no'=> $m->get_data('crn_no'),
                    'pan_card_no'=> $m->get_data('pan_card_no'),
                    'esic_no'=> $m->get_data('esic_no'),
                );
            }

            $q=$d->update("user_bank_master",$a,"bank_id='$bank_id' AND user_id='$user_id'");
                

            if ($q>0) {
                if ($isPrimaryEdit == 1) {
                    $response["message"] = "Primary account details change request has been sent to admin";
                }else{
                    $response["message"] = "Updated Successfully";                    
                }
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Faild to Add";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['deleteBank']=="deleteBank" && $user_id !=0 && $user_id!='' && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($bank_id, FILTER_VALIDATE_INT) == true ){ 


            $q=$d->delete("user_bank_master","bank_id='$bank_id' AND user_id='$user_id'");

            if ($q>0) {
               
                $response["message"] = "Deleted Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Faild to Delete";
                $response["status"] = "201";
                echo json_encode($response);
            }
 

        }else if($_POST['updateIds']=="updateIds" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $userQ = $d->select("users_master","society_id = '$society_id'");

            if (mysqli_num_rows($userQ) > 0) {
                while($data = mysqli_fetch_array($userQ)){

                    $user_id = $data['user_id'];
                    $unit_id = $data['unit_id'];

                    $m->set_data('block_id',$data['block_id']);
                    $m->set_data('floor_id',$data['floor_id']);

                    $a1= array(
                        'block_id'=> $m->get_data('block_id'),
                        'floor_id'=> $m->get_data('floor_id'),
                    );

                   
                    $q=$d->update("user_bank_master",$a1,"user_id='$user_id' AND unit_id = '$unit_id'");
                }
            }
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>