<?php

include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    //if ($key==$keydb && $auth_check=='true') {

        $response = array();
        extract(array_map("test_input" , $_POST));
        
        if($_POST['getAboutUs']=="getAboutUs" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $ourServiceQry = $d->select("company_about_us_master","society_id='$society_id'");

            if (mysqli_num_rows($ourServiceQry) > 0) {

                $data = mysqli_fetch_array($ourServiceQry);

                $response["company_about_us_id"] = $data['company_about_us_id'];

                if ($data['company_about_us_top_image'] != '') {
                    $response["company_about_us_top_image"] = $base_url.'img/about_us_image/'.$data['company_about_us_top_image'];
                }else{
                    $response["company_about_us_top_image"]="";
                }

                $response["company_about_us_top_description"] = $data['company_about_us_top_description'];

                if ($data['company_about_us_image_one'] != '') {
                    $response["company_about_us_image_one"] = $base_url.'img/about_us_image/'.$data['company_about_us_image_one'];
                }else{
                    $response["company_about_us_image_one"]="";
                }

                if ($data['company_about_us_image_two'] != '') {
                    $response["company_about_us_image_two"] = $base_url.'img/about_us_image/'.$data['company_about_us_image_two'];
                }else{
                    $response["company_about_us_image_two"]="";
                }

                $response["company_about_us_bottom_title"] = $data['company_about_us_bottom_title'];
                $response["company_about_us_bottom_description"] = $data['company_about_us_bottom_description'];
                
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            } else{
                $response["message"]="No data";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag";
            $response["status"]="201";
            echo json_encode($response);
        }
    /*}else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }*/
}
?>