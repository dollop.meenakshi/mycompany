<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        
        if ($_POST['saveVisitingCard'] == "saveVisitingCard" && $user_id!=""  && $society_id!="") {

            $extension11 = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");
            $uploadedFile1 = $_FILES["user_visiting_card"]["tmp_name"];
            $ext = pathinfo($_FILES['user_visiting_card']['name'], PATHINFO_EXTENSION);
            $ext = strtolower($ext);
            
            if (file_exists($uploadedFile1)) {
                if(in_array($ext,$extension11)) {
                    $dateJoin = date("YmdHis");
                    $user_name = str_replace(' ', '', $user_name);
                    $image_Arr = $_FILES['user_visiting_card'];   
                    $temp = explode(".", $_FILES["user_visiting_card"]["name"]);
                    $user_visiting_card = $user_name . "VisitingCard".$dateJoin.".". $ext;
                    move_uploaded_file($_FILES["user_visiting_card"]["tmp_name"], "../img/users/recident_profile/".$user_visiting_card);
                } else {
                    $response["message"]="Invalid photo.";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                } 
            } else {
                $response["message"]="Please upload valid visiting card";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }

           
            $m->set_data('user_visiting_card', $user_visiting_card);

            $a2 = array(
                'user_visiting_card' => $m->get_data('user_visiting_card'),
            );
            
            $q = $d->update("users_master",$a2,"user_id='$user_id'");


            if ($q == true) {
                
                $response["user_visiting_card"] = $base_url."img/users/recident_profile/".$user_visiting_card;
                $response["message"] = "Updated Successfully";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if ($_POST['addCombinedCard'] == "addCombinedCard" && $user_id!=""  && $society_id!="") {

            $extension11 = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");

            $uploadedFile1 = $_FILES["user_visiting_card_front"]["tmp_name"];
            $ext = pathinfo($_FILES['user_visiting_card_front']['name'], PATHINFO_EXTENSION);
            $ext = strtolower($ext);
            
            if (file_exists($uploadedFile1)) {
                if(in_array($ext,$extension11)) {
                    $dateJoin = date("YmdHis");
                    $user_name = str_replace(' ', '', $user_name);
                    $image_Arr = $_FILES['user_visiting_card_front'];   
                    $temp = explode(".", $_FILES["user_visiting_card_front"]["name"]);
                    $user_visiting_card_front = $user_name . "VisitingCardFront".$dateJoin.".". $ext;
                    move_uploaded_file($_FILES["user_visiting_card_front"]["tmp_name"], "../img/users/recident_profile/".$user_visiting_card_front);
                } else {
                    $response["message"]="Invalid photo.";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                } 
            } else {
                $response["message"]="Please upload valid visiting card";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }

            $uploadedFile2 = $_FILES["user_visiting_card_back"]["tmp_name"];
            $ext = pathinfo($_FILES['user_visiting_card_back']['name'], PATHINFO_EXTENSION);
            $ext = strtolower($ext);
            
            if (file_exists($uploadedFile2)) {
                if(in_array($ext,$extension11)) {
                    $dateJoin = date("YmdHis");
                    $user_name = str_replace(' ', '', $user_name);
                    $image_Arr = $_FILES['user_visiting_card_back'];   
                    $temp = explode(".", $_FILES["user_visiting_card_back"]["name"]);
                    $user_visiting_card_back = $user_name . "VisitingCardBack".$dateJoin.".". $ext;
                    move_uploaded_file($_FILES["user_visiting_card_back"]["tmp_name"], "../img/users/recident_profile/".$user_visiting_card_back);
                } else {
                    $response["message"]="Invalid photo.";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                } 
            }

           
            $m->set_data('user_id', $user_id);
            $m->set_data('user_visiting_card_name', $user_visiting_card_name);
            $m->set_data('user_visiting_card_front', $user_visiting_card_front."");
            $m->set_data('user_visiting_card_back', $user_visiting_card_back."");
            $m->set_data('user_visiting_card_created_date', date('Y-m-d H:i:s'));
            $m->set_data('user_visiting_card_updated_date', date('Y-m-d H:i:s'));

            $a2 = array(
                'user_id' => $m->get_data('user_id'),
                'user_visiting_card_name' => $m->get_data('user_visiting_card_name'),
                'user_visiting_card_front' => $m->get_data('user_visiting_card_front'),
                'user_visiting_card_back' => $m->get_data('user_visiting_card_back'),
                'user_visiting_card_created_date' => $m->get_data('user_visiting_card_created_date'),
            );

            $qq1 = $d->selectRow("user_visiting_card_id","user_visiting_card","user_id = '$user_id' AND user_visiting_card_name = '$user_visiting_card_name'");

            if (mysqli_num_rows($qq1) > 0) {
                $qq1Data = mysqli_fetch_array($qq1);

                $user_visiting_card_id = $qq1Data['user_visiting_card_id'];
                $a21 = array(
                    'user_visiting_card_front' => $m->get_data('user_visiting_card_front'),
                    'user_visiting_card_back' => $m->get_data('user_visiting_card_back'),
                    'user_visiting_card_updated_date' => $m->get_data('user_visiting_card_updated_date'),
                );

                $q = $d->update("user_visiting_card",$a2,"user_visiting_card_id = '$user_visiting_card_id' AND user_id = '$user_id' AND user_visiting_card_name = '$user_visiting_card_name'");

                $response["message"] = "Card Updated";

            }else{
                $q = $d->insert("user_visiting_card",$a2);

                $response["message"] = "Card added";
            }
            

            if ($q == true) {
                
                
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if ($_POST['getMyVisitingCard'] == "getMyVisitingCard" && $user_id!=""  && $society_id!="") {

            $qry = $d->select("user_visiting_card","user_id = '$user_id'","ORDER BY user_visiting_card_created_date DESC");
            
            if(mysqli_num_rows($qry)>0){
                
                $response["cards"] = array();

                while($data=mysqli_fetch_array($qry)) {

                    $data = array_map("html_entity_decode", $data);

                    $cards = array();

                    $cards['user_visiting_card_id'] = $data['user_visiting_card_id'];
                    $cards['user_visiting_card_name'] = $data['user_visiting_card_name'];
                    if ($data['user_visiting_card_front'] != '') {
                        $cards['user_visiting_card_front'] = $base_url."img/users/recident_profile/".$data['user_visiting_card_front'];
                    }else{
                        $cards['user_visiting_card_front'] = "";
                    }
                    if ($data['user_visiting_card_back'] != '') {
                        $cards['user_visiting_card_back'] = $base_url."img/users/recident_profile/".$data['user_visiting_card_back'];
                    }else{
                        $cards['user_visiting_card_back'] = "";
                    }
                    $cards['user_visiting_card_created_date'] = date('h:i A, dS M Y',strtotime($data['user_visiting_card_created_date']));

                    array_push($response["cards"], $cards);
                }
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Failed";
                $response["status"]="201";
                echo json_encode($response);
            }

        }else if ($_POST['removeVisitingCardNew'] == "removeVisitingCardNew" && $user_id!=""  && $society_id!="") {

                       
            $q = $d->delete("user_visiting_card","user_visiting_card_id='$user_visiting_card_id' AND user_id = '$user_id'");

            if ($q == true) {
                
                $response["message"] = "Visiting Card Removed";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if ($_POST['removeVisitingCard'] == "removeVisitingCard" && $user_id!=""  && $society_id!="") {

                       
            $m->set_data('user_visiting_card', "");

            $a2 = array(
                'user_visiting_card' => $m->get_data('user_visiting_card'),
            );
            
            $q = $d->update("users_master",$a2,"user_id='$user_id'");


            if ($q == true) {
                
                $response["message"] = "Visiting Card Removed";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }  else  if ($_POST['getAbout'] == "getAbout" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

           $qA = $d->select("users_master,user_employment_details,unit_master", "unit_master.unit_id=users_master.unit_id AND users_master.delete_status=0 AND user_employment_details.user_id=users_master.user_id AND user_employment_details.user_id='$user_id' AND user_employment_details.society_id='$society_id' AND user_employment_details.unit_id='$unit_id'","");

            if (mysqli_num_rows($qA) > 0) {
                
                $data = mysqli_fetch_array($qA);

                 $qb=$d->select("society_master","society_id ='$society_id' AND society_id!=0 and society_status='0'");
                $balancesheet_data=mysqli_fetch_array($qb);

                $response["employment_id"] = $data['employment_id'];
                $response["user_id"] = $data['user_id'];
                $response["society_id"] = $data['society_id'];
                $response["unit_id"] = $data['unit_id'];
                $response["user_full_name"] = $data['user_full_name'];
                $response["user_phone"] = $data['user_mobile'];
                $response["user_email"] = $data['user_email'];
                $response["company_email"] = $data['company_email'];
                $response["plot_lattitude"] = $balancesheet_data['society_latitude'];
                $response["plot_longitude"] = $balancesheet_data['society_longitude'];
                $response["employment_type"] = html_entity_decode($data['employment_type']);
                $response["business_categories"] = html_entity_decode($data['business_categories']);
                $response["business_categories_sub"] = html_entity_decode($data['business_categories_sub']);
                $response["business_categories_other"] = html_entity_decode($data['business_categories_other']);
                $response["professional_other"] = html_entity_decode($data['professional_other']);
                $response["employment_description"] = html_entity_decode($data['employment_description']);
                $response["company_name"] = html_entity_decode($balancesheet_data['society_name']);
                $response["designation"] = html_entity_decode($data['user_designation']);
                $response["company_address"] = html_entity_decode($balancesheet_data['society_address']);
                $company_website = preg_replace("(^https?://)", "", $balancesheet_data['company_website']);
                $response["company_website"] = html_entity_decode($company_website);
                $response["company_logo_old"] = html_entity_decode($data['company_logo']);
                
                if ($balancesheet_data['socieaty_logo']!="") {
                    $response["company_logo"] = $base_url.'img/society/'. $balancesheet_data['socieaty_logo'];
                } else {
                    $response["company_logo"] ="";

                }
                
                if ($data['company_brochure']!="") {
                    $response["company_brochure"] = $base_url.'img/users/recident_profile/'. $data['company_brochure'];
                } else {
                    $response["company_brochure"] ="";

                }

                if ($data['visiting_card']!="") {
                    $response["visiting_card"] = $base_url.'img/users/recident_profile/'. $data['visiting_card'];
                } else {
                    $response["visiting_card"] ="";

                }

                $response["search_keyword"] = html_entity_decode($data['search_keyword']);
                
                $response["company_contact_number"] = $data['user_mobile'];
            
                if ($data['public_mobile']==1 && $data['company_contact_number']!=0) {
                    $response["company_contact_number_view"] = "Private";
                } else if($data['company_contact_number']==0 && $data['public_mobile']==0){
                    $response["company_contact_number_view"] = "Not Available";
                } else {
                    $response["company_contact_number_view"] = $data['company_contact_number'];
                }

                $user_designation = $data['user_designation'];
                $user_mobile = $data['country_code'].$data['user_mobile'];
                $user_email = $data['user_email'];
                $user_full_name = $data['user_full_name'];

                $qr_content = urlencode("https://api.whatsapp.com/send?phone=$user_mobile&text=Hello $user_full_name");
                $qr_size = "300x300";
                $qr_correction    = strtoupper('H');
                $qr_encoding      = 'UTF-8';
                //form google chart api link
                $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                $response["visiting_card_qr_code"] = $qrImageUrl . '.png';


               

                $response["message"] = "Get About success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No About Foud";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['addAbout'] == "addAbout") {

            $qsel = $d->select("user_employment_details","user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'");
            $oldData = mysqli_fetch_array($qsel);

           
            if ($business_categories_sub!='Other') {
              $business_categories_other='';
            } if ($business_categories_sub!='Other') {
              $professional_other='';
            }

            $extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");
            $uploadedFile = $_FILES["company_logo"]["tmp_name"];
            $ext = pathinfo($_FILES['company_logo']['name'], PATHINFO_EXTENSION);
            if (file_exists($uploadedFile)) {

                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $user_id;
                $dirPath = "../img/users/recident_profile/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 500) {
                    $newWidthPercentage = 500 * 100 / $imageWidth; //for maximum 500 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }
                switch ($imageType) {

                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagepng($tmp, $dirPath . $newFileName . "_logo." . $ext);
                    break;

                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagejpeg($tmp, $dirPath . $newFileName . "_logo." . $ext);
                    break;

                case IMAGETYPE_GIF:
                    $imageSrc = imagecreatefromgif($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagegif($tmp, $dirPath . $newFileName . "_logo." . $ext);
                    break;

                default:

                    break;
                }
                $company_logo = $newFileName . "_logo." . $ext;

            } else {

               $company_logo = $company_logo_old;
            }


            $extension11 = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");
            $uploadedFile1 = $_FILES["visiting_card"]["tmp_name"];
            $ext = pathinfo($_FILES['visiting_card']['name'], PATHINFO_EXTENSION);
            if (file_exists($uploadedFile1)) {

                $sourceProperties = getimagesize($uploadedFile1);
                $newFileName = rand() . $user_id;
                $dirPath = "../img/users/recident_profile/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 2400) {
                    $newWidthPercentage = 2400 * 100 / $imageWidth; //for maximum 500 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }
                switch ($imageType) {

                case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($uploadedFile1);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagepng($tmp, $dirPath . $newFileName . "_visitingCard." . $ext);
                    break;

                case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($uploadedFile1);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagejpeg($tmp, $dirPath . $newFileName . "_visitingCard." . $ext);
                    break;

                case IMAGETYPE_GIF:
                    $imageSrc = imagecreatefromgif($uploadedFile1);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagegif($tmp, $dirPath . $newFileName . "_visitingCard." . $ext);
                    break;

                default:

                    break;
                }
                $visiting_card = $newFileName . "_visitingCard." . $ext;

            } else {

               $visiting_card =$oldData['visiting_card'];
            }


            $file11=$_FILES["company_brochure"]["tmp_name"];
            $extId = pathinfo($_FILES['company_brochure']['name'], PATHINFO_EXTENSION);
            $extAllow=array("pdf","doc","docx","png","jpg","jpeg","JPEG","PNG","JPG","JPEG");
            if (file_exists($file11)) {
              if(in_array($extId,$extAllow)) {
                 $temp = explode(".", $_FILES["company_brochure"]["name"]);
                  $company_brochure = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                  move_uploaded_file($_FILES["company_brochure"]["tmp_name"], "../img/users/recident_profile/" . $company_brochure);
              } else {
                
                $response["message"]="Invalid brochure type only JPG,PNG,Doc & PDF are allowed.";
                $response["status"]="201";
                echo json_encode($response);
                exit();
              }
            } else {
                $company_brochure=$oldData['company_brochure'];
            }
            
            $m->set_data('user_id', $user_id);
            $m->set_data('society_id', $society_id);
            $m->set_data('unit_id', $unit_id);
            $m->set_data('company_email', $user_email);
            $m->set_data('business_categories', $business_categories);
            $m->set_data('business_categories_sub', $business_categories_sub);
            $m->set_data('business_categories_other',$business_categories_other);
            $m->set_data('professional_other',$professional_other);
            $m->set_data('employment_description', $employment_description);
            $m->set_data('company_name', $company_name);
            $m->set_data('designation', $designation);
            $m->set_data('company_address', $company_address);
            $m->set_data('company_contact_number', $company_contact_number);
            $m->set_data('company_website', $company_website);
            $m->set_data('company_logo', $company_logo);
            $m->set_data('search_keyword', $search_keyword);
            $m->set_data('plot_lattitude', $plot_lattitude);
            $m->set_data('plot_longitude', $plot_longitude);
            $m->set_data('company_logo', $company_logo);
            $m->set_data('company_brochure', $company_brochure);
            $m->set_data('visiting_card', $visiting_card);

            $a = array(
                'user_id' => $m->get_data('user_id'),
                'society_id' => $m->get_data('society_id'),
                'unit_id' => $m->get_data('unit_id'),
                'business_categories' => $m->get_data('business_categories'),
                'business_categories_sub' => $m->get_data('business_categories_sub'),
                'business_categories_other'=> $m->get_data('business_categories_other'),
                'professional_other'=> $m->get_data('professional_other'),
                'employment_description' => $m->get_data('employment_description'),
                'designation' => $m->get_data('designation'),
                'search_keyword' => $m->get_data('search_keyword'),
                'visiting_card' => $m->get_data('visiting_card'),
            );

            $aUser = array(
                'user_designation' => $m->get_data('designation'),
            );

            $sq=$d->selectRow("member_status","users_master","user_id='$user_id'");
            $primaryData=mysqli_fetch_array($sq);
            if ($primaryData['member_status']==0) {
                
               
                $a2 = array(
                    'company_email' => $m->get_data('company_email'),
                    'company_name' => $m->get_data('company_name'),
                    'company_address' => $m->get_data('company_address'),
                    'company_contact_number' => $m->get_data('company_contact_number'),
                    'company_website' => $m->get_data('company_website'),
                    'plot_lattitude' => $m->get_data('plot_lattitude'),
                    'plot_longitude' => $m->get_data('plot_longitude'),
                    'company_logo' => $m->get_data('company_logo'),
                    'company_brochure' => $m->get_data('company_brochure'),
                );
                $d->update("unit_master",$a2,"unit_id='$unit_id'");
            }


            

            $q = false;

            if(mysqli_num_rows($qsel)>0){

                 $d->insert_myactivity($user_id,"$society_id","0","$user_name","Professional data updated","businessman.png");
                 
                $q = $d->update("user_employment_details",$a,"user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'");
            }else{

                 $d->insert_myactivity($user_id,"$society_id","0","$user_name","Professional data added","businessman.png");
                 
                $q = $d->insert("user_employment_details",$a);
            }

            if ($q == true) {

                $q = $d->update("users_master",$a,"user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'");
                
                $response["message"] = "Professional Details Updated";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "wrong data.";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if ($_POST['changePrivacy'] == "changePrivacy") {
            
            $m->set_data('public_mobile', $public_mobile);

            $a = array(
                'public_mobile' => $m->get_data('public_mobile'),
            );

            $q = $d->update("users_master",$a,"user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'");

            if ($q == true) {

                 $d->insert_myactivity($user_id,"$society_id","0","$user_name","Mobile number privacy changed","");
                 

                $response["message"] = "Privacy Changed Successfully";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else  if ($_POST['searchBusiness'] == "searchBusiness" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

                $q=$d->select("user_employment_details,users_master,block_master,floors_master,unit_master","users_master.delete_status=0 AND users_master.user_id=user_employment_details.user_id AND user_employment_details.society_id='$society_id' AND users_master.block_id=block_master.block_id  AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND user_employment_details.business_categories_sub!=''","ORDER BY unit_master.unit_id ASC" );
                if(mysqli_num_rows($q)>0){
                    $response["occupation"] = array();

                    while($data=mysqli_fetch_array($q)) {

                            $occupation = array(); 
                            $occupation["user_id"]=$data['user_id'];
                            $occupation["user_full_name"]=$data['user_first_name'].' '.$data['user_last_name'];
                            if ( $data['user_profile_pic']!='') {
                              $occupation["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                            } else {
                              $occupation["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                            }
                            $occupation["user_phone"]=$data['company_contact_number'];
                            $occupation["user_email"]=$data['company_email'];
                            $occupation["block_name"] = $data['block_name'];
                            $occupation["floor_name"] = $data['floor_name'];
                            $occupation["unit_name"] = $data['unit_name'];
                            $occupation["employment_type"]=html_entity_decode($data['employment_type']);
                            if ($data['business_categories']=='Other' && $data['business_categories_other']!='') {
                                $occupation["business_categories"] = html_entity_decode($data['business_categories_other']);
                            }else {
                                $occupation["business_categories"] = html_entity_decode($data['business_categories']);
                            }
                            if ($data['business_categories_sub']=="Other" && $data['professional_other']!='') {
                                $occupation["business_categories_sub"] = html_entity_decode($data['professional_other']);
                            } else {
                                $occupation["business_categories_sub"] = html_entity_decode($data['business_categories_sub']);
                            }
                            $occupation["employment_description"]=html_entity_decode($data['employment_description']);
                            $occupation["company_name"]=html_entity_decode($data['company_name']);
                            $occupation["designation"]=html_entity_decode($data['designation']);
                            $occupation["company_address"]=html_entity_decode($data['company_address']);
                            $occupation["company_contact_number"]=$data['company_contact_number'];
                            $occupation["company_website"] = html_entity_decode($data['company_website']);
                            $occupation["search_keyword"] = html_entity_decode($data['search_keyword']);
                            
                            array_push($response["occupation"], $occupation);


                    }
                    $response["message"]="Get Occupation Success.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No Occupation Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

        } else {
            $response["message"] = "wrong tag";
            $response["status"] = "201";
            echo json_encode($response);
        }

    } else {
        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 