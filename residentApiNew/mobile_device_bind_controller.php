<?php
include_once 'lib.php';


/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST) && !empty($_POST)) {

	if ($key == $keydb && $auth_check=='true') {

		$response = array();
		extract(array_map("test_input", $_POST));

		if($_POST['checkAccessDeviceBind']=="checkAccessDeviceBind" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $mobile_device_bind_Access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if ($_POST['getPendingDeviceBindList']=="getPendingDeviceBindList" &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

        	$access_type = $mobile_device_bind_Access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND user_mac_address_master.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND user_mac_address_master.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;


			$birthdayQry = $d->selectRow("user_mac_address_master.*,block_master.block_name,floors_master.floor_name,users_master.user_full_name,users_master.user_designation,users_master.user_profile_pic","user_mac_address_master,users_master,block_master,floors_master","users_master.user_status = '1' AND users_master.active_status = '0' AND users_master.delete_status = '0' AND user_mac_address_master.mac_address_change_status = '0' AND users_master.user_id = user_mac_address_master.user_id AND user_mac_address_master.society_id = '$society_id' AND users_master.block_id = block_master.block_id AND users_master.floor_id = floors_master.floor_id $appendQuery","ORDER BY user_mac_address_master.mac_address_request_date DESC");

			if (mysqli_num_rows($birthdayQry) > 0) {
			
				$response["device_list"] = array();

				while($data = mysqli_fetch_array($birthdayQry)){
					$devices = array();

					$devices['user_mac_address_id'] = $data['user_mac_address_id'];
					$devices['user_id'] = $data['user_id'];
					$devices['user_full_name'] = $data['user_full_name'];
					$devices['user_designation'] = $data['user_designation'];
					$devices['branch_name'] = $data['block_name'];
					$devices['department_name'] = $data['floor_name'];
					$devices['mac_address'] = $data['mac_address'];
					$devices['mac_address_device'] = $data['mac_address_device'];
					$devices['mac_address_phone_modal'] = $data['mac_address_phone_modal'];
					$devices['mac_address_change_reason'] = $data['mac_address_change_reason'];
					$devices['mac_address_request_date'] = date('d F Y',strtotime($data['mac_address_request_date']));
					$devices['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $data['user_profile_pic'];

							
					array_push($response["device_list"],$devices);
				}

				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			}else{
				$response["message"] = "No data";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if($_POST['approveDevice']=="approveDevice" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			$m->set_data('mac_address_change_status',"1");
			$m->set_data('mac_address_status_change_date',date('Y-m-d'));
			$m->set_data('mac_address_status_change_by',$my_user_id);

            $a = array(
                'mac_address_change_status'=>$m->get_data('mac_address_change_status'),
                'mac_address_status_change_date'=>$m->get_data('mac_address_status_change_date'),
                'mac_address_status_change_by'=>$m->get_data('mac_address_status_change_by'),
            );

            $qry = $d->update("user_mac_address_master",$a,"user_id = '$user_id' AND user_mac_address_id = '$user_mac_address_id'");

            if ($qry == true) {
            	$m->set_data('user_mac_address',$mac_address);

	            $a1 = array(
	                'user_mac_address'=>$m->get_data('user_mac_address'),
	            );

	            $qry = $d->update("users_master",$a1,"user_id = '$user_id'");

	            /*$title = "Device Change Request";
                $description = "Your mobile device changes request hase been approved by $user_name";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("device_change","",$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("device_change","",$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"device_change","smartphone.png","user_id = '$user_id'",$user_id);*/

	            $response["message"] = "Device Id updated";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "Something went wrong!";
				$response["status"] = "200";
				echo json_encode($response);
            }
                      
        }else if($_POST['rejectDevice']=="rejectDevice" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

			$m->set_data('mac_address_change_status',"2");
			$m->set_data('mac_address_status_change_date',date('Y-m-d'));
			$m->set_data('mac_address_status_change_by',$my_user_id);
			$m->set_data('mac_address_reject_reason',$mac_address_reject_reason);

            $a = array(
                'mac_address_change_status'=>$m->get_data('mac_address_change_status'),
                'mac_address_status_change_date'=>$m->get_data('mac_address_status_change_date'),
                'mac_address_status_change_by'=>$m->get_data('mac_address_status_change_by'),
                'mac_address_reject_reason'=>$m->get_data('mac_address_reject_reason'),
            );

            $qry = $d->update("user_mac_address_master",$a,"user_id = '$user_id' AND user_mac_address_id = '$user_mac_address_id'");

            if ($qry == true) {  

            	/*$title = "Device Change Request";
                $description = "Your mobile device changes request hase been rejected by $user_name";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("device_change","",$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("device_change","",$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"device_change","smartphone.png","user_id = '$user_id'",$user_id);*/

	            $response["message"] = "Device rejected";
				$response["status"] = "200";
				echo json_encode($response);
            }else{
            	$response["message"] = "Something went wrong!";
				$response["status"] = "200";
				echo json_encode($response);
            }
                      
        }else {
			$response["message"] = "wrong tag.";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {

		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}
?>