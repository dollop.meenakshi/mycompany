<?php
include_once 'lib.php';


// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);

if (isset($_POST) && !empty($_POST)) {

	if ($key == $keydb && $auth_check=='true') {

		$response = array();
		extract(array_map("test_input", $_POST));

		if (isset($getProfileData) && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			if ($getProfileData == 'getMemberProfileData') {
				if ($unit_id != '' && $unit_id != 0) {
					$quc = $d->select("unit_master", "unit_id='$unit_id' AND member_access_denied=1");
					if (mysqli_num_rows($quc) > 0) {
						$response["message"] = "Access Denied..!";
						$response["status"] = "201";
						echo json_encode($response);
						exit();
					}

				}
			}

			$q = $d->selectRow("users_master.*,block_master.*,floors_master.*,unit_master.*","users_master,block_master,floors_master,unit_master",
				"users_master.delete_status=0 AND users_master.user_id ='$user_id' AND users_master.block_id=block_master.block_id   AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id
         	");

			$user_data = mysqli_fetch_array($q);

			if ($user_data == TRUE) {
				$today = date("Y-m-d");

				if ($user_data['user_status'] == 0) {
					$response["message"] = "User yet not approved by admin !";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				}

				if ($user_data['member_date_of_birth'] != '' && $user_data['dob_view'] == 0) {
					$dob = date("d M Y", strtotime($user_data['member_date_of_birth']));
					$dobset = date("Y-m-d", strtotime($user_data['member_date_of_birth']));
				} else if ($user_data['member_date_of_birth'] != '' && $user_data['dob_view'] == 1) {
					$dob = $xml->string->private.'';
					$dobset = date("Y-m-d", strtotime($user_data['member_date_of_birth']));
				} else {
					$dob = $xml->string->not_available.'';;
					$dobset = "";
				}

				if ($user_data['unit_status'] != '4') {

					$response["user_id"] = $user_data['user_id'];
					$user_id = $user_data['user_id'];
					$response["society_id"] = $user_data['society_id'];
					$society_id = $user_data['society_id'];
					
					$user_full_name=$user_data['user_full_name'];
					$response["user_full_name"] = $user_full_name.'';
					$response["shift_time_id"] = $user_data['shift_time_id'];
					$response["user_first_name"] = $user_data['user_first_name'];
					$response["user_last_name"] = $user_data['user_last_name'];
					$response["user_mobile"] = $user_data['user_mobile'];
					$response["designation"] = $user_data['user_designation'];
					$response["marital_status "] = $user_data['marital_status '];
					$response["country_code"] = $user_data['country_code'];
					$response["country_code_alt"] = $user_data['country_code_alt'];

					if ($user_data['public_mobile'] == 1 && $user_data['user_mobile'] != 0) {
						$response["user_mobile_view"] = $xml->string->private.'';
					} else if ($user_data['user_mobile'] != 0 && $user_data['public_mobile'] == 0) {
						$response["user_mobile_view"] = $user_data['country_code'].' '.$user_data['user_mobile'] . '';
					} else {
						$response["user_mobile_view"] = "Not Available";
					}

					$response["user_email"] = $user_data['user_email'];
					$response["user_id_proof"] = $user_data['user_id_proof'];
					$response["user_type"] = $user_data['user_type'];
					$response["member_status"] = $user_data['member_status'];
					$response["block_id"] = $user_data['block_id'];
					$response["public_mobile"] = $user_data['public_mobile'];
					$response["visitor_approved"] = $user_data['visitor_approved'];
					$response["group_visitor_approval"] = $user_data['Delivery_cab_approval'];
					$response["member_date_of_birth"] = $dob . ' ';
					$response["member_date_of_birth_set"] = $dobset . ' ';
					$response["block_name"] = $user_data['block_name'];
					$response["floor_name"] = $user_data['floor_name'];
					$response["unit_name"] = $user_data['unit_name'];
					$response["unit_status"] = $user_data['unit_status'];
					$response["unit_close_for_gatekeeper"] = $user_data['unit_close_for_gatekeeper'];
				    
				    $qr_size = "300x300";
					$qr_content       = $user_data['user_mobile'];
					$qr_correction    = strtoupper('H');
					$qr_encoding      = 'UTF-8';

    				//form google chart api link
					$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
					$response["icard_qr_code"] = $qrImageUrl . '.png';
					
						$response["owner_name"] = "";
						$response["owner_email"] = "";
						$response["owner_mobile"] = "";
					// $response["owner_name"] = $user_data['owner_name'].'';
					// $response["owner_mobile"] = $user_data['owner_mobile'].'';
					$response["other_remark"] = $user_data['other_remark'] . '';
					$response["gender"] = $user_data['gender'];
					$response["floor_id"] = $user_data['floor_id'];
					$response["unit_id"] = $user_data['unit_id'];
					$unit_id = $user_data['unit_id'];
					$response["user_status"] = $user_data['user_status'];
					if ($user_data['user_profile_pic'] != '') {
						$response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $user_data['user_profile_pic'];
					} else {
						$response["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
					}

					// $response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $user_data['user_profile_pic'];

					$response["facebook"] = $user_data['facebook'];
					$response["instagram"] = $user_data['instagram'];
					$response["linkedin"] = $user_data['linkedin'];

					$response["alt_mobile"] = $user_data['alt_mobile'];

					if ($user_data['public_mobile'] == 1 && $user_data['alt_mobile'] != 0) {
						$response["alt_mobile_view"] = $xml->string->private.'';
					} else if ($user_data['alt_mobile'] != 0 && $user_data['public_mobile'] == 0) {
						$response["alt_mobile_view"] =  $user_data['country_code_alt'].' '.$user_data['alt_mobile'];
					} else {
						$response["alt_mobile_view"] = "Not Available";
					}
					$response["sos_alert"] = $user_data['sos_alert'];
					$response["tenant_view"] = $user_data['tenant_view'];
					$response["dob_view"] = $user_data['dob_view'];
					$response["mobile_for_gatekeeper"] = $user_data['mobile_for_gatekeeper'];
					$response["child_gate_approval"] = $user_data['child_gate_approval'];
					$response["timeline_notification_on"] = $user_data['timeline_notification_on'];
					if ($user_data['blood_group']!="") {
						$response["blood_group"] = $user_data['blood_group'];
					} else {
						$response["blood_group"] =$xml->string->not_available.'';
					}
					if ($user_data['member_date_of_birth'] != '') {
						if ($uDate != $today) {
							$response["dob"] = '0';
						} else {
							$response["dob"] = '1';
						}
					} else {
						$response["dob"] = '0';
					}

					$block_id = $user_data['block_id'];
					$floor_id = $user_data['floor_id'];
					$unit_id = $user_data['unit_id'];

					

					$qfamily = $d->select("users_master", "delete_status=0 AND society_id ='$society_id'and block_id='$block_id' AND floor_id='$floor_id' AND unit_id='$unit_id' AND user_type=' $user_data[user_type]' AND user_id!='$user_id'");

					$qemergency = $d->select("user_emergencycontact_master", "society_id ='$society_id' and user_id='$user_id'");

					$response["member"] = array();
					$response["emergency"] = array();

					while ($datafamily = mysqli_fetch_array($qfamily)) {

						$qAMember = $d->select("user_employment_details", "user_id='$datafamily[user_id]' AND society_id='$society_id' AND unit_id='$unit_id' ", "");
						$desData= mysqli_fetch_array($qAMember);

						$member = array();
						$member["user_id"] = $datafamily['user_id'];
						$member["shift_time_id"] = $datafamily['shift_time_id'];
						$member["user_first_name"] = $datafamily['user_first_name'];
						$member["user_last_name"] = $datafamily['user_last_name'];
						$member["user_mobile"] = $datafamily['user_mobile'];
						$member["user_email"] = $datafamily['user_email'];
						$member["gender"] = $datafamily['gender'];
						$member["public_mobile"] = $datafamily['public_mobile'];
						$member["tenant_view"] = $datafamily['tenant_view'];
						$member["mobile_for_gatekeeper"] = $datafamily['mobile_for_gatekeeper'];
						$member["sos_alert"] = $datafamily['sos_alert'];
						$member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
						$member["member_date_of_birth"] = $datafamily['member_date_of_birth'];
						$member["child_gate_approval"] = $datafamily['child_gate_approval'];
						$dateOfBirth = $datafamily['member_date_of_birth'];
						$today = date("Y-m-d");
						// $diff = date_diff(date_create($dateOfBirth), date_create($today));
						
						$member["member_age"] = "";
						
						if ($datafamily['user_status'] == 0 && $getProfileData != 'getMemberProfileData') {
							$peMsg = "Request Pending";
						}

						if ($user_data['member_status'] == 1) {
							$member["member_relation_name"] = $peMsg . '';
							$member["member_relation_set"] = $desData['designation'];
							if ($desData['designation']!="") {
							$member["member_relation_view"] = $desData['designation'];
							} else {
							$member["member_relation_view"] ="";
							}
						} else {
							$member["member_relation_name"] = '(Primary)';
							$member["member_relation_set"] = $desData['designation'];
							if ($desData['designation']!="") {
							$member["member_relation_view"] =$desData['designation'];
							} else {
							$member["member_relation_view"] ="";
							}
						}

						$member["country_code"] = $datafamily['country_code'].'';
						$member["country_code_alt"] = $datafamily['country_code_alt'];
						$member["user_status"] = $datafamily['user_status'];
						$member["member_status"] = $datafamily['member_status'];
						$qr_size = "300x300";
						$qr_content       = $datafamily['user_mobile'];
						$qr_correction    = strtoupper('H');
						$qr_encoding      = 'UTF-8';

	    				//form google chart api link
						$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
						$member["icard_qr_code"] = $qrImageUrl . '.png';
				

						$member["company_name"] = html_entity_decode($user_data['company_name'].'');
						$member["designation"] = html_entity_decode($desData['designation'].'');
						$member["company_address"] = $user_data['company_address'].'';
						// $response["company_contact_number"] = $data['company_contact_number'];
						if ($user_data['company_contact_number']!="0") {
						$member["company_contact_number"] = $user_data['company_contact_number'].'';
						} else {
						$member["company_contact_number"] = '';
						}
						$member["company_website"] = $user_data['company_website'].'';
						if ($user_data['company_logo']!="") {
		                    $member["company_logo"] = $base_url.'img/users/recident_profile/'. $user_data['company_logo'];
		                } else {
		                    $member["company_logo"] ="";
		                }




		                if ($datafamily['user_status'] == 0) {
							$qSociety = $d->selectRow("family_member_approval","society_master", "society_id ='$society_id'");
							$societyData = mysqli_fetch_array($qSociety);
							$family_member_approval = $societyData['family_member_approval'];
							if ($family_member_approval == "1") {
								$member["you_can_appove"] = "false";
							} else {
								$member["you_can_appove"] = "true";
							}
							$member["user_status_msg"] = "Request Pending";
						} else {
							$member["user_status_msg"] = "";
							$member["you_can_appove"] = "";
						}

						array_push($response["member"], $member);

					}

					while ($dataemergency = mysqli_fetch_array($qemergency)) {

						$emergency = array();
						$emergency["emergencyContact_id"] = $dataemergency['emergencyContact_id'];
						$emergency["person_name"] = $dataemergency['person_name'];
						$emergency["person_mobile"] = $dataemergency['person_mobile'];
						$emergency["relation_id"] = $dataemergency['relation_id'] . "";
						$emergency["relation"] = $dataemergency['relation'];

						array_push($response["emergency"], $emergency);
					}

					$qA = $d->select("user_employment_details", "user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'", "");

					if (mysqli_num_rows($qA) > 0) {

						$data = mysqli_fetch_array($qA);
						$response["employment_status"] = "1";
						$response["employment_id"] = $data['employment_id'];
						$response["user_id"] = $data['user_id'];
						$response["society_id"] = $data['society_id'];
						$response["unit_id"] = $data['unit_id'];
						// $response["user_full_name"] = $data['user_full_name'];
						$response["user_phone"] = $data['user_phone']."";
						$response["user_email"] = $user_data['user_email'];
						$response["company_email"] = $user_data['user_email'];
						$response["employment_type"] = html_entity_decode($data['employment_type']);

						if ($data['business_categories'] == 'Other' && $data['business_categories_other'] != '') {
							$response["business_categories"] = html_entity_decode($data['business_categories_other']);
						} else {
							$response["business_categories"] = html_entity_decode($data['business_categories']);
						}
						if ($data['business_categories_sub'] == "Other" && $data['professional_other'] != '') {
							$response["business_categories_sub"] = html_entity_decode($data['professional_other']);
						} else {
							$response["business_categories_sub"] = html_entity_decode($data['business_categories_sub']);
						}

						$response["employment_description"] = html_entity_decode($data['employment_description']);
						$response["company_name"] = html_entity_decode($user_data['company_name']);
						$response["designation"] = html_entity_decode($data['designation']);
						$response["company_address"] = $user_data['company_address'];
						// $response["company_contact_number"] = $data['company_contact_number'];
						if ($user_data['public_mobile'] == 1 && $user_data['company_contact_number'] != 0) {
							$response["company_contact_number"] = $xml->string->private.'';
						} else if ($user_data['company_contact_number'] == 0 && $data['public_mobile'] == 0) {
							$response["company_contact_number"] = $xml->string->not_available.'';
						} else {
							$response["company_contact_number"] = $user_data['company_contact_number'];
						}
						$response["company_website"] = $user_data['company_website'];

						if ($user_data['company_logo']!="") {
		                    $response["company_logo"] = $base_url.'img/users/recident_profile/'. $user_data['company_logo'];
		                } else {
		                    $response["company_logo"] ="";

		                }

		                if ($user_data['company_brochure']!="") {
		                    $response["company_brochure"] = $base_url.'img/users/recident_profile/'. $user_data['company_brochure'];
		                } else {
		                    $response["company_brochure"] ="";
		                }

		                if ($data['visiting_card']!="") {
		                    $response["visiting_card"] = $base_url.'img/users/recident_profile/'. $data['visiting_card'];
		                } else {
		                    $response["visiting_card"] ="";
		                }

					} else {
						$response["employment_status"] = "0";

					}

					

					$qSociety = $d->select("society_master", "society_id ='$society_id'");

					$user_data_society = mysqli_fetch_array($qSociety);
					$timeline_aceess_for_society = $user_data_society['timeline_aceess_for_society'];
					
					if ($timeline_aceess_for_society==1) {
		                $timeline_aceess_for_society = true;
		             } else {
		                $timeline_aceess_for_society = false;
		             }
					

					$response["society_address"] = $user_data_society['society_address'] . "";
					$response["society_latitude"] = $user_data_society['society_latitude'] . "";
					$response["society_longitude"] = $user_data_society['society_longitude'] . "";
					$response["society_name"] = html_entity_decode($user_data_society['society_name']) . "";
					$response["city_name"] = ', ' . $user_data_society['city_name'];
					$response["api_key"] = $keydb . "";
					$response["socieaty_logo"] = $base_url . 'img/society/' . $user_data_society['socieaty_logo'] . "";

					$is_society = true;
					if ($user_data_society['society_type'] == 0) {
						$response["is_society"] = true;
						$response["label_member_type"] = "Family Members";
						$response["label_setting_apartment"] = "Apartment closed";
						$response["label_setting_resident"] = "Contact Number Privacy for Residents";
					} else if ($user_data_society['society_type'] == 1) {
						$response["is_society"] = false;
						$response["label_member_type"] = "Team Members";
						$response["label_setting_apartment"] = "Unit closed";
						$response["label_setting_resident"] = "Contact Number Privacy for Members";
					} else {
						$response["is_society"] = true;
						$response["label_member_type"] = "Family Members";
						$response["label_setting_apartment"] = "Apartment closed";
						$response["label_setting_resident"] = "Contact Number Privacy for Residents";
					}

					$notiAry = array(
                      'user_id'=>$user_id,
                      'society_id'=>$society_id,
                      'my_id'=>$my_id,
                    );

					if ($my_id!=0 && $my_id!=$user_id) {
	                 	$d->insert("users_recent_view",$notiAry);
					}

					$response["message"] = "User Details get successfully...";
					$response["status"] = "200";
					echo json_encode($response);
				} else {
					$response["message"] = "Account Not activated.";
					$response["status"] = "201";
					echo json_encode($response);
				}
			} else {

				$response["message"] = "No User Found...!";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if (isset($getEmployeeProfile) && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
			$chat_access_denied  = false;
			if ($getEmployeeProfile == 'getEmployeeProfile') {
				if ($unit_id != '' && $unit_id != 0) {
					$quc = $d->select("unit_master", "unit_id='$unit_id'");
					$getMmeberUserData= mysqli_fetch_array($quc);
					if ($getMmeberUserData['member_access_denied']== 1) {
						$response["message"] = "Access Denied..!";
						$response["status"] = "201";
						echo json_encode($response);
						exit();
					}

					if ($getMmeberUserData['chat_access_denied']== 1) {
						$chat_access_denied = true;
					}

				}
			}

			$q = $d->select("users_master,block_master,floors_master,unit_master,user_employment_details",
				"users_master.delete_status=0 AND users_master.user_id ='$user_id' AND users_master.block_id=block_master.block_id   AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND users_master.user_id=user_employment_details.user_id");

			$user_data = mysqli_fetch_array($q);

			if ($user_data == TRUE) {
				$today = date("Y-m-d");

				if ($user_data['user_status'] == 0) {
					$response["message"] = "User yet not approved by admin !";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				}


				if ($user_data['unit_status'] != '4') {

					$shiftQ = $d->select("shift_timing_master","shift_time_id = '$user_data[shift_time_id]' AND is_deleted = '0'");

					if (mysqli_num_rows($shiftQ) > 0) {
						$shiftD = mysqli_fetch_array($shiftQ);
						$shiftData = array(
							"shift_time_id" => $shiftD['shift_time_id'],
							"per_day_hour" => $shiftD['per_day_hour'],
							"shift_start_time" => $shiftD['shift_start_time'],
							"shift_end_time" => $shiftD['shift_end_time'],
							"lunch_break_start_time" => $shiftD['lunch_break_start_time'],
							"lunch_break_end_time" => $shiftD['lunch_break_end_time'],
							"tea_break_start_time" => $shiftD['tea_break_start_time'],
							"tea_break_end_time" => $shiftD['tea_break_end_time'],
						);

						$response['shift_data'] = $shiftData;
					}

					
					$response["chat_access_denied"] = $chat_access_denied;
					$response["shift_time_id"] = $user_data['shift_time_id'];
					$response["user_id"] = $user_data['user_id'];
					$response["unit_id"] = $user_data['unit_id'];
					$user_id = $user_data['user_id'];
					$response["society_id"] = $user_data['society_id'];
					$society_id = $user_data['society_id'];
					$user_full_name=$user_data['user_full_name'];
					$response["user_full_name"] = $user_full_name.'';
					$response["user_first_name"] = $user_data['user_first_name'];
					$response["user_last_name"] = $user_data['user_last_name'];

					if (strlen($user_data['user_mobile']>8) && $user_data['public_mobile']==1 && $my_id!=$user_id) {
						$user_mobile = $user_data['country_code'].'-'.substr($user_data['user_mobile'], 0, 3) . '****' . substr($user_data['user_mobile'],  -3);
						$response["user_mobile"] = $user_mobile;
						$user_mobile = $user_mobile;
					} else if (strlen($user_data['user_mobile']>8)) {
						$response["user_mobile"] = $user_data['country_code'].'-'.$user_data['user_mobile'];
						$user_mobile = $user_data['country_code'].'-'.$user_data['user_mobile'];
					} else {
						$response["user_mobile"]="";
						$user_mobile="";
					}
					if (strlen($user_data['alt_mobile']>8)) {
						$response["alt_mobile"] = $user_data['country_code_alt'].'-'.$user_data['alt_mobile'];
						$alt_mobile = $user_data['country_code_alt'].'-'.$user_data['alt_mobile'];
					} else {
						$response["alt_mobile"]="";
						$alt_mobile="";
					}
					if (strlen($user_data['emergency_number']>3)) {
						$response["emergency_number"] = $user_data['country_code_emergency'].'-'.$user_data['emergency_number'];
						$emergency_number = $user_data['country_code_emergency'].'-'.$user_data['emergency_number'];
					} else {
						$response["emergency_number"]="";
						$emergency_number="";
					}
					if (strlen($user_data['whatsapp_number']>3)) {
						$response["whatsapp_number"] = $user_data['country_code_whatsapp'].'-'.$user_data['whatsapp_number'];
						$whatsapp_number = $user_data['country_code_whatsapp'].'-'.$user_data['whatsapp_number'];
					} else {
						$response["whatsapp_number"]="";
						$whatsapp_number="";
					}
					if (strlen($user_data['user_email']) > 3) {
						$response["user_email"] = $user_data['user_email'];
						$user_email =$user_data['user_email'];
					} else {
						$response["user_email"]="";
						$user_email="";
					}
					if (strlen($user_data['user_email']) > 3) {
						$response["user_email"] = $user_data['user_email'];
						$user_email =$user_data['user_email'];
					} else {
						$response["user_email"]="";
						$user_email="";
					}
					$current_address = $user_data['last_address']."";
					$permanent_address = $user_data['permanent_address']."";
				    
				    $qr_size = "300x300";
					$qr_content       = $user_data['user_mobile'];
					$qr_correction    = strtoupper('H');
					$qr_encoding      = 'UTF-8';
    				//form google chart api link
					$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
					$response["icard_qr_code"] = $qrImageUrl . '.png';
					$unit_id = $user_data['unit_id'];
					if ($user_data['user_profile_pic'] != '') {
						$response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $user_data['user_profile_pic'];
					} else {
						$response["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
					}
					$block_id = $user_data['block_id'];
					$floor_id = $user_data['floor_id'];
					$unit_id = $user_data['unit_id'];
					
					$qSociety = $d->select("society_master", "society_id ='$society_id'");

					$user_data_society = mysqli_fetch_array($qSociety);
					$timeline_aceess_for_society = $user_data_society['timeline_aceess_for_society'];
					if ($timeline_aceess_for_society==1) {
		                $timeline_aceess_for_society = true;
		             } else {
		                $timeline_aceess_for_society = false;
		             }
					
					$response["timeline_aceess_for_society"] = $timeline_aceess_for_society;
					
					$qA = $d->select("user_employment_details", "user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'", "");
					$empData = mysqli_fetch_array($qA);

					$response["designation"] = html_entity_decode($user_data['user_designation']);
					$response["employee_id"] ="";

					if ($user_data['member_date_of_birth'] == "0000-00-00" || $user_data['member_date_of_birth'] == '') {
						$response["member_date_of_birth"] = '';

					}else{
						$response["member_date_of_birth"] = $user_data['member_date_of_birth'].'';
					}
					$response["blood_group"] = $user_data['blood_group'];
					$response["gender"] = $user_data['gender'];
					$response["marital_status"] = (!empty($user_data['marital_status'])) ? $user_data['marital_status'] : "";
					$response["total_family_members"] = (!empty($user_data['total_family_members'])) ? $user_data['total_family_members'] : "";
					$response["nationality"] = (!empty($user_data['nationality'])) ? $user_data['nationality'] : "";
					
					if ($user_data['public_mobile']==0) {
						$response["public_mobile"] = true;
						$public_mobile = true;
					} else {
						$response["public_mobile"] = false;
						$public_mobile = false;
					}

					$marital_status_string = "";
					if($user_data['marital_status'] == 1 || $user_data['marital_status'] == "1")
					{
						$marital_status_string = "Single";
					}
					else if($user_data['marital_status'] == 2 || $user_data['marital_status'] == "2")
					{
						$marital_status_string = "Married";
					}
					else if($user_data['marital_status'] == 3 || $user_data['marital_status'] == "3")
					{
						$marital_status_string = "Widowed";
					}
					else if($user_data['marital_status'] == 4 || $user_data['marital_status'] == "4")
					{
						$marital_status_string = "Seperated";
					}
					else if($user_data['marital_status'] == 5 || $user_data['marital_status'] == "5")
					{
						$marital_status_string = "Divorced";
					}

					$response["personal"] = array();

					if ($user_data['member_date_of_birth'] == "0000-00-00" || $user_data['member_date_of_birth'] == '') {
						$member_birthdate_f = "";
					}else{
						$member_birthdate_f = date("d-m-Y",strtotime($user_data['member_date_of_birth']));
					}

					if ($user_data['member_date_of_birth'] == "0000-00-00" || $user_data['member_date_of_birth'] == '') {
						$member_birthdate_f_n = "";
					}else{
						$member_birthdate_f_n = $user_data['member_date_of_birth'];
					}
					
					$personal = array(
						"designation"=> html_entity_decode($user_data['user_designation']),
						"employee_id"=>(!empty($user_data['company_employee_id'])) ? html_entity_decode($user_data['company_employee_id']) : "", 
						"member_date_of_birth"=>$member_birthdate_f_n.'', 
						"blood_group"=>$user_data['blood_group'], 
						"gender"=>$user_data['gender'], 
						"marital_status"=>(!empty($user_data['marital_status'])) ? $user_data['marital_status'] : "", 
						"marital_status_st"=>$marital_status_string, 
						"total_family_members"=>(!empty($user_data['total_family_members'])) ? $user_data['total_family_members'] : "",
						"nationality"=>(!empty($user_data['nationality'])) ? html_entity_decode($user_data['nationality']) : "",
						"member_date_of_birth_view"=>$member_birthdate_f
					);

					array_push($response["personal"], $personal);


					$response["contact"] = array();
					$contact = array(
						"user_mobile"=>$user_mobile,
						"alt_mobile"=>$alt_mobile, 
						"whatsapp_number"=>$whatsapp_number, 
						"emergency_number"=>$emergency_number, 
						"user_email"=>$user_email, 
						"current_address"=>html_entity_decode($current_address),
						"permanent_address"=>html_entity_decode($permanent_address),
						"public_mobile"=>$public_mobile,
						"user_mobile_country_code"=>(!empty($user_data['country_code'])) ? $user_data['country_code'] : "",
						"without_country_code_user_mobile"=>(!empty($user_data['user_mobile'])) ? $user_data['user_mobile'] : "",
						"alt_mobile_country_code"=>(!empty($user_data['country_code_alt'])) ? $user_data['country_code_alt'] : "", 
						"without_country_code_alt_mobile"=>(!empty($user_data['alt_mobile'])) ? $user_data['alt_mobile'] : "",
						"whatsapp_number_country_code"=>(!empty($user_data['country_code_whatsapp'])) ? $user_data['country_code_whatsapp'] : "", 
						"without_country_code_whatsapp_number"=>(!empty($user_data['whatsapp_number'])) ? $user_data['whatsapp_number'] : "",
						"emergency_number_country_code"=>(!empty($user_data['country_code_emergency'])) ? $user_data['country_code_emergency'] : "", 
						"without_country_code_emergency_number"=>(!empty($user_data['emergency_number'])) ? $user_data['emergency_number'] : ""
					);
					
					array_push($response["contact"], $contact);


		            $response["employeement_skills"] = array();

		            $qe = $d->select("user_employment_details", "user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'", "LIMIT 1");
					while ($empSkil = mysqli_fetch_array($qe)) {
						$employeement_skills = array();
                    	$employeement_skills["employment_type"] = html_entity_decode($empSkil['employment_type']);
                    	$employeement_skills["joining_date"] =($empSkil['joining_date'] == "0000-00-00" || $empSkil['joining_date']=="") ? "" : date('Y-m-d', strtotime($empSkil['joining_date']));
                    	$employeement_skills["joining_date_view"] =($empSkil['joining_date'] == "0000-00-00" || $empSkil['joining_date']=="") ? "" : date("d/m/Y",strtotime($empSkil['joining_date']));
                    	$employeement_skills["total_experience"] = html_entity_decode($empSkil['total_experience']);

                    	$shiftQ = $d->select("shift_timing_master","shift_time_id = '$user_data[shift_time_id]' AND is_deleted = '0'");

						if (mysqli_num_rows($shiftQ) > 0) {
							$shiftD = mysqli_fetch_array($shiftQ);
							
							$employeement_skills["shift_timing"] = date('h:i A',strtotime($shiftD['shift_start_time'])). ' To '.date('h:i A',strtotime($shiftD['shift_end_time']));
						}else{
							$employeement_skills["shift_timing"] = html_entity_decode($empSkil['shift_timing']);
						}

                    	$employeement_skills["job_location"] = html_entity_decode($empSkil['job_location']);
                    	$employeement_skills["intrest_hobbies"] = html_entity_decode($empSkil['intrest_hobbies']);
                    	$employeement_skills["professional_skills"] = html_entity_decode($empSkil['professional_skills']);
                    	$employeement_skills["special_skills"] = html_entity_decode($empSkil['special_skills']);
                    	$employeement_skills["language_known"] = html_entity_decode($empSkil['language_known']);

	                    array_push($response["employeement_skills"], $employeement_skills);


					}

		            $response["experience"] = array();

		            $qep= $d->select("employee_experience", "user_id='$user_id'", "");
					while ($empExp = mysqli_fetch_array($qep)) {
						$experience = array();
                    	$experience["employee_experience_id"] = $empExp['employee_experience_id'];
                    	$experience["designation"] = html_entity_decode($empExp['designation']);
                    	$experience["work_from"] =  date('M Y', strtotime($empExp['work_from'])) .' - '. date('M Y', strtotime($empExp['work_to']));
                    	$experience["company_location"] = html_entity_decode($empExp['company_location']);
                    	$experience["exp_company_name"] = html_entity_decode($empExp['exp_company_name']);
                    	
	                    array_push($response["experience"], $experience);
                    	

					}

		            $response["education"] = array();

		            $qar= $d->select("employee_achievements_education", "user_id='$user_id'", "");
					while ($empArr = mysqli_fetch_array($qar)) {
						$education = array();
                    	$education["employee_achievement_id"] = $empArr['employee_achievement_id'];
                    	$education["achievement_name"] =html_entity_decode($empArr['achievement_name']);
                    	$education["achievement_date"] =  date('d M Y', strtotime($empArr['achievement_date']));
                    	$education["university_board_name"] = html_entity_decode($empArr['university_board_name']);
                    	$education["archi_type"] = $empArr['archi_type'];
	                    array_push($response["education"], $education);
                    	

					}

		            $response["social_link"] = array();
					$social_link = array(
						"facebook"=>$user_data['facebook'], 
						"instagram"=>$user_data['instagram'], 
						"linkedin"=>$user_data['linkedin'], 
						"twitter"=>$user_data['twitter'], 
						"whatsapp_number"=>$whatsapp_number,
						"whatsapp_number_country_code"=>(!empty($user_data['country_code_whatsapp'])) ? $user_data['country_code_whatsapp'] : "",
						"whatsapp_number_without_country_code"=>(!empty($user_data['whatsapp_number'])) ? $user_data['whatsapp_number'] : ""
					);
					array_push($response["social_link"], $social_link);


					$employment_edu = $d->selectRow("employee_experience_id as emp_edu","employee_experience", "user_id='$user_id'","");
					$employment_edu_arr = mysqli_fetch_array($employment_edu);


					$employment_achi = $d->selectRow("archi_type as emp_achi","employee_achievements_education", "user_id='$user_id'","");
					$employment_achi_arr = mysqli_fetch_array($employment_achi);
					
					$profile_progress = 0;
					if ($user_data['user_full_name']!="") {
						$profile_progress +="10";
					}
					if ($user_data['user_profile_pic']!="" && $user_data['user_profile_pic']!="user_default.png") {
						$profile_progress +="10";
					}
					if ($user_data['user_mobile']!="") {
						$profile_progress +="10";
					}
					if ($user_data['gender']!="") {
						$profile_progress +="5";
					}
					if ($user_data['last_address']!="") {
						$profile_progress +="5";
					}
					if ($user_data['user_email']!="") {
						$profile_progress +="5";
					}
					if ($user_data['whatsapp_number']>0) {
						$profile_progress +="5";
					}
					if ($user_data['emergency_number']>0) {
						$profile_progress +="5";
					}
					if ($user_data['permanent_address']!="") {
						$profile_progress +="3";
					}
					if ($user_data['member_date_of_birth']!="") {
						$profile_progress +="3";
					}
					if ($user_data['nationality']!="") {
						$profile_progress +="3";
					}
					if ($user_data['alt_mobile']>0) {
						$profile_progress +="3";
					}
					if ($user_data['blood_group']!="") {
						$profile_progress +="3";
					}
					if ($user_data['user_designation']!="") {
						$profile_progress +="3";
					}
					if ($user_data['marital_status']>0) {
						$profile_progress +="3";
					}
					if ($user_data['total_family_members']>0) {
						$profile_progress +="2";
					}
					if ($user_data['facebook']!="") {
						$profile_progress +="2";
					}
					if ($user_data['instagram']!="") {
						$profile_progress +="2";
					}
					if ($user_data['linkedin']!="") {
						$profile_progress +="2";
					}
					if ($user_data['twitter']!="") {
						$profile_progress +="2";
					}
					if ($empData['job_location']!="") {
						$profile_progress +="2";
					}
					if ($empData['intrest_hobbies']!="") {
						$profile_progress +="2";
					}
					if ($empData['professional_skills']!="") {
						$profile_progress +="2";
					}
					if ($empData['special_skills']!="") {
						$profile_progress +="2";
					}
					if ($empData['language_known']!="") {
						$profile_progress +="2";
					}
					if ($employment_edu_arr['emp_edu']!="" && !empty($employment_edu_arr['emp_edu'])) {
						$profile_progress +="2";
					}
					if (mysqli_num_rows($employment_achi)>0) {
						$profile_progress +="2";
					}
					
					if ($profile_progress>100) {
						$profile_progress = "100";
					}

					$response["profile_progress"] = $profile_progress."";


					$notiAry = array(
                      'user_id'=>$user_id,
                      'society_id'=>$society_id,
                      'my_id'=>$my_id,
                      'view_time'=>date("Y-m-d H:i:s"),
                    );

					$qre  = $d->selectRow("user_id","users_recent_view" , "user_id='$user_id' AND my_id='$my_id' ");
					if ($my_id!=0 && $my_id!=$user_id && mysqli_num_rows($qre)==0) {
	                 	$d->insert("users_recent_view",$notiAry);
					} else if($my_id!=0 && $my_id!=$user_id) {
	                 	$d->update("users_recent_view",$notiAry,"user_id='$user_id' AND my_id='$my_id' ");
					}

					$response["message"] = "Get successfully...";
					$response["status"] = "200";
					echo json_encode($response);
				} else {
					$response["message"] = "Account Not activated.";
					$response["status"] = "201";
					echo json_encode($response);
				}
			} else {

				$response["message"] = "No User Found...!";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if (isset($getEmployeeProfileNew) && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
			$chat_access_denied  = false;
			if ($getEmployeeProfileNew == 'getEmployeeProfileNew') {
				if ($unit_id != '' && $unit_id != 0) {
					$quc = $d->select("unit_master", "unit_id='$unit_id'");
					$getMmeberUserData= mysqli_fetch_array($quc);
					if ($getMmeberUserData['member_access_denied']== 1) {
						$response["message"] = "Access Denied..!";
						$response["status"] = "201";
						echo json_encode($response);
						exit();
					}

					if ($getMmeberUserData['chat_access_denied']== 1) {
						$chat_access_denied = true;
					}

				}
			}

			$q = $d->select("users_master,block_master,floors_master,unit_master,user_employment_details",
				"users_master.delete_status=0 AND users_master.user_id ='$user_id' AND users_master.block_id=block_master.block_id   AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND users_master.user_id=user_employment_details.user_id");

			$user_data = mysqli_fetch_array($q);

			if ($user_data == TRUE) {
				$today = date("Y-m-d");

				if ($user_data['user_status'] == 0) {
					$response["message"] = "User yet not approved by admin !";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				}


				if ($user_data['unit_status'] != '4') {

					$shiftQ = $d->select("shift_timing_master","shift_time_id = '$user_data[shift_time_id]' AND is_deleted = '0'");

					if (mysqli_num_rows($shiftQ) > 0) {
						$shiftD = mysqli_fetch_array($shiftQ);

						$todayDate = date('Y-m-d');

						$shiftType = $shiftD['shift_type'];

						
						$shiftStartTimeView = date('h:i A',strtotime($todayDate.' '.$shiftD['shift_start_time']));
						$shift_start_time = $shiftD['shift_start_time'];
						
						if ($shiftType == "Night") {
							$shiftEndTimeView = date('h:i A',strtotime($todayDate.' '.$shiftD['shift_end_time']));
						}else{				
							$nextDate = date('Y-m-d',strtotime("+1 days"));			
							$shiftEndTimeView = date('h:i A',strtotime($nextDate.' '.$shiftD['shift_end_time']));
						}
						
						$shift_end_time = $shiftD['shift_end_time'];
						
						
						$lunch_break_start_time_view = date('h:i A',strtotime($todayDate.' '.$shiftD['lunch_break_start_time']));
						$lunchTimeStart = date('Y-m-d').' '.$lunch_break_start_time_view;
						$lunch_break_start_time = $shiftD['lunch_break_start_time'];
						
						
						$lunch_break_end_time_view = date('h:i A',strtotime($todayDate.' '.$shiftD['lunch_break_end_time']));
						$lunchTimeEnd = date('Y-m-d').' '.$lunch_break_end_time_view;
						$totalLunchTime = getTotalHoursWithNames($lunchTimeStart,$lunchTimeEnd);
						$lunch_break_end_time = $shiftD['lunch_break_end_time'];
						

						if ($shiftD['lunch_break_start_time'] == "00:00:00" && $shiftD['lunch_break_end_time'] == "00:00:00") {
							$lunch_break_start_time_view = "";
							$lunchTimeStart = "";
							$lunch_break_start_time = "";

							$lunch_break_end_time_view = "";
							$lunchTimeEnd = "";
							$lunch_break_end_time = "";
							$totalLunchTime = "";
						}
						
						$tea_break_start_time_view = date('h:i A',strtotime($todayDate.' '.$shiftD['tea_break_start_time']));
						$tea_break_start_time = $shiftD['tea_break_start_time'];
						$teaTimeStart = date('Y-m-d').' '.$tea_break_start_time_view;
						
						$tea_break_end_time_view = date('h:i A',strtotime($todayDate.' '.$shiftD['tea_break_end_time']));
						$tea_break_end_time = $shiftD['tea_break_end_time'];
						$teaTimeEnd = date('Y-m-d').' '.$tea_break_end_time_view;
						$totalTeaTime = getTotalHoursWithNames($teaTimeStart,$teaTimeEnd);
						

						if ($shiftD['tea_break_start_time'] == "00:00:00" && $shiftD['tea_break_end_time'] == "00:00:00") {
							$tea_break_start_time_view = "";
							$teaTimeStart = "";
							$tea_break_start_time = "";

							$tea_break_end_time_view = "";
							$tea_break_end_time = "";
							$totalTeaTime = "";
						}


						if ($shiftD['half_day_time_start'] == "00:00:00" || $shiftD['half_day_time_start'] == null) {
							$half_day_time_start_view = "";
							$half_day_time_start = "";
						}else{
							$half_day_time_start_view = date('h:i A',strtotime($shiftD['half_day_time_start']));
							$half_day_time_start = $shiftD['half_day_time_start'];
						}

						if ($shiftD['halfday_before_time'] == "00:00:00" || $shiftD['halfday_before_time'] == null) {
							$halfday_before_time_view = "";
							$halfday_before_time = "";
						}else{
							$halfday_before_time_view = date('h:i A',strtotime($shiftD['halfday_before_time']));
							$halfday_before_time = $shiftD['halfday_before_time'];
						}

						if ($shiftD['late_time_start'] == "00:00:00" || $shiftD['late_time_start'] == null) {
							$late_time_start_view = "";
							$late_time_start = "";
						}else{
							$late_time_start_view = date('i',strtotime($shiftD['late_time_start'])).' Minutes';
							$late_time_start = $shiftD['late_time_start'];
						}

						if ($shiftD['early_out_time'] == "00:00:00" || $shiftD['early_out_time'] == null) {
							$early_out_time_view = "";
							$early_out_time = "";
						}else{
							$early_out_time_view = date('i',strtotime($shiftD['early_out_time'])). ' Minutes';
							$early_out_time = $shiftD['early_out_time'];
						}

						if ($shiftD['maximum_halfday_hours'] == "00:00:00" || $shiftD['maximum_halfday_hours'] == null) {
							$maximum_halfday_hours_view = "";
							$maximum_halfday_hours_view_total = "";
							$maximum_halfday_hours = "";
						}else{
							$maximum_halfday_hours_view = date('h:i A',strtotime($shiftD['maximum_halfday_hours']));
							$maximum_halfday_hours = $shiftD['maximum_halfday_hours'];
							$maximum_halfday_hours_view_total = getFormatedTime($maximum_halfday_hours_view);
						}

						if ($shiftD['minimum_hours_for_full_day'] == "00:00:00" || $shiftD['minimum_hours_for_full_day'] == null) {
							$minimum_hours_for_full_day_view = "";
							$minimum_hours_for_full_day = "";
							$minimum_hours_for_full_day_view_total = "";
						}else{
							$minimum_hours_for_full_day_view = date('h:i A',strtotime($shiftD['minimum_hours_for_full_day']));
							$minimum_hours_for_full_day = $shiftD['minimum_hours_for_full_day'];
							$minimum_hours_for_full_day_view_total = getFormatedTime($minimum_hours_for_full_day_view);
						}

						$per_day_hour_view = getFormatedTime($shiftD['per_day_hour']);	

						$dayNamesAlternate = explode(",",$shiftD['alternate_weekoff_days']);
						$dayNamesWeekOffs = explode(",",$shiftD['week_off_days']);	

						$allWeekOffs = array_merge($dayNamesWeekOffs,$dayNamesWeekOffs);	
						$allWeekOffs = array_unique($allWeekOffs);	

						$allWeekOffsView = "";

						for ($i=0; $i < count($allWeekOffs); $i++) { 
							if ($allWeekOffs[$i] == 0) {												
								if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
									$allWeekOffsView .="Sunday (Alternate)"."\n";
								}else{
									$allWeekOffsView .="Sunday (Every Week)"."\n";
								}
							}else if ($allWeekOffs[$i] == 1) {
								if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
									$allWeekOffsView .="Monday (Alternate)"."\n";
								}else{
									$allWeekOffsView .="Monday (Every Week)"."\n";
								}
							}else if ($allWeekOffs[$i] == 2) {
								if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
									$allWeekOffsView .="Tuesday (Alternate)"."\n";
								}else{
									$allWeekOffsView .="Tuesday (Every Week)"."\n";
								}
							}else if ($allWeekOffs[$i] == 3) {
								if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
									$allWeekOffsView .="Wednesday (Alternate)"."\n";
								}else{
									$allWeekOffsView .="Wednesday (Every Week)"."\n";
								}
							}else if ($allWeekOffs[$i] == 4) {
								if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
									$allWeekOffsView .="Thursday (Alternate)"."\n";
								}else{
									$allWeekOffsView .="Thursday (Every Week)"."\n";
								}
							}else if ($allWeekOffs[$i] == 5) {
								if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
									$allWeekOffsView .="Friday (Alternate)"."\n";
								}else{
									$allWeekOffsView .="Friday (Every Week)"."\n";
								}
							}else if ($allWeekOffs[$i] == 6) {
								if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
									$allWeekOffsView .="Saturday (Alternate)"."\n";
								}else{
									$allWeekOffsView .="Saturday (Every Week)"."\n";
								}
							}
						}

						$allWeekOffsView = substr($allWeekOffsView, 0, -1);;

						$shiftData = array(
							"shift_time_id" => $shiftD['shift_time_id'],
							"shift_name" => $shiftD['shift_name'],
							"shift_code" => "S".$shiftD['shift_time_id'],
							"per_day_hour" => $shiftD['per_day_hour'],
							"shift_start_time" => $shift_start_time,
							"shift_end_time" => $shift_end_time,
							"lunch_break_start_time" => $lunch_break_start_time,
							"lunch_break_end_time" => $lunch_break_end_time,
							"tea_break_start_time" => $tea_break_start_time,
							"tea_break_end_time" => $tea_break_end_time,							
							"week_off_days" => $shiftD['week_off_days'],
							"half_day_time_start" => $half_day_time_start,
							"halfday_before_time" => $halfday_before_time,
							"late_time_start" => $late_time_start,
							"maximum_in_out" => $shiftD['maximum_in_out'],
							"early_out_time" => $early_out_time,
							"late_in_reason" => $shiftD['late_in_reason'],
							"early_out_reason" => $shiftD['early_out_reason'],
							"has_altenate_week_off" => $shiftD['has_altenate_week_off'],
							"alternate_week_off" => $shiftD['alternate_week_off'],
							"alternate_weekoff_days" => $shiftD['alternate_weekoff_days'],
							"shift_type" => $shiftD['shift_type'],
							"maximum_halfday_hours" => $maximum_halfday_hours,
							"minimum_hours_for_full_day" => $minimum_hours_for_full_day,

							"dayNamesView" => $allWeekOffsView,
							"per_day_hour_view" => $per_day_hour_view,
							"shift_start_time_view" => $shiftStartTimeView,
							"shift_end_time_view" => $shiftEndTimeView,
							"lunch_break_start_time_view" => $lunch_break_start_time_view,
							"lunch_break_end_time_view" => $lunch_break_end_time_view,
							"total_lunch_time" => $totalLunchTime,
							"tea_break_start_time_view" => $tea_break_start_time_view,
							"tea_break_end_time_view" => $tea_break_end_time_view,							
							"total_tea_time" => $totalTeaTime,							
							"half_day_time_start_view" => $half_day_time_start_view,
							"halfday_before_time_view" => $halfday_before_time_view,
							"late_time_start_view" => $late_time_start_view,
							"early_out_time_view" => $late_time_start_view,
							"maximum_halfday_hours_view" => $maximum_halfday_hours_view,
							"maximum_halfday_hours_view_total" => $maximum_halfday_hours_view_total,
							"minimum_hours_for_full_day_view" => $minimum_hours_for_full_day_view,
							"minimum_hours_for_full_day_view_total" => $minimum_hours_for_full_day_view_total,
						);

						$response['shift_data'] = $shiftData;
					}

					
					$response["chat_access_denied"] = $chat_access_denied;
					$response["shift_time_id"] = $user_data['shift_time_id'];
					$response["user_id"] = $user_data['user_id'];
					$response["unit_id"] = $user_data['unit_id'];
					$user_id = $user_data['user_id'];
					$response["society_id"] = $user_data['society_id'];
					$society_id = $user_data['society_id'];
					$user_full_name=$user_data['user_full_name'];
					$response["user_full_name"] = $user_full_name.'';
					$response["user_first_name"] = $user_data['user_first_name'];
					$response["user_last_name"] = $user_data['user_last_name'];

					if (strlen($user_data['user_mobile']>8) && $user_data['public_mobile']==1 && $my_id!=$user_id) {
						$user_mobile = $user_data['country_code'].'-'.substr($user_data['user_mobile'], 0, 3) . '****' . substr($user_data['user_mobile'],  -3);
						$response["user_mobile"] = $user_mobile;
						$user_mobile = $user_mobile;
					} else if (strlen($user_data['user_mobile']>8)) {
						$response["user_mobile"] = $user_data['country_code'].'-'.$user_data['user_mobile'];
						$user_mobile = $user_data['country_code'].'-'.$user_data['user_mobile'];
					} else {
						$response["user_mobile"]="";
						$user_mobile="";
					}

					if (strlen($user_data['alt_mobile']>8) && $user_data['public_mobile']==1 && $my_id!=$user_id) {
						$alt_mobile = $user_data['country_code_alt'].'-'.substr($user_data['alt_mobile'], 0, 3) . '****' . substr($user_data['alt_mobile'],  -3);
						$response["alt_mobile"] = $alt_mobile;
						$alt_mobile = $alt_mobile;
					} else if (strlen($user_data['alt_mobile']>8)) {
						$response["alt_mobile"] = $user_data['country_code_alt'].'-'.$user_data['alt_mobile'];
						$alt_mobile = $user_data['country_code_alt'].'-'.$user_data['alt_mobile'];
					} else {
						$response["alt_mobile"]="";
						$alt_mobile="";
					}

					if (strlen($user_data['emergency_number']>8) && $user_data['public_mobile']==1 && $my_id!=$user_id) {
						$emergency_number = $user_data['country_code_emergency'].'-'.substr($user_data['emergency_number'], 0, 3) . '****' . substr($user_data['emergency_number'],  -3);
						$response["emergency_number"] = $emergency_number;
						$emergency_number = $emergency_number;
					} else if (strlen($user_data['emergency_number']>8)) {
						$response["emergency_number"] = $user_data['country_code_emergency'].'-'.$user_data['emergency_number'];
						$emergency_number = $user_data['country_code_emergency'].'-'.$user_data['emergency_number'];
					} else {
						$response["emergency_number"]="";
						$emergency_number="";
					}

					if (strlen($user_data['whatsapp_number']>8) && $user_data['public_mobile']==1 && $my_id!=$user_id) {
						$whatsapp_number = $user_data['country_code_whatsapp'].'-'.substr($user_data['whatsapp_number'], 0, 3) . '****' . substr($user_data['whatsapp_number'],  -3);
						$response["whatsapp_number"] = $whatsapp_number;
						$whatsapp_number = $whatsapp_number;
					} else if (strlen($user_data['whatsapp_number']>8)) {
						$response["whatsapp_number"] = $user_data['country_code_whatsapp'].'-'.$user_data['whatsapp_number'];
						$whatsapp_number = $user_data['country_code_whatsapp'].'-'.$user_data['whatsapp_number'];
					} else {
						$response["whatsapp_number"]="";
						$whatsapp_number="";
					}

					if (strlen($user_data['user_email'])>3 && $user_data['public_mobile']==1 && $my_id!=$user_id) {
						$user_email_masked = substr($user_data['user_email'], 0, 3) . '******' . substr($user_data['user_email'],  -3);
						$user_email = $user_email_masked;
						$response["user_email"] = $user_email;
					} else if (strlen($user_data['user_email'])>3) {
						$user_email = $user_data['user_email'];
						$response["user_email"] = $user_email;
					} else {
						$response["user_email"]="";
						$user_email="";
					}

					if (strlen($user_data['personal_email'])>3 && $user_data['public_mobile']==1 && $my_id!=$user_id) {
						$personal_email_masked = substr($user_data['personal_email'], 0, 3) . '******' . substr($user_data['personal_email'],  -3);
						$personal_email = $personal_email_masked;
						$response["personal_email"] = $personal_email;
					} else if (strlen($user_data['personal_email'])>3) {
						$personal_email = $user_data['personal_email'];
						$response["personal_email"] = $personal_email;
					} else {
						$response["personal_email"]="";
						$personal_email="";
					}


					if ($user_data['public_mobile']==1 && $user_data['last_address'] != '' && $my_id!=$user_id) {
						$current_address = "Private";
					}else{
						$current_address = $user_data['last_address']."";
					}

					if ($user_data['public_mobile']==1 && $user_data['permanent_address'] != '' && $my_id!=$user_id) {
						$permanent_address = "Private";
					}else{
						$permanent_address = $user_data['permanent_address']."";
					}
					
				    
				    $qr_size = "300x300";
					$qr_content       = $user_data['user_mobile'];
					$qr_correction    = strtoupper('H');
					$qr_encoding      = 'UTF-8';
    				//form google chart api link
					$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
					$response["icard_qr_code"] = $qrImageUrl . '.png';
					$unit_id = $user_data['unit_id'];
					if ($user_data['user_profile_pic'] != '') {
						$response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $user_data['user_profile_pic'];
					} else {
						$response["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
					}
					$block_id = $user_data['block_id'];
					$floor_id = $user_data['floor_id'];
					$unit_id = $user_data['unit_id'];
					
					$qSociety = $d->select("society_master", "society_id ='$society_id'");

					$user_data_society = mysqli_fetch_array($qSociety);
					$timeline_aceess_for_society = $user_data_society['timeline_aceess_for_society'];
					if ($timeline_aceess_for_society==1) {
		                $timeline_aceess_for_society = true;
		             } else {
		                $timeline_aceess_for_society = false;
		             }
					
					$response["timeline_aceess_for_society"] = $timeline_aceess_for_society;
					
					$qA = $d->select("user_employment_details", "user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'", "");
					$empData = mysqli_fetch_array($qA);

					$response["designation"] = html_entity_decode($user_data['user_designation']);
					$response["employee_id"] ="";

					if ($user_data['member_date_of_birth'] == "0000-00-00" || $user_data['member_date_of_birth'] == '') {
						$response["member_date_of_birth"] = '';

					}else{
						$response["member_date_of_birth"] = $user_data['member_date_of_birth'].'';
					}
					$response["blood_group"] = $user_data['blood_group'];
					$response["gender"] = $user_data['gender'];
					$response["marital_status"] = (!empty($user_data['marital_status'])) ? $user_data['marital_status'] : "";
					$response["total_family_members"] = (!empty($user_data['total_family_members'])) ? $user_data['total_family_members'] : "";
					$response["nationality"] = (!empty($user_data['nationality'])) ? $user_data['nationality'] : "";
					
					if ($user_data['public_mobile']==0) {
						$response["public_mobile"] = true;
						$public_mobile = true;
					} else {
						$response["public_mobile"] = false;
						$public_mobile = false;
					}

					$marital_status_string = "";
					if($user_data['marital_status'] == 1 || $user_data['marital_status'] == "1")
					{
						$marital_status_string = "Single";
					}
					else if($user_data['marital_status'] == 2 || $user_data['marital_status'] == "2")
					{
						$marital_status_string = "Married";
					}
					else if($user_data['marital_status'] == 3 || $user_data['marital_status'] == "3")
					{
						$marital_status_string = "Widowed";
					}
					else if($user_data['marital_status'] == 4 || $user_data['marital_status'] == "4")
					{
						$marital_status_string = "Seperated";
					}
					else if($user_data['marital_status'] == 5 || $user_data['marital_status'] == "5")
					{
						$marital_status_string = "Divorced";
					}

					$response["personal"] = array();

					if ($user_data['member_date_of_birth'] == "0000-00-00" || $user_data['member_date_of_birth'] == '') {
						$member_birthdate_f = "";
					}else{
						$member_birthdate_f = date("d-m-Y",strtotime($user_data['member_date_of_birth']));
					}

					if ($user_data['member_date_of_birth'] == "0000-00-00" || $user_data['member_date_of_birth'] == '') {
						$member_birthdate_f_n = "";
					}else{
						$member_birthdate_f_n = $user_data['member_date_of_birth'];
					}


					if ($user_data['wedding_anniversary_date'] == "0000-00-00" || $user_data['wedding_anniversary_date'] == '' || $user_data['wedding_anniversary_date'] == null) {
						$wedding_anniversary_date_f = "";
					}else{
						$wedding_anniversary_date_f = date("d-m-Y",strtotime($user_data['wedding_anniversary_date']));
					}

					if ($user_data['wedding_anniversary_date'] == "0000-00-00" || $user_data['wedding_anniversary_date'] == ''|| $user_data['wedding_anniversary_date'] == null) {
						$wedding_anniversary_date_f_n = "";
					}else{
						$wedding_anniversary_date_f_n = $user_data['wedding_anniversary_date'];
					}

					$response["user_face_data"] = $user_data['user_face_data'].'';
		            if ($user_data['face_added_date'] != '' && $user_data['face_added_date'] != '0000-00-00') {
		                $response["face_added_date"] = date('h:i A, d M Y',strtotime($user_data['face_added_date']));
		            } else {
		                $response["face_added_date"] = "";
		            }


		            if ($user_data['face_data_image'] != '') {
		                $response["face_data_image"] = $base_url . "img/attendance_face_image/" . $user_data['face_data_image'];
		            } else {
		                $response["face_data_image"] = "";
		            }

		            if ($user_data['face_data_image_two'] != '') {
		                $response["face_data_image_two"] = $base_url . "img/attendance_face_image/" . $user_data['face_data_image_two'];
		            } else {
		                $response["face_data_image_two"] = "";
		            }

					$qe = $d->select("user_employment_details", "user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'", "LIMIT 1");
					$empSkil = mysqli_fetch_array($qe);

                   	$response["employeement_skills"] = array();

                   	$empe_exp2=$d->select("employee_experience","user_id='$id' AND society_id='$society_id'");

					$eaec=$d->select("employee_achievements_education","user_id='$user_id' AND society_id='$society_id'");
					$expDataar = array();

					if(mysqli_num_rows($empe_exp2)>0){
					  while($tData=mysqli_fetch_assoc($empe_exp2)){
					    array_push($expDataar,$tData);
					  }
					  array_multisort(array_column($expDataar, 'work_from'), SORT_ASC, $expDataar);
					 
					  $sdate = $expDataar[0]['work_from'];
					  $edate = date('Y-m-d');
					  $getExperianseYears = $d->getMonthDays($sdate,$edate);
					  
					}else
					{
					  if($empSkil['joining_date'] !="" && $empSkil['joining_date'] !="0000-00-00"){

					    $sdate = $empSkil['joining_date'];
					    $edate = date('Y-m-d');
					    $getExperianseYears = $d->getMonthDays($sdate,$edate);
					  }
					 
					}

					
					$intrest_hobbies = html_entity_decode($empSkil['intrest_hobbies']);
                	$professional_skills = html_entity_decode($empSkil['professional_skills']);
                	$special_skills = html_entity_decode($empSkil['special_skills']);
                	$language_known = html_entity_decode($empSkil['language_known']);
	       
					$employeement_skills = array();
					$employeement_skills["designation"]= html_entity_decode($user_data['user_designation']);
					$employeement_skills["employee_id"]=(!empty($user_data['company_employee_id'])) ? html_entity_decode($user_data['company_employee_id']) : "";

					if ($empSkil['employment_type'] == 1) {
						$employeement_skills["employment_type"] = "Full Time";
					}else if ($empSkil['employment_type'] == 2) {
						$employeement_skills["employment_type"] = "Part Time";
					}else if ($empSkil['employment_type'] == 3) {
						$employeement_skills["employment_type"] = "Seasonal";
					}else if ($empSkil['employment_type'] == 4) {
						$employeement_skills["employment_type"] = "Temporary";
					}

                	// $employeement_skills["employment_type"] = html_entity_decode($empSkil['employment_type']);
                	$employeement_skills["joining_date"] =($empSkil['joining_date'] == "0000-00-00" || $empSkil['joining_date']=="") ? "" : date('Y-m-d', strtotime($empSkil['joining_date']));
                	$employeement_skills["joining_date_view"] =($empSkil['joining_date'] == "0000-00-00" || $empSkil['joining_date']=="") ? "" : date("d/m/Y",strtotime($empSkil['joining_date']));
                	$employeement_skills["total_experience"] = "$getExperianseYears";

                    array_push($response["employeement_skills"], $employeement_skills);


					
					
					$personal = array(						
						"member_date_of_birth"=>$member_birthdate_f_n.'', 
						"wedding_anniversary_date"=>$wedding_anniversary_date_f_n.'', 
						"blood_group"=>$user_data['blood_group'], 
						"gender"=>$user_data['gender'], 
						"marital_status"=>(!empty($user_data['marital_status'])) ? $user_data['marital_status'] : "", 
						"marital_status_st"=>$marital_status_string, 
						"total_family_members"=>(!empty($user_data['total_family_members'])) ? $user_data['total_family_members'] : "",
						"nationality"=>(!empty($user_data['nationality'])) ? html_entity_decode($user_data['nationality']) : "",
						"member_date_of_birth_view"=>$member_birthdate_f,
						"wedding_anniversary_date_view"=>$wedding_anniversary_date_f,
						"intrest_hobbies" => $intrest_hobbies,
                    	"professional_skills" => $professional_skills,
                    	"special_skills" => $special_skills,
                    	"language_known" => $language_known,
					);

					array_push($response["personal"], $personal);


					$response["contact"] = array();
					$contact = array(
						"user_mobile"=>$user_mobile,
						"alt_mobile"=>$alt_mobile, 
						"whatsapp_number"=>$whatsapp_number, 
						"emergency_number"=>$emergency_number, 
						"user_email"=>$user_email, 
						"personal_email"=>$personal_email, 
						"current_address"=>html_entity_decode($current_address),
						"permanent_address"=>html_entity_decode($permanent_address),
						"public_mobile"=>$public_mobile,
						"user_mobile_country_code"=>(!empty($user_data['country_code'])) ? $user_data['country_code'] : "",
						"without_country_code_user_mobile"=>(!empty($user_data['user_mobile'])) ? $user_data['user_mobile'] : "",
						"alt_mobile_country_code"=>(!empty($user_data['country_code_alt'])) ? $user_data['country_code_alt'] : "", 
						"without_country_code_alt_mobile"=>(!empty($user_data['alt_mobile'])) ? $user_data['alt_mobile'] : "",
						"whatsapp_number_country_code"=>(!empty($user_data['country_code_whatsapp'])) ? $user_data['country_code_whatsapp'] : "", 
						"without_country_code_whatsapp_number"=>(!empty($user_data['whatsapp_number'])) ? $user_data['whatsapp_number'] : "",
						"emergency_number_country_code"=>(!empty($user_data['country_code_emergency'])) ? $user_data['country_code_emergency'] : "", 
						"without_country_code_emergency_number"=>(!empty($user_data['emergency_number'])) ? $user_data['emergency_number'] : ""
					);
					
					array_push($response["contact"], $contact);


		            $response["experience"] = array();
		           
		            $qep= $d->select("employee_experience", "user_id='$user_id'", "");
					while ($empExp = mysqli_fetch_array($qep)) {
						$experience = array();
                    	$experience["employee_experience_id"] = $empExp['employee_experience_id'];
                    	$experience["designation"] = html_entity_decode($empExp['designation']);
                    	$experience["work_from"] =  date('M Y', strtotime($empExp['work_from'])) .' - '. date('M Y', strtotime($empExp['work_to']));
                    	$experience["company_location"] = html_entity_decode($empExp['company_location']);
                    	$experience["exp_company_name"] = html_entity_decode($empExp['exp_company_name']);
                    	
	                    array_push($response["experience"], $experience);
                    	

					}

		            $response["education"] = array();

		            $qar= $d->select("employee_achievements_education", "user_id='$user_id'", "");
					while ($empArr = mysqli_fetch_array($qar)) {
						$education = array();
                    	$education["employee_achievement_id"] = $empArr['employee_achievement_id'];
                    	$education["achievement_name"] =html_entity_decode($empArr['achievement_name']);
                    	$education["achievement_date"] =  date('d M Y', strtotime($empArr['achievement_date']));
                    	$education["university_board_name"] = html_entity_decode($empArr['university_board_name']);
                    	$education["archi_type"] = $empArr['archi_type'];
	                    array_push($response["education"], $education);
                    	

					}

		            $response["social_link"] = array();
					$social_link = array(
						"facebook"=>$user_data['facebook'], 
						"instagram"=>$user_data['instagram'], 
						"linkedin"=>$user_data['linkedin'], 
						"twitter"=>$user_data['twitter'], 
						"whatsapp_number"=>$whatsapp_number,
						"whatsapp_number_country_code"=>(!empty($user_data['country_code_whatsapp'])) ? $user_data['country_code_whatsapp'] : "",
						"whatsapp_number_without_country_code"=>(!empty($user_data['whatsapp_number'])) ? $user_data['whatsapp_number'] : ""
					);
					array_push($response["social_link"], $social_link);


					$employment_edu = $d->selectRow("employee_experience_id as emp_edu","employee_experience", "user_id='$user_id'","");
					$employment_edu_arr = mysqli_fetch_array($employment_edu);


					$employment_achi = $d->selectRow("archi_type as emp_achi","employee_achievements_education", "user_id='$user_id'","");
					$employment_achi_arr = mysqli_fetch_array($employment_achi);
					
					$profile_progress = 0;
					if ($user_data['user_full_name']!="") {
						$profile_progress +="10";
					}
					if ($user_data['user_profile_pic']!="" && $user_data['user_profile_pic']!="user_default.png") {
						$profile_progress +="10";
					}
					if ($user_data['user_mobile']!="") {
						$profile_progress +="10";
					}
					if ($user_data['gender']!="") {
						$profile_progress +="5";
					}
					if ($user_data['last_address']!="") {
						$profile_progress +="5";
					}
					if ($user_data['user_email']!="") {
						$profile_progress +="5";
					}
					if ($user_data['whatsapp_number']>0) {
						$profile_progress +="5";
					}
					if ($user_data['emergency_number']>0) {
						$profile_progress +="5";
					}
					if ($user_data['permanent_address']!="") {
						$profile_progress +="3";
					}
					if ($user_data['member_date_of_birth']!="") {
						$profile_progress +="3";
					}
					if ($user_data['nationality']!="") {
						$profile_progress +="3";
					}
					if ($user_data['alt_mobile']>0) {
						$profile_progress +="3";
					}
					if ($user_data['blood_group']!="") {
						$profile_progress +="3";
					}
					if ($user_data['user_designation']!="") {
						$profile_progress +="3";
					}
					if ($user_data['marital_status']>0) {
						$profile_progress +="3";
					}
					if ($user_data['total_family_members']>0) {
						$profile_progress +="2";
					}
					if ($user_data['facebook']!="") {
						$profile_progress +="2";
					}
					if ($user_data['instagram']!="") {
						$profile_progress +="2";
					}
					if ($user_data['linkedin']!="") {
						$profile_progress +="2";
					}
					if ($user_data['twitter']!="") {
						$profile_progress +="2";
					}
					if ($empData['job_location']!="") {
						$profile_progress +="2";
					}
					if ($empData['intrest_hobbies']!="") {
						$profile_progress +="2";
					}
					if ($empData['professional_skills']!="") {
						$profile_progress +="2";
					}
					if ($empData['special_skills']!="") {
						$profile_progress +="2";
					}
					if ($empData['language_known']!="") {
						$profile_progress +="2";
					}
					if ($employment_edu_arr['emp_edu']!="" && !empty($employment_edu_arr['emp_edu'])) {
						$profile_progress +="2";
					}
					if (mysqli_num_rows($employment_achi)>0) {
						$profile_progress +="2";
					}
					
					if ($profile_progress>100) {
						$profile_progress = "100";
					}

					$response["profile_progress"] = $profile_progress."";


					$notiAry = array(
                      'user_id'=>$user_id,
                      'society_id'=>$society_id,
                      'my_id'=>$my_id,
                      'view_time'=>date("Y-m-d H:i:s"),
                    );

					$qre  = $d->selectRow("user_id","users_recent_view" , "user_id='$user_id' AND my_id='$my_id' ");
					if ($my_id!=0 && $my_id!=$user_id && mysqli_num_rows($qre)==0) {
	                 	$d->insert("users_recent_view",$notiAry);
					} else if($my_id!=0 && $my_id!=$user_id) {
	                 	$d->update("users_recent_view",$notiAry,"user_id='$user_id' AND my_id='$my_id' ");
					}					

					$response["message"] = "Get successfully...";
					$response["status"] = "200";
					echo json_encode($response);
				} else {
					$response["message"] = "Account Not activated.";
					$response["status"] = "201";
					echo json_encode($response);
				}
			} else {

				$response["message"] = "No User Found...!";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if (isset($getAddress) && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
			$chat_access_denied  = false;
			if ($getAddress == 'getAddress') {
				if ($unit_id != '' && $unit_id != 0) {
					$quc = $d->select("unit_master", "unit_id='$unit_id'");
					$getMmeberUserData= mysqli_fetch_array($quc);
					if ($getMmeberUserData['member_access_denied']== 1) {
						$response["message"] = "Access Denied..!";
						$response["status"] = "201";
						echo json_encode($response);
						exit();
					}

					if ($getMmeberUserData['chat_access_denied']== 1) {
						$chat_access_denied = true;
					}

				}
			}

			$q = $d->select("users_master,block_master,floors_master,unit_master,user_employment_details",
				"users_master.delete_status=0 AND users_master.user_id ='$user_id' AND users_master.block_id=block_master.block_id   AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND users_master.user_id=user_employment_details.user_id");

			$user_data = mysqli_fetch_array($q);

			if ($user_data == TRUE) {

				if ($user_data['user_status'] == 0) {
					$response["message"] = "User yet not approved by admin !";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				}


				if ($user_data['unit_status'] != '4') {

					if ($user_data['public_mobile']==1 && $user_data['last_address'] != '' && $my_id!=$user_id) {
						$current_address = "Private";
					}else{
						$current_address = $user_data['last_address']."";
					}

					if ($user_data['public_mobile']==1 && $user_data['permanent_address'] != '' && $my_id!=$user_id) {
						$permanent_address = "Private";
					}else{
						$permanent_address = $user_data['permanent_address']."";
					}
					
					$response["current_address"]=html_entity_decode($current_address);
					$response["permanent_address"]=html_entity_decode($permanent_address);
							      
					$response["message"] = "Get successfully...";
					$response["status"] = "200";
					echo json_encode($response);
				} else {
					$response["message"] = "Account Not activated.";
					$response["status"] = "201";
					echo json_encode($response);
				}
			} else {

				$response["message"] = "No User Found...!";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}  else if (isset($setProfilePicture) && $setProfilePicture == 'setProfilePicture' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			if ($user_profile_pic != "") {
				$ddd = date("ymdhi");
				$profile_name = "user_" . rand().$user_id.$ddd. '.png';
				define('UPLOAD_DIR', '../img/users/recident_profile/');
				$img = $user_profile_pic;
				$img = str_replace('data: img/app/png;base64,', '', $img);
				$img = str_replace(' ', '+', $img);
				$data = base64_decode($img);
				$file = UPLOAD_DIR . $profile_name;
				$success = file_put_contents($file, $data);
			} else {
				$profile_name = "user_default.png";
			}

			$m->set_data('user_profile_pic', $profile_name);

			$a = array('user_profile_pic' => $m->get_data('user_profile_pic'));

			$q = $d->update("users_master", $a, "society_id='$society_id' AND user_id='$user_id'");

			if ($q == true) {

				$d->insert_myactivity($user_id, "$society_id", "0", "$user_name", "New profile photo set", "My-profilexxxhdpi.png");

				$response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $profile_name;
				$response["message"] = "Profile Photo Uploaded";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "wrong profile details.";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else if (isset($setProfilePictureNew) && $setProfilePictureNew == 'setProfilePictureNew' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$extension = array("jpeg", "jpg", "png", "gif", "JPG", "JPEG", "PNG");
			$uploadedFile = $_FILES["user_profile_pic"]["tmp_name"];
			$ext = pathinfo($_FILES['user_profile_pic']['name'], PATHINFO_EXTENSION);
			if (file_exists($uploadedFile)) {

				$sourceProperties = getimagesize($uploadedFile);
				$newFileName = rand() . $user_id;
				$dirPath = "../img/users/recident_profile/";
				$imageType = $sourceProperties[2];
				$imageHeight = $sourceProperties[1];
				$imageWidth = $sourceProperties[0];
				if ($imageWidth > 500) {
					$newWidthPercentage = 500 * 100 / $imageWidth; //for maximum 500 widht
					$newImageWidth = $imageWidth * $newWidthPercentage / 100;
					$newImageHeight = $imageHeight * $newWidthPercentage / 100;
				} else {
					$newImageWidth = $imageWidth;
					$newImageHeight = $imageHeight;
				}
				switch ($imageType) {

				case IMAGETYPE_PNG:
					$imageSrc = imagecreatefrompng($uploadedFile);
					$tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
					imagepng($tmp, $dirPath . $newFileName . "_profile." . $ext);
					break;

				case IMAGETYPE_JPEG:
					$imageSrc = imagecreatefromjpeg($uploadedFile);
					$tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
					imagejpeg($tmp, $dirPath . $newFileName . "_profile." . $ext);
					break;

				case IMAGETYPE_GIF:
					$imageSrc = imagecreatefromgif($uploadedFile);
					$tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
					imagegif($tmp, $dirPath . $newFileName . "_profile." . $ext);
					break;

				default:

					break;
				}
				$profile_name = $newFileName . "_profile." . $ext;

			} else {

				$response["message"] = "Invalid Photo Format.";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}

			$m->set_data('user_profile_pic', $profile_name);

			$a = array('user_profile_pic' => $m->get_data('user_profile_pic'));

			$q = $d->update("users_master", $a, "society_id='$society_id' AND user_id='$user_id'");

			if ($q == true) {
				$d->insert_myactivity($user_id, "$society_id", "0", "$user_name", "New profile photo set", "My-profilexxxhdpi.png");

				$response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $profile_name;
				$response["message"] = "Profile Photo Uploaded";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "wrong profile details.";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else if (isset($setEmail) && $setEmail == 'setEmail' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$m->set_data('user_email', $user_email);

			$a = array('user_email' => $m->get_data('user_email'));

			$q = $d->update("users_master", $a, "society_id='$society_id' AND user_id='$user_id'");

			if ($q == true) {
				$d->insert_myactivity($user_id, "$society_id", "0", "$user_name", "Email updated", "My-profilexxxhdpi.png");

				$response["message"] = "Email Updated";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "Something Wrong.";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else if (isset($removeProfilePicture) && $removeProfilePicture == 'removeProfilePicture' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$profile_name = "user_default.png";

			$m->set_data('user_profile_pic', $profile_name);

			$a = array('user_profile_pic' => $m->get_data('user_profile_pic'));

			$q = $d->update("users_master", $a, "society_id='$society_id' AND user_id='$user_id'");

			if ($q == true) {
				$d->insert_myactivity($user_id, "$society_id", "0", "$user_name", "Profile photo removed", "My-profilexxxhdpi.png");

				$response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $profile_name;
				$response["message"] = "Profile Photo Removed";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "wrong profile details.";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}  else if (isset($setProsnalDetails) && $setProsnalDetails == 'setProsnalDetails' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$qu=$d->selectRow("country_code,country_code_alt","users_master","user_mobile='$new_mobile' AND delete_status=0");
			$oldData = mysqli_fetch_array($qu);
			$country_code_old = $oldData['country_code'];
			if ($country_code=="") {
				$country_code = $oldData['country_code'];
			}
			if ($country_code_alt=="") {
				$country_code_alt = $oldData['country_code_alt'];
			}

			$m->set_data('user_full_name', $user_full_name);
			$m->set_data('user_first_name', $user_first_name);
			$m->set_data('user_last_name', $user_last_name);
			$m->set_data('user_email', $user_email);
			if ($member_date_of_birth == "Private") {
				$m->set_data('member_date_of_birth', "");
			} else if($member_date_of_birth!="") {
				$m->set_data('member_date_of_birth', date('Y-m-d', strtotime($member_date_of_birth)));
			} else {
				$m->set_data('member_date_of_birth',"");

			}
			$m->set_data('facebook', $facebook);
			$m->set_data('instagram', $instagram);
			$m->set_data('linkedin', $linkedin);
			$m->set_data('gender', $gender);
			$m->set_data('alt_mobile', $alt_mobile);
			$m->set_data('country_code', $country_code);
			$m->set_data('country_code_alt', $country_code_alt);
			$m->set_data('blood_group', $blood_group);

			$a = array(
				'user_full_name' => $m->get_data('user_full_name'),
				'user_first_name' => $m->get_data('user_first_name'),
				'user_last_name' => $m->get_data('user_last_name'),
				'user_email' => $m->get_data('user_email'),
				'gender' => $m->get_data('gender'),
				'member_date_of_birth' => $m->get_data('member_date_of_birth'),
				'facebook' => $m->get_data('facebook'),
				'instagram' => $m->get_data('instagram'),
				'linkedin' => $m->get_data('linkedin'),
				'alt_mobile' => $m->get_data('alt_mobile'),
				'country_code' => $m->get_data('country_code'),
				'country_code_alt' => $m->get_data('country_code_alt'),
				'blood_group' => $m->get_data('blood_group'),
			);

			if ($country_code_old!=$country_code || $new_mobile!=$old_mobile && $new_mobile!='') {
				// send otp to new mobile 
				$qf=$d->select("users_master","delete_status=0 AND parent_id='$user_id' AND member_status=1 AND unit_id='$unit_id' AND user_mobile='$new_mobile' AND country_code='$country_code'");
				if (mysqli_num_rows($qf)>0) {
					$mobile_number_is_already_register_in_this_unit = $xml->string->mobile_number_is_already_register_in_this_unit;

					$response["message"]="Mobile number is already register in this unit";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
				}

				if ($user_type==1) {
					// if tenant requiest
	                if ($new_mobile==$owner_mobile) {
						$tenant_and_user_mobile_should_be_unique = $xml->string->tenant_and_user_mobile_should_be_unique;
	                    $response["message"]="Tenant and user mobile should be unique";
	                    $response["status"]="201";
	                    echo json_encode($response);
	                    exit();
	                }
				}

				// $count5=$d->sum_data("credit_amount-debit_amount","user_wallet_master","user_mobile='$old_mobile' AND active_status=0");
    //             $row11=mysqli_fetch_array($count5);
    //             $totalDebitAmount=$row11['SUM(credit_amount-debit_amount)'];
				// $availableBalance= number_format($totalDebitAmount,2,'.','');

				$count5=$d->sum_data("user_wallet_master.debit_amount","user_wallet_master,users_master","users_master.unit_id=user_wallet_master.unit_id AND users_master.delete_status=0 AND user_wallet_master.user_mobile='$old_mobile' AND user_wallet_master.active_status=0 AND users_master.user_mobile=user_wallet_master.user_mobile AND users_master.country_code='$country_code_old'");
	            $row11=mysqli_fetch_array($count5);
	            $totalDebitAmount=$row11['SUM(user_wallet_master.debit_amount)'];

	            $cq=$d->sum_data("user_wallet_master.credit_amount","user_wallet_master,users_master","users_master.unit_id=user_wallet_master.unit_id AND users_master.delete_status=0 AND user_wallet_master.user_mobile='$old_mobile' AND user_wallet_master.active_status=0 AND users_master.user_mobile=user_wallet_master.user_mobile AND users_master.country_code='$country_code_old'");
	            $row22=mysqli_fetch_array($cq);
	            $totalCreditAmount=$row22['SUM(user_wallet_master.credit_amount)'];

	            $availableBalance= $totalCreditAmount-$totalDebitAmount;

				if ($otp_lenght=='6' & $otp_lenght!='') {
					$digits = 6;
				} else {
					$digits = 4;
				}
				$otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
				$m->set_data('otp', $otp);
				$aOtp = array(
					'otp' => $m->get_data('otp'),
					'opt_attempt' => 1,
					'otp_attempt_time' => date('Y-m-d H:i:s'),
				);
				$d->update("users_master", $aOtp, "user_id='$user_id'");
				
					if ($otp_type == 1) {
						$smsObj->send_voice_otp($new_mobile, $otp,$country_code);
						$d->add_sms_log($new_mobile,"User App Voice OTP SMS",$society_id,$country_code,1);
						$you_will_receive_your_otp_on_call = $xml->string->you_will_receive_your_otp_on_call;
						$response["message"] = "You will receive your otp on call";
					} else if ($otp_type == 2 && $user_email!='') {
						$to = $user_email;
					 	$subject= "OTP Verification for MyCo App.";
						
						$admin_name = $user_full_name;
						$msg= "$otp"; 
						include '../apAdmin/mail/adminPanelLoginOTP.php';
						include '../apAdmin/mail.php';
						$response["message"] = "You will receive your otp on your email $to";
					} else {
						$response["message"] = "OTP send successfully";
						$smsObj->send_otp($new_mobile, $otp,$country_code);
						$d->add_sms_log($new_mobile,"User App OTP SMS",$society_id,$country_code,1);
					}

					if ($user_email!='') {
						$response["is_email_otp"] = true;
					} else {
						$response["is_email_otp"] = false;
					}
					if ($country_code != "+91") {
						$response["is_voice_otp"] = false;
					} else {
						$response["is_voice_otp"] = true;
					}
					
					$response["otp_popup"] = true;
					$response["status"] = "200";
					$response["availableBalance"]=number_format($availableBalance,2,'.','');
					echo json_encode($response);
					exit();
			} else {
				$response["otp_popup"] = false;
				$response["availableBalance"]="0";
				$q = $d->update("users_master", $a, "society_id='$society_id' AND user_id='$user_id'");
			}

			if ($q == true) {
				$d->insert_myactivity($user_id, "$society_id", "0", "$user_name", "Profile updated", "My-profilexxxhdpi.png");
 

				$response["message"] = "Updated Successfully";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "Something went wrong";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}else if (isset($setFamilymember) && $setFamilymember == 'setFamilymember' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($parent_id, FILTER_VALIDATE_INT) == true && filter_var($block_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {

			// $uqw=$d->selectRow("member_status,parent_id","users_master","user_id='$parent_id'");
			// $userType = mysqli_fetch_array($uqw);
			// $member_status = $userType['member_status'];

			if ($member_status == 1) {
				$response["message"] = "Access Denied !";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}

			$qSociety = $d->select("society_master", "society_id ='$society_id'");
			$societyData = mysqli_fetch_array($qSociety);
			$family_member_approval = $societyData['family_member_approval'];
			$society_name = $societyData['society_name'];

			$qselect = $d->select("users_master", "unit_id='$unit_id' AND user_mobile='$user_mobile' AND user_mobile != '' AND user_mobile != '0' AND user_status=1 AND user_id!='$user_id'");
			$user_data = mysqli_fetch_array($qselect);
			if ($user_data == true) {
				$response["message"] = "Mobile number is already register in this unit.";
				$response["status"] = "201";
				echo json_encode($response);
				exit();
			}

			$qc = $d->selectRow("user_type,tenant_view", "users_master", "user_id='$parent_id'");
			$userTypeData = mysqli_fetch_array($qc);
			$user_type = $userTypeData['user_type'];
			$tenant_view = $userTypeData['tenant_view'];

			$q = false;
			$qselect = $d->select("users_master", "unit_id='$unit_id' AND user_mobile='$user_mobile' AND user_mobile!=0 ");
			$user_data = mysqli_fetch_array($qselect);
			$oldUserStatus = $user_data['user_status'];

			$m->set_data('parent_id', $parent_id);
			$m->set_data('society_id', $society_id);
			$m->set_data('block_id', $block_id);
			$m->set_data('floor_id', $floor_id);
			$m->set_data('unit_id', $unit_id);

			$m->set_data('member_status', '1');

			$m->set_data('user_password', $password);
			$m->set_data('user_first_name', $user_first_name);
			$m->set_data('user_last_name', $user_last_name);
			$m->set_data('country_code', $country_code);
			$m->set_data('user_full_name', $user_first_name . " " . $user_last_name);
			if ($user_status == 2) {
				$m->set_data('user_status', $user_status);
				$m->set_data('user_mobile', $user_mobile);
			} else {
				if ($family_member_approval == "1") {
					if ($user_id != 0 && $oldUserStatus == 1) {
						$m->set_data('user_status', 1);
					} else {
						$m->set_data('user_status', 0);

					}
					$adminNoti = true;
				} else {
					$adminNoti = false;
					$m->set_data('user_status', $user_status);
				}
				$m->set_data('user_mobile', $user_mobile);
			}

			
			// user status    =>  0 pending approval 1 approved 2 for login deny.
			// member_status  => 0 for Primary 1 for Sub-Member.
			$m->set_data('user_type', $user_type);

			$m->set_data('member_relation_name', $member_relation);
			$m->set_data('gender', $gender);
			$m->set_data('member_date_of_birth', $member_date_of_birth);
			$m->set_data('tenant_view', $tenant_view);
			$m->set_data('register_date', date('Y-m-d H:i:s'));

			$a1 = array(
				'parent_id' => $m->get_data('parent_id'),
				'society_id' => $m->get_data('society_id'),
				'block_id' => $m->get_data('block_id'),
				'floor_id' => $m->get_data('floor_id'),
				'unit_id' => $m->get_data('unit_id'),
				'user_first_name' => $m->get_data('user_first_name'),
				'user_last_name' => $m->get_data('user_last_name'),
				'user_full_name' => $m->get_data('user_full_name'),
				'gender' => $m->get_data('gender'),
				'user_mobile' => $m->get_data('user_mobile'),
				'user_type' => $m->get_data('user_type'),
				'user_status' => $m->get_data('user_status'),
				'member_status' => $m->get_data('member_status'),
				'member_relation_name' => $m->get_data('member_relation_name'),
				'member_date_of_birth' => $m->get_data('member_date_of_birth'),
				'register_date' => $m->get_data('register_date'),
				'tenant_view' => $m->get_data('tenant_view'),
				'country_code' => $m->get_data('country_code'),
			);

			if ($user_data == true) {
				if ($user_status == 2) {
					$user_fcm = $user_data['user_token'];
					$device = $user_data['device'];
					if ($device == 'android') {
						$nResident->noti("", "", $society_id, $user_fcm, "logout", "Admin Deleted", 'logout');
					} else if ($device == 'ios') {
						$nResident->noti_ios("", "", $society_id, $user_fcm, "logout", "Admin Deleted", 'logout');
					}
					$q = $d->update("users_master", $a1, "user_id='$user_id'");
					$a14 = array(
						'user_token' => "",
					);
					$d->update("users_master", $a14, "user_id='$user_id'");
					$d->insert_myactivity($parent_id, "$society_id", "0", "$user_name", "Family member data updated", "My-profilexxxhdpi.png");

					$response["message"] = "Family Member Details Updated";
				} else {
					$qm = $d->select("users_master", "unit_id='$unit_id' AND user_mobile='$user_mobile' AND user_mobile!=0 AND user_id!='$user_id'");
					$oData = mysqli_fetch_array($qm);
					if ($oData > 0) {
						$response["message"] = "Mobile number is already register in this unit";
						$response["status"] = "201";
						echo json_encode($response);
						exit();
					}
					if ($user_id != 0) {
						$q = $d->update("users_master", $a1, "user_id='$user_id'");
						$d->insert_myactivity($parent_id, "$society_id", "0", "$user_name", "Family member data updated", "My-profilexxxhdpi.png");
						$response["message"] = "Family Member Details Updated";
					} else {
						// $response["message"] = "Mobile and Email already register.";
						// $response["status"] = "201";
						// echo json_encode($response);
						// exit();
						$q = $d->update("users_master", $a1, "user_id='$user_id'");

						$d->insert_myactivity($parent_id, "$society_id", "0", "$user_name", "Family member data updated", "My-profilexxxhdpi.png");
						$response["message"] = "Family Member Details Updated !";

						if ($adminNoti == true) {
							$qunitName = $d->select("unit_master,block_master", "unit_master.unit_id='$unit_id' AND block_master.block_id='$block_id'");
							$data_unit = mysqli_fetch_array($qunitName);
							$blockName = $data_unit['block_name'];
							$unitName = $data_unit['unit_name'];

			                $block_id=$d->getBlockid($user_id);
			               $fcmArray=$d->selectAdminBlockwise("7",$block_id,"android");
			               $fcmArrayIos=$d->selectAdminBlockwise("7",$block_id,"ios");

							$nAdmin->noti_new($society_id,$notiImg, $fcmArray, "New Family Member Request by", "$user_first_name  for $blockName-$unitName", 'pendingUser?pendingUser=yes');
							$nAdmin->noti_ios_new($society_id,$notiImg, $fcmArrayIos, "New Family Member Request by", "$user_first_name  for $blockName-$unitName", 'pendingUser?pendingUser=yes');

							$notiAry = array(
								'society_id' => $society_id,
								'notification_tittle' => "New Family Member Request by",
								'notification_description' => $user_first_name . " for " . $data_unit['block_name'] . "-" . $data_unit['unit_name'],
								'notifiaction_date' => date('Y-m-d H:i'),
								'notification_action' => "pendingUser?pendingUser=yes",
								'admin_click_action ' => "pendingUser?pendingUser=yes",
								'notification_logo'=>'Members_1xxxhdpi.png',
							);

							$d->insert("admin_notification", $notiAry);

						}

					}
				}
			} else {

				if ($user_id == 0) {

					$smsObj->send_welcome_message($society_id,$user_mobile,$user_first_name,$society_name,$country_code);
			        $d->add_sms_log($user_mobile,"User Welcome Message",$society_id,$country_code,4);

					$q = $d->insert("users_master", $a1);
					$d->insert_myactivity($parent_id, "$society_id", "0", "$user_name", "Family member added", "My-profilexxxhdpi.png");
					$response["message"] = "Family Member Added";
					if ($adminNoti == true) {
						$qunitName = $d->select("unit_master,block_master", "unit_master.unit_id='$unit_id' AND block_master.block_id='$block_id'");
						$data_unit = mysqli_fetch_array($qunitName);
						$blockName = $data_unit['block_name'];
						$unitName = $data_unit['unit_name'];

		                $block_id=$d->getBlockid($user_id);
		               $fcmArray=$d->selectAdminBlockwise("7",$block_id,"android");
		               $fcmArrayIos=$d->selectAdminBlockwise("7",$block_id,"ios");

						$nAdmin->noti_new($society_id,$notiImg, $fcmArray, "New Family Member Request by", "$user_first_name  for $blockName-$unitName", 'pendingUser?pendingUser=yes');
						$nAdmin->noti_ios_new($society_id,$notiImg, $fcmArrayIos, "New Family Member Request by", "$user_first_name  for $blockName-$unitName", 'pendingUser?pendingUser=yes');

						$notiAry = array(
							'society_id' => $society_id,
							'notification_tittle' => "New Family Member Request by",
							'notification_description' => $user_first_name . " for " . $data_unit['block_name'] . "-" . $data_unit['unit_name'],
							'notifiaction_date' => date('Y-m-d H:i'),
							'notification_action' => "pendingUser?id=$user_id",
							'admin_click_action ' => "pendingUser?id=$user_id",
							'notification_logo'=>'Members_1xxxhdpi.png',
						);

						$d->insert("admin_notification", $notiAry);

					}
				} else {
					$d->insert_myactivity($parent_id, "$society_id", "0", "$user_name", "Family member data updated", "My-profilexxxhdpi.png");
					$response["message"] = "Family Member Details Updated...";
					$q = $d->update("users_master", $a1, "user_id='$user_id'");
				}
			}

			if ($q == true) {

				$qfamily = $d->select("users_master", "society_id ='$society_id'and block_id='$block_id' AND floor_id='$floor_id' AND unit_id='$unit_id' AND member_status='1' AND user_type='$user_type'");

				$response["member"] = array();

				while ($datafamily = mysqli_fetch_array($qfamily)) {

					$member = array();

					if ($datafamily['member_date_of_birth'] == '0000-00-00') {
						$dob = "";
					}else{
						$dob = $datafamily['member_date_of_birth'];
					}

					$member["user_id"] = $datafamily['user_id'];
					$member["user_first_name"] = $datafamily['user_first_name'];
					$member["user_last_name"] = $datafamily['user_last_name'];
					$member["user_mobile"] = $datafamily['user_mobile'];
					$member["member_date_of_birth"] = $dob.'';
					$dateOfBirth = "" . $datafamily['member_date_of_birth'];
					$today = date("Y-m-d");
					$diff = date_diff(date_create($dateOfBirth), date_create($today));
					$member["member_age"] = "" . $diff->format('%y');
					$member["member_relation_name"] = $datafamily['member_relation_name'];
					$member["user_status"] = $datafamily['user_status'];
					$member["member_status"] = $datafamily['member_status'];
					array_push($response["member"], $member);
				}

				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "Mobile Number is Already Register";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else if (isset($setEmergencyContact) && $setEmergencyContact == 'setEmergencyContact' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
			$q = false;

			$cq = $d->selectRow("person_mobile", "user_emergencycontact_master", "user_id='$user_id' AND society_id='$society_id' AND person_mobile='$person_mobile'");
			if (mysqli_num_rows($cq) > 0) {
				$response["message"] = "This number already added in emergency list.";
				$response["status"] = "201";
				echo json_encode($response);
				exit();

			}

			$m->set_data('emergencyContact_id', $emergencyContact_id);
			$m->set_data('person_name', $person_name);
			$m->set_data('person_mobile', $person_mobile);
			$m->set_data('society_id', $society_id);
			$m->set_data('user_id', $user_id);
			$m->set_data('relation', $relation);

			$a1 = array(
				'user_id' => $m->get_data('user_id'),
				'society_id' => $m->get_data('society_id'),
				'person_name' => $m->get_data('person_name'),
				'person_mobile' => $m->get_data('person_mobile'),
				'relation' => $m->get_data('relation'),
			);

			if ($emergencyContact_id != "0") {
				$d->insert_myactivity($user_id, "$society_id", "0", "$user_name", "Emergency number updated", "Emergencyxxxhdpi.png");

				$q = $d->update("user_emergencycontact_master", $a1, "emergencyContact_id='$emergencyContact_id1[$i]'");
			} else {
				$d->insert_myactivity($user_id, "$society_id", "0", "$user_name", "Emergency number added", "Emergencyxxxhdpi.png");

				$q = $d->insert("user_emergencycontact_master", $a1);
			}

			if ($q == true) {

				$qemergency = $d->select("user_emergencycontact_master", "society_id ='$society_id' and user_id='$user_id'");

				$response["emergency"] = array();

				while ($dataemergency = mysqli_fetch_array($qemergency)) {

					$emergency = array();

					$emergency["emergencyContact_id"] = $dataemergency['emergencyContact_id'];
					$emergency["person_name"] = $dataemergency['person_name'];
					$emergency["person_mobile"] = $dataemergency['person_mobile'];
					$emergency["relation_id"] = $dataemergency['relation_id'];
					$emergency["relation"] = $dataemergency['relation'];

					array_push($response["emergency"], $emergency);
				}

				$response["message"] = "Emergency Number Added Successfully.";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "wrong emergency details.";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else if (isset($getFamilymember) && $getFamilymember == 'getFamilymember' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($block_id, FILTER_VALIDATE_INT) == true) {

			$uqw = $d->selectRow("member_status,parent_id,user_type", "users_master", "user_id='$user_id'");
			$userTypeData = mysqli_fetch_array($uqw);
			$user_type = $userTypeData['user_type'];

			if ($member_status == 1) {
				$parent_id = $userTypeData['parent_id'];
				$qfamily = $d->select("users_master", "delete_status=0 AND society_id ='$society_id' and block_id='$block_id' AND floor_id='$floor_id' AND unit_id='$unit_id'  AND parent_id='$parent_id' AND user_id!='$user_id' AND user_type='$user_type' OR society_id ='$society_id' and block_id='$block_id' AND floor_id='$floor_id' AND unit_id='$unit_id'  AND user_id='$parent_id' AND user_id!='$user_id' AND member_status='0' AND user_type='$user_type'");
			} else {
				$qfamily = $d->select("users_master", "delete_status=0 AND society_id ='$society_id'and block_id='$block_id' AND floor_id='$floor_id' AND unit_id='$unit_id' AND member_status='1' AND parent_id='$user_id'");

			}

			if ($qfamily == true) {
				$response["member"] = array();

				while ($datafamily = mysqli_fetch_array($qfamily)) {
					$qAMember = $d->select("user_employment_details", "society_id='$society_id' AND unit_id='$unit_id' AND user_id='$datafamily[user_id]'", "");
					$desData= mysqli_fetch_array($qAMember);

					$member = array();
					$member["user_id"] = $datafamily['user_id'];
					$member["user_first_name"] = $datafamily['user_first_name'];
					$member["user_last_name"] = $datafamily['user_last_name'];
					$member["country_code"] = $datafamily['country_code'].'';
					if ($datafamily['user_mobile'] != 0) {
						$member["user_mobile"] = $datafamily['user_mobile'];
					} else {
						$member["user_mobile"] = "";
					}

					$member["gender"] = $datafamily['gender'];
					$member["member_date_of_birth"] = $datafamily['member_date_of_birth'].'';

					$dateOfBirth = "" . $datafamily['member_date_of_birth'];
					// $today = date("Y-m-d");
					// $diff = date_diff(date_create($dateOfBirth), date_create($today));

					if ($datafamily['user_status'] == 0) {
						$qSociety = $d->selectRow("family_member_approval","society_master", "society_id ='$society_id'");
						$societyData = mysqli_fetch_array($qSociety);
						$family_member_approval = $societyData['family_member_approval'];
						if ($family_member_approval == "1") {
							$member["you_can_appove"] = "false";
						} else {
							$member["you_can_appove"] = "true";
						}
						$member["user_status_msg"] = "Request Pending";
					} else {
						$member["user_status_msg"] = "";
						$member["you_can_appove"] = "";
					}

					$member["member_age"] = "";
					// if ($datafamily['member_status'] == 0) {
					// 	$rel = "Primary";
					// } else {
					// 	$rel = $datafamily['member_relation_name'];
					// }
					// if ($member_status == 1 && $datafamily['member_status'] != 0) {
					// 	$rel = 'Primary-'.$rel;
					// }

					$member["member_relation_name"]=html_entity_decode($desData['designation'].'');
					$member["user_status"] = $datafamily['user_status'];
					$member["member_status"] = $datafamily['member_status'];
					if ($datafamily['user_profile_pic']!='') {
					$member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
					} else {
					$member["user_profile_pic"] = '';

					}

					$qr_size = "300x300";
					

					$qr_content       = $datafamily['user_mobile'];
					$qr_correction    = strtoupper('H');
					$qr_encoding      = 'UTF-8';

    				//form google chart api link
					$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
					$member["icard_qr_code"] = $qrImageUrl . '.png';
					


					$member["company_name"] = '';
					$member["designation"] = html_entity_decode($desData['designation'].'');
					$member["company_address"] = '';
					// $response["company_contact_number"] = $data['company_contact_number'];
					if ($data['company_contact_number']!="0") {
					$member["company_contact_number"] = '';
					} else {
					$member["company_contact_number"] = '';
					}
					$member["company_website"] ='';
					if ($data['company_logo']!="") {
	                    $member["company_logo"] = "";
	                } else {
	                    $member["company_logo"] ="";
	                }

					array_push($response["member"], $member);
				}

				$response["message"] = "Get Family Members Successfully";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "No Family details found.";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else if (isset($getEmergencyContact) && $getEmergencyContact == 'getEmergencyContact' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$qemergency = $d->select("user_emergencycontact_master", "society_id ='$society_id' and user_id='$user_id'");

			if ($qemergency == true) {

				$response["emergency"] = array();

				while ($dataemergency = mysqli_fetch_array($qemergency)) {

					$emergency = array();

					$emergency["emergencyContact_id"] = $dataemergency['emergencyContact_id'];
					$emergency["person_name"] = $dataemergency['person_name'];
					$emergency["person_mobile"] = $dataemergency['person_mobile'];
					$emergency["relation_id"] = $dataemergency['relation_id'];
					$emergency["relation"] = $dataemergency['relation'];

					array_push($response["emergency"], $emergency);
				}

				$response["message"] = "$updateMsg";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "something went wrong.";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}  else if (isset($getMultiUnitsNew) && $user_mobile != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true) {
			$logoutIosDevice = false;

			$hasUpcomingEvengts=$d->select("event_master","society_id = '$society_id' AND event_status = '1'","order by event_start_date ASC LIMIT 100");

			$countUpcoming = 0;
			$response['upcoming_events'] = false;

			if(mysqli_num_rows($hasUpcomingEvengts)>0){

                while($event_data=mysqli_fetch_array($hasUpcomingEvengts)) {

                    $event_data = array_map("html_entity_decode", $event_data);
                     
                    $event = array(); 
                    $today=date('Y-m-d');
                    $cTime= date("H:i:s");
                    $expire = strtotime($event_data['event_end_date']);
                    $today = strtotime("$today midnight");

                    if($today > $expire){
                        $eventEnd=0;
                    } else {
                        $countUpcoming = $countUpcoming + 1;
                    }
                }

                if ($countUpcoming > 0) {
					$response['upcoming_events'] = true;
				}else{
					$response['upcoming_events'] = false;
				}
            }

            $startDate = date('Y-m-d');
			$endDate = date('Y-m-d',strtotime('+1 days',strtotime($startDate)));
			$startDate1 = date('m-d',strtotime($startDate));
			$endDate1 = date('m-d',strtotime($endDate));

			$birthdayQry = $d->selectRow("
				users_master.member_date_of_birth,
				users_master.user_id,
				users_master.user_full_name,
				users_master.user_designation,
				users_master.user_profile_pic,
				block_master.block_name,
				floors_master.floor_name,
				wishes_master.wish_reply,
				wishes_master.wish_id
				","
				block_master,
				floors_master,
				users_master LEFT JOIN wishes_master ON wishes_master.wish_by_id = '$user_id' AND wishes_master.wish_to_id = users_master.user_id AND DATE_FORMAT(wishes_master.wish_date,'%m-%d') = DATE_FORMAT(users_master.member_date_of_birth,'%m-%d')
				","
				users_master.user_status = '1' 
				AND users_master.active_status = '0' 
				AND users_master.delete_status = '0'  
				AND users_master.floor_id = floors_master.floor_id
				AND users_master.block_id = block_master.block_id
				AND DATE_FORMAT(users_master.member_date_of_birth,'%m-%d') >= '$startDate1' 
				AND DATE_FORMAT(users_master.member_date_of_birth,'%m-%d') <= '$endDate1' ","");

			$anniversaryQry = $d->selectRow("
				users_master.wedding_anniversary_date,
				users_master.user_id,
				users_master.user_full_name,
				users_master.user_designation,
				users_master.user_profile_pic,
				block_master.block_name,
				floors_master.floor_name,
				wishes_master.wish_reply,
				wishes_master.wish_id
				","
				block_master,
				floors_master,
				users_master LEFT JOIN wishes_master ON wishes_master.wish_by_id = '$user_id' AND wishes_master.wish_to_id = users_master.user_id AND DATE_FORMAT(wishes_master.wish_date,'%m-%d') = DATE_FORMAT(users_master.wedding_anniversary_date,'%m-%d')
				","
				users_master.user_status = '1' 
				AND users_master.active_status = '0' 
				AND users_master.delete_status = '0'  
				AND users_master.floor_id = floors_master.floor_id
				AND users_master.block_id = block_master.block_id
				AND DATE_FORMAT(users_master.wedding_anniversary_date,'%m-%d') >= '$startDate1' 
				AND DATE_FORMAT(users_master.wedding_anniversary_date,'%m-%d') <= '$endDate1'   ","");

			$workAnniversaryQry = $d->selectRow("
				user_employment_details.joining_date,
				users_master.user_id,
				users_master.user_full_name,
				users_master.user_designation,
				users_master.user_profile_pic,
				block_master.block_name,
				floors_master.floor_name,
				wishes_master.wish_reply,
				wishes_master.wish_id
				","
				users_master,
				block_master,
				floors_master,
				user_employment_details LEFT JOIN wishes_master ON wishes_master.wish_by_id = '$user_id' AND wishes_master.wish_to_id = user_employment_details.user_id AND DATE_FORMAT(wishes_master.wish_date,'%m-%d') = DATE_FORMAT(user_employment_details.joining_date,'%m-%d')
				","
				users_master.user_status = '1' 
				AND users_master.active_status = '0' 
				AND users_master.delete_status = '0' 				 
				AND users_master.floor_id = floors_master.floor_id
				AND users_master.block_id = block_master.block_id
				AND users_master.user_id = user_employment_details.user_id  
				AND DATE_FORMAT(user_employment_details.joining_date,'%m-%d') >= '$startDate1' 
				AND DATE_FORMAT(user_employment_details.joining_date,'%m-%d') <= '$endDate1' ","");


			$response["today_birth_days"] = array();
			$array1 = array();
			$array2 = array();
			$array3 = array();
			$sort = array();

			if (mysqli_num_rows($birthdayQry) > 0) {
				while($birthday_data = mysqli_fetch_array($birthdayQry)){
					array_push($array1,$birthday_data);
				}
			}

			if (mysqli_num_rows($anniversaryQry) > 0) {
				while($birthday_data = mysqli_fetch_array($anniversaryQry)){
					array_push($array2,$birthday_data);
				}
			}

			if (mysqli_num_rows($workAnniversaryQry) > 0) {
				while($birthday_data = mysqli_fetch_array($workAnniversaryQry)){
					array_push($array3,$birthday_data);
				}
			}

			$newArray = array_merge($array1,$array2,$array3);
            foreach ($newArray as $key => $part) {
            
            	if ($part['member_date_of_birth']!=null && $part['member_date_of_birth'] != '0000-00-00' && $part['member_date_of_birth'] != '') {
            		$sort[$key] = date('m-d',strtotime($part['member_date_of_birth']));
            	}else if ($part['wedding_anniversary_date']!=null && $part['wedding_anniversary_date'] != '0000-00-00' && $part['wedding_anniversary_date'] != '') {
            		$sort[$key] = date('m-d',strtotime($part['wedding_anniversary_date']));
            	}else if ($part['joining_date']!=null && $part['joining_date'] != '0000-00-00' &&$part['joining_date'] != '') {
            		$sort[$key] = date('m-d',strtotime($part['joining_date']));

            	}
            }

            if (isset($newArray)) {
	            array_multisort($sort, SORT_ASC, $newArray);
            }

            $ends = array('th','st','nd','rd','th','th','th','th','th','th');

            for ($i=0; $i <count($newArray) ; $i++) { 
                $birthDay = array(); 

                $birthDay['user_id'] = $newArray[$i]['user_id'];
				$birthDay['user_full_name'] = $newArray[$i]['user_full_name'];
				$birthDay['user_designation'] = $newArray[$i]['user_designation'];
				$birthDay['block_name'] = $newArray[$i]['block_name'];
				$birthDay['floor_name'] = $newArray[$i]['floor_name'];
				$birthDay['wish_id'] = $newArray[$i]['wish_id'].'';
				$birthDay['wish_reply'] = $newArray[$i]['wish_reply'].'';
				$birthDay['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $newArray[$i]['user_profile_pic'];

				if ($newArray[$i]['member_date_of_birth']!=null &&  $newArray[$i]['member_date_of_birth'] != '0000-00-00' &&  $newArray[$i]['member_date_of_birth'] != '') {
					$birthDayDate = $newArray[$i]['member_date_of_birth'];
					$birthDay['member_date_of_birth'] = date('d-m-Y',strtotime($birthDayDate));

					$birthDay['isBirthDay'] = true;
					$birthDay['isAnniversary'] = false;
					$birthDay['isWorkAnniversary'] = false;

					$dateOfbirth = $newArray[$i]['member_date_of_birth'];
					
					$diff = date_diff(date_create($dateOfbirth), date_create(date("Y-m-d")));

					$month = $diff->format('%m');

					if($month > 0){
						$number = $diff->format('%y') + 1;
					}else{
						$number = $diff->format('%y');
					}

					if (($number %100) >= 11 && ($number%100) <= 13){
					 	$abbreviation = $number. 'th';
					}else{
					 	$abbreviation = $number. $ends[$number % 10];
					}

					$birthDay['total_year_view'] = $abbreviation." Birthday";

				}else if ($newArray[$i]['wedding_anniversary_date']!=null &&  $newArray[$i]['wedding_anniversary_date'] != '0000-00-00' &&  $newArray[$i]['wedding_anniversary_date'] != '') {
					$birthDayDate = $newArray[$i]['wedding_anniversary_date'];
					$birthDay['wedding_anniversary_date'] = date('d-m-Y',strtotime($birthDayDate));
					
					$birthDay['isBirthDay'] = false;
					$birthDay['isAnniversary'] = true;
					$birthDay['isWorkAnniversary'] = false;

					$dateOfbirth = $newArray[$i]['wedding_anniversary_date'];
					
					$diff = date_diff(date_create($dateOfbirth), date_create(date("Y-m-d")));

					$month = $diff->format('%m');

					if($month > 0){
						$number = $diff->format('%y') + 1;
					}else{
						$number = $diff->format('%y');
					}

					if (($number %100) >= 11 && ($number%100) <= 13){
					 	$abbreviation = $number. 'th';
					}else{
					 	$abbreviation = $number. $ends[$number % 10];
					}

					$birthDay['total_year_view'] = $abbreviation." Anniversary";

				}else if ($newArray[$i]['joining_date']!=null &&  $newArray[$i]['joining_date'] != '0000-00-00' && $newArray[$i]['joining_date'] != '') {
					$birthDayDate = $newArray[$i]['joining_date'];
					$birthDay['joining_date'] = date('d-m-Y',strtotime($birthDayDate));
					
					$birthDay['isBirthDay'] = false;
					$birthDay['isAnniversary'] = false;
					$birthDay['isWorkAnniversary'] = true;

					$dateOfbirth = $newArray[$i]['joining_date'];
					
					$diff = date_diff(date_create($dateOfbirth), date_create(date("Y-m-d")));

					$month = $diff->format('%m');

					if($month > 0){
						$number = $diff->format('%y') + 1;
					}else{
						$number = $diff->format('%y');
					}

					if (($number %100) >= 11 && ($number%100) <= 13){
					 	$abbreviation = $number. 'th';
					}else{
					 	$abbreviation = $number. $ends[$number % 10];
					}

					$birthDay['total_year_view'] = $abbreviation." Work Anniversary";

				}

				$bdatet = date('d-m',strtotime($birthDayDate));

				if ($bdatet == date('d-m')) {
					$birthDay["is_today"] = true;
					$birthDay['member_date_of_birth_view'] = date('dS F',strtotime($birthDayDate));
				}else{
					$birthDay["is_today"] = false;
					$birthDay['member_date_of_birth_view'] = date('dS F',strtotime($birthDayDate));
				}			

				if ($number > 0) {
	                array_push($response['today_birth_days'],$birthDay);
				}
            }			

			$todayTime = date('Y-m-d H:i:s');

			$user_mobile = mysqli_real_escape_string($con, $user_mobile);
			if ($country_code!="") {
				$q = $d->select("users_master,block_master,floors_master,unit_master,user_employment_details",
					"users_master.delete_status=0 
					AND users_master.user_mobile ='$user_mobile'
	                AND users_master.country_code='$country_code' 
	                AND users_master.block_id=block_master.block_id
	                AND users_master.floor_id=floors_master.floor_id
	                AND users_master.unit_id=unit_master.unit_id 
	                AND users_master.society_id ='$society_id' 
	                AND user_employment_details.user_id=users_master.user_id");
			} else {
				$q = $d->select("users_master,block_master,floors_master,unit_master,user_employment_details",
					"users_master.delete_status=0 
					AND users_master.user_mobile ='$user_mobile'
	                AND users_master.block_id=block_master.block_id
	                AND users_master.floor_id=floors_master.floor_id
	                AND users_master.unit_id=unit_master.unit_id 
	                AND users_master.society_id ='$society_id' AND user_employment_details.user_id=users_master.user_id");

			}


			$qsing= $d->selectRow("tenant_agreement_end_date,VPNCheck,user_type","users_master","society_id ='$society_id' AND delete_status=0 AND user_id='$user_id'");
			$singleUserData= mysqli_fetch_array($qsing);
			if (mysqli_num_rows($qsing)>0 && $singleUserData['VPNCheck']==1) {
				$VPNCheck = false;
			} else {
				$VPNCheck = true;
			}
			if ($singleUserData['user_type']==1 && $singleUserData['tenant_agreement_end_date']!='0000-00-00' && $singleUserData['tenant_agreement_end_date']!='') {
				$expire = strtotime($singleUserData['tenant_agreement_end_date']);
				$today = strtotime("today midnight");
				if($today > $expire){
					$tenant_agreement_over = true;
				} else {
					$tenant_agreement_over = false;
				}
			} else {
				$tenant_agreement_over = false;
			}


			$qSociety = $d->select("society_master", "society_id ='$society_id'");
			$user_data_society = mysqli_fetch_array($qSociety);
			$timeline_aceess_for_society = $user_data_society['timeline_aceess_for_society'];
			$chat_aceess_for_society = $user_data_society['chat_aceess_for_society'];
			$myactivity_aceess_for_society = $user_data_society['myactivity_aceess_for_society'];
			$attendance_type = $user_data_society['attendance_type'];
			$work_report_on = $user_data_society['work_report_on'];
			$take_attendance_selfie = $user_data_society['take_attendance_selfie'];
			$society_geofence_latitude = $user_data_society['society_geofence_latitude'];
			$society_geofence_longitude = $user_data_society['society_geofence_longitude'];
			$society_geofence_range = $user_data_society['society_geofence_range'];
			$download_url_tracking_app = $user_data_society['download_url_tracking_app'];
			$tracking_app_version_android = $user_data_society['tracking_app_version_android'];
			$bind_mac_address = $user_data_society['bind_mac_address'];
			$plan_expire_date = $user_data_society['plan_expire_date'];
			$socieaty_logo = $base_url.'img/society/'.$user_data_society['socieaty_logo'];
			$branch_geo_fencing = $user_data_society['branch_geo_fencing'];
			$hide_attendance_face_menu = $user_data_society['hide_attendance_face_menu'];
			$hide_birthday_view = $user_data_society['hide_birthday_view'];
			$hide_department_employee_view = $user_data_society['hide_department_employee_view'];
			$hide_gallery_view = $user_data_society['hide_gallery_view'];
			$attendance_with_matching_face = $user_data_society['attendance_with_matching_face'];
			$retailer_product_price = $user_data_society['retailer_product_price'];
			$location_log_every_minute = $user_data_society['location_log_every_minute'];
			$share_order_access = $user_data_society['share_order_access'];
			$cancel_order_after_confirm = $user_data_society['cancel_order_after_confirm'];
			$order_without_punch_in = $user_data_society['order_without_punch_in'];
			$app_update_dailog_type = $user_data_society['app_update_dailog_type'];

			$todayDateForExpiry = date('Y-m-d');

			if ($todayDateForExpiry > $plan_expire_date) {
				$is_plan_expired = true;
			} else {
				$is_plan_expired = false;
			}

			$response['expire_msg'] = "Your MYCO plan has been expired on ".date('d F Y')." please contact to admin to use MYCO app without any interuption";

			$response['is_plan_expired'] = $is_plan_expired;
			

			if ($order_without_punch_in==1) {
				$order_without_punch_in = true;
			} else {
				$order_without_punch_in = false;
			}

			if ($share_order_access==1) {
				$share_order_access = true;
			} else {
				$share_order_access = false;
			}

			if ($cancel_order_after_confirm==1) {
				$cancel_order_after_confirm = true;
			} else {
				$cancel_order_after_confirm = false;
			}

			if ($hide_attendance_face_menu==1) {
				$hide_attendance_face_menu = true;
			} else {
				$hide_attendance_face_menu = false;
			}

			if ($retailer_product_price == 0) {
				$retailer_product_price = true;
			} else {
				$retailer_product_price = false;
			}

			if ($hide_birthday_view==1) {
				$hide_birthday_view = true;
			} else {
				$hide_birthday_view = false;
			}

			if ($hide_department_employee_view==1) {
				$hide_department_employee_view = true;
			} else {
				$hide_department_employee_view = false;
			}

			if ($hide_gallery_view==1) {
				$hide_gallery_view = true;
			} else {
				$hide_gallery_view = false;
			}

			if ($attendance_with_matching_face==1) {
				$attendance_with_matching_face = true;
			} else {
				$attendance_with_matching_face = false;
			}

			if ($timeline_aceess_for_society==1) {
				$timeline_aceess_for_society = true;
			} else {
				$timeline_aceess_for_society = false;
			}

			if ($chat_aceess_for_society==1) {
				$chat_aceess_for_society = true;
			} else {
				$chat_aceess_for_society = false;
			}

			if ($myactivity_aceess_for_society==1) {
				$myactivity_aceess_for_society = true;
			} else {
				$myactivity_aceess_for_society = false;
			}

			$qSociety1 = $d->count_data_direct("society_id","society_master", "society_id ='$society_id' AND (work_report_on = '1' OR (work_report_on = '2' AND FIND_IN_SET('$block_id',work_report_on_ids)) OR (work_report_on = '3' AND FIND_IN_SET('$floor_id',work_report_on_ids)))");

			$work_report_on = false;
		
			if ($qSociety1 > 0) {
				$work_report_on = true;
			} 

			$uq = $d->selectRow("*","users_master","society_id ='$society_id' AND user_id='$user_id'");
			$ud = mysqli_fetch_array($uq);

             $response["app_update_dailog_type"]=$app_update_dailog_type;
            
             $response["order_without_punch_in"]=$order_without_punch_in;
             $response["location_log_every_minute"]=$location_log_every_minute;
             $response["share_order_access"]=$share_order_access;
             $response["cancel_order_after_confirm"]=$cancel_order_after_confirm;
             $response["work_report_on"]=$work_report_on;
             $response["take_product_price_from_user"]=$retailer_product_price;
             $response["attendance_type"]=$attendance_type."";
             $response["take_attendance_selfie"]=$take_attendance_selfie."";
             $response["hide_timeline"]=$timeline_aceess_for_society;
             $response["hide_chat"]=$chat_aceess_for_society;
             $response["hide_myactivity"]=$myactivity_aceess_for_society;
             $response["VPNCheck"]=$VPNCheck;
             $response["tenant_agreement_over"]=$tenant_agreement_over;
             $response["socieaty_logo"]=$socieaty_logo;
             $response["download_url_tracking_app"]=$download_url_tracking_app;
             $response["tracking_app_version_android"]=$tracking_app_version_android;
             $response["hide_attendance_face_menu"]=$hide_attendance_face_menu;
             $response["hide_birthday_view"]=$hide_birthday_view;
             $response["hide_department_employee_view"]=$hide_department_employee_view;
             $response["hide_gallery_view"]=$hide_gallery_view;
             $response["attendance_with_matching_face"]=$attendance_with_matching_face;


			if ($user_data_society['bind_mac_address'] == 1) {
				// code...

				if ($ud['user_mac_address'] == '') {
					$m->set_data('user_mac_address', $user_mac_address);

					$m->set_data('user_id', $user_id);
					$m->set_data('society_id', $society_id);
					$m->set_data('mac_address_device', $phone_brand);
					$m->set_data('mac_address_phone_modal', $phone_model);
					$m->set_data('mac_address_change_status', "1");
					$m->set_data('mac_address_change_reason', "First Time Binding");
					$m->set_data('mac_address_status_change_date', date('Y-m-d'));

					$aa = array(
						'user_id' => $m->get_data('user_id'),
						'society_id' => $m->get_data('society_id'),
						'mac_address_device' => $m->get_data('mac_address_device'),
						'mac_address_phone_modal' => $m->get_data('mac_address_phone_modal'),
						'mac_address_change_status' => $m->get_data('mac_address_change_status'),
						'mac_address_change_reason' => $m->get_data('mac_address_change_reason'),
						'mac_address' => $m->get_data('user_mac_address'),
						'mac_address_status_change_date' => $m->get_data('mac_address_status_change_date'),
					);

					$qqq = $d->insert("user_mac_address_master",$aa);

					$m->set_data('user_mac_address', $user_mac_address);

					$a = array(
						'user_mac_address' => $m->get_data('user_mac_address'),
					);

					$d->update("users_master", $a, "user_mobile='$user_mobile' AND user_mobile!='' AND user_mobile!='0'");

				}
			}

			if ($device == "android") {
				$m->set_data('android_sdk_version', $android_sdk_version);
				$m->set_data('mobile_os_version', $android_version);
				$m->set_data('android_os_name', $android_os_name);

				$andData = array(
					'android_sdk_version' => $m->get_data('android_sdk_version'),
					'mobile_os_version' => $m->get_data('mobile_os_version'),
					'android_os_name' => $m->get_data('android_os_name'),
				);
			}else if ($device == "ios"){
				$m->set_data('mobile_os_version', $ios_version);

				$andData = array(
					'mobile_os_version' => $m->get_data('mobile_os_version'),
				);
			}


			$d->update("users_master", $andData, "user_mobile='$user_mobile' AND user_mobile!='' AND user_mobile!='0'");


            $q111=$d->select("reminder_master","society_id='$society_id' AND user_id='$user_id' AND reminder_status=0","ORDER BY reminder_id DESC");
          

            $response["reminder"] = array();

            $today=date('Y-m-d');
            $cTime= date("H:i:s");
            $today = strtotime("$today $cTime");
            while($data_bill_list=mysqli_fetch_array($q111)) {
                    $expire = strtotime($data_bill_list['reminder_date']);
                   if($today > $expire){
                     $reminder = array(); 
                     $reminder["reminder_id"]=$data_bill_list['reminder_id'];
                     $reminder["user_id"]=$data_bill_list['user_id'];
                     $reminder["society_id"]=$data_bill_list['society_id'];
                     $reminder["reminder_text"]=html_entity_decode($data_bill_list['reminder_text']);
                     $reminder["reminder_date"]=$data_bill_list['reminder_date'];
                     $reminder["reminder_date_view"]=date("d M Y h:i A", strtotime($data_bill_list['reminder_date']));
                     if ($data_bill_list['reminder_status']==0) {
                     $reminder["reminder_me"]=true;
                     } else {
                     $reminder["reminder_status"]=false;
                     }
                     array_push($response["reminder"], $reminder);
                  }
            
            }  

			if (mysqli_num_rows($q) > 0) {
				$response["units"] = array();

				if ($user_token=="") {
					$response["message"] = "Your login session expired please login again.";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				} 

				while ($user_data = mysqli_fetch_array($q)) {

					if ($user_data['is_demo']==1) {
						$cTime = date("Y-m-d H:i:s");
					    $expire = strtotime($user_data[demo_expire_time]);
					    $today = strtotime("$cTime");
					    if ($today >= $expire) {
					    	$isDemoExpire = 'Yes';
					    }
					} 

					
					
					if ($user_token!=$user_data['user_token'] || $isDemoExpire=='Yes') {
						$user_fcm = $user_data['user_token'];
						// $device = $user_data['device'];
						if ($device == 'android') {
							$nResident->noti("", "", $society_id, $user_token, "Logout", "Multiple Device Login !", 'logout');
						} else if ($device == 'ios') {
							$nResident->noti_ios("", "", $society_id, $user_token, "Logout", "Multiple Device Login !", 'logout');
						}
						$logoutIosDevice = true;
					}


					$units = array();
					$units["logoutIosDevice"] = $logoutIosDevice;
					$units["user_id"] = $user_data['user_id'];
					$user_id = $user_data['user_id'];
					$shift_timeID = $user_data['shift_time_id'];
					$token = $user_data['user_token'];

					$shiftQ = $d->select("shift_timing_master","shift_time_id = '$shift_timeID' AND is_deleted = '0'");

					if (mysqli_num_rows($shiftQ) > 0) {
						$shiftD = mysqli_fetch_array($shiftQ);
						$shiftData = array(
							"shift_time_id" => $shiftD['shift_time_id'],
							"per_day_hour" => $shiftD['per_day_hour'],
							"shift_start_time" => $shiftD['shift_start_time'],
							"shift_end_time" => $shiftD['shift_end_time'],
							"lunch_break_start_time" => $shiftD['lunch_break_start_time'],
							"lunch_break_end_time" => $shiftD['lunch_break_end_time'],
							"tea_break_start_time" => $shiftD['tea_break_start_time'],
							"tea_break_end_time" => $shiftD['tea_break_end_time'],
							"shift_type" => $shiftD['shift_type'],
						);

						$units['shift_data'] = $shiftData;
					}

					$wfhDate = date('Y-m-d');

					$wfhReq = $d->select("wfh_master","user_id = '$user_data[user_id]' AND floor_id = '$user_data[floor_id]' AND wfh_start_date = '$wfhDate' AND wfh_status = '1'");					

					if (mysqli_num_rows($wfhReq) > 0) {
						$wfhData = mysqli_fetch_array($wfhReq);
						$wfhArray = array(
							"wfh_id" => $wfhData['wfh_id'].'',
							"wfh_start_date" => $wfhData['wfh_start_date'].'',
							"wfh_latitude" => $wfhData['wfh_latitude'].'',
							"wfh_longitude" => $wfhData['wfh_longitude'].'',
							"wfh_attendance_range" => $wfhData['wfh_attendance_range'].'',
							"wfh_take_selfie" => $wfhData['wfh_take_selfie'].'',
							"wfh_address" => 'Work From Home',
						);

						$units['wfh_data'] = $wfhArray;
					}

					if ($user_data['expense_on_off'] == 0) {
						$units['is_expense_on'] = true;
					}else{
						$units['is_expense_on'] = false;	
					}

					$take_accessibility_permission = $user_data['take_accessibility_permission'];

					if ($take_accessibility_permission==1) {
						$take_accessibility_permission = true;
					} else {
						$take_accessibility_permission = false;
					}

					$units["take_accessibility_permission"]=$take_accessibility_permission;
					$units["society_id"] = $user_data['society_id'];
					$units["shift_time_id"] = $user_data['shift_time_id'];
					$society_id = $user_data['society_id'];
					$units["user_full_name"] = $user_data['user_full_name'];
					$units["user_first_name"] = $user_data['user_first_name'];
					$units["user_last_name"] = $user_data['user_last_name'];
					$units["user_mobile"] = $user_data['user_mobile'];
					$units["country_code"] = $user_data['country_code'];
					$units["user_email"] = $user_data['user_email'];
					$units["user_id_proof"] = $user_data['user_id_proof'];
					if ($user_data['user_visiting_card'] != '') {
						$units["user_visiting_card"] = $base_url.'img/users/recident_profile/'.$user_data['user_visiting_card'];
					}else{				
						$units["user_visiting_card"] = "";
					}
					if ($user_data['user_visiting_card_back'] != '') {
						$units["user_visiting_card_back"] = $base_url.'img/users/recident_profile/'.$user_data['user_visiting_card_back'];
					}else{				
						$units["user_visiting_card_back"] = "";
					}
					$units["user_type"] = $user_data['user_type'];
					$units["block_id"] = $user_data['block_id'];					
					$units["block_name"] = $user_data['block_name'];
					$units["floor_name"] = $user_data['floor_name'];
					$units["unit_name"] = $user_data['block_name'];
					$units["base_url"] = $base_url;
					$units["floor_id"] = $user_data['floor_id'];
					$units["unit_id"] = $user_data['unit_id'];
					$units["zone_id"] = $user_data['zone_id'];
					$units["level_id"] = $user_data['level_id'];
					$units["unit_status"] = $user_data['unit_status'];
					$units["user_status"] = $user_data['user_status'];
					$units["member_status"] = $user_data['member_status'];
					$units["public_mobile"] = $user_data['public_mobile'];
					$units["visitor_approved"] = $user_data['visitor_approved'];
					$units["member_date_of_birth"] = "" . $user_data['member_date_of_birth'];
					$units["wedding_anniversary_date"] = "" . $user_data['wedding_anniversary_date'];
					$units["facebook"] = $user_data['facebook'];
					$units["instagram"] = $user_data['instagram'];
					$units["linkedin"] = $user_data['linkedin'];
					$units["user_geofence_latitude"] = $user_data['user_geofence_latitude'];
					$units["user_geofence_longitude"] = $user_data['user_geofence_longitude'];
					$units["user_geofence_range"] = $user_data['user_geofence_range'];
					$units["track_user_time"] = $user_data['track_user_time'];

					$units['user_location_list'] = array();

					$qqGeo = $d->select("user_geofence","user_id = '$user_id'");

					if (mysqli_num_rows($qqGeo) > 0) {
						while($dataGeo = mysqli_fetch_array($qqGeo)){

							$geoArray = array();

							$geoArray['geo_fance_id'] = $dataGeo['geo_fance_id'];
							$geoArray['user_geo_address'] = $dataGeo['work_location_name'];
							$geoArray['user_geofence_range'] = $dataGeo['user_geofence_range'];
							$geoArray['user_geofence_longitude'] = $dataGeo['user_geofence_longitude'];
							$geoArray['user_geofence_latitude'] = $dataGeo['user_geofence_latitude'];

							array_push($units['user_location_list'],$geoArray);

						}
					}

					if ($branch_geo_fencing == 1) {
						$qqqGeo = $d->select("block_master","society_id = '$society_id' AND block_geofence_latitude != '' AND block_geofence_longitude != ''");
					}else{
						$qqqGeo = $d->select("block_master","society_id = '$society_id' AND block_geofence_latitude != '' AND block_geofence_longitude != '' AND block_id = '$block_id'");
					}

					if (mysqli_num_rows($qqqGeo) > 0) {
						while($dataqGeo = mysqli_fetch_array($qqqGeo)){

							$geoArray = array();

							$geoArray['geo_fance_id'] = $dataqGeo['block_id'];
							$geoArray['user_geo_address'] = $dataqGeo['block_name'];
							$geoArray['user_geofence_range'] = $dataqGeo['block_geofence_range'];
							$geoArray['user_geofence_longitude'] = $dataqGeo['block_geofence_longitude'];
							$geoArray['user_geofence_latitude'] = $dataqGeo['block_geofence_latitude'];

							array_push($units['user_location_list'],$geoArray);

						}
					}



					$units["block_geofence_latitude"]=$user_data['block_geofence_latitude']."";
		            $units["block_geofence_longitude"]=$user_data['block_geofence_longitude']."";
		            $units["block_geofence_range"]=$user_data['block_geofence_range']."";

					if($user_data['allow_wfh'] == "1"){
						$units['allow_wfh'] = true;
					}else{
						$units['allow_wfh'] = false;
					}

					if($user_data['track_user_location'] == "1"){
						$units['track_user_location'] = true;
					}else{
						$units['track_user_location'] = false;
					}
					
					$units["company_name"] = $user_data_society['society_name'];
					$units["company_address"] = $user_data_society['society_address'];
					$units["plot_lattitude"] = $user_data_society['plot_lattitude'];
					$units["plot_longitude"] = $user_data_society['plot_longitude'];
					if ($user_data_society['socieaty_logo']!="") {
	                    $units["company_logo"] = $base_url.'img/society/'. $user_data_society['socieaty_logo'];
	                } else {
	                    $units["company_logo"] ="";
	                }
	                
					if ($user_data['user_work_report_on']=="1") {
	                    $response["work_report_on"]= true;
	                }else if ($user_data['user_work_report_on']=="2") {
	                    $response["work_report_on"]= false;
	                } 
					// Check Employee Access

					$leaveAccess = $d->getModuleAccess($user_id,$leave_access);
					$expenseAccess = $d->getModuleAccess($user_id,$expense_access);
					$punchInRequestAccess = $d->getModuleAccess($user_id,$punch_in_req_access);
					$punchOutMissingAccess = $d->getModuleAccess($user_id,$punch_out_missing_req_access);
					$escalationAccess = $d->getModuleAccess($user_id,$escalation_access);
					$ideaApproveAccess = $d->getModuleAccess($user_id,$idea_approve_access);
					$pendingAttendanceApproveAccess = $d->getModuleAccess($user_id,$pending_attendance_access);
					$approveEmployeeAccess = $d->getModuleAccess($user_id,$approve_employee_access);
					$addEmployeeAccess = $d->getModuleAccess($user_id,$add_employee_access);
					$viewAbsentPresentAccess = $d->getModuleAccess($user_id,$view_absent_present_access);
					$wfhReqAccess = $d->getModuleAccess($user_id,$wfh_req_access);
					$viewEmpAttendance = $d->getModuleAccess($user_id,$view_employee_attendance_access);
					$mobileDeviceBindAccess = $d->getModuleAccess($user_id,$mobile_device_bind_Access);
					$trackEmployeeLocation = $d->getModuleAccess($user_id,$location_tracking_Access);
					$birthdayRequest = $d->getModuleAccess($user_id,$birthday_change_request_Access);
					$workReportViewAccess = $d->getModuleAccess($user_id,$work_report_Access);


					$accessData = array(
						"leaveAccess" => $leaveAccess,
						"expenseAccess" => $expenseAccess,
						"punchInRequestAccess" => $punchInRequestAccess,
						"punchOutMissingAccess" => $punchOutMissingAccess,
						"escalationAccess" => $escalationAccess,
						"ideaApproveAccess" => $ideaApproveAccess,
						"pendingAttendanceApproveAccess" => $pendingAttendanceApproveAccess,
						"approveEmployeeAccess" => $approveEmployeeAccess,
						"addEmployeeAccess" => $addEmployeeAccess,
						"viewAbsentPresentAccess" => $viewAbsentPresentAccess,
						"wfhReqAccess" => $wfhReqAccess,
						"viewEmpAttendance" => $viewEmpAttendance,
						"mobileDeviceBindAccess" => $mobileDeviceBindAccess,
						"trackEmployeeLocation" => $trackEmployeeLocation,
						"birthdayRequest" => $birthdayRequest,
						"workReportViewAccess" => $workReportViewAccess,
					);

					$units['employee_access'] = $accessData;

					
					$units["get_business_data"] = false;

					$q1=$d->select("user_employment_details","user_id='$user_data[user_id]'");
                    $proData=mysqli_fetch_array($q1);
					$units["designation"] = html_entity_decode($user_data['user_designation']);
					$units["business_categories"] = $proData['business_categories'];
					$units["business_categories_sub"] = $proData['business_categories_sub'];
					$units["business_categories_other"] = $proData['business_categories_other'];
					$units["professional_other"] = $proData['professional_other'];

					if ($user_data['active_status']==1) {
		                $units["account_deactive"]=true;
		            } else {
		                $units["account_deactive"]=false;
		            }

					if ($user_data['blood_group']!="") {
						$units["blood_group"] = $user_data['blood_group'];
					} else {
						$units["blood_group"] =$xml->string->not_available.'';
					}
					if ($user_data['alt_mobile'] != 0) {
						$units["alt_mobile"] = $user_data['alt_mobile'];
					} else {
						$units["alt_mobile"] = "";
					}
					$units["country_code_alt"] = $user_data['country_code_alt'];
					$units["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $user_data['user_profile_pic'];


					$units["owner_name"] = "";
					$units["owner_email"] = "";
					$units["owner_mobile"] ="";
					$units["owner_mobile_country_code"] = "";
					

					$units["gender"] = $user_data['gender'];
					$units["child_gate_approval"] = $user_data['child_gate_approval'];
					$qSociety = $d->select("society_master", "society_id ='$society_id'");
					$user_data_society = mysqli_fetch_array($qSociety);

					
					$units["society_address"] = html_entity_decode($user_data_society['society_address']) . "";
					$units["society_latitude"] = $user_data_society['society_latitude'] . "";
					$units["society_longitude"] = $user_data_society['society_longitude'] . "";
					$units["society_name"] = html_entity_decode($user_data_society['society_name']) . "";
					$response["country_id"] = $user_data_society['country_id'] . "";
					$response["state_id"] = $user_data_society['state_id'] . "";
					$response["city_id"] = $user_data_society['city_id'] . "";
					$units["city_name"] = ', ' . $user_data_society['city_name'];

					$units["currency"] = $user_data_society['currency']."";

					$units["api_key"] = $keydb . "";
					if ($user_data_society['socieaty_logo']!="") {
						$units["socieaty_logo"] = $base_url . 'img/society/' . $user_data_society['socieaty_logo'] . "";
					} else {
						$units["socieaty_logo"] = "https://my-company.app/img/logo.png";
					}

					$unit_id = $user_data['unit_id'];
					$block_id = $user_data['block_id'];
					$floor_id = $user_data['floor_id'];

					$qfamily = $d->select("users_master", "delete_status=0 AND society_id ='$society_id'and block_id='$block_id' AND floor_id='$floor_id' AND unit_id='$unit_id' AND member_status='1'");

					$qemergencynum = $d->select("user_emergencycontact_master", "society_id ='$society_id' and user_id='$user_id'");

					
					$employment_edu = $d->selectRow("employee_experience_id as emp_edu","employee_experience", "user_id='$user_id'","");
					$employment_edu_arr = mysqli_fetch_array($employment_edu);

					$employment_achi = $d->selectRow("archi_type as emp_achi","employee_achievements_education", "user_id='$user_id'","");
					$employment_achi_arr = mysqli_fetch_array($employment_achi);

					$units["member"] = array();
					$units["emergency"] = array();
					
					$units["is_society"] = false;
					$units["label_member_type"] = "Team Members";
					$units["label_setting_apartment"] = "Unit closed";
					$units["label_setting_resident"] = "Contact Number Privacy for Members";

					$profile_progress = 0;
					if ($user_data['user_full_name']!="") {
						$profile_progress +="10";
					}
					if ($user_data['user_profile_pic']!="" && $user_data['user_profile_pic']!="user_default.png") {
						$profile_progress +="10";
					}
					if ($user_data['user_mobile']!="") {
						$profile_progress +="10";
					}
					if ($user_data['gender']!="") {
						$profile_progress +="5";
					}
					if ($user_data['last_address']!="") {
						$profile_progress +="5";
					}
					if ($user_data['user_email']!="") {
						$profile_progress +="5";
					}
					if ($user_data['whatsapp_number']>0) {
						$profile_progress +="5";
					}
					if ($user_data['emergency_number']>0) {
						$profile_progress +="5";
					}
					if ($user_data['permanent_address']!="") {
						$profile_progress +="3";
					}
					if ($user_data['member_date_of_birth']!="") {
						$profile_progress +="3";
					}
					if ($user_data['nationality']!="") {
						$profile_progress +="3";
					}
					if ($user_data['alt_mobile']>0) {
						$profile_progress +="3";
					}
					if ($user_data['blood_group']!="") {
						$profile_progress +="3";
					}
					if ($user_data['user_designation']!="") {
						$profile_progress +="3";
					}
					if ($user_data['marital_status']>0) {
						$profile_progress +="3";
					}
					if ($user_data['total_family_members']>0) {
						$profile_progress +="2";
					}
					if ($user_data['facebook']!="") {
						$profile_progress +="2";
					}
					if ($user_data['instagram']!="") {
						$profile_progress +="2";
					}
					if ($user_data['linkedin']!="") {
						$profile_progress +="2";
					}
					if ($user_data['twitter']!="") {
						$profile_progress +="2";
					}
					if ($proData['job_location']!="") {
						$profile_progress +="2";
					}
					if ($proData['intrest_hobbies']!="") {
						$profile_progress +="2";
					}
					if ($proData['professional_skills']!="") {
						$profile_progress +="2";
					}
					if ($proData['special_skills']!="") {
						$profile_progress +="2";
					}
					if ($proData['language_known']!="") {
						$profile_progress +="2";
					}
					if ($employment_edu_arr['emp_edu']!="" && !empty($employment_edu_arr['emp_edu'])) {
						$profile_progress +="2";
					}
					if (mysqli_num_rows($employment_achi)>0) {
						$profile_progress +="2";
					}
					
					if ($profile_progress>100) {
						$profile_progress = "100";
					}

					$units["profile_progress"] = $profile_progress."";

					while ($datafamily = mysqli_fetch_array($qfamily)) {

						$member = array();
						$member["user_id"] = $datafamily['user_id'];
						$member["user_first_name"] = $datafamily['user_first_name'];
						$member["user_last_name"] = $datafamily['user_last_name'];
						$member["user_mobile"] = $datafamily['user_mobile'];
						$member["member_date_of_birth"] = $datafamily['member_date_of_birth'] . '';
						$dateOfBirth = "" . $datafamily['member_date_of_birth'];
						// $today = date("Y-m-d");
						// $diff = date_diff(date_create($dateOfBirth), date_create($today));
						$member["member_age"] = "";
						$member["member_relation_name"] = $datafamily['member_relation_name'];
						$member["user_status"] = $datafamily['user_status'];
						$member["member_status"] = $datafamily['member_status'];

						array_push($units["member"], $member);

					}
					while ($dataemergency = mysqli_fetch_array($qemergencynum)) {
						$emergency = array();

						$emergency["emergencyContact_id"] = $dataemergency['emergencyContact_id'];
						$emergency["person_name"] = $dataemergency['person_name'];
						$emergency["person_mobile"] = $dataemergency['person_mobile'];
						$emergency["relation_id"] = $dataemergency['relation_id'];
						$emergency["relation"] = $dataemergency['relation'];

						array_push($units["emergency"], $emergency);
					}

					$m->set_data('user_token', $user_token);

					if ($device!="" && $logoutIosDevice==false) 
					{	
						$aTemp = array(
							'user_token' => $m->get_data('user_token'),
							'last_login' => $todayTime,
							'phone_model' => $phone_model,
							'device' => $device,
	                        'phone_brand' => $phone_brand,
			                'app_version_code' => $app_version_code,
			                'location_permission_status' => $location_permission_status,
						);
					}
					else if($logoutIosDevice==false)
					{
						$aTemp = array(
							'user_token' => $m->get_data('user_token'),
							'last_login' => $todayTime,
							'phone_model' => $phone_model,
	                        'phone_brand' => $phone_brand,
			                'app_version_code' => $app_version_code,
			                'device' => $device,
			                'location_permission_status' => $location_permission_status,
						);
					}else if($user_data['user_token'] == '' && $user_token != ''){
						$aTemp = array(
							'user_token' => $m->get_data('user_token'),
							'last_login' => $todayTime,
							'phone_model' => $phone_model,
	                        'phone_brand' => $phone_brand,
			                'app_version_code' => $app_version_code,
			                'device' => $device,
			                'location_permission_status' => $location_permission_status,
						);
					}

					if ($user_mobile != '' && $user_token != '') {
						$d->update("users_master", $aTemp, "delete_status=0 AND user_mobile='$user_mobile'");
					}

					array_push($response["units"], $units);

				} //while end

				$qSociety = $d->select("society_master", "society_id ='$society_id'");
	            $user_data_society = mysqli_fetch_array($qSociety);
	            $visitor_on_off = $user_data_society['visitor_on_off'];
	            $entry_all_visitor_group = $user_data_society['entry_all_visitor_group'];
	            $group_chat_status = $user_data_society['group_chat_status'];
	            $screen_sort_capture_in_timeline = $user_data_society['screen_sort_capture_in_timeline'];
	            $create_group = $user_data_society['create_group'];
	            $hide_member_parking = $user_data_society['hide_member_parking'];
	            $tenant_registration = $user_data_society['tenant_registration'];
	            $virtual_wallet = $user_data_society['virtual_wallet'];
	            if($user_data_society['professional_on_off']==1) {
	                $professional_on_off =false;
	            }else {
	                $professional_on_off =true;
	            }

	            if($hide_member_parking==1) {
	                $hide_member_parking =true;
	            }else {
	                $hide_member_parking =false;
	            }
	          
	            $virtual_wallet =false;

	            $qnotification = $d->select("user_notification", "user_id='$user_id' AND notification_type=0", "ORDER BY user_notification_id DESC LIMIT 500");

	            $tq=$d->selectRow("feed_id","news_feed","status = 0 ","ORDER BY feed_id DESC ");
	            $timelineData=mysqli_fetch_array($tq);
	            if ($timelineData>0) {
	              	$last_feed_id = $timelineData['feed_id'].'';
	            } else {
	                $last_feed_id = '0';
	            }

                $qnotificationCount = $d->select("user_notification", "user_id='$user_id' AND read_status='0' AND  notification_type=0");
                $response["read_status"] = mysqli_num_rows($qnotificationCount) . "";

                $qchatCount = $d->select("chat_master", "msg_for='$user_id' 
                    AND society_id='$society_id' AND msg_status='0'", "GROUP BY msg_by");
                $response["chat_status"] = mysqli_num_rows($qchatCount) . "";
                $response["visitor_on_off"] = $visitor_on_off;
                $response["entry_all_visitor_group"] = $entry_all_visitor_group;
                $response["group_chat_status"] = $group_chat_status;
                $response["screen_sort_capture_in_timeline"] = $screen_sort_capture_in_timeline;
                $response["create_group"] = $create_group;
                $response["tenant_registration"] = $tenant_registration;
                $response["last_feed_id"] = $last_feed_id;
                $response["professional_on_off"] = $professional_on_off;
                $response["hide_member_parking"] = $hide_member_parking;
                $response["virtual_wallet"] = $virtual_wallet;
				
				
				$response["building_name"] = "Company";
				$response["unit_name"] = "Unit";
				$response["message"] = "Get Member Success. ";
				$response["status"] = "200";
				echo json_encode($response);

			} else {

				$response["message"] = "No Member found in this Company";
				$response["status"] = "201";
				echo json_encode($response);

			}

		} else if (isset($deRegisterUnit) && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$a22 = array(
				'unit_status' => 0,
			);

			$q = $d->delete("unit_master", "unit_id='$unit_id' AND unit_status=4 ");

			if ($q > 0) {

				$q = $d->delete("users_master", "user_id='$user_id' AND unit_id='$unit_id'");
				$q = $d->delete("user_employment_details", "user_id='$user_id' AND unit_id='$unit_id'");
				$response["message"] = "Un Register Successfully";
				$response["status"] = "200";
				echo json_encode($response);

			} else {

				$response["message"] = "Something Wrong";
				$response["status"] = "201";
				echo json_encode($response);

			}

			// $d->insert_log("", "$society_id", "$userId", "$created_by", "User Request Rejected");

		}
		else if(isset($getEducation) && filter_var($user_id, FILTER_VALIDATE_INT) == true)
		{
		    $qar= $d->select("employee_achievements_education", "user_id='$user_id' AND archi_type = '1'  AND delete_status = '0'", "");
		    if(mysqli_num_rows($qar) > 0)
		    {
				$response["education"] = array();
				while ($empArr = mysqli_fetch_array($qar)) 
				{
					$education = array();
	            	$education["employee_achievement_id"] = $empArr['employee_achievement_id'];
	            	$education["achievement_name"] = html_entity_decode($empArr['achievement_name']);
	            	$education["achievement_date"] = $empArr['achievement_date'];
	            	$education["university_board_name"] = html_entity_decode($empArr['university_board_name']);
	            	$education["archi_type"] = $empArr['archi_type'];
	                array_push($response["education"], $education);
	            }
	            $response["message"] = "Get Education Details Successfully...";
				$response["status"] = "200";
				echo json_encode($response);
			}
			else
			{
				$response["message"] = "No Education Details Found...";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}
		else if(isset($getAchivment) && filter_var($user_id, FILTER_VALIDATE_INT) == true)
		{
		    $qar= $d->select("employee_achievements_education", "user_id='$user_id' AND archi_type = '0' AND delete_status = '0'", "");
			$response["achievement"] = array();

		    if(mysqli_num_rows($qar) > 0)
		    {
				while ($empArr = mysqli_fetch_array($qar)) 
				{
					$achievement = array();
	            	$achievement["employee_achievement_id"] = $empArr['employee_achievement_id'];
	            	$achievement["achievement_name"] = html_entity_decode($empArr['achievement_name']);
	            	$achievement["achievement_date"] = $empArr['achievement_date'];
	            	$achievement["university_board_name"] = html_entity_decode($empArr['university_board_name']);
	            	$achievement["archi_type"] = $empArr['archi_type'];
	                array_push($response["achievement"], $achievement);
	            }
	           
			}

			$qar= $d->select("employee_achievements_education", "user_id='$user_id' AND archi_type = '1'  AND delete_status = '0'", "");
			$response["education"] = array();

		    if(mysqli_num_rows($qar) > 0)
		    {
				while ($empArr = mysqli_fetch_array($qar)) 
				{
					$education = array();
	            	$education["employee_achievement_id"] = $empArr['employee_achievement_id'];
	            	$education["achievement_name"] = html_entity_decode($empArr['achievement_name']);
	            	$education["achievement_date"] = $empArr['achievement_date'];
	            	$education["university_board_name"] = html_entity_decode($empArr['university_board_name']);
	            	$education["archi_type"] = $empArr['archi_type'];
	                array_push($response["education"], $education);
	            }
	            
			}
				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
			
		}else if(isset($sendBirthdayWishes) && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

				$title = "Birthday Wish";
				$description = $birthday_message;

			    $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
	            $data_notification = mysqli_fetch_array($qsm);
	            $user_token = $data_notification['user_token'];
	            $device = $data_notification['device'];

	            $dataAry = array(
                    "img_url"=>$base_url.'img/noticeBoard/happy_birthday.png',
                    "title"=>$title,
                    "description"=>$description,
                    "notification_time"=>date('Y-m-d h:i A'),
                );

	            if ($device == 'android') {
	                $nResident->noti("custom_notification","",$society_id,$user_token,$title,$description,$dataAry);
	            } else if ($device == 'ios') {
	                $nResident->noti_ios("custom_notification","",$society_id,$user_token,$title,$description,$dataAry);
	            }

             	$notiAry = array(
                    'society_id'=>$society_id,
                    'user_id'=>$user_id,
                    'notification_title'=>$title,
                    'notification_desc'=>$description,    
                    'notification_date'=>date('Y-m-d H:i'),
                    'notification_action'=>'custom_notification',
                    'notification_logo'=>'happy_birthday.png',
                    'notification_type'=>'0',
                );
                
                $d->insert("user_notification",$notiAry);

	            $response["message"] = "Birthday wishes sent";
				$response["status"] = "200";
				echo json_encode($response);
			}
		else {
			$response["message"] = "wrong tag.";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {

		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}

function getFormatedTime($time) {

	$timeData = explode(':', $time);

    $hours = $timeData[0];
    $minutes = $timeData[1];

    if ($hours > 0 && $minutes > 0) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
    	if ($hours>1) {
	        return sprintf('%02d Hours', $hours);    	
    	}else{
	        return sprintf('%02d Hour', $hours);    	
    	}
    }else if ($hours <= 0 && $minutes > 0) {
    	if ($minutes>1) {
	        return sprintf('%02d Minutes', $minutes);    	
    	}else{
	        return sprintf('%02d Minute', $minutes);    	
    	}
    }else{
        return "00:00";
    }
}

function getTotalHoursWithNames($startTime, $endTime) {

    $date_a = new DateTime($startTime);
    $date_b = new DateTime($endTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
    	if ($hours>1) {
	        return sprintf('%02d Hours', $hours);    	
    	}else{
	        return sprintf('%02d Hour', $hours);    	
    	}
    }else if ($hours <= 0 && $minutes > 0) {
    	if ($minutes>1) {
	        return sprintf('%02d Minutes', $minutes);    	
    	}else{
	        return sprintf('%02d Minute', $minutes);    	
    	}
    }else{
        return "00:00";
    }
}
?>