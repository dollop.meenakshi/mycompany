<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        if($_POST['checkAccessIdea']=="checkAccessIdea" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $idea_approve_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['getIdeaCategory']=="getIdeaCategory" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $ideaCatQry = $d->select("idea_category_master","society_id='$society_id' 
                AND idea_category_active_status='0' AND idea_category_delete = '0'");

            if(mysqli_num_rows($ideaCatQry)>0){
                
                $response["idea_category"] = array();

                while($leaveTypeData=mysqli_fetch_array($ideaCatQry)) {

                    $leaveTypeData = array_map("html_entity_decode", $leaveTypeData);

                    $idea_category = array(); 

                    $idea_category["idea_category_id"] = $leaveTypeData['idea_category_id'];
                    $idea_category["idea_category_name"] = $leaveTypeData['idea_category_name'];

                    array_push($response["idea_category"], $idea_category);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Idea categories not found, contact to admin!";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['addIdea']=="addIdea" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $idea_attachment = "";

            if ($_FILES["idea_attachment"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["idea_attachment"]["tmp_name"];
                $extId = pathinfo($_FILES['idea_attachment']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","png","jpg","jpeg","PDF","JPEG","PNG","JPG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["idea_attachment"]["name"]);
                    $idea_attachment = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["idea_attachment"]["tmp_name"], "../img/idea/" . $idea_attachment);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }


            $m->set_data('idea_category_id',$idea_category_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('floor_id',$floor_id);
            $m->set_data('idea_title',$idea_title);
            $m->set_data('idea_description',$idea_description);
            $m->set_data('idea_attachment',$idea_attachment);
            $m->set_data('idea_created_date',$dateTime);

            $a = array(
                'idea_category_id'=>$m->get_data('idea_category_id'),
                'society_id' =>$m->get_data('society_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'user_id'=>$m->get_data('user_id'),
                'floor_id'=>$m->get_data('floor_id'),
                'idea_title'=>$m->get_data('idea_title'),
                'idea_description'=>$m->get_data('idea_description'),
                'idea_attachment'=>$m->get_data('idea_attachment'),
                'idea_created_date'=>$m->get_data('idea_created_date'),
            );

            $applyLeaveQry = $d->insert("idea_master",$a);

            if ($applyLeaveQry == true) {

                $d->insert_myactivity($user_id, "$society_id", $user_id, "$user_name", "New Idea Added", "idea_box.png");

                $title = "New Idea Approval";
                $description = "Idea added by, ".$user_name;// ."\n".$amount.' - '.$expense_title;

                $access_type = $idea_approve_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");


                    $nResident->noti("pending_idea","",$society_id,$fcmArrayPA,$title,$description,"");
                    $nResident->noti_ios("pending_idea","",$society_id,$fcmArrayIosPA,$title,$description,"");

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"pending_idea","idea_box.png",$dataJson,"users_master.user_id IN ('$userFcmIds')");
                }


                $response["message"] = "Idea Submitted";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getMyIdeas']=="getMyIdeas" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true){

            

            if ($isMyIdea == "1") {
                if ($idea_id != null && $idea_id != '') {
                    $ideaAppend = " AND idea_master.idea_id = '$idea_id'";
                }
                $myIdeaAppendQry = " AND idea_master.user_id = '$user_id' $ideaAppend";
            }else{
                if ($idea_id != null && $idea_id != '') {
                    $myIdeaAppendQry = " AND idea_status = '1'";
                }else{
                    $myIdeaAppendQry = " AND idea_master.user_id != '$user_id' 
                        AND idea_status = '1'";
                }
            }

           
            $ideaQry = $d->select("idea_category_master,idea_master,users_master","idea_category_master.idea_category_id = idea_master.idea_category_id 
                AND idea_category_master.society_id = idea_master.society_id 
                AND users_master.user_id = idea_master.user_id 
                AND idea_master.society_id = '$society_id' $myIdeaAppendQry $singleIdeaQry","ORDER BY idea_master.idea_created_date DESC");


            if(mysqli_num_rows($ideaQry)>0){
                
                $response["idea_list"] = array();

                while($ideaData = mysqli_fetch_array($ideaQry)) {

                    
                    $ideaData = array_map("html_entity_decode", $ideaData);

                    $idea_list = array();

                    $ideaID = $ideaData['idea_id'];

                    $idea_list["idea_id"] = $ideaID;
                    $idea_list["user_id"] = $ideaData['user_id'];
                    $idea_list["idea_category_id"] = $ideaData['idea_category_id'];
                    $idea_list["idea_category_name"] = $ideaData['idea_category_name'];
                    $idea_list["idea_title"] = $ideaData['idea_title'];
                    $idea_list["idea_description"] = $ideaData['idea_description'];

                    $idea_list["idea_by"] = "By ".$ideaData['user_full_name'].' ('.$ideaData['user_designation'].')';

                    if ($ideaData['idea_attachment']!="") {
                        $idea_list["idea_attachment"] = $base_url."img/idea/". $ideaData['idea_attachment'];
                    }else{
                        $idea_list["idea_attachment"] = "";
                    }
                    $idea_list["idea_created_date"] = date("d M Y, h:i A",strtotime($ideaData["idea_created_date"]));                
                    $idea_list["idea_status"] = $ideaData['idea_status'];

                    if ($ideaData["idea_status"] == 0) {
                        $idea_list["idea_status_view"] = "Pending";
                    }else if ($ideaData["idea_status"] == 1) {
                        $idea_list["idea_status_view"] = "Approved";
                    }else if ($ideaData["idea_status"] == 2) {
                        $idea_list["idea_status_view"] = "Declined";
                    }

                    $totalIdeaComments = $d->count_data_direct("idea_comment_id","idea_comment_master","idea_id = '$ideaID' AND idea_parent_comment_id = '0'");

                    if ($totalIdeaComments == '1' || $totalIdeaComments == '0') {
                        $commentS = "Comment";
                    }else{
                        $commentS = "Comments";
                    }

                    $idea_list["total_comments"] = $totalIdeaComments.'';
                    $idea_list["commentS"] = $commentS;

                    $totalIdeaLikes = $d->count_data_direct("idea_like_id","idea_like_master","idea_id = '$ideaID'");

                    if ($totalIdeaLikes == '1' || $totalIdeaLikes == '0') {
                       $likeS = "Like";
                    }else{
                        $likeS = "Likes";
                    }

                    $idea_list["total_likes"] = $totalIdeaLikes.'';
                    $idea_list["likeS"] = $likeS;

                    $hasMyLike = $d->selectRow("idea_like_id","idea_like_master","idea_id = '$ideaID' AND user_id = '$user_id'");

                    if (mysqli_num_rows($hasMyLike) > 0) {
                        $idea_list["has_my_like"] = true;

                        $likeData = mysqli_fetch_array($hasMyLike);
                        $idea_list["idea_like_id"] = $likeData['idea_like_id'];
                    }else{
                        $idea_list["has_my_like"] = false;
                        $idea_list["idea_like_id"] = "";
                    }


                    $hasMyComment = $d->selectRow("idea_comment_id","idea_comment_master","idea_id = '$ideaID' AND user_id = '$user_id'");

                    if (mysqli_num_rows($hasMyComment) > 0) {
                        $idea_list["has_my_comment"] = true;
                    }else{
                        $idea_list["has_my_comment"] = false;
                    }

                    $idea_list['approved_by_name'] = "";

                    if ($ideaData["idea_status"] == 1) {
                        if ($ideaData['idea_status_change_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$ideaData[idea_approved_by]'");
                            $ideaDataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($ideaDataApproveName['user_full_name']!=null) {
                                $idea_list['approved_by_name'] = "By, ".$ideaDataApproveName['user_full_name']."";
                            }else{
                                $idea_list['approved_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$ideaData[idea_approved_by]'");
                            $ideaDataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($ideaDataApproveName['admin_name']!=null) {
                                $idea_list['approved_by_name'] = "By, ".$ideaDataApproveName['admin_name']."";
                            }else{
                                $idea_list['approved_by_name'] = "";
                            }
                        }
                    }


                    $idea_list['rejected_by_name'] = "";

                    if ($ideaData["idea_status"] == 2) {
                        if ($ideaData['idea_status_change_by_type'] == 0) {
                            $rejectedByQry = $d->selectRow("user_full_name","users_master","user_id = '$ideaData[idea_declined_by]'");
                            $ideaDataRejectedName = mysqli_fetch_array($rejectedByQry);

                            if ($ideaDataRejectedName['user_full_name']!=null) {
                                $idea_list['rejected_by_name'] = "By, ".$ideaDataRejectedName['user_full_name']."";
                            }else{
                                $idea_list['rejected_by_name'] = "";
                            }

                        }else{
                            $rejectedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$ideaData[idea_declined_by]'");
                            $ideaDataRejectedName = mysqli_fetch_array($rejectedByQry);

                             if ($ideaDataRejectedName['admin_name']!=null) {
                                $idea_list['rejected_by_name'] = "By, ".$ideaDataRejectedName['admin_name']."";
                            }else{
                                $idea_list['rejected_by_name'] = "";
                            }
                        }
                    }

                    array_push($response["idea_list"], $idea_list);
                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"] = "No idea added";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['getPendingIdeaList']=="getPendingIdeaList" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            $access_type = $idea_approve_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND idea_master.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND idea_master.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $ideaQry = $d->selectRow("idea_master.*,users_master.user_id,users_master.user_full_name,users_master.user_profile_pic,users_master.user_designation,floors_master.floor_name,block_master.block_name,idea_category_master.idea_category_name","idea_category_master,idea_master,users_master,floors_master,block_master","idea_category_master.idea_category_id = idea_master.idea_category_id 
                AND idea_category_master.society_id = idea_master.society_id 
                AND users_master.user_id = idea_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id 
                AND idea_master.society_id = '$society_id' 
                AND idea_master.idea_status = '0' $appendQuery","ORDER BY idea_master.idea_created_date DESC $LIMIT_DATA");

            if(mysqli_num_rows($ideaQry)>0){

                $response["idea_list"] = array();
                
                while($ideaData = mysqli_fetch_array($ideaQry)) {
                    
                    $ideaData = array_map("html_entity_decode", $ideaData);

                    $idea_list = array();

                    $ideaID = $ideaData['idea_id'];

                    $idea_list["idea_id"] = $ideaID;
                    $idea_list["user_id"] = $ideaData['user_id'];
                    $idea_list['user_full_name'] = $ideaData['user_full_name'];
                    $idea_list['branch_name'] = $ideaData['block_name'];
                    $idea_list['department_name'] = $ideaData['floor_name'];
                    $idea_list['user_designation'] = $ideaData['user_designation'];

                    if ($ideaData['user_profile_pic'] != '') {
                        $idea_list["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $ideaData['user_profile_pic'];
                    } else {
                        $idea_list["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }
                    $idea_list["idea_category_id"] = $ideaData['idea_category_id'];
                    $idea_list["idea_category_name"] = $ideaData['idea_category_name'];
                    $idea_list["idea_title"] = $ideaData['idea_title'];
                    $idea_list["idea_description"] = $ideaData['idea_description'];

                    $idea_list["idea_by"] = "By ".$ideaData['user_full_name'].' ('.$ideaData['user_designation'].')';

                    if ($ideaData['idea_attachment']!="") {
                        $idea_list["idea_attachment"] = $base_url."img/idea/". $ideaData['idea_attachment'];
                    }else{
                        $idea_list["idea_attachment"] = "";
                    }
                    $idea_list["idea_created_date"] = date("d-m-Y",strtotime($ideaData["idea_created_date"]));                
                    $idea_list["idea_status"] = $ideaData['idea_status'];

                    if ($ideaData["idea_status"] == 0) {
                        $idea_list["idea_status_view"] = "Pending";
                    }else if ($ideaData["idea_status"] == 1) {
                        $idea_list["idea_status_view"] = "Approved";
                    }else if ($ideaData["idea_status"] == 2) {
                        $idea_list["idea_status_view"] = "Declined";
                    }

                    $totalIdeaComments = $d->count_data_direct("idea_comment_id","idea_comment_master","idea_id = '$ideaID' AND idea_parent_comment_id = '0'");

                    if ($totalIdeaComments == '1' || $totalIdeaComments == '0') {
                        $commentS = "Comment";
                    }else{
                        $commentS = "Comments";
                    }

                    $idea_list["total_comments"] = $totalIdeaComments.'';
                    $idea_list["commentS"] = $commentS;

                    $totalIdeaLikes = $d->count_data_direct("idea_like_id","idea_like_master","idea_id = '$ideaID'");

                    if ($totalIdeaLikes == '1' || $totalIdeaLikes == '0') {
                       $likeS = "Like";
                    }else{
                        $likeS = "Likes";
                    }

                    $idea_list["total_likes"] = $totalIdeaLikes.'';
                    $idea_list["likeS"] = $likeS;

                    $hasMyLike = $d->selectRow("idea_like_id","idea_like_master","idea_id = '$ideaID' AND user_id = '$user_id'");

                    if (mysqli_num_rows($hasMyLike) > 0) {
                        $idea_list["has_my_like"] = true;

                        $likeData = mysqli_fetch_array($hasMyLike);
                        $idea_list["idea_like_id"] = $likeData['idea_like_id'];
                    }else{
                        $idea_list["has_my_like"] = false;
                        $idea_list["idea_like_id"] = "";
                    }

                    $hasMyComment = $d->selectRow("idea_comment_id","idea_comment_master","idea_id = '$ideaID' AND user_id = '$user_id'");

                    if (mysqli_num_rows($hasMyComment) > 0) {
                        $idea_list["has_my_comment"] = true;
                    }else{
                        $idea_list["has_my_comment"] = false;
                    }

                    array_push($response["idea_list"], $idea_list);
                }   
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"] = "No idea added";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['approveIdea']=="approveIdea" && $user_id!='' && $idea_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('idea_approved_by',$user_id);
            $m->set_data('idea_status',"1");

            $a = array(
                'idea_approved_by' =>$m->get_data('idea_approved_by'),
                'idea_status'=>$m->get_data('idea_status'),
            );

            $q1 = $d->update("idea_master",$a,"idea_id = '$idea_id' AND user_id = '$idea_user_id'");

            if ($q1 == true) { 

                $title = "Your idea has been approved";
                $description = "Approved by, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$idea_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("idea_page","",$society_id,$user_token,$title,$description,$idea_id);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("idea_page","",$society_id,$user_token,$title,$description,$idea_id);
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"idea_page","idea_box.png","user_id = '$idea_user_id'",$idea_id);


                $response["message"] = "Idea Approved";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed to approve idea";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['rejectIdea']=="rejectIdea" && $user_id!='' && $idea_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('idea_declined_by',$user_id);
            $m->set_data('idea_status',"2");

            $a = array(
                'idea_declined_by' =>$m->get_data('idea_declined_by'),
                'idea_status'=>$m->get_data('idea_status'),
            );

            $q1 = $d->update("idea_master",$a,"idea_id = '$idea_id' AND user_id = '$idea_user_id'");

            if ($q1 == true) { 

                $title = "Your idea is rejected";
                $description = "Rejected by, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$idea_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("idea_page","",$society_id,$user_token,$title,$description,$idea_id);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("idea_page","",$society_id,$user_token,$title,$description,$idea_id);
                }

                $d->insertUserNotification($society_id,$title,$description,"idea_page","idea_box.png","user_id = '$idea_user_id'",$idea_id);


                $response["message"] = "Idea Rejected";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed to reject idea";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['deleteIdea']=="deleteIdea" && $user_id!='' && $idea_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $deleteQry = $d->delete("idea_master","idea_id = '$idea_id' AND user_id = '$user_id'");

            if ($deleteQry == true) {

                $d->insert_myactivity($user_id, "$society_id", $user_id, "$user_name", "Idea Added", "idea_box.png");


                $response["message"] = "Idea deleted";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed to delete idea";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getIdeaLikeList']=="getIdeaLikeList" && $user_id!='' && $idea_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $ideaQry = $d->select("idea_like_master,idea_master,users_master","idea_master.idea_id = idea_like_master.idea_id AND idea_like_master.user_id = users_master.user_id AND 
                idea_like_master.idea_id = '$idea_id'");

            if(mysqli_num_rows($ideaQry)>0){
                
                $response["idea_like_list"] = array();

                while($ideaData = mysqli_fetch_array($ideaQry)) {

                    $idea_like_list = array();

                    $ideaID = $ideaData['idea_id'];

                    $idea_like_list["idea_id"] = $ideaID;
                    $idea_like_list["idea_like_id"] = $ideaData['idea_like_id'];
                    $idea_like_list["user_id"] = $ideaData['user_id'];
                    $idea_like_list["society_id"]=$ideaData['society_id'];
                    $idea_like_list["user_full_name"]=$ideaData['user_first_name'].' '.$ideaData['user_last_name'].' ('.$ideaData['user_designation'].')';

                    if ($ideaData['user_profile_pic'] != '') {
                        $idea_like_list["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $ideaData['user_profile_pic'];
                    } else {
                        $idea_like_list["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }
                    

                    array_push($response["idea_like_list"], $idea_like_list);
                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Likes";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['getIdeaComments']=="getIdeaComments" && $user_id!='' && $idea_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $ideaQry = $d->select("idea_comment_master,idea_master,users_master","idea_master.idea_id = idea_comment_master.idea_id AND idea_comment_master.user_id = users_master.user_id AND 
                idea_comment_master.idea_id = '$idea_id' AND idea_comment_master.idea_parent_comment_id = '0'","ORDER BY idea_comment_master.idea_comment_date DESC");

            if(mysqli_num_rows($ideaQry)>0){
                
                $response["idea_comments"] = array();

                while($ideaData = mysqli_fetch_array($ideaQry)) {

                    $idea_comments = array();

                    $ideaID = $ideaData['idea_id'];

                    $idea_comments["idea_id"] = $ideaID;
                    $idea_comments["idea_comment_id"] = $ideaData['idea_comment_id'];
                    $idea_comments["user_id"] = $ideaData['user_id'];
                    $idea_comments["society_id"]=$ideaData['society_id'];
                    $idea_comments["user_full_name"]=$ideaData['user_first_name'].' '.$ideaData['user_last_name'].' ('.$ideaData['user_designation'].')';
                    $idea_comments["idea_comment"]=$ideaData['idea_comment'];
                    $idea_comments["idea_comment_date"] = time_elapsed_string($ideaData['idea_comment_date']);

                    if ($ideaData['user_profile_pic'] != '') {
                        $idea_comments["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $ideaData['user_profile_pic'];
                    } else {
                        $idea_comments["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $subCommentQry =  $d->select("idea_comment_master,idea_master,users_master","idea_master.idea_id = idea_comment_master.idea_id AND idea_comment_master.user_id = users_master.user_id AND 
                        idea_comment_master.idea_id = '$ideaID' AND idea_comment_master.idea_parent_comment_id = '$ideaData[idea_comment_id]'","ORDER BY idea_comment_master.idea_comment_date DESC");

                    $idea_comments['sub_comments'] = array();

                    if (mysqli_num_rows($subCommentQry) > 0) {
                        while($subCommentData = mysqli_fetch_array($subCommentQry)){

                            $ideaSubComments = array();

                            $ideaSubComments["idea_id"] = $subCommentData['idea_id'];
                            $ideaSubComments["idea_sub_comment_id"] = $subCommentData['idea_comment_id'];
                            $ideaSubComments["idea_parent_comment_id"] = $subCommentData['idea_parent_comment_id'];
                            $ideaSubComments["user_id"] = $subCommentData['user_id'];
                            $ideaSubComments["society_id"]=$subCommentData['society_id'];
                            $ideaSubComments["user_full_name"]=$subCommentData['user_first_name'].' '.$subCommentData['user_last_name'].' ('.$subCommentData['user_designation'].')';
                            $ideaSubComments["idea_comment"]=$subCommentData['idea_comment'];
                            $ideaSubComments["idea_comment_date"] = time_elapsed_string($subCommentData['idea_comment_date']);

                            if ($subCommentData['user_profile_pic'] != '') {
                                $ideaSubComments["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $subCommentData['user_profile_pic'];
                            } else {
                                $ideaSubComments["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                            }

                            array_push($idea_comments['sub_comments'],$ideaSubComments);
                        }
                    }
                    

                    array_push($response["idea_comments"], $idea_comments);
                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Likes";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['addLike']=="addLike" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $isAlreadyLike = $d->select("idea_like_master","user_id = '$user_id' AND idea_id = '$idea_id'");

            if (mysqli_num_rows($isAlreadyLike) > 0) {
                $response["message"] = "Idea Liked";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            }

            $m->set_data('idea_id',$idea_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('idea_liked_date',$dateTime);

            $a = array(
                'idea_id'=>$m->get_data('idea_id'),
                'user_id' =>$m->get_data('user_id'),
                'idea_liked_date'=>$m->get_data('idea_liked_date'),
            );

            $addLikeQry = $d->insert("idea_like_master",$a);

            if ($addLikeQry == true) {

                if ($user_id != $other_employee_id) {

                    $title = "Idea Box";
                    $description = $user_name." liked your idea";

                    $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$other_employee_id'");
                    $data_notification = mysqli_fetch_array($qsm);
                    $user_token = $data_notification['user_token'];
                    $device = $data_notification['device'];

                    if ($device == 'android') {
                        $nResident->noti("idea_like","",$society_id,$user_token,$title,$description,$idea_id);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("idea_like","",$society_id,$user_token,$title,$description,$idea_id);
                    }

                    $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$other_employee_id,
                        'notification_title'=>$title,
                        'notification_desc'=>$description,    
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'idea_like',
                        'notification_logo'=>'idea_box.png',
                        'notification_type'=>'0',
                        'feed_id'=>$idea_id,
                    );
                    
                    $d->insert("user_notification",$notiAry);
                }

                $response["message"] = "Idea Liked";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['removeLike']=="removeLike" && $user_id!='' && filter_var($idea_like_id, FILTER_VALIDATE_INT) == true){

            $removeIdea = $d->delete("idea_like_master","idea_like_id = '$idea_like_id' AND idea_id = '$idea_id' AND user_id = '$user_id'");

            if ($removeIdea == true) {
                $response["message"] = "Idea Unliked";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['addIdeaComment']=="addIdeaComment" && $user_id!=''  && $idea_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('idea_id',$idea_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('idea_comment',$idea_comment);
            $m->set_data('idea_parent_comment_id',$idea_parent_comment_id);
            $m->set_data('idea_comment_date',$dateTime);

            $a = array(
                'idea_id'=>$m->get_data('idea_id'),
                'user_id' =>$m->get_data('user_id'),
                'idea_comment' =>$m->get_data('idea_comment'),
                'idea_parent_comment_id' =>$m->get_data('idea_parent_comment_id'),
                'idea_comment_date'=>$m->get_data('idea_comment_date'),
            );

            $addLikeQry = $d->insert("idea_comment_master",$a);

            if ($addLikeQry == true) {

                if ($user_id != $other_employee_id) {

                     if ($idea_parent_comment_id == "0") {
                        $response["message"] = "Comment added";
                        $title = "Idea Box";
                        $description = $user_name." commented on your idea, ".$idea_comment;
                    }else{
                        $response["message"] = "Reply added";

                        $title = "Idea Box";
                        $description = $user_name." replied on your comment, ".$idea_comment;
                    }

                    $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$other_employee_id'");
                    $data_notification = mysqli_fetch_array($qsm);
                    $user_token = $data_notification['user_token'];
                    $device = $data_notification['device'];

                    if ($device == 'android') {
                        $nResident->noti("idea_comment","",$society_id,$user_token,$title,$description,$idea_id);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("idea_comment","",$society_id,$user_token,$title,$description,$idea_id);
                    }

                    $notiAry = array(
                        'society_id'=>$society_id,
                        'user_id'=>$other_employee_id,
                        'notification_title'=>$title,
                        'notification_desc'=>$description,    
                        'notification_date'=>date('Y-m-d H:i'),
                        'notification_action'=>'idea_comment',
                        'notification_logo'=>'idea_box.png',
                        'notification_type'=>'0',
                        'feed_id'=>$idea_id,
                    );
                    
                    $d->insert("user_notification",$notiAry);
                }

               
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['removeIdeaComment']=="removeIdeaComment" && $user_id!='' && $idea_comment_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $deleteComment = $d->delete("idea_comment_master","idea_id = '$idea_id' AND user_id = '$user_id' AND idea_comment_id = '$idea_comment_id'");

            if ($deleteComment == true) {

                $deleteComment = $d->delete("idea_comment_master","idea_id = '$idea_id' AND user_id = '$user_id' AND idea_parent_comment_id = '$idea_comment_id'");

                $response["message"] = "Comment Deleted";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

?>