<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
  $maintenanceName =  $xml->string->maintenance;

  $societyLngName=  $xml->string->society;
  if ($key==$keydb && $auth_check=='true') {
  $response = array();
  extract(array_map("test_input" , $_POST));
   
    if($_POST['pay']=="pay"){
        // add data in society_transection_master table 
        $sq=$d->selectRow("society_name,currency,society_address,city_name,society_pincode,secretary_email,socieaty_logo","society_master","society_id='$society_id'");
        $societyData=mysqli_fetch_array($sq);
        $society_name=$societyData['society_name'];
        $secretary_email= $societyData['secretary_email'];
        $socieaty_logo=$societyData['socieaty_logo'];
        $society_address_invoice= $societyData['society_address'].', '.$societyData['city_name'].' ('.$societyData['society_pincode'].')';
        $currency = $societyData['currency'];
        $received_by = "Auto";
        $paid_by = $userName;

       
        $txt_status= $paymentStatus;
        $txt_id= $paymentTransactionsId;


          $qm=$d->select("users_master","user_id='$user_id' AND user_id!=0");
          $user_data_list=mysqli_fetch_array($qm);

          $send_sms_number=$user_data_list['user_mobile'];
          $user_email=$user_data_list['user_email'];

          if($paymentTypeFor==0){
            $common_id= $paymentReceivedMaintenanceId;
          }else if($paymentTypeFor==1){
            $common_id= $paymentReceivedBillId;
          }else if($paymentTypeFor==2){
            $common_id= $facilityId;
          }else if($paymentTypeFor==3){
            $common_id= $eventDayId;
          }else if($paymentTypeFor==4){
            $common_id= $penaltyId;
          }

      
          $m->set_data('society_id',$society_id);
          $transection_date=date("Y-m-d H:i:s");
         
          $m->set_data('user_id',$user_id);
          $m->set_data('unit_id',$unit_id);
          $m->set_data('payment_for_type',$paymentTypeFor);
          if ($paymentFor=="Maintenance") {
            $paymentFor = $maintenanceName;
          }
          $m->set_data('payment_for_name',$paymentForName.' - '.$paymentFor);
          $m->set_data('transaction_type',$paymentFor);
          $m->set_data('payment_txnid',$paymentTransactionsId);
          $m->set_data('payment_firstname',$userName);
          $m->set_data('user_mobile',$userMobile);
          $m->set_data('payment_email',$userEmail);
          $m->set_data('payment_phone',$userMobile);
          $m->set_data('payment_address',$unit_name);
          $m->set_data('bank_ref_num',$paymentBankReferenceNumber);
          $m->set_data('bankcode',$paymentBankCode);
          $m->set_data('error_Message',$paymentErrorMsg);
          $m->set_data('name_on_card',$paymentNameOnCard);
          $m->set_data('payment_status',$paymentStatus);       
          $m->set_data('cardnum',$paymentCardNumber);
          $m->set_data('payment_mode',$payment_mode);
          $m->set_data('discount',$paymentDiscount);
          $m->set_data('transection_amount',$paymentTransactionsAmount);
          $m->set_data('wallet_balance_used',$wallet_balance_used);
          $m->set_data('transection_date',$transection_date);
          $m->set_data('payuMoneyId',$payuMoneyId);
          $m->set_data('transaction_charges',$transaction_charges);
          $m->set_data('common_id',$common_id);
          $invoice_no= "0".date('ymdi').$user_id;
          $m->set_data('invoice_no',$invoice_no);
          $payment_firstname= $userName;
          $transection_amount = $paymentTransactionsAmount;
          $balancesheet_id = $paymentBalanceSheetId;
          $user_mobile = $userMobile;

          $a1 = array(
                   'society_id'=>$m->get_data('society_id'), 
                   'user_id'=>$m->get_data('user_id'),
                   'transection_date'=>$m->get_data('transection_date'),
                   'unit_id'=>$m->get_data('unit_id'),
                   'payment_for_type'=>$m->get_data('payment_for_type'), 
                   'payment_for_name'=>$m->get_data('payment_for_name'),
                   'transaction_type'=>$m->get_data('transaction_type'),
                   'user_mobile'=>$m->get_data('user_mobile'),
                   'payment_mode'=>$m->get_data('payment_mode'),
                   'transection_amount'=>$m->get_data('transection_amount'),
                   'wallet_balance_used'=>$m->get_data('wallet_balance_used'),
                   'transection_date'=>$m->get_data('transection_date'),
                   'payment_status'=>$m->get_data('payment_status'),
                   'payment_txnid'=>$m->get_data('payment_txnid'), 
                   'payment_firstname'=>$m->get_data('payment_firstname'),
                   'payment_lastname'=>$m->get_data('payment_lastname'),
                   'payment_phone'=>$m->get_data('payment_phone'),
                   'payment_email'=>$m->get_data('payment_email'), 
                   'payment_address'=>$m->get_data('payment_address'),
                   'bank_ref_num'=>$m->get_data('bank_ref_num'),
                   'bankcode'=>$m->get_data('bankcode'),
                   'payment_email'=>$m->get_data('payment_email'), 
                   'payment_address'=>$m->get_data('payment_address'),
                   'bank_ref_num'=>$m->get_data('bank_ref_num'),
                   'bankcode'=>$m->get_data('bankcode'),
                   'error_Message'=>$m->get_data('error_Message'), 
                   'name_on_card'=>$m->get_data('name_on_card'),
                   'cardnum'=>$m->get_data('cardnum'),
                   'discount'=>$m->get_data('discount'),
                   'invoice_no'=>$m->get_data('invoice_no'),
                   'transaction_charges'=>$m->get_data('transaction_charges'),
                   'common_id'=>$m->get_data('common_id'),
              );

            
         $q1=$d->insert("society_transection_master",$a1);  
         $transection_id = $con->insert_id;



        if ($paymentTypeFor==0) {
          // maintenance payment
            if ($is_wallet_applied=='true') {

              $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
              $row=mysqli_fetch_array($count6);
              $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

              $remark= "Payment for Maintenance $paymentForName";      

              $m->set_data('society_id',$society_id);
              $m->set_data('user_mobile',$user_mobile);
              $m->set_data('unit_id',$unit_id);
              $m->set_data('remark',$remark);
              $m->set_data('debit_amount',$wallet_balance_used);
              $m->set_data('avl_balance',$totalDebitAmount1-$wallet_balance_used);


              $aWallet = array(
                'society_id'=>$m->get_data('society_id'),
                'user_mobile'=>$m->get_data('user_mobile'),
                'unit_id'=>$m->get_data('unit_id'),
                'remark'=>$m->get_data('remark'),
                'debit_amount'=>$m->get_data('debit_amount'),
                'avl_balance'=>$m->get_data('avl_balance'),
                'created_date'=>date("Y-m-d H:i:s"), 
              );
              
               
              $wallet_amount = $wallet_balance_used;
              $wallet_amount_type=1;

              $transection_amount = $transection_amount + $wallet_balance_used;
            } else {
              $transection_amount= $paymentTransactionsAmount;
            } 
          

          $receive_maintenance_id=$paymentReceivedMaintenanceId;
          // $transection_amount= $paymentTransactionsAmount;
          $discount_amount = $paymentDiscountAmount;
          $late_fee = $paymentLateFee;
          $payment_for_name= $paymentForName;
          $user_name= $userName;
          $custom_amount= $customAmount;

          $mqq=$d->select("maintenance_master,receive_maintenance_master","receive_maintenance_master.maintenance_id=receive_maintenance_master.maintenance_id AND receive_maintenance_master.maintenance_id='$maintenance_id' AND receive_maintenance_master.receive_maintenance_id='$receive_maintenance_id'");
          $receive_maintenance_master_data=mysqli_fetch_array($mqq);

          $prv_received_amount = ($receive_maintenance_master_data['received_amount']+$transection_amount)-($transaction_charges)-$late_fee; //20
          $prv_maintenance_late_fees = $receive_maintenance_master_data['maintenance_late_fees'];
          $received_amount = $transection_amount;

          $prv_received_amount11 = $receive_maintenance_master_data['received_amount'];
          $prv_transaction_charges = $receive_maintenance_master_data['transaction_charges_all']+$transaction_charges;
          // if($custom_amount =="1"){

              $fullMaintenace =$receive_maintenance_master_data['original_maintenance_amount']+$receive_maintenance_master_data['maintenance_late_fees']+$late_fee_received;  //5
              if ($custom_amount =="0" && $receive_maintenance_master_data['received_amount']==0) {
                  $receive_maintenance_status = 1;
                  $receive_maintenance_master_data['original_maintenance_amount'];
                  $newTest = $receive_maintenance_master_data['original_maintenance_amount']+$late_fee;
                  if ($newTest>$transection_amount) {
                    $discount_amount = $discount_amount;
                  } else {
                    $discount_amount= 0;
                  }
              } else {
                if ($prv_received_amount>=$fullMaintenace) {
                  $receive_maintenance_status = 1;
                } else {
                  $receive_maintenance_status = 2;
                }
                $discount_amount= 0;
              }

                
            

               
               $m->set_data('discount_amount',$discount_amount);

              $m->set_data('society_id',$society_id);
              $m->set_data('receive_maintenance_id',$receive_maintenance_id);
              $m->set_data('maintenance_id',$maintenance_id);
              $m->set_data('amount',$received_amount-$late_fee);
              $m->set_data('total_received_amount',$received_amount);
              $m->set_data('late_fee_received',$late_fee);
              $m->set_data('paid_by',$user_id);
              $m->set_data('wallet_amount',$wallet_amount);
              $m->set_data('wallet_amount_type',$wallet_amount_type);
              $date_today =date("Y-m-d");
              $m->set_data('receive_date_maintenace',date("Y-m-d H:i:s"));

              $m->set_data('created_date',$date_today);
              $array_new= array (
                'society_id'=> $m->get_data('society_id'),
                'receive_maintenance_id'=> $m->get_data('receive_maintenance_id'),
                'maintenance_id'=> $m->get_data('maintenance_id'),
                'amount'=> $m->get_data('amount'),
                'discount_amount'=> $m->get_data('discount_amount'),
                'transaction_charges'=> $m->get_data('transaction_charges'),
                'total_received_amount'=> $m->get_data('total_received_amount'),
                'late_fee_received'=> $m->get_data('late_fee_received'),
                'payment_type'=>'2',
                'payment_detail' => $paymentTransactionsId,
                'bank_name'=> $m->get_data('bank_name'),
                'receive_date'=> $m->get_data('receive_date_maintenace'),

                'receive_receipt_photo'=> $m->get_data('receive_maintenance_receipt_photo'),
                'created_date'=> $m->get_data('created_date'),
                'ref_no'=> $m->get_data('ref_no'),
                'paid_by'=> $m->get_data('paid_by'),
                'wallet_amount'=> $m->get_data('wallet_amount'),
                'wallet_amount_type'=> $m->get_data('wallet_amount_type'),

              );


            
             
              $q15=$d->insert("receive_maintenance_master_details",$array_new);
              $detail_id = $con->insert_id;

              $m->set_data('receive_maintenance_status',$receive_maintenance_status);

              $ref_no="REF".date('Ymd').$unit_id.$receive_maintenance_id;
              $cTime =date('Y-m-d H:i:s');

              $receive_maintenance_date=date("Y-m-d H:i:s");
              $maintenance_late_fees  = $late_fee;
              $m->set_data('receive_maintenance_date',$receive_maintenance_date);
              $m->set_data('payment_type','2');

              $m->set_data('discount_amount',$discount_amount);
              $m->set_data('transection_id',$transection_id);
              $m->set_data('received_amount',$transection_amount+$prv_received_amount11);
              $m->set_data('maintenance_late_fees',$maintenance_late_fees+$prv_maintenance_late_fees);
              $m->set_data('ref_no',$ref_no);

              $a5 = array(
                       'received_amount'=>$m->get_data('received_amount'), 
                       'transaction_charges_all'=>$prv_transaction_charges, 
                       'receive_maintenance_status'=>$m->get_data('receive_maintenance_status'),
                       'receive_maintenance_date'=>$m->get_data('receive_maintenance_date'),
                       'payment_type'=>$m->get_data('payment_type'),
                       'received_amount'=>$m->get_data('received_amount'),
                       'maintenance_late_fees'=>$m->get_data('maintenance_late_fees'),
                       'discount_amount'=>$m->get_data('discount_amount'),
                       'ref_no'=>$m->get_data('ref_no') ,
                  );

              $q=$d->update("receive_maintenance_master",$a5,"receive_maintenance_id='$receive_maintenance_id'");
              
          

          if ($q==TRUE) {

               if ($wallet_amount>0) {
                    $wq=$d->insert("user_wallet_master",$aWallet);
                 }

               $block_id=$d->getBlockid($user_id);
               $fcmArray=$d->selectAdminBlockwise("2",$block_id,"android");
               $fcmArrayIos=$d->selectAdminBlockwise("2",$block_id,"ios");

              $caRy = array(
                   'common_id'=>$detail_id,
              );

              $d->update("society_transection_master",$caRy,"transection_id ='$transection_id' "); 

              

              $fq=$d->select("receive_maintenance_master","receive_maintenance_id='$receive_maintenance_id'");
              $billData=mysqli_fetch_array($fq);
              $mId=$billData['maintenance_id'];
  
              $nAdmin->noti_new($society_id,"",$fcmArray,"$payment_for_name paid by ".$user_name,"Paid Amount is $currency ".$transection_amount." ","paidMaintenace?mId=$mId");
              $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,"$payment_for_name paid by ".$user_name,"Paid Amount is $currency ".$transection_amount." ","paidMaintenace?mId=$mId");
                      
                      $notiAry = array(
                          'society_id'=>$society_id,
                          'notification_tittle'=>"$payment_for_name paid by ".$user_name,
                          'notification_description'=>"Paid amount is $currency $transection_amount",
                          'notifiaction_date'=>date('Y-m-d H:i'),
                          'notification_action'=>"paidMaintenace?mId=$mId",
                          'admin_click_action'=>"paidMaintenace?mId=$mId",
                           'notification_logo'=>'MyBillsNew.png',
                        );
                        
                        $d->insert("admin_notification",$notiAry);

                        $d->insert_myactivity($user_id,"$society_id","0","$user_name","$payment_for_name maintenance paid, paid amount is $currency $transection_amount","MyBillsNew.png");
                       
                        $d->delete("payment_paid_request","receive_maintenance_id='$receive_maintenance_id' AND unit_id='$unit_id' AND receive_maintenance_id!=0");

               $response["message"]="$maintenanceName Paid Successfully.!";
               $response["status"]="200";
               echo json_encode($response);
                if ($user_email!='') {
                  $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id&type=M&societyid=$society_id&rmId=$receive_maintenance_id&id=$detail_id&type_men=single";
                  $description= $payment_for_name ." $maintenanceName Payment";
                  $maintenance_name=$payment_for_name;
                  $received_amount=$transection_amount;
                  $receive_date = $receive_maintenance_date;
                  $category = "Maintenance";
                  $invoice_number= "INVMN".$detail_id;
                  $to=$user_email;
                  $subject=$payment_for_name. ' Payment Acknowledgement '.$invoice_number;
                  include '../apAdmin/mail/paymentReceipt.php';
                  include '../apAdmin/mail.php';
                }
            }else{
                 $response["message"]="Somthing wrong , please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);
            }

          } else  if($paymentTypeFor==1){
            // bill payment

         
            if ($is_wallet_applied=='true') {

              $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
              $row=mysqli_fetch_array($count6);
              $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

              $remark= "Payment for Bill $paymentForName";      

              $m->set_data('society_id',$society_id);
              $m->set_data('user_mobile',$user_mobile);
              $m->set_data('unit_id',$unit_id);
              $m->set_data('remark',$remark);
              $m->set_data('debit_amount',$wallet_balance_used);
              $m->set_data('avl_balance',$totalDebitAmount1-$wallet_balance_used);


              $aWallet = array(
                'society_id'=>$m->get_data('society_id'),
                'user_mobile'=>$m->get_data('user_mobile'),
                'unit_id'=>$m->get_data('unit_id'),
                'remark'=>$m->get_data('remark'),
                'debit_amount'=>$m->get_data('debit_amount'),
                'avl_balance'=>$m->get_data('avl_balance'),
                'created_date'=>date("Y-m-d H:i:s"), 
              );
              
               
              $wallet_amount = $wallet_balance_used;
              $wallet_amount_type=1;

              $transection_amount = $transection_amount + $wallet_balance_used;
            } else {
              $transection_amount= $paymentTransactionsAmount;
            } 

              $receive_bill_id=$paymentReceivedBillId;
              $discount_amount = $paymentDiscountAmount;
              $late_fee = $paymentLateFee;
              $payment_for_name= $paymentForName;
              $user_name= $userName;

              $ref_no="REF".date('Ymd').$unit_id.$receive_bill_id;
              $cTime =date('Y-m-d H:i');
            
            
              $bill_payment_date=date("Y-m-d H:i:s");
              $late_fees_amount  = $late_fee;
              $m->set_data('bill_payment_date',$bill_payment_date);
              $m->set_data('payment_type','2');
              $m->set_data('receive_bill_status','3');
              $m->set_data('paymentTransactionsId',$paymentTransactionsId);
              $m->set_data('received_amount',$transection_amount);
              $m->set_data('late_fees_amount',$late_fees_amount);
              $m->set_data('discount_amount',$discount_amount);
              $m->set_data('ref_no',$ref_no);
              $m->set_data('paid_by',$user_id);
              $m->set_data('wallet_amount',$wallet_amount);
              $m->set_data('wallet_amount_type',$wallet_amount_type);
              

              $a5 = array(
                       'received_amount'=>$m->get_data('received_amount'), 
                       'receive_bill_status'=>$m->get_data('receive_bill_status'),
                       'bill_payment_date'=>$m->get_data('bill_payment_date'),
                       'bill_payment_type'=>$m->get_data('payment_type'),
                       'payment_detail'=>$m->get_data('paymentTransactionsId'),
                       'received_amount'=>$m->get_data('received_amount'),
                       'late_fees_amount'=>$m->get_data('late_fees_amount'),
                       'discount_amount'=>$m->get_data('discount_amount'),
                       'transaction_charges'=>$m->get_data('transaction_charges'),
                        'ref_no'=>$m->get_data('ref_no'),
                        'paid_by'=> $m->get_data('paid_by'),
                        'wallet_amount'=> $m->get_data('wallet_amount'),
                        'wallet_amount_type'=> $m->get_data('wallet_amount_type'),
                  );


              $q=$d->update("receive_bill_master",$a5,"receive_bill_id='$receive_bill_id'");
              

              if ($q==TRUE) {

                 if ($wallet_amount>0) {
                    $wq=$d->insert("user_wallet_master",$aWallet);
                 }

                    $block_id=$d->getBlockid($user_id);
                   $fcmArray=$d->selectAdminBlockwise("1",$block_id,"android");
                   $fcmArrayIos=$d->selectAdminBlockwise("1",$block_id,"ios");
            
                        $fq=$d->select("receive_bill_master,bill_master","receive_bill_master.bill_master_id=bill_master.bill_master_id  AND receive_bill_master.receive_bill_id='$receive_bill_id'");
                            $billData=mysqli_fetch_array($fq);
                            $bill_master_id=$billData['bill_master_id'];
                            $bill_date=date('d M Y',strtotime($billData['bill_genrate_date'])).' to '. date('d M Y',strtotime($billData['bill_end_date'])); 
      
                    $nAdmin->noti_new($society_id,"",$fcmArray,"$payment_for_name bill paid by $user_name","Paid amount is $currency $transection_amount","paidBill?billId=$bill_master_id");
                    $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,"$payment_for_name bill paid by $user_name","Paid amount is $currency $transection_amount","paidBill?billId=$bill_master_id");
                
                            
                            $notiAry = array(
                                'society_id'=>$society_id,
                                'notification_tittle'=>"$payment_for_name bill paid by $user_name",
                                'notification_description'=>"Paid amount is $currency $transection_amount",
                                'notifiaction_date'=>date('Y-m-d H:i'),
                                'notification_action'=>"paidBill?billId=$bill_master_id",
                                'admin_click_action'=>"paidBill?billId=$bill_master_id",
                                'notification_logo'=>'MyBillsNew.png',
                              );
                              
                                $d->insert("admin_notification",$notiAry);
                                $d->insert_myactivity($user_id,"$society_id","0","$user_name","$payment_for_name bill paid, paid amount is $currency $transection_amount","MyBillsNew.png");
                              
                               
                                $d->delete("payment_paid_request","receive_bill_id='$receive_bill_id' AND unit_id='$unit_id' AND receive_bill_id!=0");


                   $response["message"]="Bill Paid Successfully. !";
                   $response["status"]="200";
                   echo json_encode($response);
                    if ($user_email!='') {

                        $description= $payment_for_name ." Bill Payment";
                        $maintenance_name=$payment_for_name;
                        $received_amount=$transection_amount;
                        $receive_date = $bill_payment_date;
                        $category = "Bill";
                        $invoice_number= "INVBL".$receive_bill_id;

                        $to=$user_email;
                        $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id&type=B&societyid=$society_id&id=$receive_bill_id";

                        $subject=$payment_for_name. ' Bill Payment Acknowledgement #'.$invoice_number;
                        
                        include '../apAdmin/mail/paymentReceipt.php';
                        include '../apAdmin/mail.php';
                      }
                  exit();
              }else{
                   $response["message"]="Somthing wrong please try again..!!";
                   $response["status"]="201";
                   echo json_encode($response);
              } 


          } else if($paymentTypeFor==2){
           
            // Facility Boooking

             if ($is_wallet_applied=='true') {

              $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
              $row=mysqli_fetch_array($count6);
              $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

              $remark= "Payment for Facility $paymentForName";      

              $m->set_data('society_id',$society_id);
              $m->set_data('user_mobile',$user_mobile);
              $m->set_data('unit_id',$unit_id);
              $m->set_data('remark',$remark);
              $m->set_data('debit_amount',$wallet_balance_used);
              $m->set_data('avl_balance',$totalDebitAmount1-$wallet_balance_used);


              $aWallet = array(
                'society_id'=>$m->get_data('society_id'),
                'user_mobile'=>$m->get_data('user_mobile'),
                'unit_id'=>$m->get_data('unit_id'),
                'remark'=>$m->get_data('remark'),
                'debit_amount'=>$m->get_data('debit_amount'),
                'avl_balance'=>$m->get_data('avl_balance'),
                'created_date'=>date("Y-m-d H:i:s"), 
              );
              
               
              $wallet_amount = $wallet_balance_used;
              $wallet_amount_type=1;

              $transection_amount = $transection_amount + $wallet_balance_used;
            } else {
              $transection_amount= $paymentTransactionsAmount;
            } 


            $facility_id=$facilityId;
            $cq = $d->select("facilities_master","facility_id='$facility_id'");
            $cData = mysqli_fetch_array($cq);
            $facility_type=$cData['facility_type'];
            $person_limit = $cData['person_limit'];
            $amount_type=$cData['amount_type'];
            $booking_start_time_days = $bookingStartTimeDays;
            $booking_end_time_days = $bookingEndTimeDays;
            // $transection_amount= $paymentTransactionsAmount;
            $payment_for_name= $paymentForName;
            $user_name= $userName;
            $event_id = $eventId;



            


            if($facility_type==0 || $facility_type==3 || $facility_type==4){
              $booked_date = date('Y-m-d',strtotime($facility_book_date));
              $ids = join("','",$_POST['bookingSelectedIds']); 
              $sq = $d->select("facility_schedule_master","facility_start_time!='00:00:00' AND facility_id='$facility_id' AND  facility_schedule_id IN ('$ids')","");
              while ($sData= mysqli_fetch_array($sq)) {
                
                $count5=$d->sum_data("no_of_person","facility_booking_months","cancel_status=0 AND facility_id='$facility_id'  AND facility_schedule_id='$sData[facility_schedule_id]' AND booking_start='$booked_date'");
                $row11=mysqli_fetch_array($count5);
               $booke_count=$row11['SUM(no_of_person)'];
                if ($booke_count=='') {
                  $booke_count =0;
                }
                $avSeat = $person_limit - $booke_count;
                if ($avSeat<$no_of_person && $amount_type==0 ||  $amount_type==1 && $booke_count>0) {
                    $response["message"]= "During the transaction booking full, please contact to $societyLngName admin for refund your payment.! ";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                  }

              }


              // Day Wise
              $booked_date = date('Y-m-d',strtotime($facility_book_date));
              $booking_start_time = date('H:i:s',strtotime($booking_start_time_days));
              $booking_end_time = date('H:i:s',strtotime($booking_end_time_days));
              // $effectiveDate = strtotime("+$no_of_days days", strtotime($booked_date)); // returns timestamp
              $booking_expire_date=$booked_date; // formatted version
              $facility_amount= $received_amount;
              $booking_expire_date = $booked_date;
            } else {
              $booked_date = date('Y-m-d',strtotime($facility_book_date));
              
              $noOfMonth =count($_POST['bookingSelectedIds']);
              $lastBookMonth=  $_POST['bookingSelectedIds'][$noOfMonth-1];
              $firstBookMonth=  $_POST['bookingSelectedIds'][0];
              $booked_date_check_temp = date("F-Y", strtotime($booked_date));
              $booking_expire_date = date("Y-m-t", strtotime($lastBookMonth));
              //Month Wise
              if ($firstBookMonth!=$booked_date_check_temp) {
                 $booked_date =  date("Y-m-1", strtotime($firstBookMonth));
              }
              
              $booking_start_time = date('H:i:s',strtotime($booking_start_time_days));
              $booking_end_time = date('H:i:s',strtotime($booking_end_time_days));

               if ($noOfMonth==0) {
                
                  $response["message"]= "Minimum 1 Month Required";
                  $response["status"]="201";
                  echo json_encode($response);
                  exit();
              }

              for ($i1check=0; $i1check <count($_POST['bookingSelectedIds']) ; $i1check++) { 
                $monthName = $_POST['bookingSelectedIds'][$i1check];
                $count5=$d->sum_data("no_of_person","facility_booking_months","(cancel_status=0 AND facility_id='$facility_id' AND month_name='$monthName')");
                $row11=mysqli_fetch_array($count5);
                $booke_count=$row11['SUM(no_of_person)'];
                if ($booke_count=='') {
                  $booke_count =0;
                }
                 $person_limit;
                 $avSeat = $person_limit - $booke_count;
                if ($avSeat<$no_of_person && $amount_type==0 ||  $amount_type==1 && $booke_count>0) {
                  $response["message"]= "During the transaction booking full, please contact to $societyLngName admin for refund your payment.! ";
                  $response["status"]="201";
                  echo json_encode($response);
                  exit();
                }
              }

             
            } 
          
                
              $ref_no="REF".date('Ymd').$unit_id.$facility_id;
              $payment_status = 0;



            $bookingDate=date("Y-m-d H:i:s");

          
            $uq=$d->select("unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND unit_master.block_id=block_master.block_id AND users_master.user_id='$user_id' AND unit_master.society_id='$society_id'");
            $unitData=mysqli_fetch_array($uq);
            $unitName=  $unitData['block_name']."-".$unitData['unit_name'];
            $userType=  $unitData['user_type'];
            $unit_id_main=  $unitData['unit_id'];


              $m->set_data('facility_id',$facility_id);
              $m->set_data('balancesheet_id',$balancesheet_id);
              $m->set_data('facility_amount',$transection_amount);
              $m->set_data('payment_type',"2");
              $m->set_data('payment_received_date',$bookingDate);
              $m->set_data('no_of_person',$no_of_person); 
              $m->set_data('society_id',$society_id);
              $m->set_data('unit_id',$unit_id);
              $m->set_data('unit_name',$unit_name);
              $m->set_data('book_status','1');
              $m->set_data('receive_amount',$transection_amount);
              $m->set_data('no_of_month', $noOfMonth);
              $m->set_data('user_id', $user_id);
              $m->set_data('userType', $userType);
              $m->set_data('booking_expire_date', $booking_expire_date);
              $m->set_data('payment_status', '0');
              $m->set_data('booked_date',$booked_date);
              $m->set_data('booking_start_time',$booking_start_time);
              $m->set_data('booking_end_time',$booking_end_time);
              $m->set_data('payment_type',"2");
              $m->set_data('payment_bank',$bankcode);
              $m->set_data('payment_ref_no',$paymentTransactionsId);
              $m->set_data('no_of_person',$no_of_person);
              $m->set_data('no_of_month',$noOfMonth);
              $m->set_data('user_id',$user_id);
              $m->set_data('userType',$userType);
              $m->set_data('wallet_amount',$wallet_amount);
              $m->set_data('wallet_amount_type',$wallet_amount_type);

              $a = array(
                  'facility_id' => $m->get_data('facility_id'),
                  'balancesheet_id'=> $m->get_data('balancesheet_id'),
                  'society_id' => $m->get_data('society_id'),
                  'booked_date' => $m->get_data('booked_date'),
                  'unit_id' => $m->get_data('unit_id'),
                  'receive_amount' => $m->get_data('receive_amount'),
                  'transaction_charges' => $m->get_data('transaction_charges'),
                  'unit_name' => $m->get_data('unit_name'),
                  'book_status' => $m->get_data('book_status'),
                  'book_request_date' => $m->get_data('book_request_date'),
                 'booking_start_time'=> $m->get_data('booking_start_time'),
                  'booking_end_time'=> $m->get_data('booking_end_time'),
                  'book_request_date'=> $booked_date,
                  'payment_type' => $m->get_data('payment_type'),
                  'payment_ref_no' => $m->get_data('payment_ref_no'),
                  'payment_status' => $m->get_data('payment_status'),
                  'payment_received_date' => $m->get_data('payment_received_date'),
                  'no_of_person' => $m->get_data('no_of_person'),
                  'booking_expire_date' => $m->get_data('booking_expire_date'),
                  'no_of_month' => $m->get_data('no_of_month'),
                  'user_id' => $m->get_data('user_id'),
                  'userType' => $m->get_data('userType'),
                  'paid_by' => $m->get_data('user_id'),
                  'wallet_amount'=> $m->get_data('wallet_amount'),
                  'wallet_amount_type'=> $m->get_data('wallet_amount_type'),
                );

               

            $q=$d->insert('facilitybooking_master',$a);
            $booking_id = $con->insert_id;
            if ($q==true) {

                 if ($wallet_amount>0) {
                    $wq=$d->insert("user_wallet_master",$aWallet);
                 }

              $caRy = array(
                   'common_id'=>$booking_id,
              );

              $d->update("society_transection_master",$caRy,"transection_id ='$transection_id' "); 


                  if($facility_type==1){
                    for ($i=0; $i <$noOfMonth ; $i++) { 
                      $m->set_data('month_name',$_POST['bookingSelectedIds'][$i]);

                       $aMonth= array (
                        'society_id'=> $society_id,
                        'booking_id'=> $booking_id,
                        'facility_id'=> $m->get_data('facility_id'),
                        'unit_id'=> $m->get_data('unit_id'),
                        'no_of_person'=> $m->get_data('no_of_person'),
                        'month_name'=> $m->get_data('month_name'),
                        'booking_start'=> $m->get_data('booked_date'),
                        'booking_end'=> $m->get_data('booking_expire_date'),
                      );
                       $d->insert("facility_booking_months",$aMonth);
                    }

                  } else {
                   for ($i1=0; $i1 <count($_POST['bookingSelectedIds']) ; $i1++) { 
                    $facility_schedule_id = $_POST['bookingSelectedIds'][$i1];
                    $m->set_data('facility_schedule_id',$facility_schedule_id);
                    
                    $sq1 = $d->select("facility_schedule_master","facility_schedule_id='$facility_schedule_id' ","");
                    $sData= mysqli_fetch_array($sq1);
                    $facility_start_time = $sData['facility_start_time'];
                    $facility_end_time = $sData['facility_end_time'];

                     $aMonth= array (
                      'society_id'=> $society_id,
                      'booking_id'=> $booking_id,
                      'facility_id'=> $m->get_data('facility_id'),
                      'unit_id'=> $m->get_data('unit_id'),
                      'no_of_person'=> $m->get_data('no_of_person'),
                      'month_name'=> 'Time Slot',
                      'booking_start_time'=> $facility_start_time,
                      'booking_end_time'=> $facility_end_time,
                      'facility_schedule_id'=> $m->get_data('facility_schedule_id'),
                      'booking_start'=> $m->get_data('booked_date'),
                      'booking_end'=> $m->get_data('booking_expire_date'),
                    );
                     $d->insert("facility_booking_months",$aMonth);
                  }
                }


              $facility_name = html_entity_decode($cData['facility_name']);
                $block_id=$d->getBlockid($user_id);
               $fcmArray=$d->selectAdminBlockwise("11",$block_id,"android");
               $fcmArrayIos=$d->selectAdminBlockwise("11",$block_id,"ios");

                  $nAdmin->noti_new($society_id,"",$fcmArray,"$facility_name Facility booked by $user_name","Paid amount is $currency $transection_amount ","facilityBook?id=$facility_id");
                  $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,"$facility_name Facility booked by $user_name","Paid amount is $currency $transection_amount ","facilityBook?id=$facility_id");
                          
                    $notiAry = array(
                        'society_id'=>$society_id,
                        'notification_tittle'=>"$facility_name Facility booked by $user_name",
                        'notification_description'=>"Paid amount is $currency $transection_amount",
                        'notifiaction_date'=>date('Y-m-d H:i'),
                        'notification_action'=>"facilityBook?id=$facility_id",
                        'admin_click_action'=>"facilityBook?id=$facility_id",
                        'notification_logo'=>'Facilitiesxxxhdpi.png',
                      );
                      $d->insert("admin_notification",$notiAry);

                   $d->insert_myactivity($user_id,"$society_id","0","$user_name","$facility_name Facility booked, paid amount $transection_amount","Facilitiesxxxhdpi.png");
                          
                           
                 

                $response["message"]="Facility Booked Successfully !";
                $response["status"]="200";
                echo json_encode($response);
                if ($user_email!='') {
                  $invoice_number= "INVFAC".$booking_id;
                  $ref_no="REF".date('Ymd').$unit_id.$facility_id;
                  $facility_name=$payment_for_name;
                  $received_amount = $transection_amount;
                  $to=$user_email;
                  $subject=$facility_name. ' Facility Booking Payment Acknowledgement #'.$invoice_number;
                  if ($facility_type==0) {
                    $description=$facility_name."  Booking Date : $booked_date";
                  }else if ($facility_type==1) {
                    $description=$facility_name." Booking Date : $booked_date to $booking_expire_date for $no_of_person Person";
                  }else if ($facility_type==3) {
                    $description=$facility_name." (Booking Date : $booked_date Time: ". date('h:i A',strtotime($booking_start_time)).' - '. date('h:i A',strtotime($booking_end_time)).')';
                  } else {
                    $description=$facility_name." Booking Date : $booked_date ";
                  }
                  $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id_main&type=Fac&societyid=$society_id&id=$booking_id&facility_id=$facility_id";

                  $receive_date = $bookingDate;
                  $category = "Facility";
                 

                  include '../apAdmin/mail/paymentReceipt.php';
                  include '../apAdmin/mail.php';
                }

            }else{

                $response["message"]="Sorry, Some Error is occure Please Try Again..!!";
                $response["status"]="201";
                echo json_encode($response);
      
            }

          }  else if($paymentTypeFor==3){

            // Event Booking
            if ($is_wallet_applied=='true') {

              $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
              $row=mysqli_fetch_array($count6);
              $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

              $remark= "Payment for Event $paymentForName";      

              $m->set_data('society_id',$society_id);
              $m->set_data('user_mobile',$user_mobile);
              $m->set_data('unit_id',$unit_id);
              $m->set_data('remark',$remark);
              $m->set_data('debit_amount',$wallet_balance_used);
              $m->set_data('avl_balance',$totalDebitAmount1-$wallet_balance_used);


              $aWallet = array(
                'society_id'=>$m->get_data('society_id'),
                'user_mobile'=>$m->get_data('user_mobile'),
                'unit_id'=>$m->get_data('unit_id'),
                'remark'=>$m->get_data('remark'),
                'debit_amount'=>$m->get_data('debit_amount'),
                'avl_balance'=>$m->get_data('avl_balance'),
                'created_date'=>date("Y-m-d H:i:s"), 
              );
              
               
              $wallet_amount = $wallet_balance_used;
              $wallet_amount_type=1;

              $transection_amount = $transection_amount + $wallet_balance_used;
            } else {
              $transection_amount= $paymentTransactionsAmount;
            } 

            $events_day_id=$eventDayId;
            // $transection_amount= $paymentTransactionsAmount;
            $payment_for_name= $paymentForName;
            $user_name= $userName;
            $event_id = $eventId;

           

            $no_of_personAry = explode("~",$no_of_person);

            $qmaint = $d->select("event_days_master","events_day_id ='$events_day_id'");
            $maintData=mysqli_fetch_array($qmaint);
            $maxiAdult=$maintData['maximum_pass_adult'];
            $maxiChild=$maintData['maximum_pass_children'];
            $maxiGuest=$maintData['maximum_pass_guests'];

            $q111 = $d->sum_data("going_person","event_attend_list","book_status=1 AND events_day_id ='$events_day_id'");
            $adultData = mysqli_fetch_array($q111);
            $q222 = $d->sum_data("going_child","event_attend_list","book_status=1 AND events_day_id ='$events_day_id' ");
            $childData  = mysqli_fetch_array($q222);
            $q333 = $d->sum_data("going_guest","event_attend_list","book_status=1 AND events_day_id ='$events_day_id'  ");
            $guestData  = mysqli_fetch_array($q333);
            $totalBookedAdult=$adultData['SUM(going_person)']+$no_of_personAry[0];
            $totalBookedChild=$childData['SUM(going_child)']+$no_of_personAry[1];
            $totalBookedGuest=$guestData['SUM(going_guest)']+$no_of_personAry[2];

            if ($maxiAdult<$totalBookedAdult ||  $maxiChild<$totalBookedChild && $no_of_personAry[1]!=0 || $maxiGuest<$totalBookedGuest && $no_of_personAry[2]!=0) {
                  $response["message"]="During the transaction booking full, please contact to $societyLngName admin for refund your payment.! ";
                  $response["status"]="201";
                  echo json_encode($response);
                exit();
            }

            $evq=$d->select("event_days_master","events_day_id = '$events_day_id'");
            $eventData=mysqli_fetch_array($evq);
            $event_name=$eventData['event_day_name'];
            $adultPrice=$eventData['adult_charge'];
            $childPrice=$eventData['child_charge'];
            $guestPrice=$eventData['guest_charge'];

            // find user type

           
            $fq=$d->select("users_master","unit_id='$unit_id' AND user_id='$user_id'");
            $ownerData=mysqli_fetch_array($fq);
            $userType=$ownerData['user_type'];
            // 3 for book event
            $payment_received_date=date("Y-m-d H:i:s");

            
            $ref_no="REF".date('Ymd').$unit_id.$event_id;
              
              $m->set_data('event_id',$event_id);
              $m->set_data('events_day_id',$events_day_id);
              $m->set_data('balancesheet_id',$balancesheet_id);
              $m->set_data('society_id',$society_id);
              $m->set_data('user_id',$user_id);
              $m->set_data('userType',$userType);
              $m->set_data('unit_id',$unit_id);
              $m->set_data('going_person',$no_of_personAry[0]); 
              $m->set_data('notes',$paymentDescription); 
              $m->set_data('going_child',$no_of_personAry[1]); 
              $m->set_data('going_guest',$no_of_personAry[2]); 
              $m->set_data('recived_amount',$transection_amount);
              $m->set_data('payment_received_date',$payment_received_date);
              $m->set_data('ref_no',$ref_no);
              $m->set_data('payment_type',2);
              $m->set_data('bank_name',$bankcode);
              $m->set_data('payment_ref_no',$paymentTransactionsId);
             
              $m->set_data('wallet_amount',$wallet_amount);
              $m->set_data('wallet_amount_type',$wallet_amount_type);
           
              $a = array(
                  'event_id' => $m->get_data('event_id'),
                  'balancesheet_id'=> $m->get_data('balancesheet_id'),
                  'events_day_id'=> $m->get_data('events_day_id'),
                  'society_id' => $m->get_data('society_id'),
                  'user_id' => $m->get_data('user_id'),
                  'userType' => $m->get_data('userType'),
                  'unit_id' => $m->get_data('unit_id'),
                  'going_person' => $m->get_data('going_person'),
                  'notes' => $m->get_data('notes'),
                  'going_child' => $m->get_data('going_child'),
                  'going_guest' => $m->get_data('going_guest'),
                  'recived_amount' => $m->get_data('recived_amount'),
                  'transaction_charges' => $m->get_data('transaction_charges'),
                  'payment_received_date' => $m->get_data('payment_received_date'),
                  'ref_no' => $m->get_data('ref_no'),
                  'payment_type' => $m->get_data('payment_type'),
                  'bank_name' => $m->get_data('bank_name'),
                  'payment_ref_no' => $m->get_data('payment_ref_no'),
                  'book_status'=>1,
                  'paid_by' => $m->get_data('user_id'),
                  'wallet_amount'=> $m->get_data('wallet_amount'),
                  'wallet_amount_type'=> $m->get_data('wallet_amount_type'),
              );

            
              $q=$d->insert('event_attend_list',$a);
              $event_attend_id = $con->insert_id;
              // add in event passes master
              $m->set_data('event_attend_id',$event_attend_id);

              $caRy = array(
                   'common_id'=>$event_attend_id,
              );

              $d->update("society_transection_master",$caRy,"transection_id ='$transection_id' "); 


              for ($i5=0; $i5 <$no_of_personAry[0] ; $i5++) { 
              
              $pass_no= "TKT".$user_id;
                $adultAry = array(
                  'event_id' => $m->get_data('event_id'),
                  'event_attend_id' => $m->get_data('event_attend_id'),
                  'events_day_id' => $m->get_data('events_day_id'),
                  'society_id' => $m->get_data('society_id'),
                  'user_id' => $m->get_data('user_id'),
                  'unit_id' =>  $unit_id,
                  'userType' =>  $userType,
                  'pass_no' =>  $pass_no,
                  'pass_amount' =>  $adultPrice,
                  'pass_type' =>  0,
                );
                $d->insert('event_passes_master',$adultAry);
              }

              for ($i1=0; $i1 <$no_of_personAry[1] ; $i1++) { 
                
              $pass_no= "TKT".$user_id;
                $childAry = array(
                  'event_id' => $m->get_data('event_id'),
                  'event_attend_id' => $m->get_data('event_attend_id'),
                   'events_day_id' => $m->get_data('events_day_id'),
                  'society_id' => $m->get_data('society_id'),
                  'user_id' => $m->get_data('user_id'),
                  'unit_id' =>  $unit_id,
                  'userType' =>  $userType,
                  'pass_no' =>  $pass_no,
                  'pass_amount' =>  $childPrice,
                  'pass_type' =>  1,

                );
                $d->insert('event_passes_master',$childAry);
              }

              for ($i2=0; $i2 <$no_of_personAry[2] ; $i2++) { 
                
              $pass_no= "TKT".$user_id;
                $guestAry = array(
                  'event_id' => $m->get_data('event_id'),
                  'event_attend_id' => $m->get_data('event_attend_id'),
                   'events_day_id' => $m->get_data('events_day_id'),
                  'society_id' => $m->get_data('society_id'),
                  'user_id' => $m->get_data('user_id'),
                  'unit_id' =>  $unit_id,
                  'userType' =>  $userType,
                  'pass_no' =>  $pass_no,
                  'pass_amount' =>  $guestPrice,
                  'pass_type' =>  2,
                );
                $d->insert('event_passes_master',$guestAry);
              }


            if ($q==true) {

                if ($wallet_amount>0) {
                    $wq=$d->insert("user_wallet_master",$aWallet);
                 }

                 $qb=$d->select("event_master,event_days_master","event_master.event_id=$event_id AND event_master.event_id=event_days_master.event_id AND  event_days_master.events_day_id='$events_day_id'");
                $bData=mysqli_fetch_array($qb);
                $eventDayName= $bData['event_day_name'];
                $eventBookDate= $bData['event_date'];



                 $block_id=$d->getBlockid($user_id);
                 $fcmArray=$d->selectAdminBlockwise("10",$block_id,"android");
                 $fcmArrayIos=$d->selectAdminBlockwise("10",$block_id,"ios");


                  $nAdmin->noti_new($society_id,"",$fcmArray,"$event_name-$eventDayName Event Booked by $user_name","Paid amount is $currency $transection_amount","viewEvents?id=$event_id");
                  $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,"$event_name-$eventDayName Event Booked by $user_name","Paid amount is $currency $transection_amount","viewEvents?id=$event_id");
                          
                  $notiAry = array(
                      'society_id'=>$society_id,
                      'notification_tittle'=>"$event_name-$eventDayName Event Booked by $user_name",
                      'notification_description'=>"Paid amount is $currency $transection_amount",
                      'notifiaction_date'=>date('Y-m-d H:i'),
                      'notification_action'=>"viewEvents?id=$event_id",
                      'admin_click_action'=>"viewEvents?id=$event_id",
                       'notification_logo'=>'Events_1xxxhdpi.png',
                    );
                            
                    $d->insert("admin_notification",$notiAry);
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","$event_name event booked, paid amount is $currency $transection_amount","Events_1xxxhdpi.png");
                        

                 

                $response["message"]="Event has been booked successfully";
                $response["status"]="200";
                echo json_encode($response);
                if ($user_email!='') {
                  $user_first_name= $user_name;
                  $to=$user_email;
                  $received_amount=$transection_amount;
                  $description= $event_name.' '.$eventDayName .' Booking for Date: '. $eventBookDate;
                  $subject=$event_name. ' Event Payment Acknowledgement '.$ref_no;
                  $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id&type=E&societyid=$society_id&id=$event_id&event_attend_id=$event_attend_id";

                  $receive_date = $payment_received_date;
                  $category = "Event";
                  $invoice_number= "INVEV".$event_attend_id;

                  $subject=$event_name. 'Event Payment Acknowledgement '.$invoice_number;
                  $q=$d->select("society_master","society_id='$society_id'");
                  $bData=mysqli_fetch_array($q);
                  include '../apAdmin/mail/paymentReceipt.php';
                  include '../apAdmin/mail.php';
                }
                exit();
            }else{

                $response["message"]="Sorry, Some Error is occure Please Try Again..!!";
                $response["status"]="201";
                echo json_encode($response);
      
            }

      }else  if($paymentTypeFor==4){


         
          // Penaly Payment 
          if ($is_wallet_applied=='true') {

            $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0 AND user_mobile='$user_mobile'");
            $row=mysqli_fetch_array($count6);
            $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];

            $remark= "Payment for Panalty $paymentForName";      

            $m->set_data('society_id',$society_id);
            $m->set_data('user_mobile',$user_mobile);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('remark',$remark);
            $m->set_data('debit_amount',$wallet_balance_used);
            $m->set_data('avl_balance',$totalDebitAmount1-$wallet_balance_used);


            $aWallet = array(
              'society_id'=>$m->get_data('society_id'),
              'user_mobile'=>$m->get_data('user_mobile'),
              'unit_id'=>$m->get_data('unit_id'),
              'remark'=>$m->get_data('remark'),
              'debit_amount'=>$m->get_data('debit_amount'),
              'avl_balance'=>$m->get_data('avl_balance'),
              'created_date'=>date("Y-m-d H:i:s"), 
            );
            

            $wallet_amount = $wallet_balance_used;
            $wallet_amount_type=1;

            $transection_amount = $transection_amount + $wallet_balance_used;
          } 

          $d1=date("Y-m-d H:i:s");
          $d2=date("Y-m-d");
          $penalty_id = $penaltyId;
          $ref_no="REF".date('Ymd').$unit_id.$penalty_id;
          
          $expDateAry = explode("-", $d1);
          $exYear = $expDateAry[0];
          $exMonth = $expDateAry[1];

          $dateObj   = DateTime::createFromFormat('!m', $exMonth);
          $monthName = $dateObj->format('F'); 
          $expenses_created_date = $monthName.'-'.$exYear;


          $penalty_receive_date= date("Y-m-d H:i:s");

          $a5 = array(
              'paid_status'=>1,
              'receive_amount'=>$transection_amount,
              'transaction_charges'=>$transaction_charges,
              'penalty_payment_type'=>2,
              'payment_ref_no'=>$paymentTransactionsId,
              'penalty_receive_date'=>$penalty_receive_date,
              'wallet_amount'=>$wallet_balance_used,
              'wallet_amount_type'=>$wallet_amount_type,
          );

          $q =$d->update("penalty_master",$a5,"penalty_id='$penalty_id'");

          if ($q==true) {

               if ($wallet_amount>0) {
                  $wq=$d->insert("user_wallet_master",$aWallet);
                }


               $block_id=$d->getBlockid($user_id);
               $fcmArray=$d->selectAdminBlockwise("12",$block_id,"android");
               $fcmArrayIos=$d->selectAdminBlockwise("12",$block_id,"ios");

                $nAdmin->noti_new($society_id,"",$fcmArray,"Penalty amount paid by $payment_firstname ($unit_name)","Paid amount is $currency $transection_amount",'penalties');
                $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,"Penalty amount paid by $payment_firstname ($unit_name)","Paid amount is $currency $transection_amount",'penalties');
                        
                $notiAry = array(
                    'society_id'=>$society_id,
                    'notification_tittle'=>"Penalty amount paid by $payment_firstname ($unit_name)",
                    'notification_description'=>"Paid amount is $currency $transection_amount",
                    'notifiaction_date'=>date('Y-m-d H:i'),
                    'notification_action'=>'penalties?type=1',
                    'admin_click_action'=>'penalties?type=1',
                    'notification_logo'=>'penaltyNew.png',
                  );
                          
                  $d->insert("admin_notification",$notiAry);

                  $d->insert_myactivity($user_id,"$society_id","0","$user_name","Penalty $paymentForName paid, paid amount is $currency $transection_amount","penaltyNew.png");
              
                $d->delete("payment_paid_request","penalty_id='$penalty_id' AND unit_id='$unit_id' AND penalty_id!=0");


              $response["message"]="Penalty Paid Successfully";
              $response["status"]="200";
              echo json_encode($response);
              if ($userEmail!='') {
                  $get_det = $d->selectRow("block_name,floor_name","users_master JOIN block_master ON block_master.block_id = users_master.block_id JOIN floors_master ON floors_master.floor_id = users_master.floor_id","users_master.user_id = '$user_id'");
                  $blo_flo = $get_det->fetch_assoc();
                  $unit_name = $blo_flo['floor_name'] . " (" . $blo_flo['block_name'] . ")";
                  $penalty_id=$penaltyId;
                  $to=$userEmail;
                  $user_name= $payment_firstname;
                  $received_amount=$transection_amount;
                  $invoice_number= "INVPN".$penalty_id;
                  $description= 'Penalty Payment ';
                  $subject='Penalty Payment Acknowledgement '.$invoice_number;
                  $invoiceUrl= $base_url."apAdmin/invoice.php?user_id=$user_id&unit_id=$unit_id&type=P&societyid=$society_id&id=$penalty_id";
                  
                  $receive_date = $penalty_receive_date;
                  $category = "Penalty";

                include '../apAdmin/mail/paymentReceipt.php';
                include '../apAdmin/mail.php';
              }
              exit();
          }else{

              $response["message"]="Sorry, Some Error is occure Please Try Again..!!";
              $response["status"]="201";
              echo json_encode($response);
    
          }

      } else {
              $response["message"]="Invalid Request Please Try Again..!!";
              $response["status"]="201";
              echo json_encode($response);
    
      }


    } else if ($_POST['getMerchantDetailsNew']=="getMerchantDetailsNew"){


        $gs=$d->select("balancesheet_master","balancesheet_id='$balancesheet_id' ");
        $row=mysqli_fetch_array($gs);

        if (mysqli_num_rows($gs)==0) {
          $response["message"]="No Balacesheet Found";
          $response["status"]="201";
          echo json_encode($response);
          exit();
        } else if ($row['society_payment_getway_id']=='' && $row['society_payment_getway_id_upi']=='') {
          $response["message"]="Online payment cannot be accepted at this moment";
          $response["status"]="201";
          echo json_encode($response);
          exit();
        } else if ($transaction_amount=='' || $transaction_amount==0){
          $response["message"]="Transaction amount missing";
          $response["status"]="201";
          echo json_encode($response);
          exit();
        }

        $society_payment_getway_id= $row['society_payment_getway_id'];
        $society_payment_getway_id_upi= $row['society_payment_getway_id_upi'];
        $paymenGatwyaAray = explode(",", $society_payment_getway_id);
        $ids = join("','",$paymenGatwyaAray);    

         $q3=$d->select("society_payment_getway,payment_getway_master"," society_payment_getway.is_upi=0 AND payment_getway_master.payment_getway_master_id=society_payment_getway.payment_getway_master_id AND society_payment_getway.status=0 AND society_payment_getway.society_payment_getway_id IN ('$ids')");

         $qUpi=$d->select("society_payment_getway,payment_getway_master"," society_payment_getway.is_upi=1 AND payment_getway_master.payment_getway_master_id=society_payment_getway.payment_getway_master_id AND society_payment_getway.status=0 AND society_payment_getway.society_payment_getway_id='$society_payment_getway_id_upi'");
          if(mysqli_num_rows($qUpi)>0){
            $upi_data=mysqli_fetch_array($qUpi);
            $response["upi_id"]=$upi_data['merchant_id'];
            $response["merchant_name"]=$upi_data['merchant_key'];
            $response["remark"]="No transaction charges apply";      
            $response["upi_enabled"]=true;
            $response["upi_name"]="Pay With UPI";
            $response["upi_logo"]=$base_url.'img/upi.png';
            $upi_enabled = 'true';

          } else {
             $response["upi_id"]="";
            $response["merchant_name"]="";
            $response["remark"]="";      
            $response["upi_enabled"]=false;
            $response["upi_name"]="";
            $response["upi_logo"]='';
            $upi_enabled = 'false';

          }

          // get wallet deatils
          $checS=$d->selectRow("virtual_wallet","society_master","society_id='$society_id'");
          $sData=mysqli_fetch_array($checS);
          $virtual_wallet= $sData['virtual_wallet'];

          if ($virtual_wallet==0) {
            $count5=$d->sum_data("credit_amount-debit_amount","user_wallet_master","user_mobile='$user_mobile' AND active_status=0");
            $row11=mysqli_fetch_array($count5);
            $totalDebitAmount=$row11['SUM(credit_amount-debit_amount)'];
            if ($totalDebitAmount>1) {
              $user_wallet_balance = number_format($totalDebitAmount,2,'.','');
              $response["wallet_balance"] = $user_wallet_balance;
              $response["is_wallet_applied"] = true;
              $response["transaction_amount"] = $transaction_amount;
              if ($transaction_amount<=$user_wallet_balance) {
                $tempWb= $user_wallet_balance-$transaction_amount;
                $response["wallet_balance_used"] = number_format($user_wallet_balance-$tempWb,2,'.',''); 
                $response["transaction_amount_using_wallet"] = '0.00';
              } else {
                $response["wallet_balance_used"] = number_format($user_wallet_balance,2,'.',''); 
                $response["transaction_amount_using_wallet"] = number_format($transaction_amount-$user_wallet_balance,2,'.','');;
              }
            } else {
              $response["wallet_balance"] = '0.00';
              $response["wallet_balance_used"] = '0.00';
              $response["is_wallet_applied"] = false;
              $response["transaction_amount"] = $transaction_amount;
              $response["transaction_amount_using_wallet"] = $transaction_amount;
            }

          } else {
            $response["wallet_balance"] = '0.00';
            $response["wallet_balance_used"] = '0.00';
            $response["is_wallet_applied"] = false;
            $response["transaction_amount"] = $transaction_amount;
            $response["transaction_amount_using_wallet"] = $transaction_amount;

          }

          if(mysqli_num_rows($q3)>0){
            $response["payments_gateways"] = array();

              while($data=mysqli_fetch_array($q3)) {
                // print_r($data);
                $payments_gateways=array();
                $payments_gateways["payment_getway_name"]=$data['payment_getway_name'];
                $payments_gateways["payment_getway_logo"]=$base_url.'img/'.$data['payment_getway_logo'];
                $payments_gateways["merchant_id"]=$data['merchant_id'];
                $payments_gateways["merchant_key"]=$data['merchant_key'];
                $payments_gateways["salt_key"]=$data['salt_key'];          
                $payments_gateways["currency"]=$data['getway_currency'];   
                
                if ($data['is_test_mode']==0) {
                  $payments_gateways["is_test_mode"]=false;
                } else {
                  $payments_gateways["is_test_mode"]=true;
                }

                if ($data['transaction_charges']>0 && $data['transaction_charges']!='') {
                  $extraCharge =$transaction_amount * $data['transaction_charges'] /100;
                  $newAmount = $extraCharge + $transaction_amount;
                  $payments_gateways["transaction_amount"]=number_format($newAmount,2,'.','');          
                  $payments_gateways["transaction_charges"]=number_format($extraCharge,2,'.','');          
                  $payments_gateways["remark"]="Transaction charges $data[transaction_charges] % apply*";  
                  if ($transaction_amount<=$user_wallet_balance) {
                    $tempWbPg= $user_wallet_balance-$transaction_amount;
                    $payments_gateways["wallet_balance_used"] = number_format($user_wallet_balance-$tempWbPg,2,'.','');  
                    $payments_gateways["transaction_amount_using_wallet"] = '0.00';
                    $payments_gateways["transaction_charges_using_wallet"]= '0.00';
                  } else {
                    $newWaTemp = number_format($transaction_amount-$user_wallet_balance,2,'.','');
                    $extraChargeWallet =$newWaTemp * $data['transaction_charges'] /100;
                    $payments_gateways["wallet_balance_used"] = $user_wallet_balance; 
                    $payments_gateways["transaction_amount_using_wallet"] = number_format($newWaTemp+$extraChargeWallet,2,'.','');  
                    $payments_gateways["transaction_charges_using_wallet"]=number_format($extraChargeWallet,2,'.','');  
                  }        
                } else {
                  $payments_gateways["transaction_amount"]=$transaction_amount;          
                  $payments_gateways["transaction_charges"]="0";          
                  $payments_gateways["remark"]="";          
                  if ($transaction_amount<=$user_wallet_balance) {
                    $tempWbPg= $user_wallet_balance-$transaction_amount;
                    $payments_gateways["wallet_balance_used"] = number_format($user_wallet_balance-$tempWbPg,2,'.','');  
                    $payments_gateways["transaction_amount_using_wallet"] = '0.00';
                  } else {
                    $payments_gateways["wallet_balance_used"] = $user_wallet_balance; 
                    $payments_gateways["transaction_amount_using_wallet"] = number_format($transaction_amount-$user_wallet_balance,2,'.','');;
                  }
                  $payments_gateways["transaction_charges_using_wallet"] = '0.00';
                }

                array_push($response["payments_gateways"], $payments_gateways); 
              }

            $response["message"]="Merchnat Details Get successfully..";
            $response["status"]="200";
            echo json_encode($response);

          }else if($upi_enabled=='true'){
            $response["message"]="Upi Details Get successfully..";
            $response["status"]="200";
            echo json_encode($response);
          }else{
            $response["message"]="Online payment cannot be accepted at this moment";
            $response["status"]="201";
            echo json_encode($response);
          }

    }else{
      $response["message"]="wrong tag";
      $response["status"]="201";
      echo json_encode($response);
    }
  }
    else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
