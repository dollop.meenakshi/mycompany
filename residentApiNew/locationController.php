<?php
include_once 'lib.php';

if (isset($_POST)){
    extract(array_map("test_input", $_POST));
    $response = array();

    if (isset($_POST['user_location'])) {

        extract(array_map("test_input", $_POST));
        $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");
        $uploadedFile = $_FILES['visit_photo']['tmp_name'];
        $ext = pathinfo($_FILES['visit_photo']['name'], PATHINFO_EXTENSION);
        if(!empty($_FILES['visit_photo']['name'])) {
            if(in_array($ext,$extension)) {
                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand();
                $dirPath = "../img/visit_photo/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth>400) {
                    $newWidthPercentage= 400*100 / $imageWidth;  //for maximum 400 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage /100;
                    $newImageHeight = $imageHeight * $newWidthPercentage /100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }



                switch ($imageType) {

                case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagepng($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;           

                case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagejpeg($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;

                case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile); 
                $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                imagegif($tmp,$dirPath. $newFileName. "_user.". $ext);
                break;

                default:
                $_SESSION['msg1']="Invalid Image";
                header("Location: ../profile");
                exit;
                break;
                }
                $visit_photo= $newFileName."_user.".$ext;
            } else {
                $_SESSION['msg1']="Invalid Photo";
                header("location:../profile");
                exit();
            }
        } else{
            $visit_photo = '';
        }

        $date = date('Y-m-d');
        $checkLogin = $d->selectArray("user_track_master","user_id='$user_id' AND DATE(user_track_date)='$date'","ORDER BY user_track_id DESC");

        if (empty($checkLogin)) {
            $distance = 'NA';
        } else{
            $url  = "https://maps.googleapis.com/maps/api/distancematrix/json";
            $url .= "?origins=".$checkLogin['user_lat'].",".$checkLogin['user_long'];
            $url .= "&destinations=".$user_lat.",".$user_long;
            $url .= "&mode=driving";
            $url .= "&departure_time=" . strtotime(date('Y-m-d H:i:s'));
            $url .= "&traffic_model=best_guess";
            $url .= "&language=en";
            $url .= "&key=AIzaSyDxtKVjfNgaStr4s-hc6Ciwa-jJ2VlXdh4";
            $routes=json_decode(file_get_contents($url),true);
            $distance = $routes['rows'][0]['elements'][0]['distance']['value'];
        }

        $m->set_data('user_id', $user_id);
        $m->set_data('address', $address);
        $m->set_data('area', $area);
        $m->set_data('locality', $locality);
        $m->set_data('user_lat', $user_lat);
        $m->set_data('user_long', $user_long);
        $m->set_data('remark', $remark);
        $m->set_data('visit_photo', $visit_photo);
        $m->set_data('user_track_date', date('Y-m-d H:i:s'));
        $m->set_data('distance_from_previous_entry', $distance);

        $a = array(
            'user_id' => $m->get_data('user_id'),
            'address' => $m->get_data('address'),
            'area' => $m->get_data('area'),
            'locality' => $m->get_data('locality'),
            'user_lat' => $m->get_data('user_lat'),
            'user_long' => $m->get_data('user_long'),
            'remark' => $m->get_data('remark'),
            'visit_photo' => $m->get_data('visit_photo'),
            'user_track_date' => $m->get_data('user_track_date'),
            'distance_from_previous_entry' => $m->get_data('distance_from_previous_entry'),
        );

        $q = $d->insert("user_track_master", $a);

        if ($q == TRUE) {

            $response["success"] = 200;
            $response["message"] = "Location Updated";

            echo json_encode($response);
        } else {
            $response["success"] = 201;
            $response["message"] = "Location Not Updated. Try Again";
            echo json_encode($response);
        }
    }

    if (isset($_POST['in_time'])) {
        if ($user_lat==0 || $user_lat=='' || $user_long==0 || $user_long=='') {
            $response["success"] = 201;
            $response["message"] = "Invalid Location Details";
            echo json_encode($response);
        } else{
            $date = date('Y-m-d');
            $checkLogin = $d->selectArray("attendance_master","user_id='$user_id' AND attendance_date='$date'");

            extract(array_map("test_input", $_POST));

            $m->set_data('user_id', $user_id);
            $m->set_data('address', $address);
            $m->set_data('area', $area);
            $m->set_data('locality', $locality);
            $m->set_data('user_lat', $user_lat);
            $m->set_data('user_long', $user_long);
            $m->set_data('user_track_date', date('Y-m-d H:i:s'));

            $a = array(
                'user_id' => $m->get_data('user_id'),
                'address' => $m->get_data('address'),
                'area' => $m->get_data('area'),
                'locality' => $m->get_data('locality'),
                'user_lat' => $m->get_data('user_lat'),
                'user_long' => $m->get_data('user_long'),
                'user_track_date' => $m->get_data('user_track_date'),
                'distance_from_previous_entry' => 'NA',
            );

            if ($out_side==1) {
                $approve = 1;
            } else{
                $approve = 0;
            }

            $q = $d->insert("user_track_master", $a);
            $in_time = date('H:i:s');
            $m->set_data('user_id', $user_id);
            $m->set_data('attendance_date', $date);
            $m->set_data('in_time', $in_time);
            $m->set_data('in_lat', $user_lat);
            $m->set_data('in_long', $user_long);
            $m->set_data('in_remark', $in_remark);
            $m->set_data('punch_type', $out_side);
            $m->set_data('approve', $approve);

            $a = array(
                'user_id' => $m->get_data('user_id'),
                'attendance_date' => $m->get_data('attendance_date'),
                'in_time' => $m->get_data('in_time'),
                'in_lat' => $m->get_data('in_lat'),
                'in_long' => $m->get_data('in_long'),
                'in_remark' => $m->get_data('in_remark'),
                'out_time' => "NULL",
                'out_lat' => "NULL",
                'out_long' => "NULL",
                'out_remark' => "",
                'punch_type' => $m->get_data('punch_type'),
                'approve' => $m->get_data('approve'),
            );

            if (empty($checkLogin)) {
                $q = $d->insert("attendance_master", $a);
            } else{
                $q = $d->update("attendance_master", $a,"attendance_date='$date' AND user_id='$user_id'");
            }

            $dataUser = $d->selectArray("users_master","user_id='$user_id'");
            $user_name = $dataUser['user_name'];
            $d->insert_log($user_id,"$user_name punched in at $in_time");

            if ($out_side==1) {
                $d->insert_log($user_id,"$user_name punched in from outside of Geo Fence");
            }

            $response["duration_sec"] = 0;

            if ($q == TRUE) {
                $response["success"] = 200;
                $response["message"] = "In time Added";

                echo json_encode($response);
            } else {
                $response["success"] = 201;
                $response["message"] = "Something Wrong. Try Again";
                echo json_encode($response);
            }
        }
    }

    if (isset($_POST['out_time'])) {
        if ($user_lat==0 || $user_lat=='' || $user_long==0 || $user_long=='') {
            $response["success"] = 201;
            $response["message"] = "Invalid Location Details";
            echo json_encode($response);
        } else{
            extract(array_map("test_input", $_POST));
            $attendance_date = date('Y-m-d');
            $getDate = $d->selectArray("attendance_master","attendance_date='$attendance_date' AND user_id ='$user_id'");
            if (!empty($getDate)) {
                $url  = "https://maps.googleapis.com/maps/api/distancematrix/json";
                $url .= "?origins=".$getDate['in_lat'].",".$getDate['in_long'];
                $url .= "&destinations=".$user_lat.",".$user_long;
                $url .= "&mode=driving";
                $url .= "&departure_time=" . strtotime(date('Y-m-d H:i:s'));
                $url .= "&traffic_model=best_guess";
                $url .= "&language=en";
                $url .= "&key=AIzaSyDxtKVjfNgaStr4s-hc6Ciwa-jJ2VlXdh4";
                $routes=json_decode(file_get_contents($url),true);
                $distance = $routes['rows'][0]['elements'][0]['distance']['value'];

                $date = date('Y-m-d');
                $checkLogin = $d->selectArray("user_track_master","user_id='$user_id' AND DATE(user_track_date)='$date'","ORDER BY user_track_id DESC");

                $url  = "https://maps.googleapis.com/maps/api/distancematrix/json";
                $url .= "?origins=".$checkLogin['user_lat'].",".$checkLogin['user_long'];
                $url .= "&destinations=".$user_lat.",".$user_long;
                $url .= "&mode=driving";
                $url .= "&departure_time=" . strtotime(date('Y-m-d H:i:s'));
                $url .= "&traffic_model=best_guess";
                $url .= "&language=en";
                $url .= "&key=AIzaSyDxtKVjfNgaStr4s-hc6Ciwa-jJ2VlXdh4";
                $routes=json_decode(file_get_contents($url),true);
                $distance_track = $routes['rows'][0]['elements'][0]['distance']['value'];


                $m->set_data('user_id', $user_id);
                $m->set_data('address', $address);
                $m->set_data('area', $area);
                $m->set_data('locality', $locality);
                $m->set_data('user_lat', $user_lat);
                $m->set_data('user_long', $user_long);
                $m->set_data('user_track_date', date('Y-m-d H:i:s'));
                $m->set_data('distance_from_previous_entry', $distance_track);

                $a = array(
                    'user_id' => $m->get_data('user_id'),
                    'address' => $m->get_data('address'),
                    'area' => $m->get_data('area'),
                    'locality' => $m->get_data('locality'),
                    'user_lat' => $m->get_data('user_lat'),
                    'user_long' => $m->get_data('user_long'),
                    'user_track_date' => $m->get_data('user_track_date'),
                    'distance_from_previous_entry' => $m->get_data('distance_from_previous_entry'),
                );

                $q = $d->insert("user_track_master", $a);

                $out_time = date('H:i:s');

                $m->set_data('user_id', $user_id);
                $m->set_data('attendance_date', date('Y-m-d'));
                $m->set_data('out_time', $out_time);
                $m->set_data('out_lat', $user_lat);
                $m->set_data('out_long', $user_long);
                $m->set_data('out_remark', $out_remark);
                $m->set_data('total_distance', $distance);

                $a = array(
                    'user_id' => $m->get_data('user_id'),
                    'attendance_date' => $m->get_data('attendance_date'),
                    'out_time' => $m->get_data('out_time'),
                    'out_lat' => $m->get_data('out_lat'),
                    'out_long' => $m->get_data('out_long'),
                    'total_distance' => $m->get_data('total_distance'),
                );
                $q = $d->update("attendance_master", $a,"attendance_date='$attendance_date' AND user_id='$user_id'");

                $dataUser = $d->selectArray("users_master","user_id='$user_id'");
                $user_name = $dataUser['user_name'];
                $d->insert_log($user_id,"$user_name punched out at $out_time");
                if ($q == TRUE) {

                    $response["success"] = 200;
                    $response["message"] = "Out time Added";

                    echo json_encode($response);
                } else {
                    $response["success"] = 201;
                    $response["message"] = "Something Wrong. Try Again";
                    echo json_encode($response);
                }
            } else{
                $response["success"] = 201;
                $response["message"] = "Data not found";
                echo json_encode($response);
            }
        }
    }

} else {
    $response["success"] = 201;
    $response["message"] = "Invalid API Key !";
    echo json_encode($response);
}

?>