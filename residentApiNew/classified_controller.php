<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
  if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
    if($_POST['getClassifiedCategories']=="getClassifiedCategories"){
      $q=$d->select("classified_category","classified_category_status='0'","");
      if(mysqli_num_rows($q)>0){
        $response["classified_category"] = array();


        while($data=mysqli_fetch_array($q)) {  

         
          $language_category_name= html_entity_decode($data['classified_category_name']);

          $classified_category_id=$data['classified_category_id'];
          $classified_category = array();
          $classified_category["classified_category_id"]=$data['classified_category_id'];
          $classified_category["classified_category_name"]=$language_category_name;
          $classified_category["classified_category_name_search"]= html_entity_decode($data['classified_category_name']);
          $classified_category["classified_category_image"]=$base_url."img/classified/classified_cat/".$data['classified_category_image'];
          array_push($response["classified_category"], $classified_category);
        }
        $response["message"]="Classified Categories get successfully.";
        $response["status"]="200";
        echo json_encode($response);
      }else{
        $response["message"]="Classified Categories Not Found.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else if($_POST['getClassifiedSubCategories']=="getClassifiedSubCategories"){
      $qs=$d->select("classified_sub_category","classified_category_id='$classified_category_id'");
      if(mysqli_num_rows($qs)>0){
        $response["classified_sub_category"] = array();
        while ($subData=mysqli_fetch_array($qs)) {

          $language_category_name= html_entity_decode($subData['classified_sub_category_name']);

          $classified_sub_category = array();
          $classified_sub_category["classified_category_id"]=$subData['classified_category_id'];
          $classified_sub_category["classified_sub_category_id"]=$subData['classified_sub_category_id'];
          $classified_sub_category["classified_sub_category_name"]=$language_category_name;
          $classified_sub_category["classified_sub_category_name_search"]=html_entity_decode($subData['classified_sub_category_name']);
          $classified_sub_category["classified_sub_category_image"]=$base_url."img/classified/classified_cat/".$subData['classified_sub_category_image'];
          array_push($response["classified_sub_category"], $classified_sub_category);
        }
        $response["message"]="Classified Sub Categories get successfully.";
        $response["status"]="200";
        echo json_encode($response);
      }else{
        $response["message"]="Classified Sub Category Data Not Found.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else if($_POST['addClassifiedItem']=="addClassifiedItem" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($classified_category_id, FILTER_VALIDATE_INT) == true && filter_var($classified_sub_category_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){
      if ($society_id==75) {
        $response["message"]="Access Denied for demo society";
        $response["status"]="201";
        echo json_encode($response);
        exit();
      }

    	// echo "<pre>";
    	// print_r($_FILES);
      $total = count($_FILES['photo']['tmp_name']);
      $pPhoto="";
      if($classified_category_id!='' && $classified_sub_category_id!='' && $society_id!='' && $user_id!='' && $classified_add_title!='' && $classified_describe_selling!='' && $classified_brand_name!='' && $classified_manufacturing_year!='' && $classified_features!='' && $classified_expected_price!='' && $user_name!='' && $user_mobile!='' ){
        if($total>0){
          for ($i = 0; $i < $total; $i++) {
            $uploadedFile = $_FILES['photo']['tmp_name'][$i];
            if ($uploadedFile != ""){
              $sourceProperties = getimagesize($uploadedFile);
              $newFileName = rand().$user_id;
              $dirPath = "../img/classified/classified_items_img/";
              $ext = pathinfo($_FILES['photo']['name'][$i], PATHINFO_EXTENSION);
              $imageType = $sourceProperties[2];
              $imageHeight = $sourceProperties[1];
              $imageWidth = $sourceProperties[0];
              if ($imageWidth>1000) {
                  $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 400 widht
                  $newImageWidth = $imageWidth * $newWidthPercentage /100;
                  $newImageHeight = $imageHeight * $newWidthPercentage /100;
              }else {
                  $newImageWidth = $imageWidth;
                  $newImageHeight = $imageHeight;
              }
              switch ($imageType) {
                case IMAGETYPE_PNG:
                  $imageSrc = imagecreatefrompng($uploadedFile); 
                  $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                  imagepng($tmp,$dirPath. $newFileName. "_classified_item_img.". $ext);
                  break;           

                case IMAGETYPE_JPEG:
                  $imageSrc = imagecreatefromjpeg($uploadedFile); 
                  $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                  imagejpeg($tmp,$dirPath. $newFileName. "_classified_item_img.". $ext);
                  break;
                
                case IMAGETYPE_GIF:
                  $imageSrc = imagecreatefromgif($uploadedFile); 
                  $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                  imagegif($tmp,$dirPath. $newFileName. "_classified_item_img.". $ext);
                  break;

                default:
                  $response["message"]="Invalid Image type.";
                  $response["status"]="201";
                  echo json_encode($response);
                  exit;
                  break;
              }
              $classified_item_img= $newFileName."_classified_item_img.".$ext;
              if ($classified_item_img!="") {
              	if($i!=0){
                   $classified_photo=$classified_photo."~".$classified_item_img;
              	}else{
                   $classified_photo=$classified_item_img;
              	}
              }
              else{
                  $classified_photo=$classified_item_img;
              }
              $pPhoto=$base_url.'img/classified/classified_items_img/'.$classified_item_img;         
            }
          }

        }

          $qs=$d->selectRow("city_name","society_master","society_id='$society_id'");
          $sData= mysqli_fetch_array($qs);
          $location = $sData['city_name'];

          $m->set_data('classified_category_id',$classified_category_id);
          $m->set_data('classified_sub_category_id',$classified_sub_category_id);
          $m->set_data('society_id',$society_id);
          $m->set_data('location',$location);
          $m->set_data('user_id',$user_id);
          $m->set_data('user_name',$user_name);
          $m->set_data('user_mobile',$user_mobile);
          $m->set_data('country_code',$country_code);
          $m->set_data('classified_add_title',$classified_add_title);
          $m->set_data('classified_describe_selling',$classified_describe_selling);
          $m->set_data('classified_brand_name',$classified_brand_name);
          $m->set_data('classified_manufacturing_year',$classified_manufacturing_year);
          $m->set_data('classified_image',$classified_photo);
          $m->set_data('classified_features',$classified_features);
          $m->set_data('product_type',$product_type);
          $m->set_data('classified_expected_price',$classified_expected_price);
          $m->set_data('item_added_date',date('Y-m-d h:i:sa'));
          $m->set_data('classified_status','0');
          $a1 = array(
                'classified_category_id'=>$m->get_data('classified_category_id'),
                'classified_sub_category_id'=>$m->get_data('classified_sub_category_id'),
                'society_id'=>$m->get_data('society_id'),
                'location'=>$m->get_data('location'),
                'user_id'=>$m->get_data('user_id'),
                'user_mobile'=>$m->get_data('user_mobile'),
                'country_code'=>$m->get_data('country_code'),
                'user_name'=>$m->get_data('user_name'),
                'classified_add_title'=>$m->get_data('classified_add_title'),
                'classified_describe_selling'=>$m->get_data('classified_describe_selling'),
                'classified_brand_name'=>$m->get_data('classified_brand_name'),
                'classified_manufacturing_year'=>$m->get_data('classified_manufacturing_year'),
                'classified_image'=>$m->get_data('classified_image'),
                'classified_features'=>$m->get_data('classified_features'),
                'product_type'=>$m->get_data('product_type'),
                'classified_expected_price'=>$m->get_data('classified_expected_price'),
                'item_added_date'=>$m->get_data('item_added_date'),
                'classified_status'=>$m->get_data('classified_status'),
            );

          $d->insert("classified_master",$a1);
          if ($d==TRUE) {

             $title= "$user_name Added Buy/Sell Item";
             $description= stripslashes(html_entity_decode($classified_add_title));
              // $d->insertUserNotification($society_id,$title,$description,"dashboard","home.png","");

             $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id != '$user_id' $bl_qry $appendNew");
             $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id != '$user_id' $bl_qry  $appendNew");
             $nResident->noti("ClassifiedFragment",$imgUrl,$society_id,$fcmArray,$title,$description,"classified");
             $nResident->noti_ios("MainTabClassifiedVC",$imgUrl,$society_id,$fcmArrayIos,$title,$description,"classified");

            $response["message"]="Item Added Successfully";
            $response["status"]="200";
            echo json_encode($response);
          }else{
            $response["message"]="Failed to Add Item";
            $response["status"]="201";
            echo json_encode($response);
          }
        
      }else{
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else if($_POST['editClassifiedItem']=="editClassifiedItem" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($classified_category_id, FILTER_VALIDATE_INT) == true && filter_var($classified_sub_category_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($classified_master_id, FILTER_VALIDATE_INT) == true  ){
      // echo "<pre>";
      // print_r($_FILES);
      $total = count($_FILES['photo']['tmp_name']);
      $pPhoto="";
      if($classified_category_id!='' && $classified_sub_category_id!='' && $society_id!='' && $user_id!='' && $classified_add_title!='' && $classified_describe_selling!='' && $classified_brand_name!='' && $classified_manufacturing_year!='' && $classified_features!='' && $classified_expected_price!='' && $user_name!='' && $user_mobile!='' ){
          
          for ($i = 0; $i < $total; $i++) {
            $uploadedFile = $_FILES['photo']['tmp_name'][$i];
            if ($uploadedFile != ""){
              $sourceProperties = getimagesize($uploadedFile);
              $newFileName = rand().$user_id;
              $dirPath = "../img/classified/classified_items_img/";
              $ext = pathinfo($_FILES['photo']['name'][$i], PATHINFO_EXTENSION);
              $imageType = $sourceProperties[2];
              $imageHeight = $sourceProperties[1];
              $imageWidth = $sourceProperties[0];
              if ($imageWidth>1000) {
                  $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 400 widht
                  $newImageWidth = $imageWidth * $newWidthPercentage /100;
                  $newImageHeight = $imageHeight * $newWidthPercentage /100;
              }else {
                  $newImageWidth = $imageWidth;
                  $newImageHeight = $imageHeight;
              }
              switch ($imageType) {
                case IMAGETYPE_PNG:
                  $imageSrc = imagecreatefrompng($uploadedFile); 
                  $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                  imagepng($tmp,$dirPath. $newFileName. "_classified_item_img.". $ext);
                  break;           

                case IMAGETYPE_JPEG:
                  $imageSrc = imagecreatefromjpeg($uploadedFile); 
                  $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                  imagejpeg($tmp,$dirPath. $newFileName. "_classified_item_img.". $ext);
                  break;
                
                case IMAGETYPE_GIF:
                  $imageSrc = imagecreatefromgif($uploadedFile); 
                  $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                  imagegif($tmp,$dirPath. $newFileName. "_classified_item_img.". $ext);
                  break;

                default:
                  $response["message"]="Invalid Image type.";
                  $response["status"]="201";
                  echo json_encode($response);
                  exit;
                  break;
              }
              $classified_item_img= $newFileName."_classified_item_img.".$ext;
              if ($classified_item_img!="") {
                if($i!=0){
                   $classified_photo=$classified_photo."~".$classified_item_img;
                }else{
                   $classified_photo=$classified_item_img;
                }
              }
              else{
                  $classified_photo=$classified_item_img;
              }
              $pPhoto=$base_url.'img/classified/classified_items_img/'.$classified_item_img;         
            }
          }

          if ($image_url_old!='' && $classified_photo!='') {
            $classified_photo=$image_url_old.'~'.$classified_photo;
          }else if ($image_url_old!='' && $classified_photo=='') {
            $classified_photo=$image_url_old;
          } else {
            $classified_photo=$classified_photo;
          }

          $qs=$d->selectRow("city_name","society_master","society_id='$society_id'");
          $sData= mysqli_fetch_array($qs);
          $location = $sData['city_name'];

          $m->set_data('classified_category_id',$classified_category_id);
          $m->set_data('classified_sub_category_id',$classified_sub_category_id);
          $m->set_data('society_id',$society_id);
          $m->set_data('location',$location);
          $m->set_data('user_id',$user_id);
          $m->set_data('user_name',$user_name);
          $m->set_data('user_mobile',$user_mobile);
          $m->set_data('country_code',$country_code);
          $m->set_data('classified_add_title',$classified_add_title);
          $m->set_data('classified_describe_selling',$classified_describe_selling);
          $m->set_data('classified_brand_name',$classified_brand_name);
          $m->set_data('classified_manufacturing_year',$classified_manufacturing_year);
          $m->set_data('classified_image',$classified_photo);
          $m->set_data('classified_features',$classified_features);
          $m->set_data('product_type',$product_type);
          $m->set_data('classified_expected_price',$classified_expected_price);
          $m->set_data('item_added_date',date('Y-m-d h:i:sa'));
          $m->set_data('classified_status','0');
          $a1 = array(
                'classified_category_id'=>$m->get_data('classified_category_id'),
                'classified_sub_category_id'=>$m->get_data('classified_sub_category_id'),
                'society_id'=>$m->get_data('society_id'),
                'location'=>$m->get_data('location'),
                'user_id'=>$m->get_data('user_id'),
                'user_mobile'=>$m->get_data('user_mobile'),
                'country_code'=>$m->get_data('country_code'),
                'user_name'=>$m->get_data('user_name'),
                'classified_add_title'=>$m->get_data('classified_add_title'),
                'classified_describe_selling'=>$m->get_data('classified_describe_selling'),
                'classified_brand_name'=>$m->get_data('classified_brand_name'),
                'classified_manufacturing_year'=>$m->get_data('classified_manufacturing_year'),
                'classified_image'=>$m->get_data('classified_image'),
                'classified_features'=>$m->get_data('classified_features'),
                'product_type'=>$m->get_data('product_type'),
                'classified_expected_price'=>$m->get_data('classified_expected_price'),
                'item_added_date'=>$m->get_data('item_added_date'),
                'classified_status'=>$m->get_data('classified_status'),
            );

          $d->update("classified_master",$a1,"classified_master_id='$classified_master_id' AND user_id='$user_id'");
          if ($d==TRUE) {
            $response["message"]="Item Updated Successfully";
            $response["status"]="200";
            echo json_encode($response);
          }else{
            $response["message"]="Failed to Update Item";
            $response["status"]="201";
            echo json_encode($response);
          }
        
      }else{
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else if($_POST['getOtherUserClassifiedItemsNew']=="getOtherUserClassifiedItemsNew"){
      if($society_id!='' && $user_id!=''){

        switch ($filter_type) {
          case 'pricelow':
           $orderBy="ORDER BY classified_master.classified_expected_price ASC";
            break;
          case 'pricehigh':
            $orderBy="ORDER BY classified_master.classified_expected_price DESC";
            break;
          
          default:
            $orderBy="ORDER BY classified_master.classified_master_id DESC";
            break;
        }


          $queryAry=array();
          if ($category_id!=0 && $category_id!='' && $category_id>0) {
            $append_query="classified_master.classified_category_id='$category_id'";
            array_push($queryAry, $append_query);
          } 
          if ($sub_category_id!=0 && $sub_category_id!='' && $sub_category_id>0) {
            $append_query1 ="classified_master.classified_sub_category_id='$sub_category_id'";
            array_push($queryAry, $append_query1);
          }

          if ($product_type!='') {
            $append_query2 ="classified_master.product_type='$product_type'";
            array_push($queryAry, $append_query2);
          }

          


          $appendQuery= implode(" AND ", $queryAry);
          if ($appendQuery!='') {
            $appendQuery= " AND ".$appendQuery;
          }
       

        
         $qs=$d->select("classified_master,society_master,classified_category,classified_sub_category,users_master","users_master.user_id=classified_master.user_id AND classified_sub_category.classified_sub_category_id=classified_master.classified_sub_category_id AND classified_category.classified_category_id=classified_master.classified_category_id AND classified_master.society_id=society_master.society_id   AND classified_master.classified_status='0' $appendQuery $appendQuery11 ","$orderBy");


        if(mysqli_num_rows($qs)>0){
          $response["listed_items"] = array();
          while ($data=mysqli_fetch_array($qs)) {
            $listedData = array();
            $cityName= $data['name'];
            $listedData["classified_master_id"]=$data['classified_master_id'];
            $listedData["user_id"]=$data['user_id'];
            $listedData["society_id"]=$data['society_id'];
            $listedData["user_name"]=$data['user_name'];
            if($data['public_mobile']==1) { 
              $listedData["user_mobile"]=$data['country_code'].' '.substr($data['user_mobile'], 0, 3) . '****' . substr($data['user_mobile'],  -3);
            } else {
              $listedData["user_mobile"]=$data['country_code'].' '.$data['user_mobile'];
            }
            $listedData["public_mobile"]=$data['public_mobile'];
            $listedData["user_profile_pic"]=$base_url.'img/users/recident_profile/'.$data['user_profile_pic'];
            $listedData["classified_add_title"]=html_entity_decode($data['classified_add_title']);
            $listedData["classified_category_name"]=html_entity_decode($data['classified_category_name'].'-'.$data['classified_sub_category_name']);
            $listedData["classified_describe_selling"]=html_entity_decode($data['classified_describe_selling']);
            $listedData["classified_brand_name"]=html_entity_decode($data['classified_brand_name']);
            $listedData["classified_manufacturing_year"]=$data['classified_manufacturing_year'];
            $listedData["location"]=html_entity_decode($data['location']);
            $listedData["classified_features"]=html_entity_decode($data['classified_features']);
            $listedData["product_type"]=$data['product_type'];
            $listedData["classified_expected_price"]=number_format($data['classified_expected_price'],2);
            $listedData["item_added_date"]=date('d M Y', strtotime($data['item_added_date']));
            $listedData["image_url"]=$base_url.'img/classified/classified_items_img/';
            $imgAray = explode('~', $data['classified_image']);
            if ($data['classified_image']!='') {
              $imgAray = explode('~', $data['classified_image']);
              $listedData["images"]=$imgAray;
            } else {
              $listedData["images"]=NULL;

            }
            array_push($response["listed_items"], $listedData);
          }
          $response["message"]="Items get successfully ";
          $response["status"]="200";
          echo json_encode($response);
        }else{
          $response["message"]="No Item Found.";
          $response["status"]="201";
          echo json_encode($response);
        }
      }else{
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else if($_POST['deleteImage']=="deleteImage" && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($classified_master_id, FILTER_VALIDATE_INT) == true ){
          $imgAray = explode("~", $image_url_old);

          if (($key = array_search($delteImgName, $imgAray)) !== false) {
              unset($imgAray[$key]);
          }
          $classified_image= implode("~", $imgAray);

          $m->set_data('classified_image',$classified_image);
          $a1 = array(
                'classified_image'=>$m->get_data('classified_image'),
            );
        

          $d->update("classified_master",$a1,"classified_master_id='$classified_master_id' AND user_id='$user_id'");
          if ($d==TRUE) {
            $response["message"]="Image Removed Successfully";
            $response["status"]="200";
            echo json_encode($response);
          }else{
            $response["message"]="Failed to Update Item";
            $response["status"]="201";
            echo json_encode($response);
          }

    }else if($_POST['getUserClassifiedItems']=="getUserClassifiedItems"){
      if($society_id!='' && $user_id!=''){
        $qs=$d->select("classified_master,society_master,classified_category,classified_sub_category,users_master","classified_sub_category.classified_sub_category_id=classified_master.classified_sub_category_id AND classified_category.classified_category_id=classified_master.classified_category_id AND users_master.user_id=classified_master.user_id AND classified_master.society_id=society_master.society_id AND classified_master.user_id='$user_id' AND classified_master.society_id='$society_id' AND classified_master.classified_status='0'","ORDER BY classified_master.classified_master_id DESC");
        if(mysqli_num_rows($qs)>0){
          $response["listed_items"] = array();
          while ($data=mysqli_fetch_array($qs)) {
            $listedData = array();
             $cityName= $data['name'];
            $listedData["classified_master_id"]=$data['classified_master_id'];
            $listedData["classified_category_id"]=$data['classified_category_id'];
            $listedData["classified_sub_category_id"]=$data['classified_sub_category_id'];
            $listedData["user_id"]=$data['user_id'];
            $listedData["society_id"]=$data['society_id'];
            $listedData["user_name"]=html_entity_decode($data['user_name']);
            $listedData["user_mobile"]=$data['user_mobile'];
            $listedData["classified_category_name"]=html_entity_decode($data['classified_category_name'].'-'.$data['classified_sub_category_name']);
            $listedData["user_profile_pic"]=$base_url.'img/users/recident_profile/'.$data['user_profile_pic'];
            $listedData["classified_add_title"]=html_entity_decode($data['classified_add_title']);
            $listedData["classified_describe_selling"]=html_entity_decode($data['classified_describe_selling']);
            $listedData["classified_brand_name"]=html_entity_decode($data['classified_brand_name']);
            $listedData["classified_manufacturing_year"]=$data['classified_manufacturing_year'];
            $listedData["location"]=html_entity_decode($data['location']);
            $listedData["classified_features"]=html_entity_decode($data['classified_features']);
            $listedData["product_type"]=$data['product_type'];
            $listedData["classified_expected_price"]=number_format($data['classified_expected_price'],2);
            $listedData["item_added_date"]=date('d M Y', strtotime($data['item_added_date']));
            $listedData["image_url_old"]=$data['classified_image'];
            $listedData["image_url"]=$base_url.'img/classified/classified_items_img/';

            if ($data['classified_image']!='') {
              $imgAray = explode('~', $data['classified_image']);
              $listedData["images"]=$imgAray;
            } else {
              $listedData["images"]=NULL;

            }
            array_push($response["listed_items"], $listedData);
          }
          $response["message"]="Items get successfully.";
          $response["status"]="200";
          echo json_encode($response);
        }else{
          $response["message"]="No Item Found.";
          $response["status"]="201";
          echo json_encode($response);
        }
      }else{
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else if($_POST['getOtherUserClassifiedItems']=="getOtherUserClassifiedItems"){
      if($society_id!='' && $category_id!='' && $sub_category_id!='' && $user_id!=''){
        switch ($filter_type) {
          case 'old':
           $orderBy="ORDER BY classified_master.classified_master_id ASC";
            break;
          case 'new':
            $orderBy="ORDER BY classified_master.classified_master_id DESC";
            break;
          case 'pricelow':
           $orderBy="ORDER BY classified_master.classified_expected_price ASC";
            break;
          case 'pricehigh':
            $orderBy="ORDER BY classified_master.classified_expected_price DESC";
            break;
          
          default:
            $orderBy="ORDER BY classified_master.classified_master_id DESC";
            break;
        }

        if ($state_id==0 && $city_id==0) {
            $qSociety = $d->select("society_master","society_id ='$society_id'");
            $user_data_society = mysqli_fetch_array($qSociety);
            $state_id = $user_data_society['state_id'];
            $city_id = $user_data_society['city_id'];
            $appendQuery = " AND society_master.state_id='$state_id'";
        } else {
          $appendQuery = " AND society_master.state_id='$state_id' AND society_master.city_id='$city_id'";
        } 

        
         $qs=$d->select("classified_master,society_master","classified_master.society_id=society_master.society_id AND classified_master.classified_category_id='$category_id' AND classified_master.classified_sub_category_id='$sub_category_id' AND classified_master.classified_status='0' $appendQuery","$orderBy");


        if(mysqli_num_rows($qs)>0){
          $response["listed_items"] = array();
          while ($data=mysqli_fetch_array($qs)) {
            $listedData = array();
            $cityName= $data['name'];
            $listedData["classified_master_id"]=$data['classified_master_id'];
            $listedData["user_id"]=$data['user_id'];
            $listedData["society_id"]=$data['society_id'];
            $listedData["user_name"]=$data['user_name'];
            if($data['public_mobile']==1) { 
              $listedData["user_mobile"]=$data['country_code'].' '.substr($data['user_mobile'], 0, 3) . '****' . substr($data['user_mobile'],  -3);
            } else {
              $listedData["user_mobile"]=$data['country_code'].' '.$data['user_mobile'];
            }
            $listedData["public_mobile"]=$data['public_mobile'];
            $listedData["classified_add_title"]=$data['classified_add_title'];
            $listedData["classified_describe_selling"]=$data['classified_describe_selling'];
            $listedData["classified_brand_name"]=$data['classified_brand_name'];
            $listedData["classified_manufacturing_year"]=$data['classified_manufacturing_year'];
            $listedData["location"]=$data['location'];
            $listedData["classified_features"]=$data['classified_features'];
            $listedData["product_type"]=$data['product_type'];
            $listedData["classified_expected_price"]=number_format($data['classified_expected_price'],2);
            $listedData["item_added_date"]=date('d M Y', strtotime($data['item_added_date']));
            $listedData["image_url"]=$base_url.'img/classified/classified_items_img/';
            $imgAray = explode('~', $data['classified_image']);
            if ($data['classified_image']!='') {
              $imgAray = explode('~', $data['classified_image']);
              $listedData["images"]=$imgAray;
            } else {
              $listedData["images"]=NULL;

            }
            array_push($response["listed_items"], $listedData);
          }
          $response["message"]="Items get successfully ";
          $response["status"]="200";
          echo json_encode($response);
        }else{
          $response["message"]="No Item Found.";
          $response["status"]="201";
          echo json_encode($response);
        }
      }else{
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else if($_POST['deleteUserClassifiedItem']=="deleteUserClassifiedItem"){
      if($society_id!='' && $user_id!='' && $classified_master_id!=''){
        $qs=$d->select("classified_master","user_id='$user_id' AND society_id='$society_id' AND classified_master_id='$classified_master_id'","");
        if(mysqli_num_rows($qs)>0){
          $d->delete("classified_master","user_id='$user_id' AND society_id='$society_id' AND classified_master_id='$classified_master_id'");
          $response["message"]="Item deleted successfully.";
          $response["status"]="200";
          echo json_encode($response);
        }else{
          $response["message"]="Invalid Item.";
          $response["status"]="201";
          echo json_encode($response);
        }
      }else{
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else if($_POST['getState']=="getState" && filter_var($country_id, FILTER_VALIDATE_INT) == true){

                $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS,
                              "getState=getState&country_id=$country_id&language_id=$language_id");

                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                  
                  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'key: bmsapikey'
                  ));

                  $server_output = curl_exec($ch);

                  curl_close ($ch);
                  $server_output=json_decode($server_output,true);

                  $totalStates= count($server_output['states']);

               $stateIdAry =array();
                 $q11=$d->select("classified_master,society_master","classified_master.society_id=society_master.society_id AND classified_master.classified_status='0' AND society_master.country_id='$country_id' ","");
                while ($pData=mysqli_fetch_array($q11)) {
                    array_push($stateIdAry, $pData['state_id']);
                }
                     // print_r(array_unique($stateIdAry));
                      // exit();


                 if($totalStates>0){

                    $response["states"] = array();

                    for ($i=0; $i <$totalStates ; $i++) { 
                        
                        if(in_array($server_output['states'][$i]['state_id'], $stateIdAry)){
                            $states = array(); 
                            $states["state_id"]=$server_output['states'][$i]['state_id'];
                            $states["name"]=$server_output['states'][$i]['name'];
                            $states["name_search"]=$server_output['states'][$i]['name_search'];
                            $states["country_id"]=$server_output['states'][$i]['country_id'];
                           
                            $state_id=$data_states_list['state_id'];
                            array_push($response["states"], $states);
                        }
                    }


                    $response["message"]="Get State success.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No State Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['getCity']=="getCity" && filter_var($state_id, FILTER_VALIDATE_INT) == true){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                            "getCity=getCity&state_id=$state_id&language_id=$language_id");

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                  'key: bmsapikey'
                ));

                $server_output = curl_exec($ch);

                curl_close ($ch);
                $server_output=json_decode($server_output,true);

                $totalStates= count($server_output['cities']);

                $stateIdAry =array();
                       $q11=$d->select("classified_master,society_master","classified_master.society_id=society_master.society_id AND classified_master.classified_status='0' AND society_master.state_id='$state_id' ","");
                      while ($pData=mysqli_fetch_array($q11)) {
                          array_push($stateIdAry, $pData['city_id']);
                      }
                     // print_r(array_unique($stateIdAry));
                      // exit();
                  if($totalStates>0){
                    
                    $response["cities"] = array();

                    for ($i=0; $i <$totalStates ; $i++) { 
                        if(in_array($server_output['cities'][$i]['city_id'], $stateIdAry)){
                            $cities = array(); 

                            $cities["city_id"]=$server_output['cities'][$i]['city_id'];
                            $cities["name"]=$server_output['cities'][$i]['name'];
                            $cities["name_search"]=$server_output['cities'][$i]['name_search'];
                            $cities["state_id"]=$server_output['cities'][$i]['state_id'];
                            $cities["country_id"]=$server_output['cities'][$i]['country_id'];
                            
                            array_push($response["cities"], $cities);
                        }
                    }



                    $response["message"]="Get City success.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No City Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else{
      $response["message"]="wrong tag.";
      $response["status"]="201";
      echo json_encode($response);
    }
  }else{
    $response["message"] = "wrong api key.";
    $response["status"] = "201";
    echo json_encode($response);
  }
}?>