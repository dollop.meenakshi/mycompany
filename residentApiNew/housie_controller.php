<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
	$response = array();
	extract(array_map("test_input" , $_POST));
  $today  = date("Y-m-d");
    if($_POST['getGame']=="getGame" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

        $todaDate= date("Y-m-d");
           
        $q=$d->select("housie_room_master","society_id='$society_id' AND game_date='$todaDate'","ORDER BY room_id DESC ");
          
         
        if(mysqli_num_rows($q)>0){

            $response["game"] = array();

            while($data=mysqli_fetch_array($q)) {


                     $today  = date("Y-m-d");
                     $game = array(); 
                     $que_interval = $data['que_interval'];
                     $game["room_id"]=$data['room_id'];
                     $game["society_id"]=$data['society_id'];
                     $game["game_name"]=$data['game_name'];
                     $game["game_date"]=date('d M Y',strtotime($data['game_date']));
                     $game["game_time"]=date('h:i A',strtotime($data['game_time']));

                    if ($data['sponser_photo']!='') {
                       $game["sponser_photo"]=$base_url.'img/game/'.$data['sponser_photo'];
                     } else {
                       $game["sponser_photo"]="";
                     }
                     $game["sponser_name"]=$data['sponser_name'];
                     $game["sponser_url"]=$data['sponser_url'];

                     $survey_question_master_qry=$d->select("housie_questions_master","room_id ='$data[room_id]' AND society_id='$society_id'");
                     $gameDate= $data['game_date'].' '.date('h:i A',strtotime($data['game_time']));
                     $totalQue= mysqli_num_rows($survey_question_master_qry);
                     $totalTimeInSecond= $totalQue * $que_interval ;
                     $time = new DateTime($gameDate);
                     $time->add(new DateInterval('PT' . $totalTimeInSecond . 'S'));
                     $gameEndTime = $time->format('Y-m-d h:i A');
                     $gameEndTimeExpire = strtotime($gameEndTime);
                     $game["game_end_time"]= $gameEndTime; 

                     $game["no_of_question"]=$totalQue;
                     $cTime= date("H:i:s");
                     $expire = strtotime($data['game_date'].' '.$data['game_time']);
                     $today = strtotime("$today $cTime");
                     if($data['room_status'] == 1 || $today >=  $gameEndTimeExpire){
                       $game["game_over"]=true;
                     } else {
                       $game["game_over"]=false;
                     }
                    
                     array_push($response["game"], $game);
              }
            
           
             $response["message"]="Get Game Successfully!".$data['game_time'];
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Game Availble.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else if($_POST['getGameNew']=="getGameNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

        $todaDate= date("Y-m-d");
           
        $q=$d->select("housie_room_master","society_id='$society_id' AND game_date='$todaDate'","ORDER BY room_id DESC ");
          
         
        if(mysqli_num_rows($q)>0){

            $response["game"] = array();

            while($data=mysqli_fetch_array($q)) {

                     if ($data['housie_type']==1) {
                       $qc=$d->select("housie_participate_player","user_id='$user_id' AND room_id='$data[room_id]'");
                       if (mysqli_num_rows($qc)>0) {
                        $push = "true";
                       } else {
                        $push = "false";
                       }
                     } else if ($data['housie_type']==0) {
                       $push = "true";
                     }

                     $today  = date("Y-m-d");
                     $game = array(); 
                     $que_interval = $data['que_interval'];
                     $game["room_id"]=$data['room_id'];
                     $game["society_id"]=$data['society_id'];
                     $game["game_name"]=$data['game_name'];
                     $game["game_date"]=date('d M Y',strtotime($data['game_date']));
                     $game["game_time"]=date('h:i A',strtotime($data['game_time']));

                    if ($data['sponser_photo']!='') {
                       $game["sponser_photo"]=$base_url.'img/game/'.$data['sponser_photo'];
                     } else {
                       $game["sponser_photo"]="";
                     }
                     $game["sponser_name"]=$data['sponser_name'];
                     $game["sponser_url"]=$data['sponser_url'];

                     $survey_question_master_qry=$d->select("housie_questions_master","room_id ='$data[room_id]' AND society_id='$society_id'");
                     $gameDate= $data['game_date'].' '.date('h:i A',strtotime($data['game_time']));
                     $totalQue= mysqli_num_rows($survey_question_master_qry);
                     $totalTimeInSecond= $totalQue * $que_interval ;
                     $time = new DateTime($gameDate);
                     $time->add(new DateInterval('PT' . $totalTimeInSecond . 'S'));
                     $gameEndTime = $time->format('Y-m-d h:i A');
                     $gameEndTimeExpire = strtotime($gameEndTime);
                     $game["game_end_time"]= $gameEndTime; 

                     $game["no_of_question"]=$totalQue;
                     $cTime= date("H:i:s");
                     $expire = strtotime($data['game_date'].' '.$data['game_time']);
                     $today = strtotime("$today $cTime");
                     if($data['room_status'] == 1 || $today >=  $gameEndTimeExpire){
                       $game["game_over"]=true;
                     } else {
                       $game["game_over"]=false;
                     }
                     $game["push"]=$push;
                    if ($push=='true') {
                     array_push($response["game"], $game);
                    }
            
            }
           
             $response["message"]="Get Game Successfully!".$data['game_time'];
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Game Availble.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else if(isset($_POST['joinRoom'])  && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($room_id, FILTER_VALIDATE_INT) == true) {

      // $timer_interval= "30";

      $rb=$d->select("housie_room_master","room_id='$room_id'");
      $blockCheck=mysqli_fetch_array($rb);
      $timer_interval = $blockCheck['que_interval'];
      if($blockCheck['room_status'] == 1){
        $response["message"]="Game Over ";
        $response["status"]='203';
        echo json_encode($response);
        exit();
      }

     $survey_question_master_qry=$d->select("housie_questions_master","room_id ='$blockCheck[room_id]' AND society_id='$society_id'","LIMIT 60");
     $totalQue= mysqli_num_rows($survey_question_master_qry);
     $totalTimeInSecond= $totalQue * $timer_interval ;
     $gameTime =$blockCheck['game_date'].' '.$blockCheck['game_time'];
     $time = new DateTime("$blockCheck[game_date] $blockCheck[game_time]");
     $time->add(new DateInterval('PT' . $totalTimeInSecond . 'S'));
     $gameEndTime = $time->format('Y-m-d H:i');
     $cTime= date("H:i:s");
     $cTimeNow= date("Y-m-d H:i:s");
     $expire = strtotime($blockCheck['game_date'].' '.$blockCheck['game_time']);
     $gameEndTimeExpire = strtotime($gameEndTime);
     $today = strtotime("$today $cTime");
      $timeFirst  = strtotime($gameTime);
      $timeSecond = strtotime($cTimeNow);
      $differenceInSeconds = $timeFirst - $timeSecond;

     if($today >=  $gameEndTimeExpire) {
        $response["message"]="Game Over at $gameEndTime";
        $response["status"]='203';
        echo json_encode($response);
        exit();
      } else if($differenceInSeconds>900){
        $gameTime=date('h:i A',strtotime($gameTime));
        $gameDateView=date('d M Y',strtotime($gameTime));
        $response["message"]="MyCo Housie will start at $gameTime on $gameDateView, Please join 15 minutes before the scheduled time.";
        $response["status"]='204';
        echo json_encode($response);
        exit();
      } else {


      $m->set_data("society_id",$society_id);
      $m->set_data("user_id",$user_id);
      $m->set_data("user_type",0);
      $m->set_data("room_id",$room_id);
      $m->set_data("join_time",date("Y-m-d H:i:s"));

      $a1 = array(
        'society_id'=>$m->get_data('society_id'),
        'room_id'=>$m->get_data('room_id'),
        'user_id'=>$m->get_data('user_id'),
        'join_time'=>$m->get_data('join_time'),
      );
      // check stage id 
     
      $ww1=$d->select("housie_join_room_master","user_id='$user_id' AND room_id='$room_id'");
        if(mysqli_num_rows($ww1)>0) {
          $q=$d->update("housie_join_room_master",$a1,"user_id='$user_id' AND room_id='$room_id'");
        } else {
          $q=$d->insert("housie_join_room_master",$a1);

          $d->insert_myactivity($user_id,"$society_id","0","$user_name","Housie game joined","housieNew.png");

          $fcmArray=$d->get_android_fcm("users_master,housie_join_room_master","users_master.user_id=housie_join_room_master.user_id AND  users_master.user_token!='' AND users_master.society_id='$society_id' AND users_master.device='android' AND housie_join_room_master.room_id='$room_id' AND housie_join_room_master.user_id!='$user_id' AND users_master.user_id!='$user_id'");
          $fcmArrayIos=$d->get_android_fcm("users_master,housie_join_room_master","users_master.user_id=housie_join_room_master.user_id AND  users_master.user_token!='' AND users_master.society_id='$society_id' AND users_master.device='ios' AND housie_join_room_master.room_id='$room_id' AND housie_join_room_master.user_id!='$user_id' AND users_master.user_id!='$user_id'");
          $nResident->noti("HousieJoinGame","",$society_id,$fcmArray,"$user_name","Joined the Game",'housie');
          $nResident->noti_ios("HousieJoinGame","",$society_id,$fcmArrayIos,"$user_name","Joined the Game",'housie');
        }
        if ($q == TRUE) {
          // find no answer 
         
          // get ticket answer
          $sq1=$d->select("housie_questions_master","room_id='$room_id'","ORDER BY rand() LIMIT 15");

          $response["ticketView"] = array();
                $ticketView = array();
          while($answerData=mysqli_fetch_array($sq1)){
            $ticketView['answer'] = html_entity_decode($answerData['answer']);
                    array_push($response['ticketView'], $ticketView);
          // print_r($answerData);
          }

          $sq1=$d->select("housie_questions_master","room_id='$room_id'","ORDER BY que_id ASC");
          $response["question"] = array();
          $question = array();
          $queAry= "";
          while($quesData=mysqli_fetch_array($sq1)){
              $question['que_id'] = $quesData['que_id'];
              $question['question'] = html_entity_decode($quesData['question']);
              $question['answer'] = html_entity_decode($quesData['answer']);
                      array_push($response['question'], $question);
            // print_r($answerData);
              $queAry=$queAry.",".$quesData['que_id'];
          }

           
             $timer_interval111 = $timer_interval/2;
            
             $response["message"]="Room Joined..";
             $response["stage_ticket_ans"]=''.$timer_interval111;
             $response["timer_interval"]=''.$timer_interval/2;
             $response["current_time"]=date("Y-m-d H:i:s");
             $response["game_start_time"]=$gameTime;
             $response["game_end_time"]=$gameEndTime;
             $response["totalGameInSecond"]=$totalTimeInSecond.'';
             $response["remaningTimeInSecond"]=$differenceInSeconds.'';
             if($today <= $expire){
               $response["status"]='201';
             }else {
               $negitPostive= abs($differenceInSeconds);
               $dividetosecon= $negitPostive/$timer_interval;
               $findQue= $dividetosecon+1;
               // $n = 1.25;
               $questionCounter = floor($findQue);      // 1
               $fraction = $findQue - $questionCounter; // .25

               $response["questionCounter"]=$questionCounter.'';
               if ($fraction>=0.5) {
                $response["questionAnswerFlag"]='1';
               } else {
                $response["questionAnswerFlag"]='0';
               }
                
               $totalGameInSecond= $totalTimeInSecond+$differenceInSeconds;
               $response["questionCounter"]=$questionCounter.'';
               $response["totalGameInSecond"]=$totalGameInSecond.'';
               $response["message"]="Game Joined";
               $response["status"]='200';

             }
             $response["room_id"]=$room_id;
             echo json_encode($response);
          }else{
            $response["message"]="Soenthing Wrong.";
            $response["status"]='201';
            echo json_encode($response);
          }
      }
    }else if(isset($_POST['getRules']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($room_id, FILTER_VALIDATE_INT) == true){

    $q=$d->select("housie_claim_rules_master,housie_rules_master","housie_rules_master.master_rule_id=housie_claim_rules_master.master_rule_id AND housie_claim_rules_master.room_id='$room_id'","ORDER BY housie_claim_rules_master.master_rule_id ASC");
    $response["rules"]=array();
    if(mysqli_num_rows($q)>0){          
        while ($data = mysqli_fetch_array($q)) {
          $rules=array();

            $q11=$d->select("housie_winner_master,housie_rules_master,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND housie_winner_master.user_id=users_master.user_id AND housie_winner_master.master_rule_id=housie_rules_master.master_rule_id AND housie_winner_master.room_id='$room_id' AND housie_winner_master.master_rule_id='$data[master_rule_id]'");
    
            $rules["winnerList"]=array();
            $userName='';
            while ($data11 = mysqli_fetch_array($q11)) {
              // echo $data11['user_name'];
                  $winnerList=array();
                  $winnerList["user_name"]=$data11['user_first_name'] .' ('.$data11['block_name'].'-'.$data11['unit_name'].')';;
                  $winnerList["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data11['user_profile_pic'];
                  $userName=$userName.$data11['user_first_name'].",";
                array_push($rules["winnerList"],$winnerList);
              }
                 
                

          $rules["master_rule_id"]=$data['master_rule_id'];
          $rules["rule_name"] = html_entity_decode($data['rule_name']);
          $rules["rele_discription"] = html_entity_decode($data['rele_discription']);
          $rules["total_player"] = $data['total_claim_player'];
          $rules["claim_player"] = mysqli_num_rows($q11).'';
          $rules["rule_points"] = $data['rule_points'];
          $rules["winner_list"] = $userName;
            array_push($response["rules"],$rules);
          }
            $response["status"] = "200";
        $response["message"] = "Rules Details";
         echo json_encode($response);
          
      } else {  
      $response["status"] = "201";
          $response["message"] = "No Rules Found";
          echo json_encode($response);
      }
  }else if(isset($_POST['claimTicket']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($room_id, FILTER_VALIDATE_INT) == true && filter_var($master_rule_id, FILTER_VALIDATE_INT) == true){

      $q=$d->select("housie_claim_rules_master","room_id='$room_id' AND master_rule_id='$master_rule_id'");
      if(mysqli_num_rows($q)>0){   
        $data=mysqli_fetch_array($q);

        $q11=$d->select("housie_winner_master,housie_rules_master,users_master","housie_winner_master.user_id=users_master.user_id AND housie_winner_master.master_rule_id=housie_rules_master.master_rule_id AND housie_winner_master.room_id='$room_id' AND housie_winner_master.master_rule_id='$master_rule_id'");

        $allow_users_claim=$data['allow_users_claim'];
        $claimUsers= mysqli_num_rows($q11);
        
        if($data['total_claim_player']<=$claimUsers){
            $response["status"] = '201';
            $response["message"] = "Already Claimed This Ticket";
            echo json_encode($response);
        } else {

          $caa=$d->select("housie_winner_master","user_id='$user_id' AND room_id='$room_id' AND master_rule_id='$master_rule_id' ");
          
          if(mysqli_num_rows($caa)>0) {
            
            $response["status"] = '201';
            $response["message"] = "Already Claimed By You";
            echo json_encode($response);

          } else {

         
            $m->set_data('user_id',$user_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('claim_id',$data['claim_id']);
            $m->set_data('master_rule_id',$data['master_rule_id']);
            $m->set_data('room_id',$room_id);
            $m->set_data('winning_point',$data['rule_points']);
            $m->set_data('quiz_date',date('Y M d'));

            $abcd=array(
              'user_id'=> $m->get_data('user_id'),
              'society_id'=> $m->get_data('society_id'),
              'master_rule_id'=> $m->get_data('master_rule_id'),
              'room_id'=> $m->get_data('room_id'),
              'quiz_date'=> $m->get_data('quiz_date'),
              'winning_point'=> $m->get_data('winning_point'),
            );
            // $ca=$d->select("winner_master","user_id='$user_id' AND room_id='$room_id' AND master_rule_id='$master_rule_id'");
           
            $d->insert("housie_winner_master",$abcd);

            $response["status"] = '200';
            $response["message"] = "Ticket Claimed";
            echo json_encode($response);

            // Game over code baki

            $ruleAry=array();
            $checkGame=$d->select("housie_claim_rules_master","room_id='$room_id'");
            while ($checkGameData=mysqli_fetch_array($checkGame)) {
                $master_rule_id11= $checkGameData['master_rule_id'];
                 $toClaimUser= $d->count_data_direct("winner_id","housie_winner_master","room_id='$room_id' AND master_rule_id='$master_rule_id11'");
                  if($checkGameData['total_claim_player']!=$toClaimUser){
                    array_push($ruleAry, $checkGameData['allow_users_claim']);
                  }
            }

            if (empty($ruleAry)) {
              // game over
              $fcmArray=$d->get_android_fcm("users_master,housie_join_room_master","users_master.user_id=housie_join_room_master.user_id AND  users_master.user_token!='' AND users_master.society_id='$society_id' AND users_master.device='android' AND housie_join_room_master.room_id='$room_id'");
              $fcmArrayIos=$d->get_android_fcm("users_master,housie_join_room_master","users_master.user_id=housie_join_room_master.user_id AND  users_master.user_token!='' AND users_master.society_id='$society_id' AND users_master.device='ios' AND housie_join_room_master.room_id='$room_id'");
              $nResident->noti("HosieInfoPageActivity","",$society_id,$fcmArray,"Game Over","All Rules Claimed",'gameOver');
              $nResident->noti_ios("HousieVC","",$society_id,$fcmArrayIos,"Game Over","All Rules Claimed",'gameOver');

              $abcd=array(
                'room_status'=> 1,
              );
              $d->update("housie_room_master",$abcd,"room_id='$room_id'");

            } else {
              // send notifiation for claim user
              $rq=$d->select("housie_rules_master","master_rule_id='$master_rule_id'");
              $rulNameData=mysqli_fetch_array($rq);
              $masterRuleName= $rulNameData['rule_name'];

              $c5=$d->select("users_master","user_id='$user_id'");
              $winData=mysqli_fetch_array($c5);
              $userName= $winData['user_first_name'];

              $fcmArray=$d->get_android_fcm("users_master,housie_join_room_master","users_master.user_id=housie_join_room_master.user_id AND  users_master.user_token!='' AND users_master.society_id='$society_id' AND users_master.device='android' AND housie_join_room_master.room_id='$room_id' AND housie_join_room_master.user_id!='$user_id'");
              $fcmArrayIos=$d->get_android_fcm("users_master,housie_join_room_master","users_master.user_id=housie_join_room_master.user_id AND  users_master.user_token!='' AND users_master.society_id='$society_id' AND users_master.device='ios' AND housie_join_room_master.room_id='$room_id' AND housie_join_room_master.user_id!='$user_id'");
              $nResident->noti("HosieInfoPageActivity","",$society_id,$fcmArray,"Rule Claim","$masterRuleName Claim By $userName",'userClaim');
              $nResident->noti_ios("HousieVC","",$society_id,$fcmArrayIos,"Rule Claim","$masterRuleName Claim By $userName",'userClaim');

            }

            
          }
        }
      } else {
          $response["status"] = '201';
          $response["message"] = "No Rules Found";
          echo json_encode($response);
      }
  }else if(isset($_POST['getWinner']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($room_id  , FILTER_VALIDATE_INT) == true ){
    
    $q=$d->select("housie_winner_master,users_master,housie_rules_master,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND housie_rules_master.master_rule_id=housie_winner_master.master_rule_id AND  housie_winner_master.user_id=users_master.user_id AND housie_winner_master.room_id='$room_id'","ORDER BY housie_winner_master.winner_id ASC");

    $response["winnerUsers"]=array();
    if(mysqli_num_rows($q)>0){          
        while ($data = mysqli_fetch_array($q)) {
           // $data = array_map('utf8_encode', $data);
          $winnerUsers=array();
          $winnerUsers["winner_id"]=$data['winner_id'];
          $winnerUsers["user_id"]=$data['user_id'];
          $winnerUsers["user_name"] = $data['user_first_name'] .' ('.$data['block_name'].'-'.$data['unit_name'].')';
          $winnerUsers["gender"] = $data['gender'];
          $winnerUsers["winning_point"] = $data['winning_point'];
          $winnerUsers["rule_name"] = html_entity_decode($data['rule_name']);
          $winnerUsers["rele_discription"] = html_entity_decode($data['rele_discription']);
          $winnerUsers["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
            array_push($response["winnerUsers"],$winnerUsers);
          }
            $response["status"] = '200';
        $response["message"] = "Users List";
         echo json_encode($response);
          
      } else {  
      $response["status"] = '201';
          $response["message"] = "No Winner Found";
          echo json_encode($response);
      }
  }else if(isset($_POST['getResult']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true ){
    
    

    $q=$d->select("housie_room_master","","ORDER BY room_id DESC");

    $response["gameList"]=array();
    if(mysqli_num_rows($q)>0){          
        while ($data = mysqli_fetch_array($q)) {
           $today  = date("Y-m-d");
           $cTime= date("H:i:s");
           $survey_question_master_qry=$d->select("housie_questions_master","room_id ='$data[room_id]' AND society_id='$society_id'");
           $gameDate= $data['game_date'].' '.date('h:i A',strtotime($data['game_time']));
           $totalQue= mysqli_num_rows($survey_question_master_qry);
           $totalTimeInSecond= $totalQue * $data['timer_interval'] ;
           $time = new DateTime($gameDate);
           $time->add(new DateInterval('PT' . $totalTimeInSecond . 'S'));
           $gameEndTime = $time->format('Y-m-d h:i A');
           $gameEndTimeExpire = strtotime($gameEndTime);
           $today = strtotime("$today $cTime");
         if($data['room_status']==1 || $today >=  $gameEndTimeExpire){
           // $data = array_map('utf8_encode', $data);
          $gameList=array();
          $gameList["room_id"]=$data['room_id'];
          $gameList["game_name"] = $data['game_name'];
          $gameList["game_date"] = date('d M Y,h:i A',strtotime($data['game_date'].' '.$data['game_time']));
          $gameList["sponser_name"] = $data['sponser_name'];
          $gameList["sponser_url"] = $data['sponser_url'];
          $gameList["gameEndTime"] = $gameEndTime;
           if ($data['sponser_photo']!='') {
             $gameList["sponser_photo"]=$base_url.'img/game/'.$data['sponser_photo'];
           } else {
             $gameList["sponser_photo"]="";
           }

            $gameList["winnerUsers"] = array();
            $qs=$d->select("housie_winner_master,users_master,housie_rules_master,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND housie_rules_master.master_rule_id=housie_winner_master.master_rule_id AND  housie_winner_master.user_id=users_master.user_id AND housie_winner_master.room_id='$data[room_id]'");
              while ($subData=mysqli_fetch_array($qs)) {
                  $winnerUsers = array();
                  $winnerUsers["user_id"]=$subData["user_id"];
                  $winnerUsers["user_name"]=$subData["user_full_name"].' ('.strtoupper($subData["block_name"].'-'.$subData['unit_name']).')';
                  $winnerUsers["gender"]=$subData["gender"];
                  $winnerUsers["winning_point"]=$subData["winning_point"];
                  $winnerUsers["rule_name"]=$subData["rule_name"];
                  $winnerUsers["rele_discription"]=$subData["rele_discription"];
                  $winnerUsers["user_profile_pic"]= $base_url."img/users/recident_profile/".$subData['user_profile_pic'];

                  array_push($gameList["winnerUsers"], $winnerUsers); 
            }

            array_push($response["gameList"],$gameList);
          }

        }

        $response["status"] = '200';
        $response["message"] = "Game List";
         echo json_encode($response);
          
      } else {  
      $response["status"] = '201';
          $response["message"] = "No Winner Found";
          echo json_encode($response);
      }
  }else  if(isset($_POST['getJoinUsers'])  && filter_var($room_id, FILTER_VALIDATE_INT) == true){
    
    $q=$d->select("housie_join_room_master,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND housie_join_room_master.user_id=users_master.user_id AND housie_join_room_master.room_id='$room_id'");

    if(mysqli_num_rows($q)>0) {          
    $response["joinUsers"]=array();
        while ($data = mysqli_fetch_array($q)) {
          $joinUsers=array();
            
            $count=$d->sum_data("winning_point","housie_winner_master","room_id='$room_id' AND user_id='$data[user_id]'","ORDER BY winning_point DESC");
              while($row=mysqli_fetch_array($count))
             {
                  $asif=$row['SUM(winning_point)'];
                  $totalWin= number_format($asif,0,'.','');
                     
              }
           
              $joinUsers["user_id"]=$data['user_id'];
              $joinUsers["user_name"] = $data['user_first_name'].' ('.$data["block_name"].'-'.$data['unit_name'].')';
              $joinUsers["user_points"] = $totalWin;
              $joinUsers["gender"] = $data['gender'];
              $joinUsers["user_profile"]= $base_url."img/users/recident_profile/".$data['user_profile_pic'];


          // $joinUsers["user_name"] = $data['user_name'];
          
            array_push($response["joinUsers"],$joinUsers);
          }

          
            $response["status"] = '200';
            $response["message"] = "Users List";
            echo json_encode($response);
          
      } else { 
           
          $response["status"] = '201';
          $response["message"] = "No Join User Found";
          echo json_encode($response);
      }
  }else{
      $response["message"]="wrong tag";
      $response["status"]="201";
      echo json_encode($response);
    }
  }
    else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
