    <?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb && $auth_check=='true') { 
    $response = array();
    extract(array_map("test_input" , $_POST));
 
    if($_POST['checkAccessPendingMember']=="checkAccessPendingMember" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $access_type = $approve_employee_access;

        include 'check_access_data.php';

        $accessResponseData['status'] = "200";
        $accessResponseData['message'] = "Data found";
        echo json_encode($accessResponseData);
                  
    }else if($_POST['getBranchList']=="getBranchList" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
        $response["branch_list"] = array();

        $qq = $d->selectRow("access_block_ids","block_master","society_id='$society_id' AND block_id = '$block_id'","ORDER BY block_sort ASC");

        $data1 = mysqli_fetch_array($qq);

        $accessBlockIds = $data1['access_block_ids'];

        if ($accessBlockIds != '') {
            $q = $d->select("block_master","society_id='$society_id' AND (block_id NOT IN ($accessBlockIds) OR block_id = '$block_id')","ORDER BY block_sort ASC");
        }else{
            $q = $d->select("block_master","society_id='$society_id'","ORDER BY block_sort ASC");
        }

        if (mysqli_num_rows($q) > 0) {
            while ($bdata = mysqli_fetch_array($q)) {
                $branch_list = array();
                $branch_list["block_id"]=$bdata['block_id'];
                $branch_list["society_id"]=$bdata['society_id'];
                $branch_list["block_name"]=$bdata['block_name'];
                 array_push($response["branch_list"], $branch_list);
            }
            $response["message"] = "Branch Get success";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Faild to get List...!!";
            $response["status"] = "201";
            echo json_encode($response);
        }

    }else if($_POST['getMembersNew']=="getMembersNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){


        $response["recent_user"] = array();

       $totalMember=  $d->count_data_direct("user_id","unit_master,users_master,block_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.block_id='$block_id' AND users_master.user_status='1' ");

        $totalPendingMember=  $d->count_data_direct("user_id","users_master","delete_status=0 AND society_id='$society_id' AND user_status=0 AND member_status = 0");

        $response["total_pending_members"]=$totalPendingMember.''; 


        $qr = $d->select("users_recent_view,users_master","users_recent_view.user_id=users_master.user_id AND users_recent_view.society_id='$society_id' AND users_recent_view.my_id='$user_id' ","ORDER BY users_recent_view.view_time DESC LIMIT 3");
        while ($rdata = mysqli_fetch_array($qr)) {

            $recent_user = array();
            $recent_user["user_id"]=$rdata['user_id'];
            $recent_user["society_id"]=$rdata['society_id'];
            $recent_user["user_first_name"]=$rdata['user_first_name'];
            $recent_user["user_last_name"]=$rdata['user_last_name'];
            $recent_user["user_full_name"]=$rdata['user_first_name'].' '.$rdata['user_last_name'];
            if ($rdata['user_profile_pic'] != '') {
                $recent_user["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $rdata['user_profile_pic'];
            } else {
                $recent_user["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
            }
             array_push($response["recent_user"], $recent_user);
        }

        if ($block_id!=0 && $block_id!='' && $block_id>0) {
            $append_block_query=" AND block_id='$block_id'";
        }

        $block_data=$d->select("block_master","block_id='$block_id' AND society_id ='$society_id' AND society_id!=0 $append_block_query","ORDER BY block_sort ASC ");
        
        if(mysqli_num_rows($block_data)>0){
            $qss=$d->select("society_master"," society_id ='$society_id'");
            $societyData = mysqli_fetch_array($qss); 
            $hide_user_type = $societyData['hide_user_type'];        
            $society_type = $societyData['society_type'];        
           

            $response["branch"] = array();

            while($data_block_list=mysqli_fetch_array($block_data)) {
            $block_id=$data_block_list['block_id'];

            $memberCount=$d->select("unit_master,users_master","users_master.unit_id=unit_master.unit_id AND unit_master.block_id ='$block_id' AND unit_master.society_id ='$society_id' AND users_master.delete_status=0  AND users_master.member_status=0 AND users_master.society_id ='$society_id' AND users_master.user_status=1","");

            $branch = array(); 

            $branch["block_id"]=$data_block_list['block_id'];
            $branch["society_id"]=$data_block_list['society_id'];
            $branch["branch_name"]=$data_block_list['block_name'];
            $branch["departments"] = array();

            if ($floor_id!=0 && $floor_id!='' && $floor_id>0) {
                $append_query_floor =" AND floor_id='$floor_id'";
              }
            $floor_data=$d->select("floors_master","block_id ='$block_id' $append_query_floor");

              while($data_floor_list=mysqli_fetch_array($floor_data)) {

                    $departments = array(); 

                    $departments["floor_id"]=$data_floor_list['floor_id'];
                    $floors_id=$data_floor_list['floor_id'];
                    $departments["society_id"]=$data_floor_list['society_id'];
                    $departments["block_id"]=$data_floor_list['block_id'];
                    $departments["department_name"]=$data_floor_list['floor_name'];

                    if ($floor_id==$my_floor_id) {
                         $departments["is_my_department"] = true;
                    } else {
                         $departments["is_my_department"] = false;
                    }

                    $departments["employees"] = array();

                     $limitQuery ="";

                  $queryAry=array();

                  if ($other_user_id!=0 && $other_user_id!='') {
                    $append_query2 ="users_master.user_id='$other_user_id'";
                    array_push($queryAry, $append_query2);
                  }


                  if ($get_new_member==1 ) {
                    $limitQuery =" ORDER BY user_id DESC LIMIT 10";
                  }else{
                    $limitQuery =" ORDER BY user_sort ASC";
                  }

                  $appendQuery= implode(" AND ", $queryAry);
                  if ($appendQuery!='') {
                    $appendQuery= " AND ".$appendQuery;
                  }

                    $unit_data=$d->select("unit_master,users_master","users_master.unit_id=unit_master.unit_id AND unit_master.floor_id ='$floors_id' AND unit_master.society_id ='$society_id' AND users_master.delete_status=0  AND users_master.society_id ='$society_id' AND users_master.user_status=1  $appendQuery","$limitQuery");

                    while($data_units_list=mysqli_fetch_array($unit_data)) {
                         
                        $employees = array(); 

                          
                        $user_full_name= $data_units_list['user_full_name'];

                        $units_id=$data_units_list['unit_id'];

                        $employees["user_id"]=$data_units_list['user_id'].'';
                        $employees["user_first_name"]=$data_units_list['user_first_name'].'';
                        $employees["user_last_name"]=$data_units_list['user_last_name'].'';
                        $employees["user_mobile"]=$data_units_list['user_mobile'].'';
                        $employees["user_full_name"]=html_entity_decode($user_full_name).'';
                        $employees["society_id"]=$data_units_list['society_id'];
                        $employees["designation"]=$data_units_list['user_designation'];
                        
                        if ($data_units_list['user_profile_pic'] != '') {
                            $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                        } else {
                            $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                        }

                        array_push($departments["employees"], $employees);
                    }
                    array_push($branch["departments"], $departments);
                }

                array_push($response["branch"], $branch); 
            }

          
            $response["total_members"]=$totalMember.'';
            $response["message"]="No data available";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{

            $response["message"]="Get Member Fail.";
            $response["status"]="201";
            echo json_encode($response);
    
        }

    }else if($_POST['getDepartments']=="getDepartments" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $access_type = $approve_employee_access;

            include "check_access_data.php";

            $response["current_month"] = date('m');

            if ($access_for == 0 || $access_for == 1) {
                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM users_master WHERE delete_status = 0 AND society_id='$society_id' AND user_status = 0 AND member_status = 0 AND block_id IN ('$accessBranchIds')) AS total_pending_members");
                
            }else if($access_for == 2){
                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM users_master WHERE delete_status = 0 AND society_id='$society_id' AND user_status = 0 AND member_status = 0 AND floor_id IN ('$accessDepIds')) AS total_pending_members");
            }

            $countData = mysqli_fetch_array($totalCounts);


            $response["total_pending_members"]=$countData['total_pending_members'].'';

            $response["recent_user"] = array();

            $qq = $d->select("access_floor_master","society_id='$society_id' AND access_floor_id = '$my_floor_id' AND block_id = '$block_id'");

            if (mysqli_num_rows($qq) > 0) {

                $floorIDArray = array();
                
                while($data1 = mysqli_fetch_array($qq)){
                    $idsArray = $data1['floor_id'];

                    array_push($floorIDArray,$idsArray);
                }
            }

            $accessFloorIds = join("','",$floorIDArray);


            if ($accessFloorIds != '') {
                $floor_data = $d->select("floors_master","society_id='$society_id' AND floor_status = '0' AND block_id = '$block_id' AND floor_id NOT IN ('$accessFloorIds')","ORDER BY floor_sort ASC");

                //echo "SELECT * FROM floors_master WHERE society_id='$society_id' AND floor_status = '0' AND floor_id NOT IN ('$accessFloorIds') ORDER BY floor_sort ASC";

            }else{
                $floor_data = $d->select("floors_master","society_id='$society_id' AND floor_status = '0' AND block_id = '$block_id'","ORDER BY floor_sort ASC");
            }

            if (mysqli_num_rows($floor_data) > 0) {
                // code...

                $response["departments"] = array();

                while($data_floor_list=mysqli_fetch_array($floor_data)) {

                    $data_floor_list = array_map("html_entity_decode", $data_floor_list);

                    $departments = array(); 

                    $floor_id=$data_floor_list['floor_id'];

                    $totalEmployeeCount = $d->count_data_direct("user_id","users_master","delete_status = 0 AND society_id='$society_id' AND user_status = 1 AND floor_id = '$floor_id'");


                    $departments["floor_id"]=$floor_id;
                    $departments["society_id"]=$data_floor_list['society_id'];
                    $departments["block_id"]=$data_floor_list['block_id'];
                    $departments["department_name"]=$data_floor_list['floor_name']." (".$totalEmployeeCount.")";

                    if ($floor_id==$my_floor_id) {
                        $departments["is_my_department"] = true;
                    } else {
                        $departments["is_my_department"] = false;
                    }
                    if ($totalEmployeeCount > 0) {
                        array_push($response["departments"], $departments);
                    }
                }
            }
          
            $response["message"]="No data available";
            $response["status"]="200";
            echo json_encode($response);

    }else if($_POST['getDepartmentsNew']=="getDepartmentsNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $access_type = $approve_employee_access;

            include "check_access_data.php";

            $response["current_month"] = date('m');

            if ($access_for == 0 || $access_for == 1) {
                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM users_master WHERE delete_status = 0 AND society_id='$society_id' AND user_status = 0 AND member_status = 0 AND block_id IN ('$accessBranchIds')) AS total_pending_members");
                
            }else if($access_for == 2){
                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM users_master WHERE delete_status = 0 AND society_id='$society_id' AND user_status = 0 AND member_status = 0 AND floor_id IN ('$accessDepIds')) AS total_pending_members");
            }

            $countData = mysqli_fetch_array($totalCounts);


            $response["total_pending_members"]=$countData['total_pending_members'].'';

            $response["recent_user"] = array();

            $qq = $d->select("access_floor_master","society_id='$society_id' AND access_floor_id = '$my_floor_id' AND block_id = '$selected_block_id'");

            if (mysqli_num_rows($qq) > 0) {

                $floorIDArray = array();
                
                while($data1 = mysqli_fetch_array($qq)){
                    $idsArray = $data1['floor_id'];

                    array_push($floorIDArray,$idsArray);
                }
            }

            $accessFloorIds = join("','",$floorIDArray);


            if ($accessFloorIds != '') {
                $floor_data = $d->select("floors_master","society_id='$society_id' AND floor_status = '0' AND block_id = '$selected_block_id' AND floor_id NOT IN ('$accessFloorIds')","ORDER BY floor_sort ASC");

            }else{
                $floor_data = $d->select("floors_master","society_id='$society_id' AND floor_status = '0' AND block_id = '$selected_block_id'","ORDER BY floor_sort ASC");

            }

            if (mysqli_num_rows($floor_data) > 0) {
                // code...

                $response["departments"] = array();

                while($data_floor_list=mysqli_fetch_array($floor_data)) {

                    $data_floor_list = array_map("html_entity_decode", $data_floor_list);

                    $departments = array(); 

                    $floor_id=$data_floor_list['floor_id'];

                    $totalEmployeeCount = $d->count_data_direct("user_id","users_master","delete_status = 0 AND society_id='$society_id' AND user_status = 1 AND floor_id = '$floor_id'");


                    $departments["floor_id"]=$floor_id;
                    $departments["society_id"]=$data_floor_list['society_id'];
                    $departments["block_id"]=$data_floor_list['block_id'];
                    $departments["department_name"]=$data_floor_list['floor_name']." (".$totalEmployeeCount.")";

                    if ($floor_id==$my_floor_id) {
                        $departments["is_my_department"] = true;
                    } else {
                        $departments["is_my_department"] = false;
                    }
                    if ($totalEmployeeCount > 0) {
                        array_push($response["departments"], $departments);
                    }
                }
            }
          
            $response["message"]="No data available";
            $response["status"]="200";
            echo json_encode($response);

    }else if($_POST['getPAMember']=="getPAMember" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

        $employeeAccess = $d->select("app_access_master","access_by_id = '$user_id' AND access_type = '9' AND society_id = '$society_id' AND user_status = '1'");

        if (mysqli_num_rows($employeeAccess) > 0) {

            $response["employees"] = array();

            while($dataAccess = mysqli_fetch_array($employeeAccess)){

                $userId = $dataAccess['access_for_id'];

                $unit_data=$d->selectRow("user_full_name,user_id","users_master","user_id = '$userId' AND floor_id = '$floor_id'");

                if (mysqli_num_rows($unit_data) > 0) {

                    $data_units_list=mysqli_fetch_array($unit_data);
                                     
                    $employees = array(); 
         
                    $user_full_name= $data_units_list['user_full_name'];

                    $units_id=$data_units_list['unit_id'];

                    $employees["user_id"]=$data_units_list['user_id'].'';
                    $employees["user_full_name"]=html_entity_decode($user_full_name).'';

                    array_push($response["employees"], $employees);
                }
            }
            $response["message"]="Employee list";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{
            $response["message"]="No Employees Found";
            $response["status"]="201";
            echo json_encode($response);
        }

    }else if($_POST['getMembersByDepartment']=="getMembersByDepartment" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){


        $unit_data=$d->select("unit_master,users_master","users_master.unit_id=unit_master.unit_id AND users_master.floor_id ='$floor_id' AND users_master.delete_status=0  AND users_master.society_id ='$society_id' AND users_master.user_status=1","ORDER BY user_first_name ASC");

        if (mysqli_num_rows($unit_data) > 0) {

            $response["employees"] = array();

            while($data_units_list=mysqli_fetch_array($unit_data)) {
                             
                $employees = array(); 
     
                $user_full_name= $data_units_list['user_full_name'];

                $units_id=$data_units_list['unit_id'];

                $employees["user_id"]=$data_units_list['user_id'].'';
                $employees["unit_id"]=$data_units_list['unit_id'].'';
                $employees["block_id"]=$data_units_list['block_id'].'';
                $employees["floor_id"]=$data_units_list['floor_id'].'';
                $employees["user_first_name"]=$data_units_list['user_first_name'].'';
                $employees["user_last_name"]=$data_units_list['user_last_name'].'';
                $employees["user_mobile"]=$data_units_list['user_mobile'].'';
                $employees["user_full_name"]=html_entity_decode($user_full_name).'';
                $employees["society_id"]=$data_units_list['society_id'];
                $employees["designation"]=$data_units_list['user_designation'];
                
                if ($data_units_list['user_profile_pic'] != '') {
                    $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                } else {
                    $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }

                array_push($response["employees"], $employees);
            }

            $response["message"]="Employee list";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{
            $response["message"]="No Employees Found";
            $response["status"]="201";
            echo json_encode($response);
        }

    }else if($_POST['getMembersByDepartment']=="getMembersByDepartmentMeeting" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){


        $unit_data=$d->select("unit_master,users_master","users_master.unit_id=unit_master.unit_id AND users_master.floor_id ='$floor_id' AND users_master.delete_status=0  AND users_master.society_id ='$society_id' AND users_master.user_status=1 AND users_master.user_id != '$user_id'","ORDER BY user_first_name ASC");

        if (mysqli_num_rows($unit_data) > 0) {

            $response["employees"] = array();

            while($data_units_list=mysqli_fetch_array($unit_data)) {
                             
                $employees = array(); 
     
                $user_full_name= $data_units_list['user_full_name'];

                $units_id=$data_units_list['unit_id'];

                $employees["user_id"]=$data_units_list['user_id'].'';
                $employees["unit_id"]=$data_units_list['unit_id'].'';
                $employees["block_id"]=$data_units_list['block_id'].'';
                $employees["floor_id"]=$data_units_list['floor_id'].'';
                $employees["user_first_name"]=$data_units_list['user_first_name'].'';
                $employees["user_last_name"]=$data_units_list['user_last_name'].'';
                $employees["user_mobile"]=$data_units_list['user_mobile'].'';
                $employees["user_full_name"]=html_entity_decode($user_full_name).'';
                $employees["society_id"]=$data_units_list['society_id'];
                $employees["designation"]=$data_units_list['user_designation'];
                
                if ($data_units_list['user_profile_pic'] != '') {
                    $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                } else {
                    $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }

                array_push($response["employees"], $employees);
            }

            $response["message"]="Employee list";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{
            $response["message"]="No Employees Found";
            $response["status"]="201";
            echo json_encode($response);
        }

    }else if($_POST['getMembersByDepartment']=="getMembersByDepartmentNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){


        $unit_data=$d->select("unit_master,users_master","users_master.unit_id=unit_master.unit_id AND users_master.floor_id ='$floor_id' AND users_master.delete_status=0  AND users_master.society_id ='$society_id' AND users_master.user_status=1","ORDER BY user_sort ASC");

        if (mysqli_num_rows($unit_data) > 0) {

            $response["employees"] = array();

            while($data_units_list=mysqli_fetch_array($unit_data)) {
                             
                $employees = array(); 
     
                $user_full_name= $data_units_list['user_full_name'];

                $units_id=$data_units_list['unit_id'];

                $employees["user_id"]=$data_units_list['user_id'].'';
                $employees["unit_id"]=$data_units_list['unit_id'].'';
                $employees["block_id"]=$data_units_list['block_id'].'';
                $employees["floor_id"]=$data_units_list['floor_id'].'';
                $employees["user_first_name"]=$data_units_list['user_first_name'].'';
                $employees["user_last_name"]=$data_units_list['user_last_name'].'';
                $employees["user_mobile"]=$data_units_list['user_mobile'].'';
                $employees["user_full_name"]=html_entity_decode($user_full_name).'';
                $employees["society_id"]=$data_units_list['society_id'];
                $employees["designation"]=$data_units_list['user_designation'];
                
                if ($data_units_list['user_profile_pic'] != '') {
                    $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                } else {
                    $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }

                array_push($response["employees"], $employees);
            }

            $response["message"]="Employee list";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{
            $response["message"]="No Employees Found";
            $response["status"]="201";
            echo json_encode($response);
        }

    }else if($_POST['getPendingMembers']=="getPendingMembers" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

        $access_type = $approve_employee_access;

        include "check_access_data.php";

        if ($selected_floor_id == '0') {
            switch ($access_for) {
                case '0':
                case '1':
                    $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                    $LIMIT_DATA = " LIMIT 100";
                    break;
                case '2':
                    $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                    $LIMIT_DATA = " LIMIT 100";
                    break;
                default:
                    // code...
                    break;
            }
        }else{
            $appendQuery = " AND users_master.floor_id = $selected_floor_id";
        }

        $response['modification_access'] = $modification_access;

        $membersQry=$d->select("users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id 
            AND users_master.delete_status = 0 
            AND users_master.block_id = block_master.block_id 
            AND users_master.unit_id = unit_master.unit_id 
            AND users_master.floor_id = floors_master.floor_id 
            AND users_master.unit_id = unit_master.unit_id 
            AND users_master.member_status = 0 
            AND users_master.user_status = 0 
            AND users_master.society_id='$society_id' $appendQuery",
            "ORDER BY users_master.user_id DESC $LIMIT_DATA");

        if (mysqli_num_rows($membersQry) > 0) {

            $response['pending_members'] = array();

            while ($data = mysqli_fetch_array($membersQry)) {

                $employee = array();

                $employee['user_id'] = $data['user_id'];
                $employee['society_id'] = $data['society_id'];
                $employee['block_name'] = $data['block_name'];
                $employee['floor_name'] = $data['floor_name'];
                $employee['user_full_name'] = $data['user_full_name'];
                $employee['user_mobile'] = $data['country_code']." ".$data['user_mobile'];
                $employee['user_designation'] = $data['user_designation'];
                
                if ($data['user_profile_pic'] != '') {
                    $employee["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                } else {
                    $employee["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }

                array_push($response['pending_members'],$employee);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{
            $response["message"]="No Pending Requests.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['approvePendingEmployee']=="approvePendingEmployee" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


        $m->set_data('user_status',"1");

        $a = array(
            'user_status'=>$m->get_data('user_status'),
        );

        $q = $d->update("users_master",$a,"society_id = '$society_id' AND user_id = '$user_id'");

        if ($q == true) {

            $unitQry = $d->selectRow("unit_id","users_master","user_id = '$user_id'");

            $au =array('unit_status'=>'1',);

            $unitData = mysqli_fetch_array($unitQry);

            $qunit=$d->update("unit_master",$au,"unit_id = '$unitData[unit_id]'");

            $response["message"] = "Employee Approved";
            $response["status"] = "200";
            echo json_encode($response);
        }else{
            $response["message"] = "Employee Approval Failed";
            $response["status"] = "201";
            echo json_encode($response);
        }        
    }else if($_POST['rejectPendingEmployee']=="rejectPendingEmployee" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $q = $d->delete("users_master","society_id = '$society_id' AND user_id = '$user_id'");

        if ($q == true) {
            $response["message"] = "Employee Rejected";
            $response["status"] = "200";
            echo json_encode($response);
        }else{
            $response["message"] = "Employee Rejection Failed";
            $response["status"] = "201";
            echo json_encode($response);
        }        
    } else if($_POST['getBlocks']=="getBlocks" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

        $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0");

        if(mysqli_num_rows($block_data)>0){

            $response["block"] = array();

            
            while($data_block_list=mysqli_fetch_array($block_data)) {

                $block = array();

                $block["block_id"]=$data_block_list['block_id'];
                $block["society_id"]=$data_block_list['society_id'];
                $block["block_name"]=$data_block_list['block_name'];
                $block["block_status"]=$data_block_list['block_status'];
                
                array_push($response["block"], $block); 
            }

            $response["message"]="Get Block success.";
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]="Get Block Fail.";
            $response["status"]="201";
            echo json_encode($response);
        
    }


}else if($_POST['getFloorandUnit']=="getFloorandUnit" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($block_id, FILTER_VALIDATE_INT) == true ){

        $block_data=$d->select("floors_master","society_id ='$society_id' AND society_id!=0 AND block_id ='$block_id' AND block_id!=0");


        if(mysqli_num_rows($block_data)>0){
            $response["floors"] = array();

            while($data_floor_list=mysqli_fetch_array($block_data)) {

                $floors = array(); 
                $floors["floor_id"]=$data_floor_list['floor_id'];
                $floors_id=$data_floor_list['floor_id'];
                $floors["society_id"]=$data_floor_list['society_id'];
                $floors["block_id"]=$data_floor_list['block_id'];
                $floors["floor_name"]=$data_floor_list['floor_name'];
                $floors["floor_status"]=$data_floor_list['floor_status'];
                
                $floors["units"] = array();

                $unit_data=$d->select("unit_master","floor_id ='$floors_id'");

                while($data_units_list=mysqli_fetch_array($unit_data)) {

                    $units = array(); 

                    $units["unit_id"]=$data_units_list['unit_id'];
                    $unit_id=$data_units_list['unit_id'];

                    $units["society_id"]=$data_units_list['society_id'];
                    $units["floor_id"]=$data_units_list['floor_id'];
                    $units["unit_name"]=$data_units_list['unit_name'];
                    $units["unit_status"]=$data_units_list['unit_status'];
                    $units["unit_type"]=$data_units_list['unit_type'];

                    array_push($floors["units"], $units);


                }

                array_push($block["floors"], $floors);
                array_push($response["floors"], $floors);

            }

            $response["message"]="Get Block success.";
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]="Get Block Fail.";
            $response["status"]="201";
            echo json_encode($response);
        
    }


    }else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }
}
else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
?>