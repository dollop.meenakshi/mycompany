<?php
session_start();

$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../apAdmin/lib/dao.php';
include_once '../apAdmin/lib/sms_api.php';
include_once '../apAdmin/lib/model.php';
include_once '../apAdmin/fcm_file/admin_fcm.php';
include_once '../apAdmin/fcm_file/gaurd_fcm.php';
include_once '../apAdmin/fcm_file/resident_fcm.php';

$d = new dao();
$m = new model();
$smsObj = new sms_api();

$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$default_time_zone=$d->getTimezone($_REQUEST['society_id']);
date_default_timezone_set($default_time_zone);
header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
$base_url=$m->base_url();
$keydb = $m->api_key();
extract(array_map("test_input", $headers));
$key = $_SERVER['HTTP_KEY'];  
$auth_user_name= $_SERVER['PHP_AUTH_USER'];
$auth_password= $_SERVER['PHP_AUTH_PW'];
$auth_check = $d->check_auth($auth_user_name,$auth_password);

if ($_REQUEST['language_id']!="" && $lang_type="user") {

	$society_id = $_REQUEST['society_id'];
	$language_id = $_REQUEST['language_id'];
	$q=$d->select("society_master","society_id='$society_id'");
    $bData=mysqli_fetch_array($q);
    $country_id  = $bData['country_id'];
    $society_id  = $bData['society_id'];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/language_controller_web.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
              "getLanguageValues=getLanguageValues&country_id=$country_id&society_id=$society_id&language_id=$language_id");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'key: bmsapikey'
    ));

    $server_output = curl_exec($ch);

    curl_close ($ch);
    $server_output=json_decode($server_output,true);

    $arrayCount= count($server_output['language_key']);

    $myFile = "../img/$language_id.xml";
    $fh = fopen($myFile, 'w') or die("can't open file");
    $rss_txt = "";
    $rss_txt .= '<?xml version="1.0" encoding="utf-8"?>';
    $rss_txt .= "<rss version='2.0'>";
        
            $rss_txt .= '<string>';
        for ($i1=0; $i1 < $arrayCount ; $i1++) { 
          $key_value = str_replace('&', '&amp;', $server_output['language_key'][$i1]['key_value']);

          $keyName  = $server_output['language_key'][$i1]['key_name'];
          

            $rss_txt .= "<$keyName>$key_value</$keyName>";
        }
            $rss_txt .= '</string>';
    $rss_txt .= '</rss>';

    fwrite($fh, $rss_txt);
    fclose($fh);


    $response["message"] = "Language Updated";
    $response["status"] = "200";
    echo json_encode($response);
    
} else if ($_REQUEST['language_id']!="" && $lang_type="gatekeeer") {

    $society_id = $_REQUEST['society_id'];
    $language_id = $_REQUEST['language_id'];
    $q=$d->select("society_master","society_id='$society_id'");
    $bData=mysqli_fetch_array($q);
    $country_id  = $bData['country_id'];
    $society_id  = $bData['society_id'];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/language_controller_web.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
              "getLanguageValuesGuard=getLanguageValuesGuard&country_id=$country_id&society_id=$society_id&language_id=$language_id");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'key: bmsapikey'
    ));

    $server_output = curl_exec($ch);

    curl_close ($ch);
    $server_output=json_decode($server_output,true);

    $arrayCount= count($server_output['language_key']);

    $language_idFileName= $language_id."Guard.xml";
    $myFile = "../img/$language_idFileName";
    $fh = fopen($myFile, 'w') or die("can't open file");
    $rss_txt = "";
    $rss_txt .= '<?xml version="1.0" encoding="utf-8"?>';
    $rss_txt .= "<rss version='2.0'>";
        
            $rss_txt .= '<string>';
        for ($i1=0; $i1 < $arrayCount ; $i1++) { 
          $key_value = str_replace('&', '&amp;', $server_output['language_key'][$i1]['key_value']);

          $keyName  = $server_output['language_key'][$i1]['key_name'];
          

            $rss_txt .= "<$keyName>$key_value</$keyName>";
        }
            $rss_txt .= '</string>';
    $rss_txt .= '</rss>';

    fwrite($fh, $rss_txt);
    fclose($fh);


    $response["message"] = "Language Updated";
    $response["status"] = "200";
    echo json_encode($response);
}



?>