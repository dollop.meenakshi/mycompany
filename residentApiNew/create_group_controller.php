<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    $response = array();
    extract(array_map("test_input", $_POST));
    $temDate = date("Y-m-d h:i A");


    if ($key == $keydb && $auth_check=='true') {
        if ($_POST['createGroupProfile'] == "createGroupProfile" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true ) {

            $uploadedFile = $_FILES["group_icon"]["tmp_name"];
            $ext = pathinfo($_FILES['group_icon']['name'], PATHINFO_EXTENSION);
            
            if (file_exists($uploadedFile)) {

                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $group_name;
                $dirPath = "../img/users/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];

                if ($imageWidth > 1000) {
                    $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }


                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    default:

                        break;
                }
                
                $group_icon = $newFileName . "_group." . $ext;

                $group_icon=$base_url.'img/users/'.$group_icon;
                $response["message"] = $group_icon;
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
        } else if (isset($getMemberListGroup) && $getMemberListGroup=='getMemberListGroup' && filter_var($society_id, FILTER_VALIDATE_INT) == true) { 

            if ($unit_id!='' && $unit_id!=0) {
                
                $quc=$d->select("unit_master","unit_id='$unit_id' AND chat_access_denied=1");
                
                if (mysqli_num_rows($quc)>0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }
            
            $ids = join("','",$_POST['userIds']);        

            $q3=$d->select("unit_master,block_master,users_master,floors_master","block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id' AND users_master.floor_id = '$floor_id' AND users_master.user_id NOT IN ('$ids')","ORDER BY block_master.block_sort ASC");

            if (mysqli_num_rows($q3)>0) {
                
                $response["member"]=array();

                while($data=mysqli_fetch_array($q3)) {

                    $member = array(); 
                    $member["user_id"]=$data['user_id'];
                    $member["user_full_name"]=$data['user_full_name'];
                    $member["user_first_name"]=$data['user_first_name'];
                    $member["user_last_name"]=$data['user_last_name'];
                    $member["gender"]=$data['gender'];
                    $member["company_name"]=html_entity_decode($data['user_designation']);
                    $member["user_type"]=$data['user_type'];
                    $member["block_name"]=$data['block_name'];
                    $member["floor_name"]=$data['floor_name'];
                    $member["unit_name"]=$data['unit_name'];
                    $member["floor_id"]=$data['floor_id'];
                    $member["unit_id"]=$data['unit_id'];
                    $member["unit_status"]=$data['unit_status'];
                    $member["user_status"]=$data['user_status'];
                    $member["member_status"]=$data['member_status'];
                    $member["user_mobile"]=$data['user_mobile'];
                    $member["public_mobile"]=$data['public_mobile'];
                    $member["member_date_of_birth"]="".$data['member_date_of_birth'];
                    $member["alt_mobile"] = $data['alt_mobile'];

                    $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
                    $member["owner_name"]=$data['owner_name']."";
                    $member["owner_email"]=$data['owner_email']."";
                    $member["owner_mobile"]=$data['owner_mobile']."";
                    array_push($response["member"], $member); 

                }

                $response["message"]="Member List";
                $response["status"]="200";

                echo json_encode($response);
            }else{
                $response["message"]="No Member Found!";
                $response["status"]="201";
                echo json_encode($response);
            }

        } else  if ($_POST['createGroup'] == "createGroup" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true ) {

            $uploadedFile = $_FILES["group_icon"]["tmp_name"];
            $ext = pathinfo($_FILES['group_icon']['name'], PATHINFO_EXTENSION);
            
            if (file_exists($uploadedFile)) {

                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $group_name;
                $dirPath = "../img/users/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                
                if ($imageWidth > 1000) {
                    $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    default:

                        break;
                }
                
                $group_icon = $newFileName . "_group." . $ext;
            } else {
                $group_icon = '';
            }

            $m->set_data('society_id', $society_id);
            $m->set_data('group_name', $group_name);
            $m->set_data('group_icon', $group_icon);
            $m->set_data('created_by', $user_id);
            $m->set_data('block_id', $block_id);
            $m->set_data('created_date', $temDate);

            $a = array(
                'group_name' => $m->get_data('group_name'),
                'group_icon' => $m->get_data('group_icon'),
                'society_id' => $m->get_data('society_id'),
                'created_by' => $m->get_data('created_by'),
                'block_id' => $m->get_data('block_id'),
                'created_date' => $m->get_data('created_date'),
            );

            $q = $d->insert("chat_group_master", $a);
            $group_id = $con->insert_id;

            if ($q == TRUE) {


                $a1 = array(
                    'society_id' => $society_id,
                    'group_id' => $group_id,
                    'msg_by' => $user_id,
                    'msg_by_name' => "",
                    'msg_data' => "Group created by $user_name",
                    'msg_status' => $m->get_data('msg_status'),
                    'send_by' => 0,
                    'sent_to' => 0,
                    'msg_date' => date('Y-m-d H:i:s')
                );

                $uidmid = explode("~",$member_id);
                $uidmid = join("','",$uidmid);

                $q = $d->insert('chat_master_group', $a1);

                $member_id = $member_id . '~' . $user_id;
                $user_id_array = explode("~", $member_id);


                for ($i = 0; $i < count($user_id_array); $i++) {

                    $notiAry = array(
                        'group_id' => $group_id,
                        'society_id' => $m->get_data('society_id'),
                        'user_id' => $user_id_array[$i]
                    );
                    $d->insert("chat_group_member_master", $notiAry);
                }

                $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$uidmid')");
                $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$uidmid')");
                
                $title = "New Group Created";
                $description = "$group_name";

                $dataAry = array(
                    "group_id"=>$group_id,
                    "user_id"=>$user_id,
                    "group_name"=>$group_name,
                    "group_icon"=>$base_url."img/users/".$group_icon,
                    "member_count"=>count($uidmid),
                );

                $dataJson = json_encode($dataAry);

                $nResident->noti("new_chat_group","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                $nResident->noti_ios("new_chat_group","",$society_id,$fcmArrayIosPA,$title,$description,$dataJson);

                $a2 = array(
                    'society_id' => $society_id,
                    'group_id' => $group_id,
                    'msg_by' => $user_id,
                    'msg_by_name' => "",
                    'msg_data' => "$member_name Joined the group",
                    'msg_status' => $m->get_data('msg_status'),
                    'send_by' => 0,
                    'sent_to' => 0,
                    'msg_date' => date('Y-m-d H:i:s')
                );

                $q = $d->insert('chat_master_group', $a2);

                $d->insert_myactivity($user_id,"$society_id","0","$user_name","$group_name chat group created","Chat_1xxxhdpi.png");
                if ($group_icon!="") {
                   $group_icon = $base_url."img/users/".$group_icon;
                } else {
                   $group_icon =  "";
                }

                $response["message"] = $group_icon;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Something went wrong.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['editGroup'] == "editGroup" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            $m->set_data('society_id', $society_id);
            $m->set_data('group_name', $group_name);

            $uploadedFile = $_FILES["group_icon"]["tmp_name"];
            $ext = pathinfo($_FILES['group_icon']['name'], PATHINFO_EXTENSION);
        
            if (file_exists($uploadedFile)) {

                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $group_name;
                $dirPath = "../img/users/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                
                if ($imageWidth > 1000) {
                    $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }

                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "_group." . $ext);
                        break;

                    default:
                        break;
                }

                $group_icon = $newFileName . "_group." . $ext;

                $m->set_data('group_icon', $group_icon);

                $a = array(
                    'group_name' => $m->get_data('group_name'),
                    'group_icon' => $m->get_data('group_icon'),
                );
            } else {
                $a = array(
                    'group_name' => $m->get_data('group_name'),
                );
            }

            $q = $d->update("chat_group_master", $a,"group_id='$group_id' AND created_by='$user_id'");

            if ($q == TRUE) {

                $a1 = array(
                    'society_id' => $society_id,
                    'group_id' => $group_id,
                    'msg_by' => $user_id,
                    'msg_by_name' => "",
                    'msg_data' => "$user_name updated group details",
                    'msg_status' => $m->get_data('msg_status'),
                    'send_by' => 0,
                    'sent_to' => 0,
                    'msg_date' => date('Y-m-d H:i:s')
                );

                $q = $d->insert('chat_master_group', $a1);

                $response["message"] = "Group Details Updated";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Something went wrong.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['deleteGroup'] == "deleteGroup" && $group_id != '' && filter_var($group_id, FILTER_VALIDATE_INT) == true ) {

            $q = $d->delete("chat_group_master", "group_id = '$group_id'");

            if ($q == TRUE) {
                $q1 = $d->delete("chat_group_member_master", "group_id = '$group_id'");
                $q2 = $d->delete("chat_master_group", "group_id = '$group_id'");

                $d->insert_myactivity($user_id,"$society_id","0","$user_name","Chat group created","Chat_1xxxhdpi.png");


                $response["message"] = "Group Deleted Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something went wrong.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['leaveGroup'] == "leaveGroup" && $group_id != '' && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($group_id, FILTER_VALIDATE_INT) == true ) {

            $q = $d->delete("chat_group_member_master", "group_id = '$group_id' AND user_id = '$user_id'");

            if ($q == TRUE) {
                if ($admin_name!='') {
                    $a1 = array(
                        'society_id' => $society_id,
                        'group_id' => $group_id,
                        'msg_by' => $user_id,
                        'msg_by_name' => "",
                        'msg_data' => "$admin_name Removed $user_name",
                        'msg_status' => $m->get_data('msg_status'),
                        'send_by' => 0,
                        'sent_to' => 0,
                        'msg_date' => date('Y-m-d H:i:s')
                    );
                } else {
                    $a1 = array(
                        'society_id' => $society_id,
                        'group_id' => $group_id,
                        'msg_by' => $user_id,
                        'msg_by_name' => "",
                        'msg_data' => "$user_name left the group",
                        'msg_status' => $m->get_data('msg_status'),
                        'send_by' => 0,
                        'sent_to' => 0,
                        'msg_date' => date('Y-m-d H:i:s')
                    );

                }

                $q = $d->insert('chat_master_group', $a1);

                $response["message"] = "Group Left Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something went wrong.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['addMoreMember'] == "addMoreMember" && $group_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($group_id, FILTER_VALIDATE_INT) == true ) {

            $uidmid = explode("~",$member_id);
            $uidmid = join("','",$uidmid);


            $user_id_array = explode("~", $member_id);

            for ($i = 0; $i < count($user_id_array); $i++) {

                $notiAry = array(
                    'group_id' => $group_id,
                    'society_id' => $society_id,
                    'user_id' => $user_id_array[$i]
                );

                $d->insert("chat_group_member_master", $notiAry);

            }
            
            $a1 = array(
                'society_id' => $society_id,
                'group_id' => $group_id,
                'msg_by' => $user_id,
                'msg_by_name' => "",
                'msg_data' => "$member_name Joined the group",
                'msg_status' => $m->get_data('msg_status'),
                'send_by' => 0,
                'sent_to' => 0,
                'msg_date' => date('Y-m-d H:i:s')
            );

            $q = $d->insert('chat_master_group', $a1);

            $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$uidmid')");
            $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$uidmid')");
            
            $groupNameQry = $d->selectRow("group_name","chat_group_master","group_id = '$group_id'");
            $groupData = mysqli_fetch_array($groupNameQry);

            $title = "You were added to the group";
            $description = $groupData['group_name'];

            $totalMember = $d->count_data_direct("chat_group_member_id","chat_group_member_master","group_id = '$group_id' AND active_status = 0 AND chat_group_member_delete = 0");

            $dataAry = array(
                "group_id"=>$group_id,
                "user_id"=>$user_id,
                "group_name"=>$groupData['group_name'],
                "group_icon"=>$base_url."img/users/".$groupData['group_icon'],
                "member_count"=>$totalMember,
            );

            $dataJson = json_encode($dataAry);

            $nResident->noti("new_chat_group","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
            $nResident->noti_ios("new_chat_group","",$society_id,$fcmArrayIosPA,$title,$description,$dataJson);

            $response["message"] = "Group Member Added Successfully.";
            $response["status"] = "200";
            echo json_encode($response);
        }  else if ($_POST['deleteMultiChat'] == "deleteMultiChat" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true ) {

            $chatIds= explode("~", $chat_id);
            $isSingleChat= explode("~", $is_group);

            for ($i=0; $i < $chatIds; $i++) { 
                if ($isSingleChat[$i]==TRUE) {
                    $q = $d->delete("chat_group_member_master", "group_id = '$chatIds[$i]' AND user_id = '$user_id'");
                    $a1 = array(
                        'society_id' => $society_id,
                        'group_id' => $chatIds[$i],
                        'msg_by' => $user_id,
                        'msg_by_name' => "",
                        'msg_data' => "$user_name left the group",
                        'send_by' => 0,
                        'sent_to' => 0,
                        'msg_date' => date('Y-m-d H:i:s')
                    );
                    $d->insert('chat_master_group', $a1);
                } else {
                    // $d->delete("chat_master", "chat_id='$chat_id' AND chat_id!=0");
                }
            }

            $q = $d->delete("chat_group_master", "group_id = '$group_id'");

            if ($q == TRUE) {

                $response["message"] = "Deleted Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something went wrong.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
