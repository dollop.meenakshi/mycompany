<?php
// error_reporting(true);
header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
extract(array_map("test_input", $_POST));
$code = $_POST['code'];
$mid = $_POST['MID'];
$orderid = $_POST['ORDER_ID'];
$amount = $_POST['AMOUNT'];
$m_key = $_POST['M_KEY'];
$payerAccount = $_POST['payerAccount'];
$paymentMode = $_POST['paymentMode'];
$txnTokenMobile = $_POST['txnTokenMobile'];
$host_url = "https://securegw.paytm.in/";
$callback_url = "https://securegw.paytm.in/theia/paytmCallback";

$code = stripslashes($code);
$mid = stripslashes($mid);
$orderid = stripslashes($orderid);
$amount = stripslashes($amount);
$host_url = stripslashes($host_url);
$callback_url = stripslashes($callback_url);

$response = array();


if ($mid != "" && $orderid!="" && $amount>0 && strlen($orderid)>10 && $paymentMode!="") {
/*
 * import checksum generation utility
 * You can get this utility from https://developer.paytm.com/docs/checksum/
 */
	// if ($paymentMode!="UPI") {
		$websiteName = 'DEFAULT';
		if (strpos($host_url, '-stage') !== FALSE) {
			$websiteName = 'WEBSTAGING';
		}
		require_once "PaytmChecksum.php";

		$Merchant_key = $m_key;

		$paytmParams = array();

		$paytmParams["body"] = array(
			"requestType" => "Payment",
			"mid" => $mid,
			"websiteName" => $websiteName,
			"orderId" => $orderid,
			"callbackUrl" => $callback_url . "?ORDER_ID=".$orderid,
			"txnAmount" => array(
				"value" => $amount,
				"currency" => "INR",
			),
			"userInfo" => array(
				"custId" => $orderid,
			),
		);

	/*
	 * Generate checksum by parameters we have in body
	 * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
	 */
		$checksum = PaytmChecksum::generateSignature(json_encode($paytmParams["body"], JSON_UNESCAPED_SLASHES), $Merchant_key);

		$paytmParams["head"] = array(
			"signature" => $checksum,
		);

		$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);

		$url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=$mid&orderId=$orderid";

		
		// $url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=$mid&orderId=$orderid";

		$ch = curl_init($url);
		
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		$responsec = curl_exec($ch);
		if ($paymentMode=="FULL") {
			echo $responsec;
			exit();
		}
		$server_output=json_decode($responsec,true);
		$txnToken = $server_output['body']['txnToken'];
	// } else {
	// 	$txnToken = $txnTokenMobile;
	// }
	if ($txnToken!="") {
		
    	$paytmParamsNew = array();

    	if ($payerAccount!="") {
    		$channelCode = "collect";
    	}

		$paytmParamsNew["body"] = array(
		    "requestType" => "NATIVE",
		    "mid"         => $mid,
		    "orderId"     => $orderid,
		    "paymentMode" => $paymentMode,
		    "channelCode" => $channelCode,
		    "payerAccount" => $payerAccount,
		    "cardInfo"    => "",
		    "authMode"    => "",
		);

		$paytmParamsNew["head"] = array(
		    "txnToken"    => $txnToken
		);

		$post_data = json_encode($paytmParamsNew, JSON_UNESCAPED_SLASHES);

		$url = "https://securegw.paytm.in/theia/api/v1/processTransaction?mid=$mid&orderId=$orderid";
		
		// $url = "https://securegw-stage.paytm.in/theia/api/v1/processTransaction?mid=$mid&orderId=$orderid";



		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json")); 
		$responsePaytm = curl_exec($ch);
		// echo $response;
		$server_output_new=json_decode($responsePaytm,true);
		$response["resultCode"]=$server_output_new['body']['resultInfo']['resultCode'];
        $response["resultMsg"]=$server_output_new['body']['resultInfo']['resultMsg'];
        $response["deepLink"]=$server_output_new['body']['deepLinkInfo']['deepLink'];
        $response["orderId"]=$server_output_new['body']['deepLinkInfo']['orderId'];
        $response["txnToken"]=$txnToken;
        $response["status"]="200";
        echo json_encode($response);

		// print_r($response);
	}
} else {
	$response["message"]="Someting went wrong";
    $response["status"]="201";
    echo json_encode($response);
}
?>