<?php
header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
extract(array_map("test_input", $_POST));
$code = $_POST['code'];
$mid = $_POST['MID'];
$orderid = $_POST['ORDER_ID'];
$amount = $_POST['AMOUNT'];
$m_key = $_POST['M_KEY'];
$upi_id = $_POST['payerAccount'];
$paymentMode = $_POST['paymentMode'];
$host_url = "https://securegw.paytm.in/";
$callback_url = "https://securegw.paytm.in/theia/paytmCallback";

$response = array();

if ($mid != "" && $orderid!="" && $m_key!="" && strlen($orderid)>10 && $upi_id!="") {

    $websiteName = 'DEFAULT';
    if (strpos($host_url, '-stage') !== FALSE) {
        $websiteName = 'WEBSTAGING';
    }
    require_once "PaytmChecksum.php";

    $Merchant_key = $m_key;

    $paytmParams = array();

    $paytmParams["body"] = array(
        "requestType" => "Payment",
        "mid" => $mid,
        "websiteName" => $websiteName,
        "orderId" => $orderid,
        "callbackUrl" => $callback_url . "?ORDER_ID=".$orderid,
        "txnAmount" => array(
            "value" => $amount,
            "currency" => "INR",
        ),
        "userInfo" => array(
            "custId" => $orderid,
        ),
    );

/*
 * Generate checksum by parameters we have in body
 * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
 */
    $checksum = PaytmChecksum::generateSignature(json_encode($paytmParams["body"], JSON_UNESCAPED_SLASHES), $Merchant_key);

    $paytmParams["head"] = array(
        "signature" => $checksum,
    );

    $post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);

    $url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=$mid&orderId=$orderid";

    
    // $url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=$mid&orderId=$orderid";

    $ch = curl_init($url);
    
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    $responsec = curl_exec($ch);
    
    $server_output=json_decode($responsec,true);
    if ($server_output['body']['txnToken']!="") {
        $txtToken = $server_output['body']['txnToken'];
        $paytmParamsNew = array();

        $paytmParamsNew["body"] = array(
            "vpa"      => $upi_id
        );

        $paytmParamsNew["head"] = array(
            "tokenType"     => "TXN_TOKEN",
            'token'         => $txtToken,
        );

        $post_dataNew = json_encode($paytmParamsNew, JSON_UNESCAPED_SLASHES);

      
        $urlNew = "https://securegw.paytm.in/theia/api/v1/vpa/validate?mid=$mid&orderId=$orderid";

        $ch = curl_init($urlNew);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_dataNew);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json"
        ));
        $responseNew = curl_exec($ch);

        $server_output_new=json_decode($responseNew,true);
        // echo $responseNew;
        $response["vpa"]=$server_output_new['body']['vpa'].'';
        $response["valid"]=$server_output_new['body']['valid'].'';
        $response["message"]=$server_output_new['body']['resultInfo']['resultMsg'];
        $response["resultCode"]=$server_output_new['body']['resultInfo']['resultCode'];
        $response["txtToken"]="$txtToken";
        $response["status"]="200";
        echo json_encode($response);
    }else {
        $response["message"]="Service Not Unavailable !";
        $response["status"]="201";
        echo json_encode($response);
    }


} else {
    $response["message"]="Someting went wrong ! ";
    $response["status"]="201";
    echo json_encode($response);
}