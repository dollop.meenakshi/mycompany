<?php
header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
extract(array_map("test_input", $_POST));
$mid = $_POST['MID'];
$orderid = $_POST['ORDER_ID'];
$m_key = $_POST['M_KEY'];
if ($mid != "" && $orderid!="" && $m_key!="" && strlen($orderid)>10 ) {

/**
* import checksum generation utility
* You can get this utility from https://developer.paytm.com/docs/checksum/
*/
require_once("PaytmChecksum.php");


$paytmParams = array();

$paytmParams["body"] = array(
    "mid"           => $mid
);

/*
* Generate checksum by parameters we have in body
* Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
*/
$checksum = PaytmChecksum::generateSignature(json_encode($paytmParams["body"], JSON_UNESCAPED_SLASHES), "$m_key");

$paytmParams["head"] = array(
    "tokenType"    => "CHECKSUM",
    "token"    => $checksum
);

$post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);

/* for Staging */
$url = "https://securegw-stage.paytm.in/theia/api/v1/fetchUPIOptions?mid=$mid&orderId=$orderid";

/* for Production */
// $url = "https://securegw.paytm.in/theia/api/v1/fetchUPIOptions?mid={mid}&orderId=ORDERID_98765";

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json")); 
$response = curl_exec($ch);
echo $response;

} else {
    $response["message"]="Someting went wrong";
    $response["status"]="201";
    echo json_encode($response);
}