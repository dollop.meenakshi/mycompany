<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

	
		$response = array();
		extract(array_map("test_input", $_POST));
		$today = date('Y-m-d');
		$todayTime = date('Y-m-d H:i:s');
		
		if (isset($sendFcm) && $sendFcm == 'sendFcm' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_mobile, FILTER_VALIDATE_INT) == true) {

			$user_mobile = mysqli_real_escape_string($con, $user_mobile);
		
		    $q = $d->select("users_master,block_master,floors_master,unit_master",
					"users_master.user_mobile ='$user_mobile' AND users_master.user_mobile !=0  AND users_master.block_id=block_master.block_id
                AND users_master.floor_id=floors_master.floor_id
                AND users_master.unit_id=unit_master.unit_id AND users_master.society_id ='$society_id'
             ");
			$user_data = mysqli_fetch_array($q);
			$user_id=$user_data['user_id'];
			$device=$user_data['device'];
			$user_token=$user_data['user_token'];
			
				if ($device=='android') {
		        	$nResident->noti("","",$society_id,$user_token,$title,$description,'');
		      	}  else if($device=='ios') {
		        	$nResident->noti_ios("","",$society_id,$user_token,$title,$description,'');
		      	}

		      	$notiAry = array(
		          'society_id'=>$society_id,
		          'user_id'=>$user_id,
		          'notification_title'=>$title,
		          'notification_desc'=>$description,    
		          'notification_date'=>date('Y-m-d H:i'),
		          'notification_action'=>'',
		          'notification_logo'=>'Visitors-mgmtxxxhdpi.png'
		        );
		        $d->insert("user_notification",$notiAry);

		        $response["message"] = "Send";
				$response["status"] = "200";
				echo json_encode($response);

		} else {
			$response["message"] = "Invalid Request";
			$response["status"] = "201";
			echo json_encode($response);

		}

}

?>