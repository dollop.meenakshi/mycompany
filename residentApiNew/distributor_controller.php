<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST)){
	extract(array_map("test_input", $_POST));
	$response = array();

	if($_POST['getDistributors']=="getDistributors" && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	
		if ($retailer_id != '') {
			$appendQuery = " AND retailer_distributor_relation_master.retailer_id = '$retailer_id' ";
		}

		$qry = $d->selectRow("
			distributor_master.*
			","
			distributor_master,
			retailer_distributor_relation_master
			","
			distributor_master.society_id = '$society_id' 
			AND distributor_master.distributor_status = '1' 			
			AND retailer_distributor_relation_master.distributor_id = distributor_master.distributor_id $appendQuery","GROUP BY distributor_master.distributor_id");

		if (mysqli_num_rows($qry) > 0) {
		
			$response["distributors"] = array();

			while($data = mysqli_fetch_array($qry)){
				$distributors = array();

				$distributors['distributor_id'] = $data['distributor_id'].'';
				$distributors['society_id'] = $data['society_id'].'';
				$distributors['distributor_area_id'] = $data['distributor_area_id'].'';
				$distributors['distributor_name'] = $data['distributor_name'].'';
				$distributors['distributor_code'] = $data['distributor_code'].'';
				$distributors['distributor_email'] = $data['distributor_email'].'';
				if ($data['distributor_contact_person'] == '') {
					$distributors['distributor_contact_person'] = "No Name";
				}else{
					$distributors['distributor_contact_person'] = $data['distributor_contact_person'].'';
				}
				$distributors['distributor_contact_person_number'] = $data['distributor_contact_person_country_code'].' '. $data['distributor_contact_person_number'].'';
				$distributors['distributor_contact_person_country_code'] = $data['distributor_contact_person_country_code'].'';
				$distributors['distributor_address'] = $data['distributor_address'].'';
				$distributors['distributor_photo'] = $data['distributor_photo'].'';
				
				array_push($response["distributors"],$distributors);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getDistributorDetails']=="getDistributorDetails" && $distributor_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	

		$qry = $d->selectRow("
			distributor_master.*,
			countries.country_name
			","
			distributor_master,countries
			","
			distributor_master.society_id = '$society_id' 
			AND distributor_master.distributor_id = '$distributor_id' 
			AND distributor_master.country_id = countries.country_id");

		if (mysqli_num_rows($qry) > 0) {
		
			$data = mysqli_fetch_array($qry);

			$response['distributor_id'] = $data['distributor_id'];
			$response['society_id'] = $data['society_id'];
			$response['country_id'] = $data['country_id'];
			$response['country_name'] = $data['country_name'];
			$response['state_id'] = "";
			$response['state_name'] = "";
			$response['city_id'] = "";
			$response['city_name'] = "";
			$response['distributor_area_id'] =  "";			
			$response['area_name'] =  "";
			$response['distributor_name'] = $data['distributor_name'];
			$response['distributor_code'] = $data['distributor_code'];
			$response['distributor_email'] = $data['distributor_email'];
			if ($data['distributor_contact_person'] == '') {
				$response['distributor_contact_person'] = "No Name";
			}else{
				$response['distributor_contact_person'] = $data['distributor_contact_person'];
			}
			$response['distributor_contact_person_country_code'] = $data['distributor_contact_person_country_code'];
			$response['distributor_contact_person_number'] = $data['distributor_contact_person_country_code'].' '. $data['distributor_contact_person_number'];
			$response['distributor_address'] = $data['distributor_address'];
			$response['distributor_google_address'] = $data['distributor_google_address'];
			$response['distributor_latitude'] = $data['distributor_latitude'];
			$response['distributor_longitude'] = $data['distributor_longitude'];
			$response['distributor_pincode'] = $data['distributor_pincode'];
			$response['distributor_gst_no'] = $data['distributor_gst_no'];
			$response['distributor_photo'] = $data['distributor_photo'];
			$response['distributor_created_date'] = date('dS F Y',strtotime($data['distributor_created_date']));						

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No distributor found";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    } else{
		$response["success"] = 201;
		$response["message"] = "Invalid Tag !";
		echo json_encode($response);
	}

} else {
	$response["success"] = 201;
	$response["message"] = "Invalid API Key !";
	echo json_encode($response);
}

?>