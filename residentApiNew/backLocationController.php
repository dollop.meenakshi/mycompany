<?php
include_once 'lib.php';

if (isset($_POST)){
	extract(array_map("test_input", $_POST));
	$response = array();

	if($_POST['checkAccessLocation']=="checkAccessLocation" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $access_type = $location_tracking_Access;

        include 'check_access_data.php';


        $accessResponseData['status'] = "200";
        $accessResponseData['message'] = "Data found";
        echo json_encode($accessResponseData);
                  
    }else if($_POST['employeeLastLocation']=="employeeLastLocation" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

    	$access_type = $location_tracking_Access;

        include "check_access_data.php";

        if ($userIds=='') {
            switch ($access_for) {
                case '0':
                case '1':
                    $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                    $LIMIT_DATA = " LIMIT 100";
                    break;
                case '2':
                    $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                    $LIMIT_DATA = " LIMIT 100";
                    break;
                case '3':
                    $appendQuery = " AND users_master.user_id IN ('$accessUserIds')";
                    $LIMIT_DATA = " LIMIT 100";
                    break;
                
                default:
                    // code...
                    break;
            }
        }else{
            $appendQuery = " AND users_master.user_id IN ($userIds)";
        }

        $response['modification_access'] = $modification_access;

		$locationQry = $d->selectRow("users_master.user_id,block_master.block_name,floors_master.floor_name,users_master.user_full_name,users_master.user_designation,users_master.user_profile_pic,users_master.last_tracking_latitude,users_master.last_tracking_longitude,users_master.last_tracking_address,users_master.last_tracking_location_time,users_master.last_tracking_battery_status,users_master.last_tracking_mock_location_app_name,users_master.last_tracking_mock_location","users_master,block_master,floors_master","users_master.user_status = '1' AND users_master.active_status = '0' AND users_master.delete_status = '0' AND users_master.block_id = block_master.block_id AND users_master.floor_id = floors_master.floor_id AND users_master.track_user_location = '1' AND users_master.last_tracking_latitude != '' AND users_master.last_tracking_longitude != '' $appendQuery","ORDER BY users_master.last_tracking_location_time DESC");

		if (mysqli_num_rows($locationQry) > 0) {
		
			$response["employee"] = array();

			while($data = mysqli_fetch_array($locationQry)){
				$devices = array();

				$devices['user_id'] = $data['user_id'];
				$devices['user_full_name'] = $data['user_full_name'];
				$devices['user_designation'] = $data['user_designation'];
				$devices['branch_name'] = $data['block_name'];
				$devices['department_name'] = $data['floor_name'];
				$devices['last_tracking_latitude'] = $data['last_tracking_latitude'];
				$devices['last_tracking_longitude'] = $data['last_tracking_longitude'];
				$devices['last_tracking_address'] = $data['last_tracking_address'];

				$devices['last_tracking_battery_status'] = $data['last_tracking_battery_status'];
				
				if ($data['last_tracking_mock_location'] == 1) {
					$devices['last_tracking_mock_location'] = true;
					$devices['last_tracking_mock_location_app_name'] = $data['last_tracking_mock_location_app_name'];
				}else{
					$devices['last_tracking_mock_location'] = false;
					$devices['last_tracking_mock_location_app_name'] = "";
				}

				if ($data['last_tracking_location_time'] != '0000-00-00 00:00:00' && $data['last_tracking_location_time'] != null) {
					$devices['last_tracking_location_date'] = date('Y-m-d',strtotime($data['last_tracking_location_time']));
					$devices['last_tracking_location_time'] = date('h:i A, dS M Y',strtotime($data['last_tracking_location_time']));
				}else{
					$devices['last_tracking_location_date'] = "";
					$devices['last_tracking_location_time'] = "";
				}
				
				$devices['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $data['user_profile_pic'];

						
				array_push($response["employee"],$devices);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['employeeLastLocationSingle']=="employeeLastLocationSingle" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

    	
		$locationQry = $d->selectRow("
			last_tracking_latitude,
			last_tracking_longitude,
			last_tracking_address,
			last_tracking_location_time
			","
			users_master
			","
			user_status = '1' 
			AND active_status = '0' 
			AND delete_status = '0' 
			AND track_user_location = '1' 
			AND last_tracking_latitude != '' 
			AND last_tracking_longitude != ''
			AND user_id = '$user_id'
			");

		if (mysqli_num_rows($locationQry) > 0) {
		
			$data = mysqli_fetch_array($locationQry);

			$response['last_tracking_latitude'] = $data['last_tracking_latitude'];
			$response['last_tracking_longitude'] = $data['last_tracking_longitude'];
			$response['last_tracking_address'] = $data['last_tracking_address'];

			if ($data['last_tracking_location_time'] != '0000-00-00 00:00:00' && $data['last_tracking_location_time'] != null) {
				$response['last_tracking_location_date'] = date('Y-m-d',strtotime($data['last_tracking_location_time']));
				$response['last_tracking_location_time'] = date('h:i A, dS M Y',strtotime($data['last_tracking_location_time']));
			}else{
				$response['last_tracking_location_date'] = "";
				$response['last_tracking_location_time'] = "";
			}
			
			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['employeeDayLocationList']=="employeeDayLocationList" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

		$locationQry = $d->select("user_back_track_master","user_back_track_master.user_id = '$user_id' AND DATE_FORMAT(user_back_track_date,'%Y-%m-%d') = '$selectedDate'","ORDER BY user_back_track_date ASC");

		if (mysqli_num_rows($locationQry) > 0) {
		
			$response["location_history"] = array();

			while($data = mysqli_fetch_array($locationQry)){
				$devices = array();

				if ($data['spend_sec']>0) {
			        $spend_minutes = $data['spend_sec']/60;
			        $hours = floor($spend_minutes / 3600);
			        $minutes = floor(($spend_minutes / 60) % 60);
			        $seconds = $spend_minutes % 60;
			        $spend_minutes =  "$hours:$minutes:$seconds";
			        // $spend_minutes = number_format((float)$spend_minutes, 2, '.', '').' Min.';
			        
			        if ($data['spend_sec']>1800) {
			          // more than 10 minutes
			          $marker_icon = "2";
			        } else if ($data['spend_sec']>600) {
			          // more than 30 minutes
			          $marker_icon = "1";
			        } else {
			          $marker_icon = "0";
			        }
			      } else {
			        $marker_icon = "0";
			        $spend_minutes = "0";
			     }

				$devices['user_id'] = $data['user_id'];
				$devices['back_address'] = $data['back_address'];
				$devices['back_area'] = $data['back_area'];
				$devices['back_locality'] = $data['back_locality'];
				$devices['back_user_lat'] = $data['back_user_lat'];
				$devices['back_user_long'] = $data['back_user_long'];
				$devices['battery_status'] = $data['battery_status'];
				$devices['gps_accuracy'] = $data['gps_accuracy'];
				$devices['marker_icon'] = $marker_icon;
				$devices['spend_minutes'] = $spend_minutes;

				if ($data['is_mock_location'] == 1) {
					$devices['is_mock_location'] = true;
					$devices['mock_app_name'] = $data['mock_app_name'];
					$devices['mock_app_package_name'] = $data['mock_app_package_name'];					
				}else{
					$devices['is_mock_location'] = false;
					$devices['mock_app_name'] = "";
					$devices['mock_app_package_name'] = "";
				}

				if ($data['user_back_track_date'] != '0000-00-00 00:00:00' && $data['user_back_track_date'] != null) {
					$devices['user_back_track_date_pass'] = date('Y-m-d',strtotime($data['user_back_track_date']));
					$devices['user_back_track_date'] = date('h:i A, dS M Y',strtotime($data['user_back_track_date']));
				}else{
					$devices['user_back_track_date_pass'] = "";
					$devices['user_back_track_date'] = "";
				}
				
				array_push($response["location_history"],$devices);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if (isset($_POST['user_back_location'])) {

		extract(array_map("test_input", $_POST));

		$m->set_data('user_id', $user_id);
		$m->set_data('society_id', $society_id);
		$m->set_data('gps_status', $gps_status);
		$m->set_data('gps_date', date('Y-m-d H:i:s'));

		$oflline_data = json_decode($_POST['oflline_data'], True);
		$oflline_data_gps = json_decode($_POST['oflline_data_gps'], True);

		for ($j=0; $j <count($oflline_data_gps); $j++) { 
			$aaa = array(
				'user_id' => $m->get_data('user_id'),
				'gps_status' => $oflline_data_gps[$j]['isOn'],
				'created_date' => $oflline_data_gps[$j]['dateTime'],
			);

			$qqq = $d->insert("user_gps_on_off", $aaa);
		}


		for ($i=0; $i <count($oflline_data); $i++) { 
			
			$oflline_data[$i]['address'];

			if ($oflline_data[$i]['netWorkStatus']==1) {
				$network_type = "Wi-Fi";
			} else if ($oflline_data[$i]['netWorkStatus']==2) {
				$network_type = "Mobile Data";
			} else {
				$network_type = "No Internet";
			}

			
			$a = array(
				'user_id' => $m->get_data('user_id'),
				'back_address' => $oflline_data[$i]['address'],
				'back_area' => $oflline_data[$i]['area'],
				'back_locality' => $oflline_data[$i]['locality'],
				'back_user_lat' => $oflline_data[$i]['user_lat'],
				'back_user_long' => $oflline_data[$i]['user_long'],
				'gps_accuracy' => $oflline_data[$i]['gps_accuracy'],
				'user_back_track_date' => $oflline_data[$i]['time'],
				'is_mock_location' => $oflline_data[$i]['isMockSettingsON'],
				'mock_app_name' => $oflline_data[$i]['mock_app_name'],
				'last_distance' => $oflline_data[$i]['last_distance'],				
				'battery_status' => $oflline_data[$i]['battery_status'],				
				'mock_app_package_name' => $oflline_data[$i]['mock_app_package_name'],
				'speed_per_km_hr' => $oflline_data[$i]['speed_per_km_hr'],
				'spend_sec' => $oflline_data[$i]['spend_sec'],
				'phone_model' => $phone_model,
				'phone_brand' => $phone_brand,
				'offline_data' => 1,
				'network_type' => $network_type,
			);

			$q = $d->insert("user_back_track_master", $a);
		}

		if ($gps_status != '') {
			$a2 = array(
				'user_id' => $m->get_data('user_id'),
				'society_id' => $m->get_data('society_id'),
				'gps_status' => $m->get_data('gps_status'),
				'gps_date' => $m->get_data('gps_date')
			);

			$q2 = $d->insert("gps_on_off", $a2);
		}


		$currentDatTime = date('Y-m-d H:i:s');

		// $userQry = $d->selectRow("track_user_time","users_master","user_id = '$user_id'");
		// $userData = mysqli_fetch_array($userQry);

		// $defaultMinutes = ($userData['track_user_time'] / 1000) / 60;

		// if ($defaultMinutes < 1) {
		// 	$defaultMinutes = 1;
		// }
		if ($netWorkStatus==1) {
			$network_type = "Wi-Fi";
		} else if ($netWorkStatus==2) {
			$network_type = "Mobile Data";
		} else {
			$network_type = "No Internet";
		}
		
		$m->set_data('address', $address);
		$m->set_data('area', $area);
		$m->set_data('locality', $locality);
		$m->set_data('user_lat', $user_lat);
		$m->set_data('user_long', $user_long);
		$m->set_data('user_track_date',$currentDatTime);
		$m->set_data('is_mock_location',$isMockSettingsON);
		$m->set_data('gps_accuracy',$gps_accuracy);
		$m->set_data('battery_status',$battery_status);
		$m->set_data('mock_app_name',$mock_app_name);
		$m->set_data('mock_app_package_name',$mock_app_package_name);
		$m->set_data('network_type',$network_type);
		$m->set_data('speed_per_km_hr',$speed_per_km_hr);
		$m->set_data('spend_sec',$spend_sec);
		$m->set_data('phone_model',$phone_model);
		$m->set_data('phone_brand',$phone_brand);

		$a = array(
			'user_id' => $m->get_data('user_id'),
			'back_address' => $m->get_data('address'),
			'back_area' => $m->get_data('area'),
			'back_locality' => $m->get_data('locality'),
			'back_user_lat' => $m->get_data('user_lat'),
			'back_user_long' => $m->get_data('user_long'),
			'user_back_track_date' => $m->get_data('user_track_date'),
			'is_mock_location' => $m->get_data('is_mock_location'),
			'gps_accuracy' => $m->get_data('gps_accuracy'),
			'battery_status' => $m->get_data('battery_status'),
			'mock_app_name' => $m->get_data('mock_app_name'),
			'mock_app_package_name' => $m->get_data('mock_app_package_name'),
			'network_type' => $m->get_data('network_type'),
			'speed_per_km_hr' => $m->get_data('speed_per_km_hr'),
			'spend_sec' => $m->get_data('spend_sec'),
			'phone_model' => $m->get_data('phone_model'),
			'phone_brand' => $m->get_data('phone_brand'),
		);

		$aa = array(
			'last_tracking_latitude' => $m->get_data('user_lat'),
			'last_tracking_longitude' => $m->get_data('user_long'),
			'last_tracking_gps_accuracy' => $m->get_data('gps_accuracy'),
			'last_tracking_address' => $m->get_data('address'),
			'last_tracking_location_time' => $m->get_data('user_track_date'),
			'last_tracking_area' => $m->get_data('area'),
			'last_tracking_locality' => $m->get_data('locality'),
			'last_tracking_mock_location' => $m->get_data('is_mock_location'),
			'last_tracking_battery_status' => $m->get_data('battery_status'),
			'last_tracking_mock_location_app_name' => $m->get_data('mock_app_name'),
		);

		$q1 = $d->update("users_master",$aa,"user_id = '$user_id'");

		$lastRecQry = $d->selectRow("*","user_back_track_master","user_id = '$user_id'","ORDER BY user_back_track_id DESC LIMIT 1");

		if (mysqli_num_rows($lastRecQry) > 0) {
			$lastData = mysqli_fetch_array($lastRecQry);

			$todayDate = date("Y-m-d");
			$lastTimeDateCheck = date("Y-m-d",strtotime($lastData['user_back_track_date']));
			$lastLatitude = $lastData['back_user_lat'];
			$lastLongitude = $lastData['back_user_long'];

            $radiusInMeeter=  $d->haversineGreatCircleDistance($user_lat,$user_long,$lastLatitude, $lastLongitude);
            if ($todayDate!=$lastTimeDateCheck) {
            	$a['last_distance'] = $radiusInMeeter;
            	$q = $d->insert("user_back_track_master", $a);
        		$response["success"] = 200;
				$response["message"] = "Location Updated ($radiusInMeeter)";
            } else if ($radiusInMeeter>50) {
            	$a['last_distance'] = $radiusInMeeter;
        		$q = $d->insert("user_back_track_master", $a);
        		$response["success"] = 200;
				$response["message"] = "Location Updated ($radiusInMeeter)";
            } else {
        		$response["success"] = 200;
				$response["message"] = "Same Location ";
            }
		}else{
			$a['last_distance'] = "0";
			$q = $d->insert("user_back_track_master", $a);	
			$response["success"] = 200;
			$response["message"] = "Location Added";		
		}
		
		echo json_encode($response);
	} else if (isset($_POST['user_back_location_new'])) {

		extract(array_map("test_input", $_POST));
		$currentDatTime = date('Y-m-d H:i:s');

		$m->set_data('user_id', $user_id);
		$m->set_data('society_id', $society_id);
		$m->set_data('gps_status', $gps_status);
		$m->set_data('gps_date', date('Y-m-d H:i:s'));

		$oflline_data = json_decode($_POST['oflline_data'], True);
		$oflline_data_gps = json_decode($_POST['oflline_data_gps'], True);
		$oflline_data_internet = json_decode($_POST['oflline_data_internet'], True);

		for ($j=0; $j <count($oflline_data_gps); $j++) { 
			$aaa = array(
				'user_id' => $m->get_data('user_id'),
				'type' => "1",
				'gps_status' => $oflline_data_gps[$j]['isOn'],
				'latitude' => $oflline_data_gps[$j]['latitude'],
				'longitude' => $oflline_data_gps[$j]['longitude'],
				'google_address' => $oflline_data_gps[$j]['google_address'],
				'created_date' => $oflline_data_gps[$j]['dateTime'],
			);

			$qqq = $d->insert("user_gps_on_off", $aaa);
		}

		for ($j=0; $j <count($oflline_data_internet); $j++) { 
			$aaa = array(
				'user_id' => $m->get_data('user_id'),
				'type' => "2",
				'gps_status' => $oflline_data_internet[$j]['isOn'],
				'latitude' => $oflline_data_internet[$j]['latitude'],
				'longitude' => $oflline_data_internet[$j]['longitude'],
				'google_address' => $oflline_data_internet[$j]['google_address'],
				'created_date' => $oflline_data_internet[$j]['dateTime'],
			);

			$qqq = $d->insert("user_gps_on_off", $aaa);
		}


		$oflline_data = array_values(array_column($oflline_data, null, 'time'));

		$totalOfflineData = count($oflline_data);

		if ($totalOfflineData>0) {
		
			end($oflline_data);         
			$key = key($oflline_data);  

			 $lastRecordLat = $oflline_data[$key]['user_lat'];
			 $lastRecordLong = $oflline_data[$key]['user_long'];
			 $spendSecoundNew = $oflline_data[$key]['spend_sec'];

			$qs = $d->selectRow("back_user_lat,back_user_long,user_back_track_date,spend_sec,user_back_track_id","user_back_track_master","user_id='$user_id' AND back_user_lat='$lastRecordLat' AND back_user_long='$lastRecordLong'","ORDER BY user_back_track_id DESC LIMIT 1");
			if (mysqli_num_rows($qs)) {
				$lastRecordData= mysqli_fetch_array($qs);
				$lastInsertTime = $lastRecordData['user_back_track_date'];
				$user_back_track_id = $lastRecordData['user_back_track_id'];
				$timeFirst  = strtotime($lastInsertTime);
				$timeSecond = strtotime($currentDatTime);
				$differenceInSeconds = $timeSecond - $timeFirst;

				$a11 = array(
					'spend_sec' => $differenceInSeconds,
				);
				$d->update("user_back_track_master",$a11,"user_back_track_id='$user_back_track_id'");
				$totalOfflineData = $totalOfflineData-1;
			}


			for ($i=0; $i <$totalOfflineData; $i++) { 
				
				$oflline_data[$i]['address'];

				if ($oflline_data[$i]['netWorkStatus']==1) {
					$network_type = "Wi-Fi";
				} else if ($oflline_data[$i]['netWorkStatus']==2) {
					$network_type = "Mobile Data";
				} else {
					$network_type = "No Internet";
				}

				
				$a = array(
					'user_id' => $m->get_data('user_id'),
					'back_address' => $oflline_data[$i]['address'],
					'back_area' => $oflline_data[$i]['area'],
					'back_locality' => $oflline_data[$i]['locality'],
					'back_user_lat' => $oflline_data[$i]['user_lat'],
					'back_user_long' => $oflline_data[$i]['user_long'],
					'gps_accuracy' => $oflline_data[$i]['gps_accuracy'],
					'user_back_track_date' => $oflline_data[$i]['time'],
					'is_mock_location' => $oflline_data[$i]['isMockSettingsON'],
					'mock_app_name' => $oflline_data[$i]['mock_app_name'],
					'last_distance' => $oflline_data[$i]['last_distance'],				
					'battery_status' => $oflline_data[$i]['battery_status'],				
					'mock_app_package_name' => $oflline_data[$i]['mock_app_package_name'],
					'speed_per_km_hr' => $oflline_data[$i]['speed_per_km_hr'],
					'spend_sec' => $oflline_data[$i]['spend_sec'],
					'phone_model' => $phone_model,
					'phone_brand' => $phone_brand,
					'offline_data' => 1,
					'network_type' => $network_type,
				);
				if ($oflline_data[$i]['user_lat']!="") {
					$q = $d->insert("user_back_track_master", $a);
				}
			}		

		}

		

		
		if ($netWorkStatus==1) {
			$network_type = "Wi-Fi";
		} else if ($netWorkStatus==2) {
			$network_type = "Mobile Data";
		} else {
			$network_type = "No Internet";
		}
		
		$m->set_data('address', $address);
		$m->set_data('area', $area);
		$m->set_data('locality', $locality);
		$m->set_data('user_lat', $user_lat);
		$m->set_data('user_long', $user_long);
		$m->set_data('user_track_date',$currentDatTime);
		$m->set_data('is_mock_location',$isMockSettingsON);
		$m->set_data('gps_accuracy',$gps_accuracy);
		$m->set_data('battery_status',$battery_status);
		$m->set_data('mock_app_name',$mock_app_name);

		
		$aa = array(
			'last_tracking_latitude' => $m->get_data('user_lat'),
			'last_tracking_longitude' => $m->get_data('user_long'),
			'last_tracking_gps_accuracy' => $m->get_data('gps_accuracy'),
			'last_tracking_address' => $m->get_data('address'),
			'last_tracking_location_time' => $m->get_data('user_track_date'),
			'last_tracking_area' => $m->get_data('area'),
			'last_tracking_locality' => $m->get_data('locality'),
			'last_tracking_mock_location' => $m->get_data('is_mock_location'),
			'last_tracking_battery_status' => $m->get_data('battery_status'),
			'last_tracking_mock_location_app_name' => $m->get_data('mock_app_name'),
		);

		$q1 = $d->update("users_master",$aa,"user_id = '$user_id'");

		$response["success"] = 200;
		$response["message"] = "Location Added";		
		
		echo json_encode($response);
	}  else{
		$response["success"] = 201;
		$response["message"] = "Invalid Tag !";
		echo json_encode($response);
	}

} else {
	$response["success"] = 201;
	$response["message"] = "Invalid API Key !";
	echo json_encode($response);
}

?>