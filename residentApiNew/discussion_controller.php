<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        $response = array();
        extract(array_map("test_input" , $_POST));
        $today  = date("Y-m-d");

        if(isset($_POST['addDiscussion']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true ){


            $maxsize    = 10097152;
            $file11=$_FILES["discussion_file"]["tmp_name"];
            if (file_exists($file11)) {

                if(($_FILES['discussion_file']['size'] >= $maxsize) || ($_FILES["discussion_file"]["size"] == 0)) {
                    $response["message"]="Document too large. Must be less than 10 MB";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }

                $extId = pathinfo($_FILES['discussion_file']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","doc","docx","csv","xls","xlsx","jpg","png","jpeg","ppt");
                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["discussion_file"]["name"]);
                    $discussion_file = "File_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["discussion_file"]["tmp_name"], "../img/request/" . $discussion_file);
                } else {
                    $response["message"]="Invalid Document only PDF,docx,csv & ppt are allowed.";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }
            } else {
                $discussion_file ="";
            }

            $uploadedFile = $_FILES["discussion_photo"]["tmp_name"];
            $ext = pathinfo($_FILES['discussion_photo']['name'], PATHINFO_EXTENSION);
            if (file_exists($uploadedFile)) {

                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $user_id;
                $dirPath = "../img/request/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1000) {
                    $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }
                
                switch ($imageType) {

                    case IMAGETYPE_PNG:
                    $imageSrc = imagecreatefrompng($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagepng($tmp, $dirPath . $newFileName . "_discussion." . $ext);
                    break;

                    case IMAGETYPE_JPEG:
                    $imageSrc = imagecreatefromjpeg($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagejpeg($tmp, $dirPath . $newFileName . "_discussion." . $ext);
                    break;

                    case IMAGETYPE_GIF:
                    $imageSrc = imagecreatefromgif($uploadedFile);
                    $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                    imagegif($tmp, $dirPath . $newFileName . "_discussion." . $ext);
                    break;

                    default:

                    break;
                }
                $discussion_photo = $newFileName . "_discussion." . $ext;

                $notiUrl = $base_url . 'img/request/' . $discussion_photo;
            } else {
                $discussion_photo = '';
                $notiUrl = "";
            }

            $fq = $d->select("users_master", "user_id='$user_id'");
            $ownerData = mysqli_fetch_array($fq);
            $userType = $ownerData['user_type'];
            $member_status = $ownerData['member_status'];

            if ($member_status==1 && $discussion_forum_for==1) {
                $discussion_forum_for = 2;
            }

            if ($discussion_forum_for!=0) {
                $discussion_forum_for = $floor_id;
            }

            $m->set_data('society_id',$society_id);
            $m->set_data('discussion_forum_title',$discussion_forum_title);
            $m->set_data('discussion_forum_description',$discussion_forum_description);
            $m->set_data('created_date',date("Y-m-d H:i:s"));
            $m->set_data('user_id',$user_id);
            $m->set_data('discussion_forum_for',$discussion_forum_for);
            $m->set_data('discussion_photo',$discussion_photo);
            $m->set_data('discussion_file',$discussion_file);

            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'discussion_forum_title'=>$m->get_data('discussion_forum_title'),
                'discussion_forum_description'=>$m->get_data('discussion_forum_description'),
                'created_date'=>$m->get_data('created_date'),
                'user_id'=>$m->get_data('user_id'),
                'discussion_forum_for'=>$m->get_data('discussion_forum_for'),
                'discussion_photo'=>$m->get_data('discussion_photo'),
                'discussion_file'=>$m->get_data('discussion_file'),
            );

            $q1=$d->insert("discussion_forum_master",$a);
            $discussion_forum_id = $con->insert_id;

            if ($q1>0) {
                $qry ="";
                if ($discussion_forum_for==0) {
                    $for="All Employee";
                    $qry ="  AND user_id!='$user_id'";
                    $append_query="users_master.user_id!='$user_id' AND users_master.delete_status=0";
                } else {
                    $for="Your Department";
                    $qry =" and delete_status=0 AND user_id!='$user_id' AND floor_id='$floor_id'";
                    $append_query="users_master.user_id!='$user_id' AND users_master.member_status =1 AND users_master.delete_status=0 AND users_master.floor_id='$floor_id'";
                }

                $title= "New Discussion Forum Added";
                $description= "By $user_name";
                $d->insertUserNotificationWithId($society_id,$title,$description,"discussion","Discussion_1xxxhdpi.png",$append_query,$discussion_forum_id);


                $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND  user_mobile!='$user_mobile' AND discussion_mute=0 AND user_token!='' AND society_id='$society_id' AND device='android' $qry ");
                $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_mobile!='$user_mobile' AND discussion_mute=0 AND user_token!='' AND society_id='$society_id' AND device='ios' $qry ");
                $nResident->noti("DiscussionFragment",$notiUrl,$society_id,$fcmArray,$title,$description,$discussion_forum_id);
                $nResident->noti_ios("DiscussionVC",$notiUrl,$society_id,$fcmArrayIos,$title,$description,$discussion_forum_id);

                $d->insert_myactivity($user_id,"$society_id","0","$user_name","New discussion forum created","Discussion_1xxxhdpi.png");

                $m->set_data('society_id',$society_id);
                $m->set_data('discussion_forum_id',$discussion_forum_id);
                $m->set_data('user_id',$user_id);

                $a111 = array(
                    'society_id'=>$m->get_data('society_id'),
                    'discussion_forum_id'=>$m->get_data('discussion_forum_id'),
                    'user_id'=>$m->get_data('user_id'),
                );

                $d->insert("discussion_forum_mute",$a111);

                $response["message"]="Discussion Forum Added";
                $response["status"]='200';
                echo json_encode($response);
            }else{
                $response["message"]="Soenthing Wrong.";
                $response["status"]='201';
                echo json_encode($response);
            }

        }else if(isset($_POST['addDiscussionComment']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('society_id',$society_id);
            $m->set_data('discussion_forum_id',$discussion_forum_id);
            $m->set_data('comment_messaage',$comment_messaage);
            $m->set_data('created_date',date("Y-m-d H:i:s"));
            $m->set_data('user_id',$user_id);
            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'discussion_forum_id'=>$m->get_data('discussion_forum_id'),
                'comment_messaage'=>$m->get_data('comment_messaage'),
                'created_date'=>$m->get_data('created_date'),
                'user_id'=>$m->get_data('user_id'),
            );

            $q1=$d->insert("discussion_forum_comment",$a);
            $comment_messaage = html_entity_decode($comment_messaage);
            if ($q1>0) {

                $dq=$d->select("discussion_forum_master","discussion_forum_id='$discussion_forum_id'");
                $data=mysqli_fetch_array($dq);
                $discussion_forum_for = $data['discussion_forum_for'];

                $qry ="";
                $append_query="";
                if ($discussion_forum_for==0) {
                    $for="All Members";
                    $qry =" AND delete_status=0 AND user_id!='$user_id'";
                    $append_query="users_master.delete_status=0 AND users_master.user_id!='$user_id'";
                } else {
                    $for="Team Members ";
                    $qry =" and member_status =1  AND delete_status=0 AND user_id!='$user_id' AND floor_id='$floor_id'";
                    $append_query="users_master.member_status =1 AND users_master.delete_status=0 AND users_master.user_id!='$user_id' AND users_master.floor_id='$floor_id'";
                }

                $title= "$comment_messaage";
                $description= "Comment On Discussion Forum By $user_name";


                $d->insertUserNotificationWithId($society_id,$title,$description,"discussion","Discussion_1xxxhdpi.png",$append_query,$discussion_forum_id);



                $muteArray=array();
                $qc11=$d->select("discussion_forum_mute,users_master","users_master.delete_status=0 AND discussion_forum_mute.user_id=users_master.user_id AND  discussion_forum_mute.discussion_forum_id='$discussion_forum_id'");
                while ($muteData=mysqli_fetch_array($qc11)) {
                    array_push($muteArray, $muteData['user_mobile']);
                }

                $ids = join("','",$muteArray);  
                if (count($muteArray)) {

                    $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND  user_id!='$user_id' AND  user_mobile IN ('$ids') AND discussion_mute=0 AND user_mobile!='$user_mobile' AND user_token!='' AND society_id='$society_id' AND device='android' $qry ");
                    $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND user_id!='$user_id' AND  user_mobile IN ('$ids') AND discussion_mute=0 AND  user_mobile!='$user_mobile' AND user_token!='' AND society_id='$society_id' AND device='ios' $qry ");
                    $nResident->noti("DiscussionFragment","",$society_id,$fcmArray,"$comment_messaage","Comment On Discussion Forum By $user_name",$discussion_forum_id);
                    $nResident->noti_ios("DiscussionVC","",$society_id,$fcmArrayIos,"$comment_messaage","Comment On Discussion Forum By $user_name",$discussion_forum_id);

                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","Comment on discussion forum","Discussion_1xxxhdpi.png");
                }
                $response["message"]="Comment Added  ";
                $response["status"]='200';
                echo json_encode($response);
            }else{
                $response["message"]="Soenthing Wrong.";
                $response["status"]='201';
                echo json_encode($response);
            }
        } else if(isset($_POST['addReplyComment']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($comment_id, FILTER_VALIDATE_INT) == true) {

            $m->set_data('society_id',$society_id);
            $m->set_data('prent_comment_id',$comment_id);
            $m->set_data('discussion_forum_id',$discussion_forum_id);
            $m->set_data('comment_messaage',$comment_messaage);
            $m->set_data('created_date',date("Y-m-d H:i:s"));
            $m->set_data('user_id',$user_id);
            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'prent_comment_id'=>$m->get_data('prent_comment_id'),
                'discussion_forum_id'=>$m->get_data('discussion_forum_id'),
                'comment_messaage'=>$m->get_data('comment_messaage'),
                'created_date'=>$m->get_data('created_date'),
                'user_id'=>$m->get_data('user_id'),
            );


            $q1=$d->insert("discussion_forum_comment",$a);
            $comment_id11 = $con->insert_id;

            $comment_messaage = html_entity_decode($comment_messaage);
            if ($q1>0) {


                $dq=$d->select("discussion_forum_master","discussion_forum_id='$discussion_forum_id'");
                $data=mysqli_fetch_array($dq);
                $discussion_forum_for = $data['discussion_forum_for'];

                $qry ="";
                if ($discussion_forum_for==0) {
                    $for="All Members";
                    $qry ="  AND user_id!='$user_id' ";
                } else {
                    $for="Team Members";
                    $qry =" and member_status =1  AND user_id!='$user_id' AND floor_id='$floor_id'";
                }

                $q111=$d->selectRow("user_full_name,user_designation","users_master","user_id='$user_id'");
                $userdataComment=mysqli_fetch_array($q111);
                $user_name=$userdataComment['user_full_name'] .'('.$userdataComment['user_designation'].')';

                $qct=$d->select("discussion_forum_comment","discussion_forum_id='$discussion_forum_id' AND comment_id='$comment_id'");
                $comentUser=mysqli_fetch_array($qct);
                $comment_user_id = $comentUser['user_id'];

                $notiAry = array(
                    'society_id'=>$society_id,
                    'user_id'=>$comment_user_id,
                    'notification_title'=>"$comment_messaage",
                    'notification_desc'=>"Comment Reply By $user_name",    
                    'notification_date'=>date('Y-m-d H:i'),
                    'notification_action'=>'discussion',
                    'notification_logo'=>'Discussion_1xxxhdpi.png',
                    'feed_id'=>$discussion_forum_id,
                );
                if ($user_id!=$comment_user_id) {
                    $d->insert("user_notification",$notiAry);
                }

                $muteArray=array();
                $qc11=$d->select("discussion_forum_mute,users_master","users_master.delete_status=0 AND discussion_forum_mute.user_id=users_master.user_id AND  discussion_forum_mute.discussion_forum_id='$discussion_forum_id'");
                while ($muteData=mysqli_fetch_array($qc11)) {
                    array_push($muteArray, $muteData['user_mobile']);
                }

                $ids = join("','",$muteArray);  

                if (count($muteArray)) {

                    $fcmArray=$d->get_android_fcm("users_master","delete_status=0 AND users_master.user_id!='$user_id' AND user_mobile IN ('$ids') AND discussion_mute=0 AND user_id='$comment_user_id' AND user_token!='' AND society_id='$society_id' AND device='android' $qry ");
                    $fcmArrayIos=$d->get_android_fcm("users_master","delete_status=0 AND  users_master.user_id!='$user_id' AND user_mobile IN ('$ids') AND discussion_mute=0 AND user_id='$comment_user_id'  AND user_token!='' AND society_id='$society_id' AND device='ios' $qry ");
                    $nResident->noti("DiscussionFragment","",$society_id,$fcmArray,"$comment_messaage","Comment Reply By $user_name",$discussion_forum_id);
                    $nResident->noti_ios("DiscussionVC","",$society_id,$fcmArrayIos,"$comment_messaage","Comment Reply By $user_name",$discussion_forum_id);

                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","Comment reply on discussion forum","Discussion_1xxxhdpi.png");

                }

                $response["comment_id"]=$comment_id11;
                $response["message"]="Reply Added";
                $response["status"]='200';
                echo json_encode($response);
            }else{
                $response["message"]="Soenthing Wrong.";
                $response["status"]='201';
                echo json_encode($response);
            }
        }else if(isset($_POST['addMute']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($discussion_forum_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('society_id',$society_id);
            $m->set_data('discussion_forum_id',$discussion_forum_id);
            $m->set_data('user_id',$user_id);

            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'discussion_forum_id'=>$m->get_data('discussion_forum_id'),
                'user_id'=>$m->get_data('user_id'),
            );

            if ($mute_type==1) {
                $qc=$d->select("discussion_forum_mute","user_id='$user_id' AND discussion_forum_id='$discussion_forum_id'");
                if (mysqli_num_rows($qc)>0) {
                    $q1=$d->update("discussion_forum_mute",$a,"user_id='$user_id' AND discussion_forum_id='$discussion_forum_id'");
                }else {
                    $q1=$d->insert("discussion_forum_mute",$a);
                }
                $response["message"]="Comment Notification On";
            }else {
                $response["message"]="Comment Notification Off";
                $q1=$d->delete("discussion_forum_mute","user_id='$user_id' AND discussion_forum_id='$discussion_forum_id'");
            }



            if ($q1>0) {

                $response["status"]='200';
                echo json_encode($response);
            }else{
                $response["message"]="Soenthing Wrong.";
                $response["status"]='201';
                echo json_encode($response);
            }
        }else if ($_POST['muteAllDiscussion'] == "muteAllDiscussion" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($user_mobile, FILTER_VALIDATE_INT) == true) {

            if ($country_code!='') {
                $appendQuery = " AND country_code='$country_code'";
            }

            $m->set_data('discussion_mute', $discussion_mute);
            $a = array('discussion_mute' => $m->get_data('discussion_mute'));
            $q = $d->update("users_master",$a,"user_mobile='$user_mobile' AND society_id='$society_id' $appendQuery ");

            if ($q == true) {
                if ($discussion_mute==1) {
                    $response["message"] = "All Discussion Muted";
                } else {
                    $response["message"] = "All Discussion Unmuted";
                }
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if(isset($_POST['deleteDiscussion']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($discussion_forum_id, FILTER_VALIDATE_INT) == true){


            $q1=$d->delete("discussion_forum_master","discussion_forum_id='$discussion_forum_id' AND user_id='$user_id'");

            if ($q1>0) {
                $d->insert_myactivity($user_id,"$society_id","0","$user_name","Discussion forum deleted","Discussion_1xxxhdpi.png");
                $d->delete("discussion_forum_comment","discussion_forum_id='$discussion_forum_id' ");
                $response["message"]="Discussion Forum Deleted";
                $response["status"]='200';
                echo json_encode($response);
            }else{
                $response["message"]="Soenthing Wrong.";
                $response["status"]='201';
                echo json_encode($response);
            }

        }else if(isset($_POST['deleteComment']) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($comment_id, FILTER_VALIDATE_INT) == true){


            $q1=$d->delete("discussion_forum_comment","comment_id='$comment_id' AND user_id='$user_id'");

            if ($q1>0) {
                $q1=$d->delete("discussion_forum_comment","prent_comment_id='$comment_id' AND user_id='$user_id'");
                $response["message"]="Comment Deleted";
                $response["status"]='200';
                echo json_encode($response);
            }else{
                $response["message"]="Soenthing Wrong.";
                $response["status"]='201';
                echo json_encode($response);
            }

        }else if($_POST['getDiscussionFast']=="getDiscussionFast" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            $array1 = array();
            $array2 = array();

            if ($user_type==1) {
                $appendQueryType = " discussion_forum_master.discussion_forum_for=2";
            } else {
                $appendQueryType = " discussion_forum_master.discussion_forum_for=1";
            }

            $q=$d->selectRow("
                block_master.block_name,
                unit_master.unit_name,
                users_master.user_full_name,
                users_master.user_profile_pic,
                discussion_forum_master.*,
                discussion_forum_mute.mute_id,
                floors_master.floor_name",
                "block_master,
                floors_master,
                unit_master,
                users_master,
                discussion_forum_master LEFT JOIN discussion_forum_mute ON discussion_forum_mute.user_id='$user_id' 
                AND discussion_forum_master.discussion_forum_id=discussion_forum_mute.discussion_forum_id",
                "(users_master.unit_id = unit_master.unit_id 
                AND block_master.block_id = unit_master.block_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.user_id = discussion_forum_master.user_id 
                AND discussion_forum_master.active_status = 0 
                AND discussion_forum_master.society_id = '$society_id') 
                AND ((discussion_forum_master.discussion_forum_for = 0 AND discussion_forum_master.discussion_block_id = '0')
                OR (discussion_forum_master.discussion_forum_for = 0 AND discussion_forum_master.discussion_block_id = '$block_id') 
                OR (discussion_forum_master.discussion_forum_for = '$floor_id' AND discussion_forum_master.discussion_block_id = '$block_id') 
                OR (discussion_forum_master.discussion_forum_for = '$floor_id' AND discussion_forum_master.discussion_block_id = '0'))",
                "ORDER BY discussion_forum_master.discussion_forum_id DESC LIMIT 50");

            $qAdmin=$d->selectRow("discussion_forum_master.*,discussion_forum_mute.mute_id,bms_admin_master.admin_name,bms_admin_master.admin_profile","bms_admin_master,discussion_forum_master LEFT JOIN discussion_forum_mute ON discussion_forum_mute.user_id='$user_id' AND discussion_forum_master.discussion_forum_id=discussion_forum_mute.discussion_forum_id ","(bms_admin_master.admin_id=discussion_forum_master.admin_id AND discussion_forum_master.active_status=0 AND discussion_forum_master.society_id='$society_id') AND 
                ((discussion_forum_master.discussion_forum_for = 0 AND discussion_forum_master.discussion_block_id = '0')
                OR (discussion_forum_master.discussion_forum_for = 0 AND discussion_forum_master.discussion_block_id = '$block_id') 
                OR (discussion_forum_master.discussion_forum_for = '$floor_id' AND discussion_forum_master.discussion_block_id = '$block_id') 
                OR (discussion_forum_master.discussion_forum_for = '$floor_id' AND discussion_forum_master.discussion_block_id = '0'))","ORDER BY discussion_forum_master.discussion_forum_id DESC LIMIT 50");


            $qchekc=$d->selectRow("discussion_mute","users_master","user_id='$user_id' ");
            $muteDataCommon=mysqli_fetch_array($qchekc);


            $response["discussion"] = array();

            while($data=mysqli_fetch_array($q)) {
                $discussionUser = array(); 
                $discussionUser["discussion_forum_id"]=$data['discussion_forum_id'];
                $discussionUser["discussion_forum_title"]=html_entity_decode($data['discussion_forum_title']);
                $discussionUser["discussion_forum_description"]=html_entity_decode($data['discussion_forum_description']);
                $discussionUser["user_id"]=html_entity_decode($data['user_id']);
                $discussionUser["created_date"]=date('h:i A, dS M Y',strtotime($data['created_date'])); 
                $discussionUser["created_date_sort"]=$data['created_date']; 
                if ($data['discussion_photo']!='') {
                    $discussionUser["discussion_photo"]= $base_url.'img/request/'.$data['discussion_photo'];
                } else {
                    $discussionUser["discussion_photo"]= "";
                }
                if ($data['discussion_file']!='') {
                    $discussionUser["discussion_file"]= $base_url.'img/request/'.$data['discussion_file'];
                } else {
                    $discussionUser["discussion_file"]= "";
                }
                $created_by= $data['user_full_name'];
                $created_unit="(". $data['block_name'].'-'.$data['floor_name'].')';
                $user_profile= $base_url."img/users/recident_profile/".$data['user_profile_pic'];

                if ($data['discussion_forum_for']==0) {
                    $discussion_forum_for= "All Members";
                } else{
                    $discussion_forum_for= "Your Department";
                }
                $discussionUser["discussion_forum_for"]=$discussion_forum_for;
                $discussionUser["user_profile"]=$user_profile;
                $discussionUser["created_by"]=html_entity_decode($created_by);
                $discussionUser["created_unit"]=html_entity_decode($created_unit);
                $discussionUser["mute_id"]=$data['mute_id'];

                $totalUserComment = $d->count_data_direct("comment_id","discussion_forum_comment,users_master","discussion_forum_comment.user_id=users_master.user_id AND users_master.delete_status=0 AND discussion_forum_comment.discussion_forum_id='$data[discussion_forum_id]'").''; 

                $totalAdminComment = $d->count_data_direct("comment_id","discussion_forum_comment,bms_admin_master","bms_admin_master.admin_id=discussion_forum_comment.admin_id AND discussion_forum_comment.discussion_forum_id='$data[discussion_forum_id]' AND bms_admin_master.admin_active_status = '0'").'';

                $discussionUser["total_coments"]= $totalUserComment+$totalAdminComment.'';

                array_push($array1, $discussionUser);
                $arrayQuery1 = true;
            }

            while($data=mysqli_fetch_array($qAdmin)) {
                $discussionAdmin = array(); 
                $discussionAdmin["discussion_forum_id"]=$data['discussion_forum_id'];
                $discussionAdmin["discussion_forum_title"]=html_entity_decode($data['discussion_forum_title']);
                $discussionAdmin["discussion_forum_description"]=html_entity_decode($data['discussion_forum_description']);
                $discussionAdmin["user_id"]=html_entity_decode($data['user_id']);
                $discussionAdmin["created_date"]=date('h:i A, dS M Y',strtotime($data['created_date']));
                $discussionAdmin["created_date_sort"]=$data['created_date']; 
                if ($data['discussion_photo']!='') {
                    $discussionAdmin["discussion_photo"]= $base_url.'img/request/'.$data['discussion_photo'];
                } else {
                    $discussionAdmin["discussion_photo"]= "";
                }
                if ($data['discussion_file']!='') {
                    $discussionAdmin["discussion_file"]= $base_url.'img/request/'.$data['discussion_file'];
                } else {
                    $discussionAdmin["discussion_file"]= "";
                }
                $created_by= "Admin ".$data['admin_name'];
                $created_unit="";
                $user_profile= $base_url."img/society/".$data['admin_profile'];

                if ($data['discussion_forum_for']==0) {
                    $discussion_forum_for= "All Members";
                } else{
                    $discussion_forum_for= "Your Department";
                }
                $discussionAdmin["discussion_forum_for"]=$discussion_forum_for;
                $discussionAdmin["user_profile"]=$user_profile;
                $discussionAdmin["created_by"]=html_entity_decode($created_by);
                $discussionAdmin["created_unit"]=html_entity_decode($created_unit);
                $discussionAdmin["mute_id"]=$data['mute_id'];

                $totalUserComment = $d->count_data_direct("comment_id","discussion_forum_comment,users_master","discussion_forum_comment.user_id=users_master.user_id AND users_master.delete_status=0 AND discussion_forum_comment.discussion_forum_id='$data[discussion_forum_id]'").''; 

                $totalAdminComment = $d->count_data_direct("comment_id","discussion_forum_comment,bms_admin_master","bms_admin_master.admin_id=discussion_forum_comment.admin_id AND discussion_forum_comment.discussion_forum_id='$data[discussion_forum_id]'").'';

                $discussionAdmin["total_coments"]= $totalUserComment+$totalAdminComment.'';

                array_push($array2, $discussionAdmin);
                $arrayQuery2 = true;
            }

            $newArray = array_merge($array1,$array2);
            foreach ($newArray as $key => $part) {
                $sort[$key] = strtotime($part['created_date_sort']);
            }
            array_multisort($sort, SORT_DESC, $newArray,SORT_DESC);

            for ($i=0; $i <count($newArray) ; $i++) { 

                $discussion = array(); 
                $discussion["discussion_forum_id"]=$newArray[$i]['discussion_forum_id'];
                $discussion["discussion_forum_title"]=$newArray[$i]['discussion_forum_title'];
                $discussion["discussion_forum_description"]=$newArray[$i]['discussion_forum_description'];
                $discussion["user_id"]=$newArray[$i]['user_id'];
                $discussion["created_date"]=$newArray[$i]['created_date']; 
                $discussion["discussion_photo"]=$newArray[$i]['discussion_photo'];
                $discussion["discussion_file"]= $newArray[$i]['discussion_file'];
                $discussion["discussion_forum_for"]=$newArray[$i]['discussion_forum_for'];
                $discussion["user_profile"]=$newArray[$i]['user_profile'];
                $discussion["created_by"]=$newArray[$i]['created_by'];
                $discussion["created_unit"]=$newArray[$i]['created_unit'];
                $discussion["mute_status"]=$newArray[$i]['mute_status'];
                $discussion["mute_id"]=$newArray[$i]['mute_id'];
                if ($newArray[$i]['mute_id']>0) {
                    $discussion["mute_status"]=false;
                }else {
                    $discussion["mute_status"]=true;
                }
                $discussion["total_coments"]=$newArray[$i]['total_coments'];

                array_push($response["discussion"], $discussion);

            }


            if ($arrayQuery1==true || $arrayQuery2==true) {

                $response["discussion_mute"]=$muteDataCommon['discussion_mute'];
                $response["message"]="$datafoundMsg";
                $response["status"]="200";
                echo json_encode($response);

            }else{
                $response["message"]="$noDatafoundMsg";
                $response["status"]="201";
                echo json_encode($response);

            }
        }else if($_POST['getDiscussionDetailsNew']=="getDiscussionDetailsNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($discussion_forum_id, FILTER_VALIDATE_INT) == true){

            $q=$d->selectRow("block_master.block_name,unit_master.unit_name,users_master.user_full_name,users_master.user_profile_pic,discussion_forum_master.*,discussion_forum_mute.mute_id","block_master,unit_master,users_master,discussion_forum_master  LEFT JOIN discussion_forum_mute ON discussion_forum_mute.user_id='$user_id' AND discussion_forum_master.discussion_forum_id=discussion_forum_mute.discussion_forum_id  ","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND users_master.user_id=discussion_forum_master.user_id AND discussion_forum_master.active_status=0 AND discussion_forum_master.society_id='$society_id' AND discussion_forum_master.discussion_forum_id='$discussion_forum_id'","ORDER BY discussion_forum_master.discussion_forum_id DESC LIMIT 100");

            if(mysqli_num_rows($q)>0){

                $response["discussion"] = array();

                while($data=mysqli_fetch_array($q)) {
                    $discussion = array(); 
                    $discussion["discussion_forum_id"]=$data['discussion_forum_id'];
                    $discussion["discussion_forum_title"]=html_entity_decode($data['discussion_forum_title']);
                    $discussion["discussion_forum_description"]=html_entity_decode($data['discussion_forum_description']);
                    $discussion["user_id"]=html_entity_decode($data['user_id']);
                    $discussion["created_date"]=date('h:i A, dS M Y',strtotime($data['created_date'])); 
                    if ($data['discussion_photo']!='') {
                        $discussion["discussion_photo"]= $base_url.'img/request/'.$data['discussion_photo'];
                    } else {
                        $discussion["discussion_photo"]= "";
                    }
                    if ($data['discussion_file']!='') {
                        $discussion["discussion_file"]= $base_url.'img/request/'.$data['discussion_file'];
                    } else {
                        $discussion["discussion_file"]= "";
                    }


                    $created_by= $data['user_full_name'];
                    $user_profile= $base_url."img/users/recident_profile/".$data['user_profile_pic'];
                    if ($data['discussion_forum_for']==0) {
                        $discussion_forum_for= "All Members";
                    } else{
                        $discussion_forum_for= "Your Department";
                    }
                    $discussion["discussion_forum_for"]=$discussion_forum_for;
                    $discussion["user_profile"]=$user_profile;
                    $discussion["created_by"]=html_entity_decode($created_by);
                    $discussion["comment"] = array();

                    $q3=$d->selectRow("
                        discussion_forum_comment.*,
                        bms_admin_master.admin_name,
                        bms_admin_master.admin_profile,
                        users_master.user_full_name,
                        users_master.user_profile_pic,
                        unit_master.unit_name,
                        block_master.block_name,
                        floors_master.floor_name,
                        floors_master.floor_id
                        ","
                        discussion_forum_comment LEFT JOIN bms_admin_master on (bms_admin_master.admin_id=discussion_forum_comment.admin_id ) LEFT JOIN users_master on (users_master.user_id=discussion_forum_comment.user_id) LEFT JOIN unit_master ON (unit_master.unit_id = users_master.unit_id) LEFT JOIN block_master ON (unit_master.block_id = block_master.block_id) LEFT JOIN floors_master ON (floors_master.floor_id = users_master.floor_id)","discussion_forum_comment.discussion_forum_id='$data[discussion_forum_id]' AND discussion_forum_comment.prent_comment_id=0","ORDER BY discussion_forum_comment.comment_id DESC");

                    while ($subData=mysqli_fetch_array($q3)) {

                        $comment = array();
                        $comment["comment_id"]=$subData['comment_id'];
                        $comment["user_id"]=$subData['user_id'];
                        $comment["comment_messaage"]=html_entity_decode($subData['comment_messaage']);
                        $comment["comment_created_date"]=time_elapsed_string($subData['created_date']);
                        if($subData['admin_id']!=0){ 
                            $created_by=$subData['admin_name'];
                            $blockName='(Admin)';
                            $user_profile= $base_url."img/society/".$subData['admin_profile'];
                        } else {
                            $created_by=$subData['user_full_name'] ;
                            $blockName="(".$subData['block_name'].'-'.$subData['floor_name'].')';
                            $user_profile= $base_url."img/users/recident_profile/".$subData['user_profile_pic'];
                        }

                        $comment["user_profile"]=$user_profile.'';
                        $comment["created_by"]=$created_by;
                        $comment["block_name"]=$blockName;

                        $comment["sub_comment"] = array();

                        $q4= $d->selectRow("discussion_forum_comment.*,bms_admin_master.admin_name,bms_admin_master.admin_profile,users_master.user_full_name,users_master.user_profile_pic,unit_master.unit_name,block_master.block_name,floors_master.floor_name,floors_master.floor_id","discussion_forum_comment  LEFT JOIN bms_admin_master on (bms_admin_master.admin_id=discussion_forum_comment.admin_id ) LEFT JOIN users_master on (users_master.user_id=discussion_forum_comment.user_id ) left join unit_master ON (unit_master.unit_id = users_master.unit_id) left join block_master ON (unit_master.block_id = block_master.block_id)  left join floors_master ON (floors_master.floor_id = users_master.floor_id)","discussion_forum_comment.discussion_forum_id='$data[discussion_forum_id]' AND discussion_forum_comment.prent_comment_id='$subData[comment_id]'","ORDER BY discussion_forum_comment.comment_id   DESC");


                        while ($subCommentData=mysqli_fetch_array($q4)) {
                            $sub_comment = array();
                            $sub_comment["comment_id"]=$subCommentData['comment_id'];
                            $sub_comment["prent_comment_id"]=$subCommentData['prent_comment_id'];
                            $sub_comment["user_id"]=$subCommentData['user_id'];
                            $sub_comment["comment_messaage"]=html_entity_decode($subCommentData['comment_messaage']);
                            $sub_comment["comment_created_date"]=time_elapsed_string($subCommentData['created_date']);
                            if($subCommentData['admin_id']!=0){ 

                                $created_by=$subCommentData['admin_name'];
                                $blockName="";
                                $user_profile= $base_url."img/society/".$subCommentData['admin_profile'];
                            } else {

                                $created_by=$subCommentData['user_full_name'];
                                $blockName="(".$subCommentData['block_name'].'-'.$subCommentData['floor_name'].')';
                                $user_profile= $base_url."img/users/recident_profile/".$subCommentData['user_profile_pic'];


                            }
                            $sub_comment["created_by"]=$created_by;
                            $sub_comment["user_profile"]=$user_profile.'';
                            $sub_comment["block_name"]=$blockName;
                            array_push($comment["sub_comment"], $sub_comment); 
                        }

                        array_push($discussion["comment"], $comment); 
                    }


                    array_push($response["discussion"], $discussion);
                    $array1 = true;
                }
            } else {

                $qAdmin=$d->select("bms_admin_master,discussion_forum_master","bms_admin_master.admin_id=discussion_forum_master.admin_id AND discussion_forum_master.active_status=0 AND discussion_forum_master.society_id='$society_id'  AND discussion_forum_master.discussion_forum_id='$discussion_forum_id'","ORDER BY discussion_forum_master.discussion_forum_id DESC LIMIT 100");

                $response["discussion"] = array();

                while($data=mysqli_fetch_array($qAdmin)) {
                    $discussion = array(); 
                    $discussion["discussion_forum_id"]=$data['discussion_forum_id'];
                    $discussion["discussion_forum_title"]=html_entity_decode($data['discussion_forum_title']);
                    $discussion["discussion_forum_description"]=html_entity_decode($data['discussion_forum_description']);
                    $discussion["user_id"]=html_entity_decode($data['user_id']);
                    $discussion["created_date"]=date('d M Y, h:i A',strtotime($data['created_date'])); 
                    if ($data['discussion_photo']!='') {
                        $discussion["discussion_photo"]= $base_url.'img/request/'.$data['discussion_photo'];
                    } else {
                        $discussion["discussion_photo"]= "";
                    }
                    if ($data['discussion_file']!='') {
                        $discussion["discussion_file"]= $base_url.'img/request/'.$data['discussion_file'];
                    } else {
                        $discussion["discussion_file"]= "";
                    }


                    $created_by= 'Admin ('. $data['admin_name'].')';
                    $user_profile= $base_url."img/society/".$data['admin_profile'];

                    if ($data['discussion_forum_for']==0) {
                        $discussion_forum_for= "All Members";
                    } else{
                        $discussion_forum_for= "Your Department";
                    }
                    $discussion["discussion_forum_for"]=$discussion_forum_for;
                    $discussion["user_profile"]=$user_profile;
                    $discussion["created_by"]=html_entity_decode($created_by);

                    $discussion["comment"] = array();
                    $q3=$d->selectRow("discussion_forum_comment.*,bms_admin_master.admin_name,bms_admin_master.admin_profile,users_master.user_full_name,users_master.user_profile_pic,unit_master.unit_name,block_master.block_name,floors_master.floor_name","discussion_forum_comment  LEFT JOIN bms_admin_master on (bms_admin_master.admin_id=discussion_forum_comment.admin_id ) LEFT JOIN users_master on (users_master.user_id=discussion_forum_comment.user_id ) left join unit_master ON (unit_master.unit_id = users_master.unit_id) left join block_master ON (unit_master.block_id = block_master.block_id)  LEFT JOIN floors_master ON (floors_master.floor_id = users_master.floor_id) ","discussion_forum_comment.discussion_forum_id='$data[discussion_forum_id]' AND discussion_forum_comment.prent_comment_id=0","ORDER BY discussion_forum_comment.comment_id   DESC");
                    while ($subData=mysqli_fetch_array($q3)) {
                        $comment = array();
                        $comment["comment_id"]=$subData['comment_id'];
                        $comment["user_id"]=$subData['user_id'];
                        $comment["comment_messaage"]=html_entity_decode($subData['comment_messaage']);
                        $comment["comment_created_date"]=time_elapsed_string($subData['created_date']);
                        if($subData['admin_id']!=0){ 
                            $created_by=$subData['admin_name'];
                            $blockName='(Admin)';
                            $user_profile= $base_url."img/society/".$subData['admin_profile'];
                        } else {
                            $created_by=$subData['user_full_name'] ;
                            $blockName="(".$subData['block_name'].'-'.$subData['floor_name'].')';
                            $user_profile= $base_url."img/users/recident_profile/".$subData['user_profile_pic'];
                        }
                        $comment["user_profile"]=$user_profile.'';
                        $comment["created_by"]=$created_by;
                        $comment["block_name"]=$blockName;

                        $comment["sub_comment"] = array();
                        $q4= $d->selectRow("discussion_forum_comment.*,bms_admin_master.admin_name,bms_admin_master.admin_profile,users_master.user_full_name,users_master.user_profile_pic,unit_master.unit_name,block_master.block_name,floors_master.floor_name","discussion_forum_comment  LEFT JOIN bms_admin_master on (bms_admin_master.admin_id=discussion_forum_comment.admin_id ) LEFT JOIN users_master on (users_master.user_id=discussion_forum_comment.user_id ) left join unit_master ON (unit_master.unit_id = users_master.unit_id) left join block_master ON (unit_master.block_id = block_master.block_id)  LEFT JOIN floors_master ON (floors_master.floor_id = users_master.floor_id) ","discussion_forum_comment.discussion_forum_id='$data[discussion_forum_id]' AND discussion_forum_comment.prent_comment_id='$subData[comment_id]'","ORDER BY discussion_forum_comment.comment_id   DESC");


                        while ($subCommentData=mysqli_fetch_array($q4)) {
                            $sub_comment = array();
                            $sub_comment["comment_id"]=$subCommentData['comment_id'];
                            $sub_comment["prent_comment_id"]=$subCommentData['prent_comment_id'];
                            $sub_comment["user_id"]=$subCommentData['user_id'];
                            $sub_comment["comment_messaage"]=html_entity_decode($subCommentData['comment_messaage']);
                            $sub_comment["comment_created_date"]=time_elapsed_string($subCommentData['created_date']);
                            if($subCommentData['admin_id']!=0){ 

                                $created_by=$subCommentData['admin_name'];
                                $blockName="";
                                $user_profile= $base_url."img/society/".$subCommentData['admin_profile'];
                            } else {

                                $created_by=$subCommentData['user_full_name'];
                                $blockName="(".$subCommentData['block_name'].'-'.$subCommentData['floor_name'].')';
                                $user_profile= $base_url."img/users/recident_profile/".$subCommentData['user_profile_pic'];


                            }
                            $sub_comment["created_by"]=$created_by;
                            $sub_comment["user_profile"]=$user_profile.'';
                            $sub_comment["block_name"]=$blockName;
                            array_push($comment["sub_comment"], $sub_comment); 
                        }

                        array_push($discussion["comment"], $comment); 
                    }


                    array_push($response["discussion"], $discussion);
                    $array2 = true;
                }


            }


            if ($array1==true || $array2==true) {
                $response["message"]="$datafoundMsg";
                $response["status"]="200";
                echo json_encode($response);

            }else{
                $response["message"]="$noDatafoundMsg";
                $response["status"]="201";
                echo json_encode($response);

            }
        }else{
            $response["message"]="wrong tag";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>