<?php

$user_block_id = $block_id;
$user_floor_id = $floor_id;
$user_user_id = $user_id;

$qry = $d->select("app_access_master","access_type = '$access_type'");

$accessResponseData = array();

if (mysqli_num_rows($qry) > 0) {

    $userIDArray = array();

    while ($data = mysqli_fetch_array($qry)) {

        $access_for = $data['access_for'];
        $access_for_id = $data['access_for_id'];
        $access_by_id = $data['access_by_id'];

        $access_for_id_array = explode(",",$access_for_id);

        if ($access_for == 0) {
            array_push($userIDArray,$access_by_id);
        }else if ($user_block_id > 0 && $access_for == 1 && in_array($user_block_id, $access_for_id_array)) {
            array_push($userIDArray,$access_by_id);
        }else if ($user_floor_id > 0 && $access_for == 2 && in_array($user_floor_id, $access_for_id_array)) {
            array_push($userIDArray,$access_by_id);
        }else if ($user_user_id > 0 && $access_for == 3 && in_array($user_user_id, $access_for_id_array)) {
            array_push($userIDArray,$access_by_id);
        }
    }
    $userFcmIds = join("','",$userIDArray); 
}
