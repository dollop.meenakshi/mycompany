<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        $startDate=date("Y-m-01");
        $currentDate=date("Y-m-d");
        $lastDate=date("Y-m-t");

        if($_POST['checkAccessPunchInReq']=="checkAccessPunchInReq" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $punch_in_req_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['checkAccessPunchOutMissingReq']=="checkAccessPunchOutMissingReq" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $punch_out_missing_req_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['checkAccessPendingAttendance']=="checkAccessPendingAttendance" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $pending_attendance_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['checkAccessAttendanceHistory']=="checkAccessAttendanceHistory" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $view_employee_attendance_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['checkAccessAbsentPresent']=="checkAccessAbsentPresent" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $view_absent_present_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['getAttendanceType']=="getAttendanceType" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $today_date = date('Y-m-d');
            $dateTimeCurrent = date('Y-m-d H:i:s');

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
            }

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $shift_type = $shiftData['shift_type'];

            $response['already_punch_in'] = false;
            $response['already_punch_out'] = false;
            $response['work_report_added'] = false;

            if ($shift_type == "Night") {

                $response['night_shift'] = true;

                $attendance_date_start = date('Y-m-d',strtotime('-1 days'));

                $isAlreadyPunchOut = $d->select("attendance_master","user_id = '$user_id' AND punch_in_time != '00:00:00' AND punch_out_time != '00:00:00' AND ((attendance_date_start = '$attendance_date_start' AND attendance_date_end = '$today_date') OR (attendance_date_start = '$today_date' AND attendance_date_end = '$today_date'))","ORDER BY attendance_id DESC LIMIT 1");

                if (mysqli_num_rows($isAlreadyPunchOut) > 0) {
                    $response['already_punch_out'] = true;
                }else{
                    $response['already_punch_out'] = false;
                }

                $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_start = '$attendance_date_start' AND attendance_date_end = '0000-00-00' AND punch_out_time = '00:00:00'","ORDER BY attendance_id DESC LIMIT 1");

                if (mysqli_num_rows($getTodayAttendace) == 0) {

                    $attendance_date_start = date('Y-m-d');

                    $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_start = '$attendance_date_start' AND attendance_date_end = '0000-00-00' AND punch_out_time = '00:00:00'","ORDER BY attendance_id DESC LIMIT 1");
                }

                $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND work_report_date = '$attendance_date_start' AND society_id = '$society_id'");

                if ($workReportQry > 0) {
                    $response['work_report_added'] = true;
                }

                
            }else{

                $attendance_date_start = date('Y-m-d');

                $response['night_shift'] = false;

                $isAlreadyPunchOut = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_start = '$attendance_date_start' AND attendance_date_end = '$attendance_date_start' AND punch_in_time != '00:00:00' AND punch_out_time != '00:00:00'","ORDER BY attendance_id DESC LIMIT 1");

                if (mysqli_num_rows($isAlreadyPunchOut) > 0) {
                    $response['already_punch_out'] = true;
                }else{
                    $response['already_punch_out'] = false;
                }

                $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_start = '$attendance_date_start' AND attendance_date_end = '0000-00-00' AND punch_out_time = '00:00:00'","ORDER BY attendance_id DESC LIMIT 1");

                $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND work_report_date = '$attendance_date_start' AND society_id = '$society_id'");

                if ($workReportQry > 0) {
                    $response['work_report_added'] = true;
                } 
            }

            if (mysqli_num_rows($getTodayAttendace) > 0) {

                $response['already_punch_in'] = true;

                $attData = mysqli_fetch_array($getTodayAttendace);

                if ($attendance_id == null || $attendance_id=="" || $attendance_id == "0") {
                    $attendance_id = $attData["attendance_id"];
                }

                $attStartDate = $attData["attendance_date_start"];
                $punchInTime = $attData["punch_in_time"];

                $pDate = $attStartDate." ".$punchInTime;

                $inTimeAMPM = date('Y-m-d H:i:s',strtotime($pDate));
                $dateTimeCurrent = date('Y-m-d H:i:s',strtotime($dateTimeCurrent));

                $date_a = new DateTime($inTimeAMPM);
                $date_b = new DateTime($dateTimeCurrent);

                $interval = $date_a->diff($date_b);
               
                $hours = $interval->format('%h');
                $minutes = $interval->format('%i');
                $sec = $interval->format('%s');

                if ($sec < 45) {
                   $sec += 5;
                }

                $finalTime =  $attStartDate." ".sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

                $time_diff = sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);
                
                $response['attendance_id'] =$attData["attendance_id"];
                $response['punch_in_time'] =$attData["punch_in_time"];
                $response['date_time'] =$finalTime;
                $response['time_diff'] =$time_diff;
            }else{
                $response['attendance_id'] ="0";
            }

            $yesterdayDate = date('Y-m-d',strtotime("-1 days"));

            $getYesterdayAttendace = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_start = '$yesterdayDate' AND attendance_date_end = '0000-00-00' AND punch_out_time = '00:00:00'");

            if (mysqli_num_rows($getYesterdayAttendace) > 0) {
                $response['yesterday_punch_out_missing'] = true;
            }else{
                $response['yesterday_punch_out_missing'] = false;
            }


            $getBreakTime = $d->select("attendance_break_history_master","attendance_id = '$attendance_id' AND break_start_date='$attendance_date_start' AND break_out_time = '00:00:00' AND break_end_date = '0000-00-00'","ORDER BY attendance_break_history_id DESC LIMIT 1");

            if (mysqli_num_rows($getBreakTime) > 0) {

                $breakData = mysqli_fetch_array($getBreakTime);

                $breakStartDate = $breakData["break_start_date"];
                $breakInTime = $breakData["break_in_time"];

                $date_a1 = new DateTime($breakStartDate." ".$breakInTime);
                $date_b1 = new DateTime($dateTimeCurrent);

                $interval1 = $date_a1->diff($date_b1);

                $hours1 = $interval1->format('%h');
                $minutes1 = $interval1->format('%i');
                $sec1 = $interval1->format('%s');

                if ($sec1 < 45) {
                    $sec1 += 5;
                }

                $break_date_time =  $breakStartDate." ".sprintf('%02d:%02d:%02d', $hours1, $minutes1,$sec1);

                $break_time_diff = sprintf('%02d:%02d:%02d', $hours1, $minutes1,$sec1);
                
                $response['attendance_break_history_id'] =$breakData["attendance_break_history_id"];
                $response['break_in_time'] =$breakData["break_in_time"];
                $response['attendance_type_id'] =$breakData["attendance_type_id"];
                $response['break_date_time'] =$break_date_time;
                $response['break_time_diff'] =$break_time_diff;
            }else{
                $response['attendance_break_history_id'] ="0";
            }

            $getBreakTimeTypeIds = $d->select("attendance_break_history_master","attendance_id = '$attendance_id' AND break_start_date = '$attendance_date_start' AND break_out_time != '00:00:00' AND break_end_date != '0000-00-00'");

            $attTypeIdArray="";

            if (mysqli_num_rows($getBreakTimeTypeIds)) {
                while($typeData = mysqli_fetch_array($getBreakTimeTypeIds)){

                    $typeData = array_map("html_entity_decode", $typeData);

                    $qry = $d->select("attendance_type_master","society_id='$society_id' AND attendance_type_id = '$typeData[attendance_type_id]' AND is_multipletime_use = '0'");

                    if (mysqli_num_rows($qry) > 0) {
                        $attTypeIdArray = $attTypeIdArray."'".$typeData["attendance_type_id"]."',";
                    }
                }
            }

            $attTypeIdArray = rtrim($attTypeIdArray, ",");

            if ($attTypeIdArray!="") {
                $attendanceTypeQry = $d->select("attendance_type_master","society_id='$society_id'  AND attendance_type_active_status = '0' AND attendance_type_delete = '0' AND attendance_type_id NOT IN ($attTypeIdArray)");
            }else{
                $attendanceTypeQry = $d->select("attendance_type_master","society_id='$society_id' AND attendance_type_active_status = '0' AND attendance_type_delete = '0'");
            }       

            $response["attendance_types"] = array();

            if(mysqli_num_rows($attendanceTypeQry)>0){
                

                while($leaveTypeData=mysqli_fetch_array($attendanceTypeQry)) {

                    $leaveTypeData = array_map("html_entity_decode", $leaveTypeData);

                    $attendance_type = array(); 

                    $attendance_type["attendance_type_id"] = $leaveTypeData['attendance_type_id'];
                    $attendance_type["attendance_type_name"] = $leaveTypeData['attendance_type_name'];

                    array_push($response["attendance_types"], $attendance_type);
                }             
            }/*else{
                if ($attData["attendance_id"] == 0 && count($response["attendance_types"]) == 0) {
                    $response["message"] = "Error";
                    $response["status"] = "201";
                    echo json_encode($response); 
                }else{
                    $response["message"] = "Success";
                    $response["status"] = "200";
                    echo json_encode($response); 
                }
            }*/

            $userQry = $d->selectRow("face_data_image,face_data_image_two","users_master","user_id = $user_id");

            $userData = mysqli_fetch_array($userQry);

            if ($userData['face_data_image'] != '') {
                $response["face_data_image"] = $base_url . "img/attendance_face_image/" . $userData['face_data_image'];
            } else {
                $response["face_data_image"] = "";
            }

            if ($userData['face_data_image_two'] != '') {
                $response["face_data_image_two"] = $base_url . "img/attendance_face_image/" . $userData['face_data_image_two'];
            } else {
                $response["face_data_image_two"] = "";
            }

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
                 
        }else if($_POST['attendancePunchIn']=="attendancePunchIn" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $punchInTime =date('H:i:s');
            $punchInDate =date('Y-m-d');

            $punchInDateTime = $punchInDate." ".$punchInTime;

            $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND unit_id = '$unit_id' AND attendance_date_start = '$punchInDate'");

            if (mysqli_num_rows($isPunchIn) > 0) {
                $response["message"] = "Already Punched In";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
            }

            $wfh_attendance = "0";

            $wfhQry = $d->selectRow("wfh_id,user_id,wfh_status,wfh_start_date,wfh_latitude,wfh_longitude,wfh_attendance_range","wfh_master","society_id ='$society_id' AND user_id='$user_id' AND wfh_status = '1' AND wfh_start_date = '$punchInDate'");
            $wfhData = mysqli_fetch_array($wfhQry);

            if (mysqli_num_rows($wfhQry) > 0) {

                $wfhLat = $wfhData['wfh_latitude'];
                $wfhLng = $wfhData['wfh_longitude'];
                $wfhRange = $wfhData['wfh_attendance_range'];

                if ($wfhData['wfh_status'] == '1') {
                    $wfh_attendance = "1";
                }else{
                    $wfh_attendance = "0";
                }
            }

            // Late IN

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shift_start_time = $shiftData['shift_start_time'];
            $late_time_start = $shiftData['late_time_start'];

            $min = date('i',strtotime($late_time_start));
            $defaultINTime = $punchInDate." ".$shift_start_time;
            $strMin = "+".$min." minutes";
            $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

            if ($punchInDateTime > $defaultTime) {
                $late_in = "1";
            }else{
                $late_in = "0";
            }

            $punch_in_image = "";

            if ($_FILES["punch_in_image"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["punch_in_image"]["tmp_name"];
                $extId = pathinfo($_FILES['punch_in_image']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["punch_in_image"]["name"]);
                    $punch_in_image = "punch_in_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["punch_in_image"]["tmp_name"], "../img/attendance/" . $punch_in_image);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $attendance_created_time = date('Y-m-d H:i:s');

            $m->set_data('shift_time_id',$shiftTimeId);
            $m->set_data('society_id',$society_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('attendance_date_start',$punchInDate);
            $m->set_data('punch_in_time',$punchInTime);
            $m->set_data('punch_in_latitude',$punch_in_latitude);
            $m->set_data('punch_in_longitude',$punch_in_longitude);
            $m->set_data('attendance_reason',$attendance_reason);
            $m->set_data('punch_in_image',$punch_in_image);
            $m->set_data('attendance_status',$attendance_status);
            $m->set_data('wfh_attendance',$wfh_attendance);
            $m->set_data('radiusInMeter',$radiusInMeter);
            $m->set_data('totalKm',$totalKm);
            $m->set_data('punch_in_in_range',$punch_in_in_range);
            $m->set_data('late_in',$late_in);
            $m->set_data('late_in_reason',$late_in_reason);
            $m->set_data('attendance_created_time',date('Y-m-d H:i:s'));

            $a = array(
                'shift_time_id'=>$m->get_data('shift_time_id'),
                'society_id' =>$m->get_data('society_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'user_id'=>$m->get_data('user_id'),
                'attendance_date_start'=>$m->get_data('attendance_date_start'),
                'punch_in_time'=>$m->get_data('punch_in_time'),
                'punch_in_latitude'=>$m->get_data('punch_in_latitude'),
                'punch_in_longitude'=>$m->get_data('punch_in_longitude'),
                'attendance_reason'=>$m->get_data('attendance_reason'),
                'punch_in_image'=>$m->get_data('punch_in_image'),
                'attendance_status'=>$m->get_data('attendance_status'),
                'wfh_attendance'=>$m->get_data('wfh_attendance'),
                'attendance_range_in_meter'=>$m->get_data('radiusInMeter'),
                'attendance_range_in_km'=>$m->get_data('totalKm'),
                'punch_in_in_range'=>$m->get_data('punch_in_in_range'),
                'late_in'=>$m->get_data('late_in'),
                'late_in_reason'=>$m->get_data('late_in_reason'),
                'attendance_created_time'=>$m->get_data('attendance_created_time'),
            );

            $punchInQry = $d->insert("attendance_master",$a);

            $attendance_id = $con->insert_id;

            if ($punchInQry == true) {

                if ($need_approval == "true") {
 
                    $title = "Attendance Approval Request";
                    $description = "Requested by, ".$user_name;

                    $access_type = $pending_attendance_access;

                    include 'check_access_data_user.php';

                    // To Employee App
                    if (count($userIDArray) > 0) {

                        $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                        $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                        $dataAry = array(
                            "block_id"=>$block_id,
                            "floor_id"=>$floor_id,
                            "user_id"=>$user_id,
                        );

                        $dataJson = json_encode($dataAry);

                        $nResident->noti("pending_attendance","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                        $nResident->noti_ios("pending_attendance","",$society_id,$fcmArrayIosPA,$title,$description,$dataJson);

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"pending_attendance","attendance_tracker.png",$dataJson,"users_master.user_id IN ('$userFcmIds')");
                    }

                    $block_id=$d->getBlockid($user_id);
                    $fcmArray=$d->selectAdminBlockwise("17",$block_id,"android");
                    $fcmArrayIos=$d->selectAdminBlockwise("17",$block_id,"ios");
              
                    $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"pendingAttendance");

                    $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"pendingAttendance");

                    $notiAry = array(
                      'society_id'=>$society_id,
                      'notification_tittle'=>$title,
                      'notification_description'=>$description,
                      'notifiaction_date'=>date('Y-m-d H:i'),
                      'notification_action'=>"pendingAttendance",
                      'admin_click_action '=>"pendingAttendance",
                      'notification_logo'=>'attendance_tracker.png',
                    );
                                
                    $d->insert("admin_notification",$notiAry);
                }

                if ($need_approval == "true") {
                    $response["message"] = "Your attendance needs approval from admin";
                }else{
                    $response["message"] = "Attendance Approved";
                }
                $response["attendance_id"] = $attendance_id."";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['checkLateIn']=="checkLateIn" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $punchInTime =date('H:i:s');
            $punchInDate =date('Y-m-d');

            $punchInTimeDate = $punchInDate." ".$punchInTime;

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
            }

            // Late IN

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shift_start_time = $shiftData['shift_start_time'];
            $late_time_start = $shiftData['late_time_start'];
            $late_in_reason = $shiftData['late_in_reason'];

            if ($late_in_reason == "1") {
                $min = date('i',strtotime($late_time_start));
                $defaultINTime = $punchInDate." ".$shift_start_time;
                $strMin = "+".$min." minutes";
                $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

                if ($punchInTimeDate > $defaultTime) {
                    $late_in = true;
                }else{
                    $late_in = false;
                }
            }else{
                $late_in = false;
            }
            
            $response["late_in"] = $late_in;
            $response["status"] = "200";
            echo json_encode($response);
                 
        }else if($_POST['checkEarlyOut']=="checkEarlyOut" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $punchInTime =date('H:i:s');
            $punchInDate =date('Y-m-d');

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
            }

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shift_start_time = $shiftData['shift_start_time'];
            $shift_end_time = $shiftData['shift_end_time'];
            $late_time_start = $shiftData['late_time_start'];
            $early_out_time = $shiftData['early_out_time'];
            $half_day_time_start = $shiftData['half_day_time_start'];
            $halfday_before_time = $shiftData['halfday_before_time'];
            $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
            $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
            $shift_type = $shiftData['shift_type'];
            $maximum_in_out = $shiftData['maximum_in_out'];
            $early_out_reason = $shiftData['early_out_reason'];

            if ($early_out_reason == "1") {
                $min = date('i',strtotime($early_out_time));
                $defaultOutTime = $punchOUTDate." ".$shift_end_time;
                $strMin = "-".$min." minutes";
                $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

                if ($datePunchOUT >= $defaultOutTime) {
                    $early_out = false;
                }else{
                    $early_out = true;              
                }
            }else{
                $early_out = false; 
            }
            
            $response["early_out"] = $early_out;
            $response["status"] = "200";
            echo json_encode($response);
                 
        }else if($_POST['punchInRequest']=="punchInRequest" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $uq = $d->selectRow("*","users_master","society_id ='$society_id' AND user_id='$user_id'");
            $ud = mysqli_fetch_array($uq);

            if ($ud['shift_time_id'] == 0) {
                $response["message"] = "Shift not assigned";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND society_id = '$society_id'  AND attendance_date_start = '$punch_in_date'");

            if (mysqli_num_rows($isPunchIn) > 0) {
                $response["message"] = "You have already punched in for ".$punch_in_date;
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $m->set_data('shift_time_id',$shift_time_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('attendance_date_start',$punch_in_date);
            $m->set_data('attendance_date_end',$punch_out_date);
            $m->set_data('punch_in_time',$punch_in_time);
            $m->set_data('punch_out_time',$punch_out_time);
            $m->set_data('attendance_reason',$punch_in_reason);
            $m->set_data('attendance_status',"0");
            $m->set_data('punch_in_request',"1");

            $a = array(
                'shift_time_id'=>$m->get_data('shift_time_id'),
                'society_id' =>$m->get_data('society_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'user_id'=>$m->get_data('user_id'),
                'attendance_date_start'=>$m->get_data('attendance_date_start'),
                'attendance_date_end'=>$m->get_data('attendance_date_end'),
                'punch_in_time'=>$m->get_data('punch_in_time'),
                'punch_out_time'=>$m->get_data('punch_out_time'),
                'attendance_reason'=>$m->get_data('attendance_reason'),
                'attendance_status'=>$m->get_data('attendance_status'),
                'punch_in_request'=>$m->get_data('punch_in_request'),
            );

            $punchInQry = $d->insert("attendance_master",$a);

            $attendance_id = $con->insert_id;

            if ($punchInQry == true) {

                $title = "Punch In Request";
                $description = "Requested by, ".$user_name."\nPunch In: ".date('d-m-Y',strtotime($punch_in_date))." ".date('h:i A',strtotime($punch_in_time))."\nPunch Out: ".date('d-m-Y',strtotime($punch_out_date))." ".date('h:i A',strtotime($punch_out_time));

                // Notification by Access

                $access_type = $punch_in_req_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmAccessAry=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmAccessAryiOS=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $nResident->noti("punch_in_request","",$society_id,$fcmAccessAry,$title,$description,"");
                    $nResident->noti_ios("punch_in_request","",$society_id,$fcmAccessAryiOS,$title,$description,"");

                    $d->insertUserNotification($society_id,$title,$description,"punch_in_request","attendance_tracker.png","users_master.user_id IN ('$userFcmIds')");
                }

                //Notification To Admin

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("17",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("17",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"pendingAttendance");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"pendingAttendance");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"pendingAttendance",
                  'admin_click_action '=>"pendingAttendance",
                  'notification_logo'=>'attendance_tracker.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Request Sent";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['approvePunchInRequest']=="approvePunchInRequest" && $attendance_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_status',"1");
            $m->set_data('attendance_status_change_by',$user_id);
            $m->set_data('attendance_date_start',$attendance_date_start);
            $m->set_data('attendance_date_end',$attendance_date_end);
            $m->set_data('punch_in_time',$punch_in_time);
            $m->set_data('punch_out_time',$punch_out_time);

            $a = array(
                'attendance_status'=>$m->get_data('attendance_status'),
                'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
                'attendance_date_start'=>$m->get_data('attendance_date_start'),
                'attendance_date_end'=>$m->get_data('attendance_date_end'),
                'punch_in_time'=>$m->get_data('punch_in_time'),
                'punch_out_time'=>$m->get_data('punch_out_time'),
            );

            $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id' AND user_id = '$attendance_user_id'");

            if ($punchInQry == true) {

                $title = "Attendance Approved";
                $description = "For the Date: ".date('d-m-Y',strtotime($attendance_date_start))." Time: ".date('h:i A',strtotime($punch_in_time))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                $response["message"] = "Attendance Approved";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['declinePunchInRequest']=="declinePunchInRequest" && $attendance_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_status',"2");
            $m->set_data('attendance_status_change_by',$user_id);
            $m->set_data('attendance_declined_reason',$attendance_declined_reason);

            $a = array(
                'attendance_status'=>$m->get_data('attendance_status'),
                'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
                'attendance_declined_reason'=>$m->get_data('attendance_declined_reason'),
            );

            $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id' AND user_id = '$attendance_user_id'");

            if ($punchInQry == true) {

                $title = "Attendance Approval Declined";
                $description = "For the Date: ".date('d-m-Y',strtotime($attendance_date_start))." Time: ".date('h:i A',strtotime($punch_in_time))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                $response["message"] = "Attendance Approval Declined";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getPunchInRequest']=="getPunchInRequest" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $punch_in_req_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND attendance_master.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND attendance_master.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $punchInReqQry = $d->select("attendance_master,users_master,floors_master,block_master","attendance_master.punch_in_request = '1' 
                AND attendance_master.attendance_status = '0' 
                AND users_master.user_id = attendance_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id $appendQuery","ORDER BY attendance_master.attendance_id DESC $LIMIT_DATA");

            if(mysqli_num_rows($punchInReqQry)>0){

                $response["punch_in_requests"] = array();        

                while($data=mysqli_fetch_array($punchInReqQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $punchIn = array();

                    $punchIn['user_id'] = $data['user_id'];
                    $punchIn['user_full_name'] = $data['user_full_name'];
                    $punchIn['branch_name'] = $data['block_name'];
                    $punchIn['department_name'] = $data['floor_name'];
                    $punchIn['user_designation'] = $data['user_designation'];
                    $punchIn['attendance_id'] = $data['attendance_id'];
                    $punchIn['block_id'] = $data['block_id'];
                    $punchIn['floor_id'] = $data['floor_id'];
                    $punchIn['unit_id'] = $data['unit_id'];
                    $punchIn['society_id'] = $data['society_id'];

                    if ($data['user_profile_pic'] != '') {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $punchIn['attendance_date_start'] = $data['attendance_date_start'];
                    $punchIn['attendance_date_start_view'] = date('D, d F Y',strtotime($data['attendance_date_start']));

                    $punchIn['attendance_date_end'] = $data['attendance_date_end'];
                    $punchIn['attendance_date_end_view'] = date('D, d F Y',strtotime($data['attendance_date_end']));
                    $punchIn['punch_in_time'] = $data['punch_in_time'];
                    $punchIn['punch_in_time_view'] = date('h:i A',strtotime($data['punch_in_time']));
                    $punchIn['punch_out_time'] = $data['punch_out_time'];
                    $punchIn['punch_out_time_view'] = date('h:i A',strtotime($data['punch_out_time']));
                    $punchIn['attendance_reason'] = $data['attendance_reason'];

                    array_push($response['punch_in_requests'],$punchIn);
                }
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found";
                $response["status"] = "201";
                echo json_encode($response);
            }             
        }else if($_POST['getPendingAttendance']=="getPendingAttendance" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            
            $access_type = $pending_attendance_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND attendance_master.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND attendance_master.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $punchInQry = $d->select("attendance_master,users_master,floors_master,block_master","
                attendance_master.punch_in_request = '0' 
                AND attendance_master.attendance_status = '0'  
                AND attendance_master.society_id = '$society_id' 
                AND users_master.user_id = attendance_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id $appendQuery","ORDER BY attendance_master.attendance_id DESC $LIMIT_DATA");
            

            if(mysqli_num_rows($punchInQry)>0){

                $response['pending_attendance'] = array();
                
                while($data=mysqli_fetch_array($punchInQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $punchIn = array();

                    $block_id = $data['block_id'];

                    $punchIn['user_id'] = $data['user_id'];
                    $punchIn['user_full_name'] = $data['user_full_name'];
                    $punchIn['branch_name'] = $data['block_name'];
                    $punchIn['department_name'] = $data['floor_name'];
                    $punchIn['user_designation'] = $data['user_designation'];
                    $punchIn['attendance_id'] = $data['attendance_id'];
                    $punchIn['block_id'] = $data['block_id'];
                    $punchIn['floor_id'] = $data['floor_id'];
                    $punchIn['unit_id'] = $data['unit_id'];
                    $punchIn['society_id'] = $data['society_id'];
                    $punchIn['punch_in_latitude'] = $data['punch_in_latitude'];
                    $punchIn['punch_in_longitude'] = $data['punch_in_longitude'];
                    $punchIn['punch_out_latitude'] = $data['punch_out_latitude'];
                    $punchIn['punch_out_longitude'] = $data['punch_out_longitude'];
                    $punchIn['punch_in_in_range'] = $data['punch_in_in_range'];
                    $punchIn['punch_out_in_range'] = $data['punch_out_in_range'];
                    $punchIn['punch_out_meter'] = $data['punch_out_meter'];
                    $punchIn['punch_out_km'] = $data['punch_out_km'];
                    $punchIn['wfh_attendance'] = $data['wfh_attendance'];
                    $punchIn['attendance_range_in_meter'] = $data['attendance_range_in_meter'];
                    $punchIn['totalKm'] = $data['attendance_range_in_km'];
                    $punchIn['attendance_reason'] = $data['attendance_reason'];
                    $punchIn['punch_out_reason'] = $data['punch_out_reason'];
                    $punchIn['late_in_reason'] = $data['late_in_reason'];
                    $punchIn['early_out_reason'] = $data['early_out_reason'];

                    if ($data['late_in'] == '1') {
                        $punchIn["late_in"] = " (Late In)";
                    } else {
                        $punchIn["late_in"] = "";
                    }

                    if ($data['early_out'] == '1') {
                        $punchIn["early_out"] = " (Early Out)";
                    } else {
                        $punchIn["early_out"] = "";
                    }

                    if ($data['punch_in_image'] != '') {
                        $punchIn["punch_in_image"] = $base_url . "img/attendance/" . $data['punch_in_image'];
                    } else {
                        $punchIn["punch_in_image"] = "";
                    }

                    if ($data['punch_out_image'] != '') {
                        $punchIn["punch_out_image"] = $base_url . "img/attendance/" . $data['punch_out_image'];
                    } else {
                        $punchIn["punch_out_image"] = "";
                    }

                    if ($data['user_profile_pic'] != '') {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    if ($data['attendance_date_start'] == $currentDate) {
                        $punchIn['is_today'] = true;
                    }else{
                        $punchIn['is_today'] = false;
                    }

                    $attendance_date_start= $data['attendance_date_start'];

                    $punchIn['attendance_date_start'] = $attendance_date_start;
                    $punchIn['attendance_date_start_view'] = date('D, d F Y',strtotime($attendance_date_start));

                    $punchIn['punch_in_time'] = $data['punch_in_time'];
                    $punchIn['punch_in_time_view'] = date('h:i A',strtotime($data['punch_in_time']));

                    if($data['attendance_date_end'] != '0000-00-00'){
                        $punchIn['attendance_date_end'] = $data['attendance_date_end'];
                        $punchIn['attendance_date_end_view'] = date('D, d F Y',strtotime($data['attendance_date_end']));
                    }else{
                        $punchIn['attendance_date_end'] = "";
                        $punchIn['attendance_date_end_view'] = "";
                    }

                    if($data['punch_out_time'] != '00:00:00'){
                        $punchIn['punch_out_time'] = $data['punch_out_time'];
                        $punchIn['punch_out_time_view'] = date('h:i A',strtotime($data['punch_out_time']));
                    }else{
                        $punchIn['punch_out_time'] = "";
                        $punchIn['punch_out_time_view'] = "";
                    }

                    $punchIn['location_list'] = array();
                    $geoArray = array();

                    $qqGeo = $d->select("user_geofence","user_id = '$data[user_id]'");

                    if (mysqli_num_rows($qqGeo) > 0) {
                        while($dataGeo = mysqli_fetch_array($qqGeo)){


                            $geoArray['geofence_address'] = $dataGeo['user_geo_address'];
                            $geoArray['geofence_range'] = $dataGeo['user_geofence_range'];
                            $geoArray['geofence_longitude'] = $dataGeo['user_geofence_longitude'];
                            $geoArray['geofence_latitude'] = $dataGeo['user_geofence_latitude'];

                            array_push($punchIn['location_list'],$geoArray);

                        }
                    }

                    $blockQry = $d->select("block_master","society_id = '$society_id' AND block_geofence_latitude != '' AND block_geofence_longitude != '' AND block_id = '$block_id'");

                    if (mysqli_num_rows($blockQry) > 0) {

                        $data = mysqli_fetch_array($blockQry);

                        $geoArray['geofence_address'] = "Company Location";
                        $geoArray['geofence_latitude'] = $data['block_geofence_latitude'];
                        $geoArray['geofence_longitude'] = $data['block_geofence_longitude'];
                        $geoArray['geofence_range'] = $data['block_geofence_range'];

                        array_push($punchIn['location_list'],$geoArray);
                    }

                    $wfhQry = $d->selectRow("wfh_address,wfh_longitude,wfh_latitude,wfh_attendance_range","wfh_master","user_id = '$data[user_id]' AND wfh_start_date = '$attendance_date_start' AND wfh_status = '1' AND society_id = '$society_id'");

                    if (mysqli_num_rows($wfhQry) > 0) {
                        while($dataGeo = mysqli_fetch_array($wfhQry)){


                            $geoArray['geofence_address'] = 'WFH Location: '.$dataGeo['wfh_address'];
                            $geoArray['geofence_range'] = $dataGeo['wfh_attendance_range'];
                            $geoArray['geofence_longitude'] = $dataGeo['wfh_longitude'];
                            $geoArray['geofence_latitude'] = $dataGeo['wfh_latitude'];

                            array_push($punchIn['location_list'],$geoArray);

                        }
                    }


                    $userQry = $d->select("users_master","user_id = '$user_id' AND society_id = '$society_id' AND user_geofence_latitude != '' AND user_geofence_longitude != '' AND user_geofence_range != ''");

                    if (mysqli_num_rows($userQry) == 0) {

                        $blockQry = $d->select("block_master","society_id = '$society_id' AND block_geofence_latitude != '' AND block_geofence_longitude != '' AND block_id = '$block_id'");

                        if (mysqli_num_rows($blockQry) > 0) {

                            $data = mysqli_fetch_array($blockQry);

                            $punchIn['geofence_latitude'] = $data['block_geofence_latitude'];
                            $punchIn['geofence_longitude'] = $data['block_geofence_longitude'];
                            $punchIn['geofence_range'] = $data['block_geofence_range'];
                        }
                    }else{

                        $data = mysqli_fetch_array($userQry);

                        $punchIn['geofence_latitude'] = $data['block_geofence_latitude'];
                        $punchIn['geofence_longitude'] = $data['block_geofence_longitude'];
                        $punchIn['geofence_range'] = $data['block_geofence_range'];
                    }

                    array_push($response['pending_attendance'],$punchIn);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found";
                $response["status"] = "201";
                echo json_encode($response);
            } 
                    
        }else if($_POST['approvePendingAttendance']=="approvePendingAttendance" && $attendance_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_status',"1");
            $m->set_data('attendance_status_change_by',$user_id);

            $a = array(
                'attendance_status'=>$m->get_data('attendance_status'),
                'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
            );

            $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id' AND user_id = '$attendance_user_id'");

            if ($punchInQry == true) {

                $title = "Your Attendance is Approved";
                $description = "For Date: ".date('d F Y',strtotime($attendance_date_start))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","users_master.user_id = '$attendance_user_id'");

                $response["message"] = "Attendance Approved";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['declinePendingAttendance']=="declinePendingAttendance" && $attendance_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_status',"2");
            $m->set_data('attendance_status_change_by',$user_id);
            $m->set_data('attendance_declined_reason',$attendance_declined_reason);

            $a = array(
                'attendance_status'=>$m->get_data('attendance_status'),
                'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
                'attendance_declined_reason'=>$m->get_data('attendance_declined_reason'),
            );

            $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id' AND user_id = '$attendance_user_id'");

            if ($punchInQry == true) {

                $title = "Your Attendance is Rejected";
                $description = "For Date: ".date('d F Y',strtotime($attendance_date_start))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                $response["message"] = "Attendance Rejected";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['attendancePunchOut']=="attendancePunchOut" && $attendance_id!='' && $user_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $punchOUTTime = date('H:i:s');
            $punchOUTDate = date('Y-m-d');

            $qq = $d->selectRow("*","attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id'");
            $dataQ = mysqli_fetch_array($qq);

            if (mysqli_num_rows($qq)>0) {

                $punch_in_time  = $dataQ['punch_in_time'];
                $attendance_date_start  = $dataQ['attendance_date_start'];

                if ($dataQ['attendance_date_end'] != "0000-00-00" && $dataQ['punch_out_time'] != "00:00:00") {
                    $response["message"] = "Already Punched Out";
                    $response["status"] = "202";
                    echo json_encode($response);
                    exit();
                }
            }

            $datePunchIN = $attendance_date_start ." ".$punch_in_time;
            $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
            $datePunchOUT = $punchOUTDate ." ".$punchOUTTime;

            if ($datePunchOUT < $datePunchIN) {
                $response["message"] = "Immediate punch out is restricted, please punch out after 1 minute.";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
            }


            // Early Punch OUT

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shift_start_time = $shiftData['shift_start_time'];
            $shift_end_time = $shiftData['shift_end_time'];
            $late_time_start = $shiftData['late_time_start'];
            $early_out_time = $shiftData['early_out_time'];
            $half_day_time_start = $shiftData['half_day_time_start'];
            $halfday_before_time = $shiftData['halfday_before_time'];
            $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
            $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
            $shift_type = $shiftData['shift_type'];
            $maximum_in_out = $shiftData['maximum_in_out'];

            $min = date('i',strtotime($early_out_time));
            $defaultOutTime = $punchOUTDate." ".$shift_end_time;
            $strMin = "-".$min." minutes";
            $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

            if ($datePunchOUT >= $defaultOutTime) {
                $early_out = "0";
            }else{
                $early_out = "1";              
            }

            // Early Punch Out END

            // Total Working Hour Count

            $parts = explode(':', $perdayHours);
            $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60)."\n";
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $totalHours = getTotalHours($attendance_date_start,$punchOUTDate,
                            $punch_in_time,$punchOUTTime);

            $times[] = $totalHours;


            $time = explode(':', $totalHours.":00");
            $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60)."\n";

            // END

            $punch_out_image = "";

            if ($_FILES["punch_out_image"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["punch_out_image"]["tmp_name"];
                $extId = pathinfo($_FILES['punch_out_image']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["punch_out_image"]["name"]);
                    $punch_out_image = "punch_out_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["punch_out_image"]["tmp_name"], "../img/attendance/" . $punch_out_image);
                } else {
            
                    $response["message"] = "Invalid Document. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }


            $m->set_data('attendance_date_end',$punchOUTDate);
            $m->set_data('punch_out_time',$punchOUTTime);
            $m->set_data('punch_out_latitude',$punch_out_latitude);
            $m->set_data('punch_out_longitude',$punch_out_longitude);
            $m->set_data('punch_out_image',$punch_out_image);
            $m->set_data('punch_out_reason',$punch_out_reason);
            $m->set_data('early_out',$early_out);
            $m->set_data('punch_out_in_range',$punch_out_in_range);
            $m->set_data('punch_out_meter',$punch_out_meter);
            $m->set_data('punch_out_km',$punch_out_km);
            $m->set_data('early_out_reason',$early_out_reason);

            $a = array(
                'attendance_date_end'=>$m->get_data('attendance_date_end'),
                'punch_out_time'=>$m->get_data('punch_out_time'),
                'punch_out_latitude'=>$m->get_data('punch_out_latitude'),
                'punch_out_longitude'=>$m->get_data('punch_out_longitude'),
                'punch_out_image'=>$m->get_data('punch_out_image'),
                'punch_out_reason'=>$m->get_data('punch_out_reason'),
                'early_out'=>$m->get_data('early_out'),
                'punch_out_in_range'=>$m->get_data('punch_out_in_range'),
                'punch_out_meter'=>$m->get_data('punch_out_meter'),
                'punch_out_km'=>$m->get_data('punch_out_km'),
                'early_out_reason'=>$m->get_data('early_out_reason'),
            );

            $punchOutQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

            if ($punchOutQry == true) {

                if ($punch_out_in_range == "1") {
                    $m->set_data('attendance_status',"0");

                    $a = array(
                        'attendance_status'=>$m->get_data('attendance_status'),
                    );

                    $qqry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

                    $title = "Attendance Approval Request";
                    $description = "Requested by, ".$user_name;

                    $access_type = $pending_attendance_access;

                    include 'check_access_data_user.php';

                    // To Employee App
                    if (count($userIDArray) > 0) {

                        $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                        $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                        $dataAry = array(
                            "block_id"=>$block_id,
                            "floor_id"=>$floor_id,
                            "user_id"=>$user_id,
                        );

                        $dataJson = json_encode($dataAry);

                        $nResident->noti("pending_attendance","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                        $nResident->noti_ios("pending_attendance","",$society_id,$fcmArrayIosPA,$title,$description,$dataJson);

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"pending_attendance","attendance_tracker.png",$dataJson,"users_master.user_id IN ('$userFcmIds')");
                    }
                }

                // Check Half Day

                $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type","leave_master","user_id = '$user_id' AND leave_start_date = '$punchOUTDate'");

                $hasLeave = false;

                if (mysqli_num_rows($qryLeave) > 0) {

                    $hasLeave = true;

                    $leaveData = mysqli_fetch_array($qryLeave);

                    $leave_id = $leaveData['leave_id'];
                    $leave_type_id = $leaveData['leave_type_id'];
                    $paid_unpaid = $leaveData['paid_unpaid'];
                    $leave_reason = $leaveData['leave_reason'];
                    $leave_day_type = $leaveData['leave_day_type'];
                }

                $leaveValue = false;

                if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                    $leaveValue = true;
                } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $punchOUTTime) {
                    $leaveValue = true;
                }

                $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);

                if ($total_minutes >= $minimum_hours_for_full_day_min) {
                    $noLeave = true;
                }else{
                    $noLeave = false;
                }

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
                $data_notification = mysqli_fetch_array($qsm);
                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];
                $tabPosition = "0"; // 1 for all leaves, 0 for my leaves

                if ($noLeave == false) {

                    if ($leaveValue == true) {

                        $leave_total_days = 1;

                        // check half day and full day

                        if ($total_minutes < $maximum_halfday_hours_min) {
                            $leaveType = "1";
                        }else{
                            $leaveType = "0";
                        }

                        if ($hasLeave == true) {

                            // Change already applied leave

                            $m->set_data('leave_type_id',$leave_type_id);
                            $m->set_data('paid_unpaid',$paid_unpaid);
                            $m->set_data('leave_day_type',$leaveType);

                            $a = array(
                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                'leave_day_type'=>$m->get_data('leave_day_type'),
                            );

                            $leaveQry = $d->update("leave_master",$a,"user_id = '$user_id' AND leave_id = '$leave_id'");
                            
                        }else{

                            // Auto Leave

                            $m->set_data('leave_type_id',"0");
                            $m->set_data('society_id',$society_id);
                            $m->set_data('paid_unpaid',"1");
                            $m->set_data('unit_id',$unit_id);
                            $m->set_data('user_id',$user_id);
                            $m->set_data('leave_reason',"Auto Leave");
                            $m->set_data('leave_start_date',$attendance_date_start);
                            $m->set_data('leave_end_date',$punchOUTDate);
                            $m->set_data('leave_total_days',$leave_total_days);
                            $m->set_data('leave_day_type',$leaveType);
                            $m->set_data('leave_status',"1");
                            $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                            $a = array(
                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                'society_id' =>$m->get_data('society_id'),
                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                'unit_id'=>$m->get_data('unit_id'),
                                'user_id'=>$m->get_data('user_id'),
                                'leave_reason'=>$m->get_data('leave_reason'),
                                'leave_start_date'=>$m->get_data('leave_start_date'),
                                'leave_end_date'=>$m->get_data('leave_end_date'),
                                'leave_total_days'=>$m->get_data('leave_total_days'),
                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                'leave_status'=>$m->get_data('leave_status'),
                                'leave_created_date'=>$m->get_data('leave_created_date'),
                            );

                            $leaveQry = $d->insert("leave_master",$a);

                            if ($leaveType == 0) {
                                $title = "Auto Leave Applied";
                            }else{
                                $title = "Auto Leave Applied";
                            }


                            $description = "For Date : ".$attendance_date_start;

                            if ($device == 'android') {
                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            }

                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                        }

                        $m->set_data('auto_leave',"1");

                        $a1 = array(
                            'auto_leave'=>$m->get_data('auto_leave'),
                        );

                        $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                    }
                }else if($noLeave == true && $hasLeave == true){

                    // Remove leave if present and full time

                    $leaveQry = $d->delete("leave_master","user_id = '$user_id' AND leave_id = '$leave_id'");

                    $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                    $title = "Applied Leave Cancelled";
                    $description = "For Date : ".$attendance_date_start;

                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                }

                // END checking half day

                // Breaks End if any start

                $attHisQry = $d->selectRow("attendance_break_history_id,break_end_date,break_out_time","attendance_break_history_master","attendance_id = '$attendance_id' AND break_end_date = '0000-00-00' AND break_out_time = '00:00:00'");

                if (mysqli_num_rows($attHisQry) > 0) {
                    while($attData = mysqli_fetch_array($attHisQry)){
                        $attendance_break_history_id = $attData['attendance_break_history_id'];
                        $break_end_date = date('Y-m-d');
                        $break_out_time = date('H:i:s');

                        $m->set_data('break_end_date',$break_end_date);
                        $m->set_data('break_out_time',$break_out_time);

                        $a = array(
                            'break_end_date'=>$m->get_data('break_end_date'),
                            'break_out_time'=>$m->get_data('break_out_time'),
                        );

                        $qry = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id' AND attendance_id = '$attendance_id'");
                    }
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['punchOutMissingRequest']=="punchOutMissingRequest" && $attendance_id!='' && $user_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $qq = $d->count_data_direct("attendance_punch_out_missing_id","attendance_punch_out_missing_request","user_id = '$user_id' AND attendance_id = '$attendance_id'");

            if ($qq > 0) {
                $response["message"] = "Punch Out Missing Request Already Sent";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }  


            if ($attDate == "0000-00-00" || $attDate == "00-00-0000") {
                $response["message"] = "Invalid Date, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }  

            /*if ($attDate == null || $attDate == '' || $attDate == " ") {
                
                $attQry = $d->selectRow("*","attendance_master","attendance_id = '$attendance_id'");
                $attData = mysqli_fetch_array($attQry);

                $sq = $d->selectRow("users_master.shift_time_id as sti,shift_type","users_master,shift_timing_master","users_master.user_id = '$user_id' AND shift_timing_master.shift_time_id = users_master.shift_time_id");

                $sdata = mysqli_fetch_array($sq);

                if ($sdata['sti'] != null && $sdata['sti'] > 0) {
                    if ($sdata['shift_type'] == "Night") {
                        $attDate = date('Y-m-d', strtotime($attData['attendance_date_start'] . ' +1 day'));
                    }else{
                        $attDate = $attData['attendance_date_start'];
                    }
                } 

            }*/


            $m->set_data('attendance_id',$attendance_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('attendance_date',$attDate);
            $m->set_data('attendance_time',$attendance_time);
            $m->set_data('punch_out_missing_reason',$punch_out_missing_reason);

            $a = array(
                'attendance_id'=>$m->get_data('attendance_id'),
                'user_id'=>$m->get_data('user_id'),
                'attendance_date'=>$m->get_data('attendance_date'),
                'attendance_time'=>$m->get_data('attendance_time'),
                'punch_out_missing_reason'=>$m->get_data('punch_out_missing_reason'),
            );

            $punchOutQry = $d->insert("attendance_punch_out_missing_request",$a);

            if ($punchOutQry == true) {

                $title = "Punch Out Missing Request";
                $description = "Requested by, ".$user_name."\nDate: ".date('d-m-Y',strtotime($attDate))." Time: ".date('h:i A',strtotime($attendance_time));

                // Notification by Access

                $access_type = $punch_out_missing_req_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmAccessAry=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmAccessAryiOS=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $nResident->noti("punch_out_missing_request","",$society_id,$fcmAccessAry,$title,$description,$tabPosition);
                    $nResident->noti_ios("punch_out_missing_request","",$society_id,$fcmAccessAryiOS,$title,$description,$tabPosition);

                    $d->insertUserNotification($society_id,$title,$description,"punch_out_missing_request","attendance_tracker.png","users_master.user_id IN ('$userFcmIds')");
                }


                // To Admin

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("18",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("18",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"punchOutMissingRequest");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"punchOutMissingRequest");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"punchOutMissingRequest",
                  'admin_click_action '=>"punchOutMissingRequest",
                  'notification_logo'=>'attendance_tracker.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Request Sent";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getPunchOutMissingRequests']=="getPunchOutMissingRequests" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $punch_out_missing_req_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND attendance_punch_out_missing_request.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND attendance_punch_out_missing_request.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $punchInReqQry = $d->select("attendance_master,attendance_punch_out_missing_request,users_master,floors_master,block_master","
                attendance_master.attendance_id = attendance_punch_out_missing_request.attendance_id 
                AND attendance_punch_out_missing_request.attendance_punch_out_missing_status = '0'  
                AND users_master.user_id = attendance_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id $appendQuery","ORDER BY attendance_punch_out_missing_request.attendance_punch_out_missing_id DESC $LIMIT_DATA");

            if(mysqli_num_rows($punchInReqQry)>0){

                $response["punch_out_missing_requests"] = array();        
                
                while($data=mysqli_fetch_array($punchInReqQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $punchIn = array();

                    $punchIn['user_id'] = $data['user_id'];
                    $punchIn['user_full_name'] = $data['user_full_name'];
                    $punchIn['branch_name'] = $data['block_name'];
                    $punchIn['department_name'] = $data['floor_name'];
                    $punchIn['user_designation'] = $data['user_designation'];
                    $punchIn['attendance_id'] = $data['attendance_id'];
                    $punchIn['attendance_punch_out_missing_id'] = $data['attendance_punch_out_missing_id'];
                    $punchIn['block_id'] = $data['block_id'];
                    $punchIn['floor_id'] = $data['floor_id'];
                    $punchIn['unit_id'] = $data['unit_id'];
                    $punchIn['society_id'] = $data['society_id'];

                    if ($data['user_profile_pic'] != '') {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $punchIn['attendance_date_start'] = $data['attendance_date_start'];
                    $punchIn['attendance_date_start_view'] = date('D, d F Y',strtotime($data['attendance_date_start']));

                    $punchIn['attendance_date_end'] = $data['attendance_date'];
                    $punchIn['attendance_date_end_view'] = date('D, d F Y',strtotime($data['attendance_date']));
                    $punchIn['punch_in_time'] = $data['punch_in_time'];
                    $punchIn['punch_in_time_view'] = date('h:i A',strtotime($data['punch_in_time']));
                    $punchIn['punch_out_time'] = $data['attendance_time'];
                    $punchIn['punch_out_time_view'] = date('h:i A',strtotime($data['attendance_time']));
                    $punchIn['punch_out_missing_reason'] = $data['punch_out_missing_reason'];

                    array_push($response['punch_out_missing_requests'],$punchIn);
                }
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found";
                $response["status"] = "201";
                echo json_encode($response);
            }             
        }else if($_POST['approvePunchOutRequest']=="approvePunchOutRequest" && $attendance_id!='' && $attendance_punch_out_missing_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_punch_out_missing_status',"1");
            $m->set_data('attendance_punch_out_missing_status_changed_by',$user_id);
            $m->set_data('attendance_date',$attendance_date);
            $m->set_data('attendance_time',$attendance_time);

            $a = array(
                'attendance_punch_out_missing_status'=>$m->get_data('attendance_punch_out_missing_status'),
                'attendance_punch_out_missing_status_changed_by'=>$m->get_data('attendance_punch_out_missing_status_changed_by'),
                'attendance_date'=>$m->get_data('attendance_date'),
                'attendance_time'=>$m->get_data('attendance_time'),
            );

            $q1 = $d->update("attendance_punch_out_missing_request",$a,"attendance_punch_out_missing_id = '$attendance_punch_out_missing_id'");

            if ($q1 == true) {

                $m->set_data('attendance_status',"1");
                $m->set_data('attendance_status_change_by',$user_id);
                $m->set_data('attendance_date_end',$attendance_date);
                $m->set_data('punch_out_time',$attendance_time);

                $a1 = array(
                    'attendance_status'=>$m->get_data('attendance_status'),
                    'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
                    'attendance_date_end'=>$m->get_data('attendance_date_end'),
                    'punch_out_time'=>$m->get_data('punch_out_time'),
                );

                $q2 = $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");

                if ($q2 == true) {

                    $title = "Punch Out Missing Request Approved";
                    $description = "For Date: ".date('d-m-Y',strtotime($attendance_date))." Time: ".date('h:i A',strtotime($attendance_time))."\nBy, ".$user_name;

                    $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");

                    $data_notification = mysqli_fetch_array($qsm);

                    $user_token = $data_notification['user_token'];
                    $device = $data_notification['device'];

                    $tabPosition = "1";

                    if ($device == 'android') {
                        $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                    $response["message"] = "Request Approved";
                    $response["status"] = "200";
                    echo json_encode($response);
                }else{
                    $response["message"] = "Something went wrong, please try again!";
                    $response["status"] = "201";
                    echo json_encode($response);
                } 
            }
                   
        }else if($_POST['declinePunchOutRequest']=="declinePunchOutRequest" && $attendance_id!=''  && $attendance_punch_out_missing_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_punch_out_missing_status',"2");
            $m->set_data('attendance_punch_out_missing_status_changed_by',$user_id);
            $m->set_data('punch_out_missing_reject_reason',$punch_out_missing_reject_reason);

            $a = array(
                'attendance_punch_out_missing_status'=>$m->get_data('attendance_punch_out_missing_status'),
                'attendance_punch_out_missing_status_changed_by'=>$m->get_data('attendance_punch_out_missing_status_changed_by'),
                'punch_out_missing_reject_reason'=>$m->get_data('punch_out_missing_reject_reason'),
            );

            $qry = $d->update("attendance_punch_out_missing_request",$a,"attendance_id = '$attendance_id' AND attendance_punch_out_missing_id = '$attendance_punch_out_missing_id'");

            if ($qry == true) {

                $title = "Punch Out Missing Request Rejected";
                $description = "For Date: ".date('d-m-Y',strtotime($attendance_date))." Time: ".date('h:i A',strtotime($attendance_time))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                $response["message"] = "Request Rejected";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['pendingAttendanceReminder']=="pendingAttendanceReminder" && $attendance_id!='' && $user_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $title = "Pending Attendance Approval Reminder";
            $description = "Requested by, ".$user_name;

            $access_type = $pending_attendance_access;

            include 'check_access_data_user.php';

            if (count($userIDArray) > 0) {

                $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                $dataAry = array(
                    "block_id"=>$block_id,
                    "floor_id"=>$floor_id,
                    "user_id"=>$user_id,
                );

                $dataJson = json_encode($dataAry);

                $nResident->noti("pending_attendance","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                $nResident->noti_ios("pending_attendance","",$society_id,$fcmArrayIosPA,$title,$description,$dataJson);

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"pending_attendance","attendance_tracker.png",$dataJson,"users_master.user_id IN ('$userFcmIds')");
            }

            // To Admin Panel

            $block_id=$d->getBlockid($user_id);
            $fcmArray=$d->selectAdminBlockwise("17",$block_id,"android");
            $fcmArrayIos=$d->selectAdminBlockwise("17",$block_id,"ios");
      
            $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"pendingAttendance");
            $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"pendingAttendance");

            $notiAry = array(
              'society_id'=>$society_id,
              'notification_tittle'=>$title,
              'notification_description'=>$description,
              'notifiaction_date'=>date('Y-m-d H:i'),
              'notification_action'=>"pendingAttendance",
              'admin_click_action '=>"pendingAttendance",
              'notification_logo'=>'attendance_tracker.png',
            );
                        
            $qry = $d->insert("admin_notification",$notiAry);

            if ($qry == true) {
                $response["message"] = "Request Sent";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            } 
        }else if($_POST['breakIn']=="breakIn" && $attendance_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_id',$attendance_id);
            $m->set_data('attendance_type_id',$attendance_type_id);
            $m->set_data('break_start_date',date('Y-m-d'));
            $m->set_data('break_in_time',date('H:i:s'));
            $m->set_data('break_in_latitude',$break_in_latitude);
            $m->set_data('break_in_longitude',$break_in_longitude);

            $a = array(
                'attendance_id'=>$m->get_data('attendance_id'),
                'attendance_type_id' =>$m->get_data('attendance_type_id'),
                'break_start_date' =>$m->get_data('break_start_date'),
                'break_in_time'=>$m->get_data('break_in_time'),
                'break_in_latitude'=>$m->get_data('break_in_latitude'),
                'break_in_longitude'=>$m->get_data('break_in_longitude')
            );

            $punchInQry = $d->insert("attendance_break_history_master",$a);

            $attendance_break_history_id = $con->insert_id;

            if ($punchInQry == true) {
                $response["message"] = "Success";
                $response["attendance_break_history_id"] = $attendance_break_history_id."";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['breakOut']=="breakOut" && $attendance_break_history_id!='' && $user_id!='' && filter_var($attendance_break_history_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('break_end_date',date('Y-m-d'));
            $m->set_data('break_out_time',date('H:i:s'));
            $m->set_data('break_out_latitude',$break_out_latitude);
            $m->set_data('break_out_longitude',$break_out_longitude);

            $a = array(
                'break_end_date'=>$m->get_data('break_end_date'),
                'break_out_time'=>$m->get_data('break_out_time'),
                'break_out_latitude'=>$m->get_data('break_out_latitude'),
                'break_out_longitude'=>$m->get_data('break_out_longitude'),
            );

            $breakOut = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id'");

            if ($breakOut == true) {
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['checkRadius']=="checkRadius" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            if ($block_id == null || $block_id == '' || $block_id == ' ' || $block_id == '0') {
                
                $blockIdQry = $d->selectRow("block_id","users_master","user_id = '$user_id'");
                
                if (mysqli_num_rows($blockIdQry) > 0) {
                    $blockData = mysqli_fetch_array($blockIdQry);
                    $block_id = $blockData['block_id'];
                }
            }

            $isRadiusON = false;

            $userQry = $d->select("users_master","user_id = '$user_id' AND society_id = '$society_id' AND user_geofence_latitude != '' AND user_geofence_longitude != '' AND user_geofence_range != ''");

            if (mysqli_num_rows($userQry) == 0) {

                $blockQry = $d->select("block_master","society_id = '$society_id' AND block_geofence_latitude != '' AND block_geofence_longitude != '' AND block_id = '$block_id'");

                if (mysqli_num_rows($blockQry) > 0) {

                    $isRadiusON = true;

                    $data = mysqli_fetch_array($blockQry);

                    $geofence_latitude = $data['block_geofence_latitude'];
                    $geofence_longitude = $data['block_geofence_longitude'];
                    $geofence_range = $data['block_geofence_range'];
                }else{
                    $isRadiusON = false;
                }
            }else{

                $isRadiusON = true;

                $data = mysqli_fetch_array($userQry);

                $geofence_latitude = $data['user_geofence_latitude'];
                $geofence_longitude = $data['user_geofence_longitude'];
                $geofence_range = $data['user_geofence_range'];
            }

            $tDate = date('Y-m-d');

            $wfhQry = $d->selectRow("wfh_id,user_id,wfh_start_date,wfh_latitude,wfh_longitude,wfh_attendance_range","wfh_master","society_id ='$society_id' AND user_id='$user_id' AND wfh_status = '1' AND wfh_start_date = '$tDate'");
            $wfhData = mysqli_fetch_array($wfhQry);

            if (mysqli_num_rows($wfhQry) > 0) {

                $geofence_latitude = $wfhData['wfh_latitude'];
                $geofence_longitude = $wfhData['wfh_longitude'];
                $geofence_range = $wfhData['wfh_attendance_range'];

                if ($wfhData['wfh_status'] == '1') {
                    $isRadiusON = true;
                }
            }


            if ($isRadiusON) {

                $radiusInMeeter=  $d->haversineGreatCircleDistance($user_latitude,$user_longitude,$geofence_latitude, $geofence_longitude);

                $totalKm= number_format($radiusInMeeter,2,'.','');

                $response['radiusInMeter'] = $totalKm;

                $dist = ($totalKm/1000);

                $KM = number_format($dist,2,'.','');

                $response['totalKm'] = $KM." KM";

                if ($user_latitude=="0.0" || $user_latitude=="") {
                    $response['take_punch_in_reason'] = true;
                    $response["message"] = "Invalid Location";
                    $response["status"] = "201";
                    echo json_encode($response);
                }else if($totalKm <= $geofence_range){
                    $response['take_punch_in_reason'] = false;
                    $response["message"] = "You are in range";
                    $response["status"] = "200";
                    echo json_encode($response);
                }else{
                    $response['take_punch_in_reason'] = true;
                    $response["message"] = "You are out of range";
                    $response["status"] = "200";
                    echo json_encode($response);
                }
            }else{
                $response['take_punch_in_reason'] = false;
                $response["message"] = "No Radius Checking Required";
                $response["status"] = "200";
                echo json_encode($response);
            }             
        }else if($_POST['getWeeklyAttendanceHistory']=="getWeeklyAttendanceHistory" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shift_time_id = $sdata['shift_time_id'];
            }

            if ($weekStartDate !=null || $weekStartDate != '') {
                $currdt= strtotime($weekStartDate);
                $nextmonth=strtotime($weekStartDate."+1 month");
                $i=0;
                $flag=true;
            }else{
                $textdt=date("01-m-Y");
                $currdt= strtotime($textdt);
                $nextmonth=strtotime($textdt."+1 month");
                $i=0;
                $flag=true;
            }

            $response["weekly_histrory"] = array();
            $response["week_position"] = "0";

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shiftType = $shiftData['shift_type'];

            $parts = explode(':', $perdayHours);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);

            $weekDates = weeks_in_month(date('m'),date('Y'));

            // Week
            for ($x = 0; $x <= (count($startarr)-1); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";
                $weekArray["total_spend_time"] = "0";
                $weekArray["total_spend_time_name"] = "No Data";

                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);

                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 
                
                $holidayAry = array();

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $days = array();
                    $days['day'] = $date->format('l');
                    $days['date'] = $dayDate = $date->format('Y-m-d');
                    $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate)) ;

                    if ($dayDate == date('Y-m-d')) {
                        $response["week_position"] = $x."";
                    }
                    
                    $holidayQry = $d->selectRow("holiday_start_date","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' ");

                    $x1 = $x+1;
                    // holiday off
                    if (mysqli_num_rows($holidayQry)>0) {
                        array_push($holidayAry,$dayDate);
                        $minusHoliday += $perdayHours;
                    } else {
                        // full day off
                        if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                            array_push($weekOffAry,$dayDate);
                            $minusWeekOffHR += $perdayHours;
                        }
                        // alternate week day off
                        if ($shiftData['has_altenate_week_off'] == "1") {  
                            if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                in_array($x1,$alternateWeekOff)) {
                                array_push($weekOffAry,$dayDate);
                                $minusAlternateWeekOffDays += $perdayHours;
                            }
                        }
                    }
                    array_push($daysArray['days'],$days);
                }

                $totalWeekDay = count($daysArray['days']);
                
                $totalWeekHour = $perdayHours * $totalWeekDay;
                $totalWeekHour1 = ($totalWeekHour - $minusWeekOffHR) - $minusAlternateWeekOffDays;
                $totalWeekHour = $totalWeekHour1 - $minusHoliday;
                $weekArray['total_hours'] = $totalWeekHour."";

                list($hours1, $wrongMinutes) = explode('.', $totalWeekHour);
                $minutes1 = ($wrongMinutes < 10 ? $wrongMinutes * 10 : $wrongMinutes) * 0.6;

                if ($hours1 > 0 && $minutes1 > 0) {
                    $weekArray['total_hours_view'] = $hours1 . ' hr ' . $minutes1.' min';
                }else if($hours1 > 0 && $minutes1 <= 0){
                    $weekArray['total_hours_view'] = $hours1.' hr';
                } else if($hours1 <= 0 && $minutes1 > 0){
                    $weekArray['total_hours_view'] = $minutes1.' min';
                } else{
                    $weekArray['total_hours_view'] = 'No Data';
                }
                
                $weekArray['week'] = date('d',strtotime($sDate))."-".date('d',strtotime($eDate))." ".date('F',strtotime($sDate))." ".date('Y',strtotime($sDate));

                // Week History

                $tdate = date('Y-m-d');
                
                $attendanceQry = $d->selectRow("am.*,apomr.attendance_id as attid, apomr.attendance_punch_out_missing_id, apomr.punch_out_missing_reject_reason,apomr.attendance_punch_out_missing_status","attendance_master AS am LEFT JOIN attendance_punch_out_missing_request AS apomr ON apomr.attendance_id = am.attendance_id","(am.society_id='$society_id' 
                    AND am.attendance_date_start != '$tdate' 
                    AND am.user_id = '$user_id' 
                    AND am.attendance_date_start BETWEEN '$sDate' AND '$eDate') 
                    OR (am.society_id='$society_id' 
                    AND am.user_id = '$user_id' 
                    AND am.attendance_date_start BETWEEN '$sDate' AND '$eDate')","ORDER BY am.attendance_date_start ASC");

                $weekArray["history"] = array();

                if(mysqli_num_rows($attendanceQry)>0){

                    $times = array();

                    while($data=mysqli_fetch_array($attendanceQry)) {

                        $attendanceHistory = array(); 

                        $attendanceHistory["attendance_id"] = $data['attendance_id'];
                        $attendanceHistory["day_name"] = date('l', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_start"] = date('d F Y', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_end"] = date('d F Y', strtotime($data['attendance_date_end']));

                        if ($data['attendance_punch_out_missing_id'] != null) {
                       
                            $attendanceHistory['attendance_punch_out_missing_id'] = $data['attendance_punch_out_missing_id'];
                            $attendanceHistory['punch_out_missing_reject_reason'] = $data['punch_out_missing_reject_reason'];

                            $attendance_punch_out_missing_status = $data['attendance_punch_out_missing_status'];

                            $attendanceHistory['attendance_punch_out_missing_status'] = $attendance_punch_out_missing_status;
                            
                            if ($attendance_punch_out_missing_status == "0") {
                                $attendanceHistory['attendance_punch_out_missing_status_view'] = "Pending";
                            }else if ($attendance_punch_out_missing_status == "1") {
                                $attendanceHistory['attendance_punch_out_missing_status_view'] = "Approved";
                            }else if ($attendance_punch_out_missing_status == "2") {
                                $attendanceHistory['attendance_punch_out_missing_status_view'] = "Rejected";
                            }
                        }

                        $attendanceDateStart = $data['attendance_date_start'];
                        $attendanceDateEnd = $data['attendance_date_end'];
                        
                        $punchInTime = $data['punch_in_time'];
                        $punchOutTime = $data['punch_out_time'];

                        if (in_array($attendanceDateStart,$weekOffAry)) {
                            $attendanceHistory['extra_day'] = true;
                        }

                        if (in_array($attendanceDateStart,$holidayAry)) {
                            $attendanceHistory['extra_day'] = true;
                        }
                        
                        $attendanceHistory["punch_in_time"] = date("h:i A",strtotime($punchInTime));
                        $attendanceHistory["punch_out_time"] = date("h:i A",strtotime($punchOutTime));

                        if ($attendanceDateEnd == "0000-00-00") {
                            $attendanceHistory["attendance_date_end"] = "";
                            $attendanceHistory["punch_out_time"] = "";
                            $attendanceDateEnd = date('Y-m-d');
                            $punchOutTime = date('H:i:s');
                        }

                        if ($data['punch_out_time'] != "00:00:00" && $data['attendance_date_end'] != "0000-00-00") {
                        
                            $totalHours = getTotalHours($attendanceDateStart,$attendanceDateEnd,$punchInTime,$punchOutTime);
                            
                            $totalHoursName = getTotalHoursWithNames($attendanceDateStart,$attendanceDateEnd,$punchInTime,$punchOutTime);

                            $times[] = $totalHours;

                            $attendanceHistory["total_hours"] = $totalHoursName;
                            $attendanceHistory["is_punch_out_missing"] = false;
                            $attendanceHistory["punch_out_missing_message"] = "";

                            $weekArray['total_spend_time'] = getTotalWeekHours($times);
                            $weekArray['total_spend_time_name'] = getTotalWeekHoursName($times);
                        }else{
                            if ($data['attendance_date_start'] == $tdate) {
                                $attendanceHistory["is_punch_out_missing"] = false;
                                $attendanceHistory["total_hours"] = "";
                                $attendanceHistory["punch_out_missing_message"] = "";
                            }else{
                                $attendanceHistory["is_punch_out_missing"] = true;
                                $attendanceHistory["total_hours"] = "";
                                $attendanceHistory["punch_out_missing_message"] = "Punch Out Missing";

                                if ($shiftType == "Night") {

                                    $previousDateView = date('dS F Y',strtotime("+1 days",strtotime($attendanceDateStart)));
                                    $previousDate = date('Y-m-d',strtotime("+1 days",strtotime($attendanceDateStart)));
                                    $datesArray = array(
                                        "date_one"=>date('Y-m-d',strtotime($attendanceDateStart)),
                                        "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                        "date_two"=>$previousDate,
                                        "date_two_view"=>$previousDateView
                                    );
                                }else{
                                    $datesArray = array(
                                        "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                        "date_one"=>date('Y-m-d',strtotime($attendanceDateStart))
                                    );
                                }

                                $attendanceHistory['punch_missing_date_ary'] = $datesArray;
                            }
                        }

                        if ($data['attendance_status'] == "0") {
                            $attendanceHistory["attendnace_pending"] = true;
                            $attendanceHistory["attendance_pending_message"] = "Attendance Pending";
                        }else{
                            $attendanceHistory["attendnace_pending"] = false;
                            $attendanceHistory["attendance_pending_message"] = "";
                        }

                        array_push($weekArray["history"], $attendanceHistory);
                    }
                }

                array_push($response['weekly_histrory'],$weekArray);
            } 
            
            echo json_encode($response);
        
        }else if($_POST['getMonthlyAttendanceHistory']=="getMonthlyAttendanceHistory" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            if ($shift_time_id == '') {

                $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

                if (mysqli_num_rows($sq)>0) {
                    $sdata = mysqli_fetch_array($sq);
                    $shift_time_id = $sdata['shift_time_id'];
                }
            }

            $response['monthly_history'] = array();
            //$response['week'] = array();

            $datesArrayOfMonth = range_date($month_start_date,$month_end_date);

            $response['month'] = date('F Y',strtotime($month_start_date));

            $holidayAry = array();
            $weekAry = array();
            $response['week'] = array();

            $currdt= strtotime($month_start_date);
            $nextmonth=strtotime($month_start_date."+1 month");
            $i=0;
            $flag=true;

            $totalMonthHours = 0;

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shiftType = $shiftData['shift_type'];
            $shift_start_time = $shiftData['shift_start_time'];
            $shift_end_time = $shiftData['shift_end_time'];
            $lunch_break_start_time = $shiftData['lunch_break_start_time'];
            $lunch_break_end_time = $shiftData['lunch_break_end_time'];
            $tea_break_start_time = $shiftData['tea_break_start_time'];
            $tea_break_end_time = $shiftData['tea_break_end_time'];
            $half_day_time_start = $shiftData['half_day_time_start'];
            $late_time_start = $shiftData['late_time_start'];
            $early_out_time = $shiftData['early_out_time'];

            $parts = explode(':', $perdayHours);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);


            // Week
        
            for ($x = 0; $x < count($startarr); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";


                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);


                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $dayN= $date->format('l');
                    $dayDate = $date->format('Y-m-d');

                        // code...
                        $days = array();
                        $hDates = array();
                        $days['day'] = $dayN;
                        $days['date'] = $dayDate;
                        $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate)) ;

                        $holidayQry = $d->selectRow("holiday_start_date,holiday_name","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' AND holiday_status = '0' ");

                        $holidayData = mysqli_fetch_array($holidayQry);

                        $x1 = $x+1;
                        // holiday off
                        if (mysqli_num_rows($holidayQry)>0) {
                            array_push($holidayAry,$dayDate);
                            $minusHoliday += $perdayHours;
                        } else {
                            // full day off
                            if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                                array_push($weekAry,$dayDate);
                                $minusWeekOffHR += $perdayHours;
                            }

                            // alternate week day off
                            if ($shiftData['has_altenate_week_off'] == "1") {  
                                if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                    in_array($x1,$alternateWeekOff)) {
                                    array_push($weekAry,$dayDate);
                                    $minusAlternateWeekOffDays += $perdayHours;
                                }
                            }
                        }
                        array_push($daysArray['days'],$days);
                }

                $totalWeekDay = count($daysArray['days']);
                
                $totalWeekHour = $perdayHours * $totalWeekDay;
                $totalWeekHour1 = ($totalWeekHour - $minusWeekOffHR) - $minusAlternateWeekOffDays;
                $totalWeekHour = $totalWeekHour1 - $minusHoliday;
                $weekArray['total_hours'] = $totalWeekHour."";

                $totalMonthHours += $totalWeekHour."";
                $weekArray['totalMonthHours'] = $totalMonthHours."";

                //array_push($response['week'],$weekArray);
            }

            array_push($response['week'],$weekAry);

            list($hours1, $wrongMinutes) = explode('.', $totalMonthHours);
                $minutes1 = ($wrongMinutes < 10 ? $wrongMinutes * 10 : $wrongMinutes) * 0.6;

            if ($hours1 > 0 && $minutes1 > 0) {
                $response['total_month_hours_view'] = $hours1 . ' hr ' . $minutes1.' min';
            }else if($hours1 > 0 && $minutes1 <= 0){
                $response['total_month_hours_view'] = $hours1.' hr';
            } else if($hours1 <= 0 && $minutes1 > 0){
                $response['total_month_hours_view'] = $minutes1.' min';
            } else{
                $response['total_month_hours_view'] = '';
            }

            $response['total_month_hours'] = $totalMonthHours."";

            // Leave Data

            $leaveQry = $d->select("leave_master","society_id = '$society_id'
                    AND user_id = '$user_id'
                    AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'
                    AND leave_status = '1'");

            $leaveArray = array();
            $halfHeaveArray = array();

            if (mysqli_num_rows($leaveQry) > 0) {

                while($leaveData = mysqli_fetch_array($leaveQry)){

                    if ($leaveData['leave_day_type'] == "0") {
                        $lary = range_date($leaveData['leave_start_date'],
                            $leaveData['leave_end_date']);

                        for ($a = 0; $a < count($lary); $a++) {
                            array_push($leaveArray,$lary[$a]);
                        }
                    }else{
                        $laryHalf = range_date($leaveData['leave_start_date'],
                            $leaveData['leave_end_date']);

                        for ($b = 0; $b < count($laryHalf); $b++) {
                            array_push($halfHeaveArray,$laryHalf[$b]);
                        }
                    }
                }
            }

            // Month

            $totalLatePunchIN = 0;
            $totalEarlyPunchOUT = 0;
            $totalPunchOUTMissing = 0;
            $totalPresent = 0;
            $totalWeekOff = 0;
            $totalHolidays = 0;
            $totalLeaves = 0;
            $totalHalfDays = 0;
            $totalAbsent = count($datesArrayOfMonth);
            $totalAbsentDays = 0;
            $totalExtraDays = 0;
            $totalPendingAttendance = 0;
            $totalRejectedAttendance = 0;

            $totalMonthHourSpent = array();
        
            for ($j = 0; $j < count($datesArrayOfMonth); $j++) {
                
                $dates = array();

                $monthDate = $datesArrayOfMonth[$j];
                $month = date('Y-m',strtotime($monthDate));
                $dates['date'] = $monthDate;
                $day_pos_new =  date('w', strtotime($monthDate));
                //echo "($monthDate) Day Position - ".$day_pos_new."\n";
                $dates['date_name'] = date('d F Y',strtotime($monthDate));
                $dates['day_name'] = date('l',strtotime($monthDate));

                $dates["total_spend_time"] = "0";

                $dates['holiday'] = false;
                $dates['week_off'] = false;
                $dates['work_report'] = false;
                $dates['leave'] = false;
                $dates['present'] = false;
                $dates['half_day'] = false;
                $dates['extra_day'] = false;
                $dates['late_in'] = false;
                $dates['early_out'] = false;
                $dates['is_punch_out_missing'] = false;
                $dates['punch_in_request_sent'] = false;

                $isDateGone = "false";

                $dates["punch_out_missing_message"] = "";

                if ($monthDate < date('Y-m-d')) {
                    $isDateGone = "true";
                    $dates['is_date_gone'] = true;
                }else{
                    $isDateGone = "false";
                    $dates['is_date_gone'] = false;
                }

                if (in_array($monthDate,$holidayAry)) {
                    $dates['holiday'] = true;
                    $totalHolidays += 1;
                    $isDateGone = "false";
                }else{
                    if (in_array($monthDate,$weekAry)) {

                        $dates['week_off'] = true;
                        $totalWeekOff += 1;
                        $isDateGone = "false";

                        //echo "\n($monthDate) - Week Off - ".date('D',strtotime($monthDate))."\n\n";
                    }

                    /*$x1 = weekOfMonth(strtotime($monthDate));

                    if ($shiftData['has_altenate_week_off'] == "1") {  
                        if (in_array($day_pos_new,$alternate_weekoff_days_array) && 
                            in_array($x1,$alternateWeekOff)) {
                            $dates['week_off'] = true;
                            $totalWeekOff += 1;
                            $isDateGone = "false";
                            //echo "\n($monthDate) - Week Off -".date('D',strtotime($monthDate))."\n\n";
                        }
                    }*/
                }

                $dates["leave_reason"] = "";
                $dates["leave_type_name"] = "";

                if (in_array($monthDate,$leaveArray)) {
                    $dates['leave'] = true;
                    $totalLeaves += 1;
                    $isDateGone = "false";

                    $leaveDataQry = $d->selectRow("leave_type_name,leave_reason,paid_unpaid","leave_master,leave_type_master","leave_master.society_id = '$society_id' AND leave_master.user_id = '$user_id' AND leave_master.leave_status = '1' AND leave_type_master.leave_type_id = leave_master.leave_type_id AND ('$monthDate' BETWEEN leave_master.leave_start_date AND leave_master.leave_end_date)");

                    if (mysqli_num_rows($leaveDataQry) > 0) {

                        $leaveDataTrue = mysqli_fetch_array($leaveDataQry);

                        if ($leaveDataTrue['paid_unpaid'] == 1) {
                            $dates["leave_type_name"] = $leaveDataTrue['leave_type_name']." - Unpaid";
                        }else{
                            $dates["leave_type_name"] = $leaveDataTrue['leave_type_name']." - Paid";
                        }

                        $dates["leave_reason"] = $leaveDataTrue['leave_reason'];
                        
                    }
                }

                if (in_array($monthDate,$halfHeaveArray)) {
                    $dates['half_day'] = true;
                    $totalHalfDays += 1;
                    $isDateGone = "false";

                    $leaveDataQry = $d->selectRow("leave_type_name,leave_reason,paid_unpaid","leave_master,leave_type_master","leave_master.society_id = '$society_id' AND leave_master.user_id = '$user_id' AND leave_master.leave_status = '1' AND leave_type_master.leave_type_id = leave_master.leave_type_id AND ('$monthDate' BETWEEN leave_master.leave_start_date AND leave_master.leave_end_date)");

                    if (mysqli_num_rows($leaveDataQry) > 0) {

                        $leaveDataTrue = mysqli_fetch_array($leaveDataQry);

                        if ($leaveDataTrue['paid_unpaid'] == 1) {
                            $dates["leave_type_name"] = $leaveDataTrue['leave_type_name']." - Unpaid";
                        }else{
                            $dates["leave_type_name"] = $leaveDataTrue['leave_type_name']." - Paid";
                        }

                        $dates["leave_reason"] = $leaveDataTrue['leave_reason'];
                        
                    }
                }

                $holidayNameQry = $d->selectRow("holiday_name,holiday_description","holiday_master","society_id = '$society_id' AND holiday_start_date = '$monthDate'");

                $dates["holiday_name"] = "";
                $dates["holiday_description"] = "";

                if (mysqli_num_rows($holidayNameQry) > 0) {

                    $holidayNameData = mysqli_fetch_array($holidayNameQry);

                    $dates["holiday_name"] = $holidayNameData['holiday_name'];
                    $dates["holiday_description"] = $holidayNameData['holiday_description'];
                }

                $tdt = date('Y-m-d');

                $attendanceQry = $d->selectRow("am.*,wrm.work_report as workReport,apomr.attendance_id as attid, apomr.attendance_punch_out_missing_id, apomr.punch_out_missing_reject_reason,apomr.attendance_punch_out_missing_status","attendance_master AS am LEFT JOIN attendance_punch_out_missing_request AS apomr ON apomr.attendance_id = am.attendance_id LEFT JOIN work_report_master as wrm ON (am.attendance_date_start = wrm.work_report_date AND am.user_id = wrm.user_id AND am.society_id = wrm.society_id)","
                    am.society_id='$society_id' 
                    AND am.user_id = '$user_id'
                    AND am.attendance_date_start = '$monthDate'");

                if(mysqli_num_rows($attendanceQry)>0){

                
                    $times = array();

                    $data = mysqli_fetch_array($attendanceQry);

                    $totalPresent += 1;
                    $dates['present'] = true;

                    if ($data['attendance_status'] == "0") {
                        $totalPendingAttendance += 1;
                        $totalPresent = $totalPresent - 1;
                        $dates["attendnace_pending"] = true;
                        $dates["attendance_pending_message"] = "Attendance Pending";
                    }else{
                        $dates["attendnace_pending"] = false;
                        $dates["attendance_pending_message"] = "";
                    }

                    if ($data['attendance_status'] == "0" && $data['punch_in_request'] == "1") {
                        $dates["punch_in_request_sent"] = true;
                    }else{
                        $dates["punch_in_request_sent"] = false;
                    }

                    if ($data['attendance_status'] == "2") {
                        $totalRejectedAttendance += 1;
                        $totalPresent = $totalPresent - 1;
                        $dates["attendance_declined"] = true;
                        $dates["attendance_declined_message"] = $data['attendance_declined_reason'];
                        if ($data['punch_in_request'] == "1"){
                            $dates["punch_in_request_sent"] = true;
                        }
                    }else{
                        $dates["attendance_declined"] = false;
                        $dates["attendance_declined_message"] = "";
                    }


                    $attendanceDateStart = $data['attendance_date_start'];
                    $attendanceDateEnd = $data['attendance_date_end'];


                    if ($data['attendance_punch_out_missing_id'] != null) {

                       
                        $dates['attendance_punch_out_missing_id'] = $data['attendance_punch_out_missing_id'];
                        $dates['punch_out_missing_reject_reason'] = $data['punch_out_missing_reject_reason'];

                        $attendance_punch_out_missing_status = $data['attendance_punch_out_missing_status'];

                        $dates['attendance_punch_out_missing_status'] = $attendance_punch_out_missing_status;
                        
                        if ($attendance_punch_out_missing_status == "0") {
                            $dates['attendance_punch_out_missing_status_view'] = "Pending";
                        }else if ($attendance_punch_out_missing_status == "1") {
                            $dates['attendance_punch_out_missing_status_view'] = "Approved";
                        }else if ($attendance_punch_out_missing_status == "2") {
                            $dates['attendance_punch_out_missing_status_view'] = "Rejected";
                        }
                    }

                    if ($data['workReport'] !=null || $data['workReport'] != "") {
                        $dates["work_report"] = true;
                    }


                    if (in_array($attendanceDateStart,$weekAry)) {
                        $dates['extra_day'] = true;

                        if (in_array($attendanceDateStart,$halfHeaveArray)) {
                            $totalExtraDays = $totalExtraDays + 0.5;
                            $totalPresent = $totalPresent - 0.5;
                        }else{
                            $totalExtraDays = $totalExtraDays + 1;
                            $totalPresent = $totalPresent - 1;
                        }
                        
                    }

                    if (in_array($attendanceDateStart,$holidayAry)) {
                        $dates['extra_day'] = true;
                        
                        if (in_array($attendanceDateStart,$halfHeaveArray)) {
                            $totalExtraDays = $totalExtraDays + 0.5;
                            $totalPresent = $totalPresent - 0.5;
                        }else{
                            $totalExtraDays = $totalExtraDays + 1;
                            $totalPresent = $totalPresent - 1;
                        }
                    }

                    /*if (in_array($attendanceDateStart,$halfHeaveArray)) {
                        $totalPresent = $totalPresent - 0.5;
                    }

                    if (in_array($attendanceDateStart,$leaveArray)) {
                        $totalLeaves = $totalLeaves - 1;
                    }*/
                    
                    $punchInTime = $data['punch_in_time'];
                    $punchOutTime = $data['punch_out_time'];

                    $dates['attendance_id'] = $data['attendance_id'];
                    $dates['unit_id'] = $data['unit_id'];
                    $dates['work_report_title'] = $data['work_report_title'].'';
                    $dates['work_report_description'] = $data['workReport'].'';
                    $dates['punch_in_time'] = date("h:i A",strtotime($punchInTime));
                    $dates['punch_out_time'] = date("h:i A",strtotime($punchOutTime));

                    if ($data['late_in'] == 1) {
                        $totalLatePunchIN += 1;
                        $dates['late_in'] = true;
                    }
                     
                    if ($data['early_out'] == 1) {
                        $totalEarlyPunchOUT += 1;
                        $dates['early_out'] = true;
                    }
                    
                    if ($attendanceDateEnd == "0000-00-00" || $punchOutTime == "00:00:00") {
                        $dates['punch_out_time']= "";
                        $attendanceDateEnd = date('Y-m-d');
                        $punchOutTime = date('H:i:s');
                    }

                    if ($data['punch_out_time'] != "00:00:00") {

                        $totalHours = getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],
                            $data['punch_in_time'],$data['punch_out_time']);

                        $totalHoursName = getTotalHoursWithNames($data['attendance_date_start'],$data['attendance_date_end'],
                            $data['punch_in_time'],$data['punch_out_time']);

                        $times[] = $totalHours;

                        $dates['total_hours_spend'] = $totalHoursName;

                        $totalMonthHourSpent[] = $totalHours;

                        $dates['total_spend_time'] = getTotalWeekHours($times);
                        $dates["punch_out_missing_message"] = "";
                        $dates['is_punch_out_missing'] = false;

                    }else{
                        $dates['total_spend_time'] = "00:00";
                        $dates['total_hours_spend'] = "00:00";
                        if ($attendanceDateStart == $tdt) {
                            $dates["punch_out_missing_message"] = "";
                            $dates['is_punch_out_missing'] = false;
                        }else if(($data['attendance_status'] == '1' || $data['attendance_status'] == '0') &&
                            $data['punch_out_time'] == "00:00:00"){                    
                            $dates["punch_out_missing_message"] = "Punch Out Missing";
                            $dates['is_punch_out_missing'] = true;
                            $totalPunchOUTMissing += 1;

                            if ($shiftType == "Night") {

                                $previousDateView = date('dS F Y',strtotime("+1 days",strtotime($attendanceDateStart)));
                                $previousDate = date('Y-m-d',strtotime("+1 days",strtotime($attendanceDateStart)));
                                $datesArray = array(
                                    "date_one"=>date('Y-m-d',strtotime($attendanceDateStart)),
                                    "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                    "date_two"=>$previousDate,
                                    "date_two_view"=>$previousDateView
                                );
                            }else{
                                $datesArray = array(
                                    "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                    "date_one"=>date('Y-m-d',strtotime($attendanceDateStart))
                                );
                            }

                            $dates['punch_missing_date_ary'] = $datesArray;
                        }else if($data['attendance_status'] == '1' || $data['attendance_status'] == '0'){
                            $dates["punch_out_missing_message"] = "";
                            $dates['is_punch_out_missing'] = false;
                        }
                    }

                    $attendanceHistoryQry = $d->select("attendance_master,attendance_break_history_master,
                        attendance_type_master","attendance_master.attendance_id = attendance_break_history_master.attendance_id
                        AND attendance_master.attendance_id = '$dates[attendance_id]'
                        AND attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id 
                        AND attendance_break_history_master.break_out_time != '00:00:00' 
                        AND attendance_break_history_master.break_end_date != '0000-00-00' ");

                    $dates["attendance_history"] = array();
                    
                    if(mysqli_num_rows($attendanceHistoryQry)>0){

                        $totalBreakTimeAtt = array();

                        while($historyData = mysqli_fetch_array($attendanceHistoryQry)){
                            $historyArray = array();

                            $breakStartDate = $historyData['break_start_date'];
                            $breakEndDate = $historyData['break_end_date'];
                            
                            $breakInTime = $historyData['break_in_time'];
                            $breakOutTime = $historyData['break_out_time'];

                            $historyArray['attendance_break_history_id'] = $historyData['attendance_break_history_id'];
                            $historyArray['attendance_type_name'] = $historyData['attendance_type_name'];
                            $historyArray['break_start_date'] = date('d F Y', strtotime($breakStartDate));
                            $historyArray['break_end_date'] = date('d F Y', strtotime($breakEndDate));
                            $historyArray['break_in_time'] = date('h:i A', strtotime($breakInTime));
                            $historyArray['break_out_time'] = date('h:i A', strtotime($breakOutTime));

                            $totalBreak = getTotalHours($breakStartDate,$breakEndDate,$breakInTime,$breakOutTime);

                            $totalBreakTimeAtt[] = $totalBreak;

                            $dates['total_spend_time_break'] = getTotalWeekHours($totalBreakTimeAtt);

                            $totalBreakHoursName = getTotalHoursWithNames($breakStartDate,$breakEndDate,$breakInTime,$breakOutTime);

                            $historyArray['total_break_hours_spend'] =$totalBreakHoursName;

                            array_push($dates['attendance_history'],$historyArray);
                        }
                    }
                }else{
                    if ($isDateGone == "true") {
                        $totalAbsentDays = ($totalAbsentDays + 1);
                    }
                }

                $response['total_month_hour_spent'] = $totalWH = getTotalWeekHours($totalMonthHourSpent);
                $response['total_month_hour_spent_view'] = getTotalWeekHoursName($totalMonthHourSpent);

                $timeWH = explode(".",$totalWH);
                $hourWH = $timeWH[0]*60;
                $minuteWH = $timeWH[1];

                $totalMinuteWH = $hourWH + $minuteWH;

                $timeMH = explode(".",$totalMonthHours);
                $hourMH = $timeMH[0]*60;
                if ($timeMH[1] == 5 || $timeMH[1] == 50) {
                    $minuteMH = 30;
                }else{
                    $minuteMH = $timeMH[1];
                }

                $totalMinuteMH = $hourMH + $minuteMH;

                if ($totalMinuteWH > $totalMinuteMH) {

                    $response['has_extra_hours'] = true;

                    $totalExtraHours = $totalMinuteWH - $totalMinuteMH;

                    $hours0 = floor($totalExtraHours / 60);
                    $minutes0 = ($totalExtraHours -  floor($totalExtraHours / 60) * 60);
    
                    if ($hours0 > 0 && $minutes0 > 0) {
                        $response['total_extra_hours_view'] = $hours0 . ' hr ' . $minutes0.' min';
                    }else if($hours0 > 0 && $minutes0 <= 0){
                        $response['total_extra_hours_view'] = $hours0.' hr';
                    } else if($hours0 <= 0 && $minutes0 > 0){
                        $response['total_extra_hours_view'] = $minutes0.' min';
                    } else{
                        $response['total_extra_hours_view'] = '0';
                    }

                    $response['total_remaining_hours_view'] = "0";                    
                }else{
                    $response['has_extra_hours'] = false; 
                    $response['total_extra_hours_view'] = "0"; 

                    $totalRemainingHours = $totalMinuteMH - $totalMinuteWH."\n";

                    $hours1 = floor($totalRemainingHours / 60);
                    $minutes1 = ($totalRemainingHours -  floor($totalRemainingHours / 60) * 60);

                    if ($hours1 > 0 && $minutes1 > 0) {
                        $response['total_remaining_hours_view'] = $hours1 . ' hr ' . $minutes1.' min';
                    }else if($hours1 > 0 && $minutes1 <= 0){
                        $response['total_remaining_hours_view'] = $hours1.' hr';
                    } else if($hours1 <= 0 && $minutes1 > 0){
                        $response['total_remaining_hours_view'] = $minutes1.' min';
                    } else{
                        $response['total_remaining_hours_view'] = '';
                    }
                }


                array_push($response['monthly_history'],$dates);
            }

            $totalPresentDays = $totalPresent + $totalExtraDays;

            $totalHD = 0;

            if ($totalHalfDays > 0) {
                $totalHD = 0.5 * $totalHalfDays;
            }
 
            $response['late_punch_in'] = $totalLatePunchIN."";
            $response['early_punch_out'] = $totalEarlyPunchOUT."";
            $response['total_present'] = $totalPresentDays - $totalHD."";
            $response['total_leave'] = $totalLeaves."";
            $response['total_half_day'] = $totalHalfDays."";
            $response['total_punch_out_missing'] = $totalPunchOUTMissing."";
            $response['total_week_off'] = $totalWeekOff."";
            $response['total_holidays'] = $totalHolidays."";
            $response['total_pending_attendance'] = $totalPendingAttendance."";
            $response['total_rejected_attendance'] = $totalRejectedAttendance."";

            $totalCData = $totalPresent + $totalWeekOff + $totalHolidays;
            $totalWorkingDays = $totalAbsent - ($totalWeekOff + $totalHolidays);

            $response['total_working_days'] = $totalWorkingDays."";

            $tempExtraDays = $totalExtraDays;

            /*if ($totalWorkingDays == $totalPresentDays || $totalWorkingDays > $totalPresentDays) {
                $totalExtraDays = 0;
            }*/

            //if ($totalExtraDays > 0) {
                $tempExtraDays = 0;
            //}

            $response['total_extra_days'] = $totalExtraDays."";
            $response['total_extra_days_view'] = $tempExtraDays."";
            $response['total_absent'] = $totalAbsentDays."";
            
            echo json_encode($response);
        
        }else if($_POST['getAbsentPresentData']=="getAbsentPresentData" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            if ($floor_id != null && $floor_id !='') {
                $appendQuery = " AND users_master.floor_id = '$floor_id'";
            }

            $tdt = date('Y-m-d');

            $usersQry=$d->select("users_master,block_master,floors_master","
                users_master.delete_status = '0' 
                AND users_master.society_id = '$society_id' 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id 
                AND users_master.user_status = '1' 
                AND users_master.floor_id = '$floor_id'","ORDER BY user_sort ASC");

            if (mysqli_num_rows($usersQry) > 0) {

                $response['present_employee'] = array();
                $response['absent_employee'] = array();
                
                while ($userData = mysqli_fetch_array($usersQry)) {

                    $users = array();

                    $userId = $userData['user_id'];

                    $users['user_name'] = $userData['user_full_name'];
                    $users['branch_name'] = $userData['block_name'];
                    $users['department_name'] = $userData['floor_name'];
                    $users['user_designation'] = $userData['user_designation'];
                    
                    if ($userData['user_profile_pic'] != '') {
                        $users["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $userData['user_profile_pic'];
                    } else {
                        $users["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $attendanceQry = $d->selectRow("*","attendance_master","attendance_date_start = '$tdt' AND user_id = '$userId'");

                    if(mysqli_num_rows($attendanceQry)>0){
                        
                        $users['is_on_break'] = false;
                        $users['break_type'] = "";
                       
                        $attendanceData = mysqli_fetch_array($attendanceQry);

                        $users['punch_in_time'] = date('h:i A',strtotime($attendanceData['punch_in_time']));
                        $users['punch_out_time'] = date('h:i A',strtotime($attendanceData['punch_out_time']));
                        
                        if ($attendanceData['punch_out_time'] == "00:00:00" && $attendanceData['attendance_date_end']) {
                            $users['punched_out'] = false;
                        }else{
                            $users['punched_out'] = true;
                        }

                        $attendanceId = $attendanceData['attendance_id'];

                        $attendanceHistoryQry = $d->selectRow("*","attendance_master,attendance_break_history_master,attendance_type_master","attendance_master.attendance_id = attendance_break_history_master.attendance_id AND attendance_master.attendance_id = '$attendanceId' AND attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id AND attendance_break_history_master.break_out_time = '00:00:00' AND  attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id AND attendance_break_history_master.break_end_date = '0000-00-00'","ORDER BY attendance_break_history_master.attendance_break_history_id DESC LIMIT 1");

                        if (mysqli_num_rows($attendanceHistoryQry) > 0) {

                            $attendanceBreakData = mysqli_fetch_array($attendanceHistoryQry);

                            $users['is_on_break'] = true;
                            $users['break_type'] = $attendanceBreakData['attendance_type_name'];
                        }

                        array_push($response['present_employee'],$users);
                    }else{
                        array_push($response['absent_employee'],$users);
                    }

                }
                $response['status'] = "200";
                $response['message'] = "success";
                echo json_encode($response);
            }else{
                $response['status'] = "201";
                $response['message'] = "No data";
                echo json_encode($response);
            }        
        }else if($_POST['checkQRCode']=="checkQRCode" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $newDateTime = date('Ymdhis',strtotime('+25 seconds'));
            $currentTimeStamp = date('Ymdhis',strtotime('-10 seconds'));

            if ($date_time >= $currentTimeStamp && $date_time <= $newDateTime) {
                $response["message"]="QR Code Verified";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Invalid QR Code";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="Wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="Wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function getTotalWeekHours($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d.%02d', $hours, $minutes);
}

function getTotalWeekHoursName($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "No Data";
    }
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}


function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "No Data";
    }

}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}

function weeks_in_month($month, $year){
    $dates = [];

    $week = 1;
    $date = new DateTime("$year-$month-01");
    $days = (int)$date->format('t'); // total number of days in the month

    $oneDay = new DateInterval('P1D');

    for ($day = 1; $day <= $days; $day++) {
        $dates["Week_$week"] []= $date->format('Y-m-d');

        $dayOfWeek = $date->format('l');
        if ($dayOfWeek === 'Saturday') {
            $week++;
        }

        $date->add($oneDay);
    }

    return $dates;
}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}
?>