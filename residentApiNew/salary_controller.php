<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
        if($_POST['getSalaries']=="getSalaries" && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q = $d->select("salary_slip_master","user_id='$user_id' AND salary_month_name LIKE '%$salary_year%' AND salary_slip_status = '2' AND share_with_user = '1'","ORDER BY salary_start_date ASC");

            $arraySalaryIds = array();

            if (mysqli_num_rows($q) > 0) {

                $response["salary"] = array();   

                $totalEarnings = 0;         
                $totalDeductions = 0;         

                while ($data = mysqli_fetch_array($q)) {
                    $salary = array();
                    $salary["salary_slip_id"] = $data['salary_slip_id'];
                    $salary["salary_id"] = $data['salary_id'];
                    $salary["salary_type_current"] = $data['salary_type_current'];                    

                    $salary_month_name = $data['salary_month_name'];
                    $salary_month_name = explode("-",$salary_month_name);

                    $month = $salary_month_name[0];
                    $monthName = date("F", mktime(0, 0, 0, $month, 10));

                    $salary["monthName"] = $monthName;
                    $salary["year"] = $salary_month_name[1];

                    $salary["total_deduction_salary"] = $data['total_deduction_salary'];
                    $salary["total_earning_salary"] = $data['total_earning_salary'];
                    $salary["total_net_salary"] = $data['total_net_salary'];
                    $salary["month_net_salary"] = $data['month_net_salary'];
                    $salary["payslip_download_url"] = $base_url.'apAdmin/SalarySlipPrint.php?sId='.$society_id.'&salId='.$data['salary_slip_id'];

                    $totalEarnings = $totalEarnings + $data['total_net_salary'];
                    $totalDeductions = $totalDeductions + $data['total_deduction_salary'];

                    array_push($response["salary"], $salary);
                    array_push($arraySalaryIds, $data['salary_slip_id']);
                }

                $finalIds = join("','",$arraySalaryIds);                 

                $qryEarning = $d->selectRow("earning_deduction_name_current,SUM(earning_deduction_amount)","salary_slip_sub_master","salary_slip_id IN ('$finalIds') AND earning_deduction_type_current = '0'","GROUP BY earning_deduction_name_current");

                $response['earnings_total'] = array();

                if (mysqli_num_rows($qryEarning) > 0) {

                    while ($dataEarning = mysqli_fetch_array($qryEarning)) {
                        $earning = array();

                        $earning['earning_type'] = $dataEarning['earning_deduction_name_current'];
                        $earning['earning_amount'] = $dataEarning['SUM(earning_deduction_amount)'];

                        array_push($response['earnings_total'],$earning);
                    }
                }

                $qryDeduction = $d->selectRow("earning_deduction_name_current,SUM(earning_deduction_amount)","salary_slip_sub_master","salary_slip_id IN ('$finalIds') AND earning_deduction_type_current = '1'","GROUP BY earning_deduction_name_current");

                $response['deductions_total'] = array();

                if (mysqli_num_rows($qryDeduction) > 0) {

                    while ($dataEarning = mysqli_fetch_array($qryDeduction)) {
                        $deductions = array();

                        $deductions['deduction_type'] = $dataEarning['earning_deduction_name_current'];
                        $deductions['deduction_amount'] = $dataEarning['SUM(earning_deduction_amount)'];

                        array_push($response['deductions_total'],$deductions);
                    }

                }


                $response["total_earning"] = number_format($totalEarnings,2,'.','');
                $response["total_deductions"] = number_format($totalDeductions,2,'.','');
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Faild to get List...!!";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['getSalaryDetails']=="getSalaryDetails" && $user_id !=0 && $user_id!='' &&  filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 

            $earnArray = array();
            $dedcutArray = array();
            
            $q2 = $d->selectRow("salary_slip_master.*,
                pre_by_admin.admin_name AS pre_by_admin_name,
                floors_master.floor_name ,
                block_master.block_name ,
                users_master.user_full_name ,
                user_bank_master.esic_no ,
                user_bank_master.account_no ,
                user_bank_master.bank_name ,
                users_master.user_designation ,
                checked_by_admin.admin_name AS checked_by_admin_name,
                pub_by_admin.admin_name AS pub_by_admin_name
                ","salary_slip_master
                LEFT JOIN bms_admin_master  AS pre_by_admin ON pre_by_admin.admin_id = salary_slip_master.prepared_by
                LEFT JOIN block_master ON block_master.block_id = salary_slip_master.block_id
                LEFT JOIN users_master ON users_master.user_id = salary_slip_master.user_id
                LEFT JOIN user_bank_master ON user_bank_master.user_id = salary_slip_master.user_id  AND user_bank_master.is_primary=1
                LEFT JOIN floors_master ON floors_master.floor_id = salary_slip_master.floor_id
                LEFT JOIN bms_admin_master AS checked_by_admin ON checked_by_admin.admin_id = salary_slip_master.checked_by
                LEFT JOIN bms_admin_master AS pub_by_admin ON pub_by_admin.admin_id = salary_slip_master.authorised_by
                ","salary_slip_id=$salary_slip_id");

            $data = mysqli_fetch_assoc($q2);

            if ($data) {
                if (isset($data['salary_month_name']) && $data['salary_month_name'] != "") {
                    $salary_month_name =  $data['salary_month_name'];
                }
                
                $q3 = $d->selectRow("salary_slip_sub_master.*", "salary_slip_sub_master", "salary_slip_id=$salary_slip_id");
                
                while ($salData = mysqli_fetch_array($q3)) {
                    if ($salData['earning_deduction_type_current'] == 0) {
                        array_push($earnArray, $salData);
                    } else {
                        array_push($dedcutArray, $salData);
                    }
                }
            }
                          
            $response["salary_slip_id"] = $data['salary_slip_id'];
            $response["total_month_days"] = $data['total_month_days'];
            $response["leave_days"] = $data['paid_leave_days'];
            $response["extra_days"] = $data['extra_days'];
            $response["total_working_days"] = $data['total_working_days'];
            $response["salary_type_current"] = $data['salary_type_current'];

            if ($data['salary_type_current'] == 2) {
                $response["day_salary"] = $data['per_hour_salary'];
            }else{
                $response["day_salary"] = $data['per_day_salary'];            
            }

            $salary_month_name = $data['salary_month_name'];
            $salary_month_name = explode("-",$salary_month_name);

            $month = $salary_month_name[0];
            $monthName = date("F", mktime(0, 0, 0, $month, 10));

            $response["salary_month_year"] = $monthName.' '. $salary_month_name[1];

            if (isset($data['salary_mode']) && $data['salary_mode'] != "") {
                if ($data['salary_mode'] == 0) {
                    $response["salary_mode"] =  "Bank Transaction";
                } else if ($data['salary_mode'] == 1) {
                    $response["salary_mode"] =  "Cash";
                } else {
                    $response["salary_mode"] = "Cheque";
                }
            }

            if (!empty($earnArray)) {
                $count = count($earnArray);
                $Dcount = count($dedcutArray);
                
                for ($i = 0; $i < $count; $i++) {

                    $earning[$i] = array(
                        "title"=> $earnArray[$i]['earning_deduction_name_current'],
                        "value"=> $earnArray[$i]['earning_deduction_amount']);
                                                
                    if ($i < $Dcount) {
                        $deduction[$i] = array(
                            "title"=> $dedcutArray[$i]['earning_deduction_name_current'],
                            "value"=> $dedcutArray[$i]['earning_deduction_amount']);
                    }
                }
            } 

            $response['earning_data'] = $earning;
            $response['deduction_data'] = $deduction;

            if(isset($data['expense_amount']) && $data['expense_amount']>0){
                    
                if (isset($data['expense_amount']) && $data['expense_amount'] > 0) {
                    $response["expense_amount"] =  $data['expense_amount'];
                } else {
                    $response["expense_amount"] = "-";
                }
            }else {
                $response["expense_amount"] = "-";
            }

            if ((isset($data['other_earning']) && $data['other_earning'] > 0) || (isset($data['other_earning']) && $data['other_earning'] > 0)) {
                    
                if (isset($data['other_earning']) && $data['other_earning'] > 0) {
                    $response["other_earning"] =  $data['other_earning'];
                } else {
                    $response["other_earning"] = "-";
                }
            }else {
                $response["other_earning"] = "-";
            }

            if(isset($data['other_deduction']) && $data['other_deduction'] > 0) {
                    
                if (isset($data['other_deduction']) && $data['other_deduction'] > 0) {
                    $response["other_deduction"] =  $data['other_deduction'];
                } else {
                    $response["other_deduction"] = "-";
                }
            }else{
                $response["other_deduction"] = "-";
            }

            if (isset($data['total_earning_salary']) && $data['total_earning_salary'] > 0) {             
                $response["gross_salary"] =  $data['total_earning_salary'];
            }else{
                $response["gross_salary"] =  "0.00";
            }

            if (isset($data['total_deduction_salary']) && $data['total_deduction_salary'] > 0) {             
                $response["total_deduction_salary"] =  $data['total_deduction_salary'];
            }else{
                $response["total_deduction_salary"] =  "0.00";
            }

            if (isset($data['total_net_salary']) && $data['total_net_salary'] > 0) {
                $netsalary = round($data['total_net_salary']);          
                $response["total_net_salary"] =  number_format($netsalary,2,'.','');
                $response["amount_in_word"] =  changeAmountInWord($netsalary).'';
                $response["amount_in_words"] =  changeAmountInWord($netsalary).'';
            }else{
                $response["total_net_salary"] =  "0.00";
                $response["amount_in_word"] =  "";
                $response["amount_in_words"] =  "";
            }

            $response["payslip_download_url"] = $base_url.'apAdmin/SalarySlipPrint.php?sId='.$society_id.'&salId='.$data['salary_slip_id'];
            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
        }else if($_POST['addSalaryIssue']=="addSalaryIssue" && $user_id !=0 && $user_id!='' &&  filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 

            $m->set_data('salary_slip_id',$salary_slip_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('salary_issue',$salary_issue);
            $m->set_data('created_date', date("Y-m-d H:i:s"));

            $a1= array (
                'salary_slip_id'=> $m->get_data('salary_slip_id'),
                'society_id'=> $m->get_data('society_id'),
                'user_id'=> $m->get_data('user_id'),
                'salary_issue'=> $m->get_data('salary_issue'),
                'created_date'=> $m->get_data('created_date'),
            );

           
            $q=$d->insert("salary_raised_issue_master",$a1);

            if ($q == true) {

                $title = "New Salary Issue Raised";
                $description = "By, ".$user_name;


                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("22",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("22",$block_id,"ios");
          
                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"salaryRaisedIssues",
                  'admin_click_action '=>"salaryRaisedIssues",
                  'notification_logo'=>'work_report.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Issue raised";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }
            
        }else if($_POST['getOtherEarning']=="getOtherEarning" && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($society_id, FILTER_VALIDATE_INT) == true ){


            $q = $d->select("leave_payout_master,leave_type_master","leave_payout_master.user_id='$user_id' AND leave_payout_master.leave_type_id = leave_type_master.leave_type_id AND leave_payout_master.leave_payout_year = '$leave_payout_year' AND leave_payout_master.is_leave_carry_forward = '0'");

            if (mysqli_num_rows($q) > 0) {

                $response["other_earnings"] = array();   

                $totalEarnings = 0;         

                while ($data = mysqli_fetch_array($q)) {
                    $otherEarning = array();
                    $otherEarning["leave_payout_id"] = $data['leave_payout_id'];
                    $otherEarning["leave_type_id"] = $data['leave_type_id'];
                    $otherEarning["leave_type_name"] = $data['leave_type_name'];
                    $otherEarning["no_of_payout_leaves"] = $data['no_of_payout_leaves'];
                    $otherEarning["leave_payout_amount"] = $data['leave_payout_amount'];                    
                    $otherEarning["leave_payout_remark"] = $data['leave_payout_remark'];                    
                    $otherEarning["leave_payout_date"] = date('d F, Y',strtotime($data['leave_payout_date']));

                    $totalEarnings = $totalEarnings + $data['leave_payout_amount'];                  

                    array_push($response["other_earnings"], $otherEarning);
                }
                
                $response["total_earnings"] = number_format($totalEarnings,2,'.','');
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Faild to get List...!!";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['getAdvanceSalary']=="getAdvanceSalary" && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q = $d->select("advance_salary","user_id = '$user_id' AND society_id = '$society_id' AND advance_salary_delete_status = '0'");

            if (mysqli_num_rows($q) > 0) {

                $response["advance_salary"] = array();   

                while ($data = mysqli_fetch_array($q)) {
                    $advanceSalary = array();
                    $advanceSalary["advance_salary_id"] = $data['advance_salary_id'];
                    $advanceSalary["advance_salary_amount"] = $data['advance_salary_amount'];
                    $advanceSalary["advance_salary_date"] = date('d F, Y',strtotime($data['advance_salary_date']));

                    if ($data['advance_salary_mode'] == 0) {
                        $advanceSalary['advance_salary_mode'] = "Bank Transfer";
                    }else if ($data['advance_salary_mode'] == 1) {
                        $advanceSalary['advance_salary_mode'] = "Cash";
                    }else if ($data['advance_salary_mode'] == 2) {
                        $advanceSalary['advance_salary_mode'] = "Cheque";
                    }

                    if ($data['is_paid'] == 0) {
                        $advanceSalary['is_paid'] = false;
                    }else if ($data['is_paid'] == 1) {
                        $advanceSalary['is_paid'] = true;
                    }

                    $advanceSalary["date_view"] =date('d F',strtotime($data['advance_salary_date']));
                    $advanceSalary["year"] =date('Y',strtotime($data['advance_salary_date']));

                    $advanceSalary["advance_salary_remark"] = $data['advance_salary_remark'];
                    $advanceSalary["created_date"] = date('d F, Y',strtotime($data['created_date']));                    

                    array_push($response["advance_salary"], $advanceSalary);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Faild to get List...!!";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['getEmployeeLoan']=="getEmployeeLoan" && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q = $d->selectRow('employee_loan_master.*,users_master.*,loan_created_by_admin.admin_name,(SELECT SUM(emi_amount) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=1) AS paid_emi_amount,(SELECT count(*) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=1) AS paidEmiCount',"employee_loan_master LEFT JOIN users_master ON users_master.user_id = employee_loan_master.user_id  LEFT JOIN bms_admin_master AS loan_created_by_admin ON loan_created_by_admin.admin_id = employee_loan_master.loan_created_by","employee_loan_master.society_id = '$society_id' AND employee_loan_master.user_id = '$user_id'","ORDER BY employee_loan_master.loan_id DESC $limit");

            if (mysqli_num_rows($q) > 0) {

                $response["loan"] = array();   

                while ($data = mysqli_fetch_array($q)) {
                    $advanceSalary = array();
                    $advanceSalary["loan_id"] = $data['loan_id'];
                    $advanceSalary["loan_date"] = date('d F, Y',strtotime($data['loan_date']));
                    $advanceSalary["loan_amount"] = $data['loan_amount'];
                    $advanceSalary["paid_emi_count"] = $data['paidEmiCount'];
                    $advanceSalary["paid_emi_amount"] = $data['paid_emi_amount'];
                    $advanceSalary["remaining_amount"] = $data['loan_amount']-$data['paid_emi_amount'].'';
                    $advanceSalary["loan_emi"] = $data['loan_emi'];                  
                    $advanceSalary["loan_by"] = $data['admin_name'];

                    array_push($response["loan"], $advanceSalary);
                }
                
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Faild to get List...!!";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['getLoanEMI']=="getLoanEMI" && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q = $d->selectRow('employee_loan_master.*,users_master.*,loan_created_by_admin.admin_name,(SELECT SUM(emi_amount) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=1) AS paid_emi_amount,(SELECT count(*) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=1) AS paidEmiCount ,(SELECT count(*) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=0) AS RemainEmiCount',"employee_loan_master LEFT JOIN users_master ON users_master.user_id = employee_loan_master.user_id  LEFT JOIN bms_admin_master AS loan_created_by_admin ON loan_created_by_admin.admin_id = employee_loan_master.loan_created_by","employee_loan_master.loan_id = '$loan_id'");

            $data = mysqli_fetch_array($q);

            $response['loan_amount'] = number_format($data['loan_amount'],2,'.','');
            $response['paid_emi_amount'] = number_format($data['paid_emi_amount'],2,'.','');
            $response['total_loan_emi'] = $data['loan_emi'];
            $response['admin_name'] = $data['admin_name'];
            $response['paid_emi_count'] = $data['paidEmiCount'];
            $response['remain_emi_count'] = $data['RemainEmiCount'];
            $remAmount = $data['loan_amount']-$data['paid_emi_amount'].'';
            $response["remaining_amount"] = number_format($remAmount,2,'.','');

            if (isset($data['loan_date']) && $data['loan_date'] != "0000-00-00 00:00:00") {
                $response['loan_date'] = date("d F, Y", strtotime($data['loan_date']));
            }

            $qry = $q = $d->selectRow('loan_emi_master.*,bms_admin_master.admin_id,bms_admin_master.admin_name','loan_emi_master LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=loan_emi_master.emi_paid_by',"loan_emi_master.loan_id='$loan_id'");

            if (mysqli_num_rows($qry) > 0) {

                $response["loan_emi"] = array();   

                while ($data = mysqli_fetch_array($qry)) {

                    $emi = array();
                    $emi["emi_amount"] = ((float)$data['emi_amount']).'';
                    $emi["emi_created_at"] = date('d-m-Y',strtotime($data['emi_created_at']));

                    if($data['is_emi_paid']==0){
                        $emi['is_emi_paid'] = false;
                    }else{
                        $emi['is_emi_paid'] = true;
                    }

                    if($data['emi_paid_date'] !="0000-00-00") { 
                        $emi['emi_paid_date'] = date('d-m-Y',strtotime($data['emi_paid_date'])); 
                    }else{
                        $emi['emi_paid_date'] = '';
                    }

                    $emi["collected_by"] = $data['admin_name'].'';
                    
                    if($data['received_date'] !="0000-00-00" && $data['received_date'] !=null) {
                        $emi['received_date'] = date('d-m-Y',strtotime($data['received_date'])); 
                    }else{
                        $emi['received_date'] = '';
                    }

                    array_push($response["loan_emi"], $emi);
                }
                
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Faild to get List...!!";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}


function changeAmountInWord($number){

    $no = floor($number);
    $point = round($number - $no, 2) * 100;
    $hundred = null;
    $digits_1 = strlen($no);
    $i = 0;
    $str = array();
    $word = array('0' => '', '1' => 'One', '2' => 'Two',
        '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
        '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
        '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
        '13' => 'Thirteen', '14' => 'Fourteen',
        '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
        '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
        '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
        '60' => 'Sixty', '70' => 'Seventy',
        '80' => 'Eighty', '90' => 'Ninety');
    $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
    while ($i < $digits_1) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += ($divider == 10) ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $word[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $word[floor($number / 10) * 10]
            . " " . $word[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
        } else $str[] = null;
    }

    $str = array_reverse($str);
    $result = implode('', $str);
    $points = ($point) ?
    "." . $word[$point / 10] . " " . 
    $word[$point = $point % 10] : '';
    return trim($result) ." Only";
}
?>