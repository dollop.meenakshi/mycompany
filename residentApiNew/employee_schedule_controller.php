<?php
include_once 'lib.php';
$employeeLngName = $xml->string->employee;
if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d h:i A");
        
            if($_POST['getSchedule']=="getScheduleNew" && $emp_id!='' && filter_var($emp_id, FILTER_VALIDATE_INT) == true ){

                $qc=$d->selectRow("emp_type","employee_master","emp_id='$emp_id'");
                $empData= mysqli_fetch_array($qc);
                if ($empData['emp_type']==0) {
                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                } else {

                $daysName = ['Monday', 'Tuesday','Wednesday','Thursday','Friday', 'Saturday','Sunday'];
                $response["days"] = array();
                for ($i = 0; $i < 7; $i++) {
                    $days = array();
                    $dayName=$daysName[$i];
                    $days["day"]=$daysName[$i];
                    $qs=$d->select("time_slot_master","time_status=0");
                    $days["timeslot"] =array();
                    while ($tData=mysqli_fetch_array($qs)) {
                        $dayName = trim($dayName);
                        $timeslot = array();
                        $timeslot["time_slot_id"]=$tData['time_slot_id'];
                        $timeslot["time_slot"]=$tData['time_slot'];
                        $timeslot["dayName"]=$dayName;
                        
                        $q4=$d->select("employee_schedule","emp_id='$emp_id' AND schedule_day='$dayName' AND time_slot_id='$tData[time_slot_id]'");
                        $scData=mysqli_fetch_array($q4);
                        if (mysqli_num_rows($q4)>0) {
                            $timeslot["emp_id"]=$scData['emp_id'];
                            $timeslot["unit_id"]=$scData['unit_id'];
                            $timeslot["availble_status"]=false;
                            if ($scData['unit_id']==$unit_id) {
                                $timeslot["already_booked_slot"]=true;
                            } else {
                                $timeslot["already_booked_slot"]=false;
                            }
                        } else {
                             $timeslot["emp_id"]=" ";
                            $timeslot["unit_id"]=" ";
                            $timeslot["availble_status"]=true;
                            $timeslot["already_booked_slot"]=true;
                        }

                        
                        


                        array_push( $days["timeslot"], $timeslot);
                    }

                    array_push($response["days"], $days );
                    
                }
               $unitNameAray = array();
                 $qu=$d->select("unit_master,block_master,employee_unit_master","unit_master.block_id=block_master.block_id AND unit_master.unit_id=employee_unit_master.unit_id AND employee_unit_master.emp_id='$emp_id'","GROUP BY employee_unit_master.unit_id");
                while ($unitData=mysqli_fetch_array($qu)) {
                    array_push($unitNameAray,$unitData['block_name'].'-'.$unitData['unit_name']);
                }   

                $unitNames = explode(",", $unitNameAray);

                $qu=$d->select("employee_unit_master","emp_id='$emp_id' AND unit_id='$unit_id'");

                if(mysqli_num_rows($qu)>0){
                    $response["is_your_employee"]=true;
                } else {
                    $response["is_your_employee"]=false;
                }

                $response["working_units"]=$unitNameAray;
                $response["message"]="$datafoundMsg";
                $response["status"]="200";
                echo json_encode($response);
                }

            }else if($_POST['getSchedule']=="getScheduleCommon"){
                $daysName = ['Monday', 'Tuesday','Wednesday','Thursday','Friday', 'Saturday','Sunday'];
                $response["days"] = array();
                for ($i = 0; $i < 7; $i++) {
                    $days = array();
                    $dayName=$daysName[$i];
                    $days["day"]=$daysName[$i];
                    $qs=$d->select("time_slot_master","");
                    $days["timeslot"] =array();
                    while ($tData=mysqli_fetch_array($qs)) {
                        $dayName = trim($dayName);
                        $timeslot = array();
                        $timeslot["time_slot_id"]=$tData['time_slot_id'];
                        $timeslot["time_slot"]=$tData['time_slot'];
                        $timeslot["dayName"]=$dayName;
                        
                
                             $timeslot["emp_id"]=" ";
                            $timeslot["unit_id"]=" ";
                            $timeslot["availble_status"]=true;
                    
                    

                        array_push( $days["timeslot"], $timeslot);
                    }

                    array_push($response["days"], $days );
                    
                }
               
                $qu=$d->select("employee_unit_master","emp_id='$emp_id' AND unit_id='$unit_id'");

                if(mysqli_num_rows($qu)>0){
                    $response["is_your_employee"]=true;
                } else {
                    $response["is_your_employee"]=false;
                }

                $response["message"]="$datafoundMsg";
                $response["status"]="200";
                echo json_encode($response);


            } else if ($_POST['addSchedule']=="addScheduleNew" && $unit_id!='' && filter_var($unit_id, FILTER_VALIDATE_INT) == true ) {

                $qselect=$d->select("employee_master","emp_id='$emp_id' AND society_id='$society_id' ");
                $user_data = mysqli_fetch_array($qselect);

                // $daysName = ['Monday', 'Tuesday','Wednesday','Thursday','Friday', 'Saturday','Sunday'];
                $response["days"] = array();
                $checkDays= trim($time_slot_id);

                $timeSlotIdAry = explode(",",$time_slot_id);
                $days_name = explode(",",$days_name);
               


                $d->delete("employee_schedule","emp_id='$emp_id' AND unit_id='$unit_id'");

                for ($i = 0; $i < count($timeSlotIdAry); $i++) {
                    
                    $dayName=trim($days_name[$i]);
                    $time_slot_id= $timeSlotIdAry[$i];

                    $m->set_data('society_id',$society_id);
                    $m->set_data('emp_id',$emp_id);
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('day',$dayName);
                    $m->set_data('time_slot_id',$time_slot_id);
                    
                    $a = array(
                        'society_id' =>$m->get_data('society_id'),
                        'emp_id'=>$m->get_data('emp_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'schedule_day'=>$m->get_data('day'),
                        'time_slot_id'=>$m->get_data('time_slot_id'),
                        );
                   
                        $q = $d->insert("employee_schedule",$a);
                    
                }

            
                if($q==TRUE){
                    

                     $m->set_data('unit_id',$unit_id);
                     $m->set_data('user_id',$user_id);
                     $m->set_data('society_id',$society_id);
                     $m->set_data('emp_id', $emp_id);

                      $a = array(
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id') ,
                        'society_id'=>$m->get_data('society_id') ,
                        'emp_id'=>$m->get_data('emp_id') 
                      );

                    $qq=$d->select("employee_unit_master","emp_id='$emp_id' AND unit_id='$unit_id'");
                    if (mysqli_num_rows($qq)>0) {
                        // check user add or not
                        $qq22=$d->select("employee_unit_master","emp_id='$emp_id' AND unit_id='$unit_id' AND user_id='$user_id'");
                        if (mysqli_num_rows($qq22)==0) {
                            $d->insert("employee_unit_master",$a);
                        }
                    } else {
                        $q=$d->insert("employee_unit_master",$a);
                    }

                    $response["message"]="$addMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$somethingWrong";
                    $response["status"]="201";
                    echo json_encode($response);

                }

                

            } else if ($_POST['deleteSchedule']=="deleteSchedule" && filter_var($unit_id, FILTER_VALIDATE_INT) == true  && filter_var($schedule_id, FILTER_VALIDATE_INT) == true ) {
                
                
                $quserDataUpdate = $d->delete("employee_schedule","schedule_id='$schedule_id' AND unit_id='$unit_id'");
            
                if($quserDataUpdate==TRUE){

                    $response["message"]="$deleteMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$somethingWrong";
                    $response["status"]="201";
                    echo json_encode($response);

                }
                

            } else if ($_POST['addRating']=="addRating" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($emp_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true ) {
                
                $m->set_data('society_id',$society_id);
                $m->set_data('emp_id',$emp_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('rating_star',$rating_star);
                $m->set_data('rating_msg',test_input($rating_msg));
                $m->set_data('ratting_date',date("Y-m-d H:i:s"));
                
                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'emp_id'=>$m->get_data('emp_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'rating_star'=>$m->get_data('rating_star'),
                    'rating_msg'=>$m->get_data('rating_msg'),
                    'ratting_date'=>$m->get_data('ratting_date'),
                    );

                $qc=$d->select("employee_rating_master","unit_id='$unit_id' AND user_id='$user_id' AND emp_id='$emp_id'");
                if (mysqli_num_rows($qc)>0) {
                    $q = $d->update("employee_rating_master",$a,"unit_id='$unit_id' AND user_id='$user_id' AND emp_id='$emp_id'");
                } else {
                    $q = $d->insert("employee_rating_master",$a);
                }
                
            
                if($q==TRUE){
                    $thank_you_for_rating = $xml->string->thank_you_for_rating;
                    $response["message"]="$thank_you_for_rating";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                        $response["message"]="$somethingWrong";
                        $response["status"]="201";
                        echo json_encode($response);

                }
                

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>