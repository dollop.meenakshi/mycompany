<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
            if($_POST['getFeed']=="getFeed" && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true){

                $totalFeed=$d->count_data_direct("service_provider_timeline_id","local_service_providers_timeline","service_provider_timeline_status = 1 ");
                
                $qnotification=$d->select("local_service_providers_timeline,local_service_provider_users","local_service_providers_timeline.local_service_provider_id=local_service_provider_users.service_provider_users_id AND local_service_providers_timeline.service_provider_timeline_status = 1","ORDER BY local_service_providers_timeline.service_provider_timeline_id DESC LIMIT $limit_feed,10");

                $totalSocietyFeedLimit= $d->count_data_direct("service_provider_timeline_status","news_feed","service_provider_timeline_status = 1");


                if(mysqli_num_rows($qnotification)>0){

                    $response["feed"] = array();
                    while($data_notification=mysqli_fetch_array($qnotification)) {
                      
                        if ($data_notification['user_id']!=0) {
                            $qu=$d->selectRow("user_profile_pic,tenant_view,user_full_name","users_master","user_id='$data_notification[user_id]' AND society_id='$society_id'");
                            $userData=mysqli_fetch_array($qu);
                            $userProfile=$base_url."img/users/recident_profile/".$userData['user_profile_pic'];
                            $user_full_name=$userData['user_full_name'];
                        } else {
                            $userProfile=$base_url."img/newFav.png";
                            $user_full_name="Fincasys Admin";
                        }

                        $feed = array(); 
                        $feed["feed_id"]=$data_notification['feed_id'];
                        $feed["society_id"]=$data_notification['society_id'];
                        $feed["feed_msg"]=html_entity_decode($data_notification['feed_msg']);
                        $feed["user_name"]=$user_full_name;
                        $feed["block_name"]=$data_notification['block_name'];
                        $feed["user_id"]=$data_notification['user_id'];
                        $feed["user_profile_pic"]=$userProfile;
                        $feed_id = $data_notification['feed_id'];


                        $feed_img=explode('~',$data_notification['feed_img']);
                        $feed_img_height=explode('~',$data_notification['feed_img_height']);
                        $feed_img_width=explode('~',$data_notification['feed_img_width']);

                        $unique=array_unique($feed_img);

                        $feed["feed_img"]=array();
                        
                        for ($iFeed=0; $iFeed < count($unique) ; $iFeed++) { 
                            $feed_img=array();
                            $feed_img["feed_img"]=$base_url."img/users/recident_feed/".$unique[$iFeed];
                            $feed_img["feed_height"]=intval($feed_img_height[$iFeed]).'';
                            $feed_img["feed_width"]=intval($feed_img_width[$iFeed]).'';
                            array_push($feed["feed_img"], $feed_img); 

                        }


                        $feed["feed_type"]=$data_notification['feed_type'];
                        $feed["feed_video"]=$base_url."img/users/recident_feed/".$data_notification['feed_video'];
                        $feed["video_thumb"]=$base_url."img/users/recident_feed/".$data_notification['feed_img'];
                        $feed["modify_date"]=date("d M Y h:i A", strtotime($data_notification['modify_date']));
                       
                       
                        array_push($response["feed"], $feed); 
                    }

                    
                    

                    $response["pos1"]=$pos1+0;
                    $response["totalSocietyFeedLimit"]=''.$totalSocietyFeedLimit;
                    $response["message"]="Get Feeds success.";
                    $response["totalFeed"]=$totalFeed;
                    $response["video_duration"]=30;
                    $response["status"]="200";
                    echo json_encode($response);

                } else{
                    $response["message"]="No Feeds Found";
                    $response["video_duration"]=30;
                    $response["status"]="201";
                    echo json_encode($response);
                }
            }  else {
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);                  
        }
    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>