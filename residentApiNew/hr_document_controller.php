<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        if($_POST['getHRDocuments']=="getHRDocuments" && $user_id!='' && $unit_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true){

            if ($floor_id == null || $floor_id =='' || $floor_id ==' ') {
                $uq = $d->selectRow("floor_id","users_master","user_id = '$user_id'");
                $ud = mysqli_fetch_array($uq);
                $floor_id = $ud['floor_id'];
            }

            if ($block_id == null || $block_id =='' || $block_id ==' ') {
                $uq1 = $d->selectRow("block_id","users_master","user_id = '$user_id'");
                $udata = mysqli_fetch_array($uq1);
                $block_id = $udata['block_id'];
            }

            $docCatQry = $d->select("
                hr_document_category_master,
                hr_document_master
                ","(hr_document_category_master.hr_document_category_id = hr_document_master.hr_document_category_id 
                AND hr_document_category_master.society_id = hr_document_master.society_id 
                AND hr_document_master.society_id = '$society_id' 
                AND hr_document_master.block_id = '0'
                AND hr_document_master.floor_id = '0'
                AND hr_document_master.user_id = '0'
                AND hr_document_master.hr_doc_deleted = '0' 
                AND hr_document_master.hr_document_status = '0')
                
                OR (hr_document_category_master.hr_document_category_id = hr_document_master.hr_document_category_id 
                AND hr_document_category_master.society_id = hr_document_master.society_id 
                AND hr_document_master.society_id = '$society_id' 
                AND hr_document_master.block_id = '$block_id'
                AND hr_document_master.floor_id = '0'
                AND hr_document_master.user_id = '0'
                AND hr_document_master.hr_doc_deleted = '0' 
                AND hr_document_master.hr_document_status = '0')

                OR (hr_document_category_master.hr_document_category_id = hr_document_master.hr_document_category_id 
                AND hr_document_category_master.society_id = hr_document_master.society_id 
                AND hr_document_master.society_id = '$society_id' 
                AND hr_document_master.block_id = '$block_id'
                AND hr_document_master.floor_id = '$floor_id'
                AND hr_document_master.user_id = '0'
                AND hr_document_master.hr_doc_deleted = '0' 
                AND hr_document_master.hr_document_status = '0')

                OR (hr_document_category_master.hr_document_category_id = hr_document_master.hr_document_category_id 
                AND hr_document_category_master.society_id = hr_document_master.society_id 
                AND hr_document_master.society_id = '$society_id' 
                AND hr_document_master.block_id = '$block_id'
                AND hr_document_master.floor_id = '$floor_id'
                AND hr_document_master.user_id = '$user_id'
                AND hr_document_master.hr_doc_deleted = '0' 
                AND hr_document_master.hr_document_status = '0')
               
                ","GROUP BY hr_document_master.hr_document_category_id");

            $response["document_categories"] = array();

            $document_categories = array();

            $document_categories["hr_document_category_id"] = "0";
            $document_categories["hr_document_category_name"] = "My ID Proofs";

            array_push($response["document_categories"], $document_categories);

            if(mysqli_num_rows($docCatQry)>0){
                
                while($docCatData = mysqli_fetch_array($docCatQry)) {

                    $document_categories = array();

                    $document_categories["hr_document_category_id"] = $docCatData['hr_document_category_id'];
                    $document_categories["hr_document_category_name"] = $docCatData['hr_document_category_name'];

                    array_push($response["document_categories"], $document_categories);
                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Error";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getHRDocumentList']=="getHRDocumentList" && $user_id!='' && $unit_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true){

            if ($floor_id == null || $floor_id =='' || $floor_id ==' ') {
                $uq = $d->selectRow("floor_id","users_master","user_id = '$user_id'");
                $ud = mysqli_fetch_array($uq);
                $floor_id = $ud['floor_id'];
            }

            if ($block_id == null || $block_id =='' || $block_id ==' ') {
                $uq1 = $d->selectRow("block_id","users_master","user_id = '$user_id'");
                $udata = mysqli_fetch_array($uq1);
                $block_id = $udata['block_id'];
            }

            $response["documents"] = array();            

            if ($hr_document_category_id == 0) {
                $hrDocQry = $d->select("user_id_proofs,id_proof_master","user_id_proofs.user_id = '$user_id' 
                    AND id_proof_master.id_proof_id = user_id_proofs.id_proof_id","");

                if(mysqli_num_rows($hrDocQry)>0){
                
                    while($docData = mysqli_fetch_array($hrDocQry)) {

                        $documents = array();

                        $documents["hr_document_id"] = $docData['user_id_proof_id'];
                        $documents["hr_document_name"] = $docData['id_proof_name'];
                        $documents["document_type"] = "0";
                        if ($docData['id_proof'] != '') {
                            $documents["hr_document_file"] =$base_url."img/id_proof/".  $docData['id_proof'];
                        }else{
                            $documents["hr_document_file"] ='';
                        }
                        if ($docData['id_proof_back'] != '') {
                            $documents["hr_document_file_back"] =$base_url."img/id_proof/".  $docData['id_proof_back'];
                        }else{
                            $documents["hr_document_file_back"] ='';
                        }
                        $documents["hr_document_uploaded_date"] = date("h:i A, dS M Y",strtotime($docData['created_date']));

                        array_push($response["documents"], $documents);
                    }

                    $response["message"]="Success";
                    $response["status"]="200";
                    echo json_encode($response);
                }
            }else{

                $hrDocQry = $d->select("
                    hr_document_category_master,
                    hr_document_master
                    ","
                    (hr_document_category_master.hr_document_category_id = hr_document_master.hr_document_category_id 
                    AND hr_document_category_master.society_id = hr_document_master.society_id 
                    AND hr_document_master.society_id = '$society_id' 
                    AND hr_document_master.block_id = '0'
                    AND hr_document_master.floor_id = '0'
                    AND hr_document_master.user_id = '0'
                    AND hr_document_master.hr_doc_deleted = '0' 
                    AND hr_document_master.hr_document_status = '0'
                    AND hr_document_master.hr_document_category_id = '$hr_document_category_id' )
                    
                    OR (hr_document_category_master.hr_document_category_id = hr_document_master.hr_document_category_id 
                    AND hr_document_category_master.society_id = hr_document_master.society_id 
                    AND hr_document_master.society_id = '$society_id' 
                    AND hr_document_master.block_id = '$block_id'
                    AND hr_document_master.floor_id = '0'
                    AND hr_document_master.user_id = '0'
                    AND hr_document_master.hr_doc_deleted = '0' 
                    AND hr_document_master.hr_document_status = '0'
                    AND hr_document_master.hr_document_category_id = '$hr_document_category_id' )

                    OR (hr_document_category_master.hr_document_category_id = hr_document_master.hr_document_category_id 
                    AND hr_document_category_master.society_id = hr_document_master.society_id 
                    AND hr_document_master.society_id = '$society_id' 
                    AND hr_document_master.block_id = '$block_id'
                    AND hr_document_master.floor_id = '$floor_id'
                    AND hr_document_master.user_id = '0'
                    AND hr_document_master.hr_doc_deleted = '0' 
                    AND hr_document_master.hr_document_status = '0'
                    AND hr_document_master.hr_document_category_id = '$hr_document_category_id' )

                    OR (hr_document_category_master.hr_document_category_id = hr_document_master.hr_document_category_id 
                    AND hr_document_category_master.society_id = hr_document_master.society_id 
                    AND hr_document_master.society_id = '$society_id' 
                    AND hr_document_master.block_id = '$block_id'
                    AND hr_document_master.floor_id = '$floor_id'
                    AND hr_document_master.user_id = '$user_id'
                    AND hr_document_master.hr_doc_deleted = '0' 
                    AND hr_document_master.hr_document_status = '0'
                    AND hr_document_master.hr_document_category_id = '$hr_document_category_id')

                    ","ORDER BY hr_document_master.hr_document_id DESC");

                if(mysqli_num_rows($hrDocQry)>0){
                

                    while($docData = mysqli_fetch_array($hrDocQry)) {

                        $documents = array();

                        $documents["hr_document_id"] = $docData['hr_document_id'];
                        $documents["hr_document_name"] = $docData['hr_document_name'];
                        $documents["document_type"] = $docData['document_type'];
                        if ($docData['document_type'] == 0) {
                            $documents["hr_document_file"] =$base_url."img/hrdoc/".  $docData['hr_document_file'];
                        }else{
                            $documents["hr_document_file"] =$docData['hr_document_file'].'';
                        }
                        $documents["hr_document_file_back"] ='';
                        $documents["hr_document_uploaded_date"] = date("h:i A, dS M Y",strtotime($docData['hr_document_uploaded_date']));

                        array_push($response["documents"], $documents);
                    }

                    $response["message"]="Success";
                    $response["status"]="200";
                    echo json_encode($response);
                }
            }


            if(mysqli_num_rows($hrDocQry)<=0){
                $response["message"]="Failed";
                $response["status"]="200";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

?>