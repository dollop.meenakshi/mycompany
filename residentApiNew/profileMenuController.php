<?php
include_once 'lib.php';


/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb) {

        $response = array();
        extract(array_map("test_input" , $_POST));

        if ($_POST['getProfileMenuDetails'] == "getProfileMenuDetails" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            if (isset($my_profile) && $my_profile == 0) {
                $apndQ = " AND view_in_employee_profile = '0'";
            }else{
                $apndQ = "";
            }
    
            $qA = $d->select("employee_profile_menu_access", "society_id='$society_id' AND profile_menu_status='0' $apndQ","ORDER BY profile_menu_sequence ASC");

            if (mysqli_num_rows($qA) > 0) {

                $response["profileMenu"] = array();

                while($data=mysqli_fetch_array($qA)) {

                    $profileMenu=array();
                    $profileMenu["profile_menu_id"] = $data['profile_menu_id'];
                    $profileMenu["profile_menu_name"] = $data['profile_menu_name'];
                    $profileMenu["menu_click"] = $data['menu_click'];
                    $profileMenu["profile_menu_photo"] = $base_url."img/app_icon/".$data["profile_menu_photo"];
                    $profileMenu["access_type"] = $data['access_type'];

                    if (isset($my_profile) && $my_profile == 0 && $data['language_key_name'] == "my_timeline") {                        
                        $profileMenu["language_key_name"] = "feed";
                    }else{
                        $profileMenu["language_key_name"] = $data['language_key_name'];
                    }
                    
                    array_push($response["profileMenu"], $profileMenu); 

                }

                $currentMonth = date('m');

                $qryPraise = $d->selectRow("employee_praise_type_master.*,employee_praise_master.praised_by_id,employee_praise_master.praised_content,(SELECT COUNT(*) FROM employee_praise_master WHERE employee_praise_master.employee_id = '$user_id' AND employee_praise_master.employee_praise_type_id = employee_praise_type_master.employee_praise_type_id) AS totalPraise","employee_praise_type_master,employee_praise_master","employee_praise_master.society_id = '$society_id' AND employee_praise_master.employee_id = '$user_id' 
                     AND employee_praise_master.employee_praise_type_id = employee_praise_type_master.employee_praise_type_id 
                    ","GROUP BY employee_praise_master.employee_praise_type_id");
                
                $response["praise_list"] = array();            


                if (mysqli_num_rows($qryPraise) > 0) {
                

                    while($dataPraise = mysqli_fetch_array($qryPraise)){

                        $praise = array();

                        $praise['employee_praise_type_id'] = $dataPraise['employee_praise_type_id'];
                        $praise['employee_praise_name'] = $dataPraise['employee_praise_name'];
                        $praise['employee_praise_content'] = $dataPraise['employee_praise_content'];
                        $praise['employee_praise_name_color_code'] = $dataPraise['employee_praise_name_color_code'];
                        $praise['employee_praise_image'] = $base_url.'img/praise_icon/'.$dataPraise['employee_praise_image'];

                        $praise['total'] = $dataPraise['totalPraise'];

                        array_push($response["praise_list"],$praise);
                    }
                }

                $q = $d->selectRow("users_master.*,block_master.*,floors_master.*,unit_master.*,employee_level_master.level_id as lId,employee_level_master.parent_level_id,employee_level_master.level_name","block_master,floors_master,unit_master,users_master LEFT JOIN employee_level_master ON employee_level_master.level_id = users_master.level_id",
                    "users_master.delete_status=0 AND users_master.user_id ='$user_id' AND users_master.block_id=block_master.block_id   AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id");

                $user_data = mysqli_fetch_array($q);

                if ($user_data == TRUE) {

                    $response["user_id"] = $user_data['user_id'];
                    $response["level_id"] = $user_data['lId'].'';
                    $response["level_name"] = $user_data['level_name'].'';
                    $response["parent_level_id"] = $user_data['parent_level_id'].'';                    
                    $response["designation"] = html_entity_decode($user_data['user_designation']);
                    $user_full_name=$user_data['user_full_name'];

                    $response["user_face_data"] = $user_data['user_face_data'].'';
                    if ($user_data['face_added_date'] != '' && $user_data['face_added_date'] != '0000-00-00') {
                        $response["face_added_date"] = date('h:i A, d M Y',strtotime($user_data['face_added_date']));
                    } else {
                        $response["face_added_date"] = "";
                    }


                    if ($user_data['face_data_image'] != '') {
                        $response["face_data_image"] = $base_url . "img/attendance_face_image/" . $user_data['face_data_image'];
                    } else {
                        $response["face_data_image"] = "";
                    }

                    if ($user_data['face_data_image_two'] != '') {
                        $response["face_data_image_two"] = $base_url . "img/attendance_face_image/" . $user_data['face_data_image_two'];
                    } else {
                        $response["face_data_image_two"] = "";
                    }

                    if ($user_data['parent_level_id'] > 0) {
                        $pld = $d->selectRow("user_id,user_full_name,user_profile_pic,level_name,user_designation","users_master,employee_level_master","users_master.level_id = '$user_data[parent_level_id]' AND users_master.level_id = employee_level_master.level_id AND users_master.delete_status = '0'");

                        $pldData = mysqli_fetch_array($pld);

                        if (mysqli_num_rows($pld) > 0) {

                            $response['reporting_person_id'] = $pldData['user_id'];
                            $response['reporting_person'] = $pldData['user_full_name'];
                            $response['reporting_person_level_name'] = $pldData['level_name'];
                            $response['reporting_person_user_designation'] = $pldData['user_designation'];

                            if ($pldData['user_profile_pic'] != '') {
                                $response["reporting_person_profile_pic"] = $base_url . "img/users/recident_profile/" .$pldData['user_profile_pic'];
                            } else {
                                $response["reporting_person_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                            }
                        }else{
                            $response['reporting_person_id'] = "";                        
                            $response['reporting_person'] = "";                        
                            $response['reporting_person_level_name'] = "";                        
                            $response['reporting_person_user_designation'] = "";                        
                            $response['reporting_person_profile_pic'] = "";     
                        }
                    }else{
                        $response['reporting_person_id'] = "";                        
                        $response['reporting_person'] = "";                        
                        $response['reporting_person_level_name'] = "";                        
                        $response['reporting_person_user_designation'] = "";                        
                        $response['reporting_person_profile_pic'] = "";                        
                    }


                    $response["user_full_name"] = $user_full_name.'';


                    if ($user_data['user_profile_pic'] != '') {
                        $response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $user_data['user_profile_pic'];
                    } else {
                        $response["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $response["user_first_name"] = $user_data['user_first_name'];
                    $response["user_last_name"] = $user_data['user_last_name'];

                    if (strlen($user_data['user_mobile']>8) && $user_data['public_mobile']==1  && isset($my_profile) && $my_profile == 0) {
                        $user_mobile = $user_data['country_code'].'-'.substr($user_data['user_mobile'], 0, 3) . '****' . substr($user_data['user_mobile'],  -3);
                        $response["user_mobile"] = $user_mobile;
                        $user_mobile = $user_mobile;
                    } else if (strlen($user_data['user_mobile']>8)) {
                        $response["user_mobile"] = $user_data['country_code'].'-'.$user_data['user_mobile'];
                        $user_mobile = $user_data['country_code'].'-'.$user_data['user_mobile'];
                    } else {
                        $response["user_mobile"]="";
                        $user_mobile="";
                    }

                    if (strlen($user_data['user_email'])>3 && $user_data['public_mobile']==1 && isset($my_profile) && $my_profile == 0) {
                        $user_email_masked = substr($user_data['user_email'], 0, 3) . '******' . substr($user_data['user_email'],  -3);
                        $user_email = $user_email_masked;
                        $response["user_email"] = $user_email;
                    } else if (strlen($user_data['user_email'])>3) {
                        $user_email = $user_data['user_email'];
                        $response["user_email"] = $user_email;
                    } else {
                        $response["user_email"]="";
                        $user_email="";
                    }

                    if (strlen($user_data['personal_email'])>3 && $user_data['public_mobile']==1 && isset($my_profile) && $my_profile == 0) {
                        $personal_email_masked = substr($user_data['personal_email'], 0, 3) . '******' . substr($user_data['personal_email'],  -3);
                        $personal_email = $personal_email_masked;
                        $response["personal_email"] = $personal_email;
                    } else if (strlen($user_data['personal_email'])>3) {
                        $personal_email = $user_data['personal_email'];
                        $response["personal_email"] = $personal_email;
                    } else {
                        $response["personal_email"]="";
                        $personal_email="";
                    }


                    if (strlen($user_data['alt_mobile']>8) && $user_data['public_mobile']==1 &&  isset($my_profile) && $my_profile == 0) {
                        $alt_mobile = $user_data['country_code_alt'].'-'.substr($user_data['alt_mobile'], 0, 3) . '****' . substr($user_data['alt_mobile'],  -3);
                        $response["alt_mobile"] = $alt_mobile;
                        $alt_mobile = $alt_mobile;
                    } else if (strlen($user_data['alt_mobile']>8)) {
                        $response["alt_mobile"] = $user_data['country_code_alt'].'-'.$user_data['alt_mobile'];
                        $alt_mobile = $user_data['country_code_alt'].'-'.$user_data['alt_mobile'];
                    } else {
                        $response["alt_mobile"]="";
                        $alt_mobile="";
                    }

                    if (strlen($user_data['emergency_number']>8) && $user_data['public_mobile']==1  && isset($my_profile) && $my_profile == 0) {
                        $emergency_number = $user_data['country_code_emergency'].'-'.substr($user_data['emergency_number'], 0, 3) . '****' . substr($user_data['emergency_number'],  -3);
                        $response["emergency_number"] = $emergency_number;
                        $emergency_number = $emergency_number;
                    } else if (strlen($user_data['emergency_number']>8)) {
                        $response["emergency_number"] = $user_data['country_code_emergency'].'-'.$user_data['emergency_number'];
                        $emergency_number = $user_data['country_code_emergency'].'-'.$user_data['emergency_number'];
                    } else {
                        $response["emergency_number"]="";
                        $emergency_number="";
                    }


                    if (strlen($user_data['whatsapp_number']>8) && $user_data['public_mobile']==1  && isset($my_profile) && $my_profile == 0) {
                        $whatsapp_number = $user_data['country_code_whatsapp'].'-'.substr($user_data['whatsapp_number'], 0, 3) . '****' . substr($user_data['whatsapp_number'],  -3);
                        $response["whatsapp_number"] = $whatsapp_number;
                        $whatsapp_number = $whatsapp_number;
                    } else if (strlen($user_data['whatsapp_number']>8)) {
                        $response["whatsapp_number"] = $user_data['country_code_whatsapp'].'-'.$user_data['whatsapp_number'];
                        $whatsapp_number = $user_data['country_code_whatsapp'].'-'.$user_data['whatsapp_number'];
                    } else {
                        $response["whatsapp_number"]="";
                        $whatsapp_number="";
                    }


                    $qA = $d->select("user_employment_details", "user_id='$user_id' AND society_id='$society_id' AND unit_id='$unit_id'", "");
                    $empData = mysqli_fetch_array($qA);

                    $response["designation"] = html_entity_decode($user_data['user_designation']);
                    $response["employee_id"] ="";

                    if ($user_data['member_date_of_birth'] == "0000-00-00" || $user_data['member_date_of_birth'] == '') {
                        $response["member_date_of_birth"] = '';

                    }else{
                        $response["member_date_of_birth"] = $user_data['member_date_of_birth'].'';
                    }
                    $response["blood_group"] = $user_data['blood_group'];
                    $response["gender"] = $user_data['gender'];
                    $response["marital_status"] = (!empty($user_data['marital_status'])) ? $user_data['marital_status'] : "";
                    $response["total_family_members"] = (!empty($user_data['total_family_members'])) ? $user_data['total_family_members'] : "";
                    $response["nationality"] = (!empty($user_data['nationality'])) ? $user_data['nationality'] : "";
                    
                    if ($user_data['public_mobile']==0) {
                        $response["public_mobile"] = true;
                        $public_mobile = true;
                    } else {
                        $response["public_mobile"] = false;
                        $public_mobile = false;
                    }

                    $marital_status_string = "";
                    if($user_data['marital_status'] == 1 || $user_data['marital_status'] == "1")
                    {
                        $marital_status_string = "Single";
                    }
                    else if($user_data['marital_status'] == 2 || $user_data['marital_status'] == "2")
                    {
                        $marital_status_string = "Married";
                    }
                    else if($user_data['marital_status'] == 3 || $user_data['marital_status'] == "3")
                    {
                        $marital_status_string = "Widowed";
                    }
                    else if($user_data['marital_status'] == 4 || $user_data['marital_status'] == "4")
                    {
                        $marital_status_string = "Seperated";
                    }
                    else if($user_data['marital_status'] == 5 || $user_data['marital_status'] == "5")
                    {
                        $marital_status_string = "Divorced";
                    }

                    $response["personal"] = array();

                    if ($user_data['member_date_of_birth'] == "0000-00-00" || $user_data['member_date_of_birth'] == '') {
                        $member_birthdate_f = "";
                    }else{
                        $member_birthdate_f = date("d-m-Y",strtotime($user_data['member_date_of_birth']));
                    }

                    if ($user_data['member_date_of_birth'] == "0000-00-00" || $user_data['member_date_of_birth'] == '') {
                        $member_birthdate_f_n = "";
                    }else{
                        $member_birthdate_f_n = $user_data['member_date_of_birth'];
                    }


                    if ($user_data['wedding_anniversary_date'] == "0000-00-00" || $user_data['wedding_anniversary_date'] == '' || $user_data['wedding_anniversary_date'] == null) {
                        $wedding_anniversary_date_f = "";
                    }else{
                        $wedding_anniversary_date_f = date("d-m-Y",strtotime($user_data['wedding_anniversary_date']));
                    }

                    if ($user_data['wedding_anniversary_date'] == "0000-00-00" || $user_data['wedding_anniversary_date'] == ''|| $user_data['wedding_anniversary_date'] == null) {
                        $wedding_anniversary_date_f_n = "";
                    }else{
                        $wedding_anniversary_date_f_n = $user_data['wedding_anniversary_date'];
                    }

                    $qe = $d->select("user_employment_details", "user_id='$user_id' AND society_id='$society_id'", "LIMIT 1");
                    $empSkil = mysqli_fetch_array($qe);

                    $response["employeement_skills"] = array();

                    $empe_exp2=$d->select("employee_experience","user_id='$user_id' AND society_id='$society_id' AND delete_status = '0'");

                    $eaec=$d->select("employee_achievements_education","user_id='$user_id' AND society_id='$society_id' AND delete_status = '0'");
                    $expDataar = array();
                    
                    if(mysqli_num_rows($empe_exp2)>0){
                        while($tData=mysqli_fetch_assoc($empe_exp2)){
                            array_push($expDataar,$tData);
                        }
                        
                        array_multisort(array_column($expDataar, 'work_from'), SORT_ASC, $expDataar);
                      
                        $sdate = $expDataar[0]['work_from'];
                        $edate = date('Y-m-d');
                        $getTotalExperience = $d->getMonthDays($sdate,$edate);
                           
                    }

                    if($empSkil['joining_date'] !="" && $empSkil['joining_date'] !="0000-00-00"){

                        $sdate = $empSkil['joining_date'];
                        $edate = $edate = date('Y-m-d');
                        
                        $getExperianseYears = $d->getMonthDays($sdate,$edate);
                    }

                    
                    $intrest_hobbies = html_entity_decode($empSkil['intrest_hobbies']);
                    $professional_skills = html_entity_decode($empSkil['professional_skills']);
                    $special_skills = html_entity_decode($empSkil['special_skills']);
                    $language_known = html_entity_decode($empSkil['language_known']);
           
                    $employeement_skills = array();
                    $employeement_skills["designation"]= html_entity_decode($user_data['user_designation']);
                    $employeement_skills["employee_id"]=(!empty($user_data['company_employee_id'])) ? html_entity_decode($user_data['company_employee_id']) : "";

                    if ($empSkil['employment_type'] == 1) {
                        $employeement_skills["employment_type"] = "Full Time";
                    }else if ($empSkil['employment_type'] == 2) {
                        $employeement_skills["employment_type"] = "Part Time";
                    }else if ($empSkil['employment_type'] == 3) {
                        $employeement_skills["employment_type"] = "Seasonal";
                    }else if ($empSkil['employment_type'] == 4) {
                        $employeement_skills["employment_type"] = "Temporary";
                    }else{
                        $employeement_skills["employment_type"] = "";
                    }

                    $employeement_skills["joining_date"] =($empSkil['joining_date'] == "0000-00-00" || $empSkil['joining_date']=="") ? "" : date('Y-m-d', strtotime('-1 days',strtotime($empSkil['joining_date'])));
                    $employeement_skills["joining_date_view"] =($empSkil['joining_date'] == "0000-00-00" || $empSkil['joining_date']=="") ? "" : date("d/m/Y",strtotime($empSkil['joining_date']));
                    $employeement_skills["company_experience"] = "$getExperianseYears";
                    $employeement_skills["total_experience"] = "$getTotalExperience";

                    array_push($response["employeement_skills"], $employeement_skills);

                    $personal = array(                      
                        "member_date_of_birth"=>$member_birthdate_f_n.'', 
                        "wedding_anniversary_date"=>$wedding_anniversary_date_f_n.'', 
                        "blood_group"=>$user_data['blood_group'], 
                        "gender"=>$user_data['gender'], 
                        "marital_status"=>(!empty($user_data['marital_status'])) ? $user_data['marital_status'] : "", 
                        "marital_status_st"=>$marital_status_string, 
                        "total_family_members"=>(!empty($user_data['total_family_members'])) ? $user_data['total_family_members'] : "",
                        "nationality"=>(!empty($user_data['nationality'])) ? html_entity_decode($user_data['nationality']) : "",
                        "member_date_of_birth_view"=>$member_birthdate_f,
                        "wedding_anniversary_date_view"=>$wedding_anniversary_date_f,
                        "intrest_hobbies" => $intrest_hobbies,
                        "professional_skills" => $professional_skills,
                        "special_skills" => $special_skills,
                        "language_known" => $language_known,
                    );

                    array_push($response["personal"], $personal);


                    $response["contact"] = array();
                    $contact = array(
                        "user_mobile"=>$user_mobile,
                        "alt_mobile"=>$alt_mobile, 
                        "whatsapp_number"=>$whatsapp_number, 
                        "emergency_number"=>$emergency_number, 
                        "user_email"=>$user_email, 
                        "personal_email"=>$personal_email, 
                        "current_address"=>html_entity_decode($user_data['last_address']),
                        "permanent_address"=>html_entity_decode($user_data['permanent_address']),
                        "public_mobile"=>$public_mobile,
                        "user_mobile_country_code"=>(!empty($user_data['country_code'])) ? $user_data['country_code'] : "",
                        "without_country_code_user_mobile"=>(!empty($user_data['user_mobile'])) ? $user_data['user_mobile'] : "",
                        "alt_mobile_country_code"=>(!empty($user_data['country_code_alt'])) ? $user_data['country_code_alt'] : "", 
                        "without_country_code_alt_mobile"=>(!empty($user_data['alt_mobile'])) ? $user_data['alt_mobile'] : "",
                        "whatsapp_number_country_code"=>(!empty($user_data['country_code_whatsapp'])) ? $user_data['country_code_whatsapp'] : "", 
                        "without_country_code_whatsapp_number"=>(!empty($user_data['whatsapp_number'])) ? $user_data['whatsapp_number'] : "",
                        "emergency_number_country_code"=>(!empty($user_data['country_code_emergency'])) ? $user_data['country_code_emergency'] : "", 
                        "without_country_code_emergency_number"=>(!empty($user_data['emergency_number'])) ? $user_data['emergency_number'] : ""
                    );
                    
                    array_push($response["contact"], $contact);

                    $response["experience"] = array();        
                    $response["education"] = array();

                    $response["social_link"] = array();
                    $social_link = array(
                        "facebook"=>$user_data['facebook'], 
                        "instagram"=>$user_data['instagram'], 
                        "linkedin"=>$user_data['linkedin'], 
                        "twitter"=>$user_data['twitter'], 
                        "whatsapp_number"=>$whatsapp_number,
                        "whatsapp_number_country_code"=>(!empty($user_data['country_code_whatsapp'])) ? $user_data['country_code_whatsapp'] : "",
                        "whatsapp_number_without_country_code"=>(!empty($user_data['whatsapp_number'])) ? $user_data['whatsapp_number'] : ""
                    );
                    
                    array_push($response["social_link"], $social_link);

                    $level_id= $user_data['lId'];

                    $teamQry = $d->selectRow("
                        employee_level_master.level_id,
                        employee_level_master.parent_level_id,
                        employee_level_master.level_name,
                        users_master.user_id,
                        users_master.user_full_name,
                        users_master.user_designation,
                        users_master.user_profile_pic
                        ","
                        users_master,
                        employee_level_master
                        ","
                        users_master.level_id = employee_level_master.level_id 
                        AND users_master.delete_status = '0'
                        AND employee_level_master.parent_level_id = '$level_id'
                        AND employee_level_master.parent_level_id != ''");


                    $response['my_team'] = array();

                    $userIDArray = array();

                    if (mysqli_num_rows($teamQry) > 0) {
                        while($teamData = mysqli_fetch_array($teamQry)){
                            $team = array();

                            $team_member_id = $teamData['user_id'];

                            array_push($userIDArray,$team_member_id);

                            $team['user_id'] = $team_member_id;
                            $team['level_id'] = $teamData['level_id'];
                            $team['level_name'] = $teamData['level_name'];
                            $team['user_full_name'] = $teamData['user_full_name'];
                            $team['user_designation'] = $teamData['user_designation'];
                            if ($teamData['user_profile_pic'] != '') {                        
                                $team['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $teamData['user_profile_pic'];
                            }else{
                                $team['user_profile_pic'] = "";
                            }

                            array_push($response['my_team'],$team);
                        }               
                    }
                }
                  
                $response["message"] = "Get Profile Menu success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Not Found";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['contactDetailsRequest'] == "contactDetailsRequest" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            $m->set_data('contact_details_change_req', html_entity_decode($contact_details_change_req));
            
            $ary = array(
                "contact_details_change_req"=>$m->get_data('contact_details_change_req'),
                "contact_details_change_req_date"=>date('Y-m-d H:i:s'),
            );

            $qry = $d->update("users_master",$ary,"user_id = '$user_id'");

            if ($qry == true) {
                $response["message"]="Contact details change request has been sent to admin.";
                $response["status"]="200";
                echo json_encode($response); 
            }else{
                $response["message"]="Something went wrong!";
                $response["status"]="201";
                echo json_encode($response);    
            }
        }else if ($_POST['profileInfoRequest'] == "profileInfoRequest" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            $m->set_data('personal_info_change_req', html_entity_decode($personal_info_change_req));
            
            $ary = array(
                "personal_info_change_req"=>$m->get_data('personal_info_change_req'),
                "personal_info_req_date"=>date('Y-m-d H:i:s'),
            );

            $qry = $d->update("users_master",$ary,"user_id = '$user_id'");

            if ($qry == true) {
                $response["message"]="Contact details change request has been sent to admin.";
                $response["status"]="200";
                echo json_encode($response); 
            }else{
                $response["message"]="Something went wrong!";
                $response["status"]="201";
                echo json_encode($response);    
            }
        }else if ($_POST['getExperience'] == "getExperience" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            $response["experience"] = array();
                   
            $qep= $d->select("employee_experience", "user_id='$user_id' AND delete_status = '0'", "");

            if (mysqli_num_rows($qep) > 0) {            
            
                while ($empExp = mysqli_fetch_array($qep)) {
                    $experience = array();
                    $experience["employee_experience_id"] = $empExp['employee_experience_id'];
                    $experience["designation"] = html_entity_decode($empExp['designation']);
                    $experience["work_from"] =  date('M Y', strtotime($empExp['work_from'])) .' - '. date('M Y', strtotime($empExp['work_to']));
                    $experience["company_location"] = html_entity_decode($empExp['company_location']);
                    $experience["exp_company_name"] = html_entity_decode($empExp['exp_company_name']);
                    
                    array_push($response["experience"], $experience);
                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response); 
            }else{
                $response["message"]="Something went wrong!";
                $response["status"]="201";
                echo json_encode($response);    
            }
        }else {
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);                  
        }
    } else {
        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
} 
 


