<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        if($_POST['getHolidays']=="getHolidays" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $year = date('Y');

            $holidayQry = $d->select("holiday_master","society_id='$society_id' 
                AND holiday_status='0' AND DATE_FORMAT(holiday_start_date,'%Y') = '$year' AND holiday_group_id = '0'","ORDER BY holiday_start_date ASC" );

            if(mysqli_num_rows($holidayQry)>0){
                
                $response["holiday"] = array();

                while($data=mysqli_fetch_array($holidayQry)) {

                    $holiday = array(); 

                    $holiday["holiday_id"] = $data['holiday_id'];
                    $holiday["society_id"] = $data['society_id'];
                    $holiday["holiday_name"] = $data['holiday_name'];
                    $holiday["holiday_description"] = $data['holiday_description'];
                    $holiday["holiday_start_date"] = $data['holiday_start_date'];
                    $holiday["holiday_day"] = date("l",strtotime($data["holiday_start_date"]));
                    $holiday["holiday_month"] = date("m",strtotime($data["holiday_start_date"]));
                    $holiday["holiday_end_date"] = $data['holiday_end_date'];

                    $holiday["month_view_start"] = date("M",strtotime($data["holiday_start_date"]));
                    $holiday["month_start"] = date("F",strtotime($data["holiday_start_date"]));
                    $holiday["month_view_end"] = date("M",strtotime($data["holiday_end_date"]));
                    $holiday["month_end"] = date("F",strtotime($data["holiday_end_date"]));
                    $holiday["day_view_start"] = date("d",strtotime($data["holiday_start_date"]));
                    $holiday["day_view_end"] = date("d",strtotime($data["holiday_end_date"]));
                    
                    if ($data['holiday_start_date'] < date('Y-m-d')) {
                        $holiday["is_gone"] = true; 
                    }else{
                        $holiday["is_gone"] = false; 
                    }

                    array_push($response["holiday"], $holiday);
                }

                $response["message"] = "Success";
                $response["current_month"] = date('m');
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['getHolidaysNew']=="getHolidaysNew" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $response['holiday'] =array();

            if ($year == '' || $year == null) {
                $year = date('Y');
            }


            $holidayQry = $d->select("holiday_master","society_id='$society_id' 
                AND holiday_status='0' AND DATE_FORMAT(holiday_start_date,'%Y') = '$year' AND holiday_group_id = '0'","ORDER BY holiday_start_date ASC" );
        
            $holidayArray = array();

            if(mysqli_num_rows($holidayQry)>0){
                while($data=mysqli_fetch_array($holidayQry)) {
                    array_push($holidayArray, $data);
                }
            }

            $holidayReasonalQry = $d->select("holiday_group_assign_master,holiday_master","((FIND_IN_SET('$block_id',holiday_group_assign_master.common_assign_id) AND holiday_group_assign_master.common_assign_type = '0') OR (FIND_IN_SET('$floor_id',holiday_group_assign_master.common_assign_id) AND holiday_group_assign_master.common_assign_type = '1') OR (FIND_IN_SET('$level_id',holiday_group_assign_master.common_assign_id) AND holiday_group_assign_master.common_assign_type = '2') OR (FIND_IN_SET('$user_id',holiday_group_assign_master.common_assign_id) AND holiday_group_assign_master.common_assign_type = '3')) AND holiday_master.holiday_group_id = holiday_group_assign_master.holiday_group_id AND holiday_master.society_id='$society_id' 
                AND holiday_master.holiday_status='0' AND DATE_FORMAT(holiday_master.holiday_start_date,'%Y') = '$year'" );
           

            $holidayReasonalArray = array();
            
            if(mysqli_num_rows($holidayReasonalQry)>0){
                while($data=mysqli_fetch_array($holidayReasonalQry)) {
                    array_push($holidayReasonalArray, $data);
                } 
            }

            $newArray = array_merge($holidayArray,$holidayReasonalArray);
            foreach ($newArray as $key => $part) {
                $sort[$key] = strtotime($part['holiday_start_date']);
            }

            array_multisort($sort, SORT_ASC, $newArray);

            for ($i=0; $i <count($newArray) ; $i++) { 
                $holiday = array(); 

                $holiday["holiday_id"] = $newArray[$i]['holiday_id'];
                $holiday["society_id"] = $newArray[$i]['society_id'];
                $holiday["holiday_name"] = $newArray[$i]['holiday_name'];
                $holiday["holiday_description"] = $newArray[$i]['holiday_description'];
                $holiday["holiday_start_date"] = $newArray[$i]['holiday_start_date'];
                $holiday["holiday_month"] = date("m",strtotime($newArray[$i]["holiday_start_date"]));
                $holiday["holiday_day"] = date("l",strtotime($newArray[$i]["holiday_start_date"]));
                $holiday["holiday_end_date"] = $newArray[$i]['holiday_end_date'];

                $holiday["month_view_start"] = date("M",strtotime($newArray[$i]["holiday_start_date"]));
                $holiday["month_start"] = date("F",strtotime($newArray[$i]["holiday_start_date"]));
                $holiday["month_view_end"] = date("M",strtotime($newArray[$i]["holiday_end_date"]));
                $holiday["month_end"] = date("F",strtotime($newArray[$i]["holiday_end_date"]));
                $holiday["day_view_start"] = date("d",strtotime($newArray[$i]["holiday_start_date"]));
                $holiday["day_view_end"] = date("d",strtotime($newArray[$i]["holiday_end_date"]));
                
                if ($newArray[$i]['holiday_start_date'] < date('Y-m-d')) {
                    $holiday["is_gone"] = true; 
                }else{
                    $holiday["is_gone"] = false; 
                }

                array_push($response['holiday'],$holiday);

            }


            $response["message"] = "Success";
            $response["current_month"] = date('m');
            $response["current_year"] = $year.'';
            $response["status"] = "200";
            echo json_encode($response);        
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

?>