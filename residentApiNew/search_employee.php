    <?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb ) { 
        $response = array();
        extract(array_map("test_input" , $_POST));
     
        if($_POST['searchEmployee']=="searchEmployee" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            if ($search_keyword != '') {
                $unit_data=$d->select("
                    block_master,
                    floors_master,
                    users_master
                    ","
                    users_master.floor_id = floors_master.floor_id 
                    AND users_master.block_id=block_master.block_id 
                    AND block_master.block_id = floors_master.block_id 
                    AND (users_master.user_full_name LIKE '%$search_keyword%' 
                        OR users_master.user_first_name LIKE '%$search_keyword%' 
                        OR users_master.user_last_name LIKE '%$search_keyword%' 
                        OR users_master.user_mobile LIKE '%$search_keyword%' 
                        OR users_master.user_designation LIKE '%$search_keyword%') 
                    AND users_master.delete_status=0 
                    AND users_master.society_id ='$society_id' 
                    AND users_master.user_status=1");
            }

            if (mysqli_num_rows($unit_data) > 0) {

                $response["employees"] = array();

                while($data_units_list=mysqli_fetch_array($unit_data)) {
                                 
                    $employees = array(); 
         
                    $user_full_name= $data_units_list['user_full_name'];

                    $units_id=$data_units_list['unit_id'];

                    $employees["user_id"]=$data_units_list['user_id'].'';
                    $employees["unit_id"]=$data_units_list['unit_id'].'';
                    $employees["block_id"]=$data_units_list['block_id'].'';
                    $employees["floor_id"]=$data_units_list['floor_id'].'';
                    $employees["block_name"]=$data_units_list['block_name'].'';
                    $employees["floor_name"]=$data_units_list['floor_name'].'';
                    $employees["user_first_name"]=$data_units_list['user_first_name'].'';
                    $employees["user_last_name"]=$data_units_list['user_last_name'].'';
                    $employees["user_mobile"]=$data_units_list['country_code'].' '.$data_units_list['user_mobile'].'';
                    $employees["user_full_name"]=html_entity_decode($user_full_name).'';
                    $employees["society_id"]=$data_units_list['society_id'];
                    $employees["designation"]=$data_units_list['user_designation'];
                    
                    if ($data_units_list['user_profile_pic'] != '') {
                        $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                    } else {
                        $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    array_push($response["employees"], $employees);
                }

                $response["message"]="Employee list";
                $response["status"]="200";
                echo json_encode($response);
        
            }else{
                $response["message"]="No Employees Found";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
?>