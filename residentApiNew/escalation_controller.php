<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        if($_POST['checkAccessEscalation']=="checkAccessEscalation" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $escalation_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['getEscalationList']=="getEscalationList" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $escaltionQry = $d->selectRow("em.*,um.user_id,um.user_profile_pic,um.user_full_name,um.user_designation,fm.floor_name,bm.block_name","floors_master AS fm,block_master AS bm,escalation_master AS em LEFT JOIN users_master AS um ON um.user_id = em.escalation_to_id","em.society_id= '$society_id' AND em.escalation_by_id = '$user_id' AND em.escalation_active_status='0' AND escalation_is_delete = '0' AND um.floor_id = fm.floor_id AND um.block_id = bm.block_id","ORDER BY em.escalation_id DESC");

            if(mysqli_num_rows($escaltionQry)>0){
                
                $response["escalations"] = array();

                while($data=mysqli_fetch_array($escaltionQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $escalations = array(); 

                    $escalations["escalation_id"] = $data['escalation_id'];
                    $escalations["escalation_to_id"] = $data['escalation_by_id'];
                    $escalations["escalation_to_user_full_name"] = $data['user_full_name'];
                    $escalations["user_designation"] = $data['user_designation'];
                    $escalations["floor_name"] = $data['floor_name'];
                    $escalations["branch_name"] = $data['block_name'];
                    if ($data['user_profile_pic'] != '') {
                        $escalations["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $escalations["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }
                    if ($data['user_profile_pic'] != '') {
                        $escalations["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $escalations["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $escalations["escalation_title"] = $data['escalation_title'];
                    $escalations["escalation_description"] = $data['escalation_description'];

                    if ($data['escalation_file'] != '') {
                        $escalations["escalation_file"] = $base_url.'img/escalation/'.$data['escalation_file'].'';
                    }else{
                        $escalations["escalation_file"] = "";
                    }
                    $escalations["escalation_replay"] = $data['escalation_replay'].'';
                    $escalations["escalation_created_date"] = date('D, d M Y',strtotime($data['escalation_created_date']));
                    $escalations["escalation_created_time"] = date('h:i A',strtotime($data['escalation_created_date']));

                    if($data['escalation_replay_date']!=null){                        
                        $escalations["escalation_replay_date"] = date('D, d M Y',strtotime($data['escalation_replay_date']));
                         $escalations["escalation_replay_time"] = date('h:i A',strtotime($data['escalation_replay_date']));
                    }else{
                        $escalations["escalation_replay_date"] = "";
                        $escalations["escalation_replay_time"] = "";
                    }

                    $escalations["escalation_active_status"] = $data['escalation_active_status'];

                    array_push($response["escalations"], $escalations);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['getEscalationUsers']=="getEscalationUsers" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $escalation_access;

            include 'check_access_data_user.php';

            $userQry = $d->selectRow("user_id,user_full_name","users_master","user_id IN ('$userFcmIds') AND delete_status = '0' AND active_status = '0'");

            if(mysqli_num_rows($userQry)>0){

                $response["escalation_users"] = array();
                
                while($data=mysqli_fetch_array($userQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $escalations = array(); 

                    $escalations["user_id"] = $data['user_id'];
                    $escalations["user_full_name"] = $data['user_full_name'];

                    array_push($response["escalation_users"], $escalations);
                }
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found.";
                $response["status"] = "201";
                echo json_encode($response);
            }    
        }else if($_POST['getEscalationListForAdmin']=="getEscalationListForAdmin" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $escalation_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND escalation_master.escalation_by_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND escalation_master.escalation_by_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $escaltionQry = $d->selectRow("escalation_master.*,users_master.user_id,users_master.user_profile_pic,users_master.user_full_name,users_master.user_designation,floors_master.floor_name,block_master.block_name","escalation_master,users_master,floors_master,block_master","escalation_master.society_id= '$society_id' AND escalation_master.escalation_to_id = '$user_id' AND escalation_master.escalation_active_status='0' AND users_master.user_id = escalation_master.escalation_by_id AND users_master.floor_id = floors_master.floor_id AND users_master.block_id = block_master.block_id $appendQuery","ORDER BY escalation_master.escalation_id DESC");

            if(mysqli_num_rows($escaltionQry)>0){
                
                $response["escalations"] = array();

                while($data=mysqli_fetch_array($escaltionQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $escalations = array(); 

                    $escalations["escalation_id"] = $data['escalation_id'];
                    $escalations["escalation_by_id"] = $data['escalation_by_id'];
                    $escalations["user_full_name"] = $data['user_full_name'];
                    $escalations["user_designation"] = $data['user_designation'];
                    $escalations["floor_name"] = $data['floor_name'];
                    $escalations["branch_name"] = $data['block_name'];
                    if ($data['user_profile_pic'] != '') {
                        $escalations["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $escalations["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }
                    $escalations["escalation_to_id"] = $data['escalation_to_id'];
                    $escalations["escalation_title"] = $data['escalation_title'];
                    $escalations["escalation_description"] = $data['escalation_description'];

                    if ($data['escalation_file'] != '') {
                        $escalations["escalation_file"] = $base_url.'img/escalation/'.$data['escalation_file'].'';
                    }else{
                        $escalations["escalation_file"] = "";
                    }
                    $escalations["escalation_replay"] = $data['escalation_replay'].'';
                    $escalations["escalation_created_date"] = date('D, d M Y',strtotime($data['escalation_created_date']));
                    $escalations["escalation_created_time"] = date('h:i A',strtotime($data['escalation_created_date']));

                    if($data['escalation_replay_date']!=null){                        
                        $escalations["escalation_replay_date"] = date('D, d M Y',strtotime($data['escalation_replay_date']));                       
                        $escalations["escalation_replay_time"] = date('h:i A',strtotime($data['escalation_replay_date']));
                    }else{
                        $escalations["escalation_replay_date"] = "";
                        $escalations["escalation_replay_time"] = "";
                    }

                    $escalations["escalation_active_status"] = $data['escalation_active_status'];

                    array_push($response["escalations"], $escalations);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['addEscalation']=="addEscalation" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $escalation_file = "";

            if ($_FILES["escalation_file"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["escalation_file"]["tmp_name"];
                $extId = pathinfo($_FILES['escalation_file']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","png","jpg","jpeg","PDF","JPEG","PNG","JPG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["escalation_file"]["name"]);
                    $escalation_file = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["escalation_file"]["tmp_name"], "../img/escalation/" . $escalation_file);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }
 
            $m->set_data('escalation_id',$escalation_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('escalation_by_id',$user_id);
            $m->set_data('escalation_to_id',$escalation_to_id);
            $m->set_data('escalation_title',$escalation_title);
            $m->set_data('escalation_description',$escalation_description);
            $m->set_data('escalation_file',$escalation_file);
            $m->set_data('escalation_created_date',$dateTime);

            $a = array(
                'escalation_id'=>$m->get_data('escalation_id'),
                'society_id' =>$m->get_data('society_id'),
                'escalation_by_id'=>$m->get_data('escalation_by_id'),
                'escalation_to_id'=>$m->get_data('escalation_to_id'),
                'escalation_title'=>$m->get_data('escalation_title'),
                'escalation_description'=>$m->get_data('escalation_description'),
                'escalation_file'=>$m->get_data('escalation_file'),
                'escalation_created_date'=>$m->get_data('escalation_created_date'),
            );

            $applyLeaveQry = $d->insert("escalation_master",$a);
            
            if ($applyLeaveQry == true) {

                // To App Access

                $title = "New Escalation Request";
                $description = "Requested by, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$escalation_to_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("escalation_admin","",$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("escalation_admin","",$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotificationWithId($society_id,$title,$description,"escalation_admin","escalation.png","user_id = '$escalation_to_id'",$escalation_to_id);

                // To Admin App

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("15",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("15",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"escalation_admin");
                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"escalation_admin");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"escalations",
                  'admin_click_action '=>"escalations",
                  'notification_logo'=>'escalation.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Escalation request sent";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again later";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['addEscalationReply']=="addEscalationReply" && $escalation_id!=''   && $escalation_by_id!=''  && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
 
            $m->set_data('escalation_replay',$escalation_replay);
            $m->set_data('escalation_replay_date',$dateTime);

            $a = array(
                'escalation_replay'=>$m->get_data('escalation_replay'),
                'escalation_replay_date'=>$m->get_data('escalation_replay_date'),
            );

            $qry = $d->update("escalation_master",$a,"escalation_id = '$escalation_id'");
            
            if ($qry == true) {

                // To App Access

                $title = "Escalation";
                $description = $user_name." replied on your escalation";

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$escalation_by_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("escalation_reply","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("escalation_reply","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"escalation_reply","escalation.png","user_id = '$escalation_by_id'");

                $response["message"] = "Replied";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again later";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>