<?php
session_start();
error_reporting(0);
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../apAdmin/lib/dao.php';
include_once '../apAdmin/lib/sms_api.php';
include_once '../apAdmin/lib/model.php';
include_once '../apAdmin/fcm_file/admin_fcm.php';
include_once '../apAdmin/fcm_file/gaurd_fcm.php';
include_once '../apAdmin/fcm_file/resident_fcm.php';
include_once '../apAdmin/fcm_file/resident_fcm_topic.php';

$d = new dao();
$m = new model();
$smsObj = new sms_api();

$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nResidentTopic = new firebase_resident_topic();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$default_time_zone=$d->getTimezone($_REQUEST['society_id']);


$leave_access = $d->getAccessLeave();
$expense_access = $d->getAccessExpense();
$punch_in_req_access = $d->getAccessPunchInReq();
$punch_out_missing_req_access = $d->getAccessPunchOutMissingReq();
$escalation_access = $d->getAccessEscalation();
$idea_approve_access = $d->getAccessIdeaApproval();
$pending_attendance_access = $d->getAccessPendingAttendance();
$approve_employee_access = $d->getAccessApproveEmployee();
$add_employee_access = $d->getAccessAddEmployee();
$view_absent_present_access = $d->getAccessViewAbsentPresent();
$wfh_req_access = $d->getAccessWFHApproval();
$view_employee_attendance_access = $d->getAccessViewAttendance();
$assign_task_access = $d->getAccessAssignTask();
$mobile_device_bind_Access = $d->getAccessMobileDeviceBind();
$location_tracking_Access = $d->getAccessLocationTracking();
$birthday_change_request_Access = $d->getAccessBirthdayRequest();
$work_report_Access = $d->getAccessWorkReport();


date_default_timezone_set($default_time_zone);
header('Access-Control-Allow-Origin: *');  //I have also tried the * wildcard and get the same response
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('content-type: application/json; charset=utf-8');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
$base_url=$m->base_url();
$keydb = $m->api_key();
if(is_array($headers)) {
	extract(array_map("test_input", $headers));
}
$key = $_SERVER['HTTP_KEY'];  
$auth_user_name= $_SERVER['PHP_AUTH_USER'];
$auth_password= $_SERVER['PHP_AUTH_PW'];
$auth_check = $d->check_auth($auth_user_name,$auth_password);

$language_id = $_POST['language_id'];
if (file_exists("../img/$language_id.xml") && $language_id!="") {
	$xml=simplexml_load_file("../img/$language_id.xml");
// echo $xml->string->app_name;
} else if ($language_id!="") {
	$society_id = $_REQUEST['society_id'];
	$language_id = $_REQUEST['language_id'];
	$q=$d->select("society_master","society_id='$society_id'");
	$bData=mysqli_fetch_array($q);
	$country_id  = $bData['country_id'];
	$society_id  = $bData['society_id'];
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/language_controller_web.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
		"getLanguageValues=getLanguageValues&country_id=$country_id&society_id=$society_id&language_id=$language_id");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'key: bmsapikey'
	));

	$server_output = curl_exec($ch);

	curl_close ($ch);
	$server_output=json_decode($server_output,true);

	$arrayCount= count($server_output['language_key']);

	$myFile = "../img/$language_id.xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	$rss_txt = "";
	$rss_txt .= '<?xml version="1.0" encoding="utf-8"?>';
	$rss_txt .= "<rss version='2.0'>";

	$rss_txt .= '<string>';
	for ($i1=0; $i1 < $arrayCount ; $i1++) { 
		$key_value = str_replace('&', '&amp;', $server_output['language_key'][$i1]['key_value']);

		$keyName  = $server_output['language_key'][$i1]['key_name'];


		$rss_txt .= "<$keyName>$key_value</$keyName>";
	}
	$rss_txt .= '</string>';
	$rss_txt .= '</rss>';

	fwrite($fh, $rss_txt);
	fclose($fh);
}

if ($_POST['country_id']==101) { 
	$cgstName = $xml->string->cgst;
	$sgstName = $xml->string->sgst;
	$igstName = $xml->string->igst;
} else if ($_POST['country_id']==161) { 
	$igstName = $xml->string->igst;
} 

$noDatafoundMsg = $xml->string->nodata_adapter;
$datafoundMsg = $xml->string->data_found;
$deleteMsg = $xml->string->deleted_successfully;
$uploadMsg = $xml->string->uploaded_succefully;
$addMsg = $xml->string->added_successfully;
$updateMsg = $xml->string->updated_successfully;
$somethingWrong = $xml->string->something_went_wrong;
$statusChanged = $xml->string->status_changed;
$not_available = $xml->string->not_available;


?>