<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");
        $currentDate = date("Y-m-d");

        if($_POST['checkAccess']=="checkAccess" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $expense_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['getExpenseCategory']=="getExpenseCategory" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){            

            $expenseCatQry = $d->select("expense_category_master","society_id = '$society_id'","" );

            if(mysqli_num_rows($expenseCatQry)>0){
                
                $response["expense_category"] = array();

                while($data=mysqli_fetch_array($expenseCatQry)) {

                    $category = array(); 

                    $category["expense_category_id"] = $data['expense_category_id'];
                    $category["expense_category_name"] = $data['expense_category_name'];
                    $category["expense_category_type"] = $data['expense_category_type'];
                    $category["expense_category_unit_name"] = $data['expense_category_unit_name'];
                    $category["expense_category_unit_price"] = $data['expense_category_unit_price'];

                    if ($data['is_attachment_required'] == 1) {
                        $category['is_attachment_required'] = true;
                    }else{
                        $category['is_attachment_required'] = false;                        
                    }

                    array_push($response["expense_category"], $category);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['getExpensesNew']=="getExpensesNew" && $society_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $qryAE = $d->selectRow("user_expenses.*,expense_category_master.*","user_expenses LEFT JOIN expense_category_master ON expense_category_master.expense_category_id = user_expenses.expense_category_id","user_expenses.user_id = '$user_id' AND user_expenses.expense_type = '1' AND user_expenses.society_id = '$society_id'","ORDER BY user_expenses.user_expense_id DESC" );

            if (mysqli_num_rows($qryAE) > 0) {
                $response['has_advance_expense'] = true;
            }else{
                $response['has_advance_expense'] = false;                
            }

            $access_type = $expense_access;

            include "check_access_data.php";

            $response["current_month"] = date('m');

            if ($access_for == 0 || $access_for == 1) {
                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '0' AND user_expenses.user_id = users_master.user_id AND users_master.block_id IN('$accessBranchIds')) AS total_pending,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '1' AND user_expenses.expense_paid_status = '0' AND user_expenses.user_id = users_master.user_id AND users_master.block_id IN('$accessBranchIds')) AS total_approved,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '2' AND user_expenses.user_id = users_master.user_id AND users_master.block_id IN('$accessBranchIds')) AS total_rejected,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '1' AND user_expenses.expense_paid_status = '1' AND user_expenses.user_id = users_master.user_id AND users_master.block_id IN('$accessBranchIds')) AS total_paid,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '1' AND user_expenses.expense_paid_status = '0' AND user_expenses.user_id = users_master.user_id AND users_master.block_id IN('$accessBranchIds')) AS total_unpaid");
                
            }else if($access_for == 2){

                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '0' AND user_expenses.user_id = users_master.user_id AND users_master.floor_id IN('$accessDepIds')) AS total_pending,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '1' AND user_expenses.expense_paid_status = '0'  AND user_expenses.user_id = users_master.user_id AND users_master.floor_id IN('$accessDepIds')) AS total_approved,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '2' AND user_expenses.user_id = users_master.user_id AND users_master.floor_id IN('$accessDepIds')) AS total_rejected,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '1' AND user_expenses.expense_paid_status = '1' AND user_expenses.user_id = users_master.user_id AND users_master.floor_id IN('$accessDepIds')) AS total_paid,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '1' AND user_expenses.expense_paid_status = '0' AND user_expenses.user_id = users_master.user_id AND users_master.floor_id IN('$accessDepIds')) AS total_unpaid");

            }else if($access_for == 3){

                $totalCounts = $d->selectMultipleCount("
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '0' AND user_expenses.user_id = users_master.user_id AND users_master.user_id IN('$accessUserIds')) AS total_pending,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '1' AND user_expenses.expense_paid_status = '0'  AND user_expenses.user_id = users_master.user_id AND users_master.user_id IN('$accessUserIds')) AS total_approved,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '2' AND user_expenses.user_id = users_master.user_id AND users_master.user_id IN('$accessUserIds')) AS total_rejected,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '1' AND user_expenses.expense_paid_status = '1' AND user_expenses.user_id = users_master.user_id AND users_master.user_id IN('$accessUserIds')) AS total_paid,
                    (SELECT COUNT(*) FROM user_expenses,users_master WHERE user_expenses.expense_status = '1' AND user_expenses.expense_paid_status = '0' AND user_expenses.user_id = users_master.user_id AND users_master.user_id IN('$accessUserIds')) AS total_unpaid");
            }

            $countData = mysqli_fetch_array($totalCounts);

            $response['total_pending_expense'] = $countData['total_pending']."";
            $response['total_approved_expense'] = $countData['total_approved']."";
            $response['total_rejected_expense'] = $countData['total_rejected']."";
            $response['total_paid_expense'] = $countData['total_paid']."";
            $response['total_unpaid_expense'] = $countData['total_unpaid']."";

            if ($s_filter != '') {
                if ($s_filter == '0' || $s_filter == '2') {
                    $appendQuery = " AND user_expenses.expense_status = '$s_filter'";
                }else{
                    $appendQuery = " AND user_expenses.expense_status = '$s_filter' AND user_expenses.expense_paid_status = '$expense_paid_status'";
                }
            }

            $expenseTyprQry = $d->selectRow("user_expenses.*,expense_category_master.expense_category_name","user_expenses LEFT JOIN expense_category_master ON expense_category_master.expense_category_id = user_expenses.expense_category_id","user_expenses.user_id = '$user_id' AND user_expenses.society_id = '$society_id' $appendQuery","ORDER BY user_expenses.user_expense_id DESC" );

            $response["current_month"] = date('m');
            
            if(mysqli_num_rows($expenseTyprQry)>0){
                
                $response["expenses"] = array();

                while($data=mysqli_fetch_array($expenseTyprQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $expenses = array(); 

                    $expenses["user_expense_id"] = $data['user_expense_id'];
                    $expenses["expense_category_id"] = $data['expense_category_id'].'';
                    $expenses["expense_category_name"] = $data['expense_category_name'].'';
                    $expenses["expense_category_type"] = $data['expense_category_type'].'';
                    $expenses["expense_category_unit_name"] = $data['expense_category_unit_name'].'';
                    $expenses["expense_category_unit_price"] = $data['expense_category_unit_price'].'';

                    if ($data['expense_category_type'] == 1) {                    
                        $expenses["expense_category_data_view"] = $data['expense_category_unit_price']." X ".$data['unit']." (".
                        $data['expense_category_unit_name'].")";
                    }else{                        
                        $expenses["expense_category_data_view"] = "";
                    }

                    $expenses["expense_title"] = $data['expense_title'];
                    $expenses["description"] = $data['description'];
                    $expenses["amount"] = $data['amount'];
                    $expenses["unit"] = $data['unit'];
                    $expenses["expense_reject_reason"] = $data['expense_reject_reason'];
                    $expenses["expense_document_name"] = "";
                    $expenses['documents'] = array();
                    
                    $jsonArray = json_decode($data['expense_document'],true);

                    if (is_array($jsonArray)) {
                        for ($i=0; $i < count($jsonArray); $i++) {
                            $document = $jsonArray["document_".$i];
                            $docs['name'] = $document;
                            $docs['document'] = $base_url."img/expense_document/" .$document;
                            array_push($expenses['documents'],$docs);
                        }
                    }else if($data['expense_document']!=''){
                        $docs = array();
                        $docs['name'] = $data['expense_document'];
                        $docs['document'] = $base_url."img/expense_document/" .$data['expense_document'];
                        array_push($expenses['documents'],$docs);
                    }
                    
                    $expenses["date_view"] = date('D, dS F Y', strtotime($data['date']));
                    $expenses["date_edit_view"] = date('d-m-Y', strtotime($data['date']));
                    $expenses["date"] = $data['date'];
                    $expenses["year"] = date('Y', strtotime($data['date']));
                    $expenses["month"] = date('m', strtotime($data['date']));
                    $expenses["expense_status"] =$data['expense_status'];

                    if ($data['expense_status'] == 0) {
                        $expenses["expense_status_name"] = "Pending";
                    }else if ($data['expense_status'] == 1) {
                        $expenses["expense_status_name"] = "Approved";
                    }else if ($data['expense_status'] == 2) {
                        $expenses["expense_status_name"] = "Rejected";
                    }

                    $expenses["expense_paid_status"] = $data['expense_paid_status'];

                    if ($data['expense_paid_status'] == 0) {
                        $expenses["is_paid"] = false;
                        $expenses["is_paid_view"] = "Unpaid";
                    }else {
                        $expenses["is_paid"] = true;
                        $expenses["is_paid_view"] = "Paid";
                    }

                    $expenses['approved_by_name'] = "";

                    if ($data["expense_status"] == 1) {
                        if ($data['expense_approved_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_approved_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $expenses['approved_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $expenses['approved_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_approved_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $expenses['approved_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $expenses['approved_by_name'] = "";
                            }
                        }
                    }

                    $expenses['paid_by_name'] = "";

                    if ($data["expense_status"] == 1 && $data['expense_paid_status'] == 1) {
                        if ($data['expense_paid_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_paid_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $expenses['paid_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $expenses['paid_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_paid_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $expenses['paid_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $expenses['paid_by_name'] = "";
                            }
                        }
                    }


                    $expenses['rejected_by_name'] = "";

                    if ($data["expense_status"] == 2) {
                        if ($data['expense_reject_by_type'] == 0) {
                            $rejectedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_reject_by_id]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                            if ($dataRejectedName['user_full_name']!=null) {
                                $expenses['rejected_by_name'] = "By, ".$dataRejectedName['user_full_name']."";
                            }else{
                                $expenses['rejected_by_name'] = "";
                            }

                        }else{
                            $rejectedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_reject_by_id]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                             if ($dataRejectedName['admin_name']!=null) {
                                $expenses['rejected_by_name'] = "By, ".$dataRejectedName['admin_name']."";
                            }else{
                                $expenses['rejected_by_name'] = "";
                            }
                        }
                    }

                    array_push($response["expenses"], $expenses);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['getAdvanceExpenses']=="getAdvanceExpenses" && $society_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){            

            /*if ($s_filter != '') {
                if ($s_filter == '0' || $s_filter == '2') {
                    $appendQuery = " AND user_expenses.expense_status = '$s_filter'";
                }else{
                    $appendQuery = " AND user_expenses.expense_status = '$s_filter' AND user_expenses.expense_paid_status = '$expense_paid_status'";
                }
            }*/

            $appendQuery = "";

            $expenseTyprQry = $d->selectRow("user_expenses.*,expense_category_master.*","user_expenses LEFT JOIN expense_category_master ON expense_category_master.expense_category_id = user_expenses.expense_category_id","user_expenses.user_id = '$user_id' AND user_expenses.expense_type = '1' AND user_expenses.society_id = '$society_id' $appendQuery","ORDER BY user_expenses.user_expense_id DESC" );

            $response["current_month"] = date('m');
            
            if(mysqli_num_rows($expenseTyprQry)>0){
                
                $response["expenses"] = array();

                while($data=mysqli_fetch_array($expenseTyprQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $expenses = array(); 

                    $expenses["user_expense_id"] = $data['user_expense_id'];
                    
                    $expenses["expense_category_id"] = $data['expense_category_id'].'';
                    $expenses["expense_category_name"] = $data['expense_category_name'].'';
                    $expenses["expense_category_type"] = $data['expense_category_type'].'';
                    $expenses["expense_category_unit_name"] = $data['expense_category_unit_name'].'';
                    $expenses["expense_category_unit_price"] = $data['expense_category_unit_price'].'';

                    if ($data['expense_category_type'] == 1) {                    
                        $expenses["expense_category_data_view"] = $data['expense_category_unit_price']." X ".$data['unit']." (".
                        $data['expense_category_unit_name'].")";
                    }else{                        
                        $expenses["expense_category_data_view"] = "";
                    }

                    $expenses["expense_title"] = $data['expense_title'];
                    $expenses["description"] = $data['description'];
                    $expenses["amount"] = $data['amount'];
                    $expenses["unit"] = $data['unit'];
                    $expenses["expense_reject_reason"] = $data['expense_reject_reason'];
                    $expenses["expense_document_name"] = "";
                    $expenses['documents'] = array();
                    
                    $jsonArray = json_decode($data['expense_document'],true);

                    if (is_array($jsonArray)) {
                        for ($i=0; $i < count($jsonArray); $i++) {
                            $document = $jsonArray["document_".$i];
                            $docs['name'] = $document;
                            $docs['document'] = $base_url."img/expense_document/" .$document;
                            array_push($expenses['documents'],$docs);
                        }
                    }else if($data['expense_document']!=''){
                        $docs = array();
                        $docs['name'] = $data['expense_document'];
                        $docs['document'] = $base_url."img/expense_document/" .$data['expense_document'];
                        array_push($expenses['documents'],$docs);
                    }
                    
                    $expenses["date_view"] = date('D, dS F Y', strtotime($data['date']));
                    $expenses["date_edit_view"] = date('d-m-Y', strtotime($data['date']));
                    $expenses["date"] = $data['date'];
                    $expenses["year"] = date('Y', strtotime($data['date']));
                    $expenses["month"] = date('m', strtotime($data['date']));
                    $expenses["expense_status"] =$data['expense_status'];

                    if ($data['expense_status'] == 0) {
                        $expenses["expense_status_name"] = "Pending";
                    }else if ($data['expense_status'] == 1) {
                        $expenses["expense_status_name"] = "Approved";
                    }else if ($data['expense_status'] == 2) {
                        $expenses["expense_status_name"] = "Rejected";
                    }

                    $expenses["expense_paid_status"] = $data['expense_paid_status'];

                    if ($data['expense_paid_status'] == 0) {
                        $expenses["is_paid"] = false;
                        $expenses["is_paid_view"] = "Unpaid";
                    }else {
                        $expenses["is_paid"] = true;
                        $expenses["is_paid_view"] = "Paid";
                    }

                    $expenses['approved_by_name'] = "";

                    if ($data["expense_status"] == 1) {
                        if ($data['expense_approved_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_approved_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $expenses['approved_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $expenses['approved_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_approved_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $expenses['approved_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $expenses['approved_by_name'] = "";
                            }
                        }
                    }

                    $expenses['paid_by_name'] = "";

                    if ($data["expense_status"] == 1 && $data['expense_paid_status'] == 1) {
                        if ($data['expense_paid_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_paid_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $expenses['paid_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $expenses['paid_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_paid_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $expenses['paid_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $expenses['paid_by_name'] = "";
                            }
                        }
                    }

                    $expenses['rejected_by_name'] = "";

                    if ($data["expense_status"] == 2) {
                        if ($data['expense_reject_by_type'] == 0) {
                            $rejectedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_reject_by_id]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                            if ($dataRejectedName['user_full_name']!=null) {
                                $expenses['rejected_by_name'] = "By, ".$dataRejectedName['user_full_name']."";
                            }else{
                                $expenses['rejected_by_name'] = "";
                            }

                        }else{
                            $rejectedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_reject_by_id]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                             if ($dataRejectedName['admin_name']!=null) {
                                $expenses['rejected_by_name'] = "By, ".$dataRejectedName['admin_name']."";
                            }else{
                                $expenses['rejected_by_name'] = "";
                            }
                        }
                    }

                    array_push($response["expenses"], $expenses);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['getPaidExpenses']=="getPaidExpenses" && $society_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){            

            $expenseTyprQry = $d->select("user_expense_history","user_id = '$user_id' AND society_id = '$society_id'","ORDER BY created_at DESC" );

            if(mysqli_num_rows($expenseTyprQry)>0){
                
                $response["paid_expenses"] = array();

                while($data=mysqli_fetch_array($expenseTyprQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $expenses = array(); 

                    $expenses["user_expense_history_id"] = $data['user_expense_history_id'];
                    $expenses["expense_id"] = $data['expense_id'];
                    $expenses["amount"] = $data['amount'].'';
                    $expenses["expense_payment_mode"] = $data['expense_payment_mode'];
                    $expenses["reference_no"] = $data['reference_no'];
                    $expenses["remark"] = $data['remark'];
                    
                    $expenses["created_at"] = date('D, dS F Y', strtotime($data['created_at']));

                    $expenses['paid_by'] = $data['paid_by'];
                    $expenses['paid_by_type'] = $data['paid_by_type'];
                    $expenses['paid_by_name'] = "";

                
                    if ($data['paid_by_type'] == 1) {
                        $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[paid_by]'");
                        $dataApproveName = mysqli_fetch_array($approvedByQry);

                        if ($dataApproveName['user_full_name']!=null) {
                            $expenses['paid_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                        }else{
                            $expenses['paid_by_name'] = "";
                        }
                    }else{
                        $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[paid_by]'");
                        $dataApproveName = mysqli_fetch_array($approvedByQry);

                        if ($dataApproveName['admin_name']!=null) {
                            $expenses['paid_by_name'] = "By, ".$dataApproveName['admin_name']."";
                        }else{
                            $expenses['paid_by_name'] = "";
                        }
                    }

                    array_push($response["paid_expenses"], $expenses);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['getPaidExpenseSubList']=="getPaidExpenseSubList" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $expenseTyprQry = $d->selectRow("user_expenses.*,expense_category_master.expense_category_name","user_expenses LEFT JOIN expense_category_master ON expense_category_master.expense_category_id = user_expenses.expense_category_id","user_expenses.user_expense_id IN ($expense_ids) AND user_expenses.society_id = '$society_id'","ORDER BY user_expenses.user_expense_id DESC" );

            $response["current_month"] = date('m');
            
            if(mysqli_num_rows($expenseTyprQry)>0){
                
                $response["expenses"] = array();

                while($data=mysqli_fetch_array($expenseTyprQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $expenses = array(); 

                    $expenses["user_expense_id"] = $data['user_expense_id'];
                    $expenses["expense_category_id"] = $data['expense_category_id'];
                    $expenses["expense_category_name"] = $data['expense_category_name'].'';
                    $expenses["expense_category_type"] = $data['expense_category_type'];
                    $expenses["expense_category_unit_name"] = $data['expense_category_unit_name'];
                    $expenses["expense_category_unit_price"] = $data['expense_category_unit_price'];
                    $expenses["expense_title"] = $data['expense_title'];
                    $expenses["description"] = $data['description'];
                    $expenses["amount"] = $data['amount'];
                    $expenses["unit"] = $data['unit'];
                    $expenses["expense_reject_reason"] = $data['expense_reject_reason'];
                    $expenses["expense_document_name"] = "";
                    $expenses['documents'] = array();
                    
                    $jsonArray = json_decode($data['expense_document'],true);

                    if (is_array($jsonArray)) {
                        for ($i=0; $i < count($jsonArray); $i++) {
                            $document = $jsonArray["document_".$i];
                            $docs['name'] = $document;
                            $docs['document'] = $base_url."img/expense_document/" .$document;
                            array_push($expenses['documents'],$docs);
                        }
                    }else if($data['expense_document']!=''){
                        $docs = array();
                        $docs['name'] = $data['expense_document'];
                        $docs['document'] = $base_url."img/expense_document/" .$data['expense_document'];
                        array_push($expenses['documents'],$docs);
                    }
                    
                    $expenses["date_view"] = date('D, dS F Y', strtotime($data['date']));
                    $expenses["date_edit_view"] = date('d-m-Y', strtotime($data['date']));
                    $expenses["date"] = $data['date'];
                    $expenses["year"] = date('Y', strtotime($data['date']));
                    $expenses["month"] = date('m', strtotime($data['date']));
                    $expenses["expense_status"] =$data['expense_status'];

                    if ($data['expense_status'] == 0) {
                        $expenses["expense_status_name"] = "Pending";
                    }else if ($data['expense_status'] == 1) {
                        $expenses["expense_status_name"] = "Approved";
                    }else if ($data['expense_status'] == 2) {
                        $expenses["expense_status_name"] = "Rejected";
                    }

                    $expenses["expense_paid_status"] = $data['expense_paid_status'];

                    if ($data['expense_paid_status'] == 0) {
                        $expenses["is_paid"] = false;
                        $expenses["is_paid_view"] = "Unpaid";
                    }else {
                        $expenses["is_paid"] = true;
                        $expenses["is_paid_view"] = "Paid";
                    }

                    $expenses['approved_by_name'] = "";

                    if ($data["expense_status"] == 1) {
                        if ($data['expense_approved_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_approved_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $expenses['approved_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $expenses['approved_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_approved_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $expenses['approved_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $expenses['approved_by_name'] = "";
                            }
                        }
                    }

                    $expenses['paid_by_name'] = "";

                    if ($data["expense_status"] == 1 && $data['expense_paid_status'] == 1) {
                        if ($data['expense_paid_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_paid_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $expenses['paid_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $expenses['paid_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_paid_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $expenses['paid_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $expenses['paid_by_name'] = "";
                            }
                        }
                    }


                    $expenses['rejected_by_name'] = "";

                    if ($data["expense_status"] == 2) {
                        if ($data['expense_reject_by_type'] == 0) {
                            $rejectedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_reject_by_id]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                            if ($dataRejectedName['user_full_name']!=null) {
                                $expenses['rejected_by_name'] = "By, ".$dataRejectedName['user_full_name']."";
                            }else{
                                $expenses['rejected_by_name'] = "";
                            }

                        }else{
                            $rejectedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_reject_by_id]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                             if ($dataRejectedName['admin_name']!=null) {
                                $expenses['rejected_by_name'] = "By, ".$dataRejectedName['admin_name']."";
                            }else{
                                $expenses['rejected_by_name'] = "";
                            }
                        }
                    }

                    array_push($response["expenses"], $expenses);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['getAllExpensesNew']=="getAllExpensesNew" && $society_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $expense_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND user_expenses.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND user_expenses.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $cYear = date('Y');

            if ($paid_unpaid != '') {
                if ($paid_unpaid == "true") {
                    $paid_unpaid = "1";
                }else{
                    $paid_unpaid = "0";
                }
            }

            if ($expense_status == '0' || $expense_status == '2') {
                $appendQuery2 = " AND expense_status = '$expense_status'";
            }else{
                $appendQuery2 = " AND expense_status = '$expense_status' AND expense_paid_status = '$paid_unpaid'";
            }

            if ($paid_unpaid != '') {
                $expenseTyprQry = $d->selectRow("
                    user_expenses.*,
                    users_master.user_full_name,
                    users_master.user_profile_pic,
                    users_master.user_designation,
                    users_master.floor_id,
                    users_master.block_id,
                    floors_master.floor_name,
                    block_master.block_name,
                    expense_category_master.expense_category_name
                    ","
                    users_master,
                    floors_master,
                    block_master,
                    user_expenses LEFT JOIN expense_category_master ON expense_category_master.expense_category_id = user_expenses.expense_category_id
                    ","
                    user_expenses.society_id = users_master.society_id 
                    AND users_master.user_id = user_expenses.user_id 
                    AND users_master.delete_status = 0
                    AND users_master.floor_id = floors_master.floor_id 
                    AND users_master.block_id = block_master.block_id 
                    AND users_master.society_id = user_expenses.society_id 
                    AND user_expenses.society_id = '$society_id' 
                    AND user_expenses.expense_status = '1' 
                    AND user_expenses.expense_paid_status = '$paid_unpaid' $appendQuery
                    ","ORDER BY user_expenses.user_expense_id DESC $LIMIT_DATA");
            }else{
                $expenseTyprQry = $d->selectRow("
                    user_expenses.*,
                    users_master.user_full_name,
                    users_master.user_profile_pic,
                    users_master.user_designation,
                    users_master.floor_id,
                    users_master.block_id,
                    floors_master.floor_name,
                    block_master.block_name,
                    expense_category_master.expense_category_name
                    ","
                    users_master,
                    block_master,
                    floors_master,
                    user_expenses LEFT JOIN expense_category_master ON expense_category_master.expense_category_id = user_expenses.expense_category_id","user_expenses.society_id = users_master.society_id 
                    AND users_master.user_id = user_expenses.user_id 
                    AND users_master.floor_id = floors_master.floor_id 
                    AND users_master.block_id = block_master.block_id 
                    AND users_master.society_id = user_expenses.society_id
                    AND user_expenses.society_id = '$society_id' $appendQuery2 
                    $appendQuery","ORDER BY user_expenses.user_expense_id DESC $LIMIT_DATA");
            }


            if(mysqli_num_rows($expenseTyprQry)>0){
                
                $response["expenses"] = array();   

                $totalAmount = 0; 

                while($data=mysqli_fetch_array($expenseTyprQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $expenses = array(); 

                    $amount = $data['amount'];

                    $expenses["user_expense_id"] = $data['user_expense_id'];
                    
                    $expenses["expense_category_id"] = $data['expense_category_id'].'';
                    $expenses["expense_category_name"] = $data['expense_category_name'].'';
                    $expenses["expense_category_type"] = $data['expense_category_type'].'';
                    $expenses["expense_category_unit_name"] = $data['expense_category_unit_name'].'';
                    $expenses["expense_category_unit_price"] = $data['expense_category_unit_price'].'';

                    if ($data['expense_category_type'] == 1) {                    
                        $expenses["expense_category_data_view"] = $data['expense_category_unit_price']." X ".$data['unit']." (".
                        $data['expense_category_unit_name'].")";
                    }else{                        
                        $expenses["expense_category_data_view"] = "";
                    }

                    $expenses["society_id"] = $data['society_id'];
                    $expenses["floor_id"] = $data['floor_id'];
                    $expenses["user_id"] = $data['user_id'];
                    $expenses["user_full_name"] = $data['user_full_name'];
                    $expenses["user_designation"] = $data['user_designation'];
                    $expenses["floor_name"] = $data['floor_name'];
                    $expenses["block_name"] = $data['block_name'];
                    $expenses["expense_title"] = $data['expense_title'];
                    $expenses["description"] = $data['description'];
                    $expenses["amount"] = $amount;
                    $expenses["expense_reject_reason"] = $data['expense_reject_reason'];
                    $expenses["expense_document_name"] = $data['expense_document'];
                    
                    $expenses['documents'] = array();
                    
                    $jsonArray = json_decode($data['expense_document'],true);

                    if (is_array($jsonArray)) {
                        for ($i=0; $i < count($jsonArray); $i++) {
                            $document = $jsonArray["document_".$i];
                            $docs['name'] = $document;
                            $docs['document'] = $base_url."img/expense_document/" .$document;
                            array_push($expenses['documents'],$docs);
                        }
                    }else if($data['expense_document']!=''){
                        $docs = array();
                        $docs['name'] = $data['expense_document'];
                        $docs['document'] = $base_url."img/expense_document/" .$data['expense_document'];
                        array_push($expenses['documents'],$docs);
                    }

                    if ($data['user_profile_pic'] != '') {
                        $expenses["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $expenses["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $totalAmount = $totalAmount + $amount;


                    $expenses["date_view"] = date('D, dS F Y', strtotime($data['date']));
                    $expenses["date"] = $data['date'];
                    $expenses["year"] = date('Y', strtotime($data['date']));
                    $expenses["month"] = date('m', strtotime($data['date']));
                    $expenses["expense_status"] =$data['expense_status'];

                    if ($data['expense_status'] == 0) {
                        $expenses["expense_status_name"] = "Pending";
                    }else if ($data['expense_status'] == 1) {
                        $expenses["expense_status_name"] = "Approved";
                    }else if ($data['expense_status'] == 2) {
                        $expenses["expense_status_name"] = "Rejected";
                    }

                    $expenses["expense_paid_status"] = $data['expense_paid_status'];

                    if ($data['expense_paid_status'] == 0) {
                        $expenses["is_paid"] = false;
                        $expenses["is_paid_view"] = "Unpaid";
                    }else {
                        $expenses["is_paid"] = true;
                        $expenses["is_paid_view"] = "Paid";
                    }

                    $expenses['approved_by_name'] = "";

                    if ($data["expense_status"] == 1) {
                        if ($data['expense_approved_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_approved_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $expenses['approved_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $expenses['approved_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_approved_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $expenses['approved_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $expenses['approved_by_name'] = "";
                            }
                        }
                    }

                    $expenses['paid_by_name'] = "";

                    if ($data["expense_status"] == 1 && $data['expense_paid_status'] == 1) {
                        if ($data['expense_paid_by_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_paid_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $expenses['paid_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $expenses['paid_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_paid_by_id]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $expenses['paid_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $expenses['paid_by_name'] = "";
                            }
                        }
                    }


                    $expenses['rejected_by_name'] = "";

                    if ($data["expense_status"] == 2) {
                        if ($data['expense_reject_by_type'] == 0) {
                            $rejectedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[expense_reject_by_id]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                            if ($dataRejectedName['user_full_name']!=null) {
                                $expenses['rejected_by_name'] = "By, ".$dataRejectedName['user_full_name']."";
                            }else{
                                $expenses['rejected_by_name'] = "";
                            }

                        }else{
                            $rejectedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[expense_reject_by_id]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                             if ($dataRejectedName['admin_name']!=null) {
                                $expenses['rejected_by_name'] = "By, ".$dataRejectedName['admin_name']."";
                            }else{
                                $expenses['rejected_by_name'] = "";
                            }
                        }
                    }

                    array_push($response["expenses"], $expenses);
                }
                $response["total_amount"] = $totalAmount;
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found.";
                $response["status"] = "201";
                echo json_encode($response);
            }     
        }else if($_POST['changeExpenseStatus']=="changeExpenseStatus" && $user_id!='' && $user_expense_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('expense_status',$expense_status);
            $m->set_data('expense_reject_reason',$expense_reject_reason);
            $m->set_data('modify_date',date('Y-m-d H:i:s'));

            if ($expense_status == 1) {
                $m->set_data('expense_approved_by_id',$my_user_id);
                $m->set_data('expense_reject_by_id',"0");
            }else{
                $m->set_data('expense_approved_by_id',"0");
                $m->set_data('expense_reject_by_id',$my_user_id);
            }

            $a = array(
                'expense_status'=>$m->get_data('expense_status'),
                'expense_reject_reason'=>$m->get_data('expense_reject_reason'),
                'modify_date'=>$m->get_data('modify_date'),
                'expense_approved_by_id'=>$m->get_data('expense_approved_by_id'),
                'expense_reject_by_id'=>$m->get_data('expense_reject_by_id'),
            );

            $changeExpensetatusQry = $d->update("user_expenses",$a,"user_expense_id = '$user_expense_id' AND user_id = '$user_id'");

            if ($changeExpensetatusQry == true) {
             
                if ($expense_status == 1) {
                    $title = "Expense Approved";
                    $msg_view = "Approved";
                }else if($expense_status == 2){
                    $title = "Expense Rejected";
                    $msg_view = "Declined";
                }

                $d->insert_myactivity($user_id, "$society_id", "0", "$user_name", "$title", "my_expense.png");

                $qry = $d->selectRow("expense_title,date,amount,description","user_expenses","user_expense_id = '$user_expense_id' AND user_id = '$user_id'");

                $qryData = mysqli_fetch_array($qry);

                $description ="By, ".$user_name."\nExpense : ".$qryData['expense_title']."\nAmount : ".$qryData['amount']."\nFor Date : ".date('D, dS F Y',strtotime($qryData['date']));

                $response["message"] = $title;
                $response["expense_status_view"] = $msg_view;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "0"; // 1 for all expense, 0 for my expense

                if ($device == 'android') {
                    $nResident->noti("expense_page","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("expense_page","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"expense_page","my_expense.png",$tabPosition,"user_id = '$user_id'");


                $response["expense_status"] = $expense_status;
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['deleteExpenseFile']=="deleteExpenseFile" && $user_id!='' && $user_expense_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('expense_status',$expense_status);
            $m->set_data('expense_reject_reason',$expense_reject_reason);
            $m->set_data('modify_date',date('Y-m-d H:i:s'));

            $a = array(
                'expense_status'=>$m->get_data('expense_status'),
                'expense_reject_reason'=>$m->get_data('expense_reject_reason'),
                'modify_date'=>$m->get_data('modify_date'),
                'expense_approved_by_id'=>$m->get_data('expense_approved_by_id'),
                'expense_reject_by_id'=>$m->get_data('expense_reject_by_id'),
            );

            $changeExpensetatusQry = $d->update("user_expenses",$a,"user_expense_id = '$user_expense_id' AND user_id = '$user_id'");

            if ($changeExpensetatusQry == true) {
             
                $title = "Expense file removed";
                $msg_view = "Approved";
                
                $d->insert_myactivity($user_id, "$society_id", "0", "$user_name", "$title", "my_expense.png");

                $response["expense_status"] = $expense_status;
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['addExpenseNew']=="addExpenseNew" && $user_id!='' && $floor_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $expense_document = "";
            $expense_document_list = array();

            $totalFiles = count($_FILES['expense_document']['tmp_name']);

            for ($i = 0; $i < $totalFiles; $i++) {

                $expenseFile = $_FILES['expense_document']['tmp_name'][$i];

                if(file_exists($expenseFile)) {

                    $file11=$_FILES["expense_document"]["tmp_name"][$i];
                    $extId = pathinfo($_FILES['expense_document']['name'][$i], PATHINFO_EXTENSION);
                    $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF","CSV","csv","xls","xlsx","XLS","XLSX");

                    if(in_array($extId,$extAllow)) {
                        $temp = explode(".", $_FILES["expense_document"]["name"][$i]);
                        $expense_document = "expense_".$user_id.round(microtime(true)).rand(10,10000) . '.' . end($temp);
                        $expense_document_list["document_".$i] = $expense_document;
                        move_uploaded_file($_FILES["expense_document"]["tmp_name"][$i], "../img/expense_document/" . $expense_document);
                    }
                }
            }

            $expense_document = !empty($expense_document_list)?json_encode($expense_document_list):'';

            $m->set_data('expense_category_id',$expense_category_id);
            $m->set_data('expense_category_type',$expense_category_type);
            $m->set_data('expense_category_unit_name',$expense_category_unit_name);
            $m->set_data('expense_category_unit_price',$expense_category_unit_price);
            $m->set_data('society_id',$society_id);
            $m->set_data('floor_id',$floor_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('expense_title',$expense_title);
            $m->set_data('description',$description);
            $m->set_data('expense_document',$expense_document);
            $m->set_data('amount',$amount);
            $m->set_data('unit',$unit);
            $m->set_data('date',$expense_date);
            $m->set_data('created_at',$dateTime);

            $a = array(
                'expense_category_id' =>$m->get_data('expense_category_id'),
                'expense_category_type' =>$m->get_data('expense_category_type'),
                'expense_category_unit_name' =>$m->get_data('expense_category_unit_name'),
                'expense_category_unit_price' =>$m->get_data('expense_category_unit_price'),
                'society_id' =>$m->get_data('society_id'),
                'floor_id'=>$m->get_data('floor_id'),
                'user_id'=>$m->get_data('user_id'),
                'expense_title'=>$m->get_data('expense_title'),
                'description'=>$m->get_data('description'),
                'expense_document'=>$m->get_data('expense_document'),
                'amount'=>$m->get_data('amount'),
                'unit'=>$m->get_data('unit'),
                'date'=>$m->get_data('date'),
                'created_at'=>$m->get_data('created_at'),
            );

            $expenseQry = $d->insert("user_expenses",$a);                
            
            if ($expenseQry == true) {

                // To App Access

                $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Expense Added", "my_expense.png");
 
                $title = "New Expense Added";
                $description = "Expense by, ".$user_name;// ."\n".$amount.' - '.$expense_title;

                $access_type = $expense_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $tabPosition = "1"; // 1 for all expense, 0 for my expense

                    $nResident->noti("expense_page","",$society_id,$fcmArrayPA,$title,$description,$tabPosition);
                    $nResident->noti_ios("expense_page","",$society_id,$fcmArrayIosPA,$title,$description,$tabPosition);

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"expense_page","my_expense.png",$tabPosition,"users_master.user_id IN ('$userFcmIds')");
                }

                // To Admin App

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("15",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("15",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"employeeExpenses");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"employeeExpenses");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"employeeExpenses",
                  'admin_click_action '=>"employeeExpenses",
                  'notification_logo'=>'my_expense.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Expense Added";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['updateExpense']=="updateExpense" && $user_id!='' && filter_var($user_expense_id, FILTER_VALIDATE_INT) == true){

            $qry = $d->select("user_expenses","user_expense_id = '$user_expense_id' AND expense_status = '1'");

            if (mysqli_num_rows($qry) > 0) {
                $response["message"] = "Your expense is already approved and paid, it can not be changed.";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $expense_document = "";

            if ($_FILES["expense_document"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["expense_document"]["tmp_name"];
                $extId = pathinfo($_FILES['expense_document']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["expense_document"]["name"]);
                    $expense_document = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["expense_document"]["tmp_name"], "../img/expense_document/" . $expense_document);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }else{
                $expense_document = $expense_document_name;
            }

            $m->set_data('expense_title',$expense_title);
            $m->set_data('description',$description);
            $m->set_data('expense_document',$expense_document);
            $m->set_data('amount',$amount);
            $m->set_data('unit',$unit);
            $m->set_data('date',$expense_date);
            $m->set_data('modify_date',$dateTime);

            $a = array(
                'expense_title'=>$m->get_data('expense_title'),
                'description'=>$m->get_data('description'),
                'expense_document'=>$m->get_data('expense_document'),
                'unit'=>$m->get_data('unit'),
                'amount'=>$m->get_data('amount'),
                'date'=>$m->get_data('date'),
                'modify_date'=>$m->get_data('modify_date'),
            );

            $expenseQry = $d->update("user_expenses",$a,"user_expense_id = '$user_expense_id'");
                             
            if ($expenseQry == true) {

                // To App Access

                $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Expense Updated", "my_expense.png");

                $title = "Expense Updated";
                $description = "Updated by, ".$user_name;

                $access_type = $expense_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $tabPosition = "1"; // 1 for all expense, 0 for my expense

                    $nResident->noti("expense_page","",$society_id,$fcmArrayPA,$title,$description,$tabPosition);
                    $nResident->noti_ios("expense_page","",$society_id,$fcmArrayIosPA,$title,$description,$tabPosition);

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"expense_page","my_expense.png",$tabPosition,"users_master.user_id IN ('$userFcmIds')");
                }

                // To Admin App

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("15",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("15",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"employeeExpenses");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"employeeExpenses");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"employeeExpenses",
                  'admin_click_action '=>"employeeExpenses",
                  'notification_logo'=>'my_expense.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Expense Updated";                
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['deleteExpense']=="deleteExpense" && $user_id!='' && $user_expense_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $deleteQry = $d->delete("user_expenses","user_expense_id = '$user_expense_id' AND user_id = '$user_id'");

            if ($deleteQry == true) {

                $d->insert_myactivity($user_id, "$society_id", $user_id, "$user_name", "Delete Expense", "my_expense.png");

                $response["message"] = "Expense deleted";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed to delete expense";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

?>