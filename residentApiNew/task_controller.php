<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));

        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");
        $currentDate = date("Y-m-d");

        if($_POST['addMainTask']=="addMainTask" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('society_id',$society_id);
            $m->set_data('task_master_main_name',$task_master_main_name);
            $m->set_data('task_master_main_created_by',$user_id);
            $m->set_data('task_master_main_created_date',$dateTime);

            $a = array(
                'society_id' =>$m->get_data('society_id'),
                'task_master_main_name'=>$m->get_data('task_master_main_name'),
                'task_master_main_created_by'=>$m->get_data('task_master_main_created_by'),
                'task_master_main_created_date'=>$m->get_data('task_master_main_created_date'),
            );

            if ($task_master_main_id != "0") {

                $a1 = array(
                    'task_master_main_name'=>$m->get_data('task_master_main_name'),
                );

                $taskMainQry = $d->update("task_master_main",$a1,"task_master_main_id = '$task_master_main_id' AND task_master_main_created_by = '$user_id'");   
            }else{
                $taskMainQry = $d->insert("task_master_main",$a);   
                $task_master_main_id = $con->insert_id;           
            }

            
            if ($taskMainQry == true) {

                $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Task Added", "task_management.png");

                $response["message"] = "Task Added";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['deleteMainTask']=="deleteMainTask" && $user_id!=''&& $task_master_main_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $taskMainQry = $d->delete("task_master_main","task_master_main_id = '$task_master_main_id' AND task_master_main_created_by = '$user_id'");   
            
            if ($taskMainQry == true) {

                $d->insert_myactivity($user_id, $society_id, "0", $user_name, "Task List Deleted", "task_management.png");

                $response["message"] = "Task List Deleted";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['addSubTask']=="addSubTask" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $task_attachment = "";

            if ($_FILES["task_attachment"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["task_attachment"]["tmp_name"];
                $extId = pathinfo($_FILES['task_attachment']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","doc","docx","png","jpg","jpeg","JPEG","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["task_attachment"]["name"]);
                    $task_attachment = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["task_attachment"]["tmp_name"], "../img/task_attachment/" . $task_attachment);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG, PNG, Doc & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $m->set_data('society_id',$society_id);
            $m->set_data('task_master_main_id',$task_master_main_id);
            $m->set_data('parent_task_id',$parent_task_id);
            $m->set_data('task_priority_id',$task_priority_id);
            $m->set_data('task_name',$task_name);
            $m->set_data('task_note',$task_note);
            $m->set_data('task_attachment',$task_attachment);
            $m->set_data('task_due_date',$task_due_date);
            $m->set_data('task_add_by',$user_id);
            $m->set_data('task_assign_to',$task_assign_to);
            $m->set_data('task_complete',"0");

            $a = array(
                'society_id' =>$m->get_data('society_id'),
                'task_master_main_id'=>$m->get_data('task_master_main_id'),
                'parent_task_id'=>$m->get_data('parent_task_id'),
                'task_priority_id'=>$m->get_data('task_priority_id'),
                'task_name'=>$m->get_data('task_name'),
                'task_note'=>$m->get_data('task_note'),
                'task_attachment'=>$m->get_data('task_attachment'),
                'task_due_date'=>$m->get_data('task_due_date'),
                'task_add_by'=>$m->get_data('task_add_by'),
                'task_assign_to'=>$m->get_data('task_assign_to'),
                'task_complete'=>$m->get_data('task_complete'),
            );

            $taskMainQry = $d->insert("task_master",$a);   

            $task_id = $con->insert_id;           
            
            if ($taskMainQry == true) {

                

                $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Task Added", "task_management.png");

                if ($user_id != $task_assign_to) {

                    $title = "New Task Assigned";
                    $description = "By, ".$user_name."\n"."In: ".$taskNameView."\nTask: ".$task_name."\nDue Date: ".$task_due_date;

                    $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$task_assign_to'");
                    $data_notification = mysqli_fetch_array($qsm);
                    $user_token = $data_notification['user_token'];
                    $device = $data_notification['device'];

                    $taskDueDate = date('Y-m-d',strtotime($task_due_date));

                    $dataAry = array(
                        "task_name"=>$taskNameView,
                        "task_name_notification"=>$task_name,
                        "parent_task_id"=>$parent_task_id,
                        "task_id"=>$task_id,
                        "task_master_main_id"=>$task_master_main_id,
                        "task_due_date"=>$taskDueDate,
                    );

                    $dataJson = json_encode($dataAry);


                    if ($device == 'android') {
                        $nResident->noti("new_task","",$society_id,$user_token,$title,$description,$dataJson);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("new_task","",$society_id,$user_token,$title,$description,$dataJson);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"new_task","task_management.png",$dataJson,"users_master.user_id = '$task_assign_to'");
                }

                $response["message"] = "Task Added";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['updateSubTask']=="updateSubTask" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $task_attachment = "";

            if ($_FILES["task_attachment"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["task_attachment"]["tmp_name"];
                $extId = pathinfo($_FILES['task_attachment']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","doc","docx","png","jpg","jpeg","JPEG","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["task_attachment"]["name"]);
                    $task_attachment = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["task_attachment"]["tmp_name"], "../img/task_attachment/" . $task_attachment);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG, PNG, Doc & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $m->set_data('task_priority_id',$task_priority_id);
            $m->set_data('task_name',$task_name);
            $m->set_data('task_note',$task_note);

            if ($task_attachment == "") {
                $m->set_data('task_attachment',$task_attachment_name);
            }else{
                $m->set_data('task_attachment',$task_attachment);
            }

            $m->set_data('task_due_date',$task_due_date);
            $m->set_data('task_assign_to',$task_assign_to);

            $a = array(
                'task_priority_id'=>$m->get_data('task_priority_id'),
                'task_name'=>$m->get_data('task_name'),
                'task_note'=>$m->get_data('task_note'),
                'task_attachment'=>$m->get_data('task_attachment'),
                'task_due_date'=>$m->get_data('task_due_date'),
                'task_assign_to'=>$m->get_data('task_assign_to'),
            );

            $taskMainQry = $d->update("task_master",$a,"task_id = '$task_id'");   

            if ($taskMainQry == true) {

                if ($oldTaskAssignedTo != $task_assign_to) {
                    $m->set_data('society_id',$society_id);
                    $m->set_data('task_id',$task_id);
                    $m->set_data('user_id',$task_assign_to);
                    $m->set_data('task_assign_date',$dateTime);

                    $ary = array(
                        'society_id' =>$m->get_data('society_id'),
                        'task_id'=>$m->get_data('task_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'task_assign_date'=>$m->get_data('task_assign_date'),
                    );

                    $taskUserQry = $d->insert("task_user_history",$ary);                    
                }

                $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Update Task", "task_management.png");

                if ($user_id != $task_assign_to) {

                    $title = "Task Updated";
                    $description = "By, ".$user_name."\n"."In: ".$taskNameView."\nTask: ".$task_name."\nDue Date: ".$task_due_date;

                    $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$task_assign_to'");
                    $data_notification = mysqli_fetch_array($qsm);
                    $user_token = $data_notification['user_token'];
                    $device = $data_notification['device'];

                    $dataAry = array(
                        "task_name"=>$taskNameView,
                        "parent_task_id"=>$parent_task_id,
                        "task_id"=>$task_id,
                        "task_master_main_id"=>$task_master_main_id,
                    );

                    $dataJson = json_encode($dataAry);

                    if ($device == 'android') {
                        $nResident->noti("task_updated","",$society_id,$user_token,$title,$description,$dataJson);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("task_updated","",$society_id,$user_token,$title,$description,$dataJson);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"task_updated","task_management.png",$dataJson,"users_master.user_id = '$task_assign_to'");

                    $d->insert_myactivity($user_id, $society_id, "0", $user_name, "Task Data Updated", "task_management.png");
                }

                $response["message"] = "Task Updated";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['addToMyDay']=="addToMyDay" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('task_id',$task_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('task_my_day_created_date',$dateTime);

            $a = array(
                'task_id' =>$m->get_data('task_id'),
                'user_id'=>$m->get_data('user_id'),
                'task_my_day_created_date'=>$m->get_data('task_my_day_created_date'),
            );

            $taskMainQry = $d->insert("task_my_day_master",$a);

            $taskMyDayId = $con->insert_id;    

            if ($taskMainQry == true) {

                $response["task_my_day_id"] = $taskMyDayId;
                $response["message"] = "Task Added To My Day";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['changeTaskStatus']=="changeTaskStatus" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            if ($task_status == 2) {
                $status = "Completed";
                $m->set_data('task_complete',"2");
                $m->set_data('task_complete_date',date('Y-m-d H:i:s'));                
            }else{
                if ($task_status == 1) {
                    $status = "Running";
                }else if ($task_status == 3) {
                    $status = "On Hold";
                }else if ($task_status == 4) {
                    $status = "Canceled";
                }
                $m->set_data('task_complete',$task_status);
            }   
           
            $m->set_data('task_complete_status_change_date',date('Y-m-d H:i:s'));

            $a = array(
                'task_complete' =>$m->get_data('task_complete'),
                'task_complete_status_change_date'=>$m->get_data('task_complete_status_change_date'),
            );

            $taskMainQry = $d->update("task_master",$a,"task_id = '$task_id'");

            if ($taskMainQry == true) {

                $task_timeline_attachment = "";

                if ($_FILES["task_timeline_attachment"]["tmp_name"] != null) {
                    // code...
                    $file11=$_FILES["task_timeline_attachment"]["tmp_name"];
                    $extId = pathinfo($_FILES['task_timeline_attachment']['name'], PATHINFO_EXTENSION);
                    $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG");

                    if(in_array($extId,$extAllow)) {
                        $temp = explode(".", $_FILES["task_timeline_attachment"]["name"]);
                        $task_timeline_attachment = "task_".$user_id.round(microtime(true)) . '.' . end($temp);
                        move_uploaded_file($_FILES["task_timeline_attachment"]["tmp_name"], "../img/task_attachment/" . $task_timeline_attachment);
                    } else {
                    
                        $response["message"] = "Invalid Document. Only JPG, JPEG & PNG files are allowed.";
                        $response["status"] = "201";
                        echo json_encode($response);
                        exit();
                    }
                }

                $m->set_data('task_id',$task_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('task_status',$task_status);
                $m->set_data('task_timeline_description',$task_timeline_description);
                $m->set_data('task_timeline_attachment',$task_timeline_attachment);
                $m->set_data('task_timeline_date',date('Y-m-d H:i:s'));

                $aa = array(
                    'task_id' =>$m->get_data('task_id'),
                    'user_id' =>$m->get_data('user_id'),
                    'task_status' =>$m->get_data('task_status'),
                    'task_timeline_description' =>$m->get_data('task_timeline_description'),
                    'task_timeline_attachment' =>$m->get_data('task_timeline_attachment'),
                    'task_timeline_date'=>$m->get_data('task_timeline_date'),
                );

                $qry = $d->insert("task_timeline_master",$aa);

                $title = "Task status changed";
                $description = "By, ".$user_name."\nTask: ".$task_name."\nStatus: ".$status;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$task_add_by'");
                $data_notification = mysqli_fetch_array($qsm);
                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $dataAry = array(
                    "task_name"=>$parent_task_name,
                    "parent_task_id"=>$parent_task_id,
                    "task_id"=>$task_id,
                    "task_master_main_id"=>$task_master_main_id,
                );

                $dataJson = json_encode($dataAry);

                if ($device == 'android') {
                    $nResident->noti("task_status","",$society_id,$user_token,$title,$description,$dataJson);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("task_status","",$society_id,$user_token,$title,$description,$dataJson);
                }

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"task_status","task_management.png",$dataJson,"users_master.user_id = '$task_add_by'");

                $d->insert_myactivity($user_id, $society_id, "0", $user_name, "Task Status Changed - $status", "task_management.png");

                $response["message"] = "Status updated";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['addTaskTimeline']=="addTaskTimeline" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            /*$task_timeline_attachment = "";

            if ($_FILES["task_timeline_attachment"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["task_timeline_attachment"]["tmp_name"];
                $extId = pathinfo($_FILES['task_timeline_attachment']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["task_timeline_attachment"]["name"]);
                    $task_timeline_attachment = "task_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["task_timeline_attachment"]["tmp_name"], "../img/task_attachment/" . $task_timeline_attachment);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG, JPEG & PNG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }*/

            $m->set_data('task_id',$task_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('task_status',"5");
            $m->set_data('task_timeline_description',$task_timeline_description);
            $m->set_data('task_timeline_date',date('Y-m-d H:i:s'));

            $aa = array(
                'task_id' =>$m->get_data('task_id'),
                'user_id' =>$m->get_data('user_id'),
                'task_status' =>$m->get_data('task_status'),
                'task_timeline_description' =>$m->get_data('task_timeline_description'),
                'task_timeline_date'=>$m->get_data('task_timeline_date'),
            );

            $qry = $d->insert("task_timeline_master",$aa);

            if ($qry == true) {
                $title = "New mesasge in task";
                $description = "By, ".$user_name."\nTask: ".$task_name."\nMessage: ".$task_timeline_description;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$other_user_id'");
                $data_notification = mysqli_fetch_array($qsm);
                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $dataAry = array(
                    "task_id"=>$task_id,
                    "task_name"=>$task_name,
                    "parent_task_name"=>$parent_task_name,
                    "parent_task_id"=>$parent_task_id,
                    "task_add_by"=>$task_add_by,
                    "task_master_main_id"=>$task_master_main_id,
                    "other_user_id"=>$other_user_id,
                    "user_id"=>$user_id,
                );

                $dataJson = json_encode($dataAry);

                if ($device == 'android') {
                    $nResident->noti("task_message","",$society_id,$user_token,$title,$description,$dataJson);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("task_message","",$society_id,$user_token,$title,$description,$dataJson);
                }

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"task_message","task_management.png",$dataJson,"users_master.user_id = '$task_add_by'");

                $response["message"] = "Message sent";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['removeFromMyDay']=="removeFromMyDay" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $taskMainQry = $d->delete("task_my_day_master","task_my_day_id = '$task_my_day_id' AND user_id = '$user_id'");   

            if ($taskMainQry == true) {

                $response["message"] = "Task Removed From My Day";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['addToImportant']=="addToImportant" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('task_id',$task_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('task_important_created_date',$dateTime);

            $a = array(
                'task_id' =>$m->get_data('task_id'),
                'user_id'=>$m->get_data('user_id'),
                'task_important_created_date'=>$m->get_data('task_important_created_date'),
            );

            $taskMainQry = $d->insert("task_important_master",$a);

            $taskImportantId = $con->insert_id;  

            if ($taskMainQry == true) {

                $response["task_important_id"] = $taskImportantId;
                $response["message"] = "Task Added To Important";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['removeFromImportant']=="removeFromImportant" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $taskMainQry = $d->delete("task_important_master","task_important_id = '$task_important_id' AND user_id = '$user_id'");   

            if ($taskMainQry == true) {

                $response["message"] = "Task Removed From Important";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['deleteTask']=="deleteTask" && $user_id!='' && $task_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $taskMainQry = $d->delete("task_master","task_id = '$task_id' AND task_add_by = '$user_id'");  

            if ($task_my_day_id != '') {
                $taskMainQry = $d->delete("task_my_day_master","task_id = '$task_id' AND user_id = '$user_id' AND task_my_day_id = '$task_my_day_id'");   
            } 

            if ($task_important_id != '') {
                $taskMainQry = $d->delete("task_important_master","task_id = '$task_id' AND user_id = '$user_id' AND task_important_id = '$task_important_id'");   
            }

            if ($taskMainQry == true) {

                $d->insert_myactivity($user_id, $society_id, "0", $user_name, "Task Deleted", "task_management.png");

                $response["message"] = "Task Deleted";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getPriority']=="getPriority" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
            
            $q = $d->select("task_priority_master","society_id = '$society_id' AND task_priority_status = '0'");

            $response['priority'] = array();

            if (mysqli_num_rows($q) > 0) {

                while ($data = mysqli_fetch_array($q)) {

                    $priority = array();

                    $task_priority_id = $data['task_priority_id'];

                    $priority["task_priority_id"] = $task_priority_id;
                    $priority["task_priority_name"] = $data['task_priority_name'];

                    array_push($response["priority"], $priority);
                }
            }

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);

        }else if($_POST['getTaskEmployees']=="getTaskEmployees" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            if ($task_added_by != '') {
                $userQry = $d->select("users_master,floors_master,block_master","
                    users_master.active_status = '0' 
                    AND users_master.delete_status = '0' 
                    AND users_master.floor_id = '$floor_id' 
                    AND users_master.society_id = '$society_id' 
                    AND users_master.user_id != '$task_added_by' 
                    AND users_master.floor_id = floors_master.floor_id 
                    AND users_master.block_id = block_master.block_id","");
            }else{

                $userQry = $d->select("users_master,floors_master,block_master","
                    users_master.active_status = '0' 
                    AND users_master.delete_status = '0' 
                    AND users_master.floor_id = '$floor_id' 
                    AND users_master.society_id = '$society_id' 
                    AND users_master.floor_id = floors_master.floor_id 
                    AND users_master.block_id = block_master.block_id","");
            }

            if (mysqli_num_rows($userQry) > 0) {

                $response['employees'] = array();

                while($data_units_list=mysqli_fetch_array($userQry)) {
                                 
                    $employees = array(); 
         
                    $employees["user_id"]=$data_units_list['user_id'].'';
                    $employees["branch_name"]=$data_units_list['block_name'].'';
                    $employees["department_name"]=$data_units_list['floor_name'].'';
                    $employees["user_full_name"]=$data_units_list['user_full_name'].'';
                    $employees["designation"]=$data_units_list['user_designation'];
                    
                    if ($data_units_list['user_profile_pic'] != '') {
                        $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                    } else {
                        $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    array_push($response["employees"], $employees);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }
        
        }else if($_POST['getTaskUsers']=="getTaskUsers" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $access_type = $assign_task_access;

            include "check_access_data.php";
            
            $accessResponseData["message"] = "Success";
            $accessResponseData["status"] = "200";
            echo json_encode($accessResponseData);
            
        }else if($_POST['getMainTasks']=="getMainTasks" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $totalMyDayTask = $d->count_data_direct("task_my_day_id","task_my_day_master","user_id = '$user_id'");

            $response["total_my_day"] = $totalMyDayTask.'';

            $totalImportantTask = $d->count_data_direct("task_important_id","task_important_master","user_id = '$user_id'");

            $response["total_important_tasks"] = $totalImportantTask.'';

            $qCompleteTasks = $d->selectRow("
                    task_master_main.task_master_main_id,
                    task_master.task_id,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '2') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '2')) AS complete_tasks
                    ","
                    task_master_main,
                    task_master
                    ","
                    task_master_main.task_master_main_id = task_master.task_master_main_id 
                    AND task_master.task_status = '0' 
                    AND task_master.task_delete = '0' 
                    AND task_master.task_complete = '2'  
                    AND (task_master.task_assign_to = '$user_id' OR task_master.task_add_by = '$user_id')");

            $response["total_complete_tasks"] = mysqli_num_rows($qCompleteTasks).'';

            if ($is_task_complete == "1") {

                $q = $d->selectRow("
                    task_master_main.*,
                    task_master.*,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0')) AS total_task,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '2') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '2')) AS complete_tasks,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '0') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '0')) AS pending_task,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '1') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '1')) AS in_progress,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '3') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '3')) AS on_hold,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '4') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '4')) AS Cancelled
                    ","
                    task_master_main,
                    task_master
                    ","
                    task_master_main.task_master_main_id = task_master.task_master_main_id 
                    AND task_master.task_status = '0' 
                    AND task_master.task_delete = '0' 
                    AND task_master.task_complete = '2' 
                    AND (task_master.task_assign_to = '$user_id' OR task_master.task_add_by = '$user_id')","GROUP BY task_master.task_master_main_id ORDER BY task_master.task_due_date ASC");
            }else{

                $q = $d->selectRow("
                    task_master_main.*,
                    task_master.*,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0')) AS total_task,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '2') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '2')) AS complete_tasks,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '0') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '0')) AS pending_task,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '1') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '1')) AS in_progress_tasks,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '3') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '3')) AS on_hold_tasks,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '4') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '4')) AS cancelled_tasks
                    ","
                    task_master_main,
                    task_master
                    ","
                    task_master_main.task_master_main_id = task_master.task_master_main_id 
                    AND task_master.task_status = '0' 
                    AND task_master.task_delete = '0' 
                    AND task_master_main.task_master_main_created_by != '$user_id'  
                    AND (task_master.task_assign_to = '$user_id' OR task_master.task_add_by = '$user_id')","GROUP BY task_master.task_master_main_id ORDER BY task_master.task_due_date ASC");
            }

            $response['task_main_list'] = array();

            if (mysqli_num_rows($q) > 0) {

                while ($data = mysqli_fetch_array($q)) {


                    $tasks = array();

                    $task_master_main_id = $data['task_master_main_id'];

                    $tasks["task_master_main_id"] = $task_master_main_id;
                    $tasks["task_master_main_name"] = $data['task_master_main_name'];
                    $tasks["task_id"] = $data['task_id'];
                    $tasks["parent_task_id"] = $data['parent_task_id'];

                    $totalTask = $data['total_task'];
                    $completedTask = $data['complete_tasks'];
                    $pending_task = $data['pending_task'];
                    $in_progress_tasks = $data['in_progress_tasks'];
                    $on_hold_tasks = $data['on_hold_tasks'];
                    $cancelled_tasks = $data['cancelled_tasks'];

                    $tasks["total_tasks"] = $totalTask;
                    $tasks["complete_tasks"] = $completedTask.'';
                    $tasks["pending_task"] = $pending_task.'';
                    $tasks["in_progress_tasks"] = $in_progress_tasks.'';
                    $tasks["on_hold_tasks"] = $on_hold_tasks.'';
                    $tasks["cancelled_tasks"] = $cancelled_tasks.'';

                    $percentage = ($completedTask * 100) / $totalTask;

                    if ($totalTask == $completedTask) {
                        $tasks['status'] = "COMPLETED";
                    }else{
                        $tasks['status'] = "RUNNING";
                    }

                    if ($percentage > 0 ) {
                        $tasks["complete_percentage"] = (int)$percentage.'';
                    }else{
                        $tasks["complete_percentage"] = '0';                        
                    }



                    array_push($response["task_main_list"], $tasks);
                }
            }

            if ($is_task_complete != "1") {

                $qqry = $d->selectRow("
                    task_master_main.*,
                    (SELECT count(task_master.task_id) FROM task_master WHERE (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' AND task_master.task_delete = '0') OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' AND task_master.task_delete = '0')) AS total_task,
                    (SELECT count(task_master.task_id) FROM task_master WHERE (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' AND task_master.task_delete = '0'  AND task_complete = '2') OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' AND task_master.task_delete = '0'  AND task_complete = '2')) AS complete_tasks,
                    (SELECT count(task_master.task_id) FROM task_master WHERE (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' AND task_master.task_delete = '0'  AND task_complete = '0') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '0')) AS pending_task,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '1') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '1')) AS in_progress_tasks,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '3') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '3')) AS on_hold_tasks,
                    (SELECT count(task_master.task_id) FROM task_master WHERE 
                        (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_assign_to = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '4') 
                        OR (task_master.task_master_main_id = task_master_main.task_master_main_id AND task_master.task_add_by = '$user_id' AND task_master.task_status = '0' 
                            AND task_master.task_delete = '0'  AND task_complete = '4')) AS cancelled_tasks
                    ","
                    task_master_main
                    ","
                    task_master_main_created_by = '$user_id'  
                    AND society_id = '$society_id'");
            }

            if (mysqli_num_rows($qqry) > 0) {

                while ($data1 = mysqli_fetch_array($qqry)) {

                    $tasks = array();

                    $task_master_main_id = $data1['task_master_main_id'];

                    $tasks["task_master_main_id"] = $task_master_main_id;
                    $tasks["task_master_main_name"] = $data1['task_master_main_name'];
                    $tasks["task_id"] = "0";
                    $tasks["parent_task_id"] = "0";
                    $tasks["task_master_main_created_by"] = $data1['task_master_main_created_by'].'';

                    $totalTask = $data1['total_task'];
                    $completedTask = $data1['complete_tasks'];
                    $pending_task = $data1['pending_task'];
                    $in_progress_tasks = $data1['in_progress_tasks'];
                    $on_hold_tasks = $data1['on_hold_tasks'];
                    $cancelled_tasks = $data1['cancelled_tasks'];

                    $tasks["total_tasks"] = $totalTask;
                    $tasks["complete_tasks"] = $completedTask.'';
                    $tasks["pending_task"] = $pending_task.'';
                    $tasks["in_progress_tasks"] = $in_progress_tasks.'';
                    $tasks["on_hold_tasks"] = $on_hold_tasks.'';
                    $tasks["cancelled_tasks"] = $cancelled_tasks.'';
                    $tasks['status'] = "";

                    $percentage = ($completedTask * 100) / $totalTask;

                    if ($percentage > 0 ) {
                        $tasks["complete_percentage"] = (int)$percentage.'';
                    }else{
                        $tasks["complete_percentage"] = '0';                        
                    }

                    array_push($response["task_main_list"], $tasks);
                }
            }

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);

        }else if($_POST['getTasks']=="getTasks" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            if ($completed_task == 1) {
                $appndQry = " AND task_complete = '2'";
                $appndQry1 = " AND task_complete = '2'";
            }else{
                $appndQry = " AND task_master.parent_task_id = '$parent_task_id'";
            }
        
            $q = $d->selectRow("
                task_master_main.task_master_main_id,
                task_master_main.task_master_main_name,
                task_priority_master.task_priority_id,
                task_priority_master.task_priority_name,
                users_master.user_id,
                users_master.user_full_name,
                users_master.user_profile_pic,
                tmd.task_my_day_id,
                tim.task_important_id,
                task_master.*","
                task_master_main,
                task_priority_master,
                users_master,
                task_master 
                LEFT JOIN task_my_day_master as tmd ON tmd.task_id = task_master.task_id AND tmd.user_id='$user_id' 
                LEFT JOIN task_important_master as tim ON tim.task_id = task_master.task_id AND tim.user_id='$user_id'","
                (task_master_main.task_master_main_id = task_master.task_master_main_id 
                AND task_master.task_priority_id = task_priority_master.task_priority_id 
                AND task_master.task_status = '0' 
                AND task_master.task_delete = '0' 
                AND task_master.task_master_main_id = '$task_master_main_id' 
                AND task_master.task_assign_to = '$user_id' 
                AND users_master.user_id = task_master.task_add_by $appndQry)                 
                OR 
                (task_master_main.task_master_main_id = task_master.task_master_main_id 
                AND task_master.task_priority_id = task_priority_master.task_priority_id 
                AND task_master.task_status = '0' 
                AND task_master.task_delete = '0' 
                AND task_master.task_master_main_id = '$task_master_main_id' 
                AND task_master.task_add_by = '$user_id' 
                AND users_master.user_id = task_master.task_assign_to $appndQry)","ORDER BY task_master.task_due_date ASC");

            $response['task_list'] = array();

            if (mysqli_num_rows($q) > 0) {

                while ($data = mysqli_fetch_array($q)) {

                    $tasks = array();

                    $task_id = $data['task_id'];

                    $tasks["task_id"] = $task_id;
                    $tasks["task_my_day_id"] = $data['task_my_day_id']."";
                    $tasks["task_important_id"] = $data['task_important_id']."";
                    $tasks["task_master_main_id"] = $data['task_master_main_id'];
                    $tasks["task_master_main_name"] = $data['task_master_main_name'];
                    $tasks["parent_task_id"] = $data['parent_task_id'];
                    $tasks["task_priority_id"] = $data['task_priority_id'];
                    $tasks["task_priority_name"] = $data['task_priority_name'];
                    $tasks["task_name"] = $data['task_name'];
                    $tasks["task_note"] = $data['task_note'];
                    $tasks["task_add_by"] = $data['task_add_by'];
                    $tasks["task_assign_to"] = $data['task_assign_to'];
                    $tasks["user_id"] = $data['user_id'];
                    $tasks["user_full_name"] = $data['user_full_name'];

                    if ($data['user_profile_pic'] != '') {
                        $tasks["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $tasks["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    if ($data['task_attachment'] != '') {
                        $tasks["task_attachment"] = $base_url."img/task_attachment/". $data['task_attachment'];
                    }else{
                        $tasks["task_attachment"] = "";
                    }

                    $tasks["task_attachment_name"] = $data['task_attachment'];

                    $oneDayBeforeDue = date('Y-m-d',strtotime("-1 days",strtotime($data['task_due_date'])));

                    $tasks["one_day_before_due_date"] = $oneDayBeforeDue;
                    $tasks["task_due_date"] = date('d M Y',strtotime($data['task_due_date']));

                    $tasks["task_created_date"] = date('h:i A, d M Y',strtotime($data['task_created_date']));

                    $task_complete = $data['task_complete'];
                    $tasks["task_complete"] = $task_complete;

                    if ($task_complete == 0) {
                        $tasks["task_complete_view"] = "Pending";
                    }else if ($task_complete == 1) {
                        $tasks["task_complete_view"] = "Running";
                    }else if ($task_complete == 2) {
                        $tasks["task_complete_view"] = "Completed";
                    }else if ($task_complete == 3) {
                        $tasks["task_complete_view"] = "ON Hold";
                    }else if ($task_complete == 4) {
                        $tasks["task_complete_view"] = "Canceled";
                    }

                    if ($tasks["task_my_day_id"] == '') {
                        $tasks["is_in_my_day"] = false;
                    }else{
                        $tasks["is_in_my_day"] = true;
                    }

                    if ($tasks["task_important_id"] == '') {
                        $tasks["is_in_important"] = false;
                    }else{
                        $tasks["is_in_important"] = true;
                    }

                    $subTaskCount = $d->count_data_direct("task_id","task_master","parent_task_id = '$task_id' AND (task_assign_to = '$user_id' OR task_add_by = '$user_id') $appndQry1");
                    $subTaskCountComplete = $d->count_data_direct("task_id","task_master","parent_task_id = '$task_id'  AND (task_assign_to = '$user_id' OR task_add_by = '$user_id') AND task_complete = '2'");

                    $tasks['total_sub_tasks'] = $subTaskCount.'';

                    $subTaskCount = $subTaskCount;

                    if ($subTaskCount > 0) {
                        $tasks['has_sub_tasks'] = true;
                    }else{
                        $tasks['has_sub_tasks'] = false;                    
                    }

                    $tasks["task_complete_date"] = $data['task_complete_date']."";

                    $percentage = ($subTaskCountComplete * 100) / $subTaskCount;

                    if ($percentage > 0) {
                        $tasks["complete_percentage"] = (int) $percentage.'';
                    }else{
                        $tasks["complete_percentage"] = '0';                        
                    }

                    array_push($response["task_list"], $tasks);
                }
            }

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);

        }else if($_POST['getMyDayTasks']=="getMyDayTasks" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q = $d->selectRow(
                "task_master_main.task_master_main_id,
                task_master_main.task_master_main_name,
                task_priority_master.task_priority_id,
                task_priority_master.task_priority_name,
                users_master.user_id,
                users_master.user_full_name,
                users_master.user_profile_pic,
                task_my_day_master.task_my_day_id,
                tim.task_important_id,
                task_master.*","
                task_master_main,
                task_priority_master,
                users_master,
                task_my_day_master,
                task_master LEFT JOIN task_important_master as tim ON tim.task_id = task_master.task_id AND tim.user_id = '$user_id'","task_master_main.task_master_main_id = task_master.task_master_main_id 
                AND task_master.task_priority_id = task_priority_master.task_priority_id 
                AND task_master.task_status = '0' 
                AND task_master.task_delete = '0' 
                AND users_master.user_id = task_master.task_assign_to 
                AND task_my_day_master.task_id = task_master.task_id 
                AND task_my_day_master.user_id = '$user_id'","ORDER BY task_master.task_due_date ASC");

            $response['task_list'] = array();

            if (mysqli_num_rows($q) > 0) {

                while ($data = mysqli_fetch_array($q)) {

                    $tasks = array();

                    $task_id = $data['task_id'];

                    $tasks["task_id"] = $task_id;
                    $tasks["task_my_day_id"] = $data['task_my_day_id']."";
                    $tasks["task_important_id"] = $data['task_important_id']."";
                    $tasks["task_master_main_id"] = $data['task_master_main_id'];
                    $tasks["task_master_main_name"] = $data['task_master_main_name'];
                    $tasks["parent_task_id"] = $data['parent_task_id'];
                    $tasks["task_priority_id"] = $data['task_priority_id'];
                    $tasks["task_priority_name"] = $data['task_priority_name'];
                    $tasks["task_name"] = $data['task_name'];
                    $tasks["task_note"] = $data['task_note'];
                    $tasks["task_add_by"] = $data['task_add_by'];
                    $tasks["task_assign_to"] = $data['task_assign_to'];
                    $tasks["user_id"] = $data['user_id'];
                    $tasks["user_full_name"] = $data['user_full_name'];

                    if ($data['user_profile_pic'] != '') {
                        $tasks["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $tasks["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    if ($data['task_attachment'] != '') {
                        $tasks["task_attachment"] = $base_url."img/task_attachment/". $data['task_attachment'];
                    }else{
                        $tasks["task_attachment"] = "";
                    }

                    $tasks["task_attachment_name"] = $data['task_attachment'];

                    $tasks["task_due_date"] = $data['task_due_date'];

                    $task_complete = $data['task_complete'];
                    $tasks["task_complete"] = $task_complete;

                    if ($task_complete == 0) {
                        $tasks["task_complete_view"] = "Pending";
                    }else if ($task_complete == 1) {
                        $tasks["task_complete_view"] = "Running";
                    }else if ($task_complete == 2) {
                        $tasks["task_complete_view"] = "Completed";
                    }else if ($task_complete == 3) {
                        $tasks["task_complete_view"] = "ON Hold";
                    }else if ($task_complete == 4) {
                        $tasks["task_complete_view"] = "Canceled";
                    }

                    $tasks["is_in_my_day"] = true;

                    if ($tasks["task_important_id"] == '') {
                        $tasks["is_in_important"] = false;
                    }else{
                        $tasks["is_in_important"] = true;
                    }

                    $subTaskCount = $d->count_data_direct("task_id","task_master","parent_task_id = '$task_id'");

                    if ($subTaskCount > 0) {
                        $tasks['has_sub_tasks'] = true;
                    }else{
                        $tasks['has_sub_tasks'] = false;                    
                    }

                    $tasks["task_complete_date"] = $data['task_complete_date']."";

                    $tasks["complete_percentage"] = "0";

                    array_push($response["task_list"], $tasks);
                }
            }

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);

        }else if($_POST['getImportantTasks']=="getImportantTasks" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q = $d->selectRow("
                task_master_main.task_master_main_id,
                task_master_main.task_master_main_name,
                task_priority_master.task_priority_id,
                task_priority_master.task_priority_name,
                users_master.user_id,
                users_master.user_full_name,
                users_master.user_profile_pic,
                tmd.task_my_day_id,
                task_important_master.task_important_id,
                task_master.*","
                task_master_main,
                task_priority_master,
                users_master,
                task_important_master,
                task_master LEFT JOIN task_my_day_master as tmd ON tmd.task_id = task_master.task_id AND tmd.user_id = '$user_id'","task_master_main.task_master_main_id = task_master.task_master_main_id 
                AND task_master.task_priority_id = task_priority_master.task_priority_id 
                AND task_master.task_status = '0' 
                AND task_master.task_delete = '0' 
                AND users_master.user_id = task_master.task_assign_to 
                AND task_important_master.task_id = task_master.task_id 
                AND task_important_master.user_id = '$user_id'","ORDER BY task_master.task_due_date ASC");

            $response['task_list'] = array();

            if (mysqli_num_rows($q) > 0) {

                while ($data = mysqli_fetch_array($q)) {

                    $tasks = array();

                    $task_id = $data['task_id'];

                    $tasks["task_id"] = $task_id;
                    $tasks["task_my_day_id"] = $data['task_my_day_id']."";
                    $tasks["task_important_id"] = $data['task_important_id']."";
                    $tasks["task_master_main_id"] = $data['task_master_main_id'];
                    $tasks["task_master_main_name"] = $data['task_master_main_name'];
                    $tasks["parent_task_id"] = $data['parent_task_id'];
                    $tasks["task_priority_id"] = $data['task_priority_id'];
                    $tasks["task_priority_name"] = $data['task_priority_name'];
                    $tasks["task_name"] = $data['task_name'];
                    $tasks["task_note"] = $data['task_note'];
                    $tasks["task_add_by"] = $data['task_add_by'];
                    $tasks["task_assign_to"] = $data['task_assign_to'];
                    $tasks["user_id"] = $data['user_id'];
                    $tasks["user_full_name"] = $data['user_full_name'];

                    if ($data['user_profile_pic'] != '') {
                        $tasks["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $tasks["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    if ($data['task_attachment'] != '') {
                        $tasks["task_attachment"] = $base_url."img/task_attachment/". $data['task_attachment'];
                    }else{
                        $tasks["task_attachment"] = "";
                    }

                    $tasks["task_attachment_name"] = $data['task_attachment'];
                    $tasks["task_due_date"] = $data['task_due_date'];

                    $task_complete = $data['task_complete'];
                    $tasks["task_complete"] = $task_complete;

                    if ($task_complete == 0) {
                        $tasks["task_complete_view"] = "Pending";
                    }else if ($task_complete == 1) {
                        $tasks["task_complete_view"] = "Running";
                    }else if ($task_complete == 2) {
                        $tasks["task_complete_view"] = "Completed";
                    }else if ($task_complete == 3) {
                        $tasks["task_complete_view"] = "ON Hold";
                    }else if ($task_complete == 4) {
                        $tasks["task_complete_view"] = "Canceled";
                    }

                    if ($tasks["task_my_day_id"] == '') {
                        $tasks["is_in_my_day"] = false;
                    }else{
                        $tasks["is_in_my_day"] = true;
                    }

                    $tasks["is_in_important"] = true;

                    $subTaskCount = $d->count_data_direct("task_id","task_master","parent_task_id = '$task_id'");

                    if ($subTaskCount > 0) {
                        $tasks['has_sub_tasks'] = true;
                    }else{
                        $tasks['has_sub_tasks'] = false;                    
                    }

                    $tasks["task_complete_date"] = $data['task_complete_date']."";

                    $tasks["complete_percentage"] = "0";

                    array_push($response["task_list"], $tasks);
                }
            }

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);

        }else if($_POST['getTaskHistory']=="getTaskHistory" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q = $d->selectRow("
                users_master.user_id,
                users_master.user_full_name,
                users_master.user_profile_pic,
                users_master.user_designation,
                task_master.task_id,
                task_user_history.task_user_history_id,
                task_user_history.task_assign_date","
                users_master,
                task_master,
                task_user_history","
                task_master.task_id = task_user_history.task_id 
                AND task_status = '0' 
                AND task_master.task_delete = '0' 
                AND task_user_history.task_id = '$task_id' 
                AND task_user_history.user_id = users_master.user_id",
                "ORDER BY task_user_history.task_user_history_id DESC");

            $response['task_history'] = array();

            if (mysqli_num_rows($q) > 0) {

                while ($data = mysqli_fetch_array($q)) {

                    $tasks = array();

                    $task_id = $data['task_id'];

                    $tasks["task_id"] = $task_id;
                    $tasks["user_id"] = $data['user_id'];
                    $tasks["user_full_name"] = $data['user_full_name'];
                    $tasks["user_designation"] = $data['user_designation'];
                    $tasks["task_assign_date"] = date('d M Y, h:i A',strtotime($data['task_assign_date']));
                    
                    if ($data['user_profile_pic'] != '') {
                        $tasks["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $tasks["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    array_push($response["task_history"], $tasks);
                }
            }

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);

        }else if($_POST['getTaskTimeline']=="getTaskTimeline" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q = $d->selectRow("task_timeline_master.*,users_master.user_id,
                users_master.user_full_name,
                users_master.user_profile_pic,
                users_master.user_designation","task_timeline_master,users_master","task_timeline_master.task_id = '$task_id' AND users_master.user_id = task_timeline_master.user_id","ORDER BY task_timeline_master.task_timeline_date ASC");


            if (mysqli_num_rows($q) > 0) {
                $response['task_timeline'] = array();

                while ($data = mysqli_fetch_array($q)) {

                    $tasks = array();

                    $tasks["task_id"] = $data['task_timeline_id'];
                    $tasks["task_id"] = $data['task_id'];
                    $tasks["user_id"] = $data['user_id'];
                    $tasks["user_full_name"] = $data['user_full_name'];
                    $tasks["user_designation"] = $data['user_designation'];
                    
                    if ($data['user_profile_pic'] != '') {
                        $tasks["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $tasks["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }
                    $tasks["task_timeline_description"] = $data['task_timeline_description'];
                    if ($data['task_timeline_attachment'] != '') {
                        $tasks["task_timeline_attachment"] = $base_url.'img/task_attachment/'. $data['task_timeline_attachment'];
                    }else{
                        $tasks["task_timeline_attachment"] = "";
                    }
                    $tasks["task_timeline_date"] = time_elapsed_string($data['task_timeline_date']);
                    $tasks["task_timeline_date_view"] = date('h:i A, d M Y',strtotime($data['task_timeline_date']));
                    $tasks["task_status"] = $data['task_status'];
                    
                    if ($data['task_status'] == 0) {
                        $tasks["task_status_view"] = "Pending";
                    }else if ($data['task_status'] == 1) {
                        $tasks["task_status_view"] = "Running";
                    }else if ($data['task_status'] == 2) {
                        $tasks["task_status_view"] = "Completed";
                    }else if ($data['task_status'] == 3) {
                        $tasks["task_status_view"] = "Hold";
                    } else if ($data['task_status'] == 4) {
                        $tasks["task_status_view"] = "Cancelled";
                    } else if ($data['task_status'] == 5) {
                        $tasks["task_status_view"] = "";
                    } 

                    array_push($response["task_timeline"], $tasks);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

?>