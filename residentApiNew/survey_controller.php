<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
      if($_POST['getSurvey']=="getSurvey" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
            $fq = $d->select("users_master", "unit_id='$unit_id' AND user_id='$user_id'");
            $ownerData = mysqli_fetch_array($fq);
            $userType = $ownerData['user_type'];
            $member_status = $ownerData['member_status'];
            
                
                $qss = $d->select("survey_master", "((society_id='$society_id') AND survey_for=0 OR  survey_for='$floor_id' )", "ORDER BY survey_id DESC");
                

                if(mysqli_num_rows($qss)>0){
                $response["survey"] = array();

                    while($data=mysqli_fetch_array($qss)) {
                        $survey_question_master_qry=$d->select("survey_question_master","survey_id ='$data[survey_id]' ");
                        $totalQue= mysqli_num_rows($survey_question_master_qry);

                        
                        $survey = array(); 
                        $survey["survey_id"]=$data['survey_id'];
                        $survey["survey_title"]=html_entity_decode($data['survey_title']);
                        $survey["survey_desciption"]=html_entity_decode($data['survey_desciption']);
                        $survey["survey_date"]=date("d M Y", strtotime($data['survey_date']));

                        $startTime = strtotime($data['survey_date']);
                        $cTime= date("00:00:00");
                        $today = strtotime("today $cTime");

                         if ($today < $startTime || $totalQue==0) { 
                            $survey["survey_status_check"] ='2';
                            $survey["survey_status"] = 'Survey Yet Not Start';

                          }else  if ($data['survey_status']==2) { 
                            $survey["survey_status_check"] ='2';
                            $survey["survey_status"] = 'Survey Yet Not Start';

                          }else if ($data['survey_status']==1) {
                            $survey["survey_status_check"] = '1';
                            $survey["survey_status"] = 'Closed';
                          }   else {
                            $survey["survey_status_check"] = '0';
                            $survey["survey_status"] = 'Open';

                          }

                        $qs=$d->select("survey_result_master","user_id='$user_id' AND unit_id='$unit_id'  AND survey_id='$data[survey_id]'");
                        if (mysqli_num_rows($qs)>0) {
                           $survey["is_survey_submited"]=true; 
                        } else {
                            $survey["is_survey_submited"]=false;
                        }
                       
                        array_push($response["survey"], $survey); 
                    }

                    $response["message"]="Get Survey success.";
                    $response["status"]="200";
                    echo json_encode($response);

                }
                else{
                $response["message"]="No Survey Avaibale";
                $response["status"]="201";
                echo json_encode($response);

            }

    }else if($_POST['getSurveyQuestion']=="getSurveyQuestion" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($survey_id, FILTER_VALIDATE_INT) == true ){

                
                $q=$d->select("survey_master,survey_question_master","survey_master.survey_id=survey_question_master.survey_id AND survey_master.society_id='$society_id' AND survey_question_master.survey_id='$survey_id'","ORDER BY survey_master.survey_id DESC");
                   


                if(mysqli_num_rows($q)>0){
                $response["survey"] = array();

                    while($data=mysqli_fetch_array($q)) {
                        $survey = array(); 
                        $survey["survey_question_id"]=$data['survey_question_id'];
                        $survey["survey_id"]=$data['survey_id'];
                        $survey["survey_question"]=html_entity_decode($data['survey_question']);
                        if ($data['question_image']!='') {
                          $survey["question_image"]=$base_url.'img/survey/'.$data['question_image'];
                        } else {
                          $survey["question_image"]="";
                        }
                        
                        $qs=$d->select("survey_option_master","survey_question_id='$data[survey_question_id]' AND survey_id='$data[survey_id]'");

                          if(mysqli_num_rows($qs)>0){

                            $survey["question_option"] = array();
                              while($data=mysqli_fetch_array($qs)) {
                                $question_option = array(); 
                                $question_option["survey_option_id"]=$data['survey_option_id'];
                                $question_option["survey_option_name"]=html_entity_decode($data['survey_option_name']);
                                array_push($survey["question_option"],$question_option); 
                              }

                            }

                        array_push($response["survey"], $survey); 
                    }

                    $response["message"]="Question get successfully";
                    $response["status"]="200";
                    echo json_encode($response);

                }
                else{
                $response["message"]="No Question for this Survey.";
                $response["status"]="201";
                echo json_encode($response);

            }

    }else  if($_POST['addSurveyAnswer']=="addSurveyAnswer" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                $surveyQuesAry = explode(",",$survey_question_id); 
                $surveyAnsry = explode(",",$survey_option_id); 
                $queCount= count($surveyQuesAry);

                for ($i=0; $i < $queCount; $i++) { 
                    $m->set_data('society_id', $society_id);
                    $m->set_data('survey_option_id', $surveyAnsry[$i]);
                    $m->set_data('survey_question_id', $surveyQuesAry[$i]);
                    $m->set_data('survey_id', $survey_id);
                    $m->set_data('user_id', $user_id);
                    $m->set_data('unit_id', $unit_id);

                    $a1 = array(
                        'society_id' => $m->get_data('society_id'),
                        'survey_option_id' => $m->get_data('survey_option_id'),
                        'survey_question_id' => $m->get_data('survey_question_id'),
                        'survey_id' => $m->get_data('survey_id'),
                        'user_id' => $m->get_data('user_id'),
                        'unit_id' => $m->get_data('unit_id'),
                    );

                    $qs=$d->select("survey_result_master","user_id='$user_id' AND unit_id='$unit_id'  AND survey_question_id='$survey_question_id[$i]'");
                    if (mysqli_num_rows($qs)>0) {
                        $q=$d->update("survey_result_master",$a1,"user_id='$user_id' AND unit_id='$unit_id' AND survey_question_id='$survey_question_id[$i]'");
                    } else {
                        $q=$d->insert("survey_result_master",$a1);
                    }


                }

               
               if ($q>0) {
             $d->insert_myactivity($user_id,"$society_id","0","$user_name","Participate in survey","Survey_1xxxhdpi.png");


                    $response["message"]="Thank you for participate ";
                    $response["status"]="200";
                    echo json_encode($response);

                }
                else{
                $response["message"]="No Survey Avaibale";
                $response["status"]="201";
                echo json_encode($response);

            }

    }else if($_POST['getSurveyResult']=="getSurveyResult" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($survey_id, FILTER_VALIDATE_INT) == true ){

                
                $q=$d->select("survey_master,survey_question_master","survey_master.survey_id=survey_question_master.survey_id AND survey_master.society_id='$society_id' AND survey_question_master.survey_id='$survey_id'","ORDER BY survey_master.survey_id DESC");
                   


                if(mysqli_num_rows($q)>0){
                $response["survey"] = array();

                    while($data=mysqli_fetch_array($q)) {
                        $survey = array(); 
                        $survey["survey_question_id"]=$data['survey_question_id'];
                        $survey["survey_id"]=$data['survey_id'];
                        $survey["survey_question"]=html_entity_decode($data['survey_question']);
                        if ($data['question_image']!='') {
                          $survey["question_image"]=$base_url.'img/survey/'.$data['question_image'];
                        } else {
                          $survey["question_image"]="";
                        }

                        $totalVoting =  $d->count_data_direct("survey_result_id","survey_result_master","survey_id='$survey_id' AND society_id='$society_id' AND survey_question_id='$data[survey_question_id]'");
                        
                        $qs=$d->select("survey_option_master","survey_question_id='$data[survey_question_id]' AND survey_id='$data[survey_id]'");

                          if(mysqli_num_rows($qs)>0){

                            $survey["question_option"] = array();
                              while($data=mysqli_fetch_array($qs)) {

                              $singleVoting =  $d->count_data_direct("survey_result_id","survey_result_master","survey_id='$survey_id' AND society_id='$society_id' AND survey_question_id='$data[survey_question_id]' AND survey_option_id='$data[survey_option_id]'");


                              $singleVoting = $singleVoting * 100;
                              if($totalVoting != 0){
                              $votingPer = number_format($singleVoting /  $totalVoting);
                              }else{
                                $votingPer ="0";
                              }


                                $question_option = array(); 
                                $question_option["survey_option_id"]=$data['survey_option_id'];
                                $question_option["survey_option_name"]=html_entity_decode($data['survey_option_name']);
                                $question_option["votingPer"] = $votingPer;
                                array_push($survey["question_option"],$question_option); 
                              }

                            }

                        array_push($response["survey"], $survey); 
                    }

                    $totalParticiapte=$d->select("survey_result_master,unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=survey_result_master.unit_id AND survey_result_master.user_id=users_master.user_id AND survey_result_master.survey_id = '$survey_id' ","GROUP BY survey_result_master.user_id");

                    $response["message"]="Question get successfully";
                    $response["total_participant"]=mysqli_num_rows($totalParticiapte);
                    $response["status"]="200";
                    echo json_encode($response);

                }
                else{
                $response["message"]="No Question for this Survey.";
                $response["status"]="201";
                echo json_encode($response);

            }

    }else{

         $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);

    }
  }

   else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>