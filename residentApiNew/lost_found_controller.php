<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d h:i A");
            if($_POST['getListNew']=="getListNew" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


                $q=$d->select("lost_found_master","society_id='$society_id'  AND active_status='0' ","ORDER BY lost_found_master_id DESC" );
                if(mysqli_num_rows($q)>0){
                        $response["lostfound"] = array();

                    while($data=mysqli_fetch_array($q)) {

                            
                        
                            $lostfound = array(); 

                            $lostfound["lost_found_master_id"]=$data['lost_found_master_id'];
                            $lostfound["society_id"]=$data['society_id'];

                            if ($data['user_id']!=0) {
                                $qbb=$d->select("lost_found_master,users_master,block_master,unit_master","lost_found_master.society_id='$society_id'  AND lost_found_master.active_status='0' AND  lost_found_master.user_id=users_master.user_id  AND block_master.block_id=unit_master.block_id AND block_master.block_id=lost_found_master.block_id AND unit_master.unit_id=lost_found_master.unit_id AND lost_found_master.user_id!=0 AND lost_found_master.lost_found_master_id='$data[lost_found_master_id]'","" );
                                $subData=mysqli_fetch_array($qbb);
                                $lostfound["unit_id"]=$subData['unit_id'];
                                $lostfound["user_id"]=$subData['user_id'];
                                $lostfound["user_type"]=$subData['user_type'];
                                $lostfound["public_mobile"]=$subData['public_mobile'];
                                $lostfound["tenant_view"]=$subData['tenant_view'];
                                $lostfound["lost_found_title"]=html_entity_decode($data['lost_found_title']);
                                $lostfound["lost_found_description"]=html_entity_decode($data['lost_found_description']);
                                $lostfound["lost_found_date"]=$data['lost_found_date'];
                                $lostfound["block_name"]=$subData['user_designation'];
                                $lostfound["user_full_name"]=$subData['user_full_name'];
                                $lostfound["user_mobile"]=$subData['user_mobile'];
                                $lostfound["public_mobile"]=$subData['public_mobile'];
                                if ($data['lost_found_image']!="") {
                                    $lostfound["lost_found_image"]=$base_url."img/lostFound/".$data['lost_found_image'];
                                } else {
                                    $lostfound["lost_found_image"]="";
                                }
                                $lostfound["user_profile_pic"]=$base_url."img/users/recident_profile/".$subData['user_profile_pic'];
                            } else {
                                  $qbb=$d->select("lost_found_master,employee_master","lost_found_master.society_id='$society_id'  AND lost_found_master.active_status='0' AND  lost_found_master.gatekeeper_id=employee_master.emp_id  AND lost_found_master.lost_found_master_id='$data[lost_found_master_id]'","" );
                                $subData=mysqli_fetch_array($qbb);
                                
                                $lostfound["unit_id"]="0";
                                $lostfound["user_id"]=$subData['emp_id'].'';
                                $lostfound["user_type"]="0";
                                $lostfound["public_mobile"]="0";
                                $lostfound["tenant_view"]="0";
                                $lostfound["lost_found_title"]=html_entity_decode($data['lost_found_title']);
                                $lostfound["lost_found_description"]=html_entity_decode($data['lost_found_description']);
                                $lostfound["lost_found_date"]=$data['lost_found_date'];
                                $security =  $xml->string->gatekeeper;
                                $lostfound["block_name"]=$security."";
                                $lostfound["user_full_name"]=$subData['emp_name'].'';
                                $lostfound["user_mobile"]=$subData['emp_mobile'].'';
                                $lostfound["public_mobile"]="0";
                                if ($data['lost_found_image']!="") {
                                    $lostfound["lost_found_image"]=$base_url."img/lostFound/".$data['lost_found_image'];
                                } else {
                                    $lostfound["lost_found_image"]="";
                                }
                                $lostfound["user_profile_pic"]=$base_url."img/emp/".$subData['emp_profile'];
                            }

                            
                            $lost = $xml->string->lost;
                            $found = $xml->string->found;
                            
                            if($data['lost_found_type']==0)  {
                                $lostfound["lost_found_type"]="$found";
                            } else {
                                $lostfound["lost_found_type"]="$lost";
                            }
                            $lostfound["lost_found_type_int"]=$data['lost_found_type'].'';

                             if ($data['user_id']!=0) {
                                 $lostfound["is_user"]=true;
                            } else {
                                 $lostfound["is_user"]=false;
                            }
                            
                            array_push($response["lostfound"], $lostfound);


                    }
                    $response["message"]="Get List Success.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No Data Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if ($_POST['addLostFound']=="addLostFound" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

                $uploadedFile = $_FILES['lost_found_image']['tmp_name']; 
                if ($uploadedFile != "") {
                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand().$user_id;
                    $dirPath = "../img/lostFound/";
                    $ext = pathinfo($_FILES['lost_found_image']['name'], PATHINFO_EXTENSION);
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];

                    if ($imageWidth>600) {
                        $newWidthPercentage= 600*100 / $imageWidth;  //for maximum 400 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage /100;
                        $newImageHeight = $imageHeight * $newWidthPercentage /100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }

                    switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagepng($tmp,$dirPath. $newFileName. "_lf.". $ext);
                        break;           

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagejpeg($tmp,$dirPath. $newFileName. "_lf.". $ext);
                        break;
                    
                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagegif($tmp,$dirPath. $newFileName. "_lf.". $ext);
                        break;

                    default:
                        $response["message"]="Invalid Image type.";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit;
                        break;
                    }
                    $lost_found_image= $newFileName."_lf.".$ext;
                    $notiUrl= $base_url.'img/lostFound/'.$lost_found_image;

                    // $temp = explode(".", $_FILES["lost_found_image"]["name"]);
                    // $lost_found_image = 'lf_'.round(microtime(true)) . '.' . end($temp);
                    // $target = '../img/lostFound/'.$lost_found_image;
                    // move_uploaded_file($_FILES['lost_found_image']['tmp_name'], $target);
                } else {
                 $lost_found_image='';
                 $notiUrl='';
                }



                $m->set_data('society_id',$society_id);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('block_id',$block_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('lost_found_title',$lost_found_title);
                $m->set_data('lost_found_description',$lost_found_description);
                $m->set_data('lost_found_date',date('d-m-Y'));
                $m->set_data('lost_found_image',$lost_found_image);
                $m->set_data('lost_found_type',$lost_found_type);
                $m->set_data('active_status',0);

                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'block_id'=>$m->get_data('block_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'lost_found_title'=>$m->get_data('lost_found_title'),
                    'lost_found_description'=>$m->get_data('lost_found_description'),
                    'lost_found_date'=>$m->get_data('lost_found_date'),
                    'lost_found_image'=>$m->get_data('lost_found_image'),
                    'lost_found_type'=>$m->get_data('lost_found_type'),
                    'active_status'=>$m->get_data('active_status')
                );

                    $quserDataUpdate = $d->insert("lost_found_master",$a);
            
                
                if($quserDataUpdate==TRUE){
                    if ($lost_found_type==0) {
                      $notification_title= $user_name.' found an item';
                      $notification_desc= $lost_found_title;
                       $response["message"]="Found item posted successfully.";
                       $d->insert_myactivity($user_id,"$society_id","0","$user_name","Found item posted successfully","Lost-foundxxxhdpi.png");
                    
                    } else {
                      $notification_title= $user_name.' lost an item';
                      $notification_desc= $lost_found_title;
                       $response["message"]="Lost item posted successfully";
                     $d->insert_myactivity($user_id,"$society_id","0","$user_name","Lost item posted successfully","Lost-foundxxxhdpi.png");
                    
                    }

                    $title= $notification_title;
                    $description= $notification_desc;
                    $d->insertUserNotification($society_id,$title,$description,"lostfound","Lost-foundxxxhdpi.png","users_master.user_id!='$user_id'");


                       
                     $fcmArray=$d->get_android_fcm("users_master","user_id!='$user_id' AND user_token!='' AND society_id='$society_id' AND device='android'");
                     $fcmArrayIos=$d->get_android_fcm("users_master","user_id!='$user_id' AND user_token!='' AND society_id='$society_id' AND device='ios'");
                     $nResident->noti("LostAndFoundFragment",$notiUrl,$society_id,$fcmArray,$notification_title,$notification_desc,'lostfound');
                     $nResident->noti_ios("LostAndFoundFragment",$notiUrl,$society_id,$fcmArrayIos,$notification_title,$notification_desc,'lostfound');

                    
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="Something Wrong.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

                

            }else if ($_POST['updateLostFound']=="updateLostFound" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($lost_found_master_id, FILTER_VALIDATE_INT) == true) {


             
                $uploadedFile = $_FILES['lost_found_image']['tmp_name']; 
                if ($uploadedFile != "") {
                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand().$user_id;
                    $dirPath = "../img/lostFound/";
                    $ext = pathinfo($_FILES['lost_found_image']['name'], PATHINFO_EXTENSION);
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];

                    if ($imageWidth>600) {
                        $newWidthPercentage= 600*100 / $imageWidth;  //for maximum 400 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage /100;
                        $newImageHeight = $imageHeight * $newWidthPercentage /100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }

                    switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagepng($tmp,$dirPath. $newFileName. "_lf.". $ext);
                        break;           

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagejpeg($tmp,$dirPath. $newFileName. "_lf.". $ext);
                        break;
                    
                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagegif($tmp,$dirPath. $newFileName. "_lf.". $ext);
                        break;

                    default:
                        $response["message"]="Invalid Image type.";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit;
                        break;
                    }
                    $lost_found_image= $newFileName."_lf.".$ext;
                    $notiUrl= $base_url.'img/lostFound/'.$lost_found_image;

                    // $temp = explode(".", $_FILES["lost_found_image"]["name"]);
                    // $lost_found_image = 'lf_'.round(microtime(true)) . '.' . end($temp);
                    // $target = '../img/lostFound/'.$lost_found_image;
                    // move_uploaded_file($_FILES['lost_found_image']['tmp_name'], $target);
                } else {
                  $qold=$d->select("lost_found_master","lost_found_master_id='$lost_found_master_id'","" );
                  $olddata=mysqli_fetch_array($qold);


                     $lost_found_image=$olddata['lost_found_image'];
                     if ($lost_found_image!="") {
                         $notiUrl= $base_url.'img/lostFound/'.$lost_found_image;
                     } else {
                         $notiUrl='';
                        
                     }
                }

                 

                $m->set_data('society_id',$society_id);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('block_id',$block_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('lost_found_title',$lost_found_title);
                $m->set_data('lost_found_description',$lost_found_description);
                $m->set_data('lost_found_date',date('d-m-Y'));
                $m->set_data('lost_found_image',$lost_found_image);
                $m->set_data('lost_found_type',$lost_found_type);
                $m->set_data('active_status',0);

                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'block_id'=>$m->get_data('block_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'lost_found_title'=>$m->get_data('lost_found_title'),
                    'lost_found_description'=>$m->get_data('lost_found_description'),
                    'lost_found_date'=>$m->get_data('lost_found_date'),
                    'lost_found_image'=>$m->get_data('lost_found_image'),
                    'lost_found_type'=>$m->get_data('lost_found_type'),
                    'active_status'=>$m->get_data('active_status')
                );
              

                    $quserDataUpdate = $d->update("lost_found_master",$a,"lost_found_master_id='$lost_found_master_id' AND user_id='$user_id'");
            
                
                if($quserDataUpdate==TRUE){
                    if ($lost_found_type==0) {
                      $notification_title= $user_name.' found an item';
                      $notification_desc= $lost_found_title;
                       $response["message"]="Found item updated successfully.";
                       $d->insert_myactivity($user_id,"$society_id","0","$user_name","Found item posted successfully","Lost-foundxxxhdpi.png");
                    
                    } else {
                      $notification_title= $user_name.' lost an item';
                      $notification_desc= $lost_found_title;
                       $response["message"]="Lost item updated successfully";
                     $d->insert_myactivity($user_id,"$society_id","0","$user_name","Lost item posted successfully","Lost-foundxxxhdpi.png");
                    
                    }

                    $title= $notification_title;
                    $description= $notification_desc;
                    $d->insertUserNotification($society_id,$title,$description,"lostfound","Lost-foundxxxhdpi.png","users_master.user_id!='$user_id'");


                       
                     $fcmArray=$d->get_android_fcm("users_master","user_id!='$user_id' AND user_token!='' AND society_id='$society_id' AND device='android'");
                     $fcmArrayIos=$d->get_android_fcm("users_master","user_id!='$user_id' AND user_token!='' AND society_id='$society_id' AND device='ios'");
                     $nResident->noti("LostAndFoundFragment",$notiUrl,$society_id,$fcmArray,$notification_title,$notification_desc,'lostfound');
                     $nResident->noti_ios("LostAndFoundFragment",$notiUrl,$society_id,$fcmArrayIos,$notification_title,$notification_desc,'lostfound');

                    
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="Something Wrong.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

                

            }else if ($_POST['deleteLostFound']=="deleteLostFound") {
                
                $a = array('active_status' =>1);

                $qc=$d->selectRow('lost_found_type',"lost_found_master","lost_found_master_id='$lost_found_master_id'");
                $empData=mysqli_fetch_array($qc);
                $lost_found_type = $empData['lost_found_type'];
                if ($lost_found_type==0) {
                    $type = "Found";
                } else {
                    $type = "Lost";
                }

                $quserDataUpdate = $d->update("lost_found_master",$a,"lost_found_master_id='$lost_found_master_id' AND user_id='$user_id'");
            
                if($quserDataUpdate==TRUE){
                     $d->insert_myactivity($user_id,"$society_id","0","$user_name","$type item deleted successfully","Lost-foundxxxhdpi.png");
                    $response["message"]="Deleted Successfully.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                        $response["message"]="deletion failed.";
                        $response["status"]="201";
                        echo json_encode($response);

                }

                

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>