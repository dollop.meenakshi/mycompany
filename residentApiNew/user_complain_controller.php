<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){



    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d h:i A");
        
            if($_POST['getComplain']=="getComplain" && $unit_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ){


                $qcomplain=$d->select("complains_master","society_id='$society_id' AND unit_id='$unit_id' AND flag_delete='0' AND user_id='$user_id'","ORDER BY complain_id DESC" );
                if(mysqli_num_rows($qcomplain)>0){
                        $response["complain"] = array();

                    while($data_complain_list=mysqli_fetch_array($qcomplain)) {

                            $complain = array(); 

                            $complain["complain_id"]=$data_complain_list['complain_id'];
                            $complain["society_id"]=$data_complain_list['society_id'];
                            $complain["complaint_category"]=$data_complain_list['complaint_category'];
                            $complain["compalain_title"]=$data_complain_list['compalain_title'];
                            $complain["complain_description"]=html_entity_decode($data_complain_list['complain_description']);
                            $complain["complain_date"]=$data_complain_list['complain_date'];
                            $complain["complain_status"]=$data_complain_list['complain_status'];
                            $complain["complain_assing_to"]=$data_complain_list['complain_assing_to'];
                            $complain["complain_photo"]=$base_url."img/complain/".$data_complain_list['complain_photo'];
                            $complain["complain_photo_old"]=$data_complain_list['complain_photo'];
                            $complain["complaint_voice_old"]=$data_complain_list['complaint_voice'].'';
                            if($data_complain_list['complaint_voice']!='')  {
                                $complain["complaint_voice"]=$base_url."img/complain/".$data_complain_list['complaint_voice'];
                            } else {
                                $complain["complaint_voice"]="";
                            }
                            $complain["complain_review_msg"]=html_entity_decode($data_complain_list['complain_review_msg']);
                            $complain["feedback_msg"]=$data_complain_list['feedback_msg'].'';
                            
                            array_push($response["complain"], $complain);


                    }
                    $response["message"]="Get complaint success.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No complaint Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if ($_POST['addComplain']=="addComplain" && $unit_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

                $file11=$_FILES["complaint_voice"]["tmp_name"];
                if(file_exists($file11)) {
                    // $complaint_voice = $_FILES['complaint_voice']['name'];
                    $complaint_voice = rand(). "." ."mp3";
                    $target = '../img/complain/'.$complaint_voice;
                    move_uploaded_file($_FILES['complaint_voice']['tmp_name'], $target);
                } else {
                 $complaint_voice='';
                }

                $m->set_data('society_id',$society_id);
                $m->set_data('compalain_title',$compalain_title);
                $m->set_data('complain_description',$complain_description);
                $m->set_data('complaint_category',$complaint_category);
                $m->set_data('complaint_voice',$complaint_voice);
                $m->set_data('complain_date',$temDate);
                $m->set_data('complain_status',$complain_status);
                $m->set_data('complain_assing_to',$complain_assing_to);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('user_id',$user_id);

                if ($complain_photo != "") {
                    $ddd = date("ymdhi");
                    $profile_name = "Complain_" . $ddd . '.png';
                    define('UPLOAD_DIR', '../img/complain/');
                    $img = $complain_photo;
                    $img = str_replace('data: img/app/png;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);
                    $file = UPLOAD_DIR . $profile_name;
                    $success = file_put_contents($file, $data);

                    $notiUrl=$base_url.'img/complain/'.$profile_name;
                } else {
                    $profile_name = "";
                    $notiUrl="";
                }

                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'complain_photo'=>$profile_name,
                    'complaint_category'=>$m->get_data('complaint_category'),
                    'complaint_voice'=>$m->get_data('complaint_voice'),
                    'compalain_title'=>$m->get_data('compalain_title'),
                    'complain_description'=>$m->get_data('complain_description'),
                    'complain_date'=>$m->get_data('complain_date'),
                    'complain_status'=>$m->get_data('complain_status'),
                    'complain_assing_to'=>$m->get_data('complain_assing_to'));

                    $quserDataUpdate = $d->insert("complains_master",$a);
            
                
                if($quserDataUpdate==TRUE){


                    $qSecretaryToken=$d->select("bms_admin_master,role_master","bms_admin_master.admin_active_status=0 AND role_master.role_id=bms_admin_master.role_id AND bms_admin_master.society_id='$society_id' ");  

                    while($data_Secretary=mysqli_fetch_array($qSecretaryToken)) {

                        if ($data_Secretary['complaint_category_id']!='') {

                            $comCatIdAry = explode(",",$data_Secretary['complaint_category_id']);
                            if(in_array($complaint_category, $comCatIdAry)){
                              $sos_Secretary_token=$data_Secretary['token'];
                              if ($data_Secretary['device']=='android') {
                                $nAdmin->noti_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint added by ".$user_name,$compalain_title,'complaints');
                              } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint added by ".$user_name,$compalain_title,'complaints');
                              }
                                    
                            }
                        } else {
                            $sos_Secretary_token=$data_Secretary['token'];
                            if ($data_Secretary['device']=='android') {
                            $nAdmin->noti_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint added by ".$user_name,$compalain_title,'complaints');
                            } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint added by ".$user_name,$compalain_title,'complaints');
                            }
                            
                        }
                        
                        $notiAry = array(
                              'society_id'=>$society_id,
                              'admin_id'=>$data_Secretary['admin_id'],
                              'notification_tittle'=>"Complaint added by ".$user_name,
                              'notification_description'=>$compalain_title,
                              'notifiaction_date'=>date('Y-m-d H:i'),
                              'notification_action'=>'Complain',
                              'admin_click_action'=>'complaints',
                              'notification_logo'=>'complain.png',
                        );
                        $d->insert("admin_notification",$notiAry);
                    }
                              
      

                    $response["message"]="Complaint Register Successfully.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                        $response["message"]="No Complaint Added.";
                        $response["status"]="201";
                        echo json_encode($response);

                }

                

            } else if ($_POST['addComplainNew']=="addComplainNew" && $unit_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ) {

                $file11=$_FILES["complaint_voice"]["tmp_name"];
                if(file_exists($file11)) {
                    // $complaint_voice = $_FILES['complaint_voice']['name'];
                    $complaint_voice = rand(). "." ."mp3";
                    $target = '../img/complain/'.$complaint_voice;
                    move_uploaded_file($_FILES['complaint_voice']['tmp_name'], $target);
                } else {
                 $complaint_voice='';
                }

                $uploadedFile=$_FILES["complain_photo"]["tmp_name"];
                $ext = pathinfo($_FILES['complain_photo']['name'], PATHINFO_EXTENSION);
                if(file_exists($uploadedFile)) {
                   
                      $sourceProperties = getimagesize($uploadedFile);
                      $newFileName = rand().$user_id;
                      $dirPath = "../img/complain/";
                      $imageType = $sourceProperties[2];
                      $imageHeight = $sourceProperties[1];
                      $imageWidth = $sourceProperties[0];
                      if ($imageWidth>1000) {
                        $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage /100;
                        $newImageHeight = $imageHeight * $newWidthPercentage /100;
                      } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                      }



                      switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagepng($tmp,$dirPath. $newFileName. "_complain.". $ext);
                            break;           

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagejpeg($tmp,$dirPath. $newFileName. "_complain.". $ext);
                            break;
                        
                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagegif($tmp,$dirPath. $newFileName. "_complain.". $ext);
                            break;

                        default:
                          
                            break;
                        }
                    $complain_photo= $newFileName."_complain.".$ext;

                    $notiUrl=$base_url.'img/complain/'.$complain_photo;
                } else {
                 $complain_photo='';
                 $notiUrl="";
                }

                $m->set_data('society_id',$society_id);
                $m->set_data('compalain_title',$compalain_title);
                $m->set_data('complain_description',$complain_description);
                $m->set_data('complaint_category',$complaint_category);
                $m->set_data('complaint_voice',$complaint_voice);
                $m->set_data('complain_date',$temDate);
                $m->set_data('complain_status',$complain_status);
                $m->set_data('complain_assing_to',$complain_assing_to);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('user_id',$user_id);

               
                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'complain_photo'=>$complain_photo,
                    'complaint_category'=>$m->get_data('complaint_category'),
                    'complaint_voice'=>$m->get_data('complaint_voice'),
                    'compalain_title'=>$m->get_data('compalain_title'),
                    'complain_description'=>$m->get_data('complain_description'),
                    'complain_date'=>$m->get_data('complain_date'),
                    'complain_status'=>$m->get_data('complain_status'),
                    'complain_assing_to'=>$m->get_data('complain_assing_to'));

                    $quserDataUpdate = $d->insert("complains_master",$a);
            
                
                if($quserDataUpdate==TRUE){


                    $qSecretaryToken=$d->select("bms_admin_master,role_master","bms_admin_master.admin_active_status=0 AND role_master.role_id=bms_admin_master.role_id AND bms_admin_master.society_id='$society_id' ");  

                    while($data_Secretary=mysqli_fetch_array($qSecretaryToken)) {

                        if ($data_Secretary['complaint_category_id']!='') {

                            $comCatIdAry = explode(",",$data_Secretary['complaint_category_id']);
                            if(in_array($complaint_category, $comCatIdAry)){
                              $sos_Secretary_token=$data_Secretary['token'];
                              if ($data_Secretary['device']=='android') {
                                $nAdmin->noti_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint added by ".$user_name,$compalain_title,'complaints');
                              } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint added by ".$user_name,$compalain_title,'complaints');
                              }
                                    
                            }
                        } else {
                            $sos_Secretary_token=$data_Secretary['token'];
                            if ($data_Secretary['device']=='android') {
                            $nAdmin->noti_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint added by ".$user_name,$compalain_title,'complaints');
                            } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint added by ".$user_name,$compalain_title,'complaints');
                            }
                            
                        }
                        
                        $notiAry = array(
                              'society_id'=>$society_id,
                              'admin_id'=>$data_Secretary['admin_id'],
                              'notification_tittle'=>"Complaint added by ".$user_name,
                              'notification_description'=>$compalain_title,
                              'notifiaction_date'=>date('Y-m-d H:i'),
                              'notification_action'=>'Complain',
                              'admin_click_action'=>'complaints',
                               'notification_logo'=>'complain.png',
                        );
                        $d->insert("admin_notification",$notiAry);
                    }
                              
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","New complaint register","complain.png");
                    

                    $response["message"]="Complaint Register Successfully.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                        $response["message"]="No Complaint Added.";
                        $response["status"]="201";
                        echo json_encode($response);

                }

                

            }else if ($_POST['getFeedback']=="getFeedback" && filter_var($complain_id, FILTER_VALIDATE_INT) == true ) {
                
                $a = array('feedback_msg' =>$feedback_msg);

                $quserDataUpdate = $d->update("complains_master",$a,"complain_id='$complain_id'");
            
                if($quserDataUpdate==TRUE){
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","Complaint feedback added","complain.png");

                    $response["message"]="Feedback Register Successfully.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                        $response["message"]="Something Wrong";
                        $response["status"]="201";
                        echo json_encode($response);

                }

                

            }else if ($_POST['deleteComplain']=="deleteComplain" && filter_var($complain_id, FILTER_VALIDATE_INT) == true ) {
                
                $a = array('flag_delete' =>1);

                $quserDataUpdate = $d->update("complains_master",$a,"complain_id='$complain_id'");
            
                if($quserDataUpdate==TRUE){
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","Complaint deleted","complain.png");

                    $response["message"]="Deleted Successfully";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                        $response["message"]="Something went wrong";
                        $response["status"]="201";
                        echo json_encode($response);

                }

                

            } else if ($_POST['editComplain']=="editComplain" && filter_var($complain_id, FILTER_VALIDATE_INT) == true ) {
                
                $file11=$_FILES["complaint_voice"]["tmp_name"];
                if(file_exists($file11)) {
                    // $complaint_voice = $_FILES['complaint_voice']['name'];
                    $complaint_voice = rand(). "." ."mp3";
                    $target = '../img/complain/'.$complaint_voice;
                    move_uploaded_file($_FILES['complaint_voice']['tmp_name'], $target);
                } else {
                 $complaint_voice=$complaint_voice_old;
                }


                if ($complain_photo != "") {
                    $ddd = date("ymdhi");
                    $profile_name = "Complain_" . $ddd . '.png';
                    define('UPLOAD_DIR', '../img/complain/');
                    $img = $complain_photo;
                    $img = str_replace('data: img/app/png;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);
                    $file = UPLOAD_DIR . $profile_name;
                    $success = file_put_contents($file, $data);

                    $notiUrl=$base_url.'img/complain/'.$profile_name;
                } else {
                    $profile_name = $complain_photo_old;
                    $notiUrl="";
                }

                $m->set_data('society_id',$society_id);
                $m->set_data('compalain_title',$compalain_title);
                $m->set_data('complain_description',$complain_description);
                $m->set_data('complaint_category',$complaint_category);
                $m->set_data('complaint_voice',$complaint_voice);
                $m->set_data('complain_status','2');
                $m->set_data('complain_assing_to',$complain_assing_to);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('user_id',$user_id);


                 $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'complain_photo'=>$profile_name,
                    'complaint_category'=>$m->get_data('complaint_category'),
                    'complaint_voice'=>$m->get_data('complaint_voice'),
                    'compalain_title'=>$m->get_data('compalain_title'),
                    'complain_description'=>$m->get_data('complain_description'),
                    'complain_status'=>$m->get_data('complain_status'),
                    // 'complain_assing_to'=>$m->get_data('complain_assing_to')
                );

                $quserDataUpdate = $d->update("complains_master",$a,"complain_id='$complain_id'");
            
                
                if($quserDataUpdate==TRUE){

                    $qSecretaryToken=$d->select("bms_admin_master,role_master","bms_admin_master.admin_active_status=0 AND role_master.role_id=bms_admin_master.role_id AND bms_admin_master.society_id='$society_id' ");  

                    while($data_Secretary=mysqli_fetch_array($qSecretaryToken)) {

                        if ($data_Secretary['complaint_category_id']!='') {

                            $comCatIdAry = explode(",",$data_Secretary['complaint_category_id']);
                            if(in_array($complaint_category, $comCatIdAry)){
                              $sos_Secretary_token=$data_Secretary['token'];
                              if ($data_Secretary['device']=='android') {
                                $nAdmin->noti_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint Reopen by ".$user_name,$compalain_title,'complaints');
                              } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint Reopen by ".$user_name,$compalain_title,'complaints');
                              }
                                    
                            }
                        } else {
                            $sos_Secretary_token=$data_Secretary['token'];
                            if ($data_Secretary['device']=='android') {
                            $nAdmin->noti_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint Reopen by ".$user_name,$compalain_title,'complaints');
                            } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint Reopen by ".$user_name,$compalain_title,'complaints');
                            }
                            
                        }
                        
                        $notiAry = array(
                              'society_id'=>$society_id,
                              'admin_id'=>$data_Secretary['admin_id'],
                              'notification_tittle'=>"Complaint Reopen by".$user_name,
                              'notification_description'=>$compalain_title,
                              'notifiaction_date'=>date('Y-m-d H:i'),
                              'notification_action'=>'Complain',
                              'admin_click_action'=>'complaints',
                               'notification_logo'=>'complain.png',
                        );
                        $d->insert("admin_notification",$notiAry);
                    }

                    $response["message"]="Complain Reopen Successfully.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                        $response["message"]="update failed.";
                        $response["status"]="201";
                        echo json_encode($response);

                }

                

            } else if ($_POST['editComplainNew']=="editComplainNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true ) {
                
                $file11=$_FILES["complaint_voice"]["tmp_name"];
                if(file_exists($file11)) {
                    // $complaint_voice = $_FILES['complaint_voice']['name'];
                    $complaint_voice = rand(). "." ."mp3";
                    $target = '../img/complain/'.$complaint_voice;
                    move_uploaded_file($_FILES['complaint_voice']['tmp_name'], $target);
                } else {
                 $complaint_voice=$complaint_voice_old;
                }


               $uploadedFile=$_FILES["complain_photo"]["tmp_name"];
                if(file_exists($uploadedFile)) {
                   $ext = pathinfo($_FILES['complain_photo']['name'], PATHINFO_EXTENSION);
                   
                      $sourceProperties = getimagesize($uploadedFile);
                      $newFileName = rand().$user_id;
                      $dirPath = "../img/complain/";
                      $imageType = $sourceProperties[2];
                      $imageHeight = $sourceProperties[1];
                      $imageWidth = $sourceProperties[0];
                      if ($imageWidth>1000) {
                        $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage /100;
                        $newImageHeight = $imageHeight * $newWidthPercentage /100;
                      } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                      }



                      switch ($imageType) {

                        case IMAGETYPE_PNG:
                            $imageSrc = imagecreatefrompng($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagepng($tmp,$dirPath. $newFileName. "_complain.". $ext);
                            break;           

                        case IMAGETYPE_JPEG:
                            $imageSrc = imagecreatefromjpeg($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagejpeg($tmp,$dirPath. $newFileName. "_complain.". $ext);
                            break;
                        
                        case IMAGETYPE_GIF:
                            $imageSrc = imagecreatefromgif($uploadedFile); 
                            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            imagegif($tmp,$dirPath. $newFileName. "_complain.". $ext);
                            break;

                        default:
                          
                            break;
                        }
                    $complain_photo= $newFileName."_complain.".$ext;

                    $notiUrl=$base_url.'img/complain/'.$complain_photo;
                } else {
                 $complain_photo=$complain_photo_old;
                 $notiUrl="";
                }
                switch ($complaint_status) {
                    case '0':
                        $complain_status_view="Reply";
                        break;
                    case '1':
                        $complain_status_view="Closed";
                        break;
                    case '2':
                        $complain_status_view="Re-Open";
                        break;
                    default:
                        # code...
                        break;
                }
                $m->set_data('society_id',$society_id);
                $m->set_data('compalain_title',$compalain_title);
                $m->set_data('complain_description',$complain_description);
                $m->set_data('complaint_category',$complaint_category);
                $m->set_data('complaint_voice',$complaint_voice);
                if ($complaint_status!='') {
                    $m->set_data('complain_status',$complaint_status);
                } else {
                    $m->set_data('complain_status','2');
                }
                $m->set_data('complain_assing_to',$complain_assing_to);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('user_id',$user_id);


                 $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'complain_photo'=>$complain_photo,
                    'complaint_category'=>$m->get_data('complaint_category'),
                    'complaint_voice'=>$m->get_data('complaint_voice'),
                    'compalain_title'=>$m->get_data('compalain_title'),
                    'complain_description'=>$m->get_data('complain_description'),
                    'complain_status'=>$m->get_data('complain_status'),
                    // 'complain_assing_to'=>$m->get_data('complain_assing_to')
                );

                $quserDataUpdate = $d->update("complains_master",$a,"complain_id='$complain_id'");
            
                
                if($quserDataUpdate==TRUE){

                    $qSecretaryToken=$d->select("bms_admin_master,role_master","bms_admin_master.admin_active_status=0 AND role_master.role_id=bms_admin_master.role_id AND bms_admin_master.society_id='$society_id' ");  

                    while($data_Secretary=mysqli_fetch_array($qSecretaryToken)) {

                        if ($data_Secretary['complaint_category_id']!='') {

                            $comCatIdAry = explode(",",$data_Secretary['complaint_category_id']);
                            if(in_array($complaint_category, $comCatIdAry)){
                              $sos_Secretary_token=$data_Secretary['token'];
                              if ($data_Secretary['device']=='android') {
                                $nAdmin->noti_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint $complain_status_view by ".$user_name,$compalain_title,'complaints');
                              } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint $complain_status_view by ".$user_name,$compalain_title,'complaints');
                              }
                                    
                            }
                        } else {
                            $sos_Secretary_token=$data_Secretary['token'];
                            if ($data_Secretary['device']=='android') {
                            $nAdmin->noti_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint $complain_status_view by ".$user_name,$compalain_title,'complaints');
                            } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl,$sos_Secretary_token,"Complaint $complain_status_view by ".$user_name,$compalain_title,'complaints');
                            }
                            
                        }
                        
                        $notiAry = array(
                              'society_id'=>$society_id,
                              'admin_id'=>$data_Secretary['admin_id'],
                              'notification_tittle'=>"Complaint $complain_status_view by".$user_name,
                              'notification_description'=>$compalain_title,
                              'notifiaction_date'=>date('Y-m-d H:i'),
                              'notification_action'=>'Complain',
                              'admin_click_action'=>'complaints',
                               'notification_logo'=>'complain.png',
                        );
                        $d->insert("admin_notification",$notiAry);
                    }

                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","Complaint $complain_status_view by you","complain.png");
                    

                    $response["message"]="Complain $complain_status_view Successfully.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                        $response["message"]="update failed.";
                        $response["status"]="201";
                        echo json_encode($response);

                }

                

            }else if($_POST['getComplainCategory']=="getComplainCategory"){


                $qcomplain=$d->select("complaint_category","active_status=0" );
                if(mysqli_num_rows($qcomplain)>0){
                        $response["complain_category"] = array();

                    while($data=mysqli_fetch_array($qcomplain)) {

                            $complain_category = array(); 
                            $complain_category["complaint_category_id"]=$data['complaint_category_id'];
                            $complain_category["category_name"]=html_entity_decode($data['category_name']);
                            
                            array_push($response["complain_category"], $complain_category);


                    }
                    $response["message"]="Get Complaint Category Success.";
                    $response["audio_duration"]=30000; //30 second
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No Categoty Found.";
                    $response["audio_duration"]=30000;
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>