<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/
 
if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        $startDate=date("Y-m-01");
        $currentDate=date("Y-m-d");
        $lastDate=date("Y-m-t");

        if($_POST['addMocklocationData']=="addMocklocationData" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('user_mobile',$user_mobile);
            $m->set_data('mock_latitude',$mock_latitude);
            $m->set_data('mock_longitude',$mock_longitude);
            $m->set_data('vertical_accuracy',$vertical_accuracy);
            $m->set_data('horizontal_accuracy',$horizontal_accuracy);
            $m->set_data('altitude',$altitude);
            $m->set_data('mock_address',$mock_address);
            $m->set_data('mock_speed',$mock_speed);
            $m->set_data('mock_time',date('Y-m-d H:i:s'));

            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'user_id'=>$m->get_data('user_id'),
                'user_mobile'=>$m->get_data('user_mobile'),
                'mock_latitude'=>$m->get_data('mock_latitude'),
                'mock_longitude'=>$m->get_data('mock_longitude'),
                'vertical_accuracy'=>$m->get_data('vertical_accuracy'),
                'horizontal_accuracy'=>$m->get_data('horizontal_accuracy'),
                'altitude'=>$m->get_data('altitude'),
                'mock_address'=>$m->get_data('mock_address'),
                'mock_speed'=>$m->get_data('mock_speed'),
                'mock_time'=>$m->get_data('mock_time'),
            );

            $qry = $d->insert("mock_location_master",$a);

            if ($qry == true) {
            
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }
                 
        }else if($_POST['getAttendanceTypeNew']=="getAttendanceTypeNew" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            $attendance_id = 0;

            $today_date = date('Y-m-d');
            $dateTimeCurrent = date('Y-m-d H:i:s');            

            $shiftQry = $d->selectRow("shift_timing_master.*,users_master.user_id,users_master.attendance_with_developer_mode_on,users_master.track_user_location","users_master,shift_timing_master","users_master.shift_time_id = shift_timing_master.shift_time_id AND users_master.user_id = '$user_id' AND shift_timing_master.is_deleted = '0'");

            $shiftData = mysqli_fetch_array($shiftQry);
            $shiftTimeId = $shiftData['shift_time_id'];
            $shift_type = $shiftData['shift_type'];
            $per_day_hour = $shiftData['per_day_hour'];
            $shift_start_time = $shiftData['shift_start_time'];
            $shift_end_time = $shiftData['shift_end_time'];
            $is_multiple_punch_in = $shiftData['is_multiple_punch_in'];
            $max_shift_hour = $shiftData['max_shift_hour'];
            $max_hour = $shiftData['max_hour'];
            $max_punch_out_time = $shiftData['max_punch_out_time'];
            $attendance_with_developer_mode_on = $shiftData['attendance_with_developer_mode_on'];
            $track_user_location = $shiftData['track_user_location'];

            if ($attendance_with_developer_mode_on == 0) {
                $response['attendance_with_developer_mode_on'] = true;
            }else{
                $response['attendance_with_developer_mode_on'] = false;
            }

            if ($track_user_location == 1) {
                $response['track_user_location'] = true;
            }else{
                $response['track_user_location'] = false;
            }

            $parts1 = explode(':', $per_day_hour);
            $per_day_hour_minutes = ($parts1[0]*60) + ($parts1[1]) + ($parts1[2]/60);

            if ($shiftData['take_out_of_range_reason'] == 1) {
                $take_out_of_range_reason = true;
            }else{
                $take_out_of_range_reason = false;
            }

            $response['take_out_of_range_reason'] = $take_out_of_range_reason;

            if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                $isMaxTimeSet = true;
            } else if ($max_shift_hour != '' && $max_shift_hour!='00:00:00') {
                $avarageDay = round($max_shift_hour/24);
                $isMaxHoursSet = true;
            }

            if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                $checkNextDayDate = true;
            }

            if ($max_shift_hour != '' && $max_shift_hour != '00:00:00') {
                $parts = explode(':', $max_shift_hour);
                $max_shift_hour_minutes = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
            }else{
                $max_shift_hour_minutes = 0;
            }

            $defaultOutTimeTemp = $today_date.' '.$shift_end_time;

            $response['already_punch_in'] = false;
            $response['already_punch_out'] = false;
            $response['work_report_added'] = false;
            $response['is_attendance_rejected'] = false;
            $response['attendance_rejected_message'] = "";

            $curDate = date('Y-m-d'); 
            $curTime = date('H:i:s'); 

            $oldQ = false;
            $alrdyOut = false;

            $attendance_date_start = date('Y-m-d');  
            

            if ($is_multiple_punch_in == 0) {
            
                $appQry = " AND attendance_date_end = '0000-00-00'";
                    
                if ($shift_type == "Day") {

                    $appQry = " AND (attendance_date_end = '0000-00-00' OR attendance_date_end = '$attendance_date_start')";

                    $response['night_shift'] = false;  

                    $isAlreadyPunchOut = $d->selectRow("attendance_id,attendance_status,punch_in_time,punch_out_time,attendance_date_end","attendance_master","user_id = '$user_id' AND attendance_date_start = '$attendance_date_start' AND attendance_date_end != '0000-00-00' ","ORDER BY attendance_date_start DESC LIMIT 1");

                    $ddata = mysqli_fetch_array($isAlreadyPunchOut);
                    $tempEndDate = $ddata['attendance_date_end'].' '.$ddata['punch_out_time'];

                    if (mysqli_num_rows($isAlreadyPunchOut) > 0) {
                        $response['already_punch_out'] = true;
                        $alrdyOut = true;
                        $response['already_punch_out_msg'] = "Already punched out on ".date('h:i A, d M Y',strtotime($tempEndDate));
                    }else{
                        $response['already_punch_out'] = false;
                        $response['already_punch_out_msg'] = "";
                    }

                    $dataP1 = mysqli_fetch_array($isAlreadyPunchOut);

                    if ($dataP1['attendance_status'] == 2) {
                        $response['is_attendance_rejected'] = true;
                        $response['attendance_rejected_message'] = "Your today's attendance was rejected";
                    }else{
                        $response['is_attendance_rejected'] = false;
                        $response['attendance_rejected_message'] = "";
                    }
                    
                    if ($isMaxHoursSet==true) {

                        if ($avarageDay<1) {
                            $avarageDay =1;
                        }
                        $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start BETWEEN  '$date2DayPre' AND '$attendance_date_start') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date BETWEEN '$date2DayPre' AND '$attendance_date_start') AND society_id = '$society_id'");
                    } else if ($isMaxTimeSet==true) {
                       
                        $date2DayPre = date("Y-m-d",strtotime("-1 days"));

                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start = '$attendance_date_start' OR attendance_date_start = '$date2DayPre')  AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC LIMIT 1");


                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date = '$attendance_date_start' OR work_report_date = '$date2DayPre') AND society_id = '$society_id'");
                    } else {
                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_start = '$attendance_date_start' AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND work_report_date = '$attendance_date_start' AND society_id = '$society_id'");
                    }

                    if ($workReportQry > 0) {
                        $response['work_report_added'] = true;
                    }

                    if (strlen($attendance_id) > 0 && $attendance_id != '0') {
                        $lastAttQry = $d->select("attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id' AND attendance_status != '2'");

                        $getTodayAttendace = $d->select("attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id'  AND attendance_status != '2'");

                        $lastAttData = mysqli_fetch_array($lastAttQry);

                        $total_working_minutes = $lastAttData['total_working_minutes'];
                        $last_punch_in_date = $lastAttData['last_punch_in_date'];
                        $last_punch_in_time = $lastAttData['last_punch_in_time'];

                        $totalHours = getTotalHours($last_punch_in_date,$curDate,$last_punch_in_time,$curTime);

                        $time = explode(':', $totalHours.":00");
                        $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                        $finalMin = $total_working_minutes + $total_minutes;

                        if ($finalMin > $maxTimeMinutes) {
                            $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_end = '0000-00-00' AND attendance_date_start = '0000-00-00' AND attendance_status != '2'","ORDER BY attendance_date_start DESC LIMIT 1");
                        }
                    }

                }else{

                    $response['night_shift'] = true;

                    $attDate = date('Y-m-d');

                    $isAlreadyPunchOutToday = $d->selectRow("attendance_id,attendance_status,attendance_date_start,punch_in_time,punch_out_time,attendance_date_end","attendance_master","user_id = '$user_id' AND attendance_date_start = '$today_date' AND attendance_date_end = '$today_date' AND attendance_status != '2'","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");


                    $dataP2 = mysqli_fetch_array($isAlreadyPunchOutToday);    
                    if (mysqli_num_rows($isAlreadyPunchOutToday) > 0) {

                        $tempStartDate = $dataP2['attendance_date_start'];
                        $tempEndDate = $dataP2['attendance_date_end'].' '.$dataP2['punch_out_time'];
                        $tempPunchInTime = $dataP2['punch_in_time'];

                        if ($tempPunchInTime > $shift_start_time) {
                            $isFirstTimeEnd = true;
                            $tempStartDate = date('Y-m-d',strtotime("+1 days",strtotime($tempStartDate)));
                        }

                        $approxShiftEndTime = $tempStartDate.' '.$shift_end_time;
                        $finalMin = (1440 - $per_day_hour_minutes)/2;
                        
                        if ($finalMin < 1) {
                            $finalMin = 1;
                        }

                        $approxShiftEndTimeNew = date('Y-m-d H:i:s',strtotime("+$finalMin minutes",strtotime($approxShiftEndTime)));
                        $approxShiftEndTimeNewTimeString = strtotime($approxShiftEndTimeNew);
                        $punchInDateTimeString = strtotime($dateTimeCurrent);

                        if ($punchInDateTimeString > $approxShiftEndTimeNewTimeString) {
                           $response['already_punch_out'] = false;
                           $response['already_punch_out_msg'] = "";
                        }else{
                            $response['already_punch_out'] = true;
                            $alrdyOut = true;
                            $response['already_punch_out_msg'] = "Already punched out on ".date('h:i A, d M Y',strtotime($tempEndDate));
                        }

                    }else{
                        $response['already_punch_out'] = false;
                        $response['already_punch_out_msg'] = "";
                    }


                    if ($dataP2['attendance_status'] == 2) {
                        $response['is_attendance_rejected'] = true;
                        $response['attendance_rejected_message'] = "Your today's attendance was rejected";
                    }else{
                        $response['is_attendance_rejected'] = false;
                        $response['attendance_rejected_message'] = "";
                    }

                    $attendance_date_start = date('Y-m-d',strtotime('-1 days'));
                    $aDt = date('Y-m-d');

                    if ($isMaxHoursSet==true) {

                        if ($avarageDay<1) {
                            $avarageDay =1;
                        }

                        $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start BETWEEN  '$date2DayPre' AND '$aDt') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC  LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date BETWEEN '$date2DayPre' AND '$aDt') AND society_id = '$society_id'","ORDER BY work_report_date DESC LIMIT 1");
                    } else if ($isMaxTimeSet==true) {
                       
                        $date2DayPre = date("Y-m-d",strtotime("-1 days"));

                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start = '$attendance_date_start' OR attendance_date_start = '$date2DayPre') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC  LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date = '$attendance_date_start' OR work_report_date = '$date2DayPre') AND society_id = '$society_id'","ORDER BY work_report_date DESC LIMIT 1");
                    }  else {

                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start BETWEEN  '$attendance_date_start' AND '$aDt') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date BETWEEN '$attendance_date_start' AND '$aDt') AND society_id = '$society_id'","ORDER BY work_report_date DESC LIMIT 1");

                    }

                    if ($workReportQry > 0) {
                        $response['work_report_added'] = true;
                    }  

                    if (strlen($attendance_id) > 0 && $attendance_id != '0') {
                        $lastAttQry = $d->select("attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id' AND attendance_status != '2'");

                        $getTodayAttendace = $d->select("attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id' AND attendance_status != '2'");

                        $lastAttData = mysqli_fetch_array($lastAttQry);

                        $attDate = $lastAttData['attendance_date_start'];
                        $total_working_minutes = $lastAttData['total_working_minutes'];
                        $last_punch_in_date = $lastAttData['last_punch_in_date'];
                        $last_punch_in_time = $lastAttData['last_punch_in_time']; 

                        $totalHours = getTotalHours($last_punch_in_date,date('Y-m-d'),$last_punch_in_time,date('H:i:s'));

                        $time = explode(':', $totalHours.":00");
                        $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                        $finalMin = $total_working_minutes + $total_minutes;
                    }          
                }
            }else{
                $appQry = "";
                    
                if ($shift_type == "Day") { 

                    $response['night_shift'] = false;  

                    $isAlreadyPunchOut = $d->selectRow("attendance_id,attendance_status,punch_in_time,punch_out_time,attendance_date_end","attendance_master","user_id = '$user_id' AND attendance_date_start = '$attendance_date_start'","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                    $dataP1 = mysqli_fetch_array($isAlreadyPunchOut);


                    if ($dataP1['attendance_status'] == 2) {
                        $response['is_attendance_rejected'] = true;
                        $response['attendance_rejected_message'] = "Your today's attendance was rejected";
                    }else{
                        $response['is_attendance_rejected'] = false;
                        $response['attendance_rejected_message'] = "";
                    }
                    
                    if ($isMaxHoursSet==true) {


                        if ($avarageDay<1) {
                            $avarageDay =1;
                        }
                        $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start BETWEEN  '$date2DayPre' AND '$attendance_date_start') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date BETWEEN '$date2DayPre' AND '$attendance_date_start') AND society_id = '$society_id'");
                    } else if ($isMaxTimeSet==true) {
                        $date2DayPre = date("Y-m-d",strtotime("-1 days"));

                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start = '$attendance_date_start' OR attendance_date_start = '$date2DayPre')  AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date = '$attendance_date_start' OR work_report_date = '$date2DayPre') AND society_id = '$society_id'");
                    } else {
                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_start = '$attendance_date_start' AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND work_report_date = '$attendance_date_start' AND society_id = '$society_id'");
                    }

                    if ($workReportQry > 0) {
                        $response['work_report_added'] = true;
                    }

                    if (strlen($attendance_id) > 0 && $attendance_id != '0') {
                        $lastAttQry = $d->select("attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id' AND attendance_status != '2'");

                        $getTodayAttendace = $d->select("attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id'  AND attendance_status != '2'");

                        $lastAttData = mysqli_fetch_array($lastAttQry);

                        $total_working_minutes = $lastAttData['total_working_minutes'];
                        $last_punch_in_date = $lastAttData['last_punch_in_date'];
                        $last_punch_in_time = $lastAttData['last_punch_in_time'];

                        $totalHours = getTotalHours($last_punch_in_date,$curDate,$last_punch_in_time,$curTime);

                        $time = explode(':', $totalHours.":00");
                        $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                        $finalMin = $total_working_minutes + $total_minutes;

                        if ($finalMin > $maxTimeMinutes) {
                            $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_end = '0000-00-00' AND attendance_date_start = '0000-00-00' AND attendance_status != '2'","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");
                        }
                    }

                }else{
                    $response['night_shift'] = true;

                    $attDate = date('Y-m-d');

                    $isAlreadyPunchOutToday = $d->selectRow("attendance_id,attendance_status,attendance_date_start,punch_in_time,punch_out_time,attendance_date_end","attendance_master","user_id = '$user_id' AND attendance_date_start = '$today_date' AND attendance_date_end = '$today_date' AND attendance_status != '2'","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                    $ddata = mysqli_fetch_array($isAlreadyPunchOutToday);
                    $tempEndDate = $ddata['attendance_date_end'].' '.$ddata['punch_out_time'];

                    if (mysqli_num_rows($isAlreadyPunchOutToday) > 0 && $is_multiple_punch_in == 0) {
                        $response['already_punch_out'] = true;
                        $alrdyOut = true;
                        $response['already_punch_out_msg'] = "Already punched out on ".date('h:i A, d M Y',strtotime($tempEndDate));
                    }else{
                        $response['already_punch_out'] = false;
                        $response['already_punch_out_msg'] = "";
                    }

                    $dataP2 = mysqli_fetch_array($isAlreadyPunchOutToday);     

                    if ($dataP2['attendance_status'] == 2) {
                        $response['is_attendance_rejected'] = true;
                        $response['attendance_rejected_message'] = "Your today's attendance was rejected";
                    }else{
                        $response['is_attendance_rejected'] = false;
                        $response['attendance_rejected_message'] = "";
                    }

                    $attendance_date_start = date('Y-m-d',strtotime('-1 days'));
                    $aDt = date('Y-m-d');

                    if ($isMaxHoursSet==true) {

                        if ($avarageDay<1) {
                            $avarageDay =1;
                        }

                        $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start BETWEEN  '$date2DayPre' AND '$aDt') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date BETWEEN '$date2DayPre' AND '$aDt') AND society_id = '$society_id'","ORDER BY work_report_date DESC LIMIT 1");
                    } else if ($isMaxTimeSet==true) {
                       
                        $date2DayPre = date("Y-m-d",strtotime("-1 days"));

                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start = '$attendance_date_start' OR attendance_date_start = '$date2DayPre') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date = '$attendance_date_start' OR work_report_date = '$date2DayPre') AND society_id = '$society_id'","ORDER BY work_report_date DESC LIMIT 1");
                    }  else {


                        $getTodayAttendace = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start BETWEEN  '$attendance_date_start' AND '$aDt') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                        $workReportQry = $d->count_data_direct("work_report_id","work_report_master","user_id = '$user_id' AND (work_report_date BETWEEN '$attendance_date_start' AND '$aDt') AND society_id = '$society_id'","ORDER BY work_report_date DESC LIMIT 1");

                    }

                    if ($workReportQry > 0) {
                        $response['work_report_added'] = true;
                    }  

                    if (strlen($attendance_id) > 0 && $attendance_id != '0') {
                        $lastAttQry = $d->select("attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id' AND attendance_status != '2'");

                        $getTodayAttendace = $d->select("attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id' AND attendance_status != '2'");

                        $lastAttData = mysqli_fetch_array($lastAttQry);

                        $attDate = $lastAttData['attendance_date_start'];
                        $total_working_minutes = $lastAttData['total_working_minutes'];
                        $last_punch_in_date = $lastAttData['last_punch_in_date'];
                        $last_punch_in_time = $lastAttData['last_punch_in_time']; 

                        $totalHours = getTotalHours($last_punch_in_date,date('Y-m-d'),$last_punch_in_time,date('H:i:s'));

                        $time = explode(':', $totalHours.":00");
                        $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                        $finalMin = $total_working_minutes + $total_minutes;
                    
                    }          
                }
            }


            if (mysqli_num_rows($getTodayAttendace) > 0) {

                $response['already_punch_in'] = true;

                $attData = mysqli_fetch_array($getTodayAttendace);

                if ($attendance_id == null || $attendance_id=="" || $attendance_id == "0") {
                    $attendance_id = $attData["attendance_id"];
                }

                if ($is_multiple_punch_in == 0) { 

                    $attStartDate = $attData["attendance_date_start"];
                    $punchInTime = $attData["punch_in_time"];

                    $totalWorkingMinutes = $attData['total_working_minutes'];
                    $lastPunchInDate = $attData['last_punch_in_date'];
                    $lastPunchInTime = $attData['last_punch_in_time']; 

                    $tHours = getTotalHours($lastPunchInDate,date('Y-m-d'),$lastPunchInTime,date('H:i:s'));

                    $tm = explode(':', $tHours.":00");
                    $tMinutes = ($tm[0]*60) + ($tm[1]) + ($tm[2]/60);

                    $tMin = $totalWorkingMinutes + $tMinutes;
                }else{

                    $multiplePunchDataArray = json_decode($attData['multiple_punch_in_out_data'],true);

                    $isMultiOut = false;

                    if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {

                        for ($i=0; $i < count($multiplePunchDataArray) ; $i++) {

                            if($multiplePunchDataArray[$i]["punch_out_date"] == '' || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){ 
                                $isMultiOut = true;
                                $attStartDate = $multiplePunchDataArray[$i]["punch_in_date"];
                                $punchInTime = $multiplePunchDataArray[$i]["punch_in_time"];
                                break;
                            }else{
                                $attStartDate = $attData["attendance_date_start"];
                                $punchInTime = $attData["punch_in_time"];
                            }
                        }  
                    }else{
                        $isMultiOut = true;
                        $attStartDate = $attData["attendance_date_start"];
                        $punchInTime = $attData["punch_in_time"];
                    }

                    if ($isMultiOut == true) {
                        $totalWorkingMinutes = $attData['total_working_minutes'];
                        $lastPunchInDate = $attData['last_punch_in_date'];
                        $lastPunchInTime = $attData['last_punch_in_time']; 

                        $tHours = getTotalHours($lastPunchInDate,date('Y-m-d'),$lastPunchInTime,date('H:i:s'));

                        $tm = explode(':', $tHours.":00");
                        $tMinutes = ($tm[0]*60) + ($tm[1]) + ($tm[2]/60);
                        $tMin = $totalWorkingMinutes + $tMinutes;
                    }else{
                        $tMin = 0;
                    }

                    if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) <= 0) {
                        $attStartDate = $attData["attendance_date_start"];
                        $punchInTime = $attData["punch_in_time"];

                        $totalWorkingMinutes = $attData['total_working_minutes'];

                        $m->set_data('last_punch_in_date',$attStartDate);
                        $m->set_data('last_punch_in_time',$punchInTime);

                        $tHours = getTotalHours($attStartDate,date('Y-m-d'),$punchInTime,date('H:i:s'));

                        $tm = explode(':', $tHours.":00");
                        $tMinutes = ($tm[0]*60) + ($tm[1]) + ($tm[2]/60);

                        $tMin = $totalWorkingMinutes + $tMinutes;
                    }
                }

                $shiftETime =  $curDate. ' ' .$shift_end_time;
                $shiftETime =  strtotime($shiftETime);

                $ctn =  strtotime($dateTimeCurrent);

                $rQ == false;   

                if ($shift_type == "Night") {
                    if ($shift_end_time > $max_punch_out_time  && $max_punch_out_time != '') {
                        $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($curDate))).' '.$max_punch_out_time;
                    }else{
                        $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($attStartDate))).' '.$max_punch_out_time;                        
                    }
                    $curTimeNew = strtotime($curTimeNew);

                    if ($max_punch_out_time == '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {
                        $newCTime = date('Y-m-d H:i:s');

                        $date_a = new DateTime($attStartDate.' '.$punchInTime);
                        $date_b = new DateTime($newCTime);

                        $interval = $date_a->diff($date_b);

                        $day = $interval->format('%days');

                        if ($day > 0) {
                            $hour = $day * 24;
                        }
                        $hours = $hour + $interval->format('%h');

                        if ($ctn > $shiftETime && $hours >= 18) {
                            $rQ = true;
                        }
                    }else if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {
                        if ($ctn > $shiftETime && $ctn > $curTimeNew) {
                            $rQ = true; 
                        }
                    }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != "00:00:00")) {
                        if ($tMin > $max_shift_hour_minutes) {
                            $rQ = true; 
                        }
                    }
                }else{
                    if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '' && $attStartDate==$curDate) {
                        $curTimeNew =  date('Y-m-d',strtotime('+1 days')).' '.$max_punch_out_time;
                        $curTimeNew = strtotime($curTimeNew);
                    }else{
                        $curTimeNew =  date('Y-m-d').' '.$max_punch_out_time;
                        $curTimeNew = strtotime($curTimeNew);
                    }

                    if ($max_punch_out_time != '' || ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                        if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {

                            if ($ctn > $curTimeNew) {
                                $rQ = true; 
                            }
                        }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != ' ' || $max_shift_hour != "00:00:00")) {
                            if ($tMin > $max_shift_hour_minutes && ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                                $rQ = true; 
                            }
                        }
                    }
                }


                if ($rQ == true) {
                    $response["already_punch_in"] = false;                
                    $response["message"] = "Punch out time limit over $tHours";                
                    $response["status"] = "200";
                    $response['attendance_start_date'] ="";
                    $response['attendance_start_date_view'] ="";
                    $response['attendance_id'] ="0";
                    $response['punch_in_time'] ="";
                    $response['date_time'] ="";
                    $response['time_diff'] ="";
                    /*echo json_encode($response);
                    exit();*/
                }


                if ($isMultiOut == false && $is_multiple_punch_in == 1) {
                    $response["already_punch_in"] = false; 
                }



                if ($attStartDate != '' && $rQ == false && (($isMultiOut == true && $is_multiple_punch_in == 1)
                    || $is_multiple_punch_in == 0) && $isFirstTimeEnd == false &&  $alrdyOut == false) {

                    if ($tMin == 0) {
                        $pDate = $attStartDate." ".$punchInTime;

                        $inTimeAMPM = date('Y-m-d H:i:s',strtotime($pDate));
                        $dateTimeCurrent = date('Y-m-d H:i:s',strtotime($dateTimeCurrent));
                        $date_a = new DateTime($inTimeAMPM);
                        $date_b = new DateTime($dateTimeCurrent);

                        $interval = $date_a->diff($date_b);

                        $day = $interval->format('%days');

                        if ($day > 0) {
                            $hour = $day * 24;
                        }
                        $hours = $hour + $interval->format('%h');
                        $minutes = $interval->format('%i');
                        $sec = $interval->format('%s');

                        /*if ($sec < 45) {
                           $sec += 5;
                        }*/
                    }else{
                        $hours  = floor($tMin/60);
                        $minutes = $tMin % 60;
                    }
                    

                    $finalTime =  $attStartDate." ".sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

                    $time_diff = sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);
                    
                    $response['attendance_start_date'] =$attStartDate;
                    $response['attendance_start_date_view'] =date('d-m-Y',strtotime($attStartDate));
                    $response['attendance_id'] =$attData["attendance_id"];
                    $response['punch_in_time'] =$attData["punch_in_time"];
                    $response['date_time'] =$finalTime;
                    $response['time_diff'] =$time_diff;
                }else{
                    if ($rQ == false && ($attendance_id != null || $attendance_id!="" || $attendance_id != "0") && $isMultiOut == true) {
                        $response['attendance_id'] = $attendance_id;
                    }else{
                        $response['attendance_id']="0";                    
                    }
                }
            }else{
                $response['attendance_id'] ="0";
            }

            // find Punch out missing and rejected attendance

            if ($attendance_id > 0 && $rQ != true) {
                $appndQ = " AND attendance_master.attendance_id < $attendance_id";
            }

            $getYesterdayAttendace = $d->selectRow("attendance_id,attendance_status,punch_in_time,punch_out_time,attendance_date_end,attendance_date_start","attendance_master","user_id = '$user_id' AND attendance_date_end = '0000-00-00' AND attendance_date_start != '$curDate' AND attendance_status !='2' $appndQ","ORDER BY attendance_date_start DESC LIMIT 1");


            $dataP2 = mysqli_fetch_array($getYesterdayAttendace);

            if (mysqli_num_rows($getYesterdayAttendace) > 0) {

                $tDate = date('Y-m-d');
                $sDate = date('Y-m-d',strtotime($dataP2['attendance_date_start']));

                $date1 = new DateTime($tDate);
                $date2 = new DateTime($sDate);
                $interval = $date1->diff($date2);
                $tYears = $interval->y; 
                $tDays = $interval->d; 
                $tMonths = $interval->m; 

                if ($tDays < 30 && $tMonths == 0 && $tYears < 1) {
                    $response['yesterday_punch_out_missing'] = true;
                    $response['yesterday_punch_out_missing_date'] = $dataP2['attendance_date_start'];
                    $response['missing_date'] = date('d',strtotime($dataP2['attendance_date_start']));
                    $response['missing_month'] = date('m',strtotime($dataP2['attendance_date_start']));
                    $response['missing_year'] = date('Y',strtotime($dataP2['attendance_date_start']));
                    $response['yesterday_punch_out_missing_msg'] = "Punch out missing on ".date('dS M Y',strtotime($dataP2['attendance_date_start']));
                }else{
                    $response['yesterday_punch_out_missing'] = false;
                    $response['yesterday_punch_out_missing_date'] = "";
                    $response['yesterday_punch_out_missing_msg'] = "";
                    $response['missing_date'] = "";
                    $response['missing_month'] = "";
                    $response['missing_year'] = "";
                }                
            }else{
                $response['yesterday_punch_out_missing'] = false;
                $response['yesterday_punch_out_missing_msg'] = "";
                $response['yesterday_punch_out_missing_date'] = "";
                $response['missing_date'] = "";
                $response['missing_month'] = "";
                $response['missing_year'] = "";
            }

            if ($dataP2['attendance_status'] == 2) {
                $response['yesterday_attendance_rejected'] = true;
                $response['yesterday_attendance_rejected_date'] = $dataP2['attendance_date_start'];
                $response['yesterday_attendance_rejected_message'] = "Attendance of ".date('dS M Y',strtotime($dataP2['attendance_date_start']))." was rejected";
            }else{
                $response['yesterday_attendance_rejected'] = false;
                $response['yesterday_attendance_rejected_message'] = "";
                $response['yesterday_attendance_rejected_date'] = "";
            }

            // break data calculation and running time

            if ($attStartDate != '' && $attStartDate != '0000-00-00') {
                $attendance_date_start = $attStartDate;
            }else{
                $attendance_date_start = date('Y-m-d');                
            }
            

            $getBreakTime = $d->select("attendance_break_history_master","attendance_id = '$attendance_id' AND break_start_date='$attendance_date_start' AND break_out_time = '00:00:00' AND break_end_date = '0000-00-00'","ORDER BY attendance_break_history_id DESC LIMIT 1");

            if (mysqli_num_rows($getBreakTime) > 0) {

                $breakData = mysqli_fetch_array($getBreakTime);

                $breakStartDate = $breakData["break_start_date"];
                $breakInTime = $breakData["break_in_time"];

                $date_a1 = new DateTime($breakStartDate." ".$breakInTime);
                $date_b1 = new DateTime($dateTimeCurrent);

                $interval1 = $date_a1->diff($date_b1);

                $hours1 = $interval1->format('%h');
                $minutes1 = $interval1->format('%i');
                $sec1 = $interval1->format('%s');

                /*if ($sec1 < 45) {
                    $sec1 += 5;
                }*/

                $break_date_time =  $breakStartDate." ".sprintf('%02d:%02d:%02d', $hours1, $minutes1,$sec1);

                $break_time_diff = sprintf('%02d:%02d:%02d', $hours1, $minutes1,$sec1);
                
                $response['attendance_break_history_id'] =$breakData["attendance_break_history_id"];
                $response['break_in_time'] =$breakData["break_in_time"];
                $response['attendance_type_id'] =$breakData["attendance_type_id"];
                $response['break_date_time'] =$break_date_time;
                $response['break_time_diff'] =$break_time_diff;
            }else{
                $response['attendance_break_history_id'] ="0";
            }

            $getBreakTimeTypeIds = $d->selectRow("attendance_break_history_master.*","attendance_break_history_master","attendance_id = '$attendance_id' AND break_start_date = '$attendance_date_start' AND break_out_time != '00:00:00' AND break_end_date != '0000-00-00'");

            $attTypeIdArray = array();

            if (mysqli_num_rows($getBreakTimeTypeIds)) {
                while($typeData = mysqli_fetch_array($getBreakTimeTypeIds)){

                    $typeData = array_map("html_entity_decode", $typeData);

                    /*$qq1 = $d->count_data_direct("attendance_type_id","attendance_break_history_master"," attendance_id = '$attendance_id' AND break_start_date = '$attendance_date_start' AND break_out_time != '00:00:00' AND break_end_date != '0000-00-00' AND attendance_type_id = '$typeData[attendance_type_id]'","GROUP BY attendance_type_id");*/

                    $qry = $d->select("attendance_type_master","society_id='$society_id' AND attendance_type_id = '$typeData[attendance_type_id]' AND is_multipletime_use = '0'");

                    if (mysqli_num_rows($qry) > 0) {
                        array_push($attTypeIdArray,$typeData["attendance_type_id"]);
                    }
                }
            }

            $attTypeIdArray = join(",",$attTypeIdArray);

            if ($attTypeIdArray!="") {
                $attendanceTypeQry = $d->select("attendance_type_master","society_id='$society_id'  AND attendance_type_active_status = '0' AND attendance_type_delete = '0' AND attendance_type_id NOT IN ('$attTypeIdArray')");
            }else{
                $attendanceTypeQry = $d->select("attendance_type_master","society_id='$society_id' AND attendance_type_active_status = '0' AND attendance_type_delete = '0'");
            }       

            $response["attendance_types"] = array();

            if(mysqli_num_rows($attendanceTypeQry)>0){
                

                while($leaveTypeData=mysqli_fetch_array($attendanceTypeQry)) {

                    $leaveTypeData = array_map("html_entity_decode", $leaveTypeData);

                    $attendance_type = array(); 

                    $attendance_type["attendance_type_id"] = $leaveTypeData['attendance_type_id'];
                    $attendance_type["attendance_type_name"] = $leaveTypeData['attendance_type_name'];

                    array_push($response["attendance_types"], $attendance_type);
                }             
            }

            $userQry = $d->selectRow("face_data_image,face_data_image_two,face_added_date,user_face_data","users_master","user_id = $user_id");

            $userData = mysqli_fetch_array($userQry);

            $response["user_face_data"] = $userData['user_face_data'].'';
            if ($userData['face_added_date'] != '' && $userData['face_added_date'] != '0000-00-00') {
                $response["face_added_date"] = date('h:i A, d M Y',strtotime($userData['face_added_date']));
            } else {
                $response["face_added_date"] = "";
            }


            if ($userData['face_data_image'] != '') {
                $response["face_data_image"] = $base_url . "img/attendance_face_image/" . $userData['face_data_image'];
            } else {
                $response["face_data_image"] = "";
            }

            if ($userData['face_data_image_two'] != '') {
                $response["face_data_image_two"] = $base_url . "img/attendance_face_image/" . $userData['face_data_image_two'];
            } else {
                $response["face_data_image_two"] = "";
            }
            if ($rQ ==false) {
                $response["message"] = "Success";
            }
            $response["status"] = "200";
            echo json_encode($response);
                 
        }else if($_POST['getUserFaceData']=="getUserFaceData" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            
            $userQry = $d->selectRow("face_data_image,face_data_image_two,face_added_date,user_face_data","users_master","user_id = '$user_id'");

            $userData = mysqli_fetch_array($userQry);

            $response["user_face_data"] = $userData['user_face_data'].'';
            if ($userData['face_added_date'] != '' && $userData['face_added_date'] != '0000-00-00') {
                $response["face_added_date"] = date('h:i A, d M Y',strtotime($userData['face_added_date']));
            } else {
                $response["face_added_date"] = "";
            }


            if ($userData['face_data_image'] != '') {
                $response["face_data_image"] = $base_url . "img/attendance_face_image/" . $userData['face_data_image'];
            } else {
                $response["face_data_image"] = "";
            }

            if ($userData['face_data_image_two'] != '') {
                $response["face_data_image_two"] = $base_url . "img/attendance_face_image/" . $userData['face_data_image_two'];
            } else {
                $response["face_data_image_two"] = "";
            }
            
            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
                 
        }else if($_POST['attendancePunchIn']=="attendancePunchIn" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $punchInTime =date('H:i:s');
            $punchInDate =date('Y-m-d');

            $dayNumber = date("w");   

            $currentMonth = date('m');

            $punchInDateTime = $punchInDate." ".$punchInTime;

            $ua = array(
                "battery_optimisation_status"=>$battery_optimisation_status
            );

            $d->update("users_master",$ua,"user_id = '$user_id'");

            $sq = $d->selectRow("users_master.shift_time_id,shift_timing_master.*","users_master,shift_timing_master","users_master.user_id = '$user_id' AND users_master.shift_time_id = shift_timing_master.shift_time_id");

            if (mysqli_num_rows($sq)>0) {
                $shiftData = mysqli_fetch_array($sq);
                $shiftTimeId = $shiftData['shift_time_id'];
                $shift_type = $shiftData['shift_type'];
                $per_day_hour = $shiftData['per_day_hour'];
                $shift_start_time = $shiftData['shift_start_time'];
                $shift_end_time = $shiftData['shift_end_time'];
                $is_multiple_punch_in = $shiftData['is_multiple_punch_in'];
                $has_altenate_week_off = $shiftData['has_altenate_week_off'];
                $alternate_week_off = $shiftData['alternate_week_off'];
                $alternate_weekoff_days = $shiftData['alternate_weekoff_days'];
                $max_shift_hour = $shiftData['max_shift_hour'];
                $max_hour = $shiftData['max_hour'];
                $max_punch_out_time = $shiftData['max_punch_out_time'];
            }else{
                $response["message"] = "Shift no assigned";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $parts1 = explode(':', $per_day_hour);
            $per_day_hour_minutes = ($parts1[0]*60) + ($parts1[1]) + ($parts1[2]/60);

            if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                $isMaxTimeSet = true;
            } else if ($max_shift_hour != '' && $max_shift_hour!='00:00:00') {
                $avarageDay = round($max_shift_hour/24);
                $isMaxHoursSet = true;
            }

            if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '') {
                $checkNextDayDate = true;
            }

            if ($max_shift_hour != '' && $max_shift_hour != '00:00:00') {
                $parts = explode(':', $max_shift_hour);
                $max_shift_hour_minutes = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
            }else{
                $max_shift_hour_minutes = 0;
            }

            $alternate_week_off = explode(",", $alternate_week_off);
            $alternate_weekoff_days = explode(",", $alternate_weekoff_days);

            $is_extra_day = "0"; 

            if (in_array($dayNumber, $alternate_week_off) || in_array($dayNumber, $alternate_weekoff_days)) {
                $is_extra_day = "1";
            }

            $holidayQry = $d->count_data_direct("holiday_start_date","holiday_master","society_id = '$society_id' AND holiday_status = 0 AND holiday_start_date = '$punchInDate'");

            if ($holidayQry > 0) {
                $is_extra_day = "1";
            }

            if ($is_multiple_punch_in == 0) {
                $appQry = " AND attendance_date_end = '0000-00-00'";
            }

            if ($shift_type == "Night") {

                $attendance_date_start = date('Y-m-d',strtotime('-1 days'));
                $aDt = date('Y-m-d');

                if ($isMaxHoursSet==true) {

                    if ($avarageDay<1) {
                        $avarageDay =1;
                    }

                    $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                    $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start BETWEEN  '$date2DayPre' AND '$aDt')  AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                } else if ($isMaxTimeSet==true) {

                    $date2DayPre = date("Y-m-d",strtotime("-1 days"));

                    $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start = '$attendance_date_start' OR attendance_date_start = '$date2DayPre') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                } else {

                    $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start BETWEEN  '$attendance_date_start' AND '$aDt') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                }
            }else{

                $attendance_date_start = date('Y-m-d');

                if ($isMaxHoursSet==true) {

                    if ($avarageDay<1) {
                        $avarageDay =1;
                    }
                    $date2DayPre = date("Y-m-d",strtotime("-$avarageDay days"));

                    $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start BETWEEN  '$date2DayPre' AND '$attendance_date_start') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");                    

                } else if ($isMaxTimeSet==true) {
                   
                    $date2DayPre = date("Y-m-d",strtotime("-1 days"));

                    $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND (attendance_date_start = '$attendance_date_start' OR attendance_date_start = '$date2DayPre') AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                } else {
                    $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND attendance_date_start = '$attendance_date_start' AND attendance_status != '2' $appQry","ORDER BY attendance_date_start DESC, attendance_id DESC LIMIT 1");

                }
            }


            if (strlen($attendance_id) > 0 && $attendance_id != '0') {
                $isPunchIn = $d->select("attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id'  AND attendance_status != '2'");
            }

            if (mysqli_num_rows($isPunchIn) > 0) {

                $shiftETime =  date('Y-m-d'). ' ' .$shift_end_time;
                $shiftETime =  strtotime($shiftETime);
                $ctn =  strtotime(date('Y-m-d H:i:s'));

                $atData = mysqli_fetch_array($isPunchIn);

                $attendance_id = $atData['attendance_id'];


                $aDate = $atData['attendance_date_start'];
                $aeDate = $atData['attendance_date_end'];
               
                $totalWorkingMinutes = $atData['total_working_minutes'];
                $lastPunchInDate = $atData['last_punch_in_date'];
                $lastPunchInTime = $atData['last_punch_in_time']; 

                $tHours = getTotalHours($lastPunchInDate,date('Y-m-d'),$lastPunchInTime,date('H:i:s'));

                $tm = explode(':', $tHours.":00");
                $tMinutes = ($tm[0]*60) + ($tm[1]) + ($tm[2]/60);

                $total_minutes = $totalWorkingMinutes + $tMinutes;

                if ($shift_type == "Night") {


                    if ($shift_end_time > $max_punch_out_time  && $max_punch_out_time != '') {
                        $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($punchInDate))).' '.$max_punch_out_time;
                    }else{
                        $curTimeNew =  date('Y-m-d',strtotime('+1 days',strtotime($aDate))).' '.$max_punch_out_time;                        
                    }
                    $curTimeNew = strtotime($curTimeNew);

                    if ($max_punch_out_time == '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {

                        $date_a = new DateTime($aDate);
                        $date_b = new DateTime(date('Y-m-d H:i:s'));

                        $interval = $date_a->diff($date_b);

                        $day = $interval->format('%days');

                        if ($day > 0) {
                            $hour = $day * 24;
                        }
                        $hours = $hour + $interval->format('%h');


                        if ($ctn > $curTimeNew && $hours > 18) {
                            $rQ = true;
                        }
                    }else if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {

                        if ($ctn > $curTimeNew && $ctn > $shiftETime) {
                            $rQ = true; 
                        }
                        
                    }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != "00:00:00")) {
                        if ($total_minutes > $max_shift_hour_minutes) {
                            $rQ = true; 
                        }
                    }
                }else{

                    if ($shift_end_time > $max_punch_out_time && $max_punch_out_time != '' && $aDate==$curDate) {
                        $curTimeNew =  date('Y-m-d',strtotime('+1 days')).' '.$max_punch_out_time;
                        $curTimeNew = strtotime($curTimeNew);
                    }else{
                        $curTimeNew =  date('Y-m-d').' '.$max_punch_out_time;
                        $curTimeNew = strtotime($curTimeNew);
                    }

                    if ($max_punch_out_time != '' || ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                        if ($max_punch_out_time != '' && ($max_shift_hour == '' || $max_shift_hour == "00:00:00")) {
                            if ($ctn > $curTimeNew) {
                                $rQ = true; 
                            }
                        }else if ($max_punch_out_time == '' && ($max_shift_hour != '' || $max_shift_hour != ' ' || $max_shift_hour != "00:00:00")) {
                            if ($total_minutes > $max_shift_hour_minutes && ($max_shift_hour != '' && $max_shift_hour != "00:00:00")) {
                                $rQ = true; 
                            }
                        }
                    }
                }
            }


            $newDate = date('Y-m-d',strtotime("+1 days",strtotime($aDate)));
            $approxShiftEndTime = $newDate.' '.$shift_end_time;
            $finalMin = (1440 - $per_day_hour_minutes)/2;
            
            if ($finalMin < 1) {
                $finalMin = 1;
            }

            $approxShiftEndTimeNew = date('Y-m-d H:i:s',strtotime("+$finalMin minutes",strtotime($approxShiftEndTime)));
            $approxShiftEndTimeNewTimeString = strtotime($approxShiftEndTimeNew);
            $punchInDateTimeString = strtotime(date('Y-m-d H:i:s'));

            if (($shift_type == "Day" || $shift_type == "Night") && $is_multiple_punch_in == 0 && mysqli_num_rows($isPunchIn) > 0 && $rQ == false && $punchInDateTimeString < $approxShiftEndTimeNewTimeString) {

                $response["message"] = "Already Punched In";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }else if (($shift_type == "Day" || $shift_type == "Night") && $is_multiple_punch_in == 1 && mysqli_num_rows($isPunchIn) > 0) {

                $attendance_id = $atData['attendance_id'];
                $multiple_punch_in_out_data = $atData['multiple_punch_in_out_data'];                        
            }

            $wfh_attendance = "0";

            $wfhQry = $d->selectRow("wfh_id,user_id,wfh_status,wfh_start_date,wfh_latitude,wfh_longitude,wfh_attendance_range","wfh_master","society_id ='$society_id' AND user_id='$user_id' AND wfh_status = '1' AND wfh_start_date = '$punchInDate'");
            $wfhData = mysqli_fetch_array($wfhQry);

            if (mysqli_num_rows($wfhQry) > 0) {

                $wfhLat = $wfhData['wfh_latitude'];
                $wfhLng = $wfhData['wfh_longitude'];
                $wfhRange = $wfhData['wfh_attendance_range'];

                if ($wfhData['wfh_status'] == '1') {
                    $wfh_attendance = "1";
                }else{
                    $wfh_attendance = "0";
                }
            }

            // Late IN

            $perdayHours = $shiftData['per_day_hour'];
            $shift_start_time = $shiftData['shift_start_time'];
            $late_time_start = $shiftData['late_time_start'];
            $maximum_in_out = $shiftData['maximum_in_out'];
 
            if ($late_time_start != '') {
                $partLI = explode(':', $late_time_start);
                $min = ($partLI[0]*60) + ($partLI[1]) + ($partLI[2]/60);
            }else{
                $min = 0;
            }
            
            $defaultINTime = $punchInDate." ".$shift_start_time;
            $strMin = "+".$min." minutes";
            $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

            $qryLeave = $d->selectRow("leave_day_type,half_day_session","leave_master","user_id = '$user_id' AND leave_start_date = '$punchInDate'");

            $hasLeave = false;

            if (mysqli_num_rows($qryLeave) > 0) {

                $hasLeave = true;

                $leaveData = mysqli_fetch_array($qryLeave);
                $leave_day_type = $leaveData['leave_day_type'];
                $half_day_session = $leaveData['half_day_session'];
            }
            if ($late_time_start != '') {
                if ($punchInDateTime > $defaultTime) {
                    $late_in = "1";
                }else{
                    $late_in = "0";
                }
            }else{
                $late_in = "0";
            }


            if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 1) {
                $late_in = "0";
            }

            $punch_in_image = "";

            if ($_FILES["punch_in_image"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["punch_in_image"]["tmp_name"];
                $extId = pathinfo($_FILES['punch_in_image']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["punch_in_image"]["name"]);
                    $punch_in_image = "punch_in_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["punch_in_image"]["tmp_name"], "../img/attendance/" . $punch_in_image);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $attendance_created_time = date('Y-m-d H:i:s');

            if ($rQ == true) {
                $attendance_id = 0;
            }

            if ($is_multiple_punch_in == 1 && $shift_type == "Day" && $aDate != $punchInDate) {
                $attendance_id = 0;
            }else if ($is_multiple_punch_in == 1 && $shift_type == "Night" && ($approxShiftEndTimeNewTimeString < $punchInDateTimeString)) {
                $attendance_id = 0;
            }

            if (($attendance_id == '' || $attendance_id == '0')) { 

                $m->set_data('shift_time_id',$shiftTimeId);
                $m->set_data('total_shift_hours',$perdayHours);
                $m->set_data('society_id',$society_id);
                $m->set_data('unit_id',$unit_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('attendance_date_start',$punchInDate);
                $m->set_data('punch_in_time',$punchInTime);
                $m->set_data('last_punch_in_date',$punchInDate);
                $m->set_data('last_punch_in_time',$punchInTime);
                $m->set_data('punch_in_latitude',$punch_in_latitude);
                $m->set_data('punch_in_longitude',$punch_in_longitude);
                $m->set_data('attendance_reason',$attendance_reason);
                $m->set_data('punch_in_image',$punch_in_image);
                $m->set_data('attendance_status',$attendance_status);
                $m->set_data('wfh_attendance',$wfh_attendance);
                $m->set_data('radiusInMeter',$radiusInMeter);
                $m->set_data('totalKm',$totalKm);
                $m->set_data('punch_in_in_range',$punch_in_in_range);
                $m->set_data('late_in',$late_in);
                $m->set_data('late_in_reason',$late_in_reason);
                $m->set_data('punch_in_address',$punch_in_address);
                $m->set_data('is_extra_day',$is_extra_day);
                $m->set_data('punch_in_gps_accuracy',$punch_in_gps_accuracy);
                $m->set_data('punch_in_branch',$location_name.'');
                $m->set_data('multiple_punch_in_out_data',"");
                $m->set_data('attendance_in_from',"1");
                $m->set_data('attendance_created_time',date('Y-m-d H:i:s'));
 
                $dataArray = array();
                $multPunchAry = array(
                    "punch_in_date" => $punchInDate,
                    "punch_in_time" => $punchInTime,
                    "punch_out_date" => "",
                    "punch_out_time" => "",
                    "punch_in_image" => $punch_in_image,
                    "punch_out_image" => "",
                    "location_name_in" => $location_name.'',
                    "location_name_out" => "",
                    "working_hour" => "",
                    "working_hour_minute" => "",
                );

                array_push($dataArray,$multPunchAry);
                $multPunchDataJson = json_encode($dataArray);

                $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);

                $a = array(
                    'shift_time_id'=>$m->get_data('shift_time_id'),
                    'total_shift_hours'=>$m->get_data('total_shift_hours'),
                    'society_id' =>$m->get_data('society_id'),
                    'unit_id'=>$m->get_data('unit_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'attendance_date_start'=>$m->get_data('attendance_date_start'),
                    'punch_in_time'=>$m->get_data('punch_in_time'),
                    'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
                    'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
                    'punch_in_latitude'=>$m->get_data('punch_in_latitude'),
                    'punch_in_longitude'=>$m->get_data('punch_in_longitude'),
                    'attendance_reason'=>$m->get_data('attendance_reason'),
                    'punch_in_image'=>$m->get_data('punch_in_image'),
                    'attendance_status'=>$m->get_data('attendance_status'),
                    'wfh_attendance'=>$m->get_data('wfh_attendance'),
                    'attendance_range_in_meter'=>$m->get_data('radiusInMeter'),
                    'attendance_range_in_km'=>$m->get_data('totalKm'),
                    'punch_in_in_range'=>$m->get_data('punch_in_in_range'),
                    'late_in'=>$m->get_data('late_in'),
                    'late_in_reason'=>$m->get_data('late_in_reason'),
                    'punch_in_address'=>$m->get_data('punch_in_address'),
                    'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                    'attendance_in_from'=>$m->get_data('attendance_in_from'),
                    'is_extra_day'=>$m->get_data('is_extra_day'),
                    'punch_in_branch'=>$m->get_data('punch_in_branch'),
                    'attendance_created_time'=>$m->get_data('attendance_created_time'),
                    'punch_in_gps_accuracy'=>$m->get_data('punch_in_gps_accuracy'),
                );

                $punchInQry = $d->insert("attendance_master",$a);

                $attendance_id = $con->insert_id;
            }else{
                $dataArray = array();
                if ($multiple_punch_in_out_data != '') {

                    $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

                    if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {

                        for ($i=0; $i < count($multiplePunchDataArray) ; $i++) {
                            $oldAry = array(); 
                            $oldAry['punch_in_date'] = $multiplePunchDataArray[$i]["punch_in_date"];
                            $oldAry['punch_in_time'] = $multiplePunchDataArray[$i]["punch_in_time"];
                            $oldAry['punch_out_date'] = $multiplePunchDataArray[$i]["punch_out_date"];
                            $oldAry['punch_out_time'] = $multiplePunchDataArray[$i]["punch_out_time"];
                            $oldAry['punch_in_image'] = $multiplePunchDataArray[$i]["punch_in_image"];
                            $oldAry['punch_out_image'] = $multiplePunchDataArray[$i]["punch_out_image"];
                            $oldAry['location_name_in'] = $multiplePunchDataArray[$i]["location_name_in"];
                            $oldAry['location_name_out'] = $multiplePunchDataArray[$i]["location_name_out"];
                            $oldAry['working_hour'] = $multiplePunchDataArray[$i]["working_hour"];
                            $oldAry['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"];
                            array_push($dataArray,$oldAry);
                        }
                    }

                    $multPunchAry = array(
                        "punch_in_date" => $punchInDate,
                        "punch_in_time" => $punchInTime,
                        "punch_out_date" => "",
                        "punch_out_time" => "",
                        "punch_in_image" => $punch_in_image,
                        "punch_out_image" => "",
                        "location_name_in" => $location_name.'',
                        "location_name_out" => "",
                        "working_hour" => "",
                        "working_hour_minute" => "",
                    );

                    array_push($dataArray,$multPunchAry);
                    $multPunchDataJson = json_encode($dataArray);
                }else{
                    $multPunchAry = array(
                        "punch_in_date" => $punchInDate,
                        "punch_in_time" => $punchInTime,
                        "punch_out_date" => "",
                        "punch_out_time" => "",
                        "punch_in_image" => $punch_in_image,
                        "punch_out_image" => "",
                        "location_name_in" => $location_name.'',
                        "location_name_out" => "",
                        "working_hour" => "",
                        "working_hour_minute" => "",
                    );

                    array_push($dataArray,$multPunchAry);
                    $multPunchDataJson = json_encode($dataArray); 
                }

                $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
                $m->set_data('last_punch_in_date',$punchInDate);
                $m->set_data('last_punch_in_time',$punchInTime);
                $m->set_data('punch_in_branch',$location_name.'');
                $m->set_data('punch_in_gps_accuracy',$punch_in_gps_accuracy.'');
                $m->set_data('attendance_date_end',"");
                $m->set_data('punch_out_time',"");

                $a = array(
                    'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                    'punch_out_time'=>$m->get_data('punch_out_time'),
                    'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
                    'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
                    'attendance_date_end'=>$m->get_data('attendance_date_end'),
                    'punch_in_branch'=>$m->get_data('punch_in_branch'),
                    'punch_in_gps_accuracy'=>$m->get_data('punch_in_gps_accuracy'),
                );

                $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");
            }

            if ($punchInQry == true) {

                if ($need_approval == "true") {
 
                    $title = "Attendance Approval Request";
                    $description = "Requested by, ".$user_name;

                    $access_type = $pending_attendance_access;

                    include 'check_access_data_user.php';

                    // To Employee App
                    if (count($userIDArray) > 0) {

                        $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                        $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                        $dataAry = array(
                            "block_id"=>$block_id,
                            "floor_id"=>$floor_id,
                            "user_id"=>$user_id,
                        );

                        $dataJson = json_encode($dataAry);

                        $nResident->noti("pending_attendance","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                        $nResident->noti_ios("pending_attendance","",$society_id,$fcmArrayIosPA,$title,$description,$dataJson);

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"pending_attendance","attendance_tracker.png",$dataJson,"users_master.user_id IN ('$userFcmIds')");
                    }

                    $block_id=$d->getBlockid($user_id);
                    $fcmArray=$d->selectAdminBlockwise("17",$block_id,"android");
                    $fcmArrayIos=$d->selectAdminBlockwise("17",$block_id,"ios");
              
                    $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"pendingAttendance");

                    $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"pendingAttendance");

                    $notiAry = array(
                      'society_id'=>$society_id,
                      'notification_tittle'=>$title,
                      'notification_description'=>$description,
                      'notifiaction_date'=>date('Y-m-d H:i'),
                      'notification_action'=>"pendingAttendance",
                      'admin_click_action '=>"pendingAttendance",
                      'notification_logo'=>'attendance_tracker.png',
                    );
                                
                    $d->insert("admin_notification",$notiAry);
                }

                if ($need_approval == "true") {
                    $response["message"] = "Your attendance needs approval from admin";
                }else{
                    $response["message"] = "Attendance Approved";
                }

                $response["punch_in_time"] = date("h:i A",strtotime($punchInTime));
                $response["attendance_id"] = $attendance_id."";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }      
        }else if($_POST['attendancePunchOut']=="attendancePunchOut" && $attendance_id!='' && $user_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $qq = $d->selectRow("*","attendance_master","attendance_id = '$attendance_id' AND user_id = '$user_id'");
            $dataQ = mysqli_fetch_array($qq);
            $shiftTimeId = $dataQ['shift_time_id'];
            $multiple_punch_in_out_data = $dataQ['multiple_punch_in_out_data'];
            $is_extra_day = $dataQ['is_extra_day'];
            $attendance_date_start  = $dataQ['attendance_date_start'];
            $punch_in_time  = $dataQ['punch_in_time'];

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);

            $is_multiple_punch_in = $shiftData['is_multiple_punch_in'];

            $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);
            
            $punchOUTTime = date('H:i:s');
            $punchOUTDate = date('Y-m-d');
            $punch_out_image = "";

            $finalTotalMinute = 0;

            if (mysqli_num_rows($qq)>0 && $is_multiple_punch_in == 0) {

                $todayLateIn  = $dataQ['late_in'];

                if ($shift_type == "Day" && $attendance_date_start != $punchOUTDate) {
                    $response["message"] = "Next day punch out not allowed";
                    $response["status"] = "202";
                    echo json_encode($response);
                    exit();
                }

                if ($dataQ['attendance_date_end'] != "0000-00-00" && $dataQ['punch_out_time'] != "00:00:00") {
                    $response["message"] = "Already Punched Out";
                    $response["status"] = "202";
                    echo json_encode($response);
                    exit();
                }
            }

            if ($_FILES["punch_out_image"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["punch_out_image"]["tmp_name"];
                $extId = pathinfo($_FILES['punch_out_image']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["punch_out_image"]["name"]);
                    $punch_out_image = "punch_out_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["punch_out_image"]["tmp_name"], "../img/attendance/" . $punch_out_image);
                } else {
                    $response["message"] = "Invalid Document. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $dataArray = array();

            if (isset($multiplePunchDataArray) && $is_multiple_punch_in == 1 && count($multiplePunchDataArray) > 0) {

                for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                    if($multiplePunchDataArray[$i]["punch_out_date"] == '' 
                        || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){

                        $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$punchOUTDate,
                                $multiplePunchDataArray[$i]["punch_in_time"],$punchOUTTime);

                        $timeHr = explode(':', $totalHours.":00");
                        $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                        $finalTotalMinute = $finalTotalMinute + $totalMinutes;

                        $last_punch_in_date = $multiplePunchDataArray[$i]["punch_in_date"].'';
                        $last_punch_in_time = $multiplePunchDataArray[$i]["punch_in_time"].'';

                        $oldAry = array(
                            "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"].'',
                            "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"].'',
                            "punch_out_date" => $punchOUTDate.'',
                            "punch_out_time" =>$punchOUTTime.'',
                            "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"].'',
                            "punch_out_image" => $punch_out_image.'',
                            "location_name_in" => $multiplePunchDataArray[$i]["location_name_in"].'',
                            "location_name_out" => $location_name.'',
                            "working_hour" => $totalHours.'',
                            "working_hour_minute" => $totalMinutes.'',
                        );

                        array_push($dataArray,$oldAry);
                    }else{
                        $oldAry = array(); 
                        $oldAry['punch_in_date'] = $multiplePunchDataArray[$i]["punch_in_date"].'';
                        $oldAry['punch_in_time'] = $multiplePunchDataArray[$i]["punch_in_time"].'';
                        $oldAry['punch_out_date'] = $multiplePunchDataArray[$i]["punch_out_date"].'';
                        $oldAry['punch_out_time'] = $multiplePunchDataArray[$i]["punch_out_time"].'';
                        $oldAry['punch_in_image'] = $multiplePunchDataArray[$i]["punch_in_image"].'';
                        $oldAry['punch_out_image'] = $multiplePunchDataArray[$i]["punch_out_image"].'';
                        $oldAry['location_name_in'] = $multiplePunchDataArray[$i]["location_name_in"].'';
                        $oldAry['location_name_out'] = $multiplePunchDataArray[$i]["location_name_out"].'';
                        $oldAry['working_hour'] = $multiplePunchDataArray[$i]["working_hour"].'';
                        $oldAry['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"].'';

                        $finalTotalMinute = $finalTotalMinute + $multiplePunchDataArray[$i]["working_hour_minute"];
                        array_push($dataArray,$oldAry);
                    }
                }  

                $multPunchDataJson = json_encode($dataArray).'';
            }else{
                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                    for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                        if($multiplePunchDataArray[$i]["punch_out_date"] == '' || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){
                            $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$punchOUTDate,
                                    $multiplePunchDataArray[$i]["punch_in_time"],$punchOUTTime);

                            $timeHr = explode(':', $totalHours.":00");
                            $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                            $finalTotalMinute = $finalTotalMinute + $totalMinutes;

                            $last_punch_in_date = $multiplePunchDataArray[$i]["punch_in_date"].'';
                            $last_punch_in_time = $multiplePunchDataArray[$i]["punch_in_time"].'';

                            $oldAry = array(
                                "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"].'',
                                "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"].'',
                                "punch_out_date" => $punchOUTDate.'',
                                "punch_out_time" =>$punchOUTTime.'',
                                "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"].'',
                                "punch_out_image" => $punch_out_image.'',
                                "location_name_in" => $multiplePunchDataArray[$i]["location_name_in"].'',
                                "location_name_out" => $location_name.'',
                                "working_hour" => $totalHours.'',
                                "working_hour_minute" => $finalTotalMinute.'',
                            );
                            array_push($dataArray,$oldAry);
                        }
                    }

                    $multPunchDataJson = json_encode($dataArray).'';
                }else{
                    $totalHoursWithName = getTotalHoursWithNames($attendance_date_start,$punchOUTDate,
                            $punch_in_time,$punchOUTTime);

                    $last_punch_in_date = $attendance_date_start.'';
                    $last_punch_in_time = $punch_in_time.'';

                    $totalHours = getTotalHours($attendance_date_start,$punchOUTDate,
                                    $punch_in_time,$punchOUTTime);

                    $times[] = $totalHours;

                    $time = explode(':', $totalHours.":00");
                    $finalTotalMinute = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                    $oldAry = array(
                        "punch_in_date" => $attendance_date_start.'',
                        "punch_in_time" => $punch_in_time.'',
                        "punch_out_date" => $punchOUTDate.'',
                        "punch_out_time" =>$punchOUTTime.'',
                        "punch_in_image" => '',
                        "punch_out_image" => $punch_image.'',
                        "location_name_in" => '',
                        "location_name_out" => $location_name.'',
                        "working_hour" => $totalHours.'',
                        "working_hour_minute" => $finalTotalMinute.'',
                    );
                    array_push($dataArray,$oldAry);

                    $multPunchDataJson = json_encode($dataArray).'';
                }
            }


            $sq = $d->selectRow("shift_time_id,track_user_location","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $track_user_location = $sdata['track_user_location'];
            }

            // Early Punch OUT

            
            $perdayHours = $shiftData['per_day_hour'];
            $shift_start_time = $shiftData['shift_start_time'];
            $shift_end_time = $shiftData['shift_end_time'];
            $late_time_start = $shiftData['late_time_start'];
            $early_out_time = $shiftData['early_out_time'];
            $half_day_time_start = $shiftData['half_day_time_start'];
            $halfday_before_time = $shiftData['halfday_before_time'];
            $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
            $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
            $shift_type = $shiftData['shift_type'];
            $maximum_in_out = $shiftData['maximum_in_out'];
           
            

            $datePunchIN = $attendance_date_start ." ".$punch_in_time;
            $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
            $datePunchOUT = $punchOUTDate ." ".$punchOUTTime;

            if ($datePunchOUT < $datePunchIN && $is_multiple_punch_in == 0) {
                $response["message"] = "Immediate punch out is restricted, please punch out after 1 minute.";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $parts = explode(':', $perdayHours);
            $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type,half_day_session,auto_leave_reason","leave_master","user_id = '$user_id' AND leave_start_date = '$punchOUTDate'");

            $hasLeave = false;

            if (mysqli_num_rows($qryLeave) > 0) {

                $hasLeave = true;

                $leaveData = mysqli_fetch_array($qryLeave);

                $leave_id = $leaveData['leave_id'];
                $leave_type_id = $leaveData['leave_type_id'];
                $paid_unpaid = $leaveData['paid_unpaid'];
                $leave_reason = $leaveData['leave_reason'];
                $leave_day_type = $leaveData['leave_day_type'];
                $half_day_session = $leaveData['half_day_session'];
                $auto_leave_reason = $leaveData['auto_leave_reason'];
            }

            if ($early_out_time != '') {
                $partEO = explode(':', $early_out_time);
                $min = ($partEO[0]*60) + ($partEO[1]) + ($partEO[2]/60);
            }else{
                $min = 0;
            }

            $defaultOutTime = $punchOUTDate." ".$shift_end_time;
            $strMin = "-".$min." minutes";
            $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

            if ($early_out_time != '') {
                if ($datePunchOUT >= $defaultOutTime) {
                    $early_out = "0";
                }else{
                    $early_out = "1";              
                }
            }else{            
                $early_out = "0";              
            }

            if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                $early_out = "0";
            }

            if ($is_multiple_punch_in == 0) {

                $totalHours = getTotalHours($attendance_date_start,$punchOUTDate,
                                $punch_in_time,$punchOUTTime);

                $totalHoursNames = getTotalHoursWithNames($attendance_date_start,$punchOUTDate,
                                $punch_in_time,$punchOUTTime);

                $times[] = $totalHours;


                $time = explode(':', $totalHours.":00");
                $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);                    
                                    
            }else{
                $total_minutes =  $finalTotalMinute;

                $hoursPM  = floor($total_minutes/60);
                $minutesPM = $total_minutes % 60;

                $totalHours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                $times[] = $totalHours;

                if ($hoursPM > 0 && $minutesPM) {
                    $totalHoursNames = sprintf('%02d hr %02d min', $hoursPM, $minutesPM);
                }else if ($hoursPM > 0 && $minutesPM <= 0) {
                    $totalHoursNames = sprintf('%02d hr', $hoursPM);
                }else if ($hoursPM <= 0 && $minutesPM > 0) {
                    $totalHoursNames = sprintf('%02d min', $minutesPM);
                }else{
                    $totalHoursNames = "";
                }
            }

            $avgWorkingDays = $total_minutes/$perDayHourMinute;
            $avg_working_days = round($avgWorkingDays * 2) / 2;

            $extra_working_hours_minutes = 0;

            if ($total_minutes > $perDayHourMinute) {
                $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
                $hours  = floor($extra_working_hours_minutes/60);
                $minutes = $extra_working_hours_minutes % 60;

                $extra_working_hours = sprintf('%02d:%02d', $hours, $minutes);
            }  

            $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

            $qryBreakData = mysqli_fetch_array($qryBreak);

            $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

            if ($total_minutes > $totalBreakinutes) {
                $productive_working_hours_minutes = $total_minutes - $totalBreakinutes;

                $hoursPM  = floor($productive_working_hours_minutes/60);
                $minutesPM = $productive_working_hours_minutes % 60;

                $productive_working_hours =sprintf('%02d:%02d', $hoursPM, $minutesPM);
            }else{
                $productive_working_hours_minutes = $total_minutes;

                $hoursPM  = floor($productive_working_hours_minutes/60);
                $minutesPM = $productive_working_hours_minutes % 60;

                $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
            }

            // END
            

            if ($track_user_location == 1) {
                $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND gps_accuracy <= 100  AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT'");

                $query11Data=mysqli_fetch_array($query11);

                if ($query11Data['SUM(last_distance)'] > 0) {
                    $total_travel_meter = $query11Data['SUM(last_distance)'].'';
                }else{
                    $total_travel_meter = '0';
                }
            }

            $m->set_data('attendance_date_end',$punchOUTDate);
            $m->set_data('punch_out_time',$punchOUTTime);
            $m->set_data('total_working_hours',$totalHours);
            $m->set_data('punch_out_latitude',$punch_out_latitude);
            $m->set_data('punch_out_longitude',$punch_out_longitude);
            $m->set_data('punch_out_image',$punch_out_image);
            $m->set_data('punch_out_reason',$punch_out_reason);
            $m->set_data('early_out',$early_out);
            $m->set_data('punch_out_in_range',$punch_out_in_range);
            $m->set_data('punch_out_meter',$punch_out_meter);
            $m->set_data('punch_out_km',$punch_out_km);
            $m->set_data('early_out_reason',$early_out_reason);
            $m->set_data('avg_working_days',$avg_working_days);
            $m->set_data('punch_out_address',$punch_out_address);
            $m->set_data('punch_in_address',$punch_in_address);
            $m->set_data('total_working_minutes',$total_minutes);
            $m->set_data('extra_working_hours',$extra_working_hours);
            $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
            $m->set_data('total_travel_meter',$total_travel_meter);
            $m->set_data('productive_working_hours',$productive_working_hours);
            $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
            $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
            $m->set_data('punch_out_branch',$location_name.'');
            $m->set_data('attendance_out_from',"1");
            $m->set_data('last_punch_in_date',$last_punch_in_date.'');
            $m->set_data('last_punch_in_time',$last_punch_in_time.'');
            $m->set_data('punch_out_gps_accuracy',$punch_out_gps_accuracy.'');

            $a = array(
                'attendance_date_end'=>$m->get_data('attendance_date_end'),
                'punch_out_time'=>$m->get_data('punch_out_time'),
                'total_working_hours'=>$m->get_data('total_working_hours'),
                'punch_out_latitude'=>$m->get_data('punch_out_latitude'),
                'punch_out_longitude'=>$m->get_data('punch_out_longitude'),
                'punch_out_image'=>$m->get_data('punch_out_image'),
                'punch_out_reason'=>$m->get_data('punch_out_reason'),
                'early_out'=>$m->get_data('early_out'),
                'punch_out_in_range'=>$m->get_data('punch_out_in_range'),
                'punch_out_meter'=>$m->get_data('punch_out_meter'),
                'punch_out_km'=>$m->get_data('punch_out_km'),
                'early_out_reason'=>$m->get_data('early_out_reason'),
                'avg_working_days'=>$m->get_data('avg_working_days'),
                'total_travel_meter'=>$m->get_data('total_travel_meter'),
                'punch_out_address'=>$m->get_data('punch_out_address'),
                'total_working_minutes'=>$m->get_data('total_working_minutes'),
                'extra_working_hours'=>$m->get_data('extra_working_hours'),
                'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
                'total_travel_meter'=>$m->get_data('total_travel_meter'),
                'productive_working_hours'=>$m->get_data('productive_working_hours'),
                'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),
                'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                'attendance_out_from'=>$m->get_data('attendance_out_from'),
                'punch_out_branch'=>$m->get_data('punch_out_branch'),
                'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
                'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
                'punch_out_gps_accuracy'=>$m->get_data('punch_out_gps_accuracy'),
            );

            $punchOutQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

            if ($punchOutQry == true) {

                if ($punch_out_in_range == "1") {
                    $m->set_data('attendance_status',"0");

                    $a = array(
                        'attendance_status'=>$m->get_data('attendance_status'),
                    );

                    $qqry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

                    $title = "Attendance Approval Request";
                    $description = "Requested by, ".$user_name;

                    $access_type = $pending_attendance_access;

                    include 'check_access_data_user.php';

                    // To Employee App
                    if (count($userIDArray) > 0) {

                        $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                        $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                        $dataAry = array(
                            "block_id"=>$block_id,
                            "floor_id"=>$floor_id,
                            "user_id"=>$user_id,
                        );

                        $dataJson = json_encode($dataAry);

                        $nResident->noti("pending_attendance","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                        $nResident->noti_ios("pending_attendance","",$society_id,$fcmArrayIosPA,$title,$description,$dataJson);

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"pending_attendance","attendance_tracker.png",$dataJson,"users_master.user_id IN ('$userFcmIds')");
                    }

                    $block_id=$d->getBlockid($user_id);
                    $fcmArray=$d->selectAdminBlockwise("17",$block_id,"android");
                    $fcmArrayIos=$d->selectAdminBlockwise("17",$block_id,"ios");
              
                    $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"pendingAttendance");

                    $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"pendingAttendance");

                    $notiAry = array(
                      'society_id'=>$society_id,
                      'notification_tittle'=>$title,
                      'notification_description'=>$description,
                      'notifiaction_date'=>date('Y-m-d H:i'),
                      'notification_action'=>"pendingAttendance",
                      'admin_click_action '=>"pendingAttendance",
                      'notification_logo'=>'attendance_tracker.png',
                    );
                                
                    $d->insert("admin_notification",$notiAry);
                }

                // Check Half Day


                $leaveValue = false;

                if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                    $leaveValue = true;
                } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $punchOUTTime) {
                    $leaveValue = true;
                }

                $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);


                if ($total_minutes >= $minimum_hours_for_full_day_min && $minimum_hours_for_full_day != "00:00:00") {
                    $noLeave = true;
                }else{
                    $noLeave = false;
                }

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$user_id'");
                $data_notification = mysqli_fetch_array($qsm);
                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];
                $tabPosition = "0"; // 1 for all leaves, 0 for my leaves

                $alreadyLeaveQryCheck = false; 

                if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {

                    $leave_total_days = 1;

                    // check half day and full day


                    if ($total_minutes < $maximum_halfday_hours_min) {
                        $leaveTypeName = "Full Day";
                        $leaveType = "0";
                        $avgWorkingDays = "0";
                        $half_day_session = "0";                
                        $is_leave = "1";                
                        $extra_day_leave_type = "1";                
                    }else{
                        $leaveType = "1";
                        $avgWorkingDays = "0.5";
                        $leaveTypeName = "Half Day";
                        $is_leave = "2";
                        $extra_day_leave_type = "2";
                    }

                    if ($is_extra_day == 1) {
                        $is_leave = "0";
                    }else{
                        $extra_day_leave_type = "0";
                    }

                    if ($hasLeave == true) {

                        // Change already applied leave

                        $title = "Auto Leave Applied - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed (".$totalHoursNames.")";
                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                        $m->set_data('leave_type_id',$leave_type_id);
                        $m->set_data('paid_unpaid',$paid_unpaid);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('half_day_session',$half_day_session);

                        if ($auto_leave_reason != '') {
                            // code...
                            $m->set_data('auto_leave_reason',$titleMessage);

                            $a = array(
                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                'half_day_session'=>$m->get_data('half_day_session'),
                                'auto_leave_reason'=>$m->get_data('auto_leave_reason')
                            );
                        }else{
                            $a = array(
                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                'leave_day_type'=>$m->get_data('leave_day_type'),
                                'half_day_session'=>$m->get_data('half_day_session')
                            );
                        }

                        $leaveQry = $d->update("leave_master",$a,"user_id = '$user_id' AND leave_id = '$leave_id'");                                               
                    }else{

                        // Auto Leave

                        $title = "Auto Leave Applied"." - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed (".$totalHoursNames.")";
                        $description = "For Date : ".$punchOUTDate."\n".$titleMessage;

                        $m->set_data('leave_type_id',"0");
                        $m->set_data('society_id',$society_id);
                        $m->set_data('paid_unpaid',"1");
                        $m->set_data('unit_id',$unit_id);
                        $m->set_data('user_id',$user_id);
                        $m->set_data('auto_leave_reason',$titleMessage);
                        $m->set_data('leave_start_date',$punchOUTDate);
                        $m->set_data('leave_end_date',$punchOUTDate);
                        $m->set_data('leave_total_days',$leave_total_days);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('leave_status',"1");
                        $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'society_id' =>$m->get_data('society_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                            'leave_start_date'=>$m->get_data('leave_start_date'),
                            'leave_end_date'=>$m->get_data('leave_end_date'),
                            'leave_total_days'=>$m->get_data('leave_total_days'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'leave_status'=>$m->get_data('leave_status'),
                            'leave_created_date'=>$m->get_data('leave_created_date'),
                        );

                        $leaveQry = $d->insert("leave_master",$a);

                        $alreadyLeaveQryCheck = true;

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                    }

                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',$avgWorkingDays);
                    $m->set_data('is_leave',$is_leave);
                    $m->set_data('extra_day_leave_type',$extra_day_leave_type);

                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'is_leave'=>$m->get_data('is_leave'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                }else if($noLeave == true && $hasLeave == true){

                    // Remove leave if present and full time

                    $leaveQry = $d->delete("leave_master","user_id = '$user_id' AND leave_id = '$leave_id'");

                    $m->set_data('auto_leave',"0");

                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");

                    $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                    $title = "Applied Leave Cancelled";
                    $description = "For Date : ".$attendance_date_start;

                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");
                }

                // Code - Maximum Late IN & Early OUT Limit Check -> Apply Leave

                $startMonthDate = date('Y-m-01');
                $endMonthDate = date('Y-m-t');


                $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND late_in = '1'");

                $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND early_out = '1'");

                $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                 if ($maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($todayLateIn == "1" || $early_out == "1")) {

                    $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";

                    $m->set_data('leave_type_id',"0");
                    $m->set_data('society_id',$society_id);
                    $m->set_data('paid_unpaid',"1");
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('user_id',$user_id);
                    $m->set_data('auto_leave_reason',$titleMessage);
                    $m->set_data('leave_start_date',$punchOUTDate);
                    $m->set_data('leave_end_date',$punchOUTDate);
                    $m->set_data('leave_total_days',"1");
                    $m->set_data('leave_day_type',"1");
                    $m->set_data('leave_status',"1");
                    $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                    $a = array(
                        'leave_type_id'=>$m->get_data('leave_type_id'),
                        'society_id' =>$m->get_data('society_id'),
                        'paid_unpaid' =>$m->get_data('paid_unpaid'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'), 
                        'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                        'leave_start_date'=>$m->get_data('leave_start_date'),
                        'leave_end_date'=>$m->get_data('leave_end_date'),
                        'leave_total_days'=>$m->get_data('leave_total_days'),
                        'leave_day_type'=>$m->get_data('leave_day_type'),
                        'leave_status'=>$m->get_data('leave_status'),
                        'leave_created_date'=>$m->get_data('leave_created_date'),
                    );

                    $leaveQry = $d->insert("leave_master",$a);

                    $title = "Auto Leave Applied - Half Day";

                    $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$user_id'");

                    if ($is_extra_day == 1) {
                        $is_leave = "0";
                    }else{
                        $extra_day_leave_type = "2";
                    }

                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',"0.5");
                    $m->set_data('is_leave',$is_leave);
                    $m->set_data('extra_day_leave_type',$extra_day_leave_type);


                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                        'is_leave'=>$m->get_data('is_leave'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                }

                // END checking half day

                // Breaks End if any start

                $attHisQry = $d->selectRow("attendance_break_history_id,break_end_date,break_out_time,break_start_date,break_in_time","attendance_break_history_master","attendance_id = '$attendance_id' AND break_end_date = '0000-00-00' AND break_out_time = '00:00:00'");

                if (mysqli_num_rows($attHisQry) > 0) {
                    while($attData = mysqli_fetch_array($attHisQry)){
                        $attendance_break_history_id = $attData['attendance_break_history_id'];
                        $break_start_date = $attData['break_start_date'];
                        $break_in_time = $attData['break_in_time'];
                        $break_end_date = date('Y-m-d');
                        $break_out_time = date('H:i:s');

                        $total_break_time = getTotalHours($break_start_date,$break_end_date,
                            $break_in_time,$break_out_time);

                        $time = explode(':', $total_break_time.":00");
                        $total_break_time_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                        $m->set_data('break_end_date',$break_end_date);
                        $m->set_data('break_out_time',$break_out_time);
                        $m->set_data('total_break_time',$total_break_time);
                        $m->set_data('total_break_time_minutes',$total_break_time_minutes);

                        $a = array(
                            'break_end_date'=>$m->get_data('break_end_date'),
                            'break_out_time'=>$m->get_data('break_out_time'),
                            'total_break_time'=>$m->get_data('total_break_time'),
                            'total_break_time_minutes'=>$m->get_data('total_break_time_minutes'),
                        );

                        $qry = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id' AND attendance_id = '$attendance_id'");
                    }
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['punchInRequest']=="punchInRequest" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $inReqDate = date('m-Y',strtotime($punch_in_date));

            $qry = $d->count_data_direct("salary_slip_id","salary_slip_master","user_id = '$user_id' AND salary_month_name = '$inReqDate'");

            if ($qry > 0) {
                $response["message"] = "This month's salary was already generated. Therefor, you can not apply attendance request";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $uq = $d->selectRow("users_master.shift_time_id,user_employment_details.joining_date,shift_timing_master.per_day_hour","users_master,user_employment_details,shift_timing_master","users_master.society_id ='$society_id' AND users_master.user_id='$user_id' AND user_employment_details.user_id = users_master.user_id AND shift_timing_master.shift_time_id = users_master.shift_time_id");

            $ud = mysqli_fetch_array($uq);
            $joining_date = date('Y-m-d',strtotime($ud['joining_date']));
            $total_shift_hours =$ud['per_day_hour'];

            if ($ud['joining_date'] == null && $ud['joining_date'] == '' && $ud['joining_date'] == '0000-00-00' && $punch_in_date < $joining_date) {
                $response["message"] = "You can not request attendance befor the date of joining";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            if ($ud['shift_time_id'] == 0) {
                $response["message"] = "Shift not assigned";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $isPunchIn = $d->select("attendance_master","user_id = '$user_id' AND society_id = '$society_id'  AND attendance_date_start = '$punch_in_date'");

            if (mysqli_num_rows($isPunchIn) > 0) {
                $response["message"] = "You have already punched in for ".$punch_in_date;
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            }

            $parts = explode(':', $total_shift_hours);
            $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);


            $total_working_hours = getTotalHours($punch_in_date,$punch_out_date,
                                $punch_in_time,$punch_out_time);

            $time = explode(':', $total_working_hours.":00");
            $total_working_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

            $avgWorkingDays = $total_working_minutes/$perDayHourMinute;
            $avg_working_days = round($avgWorkingDays * 2) / 2;

            $extra_working_hours_minutes = 0;
            $extra_working_hours = "";

            if ($total_working_minutes > $perDayHourMinute) {
                $extra_working_hours_minutes = $total_working_minutes - $perDayHourMinute;
                $hours  = floor($extra_working_hours_minutes/60);
                $minutes = $extra_working_hours_minutes % 60;

                $extra_working_hours = $hours.':'.$minutes;
            }

            $productive_working_hours_minutes = $total_working_minutes;

            $hoursPM  = floor($productive_working_hours_minutes/60);
            $minutesPM = $productive_working_hours_minutes % 60;

            $productive_working_hours = $hoursPM.':'.$minutesPM;

            $dataArray = array();

            $multPunchAry = array(
                "punch_in_date" => $punch_in_date.'',
                "punch_in_time" => $punch_in_time.'',
                "punch_out_date" => $punch_out_date.'',
                "punch_out_time" => $punch_out_time.'',
                "punch_in_image" => "",
                "punch_out_image" => "",
                "location_name_in" => $location_name.'',
                "location_name_out" => $location_name.'',
                "working_hour" => $total_working_hours.'',
                "working_hour_minute" => $total_working_minutes.'',
            );

            array_push($dataArray,$multPunchAry);
            $multPunchDataJson = json_encode($dataArray);
 
            $m->set_data('shift_time_id',$shift_time_id);
            $m->set_data('total_shift_hours',$total_shift_hours);
            $m->set_data('society_id',$society_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('attendance_date_start',$punch_in_date);
            $m->set_data('attendance_date_end',$punch_out_date);
            $m->set_data('punch_in_time',$punch_in_time);
            $m->set_data('last_punch_in_time',$punch_in_time);
            $m->set_data('last_punch_in_date',$punch_in_date);
            $m->set_data('punch_out_time',$punch_out_time);
            $m->set_data('attendance_reason',$punch_in_reason);
            $m->set_data('total_working_hours',$total_working_hours);
            $m->set_data('total_working_minutes',$total_working_minutes);
            $m->set_data('extra_working_hours',$extra_working_hours);
            $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
            $m->set_data('avg_working_days',$avg_working_days);
            $m->set_data('productive_working_hours',$productive_working_hours);
            $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
            $m->set_data('punch_in_request_day_type',$day_type);
            $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
            $m->set_data('punch_in_branch',$location_name);
            $m->set_data('punch_out_branch',$location_name);
            $m->set_data('attendance_status',"0");
            $m->set_data('punch_in_request',"1");
            $m->set_data('attendance_created_time',date('Y-m-d H:i:s'));

            $a = array(
                'shift_time_id'=>$m->get_data('shift_time_id'),
                'total_shift_hours'=>$m->get_data('total_shift_hours'),
                'society_id' =>$m->get_data('society_id'),
                'unit_id'=>$m->get_data('unit_id'),
                'user_id'=>$m->get_data('user_id'),
                'attendance_date_start'=>$m->get_data('attendance_date_start'),
                'attendance_date_end'=>$m->get_data('attendance_date_end'),
                'punch_in_time'=>$m->get_data('punch_in_time'),
                'punch_out_time'=>$m->get_data('punch_out_time'),
                'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
                'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
                'attendance_reason'=>$m->get_data('attendance_reason'),
                'attendance_status'=>$m->get_data('attendance_status'),
                'punch_in_request'=>$m->get_data('punch_in_request'),
                'total_working_hours'=>$m->get_data('total_working_hours'),
                'total_working_minutes'=>$m->get_data('total_working_minutes'),
                'extra_working_hours'=>$m->get_data('extra_working_hours'),
                'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
                'avg_working_days'=>$m->get_data('avg_working_days'),
                'productive_working_hours'=>$m->get_data('productive_working_hours'),
                'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),
                'punch_in_request_day_type'=>$m->get_data('punch_in_request_day_type'),
                'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
                'attendance_created_time'=>$m->get_data('attendance_created_time'),
                'punch_in_branch'=>$m->get_data('punch_in_branch'),
                'punch_out_branch'=>$m->get_data('punch_out_branch'),
            );

            $punchInQry = $d->insert("attendance_master",$a);

            $attendance_id = $con->insert_id;

            if ($punchInQry == true) {

                $title = "Punch In Request";
                $description = "Requested by, ".$user_name."\nPunch In: ".date('d-m-Y',strtotime($punch_in_date))." ".date('h:i A',strtotime($punch_in_time))."\nPunch Out: ".date('d-m-Y',strtotime($punch_out_date))." ".date('h:i A',strtotime($punch_out_time));

                // Notification by Access

                $access_type = $punch_in_req_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmAccessAry=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmAccessAryiOS=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $nResident->noti("punch_in_request","",$society_id,$fcmAccessAry,$title,$description,"");
                    $nResident->noti_ios("punch_in_request","",$society_id,$fcmAccessAryiOS,$title,$description,"");

                    $d->insertUserNotification($society_id,$title,$description,"punch_in_request","attendance_tracker.png","users_master.user_id IN ('$userFcmIds')");
                }

                //Notification To Admin

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("17",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("17",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"pendingAttendance");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"pendingAttendance");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"pendingAttendance",
                  'admin_click_action '=>"pendingAttendance",
                  'notification_logo'=>'attendance_tracker.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Request Sent";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['approvePunchOutRequest']=="approvePunchOutRequest" && $attendance_id!='' && $attendance_punch_out_missing_id!='' && $attendance_user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $qqry1 = $d->selectRow("shift_timing_master.*,attendance_master.multiple_punch_in_out_data,attendance_master.attendance_id,attendance_master.is_extra_day,attendance_master.attendance_date_start","attendance_master,shift_timing_master","attendance_master.attendance_id = '$attendance_id' AND attendance_master.user_id = '$attendance_user_id' AND attendance_master.shift_time_id = shift_timing_master.shift_time_id");
            $shiftData = mysqli_fetch_array($qqry1);
            $shiftTimeId = $shiftData['shift_time_id'];
            $perdayHours = $shiftData['per_day_hour'];
            $early_out_time = $shiftData['early_out_time'];
            $half_day_time_start = $shiftData['half_day_time_start'];
            $shift_end_time = $shiftData['shift_end_time'];
            $halfday_before_time = $shiftData['halfday_before_time'];
            $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
            $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
            $maximum_in_out = $shiftData['maximum_in_out'];
            $is_multiple_punch_in = $shiftData['is_multiple_punch_in'];


            $multiple_punch_in_out_data = $shiftData['multiple_punch_in_out_data'];
            $attendance_date_start = $shiftData['attendance_date_start'];
            $is_extra_day = $shiftData['is_extra_day'];
            $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

            $finalTotalMinute = 0;

            $dataArray = array();

            if (isset($multiplePunchDataArray) && $is_multiple_punch_in == 1 && count($multiplePunchDataArray) > 0) {

                for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                    if($multiplePunchDataArray[$i]["punch_out_date"] == '' 
                        || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){

                        $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$attendance_date,
                                $multiplePunchDataArray[$i]["punch_in_time"],$attendance_time);

                        $timeHr = explode(':', $totalHours.":00");
                        $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                        $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                        $oldAry = array(
                            "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"],
                            "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"],
                            "punch_out_date" => $attendance_date,
                            "punch_out_time" =>$attendance_time,
                            "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"],
                            "punch_out_image" => "",
                            "location_name_in" => $multiplePunchDataArray[$i]["location_name_in"],
                            "location_name_out" => $location_name,
                            "working_hour" => $totalHours.'',
                            "working_hour_minute" => $totalMinutes.'',
                        );

                        array_push($dataArray,$oldAry);
                    }else{
                        $oldAry = array(); 
                        $oldAry['punch_in_date'] = $multiplePunchDataArray[$i]["punch_in_date"];
                        $oldAry['punch_in_time'] = $multiplePunchDataArray[$i]["punch_in_time"];
                        $oldAry['punch_out_date'] = $multiplePunchDataArray[$i]["punch_out_date"];
                        $oldAry['punch_out_time'] = $multiplePunchDataArray[$i]["punch_out_time"];
                        $oldAry['punch_in_image'] = $multiplePunchDataArray[$i]["punch_in_image"];
                        $oldAry['punch_out_image'] = $multiplePunchDataArray[$i]["punch_out_image"];
                        $oldAry['location_name_in'] = $multiplePunchDataArray[$i]["location_name_in"];
                        $oldAry['location_name_out'] = $multiplePunchDataArray[$i]["location_name_out"];
                        $oldAry['working_hour'] = $multiplePunchDataArray[$i]["working_hour"];
                        $oldAry['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"];
                        
                        $finalTotalMinute = $finalTotalMinute + $multiplePunchDataArray[$i]["working_hour_minute"];
                        array_push($dataArray,$oldAry);
                    }
                }  

                $multPunchDataJson = json_encode($dataArray).'';
            }else{
                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                    for ($i=0; $i < count($multiplePunchDataArray) ; $i++) { 
                        if($multiplePunchDataArray[$i]["punch_out_date"] == '' || $multiplePunchDataArray[$i]["punch_out_date"] == '0000-00-00'){
                            $totalHours = getTotalHours($multiplePunchDataArray[$i]["punch_in_date"],$attendance_date,
                                    $multiplePunchDataArray[$i]["punch_in_time"],$attendance_time);

                            $timeHr = explode(':', $totalHours.":00");
                            $totalMinutes = ($timeHr[0]*60) + ($timeHr[1]) + ($timeHr[2]/60);
                            $finalTotalMinute = $finalTotalMinute + $totalMinutes;
                            $oldAry = array(
                                "punch_in_date" => $multiplePunchDataArray[$i]["punch_in_date"].'',
                                "punch_in_time" => $multiplePunchDataArray[$i]["punch_in_time"].'',
                                "punch_out_date" => $attendance_date.'',
                                "punch_out_time" =>$attendance_time.'',
                                "punch_in_image" => $multiplePunchDataArray[$i]["punch_in_image"].'',
                                "punch_out_image" => "",
                                "location_name_in" => $multiplePunchDataArray[$i]["location_name_in"].'',
                                "location_name_out" => $location_name.'',
                                "working_hour" => $totalHours.'',
                                "working_hour_minute" => $finalTotalMinute.'',
                            );
                            array_push($dataArray,$oldAry);
                        }
                    }

                    $multPunchDataJson = json_encode($dataArray).'';
                }
            }

            $qsm = $d->selectRow("user_token,device,unit_id,user_full_name","users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
            $data_notification = mysqli_fetch_array($qsm);
            $user_token = $data_notification['user_token'];
            $device = $data_notification['device'];
            $unit_id = $data_notification['unit_id'];
            $tabPosition = "0"; 

            $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type,auto_leave_reason","leave_master","user_id = '$attendance_user_id' AND leave_start_date = '$attendance_date_start'");

            $hasLeave = false;

            if (mysqli_num_rows($qryLeave) > 0) {

                $hasLeave = true;

                $leaveData = mysqli_fetch_array($qryLeave);

                $leave_id = $leaveData['leave_id'];
                $leave_type_id = $leaveData['leave_type_id'];
                $paid_unpaid = $leaveData['paid_unpaid'];
                $leave_reason = $leaveData['leave_reason'];
                $leave_day_type = $leaveData['leave_day_type'];
                $half_day_session = $leaveData['half_day_session'];
                $auto_leave_reason = $leaveData['auto_leave_reason'];
            }

            $parts = explode(':', $perdayHours);
            $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $datePunchOUT = $attendance_date ." ".$attendance_time;
            $datePunchIN = $attendance_date_start ." ".$punch_in_time;

            if ($early_out_time != '') {
                $partEO = explode(':', $early_out_time);
                $min = ($partEO[0]*60) + ($partEO[1]) + ($partEO[2]/60);
            }else{
                $min = 0;
            }
            
            $defaultOutTime = $attendance_date." ".$shift_end_time;
            $strMin = "-".$min." minutes";
            $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

            if ($early_out_time != '') {
                if ($datePunchOUT >= $defaultOutTime) {
                    $early_out = "0";
                }else{
                    $early_out = "1";              
                }
            }else{            
                $early_out = "0";              
            }

            if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                $early_out = "0";
            }

            if ($is_multiple_punch_in == 0) {
                
                $totalHours = getTotalHoursWithNames($attendance_date_start,$attendance_date,$punch_in_time,$attendance_time);

                $totalHR = getTotalHours($attendance_date_start,$attendance_date,$punch_in_time,$attendance_time);

                $time = explode(':', $totalHR.":00");
                $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);
            }else{
                $total_minutes =  $finalTotalMinute;

                $hoursPM  = floor($total_minutes/60);
                $minutesPM = $total_minutes % 60;

                $totalHR = sprintf('%02d:%02d', $hoursPM, $minutesPM);
                $times[] = $totalHR;

                if ($hoursPM > 0 && $minutesPM) {
                    $totalHoursNames = sprintf('%02d hr %02d min', $hoursPM, $minutesPM);
                }else if ($hoursPM > 0 && $minutesPM <= 0) {
                    $totalHoursNames = sprintf('%02d hr', $hoursPM);
                }else if ($hoursPM <= 0 && $minutesPM > 0) {
                    $totalHoursNames = sprintf('%02d min', $minutesPM);
                }else{
                    $totalHoursNames = "";
                }
            }

            $avgWorkingDays = $total_minutes/$perDayHourMinute;

            $avg_working_days = round($avgWorkingDays * 2) / 2;

            $extra_working_hours_minutes = 0;

            if ($total_minutes > $perDayHourMinute) {
                $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
                $hours  = floor($extra_working_hours_minutes/60);
                $minutes = $extra_working_hours_minutes % 60;

                $extra_working_hours = sprintf('%02d:%02d', $hours, $minutes);
            }

            $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

            $qryBreakData = mysqli_fetch_array($qryBreak);

            $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

            if ($total_minutes > $totalBreakinutes) {
                $productive_working_hours_minutes = $total_minutes - $totalBreakinutes;

                $hoursPM  = floor($productive_working_hours_minutes/60);
                $minutesPM = $productive_working_hours_minutes % 60;

                $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
            }else{
                $productive_working_hours_minutes = $total_minutes;

                $hoursPM  = floor($productive_working_hours_minutes/60);
                $minutesPM = $productive_working_hours_minutes % 60;

                $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
            }

            $query11 = $d->sum_data("last_distance","user_back_track_master","user_id = '$user_id' AND gps_accuracy <= 100  AND user_back_track_date BETWEEN '$datePunchIN' AND '$datePunchOUT'");

            $query11Data=mysqli_fetch_array($query11);

            $total_travel_meter = "0";
            
            if ($query11Data['SUM(last_distance)'] > 0) {
                $total_travel_meter = $query11Data['SUM(last_distance)']."";
            }

            $m->set_data('attendance_punch_out_missing_status',"1");
            $m->set_data('attendance_punch_out_missing_status_changed_by',$user_id);
            $m->set_data('attendance_date',$attendance_date);
            $m->set_data('attendance_time',$attendance_time);
            $m->set_data('total_travel_meter',$total_travel_meter);

            $aaa = array(
                'attendance_punch_out_missing_status'=>$m->get_data('attendance_punch_out_missing_status'),
                'attendance_punch_out_missing_status_changed_by'=>$m->get_data('attendance_punch_out_missing_status_changed_by'),
                'attendance_date'=>$m->get_data('attendance_date'),
                'attendance_time'=>$m->get_data('attendance_time'),
            );

            $qr1 = $d->update("attendance_punch_out_missing_request",$aaa,"attendance_punch_out_missing_id = '$attendance_punch_out_missing_id' AND user_id = '$attendance_user_id'");


            if ($qr1 == true) {

                $m->set_data('attendance_status',"1");
                $m->set_data('attendance_status_change_by',$user_id);
                $m->set_data('attendance_date_end',$attendance_date);
                $m->set_data('punch_out_time',$attendance_time);
                $m->set_data('early_out',$early_out);
                $m->set_data('total_working_hours',$totalHR);
                $m->set_data('extra_working_hours',$extra_working_hours);
                $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
                $m->set_data('avg_working_days',$avg_working_days);
                $m->set_data('productive_working_hours',$productive_working_hours);
                $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);
                $m->set_data('total_working_minutes',$total_minutes);
                $m->set_data('total_travel_meter',$total_travel_meter);
                $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
                $m->set_data('punch_out_branch',$location_name);
                $m->set_data('punch_out_missing_req_approve_date',date('Y-m-d H:i:s'));

                $a1 = array(
                    'attendance_status'=>$m->get_data('attendance_status'),
                    'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
                    'attendance_date_end'=>$m->get_data('attendance_date_end'),
                    'punch_out_time'=>$m->get_data('punch_out_time'),
                    'early_out'=>$m->get_data('early_out'),
                    'extra_working_hours'=>$m->get_data('extra_working_hours'),
                    'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
                    'avg_working_days'=>$m->get_data('avg_working_days'),
                    'productive_working_hours'=>$m->get_data('productive_working_hours'),                
                    'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),
                    'total_working_hours'=>$m->get_data('total_working_hours'),                
                    'total_working_minutes'=>$m->get_data('total_working_minutes'),                    
                    'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),                   
                    'punch_out_branch'=>$m->get_data('punch_out_branch'),                    
                    'total_travel_meter'=>$m->get_data('total_travel_meter'),                 
                    'punch_out_missing_req_approve_date'=>$m->get_data('punch_out_missing_req_approve_date'),                 
                );

                $q2 = $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");

                if ($q2 == true) {

                    $leaveValue = false;

                    if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                        $leaveValue = true;
                    } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $attendance_time) {
                        $leaveValue = true;
                    }

                    $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                    $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);

                    if ($total_minutes >= $minimum_hours_for_full_day_min && $minimum_hours_for_full_day != "00:00:00") {
                        $noLeave = true;
                    }else{
                        $noLeave = false;
                    }

                    $alreadyLeaveQryCheck = false; 

                    if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {


                        $leave_total_days = 1;

                        // check half day and full day

                        if ($total_minutes < $maximum_halfday_hours_min) {
                            $avgCount = "0";
                            $leaveTypeName = "Full Day";
                            $leaveType = "0";
                            $is_leave = "1";
                            $extra_day_leave_type = "1";
                            $half_day_session = "0";                
                        }else{
                            $avgCount = "0.5";
                            $leaveType = "1";
                            $is_leave = "2";
                            $extra_day_leave_type = "2";
                            $leaveTypeName = "Half Day";
                        }

                        if ($is_extra_day == 1) {
                            $is_leave = "0";
                        }else{
                            $extra_day_leave_type = "0";
                        }

                        if ($hasLeave == true) {

                            // Change already applied leave

                            $title = "Auto Leave Applied - ".$leaveTypeName;
                            $titleMessage = "Working hours not completed ($totalHours)";
                            $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                            $m->set_data('leave_type_id',$leave_type_id);
                            $m->set_data('paid_unpaid',$paid_unpaid);
                            $m->set_data('leave_day_type',$leaveType);
                            $m->set_data('half_day_session',$half_day_session);

                            if ($auto_leave_reason != '') {
                                $m->set_data('auto_leave_reason',$titleMessage);

                                $a = array(
                                    'leave_type_id'=>$m->get_data('leave_type_id'),
                                    'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                    'leave_day_type'=>$m->get_data('leave_day_type'),
                                    'half_day_session'=>$m->get_data('half_day_session'),
                                    'auto_leave_reason'=>$m->get_data('auto_leave_reason')
                                );
                            }else{
                                $a = array(
                                    'leave_type_id'=>$m->get_data('leave_type_id'),
                                    'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                    'leave_day_type'=>$m->get_data('leave_day_type'),
                                    'half_day_session'=>$m->get_data('half_day_session')
                                );
                            }

                            $leaveQry = $d->update("leave_master",$a,"user_id = '$attendance_user_id' AND leave_id = '$leave_id'");
                            

                            /*if ($device == 'android') {
                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            }

                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");*/
                            
                        }else{

                            // Auto Leave

                            $title = "Auto Leave Applied"." - ".$leaveTypeName;
                            $titleMessage = "Working hours not completed ($totalHours)";
                            $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                            $m->set_data('leave_type_id',"0");
                            $m->set_data('society_id',$society_id);
                            $m->set_data('paid_unpaid',"1");
                            $m->set_data('unit_id',$unit_id);
                            $m->set_data('user_id',$attendance_user_id);
                            $m->set_data('auto_leave_reason',$titleMessage);
                            $m->set_data('leave_start_date',$attendance_date_start);
                            $m->set_data('leave_end_date',$attendance_date_start);
                            $m->set_data('leave_total_days',$leave_total_days);
                            $m->set_data('leave_day_type',$leaveType);
                            $m->set_data('leave_status',"1");
                            $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                            $a = array(
                                'leave_type_id'=>$m->get_data('leave_type_id'),
                                'society_id' =>$m->get_data('society_id'),
                                'paid_unpaid' =>$m->get_data('paid_unpaid'),
                                'unit_id'=>$m->get_data('unit_id'),
                                'user_id'=>$m->get_data('user_id'),
                                'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                                'leave_start_date'=>$m->get_data('leave_start_date'),
                                'leave_end_date'=>$m->get_data('leave_end_date'),
                                'leave_total_days'=>$m->get_data('leave_total_days'),
                                'leave_day_type'=>$m->get_data('leave_day_type'),                                
                                'leave_status'=>$m->get_data('leave_status'),
                                'leave_created_date'=>$m->get_data('leave_created_date'),
                            );

                            $leaveQry = $d->insert("leave_master",$a);

                            $alreadyLeaveQryCheck = true;

                            if ($device == 'android') {
                                $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            } else if ($device == 'ios') {
                                $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                            }

                            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                        }

                        $m->set_data('auto_leave',"1");
                        $m->set_data('avg_working_days',$avgCount);
                        $m->set_data('is_leave',$is_leave);
                        $m->set_data('extra_day_leave_type',$extra_day_leave_type);


                        $a1 = array(
                            'auto_leave'=>$m->get_data('auto_leave'),
                            'avg_working_days'=>$m->get_data('avg_working_days'),
                            'is_leave'=>$m->get_data('is_leave'),
                            'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),

                        );

                        $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                        
                    }else if($noLeave == true && $hasLeave == true){
                        // Remove leave if present and full time

                        $leaveQry = $d->delete("leave_master","user_id = '$attendance_user_id' AND leave_id = '$leave_id'");

                        $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                        $title = "Applied Leave Cancelled";
                        $description = "For Date : ".$attendance_date_start;

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                    }

                    // Code - Maximum Late IN & Early OUT Limit Check -> Apply Leave

                    $startMonthDate = date('Y-m-01');
                    $endMonthDate = date('Y-m-t');


                    $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND late_in = '1'");

                    $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND early_out = '1'");

                    $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                    if ($hasLeave == false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($late_in == "1" || $early_out == "1")) {

                        $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";

                        $m->set_data('leave_type_id',"0");
                        $m->set_data('society_id',$society_id);
                        $m->set_data('paid_unpaid',"1");
                        $m->set_data('unit_id',$unit_id);
                        $m->set_data('user_id',$attendance_user_id);
                        $m->set_data('auto_leave_reason',$titleMessage);
                        $m->set_data('leave_start_date',$attendance_date_start);
                        $m->set_data('leave_end_date',$attendance_date);
                        $m->set_data('leave_total_days',"1");
                        $m->set_data('leave_day_type',"1");
                        $m->set_data('leave_status',"1");
                        $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'society_id' =>$m->get_data('society_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'), 
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                            'leave_start_date'=>$m->get_data('leave_start_date'),
                            'leave_end_date'=>$m->get_data('leave_end_date'),
                            'leave_total_days'=>$m->get_data('leave_total_days'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'leave_status'=>$m->get_data('leave_status'),
                            'leave_created_date'=>$m->get_data('leave_created_date'),
                        );

                        $leaveQry = $d->insert("leave_master",$a);

                        $title = "Auto Leave Applied - Half Day";

                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");

                        if ($is_extra_day == 1) {
                            $is_leave = "0";
                        }else{
                            $extra_day_leave_type = "0";
                        }

                        $m->set_data('auto_leave',"1");
                        $m->set_data('avg_working_days',"0.5");
                        $m->set_data('is_leave',$is_leave);
                        $m->set_data('extra_day_leave_type',$extra_day_leave_type);

                        $a1 = array(
                            'auto_leave'=>$m->get_data('auto_leave'),
                            'avg_working_days'=>$m->get_data('avg_working_days'),
                            'is_leave'=>$m->get_data('is_leave'),
                            'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                        );

                        $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                    }

                    $title = "$attendance_date - Punch Out Missing Request Approved";
                    $description = "For Date: ".date('d-m-Y',strtotime($attendance_date))." Time: ".date('h:i A',strtotime($attendance_time))."\nBy, ".$user_name;

                    $tabPosition = "1";

                    if ($device == 'android') {
                        $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                    $response["message"] = "Request Approved";
                    $response["status"] = "200";
                    echo json_encode($response);
                }else{
                    $response["message"] = "Something went wrong, please try again!";
                    $response["status"] = "201";
                    echo json_encode($response);
                } 
            }
                   
        }else if($_POST['getMonthlyAttendanceHistoryNew']=="getMonthlyAttendanceHistoryNew" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $qq12 = $d->selectRow("hide_working_hours","society_master","society_id = '$society_id'");

            $socData = mysqli_fetch_array($qq12);

            if ($socData['hide_working_hours'] == 1) {
                $response['hide_working_hours'] = true;
            }else{
                $response['hide_working_hours'] = false;                
            }

            if ($shift_time_id == '') {

                $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

                if (mysqli_num_rows($sq)>0) {
                    $sdata = mysqli_fetch_array($sq);
                    $shift_time_id = $sdata['shift_time_id'];
                }
            }

            $response['monthly_history'] = array();

            $datesArrayOfMonth = range_date($month_start_date,$month_end_date);

            $response['month'] = date('F Y',strtotime($month_start_date));

            $holidayAry = array();
            $weekAry = array();


            $currdt= strtotime($month_start_date);
            $nextmonth=strtotime($month_start_date."+1 month");
            $i=0;
            $flag=true;

            $totalMonthHours = 0;

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHoursOld = $shiftData['per_day_hour'];
            $perdayHours = $shiftData['per_day_hour'];
            $shiftType = $shiftData['shift_type'];
            $shift_start_time = $shiftData['shift_start_time'];
            $shift_end_time = $shiftData['shift_end_time'];
            $lunch_break_start_time = $shiftData['lunch_break_start_time'];
            $lunch_break_end_time = $shiftData['lunch_break_end_time'];
            $tea_break_start_time = $shiftData['tea_break_start_time'];
            $tea_break_end_time = $shiftData['tea_break_end_time'];
            $half_day_time_start = $shiftData['half_day_time_start'];
            $late_time_start = $shiftData['late_time_start'];
            $early_out_time = $shiftData['early_out_time'];

            $parts = explode(':', $perdayHours);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);

            // Week
        
            for ($x = 0; $x < count($startarr); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";

                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);

                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $dayN= $date->format('l');
                    $dayDate = $date->format('Y-m-d');

                        // code...
                        $days = array();
                        $hDates = array();
                        $days['day'] = $dayN;
                        $days['date'] = $dayDate;
                        $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate)) ;

                        $holidayQry = $d->selectRow("holiday_start_date,holiday_name","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' AND holiday_status = '0' ");

                        $holidayData = mysqli_fetch_array($holidayQry);

                        $x1 = $x+1;
                        // holiday off
                        if (mysqli_num_rows($holidayQry)>0) {
                            array_push($holidayAry,$dayDate);
                            $minusHoliday += $perdayHours;
                        } else {
                            // full day off
                            if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                                array_push($weekAry,$dayDate);
                                $minusWeekOffHR += $perdayHours;
                            }

                            // alternate week day off
                            if ($shiftData['has_altenate_week_off'] == "1") {  
                                if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                    in_array($x1,$alternateWeekOff)) {
                                    array_push($weekAry,$dayDate);
                                    $minusAlternateWeekOffDays += $perdayHours;
                                }
                            }
                        }
                        array_push($daysArray['days'],$days);
                }

                $totalWeekDay = count($daysArray['days']);

                $totalWeekHour = $perdayHours * $totalWeekDay;                

                $totalWeekHour1 = ($totalWeekHour - $minusWeekOffHR) - $minusAlternateWeekOffDays;
                $totalWeekHour = $totalWeekHour1 - $minusHoliday;
                               
                $weekArray['total_hours'] = $totalWeekHour."";

                $totalMonthHours += $totalWeekHour."";
                $weekArray['totalMonthHours'] = $totalMonthHours."";

            }
            

            // Leave Data

            $leaveQry = $d->select("leave_master","society_id = '$society_id'
                    AND user_id = '$user_id'
                    AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'
                    AND leave_status = '1'");

            $leaveArray = array();
            $paidLeaveArray = array();
            $halfHeaveArray = array();
            $paidHalfHeaveArray = array();

            if (mysqli_num_rows($leaveQry) > 0) {

                while($leaveData = mysqli_fetch_array($leaveQry)){

                    if ($leaveData['leave_day_type'] == "0") {
                        $lary = range_date($leaveData['leave_start_date'],
                            $leaveData['leave_end_date']);

                        for ($a = 0; $a < count($lary); $a++) {
                            array_push($leaveArray,$lary[$a]);  

                            if ($leaveData['paid_unpaid'] == 0) {
                                array_push($paidLeaveArray,$lary[$a]);
                            }                          
                        } 
                    }else{
                        $laryHalf = range_date($leaveData['leave_start_date'],
                            $leaveData['leave_end_date']);

                        for ($b = 0; $b < count($laryHalf); $b++) {
                            array_push($halfHeaveArray,$laryHalf[$b]);

                            if ($leaveData['paid_unpaid'] == 0) {
                                array_push($paidHalfHeaveArray,$laryHalf[$b]);
                            }
                        }                        
                    }
                }
            }


            // Month

            $totalWeekOff = 0;
            $totalAbsent = count($datesArrayOfMonth);
            $totalAbsentDays = 0;
            $totalExtraDays = 0;
            $totalIncompleteDays = 0;

            $presentDaysAry = array();

            $totalMonthHourSpent = array();

            for ($j = 0; $j < count($datesArrayOfMonth); $j++) {
                
                $dates = array();

                $monthDate = $datesArrayOfMonth[$j];
                $month = date('Y-m',strtotime($monthDate));
                $dates['date'] = $monthDate;
                $day_pos_new =  date('w', strtotime($monthDate));
                $dates['date_name'] = date('dS F Y',strtotime($monthDate));
                $dates['day_name'] = date('l',strtotime($monthDate));

                $dates["total_spend_time"] = "0";

                $dates['holiday'] = false;
                $dates['week_off'] = false;
                $dates['work_report'] = false;
                $dates['leave'] = false;
                $dates['present'] = false;
                $dates['half_day'] = false;
                $dates['extra_day'] = false;
                $dates['late_in'] = false;
                $dates['early_out'] = false;
                $dates['is_punch_out_missing'] = false;
                $dates['punch_in_request_sent'] = false;

                $isDateGone = "false";

                $dates["punch_out_missing_message"] = "";

                if ($monthDate < date('Y-m-d')) {
                    $isDateGone = "true";
                    $dates['is_date_gone'] = true;
                }else{
                    $isDateGone = "false";
                    $dates['is_date_gone'] = false;
                }

                if (in_array($monthDate,$holidayAry)) {
                    $dates['holiday'] = true;
                }else{
                    if (in_array($monthDate,$weekAry)) {
                        $dates['week_off'] = true;
                        $totalWeekOff += 1;
                    }
                }

                $dates["leave_reason"] = "";
                $dates["auto_leave_reason"] = "";
                $dates["leave_type_name"] = "";
                $dates["leave_day_view"] = "";


                if (in_array($monthDate,$leaveArray)) {
                    $dates['leave'] = true;
                    $isDateGone = "false";

                    $leaveDataQry = $d->selectRow("leave_type_name,leave_reason,leave_day_type,auto_leave_reason,paid_unpaid,leave_master.leave_type_id","leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id ","leave_master.society_id = '$society_id' AND leave_master.user_id = '$user_id' AND leave_master.leave_status = '1' AND ('$monthDate' BETWEEN leave_master.leave_start_date AND leave_master.leave_end_date)");

                    if (mysqli_num_rows($leaveDataQry) > 0) {

                        $leaveDataTrue = mysqli_fetch_array($leaveDataQry);

                        if ($leaveDataTrue['leave_type_id'] != null && $leaveDataTrue['leave_type_id'] != 0){
                            $dates["leave_type_name"] = $leaveDataTrue['leave_type_name'];                    
                        }else{
                            $dates["leave_type_name"] = "";
                        }

                        if ($leaveDataTrue['paid_unpaid'] == 0) {
                            if ($leaveDataTrue['leave_day_type'] == 0) {
                                $dates["leave_day_view"] = "Paid Leave - Full Day";
                            }else{
                                $dates["leave_day_view"] = "Paid Leave - Half Day";
                            }
                        }else if ($leaveDataTrue['paid_unpaid'] == 1) {
                            if ($leaveDataTrue['leave_day_type'] == 0) {
                                $dates["leave_day_view"] = "Unpaid Leave - Full Day";
                            }else{
                                $dates["leave_day_view"] = "Unpaid Leave - Half Day";
                            }
                        }else{
                            $dates["leave_day_view"] = "";
                        }

                        $dates["leave_reason"] = $leaveDataTrue['leave_reason'].'';
                        $dates["auto_leave_reason"] = $leaveDataTrue['auto_leave_reason'].'';

                        if ($leaveDataTrue['leave_type_id'] > 0) {
                            $dates["auto_leave_reason"] = "";
                        }         
                    }
                }

                if (in_array($monthDate,$halfHeaveArray)) {
                    $dates['half_day'] = true;
                    $isDateGone = "false";

                    $leaveDataQry = $d->selectRow("leave_type_name,leave_reason,auto_leave_reason,paid_unpaid,leave_master.leave_type_id,leave_day_type","leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id ","leave_master.society_id = '$society_id' AND leave_master.user_id = '$user_id' AND leave_master.leave_status = '1'AND ('$monthDate' BETWEEN leave_master.leave_start_date AND leave_master.leave_end_date)");

                    if (mysqli_num_rows($leaveDataQry) > 0) {

                        $leaveDataTrue = mysqli_fetch_array($leaveDataQry);

                        if ($leaveDataTrue['leave_type_id'] != null && $leaveDataTrue['leave_type_id'] != 0){
                            $dates["leave_type_name"] = $leaveDataTrue['leave_type_name'];                    
                        }else{
                            $dates["leave_type_name"] = "";
                        }

                        if ($leaveDataTrue['paid_unpaid'] == 0) {
                            if ($leaveDataTrue['leave_day_type'] == 0) {
                                $dates["leave_day_view"] = "Paid Leave - Full Day";
                            }else{
                                $dates["leave_day_view"] = "Paid Leave - Half Day";
                            }
                        }else if ($leaveDataTrue['paid_unpaid'] == 1) {
                            if ($leaveDataTrue['leave_day_type'] == 0) {
                                $dates["leave_day_view"] = "Unpaid Leave - Full Day";
                            }else{
                                $dates["leave_day_view"] = "Unpaid Leave - Half Day";
                            }
                        }else{
                            $dates["leave_day_view"] = "";
                        }

                        $dates["leave_reason"] = $leaveDataTrue['leave_reason'];
                        $dates["auto_leave_reason"] = $leaveDataTrue['auto_leave_reason'];

                        if ($leaveDataTrue['leave_type_id'] > 0) {
                            $dates["auto_leave_reason"] = "";
                        }  
                        
                    }
                }

                $dates["holiday_name"] = "";
                $dates["holiday_description"] = "";

                $holidayNameQry = $d->selectRow("holiday_name,holiday_description","holiday_master","society_id = '$society_id' AND holiday_start_date = '$monthDate' AND holiday_status = '0'");

                $dates["holiday_name"] = "";
                $dates["holiday_description"] = "";

                if (mysqli_num_rows($holidayNameQry) > 0) {

                    $holidayNameData = mysqli_fetch_array($holidayNameQry);

                    $dates["holiday_name"] = $holidayNameData['holiday_name'];
                    $dates["holiday_description"] = $holidayNameData['holiday_description'];
                }

                $tdt = date('Y-m-d');

                $attendanceQry = $d->selectRow("
                    am.*,
                    hm.holiday_name,
                    hm.holiday_description,
                    apomr.attendance_id as attid, 
                    apomr.attendance_punch_out_missing_id, 
                    apomr.punch_out_missing_reject_reason,
                    apomr.attendance_punch_out_missing_status
                    ","
                    attendance_master AS am 
                    LEFT JOIN attendance_punch_out_missing_request AS apomr ON apomr.attendance_id = am.attendance_id         
                    LEFT JOIN holiday_master AS hm ON hm.holiday_start_date = am.attendance_date_start         
                    ","
                    am.society_id='$society_id' 
                    AND am.user_id = '$user_id'
                    AND am.attendance_date_start = '$monthDate'");

                if(mysqli_num_rows($attendanceQry)>0){
                
                    $times = array();

                    $data = mysqli_fetch_array($attendanceQry);

                    $dates['present'] = true;

                    if ($data['attendance_status'] == "0") {
                        $dates["attendnace_pending"] = true;
                        $dates["attendance_pending_message"] = "Attendance Pending";
                    }else{
                        $dates["attendnace_pending"] = false;
                        $dates["attendance_pending_message"] = "";
                    }

                    if ($data['attendance_status'] == "0" && $data['punch_in_request'] == "1") {
                        $dates["punch_in_request_sent"] = true;
                    }else{
                        $dates["punch_in_request_sent"] = false;
                    }

                    if ($data['attendance_status'] == "2") {
                        $dates["attendance_declined"] = true;
                        $dates["attendance_declined_message"] = $data['attendance_declined_reason'];
                        if ($data['punch_in_request'] == "1"){
                            $dates["punch_in_request_sent"] = true;
                        }
                    }else{
                        $dates["attendance_declined"] = false;
                        $dates["attendance_declined_message"] = "";
                    }

                    $attendanceDateStart = $data['attendance_date_start'];
                    $attendanceDateEnd = $data['attendance_date_end'];

                    array_push($presentDaysAry,$attendanceDateStart);

                    if ($data['attendance_punch_out_missing_id'] != null) {
                       
                        $dates['attendance_punch_out_missing_id'] = $data['attendance_punch_out_missing_id'];
                        $dates['punch_out_missing_reject_reason'] = $data['punch_out_missing_reject_reason'];

                        $attendance_punch_out_missing_status = $data['attendance_punch_out_missing_status'];

                        $dates['attendance_punch_out_missing_status'] = $attendance_punch_out_missing_status;
                        
                        if ($attendance_punch_out_missing_status == "0") {
                            $dates['attendance_punch_out_missing_status_view'] = "Pending";
                        }else if ($attendance_punch_out_missing_status == "1") {
                            $dates['attendance_punch_out_missing_status_view'] = "Approved";
                        }else if ($attendance_punch_out_missing_status == "2") {
                            $dates['attendance_punch_out_missing_status_view'] = "Rejected";
                        }
                    }

                    if ($data['workReport'] !=null || $data['workReport'] != "") {
                        $dates["work_report"] = true;
                    }

                    $incompleteCount = false;

                    if (in_array($attendanceDateStart,$weekAry) && $data['attendance_status'] == 1) {
                        $dates['extra_day'] = true;
                        if (in_array($attendanceDateStart,$halfHeaveArray)) {
                            if ($data['punch_out_time'] != "00:00:00") {
                                $totalExtraDays = $totalExtraDays + 0.5;
                            }
                            $key = array_search($attendanceDateStart, $halfHeaveArray);
                            if (false !== $key) {
                                unset($halfHeaveArray[$key]);
                            }
                        }else if (in_array($attendanceDateStart,$leaveArray)) {
                            $totalIncompleteDays += 1;
                            $incompleteCount = true;

                            $key = array_search($attendanceDateStart, $leaveArray);
                            if (false !== $key) {
                                unset($leaveArray[$key]);
                            }
                        }else{
                            $totalExtraDays = $totalExtraDays + 1;
                        }
                    }

                    if (in_array($attendanceDateStart,$holidayAry) && $data['attendance_status'] == 1) {
                        $dates['extra_day'] = true;
                        
                        if (in_array($attendanceDateStart,$halfHeaveArray)) {
                            if ($data['punch_out_time'] != "00:00:00") {
                                $totalExtraDays = $totalExtraDays + 0.5;
                            }
                            $key = array_search($attendanceDateStart, $halfHeaveArray);
                            if (false !== $key) {
                                unset($halfHeaveArray[$key]);
                            }
                        }else if (in_array($attendanceDateStart,$leaveArray)) {
                            $key = array_search($attendanceDateStart, $leaveArray);
                            if (false !== $key) {
                                unset($leaveArray[$key]);
                            }

                            $totalIncompleteDays += 1;
                            $incompleteCount = true;
                        }else{
                            $totalExtraDays = $totalExtraDays + 1;
                        }
                    }

                    if (in_array($attendanceDateStart,$weekAry) && $data['attendance_status'] == 2) {
                        $dates['extra_day'] = true;
                        if (in_array($attendanceDateStart,$halfHeaveArray)) {
                            
                            $key = array_search($attendanceDateStart, $halfHeaveArray);
                            if (false !== $key) {
                                unset($halfHeaveArray[$key]);
                            }
                        }else if (in_array($attendanceDateStart,$leaveArray)) {                            
                            $totalIncompleteDays += 1;
                            $incompleteCount = true;
                            $key = array_search($attendanceDateStart, $leaveArray);
                            if (false !== $key) {
                                unset($leaveArray[$key]);
                            }
                        }
                    }

                    if (in_array($attendanceDateStart,$holidayAry) && $data['attendance_status'] == 2) {
                        $dates['extra_day'] = true;

                        if (in_array($attendanceDateStart,$halfHeaveArray)) {
                            
                            $key = array_search($attendanceDateStart, $halfHeaveArray);
                            if (false !== $key) {
                                unset($halfHeaveArray[$key]);
                            }
                        }else if (in_array($attendanceDateStart,$leaveArray)) {
                            $totalIncompleteDays += 1;
                            $incompleteCount = true;
                            $key = array_search($attendanceDateStart, $leaveArray);
                            if (false !== $key) {
                                unset($leaveArray[$key]);
                            }
                        }
                    }

                    if (in_array($attendanceDateStart,$leaveArray) && $data['attendance_status'] == 1 && $incompleteCount == false) {
                        $totalIncompleteDays += 1;
                    }

                    $punchInTime = $data['punch_in_time'];
                    $punchOutTime = $data['punch_out_time'];

                    $dates['attendance_id'] = $data['attendance_id'];
                    $dates['unit_id'] = $data['unit_id'];
                    $dates['punch_in_time'] = date("h:i A",strtotime($punchInTime));
                    $dates['punch_out_time'] = date("h:i A",strtotime($punchOutTime));

                    $multiple_punch_in_out_data = $data['multiple_punch_in_out_data'];
                    $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

                    $dates['punch_in_data'] = array();

                    if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                        for ($i=0; $i < count($multiplePunchDataArray); $i++) { 
                            $punchInData = array();

                            $punchInData['punch_in_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_in_date"]));
                            $punchInData['punch_in_time'] = date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_in_time"]));

                            if ($multiplePunchDataArray[$i]["punch_out_date"] != '' && $multiplePunchDataArray[$i]["punch_out_date"] != "00:00:00") {
                                $punchInData['punch_out_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_out_date"]));
                            }else{
                                if ($attendanceDateStart != date('Y-m-d')) {
                                    $punchInData['punch_out_date'] = "Missing";
                                }else{                                    
                                    $punchInData['punch_out_date'] = "";
                                }
                            }

                            if ($multiplePunchDataArray[$i]["punch_out_time"] != '' && $multiplePunchDataArray[$i]["punch_out_time"] != "00:00:00") {
                                $punchInData['punch_out_time'] = date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_out_time"]));
                            }else{
                                $punchInData['punch_out_time'] = "";
                            }

                            if ($multiplePunchDataArray[$i]["working_hour"] != '' && $multiplePunchDataArray[$i]["working_hour"] != "00:00") {
                                $punchInData['working_hour'] = $multiplePunchDataArray[$i]["working_hour"];
                            }else{
                                $punchInData['working_hour'] = "";
                            }

                            if ($multiplePunchDataArray[$i]["location_name_in"] != '') {
                                $punchInData['location_name_in'] = $multiplePunchDataArray[$i]["location_name_in"];
                            }else{
                                $punchInData['location_name_in'] = "";
                            }

                            if ($multiplePunchDataArray[$i]["location_name_out"] != '') {
                                $punchInData['location_name_out'] = $multiplePunchDataArray[$i]["location_name_out"];
                            }else{
                                $punchInData['location_name_out'] = "";
                            }

                            $punchInData['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"].'';

                            array_push($dates['punch_in_data'],$punchInData);
                        }
                    }

                    if ($attendanceDateStart != $attendanceDateEnd) {
                        $dates['punch_out_time'] = date("h:i A",strtotime($punchOutTime)).' ('.date('dS M',strtotime($attendanceDateEnd)).')';
                    }

                    if ($data['late_in'] == 1) {
                        $dates['late_in'] = true;
                    }
                     
                    if ($data['early_out'] == 1) {
                        $dates['early_out'] = true;
                    }
                    
                    if ($attendanceDateEnd == "0000-00-00" || $punchOutTime == "00:00:00") {
                        $dates['punch_out_time']= "";
                        $attendanceDateEnd = date('Y-m-d');
                        $punchOutTime = date('H:i:s');
                    }

                    if ($data['punch_out_time'] != "00:00:00" && !in_array($attendanceDateStart,$weekAry)
                        && !in_array($attendanceDateStart,$holidayAry) && $data['attendance_status'] == "1") {

                        $totalHours = $data['total_working_hours'];
                        $productive_working_hours = $data['productive_working_hours'];
                        $productive_working_hours_minutes = $data['productive_working_hours_minutes'];

                        $dates['total_hours_spend'] = timeFormat($totalHours);
                        $dates['productive_working_hours'] = timeFormat($productive_working_hours);
                        $dates['productive_working_hours_minutes'] = $productive_working_hours_minutes;
                        $dates['total_spend_time'] = "";
                        $dates["punch_out_missing_message"] = "";
                        $dates['is_punch_out_missing'] = false;

                    }else if ($data['punch_out_time'] != "00:00:00" && (in_array($attendanceDateStart,$weekAry)
                        || in_array($attendanceDateStart,$holidayAry)) && $data['attendance_status'] == "1") {
                        $totalHoursExtra = $data['total_working_hours'];
                        $productive_working_hours = $data['productive_working_hours'];
                        $productive_working_hours_minutes = $data['productive_working_hours_minutes'];

                        $dates['total_hours_spend'] = timeFormat($totalHoursExtra);
                        $dates['productive_working_hours'] = timeFormat($productive_working_hours);
                        $dates['productive_working_hours_minutes'] = $productive_working_hours_minutes;
                        $dates['total_spend_time'] = "";
                        $dates["punch_out_missing_message"] = "";
                        $dates['is_punch_out_missing'] = false;
                    }else{

                        $totalHours = $data['total_working_hours'];
                        $productive_working_hours = $data['productive_working_hours'];
                        $productive_working_hours_minutes = $data['productive_working_hours_minutes'];

                        if (($totalHours != '' && $totalHours != "00:00:00") || 
                            ($totalHours != '' && $totalHours != "00:00")) {
                            $dates['total_hours_spend'] = timeFormat($totalHours);;
                        }else{
                            
                            $dates['total_spend_time'] = "";
                            $dates['total_hours_spend'] = "";
                        }

                        if (($productive_working_hours != '' && $productive_working_hours != "00:00:00") || 
                            ($productive_working_hours != '' && $productive_working_hours != "00:00")) {
                            $dates['productive_working_hours'] = timeFormat($productive_working_hours);
                            $dates['productive_working_hours_minutes'] = $productive_working_hours_minutes;
                        }else{
                            
                            $dates['productive_working_hours'] = "";
                            $dates['productive_working_hours_minutes'] = "";
                        }

                        if ($attendanceDateStart == $tdt) {
                            $dates["punch_out_missing_message"] = "";
                            $dates['is_punch_out_missing'] = false;
                        }else if(($data['attendance_status'] == '1' || $data['attendance_status'] == '0') &&
                            $data['punch_out_time'] == "00:00:00"){                    
                            $dates["punch_out_missing_message"] = "Punch Out Missing";
                            $dates['is_punch_out_missing'] = true;

                            if ($shiftType == "Night") {

                                $previousDateView = date('dS F Y',strtotime("+1 days",strtotime($attendanceDateStart)));
                                $previousDate = date('Y-m-d',strtotime("+1 days",strtotime($attendanceDateStart)));
                                $datesArray = array(
                                    "date_one"=>date('Y-m-d',strtotime($attendanceDateStart)),
                                    "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                    "date_two"=>$previousDate,
                                    "date_two_view"=>$previousDateView
                                );
                            }else{
                                $datesArray = array(
                                    "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                    "date_one"=>date('Y-m-d',strtotime($attendanceDateStart))
                                );
                            }

                            $dates['punch_missing_date_ary'] = $datesArray;
                        }else if($data['attendance_status'] == '1' || $data['attendance_status'] == '0'){
                            $dates["punch_out_missing_message"] = "";
                            $dates['is_punch_out_missing'] = false;
                        }
                    }

                    if ($punch_in_date == $attendanceDateStart) {
                        $dates['is_punch_out_missing'] = false;
                    }

                    $attendanceHistoryQry = $d->select("attendance_master,attendance_break_history_master,
                        attendance_type_master","attendance_master.attendance_id = attendance_break_history_master.attendance_id
                        AND attendance_master.attendance_id = '$dates[attendance_id]'
                        AND attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id 
                        AND attendance_break_history_master.break_out_time != '00:00:00' 
                        AND attendance_break_history_master.break_end_date != '0000-00-00' ");

                    $dates["attendance_history"] = array();
                    
                    if(mysqli_num_rows($attendanceHistoryQry)>0){

                        $totalBreakTimeAtt = array();

                        while($historyData = mysqli_fetch_array($attendanceHistoryQry)){
                            $historyArray = array();

                            $breakStartDate = $historyData['break_start_date'];
                            $breakEndDate = $historyData['break_end_date'];
                            
                            $breakInTime = $historyData['break_in_time'];
                            $breakOutTime = $historyData['break_out_time'];

                            $historyArray['attendance_break_history_id'] = $historyData['attendance_break_history_id'];
                            $historyArray['attendance_type_name'] = $historyData['attendance_type_name'];
                            $historyArray['break_start_date'] = date('dS F Y', strtotime($breakStartDate));
                            $historyArray['break_end_date'] = date('dS F Y', strtotime($breakEndDate));
                            $historyArray['break_in_time'] = date('h:i A', strtotime($breakInTime));
                            $historyArray['break_out_time'] = date('h:i A', strtotime($breakOutTime));

                            $totalBreak =$historyData['total_break_time'];

                            $dates['total_spend_time_break'] = "";

                            $historyArray['total_break_hours_spend'] =timeFormat($totalBreak);

                            array_push($dates['attendance_history'],$historyArray);
                        }
                    }
                }else{
                    if ($isDateGone == "true") {


                        if (isset($holidayAry) && count($holidayAry) > 0 && isset($weekAry) && count($weekAry) > 0 && !in_array($monthDate,$holidayAry) && !in_array($monthDate,$weekAry)) {
                            $totalAbsentDays = ($totalAbsentDays + 1);
                        }else if (isset($holidayAry) && count($holidayAry) > 0 && isset($weekAry) && count($weekAry) <= 0 && !in_array($monthDate,$holidayAry)) {
                            $totalAbsentDays = ($totalAbsentDays + 1);
                        }else if (isset($holidayAry) && count($holidayAry) <= 0 && isset($weekAry) && count($weekAry) > 0 && !in_array($monthDate,$weekAry)) {
                            $totalAbsentDays = ($totalAbsentDays + 1);
                        }

                        if ($shiftType == "Night") {

                            $previousDateView = date('dS F Y',strtotime("+1 days",strtotime($monthDate)));
                            $previousDate = date('Y-m-d',strtotime("+1 days",strtotime($monthDate)));
                            $datesArray = array(
                                "date_one"=>date('Y-m-d',strtotime($monthDate)),
                                "date_one_view"=>date('dS F Y',strtotime($monthDate)),
                                "date_two"=>$previousDate,
                                "date_two_view"=>$previousDateView
                            );
                        }else{
                            $datesArray = array(
                                "date_one_view"=>date('dS F Y',strtotime($monthDate)),
                                "date_one"=>date('Y-m-d',strtotime($monthDate))
                            );
                        }

                        $dates['punch_missing_date_ary'] = $datesArray;
                    }
                }

                if ($shiftType == "Night") {

                    $previousDateView = date('dS F Y',strtotime("+1 days",strtotime($monthDate)));
                    $previousDate = date('Y-m-d',strtotime("+1 days",strtotime($monthDate)));
                    $datesArray = array(
                        "date_one"=>date('Y-m-d',strtotime($monthDate)),
                        "date_one_view"=>date('dS F Y',strtotime($monthDate)),
                        "date_two"=>$previousDate,
                        "date_two_view"=>$previousDateView
                    );
                }else{
                    $datesArray = array(
                        "date_one_view"=>date('dS F Y',strtotime($monthDate)),
                        "date_one"=>date('Y-m-d',strtotime($monthDate))
                    );
                }

                $dates['punch_missing_date_ary'] = $datesArray;

                array_push($response['monthly_history'],$dates);
            }

            // Total Month Hour

            if (count($leaveArray) > 0) {
                $leaveHR = (count($leaveArray) * $perdayHours);
                $totalMonthHours = $totalMonthHours - $leaveHR;
                $response['total_month_hours'] = $totalMonthHours."";
            }

            if (count($halfHeaveArray) > 0) {
                $totalWH = (count($halfHeaveArray) * ($perdayHours/2));
                $totalMonthHours = $totalMonthHours - $totalWH;
                $response['total_month_hours'] = $totalMonthHours."";
            }

            list($hours1, $wrongMinutes) = explode('.', $totalMonthHours);
            $minutes1 = ($wrongMinutes < 10 ? $wrongMinutes * 10 : $wrongMinutes) * 0.6;

            $response['total_month_hours_view'] = timeFormat($hours1.':'.$minutes1);
            $response['total_month_hours'] = $totalMonthHours."";

            $totalMonthMinutes = minutes($hours1.':'.$minutes1);
            $response['total_month_minutes'] = $totalMonthMinutes."";

            $holidayAryJoin = join("','",$holidayAry); 
            $weekAryJoin = join("','",$weekAry);

            // Total Holidays

            if (isset($holidayAry) && count($holidayAry) > 0 && isset($weekAry) && count($weekAry) > 0) {

               
                $totalPresentDays = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '1' AND user_id = '$user_id' AND (attendance_date_start NOT IN ('$holidayAryJoin') AND attendance_date_start NOT IN ('$weekAryJoin'))  AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_date_end != '0000-00-00' AND attendance_status = '1' AND is_leave != '1'");


                 // Total Half Day Leaves            

                $totalHalfDays = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date' AND leave_status = '1' AND leave_day_type = '1' AND leave_start_date NOT IN ('$holidayAryJoin') AND leave_start_date NOT IN ('$weekAryJoin')");

                // Total Full Day Leaves

                $totalLeaves = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '0'  AND leave_start_date NOT IN ('$holidayAryJoin') AND leave_start_date NOT IN ('$weekAryJoin') AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

                // Total Working Hours

                $qryTWH = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id'  AND attendance_date_start NOT IN ('$holidayAryJoin') AND attendance_date_start NOT IN ('$weekAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

                $qryTWHData = mysqli_fetch_array($qryTWH);

                $qryTEWH2 = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id' AND (attendance_date_start IN ('$holidayAryJoin')) AND (attendance_date_start IN ('$weekAryJoin')) AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

                $qryTEWHData2 = mysqli_fetch_array($qryTEWH2);
            }else if (isset($holidayAry) && count($holidayAry) > 0 && isset($weekAry) && count($weekAry) <= 0) {
                // Total Present Days

                $totalPresentDays = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '1' AND user_id = '$user_id' AND attendance_date_start NOT IN ('$holidayAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_status = '1'  AND is_leave != '1'");
                

                // Total Half Day Leaves            

                $totalHalfDays = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '1' AND leave_start_date NOT IN ('$holidayAryJoin') AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

                // Total Full Day Leaves

                $totalLeaves = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '0'  AND leave_start_date NOT IN ('$holidayAryJoin') AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

                // Total Working Hours

                $qryTWH = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id'  AND attendance_date_start NOT IN ('$holidayAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

                $qryTWHData = mysqli_fetch_array($qryTWH);

                $qryTEWH2 = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start IN ('$holidayAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

                $qryTEWHData2 = mysqli_fetch_array($qryTEWH2);

            }else if (isset($holidayAry) && count($holidayAry) <= 0 && isset($weekAry) && count($weekAry) > 0) {
                // Total Present Days
                
                $totalPresentDays = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '1' AND user_id = '$user_id' AND attendance_date_start NOT IN ('$weekAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_date_end != '0000-00-00'  AND attendance_status = '1'  AND is_leave != '1'");
                

                
                 // Total Half Day Leaves            

                $totalHalfDays = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date' AND leave_status = '1' AND leave_day_type = '1' AND leave_start_date NOT IN ('$weekAryJoin')");

                // Total Full Day Leaves

                $totalLeaves = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '0' AND leave_start_date NOT IN ('$weekAryJoin') AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

                // Total Working Hours

                $qryTWH = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start NOT IN ('$weekAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

                $qryTWHData = mysqli_fetch_array($qryTWH);

                $qryTEWH2 = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start IN ('$weekAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

                $qryTEWHData2 = mysqli_fetch_array($qryTEWH2);


            }else{
                // Total Present Days
                
                $totalPresentDays = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '1' AND user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_date_end != '0000-00-00'  AND attendance_status = '1' AND is_leave != '1'");


                // Total Half Day Leaves            

                $totalHalfDays = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date' AND leave_status = '1' AND leave_day_type = '1'");

                // Total Full Day Leaves

                $totalLeaves = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '0' AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

                // Total Working Hours

                $qryTWH = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

                $qryTWHData = mysqli_fetch_array($qryTWH);

                $qryTEWH2 = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

                $qryTEWHData2 = mysqli_fetch_array($qryTEWH2);
            }


            $totalHolidays = $d->count_data_direct("holiday_id","holiday_master","holiday_status = '0' AND holiday_start_date BETWEEN '$month_start_date' AND '$month_end_date'");


            // Total Pending Attendance

            $totalPendingAttendance = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '0' AND user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

            // Total Rejected Attendance

            $totalRejectedAttendance = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '2' AND user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

            // Total Punch Out Missing

            $totalPunchOUTMissing = $d->count_data_direct("attendance_id","attendance_master","user_id = '$user_id' AND DATE_FORMAT(attendance_date_start,'%Y-%m') = DATE_FORMAT('$month_start_date','%Y-%m') AND attendance_status = '1' AND attendance_date_end = '0000-00-00' AND attendance_date_start != '$tdt'");

           
            $totalHD = 0;

            if ($totalHalfDays > 0) {
                $totalHD = 0.5 * $totalHalfDays;
            }

            // Total Late IN

            $totalLatePunchIN = $d->count_data_direct("late_in","attendance_master","late_in = '1' AND user_id = '$user_id' AND attendance_date_start  BETWEEN '$month_start_date' AND '$month_end_date'");
            
            $response['late_punch_in'] = $totalLatePunchIN."";

            // Total Early Out


            $totalEarlyPunchOUT = $d->count_data_direct("early_out","attendance_master","early_out = '1' AND user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

            $response['early_punch_out'] = $totalEarlyPunchOUT."";


            if ($totalExtraDays > 0 && $totalPresentDays > 0) {
                $finalPresentDays =  ($totalPresentDays - $totalHD)."+$totalExtraDays";
                $response['total_present'] = $finalPresentDays; 
            }else{
                if ($totalPresentDays > 0) {
                    $response['total_present'] = $finalPresentDays = $totalPresentDays - $totalHD.""; 
                }else{
                    $response['total_present'] = $finalPresentDays = $totalPresentDays."";                 
                }
            }

            
            $response['total_month_minutes'] = $qryTWHData['SUM(total_working_minutes)'].'';

            $time = str_replace(":",".",hoursandmins($qryTWHData['SUM(total_working_minutes)']));

            if ($time=="") {
            $response['total_month_hour_spent'] = '0';
            } else {
             $response['total_month_hour_spent'] = $time.'';
            }
             $response['total_month_hour_spent_view'] = timeFormat(hoursandmins($qryTWHData['SUM(total_working_minutes)'])).'';

            // Extra Working Hour

            $qryTEWH = $d->sum_data("extra_working_hours_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");

            
            $extraDayMin = $qryTEWHData2['SUM(total_working_minutes)'];

            $qryTEWHData = mysqli_fetch_array($qryTEWH);
            $extraMin = $qryTEWHData['SUM(extra_working_hours_minutes)'];

            $finalExtraMin = $extraMin;
            $finalExtraMin = $extraDayMin + $extraMin;

            $response['total_extra_minutes'] = $finalExtraMin.'';            
            $response['total_extra_hours_view'] = timeFormat(hoursandmins($finalExtraMin)).'';            

            if ($finalExtraMin > 0) {
                $response['has_extra_hours'] = true;
            }else{
                $response['has_extra_hours'] = false;
            }

            // Remaining Working Hours

            if ($totalMonthMinutes > $qryTWHData['SUM(total_working_minutes)']) {
                $totalRemainingMin = $totalMonthMinutes - $qryTWHData['SUM(total_working_minutes)'].'';
                $response['total_remaining_minutes'] = $totalRemainingMin;
                $response['total_remaining_hours_view'] = timeFormat(hoursandmins($totalRemainingMin));
            }else{
                $totalRemainingMin = '0';
                $response['total_remaining_minutes'] = $totalRemainingMin;
                $response['total_remaining_hours_view'] = "";
            }


            // Other Counts

            $response['total_leave'] = $totalLeaves."";
            $response['total_half_day'] = $totalHalfDays."";
            $response['total_punch_out_missing'] = $totalPunchOUTMissing."";
            $response['total_week_off'] = $totalWeekOff."";
            $response['total_holidays'] = $totalHolidays."";
            $response['total_pending_attendance'] = $totalPendingAttendance."";
            $response['total_rejected_attendance'] = $totalRejectedAttendance."";
            $response['total_incomplete_days'] = $totalIncompleteDays."";

            $totalCData = ($totalWeekOff + $totalHolidays);
            $totalWorkingDays = $totalAbsent - ($totalWeekOff + $totalHolidays);

            $response['total_working_days'] = $totalWorkingDays."";

            $totalAvgPecentage =round((($finalPresentDays+count($paidLeaveArray)+(count($paidHalfHeaveArray)/2)) * 100)/$totalWorkingDays);

            if ($totalAvgPecentage > 100) {
                $totalAvgPecentage = 100;
            }

            $response['total_avg_percentage'] = $totalAvgPecentage.'';

            $tempExtraDays = 0;

            if ($totalAbsentDays <= 0) {
                $totalAbsentDays = 0;   
            }

            $response['total_extra_days'] = $totalExtraDays."";
            $response['total_extra_days_view'] = $tempExtraDays."";
            $response['total_absent'] = $totalAbsentDays."";
            $response['info_message'] = "Pending attendance, rejected attendance and the attendance which has missing punch out will not be calculated in present days";
            
            echo json_encode($response);
        
        }else if($_POST['getWeeklyAttendanceHistory']=="getWeeklyAttendanceHistory" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $qq12 = $d->selectRow("hide_working_hours","society_master","society_id = '$society_id'");

            $socData = mysqli_fetch_array($qq12);

            if ($socData['hide_working_hours'] == 1) {
                $response['hide_working_hours'] = true;
            }else{
                $response['hide_working_hours'] = false;                
            }

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shift_time_id = $sdata['shift_time_id'];
            }

           

            if ($weekStartDate !=null || $weekStartDate != '') {
                $currdt= strtotime($weekStartDate);
                $nextmonth=strtotime($weekStartDate."+1 month");
                $i=0; 
                $flag=true;
            }else{
                $textdt=date("01-m-Y");
                $currdt= strtotime($textdt);
                $nextmonth=strtotime($textdt."+1 month");
                $i=0;
                $flag=true;
            }

            $response["weekly_histrory"] = array();
            $response["week_position"] = "0";

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shiftType = $shiftData['shift_type'];

            $parts = explode(':', $perdayHours);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);

            $weekDates = weeks_in_month(date('m'),date('Y'));

            // Week
            for ($x = 0; $x <= (count($startarr)-1); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";
                $weekArray["total_spend_time"] = "0";
                $weekArray["total_spend_time_name"] = "";

                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);

                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 
                
                $holidayAry = array();

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $days = array();
                    $days['day'] = $date->format('l');
                    $days['date'] = $dayDate = $date->format('Y-m-d');
                    $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate));

                    if ($dayDate == date('Y-m-d')) {
                        $response["week_position"] = $x."";
                    }
                    
                    $holidayQry = $d->selectRow("holiday_start_date","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' ");

                    $x1 = $x+1;

                    //if ($working_day_calculation == 0) {
                        if (mysqli_num_rows($holidayQry)>0) {
                            array_push($holidayAry,$dayDate);
                            $minusHoliday += $perdayHours;
                        } else {
                            // full day off
                            if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                                array_push($weekOffAry,$dayDate);
                                $minusWeekOffHR += $perdayHours;
                            }
                            // alternate week day off
                            if ($shiftData['has_altenate_week_off'] == "1") {  
                                if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                    in_array($x1,$alternateWeekOff)) {
                                    array_push($weekOffAry,$dayDate);
                                    $minusAlternateWeekOffDays += $perdayHours;
                                }
                            }
                        }
                    
                    // holiday off
                    
                    array_push($daysArray['days'],$days);
                }

                $totalWeekDay = count($daysArray['days']);
                
                $totalWeekHour = $perdayHours * $totalWeekDay;
                $totalWeekHour1 = ($totalWeekHour - $minusWeekOffHR) - $minusAlternateWeekOffDays;
                $totalWeekHour = $totalWeekHour1 - $minusHoliday;            

                $weekArray['total_hours'] = $totalWeekHour."";

                list($hours1, $wrongMinutes) = explode('.', $totalWeekHour);
                $minutes1 = ($wrongMinutes < 10 ? $wrongMinutes * 10 : $wrongMinutes) * 0.6;

                if ($hours1 > 0 && $minutes1 > 0) {
                    $weekArray['total_hours_view'] = $hours1 . ' hr ' . $minutes1.' min';
                }else if($hours1 > 0 && $minutes1 <= 0){
                    $weekArray['total_hours_view'] = $hours1.' hr';
                } else if($hours1 <= 0 && $minutes1 > 0){
                    $weekArray['total_hours_view'] = $minutes1.' min';
                } else{
                    $weekArray['total_hours_view'] = '';
                }
                
                $weekArray['week'] = date('d',strtotime($sDate))."-".date('d',strtotime($eDate))." ".date('F',strtotime($sDate))." ".date('Y',strtotime($sDate));

                // Week History

                $tdate = date('Y-m-d');
                
                $attendanceQry = $d->selectRow("am.*,apomr.attendance_id as attid, apomr.attendance_punch_out_missing_id, apomr.punch_out_missing_reject_reason,apomr.attendance_punch_out_missing_status","attendance_master AS am LEFT JOIN attendance_punch_out_missing_request AS apomr ON apomr.attendance_id = am.attendance_id","(am.society_id='$society_id' 
                    AND am.attendance_date_start != '$tdate' 
                    AND am.user_id = '$user_id' 
                    AND am.attendance_date_start BETWEEN '$sDate' AND '$eDate') 
                    OR (am.society_id='$society_id' 
                    AND am.user_id = '$user_id' 
                    AND am.attendance_date_start BETWEEN '$sDate' AND '$eDate')","ORDER BY am.attendance_date_start ASC");

                $weekArray["history"] = array();

                if(mysqli_num_rows($attendanceQry)>0){

                    $times = array();

                    while($data=mysqli_fetch_array($attendanceQry)) {

                        $attendanceHistory = array(); 

                        $attendanceHistory["attendance_id"] = $data['attendance_id'];
                        $attendanceHistory["day_name"] = date('l', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_start"] = date('dS F Y', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_end"] = date('dS F Y', strtotime($data['attendance_date_end']));

                        if ($data['attendance_punch_out_missing_id'] != null) {
                       
                            $attendanceHistory['attendance_punch_out_missing_id'] = $data['attendance_punch_out_missing_id'];
                            $attendanceHistory['punch_out_missing_reject_reason'] = $data['punch_out_missing_reject_reason'];

                            $attendance_punch_out_missing_status = $data['attendance_punch_out_missing_status'];

                            $attendanceHistory['attendance_punch_out_missing_status'] = $attendance_punch_out_missing_status;
                            
                            if ($attendance_punch_out_missing_status == "0") {
                                $attendanceHistory['attendance_punch_out_missing_status_view'] = "Pending";
                            }else if ($attendance_punch_out_missing_status == "1") {
                                $attendanceHistory['attendance_punch_out_missing_status_view'] = "Approved";
                            }else if ($attendance_punch_out_missing_status == "2") {
                                $attendanceHistory['attendance_punch_out_missing_status_view'] = "Rejected";
                            }
                        }

                        $attendanceDateStart = $data['attendance_date_start'];
                        $attendanceDateEnd = $data['attendance_date_end'];
                        
                        $punchInTime = $data['punch_in_time'];
                        $punchOutTime = $data['punch_out_time'];

                        if (in_array($attendanceDateStart,$weekOffAry)) {
                            $attendanceHistory['extra_day'] = true;
                        }

                        if (in_array($attendanceDateStart,$holidayAry)) {
                            $attendanceHistory['extra_day'] = true;
                        }

                        if ($data['attendance_status'] == 2) {
                            $attendanceHistory['attendance_rejected'] = true;
                        }else{
                            $attendanceHistory['attendance_rejected'] = false;
                        }
                        
                        $attendanceHistory["punch_in_time"] = date("h:i A",strtotime($punchInTime));
                        $attendanceHistory["punch_out_time"] = date("h:i A",strtotime($punchOutTime));

                        if ($attendanceDateEnd == "0000-00-00") {
                            $attendanceHistory["attendance_date_end"] = "";
                            $attendanceHistory["punch_out_time"] = "";
                            $attendanceDateEnd = date('Y-m-d');
                            $punchOutTime = date('H:i:s');
                        }

                        if ($data['punch_out_time'] != "00:00:00"/* && (!in_array($attendanceDateStart,$weekOffAry) && !in_array($attendanceDateStart,$holidayAry))*/) {

                        
                            $totalHours = getTotalHours($attendanceDateStart,$attendanceDateEnd,$punchInTime,$punchOutTime);
                            
                            $totalHoursName = getTotalHoursWithNames($attendanceDateStart,$attendanceDateEnd,$punchInTime,$punchOutTime);

                            if ($data['attendance_status'] == 1) {
                                $times[] = $totalHours;
                            }

                            $attendanceHistory["total_hours"] = $totalHoursName;
                            $attendanceHistory["is_punch_out_missing"] = false;
                            $attendanceHistory["punch_out_missing_message"] = "";

                            $weekArray['total_spend_time'] = getTotalWeekHours($times);
                            $weekArray['total_spend_time_name'] = getTotalWeekHoursName($times);
                        }else{
                            if ($data['attendance_date_start'] == $tdate) {
                                $attendanceHistory["is_punch_out_missing"] = false;
                                $attendanceHistory["total_hours"] = "";
                                $attendanceHistory["punch_out_missing_message"] = "";
                            }else{

                                if ($punchOutTime == "00:00:00") {
                                    $attendanceHistory["is_punch_out_missing"] = true;
                                }else{
                                    $attendanceHistory["is_punch_out_missing"] = false;
                                }
                                $attendanceHistory["total_hours"] = "";
                                $attendanceHistory["punch_out_missing_message"] = "Punch Out Missing";

                                if ($shiftType == "Night") {

                                    $previousDateView = date('dS F Y',strtotime("+1 days",strtotime($attendanceDateStart)));
                                    $previousDate = date('Y-m-d',strtotime("+1 days",strtotime($attendanceDateStart)));
                                    $datesArray = array(
                                        "date_one"=>date('Y-m-d',strtotime($attendanceDateStart)),
                                        "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                        "date_two"=>$previousDate,
                                        "date_two_view"=>$previousDateView
                                    );
                                }else{
                                    $datesArray = array(
                                        "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                        "date_one"=>date('Y-m-d',strtotime($attendanceDateStart))
                                    );
                                }

                                $attendanceHistory['punch_missing_date_ary'] = $datesArray;
                            }
                        }

                        if ($data['attendance_status'] == "0") {
                            $attendanceHistory["attendnace_pending"] = true;
                            $attendanceHistory["attendance_pending_message"] = "Attendance Pending";
                        }else{
                            $attendanceHistory["attendnace_pending"] = false;
                            $attendanceHistory["attendance_pending_message"] = "";
                        }

                        array_push($weekArray["history"], $attendanceHistory);
                    }
                }

                array_push($response['weekly_histrory'],$weekArray);
            } 
            
            echo json_encode($response);
        
        }else if($_POST['getWeeklyAttendanceHistoryNew']=="getWeeklyAttendanceHistoryNew" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $qq12 = $d->selectRow("hide_working_hours","society_master","society_id = '$society_id'");

            $socData = mysqli_fetch_array($qq12);

            if ($socData['hide_working_hours'] == 1) {
                $response['hide_working_hours'] = true;
            }else{
                $response['hide_working_hours'] = false;                
            }

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shift_time_id = $sdata['shift_time_id'];
            }

            if ($weekStartDate !=null || $weekStartDate != '') {
                $currdt= strtotime($weekStartDate);
                $nextmonth=strtotime($weekStartDate."+1 month");
                $i=0; 
                $flag=true;
            }else{
                $textdt=date("01-m-Y");
                $currdt= strtotime($textdt);
                $nextmonth=strtotime($textdt."+1 month");
                $i=0;
                $flag=true;
            }

            $response["weekly_history"] = array();
            $response["week_position"] = "0";

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shiftType = $shiftData['shift_type'];

            $parts = explode(':', $perdayHours);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);

            $weekDates = weeks_in_month(date('m'),date('Y'));

            // Week
            for ($x = 0; $x <= (count($startarr)-1); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";
                $weekArray["total_spend_time"] = "0";
                $weekArray["total_spend_time_name"] = "";

                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);

                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 
                
                $holidayAry = array();

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $days = array();
                    $days['day'] = $date->format('l');
                    $days['date'] = $dayDate = $date->format('Y-m-d');
                    $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate));

                    if ($dayDate == date('Y-m-d')) {
                        $response["week_position"] = $x."";
                    }
                    
                    $holidayQry = $d->selectRow("holiday_start_date","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' ");

                    $x1 = $x+1;

                    //if ($working_day_calculation == 0) {
                        if (mysqli_num_rows($holidayQry)>0) {
                            array_push($holidayAry,$dayDate);
                            $minusHoliday += $perdayHours;
                        } else {
                            // full day off
                            if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                                array_push($weekOffAry,$dayDate);
                                $minusWeekOffHR += $perdayHours;
                            }
                            // alternate week day off
                            if ($shiftData['has_altenate_week_off'] == "1") {  
                                if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                    in_array($x1,$alternateWeekOff)) {
                                    array_push($weekOffAry,$dayDate);
                                    $minusAlternateWeekOffDays += $perdayHours;
                                }
                            }
                        }
                    
                    // holiday off
                    
                    array_push($daysArray['days'],$days);
                }

                $totalWeekDay = count($daysArray['days']);
                
                $totalWeekHour = $perdayHours * $totalWeekDay;
                $totalWeekHour1 = ($totalWeekHour - $minusWeekOffHR) - $minusAlternateWeekOffDays;
                $totalWeekHour = $totalWeekHour1 - $minusHoliday;            
                
                
                $weekArray['week'] = date('d',strtotime($sDate))."-".date('d',strtotime($eDate))." ".date('F',strtotime($sDate))." ".date('Y',strtotime($sDate));

                // Week History

                $tdate = date('Y-m-d');

                $leaveQry = $d->select("leave_master","society_id = '$society_id'
                    AND user_id = '$user_id'
                    AND leave_start_date BETWEEN '$sDate' AND '$eDate'
                    AND leave_status = '1'");

                $leaveArray = array();
                $paidLeaveArray = array();
                $halfHeaveArray = array();
                $paidHalfHeaveArray = array();

                if (mysqli_num_rows($leaveQry) > 0) {

                    while($leaveData = mysqli_fetch_array($leaveQry)){

                        if ($leaveData['leave_day_type'] == "0") {
                            $lary = range_date($leaveData['leave_start_date'],
                                $leaveData['leave_end_date']);

                            for ($a = 0; $a < count($lary); $a++) {
                                array_push($leaveArray,$lary[$a]);  

                                if ($leaveData['paid_unpaid'] == 0) {
                                    array_push($paidLeaveArray,$lary[$a]);
                                }                          
                            } 
                        }else{
                            $laryHalf = range_date($leaveData['leave_start_date'],
                                $leaveData['leave_end_date']);

                            for ($b = 0; $b < count($laryHalf); $b++) {
                                array_push($halfHeaveArray,$laryHalf[$b]);

                                if ($leaveData['paid_unpaid'] == 0) {
                                    array_push($paidHalfHeaveArray,$laryHalf[$b]);
                                }
                            }                        
                        }
                    }
                }

                if (count($leaveArray) > 0) {
                    $leaveHR = (count($leaveArray) * $perdayHours);
                    $totalWeekHour = $totalWeekHour - $leaveHR;
                    $weekArray['total_hours'] = $totalWeekHour."";
                }

                if (count($halfHeaveArray) > 0) {
                    $totalWH = (count($halfHeaveArray) * ($perdayHours/2));
                    $totalWeekHour = $totalWeekHour - $totalWH;
                    $weekArray['total_hours'] = $totalWeekHour."";
                }

                list($hours1, $wrongMinutes) = explode('.', $totalWeekHour);
                $minutes1 = ($wrongMinutes < 10 ? $wrongMinutes * 10 : $wrongMinutes) * 0.6;

                $weekArray['total_hours_view'] = timeFormat($hours1.':'.$minutes1);
                $weekArray['total_hours'] = $totalWeekHour."";

                $totalMonthMinutes = minutes($hours1.':'.$minutes1);
                $weekArray['total_hours_minutes'] = $totalMonthMinutes."";    

                $weekArray['total_productive_spend_time'] = "";
                $weekArray['total_productive_spend_time_name'] = "";

                $weekArray['total_extra_spend_time'] = "";
                $weekArray['total_extra_spend_time_name'] = "";  

                $attendanceQry = $d->selectRow("am.*,apomr.attendance_id as attid, apomr.attendance_punch_out_missing_id, apomr.punch_out_missing_reject_reason,apomr.attendance_punch_out_missing_status","attendance_master AS am LEFT JOIN attendance_punch_out_missing_request AS apomr ON apomr.attendance_id = am.attendance_id","(am.society_id='$society_id' 
                    AND am.attendance_date_start != '$tdate' 
                    AND am.user_id = '$user_id' 
                    AND am.attendance_date_start BETWEEN '$sDate' AND '$eDate') 
                    OR (am.society_id='$society_id' 
                    AND am.user_id = '$user_id' 
                    AND am.attendance_date_start BETWEEN '$sDate' AND '$eDate')","ORDER BY am.attendance_date_start ASC");

                $weekArray["history"] = array();

                $timesProductive = array();
                $timesExtra = array();

                if(mysqli_num_rows($attendanceQry)>0){

                    $times = array();

                    while($data=mysqli_fetch_array($attendanceQry)) {

                        $attendanceHistory = array(); 

                        $attendanceHistory['extra_day'] = false;
                        $attendanceHistory['is_punch_out_missing'] = false;


                        $attendanceHistory["attendance_id"] = $data['attendance_id'];
                        $attendanceHistory["day_name"] = date('l', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_start"] = date('dS F Y', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_end"] = date('dS F Y', strtotime($data['attendance_date_end']));

                        $attendanceDateStart = $data['attendance_date_start'];
                        $attendanceDateEnd = $data['attendance_date_end'];
                        
                        $punchInTime = $data['punch_in_time'];
                        $punchOutTime = $data['punch_out_time'];

                        if (in_array($attendanceDateStart,$weekOffAry)) {
                            $attendanceHistory['extra_day'] = true;
                        }

                        if (in_array($attendanceDateStart,$holidayAry)) {
                            $attendanceHistory['extra_day'] = true;
                        }

                        if ($data['attendance_status'] == 2) {
                            $attendanceHistory['attendance_rejected'] = true;
                        }else{
                            $attendanceHistory['attendance_rejected'] = false;
                        }
                        
                        $attendanceHistory["punch_in_time"] = date("h:i A",strtotime($punchInTime));
                        $attendanceHistory["punch_out_time"] = date("h:i A",strtotime($punchOutTime));

                        $multiple_punch_in_out_data = $data['multiple_punch_in_out_data'];
                        $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

                        $attendanceHistory['punch_in_data'] = array();

                        if ($attendanceDateEnd == "0000-00-00") {
                            $attendanceHistory["attendance_date_end"] = "";
                            $attendanceHistory["punch_out_time"] = "";
                            $attendanceDateEnd = date('Y-m-d');
                            $punchOutTime = date('H:i:s');
                        }

                        if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                            for ($i=0; $i < count($multiplePunchDataArray); $i++) { 
                                $punchInData = array();

                                $punchInData['punch_in_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_in_date"]));
                                $punchInData['punch_in_time'] = date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_in_time"]));

                                if ($multiplePunchDataArray[$i]["punch_out_date"] != '' && $multiplePunchDataArray[$i]["punch_out_date"] != "00:00:00") {
                                    $punchInData['punch_out_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_out_date"]));
                                }else{
                                    if ($attendanceDateStart != date('Y-m-d')) {
                                        $punchInData['punch_out_date'] = "Missing";
                                    }else{                                    
                                        $punchInData['punch_out_date'] = "";
                                    }
                                }

                                if ($multiplePunchDataArray[$i]["punch_out_time"] != '' && $multiplePunchDataArray[$i]["punch_out_time"] != "00:00:00") {
                                    $punchInData['punch_out_time'] = date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_out_time"]));
                                }else{
                                    $punchInData['punch_out_time'] = "";
                                }

                                if ($multiplePunchDataArray[$i]["working_hour"] != '' && $multiplePunchDataArray[$i]["working_hour"] != "00:00") {
                                    $punchInData['working_hour'] = $multiplePunchDataArray[$i]["working_hour"];
                                }else{
                                    $punchInData['working_hour'] = "";
                                }

                                if ($multiplePunchDataArray[$i]["location_name_in"] != '') {
                                    $punchInData['location_name_in'] = $multiplePunchDataArray[$i]["location_name_in"];
                                }else{
                                    $punchInData['location_name_in'] = "";
                                }

                                if ($multiplePunchDataArray[$i]["location_name_out"] != '') {
                                    $punchInData['location_name_out'] = $multiplePunchDataArray[$i]["location_name_out"];
                                }else{
                                    $punchInData['location_name_out'] = "";
                                }

                                $punchInData['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"].'';

                                array_push($attendanceHistory['punch_in_data'],$punchInData);
                            }
                        }

                        if ($data['punch_out_time'] != "00:00:00" && !in_array($attendanceDateStart,$weekOffAry)
                            && !in_array($attendanceDateStart,$holidayAry) && $data['attendance_status'] == "1") {

                            $totalHours = $data['total_working_hours'];
                            $productive_working_hours = $data['productive_working_hours'];
                            $extra_working_hours = $data['extra_working_hours'];
                            $productive_working_hours_minutes = $data['productive_working_hours_minutes'];

                            $attendanceHistory['total_hours_spend'] = timeFormat($totalHours);
                            $attendanceHistory['productive_working_hours'] = timeFormat($productive_working_hours);
                            $attendanceHistory['extra_working_hours'] = timeFormat($extra_working_hours);
                            $attendanceHistory['productive_working_hours_minutes'] = $productive_working_hours_minutes;
                            $attendanceHistory['total_spend_time'] = "";
                            $attendanceHistory["punch_out_missing_message"] = "";
                            $attendanceHistory['is_punch_out_missing'] = false;

                            $times[] = $totalHours;
                            $timesProductive[] = $productive_working_hours;
                            $timesExtra[] = $extra_working_hours;

                            $weekArray['total_spend_time'] = getTotalWeekHours($times);
                            $weekArray['total_spend_time_name'] = getTotalWeekHoursName($times);

                            $weekArray['total_productive_spend_time'] = getTotalWeekHours($timesProductive);
                            $weekArray['total_productive_spend_time_name'] = getTotalWeekHoursName($timesProductive);

                            $weekArray['total_extra_spend_time'] = getTotalWeekHours($timesExtra);
                            $weekArray['total_extra_spend_time_name'] = getTotalWeekHoursName($timesExtra);

                        }else if ($data['punch_out_time'] != "00:00:00" && (in_array($attendanceDateStart,$weekOffAry)
                            || in_array($attendanceDateStart,$holidayAry)) && $data['attendance_status'] == "1") {
                            $totalHoursExtra = $data['total_working_hours'];
                            $productive_working_hours = $data['productive_working_hours'];
                            $extra_working_hours = $data['extra_working_hours'];
                            $productive_working_hours_minutes = $data['productive_working_hours_minutes'];

                            $times[] = $totalHours;
                            $timesProductive[] = $productive_working_hours;
                            $timesExtra[] = $extra_working_hours;

                            $weekArray['total_spend_time'] = getTotalWeekHours($times);
                            $weekArray['total_spend_time_name'] = getTotalWeekHoursName($times);

                            $weekArray['total_productive_spend_time'] = getTotalWeekHours($timesProductive);
                            $weekArray['total_productive_spend_time_name'] = getTotalWeekHoursName($timesProductive);

                            $weekArray['total_extra_spend_time'] = getTotalWeekHours($timesExtra);
                            $weekArray['total_extra_spend_time_name'] = getTotalWeekHoursName($timesExtra);

                            $attendanceHistory['total_hours_spend'] = timeFormat($totalHoursExtra);
                            $attendanceHistory['productive_working_hours'] = timeFormat($productive_working_hours);
                            $attendanceHistory['extra_working_hours'] = timeFormat($extra_working_hours);
                            $attendanceHistory['productive_working_hours_minutes'] = $productive_working_hours_minutes;
                            $attendanceHistory['total_spend_time'] = "";
                            $attendanceHistory["punch_out_missing_message"] = "";
                            $attendanceHistory['is_punch_out_missing'] = false;

                        }else{

                            $totalHours = $data['total_working_hours'];
                            $productive_working_hours = $data['productive_working_hours'];
                            $extra_working_hours = $data['extra_working_hours'];
                            $productive_working_hours_minutes = $data['productive_working_hours_minutes'];

                            if ($data['attendance_status'] == 1) {
                                $times[] = $totalHours;
                                $timesProductive[] = $productive_working_hours;
                                $timesExtra[] = $extra_working_hours;

                                $weekArray['total_spend_time'] = getTotalWeekHours($times);
                                $weekArray['total_spend_time_name'] = getTotalWeekHoursName($times);

                                $weekArray['total_productive_spend_time'] = getTotalWeekHours($timesProductive);
                                $weekArray['total_productive_spend_time_name'] = getTotalWeekHoursName($timesProductive);

                                $weekArray['total_extra_spend_time'] = getTotalWeekHours($timesExtra);
                                $weekArray['total_extra_spend_time_name'] = getTotalWeekHoursName($timesExtra);
                            }


                            if (($totalHours != '' && $totalHours != "00:00:00") || 
                                ($totalHours != '' && $totalHours != "00:00")) {
                                $attendanceHistory['total_hours_spend'] = timeFormat($totalHours);;
                            }else{
                                
                                $attendanceHistory['total_spend_time'] = "";
                                $attendanceHistory['total_hours_spend'] = "";
                            }

                            if (($productive_working_hours != '' && $productive_working_hours != "00:00:00") || 
                                ($productive_working_hours != '' && $productive_working_hours != "00:00")) {
                                $attendanceHistory['productive_working_hours'] = timeFormat($productive_working_hours);
                                $attendanceHistory['productive_working_hours_minutes'] = $productive_working_hours_minutes;
                            }else{
                                
                                $attendanceHistory['productive_working_hours'] = "";
                                $attendanceHistory['productive_working_hours_minutes'] = "";
                            }

                            if (($extra_working_hours != '' && $extra_working_hours != "00:00:00") || 
                                ($extra_working_hours != '' && $extra_working_hours != "00:00")) {
                                $attendanceHistory['extra_working_hours'] = timeFormat($extra_working_hours);
                            }else{
                                
                                $attendanceHistory['extra_working_hours'] = "";
                            }

                            if ($attendanceDateStart == $tdt) {
                                $attendanceHistory["punch_out_missing_message"] = "";
                                $attendanceHistory['is_punch_out_missing'] = false;
                            }else if(($data['attendance_status'] == '1' || $data['attendance_status'] == '0') &&
                                $data['punch_out_time'] == "00:00:00"){                    
                                $attendanceHistory["punch_out_missing_message"] = "Punch Out Missing";
                                $attendanceHistory['is_punch_out_missing'] = true;
                            }else if($data['attendance_status'] == '1' || $data['attendance_status'] == '0'){
                                $attendanceHistory["punch_out_missing_message"] = "";
                                $attendanceHistory['is_punch_out_missing'] = false;
                            }
                        }

                        if ($data['attendance_status'] == "0") {
                            $attendanceHistory["attendnace_pending"] = true;
                            $attendanceHistory["attendance_pending_message"] = "Attendance Pending";
                        }else{
                            $attendanceHistory["attendnace_pending"] = false;
                            $attendanceHistory["attendance_pending_message"] = "";
                        }

                        array_push($weekArray["history"], $attendanceHistory);
                    }
                }

                array_push($response['weekly_history'],$weekArray);
            } 
            
            echo json_encode($response);
        
        }else{
            $response["message"]="Wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="Wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function getTotalWeekHours($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d.%02d', $hours, $minutes);
}

function getTotalWeekHoursName($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}


function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes > 0) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }

}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}

function weeks_in_month($month, $year){
    $dates = [];

    $week = 1;
    $date = new DateTime("$year-$month-01");
    $days = (int)$date->format('t'); // total number of days in the month

    $oneDay = new DateInterval('P1D');

    for ($day = 1; $day <= $days; $day++) {
        $dates["Week_$week"] []= $date->format('Y-m-d');

        $dayOfWeek = $date->format('l');
        if ($dayOfWeek === 'Saturday') {
            $week++;
        }

        $date->add($oneDay);
    }

    return $dates;
}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}

function timeFormat($time){
    $time = explode(':', $time);
    $hours = $time[0];
    $minutes = $time[1];
    if ($hours > 0 && $minutes > 0) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }
}


function hoursandmins($time, $format = '%02d:%02d')
{
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
?>