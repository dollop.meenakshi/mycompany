<?php

include_once 'lib.php';
extract(array_map("test_input" , $_POST));
$response = array();
if (isset($name)) {

	$to = 'asif.silverwing@gmail.com';
    $subject = "New Inquiry from MyCo App";

    $message = "
    <html>
	<head>
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<title>New Inquiry from MyCo</title>


	</head>
	<body style='-webkit-text-size-adjust: none; box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; height: 100%; line-height: 1.4; margin: 0; width: 100% !important;' bgcolor='#F2F4F6'><style type='text/css'>
	body {
	  width: 100% !important; height: 100%; margin: 0; line-height: 1.4; background-color: #F2F4F6; color: #74787E; -webkit-text-size-adjust: none;
	}

	@media only screen and (max-width: 600px) {
	  .email-body_inner {
	    width: 100% !important;
	  }
	  .email-footer {
	    width: 100% !important;
	  }
	}
	@media only screen and (max-width: 500px) {
	  .button {
	    width: 100% !important;
	  }
	}
	</style>
	<table class='email-wrapper' width='100%' cellpadding='0' cellspacing='0' style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%;' bgcolor='#F2F4F6'>
	<tr>
	<td align='center' style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word;'>
	<table class='email-content' width='100%' cellpadding='0' cellspacing='0' style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%;'>
	<tr>
	</tr>

	<tr>
	<td class='email-body' width='100%' cellpadding='0' cellspacing='0' style='-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-bottom-color: #EDEFF2; border-bottom-style: solid; border-bottom-width: 1px; border-top-color: #EDEFF2; border-top-style: solid; border-top-width: 1px; box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%; word-break: break-word;' bgcolor='#FFFFFF'>
	<table class='email-body_inner' align='center' width='570' cellpadding='0' cellspacing='0' style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0 auto; padding: 0; width: 570px;' bgcolor='#FFFFFF'>
	<tr>
	<td align='center'><img src='https://www.my-company.app/assets/img/logo.png' width='100'></td>
	</tr>    
	<tr>
	<td class='content-cell' style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 35px; word-break: break-word;'>

	<h1 style='box-sizing: border-box; color: #2F3133; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 19px; font-weight: bold; margin-top: 0;' align='center'><span style='color:red;'>New Inquiry from MyCo App</span></h1>


	<p style='box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; line-height: 1.5em; margin-top: 0;' align='center'><strong style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;'>Name: $name</strong></p>
	<p style='box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; line-height: 1.5em; margin-top: 0;' align='center'><strong style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;'>MObile: $mobile</strong></p>
	<p style='box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; line-height: 1.5em; margin-top: 0;' align='center'><strong style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;'>Email: $email</strong></p>
	<p style='box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; line-height: 1.5em; margin-top: 0;' align='center'><strong style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;'>Message: $message</strong></p>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word;'>
	<table class='email-footer' align='center' width='570' cellpadding='0' cellspacing='0' style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0 auto; padding: 0; text-align: center; width: 570px;'>
	<tr>
	<td class='content-cell' align='center' style='box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 35px; word-break: break-word;'>
	<p class='sub align-center' style='box-sizing: border-box; color: #AEAEAE; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 1.5em; margin-top: 0;' align='center'>© 2021  MyCo. All rights reserved.</p>
	<p class='sub align-center' style='box-sizing: border-box; color: #AEAEAE; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 1.5em; margin-top: 0;' align='center'>
	MyCo
	</p>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</body>
	</html>
    ";
      // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      // More headers
    $headers .= 'From: <no-reply@MyCo.com>';
    mail($to,$subject,$message,$headers);

    $response["message"] = "Thank you for your Feedback.";
    $response["status"] = "200";
    echo json_encode($response);
    
}

 ?>