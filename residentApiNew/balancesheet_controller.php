<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
    
    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
    $temDate = date("Y-m-d h:i:s");
    

        if (isset($balancesheet_master) && $balancesheet_master=='balancesheet_master' && $society_id!=''  && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

        $q=$d->select("balancesheet_master","society_id ='$society_id' AND society_id!=0 AND status='0'");


            if (mysqli_num_rows($q)>0) {
            
            $response["balancesheet"]=array();

            $cash_on_hand=0;
            $totalIncome=0;
           

            while($balancesheet_data=mysqli_fetch_array($q)) {

            $balancesheet_record = array(); 

            $empData=$balancesheet_data['balancesheet_id'];
            $balancesheet_record["balancesheet_id"]=$balancesheet_data['balancesheet_id'];
            $balancesheet_record["balancesheet_name"]=$balancesheet_data['balancesheet_name'];
            
            
            $count=$d->sum_data("received_amount","receive_maintenance_master","receive_maintenance_status='1' AND society_id='$society_id' AND balancesheet_id='$empData'");
                         while($row=mysqli_fetch_array($count))
                        {
                             $asif=$row['SUM(received_amount)'];
                             $totalMain=number_format($asif,2,'.','');

                         }

                         $count1=$d->sum_data("received_amount","receive_bill_master","society_id='$society_id' AND balancesheet_id='$empData'");
                         while($row1=mysqli_fetch_array($count1))
                        {
                             $asif1=$row1['SUM(received_amount)'];
                            $totalBill=number_format($asif1,2,'.','');

                         }

                         $count2=$d->sum_data("receive_amount","facilitybooking_master","payment_status=0 AND society_id='$society_id' AND balancesheet_id='$empData'");
                         while($row2=mysqli_fetch_array($count2))
                        {
                             $asif2=$row2['SUM(receive_amount)'];
                             $totalFac=number_format($asif2,2,'.','');

                         }

                          $icnome=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$empData'");
                         while($row2=mysqli_fetch_array($icnome))
                        {
                             $asif3=$row2['SUM(income_amount)'];
                             $totalIncome2=number_format($asif3,2,'.','');
                         }

                      
                          $exp=$d->sum_data("expenses_amount","expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$empData'");
                           while($row5=mysqli_fetch_array($exp))
                          {
                            $asif5=$row5['SUM(expenses_amount)'];
                            $expAmount=number_format($asif5,2,'.','');
                           }
                           
                           
                           
                $totalIncome2= $totalMain+$totalBill+ $totalFac+$totalIncome2;
        
                  $totalIncome=$totalIncome2-$expAmount;
               
                $balancesheet_record["current_balance"]=$totalIncome=number_format($totalIncome,2,'.','');;
                $cash_on_hand=$cash_on_hand+$totalIncome;
            
                array_push($response["balancesheet"], $balancesheet_record); 
            }
            
            
                $response["cash_on_hand"]=$cash_on_hand=number_format($cash_on_hand,2,'.','');;
        
                $response["message"]="$datafoundMsg";
                $response["status"]="200";
                echo json_encode($response);
            
            }else{
                $response["message"]="$noDatafoundMsg";
                $response["status"]="201";
                echo json_encode($response);
            }

    }else if(isset($balancesheet_recode) && $balancesheet_recode=='balancesheet_recode'  && filter_var($balancesheet_id, FILTER_VALIDATE_INT) == true){
      

        $q=$d->select("balancesheet_master","balancesheet_id ='$balancesheet_id' AND balancesheet_id!=0 AND status='0'");

        if (mysqli_num_rows($q)>0) {
        
        $response["balancesheet"]=array();

        $cash_on_hand=0;

        while($balancesheet_data=mysqli_fetch_array($q)) {

        $balancesheet_record = array(); 

        $balancesheet_record["balancesheet_id"]=$balancesheet_data['balancesheet_id'];
        $balancesheet_record["balancesheet_name"]=$balancesheet_data['balancesheet_name'];
        $balancesheet_record["current_balance"]=$balancesheet_data['current_balance'];
        $cash_on_hand=$cash_on_hand+$balancesheet_data['current_balance'];

        $balancesheet_id = $balancesheet_data['balancesheet_id'];

        if ($month=="All") {

            $qmain = $d->select("receive_maintenance_master,maintenance_master","receive_maintenance_master.balancesheet_id ='$balancesheet_id' AND receive_maintenance_master.receive_maintenance_status='1' AND receive_maintenance_master.maintenance_id=maintenance_master.maintenance_id AND receive_maintenance_master.receive_maintenance_auto_date LIKE '%$year%'");

      }else{
           $auto_created_date=$month."-".$year;

           $qmain = $d->select("receive_maintenance_master,maintenance_master","receive_maintenance_master.balancesheet_id ='$balancesheet_id' AND receive_maintenance_master.receive_maintenance_status='1' AND receive_maintenance_master.maintenance_id=maintenance_master.maintenance_id AND receive_maintenance_master.receive_maintenance_auto_date LIKE '%$auto_created_date%'");

        }
        


        if ($month=="All") {

            $qbill = $d->select("receive_bill_master,bill_master","receive_bill_master.balancesheet_id ='$balancesheet_id' AND receive_bill_master.receive_bill_status='3' AND bill_master.bill_master_id=receive_bill_master.bill_master_id AND receive_bill_master.receive_bill_auto_date LIKE '%$year%'");

      }else{
           $auto_created_date=$month."-".$year;

           $qbill = $d->select("receive_bill_master,bill_master","receive_bill_master.balancesheet_id ='$balancesheet_id' AND receive_bill_master.receive_bill_status='3' AND bill_master.bill_master_id=receive_bill_master.bill_master_id AND receive_bill_master.receive_bill_auto_date LIKE '%$auto_created_date%'");

        }

        if ($month=="All") {

            $qexpence = $d->select("expenses_balance_sheet_master","balancesheet_id ='$balancesheet_id' AND expenses_created_date LIKE '%$year%' AND income_amount!=0","ORDER BY expenses_balance_sheet_id DESC");

        }else{
           $auto_created_date=$month."-".$year;

           $qexpence = $d->select("expenses_balance_sheet_master","balancesheet_id ='$balancesheet_id' AND expenses_created_date LIKE '%$auto_created_date%' AND income_amount!=0","ORDER BY expenses_balance_sheet_id DESC");

        }

        if ($month=="All") {

            $qfacility = $d->select("facilities_master,facilitybooking_master","facilitybooking_master.facility_id=facilities_master.facility_id AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.balancesheet_id='$balancesheet_id' AND facilitybooking_master.payment_status=0","ORDER BY facilitybooking_master.booking_id DESC");

        }else{
           $auto_created_date=$month."-".$year;

           $qfacility = $d->select("facilities_master,facilitybooking_master","facilitybooking_master.facility_id=facilities_master.facility_id AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.balancesheet_id='$balancesheet_id' AND facilitybooking_master.payment_status=0","ORDER BY facilitybooking_master.booking_id DESC");

        }

        $balancesheet_record["receive_maintenance_bill_master"]=array();

        if(mysqli_num_rows($qmain)>0){


            while($maintenance_data=mysqli_fetch_array($qmain)) {

                $receive_maintenance_bill_master = array(); 

                $receive_maintenance_bill_master["type"]="m";
                $receive_maintenance_bill_master["receive_maintenance_id"]=$maintenance_data['receive_maintenance_id'];
                $receive_maintenance_bill_master["maintenance_id"]=$maintenance_data['maintenance_id'];
                $receive_maintenance_bill_master["society_id"]=$maintenance_data['society_id'];
                $receive_maintenance_bill_master["balancesheet_id"]=$maintenance_data['balancesheet_id'];
                $receive_maintenance_bill_master["income_for"]=$maintenance_data['maintenance_name'];

                $receive_maintenance_bill_master["unit_id"]=$maintenance_data['unit_id'];
                $unit_id=$maintenance_data['unit_id'];

                $receive_maintenance_bill_master["payment_type"]=$maintenance_data['payment_type'];

                $payData="";

                if($maintenance_data['payment_type']==0){

                    $payData="Cash";

                }else if($maintenance_data['payment_type']==1){

                    $payData="cheque";

                }else{
                    $payData="online";

                }


                $quserInfo=$d->select("unit_master,block_master","unit_master.block_id = block_master.block_id AND unit_master.unit_id='$unit_id'");
                $user_data=mysqli_fetch_array($quserInfo);
                
                $receive_maintenance_bill_master["block_name"]=$user_data['block_name']."-".$user_data['unit_name']." (payment mode :".$payData." ) ";

                $receive_maintenance_bill_master["received_amount"]=$maintenance_data['received_amount'];
                $receive_maintenance_bill_master["maintence_amount"]=$maintenance_data['maintence_amount'];


                $receive_maintenance_bill_master["receive_maintenance_date"]=$maintenance_data['receive_maintenance_date'];
                $receive_maintenance_bill_master["receive_maintenance_receipt_photo"]=$base_url."img/maintenanceReceipt/".$maintenance_data['receive_maintenance_receipt_photo'];
                $receive_maintenance_bill_master["receive_maintenance_status"]=$maintenance_data['receive_maintenance_status'];



                array_push($balancesheet_record["receive_maintenance_bill_master"], $receive_maintenance_bill_master); 


            }

        }

        if(mysqli_num_rows($qbill)>0){


            
            while($bill_data=mysqli_fetch_array($qbill)) {

                $receive_maintenance_bill_master = array(); 

                $receive_maintenance_bill_master["type"]="b";

                $receive_maintenance_bill_master["receive_bill_id"]=$bill_data['receive_bill_id'];
                $receive_maintenance_bill_master["bill_master_id"]=$bill_data['bill_master_id'];
                $receive_maintenance_bill_master["society_id"]=$bill_data['society_id'];
                $receive_maintenance_bill_master["balancesheet_id"]=$bill_data['balancesheet_id'];
                $receive_maintenance_bill_master["unit_id"]=$bill_data['unit_id'];
                $receive_maintenance_bill_master["income_for"]=$bill_data['bill_name'];

                $unit_id=$bill_data['unit_id'];

                $payData="";

                if($bill_data['bill_payment_type']==0){

                    $payData="Cash";

                }else if($bill_data['bill_payment_type']==1){

                    $payData="cheque";

                }else{
                    $payData="online";

                }


                $quserInfo=$d->select("unit_master,block_master","unit_master.block_id = block_master.block_id AND unit_master.unit_id='$unit_id'");
                $user_data=mysqli_fetch_array($quserInfo);
                
                $receive_maintenance_bill_master["block_name"]=$user_data['block_name']."-".$user_data['unit_name']." (payment mode :".$payData." ) ";


                $receive_maintenance_bill_master["received_amount"]=$bill_data['received_amount'];
                $receive_maintenance_bill_master["bill_amount"]=$bill_data['bill_amount'];
                $receive_maintenance_bill_master["no_of_unit"]=$bill_data['no_of_unit'];
                $receive_maintenance_bill_master["unit_price"]=$bill_data['unit_price'];
                $receive_maintenance_bill_master["unit_photo"]=$base_url."img/billUnits/".$bill_data['unit_photo'];
                $receive_maintenance_bill_master["receive_bill_receipt_photo"]=$base_url."img/billReceipt/".$bill_data['receive_bill_receipt_photo'];
                $receive_maintenance_bill_master["receive_bill_status"]=$bill_data['receive_bill_status'];
                $receive_maintenance_bill_master["bill_payment_date"]=$bill_data['bill_payment_date'];
                $receive_maintenance_bill_master["bill_payment_type"]=$bill_data['bill_payment_type'];


                array_push($balancesheet_record["receive_maintenance_bill_master"], $receive_maintenance_bill_master); 


            }



        }
        if(mysqli_num_rows($qexpence)>0){


            while($expence_data=mysqli_fetch_array($qexpence)) {

                $receive_maintenance_bill_master = array(); 

                $receive_maintenance_bill_master["type"]="e";

                $receive_maintenance_bill_master["receive_bill_id"]=$expence_data['expenses_balance_sheet_id'];
                $receive_maintenance_bill_master["bill_master_id"]=$expence_data['balancesheet_id'];
                $receive_maintenance_bill_master["society_id"]=$expence_data['society_id'];
                $receive_maintenance_bill_master["balancesheet_id"]=$expence_data['balancesheet_id'];
                $receive_maintenance_bill_master["block_name"]="self submitted";
                $receive_maintenance_bill_master["received_amount"]=$expence_data['income_amount'];
                $receive_maintenance_bill_master["bill_amount"]=$expence_data['income_amount'];
                $receive_maintenance_bill_master["bill_payment_date"]=$expence_data['expenses_add_date'];
                $receive_maintenance_bill_master["income_for"]="self submitted";


                array_push($balancesheet_record["receive_maintenance_bill_master"], $receive_maintenance_bill_master); 


            }



        }


        if(mysqli_num_rows($qfacility)>0){


            while($facility_data=mysqli_fetch_array($qfacility)) {

                $receive_maintenance_bill_master = array(); 

                $receive_maintenance_bill_master["type"]="f";

                $receive_maintenance_bill_master["income_for"]=$facility_data['facility_name'];

                $receive_maintenance_bill_master["receive_bill_id"]=$facility_data['booking_id'];
                $receive_maintenance_bill_master["bill_master_id"]=$facility_data['facility_id'];
                $receive_maintenance_bill_master["society_id"]=$facility_data['society_id'];
                $receive_maintenance_bill_master["balancesheet_id"]=$facility_data['balancesheet_id'];
                
                $quserInfo=$d->select("unit_master,block_master","unit_master.block_id = block_master.block_id AND unit_master.unit_id='$unit_id'");
                $user_data=mysqli_fetch_array($quserInfo);
                
                $receive_maintenance_bill_master["block_name"]=$user_data['block_name']."-".$user_data['unit_name']." (payment mode :".$payData." ) ";

                $receive_maintenance_bill_master["received_amount"]=$facility_data['receive_amount'];
                $receive_maintenance_bill_master["bill_amount"]=$facility_data['facility_amount'];
                $receive_maintenance_bill_master["bill_payment_date"]=$facility_data['payment_received_date'];


                array_push($balancesheet_record["receive_maintenance_bill_master"], $receive_maintenance_bill_master); 


            }



        }



            array_push($response["balancesheet"], $balancesheet_record); 
        }
            $response["cash_on_hand"]=$cash_on_hand;
        
            $response["message"]="$datafoundMsg";
            $response["status"]="200";
            echo json_encode($response);
        
        }else{
            $response["message"]="$noDatafoundMsg";
            $response["status"]="201";
            echo json_encode($response);
        }




    }else if(isset($getExpenses) && $getExpenses=='getExpenses' && filter_var($balancesheet_id, FILTER_VALIDATE_INT) == true){




        if ($month=="All") {

            $qcomplain = $d->select("expenses_balance_sheet_master", "balancesheet_id='$balancesheet_id' AND expenses_status='0' AND expenses_created_date LIKE '%$year%' AND expenses_amount!=0 AND balancesheet_id!=0","ORDER BY expenses_balance_sheet_id DESC");
      
      }else{
           $auto_created_date=$month."-".$year;

           $qcomplain = $d->select("expenses_balance_sheet_master", "balancesheet_id='$balancesheet_id' AND expenses_status='0' AND expenses_created_date LIKE '%$auto_created_date%' AND expenses_amount!=0 AND balancesheet_id!=0","ORDER BY expenses_balance_sheet_id DESC");

        }



        if (mysqli_num_rows($qcomplain) > 0) {
            $response["expenses"] = array();

            while ($data_complain_list = mysqli_fetch_array($qcomplain)) {

                $expenses = array();

                $expenses["expenses_balance_sheet_id"] = $data_complain_list['expenses_balance_sheet_id'];
                $expenses["society_id"] = $data_complain_list['society_id'];
                $expenses["balancesheet_id"] = $data_complain_list['balancesheet_id'];
                $expenses["expenses_title"] = $data_complain_list['expenses_title'];
                $expenses["expenses_amount"] = $data_complain_list['expenses_amount'];
                $expenses["expenses_add_date"] = $data_complain_list['expenses_add_date'];
                $expenses["expenses_status"] = $data_complain_list['expenses_status'];

                array_push($response["expenses"], $expenses);
            }
            $response["message"] = "$datafoundMsg";
            $response["status"] = "200";
            echo json_encode($response);
        } else {

            $response["message"] = "$noDatafoundMsg";
            $response["status"] = "201";
            echo json_encode($response);
        } 


    } else if(isset($balancesheet_pdf) && $balancesheet_pdf=='balancesheet_pdf'  && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
        $access_denied = $xml->string->access_denied;

            if ($user_type==1 && $user_type!='') {
                $quc=$d->selectRow("teanant_access_for_balancesheet","society_master","society_id='$society_id' AND teanant_access_for_balancesheet=1");
                 if (mysqli_num_rows($quc)>0) {
                    $response["message"] = "$access_denied";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                 }
                
            }

            if ($user_id!='' || $user_id!=0) {
                $sq=$d->selectRow("block_id","users_master","user_id='$user_id'");
                $userData=mysqli_fetch_array($sq);
                $block_id=$userData['block_id'];
                if ($block_id!='') {
                    $appendQuery= " OR society_id ='$society_id' AND share_to_users=1 AND block_id='$block_id'";
                }
            }

             $q1=$d->select("balancesheet_master","society_id ='$society_id' AND share_to_users=1 AND block_id=0 $appendQuery");

             $q=$d->select("balancesheet_pdf_master","society_id ='$society_id'");

            $response["balancesheet"]=array();

                if (mysqli_num_rows($q)>0) {
                
                   

                    while($balancesheet_data=mysqli_fetch_array($q)) {

                        $balancesheetPdf = array(); 

                         $qad=$d->select("bms_admin_master","admin_id='$balancesheet_data[created_by]'");
                        $adminData=mysqli_fetch_array($qad);

                        $balancesheetPdf["balancesheet_file_id"]=$balancesheet_data['balancesheet_file_id'];
                        $balancesheetPdf["balancesheet_name"]=html_entity_decode($balancesheet_data['balancesheet_name']);
                        $balancesheetPdf["file_name"]=$balancesheet_data['file_name'];
                        $balancesheetPdf["balancesheet_pdf"]=$base_url."img/balancesheet/".$balancesheet_data['file_name'];
                        $balancesheetPdf["created_date"]=$balancesheet_data['created_date'];
                        $balancesheetPdf["created_by"]=$adminData['admin_name'].'';
                        $balancesheetPdf["type"]='0';

                        array_push($response["balancesheet"],$balancesheetPdf);
                    }

                    $query1= true;
                } else {
                   $query1= false;
                }    

                if (mysqli_num_rows($q1)>0) {
                
                   

                    while($balancesheet_data=mysqli_fetch_array($q1)) {

                        $balancesheetPdf = array(); 

                         $qad=$d->select("bms_admin_master","admin_id='$balancesheet_data[shared_by]'");
                        $adminData=mysqli_fetch_array($qad);

                        $balancesheetPdf["balancesheet_file_id"]=$balancesheet_data['balancesheet_id'];
                        $balancesheetPdf["balancesheet_name"]=html_entity_decode($balancesheet_data['balancesheet_name']);
                        $balancesheetPdf["file_name"]=$balancesheet_data['balancesheet_name'];
                        $balancesheetPdf["balancesheet_pdf"]=$base_url."apAdmin/BalancesheetPublicView.php?bId=$balancesheet_data[balancesheet_id]&sId=$balancesheet_data[society_id]";
                        $balancesheetPdf["created_date"]=date("d M Y");
                        $balancesheetPdf["created_by"]=$adminData['admin_name'].'';
                        $balancesheetPdf["type"]='1';


                        array_push($response["balancesheet"],$balancesheetPdf);
                    }
                    
                    
                    $query2= true;
                } else {
                   $query2= false;
                }    

                 if ($query1==true || $query2 ==true) {
                   $response["message"] = "$datafoundMsg";
                    $response["status"] = "200";
                    echo json_encode($response);
                } else {
                    $response["message"] = "$noDatafoundMsg";
                    $response["status"] = "201";
                    echo json_encode($response);

                }

                
                
        } else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }

    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}

?>