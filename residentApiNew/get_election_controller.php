<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
            if($_POST['getElectionListNew']=="getElectionListNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                 $qcheck=$d->selectRow("user_type,member_status","users_master","user_id='$user_id'");
                $userData=mysqli_fetch_array($qcheck);


              // for owner
                $qnotification=$d->select("election_master","((society_id='$society_id' AND election_for=0) OR (society_id='$society_id' AND society_id!=0 AND election_for='$floor_id') )","ORDER BY election_id DESC");
             

                $response["election"] = array();
                $response["election_completed"] = array();

                if(mysqli_num_rows($qnotification)>0){

                    while($data_notification=mysqli_fetch_array($qnotification)) {
                        switch ($data_notification['election_status']) {
                          case '0':
                            $status= "Nomination Open";
                            break;
                          case '1':
                            $status= "Voting Open";
                            break;
                          case '2':
                            $status= "Result Published";
                            break;
                          case '3':
                            $status= "Voting Closed";
                            break;
                          default:
                           $status= "Votting Open";
                            break;
                        }
                        $election = array(); 
                        $election["election_name"]=html_entity_decode($data_notification['election_name']);
                        $election["election_description"]=html_entity_decode($data_notification['election_description']);
                        $election["election_id"]=$data_notification['election_id'];
                        $election["election_date"]=date("d M Y", strtotime($data_notification['election_date']));
                        $election["election_status_view"]=$status;
                        $election["election_status"]=$data_notification['election_status'];

                         $qad=$d->select("bms_admin_master","admin_id='$data_notification[created_by]'");
                         $adminData=mysqli_fetch_array($qad);
                         if ($adminData['admin_name']!="") {
                           $election_created_by = $adminData['admin_name'];
                         } else {
                          $election_created_by = "Admin";
                         }

                        $election["election_created_by"]=$election_created_by;


                        if ($data_notification['election_status']==0 || $data_notification['election_status']==1 ) {
                          array_push($response["election"], $election); 
                        } elseif ($data_notification['election_status']==2 || $data_notification['election_status']==3) {
                          array_push($response["election_completed"], $election); 
                        }
                    }

                    $response["message"]="Get election success.";
                    $response["status"]="200";
                    echo json_encode($response);

                }
                else{
                $response["message"]="Get Election Fail.";
                $response["status"]="201";
                echo json_encode($response);

            }

    }else if($_POST['ApplyStatus']=="ApplyStatus" && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

              $pq= $d->selectRow("election_for","election_master","election_id='$election_id'");
              $pollData=mysqli_fetch_array($pq);
              $election_for = $pollData['election_for'];

                
                  $qnotification=$d->select("election_users","election_id='$election_id' AND election_id!=0 AND user_id='$user_id'");

                $userData=mysqli_fetch_array($qnotification);

                if(mysqli_num_rows($qnotification)>0){
                    if ($userData['election_user_status']==0) {
                      $response["message"]="Nomination Pending";
                    } elseif ($userData['election_user_status']==1) {
                      $response["message"]="Nomination Approved";
                    } elseif ($userData['election_user_status']==2) {
                      $response["message"]="Nomination Rejected for ".$userData['reject_msg'];
                    } else {
                      $response["message"]="Something Wrong";
                    }

                    $response["status"]="200";
                    echo json_encode($response);
                }
                else{
                $response["message"]="Yet Not Submitted.";
                $response["status"]="201";
                echo json_encode($response);
            }
}else if($_POST['ApplyForNomination']=="ApplyForNomination" && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true  ){

	    $m->set_data('election_id',$election_id);
	  	$m->set_data('user_id',$user_id);
	    $m->set_data('society_id',$society_id);
	    $m->set_data('unit_id',$unit_id);
        $m->set_data('election_user_status','0');

          $a1 = array(
                    'election_id'=>$m->get_data('election_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'society_id'=>$m->get_data('society_id'),
                  	'unit_id'=>$m->get_data('unit_id'),
                    'election_user_status'=>$m->get_data('election_user_status')        
                );

              $qqq=$d->selectRow("user_id","election_users","user_id='$user_id' AND election_id='$election_id'");
              if (mysqli_num_rows($qqq)) {
                $response["message"]="Already Nominated";
                $response["status"]="201";
                echo json_encode($response);
                exit();
              }

                $q= $d->insert("election_users",$a1);

                if($q==true){

                      $block_id=$d->getBlockid($user_id);
                      $fcmArray=$d->selectAdminBlockwise("5",$block_id,"android");
                      $fcmArrayIos=$d->selectAdminBlockwise("5",$block_id,"ios");

                      $nAdmin->noti_new($society_id,"",$fcmArray,"New Nomination Request by",$user_name,"electionParticipateUsers?election_id=$election_i");
                      
                      $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,"New Nomination Request by",$user_name,"electionParticipateUsers?election_id=$election_i");
          
                              
                              $notiAry = array(
                                  'society_id'=>$society_id,
                                  'notification_tittle'=>"New Nomination Request by",
                                  'notification_description'=>$user_name,
                                  'notifiaction_date'=>date('Y-m-d H:i'),
                                  'notification_action'=>"electionParticipateUsers?election_id=$election_id",
                                  'admin_click_action	'=>"electionParticipateUsers?election_id=$election_id",
                                   'notification_logo'=>'ElectionNew.png',
                                );
                                
                                  $d->insert("admin_notification",$notiAry);

                  $d->insert_myactivity($user_id,"$society_id","0","$user_name","Election nomination submitted","ElectionNew.png");


                    $response["message"]="Your nomination has been submitted successfully.";
                    $response["status"]="200";
                    echo json_encode($response);
                }
                else{
                $response["message"]="Sorry,Try Again..!";
                $response["status"]="201";
                echo json_encode($response);

            }

    }else if($_POST['getElectionOptionList']=="getElectionOptionList" && filter_var($election_id, FILTER_VALIDATE_INT) == true ){

                $voting_option_data=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id='$election_id' AND election_users.election_user_status=1","ORDER BY election_users.given_vote DESC");
                if(mysqli_num_rows($voting_option_data)>0){

                    $response["option"] = array();

                    $count5 = $d->sum_data("given_vote", "election_users", "election_id='$election_id'");
                    $row = mysqli_fetch_array($count5);
                    $totalVoting = $row['SUM(given_vote)'];
            
                    while($data_voting_option_list=mysqli_fetch_array($voting_option_data)) {

                        $option = array();
                        $option["voting_option_id"]=$data_voting_option_list['election_user_id'];
                        $option["society_id"]=$data_voting_option_list['society_id'];
                        $option["voting_id"]=$data_voting_option_list['election_user_id'];
                        $option["option_name"]=html_entity_decode($data_voting_option_list['user_full_name']);

                        $votingPer = $data_voting_option_list['given_vote'];

                        if($votingPer != 0){
                          $option["votingPer"] = number_format(100 * $votingPer / $totalVoting);
                        }else{
                          $option["votingPer"]="0";
                        }
                        array_push($response["option"], $option); 
                    }

                     // add Not option
                     $nq=$d->select("election_users","election_id='$election_id' AND election_user_status=1 AND is_nota=1","");
                     $notData=mysqli_fetch_array($nq);
                    if(mysqli_num_rows($nq)>0){
                        $option["voting_option_id"]=$notData['election_user_id'];
                        $option["society_id"]=$notData['society_id'];
                        $option["voting_id"]=$notData['election_user_id'];
                        $option["option_name"]="NOTA";

                        $votingPer = $notData['given_vote'];
                        if($votingPer != 0){
                          $option["votingPer"] = number_format(100 * $votingPer / $totalVoting);
                        }else{
                          $option["votingPer"]="0";
                        }
                        array_push($response["option"], $option); 

                    }

                    $pq= $d->selectRow("election_for","election_master","election_id='$election_id'");
                    $pollData=mysqli_fetch_array($pq);
                    $election_for = $pollData['election_for'];


                    if ($election_for==1 || $election_for==2 && $unit_id!=0) {
                      $voting_result_data=$d->select("election_result_master,users_master","users_master.unit_id=users_master.unit_id AND election_result_master.election_id='$election_id'AND users_master.unit_id ='$unit_id'");
                    } else {
                      $voting_result_data=$d->select("election_result_master","election_id='$election_id'AND user_id ='$user_id'");
                    }

		                if(mysqli_num_rows($voting_result_data)>0){
		                    $response["voting_submitted"]="200";
		                }else{
		                    $response["voting_submitted"]="201";
		                }
		                  

                    $response["totalVoting"] = $totalVoting;
                    $response["message"]="Get Voting Option success.";
                    $response["status"]="200";
                    echo json_encode($response);
        
                }else{

                    $response["message"]="Election is in Nomination Mode..";
                    $response["status"]="201";
                    echo json_encode($response);
        
                }
   }else if($_POST['addElectionVote']=="addElectionVote" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

        $m->set_data('election_id',$election_id);
        $m->set_data('society_id',$society_id);
        $m->set_data('user_id',$user_id);
        $m->set_data('par_user_id',$election_user_id);



        $a= array (
          'election_id'=> $m->get_data('election_id'),
          'society_id'=> $m->get_data('society_id'),
          'user_id'=> $m->get_data('user_id'),
          'par_user_id'=> $m->get_data('par_user_id')
        );

        $q=$d->insert('election_result_master',$a);


        $voting_option_data2=$d->select("election_users","election_user_id ='$election_user_id'");


        if(mysqli_num_rows($voting_option_data2)>0){
             $data=mysqli_fetch_array($voting_option_data2);
            $option_count=$data['given_vote'];
            
        }
       $option_count=$option_count+1;
    
        $a1= array (
          'given_vote'=> $option_count
         );

         $q1=$d->update('election_users',$a1,"election_user_id ='$election_user_id'");

       if ($q==true) {
          $response["status"]="200";
          $response["message"]="Thank you for your vote";
          echo json_encode($response);
        }else{
          $response["status"]="201";
          $response["message"]="Something Wrong...!";
          echo json_encode($response);
       }
   }else if($_POST['getElectionResult']=="getElectionResult" && filter_var($election_id, FILTER_VALIDATE_INT) == true ){

              $nq=$d->select("election_users","election_id='$election_id' AND election_user_status=1 AND is_nota=1","");
              $notData=mysqli_fetch_array($nq);
                    $response["result"] = array();
              
                $voting_option_data=$d->select("election_users,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=election_users.unit_id AND users_master.user_id=election_users.user_id AND election_users.election_id='$election_id' AND election_users.election_user_status=1","ORDER BY election_users.given_vote DESC");
                if(mysqli_num_rows($voting_option_data)>0){

                  $count5 = $d->sum_data("given_vote", "election_users", "election_id='$election_id'");
                  $row = mysqli_fetch_array($count5);
                  $totalVoting = $row['SUM(given_vote)'];
                  $isTieinElection = false;
                  $maxVotes = 0;
                    $i=1;
                    while($data_voting_option_list=mysqli_fetch_array($voting_option_data)) {
                        $i1 =$i++;
                        $result = array();

                        if($maxVotes==0){
                          $maxVotes =$data_voting_option_list['given_vote'];
                        } else if($maxVotes == $data_voting_option_list['given_vote'] ){
                          $isTieinElection = true;
                        } 


                        $result["given_vote"]=$data_voting_option_list['given_vote'];
                        $result["option_name"]=$data_voting_option_list['user_full_name']."  (".html_entity_decode($data_voting_option_list['company_name']).')';
                        
                        
                        $votingPer = $data_voting_option_list['given_vote'];

                        if($votingPer != 0){
                          $result["votingPer"] = number_format(100 * $votingPer / $totalVoting);
                        }else{
                          $result["votingPer"]="0";
                        }

                        array_push($response["result"], $result); 
                    }
                   $tem00=true;
        
                }else {
                  $tem00 = false;
                }

                
              if(mysqli_num_rows($nq)>0){
                  $count5 = $d->sum_data("given_vote", "election_users", "election_id='$election_id'");
                  $row = mysqli_fetch_array($count5);
                  $totalVoting = $row['SUM(given_vote)'];

                   $result = array();

                    $result["given_vote"]=$notData['given_vote'];
                    $result["option_name"]="NOTA";
                    
                    $votingPer = $notData['given_vote'];

                    if($votingPer != 0){
                      $result["votingPer"] = number_format(100 * $votingPer / $totalVoting);
                    }else{
                      $result["votingPer"]="0";
                    }

                    array_push($response["result"], $result);

                $tem11=true;
              }else {
                $tem11 = false;
              }

                if ($tem00==true || $tem11==true) {
                    $response["totalVoting"] = $totalVoting;
                    $response["election_tie"]=$isTieinElection;
                    $response["message"]="Get Voting result success.";
                    $response["status"]="200";
                    echo json_encode($response);
                } else{

                    $response["message"]="Something Wrong...!!";
                    $response["status"]="201";
                    echo json_encode($response);
        
                }
   }else{

         $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);

    }
  }

   else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>