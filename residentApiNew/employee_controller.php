    <?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    $response = array();
    extract(array_map("test_input" , $_POST));
    
    if($_POST['getBranchList']=="getBranchList" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){
        $response["branch_list"] = array();

        $q = $d->select("block_master","society_id='$society_id'");
        
        if (mysqli_num_rows($q) > 0) {
            
            while ($bdata = mysqli_fetch_array($q)) {
                $branch_list = array();
                $branch_list["block_id"]=$bdata['block_id'];
                $branch_list["society_id"]=$bdata['society_id'];
                $branch_list["block_name"]=$bdata['block_name'];
                 array_push($response["branch_list"], $branch_list);
            }
            
            $response["message"] = "Branch Get success";
            $response["status"] = "200";
            echo json_encode($response);
        } else {
            $response["message"] = "Faild to get List...!!";
            $response["status"] = "201";
            echo json_encode($response);
        }

    }else if($_POST['getDepartments']=="getDepartments" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

        $totalPendingMember=  $d->count_data_direct("user_id","users_master","delete_status=0 AND society_id='$society_id' AND user_status=0 AND member_status = 0");

        $response["total_pending_members"]=$totalPendingMember.'';

        $response["recent_user"] = array();

        $qr = $d->select("users_recent_view,users_master","users_recent_view.user_id=users_master.user_id AND users_recent_view.society_id='$society_id' AND users_recent_view.my_id='$user_id' ","ORDER BY users_recent_view.view_time DESC LIMIT 3");

        while ($rdata = mysqli_fetch_array($qr)) {

            $recent_user = array();
            $recent_user["user_id"]=$rdata['user_id'];
            $recent_user["society_id"]=$rdata['society_id'];
            $recent_user["user_first_name"]=$rdata['user_first_name'];
            $recent_user["user_last_name"]=$rdata['user_last_name'];
            $recent_user["user_full_name"]=$rdata['user_first_name'].' '.$rdata['user_last_name'];

            if ($rdata['user_profile_pic'] != '') {
                $recent_user["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $rdata['user_profile_pic'];
            } else {
                $recent_user["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
            }
            
            array_push($response["recent_user"], $recent_user);
        }

        $floor_data=$d->select("floors_master","block_id ='$block_id'","ORDER BY floor_sort ASC");

        if (mysqli_num_rows($floor_data) > 0) {
            // code...

            $response["departments"] = array();

            while($data_floor_list=mysqli_fetch_array($floor_data)) {

                $departments = array(); 

                $departments["floor_id"]=$data_floor_list['floor_id'];
                $floor_id=$data_floor_list['floor_id'];
                $departments["society_id"]=$data_floor_list['society_id'];
                $departments["block_id"]=$data_floor_list['block_id'];
                $departments["department_name"]=$data_floor_list['floor_name'];

                if ($floor_id==$my_floor_id) {
                    $departments["is_my_department"] = true;
                } else {
                    $departments["is_my_department"] = false;
                }

                array_push($response["departments"], $departments);
            }
        }

      
        $response["message"]="No data available";
        $response["status"]="200";
        echo json_encode($response);
    }else if($_POST['getMembersByDepartment']=="getMembersByDepartment" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){


        $unit_data=$d->select("unit_master,users_master","users_master.unit_id=unit_master.unit_id AND unit_master.floor_id ='$floor_id' AND unit_master.society_id ='$society_id' AND users_master.delete_status=0  AND users_master.society_id ='$society_id' AND users_master.user_status=1  $appendQuery","ORDER BY user_sort ASC");

        if (mysqli_num_rows($unit_data) > 0) {

            $response["employees"] = array();

            while($data_units_list=mysqli_fetch_array($unit_data)) {
                             
                $employees = array(); 
     
                $user_full_name= $data_units_list['user_full_name'];

                $units_id=$data_units_list['unit_id'];

                $employees["user_id"]=$data_units_list['user_id'].'';
                $employees["user_first_name"]=$data_units_list['user_first_name'].'';
                $employees["user_last_name"]=$data_units_list['user_last_name'].'';
                $employees["user_mobile"]=$data_units_list['user_mobile'].'';
                $employees["user_full_name"]=html_entity_decode($user_full_name).'';
                $employees["society_id"]=$data_units_list['society_id'];
                $employees["designation"]=$data_units_list['user_designation'];
                
                if ($data_units_list['user_profile_pic'] != '') {
                    $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                } else {
                    $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }

                array_push($response["employees"], $employees);
            }

            $response["message"]="Employee list";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{
            $response["message"]="No Employees Found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>