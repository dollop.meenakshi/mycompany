<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb && $auth_check=='true') {
        $response = array();
        extract(array_map("test_input", $_POST));
        $temDate = date("Y-m-d h:i A");
        
        $open = $xml->string->open;
        $closed = $xml->string->closed;
        $reopen = $xml->string->reopen;
        $reply = $xml->string->reply;
        $in_progress = $xml->string->in_progress;

        if ($_POST['getComplain'] == "getComplain" && $unit_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            $add_complaint = "false";
            $blcok_message = "";
            $aq = $d->selectRow("unit_id,block_message","complains_block_master","unit_id='$unit_id'");
            if (mysqli_num_rows($aq)>0) {
                $blockData= mysqli_fetch_array($aq);
                $blcok_message = html_entity_decode($blockData['block_message']);
                $add_complaint = "true";
            }

            $qcomplain = $d->select("complains_master,complaint_category", "complains_master.complaint_category=complaint_category.complaint_category_id AND complains_master.society_id='$society_id' AND complains_master.unit_id='$unit_id' AND complains_master.flag_delete='0' AND complains_master.user_id='$user_id'", "ORDER BY complains_master.complain_id DESC LIMIT 100");
            if (mysqli_num_rows($qcomplain) > 0) {
                $response["complain"] = array();

                while ($data_complain_list = mysqli_fetch_array($qcomplain)) {

                    $complain = array();

                    $complain["complain_id"] = $data_complain_list['complain_id'];
                    $complain_id = $data_complain_list['complain_id'];
                    $complain["society_id"] = $data_complain_list['society_id'];
                    $complain["complain_no"] = 'CN'.$data_complain_list['complain_id'];
                    $complain["complaint_category"] = $data_complain_list['complaint_category'];
                    $complain["complaint_category_view"] = html_entity_decode($data_complain_list['category_name']);
                    $complain["compalain_title"] = html_entity_decode($data_complain_list['compalain_title']);
                    $complain["complain_description"] = html_entity_decode($data_complain_list['complain_description']);
                    $complain["complain_date"] = date("d M Y h:i A", strtotime($data_complain_list['complain_date']));
                    $complain["complain_status"] = $data_complain_list['complain_status'];
                    if ($data_complain_list['complain_status']==0) {
                        $complain["complain_status_view"] = "Open";
                    } elseif ($data_complain_list['complain_status']==1) { 
                        $complain["complain_status_view"] = "Closed";
                    } elseif ($data_complain_list['complain_status']==2) { 
                        $complain["complain_status_view"] = "Re Open";
                    } elseif ($data_complain_list['complain_status']==3) { 
                        $complain["complain_status_view"] = "In Progress";
                    } else {
                        $complain["complain_status_view"] = "Deleted";
                    }
                    $complain["complain_assing_to"] = $data_complain_list['complain_assing_to'];

                    $qsub = $d->select("complains_track_master", "complain_id='$complain_id' AND society_id='$society_id'", "ORDER BY complain_id DESC");
                    if (mysqli_num_rows($qsub) > 0) {

                        $complain["track"] = array();
                        while ($data_complain_track = mysqli_fetch_array($qsub)) {

                            $track = array();
                            $track["complains_track_id"] = $data_complain_track['complains_track_id'];
                            $track["complains_track_by"] = $data_complain_track['complains_track_by'];
                            $track["complains_track_msg"] = html_entity_decode($data_complain_track['complains_track_msg']);

                            if ($data_complain_track['complains_track_voice'] != '') {
                                $track["complains_track_voice"] = $base_url . "img/complain/" . $data_complain_track['complains_track_voice'];
                            } else {
                                $track["complains_track_voice"] = "";
                            }

                            if ($data_complain_track['complains_track_img'] != '') {
                                $track["complains_track_img"] = $base_url . "img/complain/" . $data_complain_track['complains_track_img'];
                            } else {
                                $track["complains_track_img"] = "";
                            }
                            $track["complain_id"] = $data_complain_track['complain_id'];
                            $track["admin_id"] = $data_complain_track['admin_id'];
                            $track["complains_track_date_time"] = $data_complain_track['complains_track_date_time'];

                            $track["complain_photo_old"] = $data_complain_track['complain_photo'];
                            $track["complaint_voice_old"] = $data_complain_track['complaint_voice'] . '';

                            array_push($complain["track"], $track);
                        }
                    }

                    if ($data_complain_list['complain_review_msg_date'] != '') {
                        $complain["complain_review_msg_date"] = date("d M Y h:i A", strtotime($data_complain_list['complain_review_msg_date']));
                    }
                    $complain["complain_review_msg"] = html_entity_decode($data_complain_list['complain_review_msg']);
                    $complain["rating_star"] = $data_complain_list['rating_star'].'';

                    $complain["feedback_msg"] = html_entity_decode($data_complain_list['feedback_msg']) . '';

                    array_push($response["complain"], $complain);
                }
                $response["block_status"] = $add_complaint;
                $response["block_message"] = $blcok_message;
                $response["message"] = "Get complaint success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["block_status"] = $add_complaint;
                $response["block_message"] = $blcok_message;
                $response["message"] = "No complaint Found.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getComplainNew'] == "getComplainNew" && $unit_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            $add_complaint = "false";
            $blcok_message = "";
            $aq = $d->selectRow("unit_id,block_message","complains_block_master","unit_id='$unit_id'");
            if (mysqli_num_rows($aq)>0) {
                $blockData= mysqli_fetch_array($aq);
                $blcok_message = html_entity_decode($blockData['block_message']);
                $add_complaint = "true";
            }

            $qcomplain = $d->select("complains_master,complaint_category,users_master", "users_master.user_id=complains_master.user_id AND complains_master.complaint_category=complaint_category.complaint_category_id AND complains_master.society_id='$society_id' AND complains_master.unit_id='$unit_id' AND complains_master.flag_delete='0' ", "ORDER BY complains_master.complain_id DESC LIMIT 100");
            if (mysqli_num_rows($qcomplain) > 0) {
                $response["complain"] = array();

                while ($data_complain_list = mysqli_fetch_array($qcomplain)) {

                    $complain = array();

                    $complain["complain_id"] = $data_complain_list['complain_id'];
                    $complain_id = $data_complain_list['complain_id'];
                    $complain["user_id"] = $data_complain_list['user_id'];
                    $complain["society_id"] = $data_complain_list['society_id'];
                    $complain["complain_no"] = 'CN'.$data_complain_list['complain_id'];
                    $complain["complaint_category"] = $data_complain_list['complaint_category'];
                    $complain["complaint_category_view"] = html_entity_decode($data_complain_list['category_name']);
                    $complain["compalain_title"] = html_entity_decode($data_complain_list['compalain_title']);
                    $complain["complain_description"] = html_entity_decode($data_complain_list['complain_description']);
                    $complain["complain_date"] = date("d M y h:i A", strtotime($data_complain_list['complain_date']));
                    $complain["complain_status"] = $data_complain_list['complain_status'];

                    $open = $xml->string->open;
                    $closed = $xml->string->closed;
                    $reopen = $xml->string->reopen;
                    $in_progress = $xml->string->in_progress;
                    $deleted = $xml->string->deleted;

                    if ($data_complain_list['complain_status']==0) {
                        $complain["complain_status_view"] = "$open";
                    } elseif ($data_complain_list['complain_status']==1) { 
                        $complain["complain_status_view"] = "$closed";
                    } elseif ($data_complain_list['complain_status']==2) { 
                        $complain["complain_status_view"] = "$reopen";
                    } elseif ($data_complain_list['complain_status']==3) { 
                        $complain["complain_status_view"] = "$in_progress";
                    } else {
                        $complain["complain_status_view"] = "$deleted";
                    }
                    $complain["complain_assing_to"] = $data_complain_list['complain_assing_to'];

                    $qsub = $d->select("complains_track_master", "complain_id='$complain_id' AND society_id='$society_id'", "ORDER BY complain_id DESC");
                    if (mysqli_num_rows($qsub) > 0) {

                        $complain["track"] = array();
                        while ($data_complain_track = mysqli_fetch_array($qsub)) {

                            $track = array();
                            $track["complains_track_id"] = $data_complain_track['complains_track_id'];
                            $track["complains_track_by"] = $data_complain_track['complains_track_by'];
                            $track["complains_track_msg"] = html_entity_decode($data_complain_track['complains_track_msg']);

                            if ($data_complain_track['complains_track_voice'] != '') {
                                $track["complains_track_voice"] = $base_url . "img/complain/" . $data_complain_track['complains_track_voice'];
                            } else {
                                $track["complains_track_voice"] = "";
                            }

                            if ($data_complain_track['complains_track_img'] != '') {
                                $track["complains_track_img"] = $base_url . "img/complain/" . $data_complain_track['complains_track_img'];
                            } else {
                                $track["complains_track_img"] = "";
                            }
                            $track["complain_id"] = $data_complain_track['complain_id'];
                            $track["admin_id"] = $data_complain_track['admin_id'];
                            $track["complains_track_date_time"] = $data_complain_track['complains_track_date_time'];

                            $track["complain_photo_old"] = $data_complain_track['complain_photo'];
                            $track["complaint_voice_old"] = $data_complain_track['complaint_voice'] . '';

                            array_push($complain["track"], $track);
                        }
                    }

                    if ($data_complain_list['complain_review_msg_date'] != '') {
                        $complain["complain_review_msg_date"] = date("d M Y h:i A", strtotime($data_complain_list['complain_review_msg_date']));
                    }
                    $complain["complain_review_msg"] = html_entity_decode($data_complain_list['complain_review_msg']);
                    $complain["rating_star"] = $data_complain_list['rating_star'].'';

                    $complain["feedback_msg"] = html_entity_decode($data_complain_list['feedback_msg']) . '';

                    array_push($response["complain"], $complain);
                }
                $response["block_status"] = $add_complaint;
                $response["block_message"] = $blcok_message;
                $response["message"] = "$datafoundMsg";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["block_status"] = $add_complaint;
                $response["block_message"] = $blcok_message;
                $response["message"] = "$noDatafoundMsg";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['addComplainNew'] == "addComplainNew" && $unit_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            if ($user_id!='' && $user_id!=0) {
                 $quc=$d->select("users_master","user_id='$user_id' AND is_defaulter=1");
                 if (mysqli_num_rows($quc)>0) {
                    $response["message"] = "Access Denied for a Register new complaint";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                 }

            } 

            $aq = $d->selectRow("unit_id","complains_block_master","unit_id='$unit_id'");
            if (mysqli_num_rows($aq)>0) {

                $response["message"] = "Access Denied for a Register new complaint";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            
           

            $file11 = $_FILES["complaint_voice"]["tmp_name"];
            if (file_exists($file11)) {
                // $complaint_voice = $_FILES['complaint_voice']['name'];
                $complaint_voice = rand() . "." . "mp3";
                $target = '../img/complain/' . $complaint_voice;
                move_uploaded_file($_FILES['complaint_voice']['tmp_name'], $target);
            } else {
                $complaint_voice = '';
            }

            $uploadedFile = $_FILES["complain_photo"]["tmp_name"];
            $ext = pathinfo($_FILES['complain_photo']['name'], PATHINFO_EXTENSION);
            if (file_exists($uploadedFile)) {

                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $user_id;
                $dirPath = "../img/complain/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1000) {
                    $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }



                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "_complain." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "_complain." . $ext);
                        break;

                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "_complain." . $ext);
                        break;

                    default:

                        break;
                }
                $complain_photo = $newFileName . "_complain." . $ext;

                $notiUrl = $base_url . 'img/complain/' . $complain_photo;
            } else {
                $complain_photo = '';
                $notiUrl = "";
            }

            

            $m->set_data('society_id', $society_id);
            $m->set_data('compalain_title', $compalain_title);
            $m->set_data('complain_description', $complain_description);
            $m->set_data('complaint_category', $complaint_category);
            $m->set_data('complaint_voice', $complaint_voice);
            $m->set_data('complain_date', $temDate);
            $m->set_data('complain_status', $complain_status);
            $m->set_data('complain_assing_to', $complain_assing_to);
            $m->set_data('unit_id', $unit_id);
            $m->set_data('user_id', $user_id);


            $a = array(
                'society_id' => $m->get_data('society_id'),
                'unit_id' => $m->get_data('unit_id'),
                'user_id' => $m->get_data('user_id'),
                'complaint_category' => $m->get_data('complaint_category'),
                'compalain_title' => $m->get_data('compalain_title'),
                'complain_description' => $m->get_data('complain_description'),
                'complain_date' => $m->get_data('complain_date'),
                'complain_status' => $m->get_data('complain_status'),
                'complain_assing_to' => $m->get_data('complain_assing_to')
            );

            $quserDataUpdate = $d->insert("complains_master", $a);
            $complain_id = $con->insert_id;
            $complain_no = "CN" . $complain_id;
            $a1 = array(
                'complains_track_by' => 0,
                'society_id' => $m->get_data('society_id'),
                'complains_track_msg' => $m->get_data('complain_description'),
                'complains_track_img' => $complain_photo,
                'complains_track_voice' => $complaint_voice,
                'complain_id' => $complain_id,
                'complains_track_date_time' => $m->get_data('complain_date'),
                'complaint_status_view' => "Open",
            );
            $d->insert("complains_track_master", $a1);
            if ($quserDataUpdate == TRUE) {

                // find block
                $qq=$d->selectRow("block_id","users_master","user_id='$user_id'");
                $userData = mysqli_fetch_array($qq);
                $block_id= $userData['user_id'];
                
                // $qSecretaryToken = $d->select("bms_admin_master,role_master", "role_master.role_id=bms_admin_master.role_id AND bms_admin_master.society_id='$society_id' ");

                $qSecretaryToken=$d->selectAdmin("ab.block_id='$block_id'");


                while ($data_Secretary = mysqli_fetch_array($qSecretaryToken)) {

                    if ($data_Secretary['complaint_category_id'] != '') {

                        $comCatIdAry = explode(",", $data_Secretary['complaint_category_id']);
                        if (in_array($complaint_category, $comCatIdAry)) {
                            $sos_Secretary_token = $data_Secretary['token'];
                            if ($data_Secretary['device'] == 'android') {
                                $nAdmin->noti_new($society_id,$notiUrl, $sos_Secretary_token, "Complaint Added by " . $user_name, $compalain_title, 'complaints');
                            } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl, $sos_Secretary_token, "Complaint Added by " . $user_name, $compalain_title, 'complaints');
                            }
                        }
                    } else {
                        $sos_Secretary_token = $data_Secretary['token'];
                        if ($data_Secretary['device'] == 'android') {
                            $nAdmin->noti_new($society_id,$notiUrl, $sos_Secretary_token, "Complaint Added by " . $user_name, $compalain_title, 'complaints');
                        } else {
                            $nAdmin->noti_ios_new($society_id,$notiUrl, $sos_Secretary_token, "Complaint Added by " . $user_name, $compalain_title, 'complaints');
                        }
                    }

                    $notiAry = array(
                        'society_id' => $society_id,
                        'admin_id' => $data_Secretary['admin_id'],
                        'notification_tittle' => "Complaint " . $complain_no . " Added by " . $user_name,
                        'notification_description' => $compalain_title,
                        'notifiaction_date' => date('Y-m-d H:i'),
                        'notification_action' => 'Complain',
                        'admin_click_action' => 'complaints',
                        'notification_logo'=>'complain.png',
                    );
                    $d->insert("admin_notification", $notiAry);
                }

                 $d->insert_myactivity($user_id,"$society_id","0","$user_name","Register new complaint ","complain.png");
                 
                 $ccq=$d->select("complaint_email_receipt","active_status=0 AND society_id='$society_id'","");
                   if(mysqli_num_rows($ccq)>0) {
                      $emailAray = array();
                       while($row=mysqli_fetch_array($ccq))
                       {
                          array_push($emailAray, $row['email_id']);
                       }
                       $qc=$d->selectRow("category_name","complaint_category","complaint_category_id='$complaint_category'");
                       $comName= mysqli_fetch_array($qc);
                       $category_name = $comName['category_name'];

                       $sq=$d->selectRow("society_name,currency,society_address,city_name,society_pincode","society_master","society_id='$society_id'");
                        $societyData=mysqli_fetch_array($sq);
                        $society_name= $societyData['society_name'];
                        $society_address= $societyData['society_address'].', '.$societyData['city_name'].' ('.$societyData['society_pincode'].')';
                       $thanksName = $complain_assing_to;
                       $to = $emailAray;
                       $dep_bra = $complain_assing_to;
                       $subject= "New Complaint added by $complain_assing_to (CN$complain_id) in $category_name Category";
                       include '../apAdmin/mail/complaintMail.php';
                       include '../apAdmin/mail.php';
                   }


                $response["message"] = "Complaint Register Successfully ";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No Complaint Added.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getFeedback'] == "getFeedback" && filter_var($complain_id, FILTER_VALIDATE_INT) == true) {

            $a = array('feedback_msg' => $feedback_msg,
                    'rating_star' => $rating_star);

            $quserDataUpdate = $d->update("complains_master", $a, "complain_id='$complain_id'");

            if ($quserDataUpdate == TRUE) {
                 $d->insert_myactivity($user_id,"$society_id","0","$user_name","Complaint feedback send","complain.png");
                
                $response["message"] = "Thank you for Feedback";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Something Wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['deleteComplain'] == "deleteComplain" && filter_var($complain_id, FILTER_VALIDATE_INT) == true) {

            $a = array('flag_delete' => 1);

            $quserDataUpdate = $d->update("complains_master", $a, "complain_id='$complain_id'");

            if ($quserDataUpdate == TRUE) {
                 $d->insert_myactivity($user_id,"$society_id","0","$user_name","Complaint deleted","complain.png");
                
                $response["message"] = "Deleted Successfully";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "deletion failed.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['editComplainNew'] == "editComplainNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {

            $file11 = $_FILES["complaint_voice"]["tmp_name"];
            if (file_exists($file11)) {
                // $complaint_voice = $_FILES['complaint_voice']['name'];
                $complaint_voice = rand() . "." . "mp3";
                $target = '../img/complain/' . $complaint_voice;
                move_uploaded_file($_FILES['complaint_voice']['tmp_name'], $target);
            } else {
                // $complaint_voice = $complaint_voice_old;
            }


            $uploadedFile = $_FILES["complain_photo"]["tmp_name"];
            if (file_exists($uploadedFile)) {
                $ext = pathinfo($_FILES['complain_photo']['name'], PATHINFO_EXTENSION);

                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $user_id;
                $dirPath = "../img/complain/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1000) {
                    $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }



                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "_complain." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "_complain." . $ext);
                        break;

                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "_complain." . $ext);
                        break;

                    default:

                        break;
                }
                $complain_photo = $newFileName . "_complain." . $ext;

                $notiUrl = $base_url . 'img/complain/' . $complain_photo;
            } else {
                // $complain_photo = $complain_photo_old;
                $notiUrl = "";
            }

            switch ($complaint_status) {
                    case '0':
                        $complain_status_view="Reply";
                        break;
                    case '1':
                        $complain_status_view="Closed";
                        break;
                    case '2':
                        $complain_status_view="Reopen";
                        break;
                    default:
                         $complain_status_view="Reply";
                        break;
                }
            $m->set_data('society_id', $society_id);
            $m->set_data('compalain_title', $compalain_title);
            $m->set_data('complain_description', $complain_description);
            $m->set_data('complaint_category', $complaint_category);
            $m->set_data('complaint_reply', $complaint_reply);
            $m->set_data('complaint_voice', $complaint_voice);
            if ($complaint_status!='') {
                $m->set_data('complain_status',$complaint_status);
            } else {
                $m->set_data('complain_status','2');
            }
            $m->set_data('complain_assing_to', $complain_assing_to);
            $m->set_data('unit_id', $unit_id);
            $m->set_data('user_id', $user_id);

            if ($complain_status_view=="Reply") {
                $a = array(
                'society_id' => $m->get_data('society_id'),
                ); 
            }else {
                $a = array(
                    'society_id' => $m->get_data('society_id'),
                    'complain_status' => $m->get_data('complain_status')
                );

            }

            $a1 = array(
                'complains_track_by' => 0,
                'society_id' => $m->get_data('society_id'),
                'complains_track_msg' => $m->get_data('complain_description'),
                'complains_track_img' => $complain_photo,
                'complains_track_voice' => $complaint_voice,
                'complain_id' => $complain_id,
                'complains_track_date_time' => $temDate,
                'complaint_status_view' => $complain_status_view,

            );
            $quserDataUpdate = $d->update("complains_master", $a, "complain_id='$complain_id'");
            $d->insert("complains_track_master", $a1);

            if ($quserDataUpdate == TRUE) {

                $qq=$d->selectRow("block_id","users_master","user_id='$user_id'");
                $userData = mysqli_fetch_array($qq);
                $block_id= $userData['user_id'];
                
                // $qSecretaryToken = $d->select("bms_admin_master,role_master", "role_master.role_id=bms_admin_master.role_id AND bms_admin_master.society_id='$society_id' ");

                $qSecretaryToken=$d->selectAdmin("ab.block_id='$block_id'");

                while ($data_Secretary = mysqli_fetch_array($qSecretaryToken)) {

                    if ($data_Secretary['complaint_category_id'] != '') {

                        $comCatIdAry = explode(",", $data_Secretary['complaint_category_id']);
                        if (in_array($complaint_category, $comCatIdAry)) {
                            $sos_Secretary_token = $data_Secretary['token'];
                            if ($data_Secretary['device'] == 'android') {
                                $nAdmin->noti_new($society_id,$notiUrl, $sos_Secretary_token, "Complaint " . $complain_no . " $complain_status_view by " . $user_name, $compalain_title, 'complaints');
                            } else {
                                $nAdmin->noti_ios_new($society_id,$notiUrl, $sos_Secretary_token, "Complaint " . $complain_no . " $complain_status_view by " . $user_name, $compalain_title, 'complaints');
                            }
                        }
                    } else {
                        $sos_Secretary_token = $data_Secretary['token'];
                        if ($data_Secretary['device'] == 'android') {
                            $nAdmin->noti_new($society_id,$notiUrl, $sos_Secretary_token, "Complaint " . $complain_no . " $complain_status_view by " . $user_name, $compalain_title, 'complaints');
                        } else {
                            $nAdmin->noti_ios_new($society_id,$notiUrl, $sos_Secretary_token, "Complaint " . $complain_no . " $complain_status_view by " . $user_name, $compalain_title, 'complaints');
                        }
                    }

                    $notiAry = array(
                        'society_id' => $society_id,
                        'admin_id' => $data_Secretary['admin_id'],
                        'notification_tittle' => "Complaint $complain_status_view by" . $user_name,
                        'notification_description' => $compalain_title,
                        'notifiaction_date' => date('Y-m-d H:i'),
                        'notification_action' => 'Complain',
                        'admin_click_action' => 'complaints',
                        'notification_logo'=>'complain.png',
                    );
                    $d->insert("admin_notification", $notiAry);
                }
                $d->insert_myactivity($user_id,"$society_id","0","$user_name","Complaint $complain_status_view by you","complain.png");
                    
                    
                $response["message"] = "Complain  $complain_status_view Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "update failed.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getComplainDetail'] == "getComplainDetail" && $complain_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($complain_id, FILTER_VALIDATE_INT) == true) {
            
            $qsub = $d->select("complains_master,complains_track_master", "complains_track_master.complain_id='$complain_id' AND complains_master.complain_id = complains_track_master.complain_id", "ORDER BY complains_track_master.complain_id DESC");
            if (mysqli_num_rows($qsub) > 0) {

                $complaint_category="";

                $response["track"] = array();

                 $tempPrvDate = "";

                while ($data_complain_track = mysqli_fetch_array($qsub)) {

                    $track = array();
                    $track["complains_track_id"] = $data_complain_track['complains_track_id'];
                    $track["complains_track_by"] = $data_complain_track['complains_track_by'];
                    if($data_complain_track['complains_track_by']==1){ 
                        $qad=$d->select("bms_admin_master","admin_id='$data_complain_track[admin_id]'");
                        $adminData=mysqli_fetch_array($qad);
                     $track["admin_name"] = $adminData['admin_name'];
                    } else {
                     $track["admin_name"] = "";
                    }
                    $track["complains_track_msg"] = html_entity_decode($data_complain_track['complains_track_msg']);

                    if ($data_complain_track['complains_track_voice'] != '') {
                        $track["complains_track_voice"] = $base_url . "img/complain/" . $data_complain_track['complains_track_voice'];
                    } else {
                        $track["complains_track_voice"] = "";
                    }
                    if ($data_complain_track['complains_track_img'] != '') {
                        $track["complains_track_img"] = $base_url . "img/complain/" . $data_complain_track['complains_track_img'];
                    } else {
                        $track["complains_track_img"] = "";
                    }
                    $track["complain_id"] = $data_complain_track['complain_id'];
                    $track["admin_id"] = $data_complain_track['admin_id'];
                    $track["complains_track_date_time"] =date("d M Y h:i A", strtotime($data_complain_track['complains_track_date_time'])); 
                    $chatDate = date("d M", strtotime($data_complain_track['complains_track_date_time']));
                    $complaint_category = $data_complain_track['complaint_category'];
                    $track["complaint_category"] = $data_complain_track['complaint_category'];
                    $track["complain_photo_old"] = $data_complain_track['complain_photo'];
                    $track["complaint_voice_old"] = $data_complain_track['complaint_voice'] . '';
                    
                    switch ($data_complain_track['complaint_status_view']) {
                        case 'Open':
                            $complaint_status_view  = $open;
                            $complaint_status_int  = "0";
                            break;
                        case 'Closed':
                            $complaint_status_view  = $closed;
                            $complaint_status_int  = "1";
                            break;
                        case 'Reopen':
                            $complaint_status_view  = $reopen;
                            $complaint_status_int  = "2";
                            break;
                        case 'In Progress':
                            $complaint_status_view  = $in_progress;
                            $complaint_status_int  = "3";
                            break;
                        case 'Reply':
                            $complaint_status_view  = $reply;
                            $complaint_status_int  = "4";
                            break;
                        default:
                           $complaint_status_view  = $reply;
                            $complaint_status_int  = "3";
                            break;
                    }

                    $track["complaint_status_view"] = $complaint_status_view.'';
                    $track["complaint_status_int"] = $complaint_status_int;

                    $track["isDate"] = false;



                    $qchek=$d->selectRow("device","users_master","user_id='$user_id'");
                    $userData=mysqli_fetch_array($qchek);
                    if ($userData['device']=="android") {
                    

                        if($tempPrvDate!=""){

                            if($tempPrvDate != $chatDate){
                                $tempPrvDate = $chatDate;
                                $chatD = array();
                                $chatD["msg_date"] = $tempPrvDate;
                                $chatD["isDate"] = true;
                                $chatD["msg_date_view"] = $chatDate;

                                array_push($response["track"], $chatD);
                            }

                        }else{
                            $tempPrvDate = $chatDate;
                            $chatD = array();
                            $chatD["msg_date"] = $tempPrvDate;
                            $chatD["isDate"] = true;
                            $chatD["msg_date_view"] = $chatDate;
                            array_push($response["track"], $chatD);
                        }
                    }
                    array_push($response["track"], $track);
                }

                    $qsub = $d->select("complains_master", "complain_id='$complain_id'");
                    $cData=mysqli_fetch_array($qsub);

                    $qsub111 = $d->select("complains_track_master,complains_master,society_master", "society_master.society_id=complains_master.society_id AND complains_master.complain_id = complains_track_master.complain_id  AND complains_track_master.complain_id='$complain_id' AND complains_master.complain_status=1 AND complains_track_master.complaint_status_view='Closed'","ORDER BY complains_track_master.complains_track_id DESC");
                    $cData11=mysqli_fetch_array($qsub111);


                    $cTime= date("Y-m-d H:i:s");    
                    // $d1= new DateTime($cData11['complains_track_date_time']); 
                    // $d2= new DateTime($cTime);
                    // $interval= $d1->diff($d2);
                    // $hoursDifrent=  ($interval->days * 24) + $interval->h;

                    $start_date = new DateTime($cData11['complains_track_date_time']);
                    $since_start = $start_date->diff(new DateTime($cTime));

                    $minutes = $since_start->days * 24 * 60;
                    $minutes += $since_start->h * 60;
                    $minutes += $since_start->i;
                    $minutes;

                    // if ($cData['complain_status']==1 && $hoursDifrent<=48) {
                    if ($cData['complain_status']==1 && $minutes<=$cData11['complaint_reopen_minutes']) {
                        $complainstatus=array("Reply"=>"0","Reopen"=>"2");
                    }else  if ($cData['complain_status']==1) {
                        $complainstatus=array("Reply"=>"0");
                    }else if ($cData['complain_status']==2) {
                        $complainstatus=array("Reply"=>"0","Close"=>"1");
                    } else if ($cData['complain_status']==0 || $cData['complain_status']) {
                        $complainstatus=array("Reply"=>"0","Close"=>"1");
                    } else {
                        $complainstatus=array("Reply"=>"0","Reopen"=>"2","Close"=>"1");
                    }
 
                    $response["complain_status_array"] = array();
                     
                    foreach ($complainstatus as $key => $value) {
                         $complain_status_array = array();
                         $complain_status_array["status"]=$key;
                         $complain_status_array["value"]=$value;
                         array_push($response["complain_status_array"], $complain_status_array);
                     }

                $response["complaint_category"] = $complaint_category;
                $response["message"] = "Get complaint success.";
                $response["hoursDifrent"] = $minutes;
                $response["last_close_date"] = $cData11['complains_track_date_time'];
                $response["audio_duration"] = 30000;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No complaint Found.";
                 $response["audio_duration"] = 30000;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getComplainDetail'] == "getComplainDetail_iOS" && $complain_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($complain_id, FILTER_VALIDATE_INT) == true) {

            $qsub = $d->select("complains_master,complains_track_master", "complains_track_master.complain_id='$complain_id' AND complains_master.complain_id = complains_track_master.complain_id", "ORDER BY complains_track_master.complain_id DESC");
            if (mysqli_num_rows($qsub) > 0) {

                $complaint_category="";

                $response["track"] = array();

                 $tempPrvDate = "";

                while ($data_complain_track = mysqli_fetch_array($qsub)) {

                    $track = array();
                    $track["complains_track_id"] = $data_complain_track['complains_track_id'];
                    $track["complains_track_by"] = $data_complain_track['complains_track_by'];
                    if($data_complain_track['complains_track_by']==1){ 
                        $qad=$d->select("bms_admin_master","admin_id='$data_complain_track[admin_id]'");
                        $adminData=mysqli_fetch_array($qad);
                     $track["admin_name"] = $adminData['admin_name'];
                    } else {
                     $track["admin_name"] = "";
                    }
                    $track["complains_track_msg"] = html_entity_decode($data_complain_track['complains_track_msg']);

                    if ($data_complain_track['complains_track_voice'] != '') {
                        $track["complains_track_voice"] = $base_url . "img/complain/" . $data_complain_track['complains_track_voice'];
                    } else {
                        $track["complains_track_voice"] = "";
                    }
                    if ($data_complain_track['complains_track_img'] != '') {
                        $track["complains_track_img"] = $base_url . "img/complain/" . $data_complain_track['complains_track_img'];
                    } else {
                        $track["complains_track_img"] = "";
                    }
                    $track["complain_id"] = $data_complain_track['complain_id'];
                    $track["admin_id"] = $data_complain_track['admin_id'];
                    $track["complains_track_date_time"] =date("d M Y h:i A", strtotime($data_complain_track['complains_track_date_time']));
                    $chatDate = date("d M", strtotime($data_complain_track['complains_track_date_time']));
                    $complaint_category = $data_complain_track['complaint_category'];
                    $track["complaint_category"] = $data_complain_track['complaint_category'];
                    $track["complain_photo_old"] = $data_complain_track['complain_photo'];
                    $track["complaint_voice_old"] = $data_complain_track['complaint_voice'] . '';
                    
                    switch ($data_complain_track['complaint_status_view']) {
                        case 'Open':
                            $complaint_status_view  = $open;
                            $complaint_status_int  = "0";
                            break;
                        case 'Closed':
                            $complaint_status_view  = $closed;
                            $complaint_status_int  = "1";
                            break;
                        case 'Reopen':
                            $complaint_status_view  = $reopen;
                            $complaint_status_int  = "2";
                            break;
                        case 'In Progress':
                            $complaint_status_view  = $in_progress;
                            $complaint_status_int  = "3";
                            break;
                        case 'Reply':
                            $complaint_status_view  = $reply;
                            $complaint_status_int  = "4";
                            break;
                        default:
                           $complaint_status_view  = $reply;
                            $complaint_status_int  = "3";
                            break;
                    }

                    $track["complaint_status_view"] = $complaint_status_view.'';
                    $track["complaint_status_int"] = $complaint_status_int;
                    $track["isDate"] = false;

                   

                        if($tempPrvDate!=""){

                            if($tempPrvDate != $chatDate){
                                $tempPrvDate = $chatDate;
                                $chatD = array();
                                $chatD["msg_date"] = $tempPrvDate;
                                $chatD["isDate"] = true;
                                $chatD["msg_date_view"] = $chatDate;

                                array_push($response["track"], $chatD);
                            }

                        }else{
                            $tempPrvDate = $chatDate;
                            $chatD = array();
                            $chatD["msg_date"] = $tempPrvDate;
                            $chatD["isDate"] = true;
                            $chatD["msg_date_view"] = $chatDate;
                            array_push($response["track"], $chatD);
                        }
                    array_push($response["track"], $track);
                }

                  $qsub = $d->select("complains_master", "complain_id='$complain_id'");
                    $cData=mysqli_fetch_array($qsub);

                    $qsub111 = $d->select("complains_track_master,complains_master", "complains_master.complain_id = complains_track_master.complain_id  AND complains_track_master.complain_id='$complain_id' AND complains_master.complain_status=1 AND complains_track_master.complaint_status_view='Closed'","ORDER BY complains_track_master.complains_track_id DESC");
                    $cData11=mysqli_fetch_array($qsub111);


                    $cTime= date("Y-m-d H:i:s");    
                    // $d1= new DateTime($cData11['complains_track_date_time']); 
                    // $d2= new DateTime($cTime);
                    // $interval= $d1->diff($d2);
                    // $hoursDifrent=  ($interval->days * 24) + $interval->h;

                    $start_date = new DateTime($cData11['complains_track_date_time']);
                    $since_start = $start_date->diff(new DateTime($cTime));

                    $minutes = $since_start->days * 24 * 60;
                    $minutes += $since_start->h * 60;
                    $minutes += $since_start->i;
                    $minutes;

                    // if ($cData['complain_status']==1 && $hoursDifrent<=48) {
                    if ($cData['complain_status']==1 && $minutes<=$cData11['complaint_reopen_minutes']) {
                        $complainstatus=array("Reply"=>"0","Reopen"=>"2");
                    }else  if ($cData['complain_status']==1) {
                        $complainstatus=array("Reply"=>"0");
                    }else if ($cData['complain_status']==2) {
                        $complainstatus=array("Reply"=>"0","Close"=>"1");
                    } else if ($cData['complain_status']==0 || $cData['complain_status']) {
                        $complainstatus=array("Reply"=>"0","Close"=>"1");
                    } else {
                        $complainstatus=array("Reply"=>"0","Reopen"=>"2","Close"=>"1");
                    }
 
                    $response["complain_status_array"] = array();
                     
                    foreach ($complainstatus as $key => $value) {
                         $complain_status_array = array();
                         $complain_status_array["status"]=$key;
                         $complain_status_array["value"]=$value;
                         array_push($response["complain_status_array"], $complain_status_array);
                     }
                     
                $response["complaint_category"] = $complaint_category;
                $response["message"] = "Get complaint success.";
                $response["audio_duration"] = 30000;
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No complaint Found.";
                 $response["audio_duration"] = 30000;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getComplainCategory'] == "getComplainCategory") {


            $qcomplain = $d->select("complaint_category", "active_status=0");
            if (mysqli_num_rows($qcomplain) > 0) {
                $response["complain_category"] = array();

                while ($data = mysqli_fetch_array($qcomplain)) {

                    $complain_category = array();
                    $complain_category["complaint_category_id"] = $data['complaint_category_id'];
                    $complain_category["category_name"] = $data['category_name'];

                    array_push($response["complain_category"], $complain_category);
                }
                $response["message"] = "Get Complaint Category Success.";
                $response["audio_duration"] = 30000; //30 second
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No Categoty Found.";
                $response["audio_duration"] = 30000;
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
