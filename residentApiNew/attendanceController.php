<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    //if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");
        
        if(isset($year) && $year !=""){
            if(isset($month) && $month !=""){
                $startDate=date("$year-$month-01");
                $lastDate=date("$year-$month-t");
                   $textdt=date("01-$month-$year");
            }
            else
            {
                $startDate=date("$year-m-01");
                $lastDate=date("$year-m-t");
                   $textdt=date("01-m-$year");
            }
            
           
        }
        else if(isset($month) && $month !="")
            {
                 $startDate=date("Y-$month-01");
                $lastDate=date("Y-$month-t");
                $textdt=date("01-$month-Y");

                
            }
        else
        {
             $startDate=date("Y-m-01");
            $lastDate=date("Y-m-t");
             $textdt=date("01-m-Y");
        }
      /*  print_r( $startDate);
       print_r( $lastDate);
       die; */
        $currentDate=date("Y-m-d");

        if($_POST['getWeeklyAttendanceHistory']=="getWeeklyAttendanceHistory" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shift_time_id = $sdata['shift_time_id'];
            }

            // Total Week of Month

            // $textdt=date("01-m-Y");
            $currdt= strtotime($textdt);
            $nextmonth=strtotime($textdt."+1 month");
            $i=0;
            $flag=true;

            $response["weekly_histrory"] = array();
            $response["week_position"] = "0";

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];

            $parts = explode(':', $perdayHours);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);

                     
            // Week
        
            for ($x = 0; $x < count($startarr); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";
                $weekArray["total_spend_time"] = "0";
                $weekArray["total_spend_time_name"] = "0";

                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);

                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $days = array();
                    $days['day'] = $date->format('l');
                    $days['date'] = $dayDate = $date->format('Y-m-d');
                    $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate)) ;

                    if ($dayDate == date('Y-m-d')) {
                        $response["week_position"] = $x."";
                    }
                    
                    $holidayQry = $d->selectRow("holiday_start_date","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' ");

                    $x1 = $x+1;
                    // holiday off
                    if (mysqli_num_rows($holidayQry)>0) {
                        $minusHoliday += $perdayHours;
                    } else {
                        // full day off
                        if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                            $minusWeekOffHR += $perdayHours;
                        }
                        // alternate week day off
                        if ($shiftData['has_altenate_week_off'] == "1") {  
                            if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                in_array($x1,$alternateWeekOff)) {
                                $minusAlternateWeekOffDays += $perdayHours;
                            }
                        }
                    }
                    array_push($daysArray['days'],$days);
                }

                $totalWeekDay = count($daysArray['days']);
                
                $totalWeekHour = $perdayHours * $totalWeekDay;
                $totalWeekHour1 = ($totalWeekHour - $minusWeekOffHR) - $minusAlternateWeekOffDays;
                $totalWeekHour = $totalWeekHour1 - $minusHoliday;
                $weekArray['total_hours'] = $totalWeekHour."";
                
                $weekArray['week'] = date('d',strtotime($sDate))."-".date('d',strtotime($eDate))." ".date('F',strtotime($sDate))." ".date('Y',strtotime($sDate));

                // Week History

                $tdate = date("$year-m-d");

                $attendanceQry = $d->select("attendance_master,users_master","(attendance_master.society_id='$society_id' 
                    AND attendance_master.user_id = users_master.user_id 
                    AND attendance_master.user_id = '$user_id'
                    AND attendance_master.attendance_date_start != '$tdate'
                    AND attendance_master.attendance_date_start BETWEEN '$sDate' AND '$eDate') OR (
                    attendance_master.society_id='$society_id' 
                    AND attendance_master.user_id = users_master.user_id 
                    AND attendance_master.user_id = '$user_id'
                    AND attendance_master.attendance_date_start = '$tdate'
                    AND attendance_master.punch_out_time != '00:00:00'
                    AND attendance_master.attendance_date_start BETWEEN '$sDate' AND '$eDate')","ORDER BY attendance_master.attendance_id ASC");

                $weekArray["history"] = array();

                if(mysqli_num_rows($attendanceQry)>0){

                    $times = array();

                    while($data=mysqli_fetch_array($attendanceQry)) {

                        $attendanceHistory = array(); 

                        $attendanceHistory["attendance_id"] = $data['attendance_id'];
                        $attendanceHistory["day_name"] = date('l', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_start"] = date('d m Y', strtotime($data['attendance_date_start']));
                        $attendanceHistory["attendance_date_end"] = date('d m Y', strtotime($data['attendance_date_end']));

                        $attendanceDateStart = $data['attendance_date_start'];
                        $attendanceDateEnd = $data['attendance_date_end'];
                        
                        $punchInTime = $data['punch_in_time'];
                        $punchOutTime = $data['punch_out_time'];
                        
                        $attendanceHistory["punch_in_time"] = date("h:i A",strtotime($punchInTime));
                        $attendanceHistory["punch_out_time"] = date("h:i A",strtotime($punchOutTime));

                        if ($attendanceDateEnd == "0000-00-00") {
                            $attendanceHistory["attendance_date_end"] = "";
                            $attendanceHistory["punch_out_time"] = "";
                            $attendanceDateEnd = date('Y-m-d');
                            $punchOutTime = date('H:i:s');
                        }

                        if ($data['punch_out_time'] != "00:00:00" && $data['attendance_date_end'] != "0000-00-00") {
                        
                            $totalHours = getTotalHours($attendanceDateStart,$attendanceDateEnd,$punchInTime,$punchOutTime);
                            
                            $totalHoursName = getTotalHoursWithNames($attendanceDateStart,$attendanceDateEnd,$punchInTime,$punchOutTime);

                            $times[] = $totalHours;

                            $attendanceHistory["total_hours"] = $totalHoursName;
                            $attendanceHistory["is_punch_out_missing"] = false;
                            $attendanceHistory["punch_out_missing_message"] = "";

                            $weekArray['total_spend_time'] = getTotalWeekHours($times);
                            $weekArray['total_spend_time_name'] = getTotalWeekHoursName($times);
                        }else{
                            $attendanceHistory["is_punch_out_missing"] = true;
                            $attendanceHistory["total_hours"] = "";
                            $attendanceHistory["punch_out_missing_message"] = "Punch Out Missing";
                        }

                        array_push($weekArray["history"], $attendanceHistory);
                    }
                }

                array_push($response['weekly_histrory'],$weekArray);
            } 
            
            echo json_encode($response);
        
        }else{
            $response["message"]="Wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    /* }else{
        $response["message"]="Wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    } */
}

function getTotalWeekHours($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d.%02d', $hours, $minutes);
}

function getTotalWeekHoursName($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d:%02d', $hours, $minutes);
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}


function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');


    return sprintf('%02d:%02d', $hours, $minutes);
}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}
?>