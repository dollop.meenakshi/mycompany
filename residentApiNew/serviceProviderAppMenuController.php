<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
	extract(array_map("test_input" , $_POST));
	if(isset($_POST["getAppMenu"])) {
		extract(array_map("test_input" , $_POST)); 

		if ($device=='android' && $app_version_code>=13) {
			$q=$d->select("local_service_providers_app_menu","","ORDER BY app_menu_index ASC");
		}else if ($device=='android') {
			$q=$d->select("local_service_providers_app_menu","app_menu_status_android='0'","ORDER BY app_menu_index ASC");

		}else{
			$q=$d->select("local_service_providers_app_menu","ORDER BY app_menu_status_ios='0'","app_menu_index ASC"); 
		}
 
		$response["slider"] = array();

		$qdefailt=$d->select("app_slider_master","slider_type=0","order by RAND()");

        while($data_notification=mysqli_fetch_array($qdefailt)) {

            $slider = array(); 

            $slider["app_slider_id"]=$data_notification['app_slider_id'];
            $slider["slider_image_name"]=$base_url."img/sliders/".$data_notification['slider_image_name'];
            $slider["youtube_url"]=$data_notification['youtube_url'];
            $slider["page_url"]=$data_notification['page_url'].'';
             $slider["page_mobile"]=$data_notification['page_mobile'].'';
            array_push($response["slider"], $slider); 
        }
		
		if(mysqli_num_rows($q)>0){

			$notiCount= $d->select("local_service_provider_notifications","service_provider_users_id='$service_provider_users_id' AND read_status=0");

			$qCompCount=$d->select("local_service_provider_complains,society_master","local_service_provider_complains.local_service_provider_id='$service_provider_users_id' AND local_service_provider_complains.status = '0' AND local_service_provider_complains.society_id=society_master.society_id","");
			$complaintCount =  mysqli_num_rows($qCompCount);

			$qCompbackCount=$d->select("local_serviceproviders_call_request,society_master","local_serviceproviders_call_request.callback_action=0 AND local_serviceproviders_call_request.society_id=society_master.society_id AND local_serviceproviders_call_request.local_service_provider_users_id='$service_provider_users_id'","");

			$callBackCount =  mysqli_num_rows($qCompbackCount);

			$response["appMenu"] = array();
			while($data=mysqli_fetch_array($q)) {
				$appMenu = array();
				$appMenu["app_menu_id"]=$data['app_menu_id'];
				$appMenu["app_menu_name"]=$data['app_menu_name'];
				$appMenu["app_menu_image"]=$base_url."img/menu_icon/".$data['app_menu_image'];
				
				if ($device=='android') {
				$appMenu["app_menu_click"]=$data['app_menu_click_android'];
				}else{
				$appMenu["app_menu_click"]=$data['app_menu_status_ios'];
				}


				if ($data['app_menu_click_android']=='complains') {
					$appMenu["menu_count"]= $complaintCount.'';
				} else if ($data['app_menu_click_android']=='callback_request') {
					$appMenu["menu_count"]= $callBackCount.'';
				} else {
					$appMenu["menu_count"]= "0";
				}

				array_push($response["appMenu"], $appMenu);
			}

			$response["unread_notification"]= mysqli_num_rows($notiCount).'';
			$response["message"]="Get AppMenu .";
			$response["invoice_url"]="http://www.africau.edu/images/default/sample.pdf";
			$response["status"]="200";
			echo json_encode($response);
			exit();
		}else {
			$response["unread_notification"]= "0";
			$response["message"]="AppMenu Not found";
	        $response["status"]="201";
	        echo json_encode($response);
	        exit();
		}
	}  else {
		$response["message"] = "Wrong Tag";
		$response["status"] = "201";
		echo json_encode($response);
	}
}else{
	$response["message"] = "Invalid Request";
	$response["status"] = "201";
	echo json_encode($response);
}

?>