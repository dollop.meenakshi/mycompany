<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {


    if ($key == $keydb && $auth_check=='true') {

        $response = array();
        extract(array_map("test_input", $_POST));

        if ($_POST['getNotification'] == "getNotification" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            $qSociety = $d->select("society_master", "society_id ='$society_id'");
            $user_data_society = mysqli_fetch_array($qSociety);
            $visitor_on_off = $user_data_society['visitor_on_off'];
            $entry_all_visitor_group = $user_data_society['entry_all_visitor_group'];
            $group_chat_status = $user_data_society['group_chat_status'];
            $screen_sort_capture_in_timeline = $user_data_society['screen_sort_capture_in_timeline'];
            $create_group = $user_data_society['create_group'];
            $tenant_registration = $user_data_society['tenant_registration'];
            if($user_data_society['professional_on_off']==1) {
                $professional_on_off =false;
            }else {
                $professional_on_off =true;
            }

            if ($isBirthdayWish == "true") {
                $qnotification = $d->select("user_notification", "user_id='$user_id' AND notification_type=0 AND notification_logo = 'happy_birthday.png'", "ORDER BY user_notification_id DESC LIMIT 500");
            }else{
                $qnotification = $d->select("user_notification", "user_id='$user_id' AND notification_type=0", "ORDER BY user_notification_id DESC LIMIT 500");
            }

           

            $tq=$d->selectRow("feed_id","news_feed","status = 0 ","ORDER BY feed_id DESC ");
            $timelineData=mysqli_fetch_array($tq);
            if ($timelineData>0) {
              $last_feed_id = $timelineData['feed_id'].'';
            } else {
                $last_feed_id = '0';
            }

            if (mysqli_num_rows($qnotification) > 0) {

                $response["notification"] = array();

                while ($data_notification = mysqli_fetch_array($qnotification)) {
                    $data_notification = array_map("html_entity_decode", $data_notification);
                    $notification = array();

                    $notification["user_notification_id"] = $data_notification['user_notification_id'];
                    $notification["notification_title"] = html_entity_decode($data_notification['notification_title']);
                    $notification["notification_desc"] = html_entity_decode($data_notification['notification_desc']);
                    $notification["notification_date"] = date("h:i A, dS M Y", strtotime($data_notification['notification_date']));
                    $notification["notification_status"] = $data_notification['notification_status'];
                    $notification["read_status"] = $data_notification['read_status'];
                    $notification["notification_action"] = $data_notification['notification_action'];
                    if ($data_notification['notification_logo']=='') {
                        $notification["notification_logo"] = $base_url.'img/logo.png';
                    } else {
                        $notification["notification_logo"] = $base_url.'img/app_icon/'.$data_notification['notification_logo'];
                    }
                    $notification["user_id"] = $data_notification['user_id'];
                    $notification["notification_data_ids"] = $data_notification['notification_data_ids'].'';
                    if ($data_notification['user_id']!=0) {
                        $notification["feed_id"] = $data_notification['feed_id'];
                    } else {
                        $notification["feed_id"] = "";
                    }

                    array_push($response["notification"], $notification);
                }


                if ($read == "1") {

                    $m->set_data('read_status', '1');

                    $arrayName = array('read_status' => $m->get_data('read_status'));

                    $q2 = $d->update("user_notification", $arrayName, "user_id='$user_id' AND  notification_type=0");
                } else {

                    $qnotificationCount = $d->select("user_notification", "user_id='$user_id' AND read_status='0' AND  notification_type=0");
                    $response["read_status"] = mysqli_num_rows($qnotificationCount) . "";

                    $qchatCount = $d->select("chat_master", "msg_for='$user_id' 
                        AND society_id='$society_id' AND msg_status='0'", "GROUP BY msg_by");
                    $response["chat_status"] = mysqli_num_rows($qchatCount) . "";

                    if (mysqli_num_rows($qchatCount) > 0) {

                        $response["user"] = array();

                        while ($data2 = mysqli_fetch_array($qchatCount)) {

                            $user = array();
                            $userId = $data2['msg_by'];
                            $user["userId"] =   $data2['msg_by'];

                            if ($data2['send_by'] == 0) {
                                $qchatUserData = $d->select("users_master,block_master,floors_master,unit_master", "users_master.user_id='$userId' 
                                AND users_master.block_id=block_master.block_id 
                                AND users_master.floor_id=floors_master.floor_id 
                                AND users_master.unit_id=unit_master.unit_id", "");

                                $chatUserData = mysqli_fetch_array($qchatUserData);

                                $user["userProfile"] =   $base_url . "img/users/recident_profile/" . $chatUserData['user_profile_pic'];
                                $user["userName"] =   $chatUserData['user_full_name'];
                                $user["block_name"] =   $chatUserData['block_name'] . "-" . $chatUserData['unit_name'];
                                $user["userType"] =   "Resident";
                            } else {

                                $qchatUserData = $d->select("employee_master", "emp_id='$userId'", "");
                                $chatUserData = mysqli_fetch_array($qchatUserData);

                                $user["userProfile"] =   $base_url . "img/emp/" . $chatUserData['user_profile_pic'];
                                $user["userName"] =   $chatUserData['emp_name'];
                                $user["block_name"] =  "Security Guard";
                                $user["userType"] =   "gaurd";
                            }

                            array_push($response["user"], $user);
                        }
                    }
                }

                $response["professional_on_off"] = $professional_on_off;
                $response["visitor_on_off"] = $visitor_on_off;
                $response["entry_all_visitor_group"] = $entry_all_visitor_group;
                $response["group_chat_status"] = $group_chat_status;
                $response["screen_sort_capture_in_timeline"] = $screen_sort_capture_in_timeline;
                $response["create_group"] = $create_group;
                $response["tenant_registration"] = $tenant_registration;
                $response["last_feed_id"] = $last_feed_id;

                $response["message"] = "Get Notification success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $qchatCount = $d->select("chat_master", "msg_for='$user_id' 
                        AND society_id='$society_id' AND msg_status='0'", "GROUP BY msg_by");
                $response["chat_status"] = mysqli_num_rows($qchatCount) . "";

                if (mysqli_num_rows($qchatCount) > 0) {

                    $response["user"] = array();

                    while ($data2 = mysqli_fetch_array($qchatCount)) {

                        $user = array();
                        $userId = $data2['msg_by'];
                        $user["userId"] =   $data2['msg_by'];

                        if ($data2['send_by'] == 0) {
                            $qchatUserData = $d->select("users_master,block_master,floors_master,unit_master", "users_master.user_id='$userId' 
                                AND users_master.block_id=block_master.block_id 
                                AND users_master.floor_id=floors_master.floor_id 
                                AND users_master.unit_id=unit_master.unit_id", "");

                            $chatUserData = mysqli_fetch_array($qchatUserData);

                            $user["userProfile"] =   $base_url . "img/users/recident_profile/" . $chatUserData['user_profile_pic'];
                            $user["userName"] =   $chatUserData['user_full_name'];
                            $user["block_name"] =   $chatUserData['block_name'] . "-" . $chatUserData['unit_name'];
                            $user["userType"] =   "Resident";
                        } else {

                            $qchatUserData = $d->select("employee_master", "emp_id='$userId'", "");
                            $chatUserData = mysqli_fetch_array($qchatUserData);

                            $user["userProfile"] =   $base_url . "img/emp/" . $chatUserData['user_profile_pic'];
                            $user["userName"] =   $chatUserData['emp_name'];
                            $user["block_name"] =  "Security Guard";
                            $user["userType"] =   "gaurd";
                        }

                        array_push($response["user"], $user);
                    }
                }
                
                $response["professional_on_off"] = $professional_on_off;
                $response["group_chat_status"] = $group_chat_status;
                $response["screen_sort_capture_in_timeline"] = $screen_sort_capture_in_timeline;
                $response["visitor_on_off"] = $visitor_on_off;
                $response["entry_all_visitor_group"] = $entry_all_visitor_group;
                $response["create_group"] = $create_group;
                $response["tenant_registration"] = $tenant_registration;
                $response["message"] = "No Notification Found.";
                $response["last_feed_id"] = $last_feed_id;
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getNotificationCount'] == "getNotificationCount" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            $qSociety = $d->select("society_master", "society_id ='$society_id'");
            $user_data_society = mysqli_fetch_array($qSociety);
            $visitor_on_off = $user_data_society['visitor_on_off'];
            $entry_all_visitor_group = $user_data_society['entry_all_visitor_group'];
            $group_chat_status = $user_data_society['group_chat_status'];
            $screen_sort_capture_in_timeline = $user_data_society['screen_sort_capture_in_timeline'];
            $create_group = $user_data_society['create_group'];
            $hide_member_parking = $user_data_society['hide_member_parking'];
            $tenant_registration = $user_data_society['tenant_registration'];
            $virtual_wallet = $user_data_society['virtual_wallet'];
            if($user_data_society['professional_on_off']==1) {
                $professional_on_off =false;
            }else {
                $professional_on_off =true;
            }

            if($hide_member_parking==1) {
                $hide_member_parking =true;
            }else {
                $hide_member_parking =false;
            }


          
            $virtual_wallet =false;

            $qnotification = $d->select("user_notification", "user_id='$user_id' AND notification_type=0", "ORDER BY user_notification_id DESC LIMIT 500");

            $tq=$d->selectRow("feed_id","news_feed","status = 0 ","ORDER BY feed_id DESC ");
            $timelineData=mysqli_fetch_array($tq);
            if ($timelineData>0) {
              $last_feed_id = $timelineData['feed_id'].'';
            } else {
                $last_feed_id = '0';
            }

               

              
                   

                $qnotificationCount = $d->select("user_notification", "user_id='$user_id' AND read_status='0' AND  notification_type=0");
                $response["read_status"] = mysqli_num_rows($qnotificationCount) . "";

                $qchatCount = $d->select("chat_master", "msg_for='$user_id' 
                    AND society_id='$society_id' AND msg_status='0'", "GROUP BY msg_by");
                $response["chat_status"] = mysqli_num_rows($qchatCount) . "";
                $response["visitor_on_off"] = $visitor_on_off;
                $response["entry_all_visitor_group"] = $entry_all_visitor_group;
                $response["group_chat_status"] = $group_chat_status;
                $response["screen_sort_capture_in_timeline"] = $screen_sort_capture_in_timeline;
                $response["create_group"] = $create_group;
                $response["tenant_registration"] = $tenant_registration;
                $response["last_feed_id"] = $last_feed_id;
                $response["professional_on_off"] = $professional_on_off;
                $response["hide_member_parking"] = $hide_member_parking;
                $response["virtual_wallet"] = $virtual_wallet;
                $response["message"] = "Get Notification Success.";
                $response["status"] = "200";
                echo json_encode($response);
           
        }else if ($_POST['getNotificationTimeline'] == "getNotificationTimeline" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true) {


            $qnotification = $d->select("user_notification", "user_id='$user_id' AND notification_type=1 AND notification_type=1", "ORDER BY user_notification_id DESC LIMIT 500");

            if (mysqli_num_rows($qnotification) > 0) {

                $response["notification"] = array();

                while ($data_notification = mysqli_fetch_array($qnotification)) {

                    $notification = array();

                    $qcd=$d->selectRow("user_profile_pic","users_master","user_id='$data_notification[other_user_id]'");
                    $userData=mysqli_fetch_array($qcd);

                    $notification["user_notification_id"] = $data_notification['user_notification_id'];
                    $notification["notification_title"] = "";
                    $notification["notification_desc"] = html_entity_decode($data_notification['notification_desc']);
                    $notification["notification_date"] = time_elapsed_string($data_notification['notification_date']);
                    $notification["notification_status"] = $data_notification['notification_status'];
                    $notification["read_status"] = $data_notification['read_status'];
                    $notification["notification_action"] = $data_notification['notification_action'];
                    if ( $userData['user_profile_pic']!='') {
                    $notification["notification_logo"] = $base_url.'img/users/recident_profile/' . $userData['user_profile_pic'];
                        # code...
                    }else {
                    $notification["notification_logo"] = $base_url.'img/users/owner/user_default.png';
                    }
                    $notification["user_id"] = $data_notification['user_id'];
                    $notification["feed_id"] = $data_notification['feed_id'];

                    array_push($response["notification"], $notification);
                }

                // if ($read == "1") {

                    $m->set_data('read_status', '1');

                    $arrayName = array('read_status' => $m->get_data('read_status'));

                    $q2 = $d->update("user_notification", $arrayName, "user_id='$user_id' AND  notification_type=1");
                // }


                $response["message"] = "Get Notification success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                 $response["message"] = "No Data Available";
                $response["status"] = "201";
                echo json_encode($response);
            }

        } else if ($_POST['DeleteUserNotification'] == "DeleteUserNotification" && $user_notification_id != '' && filter_var($user_notification_id, FILTER_VALIDATE_INT) == true) {


            $qdelete = $d->delete("user_notification", "user_notification_id='$user_notification_id' AND user_id!=0");


            if ($qdelete == TRUE) {


                $response["message"] = "Notification deleted successfully";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Delete Notification Fail.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['DeleteUserNotificationAll'] == "DeleteUserNotificationAll" && $user_id != '' && filter_var($user_id, FILTER_VALIDATE_INT) == true) {


            $qdelete = $d->delete("user_notification", "user_id='$user_id' AND user_id!=0 AND notification_type=0");


            if ($qdelete == TRUE) {


                $response["message"] = "Notification deleted successfully ";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Delete Notification Fail.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['DeleteUserNotificationAllTimline'] == "DeleteUserNotificationAllTimline" && $user_id != '' && filter_var($user_id, FILTER_VALIDATE_INT) == true) {


            $qdelete = $d->delete("user_notification", "user_id='$user_id' AND user_id!=0 AND notification_type=1");


            if ($qdelete == TRUE) {


                $response["message"] = "Notifications Deleted";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Delete Notification Fail.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
