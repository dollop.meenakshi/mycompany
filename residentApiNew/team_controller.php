<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST) && !empty($_POST)) {

	if ($key == $keydb && $auth_check=='true') {

		$response = array();
		extract(array_map("test_input", $_POST));

		if ($_POST['getMyTeam']=="getMyTeam"  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

			$teamQry = $d->selectRow("
                employee_level_master.level_id,
                employee_level_master.parent_level_id,
                employee_level_master.level_name,
                users_master.user_id,
                users_master.user_full_name,
                users_master.user_designation,
                users_master.user_profile_pic
                ","
                users_master,
                employee_level_master
                ","
                users_master.level_id = employee_level_master.level_id 
                AND users_master.delete_status = '0'
                AND employee_level_master.parent_level_id = '$level_id' AND  employee_level_master.parent_level_id != ''");

            $response['my_team'] = array();

            $userIDArray = array();

            if (mysqli_num_rows($teamQry) > 0) {
                while($teamData = mysqli_fetch_array($teamQry)){
                    $team = array();

                    $team_member_id = $teamData['user_id'];

                    array_push($userIDArray,$team_member_id);

                    $team['user_id'] = $team_member_id;
                    $team['level_id'] = $teamData['level_id'];
                    $team['level_name'] = $teamData['level_name'];
                    $team['user_full_name'] = $teamData['user_full_name'];
                    $team['user_designation'] = $teamData['user_designation'];
                    if ($teamData['user_profile_pic'] != '') {                        
                        $team['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $teamData['user_profile_pic'];
                    }else{
                        $team['user_profile_pic'] = "";
                    }

                    array_push($response['my_team'],$team);
                }				
			}

            $userIDArray = join("','",$userIDArray); 

            // Current Week

            $monday = strtotime('next Monday -1 week');
            $monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
            $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
            $this_week_sd = date("Y-m-d",$monday);
            $this_week_ed = date("Y-m-d",$sunday);

            // Next Week

            $monday = strtotime('next Monday');
            $monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
            $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
            $next_week_sd = date("Y-m-d",$monday);
            $next_week_ed = date("Y-m-d",$sunday);

            if ($limit != '') {
                $leaveCurrentWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    leave_master.leave_reason,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.delete_status = '0'
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$this_week_sd' AND '$this_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC LIMIT $limit");

                $leaveNextWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    leave_master.leave_reason,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.delete_status = '0'
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$next_week_sd' AND '$next_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC $limit");
            }else{
                $leaveCurrentWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    leave_master.leave_reason,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.delete_status = '0'
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$this_week_sd' AND '$this_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC");

                $leaveNextWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    leave_master.leave_reason,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.delete_status = '0'
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$next_week_sd' AND '$next_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC");
            }            

            $response['leave_list'] = array();
            $response['leave_list_next_week'] = array();

            if (mysqli_num_rows($leaveCurrentWeekQry) > 0) {
                while($leaveData = mysqli_fetch_array($leaveCurrentWeekQry)){
                    $leave = array();

                    $leave['user_id'] = $leaveData['user_id'];
                    $leave['leave_id'] = $leaveData['leave_id'];
                    $leave['leave_start_date'] = $leaveData['leave_start_date'];
                    $leave["leave_date_view"] = date("D, dS M Y",strtotime($leaveData["leave_start_date"]));
                    $leave['user_full_name'] = $leaveData['user_full_name'];
                    $leave['user_designation'] = $leaveData['user_designation'];
                    $leave['leave_type_name'] = $leaveData['leave_type_name'];
                    $leave['leave_reason'] = $leaveData['leave_reason'];

                    if ($leaveData['paid_unpaid'] == 0) {    
                        $leave['leave_type'] = "Paid";
                    }else{
                        $leave['leave_type'] = "Unpaid";
                    }

                    if ($leaveData['leave_day_type'] == 0) {                        
                        $leave['leave_day_type'] = "Full Day";
                        $leave['half_day_session'] = "";
                    }else{
                        $leave['leave_day_type'] = "Half Day";

                        if ($leaveData['half_day_session'] == 1) {                        
                            $leave['half_day_session'] = "First Half";
                        }else if ($leaveData['half_day_session'] == 2) {    
                            $leave['half_day_session'] = "Second Half";
                        }else{
                            $leave['half_day_session'] = "";
                        }
                    }
                    
                    if ($leaveData['user_profile_pic'] != '') {                        
                        $leave['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $leaveData['user_profile_pic'];
                    }else{
                        $leave['user_profile_pic'] = "";
                    }

                    array_push($response['leave_list'],$leave);
                }
            }

            if (mysqli_num_rows($leaveNextWeekQry) > 0) {
                while($leaveData = mysqli_fetch_array($leaveNextWeekQry)){
                    $leave = array();

                    $leave['user_id'] = $leaveData['user_id'];
                    $leave['leave_id'] = $leaveData['leave_id'];
                    $leave['leave_start_date'] = $leaveData['leave_start_date'];
                    $leave["leave_date_view"] = date("D, dS M Y",strtotime($leaveData["leave_start_date"]));
                    $leave['user_full_name'] = $leaveData['user_full_name'];
                    $leave['user_designation'] = $leaveData['user_designation'];
                    $leave['leave_type_name'] = $leaveData['leave_type_name'];
                    $leave['leave_reason'] = $leaveData['leave_reason'];
                    if ($leaveData['paid_unpaid'] == 0) {    
                        $leave['leave_type'] = "Paid";
                    }else{
                        $leave['leave_type'] = "Unpaid";
                    }
                    if ($leaveData['leave_day_type'] == 0) {                        
                        $leave['leave_day_type'] = "Full Day";
                        $leave['half_day_session'] = "";
                    }else{
                        $leave['leave_day_type'] = "Half Day";

                        if ($leaveData['half_day_session'] == 1) {                        
                            $leave['half_day_session'] = "First Half";
                        }else if ($leaveData['half_day_session'] == 2) {    
                            $leave['half_day_session'] = "Second Half";
                        }else{
                            $leave['half_day_session'] = "";
                        }
                    }
                    
                    if ($leaveData['user_profile_pic'] != '') {                        
                        $leave['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $leaveData['user_profile_pic'];
                    }else{
                        $leave['user_profile_pic'] = "";
                    }

                    array_push($response['leave_list_next_week'],$leave);
                }
            }

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
		}else if ($_POST['getMyTeamLeaves']=="getMyTeamLeaves"  &&  filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            $teamQry = $d->selectRow("
                employee_level_master.level_id,
                employee_level_master.parent_level_id,
                employee_level_master.level_name,
                users_master.user_id,
                users_master.user_full_name,
                users_master.user_designation,
                users_master.user_profile_pic
                ","
                users_master,
                employee_level_master
                ","
                users_master.level_id = employee_level_master.level_id 
                AND users_master.delete_status = '0'
                AND employee_level_master.parent_level_id = '$level_id' AND  employee_level_master.parent_level_id != ''");

            $response['my_team'] = array();

            $userIDArray = array();

            if (mysqli_num_rows($teamQry) > 0) {
                while($teamData = mysqli_fetch_array($teamQry)){
                    $team_member_id = $teamData['user_id'];

                    array_push($userIDArray,$team_member_id);
                }               
            }

            $userIDArray = join("','",$userIDArray); 

            // Current Week

            $monday = strtotime('next Monday -1 week');
            $monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
            $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
            $this_week_sd = date("Y-m-d",$monday);
            $this_week_ed = date("Y-m-d",$sunday);

            // Next Week

            $monday = strtotime('next Monday');
            $monday = date('w', $monday)==date('w') ? strtotime(date("Y-m-d",$monday)." +7 days") : $monday;
            $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
            $next_week_sd = date("Y-m-d",$monday);
            $next_week_ed = date("Y-m-d",$sunday);

            if ($limit != '') {
                $leaveCurrentWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    leave_master.leave_reason,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.delete_status = '0'
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$this_week_sd' AND '$this_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC LIMIT $limit");

                $leaveNextWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    leave_master.leave_reason,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.delete_status = '0'
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$next_week_sd' AND '$next_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC $limit");
            }else{
                $leaveCurrentWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    leave_master.leave_reason,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.delete_status = '0'
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$this_week_sd' AND '$this_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC");

                $leaveNextWeekQry = $d->selectRow("
                    leave_type_master.leave_type_name,
                    leave_master.leave_id,
                    leave_master.half_day_session,
                    leave_master.leave_start_date,
                    leave_master.leave_day_type,
                    leave_master.paid_unpaid,
                    leave_master.leave_reason,
                    users_master.user_id,
                    users_master.user_full_name,
                    users_master.user_designation,
                    users_master.user_profile_pic
                    ","
                    users_master,
                    leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id = leave_master.leave_type_id
                    ","
                    leave_master.user_id = users_master.user_id 
                    AND users_master.delete_status = '0'
                    AND users_master.user_id IN ('$userIDArray') 
                    AND leave_master.leave_start_date BETWEEN '$next_week_sd' AND '$next_week_ed'
                    ","
                    ORDER BY leave_master.leave_start_date ASC");
            }            

            $response['leave_list'] = array();
            $response['leave_list_next_week'] = array();

            if (mysqli_num_rows($leaveCurrentWeekQry) > 0) {
                while($leaveData = mysqli_fetch_array($leaveCurrentWeekQry)){
                    $leave = array();

                    $leave['user_id'] = $leaveData['user_id'];
                    $leave['leave_id'] = $leaveData['leave_id'];
                    $leave['leave_start_date'] = $leaveData['leave_start_date'];
                    $leave["leave_date_view"] = date("D, dS M Y",strtotime($leaveData["leave_start_date"]));
                    $leave['user_full_name'] = $leaveData['user_full_name'];
                    $leave['user_designation'] = $leaveData['user_designation'];
                    $leave['leave_type_name'] = $leaveData['leave_type_name'];
                    $leave['leave_reason'] = $leaveData['leave_reason'];

                    if ($leaveData['paid_unpaid'] == 0) {    
                        $leave['leave_type'] = "Paid";
                    }else{
                        $leave['leave_type'] = "Unpaid";
                    }

                    if ($leaveData['leave_day_type'] == 0) {                        
                        $leave['leave_day_type'] = "Full Day";
                        $leave['half_day_session'] = "";
                    }else{
                        $leave['leave_day_type'] = "Half Day";

                        if ($leaveData['half_day_session'] == 1) {                        
                            $leave['half_day_session'] = "First Half";
                        }else if ($leaveData['half_day_session'] == 2) {    
                            $leave['half_day_session'] = "Second Half";
                        }else{
                            $leave['half_day_session'] = "";
                        }
                    }
                    
                    if ($leaveData['user_profile_pic'] != '') {                        
                        $leave['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $leaveData['user_profile_pic'];
                    }else{
                        $leave['user_profile_pic'] = "";
                    }

                    array_push($response['leave_list'],$leave);
                }
            }

            if (mysqli_num_rows($leaveNextWeekQry) > 0) {
                while($leaveData = mysqli_fetch_array($leaveNextWeekQry)){
                    $leave = array();

                    $leave['user_id'] = $leaveData['user_id'];
                    $leave['leave_id'] = $leaveData['leave_id'];
                    $leave['leave_start_date'] = $leaveData['leave_start_date'];
                    $leave["leave_date_view"] = date("D, dS M Y",strtotime($leaveData["leave_start_date"]));
                    $leave['user_full_name'] = $leaveData['user_full_name'];
                    $leave['user_designation'] = $leaveData['user_designation'];
                    $leave['leave_type_name'] = $leaveData['leave_type_name'];
                    $leave['leave_reason'] = $leaveData['leave_reason'];
                    if ($leaveData['paid_unpaid'] == 0) {    
                        $leave['leave_type'] = "Paid";
                    }else{
                        $leave['leave_type'] = "Unpaid";
                    }
                    if ($leaveData['leave_day_type'] == 0) {                        
                        $leave['leave_day_type'] = "Full Day";
                        $leave['half_day_session'] = "";
                    }else{
                        $leave['leave_day_type'] = "Half Day";

                        if ($leaveData['half_day_session'] == 1) {                        
                            $leave['half_day_session'] = "First Half";
                        }else if ($leaveData['half_day_session'] == 2) {    
                            $leave['half_day_session'] = "Second Half";
                        }else{
                            $leave['half_day_session'] = "";
                        }
                    }
                    
                    if ($leaveData['user_profile_pic'] != '') {                        
                        $leave['user_profile_pic'] = $base_url . "img/users/recident_profile/" .  $leaveData['user_profile_pic'];
                    }else{
                        $leave['user_profile_pic'] = "";
                    }

                    array_push($response['leave_list_next_week'],$leave);
                }
            }

            $response["message"] = "Success";
            $response["status"] = "200";
            echo json_encode($response);
        }else {
			$response["message"] = "wrong tag.";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {

		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}
?>