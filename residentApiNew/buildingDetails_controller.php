<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
	$response = array();
	extract(array_map("test_input" , $_POST));
    

        if (isset($buildingDetails) && $buildingDetails=='buildingDetails' && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

        $q=$d->select("society_master","society_id ='$society_id' AND society_id!=0 and society_status='0'");

            if (mysqli_num_rows($q)>0) {
  
            $balancesheet_data=mysqli_fetch_array($q);
            
            if($balancesheet_data==TRUE) {           

            $response["society_name"]=html_entity_decode($balancesheet_data['society_name']);
            $response["society_based"]=html_entity_decode($balancesheet_data['society_based']).'';
            $response["society_address"]=html_entity_decode($balancesheet_data['society_address']);
            if ($balancesheet_data['socieaty_logo']!= "") {
            $response["socieaty_logo"]=$base_url."img/society/".$balancesheet_data['socieaty_cover_photo'];
            } else {
            $response["socieaty_logo"]=$base_url."img/society/".$balancesheet_data['socieaty_logo'];
                
            }
           
            $response["builder_name"]=html_entity_decode($balancesheet_data['builder_name']);
            $response["builder_address"]=html_entity_decode($balancesheet_data['builder_address']);
            $response["society_latitude"]=$balancesheet_data['society_latitude'];
            $response["society_longitude"]=$balancesheet_data['society_longitude'];
             if ($balancesheet_data['builder_mobile']!= "0") {
                $response["builder_mobile"]=$balancesheet_data['builder_mobile_country_code'] .' '.$balancesheet_data['builder_mobile'];
            } else {
                $response["builder_mobile"]="";
            }
            $response["secretary_email"]=$balancesheet_data['secretary_email'];
			$response["secretary_mobile"]=$balancesheet_data['secretary_mobile'];
            $q1=$d->select("block_master","society_id ='$society_id'");
            
			$q2=$d->select("unit_master,floors_master,block_master",
            "block_master.block_id=floors_master.block_id AND  unit_master.floor_id=floors_master.floor_id
            AND unit_master.society_id='$society_id'");
			// $q3=$d->select("users_master","society_id ='$society_id'");
           
            $totalFamilyMember=$d->count_data_direct("user_id","users_master","delete_status=0 AND society_id='$society_id' AND user_status!=0");
            
           

            $response["no_of_units"]=mysqli_num_rows($q2);
            $response["unit_name"]="Units";
			$response["no_of_blocks"]=mysqli_num_rows($q1);
			$response["block_name"]="Blocks";
			$response["no_of_population"]=$totalFamilyMember;
            $response["population_view"]="Population";

            

            $qCommitie=$d->select("bms_admin_master,role_master","bms_admin_master.society_id ='$society_id' AND bms_admin_master.role_id=role_master.role_id AND role_master.role_id!=1 AND role_master.role_id!=2 AND bms_admin_master.user_type=0 AND bms_admin_master.admin_active_status=0","ORDER BY bms_admin_master.order_number ASC");

            if(mysqli_num_rows($qCommitie)>0){

                $response["commitie"] = array();

                while($data_notification=mysqli_fetch_array($qCommitie)) {

                    $commitie = array(); 

                    $commitie["admin_id"]=$data_notification['admin_id'];
                    $commitie["role_id"]=$data_notification['role_id'];
                    $commitie["society_id"]=$data_notification['society_id'];
                    $commitie["admin_name"]=html_entity_decode($data_notification['admin_name']);
                    $commitie["admin_email"]=$data_notification['admin_email'];

                    if ($data_notification['mobile_private']==1) {
                     $commitie["admin_mobile"] = "".substr($data_notification['admin_mobile'], 0, 2) . '******' . substr($data_notification['admin_mobile'],  -2);
                    } else {
                        $commitie["admin_mobile"]=$data_notification['country_code'].' '.$data_notification['admin_mobile'];
                    }

                    $commitie["mobile_private"]=$data_notification['mobile_private'];
                    $commitie["admin_address"]=$data_notification['admin_address'];
                    $commitie["role_name"]=$data_notification['role_name'];
                    $commitie["admin_profile"]=$base_url."img/society/".$data_notification['admin_profile'];

                    array_push($response["commitie"], $commitie); 
                }

            }

            $response["builder_view"]="Builder Details";
            $response["authorities_view"]="Builder Authorities";
            $response["statistical_view"]="Statistical Details";
			$response["trial_days"]=$balancesheet_data['trial_days'];
            $response["message"]="Building Infromation Get success.";
            $response["status"]="200";
            echo json_encode($response);
            
            }
            
            }else{
                $response["message"]="Some Error in Building Infromation.";
                $response["status"]="201";
                echo json_encode($response);
            }

    } else if (isset($buildingDetailsNew) && $buildingDetailsNew=='buildingDetailsNew' && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

        $q=$d->select("society_master","society_id ='$society_id' AND society_id!=0 and society_status='0'");

            if (mysqli_num_rows($q)>0) {
  
            $balancesheet_data=mysqli_fetch_array($q);
            
            if($balancesheet_data==TRUE) {           

            $response["society_name"]=html_entity_decode($balancesheet_data['society_name']);
            $response["society_based"]=html_entity_decode($balancesheet_data['society_based']).'';
            $response["society_address"]=html_entity_decode($balancesheet_data['society_address']);
            if ($balancesheet_data['socieaty_logo']!= "") {
            $response["socieaty_logo"]=$base_url."img/society/".$balancesheet_data['socieaty_cover_photo'];
            } else {
            $response["socieaty_logo"]=$base_url."img/society/".$balancesheet_data['socieaty_logo'];
                
            }
           
            $response["builder_name"]=html_entity_decode($balancesheet_data['builder_name']);
            $response["builder_address"]=html_entity_decode($balancesheet_data['builder_address']);
            $response["society_latitude"]=$balancesheet_data['society_latitude'];
            $response["society_longitude"]=$balancesheet_data['society_longitude'];
             if ($balancesheet_data['builder_mobile']!= "0") {
                $response["builder_mobile"]=$balancesheet_data['builder_mobile'];
            } else {
                $response["builder_mobile"]="";
            }
            $response["secretary_email"]=$balancesheet_data['secretary_email'];
            $response["secretary_mobile"]=$balancesheet_data['secretary_mobile'];
            $q1=$d->select("block_master","society_id ='$society_id'");
            
            $q2=$d->select("unit_master,floors_master,block_master",
            "block_master.block_id=floors_master.block_id AND  unit_master.floor_id=floors_master.floor_id
            AND unit_master.society_id='$society_id'");
            // $q3=$d->select("users_master","society_id ='$society_id'");
           
            $totalFamilyMember=$d->count_data_direct("user_id","users_master","delete_status=0 AND society_id='$society_id' AND user_status!=0");
            
            
            $response["no_of_units"]=mysqli_num_rows($q2);
            $response["unit_name"]="Units";
            $response["no_of_blocks"]=mysqli_num_rows($q1);
            $response["block_name"]="Blocks";
            $response["no_of_population"]=$totalFamilyMember;
            $response["population_view"]="Population";
            
            

            $qCommitie=$d->select("bms_admin_master,role_master","bms_admin_master.society_id ='$society_id' AND bms_admin_master.role_id=role_master.role_id AND role_master.role_id!=1 AND role_master.role_id!=2 AND bms_admin_master.user_type=0 AND bms_admin_master.admin_active_status=0");

            if(mysqli_num_rows($qCommitie)>0){

                $response["commitie"] = array();

                while($data_notification=mysqli_fetch_array($qCommitie)) {

                    $commitie = array(); 

                    $commitie["admin_id"]=$data_notification['admin_id'];
                    $commitie["role_id"]=$data_notification['role_id'];
                    $commitie["society_id"]=$data_notification['society_id'];
                    $commitie["admin_name"]=html_entity_decode($data_notification['admin_name']);
                    $commitie["admin_email"]=$data_notification['admin_email'];
                    $commitie["admin_mobile"]=$data_notification['admin_mobile'];
                    $commitie["admin_address"]=$data_notification['admin_address'];
                    $commitie["role_name"]=$data_notification['role_name'];
                    $commitie["admin_profile"]=$base_url."img/society/".$data_notification['admin_profile'];

                    array_push($response["commitie"], $commitie); 
                }

            }

            $response["builder_view"]="Builder Details";
            $response["authorities_view"]="Builder Authorities";
            $response["statistical_view"]="Statistical Details";
            $response["trial_days"]=$balancesheet_data['trial_days'];
            $response["message"]="Building Infromation Get success.";
            $response["status"]="200";
            // $encrypted = $d->encrypt($response, '123'); 
           // echo json_encode($response);

            $key = "zy2dEd1pKG5i3WuWbvOBolFQR84AYbvN";
            $ivlength = openssl_cipher_iv_length('AES-256-CBC-HMAC-SHA256');
            $iv = "1234567890123456";
            echo $encryptedString = base64_encode(openssl_encrypt(json_encode($response), 'AES-256-CBC-HMAC-SHA256', $key, OPENSSL_RAW_DATA, $iv));


            }
            
            }else{
                $response["message"]="Some Error in Building Infromation.";
                $response["status"]="201";
                $key = "zy2dEd1pKG5i3WuWbvOBolFQR84AYbvN";
                $ivlength = openssl_cipher_iv_length('AES-256-CBC-HMAC-SHA256');
                $iv = "1234567890123456";
                echo $encryptedString = base64_encode(openssl_encrypt(json_encode($response), 'AES-256-CBC-HMAC-SHA256', $key, OPENSSL_RAW_DATA, $iv));
            }

    } else if (isset($_POST['datax']) ) {

        
        $key = "zy2dEd1pKG5i3WuWbvOBolFQR84AYbvN";
        $string = $datax;        
       
        echo $decrypted = openssl_decrypt(base64_decode($string), 'AES-256-CBC-HMAC-SHA256', $key, OPENSSL_RAW_DATA, $iv);  

    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);

    }

    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}

?>