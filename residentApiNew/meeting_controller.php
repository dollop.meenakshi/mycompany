<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));

        if($_POST['addMeeting']=="addMeeting" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){ 

            $meeting_file = "";

            if ($_FILES["meeting_file"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["meeting_file"]["tmp_name"];
                $extId = pathinfo($_FILES['meeting_file']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","png","jpg","jpeg","PDF","JPEG","PNG","JPG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["meeting_file"]["name"]);
                    $meeting_file = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["meeting_file"]["tmp_name"], "../img/meeting_documents/" . $meeting_file);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }
        
            $m->set_data('society_id',$society_id);
            $m->set_data('meeting_title',$meeting_title);
            $m->set_data('meeting_description',$meeting_description);
            $m->set_data('meeting_file',$meeting_file);
            $m->set_data('meeting_location',$meeting_location);
            $m->set_data('meeting_date',$meeting_date);
            $m->set_data('meeting_time',$meeting_time);
            $m->set_data('meeting_reminder_time',$meeting_reminder_time);
            $m->set_data('meeting_created_by',$meeting_created_by);
            $m->set_data('meeting_created_date',date('Y-m-d H:i:s'));

            $a1= array(
                'society_id'=> $m->get_data('society_id'),
                'meeting_title'=> $m->get_data('meeting_title'),
                'meeting_description'=> $m->get_data('meeting_description'),
                'meeting_file'=> $m->get_data('meeting_file'),
                'meeting_location'=> $m->get_data('meeting_location'),
                'meeting_date'=> $m->get_data('meeting_date'),
                'meeting_time'=> $m->get_data('meeting_time'),
                'meeting_reminder_time'=> $m->get_data('meeting_reminder_time'),
                'meeting_created_by'=> $m->get_data('meeting_created_by'),
                'meeting_created_date'=> $m->get_data('meeting_created_date'),
            );

           
            $q=$d->insert("meeting_master",$a1);

            $meeting_id = $con->insert_id;
                
            if ($q == true) {

                $meeting_member_ids = explode(",",$meeting_member_ids);

                for ($i=0; $i < count($meeting_member_ids); $i++) {

                    $m->set_data('society_id',$society_id);
                    $m->set_data('meeting_id',$meeting_id);
                    $m->set_data('user_id',$meeting_member_ids[$i]);

                    $a2= array(
                        'society_id'=> $m->get_data('society_id'),
                        'meeting_id'=> $m->get_data('meeting_id'),
                        'user_id'=> $m->get_data('user_id'),
                    );

                     $q=$d->insert("meeting_members_master",$a2);
                }

                $title = "New Meeting Invitation";
                $description = "By, ".$user_name."\nDate: ".date('D, d M Y',strtotime($meeting_date))."\nTime: ".date('h:i A',strtotime($meeting_time));

                $ids = join("','",$meeting_member_ids);

                $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$ids')");

                $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$ids')");

                if ($meeting_reminder_time!='') {
                    $hasReminder = true;
                }else{
                    $hasReminder = false;                    
                }

                $min = date('i',strtotime($meeting_reminder_time));
                $defaultINTime = $meeting_date." ".$meeting_time;
                $strMin = "-".$min." minutes";
                $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

                $meetingDate = date('D, d M Y',strtotime($meeting_date));
                $meetingTime = date('h:i A',strtotime($meeting_time));

                $reminderD = date('Y-m-d',strtotime($defaultTime));
                $reminderT = date('H:i:s',strtotime($defaultTime));
                $reminderAMPM = date('A',strtotime($defaultTime));

                $dataAry = array(
                    "has_reminder"=>$hasReminder,
                    "reminder_date"=>$reminderD.'',
                    "reminder_time"=>$reminderT.'',
                    "meeting_date"=>$meetingDate.'',
                    "meeting_time"=>$meetingTime.'',
                    "meeting_title"=>$meeting_title.'',
                    "meeting_description"=>$meeting_description.'',
                    "meeting_id"=>$meeting_id.'',
                    "meeting_by"=>$user_name.''
                );

                $dataJson = json_encode($dataAry);

                $nResident->noti("new_meeting","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                $nResident->noti_ios("new_meeting","",$society_id,$fcmArrayIosPA,$title,$meeting_id,$dataJson);

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"new_meeting","meetings.png",$dataJson,"users_master.user_id IN ('$ids')");
               
                $response["message"] = "New Meeting Scheduled";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['addMeetingDocument']=="addMeetingDocument" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){ 

            $meeting_document = "";

            if ($_FILES["meeting_document"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["meeting_document"]["tmp_name"];
                $extId = pathinfo($_FILES['meeting_document']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","png","jpg","jpeg","PDF","JPEG","PNG","JPG","DOC","doc","DOCX","docx","ppt","pptx","PPT","PPTX","mp3","TXT","txt");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["meeting_document"]["name"]);
                    $meeting_document = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["meeting_document"]["tmp_name"], "../img/meeting_documents/" . $meeting_document);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }
        
            $m->set_data('society_id',$society_id);
            $m->set_data('meeting_id',$meeting_id);
            $m->set_data('meeting_document',$meeting_document);
            $m->set_data('document_type',$document_type);
            $m->set_data('meeting_document_add_by',$user_id);
            $m->set_data('meeting_document_add_date',date('Y-m-d H:i:s'));

            $a1= array(
                'society_id'=> $m->get_data('society_id'),
                'meeting_id'=> $m->get_data('meeting_id'),
                'meeting_document'=> $m->get_data('meeting_document'),
                'document_type'=> $m->get_data('document_type'),
                'meeting_document_add_by'=> $m->get_data('meeting_document_add_by'),
                'meeting_document_add_date'=> $m->get_data('meeting_document_add_date'),
            );
           
            $q=$d->insert("meeting_documents_master",$a1);

                
            if ($q == true) {
                $response["message"] = "Document uploaded";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['inviteEmployee']=="inviteEmployee" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){ 
        
            $meeting_member_ids = explode(",",$meeting_member_ids);

            for ($i=0; $i < count($meeting_member_ids); $i++) {

                $m->set_data('society_id',$society_id);
                $m->set_data('meeting_id',$meeting_id);
                $m->set_data('user_id',$meeting_member_ids[$i]);

                $a2= array(
                    'society_id'=> $m->get_data('society_id'),
                    'meeting_id'=> $m->get_data('meeting_id'),
                    'user_id'=> $m->get_data('user_id'),
                );

                $q=$d->insert("meeting_members_master",$a2);
            }

            $meetingQry = $d->selectRow("meeting_date,meeting_time","meeting_master","meeting_id = '$meeting_id'");

            $meetingData = mysqli_fetch_array($meetingQry);

            $meeting_date = $meetingData['meeting_date'];
            $meeting_time = $meetingData['meeting_time'];

            $title = "New Meeting Invitation";
            $description = "By, ".$user_name."\nDate: ".date('D, d M Y',strtotime($meeting_date))."\nTime: ".date('h:i A',strtotime($meeting_time));

            $ids = join("','",$meeting_member_ids);

            $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$ids')");

            $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$ids')");


            $nResident->noti("new_meeting","",$society_id,$fcmArrayPA,$title,$description,$meeting_id);
            $nResident->noti_ios("new_meeting","",$society_id,$fcmArrayIosPA,$title,$meeting_id,$meeting_id);

            $d->insertUserNotificationWithId($society_id,$title,$description,"new_meeting","meetings.png","users_master.user_id IN ('$ids')",$meeting_id);

            if ($q == true) {
               
                $response["message"] = "Invitation Send";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['changeMeetingStatus']=="changeMeetingStatus" && $meeting_id != '' && $meeting_id != '0' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){ 
        

            $m->set_data('meeting_status',$meeting_status);
            $m->set_data('meeting_cancel_resson',$meeting_cancel_resson);
            $m->set_data('meeting_status_changes_time',date('Y-m-d H:i:s'));

            $a= array(
                'meeting_status'=> $m->get_data('meeting_status'),
                'meeting_cancel_resson'=> $m->get_data('meeting_cancel_resson'),
                'meeting_status_changes_time'=> $m->get_data('meeting_status_changes_time'),
            );

            $q=$d->update("meeting_master",$a,"meeting_id = '$meeting_id'");

            if ($q == true) {


                $response["message"] = "Meeting status changed";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['hostMeeting']=="hostMeeting" && $meeting_id != '' && $meeting_id != '0' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){ 
        

            $m->set_data('meeting_host_by',$user_id);

            $a= array(
                'meeting_host_by'=> $m->get_data('meeting_host_by'),
            );

            $q=$d->update("meeting_master",$a,"meeting_id = '$meeting_id'");

            if ($q == true) {

                // Status change to ongoing

                $m->set_data('meeting_status',"2");
                $m->set_data('meeting_status_changes_time',date('Y-m-d H:i:s'));

                $a= array(
                    'meeting_status'=> $m->get_data('meeting_status'),
                    'meeting_status_changes_time'=> $m->get_data('meeting_status_changes_time'),
                );

                // Status changed to joined

                $q=$d->update("meeting_master",$a,"meeting_id = '$meeting_id'");

                $m->set_data('meeting_join',"1");

                $a= array(
                    'meeting_join'=> $m->get_data('meeting_join'),
                );

                $q=$d->update("meeting_members_master",$a,"meeting_id = '$meeting_id' AND user_id = '$user_id'");

                $title = "Meeting Started";
                $description = "By, ".$user_name."\nDate: ".$meeting_date;

                $dataAry = array(
                    "meeting_id"=>$meeting_id.'',
                );

                $dataJson = json_encode($dataAry);

                $fcmArrayPA=$d->get_android_fcm("users_master,meeting_members_master","users_master.delete_status=0 AND users_master.user_token!='' AND users_master.society_id='$society_id' AND users_master.device='android' AND users_master.user_id = meeting_members_master.user_id AND meeting_members_master.meeting_id = '$meeting_id'");

                $fcmArrayIosPA=$d->get_android_fcm("users_master,meeting_members_master","users_master.delete_status=0 AND users_master.user_token!='' AND users_master.society_id='$society_id' AND users_master.device='ios' AND users_master.user_id = meeting_members_master.user_id AND meeting_members_master.meeting_id = '$meeting_id'");


                $nResident->noti("meeting_hosted","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                $nResident->noti_ios("meeting_hosted","",$society_id,$fcmArrayIosPA,$title,$meeting_id,$dataJson);

                $qryMember = $d->selectRow("meeting_members_master.*,block_master.block_name,floors_master.floor_name,users_master.user_full_name,users_master.user_profile_pic,users_master.user_designation","meeting_members_master,users_master,block_master,floors_master","meeting_members_master.meeting_id = '$meeting_id' AND users_master.user_id = meeting_members_master.user_id AND users_master.block_id = block_master.block_id AND floors_master.floor_id = users_master.floor_id");

                if (mysqli_num_rows($qryMember) > 0) {

                    $userIds = array();

                    while($dataMembers = mysqli_fetch_array($qryMember)){

                        array_push($userIds,$dataMembers['user_id']);
                    }

                    $ids = join("','",$userIds);
                }

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"meeting_hosted","meetings.png",$dataJson,"users_master.user_id IN ('$ids')");
               
                $response["message"] = "Meeting hosted";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['joinMeeting']=="joinMeeting" && $meeting_id != '' && $meeting_id != '0' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){ 
        
            $m->set_data('meeting_join',"1");

            $a= array(
                'meeting_join'=> $m->get_data('meeting_join'),
            );

            $q=$d->update("meeting_members_master",$a,"meeting_id = '$meeting_id' AND user_id = '$user_id'");

            if ($q == true) {
               
                $response["message"] = "Meeting joined";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['addMeetingDiscussion']=="addMeetingDiscussion" && $meeting_id != '' && $meeting_id != '0' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){ 
        

            $m->set_data('meeting_discussion',$meeting_discussion);

            $a= array(
                'meeting_discussion'=> $m->get_data('meeting_discussion'),
            );

            $q=$d->update("meeting_master",$a,"meeting_id = '$meeting_id'");

            if ($q == true) {
               
                $response["message"] = "Meeting discussion added";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['removeEmployee']=="removeEmployee" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){ 
        
            $q=$d->delete("meeting_members_master","meeting_id = '$meeting_id' AND user_id = '$user_id'");

            if ($q == true) {
               
                $response["message"] = "Employee removed";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['getMeetings']=="getMeetings" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $today = date('Y-m-d');
            $time = date('H:i:s');

            if ($meeting_status == 0) {                
                $query = $d->selectRow("meeting_master.*,meeting_members_master.*,users_master.user_full_name,users_master.user_profile_pic,users_master.user_designation","meeting_members_master,meeting_master,users_master","meeting_master.meeting_id = meeting_members_master.meeting_id AND meeting_members_master.user_id = '$user_id' AND users_master.user_id = meeting_master.meeting_created_by AND ((meeting_master.meeting_date = '$today'  AND meeting_master.meeting_time > '$time') OR (meeting_master.meeting_date > '$today')) AND meeting_status = '$meeting_status'","ORDER BY meeting_master.meeting_date DESC");
            }else{
                $query = $d->selectRow("meeting_master.*,meeting_members_master.*,users_master.user_full_name,users_master.user_profile_pic,users_master.user_designation","meeting_members_master,meeting_master,users_master","meeting_master.meeting_id = meeting_members_master.meeting_id AND meeting_members_master.user_id = '$user_id' AND users_master.user_id = meeting_master.meeting_created_by AND meeting_status = '$meeting_status'","ORDER BY meeting_master.meeting_date DESC");
            }


            if(mysqli_num_rows($query)>0){

                $response["meetings"] = array();

                while($data=mysqli_fetch_array($query)) {

                    $data = array_map("html_entity_decode", $data);

                    $meeting = array(); 

                    $meeting["meeting_id"] = $data['meeting_id'];
                    $meeting["meeting_title"] = $data['meeting_title'];
                    $meeting["meeting_description"] = $data['meeting_description'];
                    $meeting["meeting_location"] = $data['meeting_location'];
                    $meeting["meeting_created_by"] = $data['meeting_created_by'];
                    $meeting["meeting_host_by"] = $data['meeting_host_by'];
                    $meeting["meeting_host_name"] = $data['user_full_name'];

                    $meeting["meeting_status"] = $data['meeting_status'];

                    if ($data['meeting_status'] == 0) {
                        $meeting["meeting_status_view"] = "Upcoming";
                    }else if ($data['meeting_status'] == 1) {
                        $meeting["meeting_status_view"] = "Canceled";
                    }else if ($data['meeting_status'] == 2) {
                        $meeting["meeting_status_view"] = "Ongoing";
                    }else if ($data['meeting_status'] == 3) {
                        $meeting["meeting_status_view"] = "Completed";
                    }

                    $meeting["meeting_cancel_resson"] = $data['meeting_cancel_resson'];

                    if ($data['user_profile_pic'] != '') {
                        $meeting["meeting_host_profile"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $meeting["meeting_host_profile"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $meeting["meeting_host_designation"] = $data['user_designation'];
                    $meeting["meeting_date"] = date('D, d F Y', strtotime($data['meeting_date']));
                    $meeting["meeting_time"] = date('h:i A', strtotime($data['meeting_time']));
                   
                    $countAttendee = $d->count_data_direct("meeting_member_id","meeting_members_master","meeting_members_master.meeting_id = '$data[meeting_id]'");

                    $meeting['total_attendee'] = $countAttendee;

                    $meeting['members'] = array();

                    $qryMember = $d->selectRow("meeting_members_master.*,users_master.user_profile_pic","meeting_members_master,users_master,block_master,floors_master","meeting_members_master.meeting_id = '$data[meeting_id]' AND users_master.user_id = meeting_members_master.user_id AND users_master.block_id = block_master.block_id AND floors_master.floor_id = users_master.floor_id","");

                    if (mysqli_num_rows($qryMember) > 0) {
                        while($dataMembers = mysqli_fetch_array($qryMember)){
                            $member = array();

                            $member['meeting_member_id'] = $dataMembers['meeting_member_id'];
                            if ($dataMembers['user_profile_pic'] != '') {
                                $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $dataMembers['user_profile_pic'];
                            } else {
                                $member["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                            }

                            array_push($meeting['members'],$member);
                        }
                    }

                    array_push($response["meetings"], $meeting);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Meetings";
                $response["status"] = "201";
                echo json_encode($response);
            }           
        }else if($_POST['getMyMeetings']=="getMyMeetings" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $today = date('Y-m-d');

            $query = $d->selectRow("meeting_master.*,users_master.user_full_name,users_master.user_profile_pic,users_master.user_designation","meeting_master,users_master","users_master.user_id = meeting_master.meeting_created_by AND meeting_master.meeting_created_by = '$user_id'","ORDER BY meeting_master.meeting_date DESC");

            if(mysqli_num_rows($query)>0){
                
                $response["meetings"] = array();

                while($data=mysqli_fetch_array($query)) {

                    $data = array_map("html_entity_decode", $data);

                    $meeting = array(); 

                
                    $meeting["meeting_id"] = $data['meeting_id'];
                    $meeting["meeting_title"] = $data['meeting_title'];
                    $meeting["meeting_description"] = $data['meeting_description'];
                    $meeting["meeting_location"] = $data['meeting_location'];
                    $meeting["meeting_created_by"] = $data['meeting_created_by'];
                    $meeting["meeting_host_by"] = $data['meeting_host_by'];
                    $meeting["meeting_host_name"] = $data['user_full_name'];

                    $meeting["meeting_status"] = $data['meeting_status'];

                    if ($data['meeting_status'] == 0) {
                        $meeting["meeting_status_view"] = "Upcoming";
                    }else if ($data['meeting_status'] == 1) {
                        $meeting["meeting_status_view"] = "Canceled";
                    }else if ($data['meeting_status'] == 2) {
                        $meeting["meeting_status_view"] = "Ongoing";
                    }else if ($data['meeting_status'] == 3) {
                        $meeting["meeting_status_view"] = "Completed";
                    }

                    $meeting["meeting_cancel_resson"] = $data['meeting_cancel_resson'];

                    if ($data['user_profile_pic'] != '') {
                        $meeting["meeting_host_profile"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $meeting["meeting_host_profile"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $meetingDate = date('Y-m-d', strtotime($data['meeting_date']));

                    $meeting["meeting_host_designation"] = $data['user_designation'];
                    $meeting["meeting_date"] = date('D, d F Y', strtotime($data['meeting_date']));
                    $meeting["meeting_time"] = date('h:i A', strtotime($data['meeting_time']));

                    if ($meetingDate < $today) {
                        $meeting['is_date_gone'] = true;
                    }else{
                        $meeting['is_date_gone'] = false;                
                    }                    


                    $countAttendee = $d->count_data_direct("meeting_member_id","meeting_members_master","meeting_members_master.meeting_id = '$data[meeting_id]'");

                    $meeting['total_attendee'] = $countAttendee;

                    $meeting['members'] = array();

                    $qryMember = $d->selectRow("meeting_members_master.*,users_master.user_profile_pic","meeting_members_master,users_master,block_master,floors_master","meeting_members_master.meeting_id = '$data[meeting_id]' AND users_master.user_id = meeting_members_master.user_id AND users_master.block_id = block_master.block_id AND floors_master.floor_id = users_master.floor_id","");

                    if (mysqli_num_rows($qryMember) > 0) {
                        while($dataMembers = mysqli_fetch_array($qryMember)){
                            $member = array();

                            $member['meeting_member_id'] = $dataMembers['meeting_member_id'];
                            $member['user_id'] = $dataMembers['user_id'];
                            if ($dataMembers['user_profile_pic'] != '') {
                                $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $dataMembers['user_profile_pic'];
                            } else {
                                $member["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                            }

                            array_push($meeting['members'],$member);
                        }
                    }

                    array_push($response["meetings"], $meeting);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Meetings";
                $response["status"] = "201";
                echo json_encode($response);
            }           
        }else if($_POST['getMeetingDetails']=="getMeetingDetails" && $meeting_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $today = date('Y-m-d');

            $query = $d->selectRow("meeting_master.*,users_master.user_full_name,users_master.user_profile_pic,users_master.user_designation","meeting_master LEFT JOIN users_master ON users_master.user_id = meeting_master.meeting_host_by","meeting_master.meeting_id = '$meeting_id'");

            if (mysqli_num_rows($query) > 0) {
                // code...

                $data=mysqli_fetch_array($query);
                $data = array_map("html_entity_decode", $data);

                $response["meeting_id"] = $data['meeting_id'];
                $response["meeting_title"] = $data['meeting_title'];
                $response["meeting_description"] = $data['meeting_description'];
                $response["meeting_location"] = $data['meeting_location'];
                $response["meeting_discussion"] = $data['meeting_discussion'];
                $response["meeting_created_by"] = $data['meeting_created_by'];
                $response["meeting_host_by"] = $data['meeting_host_by'];
                $response["meeting_host_name"] = $data['user_full_name'];

                $isMeetingJoined = $d->count_data_direct("meeting_member_id","meeting_members_master","meeting_members_master.meeting_id = '$data[meeting_id]' AND user_id = '$user_id' AND meeting_join = '1'");

                if ($isMeetingJoined) {
                    $response["meeting_joined"] = true;
                }else{
                    $response["meeting_joined"] = false;                       
                }

                if ($data['meeting_host_by'] != 0) {
                    $response["meeting_hosted"] = true;
                }else{
                    $response["meeting_hosted"] = false;                       
                }

                $response["meeting_status"] = $data['meeting_status'];

                if ($data['meeting_status'] == 0) {
                    $response["meeting_status_view"] = "Upcoming";
                }else if ($data['meeting_status'] == 1) {
                    $response["meeting_status_view"] = "Canceled";
                }else if ($data['meeting_status'] == 2) {
                    $response["meeting_status_view"] = "Ongoing";
                }else if ($data['meeting_status'] == 3) {
                    $response["meeting_status_view"] = "Completed";
                }

                $response["meeting_cancel_resson"] = $data['meeting_cancel_resson'];


                if ($data['user_profile_pic'] != '') {
                    $response["meeting_host_profile"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                } else {
                    $response["meeting_host_profile"] = $base_url . "img/users/owner/user_default.png";
                }

                if ($data['meeting_file'] != '') {
                    $response["meeting_file"] = $base_url . "img/meeting_documents/" . $data['meeting_file'];
                } else {
                    $response["meeting_file"] = "";
                }
                $meetingDate = date('Y-m-d', strtotime($data['meeting_date']));

                if ($meetingDate < $today) {
                    $response['is_date_gone'] = true;
                }else{
                    $response['is_date_gone'] = false;                
                }

                $response["meeting_host_designation"] = $data['user_designation'];
                $response["meeting_date"] = date('D, d F Y', strtotime($data['meeting_date']));
                $response["meeting_time"] = date('h:i A', strtotime($data['meeting_time']));

                $response['members'] = array();
                $response['meeting_documents'] = array();

                $qryMember = $d->selectRow("meeting_members_master.*,block_master.block_name,floors_master.floor_name,users_master.user_full_name,users_master.user_profile_pic,users_master.user_designation","meeting_members_master,users_master,block_master,floors_master","meeting_members_master.meeting_id = '$data[meeting_id]' AND users_master.user_id = meeting_members_master.user_id AND users_master.block_id = block_master.block_id AND floors_master.floor_id = users_master.floor_id","LIMIT 3");

                if (mysqli_num_rows($qryMember) > 0) {
                    while($dataMembers = mysqli_fetch_array($qryMember)){
                        $member = array();

                        $member['meeting_member_id'] = $dataMembers['meeting_member_id'];
                        $member['user_id'] = $dataMembers['user_id'];
                        $member['user_full_name'] = $dataMembers['user_full_name'];
                        if ($dataMembers['user_profile_pic'] != '') {
                            $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $dataMembers['user_profile_pic'];
                        } else {
                            $member["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                        }
                        $member['user_designation'] = $dataMembers['user_designation'];
                        $member['block_name'] = $dataMembers['block_name'];
                        $member['floor_name'] = $dataMembers['floor_name'];

                        if ($dataMembers['meeting_join'] == 0) {
                            $member['meeting_joined'] = false;
                        }else{
                            $member['meeting_joined'] = true;                        
                        }

                        array_push($response['members'],$member);
                    }
                }

                $qryDoc = $d->selectRow("meeting_documents_master.*,block_master.block_name,floors_master.floor_name,users_master.user_full_name,users_master.user_profile_pic,users_master.user_designation","meeting_documents_master,users_master,block_master,floors_master","meeting_documents_master.meeting_id = '$data[meeting_id]' AND users_master.user_id = meeting_documents_master.meeting_document_add_by AND users_master.block_id = block_master.block_id AND floors_master.floor_id = users_master.floor_id");

                if (mysqli_num_rows($qryDoc) > 0) {
                    while($dataMembers = mysqli_fetch_array($qryDoc)){
                        $documents = array();

                        $documents['meeting_document_id'] = $dataMembers['meeting_document_id'];
                        $documents['user_full_name'] = $dataMembers['user_full_name'];
                        $documents['user_designation'] = $dataMembers['user_designation'];
                        $documents['block_name'] = $dataMembers['block_name'];
                        $documents['floor_name'] = $dataMembers['floor_name'];
                        if ($dataMembers['user_profile_pic'] != '') {
                            $documents["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $dataMembers['user_profile_pic'];
                        } else {
                            $documents["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                        }

                        if ($dataMembers['meeting_document'] != '') {
                            $documents["meeting_document"] = $base_url . "img/meeting_documents/" . $dataMembers['meeting_document'];
                        } else {
                            $documents["meeting_document"] = "";
                        }

                        $documents['document_type'] = $dataMembers['document_type'];
                        $documents['meeting_document_add_time'] = date('h:i A',strtotime($dataMembers['meeting_document_add_date']));                        
                        $documents['meeting_document_add_date'] = date('D, d F Y',strtotime($dataMembers['meeting_document_add_date']));
                        
                        array_push($response['meeting_documents'],$documents);
                    }
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }      
        }else if($_POST['getMeetingEmployee']=="getMeetingEmployee" && $meeting_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $response['members'] = array();

            $qryMember = $d->selectRow("meeting_members_master.*,block_master.block_name,floors_master.floor_name,users_master.user_full_name,users_master.user_profile_pic,users_master.user_designation","meeting_members_master,users_master,block_master,floors_master","meeting_members_master.meeting_id = '$meeting_id' AND users_master.user_id = meeting_members_master.user_id AND users_master.block_id = block_master.block_id AND floors_master.floor_id = users_master.floor_id");

            if (mysqli_num_rows($qryMember) > 0) {
                while($dataMembers = mysqli_fetch_array($qryMember)){
                    $member = array();

                    $member['meeting_member_id'] = $dataMembers['meeting_member_id'];
                    $member['user_id'] = $dataMembers['user_id'];
                    $member['user_full_name'] = $dataMembers['user_full_name'];
                    if ($dataMembers['user_profile_pic'] != '') {
                        $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $dataMembers['user_profile_pic'];
                    } else {
                        $member["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }
                    $member['user_designation'] = $dataMembers['user_designation'];
                    $member['block_name'] = $dataMembers['block_name'];
                    $member['floor_name'] = $dataMembers['floor_name'];

                    if ($dataMembers['meeting_join'] == 0) {
                        $member['meeting_joined'] = false;
                    }else{
                        $member['meeting_joined'] = true;                        
                    }

                    array_push($response['members'],$member);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }      
        }else if($_POST['getMembersByDepartmentMeeting']=="getMembersByDepartmentMeeting" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $qryMember = $d->selectRow("user_id","meeting_members_master","meeting_id = '$meeting_id'");

            if (mysqli_num_rows($qryMember) > 0) {

                $userIds = array();

                while($dataMembers = mysqli_fetch_array($qryMember)){

                    array_push($userIds,$dataMembers['user_id']);
                }

                $ids = join("','",$userIds);
            }


            $unit_data=$d->selectRow("users_master.user_id,users_master.unit_id,users_master.block_id,users_master.floor_id,users_master.user_first_name,users_master.user_last_name,users_master.user_mobile,users_master.user_full_name,users_master.society_id,users_master.user_designation,users_master.user_profile_pic","unit_master,users_master","users_master.unit_id=unit_master.unit_id AND users_master.floor_id ='$floor_id' AND users_master.delete_status=0  AND users_master.society_id ='$society_id' AND users_master.user_status=1 AND users_master.user_id NOT IN ('$ids')","ORDER BY user_sort ASC");

        

            if (mysqli_num_rows($unit_data) > 0) {

                $response["employees"] = array();

                while($data_units_list=mysqli_fetch_array($unit_data)) {
                                 
                    $employees = array(); 
         
                    $user_full_name= $data_units_list['user_full_name'];

                    $units_id=$data_units_list['unit_id'];

                    $employees["user_id"]=$data_units_list['user_id'].'';
                    $employees["unit_id"]=$data_units_list['unit_id'].'';
                    $employees["block_id"]=$data_units_list['block_id'].'';
                    $employees["floor_id"]=$data_units_list['floor_id'].'';
                    $employees["user_first_name"]=$data_units_list['user_first_name'].'';
                    $employees["user_last_name"]=$data_units_list['user_last_name'].'';
                    $employees["user_mobile"]=$data_units_list['user_mobile'].'';
                    $employees["user_full_name"]=html_entity_decode($user_full_name).'';
                    $employees["society_id"]=$data_units_list['society_id'];
                    $employees["designation"]=$data_units_list['user_designation'];
                    
                    if ($data_units_list['user_profile_pic'] != '') {
                        $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                    } else {
                        $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    array_push($response["employees"], $employees);
                }

                $response["message"]="Employee list";
                $response["status"]="200";
                echo json_encode($response);
        
            }else{
                $response["message"]="No Employees Found";
                $response["status"]="201";
                echo json_encode($response);
            }

        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

?>