    <?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') { 

    $response = array();
    extract(array_map("test_input" , $_POST));

    if($_POST['getMembers']=="getMembers" && filter_var($society_id, FILTER_VALIDATE_INT) == true  ){

            $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0");

        
        if(mysqli_num_rows($block_data)>0){

                $response["block"] = array();

            while($data_block_list=mysqli_fetch_array($block_data)) {

            $block = array(); 

            $block["block_id"]=$data_block_list['block_id'];
            $block_id=$data_block_list['block_id'];
            $block["society_id"]=$data_block_list['society_id'];
            $block["block_name"]=$data_block_list['block_name'];
            $block["block_status"]=$data_block_list['block_status'];
            $block["floors"] = array();

            $floor_data=$d->select("floors_master","block_id ='$block_id'");

              while($data_floor_list=mysqli_fetch_array($floor_data)) {

                    $countUsers=$d->count_data_direct("user_id","users_master","society_id!=0 AND society_id='$society_id' AND  block_id='$data_block_list[block_id]' AND  floor_id='$data_floor_list[floor_id]'");
                    if ($countUsers>0) {
                       
                        $floors = array(); 

                        $floors["floor_id"]=$data_floor_list['floor_id'];
                        $floors_id=$data_floor_list['floor_id'];


                        $floors["society_id"]=$data_floor_list['society_id'];
                        $floors["block_id"]=$data_floor_list['block_id'];
                        $floors["floor_name"]=$data_floor_list['floor_name'];
                        $floors["floor_status"]=$data_floor_list['floor_status'];


                        $floors["units"] = array();

                        $unit_data=$d->select("unit_master,users_master","unit_master.unit_id=users_master.unit_id AND unit_master.floor_id ='$floors_id' AND users_master.member_status!=1 AND users_master.user_status=1","ORDER BY unit_master.unit_id ASC");

                        while($data_units_list=mysqli_fetch_array($unit_data)) {

                            $units = array(); 

                            $units["unit_id"]=$data_units_list['unit_id'];
                            $units_id=$data_units_list['unit_id'];

                            $units["society_id"]=$data_units_list['society_id'];
                            $units["floor_id"]=$data_units_list['floor_id'];
                            $units["unit_name"]=$data_units_list['unit_name'];
                            $units["unit_status"]=$data_units_list['unit_status'];
                            $units["user_id"]=$data_units_list['user_id'];
                            $units["public_mobile"]=$data_units_list['public_mobile'];
                            $units["society_id"]=$data_units_list['society_id'];
                            $units["user_full_name"]=$data_units_list['user_full_name'];
                            $units["user_mobile"]=$data_units_list['user_mobile'];
                            $units["user_email"]=$data_units_list['user_email'];
                            $units["user_type"]=$data_units_list['user_type'];
                            $units["user_block_id"]=$data_units_list['block_id'];
                            $units["user_floor_id"]=$data_units_list['floor_id'];
                            $units["user_unit_id"]=$data_units_list['unit_id'];
                            $units["user_status"]=$data_units_list['user_status'];

                            $user_id = $data_units_list['user_id'];

                            $qfamily = $d->select("users_master", "unit_id='$units_id'");
                            $units["member"] = array();
                            $units["family_count"]=mysqli_num_rows($qfamily)."";
                            $chatCount=0;
                            while ($datafamily = mysqli_fetch_array($qfamily)) {

                                $member = array();
                                $member["user_id"] = $datafamily['user_id'];
                                $userId=$datafamily['user_id'];

                                $qchatCount=$d->select("chat_master","msg_for='$my_id' AND msg_by='$userId' AND society_id='$society_id' AND msg_status='0'");
        
                                $member["member_chat"] = mysqli_num_rows($qchatCount)."";
                                $member["user_first_name"] = $datafamily['user_first_name'];
                                $member["user_last_name"] = $datafamily['user_last_name'];
                                $member["user_mobile"] = $datafamily['user_mobile'];
                                $member["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $datafamily['user_profile_pic'];
                                $member["member_date_of_birth"] = $datafamily['member_date_of_birth'];
                                $member["member_age"] = "";
                                $member["member_relation_name"] = $datafamily['member_relation_name'];
                                $member["user_status"] = $datafamily['user_status'];
                                $member["member_status"] = $datafamily['member_status'];

                                $chatCount+=mysqli_num_rows($qchatCount);

                                array_push($units["member"], $member);

                              }
                            $units["chat_status"]="$chatCount";

                            array_push($floors["units"], $units);
                        }
                        array_push($block["floors"], $floors);
                    }
                }

                array_push($response["block"], $block); 
            }

          
            $totalPop=$d->count_data_direct("user_id","users_master","society_id!=0 AND society_id='$society_id' AND user_status=1"); 
            $response["population"]= $totalPop;

            $response["message"]="Get Member success.";
            $response["status"]="200";
            echo json_encode($response);
    
        }else{

            $response["message"]="Get Member Fail.";
            $response["status"]="201";
            echo json_encode($response);
    
        }

    } else if($_POST['getBlocks']=="getBlocks" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

        $block_data=$d->select("block_master","society_id ='$society_id' AND society_id!=0");

        if(mysqli_num_rows($block_data)>0){

            $response["block"] = array();

            
            while($data_block_list=mysqli_fetch_array($block_data)) {

                $block = array();

                $block["block_id"]=$data_block_list['block_id'];
                $block["society_id"]=$data_block_list['society_id'];
                $block["block_name"]=$data_block_list['block_name'];
                $block["block_status"]=$data_block_list['block_status'];
                
                array_push($response["block"], $block); 
            }

            $response["message"]="Get Block success.";
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]="Get Block Fail.";
            $response["status"]="201";
            echo json_encode($response);
        
    }


}else if($_POST['getFloorandUnit']=="getFloorandUnit" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($block_id, FILTER_VALIDATE_INT) == true ){

        $block_data=$d->select("floors_master","society_id ='$society_id' AND society_id!=0 AND block_id ='$block_id' AND block_id!=0");


        if(mysqli_num_rows($block_data)>0){
            $response["floors"] = array();

            while($data_floor_list=mysqli_fetch_array($block_data)) {

                $floors = array(); 
                $floors["floor_id"]=$data_floor_list['floor_id'];
                $floors_id=$data_floor_list['floor_id'];
                $floors["society_id"]=$data_floor_list['society_id'];
                $floors["block_id"]=$data_floor_list['block_id'];
                $floors["floor_name"]=$data_floor_list['floor_name'];
                $floors["floor_status"]=$data_floor_list['floor_status'];
                
                $floors["units"] = array();

                $unit_data=$d->select("unit_master","floor_id ='$floors_id'");

                while($data_units_list=mysqli_fetch_array($unit_data)) {

                    $units = array(); 

                    $units["unit_id"]=$data_units_list['unit_id'];
                    $unit_id=$data_units_list['unit_id'];

                    $units["society_id"]=$data_units_list['society_id'];
                    $units["floor_id"]=$data_units_list['floor_id'];
                    $units["unit_name"]=$data_units_list['unit_name'];
                    $units["unit_status"]=$data_units_list['unit_status'];
                    $units["unit_type"]=$data_units_list['unit_type'];

                    array_push($floors["units"], $units);


                }

                array_push($block["floors"], $floors);
                array_push($response["floors"], $floors);

            }

            $response["message"]="Get Block success.";
            $response["status"]="200";
            echo json_encode($response);
        
        }else{

            $response["message"]="Get Block Fail.";
            $response["status"]="201";
            echo json_encode($response);
        
    }


    }else{
        $response["message"]="wrong tag";
        $response["status"]="201";
        echo json_encode($response);
    }
}
else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
?>