<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {


  if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input", $_POST));

      if ($_POST['getVotingListNew'] == "getVotingListNew" && $society_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
      $fq = $d->select("users_master", "unit_id='$unit_id' AND user_id='$user_id'");
      $ownerData = mysqli_fetch_array($fq);
      $userType = $ownerData['user_type'];
      $member_status = $ownerData['member_status'];
      
          $qvoting = $d->select("voting_master", "((society_id='$society_id') AND poll_for=0 OR  poll_for='$floor_id' )", "ORDER BY voting_id DESC");
       
      if (mysqli_num_rows($qvoting) > 0) {
        $response["voting"] = array();
        $response["voting_completed"] = array();

        $poll_yet_not_start = $xml->string->poll_yet_not_start;
        $closed = $xml->string->closed;
        $open = $xml->string->open;
        while ($data_voting_list = mysqli_fetch_array($qvoting)) {

          $voting = array();
          $voting_start_date = $data_voting_list['voting_start_date'];

          $voting["voting_id"] = $data_voting_list['voting_id'];
          $voting["society_id"] = $data_voting_list['society_id'];
          $voting["voting_question"] = html_entity_decode($data_voting_list['voting_question']);
          $voting["voting_description"] = html_entity_decode($data_voting_list['voting_description']);
          $voting["voting_start_date"] = date("d M Y h:i A", strtotime($data_voting_list['voting_start_date']));
          $voting["voting_end_date"] = date("d M Y h:i A", strtotime($data_voting_list['voting_end_date']));

          if ($data_voting_list['voting_attachment'] != '') {
            $voting["voting_attachment"] = $base_url . "img/voting_doc/" . $data_voting_list['voting_attachment'];

          }else{
            $voting["voting_attachment"] = "";
          }

          $expire = strtotime($data_voting_list['voting_end_date']);
          $startTime = strtotime($data_voting_list['voting_start_date']);
          $cTime= date("H:i:s");
          $today = strtotime("today $cTime");
          if ($data_voting_list['voting_status']==1 || $today > $expire) {
            $voting["voting_status"] = '1';
            $voting["voting_status_view"] = "$closed";
            array_push($response["voting_completed"], $voting);
          } else if ($today < $startTime) { 
            $voting["voting_status"] = $data_voting_list['voting_status'];
            $voting["voting_status_view"] = "$poll_yet_not_start";
            array_push($response["voting"], $voting);

          } else {
            $voting["voting_status"] = $data_voting_list['voting_status'];
            $voting["voting_status_view"] = "$open";
            array_push($response["voting"], $voting);

          }
        }
        $response["message"] = "Get Poll Success.";
        $response["status"] = "200";
        echo json_encode($response);
      } else {

        $response["message"] = "No voting Found.";
        $response["status"] = "201";
        echo json_encode($response);
      }
    } else if ($_POST['getVotingOptionList'] == "getVotingOptionList" && filter_var($voting_id, FILTER_VALIDATE_INT) == true) {


      $count5 = $d->sum_data("option_count", "voting_option_master", "voting_id='$voting_id'");
      $row = mysqli_fetch_array($count5);
      $totalVoting = $row['SUM(option_count)'];

      $pq= $d->selectRow("poll_for","voting_master","voting_id='$voting_id'");
      $pollData=mysqli_fetch_array($pq);
      $poll_for = $pollData['poll_for'];

      $voting_option_data = $d->select("voting_option_master", "voting_id ='$voting_id'");

      if (mysqli_num_rows($voting_option_data) > 0) {

        $response["option"] = array();

        while ($data_voting_option_list = mysqli_fetch_array($voting_option_data)) {

          $sv = $d->sum_data("option_count", "voting_option_master", "voting_id='$voting_id' AND voting_option_id='$data_voting_option_list[voting_option_id]'");
          $row1 = mysqli_fetch_array($sv);
          $singleVoting = $row1['SUM(option_count)'];

          $singleVoting = $singleVoting * 100;
          if($totalVoting != 0){
          $votingPer = number_format($singleVoting /  $totalVoting);
          }else{
            $votingPer ="0";
          }
          $option = array();
          $option["voting_option_id"] = $data_voting_option_list['voting_option_id'];
          $option["society_id"] = $data_voting_option_list['society_id'];
          $option["voting_id"] = $data_voting_option_list['voting_id'];
          $option["option_name"] = $data_voting_option_list['option_name'];
          $option["votingPer"] = $votingPer;

          array_push($response["option"], $option);
        }

        if ($poll_for==1 || $poll_for==2) {
          $voting_result_data = $d->select("voting_result_master", "voting_id='$voting_id' AND unit_id ='$unit_id' AND user_id!=0");
          
        } else {
          $voting_result_data = $d->select("voting_result_master", "voting_id='$voting_id' AND unit_id ='$unit_id' AND user_id ='$user_id' AND user_id!=0");
        }


        if (mysqli_num_rows($voting_result_data) > 0) {
          $response["voting_submitted"] = "200";
        } else {
          $response["voting_submitted"] = "201";
        }

        $qvoting = $d->select("voting_master", " voting_id ='$voting_id'", "");
        $data_voting_list = mysqli_fetch_array($qvoting);
        $expire = strtotime($data_voting_list['voting_end_date']);
        $startTime = strtotime($data_voting_list['voting_start_date']);
        $cTime= date("H:i:s");
        $today = strtotime("today $cTime");
        if ($data_voting_list['voting_status']==1 || $today > $expire) {
          $response["voting_status"] = '1';
          $response["voting_status_view"] = 'Poll Closed';
        } else if ($today < $startTime) { 
          $response["voting_status"] = '2';
          $response["voting_status_view"] = 'Poll not yet started';

        } else {
          $response["voting_status"] = $data_voting_list['voting_status'];
          $response["voting_status_view"] = '';

        }

        $response["totalVoting"] = $totalVoting;
        $response["message"] = "Get Voting Option success.";
        $response["status"] = "200";
        echo json_encode($response);
      } else {

        $response["message"] = "No Voting Option Found.";
        $response["status"] = "201";
        echo json_encode($response);
      }
    } else if ($_POST['addVote'] == "addVote" && $society_id != '' && $unit_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {


      $qqq = $d->select("voting_result_master", "unit_id='$unit_id' AND user_id='$user_id' AND voting_id='$voting_id'");
      $voteData = mysqli_fetch_array($qqq);
      if ($voteData > 0) {
        $response["status"] = "201";
        $response["message"] = "You have already voted for this poll..!";
        echo json_encode($response);
        exit();
      }

      $today = date("Y-m-d H:i");
      $qv = $d->select("voting_master", "society_id='$society_id' AND voting_id='$voting_id' AND voting_start_date>='$today'");
      $vData = mysqli_fetch_array($qv);
      if ($vData > 0) {
        $response["status"] = "201";
        $startDate = date("d M Y h:i A", strtotime($vData['voting_start_date'].' '. $vData['voting_start_time']));
        $response["message"] = "This poll voting start on : $startDate";
        echo json_encode($response);
        exit();
      }

      $m->set_data('voting_id', $voting_id);
      $m->set_data('unit_id', $unit_id);
      $m->set_data('user_id', $user_id);
      $m->set_data('society_id', $society_id);
      $m->set_data('admin_id', $admin_id);
      $m->set_data('voting_option_id', $voting_option_id);

      $a = array(
        'voting_id' => $m->get_data('voting_id'),
        'unit_id' => $m->get_data('unit_id'),
        'user_id' => $m->get_data('user_id'),
        'society_id' => $m->get_data('society_id'),
        'admin_id' => $m->get_data('admin_id'),
        'voting_option_id' => $m->get_data('voting_option_id')
      );

      $q = $d->insert('voting_result_master', $a);

      $voting_option_data2 = $d->select("voting_option_master", "voting_option_id ='$voting_option_id'");
      if (mysqli_num_rows($voting_option_data2) > 0) {
        $data = mysqli_fetch_array($voting_option_data2);
        $option_count = $data['option_count'];
      }
      $option_count = $option_count + 1;

      $a1 = array(
        'option_count' => $option_count
      );

      $q1 = $d->update('voting_option_master', $a1, "voting_option_id ='$voting_option_id'");

      if ($q == true) {
         
         $d->insert_myactivity($user_id,"$society_id","0","$user_name","Participate in poll","Pollsxxxhdpi.png");

        $response["status"] = "200";
        $response["message"] = "Thank you for Vote..!";
        echo json_encode($response);
      } else {
        $response["status"] = "201";
        $response["message"] = "Something Wrong...!";
        echo json_encode($response);
      }
    } else if ($_POST['getVotingResult'] == "getVotingResult" && $voting_id != '' && filter_var($voting_id, FILTER_VALIDATE_INT) == true) {

      $voting_option_data = $d->select("voting_option_master", "voting_id ='$voting_id'", "ORDER BY option_count DESC");

      $count5 = $d->sum_data("option_count", "voting_option_master", "voting_id='$voting_id'");
      $row = mysqli_fetch_array($count5);
      $totalVoting = $row['SUM(option_count)'];

      if (mysqli_num_rows($voting_option_data) > 0) {

        $response["result"] = array();

        while ($data_voting_option_list = mysqli_fetch_array($voting_option_data)) {

          $sv = $d->sum_data("option_count", "voting_option_master", "voting_id='$voting_id' AND voting_option_id='$data_voting_option_list[voting_option_id]'");
          $row1 = mysqli_fetch_array($sv);
          $singleVoting = $row1['SUM(option_count)'];

          $singleVoting = $singleVoting * 100;
          if($totalVoting != 0){
          $votingPer = number_format($singleVoting /  $totalVoting);
          }else{
            $votingPer = "0";
          }

          $result = array();
          $result["given_vote"] = $data_voting_option_list['option_count'];
          $result["option_name"] = $data_voting_option_list['option_name'];
          $result["votingPer"] = $votingPer;
          array_push($response["result"], $result);
        }

        $voting_result_data = $d->select("voting_result_master", "voting_id='voting_id'AND unit_id ='$unit_id'");

        if (mysqli_num_rows($voting_result_data) > 0) {
          $response["voting_submitted"] = "200";
        } else {
          $response["voting_submitted"] = "201";
        }


        $response["totalVoting"] = $totalVoting;
        $response["message"] = "Get Voting Option success.";
        $response["status"] = "200";
        echo json_encode($response);
      } else {

        $response["message"] = "No Voting Option Found.";
        $response["status"] = "201";
        echo json_encode($response);
      }
    } else {
      $response["message"] = "wrong tag.";
      $response["status"] = "201";
      echo json_encode($response);
    }
  } else {

    $response["message"] = "wrong api key.";
    $response["status"] = "201";
    echo json_encode($response);
  }
}
