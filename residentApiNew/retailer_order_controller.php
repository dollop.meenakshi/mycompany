<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST)){
	extract(array_map("test_input", $_POST));
	$response = array();

	if($_POST['addNoOrder']=="addNoOrder" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 
		
		$order_date = date('Y-m-d H:i:s');
		$date = date('Y-m-d');

    	$qry1 = $d->count_data_direct("retailer_no_order_id","retailer_no_order_master","retailer_id = '$retailer_id' AND visit_by_user_id = '$user_id' AND DATE_FORMAT(no_order_created_date,'%Y-%m-%d') = '$date'");

    	if ($qry1 > 0) {
    		$response["message"] = "No order already added for the day";
			$response["status"] = "201";
			echo json_encode($response);
			exit();
    	}


		$m->set_data('society_id', $society_id);
		$m->set_data('visit_by_user_id', $user_id);
		$m->set_data('retailer_visit_id', $retailer_visit_id);
		$m->set_data('retailer_id', $retailer_id);
		$m->set_data('no_order_type_id', $no_order_type_id);
		$m->set_data('no_order_reason', $no_order_reason);
		$m->set_data('no_order_remark', $no_order_remark);
		$m->set_data('no_order_latitude', $no_order_latitude);
		$m->set_data('no_order_longitude', $no_order_longitude);
		$m->set_data('no_order_gps_accuracy', $no_order_gps_accuracy);
		$m->set_data('no_order_battery_status', $no_order_battery_status);
		$m->set_data('no_order_mock_location', $no_order_mock_location);
		$m->set_data('no_order_mock_location_app_name', $no_order_mock_location_app_name);
		$m->set_data('no_order_created_date', date('Y-m-d H:i:s'));

		$ary = array(
			'society_id' => $m->get_data('society_id'),
			'visit_by_user_id' => $m->get_data('visit_by_user_id'),
			'retailer_visit_id' => $m->get_data('retailer_visit_id'),
			'retailer_id' => $m->get_data('retailer_id'),
			'no_order_type_id' => $m->get_data('no_order_type_id'),
			'no_order_reason' => $m->get_data('no_order_reason'),
			'no_order_remark' => $m->get_data('no_order_remark'),
			'no_order_latitude' => $m->get_data('no_order_latitude'),
			'no_order_longitude' => $m->get_data('no_order_longitude'),
			'no_order_gps_accuracy' => $m->get_data('no_order_gps_accuracy'),
			'no_order_battery_status' => $m->get_data('no_order_battery_status'),
			'no_order_mock_location' => $m->get_data('no_order_mock_location'),
			'no_order_mock_location_app_name' => $m->get_data('no_order_mock_location_app_name'),
			'no_order_created_date' => $m->get_data('no_order_created_date'),
		);

		$qry = $d->insert("retailer_no_order_master",$ary);

		if ($qry == true) {

			$qry11 = $d->selectRow("retailer_latitude,retailer_longitude","retailer_master","retailer_id = '$retailer_id' AND retailer_longitude = '' AND retailer_latitude = ''");

			if (mysqli_num_rows($qry11) <= 0) {
				$ary2 = array(
					'retailer_latitude' => $m->get_data('no_order_latitude'),
					'retailer_longitude' => $m->get_data('no_order_longitude')
				);

				$qqq = $d->update("retailer_master",$ary2,"retailer_id = '$retailer_id'");
			}

			if ($retailer_daily_visit_timeline_id > 0) {
				$m->set_data('order_taken', "2");

				$ary = array(
					'order_taken' => $m->get_data('order_taken'),
				);

				$qqq1 = $d->update("retailer_daily_visit_timeline_master",$ary,"retailer_daily_visit_timeline_id = '$retailer_daily_visit_timeline_id' AND retailer_visit_by_id = '$user_id'");
			}


			$response["message"] = "No order added";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "Something went wrong!";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getMyNoOrder']=="getMyNoOrder" && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	

		$qry = $d->select(" 
			retailer_no_order_master,
			retailer_master
			","
			retailer_no_order_master.visit_by_user_id = '$user_id' 
			AND retailer_no_order_master.retailer_id = retailer_master.retailer_id ");

		if (mysqli_num_rows($qry) > 0) {
		
			$response["no_orders"] = array();

			while($data = mysqli_fetch_array($qry)){
				$noOrders = array();

				$retailer_no_order_id = $data['retailer_no_order_id'];

				$noOrders['retailer_no_order_id'] = $retailer_no_order_id;
				$noOrders['society_id'] = $data['society_id'];
				$noOrders['visit_by_user_id'] = $data['visit_by_user_id'];
				$noOrders['retailer_id'] = $data['retailer_id'];
				$noOrders['no_order_type_id'] = $data['no_order_type_id'];
				$noOrders['no_order_type'] = $data['no_order_reason'];
				$noOrders['retailer_name'] = $data['retailer_name'];
				$noOrders['retailer_contact_person'] = $data['retailer_contact_person'];
				$noOrders['retailer_contact_person_number'] = $data['retailer_contact_person_number'];
				$noOrders['retailer_address'] = $data['retailer_address'];
				$noOrders['retailer_google_address'] = $data['retailer_google_address'];
				$noOrders['retailer_latitude'] = $data['retailer_latitude'];
				$noOrders['retailer_longitude'] = $data['retailer_longitude'];
				$noOrders['no_order_reason'] = $data['no_order_reason'];
				$noOrders['no_order_remark'] = $data['no_order_remark'];
				$noOrders['no_order_latitude'] = $data['no_order_latitude'];
				$noOrders['no_order_longitude'] = $data['no_order_longitude'];
				$noOrders['no_order_gps_accuracy'] = $data['no_order_gps_accuracy'];
				$noOrders['no_order_battery_status'] = $data['no_order_battery_status'];
				$noOrders['no_order_mock_location'] = $data['no_order_mock_location'];
				$noOrders['no_order_mock_location_app_name'] = $data['no_order_mock_location_app_name'];						

				$noOrders['no_order_created_date'] = date('dS F Y',strtotime($data['no_order_created_date']));
				
				array_push($response["no_orders"],$noOrders);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getNoOrderType']=="getNoOrderType" && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	

		$qry = $d->select("no_order_type","society_id = '$society_id'");

		if (mysqli_num_rows($qry) > 0) {
		
			$response["no_order_types"] = array();

			while($data = mysqli_fetch_array($qry)){
				$noOrderType = array();

				$no_order_type_id = $data['no_order_type_id'];

				$noOrderType['no_order_type_id'] = $no_order_type_id;
				$noOrderType['no_order_type'] = $data['no_order_type'];
				
				array_push($response["no_order_types"],$noOrderType);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getMyOrders']=="getMyOrders" && filter_var($society_id, FILTER_VALIDATE_INT) == true){  

    	$appendQuery = "";

    	if ($retailer_id!='') {
      		$appendQuery = $appendQuery." AND retailer_order_master.retailer_id = '$retailer_id' ";
      	}  
    	if ($user_id!='') {
      		$appendQuery = $appendQuery." AND retailer_order_master.order_by_user_id = '$user_id'";
      	}

      	$appendQuery1 = "";

		if ($filter_type == 0 && $filter_type != '') {

			$filterDate = date('Y-m-d');
			$appendQuery1 = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 1 && $filter_type != '') {

	   		$filterDate = date('Y-m-d',strtotime('-1 days'));
			$appendQuery1 = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 2 && $filter_type != '') {

	   		$year = date('Y');
	   		$week = date('W',strtotime(date('Y-m-d')));
	   		
	   		$time = strtotime("1 January $year", time());
			$day = date('w', $time);
			$time += ((7*$week)+1-$day)*24*3600;
			$startDate = date('Y-m-d', $time);
			$time += 6*24*3600;
			$endDate = date('Y-m-d', $time);

			$appendQuery1 = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";

	   	}else if ($filter_type == 3 && $filter_type != '') {

	   		$currentMonth = date('Y-m');
			$appendQuery1 = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m') = '$currentMonth'";
	   	}



		$qry = $d->selectRow("
			retailer_order_master.*,
			retailer_master.retailer_name,
			retailer_master.retailer_contact_person,
			retailer_master.retailer_contact_person_number,
			retailer_master.retailer_contact_person_country_code,
			retailer_master.retailer_address,
			distributor_master.distributor_name,
			distributor_master.distributor_contact_person,
			distributor_master.distributor_contact_person_number,
			distributor_master.distributor_address,
			users_master.user_full_name
			","
			retailer_order_master,
			retailer_master,
			users_master,
			distributor_master
			","
			retailer_order_master.society_id = '$society_id' 
			AND retailer_order_master.retailer_id = retailer_master.retailer_id	
			AND users_master.user_id = retailer_order_master.order_by_user_id	
			AND retailer_order_master.distributor_id = distributor_master.distributor_id $appendQuery $appendQuery1","ORDER BY retailer_order_master.order_date DESC");
		
		$response["my_orders"] = array();
		
		if (mysqli_num_rows($qry) > 0) {		

			while($data = mysqli_fetch_array($qry)){
				$order = array();

				switch ($order_status) {
					case '1':
						$order_status_view = "Confirm";
						break;
					case '2':
						$order_status_view = "Rejected";
						break;
					case '3':
						$order_status_view = "Cancelled";
						break;
					default:
						$order_status_view = "Pending";
						break;
				}

				$retailer_order_id = $data['retailer_order_id'];

				$order['retailer_order_id'] = $retailer_order_id;
				$order['retailer_visit_id'] = $data['retailer_visit_id'];
				$order['order_by_user_id'] = $data['order_by_user_id'];
				$order['order_status_view'] = $data['order_status_view'];
				$order['order_by_user_name'] = $data['user_full_name'];
				$order['retailer_id'] = $data['retailer_id'];
				$order['retailer_name'] = $data['retailer_name'];
				$order['retailer_contact_person'] = $data['retailer_contact_person'];
				$order['retailer_contact_person_number'] = $data['retailer_contact_person_country_code'].' '.$data['retailer_contact_person_number'];
				$order['retailer_address'] = $data['retailer_address'];
				$order['distributor_id'] = $data['distributor_id'];
				$order['distributor_name'] = $data['distributor_name'];
				$order['distributor_contact_person'] = $data['distributor_contact_person'];
				$order['distributor_contact_person_number'] = $data['distributor_contact_person_number'];
				$order['distributor_address'] = $data['distributor_address'];
				$order['order_date'] = date('h:i A, d M Y',strtotime($data['order_date']));
				$order['order_descripion'] = $data['order_descripion'];
				$order['order_status'] = $data['order_status'];

				if ($data['order_status'] == 0) {
					$order['order_status_view'] = "Pending";
				}else if ($data['order_status'] == 1) {
					$order['order_status_view'] = "Approved";
				}else if ($data['order_status'] == 2) {
					$order['order_status_view'] = "Rejected";
				}else if ($data['order_status'] == 3) {
					$order['order_status_view'] = "Cancelled";
				}

				$order['order_reject_reason'] = $data['order_reject_reason'];
				$order['order_total_amount'] = $data['order_total_amount'];
				$order['order_discount_amount'] = $data['order_discount_amount'];
				$order['order_discount_percentage'] = $data['order_discount_percentage'];
				$order['total_order_product_qty'] = $data['total_order_product_qty'];
				$order['order_remark'] = $data['order_remark'];
				$order['order_placed_via'] = $data['order_placed_via'];
				$order['order_data'] = $data['order_data'];

				
				array_push($response["my_orders"],$order);
			}
		}

		$qry12 = $d->select(" 
			retailer_no_order_master,
			retailer_master
			","
			retailer_no_order_master.visit_by_user_id = '$user_id' 
			AND retailer_no_order_master.retailer_id = retailer_master.retailer_id ","ORDER BY retailer_no_order_master.no_order_created_date DESC");

		$response["no_orders"] = array();

		if (mysqli_num_rows($qry12) > 0) {

			while($data1 = mysqli_fetch_array($qry12)){
				$noOrders = array();

				$retailer_no_order_id = $data1['retailer_no_order_id'];

				$noOrders['retailer_no_order_id'] = $retailer_no_order_id;
				$noOrders['society_id'] = $data1['society_id'];
				$noOrders['visit_by_user_id'] = $data1['visit_by_user_id'];
				$noOrders['retailer_id'] = $data1['retailer_id'];
				$noOrders['no_order_type_id'] = $data1['no_order_type_id'];
				$noOrders['no_order_type'] = $data1['no_order_reason'];
				$noOrders['retailer_name'] = $data1['retailer_name'];
				$noOrders['retailer_contact_person'] = $data1['retailer_contact_person'];
				$noOrders['retailer_contact_person_number'] = $data1['retailer_contact_person_number'];
				$noOrders['retailer_address'] = $data1['retailer_address'];
				$noOrders['retailer_google_address'] = $data1['retailer_google_address'];
				$noOrders['retailer_latitude'] = $data1['retailer_latitude'];
				$noOrders['retailer_longitude'] = $data1['retailer_longitude'];
				$noOrders['no_order_reason'] = $data1['no_order_reason'];
				$noOrders['no_order_remark'] = $data1['no_order_remark'];
				$noOrders['no_order_latitude'] = $data1['no_order_latitude'];
				$noOrders['no_order_longitude'] = $data1['no_order_longitude'];
				$noOrders['no_order_gps_accuracy'] = $data1['no_order_gps_accuracy'];
				$noOrders['no_order_battery_status'] = $data1['no_order_battery_status'];
				$noOrders['no_order_mock_location'] = $data1['no_order_mock_location'];
				$noOrders['no_order_mock_location_app_name'] = $data1['no_order_mock_location_app_name'];						

				$noOrders['no_order_created_date'] = date('h:i A, d M Y',strtotime($data1['no_order_created_date']));
				
				array_push($response["no_orders"],$noOrders);
			}
		}


		
		$response["message"] = "Success";
		$response["status"] = "200";
		echo json_encode($response);
		              
    }else if($_POST['getMyOrdersSummary']=="getMyOrdersSummary" && filter_var($society_id, FILTER_VALIDATE_INT) == true){  

	   	$todayDate = date("Y-m-d");
	   	$today = date("Y-m-d");
		$today = date('Y-m-d', strtotime($today . ' +1 day'));
		$begin = date('Y-m-d', strtotime($today . ' -1 month'));
		$begin = new DateTime($begin);
		$end   = new DateTime($today);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		$reverse = array_reverse(iterator_to_array($period)); 

		$response['work_summary'] = array();

		foreach ($reverse as $dt) {
			$summary = array();
			$date = $dt->format("Y-m-d");
			$summary['date'] = $dt->format("d M Y");
			$summary['orders'] = array();
			$summary['no_orders'] = array();

			$qry = $d->selectRow("
				retailer_order_master.*,
				retailer_master.retailer_name,
				retailer_master.retailer_contact_person,
				retailer_master.retailer_contact_person_number,
				retailer_master.retailer_contact_person_country_code,
				retailer_master.retailer_address,
				distributor_master.distributor_name,
				distributor_master.distributor_contact_person,
				distributor_master.distributor_contact_person_number,
				distributor_master.distributor_address,
				users_master.user_full_name
				","
				retailer_order_master,
				retailer_master,
				users_master,
				distributor_master
				","
				retailer_order_master.society_id = '$society_id' 
				AND retailer_order_master.retailer_id = retailer_master.retailer_id	
				AND users_master.user_id = retailer_order_master.order_by_user_id	
				AND retailer_order_master.order_by_user_id = '$user_id'
				AND retailer_order_master.distributor_id = distributor_master.distributor_id AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$date'");

			if (mysqli_num_rows($qry) > 0) {
			
				while($data = mysqli_fetch_array($qry)){
					$order = array();

					switch ($order_status) {
						case '1':
							$order_status_view = "Confirm";
							break;
						case '2':
							$order_status_view = "Rejected";
							break;
						case '3':
							$order_status_view = "Cancelled";
							break;
						default:
							$order_status_view = "Pending";
							break;
					}

					$retailer_order_id = $data['retailer_order_id'];

					$order['retailer_order_id'] = $retailer_order_id;
					$order['retailer_visit_id'] = $data['retailer_visit_id'];
					$order['order_by_user_id'] = $data['order_by_user_id'];
					$order['order_status_view'] = $data['order_status_view'];
					$order['order_by_user_name'] = $data['user_full_name'];
					$order['retailer_id'] = $data['retailer_id'];
					$order['retailer_name'] = $data['retailer_name'];
					$order['retailer_contact_person'] = $data['retailer_contact_person'];
					$order['retailer_contact_person_number'] = $data['retailer_contact_person_country_code'].' '.$data['retailer_contact_person_number'];
					$order['retailer_address'] = $data['retailer_address'];
					$order['distributor_id'] = $data['distributor_id'];
					$order['distributor_name'] = $data['distributor_name'];
					$order['distributor_contact_person'] = $data['distributor_contact_person'];
					$order['distributor_contact_person_number'] = $data['distributor_contact_person_number'];
					$order['distributor_address'] = $data['distributor_address'];
					$order['order_date'] = date('h:i A, dS M Y',strtotime($data['order_date']));
					$order['order_descripion'] = $data['order_descripion'];
					$order['order_status'] = $data['order_status'];

					if ($data['order_status'] == 0) {
						$order['order_status_view'] = "Pending";
					}else if ($data['order_status'] == 1) {
						$order['order_status_view'] = "Approved";
					}else if ($data['order_status'] == 2) {
						$order['order_status_view'] = "Rejected";
					}else if ($data['order_status'] == 3) {
						$order['order_status_view'] = "Cancelled";
					}

					$orderDate = date('Y-m-d',strtotime($data['order_date']));

					if ($data['order_status'] == 0 && $orderDate == $todayDate) {
						$order['cancel_order'] = true;
					}else{
						$order['cancel_order'] = false;						
					}

					$order['order_reject_reason'] = $data['order_reject_reason'];
					$order['order_total_amount'] = $data['order_total_amount'];
					$order['order_discount_amount'] = $data['order_discount_amount'];
					$order['order_discount_percentage'] = $data['order_discount_percentage'];
					$order['total_order_product_qty'] = $data['total_order_product_qty'];
					$order['order_remark'] = $data['order_remark'];
					$order['order_placed_via'] = $data['order_placed_via'];
					$order['order_data'] = $data['order_data'];

					
					array_push($summary['orders'],$order);
				}				
			}

			$qry123 = $d->select(" 
				retailer_no_order_master,
				retailer_master
				","
				retailer_no_order_master.visit_by_user_id = '$user_id' 
				AND retailer_no_order_master.retailer_id = retailer_master.retailer_id 
				AND DATE_FORMAT(retailer_no_order_master.no_order_created_date,'%Y-%m-%d') = '$date'");

			if (mysqli_num_rows($qry123) > 0) {
			
				while($data1 = mysqli_fetch_array($qry123)){

					$noOrders = array();

					$retailer_no_order_id = $data1['retailer_no_order_id'];

					$noOrders['retailer_no_order_id'] = $retailer_no_order_id;
					$noOrders['society_id'] = $data1['society_id'];
					$noOrders['visit_by_user_id'] = $data1['visit_by_user_id'];
					$noOrders['retailer_id'] = $data1['retailer_id'];
					$noOrders['no_order_type_id'] = $data1['no_order_type_id'];
					$noOrders['no_order_type'] = $data1['no_order_reason'];
					$noOrders['retailer_name'] = $data1['retailer_name'];
					$noOrders['retailer_contact_person'] = $data1['retailer_contact_person'];
					$noOrders['retailer_contact_person_number'] = $data1['retailer_contact_person_number'];
					$noOrders['retailer_address'] = $data1['retailer_address'];
					$noOrders['retailer_google_address'] = $data1['retailer_google_address'];
					$noOrders['retailer_latitude'] = $data1['retailer_latitude'];
					$noOrders['retailer_longitude'] = $data1['retailer_longitude'];
					$noOrders['no_order_remark'] = $data1['no_order_remark'];
					$noOrders['no_order_latitude'] = $data1['no_order_latitude'];
					$noOrders['no_order_longitude'] = $data1['no_order_longitude'];
					$noOrders['no_order_gps_accuracy'] = $data1['no_order_gps_accuracy'];
					$noOrders['no_order_battery_status'] = $data1['no_order_battery_status'];
					$noOrders['no_order_mock_location'] = $data1['no_order_mock_location'];
		            $noOrders['no_order_mock_location_app_name'] = $data1['no_order_mock_location_app_name'];
		            $noOrders['order_status_view'] = "Cancelled";	

					$noOrders['no_order_created_date'] = date('dS F Y',strtotime($data1['no_order_created_date']));
					
					array_push($summary["no_orders"],$noOrders);
				}
			}
			array_push($response['work_summary'],$summary);
		}

		$response["message"] = "Success";
		$response["status"] = "200";
		echo json_encode($response);

	           
    }else if($_POST['addRetailerOrder']=="addRetailerOrder" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

		$order_date = date('Y-m-d H:i:s');

		$po_number = "";

		$order_data = html_entity_decode($order_data);

		$m->set_data('society_id', $society_id);
		$m->set_data('order_by_user_id', $order_by_user_id);
		$m->set_data('order_date', $order_date);
		$m->set_data('retailer_id', $retailer_id);
		$m->set_data('distributor_id', $distributor_id);
		$m->set_data('retailer_visit_id', $retailer_visit_id);
		$m->set_data('order_descripion', $order_descripion);
		$m->set_data('po_number', $po_number);
		$m->set_data('order_total_amount', $order_total_amount);
		$m->set_data('order_discount_amount', $order_discount_amount);
		$m->set_data('order_discount_percentage', $order_discount_percentage);
		$m->set_data('total_order_product_qty', $total_order_product_qty);
		$m->set_data('order_remark', $order_remark);
		$m->set_data('order_placed_via', $order_placed_via);
		$m->set_data('created_date', date('Y-m-d H:i:s'));
		$m->set_data('order_address', $order_address);
		$m->set_data('order_latitude', $order_latitude);
		$m->set_data('order_longitude', $order_longitude);
		$m->set_data('order_gps_accuracy', $order_gps_accuracy);
		$m->set_data('order_battery_status', $order_battery_status);
		$m->set_data('order_mock_location', $order_mock_location);
		$m->set_data('order_mock_location_app_name', $order_mock_location_app_name);
		$m->set_data('order_route_name', $order_route_name);
		$m->set_data('total_order_kg', $total_order_kg);
		$m->set_data('order_data', $order_data);
		$m->set_data('order_status', "1");

		$ary = array(
			'society_id' => $m->get_data('society_id'),
			'order_by_user_id' => $m->get_data('order_by_user_id'),
			'order_date' => $m->get_data('order_date'),
			'retailer_id' => $m->get_data('retailer_id'),
			'distributor_id' => $m->get_data('distributor_id'),
			'retailer_visit_id' => $m->get_data('retailer_visit_id'),
			'order_descripion' => $m->get_data('order_descripion'),
			'po_number' => $m->get_data('po_number'),
			'order_total_amount' => $m->get_data('order_total_amount'),
			'order_discount_amount' => $m->get_data('order_discount_amount'),
			'order_discount_percentage' => $m->get_data('order_discount_percentage'),
			'total_order_product_qty' => $m->get_data('total_order_product_qty'),
			'order_remark' => $m->get_data('order_remark'),
			'order_placed_via' => $m->get_data('order_placed_via'),
			'created_date' => $m->get_data('created_date'),
			'order_address' => $m->get_data('order_address'),
			'order_latitude' => $m->get_data('order_latitude'),
			'order_longitude' => $m->get_data('order_longitude'),
			'order_gps_accuracy' => $m->get_data('order_gps_accuracy'),
			'order_battery_status' => $m->get_data('order_battery_status'),
			'order_mock_location' => $m->get_data('order_mock_location'),
			'order_mock_location_app_name' => $m->get_data('order_mock_location_app_name'),
			'order_data' => $m->get_data('order_data'),
			'order_route_name' => $m->get_data('order_route_name'),
			'total_order_kg' => $m->get_data('total_order_kg'),
			'order_status' => $m->get_data('order_status'),
		);

		$qry = $d->insert("retailer_order_master",$ary);

		$retailer_order_id = $con->insert_id;

		if ($qry == true) {

			$retailerQry = $d->selectRow("retailer_name,retailer_latitude,retailer_longitude,retailer_address,area_name,city_name,retailer_contact_person,retailer_contact_person_number,retailer_contact_person_country_code","retailer_master,cities,area_master_new","retailer_master.retailer_id = '$retailer_id' AND retailer_master.city_id = cities.city_id AND retailer_master.area_id = area_master_new.area_id");

			$distributorQry = $d->selectRow("distributor_email","distributor_master","distributor_id = '$distributor_id'");

			$distData = mysqli_fetch_array($distributorQry);

			if ($retailer_daily_visit_timeline_id != '0') {
				$m->set_data('order_taken', "1");

				$ary = array(
					'order_taken' => $m->get_data('order_taken'),
				);

				$qqq1 = $d->update("retailer_daily_visit_timeline_master",$ary,"retailer_daily_visit_timeline_id = '$retailer_daily_visit_timeline_id' AND retailer_visit_by_id = '$order_by_user_id'");
			}

	    	$retData = mysqli_fetch_array($retailerQry);	    

	    	if (mysqli_num_rows($retailerQry) > 0 && $retData['retailer_latitude'] == '' && $retData['retailer_longitude'] == '' ) {
				$ary2 = array(
					'retailer_latitude' => $m->get_data('order_latitude'),
					'retailer_longitude' => $m->get_data('order_longitude'),
					'retailer_last_distributor_id' => $m->get_data('distributor_id'),
					'retailer_last_order_date' => date('Y-m-d H:i:s'),
					'retailer_last_order_amount' => $m->get_data('order_total_amount')
				);
			}else{

				$ary2 = array(
					'retailer_last_distributor_id' => $m->get_data('distributor_id'),
					'retailer_last_order_date' => date('Y-m-d H:i:s'),
					'retailer_last_order_amount' => $m->get_data('order_total_amount')
				);
			}

			$qq1 = $d->update("retailer_master",$ary2,"retailer_id = '$retailer_id'");

            $jsonArray = json_decode($order_data,true);

            if (is_array($jsonArray)) {
                for ($i=0; $i < count($jsonArray); $i++) {
                	$product_id = $jsonArray[$i]['product_id'];
					$product_variant_id = $jsonArray[$i]['product_variant_id'];
					$product_price = $jsonArray[$i]['retailer_selling_price'];
					$product_total_price = $jsonArray[$i]['total_price'];
					$product_quantity = $jsonArray[$i]['product_quantity'];
					$order_packing_type = $jsonArray[$i]['variant_bulk_type'];
					$no_of_box = $jsonArray[$i]['cases'];
					$other_units = $jsonArray[$i]['units'];
					$order_per_box_piece = $jsonArray[$i]['variant_per_box_piece'];
					$total_weight_in_kg = $jsonArray[$i]['total_weight_in_kg'];

					$m->set_data('society_id', $society_id);
					$m->set_data('product_id', $product_id);
					$m->set_data('retailer_order_id', $retailer_order_id);
					$m->set_data('product_variant_id', $product_variant_id);
					$m->set_data('product_price', $product_price);
					$m->set_data('product_total_price', $product_total_price);
					$m->set_data('product_quantity', $product_quantity);
					$m->set_data('order_packing_type', $order_packing_type);
					$m->set_data('no_of_box', $no_of_box);
					$m->set_data('order_per_box_piece', $order_per_box_piece);
					$m->set_data('other_units', $other_units);
					$m->set_data('total_weight_in_kg', $total_weight_in_kg);
					$m->set_data('record_created_date', date('Y-m-d H:i:s'));

					$ary1 = array(
						'society_id' => $m->get_data('society_id'),
						'product_id' => $m->get_data('product_id'),
						'retailer_order_id' => $m->get_data('retailer_order_id'),
						'product_variant_id' => $m->get_data('product_variant_id'),
						'product_price' => $m->get_data('product_price'),
						'product_total_price' => $m->get_data('product_total_price'),
						'product_quantity' => $m->get_data('product_quantity'),
						'order_packing_type' => $m->get_data('order_packing_type'),
						'no_of_box' => $m->get_data('no_of_box'),
						'order_per_box_piece' => $m->get_data('order_per_box_piece'),
						'other_units' => $m->get_data('other_units'),
						'total_weight_in_kg' => $m->get_data('total_weight_in_kg'),
						'record_created_date' => $m->get_data('record_created_date'),
					);

					$qry1 = $d->insert("retailer_order_product_master",$ary1);
                }
            }


			$to = $distData['distributor_email'];
			if ($to != '') {

				$q = $d->selectRow("c.city_name,amn.area_name,rpvm.sku,ropm.product_price,ropm.order_packing_type,ropm.no_of_box,ropm.order_per_box_piece,ropm.product_total_price,ropm.product_quantity,rpm.product_name,rom.*,rm.retailer_latitude,rm.retailer_longitude,rm.retailer_name,rm.retailer_contact_person,rm.retailer_contact_person_number,rm.retailer_contact_person_country_code,rm.retailer_address,distributor_latitude,dm.distributor_longitude,dm.distributor_contact_person,dm.distributor_contact_person_number,dm.distributor_contact_person_country_code,dm.distributor_name,dm.distributor_address,um.user_full_name,um.user_mobile,um.country_code,rpvm.product_variant_name","retailer_order_master AS rom JOIN retailer_master AS rm ON rm.retailer_id = rom.retailer_id JOIN distributor_master AS dm ON dm.distributor_id = rom.distributor_id JOIN users_master AS um ON um.user_id = rom.order_by_user_id JOIN retailer_order_product_master AS ropm ON ropm.retailer_order_id = rom.retailer_order_id JOIN retailer_product_master AS rpm ON rpm.product_id = ropm.product_id JOIN retailer_product_variant_master AS rpvm ON rpvm.product_variant_id = ropm.product_variant_id JOIN cities AS c ON c.city_id = rm.city_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id","rom.retailer_order_id = '$retailer_order_id'");

		        $all_data = [];
		        while($data = $q->fetch_assoc())
		        {
		            $all_data[] = $data;
		        }

	            $retailer_name = $retData['retailer_name'];
				$subject = 'New Order From '.$retailer_name . ' , ORDER NO : #'.$retailer_order_id;
	            $del_add = $retData['retailer_address'] . " , " . $retData['area_name'] . ", " . $retData['city_name'];
	            $retailer_contact_person = $retData['retailer_contact_person'];
	            $retailer_contact_person_number = $retData['retailer_contact_person_number'];
	            $retailer_contact_person_country_code = $retData['retailer_contact_person_country_code'];
				include '../apAdmin/mail/orderDetailsMailTemplate.php';
				include '../apAdmin/mail.php';
			}


			$response["message"] = "Order Placed";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "Something went wrong!";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['cancelRetailerOrder']=="cancelRetailerOrder" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

		$order_date = date('Y-m-d H:i:s');

		$m->set_data('order_status', "3");
		$m->set_data('order_cancel_date', date('Y-m-d H:i:s'));

		$ary = array(
			'order_status' => $m->get_data('order_status'),
			'order_cancel_date' => $m->get_data('order_cancel_date'),
		);

		$qry = $d->update("retailer_order_master",$ary,"retailer_id = '$retailer_id' AND order_by_user_id = '$user_id' AND retailer_order_id = '$retailer_order_id'");

		if ($qry == true) {

			if ($retailer_daily_visit_timeline_id > 0) {
				$m->set_data('order_taken', "3");

				$ary = array(
					'order_taken' => $m->get_data('order_taken'),
				);

				$qqq1 = $d->update("retailer_daily_visit_timeline_master",$ary,"retailer_daily_visit_timeline_id = '$retailer_daily_visit_timeline_id' AND retailer_visit_by_id = '$user_id'");
			}

			$response["message"] = "Order Cancelled";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "Something went wrong!";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    } else{
		$response["success"] = 201;
		$response["message"] = "Invalid Tag!";
		echo json_encode($response);
	}

} else {
	$response["success"] = 201;
	$response["message"] = "Invalid API Key!";
	echo json_encode($response);
}

?>