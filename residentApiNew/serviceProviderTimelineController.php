<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
	extract(array_map("test_input" , $_POST));
	if(isset($_POST["AddTimeline"]) &&  filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true) {
		extract(array_map("test_input" , $_POST)); 

		if ($local_service_provider_timeline_type=='0') {

			$modify_date = date("Y-m-d H:i:s");
			$total = count($_FILES['photo']['tmp_name']);


			for ($i = 0; $i < $total; $i++) {

				$uploadedFile = $_FILES['photo']['tmp_name'][$i]; 


				if ($uploadedFile != "") {

					$sourceProperties = getimagesize($uploadedFile);
					$newFileName = rand().$service_provider_users_id;
					$dirPath = "../img/service_provider_timeline/";
					$ext = pathinfo($_FILES['photo']['name'][$i], PATHINFO_EXTENSION);
					$imageType = $sourceProperties[2];
					$imageHeight = $sourceProperties[1];
					$imageWidth = $sourceProperties[0];


					if ($imageWidth>1800) {
                                $newWidthPercentage= 1800*100 / $imageWidth;  //for maximum 1200 widht
                                $newImageWidth = $imageWidth * $newWidthPercentage /100;
                                $newImageHeight = $imageHeight * $newWidthPercentage /100;
                            } else {
                            	$newImageWidth = $imageWidth;
                            	$newImageHeight = $imageHeight;
                            }


                            switch ($imageType) {

                            	case IMAGETYPE_PNG:
                            	$imageSrc = imagecreatefrompng($uploadedFile); 
                            	$tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            	imagepng($tmp,$dirPath. $newFileName. "_feed.". $ext);
                            	break;           

                            	case IMAGETYPE_JPEG:
                            	$imageSrc = imagecreatefromjpeg($uploadedFile); 
                            	$tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            	imagejpeg($tmp,$dirPath. $newFileName. "_feed.". $ext);
                            	break;

                            	case IMAGETYPE_GIF:
                            	$imageSrc = imagecreatefromgif($uploadedFile); 
                            	$tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                            	imagegif($tmp,$dirPath. $newFileName. "_feed.". $ext);
                            	break;

                            	default:
                            	$response["message"]="Invalid Image type....";
                            	$response["status"]="201";
                            	echo json_encode($response);
                            	exit;
                            	break;
                            }
                            $feed_img= $newFileName."_feed.".$ext;

                            $m->set_data('local_service_provider_id',$service_provider_users_id);
                            $m->set_data('local_service_provider_timeline_type',0);
                            $m->set_data('service_provider_timeline_detail',$service_provider_timeline_detail); 
                            $m->set_data('service_provider_timeline_image',$feed_img);
                            $m->set_data('timeline_img_height',$newImageHeight);
                            $m->set_data('timeline_img_width',$newImageWidth);
                            $m->set_data('service_provider_timeline_upload_date',date('Y-m-d', strtotime($service_provider_timeline_upload_date)));
                            $m->set_data('timeline_approval_within_days',$timeline_approval_within_days);
                            $m->set_data('created_date',$modify_date);
                            $m->set_data('card_colour',$card_colour);
                            $m->set_data('text_colour',$text_colour);

                            $a1 = array(
                            	'local_service_provider_id'=>$m->get_data('local_service_provider_id'),
                            	'local_service_provider_timeline_type'=>$m->get_data('local_service_provider_timeline_type'),
                            	'service_provider_timeline_detail'=>$m->get_data('service_provider_timeline_detail'),
                            	'service_provider_timeline_image'=>$m->get_data('service_provider_timeline_image'),
                            	'timeline_img_height'=>$m->get_data('timeline_img_height'),
                            	'timeline_img_width'=>$m->get_data('timeline_img_width'),
                            	'service_provider_timeline_upload_date'=>$m->get_data('service_provider_timeline_upload_date'),
                            	'timeline_approval_within_days'=>$m->get_data('timeline_approval_within_days'),
                                'created_date'=>$m->get_data('created_date'),
                                'card_colour'=>$m->get_data('card_colour'),
                            	'text_colour'=>$m->get_data('text_colour'),
                            );

                            $d->insert("local_service_providers_timeline",$a1);
                            $response["message"]="Timeline request sent successfully..";
                            $response["status"]="200";
                            echo json_encode($response);
                            exit();

                        }else{
                        	$response["message"]="faild.";
                        	$response["status"]="201";
                        	echo json_encode($response);
                        	exit();

                        }
                    }

                }else{
                	$response["message"]="Invalid timeline type..!";
                	$response["status"]="201";
                	echo json_encode($response);
                	exit();	
                }
            } else if(isset($_POST["AddTimelineNew"]) &&  filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true) {
                extract(array_map("test_input" , $_POST)); 


                $modify_date = date("Y-m-d H:i:s");
                $uploadedFile = $_FILES['feed_pic']['tmp_name']; 


                if ($uploadedFile != "") {

                    $sourceProperties = getimagesize($uploadedFile);
                    $newFileName = rand().$service_provider_users_id;
                    $dirPath = "../img/service_provider_timeline/";
                    $ext = pathinfo($_FILES['feed_pic']['name'], PATHINFO_EXTENSION);
                    $imageType = $sourceProperties[2];
                    $imageHeight = $sourceProperties[1];
                    $imageWidth = $sourceProperties[0];


                    if ($imageWidth>1800) {
                        $newWidthPercentage= 1800*100 / $imageWidth;  //for maximum 1200 widht
                        $newImageWidth = $imageWidth * $newWidthPercentage /100;
                        $newImageHeight = $imageHeight * $newWidthPercentage /100;
                    } else {
                        $newImageWidth = $imageWidth;
                        $newImageHeight = $imageHeight;
                    }


                    switch ($imageType) {

                        case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagepng($tmp,$dirPath. $newFileName. "_feed.". $ext);
                        break;           

                        case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagejpeg($tmp,$dirPath. $newFileName. "_feed.". $ext);
                        break;

                        case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagegif($tmp,$dirPath. $newFileName. "_feed.". $ext);
                        break;

                        default:
                        $response["message"]="Invalid Image type....";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit;
                        break;
                    }
                    $feed_img= $newFileName."_feed.".$ext;
                    $imageurl = $base_url.'img/service_provider_timeline/'.$feed_img;
                } else if ($oldPic!="") {
                    $feed_img = $oldPic;
                    $imageurl = $base_url.'img/local_service_provider/vectors/'.$feed_img;
                } else  {
                    $feed_img = "";
                }

                $qn=$d->selectRow("service_provider_name","local_service_provider_users","service_provider_users_id='$service_provider_users_id'");
                $spName =mysqli_fetch_array($qn);
                $sp_name= html_entity_decode($spName['service_provider_name']);


                $m->set_data('local_service_provider_id',$service_provider_users_id);
                $m->set_data('local_service_provider_timeline_type',0);
                $m->set_data('service_provider_timeline_detail',$service_provider_timeline_detail); 
                $m->set_data('service_provider_timeline_image',$feed_img);
                $m->set_data('timeline_img_height',$newImageHeight);
                $m->set_data('timeline_img_width',$newImageWidth);
                $m->set_data('service_provider_timeline_upload_date',date('Y-m-d', strtotime($service_provider_timeline_upload_date)));
                $m->set_data('timeline_approval_within_days',$timeline_approval_within_days);
                $m->set_data('created_date',$modify_date);
                $m->set_data('card_colour',$card_colour);
                $m->set_data('text_colour',$text_colour);

                $a1 = array(
                    'local_service_provider_id'=>$m->get_data('local_service_provider_id'),
                    'local_service_provider_timeline_type'=>$m->get_data('local_service_provider_timeline_type'),
                    'service_provider_timeline_detail'=>$m->get_data('service_provider_timeline_detail'),
                    'service_provider_timeline_image'=>$m->get_data('service_provider_timeline_image'),
                    'timeline_img_height'=>$m->get_data('timeline_img_height'),
                    'timeline_img_width'=>$m->get_data('timeline_img_width'),
                    'service_provider_timeline_upload_date'=>$m->get_data('service_provider_timeline_upload_date'),
                    'timeline_approval_within_days'=>$m->get_data('timeline_approval_within_days'),
                    'created_date'=>$m->get_data('created_date'),
                    'card_colour'=>$m->get_data('card_colour'),
                    'text_colour'=>$m->get_data('text_colour'),
                );

                $q = $d->insert("local_service_providers_timeline",$a1);
                
                if ($q>0) {

                    $noti_title = "New Timeline Request from $sp_name";
                    $noti_description = html_entity_decode($service_provider_timeline_detail);

                    $getTokens = $d->getWebFcm("web_fcm_master","");
                    $url = "https://fcm.googleapis.com/fcm/send";
                    
                    // Headers
                    $request_headers = array(
                        "Authorization:" . $cloud_messaging_server_key,
                        "Content-Type: application/json"
                    );
                    // FCM Tokens
                    $registration_ids = $getTokens;
                    // Message
                    $message = array(
                        "title" => $noti_title,
                        "body" => $noti_description,
                        "icon" => $imageurl,
                        "click_action" => "spTimelineRequests"
                    );
                    // Data
                    $fields = array(
                        'registration_ids' => $registration_ids,
                        'data' => $message,
                    );
                    /** CURL POST code */
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                    $season_data = curl_exec($ch);
                    // if (curl_errno($ch)) {
                    //     print "Error: " . curl_error($ch);
                    //     exit();
                    // }
                    // Show me the result
                    curl_close($ch);
                    $json = json_decode($season_data, true);

                           
                    $response["message"]="Timeline request sent successfully..";
                    $response["status"]="200";
                    echo json_encode($response);
                    exit();
                } else {
                    $response["message"]="Something went wrong";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();

                }


                        

            }else if (isset($_POST["GetTimeline"]) &&  filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true) {

 			 	$totalFeed=$d->count_data_direct("service_provider_timeline_id","local_service_providers_timeline","local_service_provider_id='$service_provider_users_id'");


 			 	$pos=$limit_feed;   

             	$q=$d->select("local_service_providers_timeline","local_service_provider_id='$service_provider_users_id'","ORDER BY service_provider_timeline_id DESC LIMIT $limit_feed,10");

            	if(mysqli_num_rows($q)>0){

            		$response["feed"] = array(); 

            		while($data_timeline=mysqli_fetch_array($q)) {
            			$feed = array(); 
            			$feed["service_provider_timeline_id"]=$data_timeline['service_provider_timeline_id'];
            			$feed["local_service_provider_timeline_type"]=$data_timeline['local_service_provider_timeline_type'];
            			$feed["service_provider_timeline_detail"]=html_entity_decode($data_timeline['service_provider_timeline_detail']);
            			$feed["local_service_provider_id"]=$data_timeline['local_service_provider_id'];
            			$feed["company_name"]=$data_timeline['local_service_provider_id'];
            			$feed["timeline_img_height"]=$data_timeline['timeline_img_height'];
            			$feed["timeline_img_width"]=$data_timeline['timeline_img_width'];
            			$feed["service_provider_timeline_upload_date"]=$data_timeline['service_provider_timeline_upload_date'];
            			$feed["timeline_approval_within_days"]=$data_timeline['timeline_approval_within_days'];
            			$feed["service_provider_timeline_status"]=$data_timeline['service_provider_timeline_status'];
                        if ($data_timeline['service_provider_timeline_status']==0) {
                            $feed["service_provider_timeline_status_view"]= "Pending";
                        } elseif ($data_timeline['service_provider_timeline_status']==1) {
                            $feed["service_provider_timeline_status_view"]= "Approved";
                        } else {
                            $feed["service_provider_timeline_status_view"]= "Rejected";
                        }
            			$feed["service_provider_timeline_status_desc"]=html_entity_decode($data_timeline['service_provider_timeline_status_desc']);
            			$feed["service_provider_timeline_notification"]=$data_timeline['service_provider_timeline_notification'];
            			$feed["card_colour"]=$data_timeline['card_colour'];
                        $feed["text_colour"]=$data_timeline['text_colour'];
                        $service_provider_timeline_image=$data_timeline['service_provider_timeline_image'];
                        if(file_exists("../img/local_service_provider/vectors/$service_provider_timeline_image") && $data_timeline['service_provider_timeline_image']!="") {
                            $feed["service_provider_timeline_image"]=$base_url."img/local_service_provider/vectors/".$data_timeline['service_provider_timeline_image'];
                        } else if($data_timeline['service_provider_timeline_image']!="") {
                            $feed["service_provider_timeline_image"]=$base_url."img/service_provider_timeline/".$data_timeline['service_provider_timeline_image'];
                        } else {
                            $feed["service_provider_timeline_image"] = "";
                        }

                        if ($data_timeline['text_colour']!="") {
                            $isCard = true;
                        }else {
                            $isCard = false;
                        }
                         $feed["is_card"]=$isCard;
                        
            			$feed["modify_date"]=time_elapsed_string($data_timeline['created_date']);


            	$totalCallCount=$d->count_data_direct("timeline_call_request_id","local_service_providers_timeline_call_request","local_service_provider_id = '$service_provider_users_id'");
                $totalViewCount=$d->count_data_direct("timeline_view_request_id","local_service_providers_timeline_view_request","local_service_provider_id = '$service_provider_users_id'");
                


						$feed["call_count"]="".$totalCallCount;
						$feed["view_count"]="".$totalViewCount;
						$feed["show_count"]="false";
            			$pos=$pos+1;


            		    array_push($response["feed"], $feed); 

            		}

               	  	$response["totalFeed"]=$totalFeed;
               	   	$response["pos"]="".$pos;
            		$response["message"]="Get timeline sucessfully..";
            		$response["status"]="200";
            		echo json_encode($response);
            		exit();	
            	}else{
            		$response["message"]="No Timeline found..!";
            		$response["status"]="201";
            		echo json_encode($response);
            		exit();	
            	}


            }else if (isset($_POST["GetTimelineSociety"]) && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

                $totalFeed=$d->count_data_direct("service_provider_timeline_id","local_service_providers_timeline,local_service_provider_users,common_service_providers","common_service_providers.service_provider_users_id=local_service_provider_users.service_provider_users_id  AND local_service_provider_users.service_provider_users_id=local_service_providers_timeline.local_service_provider_id AND local_service_providers_timeline.service_provider_timeline_status=1 AND common_service_providers.society_id='$society_id' GROUP BY local_service_providers_timeline.service_provider_timeline_id");


                $pos=$limit_feed;   

                $q=$d->select("local_service_providers_timeline,local_service_provider_users,common_service_providers","common_service_providers.service_provider_users_id=local_service_provider_users.service_provider_users_id  AND local_service_provider_users.service_provider_users_id=local_service_providers_timeline.local_service_provider_id AND service_provider_timeline_status=1 AND common_service_providers.society_id='$society_id'","GROUP BY local_service_providers_timeline.service_provider_timeline_id ORDER BY service_provider_timeline_id DESC LIMIT $limit_feed,10 ");

                if(mysqli_num_rows($q)>0){

                    $response["feed"] = array(); 

                    while($data_timeline=mysqli_fetch_array($q)) {
                        $feed = array(); 

                        $feed["service_provider_timeline_id"]=$data_timeline['service_provider_timeline_id'];
                        $feed["local_service_provider_timeline_type"]=$data_timeline['local_service_provider_timeline_type'];
                        $feed["service_provider_timeline_detail"]=html_entity_decode($data_timeline['service_provider_timeline_detail']);
                        $feed["local_service_provider_id"]=$data_timeline['local_service_provider_id'];
                        $feed["company_name"]=$data_timeline['local_service_provider_id'];
                        $feed["timeline_img_height"]=$data_timeline['timeline_img_height'];
                        $feed["timeline_img_width"]=$data_timeline['timeline_img_width'];
                        $feed["service_provider_timeline_upload_date"]=$data_timeline['service_provider_timeline_upload_date'];
                        $feed["timeline_approval_within_days"]=$data_timeline['timeline_approval_within_days'];
                        $feed["service_provider_timeline_status"]=$data_timeline['service_provider_timeline_status'];
                        if ($data_timeline['service_provider_timeline_status']==0) {
                            $feed["service_provider_timeline_status_view"]= "Pending";
                        } elseif ($data_timeline['service_provider_timeline_status']==1) {
                            $feed["service_provider_timeline_status_view"]= "Approved";
                        } else {
                            $feed["service_provider_timeline_status_view"]= "Rejected";
                        }
                        $feed["service_provider_timeline_status_desc"]=html_entity_decode($data_timeline['service_provider_timeline_status_desc']);
                        $feed["service_provider_timeline_notification"]=$data_timeline['service_provider_timeline_notification'];
                        $feed["card_colour"]=$data_timeline['card_colour'];
                        $feed["text_colour"]=$data_timeline['text_colour'];
                        $service_provider_timeline_image=$data_timeline['service_provider_timeline_image'];
                        if(file_exists("../img/local_service_provider/vectors/$service_provider_timeline_image") && $data_timeline['service_provider_timeline_image']!="") {
                            $feed["service_provider_timeline_image"]=$base_url."img/local_service_provider/vectors/".$data_timeline['service_provider_timeline_image'];
                        } else if($data_timeline['service_provider_timeline_image']!="") {
                            $feed["service_provider_timeline_image"]=$base_url."img/service_provider_timeline/".$data_timeline['service_provider_timeline_image'];
                        } else {
                            $feed["service_provider_timeline_image"] = "";
                        }

                        if ($data_timeline['text_colour']!="") {
                            $isCard = true;
                        }else {
                            $isCard = false;
                        }

                        if ($data_timeline['service_provider_whatsapp']>0) {
                            $feed["service_provider_whatsapp_with_code"] = $data_timeline['service_provider_whatsapp_country_code'].' '.$data_timeline['service_provider_whatsapp'];;
                        } else {
                            
                            $feed["service_provider_whatsapp_with_code"] = "";
                        }

                        $feed["service_provider_phone"] =$data_timeline['country_code'].' '. $data_timeline['service_provider_phone'];
                        $feed["service_provider_users_id"] = $data_timeline['service_provider_users_id'];
                        $feed["service_provider_name"] = html_entity_decode($data_timeline['service_provider_name']);
                        $feed["service_provider_address"] = html_entity_decode($data_timeline['service_provider_address']);

                        if ($data_timeline['service_provider_user_image']!="") {
                            $feed["service_provider_user_image"] = $base_url."img/local_service_provider/local_service_users/".$data_timeline['service_provider_user_image'];
                        } else {
                            $feed["service_provider_user_image"] = "";
                        }

                         $feed["is_card"]=$isCard;
                        
                        $feed["modify_date"]=time_elapsed_string($data_timeline['created_date']);

                        $spCatArray = array();
                          $qcd=$d->select("local_service_provider_users_category,local_service_provider_master","local_service_provider_master.local_service_provider_id=local_service_provider_users_category.category_id AND  local_service_provider_users_category.service_provider_users_id='$data_timeline[service_provider_users_id]'");
                            while ($catData=mysqli_fetch_array($qcd)) {
                            array_push($spCatArray,$catData['service_provider_category_name']);
                            }
                         $feed["service_provider_category_name"]= implode(",", $spCatArray);
                         
                        $totalCallCount=$d->count_data_direct("timeline_call_request_id","local_service_providers_timeline_call_request","local_service_provider_id = '$service_provider_users_id'");
                        $totalViewCount=$d->count_data_direct("timeline_view_request_id","local_service_providers_timeline_view_request","local_service_provider_id = '$service_provider_users_id'");
                


                        $feed["call_count"]="".$totalCallCount;
                        $feed["view_count"]="".$totalViewCount;
                        $feed["show_count"]="false";
                        $pos=$pos+1;


                        array_push($response["feed"], $feed); 

                    }

                    $response["totalFeed"]=$totalFeed;
                    $response["pos"]="".$pos;
                    $response["message"]="Get timeline sucessfully..";
                    $response["status"]="200";
                    echo json_encode($response);
                    exit(); 
                }else{
                    $response["message"]="No Timeline found..!";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit(); 
                }


            }else if (isset($_POST["GetTimelineSocietySingle"]) && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($service_provider_timeline_id, FILTER_VALIDATE_INT) == true) {

                $totalFeed=$d->count_data_direct("service_provider_timeline_id","local_service_providers_timeline,local_service_provider_users,common_service_providers","common_service_providers.service_provider_users_id=local_service_provider_users.service_provider_users_id  AND local_service_provider_users.service_provider_users_id=local_service_providers_timeline.local_service_provider_id AND local_service_providers_timeline.service_provider_timeline_status=1 AND common_service_providers.society_id='$society_id' GROUP BY local_service_providers_timeline.service_provider_timeline_id");


                $pos=$limit_feed;   

                $q=$d->select("local_service_providers_timeline,local_service_provider_users,common_service_providers","common_service_providers.service_provider_users_id=local_service_provider_users.service_provider_users_id  AND local_service_provider_users.service_provider_users_id=local_service_providers_timeline.local_service_provider_id AND service_provider_timeline_status=1 AND common_service_providers.society_id='$society_id' AND local_service_providers_timeline.service_provider_timeline_id='$service_provider_timeline_id'","LIMIT 1");

                if(mysqli_num_rows($q)>0){

                    $response["feed"] = array(); 

                    while($data_timeline=mysqli_fetch_array($q)) {
                        $feed = array(); 

                        $feed["service_provider_timeline_id"]=$data_timeline['service_provider_timeline_id'];
                        $feed["local_service_provider_timeline_type"]=$data_timeline['local_service_provider_timeline_type'];
                        $feed["service_provider_timeline_detail"]=html_entity_decode($data_timeline['service_provider_timeline_detail']);
                        $feed["local_service_provider_id"]=$data_timeline['local_service_provider_id'];
                        $feed["company_name"]=$data_timeline['local_service_provider_id'];
                        $feed["timeline_img_height"]=$data_timeline['timeline_img_height'];
                        $feed["timeline_img_width"]=$data_timeline['timeline_img_width'];
                        $feed["service_provider_timeline_upload_date"]=$data_timeline['service_provider_timeline_upload_date'];
                        $feed["timeline_approval_within_days"]=$data_timeline['timeline_approval_within_days'];
                        $feed["service_provider_timeline_status"]=$data_timeline['service_provider_timeline_status'];
                        if ($data_timeline['service_provider_timeline_status']==0) {
                            $feed["service_provider_timeline_status_view"]= "Pending";
                        } elseif ($data_timeline['service_provider_timeline_status']==1) {
                            $feed["service_provider_timeline_status_view"]= "Approved";
                        } else {
                            $feed["service_provider_timeline_status_view"]= "Rejected";
                        }
                        $feed["service_provider_timeline_status_desc"]=html_entity_decode($data_timeline['service_provider_timeline_status_desc']);
                        $feed["service_provider_timeline_notification"]=$data_timeline['service_provider_timeline_notification'];
                        $feed["card_colour"]=$data_timeline['card_colour'];
                        $feed["text_colour"]=$data_timeline['text_colour'];
                        $service_provider_timeline_image=$data_timeline['service_provider_timeline_image'];
                        if(file_exists("../img/local_service_provider/vectors/$service_provider_timeline_image") && $data_timeline['service_provider_timeline_image']!="") {
                            $feed["service_provider_timeline_image"]=$base_url."img/local_service_provider/vectors/".$data_timeline['service_provider_timeline_image'];
                        } else if($data_timeline['service_provider_timeline_image']!="") {
                            $feed["service_provider_timeline_image"]=$base_url."img/service_provider_timeline/".$data_timeline['service_provider_timeline_image'];
                        } else {
                            $feed["service_provider_timeline_image"] = "";
                        }

                        if ($data_timeline['text_colour']!="") {
                            $isCard = true;
                        }else {
                            $isCard = false;
                        }

                        if ($data_timeline['service_provider_whatsapp']>0) {
                            $feed["service_provider_whatsapp_with_code"] = $data_timeline['service_provider_whatsapp_country_code'].' '.$data_timeline['service_provider_whatsapp'];;
                        } else {
                            
                            $feed["service_provider_whatsapp_with_code"] = "";
                        }

                        $feed["service_provider_phone"] =$data_timeline['country_code'].' '. $data_timeline['service_provider_phone'];
                        $feed["service_provider_users_id"] = $data_timeline['service_provider_users_id'];
                        $feed["service_provider_name"] = html_entity_decode($data_timeline['service_provider_name']);
                        $feed["service_provider_address"] = html_entity_decode($data_timeline['service_provider_address']);

                        if ($data_timeline['service_provider_user_image']!="") {
                            $feed["service_provider_user_image"] = $base_url."img/local_service_provider/local_service_users/".$data_timeline['service_provider_user_image'];
                        } else {
                            $feed["service_provider_user_image"] = "";
                        }

                         $feed["is_card"]=$isCard;
                        
                        $feed["modify_date"]=time_elapsed_string($data_timeline['created_date']);

                        $spCatArray = array();
                          $qcd=$d->select("local_service_provider_users_category,local_service_provider_master","local_service_provider_master.local_service_provider_id=local_service_provider_users_category.category_id AND  local_service_provider_users_category.service_provider_users_id='$data_timeline[service_provider_users_id]'");
                            while ($catData=mysqli_fetch_array($qcd)) {
                            array_push($spCatArray,$catData['service_provider_category_name']);
                            }
                         $feed["service_provider_category_name"]= implode(",", $spCatArray);
                         
                        $totalCallCount=$d->count_data_direct("timeline_call_request_id","local_service_providers_timeline_call_request","local_service_provider_id = '$service_provider_users_id'");
                        $totalViewCount=$d->count_data_direct("timeline_view_request_id","local_service_providers_timeline_view_request","local_service_provider_id = '$service_provider_users_id'");
                


                        $feed["call_count"]="".$totalCallCount;
                        $feed["view_count"]="".$totalViewCount;
                        $feed["show_count"]="false";
                        $pos=$pos+1;


                        array_push($response["feed"], $feed); 

                    }

                    $response["totalFeed"]=$totalFeed;
                    $response["pos"]="".$pos;
                    $response["message"]="Get timeline sucessfully..";
                    $response["status"]="200";
                    echo json_encode($response);
                    exit(); 
                }else{
                    $response["message"]="No Offer Found..!";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit(); 
                }


            }else if (isset($_POST["deleteFeed"]) &&  filter_var($service_provider_timeline_id, FILTER_VALIDATE_INT) == true) {


                    $q= $d->delete("local_service_providers_timeline","service_provider_timeline_id='$service_provider_timeline_id' ");

                    if($q>0){

                        $response["message"]="Timeline Deleted sucessfully..";
                        $response["status"]="200";
                        echo json_encode($response);
                    }else{
                        $response["message"]="Timeline Not found..!";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit(); 

                    }

                
                }else if (isset($_POST["getColourPlate"]) &&  filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true) {

                $q=$d->select("sp_offers_color_master","active_status='0'","");

                if(mysqli_num_rows($q)>0){

                    $response["colours_list"] = array(); 

                    while($data_timeline=mysqli_fetch_array($q)) {
                        $colours_list = array(); 
                        $colours_list["sp_offers_color_id"]=$data_timeline['sp_offers_color_id'];
                        $colours_list["background_color"]=$data_timeline['background_color'];
                        $colours_list["text_color"]=$data_timeline['text_color'];
                        array_push($response["colours_list"], $colours_list); 
                    }

                }

                $spCategoryArray = array();
                $qcd=$d->select("local_service_provider_users_category,local_service_provider_master","local_service_provider_master.local_service_provider_id=local_service_provider_users_category.category_id AND  local_service_provider_users_category.service_provider_users_id='$service_provider_users_id'");
                  while ($catData=mysqli_fetch_array($qcd)) {
                    array_push($spCategoryArray, html_entity_decode($catData['category_id']));
                }

                $ids = join("','",$spCategoryArray);

                $q1=$d->select("sp_vectors","vector_status='0' AND category_id IN ('$ids')","");

                if(mysqli_num_rows($q1)>0){

                    $response["vector"] = array(); 

                    while($data_timeline=mysqli_fetch_array($q1)) {
                        $vector = array(); 
                        $vector["vector_id"]=$data_timeline['vector_id'];
                        $vector["vector_image"]=$base_url.'img/local_service_provider/vectors/'.$data_timeline['vector_image'];
                        $vector["vector_image_name"]=$data_timeline['vector_image'];
                        array_push($response["vector"], $vector); 
                    }

                }

                if (mysqli_num_rows($q)>0 || mysqli_num_rows($q1)>0) {

                    $response["message"]="Get List sucessfully..";
                    $response["status"]="200";
                    echo json_encode($response);
                    exit(); 
                }else{
                    $response["message"]="No List found..!";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit(); 
                }


            }  else {
            	$response["message"] = "Wrong Tag";
            	$response["status"] = "201";
            	echo json_encode($response);
            }
        }else{
        	$response["message"] = "Invalid Request";
        	$response["status"] = "201";
        	echo json_encode($response);
        }

        ?>