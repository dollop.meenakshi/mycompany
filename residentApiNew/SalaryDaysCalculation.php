<?php

function call($month_start_date, $shift_time_id)
{
    include_once 'lib.php';

    extract(array_map("test_input", $_POST));

    //$month_start_date = date('Y-m-1');

    $month_end_date = date("Y-m-t", strtotime($month_start_date));
    /* print_r($month_end_date);
            print_r($month_start_date);die; */
    $datesArrayOfMonth = range_date($month_start_date, $month_end_date);

    $response['month'] = date('F Y', strtotime($month_start_date));

    $holidayAry = array();
    $weekAry = array();

    $currdt = strtotime($month_start_date);
    $nextmonth = strtotime($month_start_date . "+1 month");
    $i = 0;
    $flag = true;

    $totalMonthHours = 0;

    do {
        $weekday = date("w", $currdt);
        $endday = abs($weekday - 6);
        $startarr[$i] = $currdt;
        $endarr[$i] = strtotime(date("Y-m-d", $currdt) . "+$endday day");
        $currdt = strtotime(date("Y-m-d", $endarr[$i]) . "+1 day");

        if ($endarr[$i] >= $nextmonth) {
            $endarr[$i] = strtotime(date("Y-m-d", $nextmonth) . "-1 day");;
            $flag = false;
        }

        $i++;
    } while ($flag);

    // Shift Data

    $shiftQry = $d->select("shift_timing_master", "shift_time_id = '$shift_time_id' AND is_deleted = '0'");
    $shiftData = mysqli_fetch_array($shiftQry);


    $alternateWeekOff = explode(",", $shiftData['alternate_week_off']);
    $alternate_weekoff_days_array = explode(",", $shiftData['alternate_weekoff_days']);
    $weekOffDays = explode(",", $shiftData['week_off_days']);


    // Week

    for ($x = 0; $x < count($startarr); $x++) {

        $weekArray = array();

        $weekArray["total_hours"] = "0";


        $sDate = date('Y-m-d', $startarr[$x]);
        $eDate = date('Y-m-d', $endarr[$x]);


        $datetime1 = new DateTime($sDate);
        $datetime2 = new DateTime($eDate);

        $daysArray['days'] = array();
        $weekOffAry = array();


        // Total Week Hours 

        for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
            $dayN = $date->format('l');
            $dayDate = $date->format('Y-m-d');

            // code...
            $days = array();
            $hDates = array();
            $days['day'] = $dayN;
            $days['date'] = $dayDate;
            $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate));


            $x1 = $x + 1;
            // holiday off

            // full day off
            if (in_array($day_pos, $weekOffDays) && !in_array($day_pos, $alternate_weekoff_days_array)) {
                array_push($weekAry, $dayDate);
                $minusWeekOffHR += $perdayHours;
            }

            // alternate week day off
            if ($shiftData['has_altenate_week_off'] == "1") {
                if (
                    in_array($day_pos, $alternate_weekoff_days_array) &&
                    in_array($x1, $alternateWeekOff)
                ) {
                    array_push($weekAry, $dayDate);
                    $minusAlternateWeekOffDays += $perdayHours;
                }
            }

            array_push($daysArray['days'], $days);
        }

        $totalWeekDay = count($daysArray['days']);


        //array_push($response['week'],$weekArray);

    }

    return $weekAry;
}






function range_date($first, $last)
{
    $arr = array();
    $now = strtotime($first);
    $last = strtotime($last);

    while ($now <= $last) {
        $arr[] = date('Y-m-d', $now);
        $now = strtotime('+1 day', $now);
    }

    return $arr;
}
