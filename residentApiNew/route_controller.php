<?php
include_once 'lib.php';

if (isset($_POST)){
	extract(array_map("test_input", $_POST));
	$response = array();

	$dowMap = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');

	if($_POST['getRoutes']=="getRoutes" && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	

		$qry = $d->selectRow("
			route_master.*,
			countries.country_name,
			states.state_name,
			cities.city_name,
			route_employee_assign_master.route_assign_week_days
			"," 
			route_master,
			countries,
			states,
			cities,
			route_employee_assign_master
			","
			route_master.route_active_status = '0' 
			AND route_master.society_id = '$society_id' 
			AND route_master.country_id = countries.country_id 
			AND route_master.state_id = states.state_id 			 
			AND route_master.city_id = cities.city_id
			AND route_employee_assign_master.user_id = '$user_id' 
			AND route_employee_assign_master.route_id = route_master.route_id");

		if (mysqli_num_rows($qry) > 0) {
		
			$response["routes"] = array();

			while($data = mysqli_fetch_array($qry)){
				$routes = array();

				$route_id = $data['route_id'];

				$routes['route_id'] = $route_id;
				$routes['society_id'] = $data['society_id'];
				$routes['country_id'] = $data['country_id'];
				$routes['state_id'] = $data['state_id'];
				$routes['city_id'] = $data['city_id'];
				$routes['route_name'] = $data['route_name'];
				$routes['country_name'] = $data['country_name'];
				$routes['state_name'] = $data['state_name'];
				$routes['city_name'] = $data['city_name'];
				$routes['is_today'] = false;

				$days = explode(",", $data['route_assign_week_days']);

				$routes['days'] = array();

				for ($i=0; $i < count($days); $i++) { 
					$day = array();
					$day['day'] = $dayName = $dowMap[$days[$i]];
					$todayDay = date('D',strtotime(date('Y-m-d')));

					if ($todayDay == $dayName) {
						$routes['is_today'] = true;
					}
					array_push($routes['days'],$day);
				}

				$qryRetailer = $d->selectRow("
					retailer_master.retailer_name
					","
					retailer_master,
					route_retailer_master,
					route_master,
					area_master_new
					","
					retailer_master.society_id = '$society_id' 
					AND route_retailer_master.route_id = route_master.route_id 
					AND route_retailer_master.retailer_id = retailer_master.retailer_id 
					AND route_retailer_master.route_id = '$route_id' 
					AND retailer_master.retailer_status = '1'
					AND area_master_new.area_id = retailer_master.area_id","GROUP BY retailer_master.retailer_id");

				$routes['retailers'] = array();

				if (mysqli_num_rows($qryRetailer) > 0) {
		
					while($dataRetailer = mysqli_fetch_array($qryRetailer)){
						$retailers = array();

						$retailers['retailer_name'] = $dataRetailer['retailer_name'];

						array_push($routes['retailers'],$retailers);
					}
				}

				$routes['route_created_date'] = date('dS F Y',strtotime($data['route_created_date']));
				
				array_push($response["routes"],$routes);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getSpinnerDataRetailer']=="getSpinnerDataRetailer" && filter_var($society_id, FILTER_VALIDATE_INT) == true){  

    	$routesQry = $d->selectRow("
			route_master.route_id,
			route_master.route_name,
			countries.country_id,
			states.state_id,
			cities.city_id,
			route_employee_assign_master.route_assign_week_days
			"," 
			route_master,
			countries,
			states,
			cities,
			route_employee_assign_master
			","
			route_master.route_active_status = '0' 
			AND route_master.society_id = '$society_id' 
			AND route_employee_assign_master.user_id = '$user_id' 
			AND route_master.country_id = countries.country_id 
			AND route_master.state_id = states.state_id 			 
			AND route_master.city_id = cities.city_id
			AND route_employee_assign_master.route_id = route_master.route_id");

		$response["routes"] = array();

		if (mysqli_num_rows($routesQry) > 0) {
		

			while($data = mysqli_fetch_array($routesQry)){
				$routes = array();

				$route_id = $data['route_id'];

				$routes['route_id'] = $route_id;
				$routes['route_name'] = $data['route_name'];
				$routes['country_id'] = $data['country_id'];
				$routes['state_id'] = $data['state_id'];
				$routes['city_id'] = $data['city_id'];
				
				array_push($response["routes"],$routes);
			}
		} 

		$routeAreaQry = $d->selectRow("
			route_master.route_id,
			route_master.route_name,
			area_master_new.area_id,
			area_master_new.area_name
			"," 
			route_master,
			route_employee_assign_master,
			area_master_new
			","
			route_master.route_active_status = '0' 
			AND route_master.society_id = '$society_id' 
			AND route_employee_assign_master.user_id = '$user_id' 
			AND route_employee_assign_master.route_id = route_master.route_id
			AND area_master_new.city_id = route_master.city_id");

		$response["route_areas"] = array();


		if (mysqli_num_rows($routeAreaQry) > 0) {
		

			while($data = mysqli_fetch_array($routeAreaQry)){
				$routeAreas = array();

				$routeAreas['route_id'] = $data['route_id'];
				$routeAreas['route_name'] = $data['route_name'];
				$routeAreas['area_id'] = $data['area_id'];
				$routeAreas['area_name'] = $data['area_name'];
				
				array_push($response["route_areas"],$routeAreas);
			}
		} 

		$distributorQry = $d->selectRow("
			distributor_id,
			distributor_name,
			distributor_contact_person,
			distributor_contact_person_number,
			distributor_address
			"," 
			distributor_master
			","
			society_id = '$society_id' AND distributor_status = '1'
			");

		$response["route_distributors"] = array();
		

		if (mysqli_num_rows($distributorQry) > 0) {
		

			while($data = mysqli_fetch_array($distributorQry)){
				$distributors = array();

				$distributors['area_id'] = "";
				$distributors['area_name'] = "";
				$distributors['distributor_id'] = $data['distributor_id'];
				$distributors['distributor_name'] = $data['distributor_name'];
				$distributors['distributor_contact_person'] = $data['distributor_contact_person'];
				$distributors['distributor_contact_person_number'] = $data['distributor_contact_person_number'];
				$distributors['distributor_address'] = $data['distributor_address'];
				
				array_push($response["route_distributors"],$distributors);
			}
		}

		$response["message"] = "Success";
		$response["status"] = "200";
		echo json_encode($response);
		              
    } else{
		$response["success"] = 201;
		$response["message"] = "Invalid Tag !";
		echo json_encode($response);
	}

} else {
	$response["success"] = 201;
	$response["message"] = "Invalid API Key !";
	echo json_encode($response);
}

?>