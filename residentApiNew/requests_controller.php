<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb && $auth_check=='true') {
        $response = array();
        extract(array_map("test_input", $_POST));
        $temDate = date("Y-m-d h:i A");

        $open = $xml->string->open;
        $closed = $xml->string->closed;
        $reopen = $xml->string->reopen;
        $in_progress = $xml->string->in_progress;
        $deleted = $xml->string->deleted;
        $reply = $xml->string->reply;

        if ($_POST['getRequest'] == "getRequest" && $unit_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            $qr = $d->select("request_master", "society_id='$society_id' AND request_unit_id='$unit_id'  AND request_user_id='$user_id'", "ORDER BY request_id DESC");
            if (mysqli_num_rows($qr) > 0) {
                $response["request"] = array();

                while ($data_complain_list = mysqli_fetch_array($qr)) {

                    $request = array();

                    $request["request_id"] = $data_complain_list['request_id'];
                    $request_id = $data_complain_list['request_id'];
                    $request["society_id"] = $data_complain_list['society_id'];
                    $request["request_no"] = 'REQ'.$data_complain_list['request_id'];
                    $request["user_id"] = $data_complain_list['request_user_id'];
                    $request["unit_id"] = $data_complain_list['request_unit_id'];
                    $request["request_image"] = $base_url.'img/request/'.$data_complain_list['request_image'];
                    $request["request_audio"] = $base_url.'img/request/'.$data_complain_list['request_audio'];
                    $request["request_title"] = html_entity_decode($data_complain_list['request_title']);
                    $request["request_description"] = html_entity_decode($data_complain_list['request_description']);
                    $request["request_date"] = date("d M Y h:i A", strtotime($data_complain_list['request_date']));
                    if ($data_complain_list['request_status']==1) { 
                        $request["request_status_view"] = "Closed";
                    } elseif ($data_complain_list['request_status']==2) { 
                        $request["request_status_view"] = "In Progress";
                    }  elseif ($data_complain_list['request_status']==3) {
                        $request["request_status_view"] = "Open";
                    }else {
                        $request["request_status_view"] = "Open";
                    }
                    $request["request_closed_by"] = $data_complain_list['request_closed_by'];
                    
                  
                    $qsub = $d->select("request_track_master", "request_id='$request_id'", "ORDER BY request_track_id DESC");
                    if (mysqli_num_rows($qsub) > 0) {

                        $complain["track"] = array();
                        while ($data_complain_track = mysqli_fetch_array($qsub)) {

                            $track = array();
                            $track["request_track_id"] = $data_complain_track['request_track_id'];
                            $track["request_track_by "] = $data_complain_track['request_track_by '];
                            $track["request_track_msg"] = html_entity_decode($data_complain_track['request_track_msg']);
                            $track["request_status_view"] = ''.$data_complain_track['request_status_view'];

                            if ($data_complain_track['request_track_voice'] != '') {
                                $track["request_track_voice"] = $base_url . "img/request/" . $data_complain_track['request_track_voice'];
                            } else {
                                $track["request_track_voice"] = "";
                            }

                            if ($data_complain_track['request_track_img'] != '') {
                                $track["request_track_img"] = $base_url . "img/request/" . $data_complain_track['request_track_img'];
                            } else {
                                $track["request_track_img"] = "";
                            }
                            $track["request_id"] = $data_complain_track['request_id'];
                            $track["admin_id"] = $data_complain_track['admin_id'];
                            $track["request_track_date_time"] = $data_complain_track['request_track_date_time'];

                            $track["request_track_img_old"] = $data_complain_track['request_track_img'];
                            $track["request_track_voice_old"] = $data_complain_track['request_track_voice'] . '';


                            array_push($request["track"], $track);
                        }
                    }

                    if ($data_complain_list['complain_review_msg_date'] != '') {
                        $request["complain_review_msg_date"] = date("d M Y h:i A", strtotime($data_complain_list['complain_review_msg_date']));
                    }
                    
                    $request["request_status"]=$data_complain_list['request_status'];
                    $request["feedback_msg"] = $data_complain_list['feedback_msg'] . '';

                    array_push($response["request"], $request);
                }
                $response["audio_duration"] = 30000;
                $response["message"] = "Get Request Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                 $response["audio_duration"] = 30000;
                $response["message"] = "No request Found.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['addRequestNew'] == "addRequestNew" && $unit_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {



           
            

            $file11 = $_FILES["request_audio"]["tmp_name"];
            if (file_exists($file11)) {
                // $complaint_voice = $_FILES['complaint_voice']['name'];
                $request_audio = rand() . "." . "mp3";
                $target = '../img/request/' . $request_audio;
                move_uploaded_file($_FILES['request_audio']['tmp_name'], $target);
            } else {
                $request_audio = '';
            }

            $uploadedFile = $_FILES["request_image"]["tmp_name"];
            $ext = pathinfo($_FILES['request_image']['name'], PATHINFO_EXTENSION);
            if (file_exists($uploadedFile)) {

                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $user_id;
                $dirPath = "../img/request/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1000) {
                    $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }
                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "_request." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "_request." . $ext);
                        break;

                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "_request." . $ext);
                        break;

                    default:

                        break;
                }
                $request_image = $newFileName . "_request." . $ext;

                $notiUrl = $base_url . 'img/request/' . $request_image;
            } else {
                $request_image = '';
                $notiUrl = "";
            }

              $qC = $d->select("block_master,unit_master", "unit_master.society_id='$society_id' AND block_master.block_id=unit_master.block_id AND unit_master.unit_id='$unit_id'","ORDER BY unit_master.unit_id ASC");
              $unitData=mysqli_fetch_array($qC);
              $unit_name= $unitData['block_name'].'-'.$unitData['unit_name'];

            $m->set_data('society_id', $society_id);
            $m->set_data('request_user_id', $user_id);
            $m->set_data('request_unit_id', $unit_id);
            $m->set_data('request_title', $request_title);
            $m->set_data('request_description', $request_description);
            $m->set_data('request_image', $request_image);
            $m->set_data('request_audio', $request_audio);
            $m->set_data('request_date', $temDate);
            $m->set_data('request_status', $request_status);
            $m->set_data('user_id', $user_id);


            $a = array(
                'society_id' => $m->get_data('society_id'),
                'request_unit_id' => $m->get_data('request_unit_id'),
                'request_user_id' => $m->get_data('request_user_id'),
                'request_title' => $m->get_data('request_title'),
                'request_description' => $m->get_data('request_description'),
                'request_image' => $m->get_data('request_image'),
                'request_audio' => $m->get_data('request_audio'),
                'request_date' => $m->get_data('request_date'),
                'request_status' => $m->get_data('request_status'),
            );

            $quserDataUpdate = $d->insert("request_master", $a);
            $request_id = $con->insert_id;
            $a1 = array(
                'request_track_by ' => 0,
                'society_id' => $m->get_data('society_id'),
                'request_track_msg' => $m->get_data('request_description'),
                'request_track_img' => $request_image,
                'request_track_voice' => $request_audio,
                'request_id' => $request_id,
                'request_track_date_time ' => $m->get_data('request_date'),
                'request_status_view ' => "Open",
            );
            $d->insert("request_track_master", $a1);
            if ($quserDataUpdate == TRUE) {

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("8",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("8",$block_id,"ios");
                
               
                    $nAdmin->noti_new($society_id,$notiUrl, $fcmArray, "New Request added by $user_name ($unit_name)" , $request_title, "requestHistory?request_id=$request_id");
                    $nAdmin->noti_ios_new($society_id,$notiUrl, $fcmArrayIos, "New Request added by $user_name ($unit_name)", $request_title, "requestHistory?request_id=$request_id");
                           
                    

                    $notiAry = array(
                        'society_id' => $society_id,
                        'notification_tittle' => "New Request added by $user_name ($unit_name)",
                        'notification_description' => $request_title,
                        'notifiaction_date' => date('Y-m-d H:i'),
                        'notification_action' => "requestHistory?request_id=$request_id",
                        'admin_click_action' => "requestHistory?request_id=$request_id",
                         'notification_logo'=>'request.png',
                    );
                    $d->insert("admin_notification", $notiAry);

                $d->insert_myactivity($user_id,"$society_id","0","$user_name","New request send succssfully","request.png");
                 

                $response["message"] = " Request Send Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No Request Send.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['deleteRequest'] == "deleteRequest" && filter_var($request_id, FILTER_VALIDATE_INT) == true) {

            
            $q = $d->delete("request_master", "request_id='$request_id'");
            $q = $d->delete("request_track_master", "request_id='$request_id'");

            if ($q == TRUE) {
                 $d->insert_myactivity($user_id,"$society_id","0","$user_name","Request deleted","request.png");
                 
                $response["message"] = "Request Deleted";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Something wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['editRequestNew'] == "editRequestNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {

            $file11 = $_FILES["request_audio"]["tmp_name"];
            if (file_exists($file11)) {
                // $complaint_voice = $_FILES['complaint_voice']['name'];
                $request_audio = rand() . "." . "mp3";
                $target = '../img/request/' . $request_audio;
                move_uploaded_file($_FILES['request_audio']['tmp_name'], $target);
            } else {
                $request_audio = '';
            }

            $uploadedFile = $_FILES["request_image"]["tmp_name"];
            $ext = pathinfo($_FILES['request_image']['name'], PATHINFO_EXTENSION);
            if (file_exists($uploadedFile)) {

                $sourceProperties = getimagesize($uploadedFile);
                $newFileName = rand() . $user_id;
                $dirPath = "../img/request/";
                $imageType = $sourceProperties[2];
                $imageHeight = $sourceProperties[1];
                $imageWidth = $sourceProperties[0];
                if ($imageWidth > 1000) {
                    $newWidthPercentage = 1000 * 100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage / 100;
                    $newImageHeight = $imageHeight * $newWidthPercentage / 100;
                } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                }
                switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagepng($tmp, $dirPath . $newFileName . "_request." . $ext);
                        break;

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagejpeg($tmp, $dirPath . $newFileName . "_request." . $ext);
                        break;

                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile);
                        $tmp = imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1], $newImageWidth, $newImageHeight);
                        imagegif($tmp, $dirPath . $newFileName . "_request." . $ext);
                        break;

                    default:

                        break;
                }
                $request_image = $newFileName . "_request." . $ext;

                $notiUrl = $base_url . 'img/request/' . $request_image;
            }else {
                $notiUrl= "";
            }
            switch ($request_status) {
                case '0':
                    $status= "Open";
                    break;
                case '1':
                    $status= "Closed";
                    break;
                case '2':
                    $status= "Re-Open";
                    break;
                case '3':
                    $status= "Reply";
                    break;
                default:
                    # code...
                    break;
            }

            $qC = $d->select("block_master,unit_master", "unit_master.society_id='$society_id' AND block_master.block_id=unit_master.block_id AND unit_master.unit_id='$unit_id'","ORDER BY unit_master.unit_id ASC");
              $unitData=mysqli_fetch_array($qC);
              $unit_name= $unitData['block_name'].'-'.$unitData['unit_name'];


            $m->set_data('society_id', $society_id);
            $m->set_data('request_title', $request_title);
            $m->set_data('request_description', $request_description);
            $m->set_data('request_image', $request_image);
            $m->set_data('request_audio', $request_audio);
            $m->set_data('request_status',$request_status);
            $m->set_data('request_date', $temDate);


            if ($request_status==3) {
                 $a = array(
                    'society_id' => $m->get_data('society_id'),
                );
            }else {
                $a = array(
                    'society_id' => $m->get_data('society_id'),
                    'request_status' => $m->get_data('request_status')
                );

            }


            $a1 = array(
                'request_track_by' => 0,
                'society_id' => $m->get_data('society_id'),
                'request_track_msg' => $m->get_data('request_description'),
                'request_track_img' => $request_image,
                'request_track_voice' => $request_audio,
                'request_id' => $request_id,
                'request_track_date_time ' => $m->get_data('request_date'),
                'request_status_view ' => $status,
            );

            $quserDataUpdate = $d->update("request_master", $a, "request_id='$request_id'");
            $d->insert("request_track_master", $a1);

            if ($quserDataUpdate == TRUE) {

                $block_id=$d->getBlockid($user_id);
               $fcmArray=$d->selectAdminBlockwise("8",$block_id,"android");
               $fcmArrayIos=$d->selectAdminBlockwise("8",$block_id,"ios");

                        $nAdmin->noti_new($society_id,$notiUrl, $fcmArray, "Request $request_no $status by ($unit_name)", $request_title, "requestHistory?request_id=$request_id");
                  
                        $nAdmin->noti_ios_new($society_id,$notiUrl, $fcmArrayIos, "Request $request_no $status by ($unit_name)", $request_title, "requestHistory?request_id=$request_id");
                           
                       
                    $notiAry = array(
                        'society_id' => $society_id,
                        'notification_tittle' => "Request $status by ($unit_name)",
                        'notification_description' => $request_title,
                        'notifiaction_date' => date('Y-m-d H:i'),
                        'notification_action' => "requestHistory?request_id=$request_id",
                        'admin_click_action' => "requestHistory?request_id=$request_id",
                        'notification_logo'=>'request.png',
                    );
                    $d->insert("admin_notification", $notiAry);
                
                $d->insert_myactivity($user_id,"$society_id","0","$user_name","Request $status Successfully","request.png");
                 
                $response["message"] = "Request $status Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "update failed.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getRequestDetail'] == "getRequestDetail" && $request_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($request_id, FILTER_VALIDATE_INT) == true) {

            $qsub = $d->select("request_master,request_track_master", "request_track_master.request_id='$request_id' AND request_master.request_id = request_track_master.request_id", "ORDER BY request_track_master.request_track_id ASC");
            if (mysqli_num_rows($qsub) > 0) {

                $complaint_category="";
                 $tempPrvDate = "";
                $response["track"] = array();
                while ($data_complain_track = mysqli_fetch_array($qsub)) {

                    $track = array();
                    $track["request_track_id"] = $data_complain_track['request_track_id'];
                    $track["request_track_by "] = $data_complain_track['request_track_by'];
                    $track["request_track_msg"] = html_entity_decode($data_complain_track['request_track_msg']);
                    
                     switch ($data_complain_track['request_status_view']) {
                        case 'Open':
                            $complaint_status_view  = $open;
                            $complaint_status_int  = "0";
                            break;
                        case 'Closed':
                            $complaint_status_view  = $closed;
                            $complaint_status_int  = "1";
                            break;
                        case 'Reopen':
                            $complaint_status_view  = $reopen;
                            $complaint_status_int  = "4";
                            break;
                        case 'In Progress':
                            $complaint_status_view  = $in_progress;
                            $complaint_status_int  = "2";
                            break;
                        case 'Reply':
                            $complaint_status_view  = $reply;
                            $complaint_status_int  = "3";
                            break;
                        default:
                           $complaint_status_view  = $reply;
                            $complaint_status_int  = "3";
                            break;
                    }

                    $track["request_status_view"] = $complaint_status_view.'';
                    $track["request_status_int"] = $complaint_status_int;

                    if ($data_complain_track['request_track_voice'] != '') {
                        $track["request_track_voice"] = $base_url . "img/request/" . $data_complain_track['request_track_voice'];
                    } else {
                        $track["request_track_voice"] = "";
                    }
                    if ($data_complain_track['request_track_img'] != '') {
                        $track["request_track_img"] = $base_url . "img/request/" . $data_complain_track['request_track_img'];
                    } else {
                        $track["request_track_img"] = "";
                    }
                    $track["request_id"] = $data_complain_track['request_id'];
                    $track["admin_id"] = $data_complain_track['admin_id'];
                    $track["request_track_date_time"] = date("d M Y h:i A", strtotime($data_complain_track['request_track_date_time']));
                    $chatDate = date("d M", strtotime($data_complain_track['request_track_date_time']));
                    $track["request_track_img_old"] = $data_complain_track['request_track_img'];
                    $track["request_track_voice_old"] = $data_complain_track['request_track_voice'] . '';
                    if($data_complain_track['request_track_by']==1){ 
                        $qad=$d->select("bms_admin_master","admin_id='$data_complain_track[admin_id]'");
                        $adminData=mysqli_fetch_array($qad);
                     $track["admin_name"] = $adminData['admin_name'];
                    } else {
                     $track["admin_name"] = "";
                    }
                    $track["isDate"] = false;

                    $qchek=$d->selectRow("device","users_master","user_id='$user_id'");
                    $userData=mysqli_fetch_array($qchek);
                    if ($userData['device']=="android") {
                        
                        if($tempPrvDate!=""){

                            if($tempPrvDate != $chatDate){
                                $tempPrvDate = $chatDate;
                                $chatD = array();
                                $chatD["msg_date"] = $tempPrvDate;
                                $chatD["isDate"] = true;
                                $chatD["msg_date_view"] = $chatDate;

                                array_push($response["track"], $chatD);
                            }

                        }else{
                            $tempPrvDate = $chatDate;
                            $chatD = array();
                            $chatD["msg_date"] = $tempPrvDate;
                            $chatD["isDate"] = true;
                            $chatD["msg_date_view"] = $chatDate;
                            array_push($response["track"], $chatD);
                        }
                    }
                    array_push($response["track"], $track);
                }

                $response["device"] = $userData['device'];
                $response["audio_duration"] = 30000;
                $response["message"] = "Get Request Details Successfully";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["audio_duration"] = 30000;
                $response["message"] = "No Request Found.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getRequestDetail'] == "getRequestDetail_iOS" && $request_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($request_id, FILTER_VALIDATE_INT) == true) {

            $qsub = $d->select("request_master,request_track_master", "request_track_master.request_id='$request_id' AND request_master.request_id = request_track_master.request_id", "ORDER BY request_track_master.request_track_id ASC");
            if (mysqli_num_rows($qsub) > 0) {

                $complaint_category="";
                 $tempPrvDate = "";
                $response["track"] = array();
                while ($data_complain_track = mysqli_fetch_array($qsub)) {

                    $track = array();
                    $track["request_track_id"] = $data_complain_track['request_track_id'];
                    $track["request_track_by "] = $data_complain_track['request_track_by'];
                    $track["request_track_msg"] = html_entity_decode($data_complain_track['request_track_msg']);
                    
                     switch ($data_complain_track['request_status_view']) {
                        case 'Open':
                            $complaint_status_view  = $open;
                            $complaint_status_int  = "0";
                            break;
                        case 'Closed':
                            $complaint_status_view  = $closed;
                            $complaint_status_int  = "1";
                            break;
                        case 'Reopen':
                            $complaint_status_view  = $reopen;
                            $complaint_status_int  = "4";
                            break;
                        case 'In Progress':
                            $complaint_status_view  = $in_progress;
                            $complaint_status_int  = "2";
                            break;
                        case 'Reply':
                            $complaint_status_view  = $reply;
                            $complaint_status_int  = "3";
                            break;
                        default:
                           $complaint_status_view  = $reply;
                            $complaint_status_int  = "3";
                            break;
                    }

                    $track["request_status_view"] = $complaint_status_view.'';
                    $track["request_status_int"] = $complaint_status_int;
                    
                    if ($data_complain_track['request_track_voice'] != '') {
                        $track["request_track_voice"] = $base_url . "img/request/" . $data_complain_track['request_track_voice'];
                    } else {
                        $track["request_track_voice"] = "";
                    }
                    if ($data_complain_track['request_track_img'] != '') {
                        $track["request_track_img"] = $base_url . "img/request/" . $data_complain_track['request_track_img'];
                    } else {
                        $track["request_track_img"] = "";
                    }
                    $track["request_id"] = $data_complain_track['request_id'];
                    $track["admin_id"] = $data_complain_track['admin_id'];
                    $track["request_track_date_time"] = date("d M Y h:i A", strtotime($data_complain_track['request_track_date_time']));
                    $chatDate = date("d M", strtotime($data_complain_track['request_track_date_time']));
                    $track["request_track_img_old"] = $data_complain_track['request_track_img'];
                    $track["request_track_voice_old"] = $data_complain_track['request_track_voice'] . '';
                    if($data_complain_track['request_track_by']==1){ 
                        $qad=$d->select("bms_admin_master","admin_id='$data_complain_track[admin_id]'");
                        $adminData=mysqli_fetch_array($qad);
                     $track["admin_name"] = $adminData['admin_name'];
                    } else {
                     $track["admin_name"] = "";
                    }
                    $track["isDate"] = false;

                    
                        
                        if($tempPrvDate!=""){

                            if($tempPrvDate != $chatDate){
                                $tempPrvDate = $chatDate;
                                $chatD = array();
                                $chatD["msg_date"] = $tempPrvDate;
                                $chatD["isDate"] = true;
                                $chatD["msg_date_view"] = $chatDate;

                                array_push($response["track"], $chatD);
                            }

                        }else{
                            $tempPrvDate = $chatDate;
                            $chatD = array();
                            $chatD["msg_date"] = $tempPrvDate;
                            $chatD["isDate"] = true;
                            $chatD["msg_date_view"] = $chatDate;
                            array_push($response["track"], $chatD);
                        }
                    array_push($response["track"], $track);
                }

                $response["audio_duration"] = 30000;
                $response["message"] = "Get Request Details Successfully";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["audio_duration"] = 30000;
                $response["message"] = "No Request Found.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }  else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
