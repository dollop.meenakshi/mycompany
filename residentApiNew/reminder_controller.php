<?php
include_once 'lib.php';


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
	$response = array();
	extract(array_map("test_input" , $_POST));

    if($_POST['getReminder']=="getReminder" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

           
        $q=$d->select("reminder_master","society_id='$society_id' AND user_id='$user_id' ","ORDER BY reminder_id DESC");
          
         
        if(mysqli_num_rows($q)>0){

            $response["reminder"] = array();


            while($data_bill_list=mysqli_fetch_array($q)) {
                  
                     $reminder = array(); 
                     $reminder["reminder_id"]=$data_bill_list['reminder_id'];
                     $reminder["user_id"]=$data_bill_list['user_id'];
                     $reminder["society_id"]=$data_bill_list['society_id'];
                     $reminder["reminder_text"]=html_entity_decode($data_bill_list['reminder_text']);
                     $reminder["reminder_date"]=$data_bill_list['reminder_date'];
                     $reminder["reminder_date_view"]=date("d M Y h:i A", strtotime($data_bill_list['reminder_date']));
                     if ($data_bill_list['reminder_status']==0) {
                     $reminder["reminder_me"]=true;
                     } else {
                     $reminder["reminder_status"]=false;
                     }
                     array_push($response["reminder"], $reminder);

            
            }  

           
             $response["message"]="Get Reminder Successfully !";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Reminder Found.";
            $response["status"]="201";
            echo json_encode($response);

        }
    } else  if($_POST['getReminderNotification']=="getReminderNotification" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

        $reminder_text=mysqli_real_escape_string($con, $reminder_text);
        
           
        $q=$d->select("reminder_master","society_id='$society_id' AND user_id='$user_id' AND reminder_status=0 AND reminder_text='$reminder_text'","ORDER BY reminder_id DESC");
          
         
        if(mysqli_num_rows($q)>0){

            $response["reminder"] = array();


            while($data_bill_list=mysqli_fetch_array($q)) {
                  
                     $reminder = array(); 
                     $reminder["reminder_id"]=$data_bill_list['reminder_id'];
                     $reminder["user_id"]=$data_bill_list['user_id'];
                     $reminder["society_id"]=$data_bill_list['society_id'];
                     $reminder["reminder_text"]=html_entity_decode($data_bill_list['reminder_text']);
                     $reminder["reminder_date"]=$data_bill_list['reminder_date'];
                     $reminder["reminder_date_view"]=date("d M Y h:i A", strtotime($data_bill_list['reminder_date']));
                     if ($data_bill_list['reminder_status']==0) {
                     $reminder["reminder_me"]=true;
                     } else {
                     $reminder["reminder_status"]=false;
                     }
                     array_push($response["reminder"], $reminder);

            
            }  

           
             $response["message"]="Get Reminder Successfully !";
             $response["status"]="200";
             echo json_encode($response);

        }else{
            $response["message"]="No Reminder Found.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else if($_POST['addReminder']=="addReminder" && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($society_id, FILTER_VALIDATE_INT) == true){

              
            $reminder_date = date("Y-m-d H:i:s", strtotime($reminder_date.' '.$reminder_time));

            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('reminder_text',$reminder_text);
            $m->set_data('reminder_date',$reminder_date);

               $a1 = array(
                    'society_id'=>$m->get_data('society_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'reminder_text'=>$m->get_data('reminder_text'),
                    'reminder_date'=>$m->get_data('reminder_date'),
                );

              $q1=$d->insert("reminder_master",$a1);
              $reminder_id = $con->insert_id;

            if ($q1==TRUE) {

                 $response["reminder_id"]=$reminder_id;
                 $response["message"]="Reminder Set Successfully !";
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }

    }else if($_POST['completeReminder']=="completeReminder" && filter_var($reminder_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){


                $a1 = array(
                    'reminder_status'=>1,
                );


              $q1=$d->update("reminder_master",$a1,"reminder_id='$reminder_id' AND user_id='$user_id'");
            if ($q1==TRUE) {

                 $response["reminder_id"]=$reminder_id;
                 $response["message"]="Reminder Closed Successfully !";
                 $response["due_date"]= date('d M Y',strtotime($due_date));
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }

    }else if($_POST['deleteReminder']=="deleteReminder" && filter_var($reminder_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){


            $q1=$d->delete("reminder_master","reminder_id='$reminder_id' AND user_id='$user_id'");

            if ($q1==TRUE) {

                 $response["message"]="Reminder Deleted Successfully !";
                 $response["due_date"]= date('d M Y',strtotime($due_date));
                 $response["status"]="200";
                 echo json_encode($response);
            }else{
                 $response["message"]="Somthing wrong please try again..!!";
                 $response["status"]="201";
                 echo json_encode($response);

            }

    }else{
      $response["message"]="wrong tag";
      $response["status"]="201";
      echo json_encode($response);
    }
  }
    else{
        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);

    }
}
