<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    $societyLngName=  $xml->string->society;

    if ($key==$keydb ) { 
        $response = array();
        extract(array_map("test_input" , $_POST));

        if($_POST['getShifts']=="getShifts" && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            $week_days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

            $shiftQry=$d->select("shift_timing_master","society_id ='$society_id'   AND is_deleted = '0'");

            if(mysqli_num_rows($shiftQry)>0){

                $response["shift"] = array();

                while($data_shift_list=mysqli_fetch_array($shiftQry)) {

                    $arData = array();
                      for($i=0; $i<count($week_days); $i++){
                        if($data_shift_list['week_off_days'] !="")
                        {
                          $week_off_days = explode(',',$data_shift_list['week_off_days']);
                          if(in_array($i, $week_off_days)){
                            array_push($arData,$week_days[$i]);
                          } 
                        }
                      }
                      if(!empty($arData)){
                        $showOff = implode(',',$arData);
                      }
                      else
                      {
                        $showOff = "";
                      }

                    $shift = array();

                    $shift["shift_time_id"]=$data_shift_list['shift_time_id'];
                    $shift["shift_name"]=$data_shift_list['shift_name'];
                    $shift["shift_type"]=$data_shift_list['shift_type'];
                    $shift["shift_start_time"]=$data_shift_list['shift_start_time'];
                    $shift["shift_end_time"]=$data_shift_list['shift_end_time'];
                    $shift["week_off"]=$showOff;
                    $shift["shift_time_view"]= $data_shift_list['shift_name'].'-'.date('h:i A',strtotime($data_shift_list['shift_start_time'])).' - '. date('h:i A',strtotime($data_shift_list['shift_end_time'])).' ('.$data_shift_list['shift_type'].')';
                    $shift["shift_time"]= date('h:i A',strtotime($data_shift_list['shift_start_time'])).' - '. date('h:i A',strtotime($data_shift_list['shift_end_time']));
                    
                    array_push($response["shift"], $shift); 
                }

                $response["message"]="Get shift success.";
                $response["status"]="200";
                echo json_encode($response);
            
            }else{
                $response["message"]="No Shift Found";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['changeShift']=="changeShift" && $user_id !=0 && $user_id!='' &&  filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 

            $qq = $d->count_data_direct("shift_change_request_id","shift_change_request","changed_by_id = '$user_id'");

            if ($qq > 0) {
                $response["message"] = "Shift change already requested";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            }

            $m->set_data('society_id',$society_id);
            $m->set_data('current_shift_id',$current_shift_id);
            $m->set_data('new_shift_id',$new_shift_id);
            $m->set_data('changed_by_id',$user_id);
            $m->set_data('changed_by_type',"0");
            $m->set_data('change_remark',$change_remark);
            $m->set_data('change_request_date',date('Y-m-d H:i:s'));

            $a1= array(
                'society_id'=> $m->get_data('society_id'),
                'current_shift_id'=> $m->get_data('current_shift_id'),
                'new_shift_id'=> $m->get_data('new_shift_id'),
                'changed_by_id'=> $m->get_data('changed_by_id'),
                'changed_by_type'=> $m->get_data('changed_by_type'),
                'change_remark'=> $m->get_data('change_remark'),
                'change_request_date'=> $m->get_data('change_request_date'),
            );
           
            $q=$d->insert("shift_change_request",$a1);

            if ($q == true) {

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("24",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("24",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"shiftChangeRequest");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"shiftChangeRequest");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>"Shift Change Request",
                  'notification_description'=>"Requested by, $user_name",
                  'notifiaction_date'=>date('Y-m-d H:i:s'),
                  'notification_action'=>"shiftChangeRequest",
                  'admin_click_action '=>"shiftChangeRequest",
                  'notification_logo'=>'ic_shift.png',
                );
                            
                $d->insert("admin_notification",$notiAry);
               
                $response["message"] = "Shift change request submitted";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Faild to Add";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['getMyShift']=="getMyShift" && $user_id !=0 && $user_id!='' &&  filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 

            $shiftQ = $d->selectRow("shift_timing_master.*,users_master.user_id","shift_timing_master,users_master","shift_timing_master.shift_time_id = users_master.shift_time_id AND shift_timing_master.is_deleted = '0' AND users_master.user_id = '$user_id'");

            if (mysqli_num_rows($shiftQ) > 0) {
                $shiftD = mysqli_fetch_array($shiftQ);

                $todayDate = date('Y-m-d');

                $shiftType = $shiftD['shift_type'];
            
                $shiftStartTimeView = date('h:i A',strtotime($todayDate.' '.$shiftD['shift_start_time']));
                $shift_start_time = $shiftD['shift_start_time'];
                
                if ($shiftType == "Night") {
                    $shiftEndTimeView = date('h:i A',strtotime($todayDate.' '.$shiftD['shift_end_time']));
                }else{              
                    $nextDate = date('Y-m-d',strtotime("+1 days"));         
                    $shiftEndTimeView = date('h:i A',strtotime($nextDate.' '.$shiftD['shift_end_time']));
                }
                
                $shift_end_time = $shiftD['shift_end_time'];
                
                
                $lunch_break_start_time_view = date('h:i A',strtotime($todayDate.' '.$shiftD['lunch_break_start_time']));
                $lunchTimeStart = date('Y-m-d').' '.$lunch_break_start_time_view;
                $lunch_break_start_time = $shiftD['lunch_break_start_time'];
                
                
                $lunch_break_end_time_view = date('h:i A',strtotime($todayDate.' '.$shiftD['lunch_break_end_time']));
                $lunchTimeEnd = date('Y-m-d').' '.$lunch_break_end_time_view;
                $totalLunchTime = getTotalHoursWithNames($lunchTimeStart,$lunchTimeEnd);
                $lunch_break_end_time = $shiftD['lunch_break_end_time'];
                

                if ($shiftD['lunch_break_start_time'] == "00:00:00" && $shiftD['lunch_break_end_time'] == "00:00:00") {
                    $lunch_break_start_time_view = "";
                    $lunchTimeStart = "";
                    $lunch_break_start_time = "";

                    $lunch_break_end_time_view = "";
                    $lunchTimeEnd = "";
                    $lunch_break_end_time = "";
                    $totalLunchTime = "";
                }
                
                $tea_break_start_time_view = date('h:i A',strtotime($todayDate.' '.$shiftD['tea_break_start_time']));
                $tea_break_start_time = $shiftD['tea_break_start_time'];
                $teaTimeStart = date('Y-m-d').' '.$tea_break_start_time_view;
                
                $tea_break_end_time_view = date('h:i A',strtotime($todayDate.' '.$shiftD['tea_break_end_time']));
                $tea_break_end_time = $shiftD['tea_break_end_time'];
                $teaTimeEnd = date('Y-m-d').' '.$tea_break_end_time_view;
                $totalTeaTime = getTotalHoursWithNames($teaTimeStart,$teaTimeEnd);
                

                if ($shiftD['tea_break_start_time'] == "00:00:00" && $shiftD['tea_break_end_time'] == "00:00:00") {
                    $tea_break_start_time_view = "";
                    $teaTimeStart = "";
                    $tea_break_start_time = "";

                    $tea_break_end_time_view = "";
                    $tea_break_end_time = "";
                    $totalTeaTime = "";
                }


                if ($shiftD['half_day_time_start'] == "00:00:00" || $shiftD['half_day_time_start'] == null) {
                    $half_day_time_start_view = "";
                    $half_day_time_start = "";
                }else{
                    $half_day_time_start_view = date('h:i A',strtotime($shiftD['half_day_time_start']));
                    $half_day_time_start = $shiftD['half_day_time_start'];
                }

                if ($shiftD['halfday_before_time'] == "00:00:00" || $shiftD['halfday_before_time'] == null) {
                    $halfday_before_time_view = "";
                    $halfday_before_time = "";
                }else{
                    $halfday_before_time_view = date('h:i A',strtotime($shiftD['halfday_before_time']));
                    $halfday_before_time = $shiftD['halfday_before_time'];
                }

                if ($shiftD['late_time_start'] == "00:00:00" || $shiftD['late_time_start'] == null) {
                    $late_time_start_view = "";
                    $late_time_start = "";
                }else{
                    $late_time_start_view = date('i',strtotime($shiftD['late_time_start'])).' Minutes';
                    $late_time_start = $shiftD['late_time_start'];
                }

                if ($shiftD['early_out_time'] == "00:00:00" || $shiftD['early_out_time'] == null) {
                    $early_out_time_view = "";
                    $early_out_time = "";
                }else{
                    $early_out_time_view = date('i',strtotime($shiftD['early_out_time'])). ' Minutes';
                    $early_out_time = $shiftD['early_out_time'];
                }

                if ($shiftD['maximum_halfday_hours'] == "00:00:00" || $shiftD['maximum_halfday_hours'] == null) {
                    $maximum_halfday_hours_view = "";
                    $maximum_halfday_hours_view_total = "";
                    $maximum_halfday_hours = "";
                }else{
                    $maximum_halfday_hours_view = date('h:i A',strtotime($shiftD['maximum_halfday_hours']));
                    $maximum_halfday_hours = $shiftD['maximum_halfday_hours'];
                    $maximum_halfday_hours_view_total = getFormatedTime($maximum_halfday_hours_view);
                }

                if ($shiftD['minimum_hours_for_full_day'] == "00:00:00" || $shiftD['minimum_hours_for_full_day'] == null) {
                    $minimum_hours_for_full_day_view = "";
                    $minimum_hours_for_full_day = "";
                    $minimum_hours_for_full_day_view_total = "";
                }else{
                    $minimum_hours_for_full_day_view = date('h:i A',strtotime($shiftD['minimum_hours_for_full_day']));
                    $minimum_hours_for_full_day = $shiftD['minimum_hours_for_full_day'];
                    $minimum_hours_for_full_day_view_total = getFormatedTime($minimum_hours_for_full_day_view);
                }

                $per_day_hour_view = getFormatedTime($shiftD['per_day_hour']);  

                $dayNamesAlternate = explode(",",$shiftD['alternate_weekoff_days']);
                $dayNamesWeekOffs = explode(",",$shiftD['week_off_days']);  

                $allWeekOffs = array_merge($dayNamesWeekOffs,$dayNamesWeekOffs);    
                $allWeekOffs = array_unique($allWeekOffs);  

                $allWeekOffsView = "";

                for ($i=0; $i < count($allWeekOffs); $i++) { 
                    if ($allWeekOffs[$i] == 0) {                                                
                        if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
                            $allWeekOffsView .="Sunday (Alternate)"."\n";
                        }else{
                            $allWeekOffsView .="Sunday (Every Week)"."\n";
                        }
                    }else if ($allWeekOffs[$i] == 1) {
                        if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
                            $allWeekOffsView .="Monday (Alternate)"."\n";
                        }else{
                            $allWeekOffsView .="Monday (Every Week)"."\n";
                        }
                    }else if ($allWeekOffs[$i] == 2) {
                        if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
                            $allWeekOffsView .="Tuesday (Alternate)"."\n";
                        }else{
                            $allWeekOffsView .="Tuesday (Every Week)"."\n";
                        }
                    }else if ($allWeekOffs[$i] == 3) {
                        if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
                            $allWeekOffsView .="Wednesday (Alternate)"."\n";
                        }else{
                            $allWeekOffsView .="Wednesday (Every Week)"."\n";
                        }
                    }else if ($allWeekOffs[$i] == 4) {
                        if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
                            $allWeekOffsView .="Thursday (Alternate)"."\n";
                        }else{
                            $allWeekOffsView .="Thursday (Every Week)"."\n";
                        }
                    }else if ($allWeekOffs[$i] == 5) {
                        if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
                            $allWeekOffsView .="Friday (Alternate)"."\n";
                        }else{
                            $allWeekOffsView .="Friday (Every Week)"."\n";
                        }
                    }else if ($allWeekOffs[$i] == 6) {
                        if (in_array($allWeekOffs[$i],$dayNamesAlternate)) {
                            $allWeekOffsView .="Saturday (Alternate)"."\n";
                        }else{
                            $allWeekOffsView .="Saturday (Every Week)"."\n";
                        }
                    }
                }

                $allWeekOffsView = substr($allWeekOffsView, 0, -1);;

                
                $response["shift_time_id"] = $shiftD['shift_time_id'];
                $response["shift_name"] = $shiftD['shift_name'];
                $response["shift_code"] = "S".$shiftD['shift_time_id'];
                $response["per_day_hour"] = $shiftD['per_day_hour'];
                $response["shift_start_time"] = $shift_start_time;
                $response["shift_end_time"] = $shift_end_time;
                $response["lunch_break_start_time"] = $lunch_break_start_time;
                $response["lunch_break_end_time"] = $lunch_break_end_time;
                $response["tea_break_start_time"] = $tea_break_start_time;
                $response["tea_break_end_time"] = $tea_break_end_time;                            
                $response["week_off_days"] = $shiftD['week_off_days'];
                $response["half_day_time_start"] = $half_day_time_start;
                $response["halfday_before_time"] = $halfday_before_time;
                $response["late_time_start"] = $late_time_start;
                $response["maximum_in_out"] = $shiftD['maximum_in_out'];
                $response["early_out_time"] = $early_out_time;
                $response["late_in_reason"] = $shiftD['late_in_reason'];
                $response["early_out_reason"] = $shiftD['early_out_reason'];
                $response["has_altenate_week_off"] = $shiftD['has_altenate_week_off'];
                $response["alternate_week_off"] = $shiftD['alternate_week_off'];
                $response["alternate_weekoff_days"] = $shiftD['alternate_weekoff_days'];
                $response["shift_type"] = $shiftD['shift_type'];
                $response["maximum_halfday_hours"] = $maximum_halfday_hours;
                $response["minimum_hours_for_full_day"] = $minimum_hours_for_full_day;

                $response["dayNamesView"] = $allWeekOffsView;
                $response["per_day_hour_view"] = $per_day_hour_view;
                $response["shift_start_time_view"] = $shiftStartTimeView;
                $response["shift_end_time_view"] = $shiftEndTimeView;
                $response["lunch_break_start_time_view"] = $lunch_break_start_time_view;
                $response["lunch_break_end_time_view"] = $lunch_break_end_time_view;
                $response["total_lunch_time"] = $totalLunchTime;
                $response["tea_break_start_time_view"] = $tea_break_start_time_view;
                $response["tea_break_end_time_view"] = $tea_break_end_time_view;                          
                $response["total_tea_time"] = $totalTeaTime;                          
                $response["half_day_time_start_view"] = $half_day_time_start_view;
                $response["halfday_before_time_view"] = $halfday_before_time_view;
                $response["late_time_start_view"] = $late_time_start_view;
                $response["early_out_time_view"] = $late_time_start_view;
                $response["maximum_halfday_hours_view"] = $maximum_halfday_hours_view;
                $response["maximum_halfday_hours_view_total"] = $maximum_halfday_hours_view_total;
                $response["minimum_hours_for_full_day_view"] = $minimum_hours_for_full_day_view;
                $response["minimum_hours_for_full_day_view_total"] = $minimum_hours_for_full_day_view_total;
                
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);    
                exit();
            }else{
                $response["message"]="Something went wrong!";
                $response["status"]="201";
                echo json_encode($response);
                exit();    
            }
        }else{
            $response["message"]="wrong api key";
            $response["status"]="201";
            echo json_encode($response);
        }
    }
}

function getFormatedTime($time) {

    $timeData = explode(':', $time);

    $hours = $timeData[0];
    $minutes = $timeData[1];

    if ($hours > 0 && $minutes > 0) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        if ($hours>1) {
            return sprintf('%02d Hours', $hours);       
        }else{
            return sprintf('%02d Hour', $hours);        
        }
    }else if ($hours <= 0 && $minutes > 0) {
        if ($minutes>1) {
            return sprintf('%02d Minutes', $minutes);       
        }else{
            return sprintf('%02d Minute', $minutes);        
        }
    }else{
        return "00:00";
    }
}

function getTotalHoursWithNames($startTime, $endTime) {

    $date_a = new DateTime($startTime);
    $date_b = new DateTime($endTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        if ($hours>1) {
            return sprintf('%02d Hours', $hours);       
        }else{
            return sprintf('%02d Hour', $hours);        
        }
    }else if ($hours <= 0 && $minutes > 0) {
        if ($minutes>1) {
            return sprintf('%02d Minutes', $minutes);       
        }else{
            return sprintf('%02d Minute', $minutes);        
        }
    }else{
        return "00:00";
    }
}
?>