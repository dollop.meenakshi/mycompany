<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

$response = array();
extract(array_map("test_input" , $_POST));
$dateTime = date("Y-m-d H:i:s");
$temDate = date("Y-m-d h:i A");

$startDate=date("Y-m-01");
$currentDate=date("Y-m-d");
$lastDate=date("Y-m-t");

$qry = $d->select("attendance_break_history_master","break_start_date != '0000-00-00' AND break_end_date != '0000-00-00'");


if (mysqli_num_rows($qry) > 0) {
    while($dataQry = mysqli_fetch_array($qry)){

        $attendance_break_history_id = $dataQry['attendance_break_history_id'];
        $break_start_date = $dataQry['break_start_date'];
        $break_in_time = $dataQry['break_in_time'];
        $break_end_date = $dataQry['break_end_date'];
        $break_out_time = $dataQry['break_out_time'];

        $total_break_time = getTotalHours($break_start_date,$break_end_date,$break_in_time,$break_out_time);

        $time1 = explode(':', $total_break_time.":00");
        $total_break_time_minutes = ($time1[0]*60) + ($time1[1]) + ($time1[2]/60);


        $m->set_data('total_break_time',$total_break_time);
        $m->set_data('total_break_time_minutes',$total_break_time_minutes);

        $a = array(
            'total_break_time'=>$m->get_data('total_break_time'),
            'total_break_time_minutes'=>$m->get_data('total_break_time_minutes'),
        );

        $punchInQry = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id'");

    }
}


$q = $d->select("attendance_master,shift_timing_master","(attendance_master.attendance_date_start != '0000-00-00' AND attendance_master.attendance_date_end != '0000-00-00' AND multiple_punch_in_out_data != '' AND shift_timing_master.shift_time_id = attendance_master.shift_time_id) OR (attendance_master.attendance_date_start != '0000-00-00' AND attendance_master.attendance_date_end = '0000-00-00' AND multiple_punch_in_out_data != '' AND shift_timing_master.shift_time_id = attendance_master.shift_time_id) ");


if (mysqli_num_rows($q) > 0) {
    while($data = mysqli_fetch_array($q)){

        $attendance_id = $data['attendance_id'];
        $attendance_date_start = $data['attendance_date_start'];
        $punch_in_time = $data['punch_in_time'];
        $attendance_date_end = $data['attendance_date_end'];
        $punch_out_time = $data['punch_out_time'];
        $punch_in_image = $data['punch_in_image'];
        $punch_out_image = $data['punch_out_image'];
        $per_day_hour = $data['per_day_hour'];

        $parts = explode(':', $per_day_hour);
        $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);

        $total_working_hours = getTotalHours($attendance_date_start,$attendance_date_end,$punch_in_time,$punch_out_time);

        $time = explode(':', $total_working_hours.":00");
        $total_working_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

        $dataArray = array();

        if ($attendance_date_end == "0000-00-00") {
            $total_working_hours = "00:00";
            $total_working_minutes = 0;
            $attendance_date_end = "";
            $punch_out_time = "";
            $punch_out_image = "";
        }

        $multPunchAry = array(
            "punch_in_date" => $attendance_date_start.'',
            "punch_in_time" => $punch_in_time.'',
            "punch_out_date" => $attendance_date_end.'',
            "punch_out_time" => $punch_out_time.'',
            "punch_in_image" => $punch_in_image.'',
            "punch_out_image" => $punch_out_image.'',
            "location_name_in" => '',
            "location_name_out" => '',
            "working_hour" => $total_working_hours.'',
            "working_hour_minute" => $total_working_minutes.'',
        );

        array_push($dataArray,$multPunchAry);
        $multPunchDataJson = json_encode($dataArray);

        $avgWorkingDays = $total_working_minutes/$perDayHourMinute;
        $avg_working_days = round($avgWorkingDays * 2) / 2;

        $extra_working_hours_minutes = 0;

        $extra_working_hours=0;

        if ($total_working_minutes > $perDayHourMinute) {
            $extra_working_hours_minutes = $total_working_minutes - $perDayHourMinute;
            $hours  = floor($extra_working_hours_minutes/60);
            $minutes = $extra_working_hours_minutes % 60;

            $extra_working_hours = sprintf('%02d:%02d', $hours, $minutes);
        }

        $qryBreak = $d->sum_data("total_break_time_minutes","attendance_break_history_master","attendance_id = '$attendance_id'");

        $qryBreakData = mysqli_fetch_array($qryBreak);

        $totalBreakinutes = $qryBreakData['SUM(total_break_time_minutes)'];

        $productive_working_hours_minutes = 0;
        $productive_working_hours = '0';

        if ($total_working_minutes > $totalBreakinutes) {
            $productive_working_hours_minutes = $total_working_minutes - $totalBreakinutes;

            $hoursPM  = floor($productive_working_hours_minutes/60);
            $minutesPM = $productive_working_hours_minutes % 60;

            $productive_working_hours = $hoursPM.':'.$minutesPM;
        }else{
            $productive_working_hours_minutes = $total_working_minutes;

            $hoursPM  = floor($productive_working_hours_minutes/60);
            $minutesPM = $productive_working_hours_minutes % 60;

            $productive_working_hours = sprintf('%02d:%02d', $hoursPM, $minutesPM);
        }

        $m->set_data('total_shift_hours',$per_day_hour);
        $m->set_data('multiple_punch_in_out_data',$multPunchDataJson);
        $m->set_data('last_punch_in_date',$attendance_date_start);
        $m->set_data('last_punch_in_time',$punch_in_time);
        $m->set_data('total_working_hours',$total_working_hours);
        $m->set_data('total_working_minutes',$total_working_minutes);
        $m->set_data('avg_working_days',$avg_working_days);
        $m->set_data('extra_working_hours',$extra_working_hours);
        $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
        $m->set_data('productive_working_hours',$productive_working_hours);
        $m->set_data('productive_working_hours_minutes',$productive_working_hours_minutes);

        $a = array(
            'total_shift_hours'=>$m->get_data('total_shift_hours'),
            'multiple_punch_in_out_data'=>$m->get_data('multiple_punch_in_out_data'),
            'last_punch_in_date'=>$m->get_data('last_punch_in_date'),
            'last_punch_in_time'=>$m->get_data('last_punch_in_time'),
            'total_working_hours'=>$m->get_data('total_working_hours'),
            'total_working_minutes'=>$m->get_data('total_working_minutes'),
            'avg_working_days'=>$m->get_data('avg_working_days'),
            'extra_working_hours'=>$m->get_data('extra_working_hours'),
            'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
            'productive_working_hours'=>$m->get_data('productive_working_hours'),
            'productive_working_hours_minutes'=>$m->get_data('productive_working_hours_minutes'),
        );

        $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");

    }

    $leaveQry = $d->selectRow("leave_id,user_id,leave_start_date,leave_day_type","leave_master","society_id = '$society_id' AND leave_status = 1");

    if (mysqli_num_rows($leaveQry) > 0) {
        while($dataLeave = mysqli_fetch_array($leaveQry)){

            $leave_id = $dataLeave['leave_id'];
            $user_id = $dataLeave['user_id'];
            $leave_start_date = $dataLeave['leave_start_date'];
            $leave_day_type_view = $dataLeave['leave_day_type'];

            if ($leave_day_type_view == 0) {
                $leave_day_type = 1;
            }else if ($leave_day_type_view == 1) {
                $leave_day_type = 2;
            }else{
                $leave_day_type = 0;            
            }

            $attQry = $d->selectRow("attendance_id","attendance_master","user_id = '$user_id' AND attendance_date_start = '$leave_start_date'");

            if (mysqli_num_rows($attQry) > 0) {
                while($dataAtt = mysqli_fetch_array($attQry)){

                    $attendance_id = $dataAtt['attendance_id'];

                    $m->set_data('is_leave',$leave_day_type);

                    $a = array(
                        'is_leave'=>$m->get_data('is_leave'),
                    );

                    $attUpdate = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");
                }
            }
        }
    }

    $holidayQry = $d->selectRow("holiday_start_date","holiday_master","society_id = '$society_id' AND holiday_status = 0");

    if (mysqli_num_rows($holidayQry) > 0) {
        while($dataLeave = mysqli_fetch_array($holidayQry)){

            $holiday_start_date = $dataLeave['holiday_start_date'];


            $attQry = $d->selectRow("attendance_id,attendance_date_start,user_id","attendance_master","attendance_date_start = '$holiday_start_date'");

            if (mysqli_num_rows($attQry) > 0) {
                while($dataAtt = mysqli_fetch_array($attQry)){

                    $attendance_id = $dataAtt['attendance_id'];
                    $attendance_date_start = $dataAtt['attendance_date_start'];
                    $user_id = $dataAtt['user_id'];

                    $lq = $d->selectRow("leave_id,user_id,leave_start_date,leave_day_type","leave_master","society_id = '$society_id' AND user_id = '$user_id' AND leave_status = 1 AND leave_start_date = '$attendance_date_start'");

                    $leave_day_type = "0";

                    if (mysqli_num_rows($lq) > 0) {
                        $lqData = mysqli_fetch_array($lq);

                        $leave_id = $lqData['leave_id'];
                        $user_id = $lqData['user_id'];
                        $leave_start_date = $lqData['leave_start_date'];
                        $leave_day_type_view = $lqData['leave_day_type'];

                        if ($leave_day_type_view == 0) {
                            $leave_day_type = 1;
                        }else if ($leave_day_type_view == 1) {
                            $leave_day_type = 2;
                        }else{
                            $leave_day_type = 0;            
                        }
                    }

                    $m->set_data('is_leave',"0");
                    $m->set_data('is_extra_day',"1");
                    $m->set_data('extra_day_leave_type',$leave_day_type);

                    $a = array(
                        'is_leave'=>$m->get_data('is_leave'),
                        'is_extra_day'=>$m->get_data('is_extra_day'),
                        'extra_day_leave_type'=>$m->get_data('extra_day_leave_type'),
                    );

                    $attUpdate = $d->update("attendance_master",$a,"attendance_id = '$attendance_id'");
                }
            }
        }
    }
}


function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}
?>