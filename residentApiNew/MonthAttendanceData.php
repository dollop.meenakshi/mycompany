<?php
include_once 'lib.php';

/* ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL); */
 
if(isset($_POST) && !empty($_POST)){

    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    $startDate=date("Y-m-01");
    $currentDate=date("Y-m-d");
    $lastDate=date("Y-m-t");

    if($_POST['getMonthlyAttendanceHistoryNew']=="getMonthlyAttendanceHistoryNew" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qq12 = $d->selectRow("hide_working_hours","society_master","society_id = '$society_id'");

        $socData = mysqli_fetch_array($qq12);

        if ($socData['hide_working_hours'] == 1) {
            $response['hide_working_hours'] = true;
        }else{
            $response['hide_working_hours'] = false;                
        }

    
        $leaveArray = array();
        $paidLeaveArray = array();
        $halfHeaveArray = array();
        $paidHalfHeaveArray = array();
        $fullDayDateAry = array();
        $halfDayDateArray = array();

        $allDataFullDayLeave = [];
        $allDataHalfDayLeave = [];
        $totalWeekOff = 0;
        $totalAbsentDays = 0;
        $totalExtraDays = 0;
        $totalWorkingDays = 0;
        $totalIncompleteDays = 0;
        $totalLatePunchIN = 0;
        $totalEarlyPunchOUT = 0;
        $totalPendingAttendance = 0;
        $totalRejectedAttendance = 0;
        $totalPunchOUTMissing = 0;

        $presentDaysAry = array();
        $totalMonthHourSpent = array();
        $arrayAttendanceData = array();
        $all_dates = [];
        $arrayDayWiseHours = array();
        $weekAry = array();

        $holidayDataArray = array();
        $holidayAry = array();
        $holiday_dates = [];

        $totalHoursWeekDays = 0;
        $totalHoursWeekOffs = 0;
        $perdayHoursMonday = 0;
        $perdayHoursTuesday = 0;
        $perdayHoursWednesday = 0;
        $perdayHoursThursday = 0;
        $perdayHoursFriday = 0;
        $perdayHoursSaturday = 0;
        $perdayHoursSunday = 0;

        $sq = $d->selectRow("
            shift_timing_master.shift_time_id,
            shift_timing_master.week_off_days,
            shift_timing_master.has_altenate_week_off,
            shift_timing_master.alternate_week_off,
            shift_timing_master.alternate_weekoff_days,
            shift_day_master.per_day_hour,
            shift_day_master.shift_type,
            shift_day_master.shift_day,
            shift_day_master.shift_day_name

            ","
            users_master,
            shift_timing_master,
            shift_day_master
            ","
            users_master.user_id = '$user_id' 
            AND shift_timing_master.shift_time_id = users_master.shift_time_id 
            AND shift_timing_master.shift_time_id = shift_day_master.shift_time_id");            

        if (mysqli_num_rows($sq)>0) {

            while($shiftData = mysqli_fetch_array($sq)){

                $shift_time_id = $shiftData['shift_time_id'];

                $perdayHours = $shiftData['per_day_hour'];
                $shiftType = $shiftData['shift_type'];
                $shift_day = $shiftData['shift_day'];
                $shift_day_name = $shiftData['shift_day_name'];

                $week_off_days = $shiftData['week_off_days'];
                $has_altenate_week_off = $shiftData['has_altenate_week_off'];
                $alternate_week_off = $shiftData['alternate_week_off'];
                $alternate_weekoff_days = $shiftData['alternate_weekoff_days'];

                $weekOffDays = array();

                if ($week_off_days != '') {
                    $weekOffDays = explode(",",$week_off_days);
                }

                $alternateWeekOff = array();

                if ($alternate_week_off != '') {
                    $alternateWeekOff = explode(",",$alternate_week_off);
                }

                $alternateWeekOffDays = array();

                if ($alternate_weekoff_days != '') {
                    $alternateWeekOffDays = explode(",",$alternate_weekoff_days);
                }

                $parts = explode(':', $perdayHours);
                $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;     

                if ($shift_day == 0) {
                    $perdayHoursSunday = $perdayHours;
                }else if ($shift_day == 1) {
                    $perdayHoursMonday = $perdayHours;
                }  else if ($shift_day == 2) {
                    $perdayHoursTuesday = $perdayHours;
                }  else if ($shift_day == 3) {
                    $perdayHoursWednesday = $perdayHours;
                }  else if ($shift_day == 4) {
                    $perdayHoursThursday = $perdayHours;
                }  else if ($shift_day == 5) {
                    $perdayHoursFriday = $perdayHours;
                }  else if ($shift_day == 6) {
                    $perdayHoursSaturday = $perdayHours;
                }       
            }
        }       

        $response['monthly_history'] = array();
        $response['month'] = date('F Y',strtotime($month_start_date));

        $datesArrayOfMonth = range_date($month_start_date,$month_end_date);

        $currdt= strtotime($month_start_date);
        $nextmonth=strtotime($month_start_date."+1 month");
        $totalMonthHours = 0;


        // Leave Data

        $leaveQry = $d->select("leave_master","society_id = '$society_id'
                AND user_id = '$user_id'
                AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'
                AND leave_status = '1'");

        if (mysqli_num_rows($leaveQry) > 0) {

            while($leaveData = mysqli_fetch_array($leaveQry)){

                if ($leaveData['leave_day_type'] == "0") {
                    array_push($fullDayDateAry,$leaveData['leave_start_date']);
                    $allDataFullDayLeave[$leaveData['leave_start_date']] = $leaveData;
                    $lary = range_date($leaveData['leave_start_date'],
                        $leaveData['leave_end_date']);

                    for ($a = 0; $a < count($lary); $a++) {
                        array_push($leaveArray,$lary[$a]);  

                        if ($leaveData['paid_unpaid'] == 0) {
                            array_push($paidLeaveArray,$lary[$a]);
                        }                          
                    } 
                }else{
                    array_push($halfDayDateArray,$leaveData['leave_start_date']);
                    $allDataHalfDayLeave[$leaveData['leave_start_date']] = $leaveData;
                    $laryHalf = range_date($leaveData['leave_start_date'],
                        $leaveData['leave_end_date']);

                    for ($b = 0; $b < count($laryHalf); $b++) {
                        array_push($halfHeaveArray,$laryHalf[$b]);

                        if ($leaveData['paid_unpaid'] == 0) {
                            array_push($paidHalfHeaveArray,$laryHalf[$b]);
                        }
                    }                        
                }
            }
        }
        
        $holidayNameQry = $d->select("holiday_master","society_id = '$society_id' AND holiday_start_date BETWEEN '$month_start_date' AND '$month_end_date' AND holiday_status = '0'","ORDER BY holiday_start_date ASC");

        if (mysqli_num_rows($holidayNameQry) > 0) {
            while($holidayData = mysqli_fetch_array($holidayNameQry)){
                array_push($holidayAry,$holidayData['holiday_start_date']);
                $holiday_dates[$holidayData['holiday_start_date']] = $holidayData;
            }
        }

        // Month

        $attendanceQryAll = $d->selectRow("
                am.*,
                hm.holiday_name,
                hm.holiday_description,
                apomr.attendance_id as attid, 
                apomr.attendance_punch_out_missing_id, 
                apomr.punch_out_missing_reject_reason,
                apomr.attendance_punch_out_missing_status
                ","
                attendance_master AS am 
                LEFT JOIN attendance_punch_out_missing_request AS apomr ON apomr.attendance_id = am.attendance_id         
                LEFT JOIN holiday_master AS hm ON hm.holiday_start_date = am.attendance_date_start         
                ","
                am.society_id='$society_id' 
                AND am.user_id = '$user_id'
                AND am.attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date'");            

        while($attData = mysqli_fetch_array($attendanceQryAll)){
            for($i = 0; $i < count($attData); $i++){
                if($i % 2 || !($i % 2)){
                    unset($attData[$i]);
                }
            }
            $all_dates[$attData['attendance_date_start'].''] = $attData;
            array_push($arrayAttendanceData,$attData);
        }            

        for ($j = 0; $j < count($datesArrayOfMonth); $j++) {
            
            $dates = array();

            $wee = false;

            $monthDate = $datesArrayOfMonth[$j];
            $month = date('Y-m',strtotime($monthDate));
            $dates['date'] = $monthDate;
            $day_pos_new =  date('w', strtotime($monthDate));
            $weekOfMonth =  weekOfMonth(strtotime($monthDate));
            $dates['date_name'] = date('dS F Y',strtotime($monthDate));
            $dates['day_name'] = date('l',strtotime($monthDate));

            if (isset($weekOffDays) && count($weekOffDays) > 0 && !in_array($day_pos_new,$weekOffDays) && (isset($alternateWeekOffDays) && !in_array($day_pos_new,$alternateWeekOffDays)) && (isset($holidayAry) && !in_array($monthDate,$holidayAry)) && (isset($fullDayDateAry) && !in_array($monthDate,$fullDayDateAry))) {
                if ($day_pos == 0) {
                    if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                        array_push($arrayDayWiseHours,$perdayHoursSunday);
                    }else{
                        array_push($arrayDayWiseHours,($perdayHoursSunday/2));                                
                    }
                }else if ($day_pos == 1) {
                    if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                        array_push($arrayDayWiseHours,$perdayHoursMonday);
                    }else{
                        array_push($arrayDayWiseHours,($perdayHoursMonday/2));                                
                    }
                }else if ($day_pos == 2) {
                    if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                        array_push($arrayDayWiseHours,$perdayHoursTuesday);
                    }else{
                        array_push($arrayDayWiseHours,($perdayHoursTuesday/2));                                
                    }
                }else if ($day_pos == 3) {
                    if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                        array_push($arrayDayWiseHours,$perdayHoursWednesday);
                    }else{
                        array_push($arrayDayWiseHours,($perdayHoursWednesday/2));                                
                    }
                }else if ($day_pos == 4) {
                    if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                        array_push($arrayDayWiseHours,$perdayHoursThursday);
                    }else{
                        array_push($arrayDayWiseHours,($perdayHoursThursday/2));                                
                    }
                }else if ($day_pos == 5) {
                    if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                        array_push($arrayDayWiseHours,$perdayHoursFriday);
                    }else{
                        array_push($arrayDayWiseHours,($perdayHoursFriday/2));                                
                    }
                }else if ($day_pos == 6) {
                    if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                        array_push($arrayDayWiseHours,$perdayHoursSaturday);
                    }else{
                        array_push($arrayDayWiseHours,($perdayHoursSaturday/2));                                
                    }
                }
            }

            if ($has_altenate_week_off == 1) {
                if (isset($alternateWeekOffDays) && isset($alternateWeekOff) && in_array($day_pos_new,$alternateWeekOffDays) && in_array($weekOfMonth,$alternateWeekOff) && isset($holidayAry) && !in_array($monthDate,$holidayAry) && isset($fullDayDateAry) && !in_array($monthDate,$fullDayDateAry)) {
                    
                    if ($day_pos == 0) {
                        if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                            array_push($arrayDayWiseHours,$perdayHoursSunday);
                        }else{
                            array_push($arrayDayWiseHours,($perdayHoursSunday/2));                                
                        }
                    }else if ($day_pos == 1) {
                        if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                            array_push($arrayDayWiseHours,$perdayHoursMonday);
                        }else{
                            array_push($arrayDayWiseHours,($perdayHoursMonday/2));                                
                        }
                    }else if ($day_pos == 2) {
                        if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                            array_push($arrayDayWiseHours,$perdayHoursTuesday);
                        }else{
                            array_push($arrayDayWiseHours,($perdayHoursTuesday/2));                                
                        }
                    }else if ($day_pos == 3) {
                        if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                            array_push($arrayDayWiseHours,$perdayHoursWednesday);
                        }else{
                            array_push($arrayDayWiseHours,($perdayHoursWednesday/2));                                
                        }
                    }else if ($day_pos == 4) {
                        if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                            array_push($arrayDayWiseHours,$perdayHoursThursday);
                        }else{
                            array_push($arrayDayWiseHours,($perdayHoursThursday/2));                                
                        }
                    }else if ($day_pos == 5) {
                        if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                            array_push($arrayDayWiseHours,$perdayHoursFriday);
                        }else{
                            array_push($arrayDayWiseHours,($perdayHoursFriday/2));                                
                        }
                    }else if ($day_pos == 6) {
                        if (isset($halfDayDateArray) && !in_array($monthDate,$halfDayDateArray)) {
                            array_push($arrayDayWiseHours,$perdayHoursSaturday);
                        }else{
                            array_push($arrayDayWiseHours,($perdayHoursSaturday/2));                                
                        }
                    }
                }
            }

            $dates["total_spend_time"] = "0";

            $dates['holiday'] = false;
            $dates['week_off'] = false;
            $dates['work_report'] = false;
            $dates['leave'] = false;
            $dates['present'] = false;
            $dates['half_day'] = false;
            $dates['extra_day'] = false;
            $dates['late_in'] = false;
            $dates['early_out'] = false;
            $dates['is_punch_out_missing'] = false;
            $dates['punch_in_request_sent'] = false;

            $isDateGone = "false";

            $dates["punch_out_missing_message"] = "";

            if ($monthDate < date('Y-m-d')) {
                $isDateGone = "true";
                $dates['is_date_gone'] = true;
            }else{
                $isDateGone = "false";
                $dates['is_date_gone'] = false;
            }

            $dates["holiday_name"] = "";
            $dates["holiday_description"] = "";

            if (isset($holidayAry) && in_array($monthDate,$holidayAry)) {
                $dates['holiday'] = true;
                $holidayData = $holidayAry;
                $dates["holiday_name"] = $holidayData['holiday_name'];
                $dates["holiday_description"] = $holidayData['holiday_description'];
            }else{
                if (isset($weekOffDays) && count($weekOffDays) > 0 && in_array($day_pos_new,$weekOffDays)
                && (isset($alternateWeekOffDays) && !in_array($day_pos_new,$alternateWeekOffDays)) && (isset($holidayAry) && !in_array($monthDate,$holidayAry))) {
                    $dates['week_off'] = true;
                    $totalWeekOff += 1;
                    $wee = true;
                }
                if ($has_altenate_week_off == 1) {
                    if (isset($alternateWeekOffDays) && in_array($day_pos_new,$alternateWeekOffDays) && 
                        in_array($weekOfMonth,$alternateWeekOff) && isset($holidayAry) && !in_array($monthDate,$holidayAry)) {
                        $dates['week_off'] = true;
                        $totalWeekOff += 1;
                        $wee = true;
                    }
                }

                if ($wee == false) {
                    $totalWorkingDays = $totalWorkingDays + 1;
                }
            }

            if (isset($weekOffDays) && isset($alternateWeekOffDays) && count($weekOffDays) > 0 && in_array($day_pos_new,$weekOffDays)
                && !in_array($day_pos_new,$alternateWeekOffDays)) {
                array_push($weekAry,$monthDate);
            }
            if ($has_altenate_week_off == 1) {
                if (isset($alternateWeekOffDays) && isset($alternateWeekOff) && in_array($day_pos_new,$alternateWeekOffDays) && 
                    in_array($weekOfMonth,$alternateWeekOff)) {
                    array_push($weekAry,$monthDate);
                }
            }

            $dates["leave_reason"] = "";
            $dates["auto_leave_reason"] = "";
            $dates["leave_type_name"] = "";
            $dates["leave_day_view"] = "";

            if ((isset($leaveArray) && in_array($monthDate,$leaveArray)) || (isset($halfHeaveArray) || in_array($monthDate,$halfHeaveArray))) {
                
                $isDateGone = "false";

                if (isset($leaveArray) && in_array($monthDate,$leaveArray)) {
                    $dates['leave'] = true;
                    $leaveDataTrue = $allDataFullDayLeave[$monthDate];
                }

                if (isset($halfHeaveArray) && in_array($monthDate,$halfHeaveArray)) {
                    $dates['half_day'] = true;
                    $leaveDataTrue = $allDataHalfDayLeave[$monthDate];
                }


                if ($leaveDataTrue['leave_type_id'] != null && $leaveDataTrue['leave_type_id'] != 0){
                    $dates["leave_type_name"] = $leaveDataTrue['leave_type_name'].'';                    
                }else{
                    $dates["leave_type_name"] = "";
                }

                if ($leaveDataTrue['paid_unpaid'] == 0) {
                    if ($leaveDataTrue['leave_day_type'] == 0) {
                        $dates["leave_day_view"] = "Paid Leave - Full Day";
                    }else{
                        $dates["leave_day_view"] = "Paid Leave - Half Day";
                    }
                }else if ($leaveDataTrue['paid_unpaid'] == 1) {
                    if ($leaveDataTrue['leave_day_type'] == 0) {
                        $dates["leave_day_view"] = "Unpaid Leave - Full Day";
                    }else{
                        $dates["leave_day_view"] = "Unpaid Leave - Half Day";
                    }
                }else{
                    $dates["leave_day_view"] = "";
                }

                $dates["leave_reason"] = $leaveDataTrue['leave_reason'].'';
                $dates["auto_leave_reason"] = $leaveDataTrue['auto_leave_reason'].'';

                if ($leaveDataTrue['leave_type_id'] > 0) {
                    $dates["auto_leave_reason"] = "";
                }         
            }                

            $tdt = date('Y-m-d');

            if(isset($all_dates[$monthDate]) && in_array($monthDate,$all_dates[$monthDate])){

                $times = array();

                $data = $all_dates[$monthDate];

                $dates['present'] = true;

                if ($data['attendance_status'] == "0") {
                    $totalPendingAttendance = $totalPendingAttendance + 1;
                    $dates["attendnace_pending"] = true;
                    $dates["attendance_pending_message"] = "Attendance Pending";
                }else{
                    $dates["attendnace_pending"] = false;
                    $dates["attendance_pending_message"] = "";
                }

                if ($data['attendance_status'] == "0" && $data['punch_in_request'] == "1") {
                    $dates["punch_in_request_sent"] = true;
                }else{
                    $dates["punch_in_request_sent"] = false;
                }

                if ($data['attendance_status'] == "2") {
                    $totalRejectedAttendance = $totalRejectedAttendance + 1;
                    $dates["attendance_declined"] = true;
                    $dates["attendance_declined_message"] = $data['attendance_declined_reason'];
                    if ($data['punch_in_request'] == "1"){
                        $dates["punch_in_request_sent"] = true;
                    }
                }else{
                    $dates["attendance_declined"] = false;
                    $dates["attendance_declined_message"] = "";
                }

                $attendanceDateStart = $data['attendance_date_start'];
                $attendanceDateEnd = $data['attendance_date_end'];

                array_push($presentDaysAry,$attendanceDateStart);

                if ($data['attendance_punch_out_missing_id'] != null) {
                   
                    $dates['attendance_punch_out_missing_id'] = $data['attendance_punch_out_missing_id'];
                    $dates['punch_out_missing_reject_reason'] = $data['punch_out_missing_reject_reason'];

                    $attendance_punch_out_missing_status = $data['attendance_punch_out_missing_status'];

                    $dates['attendance_punch_out_missing_status'] = $attendance_punch_out_missing_status;
                    
                    if ($attendance_punch_out_missing_status == "0") {
                        $dates['attendance_punch_out_missing_status_view'] = "Pending";
                    }else if ($attendance_punch_out_missing_status == "1") {
                        $dates['attendance_punch_out_missing_status_view'] = "Approved";
                    }else if ($attendance_punch_out_missing_status == "2") {
                        $dates['attendance_punch_out_missing_status_view'] = "Rejected";
                    }
                }

                $dates["work_report"] = false;

                $incompleteCount = false;

                // new Check

                if (isset($weekAry) && in_array($attendanceDateStart,$weekAry) && $data['attendance_status'] == 1) {
                    $dates['extra_day'] = true;
                    if (isset($halfHeaveArray) && in_array($attendanceDateStart,$halfHeaveArray)) {
                        if ($data['punch_out_time'] != "00:00:00") {
                            $totalExtraDays = $totalExtraDays + 0.5;
                        }
                        $key = array_search($attendanceDateStart, $halfHeaveArray);
                        if (false !== $key) {
                            unset($halfHeaveArray[$key]);
                        }
                    }else if (isset($leaveArray) && in_array($attendanceDateStart,$leaveArray)) {
                        $totalIncompleteDays += 1;
                        $incompleteCount = true;

                        $key = array_search($attendanceDateStart, $leaveArray);
                        if (false !== $key) {
                            unset($leaveArray[$key]);
                        }
                    }else{
                        $totalExtraDays = $totalExtraDays + 1;
                    }
                }

                // new Check

                if (isset($weekAry) && in_array($attendanceDateStart,$weekAry) && $data['attendance_status'] == 2) {
                    $dates['extra_day'] = true;
                    if (isset($halfHeaveArray) && in_array($attendanceDateStart,$halfHeaveArray)) {
                        
                        $key = array_search($attendanceDateStart, $halfHeaveArray);
                        if (false !== $key) {
                            unset($halfHeaveArray[$key]);
                        }
                    }else if (isset($leaveArray) && in_array($attendanceDateStart,$leaveArray)) {                            
                        $totalIncompleteDays += 1;
                        $incompleteCount = true;
                        $key = array_search($attendanceDateStart, $leaveArray);
                        if (false !== $key) {
                            unset($leaveArray[$key]);
                        }
                    }
                }


                if (isset($holiday_dates[$attendanceDateStart]) && in_array($attendanceDateStart,$holiday_dates[$attendanceDateStart]) && $data['attendance_status'] == 1) {
                    $dates['extra_day'] = true;
                    
                    if (isset($halfHeaveArray) && in_array($attendanceDateStart,$halfHeaveArray)) {
                        if ($data['punch_out_time'] != "00:00:00") {
                            $totalExtraDays = $totalExtraDays + 0.5;
                        }
                        $key = array_search($attendanceDateStart, $halfHeaveArray);
                        if (false !== $key) {
                            unset($halfHeaveArray[$key]);
                        }
                    }else if (isset($leaveArray) && in_array($attendanceDateStart,$leaveArray)) {
                        $key = array_search($attendanceDateStart, $leaveArray);
                        if (false !== $key) {
                            unset($leaveArray[$key]);
                        }

                        $totalIncompleteDays += 1;
                        $incompleteCount = true;
                    }else{
                        $totalExtraDays = $totalExtraDays + 1;
                    }
                }
                
                if (isset($holiday_dates[$attendanceDateStart]) && in_array($attendanceDateStart,$holiday_dates[$attendanceDateStart]) && $data['attendance_status'] == 2) {
                    $dates['extra_day'] = true;

                    if (isset($halfHeaveArray) && in_array($attendanceDateStart,$halfHeaveArray)) {
                        
                        $key = array_search($attendanceDateStart, $halfHeaveArray);
                        if (false !== $key) {
                            unset($halfHeaveArray[$key]);
                        }
                    }else if (isset($leaveArray) && in_array($attendanceDateStart,$leaveArray)) {
                        $totalIncompleteDays += 1;
                        $incompleteCount = true;
                        $key = array_search($attendanceDateStart, $leaveArray);
                        if (false !== $key) {
                            unset($leaveArray[$key]);
                        }
                    }
                }

                if (isset($leaveArray) && in_array($attendanceDateStart,$leaveArray) && $data['attendance_status'] == 1 && $incompleteCount == false) {
                    $totalIncompleteDays += 1;
                }

                $punchInTime = $data['punch_in_time'];
                $punchOutTime = $data['punch_out_time'];

                $dates['attendance_id'] = $data['attendance_id'];
                
                if ($data['max_attendance_punch_out_time'] != null ) {
                    $dates['max_attendance_punch_out_date'] = date('Y-m-d',strtotime($data['max_attendance_punch_out_time'])).'';
                }else{
                    $dates['max_attendance_punch_out_date'] = '';
                }

                if ($data['max_attendance_punch_out_time'] != null ) {
                    $dates['max_attendance_punch_out_time'] = date('H:i:s',strtotime($data['max_attendance_punch_out_time'])).'';
                }else{
                    $dates['max_attendance_punch_out_time'] = '';
                }

                $dates['unit_id'] = $data['unit_id'];
                $dates['punch_in_time'] = date("h:i A",strtotime($punchInTime));
                $dates['punch_out_time'] = date("h:i A",strtotime($punchOutTime));

                $multiple_punch_in_out_data = $data['multiple_punch_in_out_data'];
                $multiplePunchDataArray = json_decode($multiple_punch_in_out_data,true);

                $dates['punch_in_data'] = array();

                if (isset($multiplePunchDataArray) && count($multiplePunchDataArray) > 0) {
                    for ($i=0; $i < count($multiplePunchDataArray); $i++) { 
                        $punchInData = array();

                        $punchInData['punch_in_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_in_date"]));
                        $punchInData['punch_in_time'] = date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_in_time"]));

                        if ($multiplePunchDataArray[$i]["punch_out_date"] != '' && $multiplePunchDataArray[$i]["punch_out_date"] != "00:00:00") {
                            $punchInData['punch_out_date'] = date('d M Y',strtotime($multiplePunchDataArray[$i]["punch_out_date"]));
                        }else{
                            if ($attendanceDateStart != date('Y-m-d')) {
                                $punchInData['punch_out_date'] = "Missing";
                            }else{                                    
                                $punchInData['punch_out_date'] = "";
                            }
                        }

                        if ($multiplePunchDataArray[$i]["punch_out_time"] != '' && $multiplePunchDataArray[$i]["punch_out_time"] != "00:00:00") {
                            $punchInData['punch_out_time'] = date('h:i A',strtotime($multiplePunchDataArray[$i]["punch_out_time"]));
                        }else{
                            $punchInData['punch_out_time'] = "";
                        }

                        if ($multiplePunchDataArray[$i]["working_hour"] != '' && $multiplePunchDataArray[$i]["working_hour"] != "00:00") {
                            $punchInData['working_hour'] = $multiplePunchDataArray[$i]["working_hour"];
                        }else{
                            $punchInData['working_hour'] = "";
                        }

                        if ($multiplePunchDataArray[$i]["location_name_in"] != '') {
                            $punchInData['location_name_in'] = $multiplePunchDataArray[$i]["location_name_in"];
                        }else{
                            $punchInData['location_name_in'] = "";
                        }

                        if ($multiplePunchDataArray[$i]["location_name_out"] != '') {
                            $punchInData['location_name_out'] = $multiplePunchDataArray[$i]["location_name_out"];
                        }else{
                            $punchInData['location_name_out'] = "";
                        }

                        $punchInData['working_hour_minute'] = $multiplePunchDataArray[$i]["working_hour_minute"].'';

                        array_push($dates['punch_in_data'],$punchInData);
                    }
                }

                if ($attendanceDateStart != $attendanceDateEnd) {
                    $dates['punch_out_time'] = date("h:i A",strtotime($punchOutTime)).' ('.date('dS M',strtotime($attendanceDateEnd)).')';
                }

                if ($data['late_in'] == 1) {
                    $totalLatePunchIN = $totalLatePunchIN + 1;
                    $dates['late_in'] = true;
                }
                 
                if ($data['early_out'] == 1) {
                    $totalEarlyPunchOUT = $totalEarlyPunchOUT + 1;
                    $dates['early_out'] = true;
                }
                
                if ($attendanceDateEnd == "0000-00-00" || $punchOutTime == "00:00:00") {
                    $dates['punch_out_time']= "";
                    $attendanceDateEnd = date('Y-m-d');
                    $punchOutTime = date('H:i:s');
                }

                // new check

                if ($data['punch_out_time'] != "00:00:00" && isset($weekAry) && !in_array($attendanceDateStart,$weekAry)
                    && isset($holiday_dates[$attendanceDateStart]) && !in_array($attendanceDateStart,$holiday_dates[$attendanceDateStart]) && $data['attendance_status'] == "1") {

                    $totalHours = $data['total_working_hours'];
                    $productive_working_hours = $data['productive_working_hours'];
                    $productive_working_hours_minutes = $data['productive_working_hours_minutes'];

                    $dates['total_hours_spend'] = timeFormat($totalHours);
                    $dates['productive_working_hours'] = timeFormat($productive_working_hours);
                    $dates['productive_working_hours_minutes'] = $productive_working_hours_minutes;
                    $dates['total_spend_time'] = "";
                    $dates["punch_out_missing_message"] = "";
                    $dates['is_punch_out_missing'] = false;

                }else if ($data['punch_out_time'] != "00:00:00" && isset($weekAry) && (in_array($attendanceDateStart,$weekAry)
                    || (isset($holiday_dates[$attendanceDateStart]) && in_array($attendanceDateStart,$holiday_dates[$attendanceDateStart]))) && $data['attendance_status'] == "1") {

                    $totalHoursExtra = $data['total_working_hours'];
                    $productive_working_hours = $data['productive_working_hours'];
                    $productive_working_hours_minutes = $data['productive_working_hours_minutes'];

                    $dates['total_hours_spend'] = timeFormat($totalHoursExtra);
                    $dates['productive_working_hours'] = timeFormat($productive_working_hours);
                    $dates['productive_working_hours_minutes'] = $productive_working_hours_minutes;
                    $dates['total_spend_time'] = "";
                    $dates["punch_out_missing_message"] = "";
                    $dates['is_punch_out_missing'] = false;
                }else{

                    $totalHours = $data['total_working_hours'];
                    $productive_working_hours = $data['productive_working_hours'];
                    $productive_working_hours_minutes = $data['productive_working_hours_minutes'];

                    if (($totalHours != '' && $totalHours != "00:00:00") || 
                        ($totalHours != '' && $totalHours != "00:00")) {
                        $dates['total_hours_spend'] = timeFormat($totalHours);;
                    }else{                            
                        $dates['total_spend_time'] = "";
                        $dates['total_hours_spend'] = "";
                    }

                    if (($productive_working_hours != '' && $productive_working_hours != "00:00:00") || 
                        ($productive_working_hours != '' && $productive_working_hours != "00:00")) {
                        $dates['productive_working_hours'] = timeFormat($productive_working_hours);
                        $dates['productive_working_hours_minutes'] = $productive_working_hours_minutes;
                    }else{
                        
                        $dates['productive_working_hours'] = "";
                        $dates['productive_working_hours_minutes'] = "";
                    }

                    if ($attendanceDateStart == $tdt) {
                        $dates["punch_out_missing_message"] = "";
                        $dates['is_punch_out_missing'] = false;
                    }else if(($data['attendance_status'] == '1' || $data['attendance_status'] == '0') &&
                        $data['punch_out_time'] == "00:00:00"){                    
                        $dates["punch_out_missing_message"] = "Punch Out Missing";
                        $dates['is_punch_out_missing'] = true;
                        $totalPunchOUTMissing = $totalPunchOUTMissing + 1;

                        if ($shiftType == "Night") {

                            if ($data['max_attendance_punch_out_time'] != null) {
                                $datesArray = range_date($attendanceDateStart,date('Y-m-d',strtotime($data['max_attendance_punch_out_time'])));
                            }else{                                
                                $previousDateView = date('dS F Y',strtotime("+1 days",strtotime($attendanceDateStart)));
                                $previousDate = date('Y-m-d',strtotime("+1 days",strtotime($attendanceDateStart)));
                                $datesArray = array(
                                    "date_one"=>date('Y-m-d',strtotime($attendanceDateStart)),
                                    "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                    "date_two"=>$previousDate,
                                    "date_two_view"=>$previousDateView
                                );
                            }
                            
                        }else{
                            $datesArray = array(
                                "date_one_view"=>date('dS F Y',strtotime($attendanceDateStart)),
                                "date_one"=>date('Y-m-d',strtotime($attendanceDateStart))
                            );
                        }
                        
                        $dates['punch_missing_date_ary'] = $datesArray;
                    }else if($data['attendance_status'] == '1' || $data['attendance_status'] == '0'){
                        $dates["punch_out_missing_message"] = "";
                        $dates['is_punch_out_missing'] = false;
                    }
                }

                if ($punch_in_date == $attendanceDateStart) {
                    $dates['is_punch_out_missing'] = false;
                }

                $attendanceHistoryQry = $d->select("attendance_master,attendance_break_history_master,
                    attendance_type_master","attendance_master.attendance_id = attendance_break_history_master.attendance_id
                    AND attendance_master.attendance_id = '$dates[attendance_id]'
                    AND attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id 
                    AND attendance_break_history_master.break_out_time != '00:00:00' 
                    AND attendance_break_history_master.break_end_date != '0000-00-00' ");

                $dates["attendance_history"] = array();
                
                if(mysqli_num_rows($attendanceHistoryQry)>0){

                    $totalBreakTimeAtt = array();

                    while($historyData = mysqli_fetch_array($attendanceHistoryQry)){
                        $historyArray = array();

                        $breakStartDate = $historyData['break_start_date'];
                        $breakEndDate = $historyData['break_end_date'];
                        
                        $breakInTime = $historyData['break_in_time'];
                        $breakOutTime = $historyData['break_out_time'];

                        $historyArray['attendance_break_history_id'] = $historyData['attendance_break_history_id'];
                        $historyArray['attendance_type_name'] = $historyData['attendance_type_name'];
                        $historyArray['break_start_date'] = date('dS F Y', strtotime($breakStartDate));
                        $historyArray['break_end_date'] = date('dS F Y', strtotime($breakEndDate));
                        $historyArray['break_in_time'] = date('h:i A', strtotime($breakInTime));
                        $historyArray['break_out_time'] = date('h:i A', strtotime($breakOutTime));

                        $totalBreak =$historyData['total_break_time'];

                        $dates['total_spend_time_break'] = "";

                        $historyArray['total_break_hours_spend'] =timeFormat($totalBreak);

                        array_push($dates['attendance_history'],$historyArray);
                    }
                }
            }else{
                if ($isDateGone == "true") {
                    // new check

                    if (isset($holidayAry) && count($holidayAry) > 0 && isset($weekAry) && count($weekAry) > 0 && !in_array($monthDate,$holidayAry) && !in_array($monthDate,$weekAry)) {
                        $totalAbsentDays = ($totalAbsentDays + 1);
                    }else if (isset($holidayAry) && count($holidayAry) > 0 && isset($weekAry) && count($weekAry) <= 0 && !in_array($monthDate,$holidayAry)) {
                        $totalAbsentDays = ($totalAbsentDays + 1);
                    }else if (isset($holidayAry) && count($holidayAry) <= 0 && isset($weekAry) && count($weekAry) > 0 && !in_array($monthDate,$weekAry)) {
                        $totalAbsentDays = ($totalAbsentDays + 1);
                    }

                    if ($shiftType == "Night") {

                        $previousDateView = date('dS F Y',strtotime("+1 days",strtotime($monthDate)));
                        $previousDate = date('Y-m-d',strtotime("+1 days",strtotime($monthDate)));
                        $datesArray = array(
                            "date_one"=>date('Y-m-d',strtotime($monthDate)),
                            "date_one_view"=>date('dS F Y',strtotime($monthDate)),
                            "date_two"=>$previousDate,
                            "date_two_view"=>$previousDateView
                        );
                    }else{
                        $datesArray = array(
                            "date_one_view"=>date('dS F Y',strtotime($monthDate)),
                            "date_one"=>date('Y-m-d',strtotime($monthDate))
                        );
                    }

                    $dates['punch_missing_date_ary'] = $datesArray;
                }
            }

            if ($shiftType == "Night") {

                $previousDateView = date('dS F Y',strtotime("+1 days",strtotime($monthDate)));
                $previousDate = date('Y-m-d',strtotime("+1 days",strtotime($monthDate)));
                $datesArray = array(
                    "date_one"=>date('Y-m-d',strtotime($monthDate)),
                    "date_one_view"=>date('dS F Y',strtotime($monthDate)),
                    "date_two"=>$previousDate,
                    "date_two_view"=>$previousDateView
                );
            }else{
                $datesArray = array(
                    "date_one_view"=>date('dS F Y',strtotime($monthDate)),
                    "date_one"=>date('Y-m-d',strtotime($monthDate))
                );
            }

            $dates['punch_missing_date_ary'] = $datesArray;

            array_push($response['monthly_history'],$dates);
        }


        $totalMonthHours = array_sum($arrayDayWiseHours);

        if ($totalMonthHours > 0) {
            list($hours1, $wrongMinutes) = explode('.', $totalMonthHours);
            $minutes1 = ($wrongMinutes < 10 ? $wrongMinutes * 10 : $wrongMinutes) * 0.6;

            $response['total_month_hours_view'] = timeFormat($hours1.':'.$minutes1);
            $response['total_month_hours'] = $totalMonthHours."";

            $totalMonthMinutes = minutes($hours1.':'.$minutes1);
            $response['total_month_minutes'] = $totalMonthMinutes."";
        }else{
            $response['total_month_hours_view'] = "";
            $response['total_month_hours'] = "";
            $response['total_month_minutes'] = "";
        }


        // all new check

        $holidayAryJoin = array();
        $weekAryJoin = array();

        $holidayAryJoin = join("','",$holidayAry); 
        $weekAryJoin = join("','",$weekAry);

        // Total Holidays

        if (isset($holidayAry) && count($holidayAry) > 0 && isset($weekAry) && count($weekAry) > 0) {

            $totalPresentDays = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '1' AND user_id = '$user_id' AND (attendance_date_start NOT IN ('$holidayAryJoin') AND attendance_date_start NOT IN ('$weekAryJoin'))  AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_date_end != '0000-00-00' AND is_leave != '1'");

             // Total Half Day Leaves            

            $totalHalfDays = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date' AND leave_status = '1' AND leave_day_type = '1' AND leave_start_date NOT IN ('$holidayAryJoin') AND leave_start_date NOT IN ('$weekAryJoin')");

            // Total Full Day Leaves

            $totalLeaves = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '0'  AND leave_start_date NOT IN ('$holidayAryJoin') AND leave_start_date NOT IN ('$weekAryJoin') AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

            // Total Working Hours

            $qryTWH = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id'  AND attendance_date_start NOT IN ('$holidayAryJoin') AND attendance_date_start NOT IN ('$weekAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_status = '1'");
            
            $qryTWHData = mysqli_fetch_array($qryTWH);

            $tTime = $qryTWHData['SUM(total_working_minutes)'];

        }else if (isset($holidayAry) && count($holidayAry) > 0 && isset($weekAry) && count($weekAry) <= 0) {
            // Total Present Days

            $totalPresentDays = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '1' AND user_id = '$user_id' AND attendance_date_start NOT IN ('$holidayAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_status = '1'  AND is_leave != '1'");
            

            // Total Half Day Leaves            

            $totalHalfDays = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '1' AND leave_start_date NOT IN ('$holidayAryJoin') AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

            // Total Full Day Leaves

            $totalLeaves = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '0'  AND leave_start_date NOT IN ('$holidayAryJoin') AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

            // Total Working Hours

            $qryTWH = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_status = '1'");

            $qryTWHData = mysqli_fetch_array($qryTWH);

            $tTime = $qryTWHData['SUM(total_working_minutes)'];

        }else if (isset($holidayAry) && count($holidayAry) <= 0 && isset($weekAry) && count($weekAry) > 0) {
            // Total Present Days
            
            $totalPresentDays = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '1' AND user_id = '$user_id' AND attendance_date_start NOT IN ('$weekAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_date_end != '0000-00-00'  AND attendance_status = '1'  AND is_leave != '1'");

            
             // Total Half Day Leaves            

            $totalHalfDays = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date' AND leave_status = '1' AND leave_day_type = '1' AND leave_start_date NOT IN ('$weekAryJoin')");

            // Total Full Day Leaves

            $totalLeaves = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '0' AND leave_start_date NOT IN ('$weekAryJoin') AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

            // Total Working Hours

            $qryTWH = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_status = '1'");

            $qryTWHData = mysqli_fetch_array($qryTWH);

            $tTime = $qryTWHData['SUM(total_working_minutes)'];

        }else{
            // Total Present Days
            
            $totalPresentDays = $d->count_data_direct("attendance_id","attendance_master","attendance_status = '1' AND user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_date_end != '0000-00-00'  AND attendance_status = '1' AND is_leave != '1'");

            // Total Half Day Leaves            

            $totalHalfDays = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date' AND leave_status = '1' AND leave_day_type = '1'");

            // Total Full Day Leaves

            $totalLeaves = $d->count_data_direct("leave_id","leave_master","user_id = '$user_id' AND leave_status = '1' AND leave_day_type = '0' AND leave_start_date BETWEEN '$month_start_date' AND '$month_end_date'");

            // Total Working Hours

            $qryTWH = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_status = '1'");

            $qryTWHData = mysqli_fetch_array($qryTWH);

            $tTime = $qryTWHData['SUM(total_working_minutes)'];
        }


        $totalHolidays = mysqli_num_rows($holidayNameQry);

        $totalHD = 0;

        if ($totalHalfDays > 0) {
            $totalHD = 0.5 * $totalHalfDays;
        }
       
        $response['late_punch_in'] = $totalLatePunchIN."";
        $response['early_punch_out'] = $totalEarlyPunchOUT."";

        if ($totalExtraDays > 0 && $totalPresentDays > 0) {
            $finalPresentDays =  ($totalPresentDays - $totalHD)."+$totalExtraDays";
            $response['total_present'] = $finalPresentDays; 
        }else{
            if ($totalPresentDays > 0) {
                $response['total_present'] = $finalPresentDays = $totalPresentDays - $totalHD.""; 
            }else{
                $response['total_present'] = $finalPresentDays = $totalPresentDays."";                 
            }
        }

        
        $response['total_month_minutes'] = $tTime.'';

        $time = str_replace(":",".",hoursandmins($tTime));

        if ($time=="") {
            $response['total_month_hour_spent'] = '0';
        } else {
            $response['total_month_hour_spent'] = $time.'';
        }
        
        $response['total_month_hour_spent_view'] = timeFormat(hoursandmins($tTime)).'';

        // Extra Working Hour

        if (empty($holidayAryJoin)) {
            $holidayAryJoin = "0000-00-00";
        }

        if (empty($weekAryJoin)) {
            $weekAryJoin = "0000-00-00";
        }

        $e11 = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id'  AND attendance_date_start IN ('$holidayAryJoin') AND attendance_date_start NOT IN ('$weekAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_status = 1");

            
        $e22 = $d->sum_data("total_working_minutes","attendance_master","user_id = '$user_id'  AND attendance_date_start NOT IN ('$holidayAryJoin') AND attendance_date_start IN ('$weekAryJoin') AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_status = 1");


        $eData11 = mysqli_fetch_array($e11);
        $eData22 = mysqli_fetch_array($e22);

        $extraDayMin = $eData11['SUM(total_working_minutes)'] + $eData22['SUM(total_working_minutes)'];

        $qryTEWH = $d->sum_data("extra_working_hours_minutes","attendance_master","user_id = '$user_id' AND attendance_date_start BETWEEN '$month_start_date' AND '$month_end_date' AND attendance_status = 1 AND attendance_date_start NOT IN ('$holidayAryJoin') AND attendance_date_start NOT IN ('$weekAryJoin')");

        $qryTEWHData = mysqli_fetch_array($qryTEWH);
        $extraMin = $qryTEWHData['SUM(extra_working_hours_minutes)'];

        $finalExtraMin = $extraMin;
        $finalExtraMin = $extraDayMin + $extraMin;

        $response['total_extra_minutes'] = $finalExtraMin.'';            
        $response['total_extra_hours_view'] = timeFormat(hoursandmins($finalExtraMin)).'';            

        if ($finalExtraMin > 0) {
            $response['has_extra_hours'] = true;
        }else{
            $response['has_extra_hours'] = false;
        }

        // Remaining Working Hours

        if ($totalMonthMinutes > $qryTWHData['SUM(total_working_minutes)']) {
            $totalRemainingMin = $totalMonthMinutes - $qryTWHData['SUM(total_working_minutes)'].'';
            $response['total_remaining_minutes'] = $totalRemainingMin;
            $response['total_remaining_hours_view'] = timeFormat(hoursandmins($totalRemainingMin));
        }else{
            $totalRemainingMin = '0';
            $response['total_remaining_minutes'] = $totalRemainingMin;
            $response['total_remaining_hours_view'] = "";
        }


        // Other Counts

        $response['total_leave'] = $totalLeaves."";
        $response['total_half_day'] = $totalHalfDays."";
        $response['total_punch_out_missing'] = $totalPunchOUTMissing."";
        $response['total_week_off'] = $totalWeekOff."";
        $response['total_holidays'] = $totalHolidays."";
        $response['total_pending_attendance'] = $totalPendingAttendance."";
        $response['total_rejected_attendance'] = $totalRejectedAttendance."";
        $response['total_incomplete_days'] = $totalIncompleteDays."";

        $totalCData = ($totalWeekOff + $totalHolidays);

        $response['total_working_days'] = $totalWorkingDays."";

        $totalAvgPecentage =round((($finalPresentDays+count($paidLeaveArray)+(count($paidHalfHeaveArray)/2)) * 100)/$totalWorkingDays);

        if ($totalAvgPecentage > 100) {
            $totalAvgPecentage = 100;
        }

        $response['total_avg_percentage'] = $totalAvgPecentage.'';

        $tempExtraDays = 0;

        if ($totalAbsentDays <= 0) {
            $totalAbsentDays = 0;   
        }

        $response['total_extra_days'] = $totalExtraDays."";
        $response['total_extra_days_view'] = $tempExtraDays."";
        $response['total_absent'] = $totalAbsentDays."";
        $response['info_message'] = "Pending attendance, rejected attendance and the attendance which has missing punch out will not be calculated in present days";
        
        echo json_encode($response);
    }else{
        $response["message"]="Wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
    
}

function getAllDaysInAMonth($year, $month, $day) {
    $dateStart = $year.'-'.$month.'-1 '.$day; 
    $dateEnd = date($year.'-'.$month.'-t'); 

    $date = strtotime($dateStart);
    $dateMax = strtotime($dateEnd);

    $daysArray = array();

    $nbr = 0;
    while ($date <= $dateMax) {
        array_push($daysArray,date('Y-m-d', $date));
        $nbr++;
        $date += 7 * 24 * 3600;
    }
    return count($daysArray);
}

function getTotalWeekHours($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d.%02d', $hours, $minutes);
}

function getTotalWeekHoursName($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}

function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes > 0) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }

}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}

function weeks_in_month($month, $year){
    $dates = [];

    $week = 1;
    $date = new DateTime("$year-$month-01");
    $days = (int)$date->format('t'); // total number of days in the month

    $oneDay = new DateInterval('P1D');

    for ($day = 1; $day <= $days; $day++) {
        $dates["Week_$week"] []= $date->format('Y-m-d');

        $dayOfWeek = $date->format('l');
        if ($dayOfWeek === 'Saturday') {
            $week++;
        }

        $date->add($oneDay);
    }

    return $dates;
}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}

function timeFormat($time){
    $time = explode(':', $time);
    $hours = $time[0];
    $minutes = $time[1];
    if ($hours > 0 && $minutes > 0) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }
}
function timeFormatDot($time){
    $time = explode(':', $time);
    $hours = $time[0];
    $minutes = $time[1];
    return sprintf('%02d.%02d', $hours, $minutes);
}

function hoursandmins($time, $format = '%02d:%02d'){
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
?>