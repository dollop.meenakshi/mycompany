<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST) && !empty($_POST)){

	if ($key == $keydb && $auth_check=='true'){

		$response = array();
		extract(array_map("test_input", $_POST));

		if (isset($setPersonalDetails) && $setPersonalDetails == "setPersonalDetails" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$m->set_data('intrest_hobbies', $intrest_hobbies);
			$m->set_data('professional_skills', $professional_skills);
			$m->set_data('special_skills', $special_skills);
			$m->set_data('language_known', $language_known);

			$employment_info = array();

			$employment_info = array(
				"intrest_hobbies" => $m->get_data('intrest_hobbies'),
				"professional_skills" => $m->get_data('professional_skills'),
				"special_skills" => $m->get_data('special_skills'),
				"language_known" => $m->get_data('language_known'),
				"modify_date" => date("Y-m-d H:i:s")
			);


			$qq = $d->update("user_employment_details", $employment_info, "society_id='$society_id' AND user_id='$user_id'");

			$m->set_data('blood_group', $blood_group);
			$m->set_data('member_date_of_birth', $member_date_of_birth);
			$m->set_data('gender', $gender);
			$m->set_data('marital_status', $marital_status);
			$m->set_data('total_family_members', $total_family_members);
			$m->set_data('wedding_anniversary_date', $wedding_anniversary_date);
			$m->set_data('nationality', $nationality);
			
			$personal_info = array();
			$personal_info = array(				
				"blood_group" => $m->get_data('blood_group'),
				"member_date_of_birth" => $m->get_data('member_date_of_birth'),
				"gender" => $m->get_data('gender'),
				"marital_status" => $m->get_data('marital_status'),
				"total_family_members" => $m->get_data('total_family_members'),
				"wedding_anniversary_date" => $m->get_data('wedding_anniversary_date'),
				"nationality" => $m->get_data('nationality'),
				"modify_date" => date("Y-m-d H:i:s")
			);

			$q = $d->update("users_master", $personal_info, "society_id='$society_id' AND user_id='$user_id'");

			if($q == true){	

				if ($old_data != '' && $new_data != '') {
					
					$m->set_data('old_data', html_entity_decode($old_data));
					$m->set_data('new_data', html_entity_decode($new_data));
					$ary = array(
						"user_id"=>$user_id,
						"old_data"=>$m->get_data('old_data'),
						"new_data"=>$m->get_data('new_data'),
						"user_details_menu_type"=>"1",
						"created_date"=>date('Y-m-d H:i:s')
					);

					$d->insert("user_details_change_history_master",$ary);
				}

				$response["message"] = "$updateMsg";
				$response["status"] = "200";
				echo json_encode($response);
				exit;	
			}else{
				$response["message"] = "$somethingWrong";
				$response["status"] = "201";
				echo json_encode($response);
				exit;		
			}			
		}else if (isset($setContactDetails) && $setContactDetails == "setContactDetails" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
			
			$m->set_data('country_code_alt', $alt_mobile_country_code);
			$m->set_data('alt_mobile', $alt_mobile);
			$m->set_data('country_code_whatsapp', $whatsapp_number_country_code);
			$m->set_data('whatsapp_number', $whatsapp_number);
			$m->set_data('country_code_emergency', $emergency_number_country_code);
			$m->set_data('emergency_number', $emergency_number);
			$m->set_data('user_email', $user_email);
			$m->set_data('personal_email', $personal_email);
			$m->set_data('last_address', $last_address);
			$m->set_data('permanent_address', $permanent_address);

			$contact_info = array();
			$contact_info = array(
				"country_code_alt" => $m->get_data('country_code_alt'),
				"alt_mobile" => $m->get_data('alt_mobile'),
				"country_code_whatsapp" => $m->get_data('country_code_whatsapp'),
				"whatsapp_number" => $m->get_data('whatsapp_number'),
				"country_code_emergency" => $m->get_data('country_code_emergency'),
				"emergency_number" => $m->get_data('emergency_number'),
				"user_email" => $m->get_data('user_email'),
				"personal_email" => $m->get_data('personal_email'),
				"last_address" => $m->get_data('last_address'),
				"permanent_address" => $m->get_data('permanent_address'),
				"modify_date" => date("Y-m-d H:i:s")
			);

			$q = $d->update("users_master", $contact_info, "society_id='$society_id' AND user_id='$user_id'");

			if($q == true){
				if ($old_data != '' && $new_data != '') {
					
					$m->set_data('old_data', html_entity_decode($old_data));
					$m->set_data('new_data', html_entity_decode($new_data));
					$ary = array(
						"user_id"=>$user_id,
						"old_data"=>$m->get_data('old_data'),
						"new_data"=>$m->get_data('new_data'),
						"user_details_menu_type"=>"0",
						"created_date"=>date('Y-m-d H:i:s')
					);

					$d->insert("user_details_change_history_master",$ary);
				}

				$response["message"] = "$updateMsg";
				$response["status"] = "200";
				echo json_encode($response);
				exit;	
			}else{
				$response["message"] = "$somethingWrong";
				$response["status"] = "201";
				echo json_encode($response);
				exit;		
			}			
		}else if (isset($setAccessibilityData) && $setAccessibilityData == "setAccessibilityData" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			if ($isDenied == "0") {
				$m->set_data('user_id', $user_id);
				$m->set_data('accessibility_permision_denied_date', date('Y-m-d H:i:s'));

				$a1 = array();
				$a1 = array(
					"user_id" => $m->get_data('user_id'),
					"accessibility_permision_denied_date" => $m->get_data('accessibility_permision_denied_date'),
				);

				$q = $d->insert("accessibility_permission_denied_master",$a1);
			}
			
			$m->set_data('accessibility_permision_on', $accessibility_permision_on);

			$a1 = array();
			$a1 = array(
				"accessibility_permision_on" => $m->get_data('accessibility_permision_on'),
			);

			$q = $d->update("users_master",$a1,"user_id = '$user_id'");
			
			

			if($q == true){
				$response["message"] = "Success";
				$response["status"] = "200";
				echo json_encode($response);
				exit;	
			}else{
				$response["message"] = "$somethingWrong";
				$response["status"] = "201";
				echo json_encode($response);
				exit;		
			}			
		}else if (isset($setEmployeementDetails) && $setEmployeementDetails == "setEmployeementDetails" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
			
			$m->set_data('intrest_hobbies', $intrest_hobbies);
			$m->set_data('professional_skills', $professional_skills);
			$m->set_data('special_skills', $special_skills);
			$m->set_data('language_known', $language_known);

			$employment_info = array(
				"intrest_hobbies" => $m->get_data('intrest_hobbies'),
				"professional_skills" => $m->get_data('professional_skills'),
				"special_skills" => $m->get_data('special_skills'),
				"language_known" => $m->get_data('language_known'),
				"modify_date" => date("Y-m-d H:i:s")
			);

			
			$q = $d->update("user_employment_details", $employment_info, "society_id='$society_id' AND user_id='$user_id'");

			if($q == true){
				$response["message"] = "$updateMsg";
				$response["status"] = "200";
				echo json_encode($response);
				exit;	
			}else{
				$response["message"] = "$somethingWrong";
				$response["status"] = "201";
				echo json_encode($response);
				exit;		
			}			
		}else if (isset($setSocialDetails) && $setSocialDetails == "setSocialDetails" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$m->set_data('facebook', $facebook);
			$m->set_data('instagram', $instagram);
			$m->set_data('linkedin', $linkedin);
			$m->set_data('twitter', $twitter);
			$m->set_data('whatsapp_number', $whatsapp_number);
			$m->set_data('country_code_whatsapp', $country_code_whatsapp);

			$social_info = array();
			$social_info = array(
				"facebook" => $m->get_data('facebook'),
				"instagram" => $m->get_data('instagram'),
				"linkedin" => $m->get_data('linkedin'),
				"twitter" => $m->get_data('twitter'),
				"whatsapp_number" => $m->get_data('whatsapp_number'),
				"country_code_whatsapp" => $m->get_data('country_code_whatsapp'),
				"modify_date" => date("Y-m-d H:i:s")
			);

			$q = $d->update("users_master", $social_info, "society_id='$society_id' AND user_id='$user_id'");

			if($q == true){
				if ($old_data != '' && $new_data != '') {
					
					$m->set_data('old_data', html_entity_decode($old_data));
					$m->set_data('new_data', html_entity_decode($new_data));
					$ary = array(
						"user_id"=>$user_id,
						"old_data"=>$m->get_data('old_data'),
						"new_data"=>$m->get_data('new_data'),
						"user_details_menu_type"=>"3",
						"created_date"=>date('Y-m-d H:i:s')
					);

					$d->insert("user_details_change_history_master",$ary);
				}

				$response["message"] = "$updateMsg";
				$response["status"] = "200";
				echo json_encode($response);
				exit;	
			}else{
				$response["message"] = "$somethingWrong";
				$response["status"] = "201";
				echo json_encode($response);
				exit;		
			}			
		}else if (isset($setAchievementDetails) && $setAchievementDetails == "setAchievementDetails" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			if($employee_achievement_id == 0 || $employee_achievement_id == "0"){
				$m->set_data('user_id', $user_id);
				$m->set_data('society_id', $society_id);
				$m->set_data('achievement_name', $achievement_name);
				$m->set_data('achievement_date', $achievement_date);
				$m->set_data('university_board_name', $university_board_name);
				$m->set_data('archi_type', $archi_type);

				$achievement_info = array();
				$achievement_info = array(
					"user_id" => $m->get_data('user_id'),
					"society_id" => $m->get_data('society_id'),
					"achievement_name" => $m->get_data('achievement_name'),
					"achievement_date" => $m->get_data('achievement_date'),
					"university_board_name" => $m->get_data('university_board_name'),
					"archi_type" => $m->get_data('archi_type'),
					"add_date" => date("Y-m-d H:i:s")
				);

				$q = $d->insert("employee_achievements_education", $achievement_info);

				if($q == true){
					$response["message"] = "$addMsg";
					$response["status"] = "200";
					echo json_encode($response);
					exit;	
				}else{
					$response["message"] = "$somethingWrong";
					$response["status"] = "201";
					echo json_encode($response);
					exit;		
				}
			}else{
				$m->set_data('achievement_name', $achievement_name);
				$m->set_data('achievement_date', $achievement_date);
				$m->set_data('university_board_name', $university_board_name);
				$m->set_data('archi_type', $archi_type);

				$achievement_info = array();
				$achievement_info = array(
					"achievement_name" => $m->get_data('achievement_name'),
					"achievement_date" => $m->get_data('achievement_date'),
					"university_board_name" => $m->get_data('university_board_name'),
					"archi_type" => $m->get_data('archi_type'),
					"modify_date" => date("Y-m-d H:i:s")
				);

				$q = $d->update("employee_achievements_education", $achievement_info, "society_id='$society_id' AND user_id='$user_id' AND employee_achievement_id='$employee_achievement_id'");

				if($q == true){
					$response["message"] = "$updateMsg";
					$response["status"] = "200";
					echo json_encode($response);
					exit;	
				}else{
					$response["message"] = "$somethingWrong";
					$response["status"] = "201";
					echo json_encode($response);
					exit;		
				}
			}		
		}else if (isset($deleteAchievement) && $deleteAchievement == "deleteAchievement" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$ary = array(
				'delete_status'=>'1'
			);

			$q = $d->update("employee_achievements_education",$ary, "employee_achievement_id='$employee_achievement_id' AND user_id = '$user_id' AND society_id = '$society_id'");

			if($q > 0){
				$response["message"] = "$deleteMsg";
				$response["status"] = "200";
				echo json_encode($response);
				exit;
			} else {
				$response["message"] = "$somethingWrong";
				$response["status"] = "201";
				echo json_encode($response);
				exit;
			}
		}else if (isset($setExperienceDetails) && $setExperienceDetails == "setExperienceDetails" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			if($employee_experience_id == 0 || $employee_experience_id == "0"){

				$m->set_data('user_id', $user_id);
				$m->set_data('society_id', $society_id);
				$m->set_data('exp_company_name', $exp_company_name);
				$m->set_data('designation', $designation);
				$m->set_data('work_from', $work_from);
				$m->set_data('work_to', $work_to);
				$m->set_data('company_location', $company_location);

				$employee_experience_info = array();
				$employee_experience_info = array(
					"user_id" => $m->get_data('user_id'),
					"society_id" => $m->get_data('society_id'),
					"exp_company_name" => $m->get_data('exp_company_name'),
					"designation" => $m->get_data('designation'),
					"work_from" => $m->get_data('work_from'),
					"work_to" => $m->get_data('work_to'),
					"company_location" => $m->get_data('company_location'),
					"add_date" => date("Y-m-d H:i:s")
				);

				$q = $d->insert("employee_experience", $employee_experience_info);

				if($q == true){
					$response["message"] = "$addMsg";
					$response["status"] = "200";
					echo json_encode($response);
					exit;	
				}else{
					$response["message"] = "$somethingWrong";
					$response["status"] = "201";
					echo json_encode($response);
					exit;		
				}
			}else{
				$m->set_data('exp_company_name', $exp_company_name);
				$m->set_data('designation', $designation);
				$m->set_data('work_from', $work_from);
				$m->set_data('work_to', $work_to);
				$m->set_data('company_location', $company_location);

				$employee_experience_info = array();
				$employee_experience_info = array(
					"exp_company_name" => $m->get_data('exp_company_name'),
					"designation" => $m->get_data('designation'),
					"work_from" => $m->get_data('work_from'),
					"work_to" => $m->get_data('work_to'),
					"company_location" => $m->get_data('company_location'),
					"modify_date" => date("Y-m-d H:i:s")
				);

				$q = $d->update("employee_experience", $employee_experience_info, "society_id='$society_id' AND user_id='$user_id' AND employee_experience_id='$employee_experience_id'");

				if($q == true){
					$response["message"] = "$updateMsg";
					$response["status"] = "200";
					echo json_encode($response);
					exit;	
				}else{
					$response["message"] = "$somethingWrong";
					$response["status"] = "201";
					echo json_encode($response);
					exit;		
				}
			}		
		}else if (isset($deleteExperience) && $deleteExperience == "deleteExperience" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
			$ary = array(
				"delete_status"=>'1'
				);
			$q = $d->update("employee_experience",$ary, "employee_experience_id='$employee_experience_id' AND user_id = '$user_id' AND society_id = '$society_id'");
			if($q > 0){
				$response["message"] = "$deleteMsg";
				$response["status"] = "200";
				echo json_encode($response);
				exit;
			}else {
				$response["message"] = "$somethingWrong";
				$response["status"] = "201";
				echo json_encode($response);
				exit;
			}
		}
		else {
			$response["message"] = "wrong tag.";
			$response["status"] = "201";
			echo json_encode($response);
			exit;
		}
	}else {
		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
		exit;
	}
}
?>