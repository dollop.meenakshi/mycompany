<?php

$qry = $d->select("app_access_master","access_by_id = '$user_id' AND access_type = '$access_type'");

$accessResponseData = array();

$accessResponseData['isBranchWise'] = false;
$accessResponseData['isDepartmentWise'] = false;
$accessResponseData['isEmployeeWise'] = false;

$accessResponseData['branch_list'] = array();
$accessResponseData['departments'] = array();
$accessResponseData['employees'] = array();

if (mysqli_num_rows($qry) > 0) {

    $data = mysqli_fetch_array($qry);

    $access_for = $data['access_for'];
    $access_for_id = $data['access_for_id'];

    if ($data['access_mode'] == 0) {
        $modification_access = true;
    }else{
        $modification_access = false;
    }

    $accessResponseData['modification_access'] = $modification_access;

    if ($access_for == 0) {

        $accessResponseData['isBranchWise'] = true;        

        $qry = $d->select("block_master","society_id='$society_id'");

        if (mysqli_num_rows($qry) > 0) {

            $branchAry['block_id_array'] = array();

            while ($bdata = mysqli_fetch_array($qry)) {
                $branch_list = array();
                $branch_list["block_id"]=$bdata['block_id'];
                $branch_list["block_name"]=$bdata['block_name'];

                array_push($accessResponseData["branch_list"], $branch_list);

                $idsArray = $bdata['block_id'];

                array_push($branchAry['block_id_array'],$idsArray);
            }
        }

        $accessBranchIds = join("','",$branchAry['block_id_array']);

        $floor_data =  $d->select("floors_master,block_master","floors_master.society_id='$society_id' AND floors_master.block_id = block_master.block_id AND floors_master.floor_status = '0' AND floors_master.block_id IN ('$accessBranchIds')");

        if (mysqli_num_rows($floor_data) > 0) {

            while($data_floor_list=mysqli_fetch_array($floor_data)) {

                $data_floor_list = array_map("html_entity_decode", $data_floor_list);

                $departments = array(); 

                $floor_id=$data_floor_list['floor_id'];

                $totalEmployeeCount = $d->count_data_direct("user_id","users_master","delete_status = 0 AND society_id='$society_id' AND user_status = 1 AND floor_id = '$floor_id'");

                $departments["floor_id"]=$data_floor_list['floor_id'];
                $departments["block_id"]=$data_floor_list['block_id'];
                $departments["department_name"]=$data_floor_list['floor_name'];

                if ($totalEmployeeCount > 0) {
                    array_push($accessResponseData["departments"], $departments);
                }

            }
        }
    }else if ($access_for == 1) {

        $accessResponseData['isBranchWise'] = true;

        $qry = $d->select("block_master","society_id='$society_id' AND block_id IN ($access_for_id)");

        if (mysqli_num_rows($qry) > 0) {

            $branchAry['block_id_array'] = array();

            while ($bdata = mysqli_fetch_array($qry)) {
                $branch_list = array();
                $branch_list["block_id"]=$bdata['block_id'];
                $branch_list["block_name"]=$bdata['block_name'];
                array_push($accessResponseData["branch_list"], $branch_list);

                $idsArray = $bdata['block_id'];

                array_push($branchAry['block_id_array'],$idsArray);
            }
        }

        $accessBranchIds = join("','",$branchAry['block_id_array']);

        $floor_data = $d->select("floors_master,block_master","floors_master.society_id='$society_id' AND floors_master.block_id = block_master.block_id AND floors_master.floor_status = '0' AND floors_master.block_id IN ('$accessBranchIds')");

        if (mysqli_num_rows($floor_data) > 0) {

            while($data_floor_list=mysqli_fetch_array($floor_data)) {

                $data_floor_list = array_map("html_entity_decode", $data_floor_list);

                $departments = array(); 

                $floor_id=$data_floor_list['floor_id'];

                $totalEmployeeCount = $d->count_data_direct("user_id","users_master","delete_status = 0 AND society_id='$society_id' AND user_status = 1 AND floor_id = '$floor_id'");

                $departments["floor_id"]=$data_floor_list['floor_id'];
                $departments["block_id"]=$data_floor_list['block_id'];
                $departments["department_name"]=$data_floor_list['floor_name'];

                if ($totalEmployeeCount > 0) {
                    array_push($accessResponseData["departments"], $departments);
                }
            }
        }

    }else if ($access_for == 2) {

        $accessResponseData['isDepartmentWise'] = true;  

        $qry = $d->select("floors_master,block_master","block_master.society_id='$society_id' AND block_master.block_id = floors_master.block_id AND floors_master.floor_id IN ($access_for_id)","GROUP BY block_master.block_id"); 

        if (mysqli_num_rows($qry) > 0) {
            $accessResponseData['branch_list'] = array();

            while ($bdata = mysqli_fetch_array($qry)) {
                $branch_list = array();
                $branch_list["block_id"]=$bdata['block_id'];
                $branch_list["block_name"]=$bdata['block_name'];
                array_push($accessResponseData["branch_list"], $branch_list);
            }
        }

        $floor_data = $d->select("floors_master,block_master "," floors_master.society_id='$society_id' AND floors_master.block_id = block_master.block_id AND floors_master.floor_status = '0' AND floors_master.floor_id IN ($access_for_id) ORDER BY floors_master.floor_sort ASC"); 

        if (mysqli_num_rows($floor_data) > 0) {

            $depIdArry['dep_ids_ary'] = array();

            while($data_floor_list=mysqli_fetch_array($floor_data)) {

                $data_floor_list = array_map("html_entity_decode", $data_floor_list);

                $departments = array(); 

                $floor_id=$data_floor_list['floor_id'];
                $departments["floor_id"]=$data_floor_list['floor_id'];
                $departments["block_id"]=$data_floor_list['block_id'];
                $departments["department_name"]=$data_floor_list['floor_name'];
                
                $idsArray = $data_floor_list['floor_id'];

                array_push($depIdArry['dep_ids_ary'],$idsArray);

                $totalEmployeeCount = $d->count_data_direct("user_id","users_master","delete_status = 0 AND society_id='$society_id' AND user_status = 1 AND floor_id = '$floor_id'");

                if ($totalEmployeeCount > 0) {
                    array_push($accessResponseData["departments"], $departments);
                }
            }

            $accessDepIds = join("','",$depIdArry['dep_ids_ary']);

        }
    }else if ($access_for == 3) {

        $accessResponseData['isEmployeeWise'] = true;

        if ($task_added_by != '') {
            $userQry = $d->select("users_master,floors_master,block_master "," users_master.user_id IN ($access_for_id) 
                AND users_master.society_id = '$society_id' 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id 
                AND users_master.active_status = '0' 
                AND users_master.delete_status = '0'
                AND users_master.user_id != '$task_added_by'"); 
        }else{

            $userQry = $d->select("users_master,floors_master,block_master "," users_master.user_id IN ($access_for_id) 
                AND users_master.society_id = '$society_id' 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id 
                AND users_master.active_status = '0' 
                AND users_master.delete_status = '0'"); 
        }

        if (mysqli_num_rows($userQry) > 0) {

            $empIdsAry['emp_ids_ary'] = array();

            while($data_units_list=mysqli_fetch_array($userQry)) {

                $employees = array(); 

                $employees["user_id"]=$data_units_list['user_id'].'';
                $employees["block_id"]=$data_units_list['block_id'].'';
                $employees["floor_id"]=$data_units_list['floor_id'].'';
                $employees["unit_id"]=$data_units_list['unit_id'].'';
                $employees["user_full_name"]=$data_units_list['user_full_name'].'';
                $employees["branch_name"]=$data_units_list['block_name'].'';
                $employees["department_name"]=$data_units_list['floor_name'].'';
                $employees["designation"]=$data_units_list['user_designation'].'';

                if ($data_units_list['user_profile_pic'] != '') {
                    $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                } else {
                    $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }


                $idsArray = $data_units_list['user_id'];

                array_push($empIdsAry['emp_ids_ary'],$idsArray);

                array_push($accessResponseData["employees"], $employees);
            }

            $accessUserIds = join("','",$empIdsAry['emp_ids_ary']);
        }
    }
}else{
    if ($access_type == $assign_task_access) {

        $accessResponseData['isEmployeeWise'] = true;

        $userQry = $d->select("users_master,floors_master,block_master ","users_master.society_id = '$society_id' 
            AND users_master.block_id = '$block_id' 
            AND users_master.floor_id = '$floor_id' 
            AND users_master.floor_id = floors_master.floor_id 
            AND users_master.block_id = block_master.block_id 
            AND users_master.active_status = '0' 
            AND users_master.delete_status = '0'"); 

        if (mysqli_num_rows($userQry) > 0) {

            while($data_units_list=mysqli_fetch_array($userQry)) {

                $employees = array(); 

                $employees["user_id"]=$data_units_list['user_id'].'';
                $employees["block_id"]=$data_units_list['block_id'].'';
                $employees["floor_id"]=$data_units_list['floor_id'].'';
                $employees["unit_id"]=$data_units_list['unit_id'].'';
                $employees["user_full_name"]=$data_units_list['user_full_name'].'';
                $employees["branch_name"]=$data_units_list['block_name'].'';
                $employees["department_name"]=$data_units_list['floor_name'].'';
                $employees["designation"]=$data_units_list['user_designation'].'';

                if ($data_units_list['user_profile_pic'] != '') {
                    $employees["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data_units_list['user_profile_pic'];
                } else {
                    $employees["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }


                array_push($accessResponseData["employees"], $employees);
            }

            $accessUserIds = join("','",$empIdsAry['emp_ids_ary']);
        }
    }
}
