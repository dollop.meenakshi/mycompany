<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        if($_POST['getAssets']=="getAssets" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            if ($old_items == "0") {
                $qry = " AND assets_detail_inventory_master.end_date = '0000-00-00'";
            }else{
                $qry = " AND assets_detail_inventory_master.end_date != '0000-00-00'";
            }          

            $assetsQry = $d->select("assets_detail_inventory_master,assets_item_detail_master,assets_category_master","
                assets_item_detail_master.assets_id = assets_detail_inventory_master.assets_id AND 
                assets_category_master.assets_category_id = assets_detail_inventory_master.assets_category_id AND assets_detail_inventory_master.user_id = '$user_id' AND assets_detail_inventory_master.society_id = '$society_id' $qry");


            if(mysqli_num_rows($assetsQry)>0){
                
                $response["assets"] = array();

                while($data=mysqli_fetch_array($assetsQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $assets = array();

                    $assets['inventory_id'] = $data['inventory_id'];
                    $assets['assets_id'] = $data['assets_id'];
                    $assets['assets_category'] = $data['assets_category'];
                    $assets['assets_brand_name'] = $data['assets_brand_name'];
                    $assets['assets_name'] = $data['assets_name'];
                    $assets['sr_no'] = $data['sr_no'];
                    
                    if ($data['item_purchase_date'] == "0000-00-00") {
                        $assets['item_purchase_date'] = "";
                    }else{
                        $assets['item_purchase_date'] = date('d-m-Y',strtotime($data['item_purchase_date']));
                    }

                    $assets['item_price'] = $data['item_price'];
                    $assets['assets_file'] = $base_url."img/society/".$data['assets_file'];

                    if ($data['start_date'] == "0000-00-00") {
                        $assets['handover_date'] = "";
                    }else{
                        $assets['handover_date'] = date('d-m-Y',strtotime($data['start_date']));
                    }

                    if ($data['end_date'] == "0000-00-00") {
                        $assets['takeover_date'] = "";
                    }else{
                        $assets['takeover_date'] = date('d-m-Y',strtotime($data['end_date']));
                    }
                    $assets['iteam_created_date'] = date('d-m-Y h:i A',strtotime($data['iteam_created_date']));

                    array_push($response["assets"], $assets);
                }
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Failed";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getAssetDetails']=="getAssetDetails" && $assets_id!='' && $assets_id!='0' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            $assetsQry = $d->selectRow("
                users_master.user_full_name,
                users_master.user_designation,
                users_master.user_profile_pic,
                block_master.block_name,
                floors_master.floor_name,
                assets_detail_inventory_master.*,
                assets_item_detail_master.*,
                assets_category_master.*
                ","
                assets_detail_inventory_master,
                assets_item_detail_master,
                assets_category_master,
                users_master,
                block_master,
                floors_master
                ","
                assets_item_detail_master.assets_id = assets_detail_inventory_master.assets_id 
                AND assets_category_master.assets_category_id = assets_detail_inventory_master.assets_category_id 
                AND assets_detail_inventory_master.user_id = users_master.user_id
                AND users_master.block_id = block_master.block_id 
                AND floors_master.floor_id = users_master.floor_id
                AND assets_item_detail_master.assets_id = '$assets_id'
                AND assets_detail_inventory_master.society_id = '$society_id'");


            if(mysqli_num_rows($assetsQry)>0){
                
                $data=mysqli_fetch_array($assetsQry);

                $data = array_map("html_entity_decode", $data);

                $response['inventory_id'] = $data['inventory_id'];
                $response['assets_id'] = $data['assets_id'];
                $response['assets_category'] = $data['assets_category'];
                $response['assets_brand_name'] = $data['assets_brand_name'];
                $response['assets_name'] = $data['assets_name'];
                $response['user_full_name'] = $data['user_full_name'];
                $response['block_name'] = $data['block_name'];
                $response['floor_name'] = $data['floor_name'];
                $response['user_designation'] = $data['user_designation'];
                $response['sr_no'] = $data['sr_no'];

                if ($data['user_profile_pic'] != '') {
                    $response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                } else {
                    $response["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }
                
                if ($data['item_purchase_date'] == "0000-00-00") {
                    $response['item_purchase_date'] = "";
                }else{
                    $response['item_purchase_date'] = date('d-m-Y',strtotime($data['item_purchase_date']));
                }

                $response['item_price'] = $data['item_price'];
                $response['assets_file'] = $base_url."img/society/".$data['assets_file'];

                if ($data['start_date'] == "0000-00-00") {
                    $response['handover_date'] = "";
                }else{
                    $response['handover_date'] = date('d-m-Y',strtotime($data['start_date']));
                }

                if ($data['end_date'] == "0000-00-00") {
                    $response['takeover_date'] = "";
                }else{
                    $response['takeover_date'] = date('d-m-Y',strtotime($data['end_date']));
                }
                $response['iteam_created_date'] = date('d-m-Y h:i A',strtotime($data['iteam_created_date']));

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Failed";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['wrongAssetAssigned']=="wrongAssetAssigned" && $user_id !=0 && $user_id!='' &&  filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 



            $m->set_data('user_id',$user_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('inventory_id',$inventory_id);
            $m->set_data('created_date',date('Y-m-d H:i:s'));

            $a1= array(
                'user_id'=> $m->get_data('user_id'),
                'society_id'=> $m->get_data('society_id'),
                'inventory_id'=> $m->get_data('inventory_id'),
                'created_date'=> $m->get_data('created_date'),
            );

           
            $q=$d->insert("assets_wrong_assigned",$a1);
                

            if ($q == true) {
               
                $response["message"] = "Request submitted";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Faild to Add";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['raisedAssetIssue']=="raisedAssetIssue" && $user_id !=0 && $user_id!='' &&  filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 

            $asset_file = "";

            if ($_FILES["asset_file"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["asset_file"]["tmp_name"];
                $extId = pathinfo($_FILES['asset_file']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","jpeg","PNG","JPG","JPEG");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["asset_file"]["name"]);
                    $asset_file = "IMG_ISSUE".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["asset_file"]["tmp_name"], "../img/assets_issue/" . $asset_file);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & JPEG files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }


            $m->set_data('user_id',$user_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('inventory_id',$inventory_id);
            $m->set_data('asset_issue',$asset_issue);
            $m->set_data('asset_file',$asset_file);
            $m->set_data('issue_created_date',date('Y-m-d H:i:s'));

            $a1= array(
                'user_id'=> $m->get_data('user_id'),
                'society_id'=> $m->get_data('society_id'),
                'inventory_id'=> $m->get_data('inventory_id'),
                'asset_issue'=> $m->get_data('asset_issue'),
                'asset_file'=> $m->get_data('asset_file'),
                'issue_created_date'=> $m->get_data('issue_created_date'),
            );

           
            $q=$d->insert("assets_item_raised_issue",$a1);
                

            if ($q == true) {
               
                $response["message"] = "Issue raised";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Something went wrong!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

?>