<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/


if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        if($_POST['getAllDepartmentsAbsentPresent']=="getAllDepartmentsAbsentPresent" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            $employeeAccess = $d->select("app_access_master","access_by_id = '$user_id' AND access_type = '12' AND society_id = '$society_id' AND user_status = '1'");

            if (mysqli_num_rows($employeeAccess) > 0) {

                $response["departments"] = array();

                while($dataAccess = mysqli_fetch_array($employeeAccess)){

                    $floor_id = $dataAccess['floor_id'];
                    $block_id = $dataAccess['block_id'];

                    $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.floor_id = '$floor_id' AND floors_master.block_id = '$block_id'");

                    if(mysqli_num_rows($qd)>0){
                        
                        while($data=mysqli_fetch_array($qd)) {

                            $data = array_map("html_entity_decode", $data);

                            $departments = array();

                            $departments['block_id'] = $data['block_id'];
                            $departments['floor_id'] = $data['floor_id'];
                            $departments['department_name'] = $data['floor_name']." (".$data['block_name'].")";

                            array_push($response["departments"], $departments);
                        }
                    }
                }
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Failed";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getAllDepartmentsAccessWise']=="getAllDepartmentsAccessWise" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            $employeeAccess = $d->select("app_access_master","access_by_id = '$user_id' AND access_type = '9' AND society_id = '$society_id' AND user_status = '1'","GROUP BY floor_id");

            $floorIds = array();

            if (mysqli_num_rows($employeeAccess) > 0) {
                while($dataAccess = mysqli_fetch_array($employeeAccess)){
                    array_push($floorIds,$dataAccess['floor_id']);
                }
            }

            $floorIds = implode(",", $floorIds);

            $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.floor_id IN ($floorIds)");

            if(mysqli_num_rows($qd)>0){

                $response["departments"] = array();
                
                while($data=mysqli_fetch_array($qd)) {

                    $data = array_map("html_entity_decode", $data);

                    $departments = array();

                    $departments['block_id'] = $data['block_id'];
                    $departments['floor_id'] = $data['floor_id'];
                    $departments['department_name'] = $data['floor_name']." (".$data['block_name'].")";

                    array_push($response["departments"], $departments);
                }
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Failed";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getDepartmentsPendingAttendance']=="getDepartmentsPendingAttendance" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            $employeeAccess = $d->select("app_access_master","access_by_id = '$user_id' AND access_type = '9' AND society_id = '$society_id' AND user_status = '1'","GROUP BY floor_id");

            $floorIds = array();

            if (mysqli_num_rows($employeeAccess) > 0) {
                while($dataAccess = mysqli_fetch_array($employeeAccess)){
                    array_push($floorIds,$dataAccess['floor_id']);
                }
            }

            $floorIds = implode(",", $floorIds);

            $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.floor_id IN ($floorIds)");

            if(mysqli_num_rows($qd)>0){

                $response["departments"] = array();
                
                while($data=mysqli_fetch_array($qd)) {

                    $data = array_map("html_entity_decode", $data);

                    $departments = array();

                    $departments['block_id'] = $data['block_id'];
                    $departments['floor_id'] = $data['floor_id'];
                    $departments['department_name'] = $data['floor_name']." (".$data['block_name'].")";

                    array_push($response["departments"], $departments);
                }
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Failed";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getAllDepartments']=="getAllDepartments" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            $qd = $d->selectRow("block_master.*,floors_master.*,(SELECT COUNT(*) FROM users_master WHERE users_master.block_id = block_master.block_id AND user_status!=0 AND delete_status=0) AS total_employees","block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id","ORDER BY block_master.block_name ASC,floors_master.floor_name ASC");

            if(mysqli_num_rows($qd)>0){

                $response["departments"] = array();
                
                while($data=mysqli_fetch_array($qd)) {

                    $data = array_map("html_entity_decode", $data);

                    $departments = array();

                    $departments['block_id'] = $data['block_id'];
                    $departments['floor_id'] = $data['floor_id'];
                    $departments['department_name'] = $data['floor_name']." (".$data['block_name'].")";
                    if ($data['total_employees']>0) {
                        // code...
                        array_push($response["departments"], $departments);
                    }
                }
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Failed";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getDepatmentViewAttendance']=="getDepatmentViewAttendance" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){

            $employeeAccess = $d->select("app_access_master","access_by_id = '$user_id' AND access_type = '14' AND society_id = '$society_id' AND user_status = '1'","GROUP BY floor_id");

            $floorIds = array();

            if (mysqli_num_rows($employeeAccess) > 0) {
                while($dataAccess = mysqli_fetch_array($employeeAccess)){
                    array_push($floorIds,$dataAccess['floor_id']);
                }
            }

            $floorIds = implode(",", $floorIds);

            $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.floor_id IN ($floorIds)");

            if(mysqli_num_rows($qd)>0){

                $response["departments"] = array();
                
                while($data=mysqli_fetch_array($qd)) {

                    $data = array_map("html_entity_decode", $data);

                    $departments = array();

                    $departments['block_id'] = $data['block_id'];
                    $departments['floor_id'] = $data['floor_id'];
                    $departments['department_name'] = $data['floor_name']." (".$data['block_name'].")";

                    array_push($response["departments"], $departments);
                }
                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Failed";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

?>