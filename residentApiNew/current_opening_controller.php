<?php

include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {

        $response = array();
        extract(array_map("test_input" , $_POST));
        
        if($_POST['getCurrentOpening']=="getCurrentOpening" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $response["current_opening"] = array();

            $openingQry = $d->select("company_current_opening_master","society_id='$society_id' AND company_current_opening_status = '0'");
            
            if (mysqli_num_rows($openingQry) > 0) {
                
                while ($openingData = mysqli_fetch_array($openingQry)) {

                    $opening = array();

                    $opening["company_current_opening_id"] = $openingData['company_current_opening_id'];
                    $opening["company_current_opening_title"] = $openingData['company_current_opening_title'];
                    $opening["company_current_opening_position"] = $openingData['company_current_opening_position'];
                    $opening["company_current_opening_timing"] = $openingData['company_current_opening_timing'];
                    $opening["company_current_opening_address"] = $openingData['company_current_opening_address'];
                    $opening["company_current_opening_schedule"] = $openingData['company_current_opening_schedule'];
                    $opening["company_current_opening_description"] = $openingData['company_current_opening_description'];
                    $opening["company_current_opening_latitude"] = $openingData['company_current_opening_latitude'];
                    $opening["company_current_opening_longitude"] = $openingData['company_current_opening_longitude'];
                    $opening["company_current_opening_pay_scale"] = $openingData['company_current_opening_pay_scale'];
                    $opening["company_current_opening_salary_period"] = $openingData['company_current_opening_salary_period'];
                    $opening["company_current_opening_experience"] = $openingData['company_current_opening_experience'];
                    $opening["company_current_opening_status"] = $openingData['company_current_opening_status'];

                    array_push($response["current_opening"], $opening);
                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="No data";
                $response["status"]="201";
                echo json_encode($response);
            }

        }else if($_POST['applyOpening']=="applyOpening" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $company_current_opening_resume = "";

            if ($_FILES["company_current_opening_resume"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["company_current_opening_resume"]["tmp_name"];
                $extId = pathinfo($_FILES['company_current_opening_resume']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","PDF","png","PNG","jpg","JPG","JPEG","jpeg");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["company_current_opening_resume"]["name"]);
                    $company_current_opening_resume = "resume_".round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["company_current_opening_resume"]["tmp_name"], "../img/opening_resume/" . $company_current_opening_resume);
                } else {
                
                    $response["message"] = "Invalid Document. Only PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $m->set_data('society_id',$society_id);
            $m->set_data('company_current_opening_id',$company_current_opening_id);
            $m->set_data('company_current_opening_name',$company_current_opening_name);
            $m->set_data('company_current_opening_dob',$company_current_opening_dob);
            $m->set_data('company_current_opening_gender',$company_current_opening_gender);
            $m->set_data('company_current_opening_state',$company_current_opening_state);
            $m->set_data('company_current_opening_city',$company_current_opening_city);
            $m->set_data('company_current_opening_contact_no ',$company_current_opening_contact_no );
            $m->set_data('company_current_opening_email_id',$company_current_opening_email_id);
            $m->set_data('company_current_opening_education_info',$company_current_opening_education_info);
            $m->set_data('company_current_opening_experience',$company_current_opening_experience);
            $m->set_data('company_current_opening_resume',$company_current_opening_resume);

            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'company_current_opening_id'=>$m->get_data('company_current_opening_id'),
                'company_current_opening_name' =>$m->get_data('company_current_opening_name'),
                'company_current_opening_dob'=>$m->get_data('company_current_opening_dob'),
                'company_current_opening_gender'=>$m->get_data('company_current_opening_gender'),
                'company_current_opening_state'=>$m->get_data('company_current_opening_state'),
                'company_current_opening_city'=>$m->get_data('company_current_opening_city'),
                'company_current_opening_contact_no '=>$m->get_data('company_current_opening_contact_no '),
                'company_current_opening_email_id'=>$m->get_data('company_current_opening_email_id'),
                'company_current_opening_education_info'=>$m->get_data('company_current_opening_education_info'),
                'company_current_opening_experience'=>$m->get_data('company_current_opening_experience'),
                'company_current_opening_resume'=>$m->get_data('company_current_opening_resume'),
            );

            $applyOpeningQry = $d->insert("company_current_opening_apply_master",$a);

            if ($applyOpeningQry == TRUE) {
                $response["message"] = "Thank you for applying";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try agian!";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['getMyReferList']=="getMyReferList" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $referQry = $d->select("company_opening_refer_master,company_current_opening_master","company_opening_refer_master.society_id='$society_id' AND company_opening_refer_master.company_opening_refer_by = '$user_id' AND company_current_opening_master.company_current_opening_id = company_opening_refer_master.company_current_opening_id","ORDER BY company_opening_refer_master.company_opening_refer_created_date DESC");
            
            if (mysqli_num_rows($referQry) > 0) {

                $response["my_refer_list"] = array();                
                
                while ($referData = mysqli_fetch_array($referQry)) {

                    $refer = array();

                    $refer["company_opening_refer_id"] = $referData['company_opening_refer_id'];
                    $refer["company_current_opening_id"] = $referData['company_current_opening_id'];
                    $refer["company_opening_refer_for"] = $referData['company_current_opening_title'];
                    $refer["company_opening_refer_name"] = $referData['company_opening_refer_name'];
                    $refer["company_opening_refer_gender"] = $referData['company_opening_refer_gender'];
                    $refer["company_opening_refer_country_code"] = $referData['company_opening_refer_country_code'];
                    $refer["company_opening_refer_contact_no"] = $referData['company_opening_refer_contact_no'];
                    $refer["company_opening_refer_email_id"] = $referData['company_opening_refer_email_id'];
                    $refer["company_opening_refer_experience"] = $referData['company_opening_refer_experience'];

                    if ($referData['company_opening_refer_resume'] != '') {                    
                        $refer["company_opening_refer_resume"] = $base_url.'img/bring_your_buddy_resume/'.$referData['company_opening_refer_resume'];
                    }else{                        
                        $refer["company_opening_refer_resume"] = "";
                    }

                    $refer["company_opening_refer_created_date"] = date('h:i A, dS M Y',strtotime($referData['company_opening_refer_created_date']));
                    $refer["company_opening_refer_status"] = $referData['company_opening_refer_status'];
                    $refer["company_opening_refer_admin_note"] = $referData['company_opening_refer_admin_note'];

                    if ($referData['company_opening_refer_status'] == 0) {
                        $refer["company_opening_refer_status_view"] = "Pending";
                    }else if ($referData['company_opening_refer_status'] == 1) {
                        $refer["company_opening_refer_status_view"] = "Approved";
                    }else if ($referData['company_opening_refer_status'] == 2) {
                        $refer["company_opening_refer_status_view"] = "Rejected";
                    }else if ($referData['company_opening_refer_status'] == 3) {
                        $refer["company_opening_refer_status_view"] = "Canceled";
                    }

                    array_push($response["my_refer_list"], $refer);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try agian!";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['referOpening']=="referOpening" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $qry = $d->count_data_direct("user_id","users_master","delete_status=0 AND society_id='$society_id' AND user_status=1 AND member_status = 0 AND ((user_mobile = '$company_opening_refer_contact_no' AND country_code = '$company_opening_refer_country_code') OR (user_email = '$company_opening_refer_email_id' AND user_email != ''))");


            if ($qry > 0) {
                $response["message"] = $company_opening_refer_name." is already employee of the company";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $company_opening_refer_resume = "";

            if ($_FILES["company_opening_refer_resume"]["tmp_name"] != '') {
                // code...
                $file11=$_FILES["company_opening_refer_resume"]["tmp_name"];
                $extId = pathinfo($_FILES['company_opening_refer_resume']['name'], PATHINFO_EXTENSION);
                $extAllow=array("pdf","PDF");


                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["company_opening_refer_resume"]["name"]);
                    $company_opening_refer_resume = "resume_".round(microtime(true)) . '.' . end($temp);

                    move_uploaded_file($_FILES["company_opening_refer_resume"]["tmp_name"], "../img/bring_your_buddy_resume/" . $company_opening_refer_resume);
                } else {
                
                    $response["message"] = "Invalid Document. Only PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $m->set_data('society_id',$society_id);
            $m->set_data('company_current_opening_id',$company_current_opening_id);
            $m->set_data('company_opening_refer_name',$company_opening_refer_name);
            $m->set_data('company_opening_refer_gender',$company_opening_refer_gender);
            $m->set_data('company_opening_refer_country_code',$company_opening_refer_country_code);
            $m->set_data('company_opening_refer_contact_no',$company_opening_refer_contact_no);
            $m->set_data('company_opening_refer_email_id',$company_opening_refer_email_id);
            $m->set_data('company_opening_refer_experience ',$company_opening_refer_experience );
            $m->set_data('company_opening_refer_created_date',date('Y-m-d H:i:s'));
            $m->set_data('company_opening_refer_by',$company_opening_refer_by);
            $m->set_data('company_opening_refer_resume',$company_opening_refer_resume);

            $a = array(
                'society_id'=>$m->get_data('society_id'),
                'company_current_opening_id'=>$m->get_data('company_current_opening_id'),
                'company_opening_refer_name' =>$m->get_data('company_opening_refer_name'),
                'company_opening_refer_gender'=>$m->get_data('company_opening_refer_gender'),
                'company_opening_refer_country_code'=>$m->get_data('company_opening_refer_country_code'),
                'company_opening_refer_contact_no'=>$m->get_data('company_opening_refer_contact_no'),
                'company_opening_refer_email_id'=>$m->get_data('company_opening_refer_email_id'),
                'company_opening_refer_experience '=>$m->get_data('company_opening_refer_experience '),
                'company_opening_refer_created_date'=>$m->get_data('company_opening_refer_created_date'),
                'company_opening_refer_by'=>$m->get_data('company_opening_refer_by'),
                'company_opening_refer_resume'=>$m->get_data('company_opening_refer_resume'),
            );

            $referQry = $d->insert("company_opening_refer_master",$a);

            if ($referQry == TRUE) {

                $title = "New Bring Your Buddy Refferal";
                $description = "Refer by, ".$user_name;

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("22",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("22",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"bringYourBuddies");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"bringYourBuddies");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"companyOpeningRefer",
                  'admin_click_action '=>"companyOpeningRefer",
                  'notification_logo'=>'bring_your_buddy.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Thank you for referring your buddy";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try agian!";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if($_POST['cancelRefferal']=="cancelRefferal" && filter_var($society_id, FILTER_VALIDATE_INT) == true){            

            $a = array(
                'company_opening_refer_status'=>"3",
            );

            $referQry = $d->update("company_opening_refer_master",$a,"company_opening_refer_id = '$company_opening_refer_id' AND company_opening_refer_by = '$user_id'");

            if ($referQry == TRUE) {

                $response["message"] = "Referral Canceled.";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try agian!";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>