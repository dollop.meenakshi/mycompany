<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/
 
if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        $startDate=date("Y-m-01");
        $currentDate=date("Y-m-d");
        $lastDate=date("Y-m-t");

        if($_POST['addShiftDayData']=="addShiftDayData"){


            $shiftQry = $d->select("shift_timing_master","society_id = '$society_id'");

            while($shiftData = mysqli_fetch_array($shiftQry)){

                $shift_time_id = $shiftData['shift_time_id'];
                $society_id = $shiftData['society_id'];
                $shift_start_time = $shiftData['shift_start_time'];
                $shift_end_time = $shiftData['shift_end_time'];
                $lunch_break_start_time = $shiftData['lunch_break_start_time'];
                $lunch_break_end_time = $shiftData['lunch_break_end_time'];
                $tea_break_start_time = $shiftData['tea_break_start_time'];
                $tea_break_end_time = $shiftData['tea_break_end_time'];
                $per_day_hour = $shiftData['per_day_hour'];
                $week_off_days = $shiftData['week_off_days'];
                $half_day_time_start = $shiftData['half_day_time_start'];
                $halfday_before_time = $shiftData['halfday_before_time'];
                $late_time_start = $shiftData['late_time_start'];
                $early_out_time = $shiftData['early_out_time'];
                $has_altenate_week_off = $shiftData['has_altenate_week_off'];
                $alternate_week_off = $shiftData['alternate_week_off'];
                $alternate_weekoff_days = $shiftData['alternate_weekoff_days'];
                $shift_type = $shiftData['shift_type'];
                $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
                $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
                $max_hour = $shiftData['max_hour'];
                $max_punch_out_time = $shiftData['max_punch_out_time'];
                $max_shift_hour = $shiftData['max_shift_hour'];
                $max_tea_break = $shiftData['max_tea_break'];
                $max_lunch_break = $shiftData['max_lunch_break'];
                $max_personal_break = $shiftData['max_personal_break'];

                $dayNameArray = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");

                $week_off_days = explode(",", $week_off_days);

                for ($i=0; $i <= 6; $i++) { 
                    $m->set_data('shift_time_id',$shift_time_id);
                    $m->set_data('shift_day',$i);
                    $m->set_data('shift_day_name',$dayNameArray[$i]);
                    $m->set_data('society_id',$society_id);
                    $m->set_data('shift_start_time',$shift_start_time);
                    $m->set_data('shift_end_time',$shift_end_time);
                    $m->set_data('lunch_break_start_time',$lunch_break_start_time);
                    $m->set_data('lunch_break_end_time',$lunch_break_end_time);
                    $m->set_data('tea_break_start_time',$tea_break_start_time);
                    $m->set_data('tea_break_end_time',$tea_break_end_time);
                    $m->set_data('per_day_hour',$per_day_hour);
                    $m->set_data('half_day_time_start',$half_day_time_start);
                    $m->set_data('halfday_before_time',$halfday_before_time);
                    $m->set_data('late_time_start',$late_time_start);
                    $m->set_data('early_out_time',$early_out_time);
                    $m->set_data('shift_type',$shift_type);
                    $m->set_data('maximum_halfday_hours',$maximum_halfday_hours);
                    $m->set_data('minimum_hours_for_full_day',$minimum_hours_for_full_day);
                    $m->set_data('max_hour',$max_hour);
                    $m->set_data('max_punch_out_time',$max_punch_out_time);
                    $m->set_data('max_shift_hour',$max_shift_hour);
                    $m->set_data('max_tea_break',$max_tea_break);
                    $m->set_data('max_lunch_break',$max_lunch_break);
                    $m->set_data('max_personal_break',$max_personal_break);
                    $m->set_data('created_by',$created_by);
                    $m->set_data('created_date',date('Y-m-d H:i:s'));
                    
                    if (isset($week_off_days) && count($week_off_days) > 0 && in_array($i,$week_off_days)) {
                        $m->set_data('is_week_off_day',"1");
                    }else{
                        $m->set_data('is_week_off_day',"0");                    
                    }                

                    $a = array(
                        'society_id'=> $m->get_data('society_id'),
                        'shift_time_id' => $m->get_data('shift_time_id'),
                        'shift_day' => $m->get_data('shift_day'),
                        'shift_day_name' => $m->get_data('shift_day_name'),
                        'society_id' => $m->get_data('society_id'),
                        'shift_start_time' => $m->get_data('shift_start_time'),
                        'shift_end_time' => $m->get_data('shift_end_time'),
                        'lunch_break_start_time' => $m->get_data('lunch_break_start_time'),
                        'lunch_break_end_time' => $m->get_data('lunch_break_end_time'),
                        'tea_break_start_time' => $m->get_data('tea_break_start_time'),
                        'tea_break_end_time' => $m->get_data('tea_break_end_time'),
                        'per_day_hour' => $m->get_data('per_day_hour'),
                        'half_day_time_start' => $m->get_data('half_day_time_start'),
                        'halfday_before_time' => $m->get_data('halfday_before_time'),
                        'late_time_start' => $m->get_data('late_time_start'),
                        'early_out_time' => $m->get_data('early_out_time'),
                        'shift_type' => $m->get_data('shift_type'),
                        'maximum_halfday_hours' => $m->get_data('maximum_halfday_hours'),
                        'minimum_hours_for_full_day' => $m->get_data('minimum_hours_for_full_day'),
                        'max_hour' => $m->get_data('max_hour'),
                        'max_punch_out_time' => $m->get_data('max_punch_out_time'),
                        'max_shift_hour' => $m->get_data('max_shift_hour'),
                        'max_tea_break' => $m->get_data('max_tea_break'),
                        'max_lunch_break' => $m->get_data('max_lunch_break'),
                        'max_personal_break' => $m->get_data('max_personal_break'),
                        'created_by' => $m->get_data('created_by'),
                        'created_date' =>date('Y-m-d H:i:s'),
                        'is_week_off_day' => $m->get_data('is_week_off_day'),
                    );

                    $qry = $d->insert("shift_day_master",$a);
                }
            }
        }else{
            $response["message"]="Wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="Wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function getTotalWeekHours($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d.%02d', $hours, $minutes);
}

function getTotalWeekHoursName($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}


function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes > 0) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }

}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}

function weeks_in_month($month, $year){
    $dates = [];

    $week = 1;
    $date = new DateTime("$year-$month-01");
    $days = (int)$date->format('t'); // total number of days in the month

    $oneDay = new DateInterval('P1D');

    for ($day = 1; $day <= $days; $day++) {
        $dates["Week_$week"] []= $date->format('Y-m-d');

        $dayOfWeek = $date->format('l');
        if ($dayOfWeek === 'Saturday') {
            $week++;
        }

        $date->add($oneDay);
    }

    return $dates;
}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}

function timeFormat($time){
    $time = explode(':', $time);
    $hours = $time[0];
    $minutes = $time[1];
    if ($hours > 0 && $minutes > 0) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "";
    }
}


function hoursandmins($time, $format = '%02d:%02d')
{
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
?>