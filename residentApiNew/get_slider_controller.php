<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
    if ($key==$keydb ) {
        
        $response = array();
        extract(array_map("test_input" , $_POST));

        if($_POST['getSlider']=="getSlider" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id' AND users_master.floor_id='$floor_id'","ORDER BY RAND() LIMIT 3");

             $response["member"]=array();

            while($data=mysqli_fetch_array($q3)) {

                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
       

                array_push($response["member"], $member); 
            }


            $qg = $d->select("gallery_master","society_id='$society_id' AND society_id!=0","ORDER BY current_time_desc DESC LIMIT 3");

             $response["gallery"] = array();

             while ($data_complain_list = mysqli_fetch_array($qg)) {

                    $gallery = array();

                    $gallery["gallery_id"] = $data_complain_list['gallery_id'];
                    $gallery["society_id"] = $data_complain_list['society_id'];
                    $gallery["gallery_title"] = $data_complain_list['gallery_title'];
                    $gallery["gallery_photo"] = $base_url.'img/gallery/'.$data_complain_list['gallery_photo'];
                    $gallery["event_id"] = $data_complain_list['event_id'];
                    $gallery["upload_date_time"] = date('d M, Y', strtotime($data_complain_list['current_time_desc']));

                    array_push($response["gallery"], $gallery);
                }

            $qnotification=$d->select("app_slider_master","slider_type=0","order by RAND()");

            $response["slider"] = array();

            if(mysqli_num_rows($qnotification)>0){

                while($data_notification=mysqli_fetch_array($qnotification)) {

                    // print_r($data_notification);

                    $slider = array(); 

                    $slider["app_slider_id"]=$data_notification['app_slider_id'];
                    $slider["society_id"]=$data_notification['society_id'];
                    $slider["slider_image_name"]=$base_url."img/sliders/".$data_notification['slider_image_name'];
                    $slider["slider_status"]=$data_notification['slider_status'];
                    $slider["page_url"]=$data_notification['page_url'].'';
                    $slider["page_mobile"]=$data_notification['page_mobile'].'';
                    $slider["about_offer"]=$data_notification['about_offer'].'';
                    $slider["date_view"]=$data_notification['about_offer']."";
                    $slider["youtube_url"]=($data_notification['youtube_url']!='' || $data_notification['youtube_url']!=null) ? $data_notification['youtube_url'] : "";

                    array_push($response["slider"], $slider); 
                }

                $response["message"]="Get slider success.";
                $response["status"]="200";
                echo json_encode($response);

            }
            else{

                $qdefailt=$d->select("app_slider_master","slider_type='1'","order by RAND()");

                while($data_notification=mysqli_fetch_array($qdefailt)) {

                    $slider = array(); 

                    $slider["app_slider_id"]=$data_notification['app_slider_id'];
                    $slider["society_id"]=$data_notification['society_id'];
                    $slider["slider_image_name"]=$base_url."img/sliders/".$data_notification['slider_image_name'];
                    $slider["slider_status"]=$data_notification['slider_status'];
                    $slider["page_url"]=$data_notification['page_url'].'';
                    $slider["page_mobile"]=$data_notification['page_mobile'].'';
                    $slider["date_view"]=date("d M Y");
                    array_push($response["slider"], $slider); 
                }

                $response["message"]="Get slider success.";
                $response["status"]="200";
                echo json_encode($response);
            }
        }else if($_POST['getSliderNew']=="getSliderNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.user_id!='$user_id' AND users_master.floor_id='$floor_id'","ORDER BY RAND() LIMIT 3");

             $response["member"]=array();

            while($data=mysqli_fetch_array($q3)) {

                $member = array(); 
                $member["user_id"]=$data['user_id'];
                $member["user_full_name"]=$data['user_full_name'];
                $member["user_first_name"]=$data['user_first_name'];
                $member["user_last_name"]=$data['user_last_name'];
                $member["user_profile_pic"]=$base_url."img/users/recident_profile/".$data['user_profile_pic'];
       
                array_push($response["member"], $member); 
            }


            if ($block_id == '' || $block_id == ' ') {
                $sq = $d->selectRow("block_id","users_master","user_id = '$user_id'");

                if (mysqli_num_rows($sq)>0) {
                    $sdata = mysqli_fetch_array($sq);
                    $block_id = $sdata['block_id'];
                }
            }

            $qevent = $d->select("gallery_album_master","(society_id = '$society_id' AND block_id = '0' AND floor_id = '0') OR (society_id = '$society_id' AND FIND_IN_SET('$block_id',block_id) AND FIND_IN_SET($floor_id,floor_id)) OR (society_id = '$society_id' AND FIND_IN_SET($block_id,block_id) AND floor_id = '0')","ORDER BY gallery_album_update_at DESC LIMIT 3");

            if (mysqli_num_rows($qevent) > 0) {

                $response["event_album"] = array();

                while ($data = mysqli_fetch_array($qevent)) {
                    $event = array();

                    $gallery_album_id =$data['gallery_album_id'];
                    
                    
                    $event["gallery_album_id"] = $gallery_album_id;
                    $event["album_title"] = $data['album_title'];
                    $event["event_id"] = $data['event_id'];
                    $event["event_date"] = $data['event_date'];
                    $event["block_id"] = $data['block_id'];
                    $event["floor_id"] = $data['floor_id'];
                    $event["upload_date"] = date("d M Y", strtotime($data['gallery_album_update_at']));
                    $event["image_one"] = "";

                    $qryImg = $d->select("gallery_master","society_id='$society_id' AND society_id!=0  AND gallery_album_id = '$gallery_album_id'","ORDER BY RAND() LIMIT 1");

                    if (mysqli_num_rows($qryImg) > 0) {

                        while ($dataImg = mysqli_fetch_array($qryImg)) {
                            $event["image_one"] = $base_url.'img/gallery/'.$dataImg['gallery_photo'];
                        }
                        array_push($response["event_album"], $event);
                    }
                
                }
            }

            $qnotification=$d->select("app_slider_master","slider_type=0","order by RAND()");

            $response["slider"] = array();

            if(mysqli_num_rows($qnotification)>0){

                while($data_notification=mysqli_fetch_array($qnotification)) {

                    // print_r($data_notification);

                    $slider = array(); 

                    $slider["app_slider_id"]=$data_notification['app_slider_id'];
                    $slider["society_id"]=$data_notification['society_id'];
                    $slider["slider_image_name"]=$base_url."img/sliders/".$data_notification['slider_image_name'];
                    $slider["slider_status"]=$data_notification['slider_status'];
                    $slider["page_url"]=$data_notification['page_url'].'';
                    $slider["page_mobile"]=$data_notification['page_mobile'].'';
                    $slider["about_offer"]=$data_notification['about_offer'].'';
                    $slider["date_view"]=$data_notification['about_offer']."";
                    $slider["youtube_url"]=($data_notification['youtube_url']!='' || $data_notification['youtube_url']!=null) ? $data_notification['youtube_url'] : "";

                    array_push($response["slider"], $slider); 
                }

                $response["message"]="Get slider success.";
                $response["status"]="200";
                echo json_encode($response);

            }
            else{

                $qdefailt=$d->select("app_slider_master","slider_type='1'","order by RAND()");

                while($data_notification=mysqli_fetch_array($qdefailt)) {

                    $slider = array(); 

                    $slider["app_slider_id"]=$data_notification['app_slider_id'];
                    $slider["society_id"]=$data_notification['society_id'];
                    $slider["slider_image_name"]=$base_url."img/sliders/".$data_notification['slider_image_name'];
                    $slider["slider_status"]=$data_notification['slider_status'];
                    $slider["page_url"]=$data_notification['page_url'].'';
                    $slider["page_mobile"]=$data_notification['page_mobile'].'';
                    $slider["date_view"]=date("d M Y");
                    array_push($response["slider"], $slider); 
                }

                $response["message"]="Get slider success.";
                $response["status"]="200";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{

        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}?>