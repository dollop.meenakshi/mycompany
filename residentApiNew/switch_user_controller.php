<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {


  if ($key == $keydb && $auth_check=='true') {

    $response = array();
    extract(array_map("test_input", $_POST));
    if (isset($_POST['removeTenant']) && $unit_id != '' && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
      $userIdAry = array();
       

      $count7=$d->sum_data("penalty_master.penalty_amount","penalty_master,users_master","penalty_master.user_id=users_master.user_id AND penalty_master.paid_status=0 AND penalty_master.unit_id='$unit_id' AND users_master.user_type='1'");
      $row3=mysqli_fetch_array($count7);
      $totalPenalyy=$row3['SUM(penalty_master.penalty_amount)'];


      if ($totalPenalyy>0) {
        $response["message"] = "Pay all due payment first of this user!";
        $response["status"] = "201";
        echo json_encode($response);
        exit();
      }

   

      $a22 = array(
        'unit_status' => 1
      );
      $gu = $d->select("users_master", "delete_status=0 AND unit_id='$unit_id' AND society_id='$society_id' AND user_type=1");

      while ($userData = mysqli_fetch_array($gu)) {
        $user_id = $userData['user_id'];
        $user_fcm = $userData['user_token'];

        $d->delete("user_notification","user_id='$user_id'");
        $d->delete("log_master","user_id='$user_id'");
        $d->delete("chat_master","msg_by='$user_id'");
        $d->delete("chat_master","msg_for='$user_id'");
        $d->delete("document_shared_master","user_id='$user_id'");
        $d->delete("document_master","uploaded_user_id='$user_id'");
        $d->delete("user_emergencycontact_master","user_id='$user_id'");
        $d->delete("news_feed","user_id='$user_id'");
        $d->delete("news_comments","user_id='$user_id'");
        $d->delete("news_like","user_id='$user_id'");
        $d->delete("employee_unit_master","user_id='$user_id'");
        $d->delete("chat_group_member_master","user_id='$user_id'");
        $d->delete("discussion_forum_comment","user_id='$user_id'");
        $d->delete("discussion_forum_master","user_id='$user_id'");
        $d->delete("discussion_forum_comment","user_id='$user_id'");
        $d->delete("daily_visitor_unit_master","user_id='$user_id'");

        $d->delete("employee_schedule","unit_id='$unit_id'");

        $device = $userData['device'];
        if ($device == 'android') {
          $nResident->noti("","", $society_id, $user_fcm, "logout", "Account deleted by owner", 'logout');
        } else if ($device == 'ios') {
          $nResident->noti_ios("","", $society_id, $user_fcm, "logout", "Account deleted by owner", 'logout');
        }

         $aDeactive =array(
          'user_token'=> '',
          'delete_status'=> 1,
          'deleted_date'=>date("Y-m-d")
        );

        $d->update("users_master",$aDeactive,"unit_id='$unit_id' AND society_id='$society_id' AND user_id='$user_id'");
      }

      $q11 = $d->update("unit_master", $a22, "unit_id='$unit_id'");

      if ($q11 > 0) {
        $qo = $d->select("users_master", "unit_id='$unit_id' AND society_id='$society_id' AND member_status=0 AND user_type=0");
        $owndata = mysqli_fetch_array($qo);
        $token = $owndata['token'];
        $device1 = $owndata['device'];

        if ($device1 == 'android') {
          $nResident->noti("","", $society_id, $user_fcm, "Tenant Account Deleted", "By Owner", '');
        } else if ($device == 'ios') {
          $nResident->noti_ios("","", $society_id, $user_fcm, "Tenant Account Deleted", "By Owner", '');
        }

        $d->insert_myactivity($user_id,"$society_id","0","$user_name","Tenanat account deleted","My-profilexxxhdpi.png");

        $response["message"] = "Tenant Account Deleted";
        $response["status"] = "200";
        echo json_encode($response);
      } else {
        $response["message"] = "Something Wrong";
        $response["status"] = "201";
        echo json_encode($response);
      }
    } else if (isset($_POST['addTenant']) && $unit_id != '' && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

      $user_mobile = mysqli_real_escape_string($con, $user_mobile);
      $user_email = mysqli_real_escape_string($con, $user_email);

      $qselect = $d->select("users_master", "delete_status=0 AND user_mobile='$user_mobile'  AND user_mobile != '' AND user_mobile != '0' ");
      $user_data = mysqli_fetch_array($qselect);
      if ($user_data == true) {
        $response["message"] = "Mobile Number Already Register..";
        $response["status"] = "201";
        echo json_encode($response);
        exit();
      }

      if ($user_profile_pic != "") {
        $ddd = date("ymdhi");
        $profile_name= "user_".$user_mobile.round(microtime(true)) .'.png';
        define('UPLOAD_DIR', '../img/users/recident_profile/');
        $img = $user_profile_pic;
        $img = str_replace('data: img/app/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $profile_name;
        $success = file_put_contents($file, $data);
      } else {
        $profile_name = "user_default.png";
      }

      $file11 = $_FILES["tenant_doc"]["tmp_name"];
      if (file_exists($file11)) {
        $temp = explode(".", $_FILES["tenant_doc"]["name"]);
        $tenant_doc = "Tenant_" . round(microtime(true)) . '.' . end($temp);
        move_uploaded_file($_FILES["tenant_doc"]["tmp_name"], "../img/documents/" . $tenant_doc);
      } else {
        $tenant_doc = '';
      }

      $m->set_data('society_id', $society_id);
      $m->set_data('user_profile_pic', $profile_name);
      $m->set_data('user_full_name', $user_first_name . " " . $user_last_name);
      $m->set_data('user_first_name', $user_first_name);
      $m->set_data('user_last_name', $user_last_name);
      $m->set_data('user_mobile', $user_mobile);
      $m->set_data('user_email', $user_email);
      $m->set_data('owner_name', $owner_first_name . " " . $owner_last_name);
      $m->set_data('owner_mobile', $owner_mobile);
      $m->set_data('last_address', $last_address);
      $m->set_data('unit_status', $unit_status);
      $m->set_data('block_id', $block_id);
      $m->set_data('floor_id', $floor_id);
      $m->set_data('unit_id', $unit_id);
      $m->set_data('user_status', 1);
      $m->set_data('user_type', 1);
      $m->set_data('member_status', 0);
      $m->set_data('tenant_doc', $tenant_doc);
      $m->set_data('country_code', $country_code);
      $m->set_data('tenant_agreement_start_date', date('Y-m-d',strtotime($tenant_agreement_start_date)));
      $m->set_data('tenant_agreement_end_date', date('Y-m-d',strtotime($tenant_agreement_end_date)));

      $a = array(
        'society_id' => $m->get_data('society_id'),
        'user_profile_pic' => $m->get_data('user_profile_pic'),
        'user_full_name' => $m->get_data('user_full_name'),
        'user_first_name' => $m->get_data('user_first_name'),
        'user_last_name' => $m->get_data('user_last_name'),
        'user_mobile' => $m->get_data('user_mobile'),
        'user_email' => $m->get_data('user_email'),
        'last_address' => $m->get_data('last_address'),
        'user_type' => $m->get_data('user_type'),
        'block_id' => $m->get_data('block_id'),
        'floor_id' => $m->get_data('floor_id'),
        'unit_id' => $m->get_data('unit_id'),
        'user_status' => $m->get_data('user_status'),
        'owner_name' => $m->get_data('owner_name'),
        // 'owner_email'=> $m->get_data('owner_email'),
        'owner_mobile' => $m->get_data('owner_mobile'),
        'tenant_doc' => $m->get_data('tenant_doc'),
        'tenant_agreement_start_date' => $m->get_data('tenant_agreement_start_date'),
        'tenant_agreement_end_date' => $m->get_data('tenant_agreement_end_date'),
        'country_code' => $m->get_data('country_code'),
      );

      $q = $d->insert("users_master", $a);
       $tenant_user_id = $con->insert_id;

      $a22 = array(
        'unit_status' => 3
      );
      $q11 = $d->update("unit_master", $a22, "unit_id='$unit_id'");

      if ($q > 0) {

        $smsObj->send_welcome_message($society_id,$user_mobile,$user_first_name,$society_name,$country_code);
        $d->add_sms_log($user_mobile,"User Welcome Message",$society_id,$country_code,4);
        
        $qunitName = $d->select("unit_master,block_master", "unit_master.unit_id='$unit_id' AND block_master.block_id=unit_master.block_id");
        $data_unit = mysqli_fetch_array($qunitName);

         // $block_id=$d->getBlockid($user_id);
         $fcmArray=$d->selectAdminBlockwise("7",$block_id,"android");
         $fcmArrayIos=$d->selectAdminBlockwise("7",$block_id,"ios");

        $nAdmin->noti_new($society_id,"", $fcmArray, "New Tenant Added by", $owner_first_name . " for " . $data_unit['block_name'] . "-" . $data_unit['unit_name'], "pendingUser?id=$user_id");
        $nAdmin->noti_ios_new($society_id,"", $fcmArrayIos, "New Tenant Added by", $owner_first_name . " for " . $data_unit['block_name'] . "-" . $data_unit['unit_name'], "pendingUser?id=$user_id");

        $notiAry = array(
          'society_id' => $society_id,
          'notification_tittle' => "New Tenant Added by $owner_first_name",
          'notification_description' => "for " . $data_unit['block_name'] . "-" . $data_unit['unit_name'],
          'notifiaction_date' => date('Y-m-d H:i'),
          'notification_action' => "employeeDetails?id=$tenant_user_id",
          'admin_click_action ' => "employeeDetails?id=$tenant_user_id",
          'notification_logo'=>'My-profilexxxhdpi.png',
        );

        $d->insert("admin_notification", $notiAry);

       
         $d->insert_myactivity($user_id,"$society_id","0","$user_name","Tenanat account created","My-profilexxxhdpi.png");

        $response["message"] = "Tenant Account Created";
        $response["status"] = "200";
        echo json_encode($response);
      } else {
        $response["message"] = "Something Wrong";
        $response["status"] = "201";
        echo json_encode($response);
      }
    } else if (isset($_POST['addTenantNew']) && $unit_id != '' && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

      $qSociety = $d->select("society_master","society_id ='$society_id'");
      $societyData = mysqli_fetch_array($qSociety);
      $society_name=$societyData['society_name'];
      $tenant_agreement_required=$societyData['tenant_agreement_required'];
      $tenant_pr_report_required=$societyData['tenant_pr_report_required'];


      $total = count($_FILES['tenant_doc']['tmp_name']);
      if ($tenant_agreement_required==1 &&  $total<1) {
        $response["message"] = "Please Attach Rental Agreement Document !";
        $response["status"] = "201";
        echo json_encode($response);
        exit();
      }

     $totalPvr = count($_FILES['prv_doc']['tmp_name']);
      if ($tenant_pr_report_required==1 &&  $totalPvr<1) {
        $response["message"] = "Please Attach Tenant Police Verification Docuement !";
        $response["status"] = "201";
        echo json_encode($response);
        exit();
      }


      $user_mobile = mysqli_real_escape_string($con, $user_mobile);
      $user_email = mysqli_real_escape_string($con, $user_email);

      $qselect = $d->select("users_master", "delete_status=0 AND user_mobile='$user_mobile'  AND user_mobile != '' AND user_mobile != '0' ");
      $user_data = mysqli_fetch_array($qselect);
      if ($user_data == true) {
        $response["message"] = "Mobile Number Already Register..";
        $response["status"] = "201";
        echo json_encode($response);
        exit();
      }



      if ($user_profile_pic != "") {
        $ddd = date("ymdhi");
        $profile_name= "user_".$user_mobile.round(microtime(true)) .'.png';
        define('UPLOAD_DIR', '../img/users/recident_profile/');
        $img = $user_profile_pic;
        $img = str_replace('data: img/app/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = UPLOAD_DIR . $profile_name;
        $success = file_put_contents($file, $data);
      } else {
        $profile_name = "user_default.png";
      }


     


      if ($total>0) {
        $tenant_doc = "";
        for ($i = 0; $i < $total; $i++) {
          $uploadedFile = $_FILES['tenant_doc']['name'][$i];
          if ($uploadedFile != "") {
            $temp = explode(".", $uploadedFile);
            $feed_img = "Tenant_" . round(microtime(true)) .$i.'.' . end($temp);
            move_uploaded_file($_FILES['tenant_doc']['tmp_name'][$i], "../img/documents/" . $feed_img);

            if ($tenant_doc != "") {
              $tenant_doc = $tenant_doc . "~" . $feed_img;

            } else {
              $tenant_doc = $feed_img;
            }
          }
        }
      }

      
     if ($totalPvr>0) {
      $prv_doc = "";
      for ($i = 0; $i < $totalPvr; $i++) {
        $uploadedFile = $_FILES['prv_doc']['name'][$i];
        if ($uploadedFile != "") {
          $temp = explode(".", $uploadedFile);
          $feed_img = "Tenant_Prv_" . round(microtime(true)) .$i.'.' . end($temp);
          move_uploaded_file($_FILES['prv_doc']['tmp_name'][$i], "../img/documents/" . $feed_img);

          if ($prv_doc != "") {
            $prv_doc = $prv_doc . "~" . $feed_img;

          } else {
            $prv_doc = $feed_img;
          }
        }
      }
    }

      $m->set_data('society_id', $society_id);
      $m->set_data('user_profile_pic', $profile_name);
      $m->set_data('user_full_name', $user_first_name . " " . $user_last_name);
      $m->set_data('user_first_name', $user_first_name);
      $m->set_data('user_last_name', $user_last_name);
      $m->set_data('gender', $gender);
      $m->set_data('user_mobile', $user_mobile);
      $m->set_data('user_email', $user_email);
      $m->set_data('owner_name', $owner_first_name . " " . $owner_last_name);
      $m->set_data('owner_mobile', $owner_mobile);
      $m->set_data('last_address', $last_address);
      $m->set_data('unit_status', $unit_status);
      $m->set_data('block_id', $block_id);
      $m->set_data('floor_id', $floor_id);
      $m->set_data('unit_id', $unit_id);
      $m->set_data('user_status', 1);
      $m->set_data('user_type', 1);
      $m->set_data('member_status', 0);
      $m->set_data('tenant_doc', $tenant_doc);
      $m->set_data('country_code', $country_code);
      $m->set_data('prv_doc', $prv_doc);
       if ($tenant_agreement_start_date!="") {
          $m->set_data('tenant_agreement_start_date', date('Y-m-d',strtotime($tenant_agreement_start_date)));
        } else {
          $m->set_data('tenant_agreement_start_date', "");
        }
        if ($tenant_agreement_end_date!="") {
        $m->set_data('tenant_agreement_end_date', date('Y-m-d',strtotime($tenant_agreement_end_date)));
        } else {
          $m->set_data('tenant_agreement_end_date', "");
        }

      $a = array(
        'society_id' => $m->get_data('society_id'),
        'user_profile_pic' => $m->get_data('user_profile_pic'),
        'user_full_name' => $m->get_data('user_full_name'),
        'user_first_name' => $m->get_data('user_first_name'),
        'user_last_name' => $m->get_data('user_last_name'),
        'gender' => $m->get_data('gender'),
        'user_mobile' => $m->get_data('user_mobile'),
        'user_email' => $m->get_data('user_email'),
        'last_address' => $m->get_data('last_address'),
        'user_type' => $m->get_data('user_type'),
        'block_id' => $m->get_data('block_id'),
        'floor_id' => $m->get_data('floor_id'),
        'unit_id' => $m->get_data('unit_id'),
        'user_status' => $m->get_data('user_status'),
        'owner_name' => $m->get_data('owner_name'),
        // 'owner_email'=> $m->get_data('owner_email'),
        'owner_mobile' => $m->get_data('owner_mobile'),
        'tenant_doc' => $m->get_data('tenant_doc'),
        'tenant_agreement_start_date' => $m->get_data('tenant_agreement_start_date'),
        'tenant_agreement_end_date' => $m->get_data('tenant_agreement_end_date'),
        'prv_doc' => $m->get_data('prv_doc'),
        'country_code' => $m->get_data('country_code'),
      );

  
    
      $q = $d->insert("users_master", $a);
       $tenant_user_id = $con->insert_id;
      $a22 = array(
        'unit_status' => 3
      );

      $q11 = $d->update("unit_master", $a22, "unit_id='$unit_id'");

      if ($q > 0) {

        $smsObj->send_welcome_message($society_id,$user_mobile,$user_first_name,$society_name,$country_code);
        $d->add_sms_log($user_mobile,"User Welcome Message",$society_id,$country_code,4);
        

        $qunitName = $d->select("unit_master,block_master", "unit_master.unit_id='$unit_id' AND block_master.block_id=unit_master.block_id");
        $data_unit = mysqli_fetch_array($qunitName);

        // $block_id=$d->getBlockid($user_id);
         $fcmArray=$d->selectAdminBlockwise("7",$block_id,"android");
         $fcmArrayIos=$d->selectAdminBlockwise("7",$block_id,"ios");

        $nAdmin->noti_new($society_id,"", $fcmArray, "New Tenant Added by", $owner_first_name . " for " . $data_unit['block_name'] . "-" . $data_unit['unit_name'], "employeeDetails?id=$tenant_user_id");
        $nAdmin->noti_ios_new($society_id,"", $fcmArrayIos, "New Tenant Added by", $owner_first_name . " for " . $data_unit['block_name'] . "-" . $data_unit['unit_name'], "employeeDetails?id=$tenant_user_id");

        $notiAry = array(
          'society_id' => $society_id,
          'notification_tittle' => "New Tenant Added by $owner_first_name",
          'notification_description' => "for " . $data_unit['block_name'] . "-" . $data_unit['unit_name'],
          'notifiaction_date' => date('Y-m-d H:i'),
          'notification_action' => "employeeDetails?id=$tenant_user_id",
          'admin_click_action ' => 'owners',
          'notification_logo'=>'My-profilexxxhdpi.png',
        );

        $d->insert("admin_notification", $notiAry);

      
         $d->insert_myactivity($user_id,"$society_id","0","$user_name","Tenanat account created","My-profilexxxhdpi.png");

        $response["message"] = "Tenant Account Created";
        $response["status"] = "200";
        echo json_encode($response);
      } else {
        $response["message"] = "Something Wrong";
        $response["status"] = "201";
        echo json_encode($response);
      }
    } else {
      $response["message"] = "wrong tag";
      $response["status"] = "201";
      echo json_encode($response);
    }
  } else {
    $response["message"] = "wrong api key";
    $response["status"] = "201";
    echo json_encode($response);
  }
}
