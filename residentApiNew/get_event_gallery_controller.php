<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb && $auth_check=='true'){ 

        $response = array();
        extract(array_map("test_input" , $_POST));
        
        if ($_POST['getGalleryAlbum'] == "getGalleryAlbum" && filter_var($society_id, FILTER_VALIDATE_INT) == true ) {

            if ($block_id == '' || $block_id == ' ') {
                $sq = $d->selectRow("block_id","users_master","user_id = '$user_id'");

                if (mysqli_num_rows($sq)>0) {
                    $sdata = mysqli_fetch_array($sq);
                    $block_id = $sdata['block_id'];
                }
            }

            if ($filter_year == "") {
        
                $qevent = $d->select("gallery_album_master","(society_id = '$society_id' AND block_id = '0' AND floor_id = '0') OR (society_id = '$society_id' AND FIND_IN_SET('$block_id',block_id) AND FIND_IN_SET($floor_id,floor_id)) OR (society_id = '$society_id' AND FIND_IN_SET($block_id,block_id) AND floor_id = '0')","ORDER BY gallery_album_update_at DESC");
            }else {
                $qevent = $d->select("gallery_album_master","((society_id = '$society_id' AND block_id = '0' AND floor_id = '0') OR (society_id = '$society_id' AND FIND_IN_SET('$block_id',block_id) AND FIND_IN_SET($floor_id,floor_id)) OR (society_id = '$society_id' AND FIND_IN_SET($block_id,block_id) AND floor_id = '0')) AND DATE_FORMAT(event_date,'%Y') = '$filter_year'","ORDER BY gallery_album_update_at DESC");

            }



            if (mysqli_num_rows($qevent) > 0) {

                $response["event_album"] = array();

                while ($data = mysqli_fetch_array($qevent)) {
                    $event = array();

                    $gallery_album_id =$data['gallery_album_id'];
                    
                    
                    $event["gallery_album_id"] = $gallery_album_id;
                    $event["album_title"] = $data['album_title'];
                    $event["event_id"] = $data['event_id'];
                    $event["event_date"] = $data['event_date'];
                    $event["block_id"] = $data['block_id'];
                    $event["floor_id"] = $data['floor_id'];
                    $event["upload_date"] = date("d M Y", strtotime($data['event_date']));

                    $event["images"] = array();


                    $qryImg = $d->select("gallery_master","society_id='$society_id' AND society_id!=0  AND gallery_album_id = '$gallery_album_id'","ORDER BY current_time_desc DESC LIMIT 4");

                    if (mysqli_num_rows($qryImg) > 0) {

                        while ($dataImg = mysqli_fetch_array($qryImg)) {

                            $images = array();
                            
                            $images["gallery_id"] = $dataImg['gallery_id'];
                            $images["gallery_photo"] = $base_url.'img/gallery/'.$dataImg['gallery_photo'];

                            array_push($event["images"], $images);
                        }

                        array_push($response["event_album"], $event);
                    }
                
                }
                $response["message"] = "Succes";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No album Found.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getGalleryNew'] == "getGalleryNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true ) {


            $qevent = $d->select("gallery_master","society_id='$society_id' AND society_id!=0  AND gallery_album_id = '$gallery_album_id'","ORDER BY gallery_id DESC");


            if (mysqli_num_rows($qevent) > 0) {

                $response["images"] = array();
                while ($data = mysqli_fetch_array($qevent)) {
                    
                    $images = array();
                    
                    $images["gallery_id"] = $data['gallery_id'];
                    $images["gallery_title"] = $data['gallery_title'];
                    $images["gallery_photo"] = $base_url.'img/gallery/'.$data['gallery_photo'];
                    $images["image_size"] = $data['image_size'];
                    $images["event_id"] = $data['event_id'];
                    $images["gallary_group_id"] = $data['gallary_group_id'];


                    array_push($response["images"], $images);
                }
                $response["message"] = "Get gallery success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No gallery image Found.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getGallery'] == "getGallery" && filter_var($society_id, FILTER_VALIDATE_INT) == true ) {


            $qevent = $d->select("gallery_master","society_id='$society_id' AND society_id!=0","GROUP BY gallery_title,event_id ORDER BY current_time_desc DESC");


            if (mysqli_num_rows($qevent) > 0) {

                $response["event"] = array();

                while ($data_event_list = mysqli_fetch_array($qevent)) {
                    $event = array();

                    $event_id =$data_event_list['event_id'];
                    if($event_id!=0) {
                        $eq = $d->select("gallery_master","event_id='$event_id' AND society_id = '$society_id'");
                        $ed=$d->selectRow("event_start_date","event_master","event_id='$event_id' AND society_id = '$society_id'");
                        $eventData= mysqli_fetch_array($ed);
                        $upload_date_time = $eventData['event_start_date'];
                    } else {
                        $eq = $d->select("gallery_master","gallery_title='$gallery_title' AND society_id = '$society_id'");
                        $upload_date_time = $data_event_list['current_time_desc'];
                    }
                    
                    $event["gallery_album_id"] = $data_event_list['gallery_title'];
                    $event["upload_date"] = date("d M Y", strtotime($upload_date_time));
                
                    $event["gallery"] = array();

                    while ($data_complain_list = mysqli_fetch_array($eq)) {

                        $gallery = array();

                        $gallery["gallery_id"] = $data_complain_list['gallery_id'];
                        $gallery["society_id"] = $data_complain_list['society_id'];
                        $gallery["gallery_title"] = $data_complain_list['gallery_title'];
                        $gallery["gallery_photo"] = $base_url.'img/gallery/'.$data_complain_list['gallery_photo'];
                        $gallery["event_id"] = $data_complain_list['event_id'];
                        $gallery["upload_date_time"] = $data_complain_list['upload_date_time'];

                        array_push($event["gallery"], $gallery);
                    }

                    array_push($response["event"], $event);
                }
                $response["message"] = "Get gallery success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No gallery image Found.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{

        $response["message"]="wrong api key";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>