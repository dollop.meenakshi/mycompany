<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST)){
	extract(array_map("test_input", $_POST));
	$response = array();
	$todayDate = date('Y-m-d');

	if($_POST['getRetailers']=="getRetailers" && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	

		$qry = $d->selectRow("
			route_retailer_master.*,
			route_master.*,
			area_master_new.area_name,
			retailer_master.*,
			users_master.user_full_name,
			retailer_order_master.retailer_order_id,
			retailer_order_master.retailer_visit_id,
			retailer_order_master.order_status,
			retailer_no_order_master.retailer_no_order_id
			","
			route_retailer_master,
			route_master,
			area_master_new,
			retailer_master 
			LEFT JOIN retailer_order_master ON retailer_order_master.retailer_id = retailer_master.retailer_id AND retailer_order_master.order_by_user_id = '$user_id' AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$todayDate' 
			LEFT JOIN retailer_no_order_master ON retailer_no_order_master.retailer_id = retailer_master.retailer_id AND retailer_no_order_master.visit_by_user_id = '$user_id' AND DATE_FORMAT(retailer_no_order_master.no_order_created_date,'%Y-%m-%d') = '$todayDate'
			LEFT JOIN users_master ON users_master.user_id = retailer_master.retailer_last_visit_by 

			","
			retailer_master.society_id = '$society_id' 
			AND route_retailer_master.route_id = route_master.route_id 
			AND route_retailer_master.retailer_id = retailer_master.retailer_id 
			AND route_retailer_master.route_id = '$route_id' 
			AND area_master_new.area_id = retailer_master.area_id 
			AND retailer_master.retailer_status = '1'","GROUP BY retailer_master.retailer_id");

		$response["retailers"] = array();
		$tempArray = array();

		if (mysqli_num_rows($qry) > 0) {

			while($data = mysqli_fetch_array($qry)){
				$retailers = array();

				$retailer_id = $data['retailer_id'];

				$qq = $d->count_data_direct("retailer_daily_visit_timeline_id","retailer_daily_visit_timeline_master","retailer_id = '$retailer_id' AND retailer_visit_by_id = '$user_id' AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') = '$todayDate'");

				if ($qq <= 0) {
					$m->set_data('route_id', $route_id);
					$m->set_data('retailer_id', $retailer_id);
		            $m->set_data('retailer_visit_by_id', $user_id);
		            $m->set_data('retailer_visit_created_date', date('Y-m-d H:i:s'));

		            $a1= array(
		                'route_id'=> $m->get_data('route_id'),
		                'retailer_id'=> $m->get_data('retailer_id'),
		                'retailer_visit_by_id'=> $m->get_data('retailer_visit_by_id'),
		                'retailer_visit_created_date'=> $m->get_data('retailer_visit_created_date'),
		            );
		           
		            $qqq = $d->insert("retailer_daily_visit_timeline_master",$a1);
				}
				
				$retailers['retailer_id'] = $retailer_id;
				$retailers['society_id'] = $data['society_id'];
				$retailers['retailer_name'] = $data['retailer_name'];
				$retailers['retailer_code'] = 'RT'.$data['retailer_id'];
				$retailers['retailer_contact_person'] = $data['retailer_contact_person'];
				$retailers['retailer_contact_person_number'] =$data['retailer_contact_person_country_code'].' '. $data['retailer_contact_person_number'];
				$retailers['retailer_contact_person_country_code'] = $data['retailer_contact_person_country_code'];
				$retailers['retailer_address'] = $data['retailer_address'];
				$retailers['area_name'] = $data['area_name'];
				$retailers['retailer_google_address'] = $data['retailer_google_address'];
				$retailers['retailer_latitude'] = $data['retailer_latitude'];
				$retailers['retailer_longitude'] = $data['retailer_longitude'];
				$retailers['retailer_geofence_range'] = $data['retailer_geofence_range'];
				$retailers['retailer_pincode'] = $data['retailer_pincode'];
				$retailers['retailer_photo_name'] = $data['retailer_photo'];

				if ($data['retailer_last_order_date'] != null || $data['retailer_last_order_date'] != '') {
					$retailers['retailer_last_order_date'] = date('h:i A, dS M Y',strtotime($data['retailer_last_order_date']));
				}else{
					$retailers['retailer_last_order_date'] = "";
				}

				if ($data['retailer_last_visit_date'] != null || $data['retailer_last_visit_date'] != '') {
					$retailers['retailer_last_visit_date'] = date('h:i A, dS M Y',strtotime($data['retailer_last_visit_date']));
				}else{
					$retailers['retailer_last_visit_date'] = "";
				}

				$retailers['retailer_last_order_amount'] = $data['retailer_last_order_amount'].'';
				$retailers['retailer_last_visit_by'] = $data['retailer_last_visit_by'].'';
				$retailers['retailer_last_visit_by_name'] = $data['user_full_name'].'';
				
				$retailers['retailer_created_date'] = date('dS M Y',strtotime($data['retailer_created_date']));

				if ($data['retailer_latitude'] !='' && $data['retailer_longitude'] != '') {

					$radiusInMeeter=  $d->haversineGreatCircleDistance($user_latitude,$user_longitude,$data['retailer_latitude'],$data['retailer_longitude']);

					$distanceInKM = number_format($radiusInMeeter/1000,2,'.','');

					$retailers['distance_meter'] = number_format($radiusInMeeter,2,'.','').'';
					$retailers['distance_km'] = $distanceInKM." KM";
					$retailers['distance_sort'] = $distanceInKM;
				}else{
				
					$retailers['distance_meter'] = '0';
					$retailers['distance_km'] = "0 KM";
					$retailers['distance_sort'] = 0;
				}
				$retailers['today_status'] = "0";

				$retailers['retailer_daily_visit_timeline_id'] = $data['retailer_visit_id'];

				if ($data['order_status'] != '' || $data['order_status'] != null) {
					$retailers['today_status'] = $data['order_status'];
				}else{
					if ($data['retailer_no_order_id'] != '' || $data['retailer_no_order_id'] != null) {
						$retailers['today_status'] = "2";
					}
				}

				
				array_push($tempArray,$retailers);
			}

			function sortByOrder($a, $b) {
            	return $a['distance_sort'] - $b['distance_sort'];
          	}

          	usort($tempArray, 'sortByOrder');

          	for ($i=0; $i <count($tempArray) ; $i++) { 
            	
            	$retailers = array();

				$retailers['retailer_id'] = $tempArray[$i]['retailer_id'];
				$retailers['society_id'] = $tempArray[$i]['society_id'];
				$retailers['retailer_name'] = $tempArray[$i]['retailer_name'];
				$retailers['retailer_code'] = 'RT'.$tempArray[$i]['retailer_id'];
				$retailers['retailer_contact_person'] = $tempArray[$i]['retailer_contact_person'];
				$retailers['retailer_contact_person_number'] = $tempArray[$i]['retailer_contact_person_number'];
				$retailers['retailer_address'] = $tempArray[$i]['retailer_address'];
				$retailers['area_name'] = $tempArray[$i]['area_name'];
				$retailers['retailer_google_address'] = $tempArray[$i]['retailer_google_address'];
				$retailers['retailer_latitude'] = $tempArray[$i]['retailer_latitude'];
				$retailers['retailer_longitude'] = $tempArray[$i]['retailer_longitude'];
				$retailers['retailer_geofence_range'] = $tempArray[$i]['retailer_geofence_range'];
				$retailers['retailer_pincode'] = $tempArray[$i]['retailer_pincode'];
				$retailers['retailer_last_order_date'] = $tempArray[$i]['retailer_last_order_date'].'';
				$retailers['retailer_last_visit_date'] = $tempArray[$i]['retailer_last_visit_date'].'';
				$retailers['retailer_last_order_amount'] = $tempArray[$i]['retailer_last_order_amount'].'';
				$retailers['retailer_last_visit_by'] = $tempArray[$i]['retailer_last_visit_by'].'';
				$retailers['retailer_last_visit_by_name'] = $tempArray[$i]['retailer_last_visit_by_name'].'';
				$retailers['retailer_created_date'] = $tempArray[$i]['retailer_created_date'];				
				$retailers['distance_meter'] = $tempArray[$i]['distance_meter'].'';
				$retailers['distance_km'] = $tempArray[$i]['distance_km'].'';
				$retailers['retailer_daily_visit_timeline_id'] = $tempArray[$i]['retailer_daily_visit_timeline_id'].'';
				$retailers['today_status'] = $tempArray[$i]['today_status'].'';
				
				array_push($response["retailers"],$retailers);
          	}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getRetailerVisitTimeline']=="getRetailerVisitTimeline" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){  

    	$appendQuery = "";

    	if ($retailer_id != '') {
    		$appendQuery = $appendQuery." AND retailer_daily_visit_timeline_master.retailer_id = '$retailer_id'";
	  	}  

  		if ($filter_type == 0) {

			$filterDate = date('Y-m-d');
			$appendQuery = " AND DATE_FORMAT(retailer_daily_visit_timeline_master.retailer_visit_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 1) {

	   		$filterDate = date('Y-m-d',strtotime('-1 days'));
			$appendQuery = " AND DATE_FORMAT(retailer_daily_visit_timeline_master.retailer_visit_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 2) {

	   		$year = date('Y');
	   		$week = date('W',strtotime(date('Y-m-d')));
	   		
	   		$time = strtotime("1 January $year", time());
			$day = date('w', $time);
			$time += ((7*$week)+1-$day)*24*3600;
			$startDate = date('Y-m-d', $time);
			$time += 6*24*3600;
			$endDate = date('Y-m-d', $time);

			$appendQuery = " AND DATE_FORMAT(retailer_daily_visit_timeline_master.retailer_visit_created_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";

	   	}else if ($filter_type == 3) {

	   		$currentMonth = date('Y-m');
			$appendQuery = " AND DATE_FORMAT(retailer_daily_visit_timeline_master.retailer_visit_created_date,'%Y-%m') = '$currentMonth'";
	   	}	

		$qry = $d->selectRow("
			retailer_master.*,
			retailer_order_master.retailer_order_id,
			retailer_order_master.order_status,
			retailer_no_order_master.retailer_no_order_id,
			retailer_daily_visit_timeline_master.*
			","
			retailer_master,
			retailer_daily_visit_timeline_master LEFT JOIN retailer_order_master ON retailer_order_master.retailer_visit_id = retailer_daily_visit_timeline_master.retailer_daily_visit_timeline_id LEFT JOIN retailer_no_order_master ON retailer_no_order_master.retailer_visit_id = retailer_daily_visit_timeline_master.retailer_daily_visit_timeline_id
			","
			retailer_master.retailer_id = retailer_daily_visit_timeline_master.retailer_id 
			AND retailer_daily_visit_timeline_master.retailer_visit_by_id = '$user_id' AND retailer_daily_visit_timeline_master.retailer_visit_end_datetime IS NOT NULL $appendQuery
			","
			ORDER BY retailer_daily_visit_timeline_master.retailer_visit_start_datetime DESC");

		$response["retailer_visit_timeline"] = array();

		if (mysqli_num_rows($qry) > 0) {
		
			while($data = mysqli_fetch_array($qry)){
				$timeline = array();

				$timeline['retailer_visit_id'] = $data['retailer_daily_visit_timeline_id'];
				$timeline['retailer_id'] = $data['retailer_id'];
				$timeline['society_id'] = $data['society_id'];
				$timeline['retailer_name'] = $data['retailer_name'];
				$timeline['retailer_code'] = 'RT'.$data['retailer_id'];
				$timeline['retailer_contact_person'] = $data['retailer_contact_person'];
				$timeline['retailer_contact_person_number'] = $data['retailer_contact_person_number'];
				$timeline['retailer_address'] = $data['retailer_address'];
				$timeline['retailer_latitude'] = $data['retailer_latitude'];
				$timeline['retailer_longitude'] = $data['retailer_longitude'];
				$timeline['retailer_pincode'] = $data['retailer_pincode'];

				if ($data['retailer_visit_start_datetime'] != null || $data['retailer_visit_start_datetime'] != '') {
					$timeline['retailer_visit_start_date'] = date('h:i A, dS M Y',strtotime($data['retailer_visit_start_datetime']));
				}else{
					$timeline['retailer_visit_start_date'] = "";
				}

				if ($data['retailer_visit_end_datetime'] != null || $data['retailer_visit_end_datetime'] != '') {
					$timeline['retailer_visit_end_date'] = date('h:i A, dS M Y',strtotime($data['retailer_visit_end_datetime']));
				}else{
					$timeline['retailer_visit_end_date'] = "";
				}

			  	$pTime = date('Y-m-d H:i:s',strtotime($data['retailer_visit_start_datetime']));
			    $eTime = date('Y-m-d H:i:s',strtotime($data['retailer_visit_end_datetime']));

				$date_a = new DateTime($pTime);
			    $date_b = new DateTime($eTime);

			    $interval = $date_a->diff($date_b);
			   
			    $days = $interval->format('%d')*24;
			    $hours = $interval->format('%h');
			    $hours = $hours+$days;
			    $minutes = $interval->format('%i');
			    $sec = $interval->format('%s');

			    if ($hours <= 0 && $minutes <= 0) {
			    	$timeline['total_visit_duration'] = sprintf('%02d sec', $sec);
			    }else if ($hours > 0 && $minutes > 0) {
			        $timeline['total_visit_duration'] = sprintf('%02d hr %02d min', $hours, $minutes);
			    }else if ($hours > 0 && $minutes <= 0) {
			        $timeline['total_visit_duration'] = sprintf('%02d hr', $hours);
			    }else if ($hours <= 0 && $minutes > 0) {
			        $timeline['total_visit_duration'] = sprintf('%02d min', $minutes);
			    }else{
			        $timeline['total_visit_duration'] = "00:00";
			    }

				$timeline['retailer_visit_start_latitude'] = $data['retailer_visit_start_latitude'].'';
				$timeline['retailer_visit_start_longitude'] = $data['retailer_visit_start_longitude'].'';
				$timeline['retailer_visit_start_gps_accuracy'] = $data['retailer_visit_start_gps_accuracy'].'';
				$timeline['retailer_visit_end_latitude'] = $data['retailer_visit_end_latitude'].'';
				$timeline['retailer_visit_start_latitude'] = $data['retailer_visit_start_latitude'].'';
				$timeline['retailer_visit_end_longitude'] = $data['retailer_visit_end_longitude'].'';
				$timeline['retailer_visit_end_gps_accuracy'] = $data['retailer_visit_end_gps_accuracy'].'';

				$timeline['visit_data'] = "Only Visit";

				if ($data['order_status']  == '1') {
					$timeline['visit_data'] = "Place Order";
				}else if ($data['order_status']  == '3' || $data['order_status']  == '2') {
					$timeline['visit_data'] = "Order Cancelled";
				}else{
					if ($data['retailer_no_order_id'] != '') {
						$timeline['visit_data'] = "No Order";
					}else{
						$timeline['visit_data'] = "Only Visit";
					}
				}
				

				array_push($response["retailer_visit_timeline"],$timeline);
			}

			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "No data";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['getRetailerDetails']=="getRetailerDetails" && $retailer_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){    	

		$qry = $d->selectRow("
			retailer_master.*,
			countries.country_name,
			states.state_name,
			cities.city_name,
			route_master.route_id,
			route_master.route_name,
			route_retailer_master.route_id,
			users_master.user_full_name
			","
			countries,states,cities,route_retailer_master,route_master,retailer_master LEFT JOIN users_master ON users_master.user_id = retailer_master.retailer_last_visit_by
			","
			retailer_master.society_id = '$society_id' 
			AND retailer_master.retailer_id = '$retailer_id' 
			AND route_retailer_master.retailer_id = retailer_master.retailer_id 
			AND route_retailer_master.route_id = route_master.route_id 
			AND retailer_master.country_id = countries.country_id 
			AND retailer_master.state_id = states.state_id 
			AND retailer_master.city_id = cities.city_id");		
		
		$data = mysqli_fetch_array($qry);

		$retailer_id = $data['retailer_id'];
		$response['retailer_id'] = $retailer_id;
		$response['route_id'] = $data['route_id'];
		$response['route_name'] = $data['route_name'];
		$response['society_id'] = $data['society_id'];
		$response['country_id'] = $data['country_id'];
		$response['country_name'] = $data['country_name'];
		$response['state_id'] = $data['state_id'];
		$response['state_name'] = $data['state_name'];
		$response['city_id'] = $data['city_id'];			
		$response['city_name'] = $data['city_name'];
		$response['retailer_name'] = $data['retailer_name'];
		$response['retailer_code'] = 'RT'.$retailer_id;
		$response['retailer_contact_person'] = $data['retailer_contact_person'];
		$response['retailer_contact_person_number'] = $data['retailer_contact_person_country_code'].' '. $data['retailer_contact_person_number'];
		$response['retailer_contact_person_country_code'] = $data['retailer_contact_person_country_code'];
		$response['retailer_address'] = $data['retailer_address'];
		$response['retailer_google_address'] = $data['retailer_google_address'];
		$response['retailer_latitude'] = $data['retailer_latitude'];
		$response['retailer_longitude'] = $data['retailer_longitude'];
		$response['retailer_geofence_range'] = $data['retailer_geofence_range'];
		$response['retailer_pincode'] = $data['retailer_pincode'];
		$response['retailer_gst_no'] = $data['retailer_gst_no'];
		$response['retailer_type'] = $data['retailer_type'];
		$response['retailer_credit_limit'] = $data['retailer_credit_limit'];
		$response['retailer_credit_days'] = $data['retailer_credit_days'];
		$response['retailer_remark'] = $data['retailer_remark'];
		$response['retailer_photo_name'] = $data['retailer_photo'];
		$response['retailer_last_distributor_id'] = $data['retailer_last_distributor_id'].'';
		$response['retailer_last_visit_by'] = $data['retailer_last_visit_by'].'';

		if ($data['user_full_name'] != null || $data['user_full_name'] != '') {
			$response['retailer_last_visit_by_name'] = $data['user_full_name'];
		}else{
			$response['retailer_last_visit_by_name'] = $data['user_full_name'].'';			
		}

		if ($data['retailer_photo'] != '') {
			$response['retailer_photo'] = $base_url."img/users/recident_profile/".$data['retailer_photo'];
		}else{
			$response['retailer_photo'] = "";				
		}

		if ($data['retailer_verified'] == 1) {
			$response['retailer_verified'] = true;
		}else{
			$response['retailer_verified'] = false;				
		}


		if ($data['retailer_last_order_date'] != null || $data['retailer_last_order_date'] != '') {
			$response['retailer_last_order_date'] = date('h:i A, dS F Y',strtotime($data['retailer_last_order_date']));
		}else{
			$response['retailer_last_order_date'] = "";
		}

		if ($data['retailer_last_visit_date'] != null || $data['retailer_last_visit_date'] != '') {
			$response['retailer_last_visit_date'] = date('h:i A, dS F Y',strtotime($data['retailer_last_visit_date']));
		}else{
			$response['retailer_last_visit_date'] = "";
		}

		$response['retailer_last_order_amount'] = $data['retailer_last_order_amount'];
		$response['retailer_created_date'] = date('dS F Y',strtotime($data['retailer_created_date']));				

		$qry = $d->selectRow("
			distributor_master.*
			","
			distributor_master,
			retailer_distributor_relation_master
			","
			distributor_master.society_id = '$society_id' 
			AND retailer_distributor_relation_master.retailer_id = '$retailer_id' 
			AND retailer_distributor_relation_master.distributor_id = distributor_master.distributor_id");

		$response["distributors"] = array();
		
		if (mysqli_num_rows($qry) > 0) {
		
			while($data = mysqli_fetch_array($qry)){
				$distributors = array();

				$distributors['distributor_id'] = $data['distributor_id'];
				$distributors['society_id'] = $data['society_id'];
				$distributors['distributor_area_id'] = $data['distributor_area_id'];			
				$distributors['distributor_name'] = $data['distributor_name'];
				$distributors['distributor_code'] = $data['distributor_code'];
				$distributors['distributor_contact_person'] = $data['distributor_contact_person'];
				$distributors['distributor_contact_person_number'] = $data['distributor_contact_person_number'];
				$distributors['distributor_address'] = $data['distributor_address'];
				
				array_push($response["distributors"],$distributors);
			}
		}		

		$response["message"] = "Success";
		$response["status"] = "200";
		echo json_encode($response);
		              
    }else if($_POST['addRetailer']=="addRetailer" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

    	$isRatailerExists = $d->count_data_direct("retailer_id","retailer_master","retailer_name = '$retailer_name' AND retailer_contact_person_number = '$retailer_contact_person_number' AND retailer_contact_person_country_code = '$retailer_contact_person_country_code' AND area_id = '$area_id'");

    	if ($isRatailerExists > 0) {
    		$response["message"] = "Retailer already exists";
			$response["status"] = "201";
			echo json_encode($response);
			exit();
    	}

    	$retailer_photo = "";

        if ($_FILES["retailer_photo"]["tmp_name"] != null) {
            // code...
            $file11=$_FILES["retailer_photo"]["tmp_name"];
            $extId = pathinfo($_FILES['retailer_photo']['name'], PATHINFO_EXTENSION);
            $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG");

            if(in_array($extId,$extAllow)) {
                $temp = explode(".", $_FILES["retailer_photo"]["name"]);
                $retailer_photo = "retailer_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["retailer_photo"]["tmp_name"], "../img/users/recident_profile/" . $retailer_photo);
            } else {
            
                $response["message"] = "Invalid Document. Only JPG, JPEG & PNG files are allowed.";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
        }else{
        	$retailer_photo = $retailer_photo_name;
        }

		$m->set_data('society_id', $society_id);
		$m->set_data('country_id', $country_id);
		$m->set_data('state_id', $state_id);
		$m->set_data('city_id', $city_id);
		$m->set_data('area_id', $area_id);
		$m->set_data('retailer_name', $retailer_name);
		$m->set_data('retailer_code', $retailer_code);
		$m->set_data('retailer_contact_person', $retailer_contact_person);
		$m->set_data('retailer_contact_person_number', $retailer_contact_person_number);
		$m->set_data('retailer_contact_person_country_code', $retailer_contact_person_country_code);
		$m->set_data('retailer_address', $retailer_address);
		$m->set_data('retailer_google_address', $retailer_google_address);
		$m->set_data('retailer_latitude', $retailer_latitude);
		$m->set_data('retailer_longitude', $retailer_longitude);
		$m->set_data('retailer_geofence_range', "50");
		$m->set_data('retailer_pincode', $retailer_pincode);
		$m->set_data('retailer_gst_no', $retailer_gst_no);
		$m->set_data('retailer_type', $retailer_type);
		$m->set_data('retailer_credit_limit', $retailer_credit_limit);
		$m->set_data('retailer_credit_days', $retailer_credit_days);
		$m->set_data('retailer_photo', $retailer_photo);
		$m->set_data('retailer_created_date', date('Y-m-d H:i:s'));
		$m->set_data('retailer_created_by', $user_id);

		$ary = array(
			'society_id' => $m->get_data('society_id'),
			'country_id' => $m->get_data('country_id'),
			'state_id' => $m->get_data('state_id'),
			'city_id' => $m->get_data('city_id'),
			'area_id' => $m->get_data('area_id'),
			'retailer_name' => $m->get_data('retailer_name'),
			'retailer_code' => $m->get_data('retailer_code'),
			'retailer_contact_person' => $m->get_data('retailer_contact_person'),
			'retailer_contact_person_number' => $m->get_data('retailer_contact_person_number'),
			'retailer_contact_person_country_code' => $m->get_data('retailer_contact_person_country_code'),
			'retailer_address' => $m->get_data('retailer_address'),
			'retailer_google_address' => $m->get_data('retailer_google_address'),
			'retailer_latitude' => $m->get_data('retailer_latitude'),
			'retailer_longitude' => $m->get_data('retailer_longitude'),
			'retailer_geofence_range' => $m->get_data('retailer_geofence_range'),
			'retailer_pincode' => $m->get_data('retailer_pincode'),
			'retailer_gst_no' => $m->get_data('retailer_gst_no'),
			'retailer_type' => $m->get_data('retailer_type'),
			'retailer_credit_limit' => $m->get_data('retailer_credit_limit'),
			'retailer_credit_days' => $m->get_data('retailer_credit_days'),
			'retailer_photo' => $m->get_data('retailer_photo'),
			'retailer_created_date' => $m->get_data('retailer_created_date'),
			'retailer_created_by' => $m->get_data('retailer_created_by'),
		);

		if ($retailer_id != '') {
			$qry = $d->update("retailer_master",$ary,"retailer_id = '$retailer_id'");
		}else{
			$qry = $d->insert("retailer_master",$ary);
			$retailer_id = $con->insert_id;
		}

		if ($qry == true) {

			$m->set_data('society_id', $society_id);
			$m->set_data('route_id', $route_id);
			$m->set_data('retailer_id', $retailer_id);
			$m->set_data('route_retailer_created_date', date('Y-m-d H:i:s'));
			$m->set_data('route_retailer_created_by', $user_id);

			$ary2 = array(
				'society_id' => $m->get_data('society_id'),
				'route_id' => $m->get_data('route_id'),
				'retailer_id' => $m->get_data('retailer_id'),
				'route_retailer_created_date' => $m->get_data('route_retailer_created_date'),
				'route_retailer_created_by' => $m->get_data('route_retailer_created_by'),
			);

			$qry1 = $d->insert("route_retailer_master",$ary2);

			if ($retailer_id != '') {
				$qry1 = $d->delete("retailer_distributor_relation_master","retailer_id = '$retailer_id' AND distributor_id = '$distributor_id' AND created_by = '$user_id'");
			}

			$distributors = explode(",", $distributor_ids);

			for ($i=0; $i < count($distributors); $i++) { 
				$m->set_data('society_id', $society_id);
				$m->set_data('retailer_id', $retailer_id);
				$m->set_data('distributor_id', $distributors[$i]);
				$m->set_data('created_date', date('Y-m-d H:i:s'));
				$m->set_data('created_by', $user_id);

				$ary1 = array(
					'society_id' => $m->get_data('society_id'),
					'retailer_id' => $m->get_data('retailer_id'),
					'distributor_id' => $m->get_data('distributor_id'),
					'created_date' => $m->get_data('created_date'),
					'created_by' => $m->get_data('created_by'),
				);

				$qry1 = $d->insert("retailer_distributor_relation_master",$ary1);
			}

			$response["message"] = "New retailer created";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "Something went wrong!";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['editRetailer']=="editRetailer" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

    	$retailer_photo = "";

        if ($_FILES["retailer_photo"]["tmp_name"] != null) {
            // code...
            $file11=$_FILES["retailer_photo"]["tmp_name"];
            $extId = pathinfo($_FILES['retailer_photo']['name'], PATHINFO_EXTENSION);
            $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG");

            if(in_array($extId,$extAllow)) {
                $temp = explode(".", $_FILES["retailer_photo"]["name"]);
                $retailer_photo = "retailer_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["retailer_photo"]["tmp_name"], "../img/users/recident_profile/" . $retailer_photo);
            } else {
            
                $response["message"] = "Invalid Document. Only JPG, JPEG & PNG files are allowed.";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
        }else{
        	$retailer_photo = $retailer_photo_name;
        }

		$m->set_data('society_id', $society_id);
		$m->set_data('country_id', $country_id);
		$m->set_data('state_id', $state_id);
		$m->set_data('city_id', $city_id);
		$m->set_data('area_id', $area_id);
		$m->set_data('retailer_name', $retailer_name);
		$m->set_data('retailer_code', $retailer_code);
		$m->set_data('retailer_contact_person', $retailer_contact_person);
		$m->set_data('retailer_contact_person_number', $retailer_contact_person_number);
		$m->set_data('retailer_contact_person_country_code', $retailer_contact_person_country_code);
		$m->set_data('retailer_address', $retailer_address);
		$m->set_data('retailer_google_address', $retailer_google_address);
		$m->set_data('retailer_latitude', $retailer_latitude);
		$m->set_data('retailer_longitude', $retailer_longitude);
		$m->set_data('retailer_geofence_range', $retailer_geofence_range);
		$m->set_data('retailer_pincode', $retailer_pincode);
		$m->set_data('retailer_gst_no', $retailer_gst_no);
		$m->set_data('retailer_type', $retailer_type);
		$m->set_data('retailer_credit_limit', $retailer_credit_limit);
		$m->set_data('retailer_credit_days', $retailer_credit_days);
		$m->set_data('retailer_photo', $retailer_photo);
		$m->set_data('retailer_modified_date', date('Y-m-d H:i:s'));
		$m->set_data('retailer_updated_by', $user_id);

		$ary = array(
			'society_id' => $m->get_data('society_id'),
			'country_id' => $m->get_data('country_id'),
			'state_id' => $m->get_data('state_id'),
			'city_id' => $m->get_data('city_id'),
			'area_id' => $m->get_data('area_id'),
			'retailer_name' => $m->get_data('retailer_name'),
			'retailer_code' => $m->get_data('retailer_code'),
			'retailer_contact_person' => $m->get_data('retailer_contact_person'),
			'retailer_contact_person_number' => $m->get_data('retailer_contact_person_number'),
			'retailer_contact_person_country_code' => $m->get_data('retailer_contact_person_country_code'),
			'retailer_address' => $m->get_data('retailer_address'),
			'retailer_google_address' => $m->get_data('retailer_google_address'),
			'retailer_latitude' => $m->get_data('retailer_latitude'),
			'retailer_longitude' => $m->get_data('retailer_longitude'),
			'retailer_geofence_range' => $m->get_data('retailer_geofence_range'),
			'retailer_pincode' => $m->get_data('retailer_pincode'),
			'retailer_gst_no' => $m->get_data('retailer_gst_no'),
			'retailer_type' => $m->get_data('retailer_type'),
			'retailer_credit_limit' => $m->get_data('retailer_credit_limit'),
			'retailer_credit_days' => $m->get_data('retailer_credit_days'),
			'retailer_photo' => $m->get_data('retailer_photo'),
			'retailer_modified_date' => $m->get_data('retailer_modified_date'),
			'retailer_updated_by' => $m->get_data('retailer_updated_by'),
		);

		$qry = $d->update("retailer_master",$ary,"retailer_id = '$retailer_id'");

		if ($qry == true) {

			$qry1 = $d->delete("retailer_distributor_relation_master","retailer_id = '$retailer_id'");

			$distributors = explode(",", $distributor_ids);

			for ($i=0; $i < count($distributors); $i++) { 

				$distributor_id = $distributors[$i];
				
				$m->set_data('society_id', $society_id);
				$m->set_data('retailer_id', $retailer_id);
				$m->set_data('distributor_id', $distributor_id);
				$m->set_data('created_date', date('Y-m-d H:i:s'));
				$m->set_data('created_by', $user_id);

				$ary1 = array(
					'society_id' => $m->get_data('society_id'),
					'retailer_id' => $m->get_data('retailer_id'),
					'distributor_id' => $m->get_data('distributor_id'),
					'created_date' => $m->get_data('created_date'),
					'created_by' => $m->get_data('created_by'),
				);

				$qry1 = $d->insert("retailer_distributor_relation_master",$ary1);
			}

			$response["message"] = "Retailer data updated";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "Something went wrong!";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['startRetailerVisit']=="startRetailerVisit" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

    	$todayDate = date('Y-m-d');
    	if ($retailer_visit_id == '') {
	    	$isVisitStart = $d->selectRow("retailer_visit_id","retailer_visit_timeline","retailer_visit_by_id = '$user_id' AND retailer_id = '$retailer_id' AND DATE_FORMAT(retailer_visit_start_date,'%Y-%m-%d') = '$todayDate' AND retailer_visit_end_date IS NULL");

	    	if (mysqli_num_rows($isVisitStart) > 0) {
	    		$oldVisitData = mysqli_fetch_array($isVisitStart);
	    		$retailer_visit_id = $oldVisitData['retailer_visit_id'];
	    		$response["retailer_visit_id"] = $retailer_visit_id.'';
	    		$response["message"] = "Visit already started";
				$response["status"] = "200";
				echo json_encode($response);
				exit();
	    	}
	    }

	    $startDate = date('Y-m-d H:i:s');

		$m->set_data('society_id', $society_id);
		$m->set_data('retailer_visit_by_id', $user_id);
		$m->set_data('retailer_id', $retailer_id);
		$m->set_data('retailer_visit_start_date', $startDate);
		$m->set_data('retailer_visit_start_latitude', $retailer_visit_start_latitude);
		$m->set_data('retailer_visit_start_longitude', $retailer_visit_start_longitude);
		$m->set_data('retailer_visit_start_gps_accuracy', $retailer_visit_start_gps_accuracy);

		$ary = array(
			'society_id' => $m->get_data('society_id'),
			'retailer_visit_by_id' => $m->get_data('retailer_visit_by_id'),
			'retailer_id' => $m->get_data('retailer_id'),
			'retailer_visit_start_date' => $m->get_data('retailer_visit_start_date'),
			'retailer_visit_start_latitude' => $m->get_data('retailer_visit_start_latitude'),
			'retailer_visit_start_longitude' => $m->get_data('retailer_visit_start_longitude'),
			'retailer_visit_start_gps_accuracy' => $m->get_data('retailer_visit_start_gps_accuracy'),
		);

		if ($retailer_visit_id != '') {

			$oldQ = $d->selectRow("retailer_visit_start_date","retailer_visit_timeline","retailer_visit_by_id = '$user_id' AND retailer_visit_id = '$retailer_visit_id' ");
			$getData= mysqli_fetch_array($oldQ);
			$visit_start_date_time = $getData['retailer_visit_start_date'];

			$endDate = date('Y-m-d H:i:s');
			
			
			
			$timeFirst  = strtotime($visit_start_date_time);
			$timeSecond = strtotime($endDate);
			$differenceInSeconds = $timeSecond - $timeFirst;
          
			$m->set_data('retailer_visit_end_date', $endDate);
			$m->set_data('retailer_visit_end_latitude', $retailer_visit_end_latitude);
			$m->set_data('retailer_visit_end_longitude', $retailer_visit_end_longitude);
			$m->set_data('retailer_visit_end_gps_accuracy', $retailer_visit_end_gps_accuracy);
			$m->set_data('retailer_visit_duration', $differenceInSeconds);

			$ary1 = array(
				'retailer_visit_end_date' => $m->get_data('retailer_visit_end_date'),
				'retailer_visit_end_latitude' => $m->get_data('retailer_visit_end_latitude'),
				'retailer_visit_end_longitude' => $m->get_data('retailer_visit_end_longitude'),
				'retailer_visit_end_gps_accuracy' => $m->get_data('retailer_visit_end_gps_accuracy'),
				'retailer_visit_duration' => $m->get_data('retailer_visit_duration'),
			);

			$qry = $d->update("retailer_visit_timeline",$ary1,"retailer_visit_id = '$retailer_visit_id' AND retailer_visit_by_id = '$user_id' AND retailer_id = '$retailer_id'");

			$ary2 = array(
				'retailer_last_visit_date' => date('Y-m-d H:i:s'),
				'retailer_last_visit_by' => $user_id,
			);

			$qq1 = $d->update("retailer_master",$ary2,"retailer_id = '$retailer_id'");

			$response["message"] = "Retailer visit ended";

		}else{
			$qry = $d->insert("retailer_visit_timeline",$ary);
			$retailer_visit_id = $con->insert_id;

			$response["message"] = "Retailer visit started";

		}


		if ($qry == true) {

			$response["retailer_visit_id"] = $retailer_visit_id.'';
			$response["visit_start_date_time"] = $startDate.'';
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "Something went wrong!";
			$response["status"] = "201";
			echo json_encode($response);
		}                 
    }else if($_POST['startRetailerVisitNew']=="startRetailerVisitNew" && $user_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

    	$todayDate = date('Y-m-d');
    	$isVisitStart = $d->selectRow("retailer_daily_visit_timeline_id,retailer_visit_by_id","retailer_daily_visit_timeline_master","retailer_visit_by_id = '$user_id' AND retailer_id = '$retailer_id' AND route_id = '$route_id' AND retailer_daily_visit_timeline_id = '$retailer_daily_visit_timeline_id' AND retailer_visit_start_datetime IS NOT NULL AND retailer_visit_end_datetime IS NULL");

    	if (mysqli_num_rows($isVisitStart) > 0) {
    		$oldVisitData = mysqli_fetch_array($isVisitStart);
    		$retailer_visit_id = $oldVisitData['retailer_visit_by_id'];
    		$retailer_daily_visit_timeline_id = $oldVisitData['retailer_daily_visit_timeline_id'];
    		$response["retailer_visit_id"] = $retailer_daily_visit_timeline_id.'';
    		$response["message"] = "Visit already started";
			$response["status"] = "200";
			echo json_encode($response);
			exit();
    	}

	    if ($is_end == '0') {

		    $startDate = date('Y-m-d H:i:s');
		    $sDate = date('Y-m-d');

		    if ($out_of_range_reason != '') {
		    	$out_of_range = 1;
		    }else{		    	
		    	$out_of_range = 0;
		    }

			$m->set_data('retailer_visit_start_datetime', $startDate);
			$m->set_data('retailer_visit_start_latitude', $retailer_visit_start_latitude);
			$m->set_data('retailer_visit_start_longitude', $retailer_visit_start_longitude);
			$m->set_data('retailer_visit_start_gps_accuracy', $retailer_visit_start_gps_accuracy);
			$m->set_data('out_of_range', $out_of_range);
			$m->set_data('out_of_range_reason', $out_of_range_reason);

			$ary = array(
				'retailer_visit_start_datetime' => $m->get_data('retailer_visit_start_datetime'),
				'retailer_visit_start_latitude' => $m->get_data('retailer_visit_start_latitude'),
				'retailer_visit_start_longitude' => $m->get_data('retailer_visit_start_longitude'),
				'retailer_visit_start_gps_accuracy' => $m->get_data('retailer_visit_start_gps_accuracy'),
				'out_of_range' => $m->get_data('out_of_range'),
				'out_of_range_reason' => $m->get_data('out_of_range_reason'),
			);
			$response["message"] = "Retailer visit started";

			$qry0 = $d->count_data_direct("retailer_id","retailer_master","retailer_id = '$retailer_id' AND retailer_latitude = '' AND retailer_longitude = ''");

			if ($qry0 > 0) {
				
				$ary2 = array(
					'retailer_last_visit_date' => date('Y-m-d H:i:s'),
					'retailer_last_visit_by' => $user_id,
					'retailer_latitude' => $retailer_visit_start_latitude,
					'retailer_longitude' => $retailer_visit_start_longitude,
				);
				$qq1 = $d->update("retailer_master",$ary2,"retailer_id = '$retailer_id'");
			}

			$qry123 = $d->selectRow("retailer_daily_visit_timeline_id","retailer_daily_visit_timeline_master","retailer_id = '$retailer_id' AND retailer_visit_by_id = '$user_id' AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') = '$sDate'");

			$dataQry123 = mysqli_fetch_array($qry123);
			$retailer_daily_visit_timeline_id = $dataQry123['retailer_daily_visit_timeline_id'];
	    }


		$qqq1 = $d->update("retailer_daily_visit_timeline_master",$ary,"retailer_id = '$retailer_id' AND retailer_visit_by_id = '$user_id' AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') = '$sDate'");

		if ($is_end == '1') {

			$oldQ = $d->selectRow("retailer_visit_start_datetime","retailer_daily_visit_timeline_master","retailer_visit_by_id = '$user_id' AND retailer_daily_visit_timeline_id = '$retailer_daily_visit_timeline_id' ");
			$getData= mysqli_fetch_array($oldQ);
			$sDTime = $getData['retailer_visit_start_datetime'];

			$endDate = date('Y-m-d H:i:s');
			
			$timeFirst  = strtotime($sDTime);
			$timeSecond = strtotime($endDate);
			$differenceInSeconds = $timeSecond - $timeFirst;

			$pTime = date('Y-m-d h:i A',strtotime($sDTime));
		    $eTime = date('Y-m-d h:i A',strtotime($endDate));

		    $date_a = new DateTime($pTime);
		    $date_b = new DateTime($eTime);

		    $interval = $date_a->diff($date_b);
		   
		    $days = $interval->format('%d')*24;
		    $hours = $interval->format('%h');
		    $hours = $hours+$days;
		    $minutes = $interval->format('%i');
		    $sec = $interval->format('%s');

		    $duration =  sprintf('%02d:%02d:%02d', $hours, $minutes,$sec);

		    $time = explode(':', $duration.":00");
            $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);
          
			$m->set_data('retailer_visit_end_datetime', $endDate);
			$m->set_data('retailer_visit_end_latitude', $retailer_visit_end_latitude);
			$m->set_data('retailer_visit_end_longitude', $retailer_visit_end_longitude);
			$m->set_data('retailer_visit_end_gps_accuracy', $retailer_visit_end_gps_accuracy);
			$m->set_data('retailer_visit_duration', $differenceInSeconds);
			$m->set_data('retailer_visit_duration_minutes', $total_minutes);

			$ary1 = array(
				'retailer_visit_end_datetime' => $m->get_data('retailer_visit_end_datetime'),
				'retailer_visit_end_latitude' => $m->get_data('retailer_visit_end_latitude'),
				'retailer_visit_end_longitude' => $m->get_data('retailer_visit_end_longitude'),
				'retailer_visit_end_gps_accuracy' => $m->get_data('retailer_visit_end_gps_accuracy'),
				'retailer_visit_duration' => $m->get_data('retailer_visit_duration'),
				'retailer_visit_duration_minutes' => $m->get_data('retailer_visit_duration_minutes'),
			);

			$qqq1 = $d->update("retailer_daily_visit_timeline_master",$ary1,"retailer_daily_visit_timeline_id = '$retailer_daily_visit_timeline_id'");

			
			$ary2 = array(
				'retailer_last_visit_date' => date('Y-m-d H:i:s'),
				'retailer_last_visit_by' => $user_id,
			);
			
			$qq1 = $d->update("retailer_master",$ary2,"retailer_id = '$retailer_id'");

			$response["message"] = "Retailer visit ended";

		}


		$response["retailer_visit_id"] = $retailer_daily_visit_timeline_id.'';
		$response["visit_start_date_time"] = $startDate.'';
		$response["status"] = "200";
		echo json_encode($response);
    } else{
		$response["success"] = 201;
		$response["message"] = "Invalid Tag !";
		echo json_encode($response);
	}

} else {
	$response["success"] = 201;
	$response["message"] = "Invalid API Key !";
	echo json_encode($response);
}

?>