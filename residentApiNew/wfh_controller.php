<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));

        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");
        $currentDate = date("Y-m-d");

        if($_POST['checkAccessWFH']=="checkAccessWFH" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $wfh_req_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['getMyWFH']=="getMyWFH" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $wfhQry = $d->select("wfh_master","society_id = '$society_id' 
                AND user_id = '$user_id'","ORDER BY wfh_start_date DESC");
                   
            if(mysqli_num_rows($wfhQry)>0){
                
                $response["wfh_list"] = array();

                while($data=mysqli_fetch_array($wfhQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $wfh = array(); 

                    $wfh["wfh_id"] = $data['wfh_id'];
                    $wfh["month"] = date('m', strtotime($data['wfh_start_date']));
                    $wfh["year"] = date('Y', strtotime($data['wfh_start_date']));
                    $wfh["wfh_start_date"] = date('Y-m-d', strtotime($data['wfh_start_date']));
                    $wfh["wfh_start_date_view"] = date('D, dS F Y', strtotime($data['wfh_start_date']));
                    $wfh["wfh_end_date"] = date('Y-m-d', strtotime($data['wfh_end_date']));
                    $wfh["wfh_end_date_view"] = date('D, dS F Y', strtotime($data['wfh_end_date']));
                    $wfh["wfh_reason"] = $data['wfh_reason'];
                    $wfh["wfh_latitude"] = $data['wfh_latitude'];
                    $wfh["wfh_longitude"] = $data['wfh_longitude'];
                    $wfh["wfh_address"] = $data['wfh_address'];
                    $wfh["wfh_attendance_range"] = $data['wfh_attendance_range'];
                    $wfh["wfh_attachment_name"] = $data['wfh_attachment'];

                    if ($data['wfh_day_type'] == "0") {
                        $wfh["wfh_day_type"] = "Full Day";
                        $wfh["wfh_first_second_half"] = "";
                    }else{
                        $wfh["wfh_day_type"] = "Half Day";

                        if ($data['wfh_first_second_half'] == "1") {
                            $wfh["wfh_first_second_half"] = "First Half";
                        }else{
                            $wfh["wfh_first_second_half"] = "Second Half";
                        }
                    }

                    

                    if ($data['wfh_attachment'] == "") {
                        $wfh["wfh_attachment"] = "";
                    }else{
                        $wfh["wfh_attachment"] = $base_url."img/wfh_attachment/" .$data['wfh_attachment'];
                    }

                    $wfh["wfh_status"] = $data['wfh_status'];
                    
                    if ($data['wfh_status'] == 0) {
                        $wfh["wfh_status_view"] = "Pending";
                    }else if ($data['wfh_status'] == 1) {
                        $wfh["wfh_status_view"] = "Approved";
                    }else if ($data['wfh_status'] == 2) {
                        $wfh["wfh_status_view"] = "Rejected";
                    }else if ($data['wfh_status'] == 3) {
                        $wfh["wfh_status_view"] = "Auto Rejected";
                    }
                    
                    $wfh["wfh_declined_reason"] = $data['wfh_declined_reason'];

                    $wfh['approved_by_name'] = "";

                    if ($data["wfh_status"] == 1) {
                        if ($data['wfh_status_changed_type'] == 0) {
                            $approvedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[wfh_status_changed_by]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['user_full_name']!=null) {
                                $wfh['approved_by_name'] = "By, ".$dataApproveName['user_full_name']."";
                            }else{
                                $wfh['approved_by_name'] = "";
                            }
                        }else{
                            $approvedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[wfh_status_changed_by]'");
                            $dataApproveName = mysqli_fetch_array($approvedByQry);

                            if ($dataApproveName['admin_name']!=null) {
                                $wfh['approved_by_name'] = "By, ".$dataApproveName['admin_name']."";
                            }else{
                                $wfh['approved_by_name'] = "";
                            }
                        }
                    }


                    $wfh['rejected_by_name'] = "";

                    if ($data["wfh_status"] == 2) {
                        if ($data['wfh_status_changed_type'] == 0) {
                            $rejectedByQry = $d->selectRow("user_full_name","users_master","user_id = '$data[wfh_status_changed_by]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                            if ($dataRejectedName['user_full_name']!=null) {
                                $wfh['rejected_by_name'] = "By, ".$dataRejectedName['user_full_name']."";
                            }else{
                                $wfh['rejected_by_name'] = "";
                            }

                        }else{
                            $rejectedByQry = $d->selectRow("admin_name","bms_admin_master","admin_id = '$data[wfh_status_changed_by]'");
                            $dataRejectedName = mysqli_fetch_array($rejectedByQry);

                             if ($dataRejectedName['admin_name']!=null) {
                                $wfh['rejected_by_name'] = "By, ".$dataRejectedName['admin_name']."";
                            }else{
                                $wfh['rejected_by_name'] = "";
                            }
                        }
                    }

                    array_push($response["wfh_list"], $wfh);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Task Found";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['getAllWFH']=="getAllWFH" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $wfh_req_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND wfh_master.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND wfh_master.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $wfhQry = $d->selectRow("wfh_master.*,users_master.user_id,users_master.user_full_name,users_master.user_designation,users_master.user_profile_pic,floors_master.floor_name,block_master.block_name","wfh_master,users_master,block_master,floors_master","wfh_master.society_id = '$society_id' 
                AND users_master.user_id = wfh_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND block_master.block_id = users_master.block_id 
                AND wfh_master.wfh_status = '0' $appendQuery","ORDER BY wfh_master.wfh_id DESC $LIMIT_DATA");
                   
            if(mysqli_num_rows($wfhQry)>0){
                
                $response["wfh_list"] = array();

                while($data=mysqli_fetch_array($wfhQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $wfh = array(); 

                    $wfhId = $data['wfh_id'];
                    $wfh["wfh_id"] = $data['wfh_id'];

                    if ($data['wfh_attachment'] == "") {
                        $wfh["wfh_attachment"] = "";
                    }else{
                        $wfh["wfh_attachment"] = $base_url."img/wfh_attachment/" .$data['wfh_attachment'];
                    }

                    if ($data['wfh_day_type'] == "0") {
                        $wfh["wfh_day_type"] = "Full Day";
                        $wfh["wfh_first_second_half"] = "";
                    }else{
                        $wfh["wfh_day_type"] = "Half Day";

                        if ($data['wfh_first_second_half'] == "1") {
                            $wfh["wfh_first_second_half"] = "First Half";
                        }else{
                            $wfh["wfh_first_second_half"] = "Second Half";
                        }
                    }

                    $wfhDate = $data['wfh_start_date'];
                    $wfhEndDate = $data['wfh_end_date'];
                    

                    $wfh["month"] = date('m', strtotime($data['wfh_start_date']));
                    $wfh["year"] = date('Y', strtotime($data['wfh_start_date']));
                    $wfh["wfh_start_date"] = date('Y-m-d', strtotime($wfhDate));
                    $wfh["wfh_start_date_view"] = date('D, dS F Y', strtotime($wfhDate));
                    $wfh["wfh_end_date"] = date('Y-m-d', strtotime($wfhEndDate));
                    $wfh["wfh_end_date_view"] = date('D, dS F Y', strtotime($wfhEndDate));
                    $wfh['user_id'] = $data['user_id'];
                    $wfh['user_full_name'] = $data['user_full_name'];
                    $wfh['branch_name'] = $data['block_name'];
                    $wfh['department_name'] = $data['floor_name'];
                    $wfh['user_designation'] = $data['user_designation'];
                    $wfh["wfh_reason"] = $data['wfh_reason'];
                    $wfh["wfh_latitude"] = $data['wfh_latitude'];
                    $wfh["wfh_longitude"] = $data['wfh_longitude'];
                    $wfh["wfh_address"] = $data['wfh_address'];
                    $wfh["wfh_attendance_range"] = $data['wfh_attendance_range'];

                    if ($data['user_profile_pic'] != '') {
                        $wfh["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $wfh["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $wfh["wfh_status"] = $data['wfh_status'];
                    
                    if ($data['wfh_status'] == 0) {
                        $wfh["wfh_status_view"] = "Pending";
                    }else if ($data['wfh_status'] == 1) {
                        $wfh["wfh_status_view"] = "Approved";
                    }else if ($data['wfh_status'] == 2) {
                        $wfh["wfh_status_view"] = "Rejected";
                    }

                    if ($wfhDate < $currentDate) {
                        $m->set_data('wfh_status_changed_by',$user_id);
                        $m->set_data('wfh_status',"3");
                        $m->set_data('wfh_declined_reason',"Auto Rejected");
                        $m->set_data('wfh_declined_date',date('Y-m-d H:i:s'));

                        $a = array(
                            'wfh_status_changed_by' =>$m->get_data('wfh_status_changed_by'),
                            'wfh_status'=>$m->get_data('wfh_status'),
                            'wfh_declined_reason'=>$m->get_data('wfh_declined_reason'),
                            'wfh_declined_date'=>$m->get_data('wfh_declined_date'),
                        );

                        $wfhQry = $d->update("wfh_master",$a,"wfh_id = '$wfhId'"); 
                    }else{
                        array_push($response["wfh_list"], $wfh);                        
                    }

                }
 
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No Task Found";
                $response["status"] = "201";
                echo json_encode($response);
            }            
        }else if($_POST['addWFHRequest']=="addWFHRequest" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $wfh_attachment = "";

            if ($_FILES["wfh_attachment"]["tmp_name"] != null) {
                // code...
                $file11=$_FILES["wfh_attachment"]["tmp_name"];
                $extId = pathinfo($_FILES['wfh_attachment']['name'], PATHINFO_EXTENSION);
                $extAllow=array("png","jpg","JPG","jpeg","JPEG","PNG","pdf","PDF");

                if(in_array($extId,$extAllow)) {
                    $temp = explode(".", $_FILES["wfh_attachment"]["name"]);
                    $wfh_attachment = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES["wfh_attachment"]["tmp_name"], "../img/wfh_attachment/" . $wfh_attachment);
                } else {
                
                    $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $qq1 = $d->select("wfh_group_id_master","wfh_group_id = '1'");
            $qqData = mysqli_fetch_array($qq1);

            $lastWFHGroupId = $qqData['wfh_last_group_id'];
            $newWFHGroupId = $lastWFHGroupId + 1;

            $m->set_data('wfh_last_group_id',$newWFHGroupId);

            $aa1 = array(
                'wfh_last_group_id' =>$m->get_data('wfh_last_group_id')
            );

            $d->update("wfh_group_id_master",$aa1,"wfh_group_id = '1'");

            $wfh_datesList = explode(",",$wfh_dates);
            $wfh_day_typeList = explode(",",$wfh_day_types);
            $wfh_first_second_half = explode(",",$wfh_first_second_half);

            for ($i=0; $i < COUNT($wfh_datesList); $i++) { 

                $m->set_data('society_id',$society_id);
                $m->set_data('floor_id',$floor_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('wfh_group_id',$newWFHGroupId);
                $m->set_data('wfh_start_date',$wfh_datesList[$i]);
                $m->set_data('wfh_end_date',$wfh_datesList[$i]);
                $m->set_data('wfh_day_type',$wfh_day_typeList[$i]);
                $m->set_data('wfh_first_second_half',$wfh_first_second_half[$i]);
                $m->set_data('wfh_attachment',$wfh_attachment);
                $m->set_data('wfh_reason',$wfh_reason);
                $m->set_data('wfh_latitude',$wfh_latitude);
                $m->set_data('wfh_longitude',$wfh_longitude);
                $m->set_data('wfh_address',$wfh_address);
                $m->set_data('wfh_created_at',$dateTime);
                $m->set_data('wfh_created_at',$dateTime);

                $a = array(
                    'society_id' =>$m->get_data('society_id'),
                    'floor_id'=>$m->get_data('floor_id'),
                    'user_id'=>$m->get_data('user_id'),
                    'wfh_group_id'=>$m->get_data('wfh_group_id'),
                    'wfh_start_date'=>$m->get_data('wfh_start_date'),
                    'wfh_end_date'=>$m->get_data('wfh_end_date'),
                    'wfh_day_type'=>$m->get_data('wfh_day_type'),
                    'wfh_first_second_half'=>$m->get_data('wfh_first_second_half'),
                    'wfh_attachment'=>$m->get_data('wfh_attachment'),
                    'wfh_reason'=>$m->get_data('wfh_reason'),
                    'wfh_latitude'=>$m->get_data('wfh_latitude'),
                    'wfh_longitude'=>$m->get_data('wfh_longitude'),
                    'wfh_address'=>$m->get_data('wfh_address'),
                    'wfh_created_at'=>$m->get_data('wfh_created_at'),
                );

                $wfhQry = $d->insert("wfh_master",$a); 

            }               
            
            if ($wfhQry == true) {

                // To App Access

                $title = "New Work From Home Request";
                $description = "Requested by, ".$user_name;

                $access_type = $wfh_req_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmAccessAry=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmAccessAryiOS=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $nResident->noti("wfh_request","",$society_id,$fcmAccessAry,$title,$description,$tabPosition);
                    $nResident->noti_ios("wfh_request","",$society_id,$fcmAccessAryiOS,$title,$description,$tabPosition);

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"wfh_request","wfh.png",$tabPosition,"users_master.user_id IN ('$userFcmIds')");
                }

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"wfh_request","wfh.png",$tabPosition,"users_master.user_id IN ('$userIds')");

                // To Admin App

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("19",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("19",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"wfhMaster");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"wfhMaster");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"wfhMaster",
                  'admin_click_action '=>"wfhMaster",
                  'notification_logo'=>'wfh.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "WFH Requested";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['approveWFH']=="approveWFH" && $user_id!='' && $wfh_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $m->set_data('wfh_attendance_range',$wfh_attendance_range);
            $m->set_data('wfh_status_changed_by',$user_id);
            $m->set_data('wfh_status',"1");
            $m->set_data('wfh_approved_date',$dateTime);
            $m->set_data('selfie_attendance',$selfie_attendance);

            $a = array(
                'wfh_attendance_range'=>$m->get_data('wfh_attendance_range'),
                'wfh_status_changed_by'=>$m->get_data('wfh_status_changed_by'),
                'wfh_status'=>$m->get_data('wfh_status'),
                'wfh_approved_date'=>$m->get_data('wfh_approved_date'),
                'wfh_take_selfie'=>$m->get_data('selfie_attendance'),
            );

            $wfhQry = $d->update("wfh_master",$a,"wfh_id = '$wfh_id' AND user_id  = '$wfh_user_id'");
                             
            if ($wfhQry == true) {

                $title = "Work From Home request has been approved";
                $description = "By, ".$user_name."\n"."For ".date('D, dS F Y',strtotime($wfh_start_date));

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$wfh_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];


                if ($device == 'android') {
                    $nResident->noti("wfh_approved","",$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("wfh_approved","",$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotification($society_id,$title,$description,"wfh_approved","wfh.png","user_id = '$wfh_user_id'");

                $response["message"] = $title;                
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['rejectWFH']=="rejectWFH" && $user_id!='' && $wfh_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $m->set_data('wfh_status_changed_type',"0");
            $m->set_data('wfh_status_changed_by',$user_id);
            $m->set_data('wfh_status',"2");
            $m->set_data('wfh_declined_reason',$wfh_declined_reason);
            $m->set_data('wfh_declined_date',$dateTime);

            $a = array(
                'wfh_status_changed_type'=>$m->get_data('wfh_status_changed_type'),
                'wfh_status_changed_by'=>$m->get_data('wfh_status_changed_by'),
                'wfh_status'=>$m->get_data('wfh_status'),
                'wfh_declined_reason'=>$m->get_data('wfh_declined_reason'),
                'wfh_declined_date'=>$m->get_data('wfh_declined_date'),
            );

            $wfhQry = $d->update("wfh_master",$a,"wfh_id = '$wfh_id' AND user_id  = '$wfh_user_id'");
                             
            if ($wfhQry == true) {

                $title = "Work From Home request has been rejected";
                $description = "By, ".$user_name."\n"."For ".date('D, dS F Y',strtotime($wfh_start_date));

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$wfh_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                if ($device == 'android') {
                    $nResident->noti("wfh_rejected","",$society_id,$user_token,$title,$description,"");
                } else if ($device == 'ios') {
                    $nResident->noti_ios("wfh_rejected","",$society_id,$user_token,$title,$description,"");
                }

                $d->insertUserNotification($society_id,$title,$description,"wfh_rejected","wfh.png","user_id = '$wfh_user_id'");

                $response["message"] = $title;                
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['deleteWFHRequest']=="deleteWFHRequest" && $user_id!='' && $wfh_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $deleteQry = $d->delete("wfh_master","wfh_id = '$wfh_id' AND user_id = '$user_id' AND wfh_status = '0'");

            if ($deleteQry == true) {

                $d->insert_myactivity($user_id, "$society_id", $user_id, "$user_name", "WFH Request Deleted", "wfh.png");

                $response["message"] = "WFH Request Deleted";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Failed to delete expense";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['checkHasAttendance']=="checkHasAttendance" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $attQry = $d->selectRow("attendance_id","attendance_master","user_id = '$user_id' AND attendance_date_start = '$wfh_date'");

            if (mysqli_num_rows($attQry) > 0) {

                $response["message"] = "Already Punched IN/OUT";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['wfhCalendar']=="wfhCalendar" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shift_time_id = $sdata['shift_time_id'];
            }

            $response['monthly_history'] = array();
            //$response['week'] = array();

            $datesArrayOfMonth = range_date($month_start_date,$month_end_date);

            $holidayAry = array();
            $weekAry = array();

            $currdt= strtotime($month_start_date);
            $nextmonth=strtotime($month_start_date."+1 month");
            $i=0;
            $flag=true;

            $totalMonthHours = 0;

            do{
                $weekday= date("w",$currdt);
                $endday=abs($weekday-6);
                $startarr[$i]=$currdt;
                $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
                $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");

                if($endarr[$i]>=$nextmonth){
                    $endarr[$i]=strtotime(date("Y-m-d",$nextmonth)."-1 day");;
                    $flag=false;        
                }

                $i++;

            }while($flag);

            // Shift Data

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shift_time_id' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);

            $alternateWeekOff = explode(",",$shiftData['alternate_week_off']);
            $alternate_weekoff_days_array = explode(",",$shiftData['alternate_weekoff_days']);
            $weekOffDays = explode(",",$shiftData['week_off_days']);

            // Week
        
            for ($x = 0; $x < count($startarr); $x++) {

                $weekArray = array();

                $weekArray["total_hours"] = "0";

                $sDate = date('Y-m-d',$startarr[$x]);
                $eDate = date('Y-m-d',$endarr[$x]);

                $datetime1 = new DateTime($sDate);
                $datetime2 = new DateTime($eDate);

                $daysArray['days'] = array();
                $weekOffAry = array();
                $minusWeekOffHR = 0;
                $minusAlternateWeekOffDays = 0;
                $minusHoliday = 0;

                // Total Week Hours 

                for ($date = $datetime1; $date <= $datetime2; $date->modify('+1 day')) {
                    $dayN= $date->format('l');
                    $dayDate = $date->format('Y-m-d');

                        // code...
                        $days = array();
                        $hDates = array();
                        $days['day'] = $dayN;
                        $days['date'] = $dayDate;
                        $days['day_pos'] = $day_pos =  date('w', strtotime($dayDate)) ;
                        
                        $holidayQry = $d->selectRow("holiday_start_date,holiday_name","holiday_master","society_id = '$society_id' AND holiday_start_date='$dayDate' ");

                        $holidayData = mysqli_fetch_array($holidayQry);

                        $x1 = $x+1;
                        // holiday off
                        if (mysqli_num_rows($holidayQry)>0) {
                            array_push($holidayAry,$dayDate);
                            $minusHoliday += $perdayHours;
                        } else {
                            // full day off
                            if (in_array($day_pos,$weekOffDays) && !in_array($day_pos,$alternate_weekoff_days_array)) {
                                array_push($weekAry,$dayDate);
                                $minusWeekOffHR += $perdayHours;
                            }

                            // alternate week day off
                            if ($shiftData['has_altenate_week_off'] == "1") {  
                                if (in_array($day_pos,$alternate_weekoff_days_array) && 
                                    in_array($x1,$alternateWeekOff)) {
                                    array_push($weekAry,$dayDate);
                                    $minusAlternateWeekOffDays += $perdayHours;
                                }
                            }
                        }
                        array_push($daysArray['days'],$days);
                }
            }

            // Month

            $totalWeekOff = 0;
            $totalHolidays = 0;
 
            for ($j = 0; $j < count($datesArrayOfMonth); $j++) {
                
                $dates = array();

                $monthDate = $datesArrayOfMonth[$j];
                $month = date('Y-m',strtotime($monthDate));
                $dates['date'] = $monthDate;
                $day_pos_new =  date('w', strtotime($monthDate));

                $dates['holiday'] = false;
                $dates['week_off'] = false;
                $dates['leave_applied'] = false;
                $dates['wfh_applied'] = false;
                $dates['date_gone'] = false;
                $dates['has_attendance'] = false;

                if ($monthDate < $currentDate) {
                    $dates['date_gone'] = true;
                }

                $attQry = $d->selectRow("attendance_id","attendance_master","user_id = '$user_id' AND society_id = '$society_id' AND attendance_date_start = '$monthDate'");

                if (mysqli_num_rows($attQry) > 0) {
                    $dates['has_attendance'] = true;
                }

                if (in_array($monthDate,$holidayAry)) {
                    $dates['holiday'] = true;
                    $totalHolidays += 1;
                }else{
                    if (in_array($monthDate,$weekAry)) {
                        $dates['week_off'] = true;
                        $totalWeekOff += 1;
                        $isDateGone = "false";
                    }
                }

                $holidayNameQry = $d->selectRow("holiday_name,holiday_description","holiday_master","society_id = '$society_id' AND holiday_start_date = '$monthDate'");

                $dates["holiday_name"] = "";

                if (mysqli_num_rows($holidayNameQry) > 0) {

                    $holidayNameData = mysqli_fetch_array($holidayNameQry);

                    $dates["holiday_name"] = $holidayNameData['holiday_name'];
                }

                // Check WFH Applied

                $wfhQry = $d->selectRow("*","wfh_master","society_id = '$society_id' AND user_id = '$user_id' AND wfh_start_date = '$monthDate'");


                if (mysqli_num_rows($wfhQry) > 0) {

                    $wfhData = mysqli_fetch_array($wfhQry);
                    $dates['wfh_applied'] = true;
                    $dates["wfh_reason"] = $wfhData['wfh_reason'];
                    $dates["wfh_status"] = $wfhData['wfh_status'];
                    $dates["wfh_start_date"] = $wfhData['wfh_start_date'];
                    
                    if ($wfhData['wfh_status'] == 0) {
                        $dates["wfh_status_view"] = "Pending";
                    }else if ($wfhData['wfh_status'] == 1) {
                        $dates["wfh_status_view"] = "Approved";
                    }else if ($wfhData['wfh_status'] == 2) {
                        $dates["wfh_status_view"] = "Rejected";
                    }
                }

                $leaveAppliedQry = $d->selectRow("leave_master.*,leave_type_master.leave_type_name","leave_master,leave_type_master","leave_master.society_id = '$society_id' AND leave_master.leave_start_date = '$monthDate' AND leave_master.user_id = '$user_id' AND leave_master.leave_type_id = leave_type_master.leave_type_id AND (leave_master.leave_status = '1' OR leave_master.leave_status = '0')");

                if (mysqli_num_rows($leaveAppliedQry) > 0) {

                    $leaveAppliedData = mysqli_fetch_array($leaveAppliedQry);

                    $dates['leave_applied'] = true;

                    $dates["leave_type_name"] = $leaveAppliedData['leave_type_name'];
                    $dates["leave_reason"] = $leaveAppliedData['leave_reason'];
                    $dates["leave_start_date"] = $leaveAppliedData['leave_start_date'];

                    $paidUnpaid = $leaveAppliedData['paid_unpaid'];
                    $leaveDay = $leaveAppliedData['leave_day_type'];
                    $leaveStatus = $leaveAppliedData['leave_status'];

                    if ($paidUnpaid == '0') {
                        $dates["paid_unpaid"] = "Paid";
                    }else{
                        $dates["paid_unpaid"] = "Unpaid";
                    }
                    
                    if ($leaveDay == '0') {
                        $dates["leave_day_type"] = "Full Day";
                    }else{
                        $dates["leave_day_type"] = "Half Day";
                    }

                    
                    if ($leaveStatus == '0') {
                        $dates["leave_status"] = "Pending";
                    }else{
                        $dates["leave_status"] = "Approved";
                    }
                }
                array_push($response['monthly_history'],$dates);
            }

            echo json_encode($response);    
        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function createRange($start, $end, $format = 'Y-m-d') {
    $start  = new DateTime($start);
    $end    = new DateTime($end);
    $invert = $start > $end;

    $dates = array();
    $dates[] = $start->format($format);
    while ($start != $end) {
        $start->modify(($invert ? '-' : '+') . '1 day');
        $dates[] = $start->format($format);
    }
    return $dates;
}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}

?>