<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

  if ($key == $keydb) {
    $response = array();
    extract(array_map("test_input" , $_POST));
    // with new Logic
     if($_POST['getLocalServiceProviders']=="getLocalServiceProviders"){
      $local_service_providerq=$d->select("local_service_provider_master","service_provider_category_status='0'","ORDER BY service_provider_category_name ASC");

      if(mysqli_num_rows($local_service_providerq)>0){

        $response["local_service_provider"] = array();


        while($data_local_service_provider=mysqli_fetch_array($local_service_providerq)) {

         
              $language_category_name= html_entity_decode($data_local_service_provider['service_provider_category_name']);

          // $usersCount= $d->count_data_direct("service_provider_users_id","local_service_provider_users","local_service_master_id='$data_local_service_provider[local_service_provider_id]'");

          $MainCoutTotal=$d->select("local_service_provider_users_category,local_service_provider_master,local_service_provider_users","local_service_provider_users.service_provider_users_id=local_service_provider_users_category.service_provider_users_id AND local_service_provider_users_category.category_id=local_service_provider_master.local_service_provider_id AND   local_service_provider_users_category.category_id='$data_local_service_provider[local_service_provider_id]' AND local_service_provider_users.service_provider_status=0 AND local_service_provider_users.service_provider_delete_status = 0","");
               $subCoutTotal11= mysqli_num_rows($MainCoutTotal);

          if ($subCoutTotal11>0) {

            $local_service_provider_id=$data_local_service_provider['local_service_provider_id'];
            $local_service_provider = array();
            $local_service_provider["usersCount"]=$subCoutTotal11;

            $local_service_provider["local_service_provider_id"]=$data_local_service_provider['local_service_provider_id'];
            $local_service_provider["service_provider_category_name"]=$language_category_name;
            $local_service_provider["service_provider_category_name_search"]=html_entity_decode($data_local_service_provider['service_provider_category_name']);

            $local_service_provider["service_provider_category_image"]=$base_url."img/local_service_provider/local_service_cat/".$data_local_service_provider['service_provider_category_image'];

            $local_service_provider["local_service_sub_provider"] = array();
            $qs=$d->select("local_service_provider_sub_master","local_service_provider_id='$local_service_provider_id'","ORDER BY local_service_provider_sub_id ASC");

            $otherCheck=$d->select("local_service_provider_sub_master","local_service_provider_id='$local_service_provider_id' AND service_provider_sub_category_name!='Other'","ORDER BY local_service_provider_sub_id ASC");
            $otherCount=mysqli_num_rows($otherCheck);
            if ($otherCount>0) {
              while ($subData=mysqli_fetch_array($qs)) {
                  
                
                $language_category_name= html_entity_decode($subData['service_provider_sub_category_name']);

                $subCoutTotal=$d->select("local_service_provider_users,local_service_provider_users_category","local_service_provider_users_category.service_provider_users_id=local_service_provider_users_category.service_provider_users_id AND local_service_provider_users.local_service_master_id='$local_service_provider_id' AND  local_service_provider_users_category.sub_category_id='$subData[local_service_provider_sub_id]'  ","");
               $subCoutTotal= mysqli_num_rows($subCoutTotal);
                if ($subCoutTotal>0) {
                 
                  $local_service_sub_provider = array();
                  $local_service_sub_provider["local_service_provider_id"]=$subData['local_service_provider_id'];
                  $local_service_sub_provider["local_service_provider_sub_id"]=$subData['local_service_provider_sub_id'];
                  $local_service_sub_provider["service_provider_sub_category_name"]=$language_category_name;
                  $local_service_sub_provider["service_provider_sub_category_name_search"]= html_entity_decode($subData['service_provider_sub_category_name']);
                  $local_service_sub_provider["service_provider_sub_category_image"]=$base_url."img/local_service_provider/local_service_cat/".$subData['service_provider_sub_category_image'];
                  
                  $local_service_sub_provider["subCoutTotal"]=$subCoutTotal;

                  array_push($local_service_provider["local_service_sub_provider"], $local_service_sub_provider); 
                }
              }

            }
            array_push($response["local_service_provider"], $local_service_provider); 
          }
        } 

        $response["message"]="Local Service Providers get successfully.";
        $response["status"]="200";
        echo json_encode($response);

      }else{

        $response["message"]="Local Service Providers Details Not Found.";
        $response["status"]="201";
        echo json_encode($response);

      }

    } else if($_POST['getServiceProviderUser']=="getServiceProviderUser" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){
        if ($local_service_provider_sub_id==0 || $local_service_provider_sub_id=='') {

          $fq=$d->selectRow("local_service_provider_sub_id","local_service_provider_sub_master","service_provider_sub_category_name='Other' AND local_service_provider_id='$local_service_provider_id'");

          $otherData=mysqli_fetch_array($fq);
          $otherId= $otherData['local_service_provider_sub_id'];
          if ($otherId!='') {
            $appendqqq = "  AND local_service_provider_users_category.sub_category_id='$otherId' ";
          }

          $local_service_providerq=$d->select("local_service_provider_users,local_service_provider_users_category","local_service_provider_users_category.service_provider_users_id=local_service_provider_users.service_provider_users_id AND local_service_provider_users_category.category_id='$local_service_provider_id'   AND local_service_provider_users.service_provider_status=0 OR local_service_provider_users_category.service_provider_users_id=local_service_provider_users.service_provider_users_id AND local_service_provider_users_category.category_id='$local_service_provider_id' AND  local_service_provider_users.service_provider_status=0   $appendqqq ","");
         
        } else {

          $local_service_providerq=$d->select("local_service_provider_users,local_service_provider_users_category","local_service_provider_users_category.service_provider_users_id=local_service_provider_users.service_provider_users_id AND local_service_provider_users_category.category_id='$local_service_provider_id' AND local_service_provider_users_category.sub_category_id='$local_service_provider_sub_id' AND  local_service_provider_users.service_provider_status=0 AND local_service_provider_users.service_provider_status=0 ","");
        }


        if(mysqli_num_rows($local_service_providerq)>0){

          $response["local_service_provider"] = array();

          $tempArray=array();


          while($data_local_service_provider=mysqli_fetch_array($local_service_providerq)) {

            $local_service_provider = array();
            $local_service_provider["otherId"]=$otherId.'';
            $local_service_provider["service_provider_users_id"]=$data_local_service_provider['service_provider_users_id'];
            $local_service_provider["service_provider_name"]=html_entity_decode($data_local_service_provider['service_provider_name']);
            $local_service_provider["service_provider_address"]=html_entity_decode($data_local_service_provider['service_provider_address']);
            $local_service_provider["service_provider_latitude"]=$data_local_service_provider['service_provider_latitude'];
            $local_service_provider["service_provider_logitude"]=$data_local_service_provider['service_provider_logitude'];
            $local_service_provider["service_provider_phone"]=$data_local_service_provider['service_provider_phone'];
            $local_service_provider["sp_webiste"]=$data_local_service_provider['sp_webiste'];
            $local_service_provider["work_description"]=html_entity_decode($data_local_service_provider['work_description']);
            $local_service_provider["service_provider_phone_view"]=$data_local_service_provider['country_code'].' '.$data_local_service_provider['service_provider_phone'];
            $local_service_provider["service_provider_email"]=$data_local_service_provider['service_provider_email'];
            $local_service_provider["token"]=$data_local_service_provider['token'];
            $local_service_provider["is_kyc"]=$data_local_service_provider['kyc_status'];
            $currenttime = date('h:i');
            if($currenttime< date('h:i', strtotime($data_local_service_provider['open_time'])) && $currenttime> date('h:i', strtotime($data_local_service_provider['close_time']))){ $openstatus = "Closed"; }else{ $openstatus = "Open"; }
            $local_service_provider["openStatus"]=($data_local_service_provider['open_time']!='') ? $openstatus : '';

            $startTime  = date('h:i A', strtotime($data_local_service_provider['open_time']));
            $closeTime  = date('h:i A', strtotime($data_local_service_provider['close_time']));

            $local_service_provider["timing"]=($data_local_service_provider['open_time']!='') ? $startTime.' to '.$closeTime : '';
            $local_service_provider["service_provider_user_image"]=$base_url."img/local_service_provider/local_service_users/".$data_local_service_provider['service_provider_user_image'];

            if ($data_local_service_provider['brochure_profile']!="") {
              $local_service_provider["brochure_profile"]=$base_url."img/local_service_provider/local_service_location_proof/".$data_local_service_provider['brochure_profile'];
            } else {
              $local_service_provider["brochure_profile"]="" ;
            }

            $local_service_provider_id = $data_local_service_provider['service_provider_users_id'];
            /**************************** Average Rating ************************************/
            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND society_id='$society_id' AND status=0" );
            $average = 0;
            while ( $avg = mysqli_fetch_array($data)) {
              $average = $average + $avg['rating_point'];
            }
            $totalRating = mysqli_num_rows($data);
            if($totalRating>0){
              $averagerating = $average/$totalRating;
              $local_service_provider['totalRatings'] = number_format($totalRating);
              $local_service_provider['averageRating'] = number_format($averagerating,1);
            }else{
              $local_service_provider['totalRatings'] = '0';
              $local_service_provider['averageRating'] = '0';          
            }
            /**************************** Average Rating ************************************/
            /**************************** Previous Rated Rating *****************************/
            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND society_id='$society_id' AND user_id='$user_id' AND status=0");
            if(mysqli_num_rows($data)>0){
              $userdata = mysqli_fetch_array($data);
              $local_service_provider['userPreviousRating'] = $userdata['rating_point'];
              $local_service_provider['userPreviousComment'] = $userdata['rating_comment'];
            }else{
              $local_service_provider["userPreviousRating"]="";
              $local_service_provider["userPreviousComment"]="";
            }
            /**************************** Previous Rated Rating *****************************/
            /**************************** Distance Calculation ******************************/
            $service_provider_latitude = $data_local_service_provider['service_provider_latitude'];
            $service_provider_logitude = $data_local_service_provider['service_provider_logitude'];

             $radiusInMeeter=  $d->haversineGreatCircleDistance($user_latitude,$user_longitude,$service_provider_latitude, $service_provider_logitude);

            $totalKm= number_format($radiusInMeeter/1000,2,'.','');

            if ($user_latitude=="0.0" || $user_latitude=="") {
            $local_service_provider["distance"]='No Data Available';
            $local_service_provider["distance_sort"]=$totalKm;
            }else {
            $local_service_provider["distance"]=$totalKm. ' KM';
            $local_service_provider["distance_sort"]=$totalKm;
            }
            
            array_push($tempArray, $local_service_provider); 

          } 

          function sortByOrder($a, $b) {
              return $a['distance_sort'] - $b['distance_sort'];
          }

          usort($tempArray, 'sortByOrder');

          // print_r($tempArray);
          
          for ($i1=0; $i1 <count($tempArray) ; $i1++) { 
              $local_service_provider = array();
            $local_service_provider["otherId"]=$otherId.'';
            $local_service_provider["service_provider_users_id"]=$tempArray[$i1]['service_provider_users_id'];
            $local_service_provider["service_provider_name"]=$tempArray[$i1]['service_provider_name'];
            $local_service_provider["service_provider_address"]=$tempArray[$i1]['service_provider_address'];
            $local_service_provider["service_provider_latitude"]=$tempArray[$i1]['service_provider_latitude'];
            $local_service_provider["service_provider_logitude"]=$tempArray[$i1]['service_provider_logitude'];
            $local_service_provider["service_provider_phone"]=$tempArray[$i1]['service_provider_phone'];
            $local_service_provider["service_provider_phone_view"]=$tempArray[$i1]['service_provider_phone_view'];
            $local_service_provider["sp_webiste"]=$tempArray[$i1]['sp_webiste'];
            $local_service_provider["work_description"]=$tempArray[$i1]['work_description'];

            $local_service_provider["service_provider_email"]=$tempArray[$i1]['service_provider_email'];
            $local_service_provider["is_kyc"]=$tempArray[$i1]['is_kyc'];
            $local_service_provider["token"]=$tempArray[$i1]['token'];
            
            $local_service_provider["openStatus"]=$tempArray[$i1]['openStatus'] ;
            $local_service_provider["timing"]=$tempArray[$i1]['timing'];
            $local_service_provider["service_provider_user_image"]= $tempArray[$i1]['service_provider_user_image'];
            $local_service_provider["brochure_profile"]= $tempArray[$i1]['brochure_profile'];
            $local_service_provider_id = $tempArray[$i1]['service_provider_users_id'];
           
            $averagerating = $average/$totalRating;
            $local_service_provider['totalRatings'] =  $tempArray[$i1]['totalRatings'];
            $local_service_provider['averageRating'] =  $tempArray[$i1]['averageRating'];
            
           
            $local_service_provider["userPreviousRating"]= $tempArray[$i1]['userPreviousRating'];
            $local_service_provider["userPreviousComment"]= $tempArray[$i1]['userPreviousComment'];

            $service_provider_latitude = $tempArray[$i1]['service_provider_latitude'];
            $service_provider_logitude = $tempArray[$i1]['service_provider_logitude'];

           
            $local_service_provider["distance"]=$tempArray[$i1]['distance'];
            $local_service_provider["distance_sort"]=$tempArray[$i1]['distance_sort'];
            
            
            array_push($response["local_service_provider"], $local_service_provider); 
          }

          $response["message"]="Get Local Service Providers Details successfully.";
          $response["status"]="200";
          echo json_encode($response);

        }else{

          $response["message"]="Local Service Provider Users Details Not Found.";
          $response["status"]="201";
          echo json_encode($response);

        }
      


    }else if($_POST['getServiceProviderUserNew']=="getServiceProviderUserNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){

          $queryAry=array();
          if ($local_service_provider_id!=0 && $local_service_provider_id!='' && $local_service_provider_id>0) {
            $append_query="local_service_provider_users_category.category_id='$local_service_provider_id'";
            array_push($queryAry, $append_query);
          } 
          if ($local_service_provider_sub_id!=0 && $local_service_provider_sub_id!='' && $local_service_provider_sub_id>0) {
            $append_query1 ="local_service_provider_users_category.sub_category_id='$local_service_provider_sub_id'";
            array_push($queryAry, $append_query1);
          }

          if ($search_keyword!='') {
            $append_query1 ="local_service_provider_users.service_provider_name LIKE '%$search_keyword%'";
            array_push($queryAry, $append_query1);
          }


          $appendQuery= implode(" AND ", $queryAry);
          if ($appendQuery!='') {
            $appendQuery= " AND ".$appendQuery;
          }

         
          $local_service_providerq=$d->select("local_service_provider_users,local_service_provider_users_category,local_service_provider_master","local_service_provider_master.local_service_provider_id=local_service_provider_users_category.category_id AND local_service_provider_users_category.service_provider_users_id=local_service_provider_users.service_provider_users_id AND  local_service_provider_users.service_provider_status=0 AND local_service_provider_users.service_provider_delete_status = 0 $appendQuery ","");


        if(mysqli_num_rows($local_service_providerq)>0){

          $response["local_service_provider"] = array();

          $tempArray=array();


          while($data_local_service_provider=mysqli_fetch_array($local_service_providerq)) {

            $local_service_provider = array();
            $local_service_provider["otherId"]=$otherId.'';
            $local_service_provider["service_provider_users_id"]=$data_local_service_provider['service_provider_users_id'];
            $local_service_provider["service_provider_name"]=html_entity_decode($data_local_service_provider['service_provider_name']);
            $local_service_provider["service_provider_category_name"]=html_entity_decode($data_local_service_provider['service_provider_category_name']);
            $local_service_provider["service_provider_address"]=html_entity_decode($data_local_service_provider['service_provider_address']);
            $local_service_provider["service_provider_latitude"]=$data_local_service_provider['service_provider_latitude'];
            $local_service_provider["service_provider_logitude"]=$data_local_service_provider['service_provider_logitude'];
            $local_service_provider["service_provider_phone"]=$data_local_service_provider['service_provider_phone'];
            $local_service_provider["sp_webiste"]=$data_local_service_provider['sp_webiste'];
            $local_service_provider["work_description"]=html_entity_decode($data_local_service_provider['work_description']);
            $local_service_provider["service_provider_phone_view"]=$data_local_service_provider['country_code'].' '.$data_local_service_provider['service_provider_phone'];
            $local_service_provider["service_provider_email"]=$data_local_service_provider['service_provider_email'];
            $local_service_provider["token"]=$data_local_service_provider['token'];
            $local_service_provider["is_kyc"]=$data_local_service_provider['kyc_status'];
            $currenttime = date('h:i');
            if($currenttime< date('h:i', strtotime($data_local_service_provider['open_time'])) && $currenttime> date('h:i', strtotime($data_local_service_provider['close_time']))){ $openstatus = "Closed"; }else{ $openstatus = "Open"; }
            $local_service_provider["openStatus"]=($data_local_service_provider['open_time']!='') ? $openstatus : '';

            $startTime  = date('h:i A', strtotime($data_local_service_provider['open_time']));
            $closeTime  = date('h:i A', strtotime($data_local_service_provider['close_time']));

            $local_service_provider["timing"]=($data_local_service_provider['open_time']!='') ? $startTime.' to '.$closeTime : '';

            if ($data_local_service_provider['service_provider_user_image']!="") {
            $local_service_provider["service_provider_user_image"]=$base_url."img/local_service_provider/local_service_users/".$data_local_service_provider['service_provider_user_image'];
            } else {
              $local_service_provider["service_provider_user_image"]= "";
            }

            if ($data_local_service_provider['brochure_profile']!="") {
              $local_service_provider["brochure_profile"]=$base_url."img/local_service_provider/local_service_location_proof/".$data_local_service_provider['brochure_profile'];
            } else {
              $local_service_provider["brochure_profile"]="" ;
            }

            $local_service_provider_id = $data_local_service_provider['service_provider_users_id'];
            /**************************** Average Rating ************************************/
            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND society_id='$society_id' AND status=0" );
            $average = 0;
            while ( $avg = mysqli_fetch_array($data)) {
              $average = $average + $avg['rating_point'];
            }
            $totalRating = mysqli_num_rows($data);
            if($totalRating>0){
              $averagerating = $average/$totalRating;
              $local_service_provider['totalRatings'] = number_format($totalRating);
              $local_service_provider['averageRating'] = number_format($averagerating,1);
            }else{
              $local_service_provider['totalRatings'] = '0';
              $local_service_provider['averageRating'] = '0';          
            }
            /**************************** Average Rating ************************************/
            /**************************** Previous Rated Rating *****************************/
            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND society_id='$society_id' AND user_id='$user_id' AND status=0");
            if(mysqli_num_rows($data)>0){
              $userdata = mysqli_fetch_array($data);
              $local_service_provider['userPreviousRating'] = $userdata['rating_point'];
              $local_service_provider['userPreviousComment'] = $userdata['rating_comment'];
            }else{
              $local_service_provider["userPreviousRating"]="";
              $local_service_provider["userPreviousComment"]="";
            }
            /**************************** Previous Rated Rating *****************************/
            /**************************** Distance Calculation ******************************/
            $service_provider_latitude = $data_local_service_provider['service_provider_latitude'];
            $service_provider_logitude = $data_local_service_provider['service_provider_logitude'];

             $radiusInMeeter=  $d->haversineGreatCircleDistance($user_latitude,$user_longitude,$service_provider_latitude, $service_provider_logitude);

            $totalKm= number_format($radiusInMeeter/1000,2,'.','');

            if ($user_latitude=="0.0" || $user_latitude=="") {
            $local_service_provider["distance_in_km"]='';
            $local_service_provider["distance_sort"]=$totalKm;
            }else {
            $local_service_provider["distance_in_km"]=$totalKm. ' KM';
            $local_service_provider["distance_sort"]=$totalKm;
            }
            
            array_push($tempArray, $local_service_provider); 

          } 

          function sortByOrder($a, $b) {
              return $a['distance_sort'] - $b['distance_sort'];
          }

          usort($tempArray, 'sortByOrder');

          // print_r($tempArray);
          
          for ($i1=0; $i1 <count($tempArray) ; $i1++) { 
              $local_service_provider = array();
            $local_service_provider["otherId"]=$otherId.'';
            $local_service_provider["service_provider_users_id"]=$tempArray[$i1]['service_provider_users_id'];
            $local_service_provider["service_provider_name"]=$tempArray[$i1]['service_provider_name'];
            $local_service_provider["service_provider_category_name"]=$tempArray[$i1]['service_provider_category_name'];
            $local_service_provider["service_provider_address"]=$tempArray[$i1]['service_provider_address'];
            $local_service_provider["service_provider_latitude"]=$tempArray[$i1]['service_provider_latitude'];
            $local_service_provider["service_provider_logitude"]=$tempArray[$i1]['service_provider_logitude'];
            $local_service_provider["service_provider_phone"]=$tempArray[$i1]['service_provider_phone'];
            $local_service_provider["service_provider_phone_view"]=$tempArray[$i1]['service_provider_phone_view'];
            $local_service_provider["sp_webiste"]=$tempArray[$i1]['sp_webiste'];
            $local_service_provider["work_description"]=$tempArray[$i1]['work_description'];

            $local_service_provider["service_provider_email"]=$tempArray[$i1]['service_provider_email'];
            $local_service_provider["is_kyc"]=$tempArray[$i1]['is_kyc'];
            $local_service_provider["token"]=$tempArray[$i1]['token'];
            
            $local_service_provider["openStatus"]=$tempArray[$i1]['openStatus'] ;
            $local_service_provider["timing"]=$tempArray[$i1]['timing'];
            $local_service_provider["service_provider_user_image"]= $tempArray[$i1]['service_provider_user_image'];
            $local_service_provider["brochure_profile"]= $tempArray[$i1]['brochure_profile'];
            $local_service_provider_id = $tempArray[$i1]['service_provider_users_id'];
           
            $averagerating = $average/$totalRating;
            $local_service_provider['totalRatings'] =  $tempArray[$i1]['totalRatings'];
            $local_service_provider['averageRating'] =  $tempArray[$i1]['averageRating'];
            
           
            $local_service_provider["userPreviousRating"]= $tempArray[$i1]['userPreviousRating'];
            $local_service_provider["userPreviousComment"]= $tempArray[$i1]['userPreviousComment'];

            $service_provider_latitude = $tempArray[$i1]['service_provider_latitude'];
            $service_provider_logitude = $tempArray[$i1]['service_provider_logitude'];

           
            $local_service_provider["distance_in_km"]=$tempArray[$i1]['distance_in_km'];
            $local_service_provider["distance_sort"]=$tempArray[$i1]['distance_sort'];
            
            
            array_push($response["local_service_provider"], $local_service_provider); 
          }

          $response["message"]="Get Successfully.".mysqli_num_rows($local_service_providerq);
          $response["status"]="200";
          echo json_encode($response);

        }else{

          $response["message"]="No Vendor Found.";
          $response["status"]="201";
          echo json_encode($response);

        }
      


    }else if($_POST['getServiceProviderUserMapData']=="getServiceProviderUserMapData" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){
      if($user_latitude!='' && $user_longitude){

       

         $local_service_providerq=$d->select("local_service_provider_users,local_service_provider_users_category,local_service_provider_master","local_service_provider_master.local_service_provider_id=local_service_provider_users_category.category_id AND local_service_provider_users_category.service_provider_users_id=local_service_provider_users.service_provider_users_id AND  local_service_provider_users.service_provider_status=0 AND local_service_provider_users.service_provider_delete_status=0","");
       


        if(mysqli_num_rows($local_service_providerq)>0){

          $response["local_service_provider"] = array();


          while($data_local_service_provider=mysqli_fetch_array($local_service_providerq)) {

            $local_service_provider = array();
            $local_service_provider["service_provider_users_id"]=$data_local_service_provider['service_provider_users_id'];
            $local_service_provider["service_provider_name"]=html_entity_decode($data_local_service_provider['service_provider_name']);
            $local_service_provider["service_provider_address"]=$data_local_service_provider['service_provider_address'];
            $local_service_provider["service_provider_latitude"]=$data_local_service_provider['service_provider_latitude'];
            $local_service_provider["service_provider_logitude"]=$data_local_service_provider['service_provider_logitude'];
            $local_service_provider["service_provider_phone"]=$data_local_service_provider['service_provider_phone'];
            $local_service_provider["service_provider_email"]=$data_local_service_provider['service_provider_email'];
            $local_service_provider["sp_webiste"]=$data_local_service_provider['sp_webiste'];
            $local_service_provider["work_description"]=html_entity_decode($data_local_service_provider['work_description']);
            $local_service_provider["is_kyc"]=$data_local_service_provider['kyc_status'];
            $currenttime = date('h:i');
            if($currenttime< date('h:i', strtotime($data_local_service_provider['open_time'])) && $currenttime> date('h:i', strtotime($data_local_service_provider['close_time']))){ $openstatus = "Closed"; }else{ $openstatus = "Open"; }
            $local_service_provider["openStatus"]=($data_local_service_provider['open_time']!='') ? $openstatus : '';
            $local_service_provider["timing"]=($data_local_service_provider['open_time']!='') ? $data_local_service_provider['open_time'].' to '.$data_local_service_provider['close_time'] : '';
            $local_service_provider["service_provider_user_image"]=$base_url."img/local_service_provider/local_service_users/".$data_local_service_provider['service_provider_user_image'];

            $local_service_provider_id = $data_local_service_provider['service_provider_users_id'];
            /**************************** Average Rating ************************************/
            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND society_id='$society_id' AND status=0");
            $average = 0;
            while ( $avg = mysqli_fetch_array($data)) {
              $average = $average + $avg['rating_point'];
            }
            $totalRating = mysqli_num_rows($data);
            if($totalRating>0){
              $averagerating = $average/$totalRating;
              $local_service_provider['totalRatings'] = number_format($totalRating);
              $local_service_provider['averageRating'] = number_format($averagerating,1);
            }else{
              $local_service_provider['totalRatings'] = '0';
              $local_service_provider['averageRating'] = '0';          
            }
            /**************************** Average Rating ************************************/
            /**************************** Previous Rated Rating *****************************/
            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND society_id='$society_id' AND user_id='$user_id' AND status=0");
            if(mysqli_num_rows($data)>0){
              $userdata = mysqli_fetch_array($data);
              $local_service_provider['userPreviousRating'] = $userdata['rating_point'];
              $local_service_provider['userPreviousComment'] = $userdata['rating_comment'];
            }else{
              $local_service_provider["userPreviousRating"]="";
              $local_service_provider["userPreviousComment"]="";
            }
            /**************************** Previous Rated Rating *****************************/
            /**************************** Distance Calculation ******************************/
            $service_provider_latitude = $data_local_service_provider['service_provider_latitude'];
            $service_provider_logitude = $data_local_service_provider['service_provider_logitude'];

        
            $radiusInMeeter=  $d->haversineGreatCircleDistance($user_latitude,$user_longitude,$service_provider_latitude, $service_provider_logitude);

            $totalKm= number_format($radiusInMeeter/1000,2,'.','');


            $local_service_provider["distance_in_km"]=$totalKm. ' KM';
            /**************************** Distance Calculation ******************************/
            array_push($response["local_service_provider"], $local_service_provider); 

          } 

          $response["message"]="Get Local Service Providers Details successfully.";
          $response["status"]="200";
          echo json_encode($response);

        }else{

          $response["message"]="No Near By Service Provider Found";
          $response["status"]="201";
          echo json_encode($response);

        }
      }else {
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }


    }else if($_POST['getVendorDetails']=="getVendorDetails" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true){
      if($user_latitude!='' && $user_longitude){

        

       
        $local_service_providerq=$d->select("local_service_provider_users,local_service_provider_users_category","local_service_provider_users_category.service_provider_users_id=local_service_provider_users.service_provider_users_id AND local_service_provider_users.service_provider_users_id='$service_provider_users_id' AND  local_service_provider_users.service_provider_status=0 ","");

        if(mysqli_num_rows($local_service_providerq)>0){

          $response["local_service_provider"] = array();


          while($data_local_service_provider=mysqli_fetch_array($local_service_providerq)) {

            $local_service_provider = array();
            $local_service_provider["service_provider_users_id"]=$data_local_service_provider['service_provider_users_id'];
            $local_service_provider["service_provider_name"]=html_entity_decode($data_local_service_provider['service_provider_name']);
            $local_service_provider["service_provider_address"]=$data_local_service_provider['service_provider_address'];
            $local_service_provider["service_provider_latitude"]=$data_local_service_provider['service_provider_latitude'];
            $local_service_provider["service_provider_logitude"]=$data_local_service_provider['service_provider_logitude'];
            $local_service_provider["service_provider_phone"]=$data_local_service_provider['service_provider_phone'];
            $local_service_provider["service_provider_phone_view"]=$data_local_service_provider['country_code'].' '.$data_local_service_provider['service_provider_phone'];
            $local_service_provider["service_provider_email"]=$data_local_service_provider['service_provider_email'];
            $local_service_provider["sp_webiste"]=$data_local_service_provider['sp_webiste'];
            $local_service_provider["work_description"]=html_entity_decode($data_local_service_provider['work_description']);
            $local_service_provider["is_kyc"]=$data_local_service_provider['kyc_status'];
            $currenttime = date('h:i');
            if($currenttime< date('h:i', strtotime($data_local_service_provider['open_time'])) && $currenttime> date('h:i', strtotime($data_local_service_provider['close_time']))){ $openstatus = "Closed"; }else{ $openstatus = "Open"; }
            $local_service_provider["openStatus"]=($data_local_service_provider['open_time']!='') ? $openstatus : '';
            
            $startTime  = date('h:i A', strtotime($data_local_service_provider['open_time']));
            $closeTime  = date('h:i A', strtotime($data_local_service_provider['close_time']));

            $local_service_provider["timing"]=($data_local_service_provider['open_time']!='') ? $startTime.' to '.$closeTime : '';
            $local_service_provider["service_provider_user_image"]=$base_url."img/local_service_provider/local_service_users/".$data_local_service_provider['service_provider_user_image'];

            if ($data_local_service_provider['brochure_profile']!="") {
              $local_service_provider["brochure_profile"]=$base_url."img/local_service_provider/local_service_location_proof/".$data_local_service_provider['brochure_profile'];
            } else {
              $local_service_provider["brochure_profile"]="" ;
            }

            $local_service_provider_id = $data_local_service_provider['service_provider_users_id'];
            /**************************** Average Rating ************************************/
            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND society_id='$society_id' AND status=0");
            $average = 0;
            while ( $avg = mysqli_fetch_array($data)) {
              $average = $average + $avg['rating_point'];
            }
            $totalRating = mysqli_num_rows($data);
            if($totalRating>0){
              $averagerating = $average/$totalRating;
              $local_service_provider['totalRatings'] = number_format($totalRating);
              $local_service_provider['averageRating'] = number_format($averagerating,1);
            }else{
              $local_service_provider['totalRatings'] = '0';
              $local_service_provider['averageRating'] = '0';          
            }
            /**************************** Average Rating ************************************/
            /**************************** Previous Rated Rating *****************************/
            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND society_id='$society_id' AND user_id='$user_id' AND status=0");
            if(mysqli_num_rows($data)>0){
              $userdata = mysqli_fetch_array($data);
              $local_service_provider['userPreviousRating'] = $userdata['rating_point'];
              $local_service_provider['userPreviousComment'] = $userdata['rating_comment'];
            }else{
              $local_service_provider["userPreviousRating"]="";
              $local_service_provider["userPreviousComment"]="";
            }
            /**************************** Previous Rated Rating *****************************/
            /**************************** Distance Calculation ******************************/
            $service_provider_latitude = $data_local_service_provider['service_provider_latitude'];
            $service_provider_logitude = $data_local_service_provider['service_provider_logitude'];

        
            $radiusInMeeter=  $d->haversineGreatCircleDistance($user_latitude,$user_longitude,$service_provider_latitude, $service_provider_logitude);

            $totalKm= number_format($radiusInMeeter/1000,2,'.','');


            $local_service_provider["distance_in_km"]=$totalKm. ' KM';
            /**************************** Distance Calculation ******************************/
            array_push($response["local_service_provider"], $local_service_provider); 

          } 

          $response["message"]="Get Local Service Providers Details successfully.";
          $response["status"]="200";
          echo json_encode($response);

        }else{

          $response["message"]="No Near By Service Provider Found";
          $response["status"]="201";
          echo json_encode($response);

        }
      }else {
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }


    }else if($_POST['addCallRequest']=="addCallRequest" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){


      $spAray = explode(",",$local_service_provider_users_id);
      $totalCount= count($spAray);
      for ($i=0; $i <$totalCount ; $i++) { 

        
        $getToken= $d->select("local_service_provider_users","service_provider_users_id='$spAray[$i]' AND token!='' AND service_provider_status=0");
        $fcmData=mysqli_fetch_array($getToken);
        $token = $fcmData['token'];
        $title = "Call Back Request From $user_name";
        $description = "Call on $user_mobile";

        $m->set_data('society_id',$society_id);
        $m->set_data('local_service_provider_users_id',$spAray[$i]);
        $m->set_data('user_id',$user_id);
        $m->set_data('user_mobile',$user_mobile);
        $m->set_data('country_code',$country_code);
        $m->set_data('user_name',$user_name);
        $m->set_data('notification_title',$title);
        $m->set_data('notification_description',$description);

        $a1 =array(
          'local_service_provider_users_id'=> $m->get_data('local_service_provider_users_id'),
          'user_id'=> $m->get_data('user_id'),
          'user_mobile'=> $m->get_data('user_mobile'),
          'country_code'=> $m->get_data('country_code'),
          'user_name'=> $m->get_data('user_name'),
          'society_id'=> $m->get_data('society_id'),
          'request_date'=> date("Y-m-d H:i:s"),
        );

        $q =$d->insert("local_serviceproviders_call_request",$a1);
        $call_request_id = $con->insert_id;

         $aNoti =array(
          'service_provider_users_id'=> $m->get_data('local_service_provider_users_id'),
          'notification_title'=> $m->get_data('notification_title'),
          'notification_description'=> $m->get_data('notification_description'),
          'notification_date'=> date("Y-m-d H:i:s"),
          'menu_click'=>'call_request'
        );

        $q1 =$d->insert("local_service_provider_notifications",$aNoti);

        $nResident->noti("call_request","",'0',$token,$title,$description,"$call_request_id");


      }
      if ($q>0) {
        if ($totalCount==1) {
          $response["message"]="Call Request Send to Service Providers";
        } else {
          $response["message"]="Call Request Send to $totalCount Service Providers";
        }
        $response["status"]="200";
        echo json_encode($response);
      } else {
        $response["message"]="Something Wrong.";
        $response["status"]="201";
        echo json_encode($response);
      }

    }else if($_POST['addReview']=="addReview" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){
      if($society_id!='' && $local_service_provider_id!='' && $user_id!='' && $user_mobile!='' && $user_name!='' && $rating_point!='' ){
        $countdata = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND society_id='$society_id' AND user_id='$user_id' ");
        if(mysqli_num_rows($countdata)==0){


          $getToken= $d->select("local_service_provider_users","service_provider_users_id='$local_service_provider_id' AND token!='' AND service_provider_status=0");
          $fcmData=mysqli_fetch_array($getToken);
          $token = $fcmData['token'];

          $title ="New Review Added";
          $description ="Added by $user_name";

          $m->set_data('society_id',$society_id);
          $m->set_data('local_service_provider_id',$local_service_provider_id);
          $m->set_data('user_id',$user_id);
          $m->set_data('user_mobile',$user_mobile);
          $m->set_data('country_code',$country_code);
          $m->set_data('user_name',$user_name);
          $m->set_data('rating_point',$rating_point);
          $m->set_data('rating_comment',$rating_comment);
          $m->set_data('rating_date',date('Y-m-d h:i:sa'));

          $a1 =array(
            'local_service_provider_id'=> $m->get_data('local_service_provider_id'),
            'user_id'=> $m->get_data('user_id'),
            'user_mobile'=> $m->get_data('user_mobile'),
            'country_code'=> $m->get_data('country_code'),
            'user_name'=> $m->get_data('user_name'),
            'society_id'=> $m->get_data('society_id'),
            'rating_point'=> $m->get_data('rating_point'),
            'rating_comment'=> $m->get_data('rating_comment'),
            'rating_date'=> $m->get_data('rating_date'),
          );

          $q=$d->insert("local_service_providers_ratings",$a1);
          if ($q>0) {

             $nResident->noti("rating","",'0',$token,$title,$description,"");


            $response["message"]="Thank you for your review..";
            $response["status"]="200";
            echo json_encode($response);
          } else {
            $response["message"]="Something Wrong.";
            $response["status"]="201";
            echo json_encode($response);
          }
        }else{

           $getToken= $d->select("local_service_provider_users","service_provider_users_id='$local_service_provider_id' AND token!='' AND service_provider_status=0");
          $fcmData=mysqli_fetch_array($getToken);
          $token = $fcmData['token'];

          $title ="New Review Added";
          $description ="Added by $user_name";

          $m->set_data('user_mobile',$user_mobile);
          $m->set_data('country_code',$country_code);
          $m->set_data('user_name',$user_name);
          $m->set_data('rating_point',$rating_point);
          $m->set_data('rating_comment',$rating_comment);
          $m->set_data('rating_updated_date',date('Y-m-d h:i:sa'));
          $m->set_data('status',0);

          $a1 =array(
            'user_mobile'=> $m->get_data('user_mobile'),
            'country_code'=> $m->get_data('country_code'),
            'user_name'=> $m->get_data('user_name'),
            'rating_point'=> $m->get_data('rating_point'),
            'rating_comment'=> $m->get_data('rating_comment'),
            'rating_updated_date'=> $m->get_data('rating_updated_date'),
            'status'=> $m->get_data('status'),
          );

          $q=$d->update("local_service_providers_ratings",$a1,"user_id='$user_id' AND society_id='$society_id' AND local_service_provider_id='$local_service_provider_id'");
          if ($q>0) {

            $nResident->noti("rating","",'0',$token,$title,$description,"");


            $response["message"]="Thank you for your review.";
            $response["status"]="200";
            echo json_encode($response);
          } else {
            $response["message"]="Something Wrong.";
            $response["status"]="201";
            echo json_encode($response);
          }
        }
      }else {
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else if($_POST['addComplain']=="addComplain" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true){
      if($society_id!='' && $local_service_provider_id!='' && $user_id!='' && $user_mobile!='' && $user_name!='' && $comment ){

        $getToken= $d->select("local_service_provider_users","service_provider_users_id='$local_service_provider_id' AND token!='' AND service_provider_status=0");
          $fcmData=mysqli_fetch_array($getToken);
          $token = $fcmData['token'];

          $title ="New Complain Added";
          $description ="Added by $user_name ($user_mobile)";

        $m->set_data('society_id',$society_id);
        $m->set_data('local_service_provider_id',$local_service_provider_id);
        $m->set_data('user_id',$user_id);
        $m->set_data('user_mobile',$user_mobile);
        $m->set_data('country_code',$country_code);
        $m->set_data('user_name',$user_name);
        $m->set_data('comment',$comment);
        $m->set_data('complain_date',date('Y-m-d h:i:sa'));

        $a1 =array(
          'local_service_provider_id'=> $m->get_data('local_service_provider_id'),
          'user_id'=> $m->get_data('user_id'),
          'user_mobile'=> $m->get_data('user_mobile'),
          'country_code'=> $m->get_data('country_code'),
          'user_name'=> $m->get_data('user_name'),
          'society_id'=> $m->get_data('society_id'),
          'comment'=> $m->get_data('comment'),
          'complain_date'=> $m->get_data('complain_date'),
          // 'menu_click'=> "complain",
        );

        $q=$d->insert("local_service_provider_complains",$a1);
        if ($q>0) {

           $nResident->noti("complain","",'0',$token,$title,$description,"");


          $response["message"]="Complain Added Successfully.";
          $response["status"]="200";
          echo json_encode($response);
        } else {
          $response["message"]="Something Wrong.";
          $response["status"]="201";
          echo json_encode($response);
        }
      }else {
        $response["message"]="Please Complete All Mandatory Fields.";
        $response["status"]="201";
        echo json_encode($response);
      }
    }else {
      $response["message"]="wrong tag.";
      $response["status"]="201";
      echo json_encode($response);

    }
  } else {
    $response["message"] = "wrong api key.";
    $response["status"] = "201";
    echo json_encode($response);
  }
}?>