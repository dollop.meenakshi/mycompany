<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb && $auth_check=='true') {
        $response = array();
        extract(array_map("test_input", $_POST));
        $date = date('Y-m-d H:i:s');

        if ($_POST['getPrvChat'] == "getPrvChat" && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_id != '' && $unit_id != 0) {
                $quc = $d->select("unit_master", "unit_id='$unit_id' AND chat_access_denied=1");
                if (mysqli_num_rows($quc) > 0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            if ($group_id != '') {
                $qn = $d->select("chat_master_group", "society_id='$society_id' AND group_id='$group_id'", "ORDER BY chat_id ASC limit 1500");

                if ($read_flag == '1') {

                    $qf1 = $d->select("chat_group_member_master", "group_id='$group_id' AND society_id='$society_id' AND  user_id='$user_id'");
                    $oldData = mysqli_fetch_array($qf1);

                    $m->set_data('unread_count', $unread_count + $oldData['unread_count']);

                    $a = array(
                        'unread_count' => $m->get_data('unread_count'),
                    );

                    $q = $d->update("chat_group_member_master", $a, "user_id='$user_id' AND society_id='$society_id' AND group_id='$group_id'");
                }
            } else {
                $qn = $d->select("chat_master_group", "society_id='$society_id' ", "ORDER BY chat_id ASC limit 1500");
            }




            if (mysqli_num_rows($qn) > 0) {
                $response["chat"] = array();

                while ($data_notification = mysqli_fetch_array($qn)) {
                    // print_r($data_notification);

                    $chat = array();
                    $chat["chat_id"] = $data_notification['chat_id'];
                    $chat_id = $data_notification['chat_id'];
                    $chat["society_id"] = $data_notification['society_id'];
                    $chat["msg_by"] = $data_notification['msg_by'];
                    $chat["msg_for"] = $data_notification['msg_by_name'];
                    $chat["msg_data"] = $data_notification['msg_data'];
                    $chat["msg_status"] = $data_notification['msg_status'];
                    $chat["msg_delete"] = $data_notification['msg_delete'];
                    $chat["msg_date"] = date("d M Y h:i A", strtotime($data_notification['msg_date']));

                    if ($data_notification['msg_by'] == $user_id) {

                        $chat["my_msg"] = '1';
                    } else if ($data_notification['msg_by'] == 0) {
                        $chat["my_msg"] = '2';
                    } else {
                        $chat["my_msg"] = '0';

                        $m->set_data('msg_status', '1');

                        $a1 = array(
                            'msg_status' => $m->get_data('msg_status')
                        );

                        $d->update('chat_master', $a1, "chat_id='$chat_id'");
                    }

                    array_push($response["chat"], $chat);
                }
                if ($read_flag == '0') {



                    $m->set_data('unread_count', count($response["chat"]));

                    $a = array(
                        'unread_count' => $m->get_data('unread_count'),
                    );

                    $q = $d->update("chat_group_member_master", $a, "user_id='$user_id' AND society_id='$society_id' AND group_id='$group_id'");
                }

                $response["message"] = "Get Group Chat Success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No Chat Found in this Group.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['getPrvChatNew'] == "getPrvChatNew" && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_id != '' && $unit_id != 0) {
                $quc = $d->select("unit_master", "unit_id='$unit_id' AND chat_access_denied=1");
                if (mysqli_num_rows($quc) > 0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            if ($group_id != '') {
                $qn = $d->select("chat_master_group,users_master,unit_master,block_master", "block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND users_master.user_id=chat_master_group.msg_by AND  chat_master_group.society_id='$society_id' AND chat_master_group.group_id='$group_id'", "ORDER BY chat_master_group.chat_id ASC limit 5000");

                if ($read_flag == '1') {

                    $qf1 = $d->select("chat_group_member_master", "group_id='$group_id' AND society_id='$society_id' AND  user_id='$user_id'");
                    $oldData = mysqli_fetch_array($qf1);

                    $m->set_data('unread_count', $unread_count + $oldData['unread_count']);

                    $a = array(
                        'unread_count' => $m->get_data('unread_count'),
                    );

                    $q = $d->update("chat_group_member_master", $a, "user_id='$user_id' AND society_id='$society_id' AND group_id='$group_id'");
                }
            } else {
                $qn = $d->select("chat_master_group", "society_id='$society_id' ", "ORDER BY chat_id ASC limit 1500");
            }




            if (mysqli_num_rows($qn) > 0) {
                $response["chat"] = array();

                $tempPrvDate = "";
                while ($data_notification = mysqli_fetch_array($qn)) {
                    // print_r($data_notification);

                    $chat = array();
                    $chat["chat_id"] = $data_notification['chat_id'];
                    $chat["chat_id_reply"] = $data_notification['chat_id_reply'];
                    if ($data_notification['chat_id_reply']!=0) {
                        $qs=$d->selectRow("msg_data,msg_img,msg_type,msg_by_name","chat_master_group","chat_id='$data_notification[chat_id_reply]'");
                        $replyData=mysqli_fetch_array($qs);
                        $chat["reply_msg"] = $replyData['msg_data'].'';
                        $chat["reply_msg_img"] = $base_url."img/chatImg/".$replyData['msg_img'];
                        $chat["reply_msg_type"] = $replyData['msg_type'].'';
                        $chat["reply_user_name"] = $replyData['msg_by_name'].'';
                    } else {
                        $chat["reply_msg"] = "";
                        $chat["reply_msg_img"]="";
                        $chat["reply_msg_type"]="";
                        $chat["reply_user_name"]="";
                    }
                    $chat_id = $data_notification['chat_id'];
                    $chat["society_id"] = $data_notification['society_id'];
                    $chat["msg_by"] = $data_notification['msg_by'];
                    $chat["msg_for"] = $data_notification['user_full_name'].' ('.$data_notification['block_name'].'-'.$data_notification['unit_name'].')';
                    $chat["msg_data"] = $data_notification['msg_data'];
                    $chat["msg_status"] = $data_notification['msg_status'];
                    $chat["msg_delete"] = $data_notification['msg_delete'];
                    $chat["msg_type"] = $data_notification['msg_type'];
                    $chat["msg_date"] = date("h:i A", strtotime($data_notification['msg_date']));
                     $chatDate = date("d M Y", strtotime($data_notification['msg_date']));
                    $chat["msg_date_view"] = $chatDate;

                    $chat["isDate"] = false;
                    if ($data_notification['msg_img']!='') {
                        $chat["msg_img"] = $base_url."img/chatImg/".$data_notification['msg_img'];
                    } else {
                        $chat["msg_img"] = "";
                    }

                    $chat["file_original_name"] = $data_notification['file_original_name'].'';
                    $chat["location_lat_long"] = $data_notification['location_lat_long'].'';
                    $chat["file_size"] = $data_notification['file_size'].'';
                    $chat["file_duration"] = $data_notification['file_duration'].'';

                    if ($data_notification['msg_by'] == $user_id && $data_notification['msg_by_name'] !='') {
                        
                        $chat["my_msg"] = '1';
                    } else if ($data_notification['msg_by_name'] =='') {
                        $chat["my_msg"] = '2';
                    } else {
                        $chat["my_msg"] = '0';

                        $m->set_data('msg_status', '1');

                        $a1 = array(
                            'msg_status' => $m->get_data('msg_status')
                        );

                        $d->update('chat_master', $a1, "chat_id='$chat_id'");
                    }

                    if($tempPrvDate!=""){

                        if($tempPrvDate != $chatDate){
                            $tempPrvDate = $chatDate;
                            $chatD = array();
                            $chatD["msg_date"] = $tempPrvDate;
                            $chatD["isDate"] = true;
                            $chatD["msg_date_view"] = $chatDate;

                            array_push($response["chat"], $chatD);
                        }

                    }else{
                        $tempPrvDate = $chatDate;
                        $chatD = array();
                        $chatD["msg_date"] = $tempPrvDate;
                        $chatD["isDate"] = true;
                        $chatD["msg_date_view"] = $chatDate;
                        array_push($response["chat"], $chatD);
                    }

                    array_push($response["chat"], $chat);
                }
                if ($read_flag == '0') {



                    $m->set_data('unread_count', count($response["chat"]));

                    $a = array(
                        'unread_count' => $m->get_data('unread_count'),
                    );

                    $q = $d->update("chat_group_member_master", $a, "user_id='$user_id' AND society_id='$society_id' AND group_id='$group_id'");
                }

                $response["message"] = "Get Group Chat Success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No Chat Found in this Group.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getPrvChatNewDesc'] == "getPrvChatNewDesc" && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_id != '' && $unit_id != 0) {
                $quc = $d->select("unit_master", "unit_id='$unit_id' AND chat_access_denied=1");
                if (mysqli_num_rows($quc) > 0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            if ($group_id != '') {
                $qn = $d->select("chat_master_group,users_master,unit_master,block_master", "block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND users_master.user_id=chat_master_group.msg_by AND  chat_master_group.society_id='$society_id' AND chat_master_group.group_id='$group_id'", "ORDER BY chat_master_group.chat_id DESC limit 5000");

                if ($read_flag == '1') {

                    $qf1 = $d->select("chat_group_member_master", "group_id='$group_id' AND society_id='$society_id' AND  user_id='$user_id'");
                    $oldData = mysqli_fetch_array($qf1);

                    $m->set_data('unread_count', $unread_count + $oldData['unread_count']);

                    $a = array(
                        'unread_count' => $m->get_data('unread_count'),
                    );

                    $q = $d->update("chat_group_member_master", $a, "user_id='$user_id' AND society_id='$society_id' AND group_id='$group_id'");
                }
            } else {
                $qn = $d->select("chat_master_group", "society_id='$society_id' ", "ORDER BY chat_id ASC limit 1500");
            }




            if (mysqli_num_rows($qn) > 0) {
                $response["chat"] = array();

                $tempPrvDate = "";
                
                while ($data_notification = mysqli_fetch_array($qn)) {
                    // print_r($data_notification);

                    $chat = array();
                    $chat["chat_id"] = $data_notification['chat_id'];
                    $chat["chat_id_reply"] = $data_notification['chat_id_reply'];
                    if ($data_notification['chat_id_reply']!=0) {
                        $qs=$d->selectRow("msg_data,msg_img,msg_type,msg_by_name","chat_master_group","chat_id='$data_notification[chat_id_reply]'");
                        $replyData=mysqli_fetch_array($qs);
                        $chat["reply_msg"] = $replyData['msg_data'].'';
                        $chat["reply_msg_img"] = $base_url."img/chatImg/".$replyData['msg_img'];
                        $chat["reply_msg_type"] = $replyData['msg_type'].'';
                        $chat["reply_user_name"] = $replyData['msg_by_name'].'';
                    } else {
                        $chat["reply_msg"] = "";
                        $chat["reply_msg_img"]="";
                        $chat["reply_msg_type"]="";
                        $chat["reply_user_name"]="";
                    }
                    $chat_id = $data_notification['chat_id'];
                    $chat["society_id"] = $data_notification['society_id'];
                    $chat["msg_by"] = $data_notification['msg_by'];
                    $chat["msg_for"] = $data_notification['user_full_name'].' ('.$data_notification['user_designation'].')';
                    $chat["msg_data"] = $data_notification['msg_data'];
                    $chat["msg_status"] = $data_notification['msg_status'];
                    $chat["msg_delete"] = $data_notification['msg_delete'];
                    $chat["msg_type"] = $data_notification['msg_type'];
                    $chat["msg_date"] = date("h:i A", strtotime($data_notification['msg_date']));
                     $chatDate = date("d M Y", strtotime($data_notification['msg_date']));
                    $chat["msg_date_view"] = $chatDate;

                    $chat["isDate"] = false;
                    if ($data_notification['msg_img']!='') {
                        $chat["msg_img"] = $base_url."img/chatImg/".$data_notification['msg_img'];
                    } else {
                        $chat["msg_img"] = "";
                    }

                    $chat["file_original_name"] = $data_notification['file_original_name'].'';
                    $chat["location_lat_long"] = $data_notification['location_lat_long'].'';
                    $chat["file_size"] = $data_notification['file_size'].'';
                    $chat["file_duration"] = $data_notification['file_duration'].'';

                    if ($data_notification['msg_by'] == $user_id && $data_notification['msg_by_name'] !='') {
                        
                        $chat["my_msg"] = '1';
                    } else if ($data_notification['msg_by_name'] =='') {
                        $chat["my_msg"] = '2';
                    } else {
                        $chat["my_msg"] = '0';

                        $m->set_data('msg_status', '1');

                        $a1 = array(
                            'msg_status' => $m->get_data('msg_status')
                        );

                        $d->update('chat_master', $a1, "chat_id='$chat_id'");
                    }

                    if ($tempPrvDate=='') {
                      $tempPrvDate = $chatDate;
                      $chatD["msg_date"] = $tempPrvDate;
                      $chatD["isDate"] = false;
                      $chatD["msg_date_view"] = $chatDate;
                      // array_push($response["chat"], $chatD);

                    } else if ($tempPrvDate==$chatDate) {
                      $tempPrvDate = $chatDate;
                      $chatD["msg_date"] = $tempPrvDate;
                      $chatD["isDate"] = false;
                      $chatD["msg_date_view"] = $chatDate;
                      // array_push($response["chat"], $chatD);
                    }  else {
                        $chatD = array();
                        $chatD["msg_date"] = $tempPrvDate;
                        $chatD["isDate"] = true;
                        $chatD["msg_date_view"] = $tempPrvDate;
                        array_push($response["chat"], $chatD);
                        $tempPrvDate = $chatDate;
                    }

                    

                    array_push($response["chat"], $chat);
                }
                if ($read_flag == '0') {



                    $m->set_data('unread_count', count($response["chat"]));

                    $a = array(
                        'unread_count' => $m->get_data('unread_count'),
                    );

                    $q = $d->update("chat_group_member_master", $a, "user_id='$user_id' AND society_id='$society_id' AND group_id='$group_id'");
                }

                $response["message"] = "Get Group Chat Success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No Chat Found in this Group.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['getPrvChatNewDescNew'] == "getPrvChatNewDescNew" && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_id != '' && $unit_id != 0) {
                $quc = $d->select("unit_master", "unit_id='$unit_id' AND chat_access_denied=1");
                if (mysqli_num_rows($quc) > 0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $qs=$d->selectRow("msg_data,msg_img,msg_type,msg_by_name","chat_master_group","chat_id='$data_notification[chat_id_reply]'");

                $qn = $d->selectRow("chat_master_group.*,users_master.user_full_name,users_master.user_designation","chat_master_group,users_master,unit_master,block_master", "block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND users_master.user_id=chat_master_group.msg_by AND  chat_master_group.society_id='$society_id' AND chat_master_group.group_id='$group_id'", "ORDER BY chat_master_group.chat_id DESC limit 5000");

                // Unread message calculation

                if ($read_flag == '1') {

                    $qf1 = $d->select("chat_group_unread_message", "group_id='$group_id' AND society_id='$society_id' AND  user_id='$user_id'");
                    $oldData = mysqli_fetch_array($qf1);

                    if (mysqli_num_rows($qf1) == 0) {
                        $m->set_data('unread_count', $unread_count);
                        $a = array(
                            'unread_count' => $m->get_data('unread_count'),
                            'user_id' => $user_id,
                            'group_id' => $group_id,
                            'society_id' => $society_id,
                            'last_update_time' => date('Y-m-d H:i:s'),
                        );

                        $q = $d->insert("chat_group_unread_message", $a);
                    }else{

                        $m->set_data('unread_count', $unread_count + $oldData['unread_count']);
                        $a = array(
                            'unread_count' => $m->get_data('unread_count'),
                            'last_update_time' => date('Y-m-d H:i:s'),
                        );

                        $q = $d->update("chat_group_unread_message", $a, "user_id='$user_id' AND society_id='$society_id' AND group_id='$group_id'");
                    }

                }
            




            if (mysqli_num_rows($qn) > 0) {
                $response["chat"] = array();

                $tempPrvDate = "";
                
                while ($data_notification = mysqli_fetch_array($qn)) {
                    // print_r($data_notification);

                    $chat = array();
                    $chat["chat_id"] = $data_notification['chat_id'];
                    $chat["chat_id_reply"] = $data_notification['chat_id_reply'];
                    if ($data_notification['chat_id_reply']!=0) {
                        $chat["reply_msg"] = $data_notification['reply_msg'].'';
                        if ($data_notification['reply_msg_img'] !='') {
                            $chat["reply_msg_img"] = $base_url."img/chatImg/".$data_notification['reply_msg_img'].'';
                        }else{
                            $chat["reply_msg_img"] = "";                        
                        }
                        $chat["reply_msg_type"] = $data_notification['reply_msg_type'].'';
                        $chat["reply_user_name"] = $data_notification['reply_user_name'].'';
                    } else {
                        $chat["reply_msg"] = "";
                        $chat["reply_msg_img"]="";
                        $chat["reply_msg_type"]="";
                        $chat["reply_user_name"]="";
                    }
                    $chat_id = $data_notification['chat_id'];
                    $chat["society_id"] = $data_notification['society_id'];
                    $chat["msg_by"] = $data_notification['msg_by'];
                    $chat["msg_for"] = $data_notification['user_full_name'].' ('.$data_notification['user_designation'].')';
                    $chat["msg_data"] = $data_notification['msg_data'];
                    $chat["msg_status"] = $data_notification['msg_status'];
                    $chat["msg_delete"] = $data_notification['msg_delete'];
                    $chat["msg_type"] = $data_notification['msg_type'];
                    $chat["msg_date"] = date("h:i A", strtotime($data_notification['msg_date']));
                     $chatDate = date("d M Y", strtotime($data_notification['msg_date']));
                    $chat["msg_date_view"] = $chatDate;

                    $chat["isDate"] = false;
                    if ($data_notification['msg_img']!='') {
                        $chat["msg_img"] = $base_url."img/chatImg/".$data_notification['msg_img'];
                    } else {
                        $chat["msg_img"] = "";
                    }

                    $chat["file_original_name"] = $data_notification['file_original_name'].'';
                    $chat["location_lat_long"] = $data_notification['location_lat_long'].'';
                    $chat["file_size"] = $data_notification['file_size'].'';
                    $chat["file_duration"] = $data_notification['file_duration'].'';

                    if ($data_notification['msg_by'] == $user_id && $data_notification['msg_by_name'] !='') {
                        
                        $chat["my_msg"] = '1';
                    } else if ($data_notification['msg_by_name'] =='') {
                        $chat["my_msg"] = '2';
                    } else {
                        $chat["my_msg"] = '0';

                        $m->set_data('msg_status', '1');

                        $a1 = array(
                            'msg_status' => $m->get_data('msg_status')
                        );

                        $d->update('chat_master', $a1, "chat_id='$chat_id'");
                    }

                    if ($tempPrvDate=='') {
                      $tempPrvDate = $chatDate;
                      $chatD["msg_date"] = $tempPrvDate;
                      $chatD["isDate"] = false;
                      $chatD["msg_date_view"] = $chatDate;
                      // array_push($response["chat"], $chatD);

                    } else if ($tempPrvDate==$chatDate) {
                      $tempPrvDate = $chatDate;
                      $chatD["msg_date"] = $tempPrvDate;
                      $chatD["isDate"] = false;
                      $chatD["msg_date_view"] = $chatDate;
                      // array_push($response["chat"], $chatD);
                    }  else {
                        $chatD = array();
                        $chatD["msg_date"] = $tempPrvDate;
                        $chatD["isDate"] = true;
                        $chatD["msg_date_view"] = $tempPrvDate;
                        array_push($response["chat"], $chatD);
                        $tempPrvDate = $chatDate;
                    }

                    

                    array_push($response["chat"], $chat);
                }
                if ($read_flag == '0') {



                    $m->set_data('unread_count', count($response["chat"]));

                    $a = array(
                        'unread_count' => $m->get_data('unread_count'),
                        'last_update_time' => date('Y-m-d H:i:s'),
                    );

                    $q = $d->update("chat_group_unread_message", $a, "user_id='$user_id' AND society_id='$society_id' AND group_id='$group_id'");
                }

                $response["message"] = "Get Group Chat Success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No Chat Found in this Group.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }else if ($_POST['addChat'] == "addChat" && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_id != '' && $unit_id != 0) {
                $quc = $d->select("unit_master", "unit_id='$unit_id' AND chat_access_denied=1");
                if (mysqli_num_rows($quc) > 0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            if ($msg_data == '') {
                $response["message"] = "Enter message";
                $response["status"] = "201";
                exit();
            }

            $msg_data = html_entity_decode($msg_data);


            $m->set_data('chat_id_reply', $chat_id_reply);
            $m->set_data('society_id', $society_id);
            $m->set_data('group_id', $group_id);
            $m->set_data('msg_by', $msg_by);
            $m->set_data('msg_for', $msg_for);
            $m->set_data('msg_data', $msg_data);
            $m->set_data('msg_status', '0');
            $m->set_data('send_by', '0');
            $m->set_data('sent_to', $sent_to);
            $m->set_data('msg_date', $date);
            $a1 = array(
                 'chat_id_reply' => $m->get_data('chat_id_reply'),
                'society_id' => $m->get_data('society_id'),
                'group_id' => $m->get_data('group_id'),
                'msg_by' => $m->get_data('msg_by'),
                'msg_by_name' => $m->get_data('msg_for'),
                'msg_data' => $m->get_data('msg_data'),
                'msg_status' => $m->get_data('msg_status'),
                'send_by' => $m->get_data('send_by'),
                'sent_to' => $m->get_data('sent_to'),
                'msg_date' => $m->get_data('msg_date')
            );

            $q = $d->insert('chat_master_group', $a1);


            if ($q == true) {


                $fcmArray = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND chat_group_member_master.society_id='$society_id' AND users_master.device='android' AND chat_group_member_master.group_id='$group_id' AND chat_group_member_master.user_id!='$msg_by'");

                $fcmArrayIos = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND users_master.society_id='$society_id' AND users_master.device='ios' AND chat_group_member_master.group_id='$group_id' AND chat_group_member_master.user_id!='$msg_by'");

                $nResident->noti("", "", $society_id, $fcmArray, "$unit_name @ $group_name", $msg_data, "chatMsgGroup");
                $nResident->noti_ios("", "", $society_id, $fcmArrayIos, "$unit_name @ $group_name", $msg_data, "chatMsgGroup");

                $response["message"] = "Message Send";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Fail to Send.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['addChatWithImg'] == "addChatWithImg" && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_id != '' && $unit_id != 0) {
                $quc = $d->select("unit_master", "unit_id='$unit_id' AND chat_access_denied=1");
                if (mysqli_num_rows($quc) > 0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $total = count($_FILES['chat_img']['tmp_name']);
            for ($i = 0; $i < $total; $i++) {
                $uploadedFile = $_FILES['chat_img']['name'][$i];
                if ($uploadedFile != "") {
                    $temp = explode(".", $uploadedFile);
                    $feed_img = "Chat_" . round(microtime(true)) . $i . '.' . end($temp);
                    move_uploaded_file($_FILES['chat_img']['tmp_name'][$i], "../img/chatImg/" . $feed_img);

                    $m->set_data('chat_id_reply', $chat_id_reply);
                    $m->set_data('society_id', $society_id);
                    $m->set_data('group_id', $group_id);
                    $m->set_data('msg_by', $msg_by);
                    $m->set_data('msg_for', $msg_for);
                    $m->set_data('msg_data', $_POST['msg_data'][$i]);
                    $m->set_data('msg_status', '0');
                    $m->set_data('send_by', '0');
                    $m->set_data('sent_to', $sent_to);
                    $m->set_data('msg_date', $date);
                    $m->set_data('msg_type', "1");
                    $m->set_data('msg_img', $feed_img);

                    $a1 = array(
                        'chat_id_reply' => $m->get_data('chat_id_reply'),
                        'society_id' => $m->get_data('society_id'),
                        'group_id' => $m->get_data('group_id'),
                        'msg_by' => $m->get_data('msg_by'),
                        'msg_by_name' => $m->get_data('msg_for'),
                        'msg_data' => $m->get_data('msg_data'),
                        'msg_type' => $m->get_data('msg_type'),
                        'msg_img' => $m->get_data('msg_img'),
                        'msg_status' => $m->get_data('msg_status'),
                        'send_by' => $m->get_data('send_by'),
                        'sent_to' => $m->get_data('sent_to'),
                        'msg_date' => $m->get_data('msg_date'),
                        'msg_img' => $m->get_data('msg_img')
                    );

                    $q = $d->insert('chat_master_group', $a1);
                }
            }

            $fcmArray = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND chat_group_member_master.society_id='$society_id' AND users_master.device='android' AND chat_group_member_master.group_id='$group_id'");

            $fcmArrayIos = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND users_master.society_id='$society_id' AND users_master.device='ios' AND chat_group_member_master.group_id='$group_id'");

            $nResident->noti("", "", $society_id, $fcmArray, "$unit_name @ $group_name", "📸 Image", "chatMsgGroup");
            $nResident->noti_ios("", "", $society_id, $fcmArrayIos, "$unit_name @ $group_name", "📸 Image", "chatMsgGroup");

            $response["message"] = "Message Send";
            $response["status"] = "200";
            echo json_encode($response);
        }else if ($_POST['addChat'] == "addChatNew" && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_id != '' && $unit_id != 0) {
                $quc = $d->select("unit_master", "unit_id='$unit_id' AND chat_access_denied=1");
                if (mysqli_num_rows($quc) > 0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            if ($msg_data == '') {
                $response["message"] = "Enter message";
                $response["status"] = "201";
                exit();
            }

            $msg_data = html_entity_decode($msg_data);


            $m->set_data('chat_id_reply', $chat_id_reply);
            $m->set_data('society_id', $society_id);
            $m->set_data('group_id', $group_id);
            $m->set_data('msg_by', $msg_by);
            $m->set_data('msg_for', $msg_for);
            $m->set_data('msg_data', $msg_data);
            $m->set_data('msg_status', '0');
            $m->set_data('send_by', '0');
            $m->set_data('sent_to', $sent_to);
            $m->set_data('msg_type', $msgType);
            $m->set_data('msg_date', $date);
            $m->set_data('location_lat_long', $location_lat_long);
            $m->set_data('reply_msg', $reply_msg);
            $m->set_data('reply_msg_img', $reply_msg_img);
            $m->set_data('reply_msg_type', $reply_msg_type);
            $m->set_data('reply_user_name', $reply_user_name);
            
            $a1 = array(
                'chat_id_reply' => $m->get_data('chat_id_reply'),
                'society_id' => $m->get_data('society_id'),
                'group_id' => $m->get_data('group_id'),
                'msg_by' => $m->get_data('msg_by'),
                'msg_by_name' => $m->get_data('msg_for'),
                'msg_data' => $m->get_data('msg_data'),
                'msg_type' => $m->get_data('msg_type'),
                'location_lat_long' => $m->get_data('location_lat_long'),
                'msg_status' => $m->get_data('msg_status'),
                'send_by' => $m->get_data('send_by'),
                'sent_to' => $m->get_data('sent_to'),
                'msg_date' => $m->get_data('msg_date'),
                'reply_msg' => $m->get_data('reply_msg'),
                'reply_msg_img' => $m->get_data('reply_msg_img'),
                'reply_msg_type' => $m->get_data('reply_msg_type'),
                'reply_user_name' => $m->get_data('reply_user_name'),
            );

            $q = $d->insert('chat_master_group', $a1);


            if ($q == true) {

                if ($msgType=='1') {
                $notiDescription="📸 Image";
                }else if ($msgType=='2') {
                   $notiDescription="📃 Document";
                }else if ($msgType=='3') {
                   $notiDescription="🎧 Audio";
                }else if ($msgType=='4') {
                   $notiDescription="📌 Location";
                }else if ($msgType=='5') {
                   $notiDescription="👤 Contact";
                }else if ($msgType=='6') {
                   $notiDescription="🎞️ Video";
                }else {
                    $notiDescription =  $msg_data;
                }
               

                $notAry = array(
                    'group_id' => $group_id,
                    'userId' => $m->get_data('msg_by'),
                    'group_name' => $group_name,
                    'group_icon' => $group_icon,
                    'member_count' => $member_count,
                    'unread_count' => "1",
                );

                if ($group_type == 1) {                    
                    $fcmArray = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.block_id = '$block_id' AND chat_group_master.group_id = '$group_id' AND block_master.block_id = chat_group_master.group_common_id  AND users_master.device='android' AND users_master.user_token!=''");

                    $fcmArrayIos = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.block_id = '$block_id' AND chat_group_master.group_id = '$group_id' AND block_master.block_id = chat_group_master.group_common_id  AND users_master.device='ios' AND users_master.user_token!=''");
                }else if ($group_type == 2) {
                    $fcmArray = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.floor_id = '$floor_id' AND chat_group_master.group_id = '$group_id' AND floors_master.floor_id = chat_group_master.group_common_id   AND users_master.device='android' AND users_master.user_token!=''");

                     $fcmArrayIos = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.floor_id = '$floor_id' AND chat_group_master.group_id = '$group_id' AND floors_master.floor_id = chat_group_master.group_common_id   AND users_master.device='ios' AND users_master.user_token!=''");
                }else if ($group_type == 3) {
                    $fcmArray = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master,zone_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.zone_id = '$zone_id' AND chat_group_master.group_id = '$group_id' AND zone_master.zone_id = chat_group_master.group_common_id   AND users_master.device='android' AND users_master.user_token!=''");
                    $fcmArrayIos = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master,zone_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.zone_id = '$zone_id' AND chat_group_master.group_id = '$group_id' AND zone_master.zone_id = chat_group_master.group_common_id   AND users_master.device='ios' AND users_master.user_token!=''");
                }else if ($group_type == 4) {
                    $fcmArray = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master,employee_level_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.level_id = '$level_id' AND chat_group_master.group_id = '$group_id' AND employee_level_master.level_id = chat_group_master.group_common_id   AND users_master.device='android' AND users_master.user_token!=''");
                    $fcmArrayIos = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master,employee_level_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.level_id = '$level_id' AND chat_group_master.group_id = '$group_id' AND employee_level_master.level_id = chat_group_master.group_common_id   AND users_master.device='ios' AND users_master.user_token!=''");
                }else{
                    $fcmArray = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND chat_group_member_master.society_id='$society_id' AND users_master.device='android' AND chat_group_member_master.group_id='$group_id' AND chat_group_member_master.user_id!='$msg_by'");

                    $fcmArrayIos = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND users_master.society_id='$society_id' AND users_master.device='ios' AND chat_group_member_master.group_id='$group_id' AND chat_group_member_master.user_id!='$msg_by'");

                }


                
                $nResident->noti("chatMsgGroup", "", $society_id, $fcmArray, "$unit_name @ $group_name", $notiDescription, $notAry);
                $nResident->noti_ios("chatMsgGroup", "", $society_id, $fcmArrayIos, "$unit_name @ $group_name", $notiDescription, $notAry);

                $response["message"] = "Message Send";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Fail to Send.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if ($_POST['addChatWithImg'] == "addChatWithImgNew" && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_id != '' && $unit_id != 0) {
                $quc = $d->select("unit_master", "unit_id='$unit_id' AND chat_access_denied=1");
                if (mysqli_num_rows($quc) > 0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $total = count($_FILES['chat_img']['tmp_name']);
            for ($i = 0; $i < $total; $i++) {
                $uploadedFile = $_FILES['chat_img']['name'][$i];
                if ($uploadedFile != "") {
                    $temp = explode(".", $uploadedFile);
                    $feed_img = "Chat_" . round(microtime(true)) . $i . '.' . end($temp);
                    move_uploaded_file($_FILES['chat_img']['tmp_name'][$i], "../img/chatImg/" . $feed_img);

                    $m->set_data('chat_id_reply', $chat_id_reply);
                    $m->set_data('society_id', $society_id);
                    $m->set_data('group_id', $group_id);
                    $m->set_data('msg_by', $msg_by);
                    $m->set_data('msg_for', $msg_for);
                    $m->set_data('msg_data', $_POST['msg_data'][$i]);
                    $m->set_data('msg_status', '0');
                    $m->set_data('send_by', '0');
                    $m->set_data('sent_to', $sent_to);
                    $m->set_data('msg_date', $date);
                    $m->set_data('msg_type', "1");
                    $m->set_data('msg_img', $feed_img);

                    $a1 = array(
                        'chat_id_reply' => $m->get_data('chat_id_reply'),
                        'society_id' => $m->get_data('society_id'),
                        'group_id' => $m->get_data('group_id'),
                        'msg_by' => $m->get_data('msg_by'),
                        'msg_by_name' => $m->get_data('msg_for'),
                        'msg_data' => $m->get_data('msg_data'),
                        'msg_type' => $m->get_data('msg_type'),
                        'msg_img' => $m->get_data('msg_img'),
                        'msg_status' => $m->get_data('msg_status'),
                        'send_by' => $m->get_data('send_by'),
                        'sent_to' => $m->get_data('sent_to'),
                        'msg_date' => $m->get_data('msg_date'),
                        'msg_img' => $m->get_data('msg_img')
                    );

                    $q = $d->insert('chat_master_group', $a1);
                }
            }

             $notAry = array(
                    'group_id' => $group_id,
                    'userId' => $m->get_data('msg_by'),
                    'group_name' => $group_name,
                    'group_icon' => $group_icon,
                    'member_count' => $member_count,
                    'unread_count' => "1",
                );

                $fcmArray = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND chat_group_member_master.society_id='$society_id' AND users_master.device='android' AND chat_group_member_master.group_id='$group_id'");

                $fcmArrayIos = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND users_master.society_id='$society_id' AND users_master.device='ios' AND chat_group_member_master.group_id='$group_id'");

                $nResident->noti("chatMsgGroup", "", $society_id, $fcmArray, "$unit_name @ $group_name", "📸 Image", $notAry);
                $nResident->noti_ios("chatMsgGroup", "", $society_id, $fcmArrayIos, "$unit_name @ $group_name", "📸 Image", $notAry);

            $response["message"] = "Message Send";
            $response["status"] = "200";
            echo json_encode($response);
        }else if ($_POST['addChatWithDoc'] == "addChatWithDoc" && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_id != '' && $unit_id != 0) {
                $quc = $d->select("unit_master", "unit_id='$unit_id' AND chat_access_denied=1");
                if (mysqli_num_rows($quc) > 0) {
                    $response["message"] = "Access Denied..!";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                }
            }

            $total = count($_FILES['chat_doc']['tmp_name']);
            for ($i = 0; $i < $total; $i++) {
                $uploadedFile = $_FILES['chat_doc']['name'][$i];
                if ($uploadedFile != "") {
                    $temp = explode(".", $uploadedFile);
                    $feed_img = "Chat_" . round(microtime(true)) . $i . '.' . end($temp);
                    move_uploaded_file($_FILES['chat_doc']['tmp_name'][$i], "../img/chatImg/" . $feed_img);
                    $file_original_name = $uploadedFile;
                    $bytesSize = $_FILES['chat_doc']['size'][$i];
                    $file_size= $d->formatSizeUnits($bytesSize);
                    $m->set_data('chat_id_reply', $chat_id_reply);
                    $m->set_data('society_id', $society_id);
                    $m->set_data('group_id', $group_id);
                    $m->set_data('msg_by', $msg_by);
                    $m->set_data('msg_for', $msg_for);
                    $m->set_data('msg_data', $_POST['msg_data'][$i]);
                    $m->set_data('msg_status', '0');
                    $m->set_data('send_by', '0');
                    $m->set_data('sent_to', $sent_to);
                    $m->set_data('msg_date', $date);
                    $m->set_data('msg_type', $msgType);
                    $m->set_data('msg_img', $feed_img);
                    $m->set_data('file_original_name', $file_original_name);
                    $m->set_data('location_lat_long', $location_lat_long);
                    $m->set_data('file_size', $file_size);
                    $m->set_data('file_duration', $file_duration);
                    $m->set_data('reply_msg', $reply_msg);
                    $m->set_data('reply_msg_img', $reply_msg_img);
                    $m->set_data('reply_msg_type', $reply_msg_type);
                    $m->set_data('reply_user_name', $reply_user_name);
        
                    $a1 = array(
                        'chat_id_reply' => $m->get_data('chat_id_reply'),
                        'society_id' => $m->get_data('society_id'),
                        'group_id' => $m->get_data('group_id'),
                        'msg_by' => $m->get_data('msg_by'),
                        'msg_by_name' => $m->get_data('msg_for'),
                        'msg_data' => $m->get_data('msg_data'),
                        'msg_type' => $m->get_data('msg_type'),
                        'msg_img' => $m->get_data('msg_img'),
                        'file_original_name' => $m->get_data('file_original_name'),
                        'location_lat_long' => $m->get_data('location_lat_long'),
                        'file_size' => $m->get_data('file_size'),
                        'file_duration' => $m->get_data('file_duration'),
                        'msg_status' => $m->get_data('msg_status'),
                        'send_by' => $m->get_data('send_by'),
                        'sent_to' => $m->get_data('sent_to'),
                        'msg_date' => $m->get_data('msg_date'),
                        'msg_img' => $m->get_data('msg_img'),
                        'reply_msg' => $m->get_data('reply_msg'),
                        'reply_msg_img' => $m->get_data('reply_msg_img'),
                        'reply_msg_type' => $m->get_data('reply_msg_type'),
                        'reply_user_name' => $m->get_data('reply_user_name'),
                    );

                    $q = $d->insert('chat_master_group', $a1);
                }
            }

            if ($msgType=='1') {
                $notiDescription="📸 Image";
            }else if ($msgType=='2') {
               $notiDescription="📃 Document";
            }else if ($msgType=='3') {
               $notiDescription="🎧 Audio";
            }else if ($msgType=='4') {
               $notiDescription="📌 Location";
            }else if ($msgType=='5') {
               $notiDescription="👤 Contact";
            }else if ($msgType=='6') {
                   $notiDescription="🎞️ Video";
            }else {
                $notiDescription = "📃 Document";
            }

             $notAry = array(
                    'group_id' => $group_id,
                    'userId' => $m->get_data('msg_by'),
                    'group_name' => $group_name,
                    'group_icon' => $group_icon,
                    'member_count' => $member_count,
                    'unread_count' => "1",
                );

             if ($group_type == 1) {                    
                    $fcmArray = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.block_id = '$block_id' AND chat_group_master.group_id = '$group_id' AND block_master.block_id = chat_group_master.group_common_id  AND users_master.device='android' AND users_master.user_token!=''");

                    $fcmArrayIos = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.block_id = '$block_id' AND chat_group_master.group_id = '$group_id' AND block_master.block_id = chat_group_master.group_common_id  AND users_master.device='ios' AND users_master.user_token!=''");
                }else if ($group_type == 2) {
                    $fcmArray = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.floor_id = '$floor_id' AND chat_group_master.group_id = '$group_id' AND floors_master.floor_id = chat_group_master.group_common_id   AND users_master.device='android' AND users_master.user_token!=''");

                     $fcmArrayIos = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.floor_id = '$floor_id' AND chat_group_master.group_id = '$group_id' AND floors_master.floor_id = chat_group_master.group_common_id   AND users_master.device='ios' AND users_master.user_token!=''");
                }else if ($group_type == 3) {
                    $fcmArray = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master,zone_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.zone_id = '$zone_id' AND chat_group_master.group_id = '$group_id' AND zone_master.zone_id = chat_group_master.group_common_id   AND users_master.device='android' AND users_master.user_token!=''");
                    $fcmArrayIos = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master,zone_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.zone_id = '$zone_id' AND chat_group_master.group_id = '$group_id' AND zone_master.zone_id = chat_group_master.group_common_id   AND users_master.device='ios' AND users_master.user_token!=''");
                }else if ($group_type == 4) {
                    $fcmArray = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master,employee_level_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.level_id = '$level_id' AND chat_group_master.group_id = '$group_id' AND employee_level_master.level_id = chat_group_master.group_common_id   AND users_master.device='android' AND users_master.user_token!=''");
                    $fcmArrayIos = $d->get_android_fcm("unit_master,block_master,users_master,floors_master,chat_group_master,employee_level_master","users_master.delete_status=0 AND users_master.active_status = 0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND users_master.user_status!=0 AND users_master.user_id!='$msg_by'  AND users_master.level_id = '$level_id' AND chat_group_master.group_id = '$group_id' AND employee_level_master.level_id = chat_group_master.group_common_id   AND users_master.device='ios' AND users_master.user_token!=''");
                }else{
                    $fcmArray = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND chat_group_member_master.society_id='$society_id' AND users_master.device='android' AND chat_group_member_master.group_id='$group_id'");

                    $fcmArrayIos = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND users_master.society_id='$society_id' AND users_master.device='ios' AND chat_group_member_master.group_id='$group_id'");

                }

           /* $fcmArray = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND chat_group_member_master.society_id='$society_id' AND users_master.device='android' AND chat_group_member_master.group_id='$group_id'");

            $fcmArrayIos = $d->get_android_fcm("users_master,chat_group_member_master", "users_master.user_id=chat_group_member_master.user_id AND users_master.society_id='$society_id' AND users_master.device='ios' AND chat_group_member_master.group_id='$group_id'");*/

            $nResident->noti("chatMsgGroup", "", $society_id, $fcmArray, "$unit_name @ $group_name", $notiDescription, $notAry);
            $nResident->noti_ios("chatMsgGroup", "", $society_id, $fcmArrayIos, "$unit_name @ $group_name",$notiDescription, $notAry);

            $response["message"] = "Message Send";
            $response["status"] = "200";
            echo json_encode($response);
        } else if ($_POST['getdelChat'] == "getdelChat" && $chat_id != '' && filter_var($chat_id, FILTER_VALIDATE_INT) == true) {

            $qnotification = $d->delete("chat_master_group", "chat_id='$chat_id'");

            if ($qnotification == true) {

                $response["message"] = "Deleted successfully.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "No deleted Notification.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } elseif (isset($deleteChatMulti) && $deleteChatMulti=="deleteChatMulti" && filter_var($user_id, FILTER_VALIDATE_INT) == true) {


                 $chat_idAry = explode(",",$chat_id);
                for ($i=0; $i <count($chat_idAry); $i++) { 
                  $q=$d->delete("chat_master_group","chat_id='$chat_idAry[$i]' AND chat_id!=0 ");
                }

                if($q==true){
                   
                    $response["message"]="Deleted Successfully";
                    $response["status"]="200";
                    echo json_encode($response);

                }else {
                    $response["message"]="Sumething Wrong";
                    $response["status"]="201";
                    echo json_encode($response);                        
                }

            
        } else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
