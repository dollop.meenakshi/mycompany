<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
        if($_POST['getDoc']=="getDoc" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

            $response["list"] = array();

           $gu=$d->select("users_master,unit_master","users_master.unit_id=unit_master.unit_id AND users_master.user_id='$user_id' AND users_master.society_id='$society_id' AND users_master.user_status=1 AND users_master.user_type=0","");
           $ownerData=mysqli_fetch_array($gu);
        
           if ($ownerData['unit_status']==3) {
               // find tenant User Id
                 $gu=$d->select("users_master","unit_id='$ownerData[unit_id]' AND society_id='$society_id' AND member_status=0 AND user_type=1","ORDER BY user_id DESC");
                $tenantData=mysqli_fetch_array($gu);
                $tenant_id= $tenantData['user_id'];

                // Find Owner

            $qr = $d->select("users_master","(society_id='$society_id' AND user_type='1' AND user_status='1' AND user_id='$tenant_id' AND  (tenant_doc!='' OR prv_doc!='') )");
           } else {
            $qr = $d->select("users_master","(society_id='$society_id' AND user_type='1' AND user_status='1' AND user_id='$user_id' AND  (tenant_doc!='' OR prv_doc!='') )");

           }

            $gu1=$d->select("users_master","user_id='$user_id' AND society_id='$society_id' AND member_status=0 AND user_type=0","");
            $ownerData=mysqli_fetch_array($gu1);
            if ($ownerData>0) {
                $owner_id= $ownerData['user_id'];
            } 
               else {
                $owner_id="0";
            }

            // for Tenant Document
                $data = mysqli_fetch_array($qr);


            if (mysqli_num_rows($qr) > 0) {
                if ($data['tenant_doc']!='') {
                    $list = array();
                    $list["document_id"] ="tnt";
                    $list["user_id"] = $owner_id;
                    $list["ducument_name"] = "Tenant Agreement";
                    $list["ducument_description"] = "Tenant Agreement";
                    $list["document_file"] = $base_url."img/documents/". $data['tenant_doc'];
                    $list["uploade_date"] = "";
                    $list["share_with"] = "";
                    array_push($response["list"], $list);
                }
                if ($data['prv_doc']!='') {
                    $list = array();
                   
                    $list["document_id"] ="pvr";
                    $list["user_id"] = $owner_id;
                    $list["ducument_name"] = "Police Verification";
                    $list["ducument_description"] = "Police Verification";
                    $list["document_file"] = $base_url."img/documents/". $data['prv_doc'];
                    $list["uploade_date"] = "";
                    $list["share_with"] = "";
                    array_push($response["list"], $list);
                }
                $temp = true;
            } else {
                $temp = false;
            }

            $fq = $d->select("users_master", "user_id='$user_id'");
            $ownerData = mysqli_fetch_array($fq);
            $userType = $ownerData['user_type'];
            $member_status = $ownerData['member_status'];

            if ($member_status==0) {
            $q = $d->select("document_master,society_master","document_master.society_id=society_master.society_id AND document_master.society_id='$society_id' AND document_master.document_status='0' AND document_master.share_with='public'  OR document_master.society_id='$society_id' AND document_master.society_id!=0 AND document_master.document_status='0' AND document_master.uploaded_user_id='$user_id' ");
            } else {
                
            $q = $d->select("document_master,society_master","document_master.society_id=society_master.society_id AND document_master.society_id='$society_id' AND document_master.document_status='0' AND document_master.share_with='public'  AND society_master.tenant_access_for_document=0 OR document_master.society_id='$society_id' AND document_master.society_id!=0 AND document_master.document_status='0' AND document_master.uploaded_user_id='$user_id' AND society_master.tenant_access_for_document=0 ");
            }



            if (mysqli_num_rows($q) > 0) {
                while ($data = mysqli_fetch_array($q)) {
                    $list = array();
                    $list["document_id"] = $data['document_id'];
                    $list["user_id"] = $data['uploaded_user_id'];
                    $list["ducument_name"] = $data['ducument_name'];
                    $list["ducument_description"] = $data['ducument_description'];
                    $list["document_file"] = $base_url."img/documents/". $data['document_file'];
                    $list["uploade_date"] = $data['uploade_date'];
                    $list["share_with"] = $data['share_with'];
                    array_push($response["list"], $list);
                }
                $temp1 = true;
            } else {
                $temp1 = false;
            }

            $q2 = $d->select("document_master,document_shared_master","document_master.society_id='$society_id' AND document_shared_master.society_id='$society_id' 
            AND document_master.document_id=document_shared_master.document_id AND document_shared_master.user_id='$user_id' AND document_master.document_status='0'");

            if (mysqli_num_rows($q2) > 0) {
                while ($data2 = mysqli_fetch_array($q2)) {
                    $list = array();
                    $list["document_id"] = $data2['document_id'];
                    $list["user_id"] = $data2['uploaded_user_id'];
                    $list["document_type_id"] = $data2['document_type_id'];
                    $list["ducument_name"] = $data2['ducument_name'];
                    $list["ducument_description"] = $data2['ducument_description'];
                    $list["document_file"] = $base_url."img/documents/". $data2['document_file'];
                    $list["uploade_date"] = $data2['uploade_date'];
                    $list["share_with"] = $data2['share_with'];
                    array_push($response["list"], $list);
                }
                $temp2 = true;
            } else {
                $temp2 = false;
            }
            if ($temp1==true || $temp2 ==true || $temp==true) {
                $response["message"] = "list get success.";
                $response["status"] = "200";
                echo json_encode($response);
            } else {
                $response["message"] = "Faild to get List...!!";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['addDoc']=="addDoc" && $user_id !=0 && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 

            $totalDocuments=$d->count_data_direct("document_id","document_master","society_id='$society_id' AND  uploaded_user_id='$user_id'");

            if ($totalDocuments>=6) {
                $response["message"] = "Maximum 6 Documents Allowed";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            if ($document_type=="Rent Agreement" || $document_type=="Tenant Police Verification") {
                $gu=$d->select("users_master","unit_id='$unit_id' AND society_id='$society_id' AND member_status=0 AND user_type=1","ORDER BY user_id DESC");
                $tenantData=mysqli_fetch_array($gu);
                $tenantUserId= $tenantData['user_id'];
                if ($tenantData<1) {
                    $response["message"] = "Your Unit is not rented.";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                } else {
                    $gu=$d->select("users_master,unit_master","users_master.unit_id=unit_master.unit_id AND users_master.user_id='$user_id' AND users_master.society_id='$society_id' AND users_master.user_status=1 AND users_master.user_type=0","");
                    $ownerData=mysqli_fetch_array($gu);

                     $gu=$d->select("users_master","unit_id='$ownerData[unit_id]' AND society_id='$society_id' AND member_status=0 AND user_type=1","ORDER BY user_id DESC");
                    $tenantData=mysqli_fetch_array($gu);
                    $tenant_id= $tenantData['user_id'];

                    $file11=$_FILES["doc_file"]["tmp_name"];
                    $extId = pathinfo($_FILES['doc_file']['name'], PATHINFO_EXTENSION);
                    $extAllow=array("pdf","doc","docx","png","jpg","jpeg","JPEG","PNG","JPG","JPEG");

                      if(in_array($extId,$extAllow)) {
                         $temp = explode(".", $_FILES["doc_file"]["name"]);
                          $doc_file = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                          move_uploaded_file($_FILES["doc_file"]["tmp_name"], "../img/documents/" . $doc_file);
                      } else {
                        
                        $response["message"]="Invalid Document only JPG,PNG,Doc & PDF are allowed.";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit();
                      }

                    if ($document_type=="Rent Agreement") {
                        $a = array(
                        'tenant_doc' => $doc_file,
                        );
                    } else if ($document_type=="Tenant Police Verification") {
                        $a = array(
                        'prv_doc' => $doc_file,
                        );
                    }

                    $d->update("users_master",$a,"user_id='$tenant_id'");

                    $response["message"] = "Document Uploaded";
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();

                }
            }

            $file11=$_FILES["doc_file"]["tmp_name"];
            $extId = pathinfo($_FILES['doc_file']['name'], PATHINFO_EXTENSION);
            $extAllow=array("pdf","doc","docx","png","jpg","jpeg","PDF","JPEG","PNG","JPG","JPEG");

              if(in_array($extId,$extAllow)) {
                 $temp = explode(".", $_FILES["doc_file"]["name"]);
                  $doc_file = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                  move_uploaded_file($_FILES["doc_file"]["tmp_name"], "../img/documents/" . $doc_file);
              } else {
                
                $response["message"]="Invalid Document only JPG,PNG,Doc & PDF are allowed.";
                $response["status"]="201";
                echo json_encode($response);
                exit();
              }

           

            $m->set_data('society_id',$society_id);
            $m->set_data('uploaded_user_id',$user_id);
            $m->set_data('document_type_id',0);
            $m->set_data('document_type',$document_type);
            $m->set_data('ducument_name',$ducument_name);
            $m->set_data('document_file',$doc_file);
            $m->set_data('uploade_date', date("Y-m-d H:i"));

            $a1= array (
                'society_id'=> $m->get_data('society_id'),
                'uploaded_user_id'=> $m->get_data('uploaded_user_id'),
                'document_type_id'=> $m->get_data('document_type_id'),
                'document_type'=> $m->get_data('document_type'),
                'ducument_name'=> $m->get_data('ducument_name'),
                'document_file'=> $m->get_data('document_file'),
                'uploade_date'=> $m->get_data('uploade_date'),
            );

           
            $q=$d->insert("document_master",$a1);
                

            if ($q>0) {

              $qUserToken=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
              $data_notification=mysqli_fetch_array($qUserToken);
              $sos_user_token=$data_notification['user_token'];
              $device=$data_notification['device'];
              if ($device=='android') {
                 $nResident->noti("","",$society_id,$sos_user_token,"Document Uploaded","Successfully",'docUpload');
              }  else if($device=='ios') {
                $nResident->noti_ios("","",$society_id,$sos_user_token,"Document Uploaded","Successfully",'docUpload');
              }

              $d->insert_myactivity($user_id,"$society_id","0","$user_name","New document uploaded","document_management.png");


                $response["message"] = "Document Uploaded Successfully";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Faild to Upload";
                $response["status"] = "201";
                echo json_encode($response);
            }

        }else if($_POST['deleteDoc']=="deleteDoc" && $user_id !=0 && $user_id!='' && filter_var($user_id, FILTER_VALIDATE_INT) == true ){ 

            if ($document_id=="tnt") {
                $gu=$d->select("users_master,unit_master","users_master.unit_id=unit_master.unit_id AND users_master.user_id='$user_id' AND users_master.society_id='$society_id' AND users_master.user_status=1 AND users_master.user_type=0","");
                $ownerData=mysqli_fetch_array($gu);

                 $gu=$d->select("users_master","unit_id='$ownerData[unit_id]' AND society_id='$society_id' AND member_status=0 AND user_type=1","ORDER BY user_id DESC");
                $tenantData=mysqli_fetch_array($gu);
                $tenant_id= $tenantData['user_id'];

                $a = array(
                'tenant_doc' => "",
                );

                $d->update("users_master",$a,"user_id='$tenant_id'");

                $response["message"] = "Tenant Agreement Deleted";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            }

            if ($document_id=='pvr') {
                 $gu=$d->select("users_master,unit_master","users_master.unit_id=unit_master.unit_id AND users_master.user_id='$user_id' AND users_master.society_id='$society_id' AND users_master.user_status=1 AND users_master.user_type=0","");
                $ownerData=mysqli_fetch_array($gu);

                 $gu=$d->select("users_master","unit_id='$ownerData[unit_id]' AND society_id='$society_id' AND member_status=0 AND user_type=1","ORDER BY user_id DESC");
                $tenantData=mysqli_fetch_array($gu);
                $tenant_id= $tenantData['user_id'];

                $a = array(
                'prv_doc' => "",
                );

                $d->update("users_master",$a,"user_id='$tenant_id'");

                $response["message"] = "Police Verification Report Deleted";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            }


            $q=$d->delete("document_master","uploaded_user_id='$user_id' AND document_id='$document_id'");

            if ($q>0) {

                $d->insert_myactivity($user_id,"$society_id","0","$user_name","Document deleted","document_management.png");

                $response["message"] = "Document Uploaded Successfully.";
                $response["status"] = "200";
                echo json_encode($response);
                exit();
            } else {
                $response["message"] = "Faild to Delete";
                $response["status"] = "201";
                echo json_encode($response);
            }
 



        }else if($_POST['readDoc']=="readDoc" && $user_id !=0 && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true  && filter_var($document_id, FILTER_VALIDATE_INT) == true ){ 
            
            $q=$d->select("document_master","uploaded_user_id='0' AND  document_id='$document_id'");

           

            if (mysqli_num_rows($q)>0) {
                 $aa = array(
                    'society_id' => $society_id,
                    'user_id' => $user_id,
                    'document_id' => $document_id,
                    'read_time' => date("Y-m-d H:i:s"),
                    'unit_id' => $unit_id,
                );

                $cqqq=$d->selectRow("read_id","document_read_status","document_id='$document_id' AND user_id='$user_id'");

                if (mysqli_num_rows($cqqq)>0) {
                    $oldData=mysqli_fetch_array($cqqq);
                    $read_id=$oldData['read_id'];


                   $d->update("document_read_status",$aa,"document_id='$document_id' AND user_id='$user_id'");
                } else {
                   $d->insert("document_read_status",$aa);
                }

                $response["message"] = "Done ";
                $response["status"] = "201";
                echo json_encode($response);

            } else {
                $response["message"] = "Faild ";
                $response["status"] = "201";
                echo json_encode($response);
            }


        }else{
            $response["message"]="wrong tag.";
            $response["status"]="201";
            echo json_encode($response);

        }
    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>