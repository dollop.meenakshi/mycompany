<?php
include_once 'lib.php';
 
// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);

if(isset($_POST) && !empty($_POST)){

    if ($key==$keydb && $auth_check=='true') {
        
        $response = array();
        extract(array_map("test_input" , $_POST));
        $dateTime = date("Y-m-d H:i:s");
        $temDate = date("Y-m-d h:i A");

        $startDate=date("Y-m-01");
        $currentDate=date("Y-m-d");
        $lastDate=date("Y-m-t");

        if($_POST['checkAccessPunchInReq']=="checkAccessPunchInReq" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $punch_in_req_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['checkAccessPunchOutMissingReq']=="checkAccessPunchOutMissingReq" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $punch_out_missing_req_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['checkAccessPendingAttendance']=="checkAccessPendingAttendance" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $pending_attendance_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['checkAccessAttendanceHistory']=="checkAccessAttendanceHistory" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $view_employee_attendance_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['checkAccessAbsentPresent']=="checkAccessAbsentPresent" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $view_absent_present_access;

            include 'check_access_data.php';

            $accessResponseData['status'] = "200";
            $accessResponseData['message'] = "Data found";
            echo json_encode($accessResponseData);
                      
        }else if($_POST['getAttendanceType']=="getAttendanceType" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $response["message"] = "New version of MyCo is available on Play Store, please update your MyCo app";
            $response["status"] = "201";
            echo json_encode($response);
            exit();

        }else if($_POST['attendancePunchIn']=="attendancePunchIn" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            $response["message"] = "New version of MyCo is available on Play Store, please update your MyCo app";
            $response["status"] = "201";
            echo json_encode($response);
            exit();
        }else if($_POST['checkLateIn']=="checkLateIn" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $punchInTime =date('H:i:s');
            $punchInDate =date('Y-m-d');

            $punchInTimeDate = $punchInDate." ".$punchInTime;

            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
            }

            // Late IN

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $shift_start_time = $shiftData['shift_start_time'];
            $late_time_start = $shiftData['late_time_start'];
            $late_in_reason = $shiftData['late_in_reason'];

            $qryLeave = $d->selectRow("leave_day_type,half_day_session","leave_master","user_id = '$user_id' AND leave_start_date = '$punchInDate'");

            if (mysqli_num_rows($qryLeave) > 0) {
                $leaveData = mysqli_fetch_array($qryLeave);
                $leave_day_type = $leaveData['leave_day_type'];
                $half_day_session = $leaveData['half_day_session'];
            }

            if ($late_in_reason == "1") {
                $min = date('i',strtotime($late_time_start));
                $defaultINTime = $punchInDate." ".$shift_start_time;
                $strMin = "+".$min." minutes";
                $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

                if ($late_time_start != '') {
                    if ($punchInTimeDate > $defaultTime) {
                        $late_in = true;
                    }else{
                        $late_in = false;
                    }
                }else{
                    $late_in = false;
                }

                if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 1) {
                    $late_in = false;
                }
            }else{
                $late_in = false;
            }
            
            $response["late_in"] = $late_in;
            $response["status"] = "200";
            echo json_encode($response);
                 
        }else if($_POST['checkEarlyOut']=="checkEarlyOut" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $punchOUTDate =date('Y-m-d');
            $punchOUTTime =date('H:i:s');

            $datePunchIN = $punchInDate ." ".$punchInTime;
            $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
            $datePunchOUT = $punchOUTDate ." ".$punchOUTTime;


            $sq = $d->selectRow("shift_time_id","users_master","user_id = '$user_id'");

            if (mysqli_num_rows($sq)>0) {
                $sdata = mysqli_fetch_array($sq);
                $shiftTimeId = $sdata['shift_time_id'];
            }

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $shift_start_time = $shiftData['shift_start_time'];
            $shift_end_time = $shiftData['shift_end_time'];
            $late_time_start = $shiftData['late_time_start'];
            $early_out_time = $shiftData['early_out_time'];
            $shift_type = $shiftData['shift_type'];
            $early_out_reason = $shiftData['early_out_reason'];

            $qryLeave = $d->selectRow("leave_day_type,half_day_session","leave_master","user_id = '$user_id' AND leave_start_date = '$punchOUTDate'");


            if (mysqli_num_rows($qryLeave) > 0) {

                $leaveData = mysqli_fetch_array($qryLeave);
                $leave_day_type = $leaveData['leave_day_type'];
                $half_day_session = $leaveData['half_day_session'];
            }


            if ($early_out_reason == "1") {
                $min = date('i',strtotime($early_out_time));

                $parts1 = explode(':', $early_out_time);
                $early_out_time_minutes = ($parts1[0]*60) + ($parts1[1]) + ($parts1[2]/60);

                $defaultOutTime = $punchOUTDate." ".$shift_end_time;
                $strMin = "-".$early_out_time_minutes." minutes";
                $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));

                //if ($early_out_time != '') {
                    if ($datePunchOUT >= $defaultOutTime) {
                        $early_out = false;
                    }else{
                        $early_out = true;              
                    }
                /*}else{
                    $early_out = false;
                }*/

                if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                    $early_out = false;
                }
            }else{
                $early_out = false; 
            }
            
            $response["early_out"] = $early_out;
            $response["status"] = "200";
            echo json_encode($response);
            exit(); 
                 
        }else if($_POST['punchInRequest']=="punchInRequest" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            $response["message"] = "New version of MyCo is available on Play Store, please update your MyCo app";
            $response["status"] = "201";
            echo json_encode($response);
            exit();
        }else if($_POST['approvePunchInRequest']=="approvePunchInRequest" && $attendance_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $qsm = $d->selectRow("user_token,device,shift_time_id,unit_id,user_full_name","users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
            $data_notification = mysqli_fetch_array($qsm);
            $user_token = $data_notification['user_token'];
            $device = $data_notification['device'];
            $unit_id = $data_notification['unit_id'];
            $shiftTimeId = $data_notification['shift_time_id'];
            $tabPosition = "0"; 

            $shiftQry = $d->select("shift_timing_master","shift_time_id = '$shiftTimeId' AND is_deleted = '0'");
            $shiftData = mysqli_fetch_array($shiftQry);
            $perdayHours = $shiftData['per_day_hour'];
            $late_time_start = $shiftData['late_time_start'];
            $early_out_time = $shiftData['early_out_time'];
            $shift_end_time = $shiftData['shift_end_time'];
            $half_day_time_start = $shiftData['half_day_time_start'];
            $shift_start_time = $shiftData['shift_start_time'];
            $halfday_before_time = $shiftData['halfday_before_time'];
            $maximum_halfday_hours = $shiftData['maximum_halfday_hours'];
            $minimum_hours_for_full_day = $shiftData['minimum_hours_for_full_day'];
            $maximum_in_out = $shiftData['maximum_in_out'];

            $qryLeave = $d->selectRow("leave_id,leave_type_id,paid_unpaid,leave_reason,leave_day_type","leave_master","user_id = '$attendance_user_id' AND leave_start_date = '$attendance_date_start'");

            $hasLeave = false;

            if (mysqli_num_rows($qryLeave) > 0) {

                $hasLeave = true;

                $leaveData = mysqli_fetch_array($qryLeave);

                $leave_id = $leaveData['leave_id'];
                $leave_type_id = $leaveData['leave_type_id'];
                $paid_unpaid = $leaveData['paid_unpaid'];
                $leave_reason = $leaveData['leave_reason'];
                $leave_day_type = $leaveData['leave_day_type'];
                $half_day_session = $leaveData['half_day_session'];
            }

            $punchInDateTime = $attendance_date_start." ".$punch_in_time;

            if ($late_time_start != '') {
                $partLI = explode(':', $late_time_start);
                $min = ($partLI[0]*60) + ($partLI[1]) + ($partLI[2]/60);
            }else{
                $min = 0;
            }

            $defaultINTime = $attendance_date_start." ".$shift_start_time;
            $strMin = "+".$min." minutes";
            $defaultTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultINTime)));

            if ($late_time_start != '') {
                if ($punchInDateTime > $defaultTime) {
                    $late_in = "1";
                }else{
                    $late_in = "0";
                }
            }else{
                $late_in = "0";
            }

            if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 1) {
                $late_in = "0";
            }

            $datePunchOUT = $attendance_date_end." ".$punch_out_time;

            if ($early_out_time != '') {
                $partEO = explode(':', $early_out_time);
                $min = ($partEO[0]*60) + ($partEO[1]) + ($partEO[2]/60);
            }else{
                $min = 0;
            }
            
            $defaultOutTime = $attendance_date_end." ".$shift_end_time;
            $strMin = "-".$min." minutes";
            $defaultOutTime = date('Y-m-d H:i:s',strtotime($strMin,strtotime($defaultOutTime)));


            if ($early_out_time != '') {
                if ($datePunchOUT >= $defaultOutTime) {
                    $early_out = "0";
                }else{
                    $early_out = "1";              
                }
            }else{
                $early_out = "0";
            }

            if ($leave_day_type == 1 && $half_day_session != null && $half_day_session == 2) {
                $early_out = "0";
            }

            $parts = explode(':', $perdayHours);
            $perDayHourMinute = ($parts[0]*60) + ($parts[1]) + ($parts[2]/60);
            $perdayHours = $parts[0] + floor(($parts[1]/60)*100) / 100 . PHP_EOL;

            $totalHours = getTotalHoursWithNames($attendance_date_start,$attendance_date_end,$punch_in_time,$punch_out_time);
            $totalHR = getTotalHours($attendance_date_start,$attendance_date_end,$punch_in_time,$punch_out_time);

            $times[] = $totalHR;

            $time = explode(':', $totalHR.":00");
            $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

            $avgWorkingDays = $total_minutes/$perDayHourMinute;

            $avg_working_days = round($avgWorkingDays * 2) / 2;

            $extra_working_hours_minutes = 0;

            if ($total_minutes > $perDayHourMinute) {
                $extra_working_hours_minutes = $total_minutes - $perDayHourMinute;
                $hours  = floor($extra_working_hours_minutes/60);
                $minutes = $extra_working_hours_minutes % 60;

                $extra_working_hours =sprintf('%02d:%02d', $hours, $minutes);

            }

            $m->set_data('attendance_status',"1");
            $m->set_data('attendance_status_change_by',$user_id);
            $m->set_data('attendance_date_start',$attendance_date_start);
            $m->set_data('attendance_date_end',$attendance_date_end);
            $m->set_data('punch_in_time',$punch_in_time);
            $m->set_data('punch_out_time',$punch_out_time);
            $m->set_data('total_working_hours',$totalHR);
            $m->set_data('avg_working_days',$avg_working_days);
            $m->set_data('total_working_minutes',$total_minutes);
            $m->set_data('extra_working_hours',$extra_working_hours);
            $m->set_data('extra_working_hours_minutes',$extra_working_hours_minutes);
            $m->set_data('late_in',$late_in);
            $m->set_data('early_out',$early_out);
            $m->set_data('punch_in_req_approve_date',date('Y-m-d H:i:s'));

            $a = array(
                'attendance_status'=>$m->get_data('attendance_status'),
                'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
                'attendance_date_start'=>$m->get_data('attendance_date_start'),
                'attendance_date_end'=>$m->get_data('attendance_date_end'),
                'punch_in_time'=>$m->get_data('punch_in_time'),
                'punch_out_time'=>$m->get_data('punch_out_time'),
                'total_working_hours'=>$m->get_data('total_working_hours'),
                'avg_working_days'=>$m->get_data('avg_working_days'),
                'total_working_minutes'=>$m->get_data('total_working_minutes'),
                'extra_working_hours'=>$m->get_data('extra_working_hours'),
                'extra_working_hours_minutes'=>$m->get_data('extra_working_hours_minutes'),
                'late_in'=>$m->get_data('late_in'),
                'early_out'=>$m->get_data('early_out'),
                'punch_in_req_approve_date'=>$m->get_data('punch_in_req_approve_date'),
            );

            $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id' AND user_id = '$attendance_user_id'");

            if ($punchInQry == true) {                

                $leaveValue = false;

                if ($half_day_time_start != '' && $half_day_time_start < $punch_in_time && $half_day_time_start != "00:00:00") {
                    $leaveValue = true;
                } else if ($halfday_before_time !='' && $halfday_before_time != '00:00:00' && $halfday_before_time > $punch_out_time) {
                    $leaveValue = true;
                }

                $maximum_halfday_hours_min = minutes($maximum_halfday_hours);
                $minimum_hours_for_full_day_min = minutes($minimum_hours_for_full_day);

                if ($total_minutes >= $minimum_hours_for_full_day_min && $minimum_hours_for_full_day != "00:00:00") {
                    $noLeave = true;
                }else{
                    $noLeave = false;
                }

                $alreadyLeaveQryCheck = false; 

                if ($noLeave == false && $maximum_halfday_hours_min > 0 && $minimum_hours_for_full_day_min > 0) {


                    $leave_total_days = 1;

                    // check half day and full day

                    if ($total_minutes < $maximum_halfday_hours_min) {
                        $avgCount = "0";
                        $leaveTypeName = "Full Day";
                        $leaveType = "0";
                        $half_day_session = "0";                
                    }else{
                        $avgCount = "0.5";
                        $leaveType = "1";
                        $leaveTypeName = "Half Day";
                    }

                    if ($hasLeave == true) {

                        // Change already applied leave

                        $title = "Auto Leave Applied - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed ($totalHours)";
                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                        $m->set_data('leave_type_id',$leave_type_id);
                        $m->set_data('paid_unpaid',$paid_unpaid);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('half_day_session',$half_day_session);

                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'half_day_session'=>$m->get_data('half_day_session'),
                        );

                        $leaveQry = $d->update("leave_master",$a,"user_id = '$attendance_user_id' AND leave_id = '$leave_id'");
                        

                        /*if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");*/
                        
                    }else{

                        // Auto Leave

                        $title = "Auto Leave Applied"." - ".$leaveTypeName;
                        $titleMessage = "Working hours not completed ($totalHours)";
                        $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                        $m->set_data('leave_type_id',"0");
                        $m->set_data('society_id',$society_id);
                        $m->set_data('paid_unpaid',"1");
                        $m->set_data('unit_id',$unit_id);
                        $m->set_data('user_id',$attendance_user_id);
                        $m->set_data('auto_leave_reason',$titleMessage);
                        $m->set_data('leave_start_date',$attendance_date_start);
                        $m->set_data('leave_end_date',$attendance_date_start);
                        $m->set_data('leave_total_days',$leave_total_days);
                        $m->set_data('leave_day_type',$leaveType);
                        $m->set_data('leave_status',"1");
                        $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                        $a = array(
                            'leave_type_id'=>$m->get_data('leave_type_id'),
                            'society_id' =>$m->get_data('society_id'),
                            'paid_unpaid' =>$m->get_data('paid_unpaid'),
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                            'leave_start_date'=>$m->get_data('leave_start_date'),
                            'leave_end_date'=>$m->get_data('leave_end_date'),
                            'leave_total_days'=>$m->get_data('leave_total_days'),
                            'leave_day_type'=>$m->get_data('leave_day_type'),
                            'leave_status'=>$m->get_data('leave_status'),
                            'leave_created_date'=>$m->get_data('leave_created_date'),
                        );

                        $leaveQry = $d->insert("leave_master",$a);

                        $alreadyLeaveQryCheck = true;

                        if ($device == 'android') {
                            $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        } else if ($device == 'ios') {
                            $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                        }

                        $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                    }

                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',$avgCount);


                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                }else if($noLeave == true && $hasLeave == true){

                    // Remove leave if present and full time

                    $leaveQry = $d->delete("leave_master","user_id = '$attendance_user_id' AND leave_id = '$leave_id'");

                    $d->insert_myactivity($user_id, $society_id, $user_id, $user_name, "Leave Cancelled", "leave_tracker.png");

                    $title = "Applied Leave Cancelled";
                    $description = "For Date : ".$attendance_date_start;

                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");
                }

                // Code - Maximum Late IN & Early OUT Limit Check -> Apply Leave

                $startMonthDate = date('Y-m-01');
                $endMonthDate = date('Y-m-t');


                $currentMonthLateInCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND late_in = '1'");

                $currentMonthEarlyOutCount = $d->count_data_direct("attendance_id","attendance_master","user_id = '$attendance_user_id' AND society_id = '$society_id' AND attendance_date_start BETWEEN '$startMonthDate' AND '$endMonthDate' AND early_out = '1'");


                $totalLateInOutCount = $currentMonthLateInCount + $currentMonthEarlyOutCount;

                 if ($hasLeave == false && $maximum_in_out != '' && $maximum_in_out > 0 && $totalLateInOutCount > $maximum_in_out && $alreadyLeaveQryCheck == false && $hasLeave == false && ($late_in == "1" || $early_out == "1")) {

                    $titleMessage = "This month late in($currentMonthLateInCount) + early out($currentMonthEarlyOutCount) limit reached. Maximum In/Out allowed: $maximum_in_out";

                    $m->set_data('leave_type_id',"0");
                    $m->set_data('society_id',$society_id);
                    $m->set_data('paid_unpaid',"1");
                    $m->set_data('unit_id',$unit_id);
                    $m->set_data('user_id',$attendance_user_id);
                    $m->set_data('auto_leave_reason',$titleMessage);
                    $m->set_data('leave_start_date',$attendance_date_start);
                    $m->set_data('leave_end_date',$attendance_date_start);
                    $m->set_data('leave_total_days',"1");
                    $m->set_data('leave_day_type',"1");
                    $m->set_data('leave_status',"1");
                    $m->set_data('leave_created_date',date('Y-m-d H:i:s'));

                    $a = array(
                        'leave_type_id'=>$m->get_data('leave_type_id'),
                        'society_id' =>$m->get_data('society_id'),
                        'paid_unpaid' =>$m->get_data('paid_unpaid'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'), 
                        'auto_leave_reason'=>$m->get_data('auto_leave_reason'),
                        'leave_start_date'=>$m->get_data('leave_start_date'),
                        'leave_end_date'=>$m->get_data('leave_end_date'),
                        'leave_total_days'=>$m->get_data('leave_total_days'),
                        'leave_day_type'=>$m->get_data('leave_day_type'),
                        'leave_status'=>$m->get_data('leave_status'),
                        'leave_created_date'=>$m->get_data('leave_created_date'),
                    );

                    $leaveQry = $d->insert("leave_master",$a);

                    $title = "Auto Leave Applied - Half Day";

                    $description = "For Date : ".$attendance_date_start."\n".$titleMessage;

                    if ($device == 'android') {
                        $nResident->noti("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    } else if ($device == 'ios') {
                        $nResident->noti_ios("leave_tracker","",$society_id,$user_token,$title,$description,$tabPosition);
                    }

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"leave_tracker","leave_tracker.png",$tabPosition,"user_id = '$attendance_user_id'");

                    $m->set_data('auto_leave',"1");
                    $m->set_data('avg_working_days',"0.5");

                    $a1 = array(
                        'auto_leave'=>$m->get_data('auto_leave'),
                        'avg_working_days'=>$m->get_data('avg_working_days'),
                    );

                    $d->update("attendance_master",$a1,"attendance_id = '$attendance_id'");
                }

                $title = "Attendance Approved";
                $description = "For the Date: ".date('d-m-Y',strtotime($attendance_date_start))." Time: ".date('h:i A',strtotime($punch_in_time))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                $response["message"] = "Attendance Approved";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['declinePunchInRequest']=="declinePunchInRequest" && $attendance_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_status',"2");
            $m->set_data('attendance_status_change_by',$user_id);
            $m->set_data('avg_working_days',"0");
            $m->set_data('attendance_declined_reason',$attendance_declined_reason);
            $m->set_data('attendance_rejected_date',date('Y-m-d H:i:s'));

            $a = array(
                'attendance_status'=>$m->get_data('attendance_status'),
                'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
                'avg_working_days'=>$m->get_data('avg_working_days'),
                'attendance_declined_reason'=>$m->get_data('attendance_declined_reason'),
                'attendance_rejected_date'=>$m->get_data('attendance_rejected_date'),
            );

            $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id' AND user_id = '$attendance_user_id'");

            if ($punchInQry == true) {

                $title = "Attendance Approval Declined";
                $description = "For the Date: ".date('d-m-Y',strtotime($attendance_date_start))." Time: ".date('h:i A',strtotime($punch_in_time))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                $response["message"] = "Attendance Approval Declined";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getPunchInRequest']=="getPunchInRequest" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $punch_in_req_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND attendance_master.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND attendance_master.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $punchInReqQry = $d->select("attendance_master,users_master,floors_master,block_master","attendance_master.punch_in_request = '1' 
                AND attendance_master.attendance_status = '0' 
                AND users_master.user_id = attendance_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id $appendQuery","ORDER BY attendance_master.attendance_id DESC $LIMIT_DATA");

            if(mysqli_num_rows($punchInReqQry)>0){

                $response["punch_in_requests"] = array();        

                while($data=mysqli_fetch_array($punchInReqQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $punchIn = array();

                    $punchIn['user_id'] = $data['user_id'];
                    $punchIn['user_full_name'] = $data['user_full_name'];
                    $punchIn['branch_name'] = $data['block_name'];
                    $punchIn['department_name'] = $data['floor_name'];
                    $punchIn['user_designation'] = $data['user_designation'];
                    $punchIn['attendance_id'] = $data['attendance_id'];
                    $punchIn['block_id'] = $data['block_id'];
                    $punchIn['floor_id'] = $data['floor_id'];
                    $punchIn['unit_id'] = $data['unit_id'];
                    $punchIn['society_id'] = $data['society_id'];

                    if ($data['user_profile_pic'] != '') {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $punchIn['attendance_date_start'] = $data['attendance_date_start'];
                    $punchIn['attendance_date_start_view'] = date('D, dS F Y',strtotime($data['attendance_date_start']));

                    $punchIn['attendance_date_end'] = $data['attendance_date_end'];
                    $punchIn['attendance_date_end_view'] = date('D, dS F Y',strtotime($data['attendance_date_end']));
                    $punchIn['punch_in_time'] = $data['punch_in_time'];
                    $punchIn['punch_in_time_view'] = date('h:i A',strtotime($data['punch_in_time']));
                    $punchIn['punch_out_time'] = $data['punch_out_time'];
                    $punchIn['punch_out_time_view'] = date('h:i A',strtotime($data['punch_out_time']));
                    $punchIn['attendance_reason'] = $data['attendance_reason'];
                    $punchIn['punch_in_address'] = $data['punch_in_address'].'';
                    $punchIn['punch_out_address'] = $data['punch_out_address'].'';

                    $totalHours = getTotalHoursWithNames($data['attendance_date_start'],$data['attendance_date_end'],$data['punch_in_time'],$data['punch_out_time']);
                    $hr = getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],$data['punch_in_time'],$data['punch_out_time']);
                    $time = explode(':', $hr.":00");
                    $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                    $punchIn['total_hours'] = $totalHours;
                    $punchIn['total_minutes'] = $total_minutes.'';


                    if ($data['punch_in_request_day_type'] != null) {
                        $punchIn['punch_in_request_day_type'] = $data['punch_in_request_day_type'];
                    }else{
                        $punchIn['punch_in_request_day_type'] = "";
                    }

                    if ($data['punch_in_request'] == "0") {
                        $punchIn['punch_in_request'] = false;
                    }else{
                        $punchIn['punch_in_request'] = true;
                    }

                    array_push($response['punch_in_requests'],$punchIn);
                }
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found";
                $response["status"] = "201";
                echo json_encode($response);
            }             
        }else if($_POST['getPendingAttendance']=="getPendingAttendance" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            
            $access_type = $pending_attendance_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND attendance_master.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        // code...
                        break;
                }
            }else{
                $appendQuery = " AND attendance_master.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $punchInQry = $d->select("attendance_master,users_master,floors_master,block_master","
                attendance_master.punch_in_request = '0' 
                AND attendance_master.attendance_status = '0'  
                AND attendance_master.society_id = '$society_id' 
                AND users_master.user_id = attendance_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id $appendQuery","ORDER BY attendance_master.attendance_id DESC $LIMIT_DATA");
            

            if(mysqli_num_rows($punchInQry)>0){

                $response['pending_attendance'] = array();
                
                while($data=mysqli_fetch_array($punchInQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $punchIn = array();

                    $block_id = $data['block_id'];

                    $punchIn['user_id'] = $data['user_id'];
                    $punchIn['user_full_name'] = $data['user_full_name'];
                    $punchIn['branch_name'] = $data['block_name'];
                    $punchIn['department_name'] = $data['floor_name'];
                    $punchIn['user_designation'] = $data['user_designation'];
                    $punchIn['attendance_id'] = $data['attendance_id'];
                    $punchIn['block_id'] = $data['block_id'];
                    $punchIn['floor_id'] = $data['floor_id'];
                    $punchIn['unit_id'] = $data['unit_id'];
                    $punchIn['society_id'] = $data['society_id'];
                    $punchIn['punch_in_latitude'] = $data['punch_in_latitude'];
                    $punchIn['punch_in_longitude'] = $data['punch_in_longitude'];
                    $punchIn['punch_out_latitude'] = $data['punch_out_latitude'];
                    $punchIn['punch_out_longitude'] = $data['punch_out_longitude'];
                    $punchIn['punch_in_in_range'] = $data['punch_in_in_range'];
                    $punchIn['punch_out_in_range'] = $data['punch_out_in_range'];
                    $punchIn['punch_out_meter'] = $data['punch_out_meter'];
                    $punchIn['punch_out_km'] = $data['punch_out_km'];
                    $punchIn['wfh_attendance'] = $data['wfh_attendance'];
                    $punchIn['attendance_range_in_meter'] = $data['attendance_range_in_meter'];
                    $punchIn['totalKm'] = $data['attendance_range_in_km'];
                    $punchIn['attendance_reason'] = $data['attendance_reason'];
                    $punchIn['punch_out_reason'] = $data['punch_out_reason'];
                    $punchIn['late_in_reason'] = $data['late_in_reason'];
                    $punchIn['early_out_reason'] = $data['early_out_reason'];
                    $punchIn['punch_in_address'] = $data['punch_in_address'].'';
                    $punchIn['punch_out_address'] = $data['punch_out_address'].'';

                    if ($data['attendance_date_end'] != '0000-00-00') {
                        
                        $totalHours = getTotalHoursWithNames($data['attendance_date_start'],$data['attendance_date_end'],$data['punch_in_time'],$data['punch_out_time']);
                        $hr = getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],$data['punch_in_time'],$data['punch_out_time']);
                        $time = explode(':', $hr.":00");
                        $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                        $punchIn['total_hours'] = $totalHours;
                        $punchIn['total_minutes'] = $total_minutes.'';

                    }else{
                        $punchIn['total_hours'] = "";
                        $punchIn['total_minutes'] = "";
                    }
                    

                    if ($data['late_in'] == '1') {
                        $punchIn["late_in"] = " (Late In)";
                    } else {
                        $punchIn["late_in"] = "";
                    }

                    if ($data['early_out'] == '1') {
                        $punchIn["early_out"] = " (Early Out)";
                    } else {
                        $punchIn["early_out"] = "";
                    }

                    if ($data['punch_in_image'] != '') {
                        $punchIn["punch_in_image"] = $base_url . "img/attendance/" . $data['punch_in_image'];
                    } else {
                        $punchIn["punch_in_image"] = "";
                    }

                    if ($data['punch_out_image'] != '') {
                        $punchIn["punch_out_image"] = $base_url . "img/attendance/" . $data['punch_out_image'];
                    } else {
                        $punchIn["punch_out_image"] = "";
                    }

                    if ($data['user_profile_pic'] != '') {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    if ($data['attendance_date_start'] == $currentDate) {
                        $punchIn['is_today'] = true;
                    }else{
                        $punchIn['is_today'] = false;
                    }

                    $attendance_date_start= $data['attendance_date_start'];

                    $punchIn['attendance_date_start'] = $attendance_date_start;
                    $punchIn['attendance_date_start_view'] = date('D, dS F Y',strtotime($attendance_date_start));

                    $punchIn['punch_in_time'] = $data['punch_in_time'];
                    $punchIn['punch_in_time_view'] = date('h:i A',strtotime($data['punch_in_time']));

                    if($data['attendance_date_end'] != '0000-00-00'){
                        $punchIn['attendance_date_end'] = $data['attendance_date_end'];
                        $punchIn['attendance_date_end_view'] = date('D, dS F Y',strtotime($data['attendance_date_end']));
                    }else{
                        $punchIn['attendance_date_end'] = "";
                        $punchIn['attendance_date_end_view'] = "";
                    }

                    if($data['punch_out_time'] != '00:00:00'){
                        $punchIn['punch_out_time'] = $data['punch_out_time'];
                        $punchIn['punch_out_time_view'] = date('h:i A',strtotime($data['punch_out_time']));
                    }else{
                        $punchIn['punch_out_time'] = "";
                        $punchIn['punch_out_time_view'] = "";
                    }

                    $punchIn['location_list'] = array();
                    $geoArray = array();

                    $qqGeo = $d->select("user_geofence","user_id = '$data[user_id]'");

                    if (mysqli_num_rows($qqGeo) > 0) {
                        while($dataGeo = mysqli_fetch_array($qqGeo)){


                            $geoArray['geofence_address'] = $dataGeo['user_geo_address'];
                            $geoArray['geofence_range'] = $dataGeo['user_geofence_range'];
                            $geoArray['geofence_longitude'] = $dataGeo['user_geofence_longitude'];
                            $geoArray['geofence_latitude'] = $dataGeo['user_geofence_latitude'];

                            array_push($punchIn['location_list'],$geoArray);

                        }
                    }

                    $blockQry = $d->select("block_master","society_id = '$society_id' AND block_geofence_latitude != '' AND block_geofence_longitude != '' AND block_id = '$block_id'");

                    if (mysqli_num_rows($blockQry) > 0) {

                        $data = mysqli_fetch_array($blockQry);

                        $geoArray['geofence_address'] = "Company Location";
                        $geoArray['geofence_latitude'] = $data['block_geofence_latitude'];
                        $geoArray['geofence_longitude'] = $data['block_geofence_longitude'];
                        $geoArray['geofence_range'] = $data['block_geofence_range'];

                        array_push($punchIn['location_list'],$geoArray);
                    }

                    $wfhQry = $d->selectRow("wfh_address,wfh_longitude,wfh_latitude,wfh_attendance_range","wfh_master","user_id = '$data[user_id]' AND wfh_start_date = '$attendance_date_start' AND wfh_status = '1' AND society_id = '$society_id'");

                    if (mysqli_num_rows($wfhQry) > 0) {
                        while($dataGeo = mysqli_fetch_array($wfhQry)){


                            $geoArray['geofence_address'] = 'WFH Location: '.$dataGeo['wfh_address'];
                            $geoArray['geofence_range'] = $dataGeo['wfh_attendance_range'];
                            $geoArray['geofence_longitude'] = $dataGeo['wfh_longitude'];
                            $geoArray['geofence_latitude'] = $dataGeo['wfh_latitude'];

                            array_push($punchIn['location_list'],$geoArray);

                        }
                    }


                    $userQry = $d->select("users_master","user_id = '$user_id' AND society_id = '$society_id' AND user_geofence_latitude != '' AND user_geofence_longitude != '' AND user_geofence_range != ''");

                    if (mysqli_num_rows($userQry) == 0) {

                        $blockQry = $d->select("block_master","society_id = '$society_id' AND block_geofence_latitude != '' AND block_geofence_longitude != '' AND block_id = '$block_id'");

                        if (mysqli_num_rows($blockQry) > 0) {

                            $data = mysqli_fetch_array($blockQry);

                            $punchIn['geofence_latitude'] = $data['block_geofence_latitude'];
                            $punchIn['geofence_longitude'] = $data['block_geofence_longitude'];
                            $punchIn['geofence_range'] = $data['block_geofence_range'];
                        }
                    }else{

                        $data = mysqli_fetch_array($userQry);

                        $punchIn['geofence_latitude'] = $data['block_geofence_latitude'];
                        $punchIn['geofence_longitude'] = $data['block_geofence_longitude'];
                        $punchIn['geofence_range'] = $data['block_geofence_range'];
                    }

                    array_push($response['pending_attendance'],$punchIn);
                }

                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found";
                $response["status"] = "201";
                echo json_encode($response);
            } 
                    
        }else if($_POST['approvePendingAttendance']=="approvePendingAttendance" && $attendance_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_status',"1");
            $m->set_data('attendance_status_change_by_type',"0");
            $m->set_data('attendance_status_change_by',$user_id);
            $m->set_data('pending_attendance_approve_date',date('Y-m-d H:i:s'));
            $m->set_data('out_of_range_req_approve_date',date('Y-m-d H:i:s'));

            $a = array(
                'attendance_status'=>$m->get_data('attendance_status'),
                'attendance_status_change_by_type'=>$m->get_data('attendance_status_change_by_type'),
                'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
                'pending_attendance_approve_date'=>$m->get_data('pending_attendance_approve_date'),
                'out_of_range_req_approve_date'=>$m->get_data('out_of_range_req_approve_date'),
            );

            $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id' AND user_id = '$attendance_user_id'");

            if ($punchInQry == true) {

                $title = "Your Attendance is Approved";
                $description = "For Date: ".date('dS F Y',strtotime($attendance_date_start))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","users_master.user_id = '$attendance_user_id'");

                $response["message"] = "Attendance Approved";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['declinePendingAttendance']=="declinePendingAttendance" && $attendance_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_status',"2");
            $m->set_data('attendance_status_change_by',$user_id);
            $m->set_data('avg_working_days',"0");
            $m->set_data('attendance_declined_reason',$attendance_declined_reason);
            $m->set_data('attendance_rejected_date',date('Y-m-d H:i:s'));

            $a = array(
                'attendance_status'=>$m->get_data('attendance_status'),
                'attendance_status_change_by'=>$m->get_data('attendance_status_change_by'),
                'avg_working_days'=>$m->get_data('avg_working_days'),
                'attendance_declined_reason'=>$m->get_data('attendance_declined_reason'),
                'attendance_rejected_date'=>$m->get_data('attendance_rejected_date'),
            );

            $punchInQry = $d->update("attendance_master",$a,"attendance_id = '$attendance_id' AND user_id = '$attendance_user_id'");

            if ($punchInQry == true) {

                $attQry = $d->selectRow("attendance_date_start","attendance_master","attendance_id = '$attendance_id'");
                $attData = mysqli_fetch_array($attQry);
                $attendance_date_start = $attData['attendance_date_start'];

                $d->delete("leave_master","leave_start_date = '$attendance_date_start' AND user_id = '$attendance_user_id'");

                $title = "Your Attendance is Rejected";
                $description = "For Date: ".date('dS F Y',strtotime($attendance_date_start))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                $response["message"] = "Attendance Rejected";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['attendancePunchOut']=="attendancePunchOut" && $attendance_id!='' && $user_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){
            $response["message"] = "New version of MyCo is available on Play Store, please update your MyCo app";
            $response["status"] = "201";
            echo json_encode($response);
            exit(); 
        }else if($_POST['punchOutMissingRequest']=="punchOutMissingRequest" && $attendance_id!='' && $user_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $inReqDate = date('m-Y',strtotime($attDate));

            $qry = $d->count_data_direct("salary_slip_id","salary_slip_master","user_id = '$user_id' AND salary_month_name = '$inReqDate'");

            if ($qry > 0) {
                $response["message"] = "This month's salary was already generated. Therefor, you can not apply punch out missing request";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

            $qq = $d->count_data_direct("attendance_punch_out_missing_id","attendance_punch_out_missing_request","user_id = '$user_id' AND attendance_id = '$attendance_id'");

            if ($qq > 0) {
                $response["message"] = "Punch Out Missing Request Already Sent";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }  


            if ($attDate == "0000-00-00" || $attDate == "00-00-0000") {
                $response["message"] = "Invalid Date, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }  

            /*if ($attDate == null || $attDate == '' || $attDate == " ") {
                
                $attQry = $d->selectRow("*","attendance_master","attendance_id = '$attendance_id'");
                $attData = mysqli_fetch_array($attQry);

                $sq = $d->selectRow("users_master.shift_time_id as sti,shift_type","users_master,shift_timing_master","users_master.user_id = '$user_id' AND shift_timing_master.shift_time_id = users_master.shift_time_id");

                $sdata = mysqli_fetch_array($sq);

                if ($sdata['sti'] != null && $sdata['sti'] > 0) {
                    if ($sdata['shift_type'] == "Night") {
                        $attDate = date('Y-m-d', strtotime($attData['attendance_date_start'] . ' +1 day'));
                    }else{
                        $attDate = $attData['attendance_date_start'];
                    }
                } 

            }*/


            $m->set_data('attendance_id',$attendance_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('attendance_date',$attDate);
            $m->set_data('attendance_time',$attendance_time);
            $m->set_data('punch_out_missing_reason',$punch_out_missing_reason);
            $m->set_data('record_created_date',date('Y-m-d H:i:s'));

            $a = array(
                'attendance_id'=>$m->get_data('attendance_id'),
                'user_id'=>$m->get_data('user_id'),
                'attendance_date'=>$m->get_data('attendance_date'),
                'attendance_time'=>$m->get_data('attendance_time'),
                'punch_out_missing_reason'=>$m->get_data('punch_out_missing_reason'),
                'record_created_date'=>$m->get_data('record_created_date'),
            );

            $punchOutQry = $d->insert("attendance_punch_out_missing_request",$a);

            if ($punchOutQry == true) {

                $title = "Punch Out Missing Request";
                $description = "Requested by, ".$user_name."\nDate: ".date('d-m-Y',strtotime($attDate))." Time: ".date('h:i A',strtotime($attendance_time));

                // Notification by Access

                $access_type = $punch_out_missing_req_access;

                include 'check_access_data_user.php';

                if (count($userIDArray) > 0) {

                    $fcmAccessAry=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmAccessAryiOS=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $nResident->noti("punch_out_missing_request","",$society_id,$fcmAccessAry,$title,$description,$tabPosition);
                    $nResident->noti_ios("punch_out_missing_request","",$society_id,$fcmAccessAryiOS,$title,$description,$tabPosition);

                    $d->insertUserNotification($society_id,$title,$description,"punch_out_missing_request","attendance_tracker.png","users_master.user_id IN ('$userFcmIds')");
                }


                // To Admin

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("18",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("18",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"punchOutMissingRequest");

                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"punchOutMissingRequest");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i:s'),
                  'notification_action'=>"punchOutMissingRequest",
                  'admin_click_action '=>"punchOutMissingRequest",
                  'notification_logo'=>'attendance_tracker.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

                $response["message"] = "Request Sent";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Error";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['getPunchOutMissingRequests']=="getPunchOutMissingRequests" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $access_type = $punch_out_missing_req_access;

            include "check_access_data.php";

            if ($userIds=='') {
                switch ($access_for) {
                    case '0':
                    case '1':
                        $appendQuery = " AND users_master.block_id IN ('$accessBranchIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '2':
                        $appendQuery = " AND users_master.floor_id IN ('$accessDepIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    case '3':
                        $appendQuery = " AND attendance_punch_out_missing_request.user_id IN ('$accessUserIds')";
                        $LIMIT_DATA = " LIMIT 100";
                        break;
                    
                    default:
                        
                        break;
                }
            }else{
                $appendQuery = " AND attendance_punch_out_missing_request.user_id IN ($userIds)";
            }

            $response['modification_access'] = $modification_access;

            $punchInReqQry = $d->select("attendance_master,attendance_punch_out_missing_request,floors_master,block_master,users_master","
                attendance_master.attendance_id = attendance_punch_out_missing_request.attendance_id 
                AND attendance_punch_out_missing_request.attendance_punch_out_missing_status = '0'  
                AND users_master.user_id = attendance_master.user_id 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id $appendQuery","ORDER BY attendance_punch_out_missing_request.attendance_punch_out_missing_id DESC $LIMIT_DATA");

            if(mysqli_num_rows($punchInReqQry)>0){

                $response["punch_out_missing_requests"] = array();        
                
                while($data=mysqli_fetch_array($punchInReqQry)) {

                    $data = array_map("html_entity_decode", $data);

                    $punchIn = array();

                    $punchIn['user_id'] = $data['user_id'];
                    $punchIn['user_full_name'] = $data['user_full_name'];
                    $punchIn['branch_name'] = $data['block_name'];
                    $punchIn['department_name'] = $data['floor_name'];
                    $punchIn['user_designation'] = $data['user_designation'];
                    $punchIn['attendance_id'] = $data['attendance_id'];
                    $punchIn['attendance_punch_out_missing_id'] = $data['attendance_punch_out_missing_id'];
                    $punchIn['block_id'] = $data['block_id'];
                    $punchIn['floor_id'] = $data['floor_id'];
                    $punchIn['unit_id'] = $data['unit_id'];
                    $punchIn['society_id'] = $data['society_id'];
                    $punchIn['late_in'] = $data['late_in'];
                    $punchIn['early_out'] = $data['early_out'];
                    $punchIn['punch_in_address'] = $data['punch_in_address'].'';
                    $punchIn['punch_out_address'] = $data['punch_out_address'].'';

                    if ($data['user_profile_pic'] != '') {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                    } else {
                        $punchIn["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $punchIn['attendance_date_start'] = $data['attendance_date_start'];
                    $punchIn['attendance_date_start_view'] = date('D, dS F Y',strtotime($data['attendance_date_start']));

                    $punchIn['attendance_date_end'] = $data['attendance_date'];
                    $punchIn['attendance_date_end_view'] = date('D, dS F Y',strtotime($data['attendance_date']));
                    $punchIn['punch_in_time'] = $data['punch_in_time'];
                    $punchIn['punch_in_time_view'] = date('h:i A',strtotime($data['punch_in_time']));
                    $punchIn['punch_out_time'] = $data['attendance_time'];
                    $punchIn['punch_out_time_view'] = date('h:i A',strtotime($data['attendance_time']));
                    $punchIn['punch_out_missing_reason'] = $data['punch_out_missing_reason'];

                    $totalHours = getTotalHoursWithNames($data['attendance_date_start'],$data['attendance_date'],
                                $data['punch_in_time'],$data['attendance_time']);
                    $hr = getTotalHours($data['attendance_date_start'],$data['attendance_date'],$data['punch_in_time'],$data['attendance_time']);
                    $time = explode(':', $hr.":00");
                    $total_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

                    $punchIn['total_hours'] = $totalHours;
                    $punchIn['total_minutes'] = $total_minutes.'';

                    array_push($response['punch_out_missing_requests'],$punchIn);
                }
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "No data found";
                $response["status"] = "201";
                echo json_encode($response);
            }             
        }else if($_POST['approvePunchOutRequest']=="approvePunchOutRequest" && $attendance_id!='' && $attendance_punch_out_missing_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            $response["message"] = "New version of MyCo is available on Play Store, please update your MyCo app";
            $response["status"] = "201";
            echo json_encode($response);
            exit();
                   
        }else if($_POST['declinePunchOutRequest']=="declinePunchOutRequest" && $attendance_id!=''  && $attendance_punch_out_missing_id!='' && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_punch_out_missing_status',"2");
            $m->set_data('attendance_punch_out_missing_status_changed_by',$user_id);
            $m->set_data('punch_out_missing_reject_reason',$punch_out_missing_reject_reason);
            $m->set_data('rejected_date',date('Y-m-d H:i:s'));

            $a = array(
                'attendance_punch_out_missing_status'=>$m->get_data('attendance_punch_out_missing_status'),
                'attendance_punch_out_missing_status_changed_by'=>$m->get_data('attendance_punch_out_missing_status_changed_by'),
                'punch_out_missing_reject_reason'=>$m->get_data('punch_out_missing_reject_reason'),
                'rejected_date'=>$m->get_data('rejected_date'),
            );

            $qry = $d->update("attendance_punch_out_missing_request",$a,"attendance_id = '$attendance_id' AND attendance_punch_out_missing_id = '$attendance_punch_out_missing_id'");

            if ($qry == true) {

                $title = "Punch Out Missing Request Rejected";
                $description = "For Date: ".date('d-m-Y',strtotime($attendance_date))." Time: ".date('h:i A',strtotime($attendance_time))."\nBy, ".$user_name;

                $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$attendance_user_id'");
                $data_notification = mysqli_fetch_array($qsm);

                $user_token = $data_notification['user_token'];
                $device = $data_notification['device'];

                $tabPosition = "1";

                if ($device == 'android') {
                    $nResident->noti("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                } else if ($device == 'ios') {
                    $nResident->noti_ios("attendance","",$society_id,$user_token,$title,$description,$tabPosition);
                }

                $d->insertUserNotification($society_id,$title,$description,"attendance","attendance_tracker.png","user_id = '$attendance_user_id'");

                $response["message"] = "Request Rejected";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['pendingAttendanceReminder']=="pendingAttendanceReminder" && $attendance_id!='' && $user_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $title = "Pending Attendance Approval Reminder";
            $description = "Requested by, ".$user_name;

            $access_type = $pending_attendance_access;

            include 'check_access_data_user.php';

            if (count($userIDArray) > 0) {

                $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                $dataAry = array(
                    "block_id"=>$block_id,
                    "floor_id"=>$floor_id,
                    "user_id"=>$user_id,
                );

                $dataJson = json_encode($dataAry);

                $nResident->noti("pending_attendance","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                $nResident->noti_ios("pending_attendance","",$society_id,$fcmArrayIosPA,$title,$description,$dataJson);

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"pending_attendance","attendance_tracker.png",$dataJson,"users_master.user_id IN ('$userFcmIds')");
            }

            // To Admin Panel

            $block_id=$d->getBlockid($user_id);
            $fcmArray=$d->selectAdminBlockwise("17",$block_id,"android");
            $fcmArrayIos=$d->selectAdminBlockwise("17",$block_id,"ios");
      
            $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"pendingAttendance");
            $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"pendingAttendance");

            $notiAry = array(
              'society_id'=>$society_id,
              'notification_tittle'=>$title,
              'notification_description'=>$description,
              'notifiaction_date'=>date('Y-m-d H:i'),
              'notification_action'=>"pendingAttendance",
              'admin_click_action '=>"pendingAttendance",
              'notification_logo'=>'attendance_tracker.png',
            );
                        
            $qry = $d->insert("admin_notification",$notiAry);

            if ($qry == true) {
                $response["message"] = "Request Sent";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong, please try again!";
                $response["status"] = "201";
                echo json_encode($response);
            } 
        }else if($_POST['breakIn']=="breakIn" && $attendance_id!='' && filter_var($attendance_id, FILTER_VALIDATE_INT) == true){

            $m->set_data('attendance_id',$attendance_id);
            $m->set_data('attendance_type_id',$attendance_type_id);
            $m->set_data('break_start_date',date('Y-m-d'));
            $m->set_data('break_in_time',date('H:i:s'));
            $m->set_data('break_in_latitude',$break_in_latitude);
            $m->set_data('break_in_longitude',$break_in_longitude);

            $a = array(
                'attendance_id'=>$m->get_data('attendance_id'),
                'attendance_type_id' =>$m->get_data('attendance_type_id'),
                'break_start_date' =>$m->get_data('break_start_date'),
                'break_in_time'=>$m->get_data('break_in_time'),
                'break_in_latitude'=>$m->get_data('break_in_latitude'),
                'break_in_longitude'=>$m->get_data('break_in_longitude')
            );

            $punchInQry = $d->insert("attendance_break_history_master",$a);

            $attendance_break_history_id = $con->insert_id;

            if ($punchInQry == true) {
                $response["message"] = "Success";
                $response["attendance_break_history_id"] = $attendance_break_history_id."";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['breakOut']=="breakOut" && $attendance_break_history_id!='' && $user_id!='' && filter_var($attendance_break_history_id, FILTER_VALIDATE_INT) == true){

            $qq = $d->selectRow("break_start_date,break_in_time","attendance_break_history_master","attendance_break_history_id = '$attendance_break_history_id'");

            $qqData = mysqli_fetch_array($qq);

            $breakStartDate = $qqData['break_start_date'];
            $breakStartTime = $qqData['break_in_time'];

            $breakEndDate = date('Y-m-d');
            $breakEndTime = date('H:i:s');

            $datePunchIN = $breakStartDate ." ".$breakStartTime;
            $datePunchIN = date('Y-m-d H:i:s',strtotime("+1 minutes",strtotime($datePunchIN)));
            $datePunchOUT = $breakEndDate ." ".$breakEndTime;

            if ($datePunchOUT < $datePunchIN) {
                $response["message"] = "Immediate end of the break is restricted, please end the break after 1 minute.";
                $response["status"] = "202";
                echo json_encode($response);
                exit();
            } 

            $total_break_time = getTotalHours($breakStartDate,$breakEndDate,
                            $breakStartTime,$breakEndTime);

            $time = explode(':', $total_break_time.":00");
            $total_break_time_minutes = ($time[0]*60) + ($time[1]) + ($time[2]/60);

            $m->set_data('break_end_date',$breakEndDate);
            $m->set_data('break_out_time',$breakEndTime);
            $m->set_data('break_out_latitude',$break_out_latitude);
            $m->set_data('break_out_longitude',$break_out_longitude);
            $m->set_data('total_break_time',$total_break_time);
            $m->set_data('total_break_time_minutes',$total_break_time_minutes);

            $a = array(
                'break_end_date'=>$m->get_data('break_end_date'),
                'break_out_time'=>$m->get_data('break_out_time'),
                'break_out_latitude'=>$m->get_data('break_out_latitude'),
                'break_out_longitude'=>$m->get_data('break_out_longitude'),
                'total_break_time'=>$m->get_data('total_break_time'),
                'total_break_time_minutes'=>$m->get_data('total_break_time_minutes'),
            );

            $breakOut = $d->update("attendance_break_history_master",$a,"attendance_break_history_id = '$attendance_break_history_id'");

            if ($breakOut == true) {
                $response["message"] = "Success";
                $response["status"] = "200";
                echo json_encode($response);
            }else{
                $response["message"] = "Something went wrong";
                $response["status"] = "201";
                echo json_encode($response);
            }        
        }else if($_POST['checkRadius']=="checkRadius" && $user_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            if ($block_id == null || $block_id == '' || $block_id == ' ' || $block_id == '0') {
                
                $blockIdQry = $d->selectRow("block_id","users_master","user_id = '$user_id'");
                
                if (mysqli_num_rows($blockIdQry) > 0) {
                    $blockData = mysqli_fetch_array($blockIdQry);
                    $block_id = $blockData['block_id'];
                }
            }

            $isRadiusON = false;

            $userQry = $d->select("users_master","user_id = '$user_id' AND society_id = '$society_id' AND user_geofence_latitude != '' AND user_geofence_longitude != '' AND user_geofence_range != ''");

            if (mysqli_num_rows($userQry) == 0) {

                $blockQry = $d->select("block_master","society_id = '$society_id' AND block_geofence_latitude != '' AND block_geofence_longitude != '' AND block_id = '$block_id'");

                if (mysqli_num_rows($blockQry) > 0) {

                    $isRadiusON = true;

                    $data = mysqli_fetch_array($blockQry);

                    $geofence_latitude = $data['block_geofence_latitude'];
                    $geofence_longitude = $data['block_geofence_longitude'];
                    $geofence_range = $data['block_geofence_range'];
                }else{
                    $isRadiusON = false;
                }
            }else{

                $isRadiusON = true;

                $data = mysqli_fetch_array($userQry);

                $geofence_latitude = $data['user_geofence_latitude'];
                $geofence_longitude = $data['user_geofence_longitude'];
                $geofence_range = $data['user_geofence_range'];
            }

            $tDate = date('Y-m-d');

            $wfhQry = $d->selectRow("wfh_id,user_id,wfh_start_date,wfh_latitude,wfh_longitude,wfh_attendance_range","wfh_master","society_id ='$society_id' AND user_id='$user_id' AND wfh_status = '1' AND wfh_start_date = '$tDate'");
            $wfhData = mysqli_fetch_array($wfhQry);

            if (mysqli_num_rows($wfhQry) > 0) {

                $geofence_latitude = $wfhData['wfh_latitude'];
                $geofence_longitude = $wfhData['wfh_longitude'];
                $geofence_range = $wfhData['wfh_attendance_range'];

                if ($wfhData['wfh_status'] == '1') {
                    $isRadiusON = true;
                }
            }


            if ($isRadiusON) {

                $radiusInMeeter=  $d->haversineGreatCircleDistance($user_latitude,$user_longitude,$geofence_latitude, $geofence_longitude);

                $totalKm= number_format($radiusInMeeter,2,'.','');

                $response['radiusInMeter'] = $totalKm;

                $dist = ($totalKm/1000);

                $KM = number_format($dist,2,'.','');

                $response['totalKm'] = $KM." KM";

                if ($user_latitude=="0.0" || $user_latitude=="") {
                    $response['take_punch_in_reason'] = true;
                    $response["message"] = "Invalid Location";
                    $response["status"] = "201";
                    echo json_encode($response);
                }else if($totalKm <= $geofence_range){
                    $response['take_punch_in_reason'] = false;
                    $response["message"] = "You are in range";
                    $response["status"] = "200";
                    echo json_encode($response);
                }else{
                    $response['take_punch_in_reason'] = true;
                    $response["message"] = "You are out of range";
                    $response["status"] = "200";
                    echo json_encode($response);
                }
            }else{
                $response['take_punch_in_reason'] = false;
                $response["message"] = "No Radius Checking Required";
                $response["status"] = "200";
                echo json_encode($response);
            }             
        }else if($_POST['getWeeklyAttendanceHistory']=="getWeeklyAttendanceHistory" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            $response["message"] = "New version of MyCo is available on Play Store, please update your MyCo app";
            $response["status"] = "201";
            echo json_encode($response);
            exit();
        
        }else if($_POST['getMonthlyAttendanceHistory']=="getMonthlyAttendanceHistory" && $user_id!='' && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){
            $response["message"] = "New version of MyCo is available on Play Store, please update your MyCo app";
            $response["status"] = "201";
            echo json_encode($response);
            exit();
        
        }else if($_POST['getAbsentPresentData']=="getAbsentPresentData" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            if ($floor_id != null && $floor_id !='') {
                $appendQuery = " AND users_master.floor_id = '$floor_id'";
            }

            $tdt = date('Y-m-d');

            $usersQry=$d->select("users_master,block_master,floors_master,shift_timing_master","
                users_master.delete_status = '0' 
                AND users_master.society_id = '$society_id' 
                AND users_master.floor_id = floors_master.floor_id 
                AND users_master.block_id = block_master.block_id 
                AND users_master.user_status = '1'
                AND users_master.shift_time_id = shift_timing_master.shift_time_id
                AND users_master.floor_id = '$floor_id'","ORDER BY user_sort ASC");

            $monthWeekNo = ceil(date( 'j', strtotime($tdt) ) / 7 );
            $todayWeekNo = date('w');

            if (mysqli_num_rows($usersQry) > 0) {

                $response['present_employee'] = array();
                $response['absent_employee'] = array();
                
                while ($userData = mysqli_fetch_array($usersQry)) {

                    $userData = array_map("html_entity_decode", $userData);

                    $has_altenate_week_off = $userData['has_altenate_week_off'];

                    $weekOffArray = explode(",",$userData['week_off_days']);

                    $alternate_week_off = explode(",",$userData['alternate_week_off']);
                    $alternate_weekoff_days = explode(",",$userData['alternate_weekoff_days']);

                    $users = array();

                    $userId = $userData['user_id'];

                    $users['user_name'] = $userData['user_full_name'];
                    $users['branch_name'] = $userData['block_name'];
                    $users['department_name'] = $userData['floor_name'];
                    $users['user_designation'] = $userData['user_designation'];
                    
                    if ($userData['user_profile_pic'] != '') {
                        $users["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $userData['user_profile_pic'];
                    } else {
                        $users["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                    }

                    $leaveQry = $d->selectRow("leave_id,leave_day_type,half_day_session","leave_master","leave_start_date = '$tdt' AND user_id = '$userId'");

                    $leaveData = mysqli_fetch_array($leaveQry);

                    if ($leaveData['leave_id'] != '' && $leaveData['leave_day_type'] == "1") {
                        $users['half_day'] = true;
                        $users['leave_message'] = "On Leave : Half Day";

                        if ($leaveData['half_day_session'] == 1) {
                            $users['half_day_type'] = "(First Half)";
                        }else if ($leaveData['half_day_session'] == 2) {
                            $users['half_day_type'] = "(Second Half)";
                        }else{
                            $users['half_day_type'] = "";
                        }
                    }else if ($leaveData['leave_id'] != '' && $leaveData['leave_day_type'] == "0") {
                        $users['full_day'] = true;
                        $users['half_day'] = false;  
                        $users['half_day_type'] = "";
                        $users['leave_message'] = "On Leave : Full Day";                
                    }else{
                        $users['half_day'] = false;  
                        $users['full_day'] = false;
                        $users['half_day_type'] = "";   
                        $users['leave_message'] = "";                      
                    }

                    $attendanceQry = $d->selectRow("*","attendance_master","attendance_date_start = '$tdt' AND user_id = '$userId'");

                    if(mysqli_num_rows($attendanceQry)>0){
                        
                        $users['is_on_break'] = false;
                        $users['break_type'] = "";
                       
                        $attendanceData = mysqli_fetch_array($attendanceQry);

                        $users['punch_in_time'] = date('h:i A',strtotime($attendanceData['punch_in_time']));
                        $users['punch_out_time'] = date('h:i A',strtotime($attendanceData['punch_out_time']));
                        
                        if ($attendanceData['punch_in_image'] != '') {
                            $users["punch_in_image"] = $base_url . "img/attendance/" . $attendanceData['punch_in_image'];
                        } else {
                            $users["punch_in_image"] = "";
                        }

                        if ($attendanceData['punch_out_image'] != '') {
                            $users["punch_out_image"] = $base_url . "img/attendance/" . $attendanceData['punch_out_image'];
                        } else {
                            $users["punch_out_image"] = "";
                        }

                        
                        if ($attendanceData['punch_out_time'] == "00:00:00" && $attendanceData['attendance_date_end']) {
                            $users['punched_out'] = false;
                        }else{
                            $users['punched_out'] = true;
                        }                        

                        $attendanceId = $attendanceData['attendance_id'];

                        $attendanceHistoryQry = $d->selectRow("*","attendance_master,attendance_break_history_master,attendance_type_master","attendance_master.attendance_id = attendance_break_history_master.attendance_id AND attendance_master.attendance_id = '$attendanceId' AND attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id AND attendance_break_history_master.break_out_time = '00:00:00' AND  attendance_break_history_master.attendance_type_id = attendance_type_master.attendance_type_id AND attendance_break_history_master.break_end_date = '0000-00-00'","ORDER BY attendance_break_history_master.attendance_break_history_id DESC LIMIT 1");

                        if (mysqli_num_rows($attendanceHistoryQry) > 0) {

                            $attendanceBreakData = mysqli_fetch_array($attendanceHistoryQry);

                            $users['is_on_break'] = true;
                            $users['break_type'] = $attendanceBreakData['attendance_type_name'];
                        }

                        array_push($response['present_employee'],$users);
                    }else if ($has_altenate_week_off == 0 && !in_array($todayWeekNo,$weekOffArray)) {

                        array_push($response['absent_employee'],$users);
                    }else if ($has_altenate_week_off == 1 && !in_array($monthWeekNo,$alternate_week_off) && in_array($todayWeekNo,$alternate_weekoff_days)) {
                        array_push($response['absent_employee'],$users);
                    }

                }
                $response['status'] = "200";
                $response['message'] = "success";
                echo json_encode($response);
            }else{
                $response['status'] = "201";
                $response['message'] = "No data";
                echo json_encode($response);
            }        
        }else if($_POST['checkQRCode']=="checkQRCode" && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

            $newDateTime = date('Ymdhis',strtotime('+25 seconds'));
            $currentTimeStamp = date('Ymdhis',strtotime('-10 seconds'));

            if ($date_time >= $currentTimeStamp && $date_time <= $newDateTime) {
                $response["message"]="QR Code Verified";
                $response["status"]="200";
                echo json_encode($response);
            }else{
                $response["message"]="Invalid QR Code";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="Wrong tag.";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="Wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }
}

function getTotalWeekHours($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    // returns the time already formatted
    return sprintf('%02d.%02d', $hours, $minutes);
}

function getTotalWeekHoursName($times) {
    $minutes = 0; //declare minutes either it gives Notice: Undefined variable
    // loop throught all the times
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }

    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "No Data";
    }
}

function range_date($first, $last) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now <= $last ) {
    $arr[] = date('Y-m-d', $now);
    $now = strtotime('+1 day', $now);
  }

  return $arr;
}

function getTotalHours($startDate, $endDate, $startTime, $endTime) {

    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    return sprintf('%02d:%02d', $hours, $minutes);
}


function getTotalHoursWithNames($startDate, $endDate, $startTime, $endTime) {
   
    $sDTime = $startDate." ".$startTime;
    $eDTime = $endDate." ".$endTime;

    $pTime = date('Y-m-d h:i A',strtotime($sDTime));
    $eTime = date('Y-m-d h:i A',strtotime($eDTime));

    $date_a = new DateTime($pTime);
    $date_b = new DateTime($eTime);

    $interval = $date_a->diff($date_b);
   
    $days = $interval->format('%d')*24;
    $hours = $interval->format('%h');
    $hours = $hours+$days;
    $minutes = $interval->format('%i');
    $sec = $interval->format('%s');

    if ($hours > 0 && $minutes) {
        return sprintf('%02d hr %02d min', $hours, $minutes);
    }else if ($hours > 0 && $minutes <= 0) {
        return sprintf('%02d hr', $hours);
    }else if ($hours <= 0 && $minutes > 0) {
        return sprintf('%02d min', $minutes);
    }else{
        return "No Data";
    }

}

function weekOfMonth($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return weekOfYear($date) - weekOfYear($firstOfMonth) + 1;
}

function weekOfYear($date) {
    $weekOfYear = intval(date("W", $date));
    if (date('n', $date) == "1" && $weekOfYear > 51) {
        // It's the last week of the previos year.
        return 0;
    }
    else if (date('n', $date) == "12" && $weekOfYear == 1) {
        // It's the first week of the next year.
        return 53;
    }
    else {
        // It's a "normal" week.
        return $weekOfYear;
    }
}

function weeks_in_month($month, $year){
    $dates = [];

    $week = 1;
    $date = new DateTime("$year-$month-01");
    $days = (int)$date->format('t'); // total number of days in the month

    $oneDay = new DateInterval('P1D');

    for ($day = 1; $day <= $days; $day++) {
        $dates["Week_$week"] []= $date->format('Y-m-d');

        $dayOfWeek = $date->format('l');
        if ($dayOfWeek === 'Saturday') {
            $week++;
        }

        $date->add($oneDay);
    }

    return $dates;
}

function minutes($time){
    $time = explode(':', $time);
    return ($time[0]*60) + ($time[1]) + ($time[2]/60);
}

function hoursandmins($time, $format = '%02d:%02d')
{
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
?>