<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if (isset($_POST)){
	extract(array_map("test_input", $_POST));
	$response = array();

	if($_POST['getRetailerDashboard']=="getRetailerDashboard" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

		$appendQuery = "";
		$appendQuery2 = "";

		if ($filter_type == 0) {

			$filterDate = date('Y-m-d');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 1) {

	   		$filterDate = date('Y-m-d',strtotime('-1 days'));
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 2) {

	   		$year = date('Y');
	   		$week = date('W',strtotime(date('Y-m-d')));
	   		
	   		$time = strtotime("1 January $year", time());
			$day = date('w', $time);
			$time += ((7*$week)+1-$day)*24*3600;
			$startDate = date('Y-m-d', $time);
			$time += 6*24*3600;
			$endDate = date('Y-m-d', $time);

			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";

	   	}else if ($filter_type == 3) {

	   		$currentMonth = date('Y-m');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m') = '$currentMonth'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m') = '$currentMonth'";
	   	}

	   
   		$totalAmount = $d->sum_data("order_total_amount","retailer_order_master","order_by_user_id = '$user_id' AND order_status = '1' $appendQuery");

   		$totalDuration = $d->sum_data("retailer_visit_duration","retailer_daily_visit_timeline_master","retailer_visit_by_id = '$user_id' $appendQuery2");

   		$totalQuantity = $d->sum_data("total_order_product_qty","retailer_order_master","order_by_user_id = '$user_id' AND order_status = 1 $appendQuery");

   		$totalVisits = $d->count_data_direct("retailer_daily_visit_timeline_id","retailer_daily_visit_timeline_master","retailer_visit_by_id = '$user_id' AND retailer_visit_start_datetime IS NOT NULL $appendQuery2");

		$totalOrders = $d->selectRow("retailer_order_id","retailer_order_master","order_by_user_id = '$user_id' AND order_status = '1' $appendQuery","GROUP BY retailer_id,DATE_FORMAT(order_date,'%Y-%m-%d')");

		$tOrder = $d->count_data_direct("retailer_order_id","retailer_order_master","order_by_user_id = '$user_id' AND order_status = '1' $appendQuery");

		$totalOrders = mysqli_num_rows($totalOrders);

		$response['total_orders'] = $tOrder.'';

		if ($totalOrders > 0) {
			$totalProductivity = ($totalOrders * 100) / $totalVisits;

			if ($totalProductivity > 100) {
				$totalProductivity = "100";
			}
		}else{
			$totalProductivity = 0;
		}

		$totalAmountData=mysqli_fetch_array($totalAmount);

        if ($totalAmountData['SUM(order_total_amount)'] != '') {
	        $response['total_sales'] = $totalAmountData['SUM(order_total_amount)'].'';
		}else{
			$response['total_sales'] = "0";
		}

		$totalQuantityData=mysqli_fetch_array($totalQuantity);
		if ($totalQuantityData['SUM(total_order_product_qty)'] != '') {
	        $response['total_quantity'] = $totalQuantityData['SUM(total_order_product_qty)'].'';
		}else{
			$response['total_quantity'] = "0";
		}

		$totalDurationData=mysqli_fetch_array($totalDuration);
	    $totalSecounds = $totalDurationData['SUM(retailer_visit_duration)'].'';

	    $totalVisitDuration = gmdate("H:i:s", $totalSecounds);
        $response['total_visits'] = $totalVisits.'';
        $response['total_visit_duration'] = $totalVisitDuration.'';
        $response['productivity'] = number_format($totalProductivity,2,'.','').'';

        // Top Retailers

        $qry = $d->selectRow("
        	retailer_order_master.retailer_order_id,
        	retailer_order_master.retailer_id,
        	retailer_master.retailer_name,
        	SUM(retailer_order_master.order_total_amount)
        	","
        	retailer_order_master,
        	retailer_master
        	","
        	retailer_order_master.society_id = '$society_id' 
        	AND retailer_order_master.order_by_user_id = '$user_id'
        	AND retailer_order_master.order_status!= 3
        	AND retailer_order_master.retailer_id = retailer_master.retailer_id $appendQuery
        	","
        	GROUP BY retailer_order_master.retailer_id ORDER BY retailer_order_master.order_total_amount DESC LIMIT 3");

		$response["top_retailers"] = array();

		if (mysqli_num_rows($qry) > 0) {
		
			while($data = mysqli_fetch_array($qry)){
				$topRetailer = array();

				$retailer_order_id = $data['retailer_order_id'];

				$topRetailer['retailer_order_id'] = $retailer_order_id;
				$topRetailer['retailer_id'] = $data['retailer_id'];
				$topRetailer['retailer_name'] = $data['retailer_name'];
				$topRetailer['total_amount'] = $data['SUM(retailer_order_master.order_total_amount)'];
				
				array_push($response["top_retailers"],$topRetailer);
			}
		}

		// Top Routes

        $qry = $d->selectRow("
        	route_master.route_id,
        	route_master.route_name,
        	SUM(retailer_order_master.order_total_amount)
        	","
        	retailer_order_master,
        	retailer_master,
        	route_master,
        	route_retailer_master
        	","
        	retailer_order_master.society_id = '$society_id' 
        	AND retailer_order_master.order_by_user_id = '$user_id'
        	AND retailer_order_master.retailer_id = retailer_master.retailer_id
        	AND route_retailer_master.retailer_id = retailer_master.retailer_id
        	AND route_retailer_master.route_id = route_master.route_id
        	AND retailer_order_master.retailer_id = route_retailer_master.retailer_id 
        	AND retailer_order_master.order_status!= 3 $appendQuery
        	","
        	GROUP BY retailer_order_master.retailer_id ORDER BY retailer_order_master.order_total_amount DESC LIMIT 3");

		$response["top_routes"] = array();


		if (mysqli_num_rows($qry) > 0) {
		
			while($data = mysqli_fetch_array($qry)){
				$topRetailer = array();

				$topRetailer['route_id'] = $data['route_id'];
				$topRetailer['route_name'] = $data['route_name'];
				$topRetailer['total_amount'] = $data['SUM(retailer_order_master.order_total_amount)'];
				
				array_push($response["top_routes"],$topRetailer);
			}
		}

        $response["success"] = "200";
		$response["message"] = "Success";
		echo json_encode($response);
    }else if($_POST['getChartOrder']=="getChartOrder" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

		$appendQuery = "";

		if ($filter_type == 0) {

			$filterDate = date('Y-m-d');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery1 = " AND DATE_FORMAT(retailer_no_order_master.no_order_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 1) {

	   		$filterDate = date('Y-m-d',strtotime('-1 days'));
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery1 = " AND DATE_FORMAT(retailer_no_order_master.no_order_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 2) {

	   		$year = date('Y');
	   		$week = date('W',strtotime(date('Y-m-d')));
	   		
	   		$time = strtotime("1 January $year", time());
			$day = date('w', $time);
			$time += ((7*$week)+1-$day)*24*3600;
			$startDate = date('Y-m-d', $time);
			$time += 6*24*3600;
			$endDate = date('Y-m-d', $time);

			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";
			$appendQuery1 = " AND DATE_FORMAT(retailer_no_order_master.no_order_created_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";

	   	}else if ($filter_type == 3) {

	   		$currentMonth = date('Y-m');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m') = '$currentMonth'";
			$appendQuery1 = " AND DATE_FORMAT(retailer_no_order_master.no_order_created_date,'%Y-%m') = '$currentMonth'";
	   	}

		// Top Routes

        $qry = $d->selectRow("
			route_master.*,
			route_employee_assign_master.route_assign_week_days
			"," 
			route_master,
			route_employee_assign_master
			","
			route_master.route_active_status = '0' 
			AND route_master.society_id = '$society_id' 
			AND route_employee_assign_master.user_id = '$user_id' 
			AND route_employee_assign_master.route_id = route_master.route_id");

        $topOrder = 0;
        $topNoOrder = 0;

		if (mysqli_num_rows($qry) > 0) {
		
			$response["routes"] = array();

			while($data = mysqli_fetch_array($qry)){
				$routes = array();

				$route_id = $data['route_id'];

				$routes['route_id'] = $route_id;
				$routes['route_name'] = $data['route_name'];

				$qry1 = $d->selectRow("
		        	route_master.route_id,
		        	route_master.route_name,
		        	count(retailer_order_master.retailer_order_id)
		        	","
		        	retailer_order_master,
		        	retailer_master,
		        	route_master,
		        	route_retailer_master
		        	","
		        	retailer_order_master.society_id = '$society_id' 
		        	AND retailer_order_master.order_by_user_id = '$user_id'
		        	AND retailer_order_master.retailer_id = retailer_master.retailer_id
		        	AND route_retailer_master.retailer_id = retailer_master.retailer_id
		        	AND route_retailer_master.retailer_id = retailer_order_master.retailer_id
		        	AND route_retailer_master.route_id = route_master.route_id
		        	AND route_master.route_id = '$route_id'
		        	AND retailer_order_master.order_status = '1'
		        	AND retailer_order_master.retailer_id = route_retailer_master.retailer_id $appendQuery
		        	","
		        	GROUP BY route_master.route_id,route_retailer_master.retailer_id ORDER BY count(retailer_order_master.retailer_order_id) DESC");

				$routes['total_order'] = '0';
		

				while($qry1Data=mysqli_fetch_array($qry1)){
					if ($qry1Data['count(retailer_order_master.retailer_order_id)'] > 0) {
						$order = $qry1Data['count(retailer_order_master.retailer_order_id)'];
						if ($order > $topOrder) {
							$topOrder = $order;
							$response['top_order'] = $topOrder."";
						}
				        $routes['total_order'] = $order."";
					}else{
				        $routes['total_order'] = "0";
					}
				}
				
				$qqry = $d->selectRow("
		        	retailer_no_order_master.retailer_no_order_id
		        	","
		        	retailer_no_order_master,
		        	route_master,
		        	retailer_master,
		        	route_retailer_master
		        	","
		        	retailer_no_order_master.visit_by_user_id = '$user_id' 
		        	AND route_master.route_id = route_retailer_master.route_id 
		        	AND route_retailer_master.retailer_id = retailer_master.retailer_id 
		        	AND retailer_master.retailer_id = retailer_no_order_master.retailer_id 
		        	AND route_master.route_id = '$route_id' $appendQuery1
		        	");

				$noOrder = mysqli_num_rows($qqry);
				$routes['total_no_order'] = mysqli_num_rows($qqry)."";

				if ($noOrder > $topNoOrder) {
					$topNoOrder = $noOrder;
					$response['top_no_order'] = $topNoOrder."";
				}else{
					$response['top_no_order'] = "0";
				}

				array_push($response["routes"],$routes);
			}
		}

        $response["success"] = "200";
		$response["message"] = "Success";
		echo json_encode($response);
    }else if($_POST['getChartSales']=="getChartSales" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

		$appendQuery = "";

		if ($filter_type == 0) {

			$filterDate = date('Y-m-d');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 1) {

	   		$filterDate = date('Y-m-d',strtotime('-1 days'));
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 2) {

	   		$year = date('Y');
	   		$week = date('W',strtotime(date('Y-m-d')));
	   		
	   		$time = strtotime("1 January $year", time());
			$day = date('w', $time);
			$time += ((7*$week)+1-$day)*24*3600;
			$startDate = date('Y-m-d', $time);
			$time += 6*24*3600;
			$endDate = date('Y-m-d', $time);

			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";

	   	}else if ($filter_type == 3) {

	   		$currentMonth = date('Y-m');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m') = '$currentMonth'";
	   	}

		// Top Routes

        $qry = $d->selectRow("
        	route_master.route_id,
        	route_master.route_name,
        	SUM(retailer_order_master.order_total_amount)
        	","
        	retailer_order_master,
        	retailer_master,
        	route_master,
        	route_retailer_master
        	","
        	retailer_order_master.society_id = '$society_id' 
        	AND retailer_order_master.order_by_user_id = '$user_id'
        	AND retailer_order_master.retailer_id = retailer_master.retailer_id
        	AND route_retailer_master.retailer_id = retailer_master.retailer_id
        	AND route_retailer_master.route_id = route_master.route_id
        	AND retailer_order_master.order_status = '1'
        	AND retailer_order_master.retailer_id = route_retailer_master.retailer_id $appendQuery
        	","
        	GROUP BY retailer_order_master.retailer_id ORDER BY retailer_order_master.order_total_amount DESC LIMIT 10");

		$response["chart_routes"] = array();

		$totalSum = 0;

		if (mysqli_num_rows($qry) > 0) {
		
			while($data = mysqli_fetch_array($qry)){
				$topRetailer = array();

				$topRetailer['route_id'] = $data['route_id'];
				$topRetailer['route_name'] = $data['route_name'];
				$topRetailer['total_amount'] = number_format($data['SUM(retailer_order_master.order_total_amount)'],2,'.','');

				$totalSum = $totalSum + $data['SUM(retailer_order_master.order_total_amount)'];

				
				array_push($response["chart_routes"],$topRetailer);
			}
		}

        $response["success"] = "200";
        $response["final_amount"] = number_format($totalSum,2,'.','').'';
		$response["message"] = "Success";
		echo json_encode($response);
    }else if($_POST['getChartProductivity']=="getChartProductivity" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

		$appendQuery = "";
		$appendQuery2 = "";
		$appendQuery3 = "";

		if ($filter_type == 0) {

			$filterDate = date('Y-m-d');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery3 = " AND DATE_FORMAT(no_order_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 1) {

	   		$filterDate = date('Y-m-d',strtotime('-1 days'));
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery3 = " AND DATE_FORMAT(no_order_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 2) {

	   		$year = date('Y');
	   		$week = date('W',strtotime(date('Y-m-d')));
	   		
	   		$time = strtotime("1 January $year", time());
			$day = date('w', $time);
			$time += ((7*$week)+1-$day)*24*3600;
			$startDate = date('Y-m-d', $time);
			$time += 6*24*3600;
			$endDate = date('Y-m-d', $time);

			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";
			$appendQuery3 = " AND DATE_FORMAT(no_order_created_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";

	   	}else if ($filter_type == 3) {

	   		$currentMonth = date('Y-m');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m') = '$currentMonth'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m') = '$currentMonth'";
			$appendQuery3 = " AND DATE_FORMAT(no_order_created_date,'%Y-%m') = '$currentMonth'";
	   	}

	   	$totalVisits = $d->count_data_direct("retailer_daily_visit_timeline_id","retailer_daily_visit_timeline_master","retailer_visit_by_id = '$user_id' AND retailer_visit_start_datetime IS NOT NULL AND retailer_visit_end_datetime IS NOT NULL $appendQuery2");

		$totalOrders = $d->selectRow("retailer_order_id","retailer_order_master","order_by_user_id = '$user_id' AND order_status = '1' $appendQuery","GROUP BY retailer_id, DATE_FORMAT(order_date,'%Y-%m-%d')");

		$tOrder = $d->count_data_direct("retailer_order_id","retailer_order_master","order_by_user_id = '$user_id' AND order_status = '1' $appendQuery");

   		$totalCancelledOrders = $d->count_data_direct("retailer_order_id","retailer_order_master","retailer_visit_id = '$user_id' AND order_status = '3' $appendQuery");

   		$totalNoOrders = $d->count_data_direct("retailer_no_order_id","retailer_no_order_master","visit_by_user_id = '$user_id' $appendQuery3");

   		$totalOrders = mysqli_num_rows($totalOrders);

        $response["total_visits"] = $totalVisits."";
        $response["total_orders"] = $tOrder."";
        $response["total_cancelled"] = $totalCancelledOrders."";
        $response["total_no_orders"] = $totalNoOrders."";

        if ($totalVisits>0) {

        	if ($totalOrders > 0) {
				$totalOrderPercentage = ($totalOrders * 100) / $totalVisits;

				if ($totalOrderPercentage > 100) {
					$totalOrderPercentage = "100";
				}
			}else{
				$totalOrderPercentage = 0;
			}

	        $totalNoOrdersPercentage = ($totalNoOrders*100)/$totalVisits;
	        $totalCancelOrderPercentage = ($totalCancelledOrders*100)/$totalVisits;
        } else {
        	$totalOrderPercentage = "0";
        	$totalNoOrdersPercentage = "0";
        	$totalCancelOrderPercentage = "0";
        }

        $response['total_order_percentage'] = number_format($totalOrderPercentage,2,'.','').'';
        $response['total_no_order_percentage'] = number_format($totalNoOrdersPercentage,2,'.','').'';
        $response['total_cancel_order_percentage'] = number_format($totalCancelOrderPercentage,2,'.','').'';

        $response["success"] = "200";
		$response["message"] = "Success";
		echo json_encode($response);
    }else if($_POST['getChartQuantity']=="getChartQuantity" && filter_var($society_id, FILTER_VALIDATE_INT) == true){ 

		$appendQuery = "";
		$appendQuery2 = "";
		$appendQuery3 = "";

		if ($filter_type == 0) {

			$filterDate = date('Y-m-d');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_start_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery3 = " AND DATE_FORMAT(no_order_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 1) {

	   		$filterDate = date('Y-m-d',strtotime('-1 days'));
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_start_date,'%Y-%m-%d') = '$filterDate'";
			$appendQuery3 = " AND DATE_FORMAT(no_order_created_date,'%Y-%m-%d') = '$filterDate'";

	   	}else if ($filter_type == 2) {

	   		$year = date('Y');
	   		$week = date('W',strtotime(date('Y-m-d')));
	   		
	   		$time = strtotime("1 January $year", time());
			$day = date('w', $time);
			$time += ((7*$week)+1-$day)*24*3600;
			$startDate = date('Y-m-d', $time);
			$time += 6*24*3600;
			$endDate = date('Y-m-d', $time);

			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_start_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";
			$appendQuery3 = " AND DATE_FORMAT(no_order_created_date,'%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";

	   	}else if ($filter_type == 3) {

	   		$currentMonth = date('Y-m');
			$appendQuery = " AND DATE_FORMAT(retailer_order_master.order_date,'%Y-%m') = '$currentMonth'";
			$appendQuery2 = " AND DATE_FORMAT(retailer_visit_start_date,'%Y-%m') = '$currentMonth'";
			$appendQuery3 = " AND DATE_FORMAT(no_order_created_date,'%Y-%m') = '$currentMonth'";
	   	}

	   	$qry = $d->selectRow("
        	retailer_order_master.retailer_order_id,
        	retailer_product_master.product_name,
        	retailer_product_variant_master.product_variant_name,
        	unit_measurement_master.unit_measurement_name,
        	SUM(retailer_order_product_master.product_quantity)
        	","
        	retailer_order_master,
        	retailer_order_product_master,
        	retailer_product_master,
        	unit_measurement_master,
        	retailer_product_variant_master
        	","
        	retailer_order_master.society_id = '$society_id' 
        	AND retailer_order_master.order_by_user_id = '$user_id'
        	AND retailer_order_master.retailer_order_id = retailer_order_product_master.retailer_order_id 
        	AND retailer_product_variant_master.product_variant_id = retailer_order_product_master.product_variant_id 
        	AND retailer_product_variant_master.product_id = retailer_order_product_master.product_id 
        	AND retailer_product_variant_master.product_id = retailer_product_master.product_id 
        	AND retailer_order_master.order_status = '1'
        	AND retailer_product_variant_master.unit_measurement_id = unit_measurement_master.unit_measurement_id 
        	$appendQuery
        	","
        	GROUP BY retailer_order_product_master.product_variant_id ORDER BY SUM(retailer_order_product_master.product_quantity) DESC LIMIT 10");

		$response["products"] = array();

		$totalSum = 0;

		if (mysqli_num_rows($qry) > 0) {
		
			while($data = mysqli_fetch_array($qry)){
				$products = array();

				$amount = number_format($data['SUM(retailer_order_product_master.product_quantity)'],2,'.','');

				$products['product_name'] = $data['product_name'];
				$products['product_variant_name'] = $data['product_variant_name'];
				$products['total_quantity'] = $amount;
				$products['unit_measurement_name'] = $data['unit_measurement_name'];

				$totalSum = $totalSum + $amount;
				
				array_push($response["products"],$products);
			}
		}

        $response["final_quantity"] = number_format($totalSum,2,'.','');
        $response["success"] = "200";
		$response["message"] = "Success";
		echo json_encode($response);
    }else{
		$response["success"] = "201";
		$response["message"] = "Invalid Tag !";
		echo json_encode($response);
	}
} else {
	$response["success"] = "201";
	$response["message"] = "Invalid API Key !";
	echo json_encode($response);
}

?>