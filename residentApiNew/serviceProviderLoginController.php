<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
	extract(array_map("test_input" , $_POST));
	if(isset($_POST["loginSp"])) {
		extract(array_map("test_input" , $_POST)); 
		$mobile=mysqli_real_escape_string($con, $mobile);
		if (strlen($mobile)<10) {
		  $response["message"]="Please Enter Valid Mobile Number";
		  $response["status"]="201";
		  echo json_encode($response);
		  exit();
		}
		$q=$d->select("local_service_provider_users","service_provider_phone='$mobile' AND country_code='$country_code' AND service_provider_delete_status = '0' AND service_provider_status = '0'");
		$data = mysqli_fetch_array($q); 
		if ($data > 0) {

			if ($data['service_provider_status']==1) {
				$response["message"]="Account Not Activated.";
				$response["status"]="201";
				echo json_encode($response);
				exit();
			}

			$mobile=$data['service_provider_phone'];
			$login_name=$data['service_provider_name'];
			if ($is_six_digit=='yes') {
				$digits = 6;
			}else {
				$digits = 4;
			}
	        $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
			$token = bin2hex(openssl_random_pseudo_bytes(16));
			$m->set_data('mobile',$mobile);
			$m->set_data('otp',$otp);
			$m->set_data('date',date('Y-m-d H:i:s'));
			
			$a1= array (
				'otp'=> $m->get_data('otp'),
			);
	        $message = $otp." is your OTP to login Service Provider App";
	        if ($country_code=="+91") {
			    $smsObj->send_otp($mobile,$otp,$country_code);
	        } else {
				$response["message"]="Service Not Available";
				$response["status"]="201";
				echo json_encode($response);
				exit();
			}
			$d->update('local_service_provider_users',$a1,"service_provider_phone='$mobile'");
			
	        

			$response["message"]="OTP Send Successfully";
			$response["status"]="200";
			echo json_encode($response);
			exit();
		}else {
			 $response["message"]="Mobile Number Not Register";
	          $response["status"]="201";
	          echo json_encode($response);
	          exit();
		}
	} else if (isset($sp_verify) && $sp_verify == 'sp_verify' && filter_var($otp, FILTER_VALIDATE_INT) == true ) {

		if ($country_code!="") {
			$appendQuery = " AND country_code='$country_code'";
		}

		$mobile = mysqli_real_escape_string($con, $mobile);
		$otp = mysqli_real_escape_string($con, $otp);
		
		if ($mobile=="9737564998") {
			$q=$d->select("local_service_provider_users","service_provider_phone='$mobile' AND otp!='0' $appendQuery");
		} else {
			$q=$d->select("local_service_provider_users","service_provider_phone='$mobile' AND otp='$otp' AND otp!='0' $appendQuery");

		}
		if(mysqli_num_rows($q) >0){
			$user_data = mysqli_fetch_array($q);

			$otpupdata['otp'] = null;

			$d->update('local_service_provider_users',$otpupdata,"service_provider_phone='$mobile'");

				if ($user_data == TRUE && mysqli_num_rows($q) == 1) {
						$oldToken = $user_data['token'];
						$device = $user_data['device'];
						if ($device=="Android" || $device=="android") {
						$nResident->noti("","",'0',$oldToken,"Logout","Logout from anothere device","");
						}else {
						$nResident->noti_ios("","",'0',$oldToken,"Logout","Logout from anothere device","");
						}

						$notiCount= $d->select("local_service_provider_notifications","service_provider_users_id='$user_data[service_provider_users_id]' AND read_status=0");

						$m->set_data('token', $fcm_token);
						$a = array('token' => $m->get_data('token'),
							'sp_device' => $sp_device,
							'sp_brand' => $sp_brand,
							'sp_model' => $sp_model,
							'token_date' => date('Y-m-d'),
						);
						$d->update("local_service_provider_users", $a, "service_provider_phone='$mobile' ");

						$response["service_provider_users_id"] = $user_data['service_provider_users_id'];
						$response["service_provider_name"] = html_entity_decode($user_data['service_provider_name']);
						$response["service_provider_address"] = html_entity_decode($user_data['service_provider_address']);
						$response["service_provider_zipcode"] = $user_data['service_provider_zipcode'];
						$response["service_provider_latitude"] = $user_data['service_provider_latitude'];
						$response["service_provider_logitude"] = $user_data['service_provider_logitude'];
						$response["service_provider_phone"] = $user_data['service_provider_phone'];
						$response["country_code"] = $user_data['country_code'];

						if ($user_data['service_provider_whatsapp']>0) {
							# code...
						$response["service_provider_whatsapp"] = $user_data['service_provider_whatsapp'];
						$response["service_provider_whatsapp_country_code"] = $user_data['service_provider_whatsapp_country_code'];

						$response["service_provider_whatsapp_with_code"] = $user_data['service_provider_whatsapp_country_code'].' '.$user_data['service_provider_whatsapp'];;
						} else {
							$response["service_provider_whatsapp"] = "";
							$response["service_provider_whatsapp_country_code"] ="";

							$response["service_provider_whatsapp_with_code"] = "";
						}


						$response["service_provider_email"] = $user_data['service_provider_email'];
						
						$response["contact_person_name"] = $user_data['contact_person_name'];
						$response["sp_webiste"] = $user_data['sp_webiste'];
						$response["work_description"] = $user_data['work_description'];
						

						$response["country_id"] = $user_data['country_id'];
						$response["state_id"] = $user_data['state_id'];
						$response["city_id"] = $user_data['city_id'];
						$response["service_provider_start_date"] = $user_data['service_provider_start_date'];
						$response["service_provider_end_date"] = $user_data['service_provider_end_date'];
						$response["open_time"] = date("H:i", strtotime($user_data['open_time']));
						$response["close_time"] = date("H:i", strtotime($user_data['close_time']));

						$spCategoryArray = array();
						 $qcd=$d->select("local_service_provider_users_category,local_service_provider_master","local_service_provider_master.local_service_provider_id=local_service_provider_users_category.category_id AND  local_service_provider_users_category.service_provider_users_id='$user_data[service_provider_users_id]'");
					      while ($catData=mysqli_fetch_array($qcd)) {
					      	array_push($spCategoryArray, html_entity_decode($catData['service_provider_category_name']));
					      }

						$response["sp_category"] = $spCategoryArray;


						$response["unread_notification"]= mysqli_num_rows($notiCount);

						$response["opening_time_view"] =  date('h:i A', strtotime($user_data['open_time']))." to ". date('h:i A', strtotime($user_data['close_time']));
						$response["is_kyc"] = (int)$user_data['kyc_status'];
						

						if ($user_data['id_proof']!="") {
							$response["id_proof"] = $base_url . "img/local_service_provider/local_service_id_proof/" . $user_data['id_proof'];
						} else {
							$response["id_proof"] = "";
						}
						if ($user_data['location_proof']!="") {
							$response["location_proof"] = $base_url . "img/local_service_provider/local_service_location_proof/" . $user_data['location_proof'];
						} else {
							$response["location_proof"] = "";
						}
						if ($user_data['service_provider_user_image']!="") {
							$response["service_provider_user_image"] = $base_url."img/local_service_provider/local_service_users/".$user_data['service_provider_user_image'];
						} else {
							$response["service_provider_user_image"] = "";
						}
						if ($user_data['brochure_profile']!="") {
							$response["brochure_profile"] = $base_url."img/local_service_provider/local_service_location_proof/".$user_data['brochure_profile'];
						} else {
							$response["brochure_profile"] ="";
						}
			

						$local_service_provider_id = $user_data['service_provider_users_id'];
			            /**************************** Average Rating ************************************/
			            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' " );
			            $average = 0;
			            while ( $avg = mysqli_fetch_array($data)) {
			              $average = $average + $avg['rating_point'];
			            }
			            $totalRating = mysqli_num_rows($data);
			            if($totalRating>0){
			              $averagerating = $average/$totalRating;
			              $response['totalRatings'] = number_format($totalRating);
			              $response['averageRating'] = number_format($averagerating,1);
			            }else{
			              $response['totalRatings'] = '0';
			              $response['averageRating'] = '0';          
			            }
						$response["message"] = "Login Successfully";
						$response["status"] = "200";
						echo json_encode($response);

				} else {
					$response["message"] = "Invalid User";
					$response["status"] = "201";
					echo json_encode($response);
				}
			
		}else{
			$response["message"] = "OTP not match, Please try again";
			$response["status"] = "201";
			echo json_encode($response);
		}
	}else if (isset($sp_verify_change_mobile) && $sp_verify_change_mobile == 'sp_verify_change_mobile' && filter_var($otp, FILTER_VALIDATE_INT) == true ) {


		if ($country_code!="") {
			$appendQuery = " AND country_code='$country_code'";
		}

		$mobile = mysqli_real_escape_string($con, $mobile);
		$otp = mysqli_real_escape_string($con, $otp);
		
		$qci=$d->selectRow("service_provider_phone","local_service_provider_users","service_provider_phone='$mobile' $appendQuery");
	    if (mysqli_num_rows($qci)>0) {
	     $response["message"]="Mobile Number Already Register";
	     $response["status"]="201";
		 echo json_encode($response);
		 exit();
	    }

		$q=$d->select("local_service_provider_users","service_provider_users_id='$service_provider_users_id' AND otp='$otp' AND otp!='0'");
		if(mysqli_num_rows($q) >0){
			$user_data = mysqli_fetch_array($q);

			$a1= array (
				'otp'=> "",
				'service_provider_phone'=> $mobile,
				'country_code'=> $country_code,
			);
			
			$d->update('local_service_provider_users',$a1,"service_provider_users_id='$service_provider_users_id'");

			$response["message"] = "Mobile Number Successfully Changed";
			$response["status"] = "200";
			echo json_encode($response);
			
		}else{
			$response["message"] = "OTP not match, Please try again";
			$response["status"] = "201";
			echo json_encode($response);
		}
	}else if (isset($getRating) && $getRating == 'getRating' && filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true ) {
		 $q=$d->select("local_service_providers_ratings","local_service_provider_id='$service_provider_users_id'","");
		if(mysqli_num_rows($q)>0){
			$response["ratings"] = array();
			while($data=mysqli_fetch_array($q)) {
				$ratings = array();
				$ratings["user_name"]=$data['user_name'];
				$ratings["rating_date"]=$data['rating_date'];
				$ratings["rating_point"]=html_entity_decode($data['rating_point']);
				$ratings["rating_comment"]=html_entity_decode($data['rating_comment']);
				array_push($response["ratings"], $ratings);
			}
			$response["message"]="get rating successfully.";
			$response["status"]="200";
			echo json_encode($response);
		}else{
			$response["message"]="No Rating Found.";
			$response["status"]="201";
			echo json_encode($response);
		}
	}else if (isset($getProfileDetails) && $getProfileDetails == 'getProfileDetails' && filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true ) {
		$q=$d->select("local_service_provider_users","service_provider_users_id='$service_provider_users_id' AND service_provider_status=0","");
		$user_data = mysqli_fetch_array($q);
		if ($user_data == TRUE && mysqli_num_rows($q) == 1) {

			$notiCount= $d->select("local_service_provider_notifications","service_provider_users_id='$user_data[service_provider_users_id]' AND read_status=0");

			$response["service_provider_users_id"] = $user_data['service_provider_users_id'];
			$response["service_provider_name"] = html_entity_decode($user_data['service_provider_name']);
			$response["service_provider_address"] = html_entity_decode($user_data['service_provider_address']);
			$response["service_provider_zipcode"] = $user_data['service_provider_zipcode'];
			$response["service_provider_latitude"] = $user_data['service_provider_latitude'];
			$response["service_provider_logitude"] = $user_data['service_provider_logitude'];
			$response["service_provider_phone"] = $user_data['service_provider_phone'];
			$response["country_code"] = $user_data['country_code'];
			
			if ($user_data['service_provider_whatsapp']>0) {
							# code...
			$response["service_provider_whatsapp"] = $user_data['service_provider_whatsapp'];
			$response["service_provider_whatsapp_country_code"] = $user_data['service_provider_whatsapp_country_code'];
			$response["service_provider_whatsapp_with_code"] = $user_data['service_provider_whatsapp_country_code'].' '.$user_data['service_provider_whatsapp'];
			} else {
				$response["service_provider_whatsapp"] = "";
				$response["service_provider_whatsapp_country_code"] ="";

				$response["service_provider_whatsapp_with_code"] = "";
			}

			$response["service_provider_email"] = $user_data['service_provider_email'];

			$response["contact_person_name"] = $user_data['contact_person_name'];
			$response["sp_webiste"] = $user_data['sp_webiste'];
			$response["work_description"] = $user_data['work_description'];
			$response["gst_no"] = $user_data['gst_no'];
						
			$response["country_id"] = $user_data['country_id'];
			$response["state_id"] = $user_data['state_id'];
			$response["city_id"] = $user_data['city_id'];
			$response["service_provider_start_date"] = $user_data['service_provider_start_date'];
			$response["service_provider_end_date"] = $user_data['service_provider_end_date'];
			$response["open_time"] = date("H:i", strtotime($user_data['open_time']));
			$response["close_time"] = date("H:i", strtotime($user_data['close_time']));

			$spCategoryArray = array();
		 $qcd=$d->select("local_service_provider_users_category,local_service_provider_master","local_service_provider_master.local_service_provider_id=local_service_provider_users_category.category_id AND  local_service_provider_users_category.service_provider_users_id='$user_data[service_provider_users_id]'");
	      while ($catData=mysqli_fetch_array($qcd)) {
	      	array_push($spCategoryArray, html_entity_decode($catData['service_provider_category_name']));
	      }

		$response["sp_category"] = $spCategoryArray;

			$response["unread_notification"]= mysqli_num_rows($notiCount);

			$response["opening_time_view"] =  date('h:i A', strtotime($user_data['open_time']))." to ". date('h:i A', strtotime($user_data['close_time']));
			$response["is_kyc"] = (int)$user_data['kyc_status'];


			if ($user_data['id_proof']!="") {
				$response["id_proof"] = $base_url . "img/local_service_provider/local_service_id_proof/" . $user_data['id_proof'];
			} else {
				$response["id_proof"] = "";
			}
			if ($user_data['location_proof']!="") {
				$response["location_proof"] = $base_url . "img/local_service_provider/local_service_location_proof/" . $user_data['location_proof'];
			} else {
				$response["location_proof"] = "";
			}
			if ($user_data['contact_person_photo']!="") {
				$response["contact_person_photo"] = $base_url."img/local_service_provider/local_service_users/".$user_data['contact_person_photo'];
			} else {
				$response["contact_person_photo"] = "";
			}

			if ($user_data['service_provider_user_image']!="") {
				$response["service_provider_user_image"] = $base_url."img/local_service_provider/local_service_users/".$user_data['service_provider_user_image'];
			} else {
				$response["service_provider_user_image"] = "";
			}
			
			if ($user_data['visiting_card']!="") {
				$response["visiting_card"] = $base_url."img/local_service_provider/local_service_users/".$user_data['visiting_card'];
			} else {
				$response["visiting_card"] ="";
			}

			if ($user_data['brochure_profile']!="") {
				$response["brochure_profile"] = $base_url."img/local_service_provider/local_service_location_proof/".$user_data['brochure_profile'];
			} else {
				$response["brochure_profile"] ="";
			}
			

			$local_service_provider_id = $user_data['service_provider_users_id'];
            /**************************** Average Rating ************************************/
            $data = $d->select('local_service_providers_ratings',"local_service_provider_id='$local_service_provider_id' AND status=0" );
            $average = 0;
            while ( $avg = mysqli_fetch_array($data)) {
              $average = $average + $avg['rating_point'];
            }
            $totalRating = mysqli_num_rows($data);
            if($totalRating>0){
              $averagerating = $average/$totalRating;
              $response['totalRatings'] = number_format($totalRating);
              $response['averageRating'] = number_format($averagerating,1);
            }else{
              $response['totalRatings'] = '0';
              $response['averageRating'] = '0';          
            }

            $ab= array (
				'app_version_code'=> $app_version_code,
				'phone_model'=> $phone_model.'-'.$phone_brand,
				'device'=> $device,
				'last_access_time'=> date("Y-m-d H:i:s")
			);
			
			$d->update('local_service_provider_users',$ab,"service_provider_users_id='$user_data[service_provider_users_id]'");

			$response["version_code"] = "1";
			$response["message"] = "Success";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
	    	$response["message"] = "Service Provider Not Found.";
			$response["status"] = "201";
			echo json_encode($response);
	    }
	}else if (isset($getComplains) && $getComplains == 'getComplains' && filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true ) {
		$q=$d->select("local_service_provider_complains,society_master","local_service_provider_complains.local_service_provider_id='$service_provider_users_id' AND local_service_provider_complains.status = '0' AND local_service_provider_complains.society_id=society_master.society_id","ORDER BY local_service_provider_complains.complain_id DESC");
		if(mysqli_num_rows($q)>0){
			$response["complains"] = array();
			while($data=mysqli_fetch_array($q)) {
				$complains = array();
				$complains["local_service_provider_id"]=$data['local_service_provider_id'];
				$complains["society_id"]=$data['society_id'];
				$complains["society_name"]=html_entity_decode($data['society_name']);
				$complains["society_address"]=html_entity_decode($data['society_address']);
				$complains["user_id"]=$data['user_id'];
				$complains["user_name"]=$data['user_name'];
				$complains["user_mobile"]=$data['country_code'].' '.$data['user_mobile'];
				$complains["comment"]=$data['comment'];
				$complains["complain_date"]=$data['complain_date'];
				$complains["status"]=$data['status'];
				array_push($response["complains"], $complains);
			}
			$response["message"]="get complains successfully.";
			$response["status"]="200";
			echo json_encode($response);
		}else{
			$response["message"]="No Complains Found.";
			$response["status"]="201";
			echo json_encode($response);
		}
	}else if (isset($getCallRequests) && $getCallRequests == 'getCallRequests' && filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true ) {
		if ($call_request_id!='' || $call_request_id) {
		$q=$d->select("local_serviceproviders_call_request,society_master","local_serviceproviders_call_request.society_id=society_master.society_id AND local_serviceproviders_call_request.call_request_id='$call_request_id'","ORDER BY local_serviceproviders_call_request.call_request_id DESC");
		} else {
		$q=$d->select("local_serviceproviders_call_request,society_master","local_serviceproviders_call_request.society_id=society_master.society_id","ORDER BY local_serviceproviders_call_request.call_request_id DESC");

		}
		if(mysqli_num_rows($q)>0){
			$response["requests"] = array();
			$i = 0;
			while($data=mysqli_fetch_array($q)) {
				$spid = explode(',',$data['local_service_provider_users_id']);
				if(in_array($service_provider_users_id, $spid)){
					$requests = array();
					$requests["call_request_id"]=$data['call_request_id'];
					$requests["local_service_provider_id"]=$service_provider_users_id;
					$requests["society_id"]=$data['society_id'];
					$requests["society_name"]=html_entity_decode($data['society_name']);
					$requests["society_address"]=html_entity_decode($data['society_address']);
					$requests["society_latitude"]=$data['society_latitude'];
					$requests["society_longitude"]=$data['society_longitude'];
					$requests["user_id"]=$data['user_id'];
					$requests["user_name"]=$data['user_name'];
					$requests["user_mobile"]=$data['country_code'].' '.$data['user_mobile'];
					if ($data['request_date']!='') {
						$request_date=  date("d M Y h:i A", strtotime($data['request_date']));
					} else {
						$request_date="";
					}
					$requests["request_date"]=$request_date;
					$q1 = $d->select("local_service_providers_call_request_action","call_request_id='$data[call_request_id]' and local_service_provider_users_id='$service_provider_users_id'","");
					$data4=mysqli_fetch_array($q1);
					if($q1->num_rows==0){ $status = "Pending";}
                    elseif ($data4['active_status']==0) { $status = "Going For Service";}
                    elseif ($data4['active_status']==1) { $status = "Complete";}
                    elseif ($data4['active_status']==2) { $status = "Rejected";}
                    elseif ($data4['active_status']==3) { $status = "In Progress";}
                    elseif ($data4['active_status']==4) { $status = "Not Interested";}
                    $requests["action_status"]=$status;
                    if($q1->num_rows==0 ){
                    	$action['value']['0'] = "0";
                    	$action['name']['0'] = "Going For Service";
                    	$action['value']['1'] = "2";
                    	$action['name']['1'] = "Rejected";
                    	$action['value']['2'] = "4";
                    	$action['name']['2'] = "Not Interested";
	                }else if($data4['active_status']==0){
	                	$action['value']['0'] = "3";
                    	$action['name']['0'] = "In Progress";
                    	$action['value']['1'] = "2";
                    	$action['name']['1'] = "Rejected";
                    	$action['value']['2'] = "4";
                    	$action['name']['2'] = "Not Interested";
	                }else if($data4['active_status']==3){
	                	$action['value']['0'] = "1";
                    	$action['name']['0'] = "Completed";
	                }else{
	                	$action['value'] = array();
                    	$action['name'] = array();
	                }
	                $requests["action_options"]=$action;
	                $requests["otp"]=$data4['otp'];
					array_push($response["requests"], $requests);
					$i++;
				}
			}
			if($i>0){
				$response["message"]="get requests successfully.";
				$response["status"]="200";
				echo json_encode($response);
			}else{
				$response["message"]="No Requests Found.";
				$response["status"]="201";
				echo json_encode($response);
			}
		}else{
			$response["message"]="No Requests Found.";
			$response["status"]="201";
			echo json_encode($response);
		}
	}else if (isset($callRequestAction) && $callRequestAction == 'callRequestAction' && filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true ) {
		$q1 = $d->select("local_service_providers_call_request_action","call_request_id='$call_request_id' and local_service_provider_users_id='$service_provider_users_id'","");
		$id = $action_options_value;
		$call_id = $call_request_id;
		if($q1->num_rows==0){
			if($id==3){
				$s = $d->select("local_serviceproviders_call_request","call_request_id='$call_id'","");
				$userdetails = mysqli_fetch_array($s);
				$usermobile = $userdetails['user_mobile'];
				$message = "Your OTP is ".$otpdata[otp].". Share this OTP to Service Provider at the time of service.";
				if($send_sms=='true') {
					$d->send_sms($usermobile,$message);
					$response["message"]="OTP Send on $usermobile";
					$response["status"]="200";
					echo json_encode($response);
					exit();
				} 
				$otpdata = mysqli_fetch_array($q1);
				if($otp!=$otpdata['otp']){
					$response["message"]="Invalid OTP";
					$response["status"]="201";
					echo json_encode($response);
					exit();
				}
			}
			$m->set_data('call_request_id',$call_id);
			$m->set_data('local_service_provider_users_id',$service_provider_users_id);
			$m->set_data('active_status',$id);
			if($id=='0'){
				$s = $d->select("local_serviceproviders_call_request","call_request_id='$call_id'","");
				$userdetails = mysqli_fetch_array($s);
				$usermobile = $userdetails['user_mobile'];
				$society_id = $userdetails['society_id'];
				$digits = 6;
				$otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
				// $message = "Your OTP is ".$otp.". Share this OTP to Service Provider at the time of service.";
				// $d->send_sms($usermobile,$message);
				// $m->set_data('otp',$otp);

				$title = "OTP : $otp";
				$description = "Share this OTP to $sp_name Service Provider at the time of service.";
			
				$qc=$d->select("society_master","society_id='$userdetails[society_id]'");
    			$data=mysqli_fetch_array($qc);

    			$callUr= $data['sub_domain'].'residentApiNew/sp_service.php';

				$usermobile = $userdetails['user_mobile'];
				$m->set_data('otp',$otp);
				$message = "Your OTP is ".$otp.". Share this OTP to Service Provider at the time of service.";
				
				$ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL,$callUr);
			    curl_setopt($ch, CURLOPT_POST, 1);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			    curl_setopt($ch, CURLOPT_POSTFIELDS,"otp=$otp & society_id=$society_id & sendFcm=sendFcm & user_mobile=$usermobile & sp_name=$service_provider_name & title=$title & description=$description");

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    $server_output = curl_exec($ch);
			    curl_close ($ch);
			    $json = json_decode($server_output, true);
			}
			$idarray['local_service_provider_users_id'] = $m->get_data('local_service_provider_users_id');
			$idarray['active_status'] = $m->get_data('active_status');
			$idarray['call_request_id'] = $m->get_data('call_request_id');
			if($id=='0'){
				$idarray['otp'] = $m->get_data('otp');
			}
			if($id==4){
				$s = $d->select("local_serviceproviders_call_request","call_request_id='$call_id'","");
				$userdetails = mysqli_fetch_array($s);
				$usermobile = $userdetails['user_mobile'];
				$society_id = $userdetails['society_id'];
				
				$title = "$service_provider_name Not Interested ";
				$description = "For Provide Service.";
			
				$qc=$d->select("society_master","society_id='$userdetails[society_id]'");
    			$data=mysqli_fetch_array($qc);
    			$callUr= $data['sub_domain'].'residentApiNew/sp_service.php';

				$usermobile = $userdetails['user_mobile'];
				
				$ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL,$callUr);
			    curl_setopt($ch, CURLOPT_POST, 1);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			    curl_setopt($ch, CURLOPT_POSTFIELDS,"otp=$otp & society_id=$society_id & sendFcm=sendFcm & user_mobile=$usermobile & sp_name=$service_provider_name & title=$title & description=$description");

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    $server_output = curl_exec($ch);
			    curl_close ($ch);
			    $json = json_decode($server_output, true);


			}
			if($id==2 ){
				$s = $d->select("local_serviceproviders_call_request","call_request_id='$call_id'","");
				$userdetails = mysqli_fetch_array($s);
				$usermobile = $userdetails['user_mobile'];
				$society_id = $userdetails['society_id'];
				
				$title = "$service_provider_name rejected your Request ";
				$description = "For Provide Service.";
			
				$qc=$d->select("society_master","society_id='$userdetails[society_id]'");
    			$data=mysqli_fetch_array($qc);
    			$callUr= $data['sub_domain'].'residentApiNew/sp_service.php';

				$usermobile = $userdetails['user_mobile'];
				
				$ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL,$callUr);
			    curl_setopt($ch, CURLOPT_POST, 1);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			    curl_setopt($ch, CURLOPT_POSTFIELDS,"otp=$otp & society_id=$society_id & sendFcm=sendFcm & user_mobile=$usermobile & sp_name=$service_provider_name & title=$title & description=$description");

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    $server_output = curl_exec($ch);
			    curl_close ($ch);
			    $json = json_decode($server_output, true);


			}

			$idquery=$d->insert('local_service_providers_call_request_action',$idarray);
			if($idquery===TRUE){

				$aStatus = array(
	                'callback_action'=>1,
	            );

				$d->update("local_serviceproviders_call_request",$aStatus,"call_request_id='$call_id'","");
				$response["message"]="Action Successfully Saved";
				$response["status"]="200";
				echo json_encode($response);
				exit();
			}else{
				$response["message"]="Something Wents Wrong";
				$response["status"]="201";
				echo json_encode($response);
				exit();
			}
		}else{
			if($id==3){
				$otpdata = mysqli_fetch_array($q1);
				$s = $d->select("local_serviceproviders_call_request","call_request_id='$call_id'","");
				$userdetails = mysqli_fetch_array($s);
				$usermobile = $userdetails['user_mobile'];
				$message = "Your OTP is ".$otpdata[otp].". Share this OTP to Service Provider at the time of service.";
				if($send_sms=='true') {
					$d->send_sms($usermobile,$message);
					$response["message"]="OTP Send on $usermobile";
					$response["status"]="200";
					echo json_encode($response);
					exit();
				} 
				if($otp!=$otpdata['otp']){
					$response["message"]="Invalid OTP";
					$response["status"]="201";
					echo json_encode($response);
					exit();
				}
			}
			if($id=='0'){
				$s = $d->select("local_serviceproviders_call_request","call_request_id='$call_id'","");
				$userdetails = mysqli_fetch_array($s);
				$usermobile = $userdetails['user_mobile'];
				$society_id = $userdetails['society_id'];

				$digits = 6;
				$otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
				// $message = "Your OTP is ".$otp.". Share this OTP to Service Provider at the time of service.";
				// $d->send_sms($usermobile,$message);
				// $m->set_data('otp',$otp);

				$title = "OTP : $otp";
				$description = "Share this OTP to $sp_name Service Provider at the time of service.";
			
			

				$qc=$d->select("society_master","society_id='$userdetails[society_id]'");
    			$data=mysqli_fetch_array($qc);

    			$callUr= $data['sub_domain'].'residentApiNew/sp_service.php';

				$usermobile = $userdetails['user_mobile'];
				$m->set_data('otp',$otp);

				if($send_sms==true) {
					$message = "Your OTP is ".$otp.". Share this OTP to Service Provider at the time of service.";
					$d->send_sms($usermobile,$message);
				} else {

					$ch = curl_init();
				    curl_setopt($ch, CURLOPT_URL,$callUr);
				    curl_setopt($ch, CURLOPT_POST, 1);
				    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
				    curl_setopt($ch, CURLOPT_POSTFIELDS,"otp=$otp & society_id=$society_id & sendFcm=sendFcm & user_mobile=$usermobile & sp_name=$service_provider_name	& title=$title & description=$description");
				    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				    $server_output = curl_exec($ch);
				    curl_close ($ch);
				    $json = json_decode($server_output, true);
				}

			}

			if($id==4 ){
				$s = $d->select("local_serviceproviders_call_request","call_request_id='$call_id'","");
				$userdetails = mysqli_fetch_array($s);
				$usermobile = $userdetails['user_mobile'];
				$society_id = $userdetails['society_id'];
				
				$title = "$service_provider_name Not Interested ";
				$description = "For Provide Service.";
			
				$qc=$d->select("society_master","society_id='$userdetails[society_id]'");
    			$data=mysqli_fetch_array($qc);
    			$callUr= $data['sub_domain'].'residentApiNew/sp_service.php';

				$usermobile = $userdetails['user_mobile'];
				
				$ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL,$callUr);
			    curl_setopt($ch, CURLOPT_POST, 1);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			    curl_setopt($ch, CURLOPT_POSTFIELDS,"otp=$otp & society_id=$society_id & sendFcm=sendFcm & user_mobile=$usermobile & sp_name=$service_provider_name & title=$title & description=$description");

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    $server_output = curl_exec($ch);
			    curl_close ($ch);
			    $json = json_decode($server_output, true);


			}

			if($id==2){
				$s = $d->select("local_serviceproviders_call_request","call_request_id='$call_id'","");
				$userdetails = mysqli_fetch_array($s);
				$usermobile = $userdetails['user_mobile'];
				$society_id = $userdetails['society_id'];
				
				$title = "$service_provider_name rejected your Request ";
				$description = "For Provide Service.";
			
				$qc=$d->select("society_master","society_id='$userdetails[society_id]'");
    			$data=mysqli_fetch_array($qc);
    			$callUr= $data['sub_domain'].'residentApiNew/sp_service.php';

				$usermobile = $userdetails['user_mobile'];
				
				$ch = curl_init();
			    curl_setopt($ch, CURLOPT_URL,$callUr);
			    curl_setopt($ch, CURLOPT_POST, 1);
			    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			    curl_setopt($ch, CURLOPT_POSTFIELDS,"otp=$otp & society_id=$society_id & sendFcm=sendFcm & user_mobile=$usermobile & sp_name=$service_provider_name & title=$title & description=$description");

			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    $server_output = curl_exec($ch);
			    curl_close ($ch);
			    $json = json_decode($server_output, true);


			}

			$m->set_data('active_status',$id);

			$idarray['active_status'] = $m->get_data('active_status');
			if($id=='0'){
				$idarray['otp'] = $m->get_data('otp');
			}

			$idquery=$d->update('local_service_providers_call_request_action',$idarray,"call_request_id='$call_id' and local_service_provider_users_id='$service_provider_users_id'");
			if($idquery===TRUE){

				$aStatus = array(
	                'callback_action'=>1,
	            );

				$d->update("local_serviceproviders_call_request",$aStatus,"call_request_id='$call_id'","");
				
				$response["message"]="Action Successfully Saved";
				$response["status"]="200";
				echo json_encode($response);
				exit();
			}else{ 
				$response["message"]="Something Wents Wrong";
				$response["status"]="201";
				echo json_encode($response);
				exit(); 
			}
		}
	}else if(isset($resendOtp) && $resendOtp == 'resendOtp') {
		$mobile=mysqli_real_escape_string($con, $mobile);
		if (strlen($mobile)<10) {
		  $response["message"]="Please Enter Valid Mobile Number";
		  $response["status"]="201";
		  echo json_encode($response);
		  exit();
		}
		if ($country_code!='') {
			$appendQuery = " AND country_code='$country_code'";
		}

		$q=$d->select("local_service_provider_users","service_provider_phone='$mobile' $appendQuery");
		$data = mysqli_fetch_array($q); 
		if ($data > 0 && $data['otp']!=null) {
			$otp = $data['otp'];
	        $message = $otp." is your OTP to login Service Provider App";
	        if ($country_code=="+91") {
			    $smsObj->send_otp($mobile,$otp,$country_code);
			} else {
				$response["message"]="Service Not Available";
				$response["status"]="201";
				echo json_encode($response);
				exit();
			}
			$response["message"]="OTP Send Successfully";
			$response["status"]="200";
			echo json_encode($response);
			exit();
		}else{
			$response["message"]="OTP Expired. Please Login Again";
			$response["status"]="201";
			echo json_encode($response);
			exit();
		}
	}else if(isset($resendOtpChangeMobile) && $resendOtpChangeMobile == 'resendOtpChangeMobile') {
		$mobile=mysqli_real_escape_string($con, $mobile);
		if (strlen($mobile)<10) {
		  $response["message"]="Please Enter Valid Mobile Number !";
		  $response["status"]="201";
		  echo json_encode($response);
		  exit();
		}
		

		$q=$d->select("local_service_provider_users","service_provider_users_id='$service_provider_users_id'");
		$data = mysqli_fetch_array($q); 
		if ($data > 0 && $data['otp']!=null) {
			$otp = $data['otp'];
	        $message = $otp." is your OTP to login Service Provider App";
	        if ($country_code=="+91") {
			    $smsObj->send_otp($mobile,$otp,$country_code);
			} else {
				$response["message"]="Service Not Available";
				$response["status"]="201";
				echo json_encode($response);
				exit();
			}
			$response["message"]="OTP Send Successfully !";
			$response["status"]="200";
			echo json_encode($response);
			exit();
		}else{
			$response["message"]="OTP Expired. Please Login Again";
			$response["status"]="201";
			echo json_encode($response);
			exit();
		}
	}else if(isset($changeMobile)){
		$mobile=mysqli_real_escape_string($con, $mobile);
		if (strlen($mobile)<10) {
		  $response["message"]="Please Enter Valid Mobile Number";
		  $response["status"]="201";
		  echo json_encode($response);
		  exit();
		}	

		if ($country_code!='') {
			$appendQuery = " AND country_code='$country_code'";
		}

		$qci=$d->selectRow("service_provider_phone","local_service_provider_users","service_provider_phone='$mobile' $appendQuery");
	    if (mysqli_num_rows($qci)>0) {
	     $response["message"]="Mobile Number Already Register";
	     $response["status"]="201";
		 echo json_encode($response);
		 exit();
	    }

		if ($is_six_digit=='yes') {
			$digits = 6;
		}else {
			$digits = 4;
		}

        $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);
		$token = bin2hex(openssl_random_pseudo_bytes(16));
		$m->set_data('mobile',$mobile);
		$m->set_data('otp',$otp);
		
			$a1= array (
				'otp'=> $m->get_data('otp'),
			);
        if ($country_code=="+91") {
		    $smsObj->send_otp($mobile,$otp,$country_code);
        } else {
        	$response["message"]="Service Not Available";
			$response["status"]="201";
			echo json_encode($response);
			exit();
        }
		$d->update('local_service_provider_users',$a1,"service_provider_users_id='$service_provider_users_id'");
		
        $message = $otp." is your OTP to change your mobile number";
        

		$response["message"]="OTP Send Successfully ";
		$response["status"]="200";
		echo json_encode($response);
		exit();
	}else if(isset($editProfile) && $editProfile == 'editProfile') {

		$brochure_profile = "";
		$location_proof = "";
		$id_proof = "";
		$user_img = "";
			
			
		$extAllow=array("pdf","doc","docx","png","jpg","jpeg","JPEG","PNG","JPG","JPEG");

			
		if(!empty($_FILES['id_proof']['tmp_name'])){

		  $extIdProof = pathinfo($_FILES['id_proof']['name'], PATHINFO_EXTENSION);
		  if(in_array($extIdProof,$extAllow)) {

		      $ddd=date("ymdhi");
		      $image_Arr = $_FILES['id_proof'];   
		      $temp = explode(".", $_FILES["id_proof"]["name"]);
		      $id_proof = 'id_proof_'.$ddd.'.' . end($temp);
		      move_uploaded_file($_FILES["id_proof"]["tmp_name"], "../img/local_service_provider/local_service_id_proof/".$id_proof);
		  } else {
		  	$response["message"]="Invalid ID Proof only JPG,PNG,Doc & PDF are allowed.";
            $response["status"]="201";
            echo json_encode($response);
            exit();
		  }

	    }


		if(!empty($_FILES['location_proof']['tmp_name'])){
		  $extIdLocation = pathinfo($_FILES['location_proof']['name'], PATHINFO_EXTENSION);
		  if(in_array($extIdLocation,$extAllow)) {

		      $ddd=date("ymdhi");
		      $image_Arr = $_FILES['location_proof'];   
		      $temp = explode(".", $_FILES["location_proof"]["name"]);
		      $location_proof = 'location_proof'.$ddd.'.' . end($temp);
		      move_uploaded_file($_FILES["location_proof"]["tmp_name"], "../img/local_service_provider/local_service_location_proof/".$location_proof);
		   } else {
		   	 $response["message"]="Invalid Address Proof only JPG,PNG,Doc & PDF are allowed.";
            $response["status"]="201";
            echo json_encode($response);
            exit();
		   }

    	}

	    if(!empty($_FILES['brochure_profile']['tmp_name'])){
		  $extIdCrocure = pathinfo($_FILES['brochure_profile']['name'], PATHINFO_EXTENSION);
		  if(in_array($extIdCrocure,$extAllow)) {

		      $ddd=date("ymdhi");
		      $image_Arr = $_FILES['brochure_profile'];   
		      $temp = explode(".", $_FILES["brochure_profile"]["name"]);
		      $brochure_profile = 'company_profile_'.$ddd.'.' . end($temp);
		      move_uploaded_file($_FILES["brochure_profile"]["tmp_name"], "../img/local_service_provider/local_service_location_proof/".$brochure_profile);
		   } else {
		   	 $response["message"]="Invalid Brochure only JPG,PNG,Doc & PDF are allowed.";
            $response["status"]="201";
            echo json_encode($response);
            exit();
		   }
	    }

	    $extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");

		if(!empty($_FILES['service_provider_user_image']['tmp_name'])){
	    	$logo_size = $_FILES['service_provider_user_image']['size'];
			$ext = pathinfo($_FILES['service_provider_user_image']['name'], PATHINFO_EXTENSION);
			
			if(!in_array($ext,$extension)) {
			  $response["message"]="Invalid Profile Image type.";
              $response["status"]="201";
              echo json_encode($response);
              exit;
			}

			$uploadedFile = $_FILES['service_provider_user_image']['tmp_name'];
	        if ($uploadedFile != ""){
				if($logo_size >= '4097152') {
			        $sourceProperties = getimagesize($uploadedFile);
			        $newFileName = $service_provider_users_id.'_'.rand();
			        $dirPath = "../img/local_service_provider/local_service_users/";
			        $ext = pathinfo($_FILES['service_provider_user_image']['name'], PATHINFO_EXTENSION);
			        $imageType = $sourceProperties[2];
			        $imageHeight = $sourceProperties[1];
			        $imageWidth = $sourceProperties[0];
					if ($imageWidth>3000) {
						$newWidthPercentage= 3000*100 / $imageWidth;  //for maximum 400 widht
						$newImageWidth = $imageWidth * $newWidthPercentage /100;
						$newImageHeight = $imageHeight * $newWidthPercentage /100;
					}else {
						$newImageWidth = $imageWidth;
						$newImageHeight = $imageHeight;
					}
			          switch ($imageType) {
			            case IMAGETYPE_PNG:
			              $imageSrc = imagecreatefrompng($uploadedFile); 
			              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
			              imagepng($tmp,$dirPath. $newFileName. "_user_picture.". $ext);
			              break;           

			            case IMAGETYPE_JPEG:
			              $imageSrc = imagecreatefromjpeg($uploadedFile); 
			              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
			              imagejpeg($tmp,$dirPath. $newFileName. "_user_picture.". $ext);
			              break;
			            
			            case IMAGETYPE_GIF:
			              $imageSrc = imagecreatefromgif($uploadedFile); 
			              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
			              imagegif($tmp,$dirPath. $newFileName. "_user_picture.". $ext);
			              break;

			            default:
			              $response["message"]="Invalid Profile Image type.";
			              $response["status"]="201";
			              echo json_encode($response);
			              exit;
			              break;
			          }
			          $user_img= $newFileName."_user_picture.".$ext;       
			    } else {

			    	$image_Arr = $_FILES['service_provider_user_image'];   
		            $temp = explode(".", $_FILES["service_provider_user_image"]["name"]);
		            $user_img = $service_provider_users_id.'_profile_'.round(microtime(true)) . '.' . end($temp);
		            move_uploaded_file($_FILES["service_provider_user_image"]["tmp_name"], "../img/local_service_provider/local_service_users/".$user_img);
			    }
	        }
		}
		
		$m->set_data('service_provider_email', $service_provider_email);
		$m->set_data('service_provider_name', $service_provider_name);
		$m->set_data('service_provider_whatsapp_country_code', $service_provider_whatsapp_country_code);
		$m->set_data('service_provider_address', $service_provider_address);
		$m->set_data('service_provider_zipcode', $service_provider_zipcode);
		$m->set_data('service_provider_latitude', $service_provider_latitude);
		$m->set_data('service_provider_logitude', $service_provider_logitude);
		$m->set_data('service_provider_whatsapp', $service_provider_whatsapp);
		$m->set_data('gst_no', $gst_no);
		$m->set_data('contact_person_name', $contact_person_name);
		$m->set_data('sp_webiste', $sp_webiste);
		$m->set_data('work_description', $work_description);
		$m->set_data('open_time', $open_time);
		$m->set_data('close_time', $close_time);
		$m->set_data('kyc_status', $kyc_status);
		$m->set_data('brochure_profile', $brochure_profile); 
		$m->set_data('id_proof', $id_proof); 
		$m->set_data('location_proof', $location_proof); 
		$m->set_data('service_provider_user_image', $user_img);

		$updata = array();

		$updata['service_provider_email'] = $m->get_data('service_provider_email');
		$updata['service_provider_name'] = $m->get_data('service_provider_name');
		$updata['service_provider_whatsapp_country_code'] = $m->get_data('service_provider_whatsapp_country_code');
		$updata['service_provider_address'] = $m->get_data('service_provider_address');
		$updata['service_provider_zipcode'] = $m->get_data('service_provider_zipcode');
		$updata['service_provider_latitude'] = $m->get_data('service_provider_latitude');
		$updata['service_provider_logitude'] = $m->get_data('service_provider_logitude');
		$updata['service_provider_whatsapp'] = $m->get_data('service_provider_whatsapp');
		$updata['gst_no'] = $m->get_data('gst_no');
		$updata['contact_person_name'] = $m->get_data('contact_person_name');
		$updata['sp_webiste'] = $m->get_data('sp_webiste');
		$updata['work_description'] = $m->get_data('work_description');
		$updata['kyc_status'] = $m->get_data('kyc_status'); 
		$updata['id_proof'] = $m->get_data('id_proof'); 
		$updata['location_proof'] = $m->get_data('location_proof');
		$updata['brochure_profile'] = $m->get_data('brochure_profile'); 
		$updata['service_provider_user_image'] = $m->get_data('service_provider_user_image');
		$updata['open_time'] = $m->get_data('open_time');
		$updata['close_time'] = $m->get_data('close_time');

		$qqq = $d->update('local_service_provider_users',$updata,"service_provider_users_id=$service_provider_users_id");
		
		if($qqq == true){
			$response["message"] = "Profile Updated Successfully.";
			$response["status"] = "200";
			echo json_encode($response);
		}else{
			$response["message"] = "Something Wents Wrong.";
			$response["status"] = "201";
			echo json_encode($response);
		}
	}else if(isset($updateProfilePicture) && $updateProfilePicture == 'updateProfilePicture') {
		if(isset($_FILES)){
			$extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");

			if(!empty($_FILES['contact_person_profile']['tmp_name'])){
				
				$logo_size = $_FILES['contact_person_profile']['size'];
				$ext = pathinfo($_FILES['contact_person_profile']['name'], PATHINFO_EXTENSION);
				
				if(!in_array($ext,$extension)) {
				  $response["message"]="Invalid Profile Image type.";
	              $response["status"]="201";
	              echo json_encode($response);
	              exit;
				}

				$uploadedFile = $_FILES['contact_person_profile']['tmp_name'];
		        if ($uploadedFile != ""){
					if($logo_size >= '4097152') {
				        $sourceProperties = getimagesize($uploadedFile);
				        $newFileName = $service_provider_users_id.'_'.rand();
				        $dirPath = "../img/local_service_provider/local_service_users/";
				        $ext = pathinfo($_FILES['contact_person_profile']['name'], PATHINFO_EXTENSION);
				        $imageType = $sourceProperties[2];
				        $imageHeight = $sourceProperties[1];
				        $imageWidth = $sourceProperties[0];
						if ($imageWidth>3000) {
							$newWidthPercentage= 3000*100 / $imageWidth;  //for maximum 400 widht
							$newImageWidth = $imageWidth * $newWidthPercentage /100;
							$newImageHeight = $imageHeight * $newWidthPercentage /100;
						}else {
							$newImageWidth = $imageWidth;
							$newImageHeight = $imageHeight;
						}
				          switch ($imageType) {
				            case IMAGETYPE_PNG:
				              $imageSrc = imagecreatefrompng($uploadedFile); 
				              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
				              imagepng($tmp,$dirPath. $newFileName. "_user_picture.". $ext);
				              break;           

				            case IMAGETYPE_JPEG:
				              $imageSrc = imagecreatefromjpeg($uploadedFile); 
				              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
				              imagejpeg($tmp,$dirPath. $newFileName. "_user_picture.". $ext);
				              break;
				            
				            case IMAGETYPE_GIF:
				              $imageSrc = imagecreatefromgif($uploadedFile); 
				              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
				              imagegif($tmp,$dirPath. $newFileName. "_user_picture.". $ext);
				              break;

				            default:
				              $response["message"]="Invalid Profile Image type.";
				              $response["status"]="201";
				              echo json_encode($response);
				              exit;
				              break;
				          }
				          $user_img= $newFileName."_user_picture.".$ext;       
				    } else {

				    	$image_Arr = $_FILES['contact_person_profile'];   
			            $temp = explode(".", $_FILES["contact_person_profile"]["name"]);
			            $user_img = $service_provider_users_id.'_profile_'.round(microtime(true)) . '.' . end($temp);
			            move_uploaded_file($_FILES["contact_person_profile"]["tmp_name"], "../img/local_service_provider/local_service_users/".$user_img);
				    }
		        }

		        $m->set_data('contact_person_profile', $user_img);
		        $updata['contact_person_photo'] = $m->get_data('contact_person_profile');
		        
		        if($d->update('local_service_provider_users',$updata,"service_provider_users_id=$service_provider_users_id")){
					$response["contact_person_profile"] = $base_url."img/local_service_provider/local_service_users/".$user_img;
					$response["message"] = "Profile Picture Updates Successfully.";
					$response["status"] = "200";
					echo json_encode($response);
				}else{
					$response["message"] = "Something Wents Wrong.";
					$response["status"] = "201";
					echo json_encode($response);
				}
		    }else{
		    	$response["message"] = "Please Select Profile Picture.";
				$response["status"] = "201";
				echo json_encode($response);
		    }
	    }else{
	    	$response["message"] = "Something Wents Wrong.";
			$response["status"] = "201";
			echo json_encode($response);
	    }
	}else if(isset($uploadVisitingcard) && $uploadVisitingcard == 'uploadVisitingcard') {
		if(isset($_FILES)){
			$extension=array("jpeg","jpg","png","gif","JPG","JPEG","PNG");

			if(!empty($_FILES['visiting_card']['tmp_name'])){
				 $ext = pathinfo($_FILES['visiting_card']['name'], PATHINFO_EXTENSION);
				if(!in_array($ext,$extension)) {
				  $response["message"]="Invalid Visiting Card type.";
	              $response["status"]="201";
	              echo json_encode($response);
	              exit;
				}

				$uploadedFile = $_FILES['visiting_card']['tmp_name'];
		        if ($uploadedFile != ""){
		          $sourceProperties = getimagesize($uploadedFile);
		          $newFileName = rand().'_'.$service_provider_users_id;
		          $dirPath = "../img/local_service_provider/local_service_users/";
		          $ext = pathinfo($_FILES['visiting_card']['name'], PATHINFO_EXTENSION);
		          $imageType = $sourceProperties[2];
		          $imageHeight = $sourceProperties[1];
		          $imageWidth = $sourceProperties[0];
		          if ($imageWidth>2400) {
		              $newWidthPercentage= 2400*100 / $imageWidth;  //for maximum 400 widht
		              $newImageWidth = $imageWidth * $newWidthPercentage /100;
		              $newImageHeight = $imageHeight * $newWidthPercentage /100;
		          }else {
		              $newImageWidth = $imageWidth;
		              $newImageHeight = $imageHeight;
		          }
		          switch ($imageType) {
		            case IMAGETYPE_PNG:
		              $imageSrc = imagecreatefrompng($uploadedFile); 
		              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
		              imagepng($tmp,$dirPath. $newFileName. "_sp_visiting_card.". $ext);
		              break;           

		            case IMAGETYPE_JPEG:
		              $imageSrc = imagecreatefromjpeg($uploadedFile); 
		              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
		              imagejpeg($tmp,$dirPath. $newFileName. "_sp_visiting_card.". $ext);
		              break;
		            
		            case IMAGETYPE_GIF:
		              $imageSrc = imagecreatefromgif($uploadedFile); 
		              $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
		              imagegif($tmp,$dirPath. $newFileName. "_sp_visiting_card.". $ext);
		              break;

		            default:
		              $response["message"]="Invalid Visiting Card Image type.";
		              $response["status"]="201";
		              echo json_encode($response);
		              exit;
		              break;
		          }
		          $visiting_card= $newFileName."_sp_visiting_card.".$ext;       
		        }
		        $m->set_data('visiting_card', $visiting_card);
		        $updata['visiting_card'] = $m->get_data('visiting_card');

		        if($d->update('local_service_provider_users',$updata,"service_provider_users_id=$service_provider_users_id")){
					
					$response["message"] = "Visiting Card Updates Successfully.";
					$response["status"] = "200";
					echo json_encode($response);
				}else{
					$response["message"] = "Something Wents Wrong.";
					$response["status"] = "201";
					echo json_encode($response);
				}
		    }else{
		    	$response["message"] = "Please Select Visiting Card.";
				$response["status"] = "201";
				echo json_encode($response);
		    }
	    }else{
	    	$response["message"] = "Something Wents Wrong.";
			$response["status"] = "201";
			echo json_encode($response);
	    }
	}else if(isset($logout) && $logout == 'logout') {
		$updata['token'] = '';
		$d->update('local_service_provider_users',$updata,"service_provider_users_id='$service_provider_users_id'");
		$response["message"] = "Logged Out Successfully.";
		$response["status"] = "200";
		echo json_encode($response);
	}else if(isset($tokenUpdate) && $tokenUpdate == 'tokenUpdate') {
		if($newToken==''){
			$response["message"] = "Please pass new token.";
			$response["status"] = "201";
		}else{
			$updata['token'] = $newToken;
			$d->update('local_service_provider_users',$updata,"service_provider_users_id='$service_provider_users_id'");
			$response["message"] = "Token Updated Successfully.";
			$response["status"] = "200";
		}
		echo json_encode($response);
	}else if (isset($getNotificationList) && $getNotificationList == 'getNotificationList' && filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true ) {

		$aNoti = array(
	        'read_status' => 1,
	       );
         
         $d->update("local_service_provider_notifications", $aNoti,"service_provider_users_id='$service_provider_users_id' ");
           


		$q=$d->select("local_service_provider_notifications","service_provider_users_id=$service_provider_users_id","ORDER BY notification_id DESC");
		if(mysqli_num_rows($q)>0){
			$response["notifications"] = array();
			while($data=mysqli_fetch_array($q)) {
				$notifications = array();
				$notifications["notification_id"]=$data['notification_id'];
				$notifications["service_provider_users_id"]=$service_provider_users_id;
				$notifications["notification_title"]=$data['notification_title'];
				$notifications["notification_description"]=$data['notification_description'];
				$notifications["notification_for"]=$data['notification_for'];
				$notifications["notification_date"]=$data['notification_date'];
				$notifications["menu_click"]=$data['menu_click'];
				$notifications["notification_data"]=$data['notification_data'];
				array_push($response["notifications"], $notifications);
			}
			$response["message"]="get notifications successfully.";
			$response["status"]="200";
			echo json_encode($response);
		}else{
			$response["message"]="No Notification Found.";
			$response["status"]="201";
			echo json_encode($response);
		}
	}else if (isset($deleteNotification) && $deleteNotification == 'deleteNotification' && filter_var($notification_id, FILTER_VALIDATE_INT) == true ) {

         
        $q= $d->delete("local_service_provider_notifications","notification_id='$notification_id' ");

		if($q>0){
			
			$response["message"]="Notifications Deleted Successfully.";
			$response["status"]="200";
			echo json_encode($response);
		}else{
			$response["message"]="No Notification Found.";
			$response["status"]="201";
			echo json_encode($response);
		}
	} else if (isset($deleteAllNotification) && $deleteAllNotification == 'deleteAllNotification' && filter_var($service_provider_users_id, FILTER_VALIDATE_INT) == true ) {

         
        $q= $d->delete("local_service_provider_notifications","service_provider_users_id='$service_provider_users_id' ");

		if($q>0){
			
			$response["message"]="Notifications Deleted Successfully.";
			$response["status"]="200";
			echo json_encode($response);
		}else{
			$response["message"]="No Notification Found.";
			$response["status"]="201";
			echo json_encode($response);
		}
	} else {
		$response["message"] = "Wrong Tag";
		$response["status"] = "201";
		echo json_encode($response);
	}
}else{
	$response["message"] = "Invalid Request";
	$response["status"] = "201";
	echo json_encode($response);
}

?>