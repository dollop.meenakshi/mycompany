<?php

include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){

    //if ($key==$keydb && $auth_check=='true') {

        $response = array();
        extract(array_map("test_input" , $_POST));
        
        if($_POST['getOurServices']=="getOurServices" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $response["our_services"] = array();

            $ourServiceQry = $d->select("company_service_master","society_id='$society_id'");

            if (mysqli_num_rows($ourServiceQry) > 0) {
                
                while ($serviceData = mysqli_fetch_array($ourServiceQry)) {

                    $service = array();

                    $service["company_service_id"] = $serviceData['company_service_id'];

                    if ($serviceData['company_service_image'] != '') {
                        $service["company_service_image"] = $base_url.'img/company_services/'.$serviceData['company_service_image'];
                    }else{
                        $service["company_service_image"]="";
                    }
                    
                    $service["company_service_name"] = $serviceData['company_service_name'];

                    array_push($response["our_services"], $service);
                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            } else{
                $response["message"]="No data";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getOurServicesDetails']=="getOurServicesDetails" && $company_service_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

            $ourServiceQry = $d->selectRow("*","company_service_details_master","society_id='$society_id' AND company_service_id = '$company_service_id'");

            if (mysqli_num_rows($ourServiceQry) > 0) {

                $data = mysqli_fetch_array($ourServiceQry);

                $company_service_details_id = $data['company_service_details_id'];
                
                $response['company_service_details_id'] = $company_service_details_id;
                $response['company_service_id'] = $data['company_service_id'];
                $response['society_id'] = $data['society_id'];
                $response['company_service_details_title'] = $data['company_service_details_title'];
                $response['company_service_details_description'] = $data['company_service_details_description'];

                // --------- Service List ----------

                $response['service_list'] = array();

                $serviceListQry = $d->select("company_service_master,company_service_details_master,company_service_detail_list_master","company_service_details_master.company_service_id = company_service_master.company_service_id AND company_service_detail_list_master.company_service_details_id = company_service_details_master.company_service_details_id AND company_service_details_master.company_service_id = '$company_service_id'");

                if (mysqli_num_rows($serviceListQry) > 0) {
                    while ($serviceListData = mysqli_fetch_array($serviceListQry)) {
                        $serviceList = array();

                        $serviceList['company_service_detail_list_id'] = $serviceListData['company_service_detail_list_id'];
                        $serviceList['company_service_detail_list_name'] = $serviceListData['company_service_detail_list_name'];

                        array_push($response['service_list'],$serviceList);
                    }
                }

                // ---------- Image List Data ----------

                $response['data_list'] = array();

                $serviceImageListQry = $d->select("company_service_more_details_master","company_service_details_id = '$company_service_details_id'");

                if (mysqli_num_rows($serviceImageListQry) > 0) {
                    while ($serviceImageListData = mysqli_fetch_array($serviceImageListQry)) {
                        $serviceList = array();

                        $serviceList['company_service_more_details_id'] = $serviceImageListData['company_service_more_details_id'];
                        $serviceList['company_service_more_details_name'] = $serviceImageListData['company_service_more_details_name'];

                        if ($serviceImageListData['company_service_more_details_image'] != '') {
                            $serviceList["company_service_more_details_image"] = $base_url.'img/company_services/'.$serviceImageListData['company_service_more_details_image'];
                        }else{
                            $serviceList["company_service_more_details_image"]="";
                        }
                        array_push($response['data_list'],$serviceList);
                    }
                }


                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            } else{
                $response["message"]="No data";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else if($_POST['getOurServicesDetailsMore']=="getOurServicesDetailsMore" && $company_service_details_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){


            $serviceImageListQry = $d->select("company_service_more_details_master","company_service_details_id = '$company_service_details_id'");

            if (mysqli_num_rows($serviceImageListQry) > 0) {
               
                $serviceImageListData = mysqli_fetch_array($serviceImageListQry);

                $company_service_more_details_id = $serviceImageListData['company_service_more_details_id'];

                $response['company_service_more_details_id'] = $company_service_more_details_id;
                $response['company_service_more_details_name'] = $serviceImageListData['company_service_more_details_name'];
                $response['company_service_more_details_title'] = $serviceImageListData['company_service_more_details_title'];
                $response['company_service_more_details_description'] = $serviceImageListData['company_service_more_details_description'];

                if ($serviceImageListData['company_service_more_details_image'] != '') {
                    $response["company_service_more_details_image"] = $base_url.'img/company_services/'.$serviceImageListData['company_service_more_details_image'];
                }else{
                    $response["company_service_more_details_image"]="";
                }

                // --------- Service List ----------

                $response['data_list'] = array();

                $serviceListQry = $d->select("company_service_more_details_list_master","company_service_more_details_id = '$company_service_more_details_id'");

                if (mysqli_num_rows($serviceListQry) > 0) {
                    while ($serviceListData = mysqli_fetch_array($serviceListQry)) {
                        $serviceMoreList = array();

                        $serviceMoreList['company_service_more_details_list_id'] = $serviceListData['company_service_more_details_list_id'];
                        $serviceMoreList['company_service_more_detail_list_name'] = $serviceListData['company_service_more_detail_list_name'];

                        array_push($response['data_list'],$serviceMoreList);
                    }
                }

                $response["message"]="Success";
                $response["status"]="200";
                echo json_encode($response);
            } else{
                $response["message"]="No data";
                $response["status"]="201";
                echo json_encode($response);
            }
        }else{
            $response["message"]="wrong tag";
            $response["status"]="201";
            echo json_encode($response);
        }
    /*}else{
        $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);
    }*/
}
?>