<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){
$maintenanceName =  $xml->string->maintenance;

    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
        if($_POST['addEventAttendList']=="addEventAttendList" && $unit_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true ){

            $fq=$d->select("users_master","unit_id='$unit_id' AND user_id='$user_id'");
            $ownerData=mysqli_fetch_array($fq);
            $userType=$ownerData['user_type'];

            if ($adult_person==0 && $child_person==0 && $guest_person==0) {
                $response["message"]="Please choose minimum 1 member to attend event";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }

            $m->set_data('event_id',$event_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('going_person',$adult_person);
            $m->set_data('going_child',$child_person);
            $m->set_data('going_guest',$guest_person);
            $m->set_data('notes',$notes);
        
            $a = array(
                'event_id'=>$m->get_data('event_id'),
                'society_id'=>$m->get_data('society_id'),
                'user_id'=>$m->get_data('user_id'),
                'userType'=>$userType,
                'unit_id'=>$m->get_data('unit_id'),
                'going_person'=>$m->get_data('going_person'),
                'going_child'=>$m->get_data('going_child'),
                'going_guest'=>$m->get_data('going_guest'),
                'notes'=>$m->get_data('notes'));

            $going=$d->select("event_attend_list","user_id = '$user_id' AND event_id = '$event_id' AND unit_id='$unit_id'");


            if(mysqli_num_rows($going)>0)
                {

                    $d->delete('event_passes_master',"event_id='$event_id' AND user_id='$user_id'");
                	$data = mysqli_fetch_array($going);
                	$id = $data['event_attend_id'];
                    $quserDataUpdate = $d->update("event_attend_list",$a,"event_attend_id= '$id'");
                    $response["message"]="Event booking updated successfully.";

                }
                else
                {
                    $quserDataUpdate = $d->insert("event_attend_list",$a);
                    $response["message"]="Event has been booked successfully";

                }
    
            
            if($quserDataUpdate==TRUE){
                

                for ($i5=0; $i5 <$adult_person; $i5++) { 
               
                $pass_no= "TKT".$user_id;
                  $adultAry = array(
                    'event_id' => $m->get_data('event_id'),
                    'society_id' => $m->get_data('society_id'),
                    'user_id' => $m->get_data('user_id'),
                    'unit_id' =>  $unit_id,
                    'userType' => $userType,
                    'pass_no' =>  $pass_no,
                    'pass_amount' =>  '0',
                    'pass_type' =>  0,
                  );
                  $d->insert('event_passes_master',$adultAry);
                }

                for ($i6=0; $i6 <$child_person; $i6++) { 
               
                $pass_no= "TKT".$user_id;
                  $adultArrChild = array(
                    'event_id' => $m->get_data('event_id'),
                    'society_id' => $m->get_data('society_id'),
                    'user_id' => $m->get_data('user_id'),
                    'unit_id' =>  $unit_id,
                    'userType' =>  $userType,
                    'pass_no' =>  $pass_no,
                    'pass_amount' =>  '0',
                    'pass_type' =>  1,
                  );
                  $d->insert('event_passes_master',$adultArrChild);
                }

                for ($i7=0; $i7 <$guest_person; $i7++) { 
               
                $pass_no= "TKT".$user_id;
                  $adultArrChild = array(
                    'event_id' => $m->get_data('event_id'),
                    'society_id' => $m->get_data('society_id'),
                    'user_id' => $m->get_data('user_id'),
                    'unit_id' =>  $unit_id,
                    'userType' =>  $userType,
                    'pass_no' =>  $pass_no,
                    'pass_amount' =>  '0',
                    'pass_type' =>  2,
                  );
                  $d->insert('event_passes_master',$adultArrChild);
                }

                    $block_id=$d->getBlockid($user_id);
                     $fcmArray=$d->selectAdminBlockwise("10",$block_id,"android");
                     $fcmArrayIos=$d->selectAdminBlockwise("10",$block_id,"ios");

		
                    $nAdmin->noti_new($society_id,"",$fcmArray,"Event to be attended by ".$user_name,$user_name." Attending event with  ".$going_person." people","viewEvents?id=$event_id");
					$nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,"Event to be attended by ".$user_name,$user_name." Attending event with  ".$going_person." people","viewEvents?id=$event_id");

                    
                    $notiAry = array(
                        'society_id'=>$society_id,
                        'notification_tittle'=>"Event to be attended by  ".$user_name,
                        'notification_description'=>"Attending event with  ".$going_person." people",
                        'notifiaction_date'=>date('Y-m-d H:i'),
                        'notification_action'=>"viewEvents?id=$event_id",
                        'admin_click_action'=>"viewEvents?id=$event_id",
                         'notification_logo'=>'Events_1xxxhdpi.png',
                      );
                      
                        $d->insert("admin_notification",$notiAry);

                $response["status"]="200";
                echo json_encode($response);

            }else{

                $response["message"]="No Attending List Added.";
                $response["status"]="201";
                echo json_encode($response);

            }

        } else  if($_POST['bookEventUnpaid']=="bookEventUnpaid" && $unit_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($events_day_id, FILTER_VALIDATE_INT) == true ){

            if ($user_id!='' && $user_id!=0) {
                 $quc=$d->select("users_master","user_id='$user_id' AND is_defaulter=1");
                 if (mysqli_num_rows($quc)>0) {
                    $response["message"] = "Access Denied for book event";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                 }

            }

            $eq = $d->select("event_master","event_id = '$event_id'");
            $event_master_data = $eData = mysqli_fetch_array($eq);
            $event_name= $eData['event_title'];

            $fq=$d->select("users_master","unit_id='$unit_id' AND user_id='$user_id'");
            $ownerData=mysqli_fetch_array($fq);
            $userType=$ownerData['user_type'];


            if ($adult_person==0 && $child_person==0 && $guest_person==0) {
                $response["message"]="Please choose minimum 1 member to attend event";
                $response["status"]="201";
                echo json_encode($response);
                exit();
            }

            $qmaint = $d->select("event_days_master","events_day_id ='$events_day_id'");
            $maintData=mysqli_fetch_array($qmaint);
            $maxiAdult=$maintData['maximum_pass_adult'];
            $maxiChild=$maintData['maximum_pass_children'];
            $maxiGuest=$maintData['maximum_pass_guests'];

            $q111 = $d->sum_data("going_person","event_attend_list","book_status=1 AND events_day_id ='$events_day_id'");
            $adultData = mysqli_fetch_array($q111);
            $q222 = $d->sum_data("going_child","event_attend_list","book_status=1 AND events_day_id ='$events_day_id' ");
            $childData  = mysqli_fetch_array($q222);
            $q333 = $d->sum_data("going_guest","event_attend_list","book_status=1 AND events_day_id ='$events_day_id'  ");
            $guestData  = mysqli_fetch_array($q333);
            $totalBookedAdult=$adultData['SUM(going_person)']+$adult_person;
            $totalBookedChild=$childData['SUM(going_child)']+$child_person;
            $totalBookedGuest=$guestData['SUM(going_guest)']+$guest_person;

            if ($maxiAdult<$totalBookedAdult ||  $maxiChild<$totalBookedChild && $child_person!=0 || $maxiGuest<$totalBookedGuest && $guest_person!=0) {
                  $during_trans_booking_full = $xml->string->during_booking_full;
                  $response["message"]="$during_trans_booking_full";
                  $response["status"]="201";
                  echo json_encode($response);
                exit();
            }

            $payment_received_date=date("Y-m-d H:i:s");
            $m->set_data('payment_received_date',$payment_received_date);
            $m->set_data('event_id',$event_id);
            $m->set_data('events_day_id',$events_day_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('going_person',$adult_person);
            $m->set_data('going_child',$child_person);
            $m->set_data('going_guest',$guest_person);
            $m->set_data('notes',$notes);
            
        
            $a = array(
                'event_id'=>$m->get_data('event_id'),
                'events_day_id'=>$m->get_data('events_day_id'),
                'society_id'=>$m->get_data('society_id'),
                'user_id'=>$m->get_data('user_id'),
                'payment_received_date'=> $m->get_data('payment_received_date'),
                'userType'=>$userType,
                'unit_id'=>$m->get_data('unit_id'),
                'going_person'=>$m->get_data('going_person'),
                'going_child'=>$m->get_data('going_child'),
                'going_guest'=>$m->get_data('going_guest'),
                'notes'=>$m->get_data('notes'),
                'book_status'=>1,
            );

            $going11=$d->select("event_attend_list","user_id = '$user_id' AND event_id = '$event_id' AND unit_id='$unit_id' AND events_day_id='$events_day_id' AND book_status!=3");

 
                if(mysqli_num_rows($going11)>0)
                {

    
                    $response["message"]="Event already booked by you, go to my booking section.";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();

                }
                else
                {
                    $quserDataUpdate = $d->insert("event_attend_list",$a);
                    $event_attend_id = $con->insert_id;
                    $response["message"]="Event has been booked successfully";

                }
        
      
            if($quserDataUpdate==TRUE){
                $m->set_data('event_attend_id',$event_attend_id);

                for ($i5=0; $i5 <$adult_person; $i5++) { 
                
                $pass_no= "TKT".$user_id;
                  $adultAry = array(
                    'event_id' => $m->get_data('event_id'),
                    'event_attend_id' => $m->get_data('event_attend_id'),
                    'events_day_id' => $m->get_data('events_day_id'),
                    'society_id' => $m->get_data('society_id'),
                    'user_id' => $m->get_data('user_id'),
                    'unit_id' =>  $unit_id,
                    'userType' => $userType,
                    'pass_no' =>  $pass_no,
                    'pass_amount' =>  '0',
                    'pass_type' =>  0,
                  );
                  $d->insert('event_passes_master',$adultAry);
                }

                for ($i6=0; $i6 <$child_person; $i6++) { 
               
                $pass_no= "TKT".$user_id;
                  $adultArrChild = array(
                    'event_id' => $m->get_data('event_id'),
                    'event_attend_id' => $m->get_data('event_attend_id'),
                    'events_day_id' => $m->get_data('events_day_id'),
                    'society_id' => $m->get_data('society_id'),
                    'user_id' => $m->get_data('user_id'),
                    'unit_id' =>  $unit_id,
                    'userType' =>  $userType,
                    'pass_no' =>  $pass_no,
                    'pass_amount' =>  '0',
                    'pass_type' =>  1,
                  );
                  $d->insert('event_passes_master',$adultArrChild);
                }

                for ($i7=0; $i7 <$guest_person; $i7++) { 
                
                $pass_no= "TKT".$user_id;
                  $adultArrChild = array(
                    'event_id' => $m->get_data('event_id'),
                    'event_attend_id' => $m->get_data('event_attend_id'),
                    'events_day_id' => $m->get_data('events_day_id'),
                    'society_id' => $m->get_data('society_id'),
                    'user_id' => $m->get_data('user_id'),
                    'unit_id' =>  $unit_id,
                    'userType' =>  $userType,
                    'pass_no' =>  $pass_no,
                    'pass_amount' =>  '0',
                    'pass_type' =>  2,
                  );
                  $d->insert('event_passes_master',$adultArrChild);
                }
                    $block_id=$d->getBlockid($user_id);
                     $fcmArray=$d->selectAdminBlockwise("10",$block_id,"android");
                     $fcmArrayIos=$d->selectAdminBlockwise("10",$block_id,"ios");

                
        

                    $nAdmin->noti_new($society_id,"",$fcmArray,"Event to be attended by ".$user_name,$user_name." Attending event with  ".$going_person." people","viewEvents?id=$event_id");
                    $nAdmin->noti_ios_new($society_id,"",$fcmArrayIos,"Event to be attended by ".$user_name,$user_name." Attending event with  ".$going_person." people","viewEvents?id=$event_id");
                    
                    $notiAry = array(
                        'society_id'=>$society_id,
                        'notification_tittle'=>"Event to be attended by  ".$user_name,
                        'notification_description'=>"Attending event with  ".$going_person." people",
                        'notifiaction_date'=>date('Y-m-d H:i'),
                        'notification_action'=>"viewEvents?id=$event_id",
                        'admin_click_action'=>"viewEvents?id=$event_id",
                        'notification_logo'=>'Events_1xxxhdpi.png',
                      );
                      
                        $d->insert("admin_notification",$notiAry);

                $d->insert_myactivity($user_id,"$society_id","0","$user_name","$event_name event booked successfully","Events_1xxxhdpi.png");

                $response["status"]="200";
                echo json_encode($response);

            }else{

                $response["message"]="No Attending List Added.";
                $response["status"]="201";
                echo json_encode($response);

            }

        }else if($_POST['getEventList']=="getEventListNew" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                if ($user_id!='' && $user_id!=0) {
                     $quc=$d->select("users_master","user_id='$user_id' AND is_defaulter=1");
                     if (mysqli_num_rows($quc)>0) {
                        $response["message"] = "Access Denied ";
                        $response["status"] = "201";
                        echo json_encode($response);
                        exit();
                     }

                }

                $qnotification=$d->select("event_master","society_id = '$society_id' AND event_status = '1' AND ((block_id = '$block_id' AND floor_id = '0') OR (block_id = '0' AND floor_id = '0') OR (block_id = '$block_id' AND floor_id = '$floor_id'))","order by event_start_date ASC LIMIT 100");
                $qeComp=$d->select("event_master","society_id = '$society_id' AND event_status = '1'  AND ((block_id = '$block_id' AND floor_id = '0') OR (block_id = '0' AND floor_id = '0') OR (block_id = '$block_id' AND floor_id = '$floor_id'))","order by event_start_date DESC LIMIT 100");

                // $response["event"] = array();
                    $response["event"] = array();
                    $response["event_completed"] = array();

                if(mysqli_num_rows($qnotification)>0){

                    // $response["event_completed"] = array();

                    while($data_notification=mysqli_fetch_array($qnotification)) {

                        $data_notification = array_map("html_entity_decode", $data_notification);
                         
                        $event_id=$data_notification['event_id'];

                        $event = array(); 
                        $today=date('Y-m-d');
                        $cTime= date("H:i:s");
                        $expire = strtotime($data_notification['event_end_date']);
                        $today = strtotime("$today midnight");
                        if($today > $expire){
                            $eventEnd=0;
                            // $event["hide_status"]="0";
                          } else {
                            $eventEnd=1;
                            // $event["hide_status"]="1";
                          }
                        $event["event_id"]=$data_notification['event_id'];
                        $id=$data_notification['event_id'];
                        $event["event_title"]=html_entity_decode($data_notification['event_title']);
                        if(file_exists("../img/event_image/$data_notification[event_image]")) { 
                            $event["event_image"]=$base_url."img/event_image/".$data_notification['event_image'];
                        } else {
                            $event["event_image"]=$base_url."img/eventDefault.png";
                        }
                        
                        $event["event_start_date"]=$data_notification['event_start_date'];
                        $event["event_start_date_view"]=date("d M Y h:i A", strtotime($data_notification['event_start_date']));
                        $event["event_end_date"]=date("d M Y h:i A", strtotime($data_notification['event_end_date']));

                        $event["event_end_veiw_only"]=date("d F Y", strtotime($data_notification['event_end_date']));

                        $event["event_day_only"]=date("d", strtotime($data_notification['event_start_date']));
                        $event["event_time_only"]="";
                        $event["event_month_year_only"]=date("M, Y", strtotime($data_notification['event_start_date']));
                        $event["no_of_days"]="".$d->count_data_direct("events_day_id","event_days_master","event_id='$data_notification[event_id]'"); ;

                       
                        $event["event_type"]="0";
                        if ($eventEnd==1) {
                            array_push($response["event"], $event);
                        }


                         
                    }

                    // $response["message"]="Get Event Success.";
                    // $response["status"]="200";
                    // echo json_encode($response);
                    $temp=true;

                }else {
                    $temp=false;
                }

                if(mysqli_num_rows($qeComp)>0){

                    

                    while($data_notification=mysqli_fetch_array($qeComp)) {

                        $data_notification = array_map("html_entity_decode", $data_notification);
                         
                        $event_id=$data_notification['event_id'];

                        $event = array(); 
                        $today=date('Y-m-d');
                        $cTime= date("H:i:s");
                        $expire = strtotime($data_notification['event_end_date']);
                        $today = strtotime("$today midnight");
                        if($today > $expire){
                            $eventEnd=0;
                            // $event["hide_status"]="0";
                          } else {
                            $eventEnd=1;
                            // $event["hide_status"]="1";
                          }
                        $event["event_id"]=$data_notification['event_id'];
                        $id=$data_notification['event_id'];
                        $event["event_title"]=html_entity_decode($data_notification['event_title']);
                        if(file_exists("../img/event_image/$data_notification[event_image]")) { 
                            $event["event_image"]=$base_url."img/event_image/".$data_notification['event_image'];
                        } else {
                            $event["event_image"]=$base_url."img/eventDefault.png";
                        }
                        
                        
                        $event["event_start_date"]=$data_notification['event_start_date'];
                        $event["event_start_date_view"]=date("d M Y h:i A", strtotime($data_notification['event_start_date']));
                        $event["event_end_date"]=date("d M Y h:i A", strtotime($data_notification['event_end_date']));

                        $event["event_end_veiw_only"]=date("d F Y", strtotime($data_notification['event_end_date']));

                        $event["event_day_only"]=date("d", strtotime($data_notification['event_start_date']));
                        $event["event_time_only"]="";
                        $event["event_month_year_only"]=date("M, Y", strtotime($data_notification['event_start_date']));
                        $event["no_of_days"]="".$d->count_data_direct("events_day_id","event_days_master","event_id='$data_notification[event_id]'"); ;

                       
                        $event["event_type"]="0";
                        if ($eventEnd==0) {
                            array_push($response["event_completed"], $event);

                        }


                    }

                    // $response["message"]="Get Event Success.";
                    // $response["status"]="200";
                    // echo json_encode($response);
                    $temp2=true;

                }else {
                    $temp2=false;
                }

                if ($temp==true || $temp2 ==true) {
                    $response["message"]="Get Event Success.!";
                    $response["status"]="200";
                    echo json_encode($response);

                } else{

                    $response["message"]="No Event Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['getMyBooking']=="getMyBooking" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true ){



                $qnotification=$d->select("event_master,event_attend_list,event_days_master","event_days_master.events_day_id=event_attend_list.events_day_id AND event_attend_list.event_id=event_master.event_id AND  event_master.society_id = '$society_id' AND  event_attend_list.unit_id='$unit_id' AND event_attend_list.userType='$user_type' AND event_attend_list.book_status=1","order by event_attend_list.event_attend_id DESC LIMIT 100");

                if(mysqli_num_rows($qnotification)>0){

                    $response["event"] = array();

                    while($data_notification=mysqli_fetch_array($qnotification)) {

                        $data_notification = array_map("html_entity_decode", $data_notification);
                         
                        $event_id=$data_notification['event_id'];

                        $event = array(); 
                        $today=date('Y-m-d');
                        $cTime= date("H:i:s");
                        $expire = strtotime($data_notification['event_date']);
                        $today = strtotime("$today $cTime");
                        if($data_notification['active_status']==1 || $today > $expire){
                            $eventEnd=0;
                            $event["event_expire"]=true;
                          } else {
                            $eventEnd=1;
                            $event["event_expire"]=false;
                          }
                        $event["event_id"]=$data_notification['event_id'];
                        $event["event_attend_id"]=$data_notification['event_attend_id'];
                        $id=$data_notification['event_id'];
                        $event["event_title"]=html_entity_decode($data_notification['event_title']);
                        if(file_exists("../img/event_image/$data_notification[event_image]")) { 
                            $event["event_image"]=$base_url."img/event_image/".$data_notification['event_image'];
                        } else {
                            $event["event_image"]=$base_url."img/eventDefault.png";
                        }
                        $event["event_date"]=$data_notification['event_date'];
                        $event["event_day_only"]=date("d", strtotime($data_notification['event_date']));
                        $event["event_month_year_only"]=date("M, Y", strtotime($data_notification['event_date']));
                       
                        $event["event_type"]=$data_notification['event_type'];
                        $event["event_day_name"]=html_entity_decode($data_notification['event_day_name']);
                        $event["event_type"]=$data_notification['event_type'];
                        $event["event_time"]=date("h:i A", strtotime($data['event_date'].' '.$data_notification['event_time']));
                        $event["event_location"]=$data_notification['eventMom'];
                        $event["adult_booked"]=$data_notification['going_person'];
                        $event["child_booked"]=$data_notification['going_child'];
                        $event["guest_booked"]=$data_notification['going_guest'];
                        $event["recived_amount"]=number_format($data_notification['recived_amount']-$data_notification['transaction_charges'],2); 
                        $event["booking_id"]='BKG'.$data_notification['event_attend_id'];
                        $event["invoice_url"] =  $base_url."apAdmin/invoice.php?user_id=$data_notification[user_id]&unit_id=$data_notification[unit_id]&type=E&societyid=$data_notification[society_id]&id=$data_notification[event_id]&event_attend_id=$data_notification[event_attend_id]&language_id=$language_id";
                         array_push($response["event"], $event);
                    }

                    $response["message"]="Get Event Booking Success.";
                    $response["status"]="200";
                    echo json_encode($response);


                }else{

                    $response["message"]="No Event Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['getEventDetails']=="getEventDetails" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($event_id, FILTER_VALIDATE_INT) == true ){

                $qnotification=$d->select("event_master","society_id = '$society_id' AND event_id='$event_id'","");

                if(mysqli_num_rows($qnotification)>0){

                    $response["event"] = array();

                    $data_notification=mysqli_fetch_array($qnotification);

                    $data_notification = array_map("html_entity_decode", $data_notification);
                         
                        $event_id=$data_notification['event_id'];

                        $event = array(); 
                        $today=date('Y-m-d');
                        $cTime= date("H:i:s");
                        $expire = strtotime($data_notification['event_end_date']);
                        $today = strtotime("today $cTime");
                        if($data_notification['active_status']==1 || $today > $expire){
                            $eventEnd=0;
                            $event["hide_status"]="0";
                          } else {
                            $eventEnd=1;
                            $event["hide_status"]="1";
                          }
                        $event["event_id"]=$data_notification['event_id'];
                        $id=$data_notification['event_id'];
                        $event["event_title"]=html_entity_decode($data_notification['event_title']);
                        if(file_exists("../img/event_image/$data_notification[event_image]")) { 
                            $event["event_image"]=$base_url."img/event_image/".$data_notification['event_image'];
                        } else {
                            $event["event_image"]=$base_url."img/eventDefault.png";
                        }
                        $event["event_upcoming_completed"]=$data_notification['event_upcoming_completed'];
                        $event["event_description"]=html_entity_decode($data_notification['event_description']);
                        $event["event_start_date"]=$data_notification['event_start_date'];
                        $event["event_start_date_view"]=date("d M Y h:i A", strtotime($data_notification['event_start_date']));
                        $event["event_end_date"]=date("d M Y h:i A", strtotime($data_notification['event_end_date']));
                        $event["event_location"]=$data_notification['eventMom'];
                        $event["event_type"]=$data_notification['event_type'];
                        $event["adult_charge"]=$data_notification['adult_charge'];
                        $event["child_charge"]=$data_notification['child_charge'];
                        $event["guest_charge"]=$data_notification['guest_charge'];
                        $event["balancesheet_id"]=$data_notification['balancesheet_id'];
                        $event["maximum_pass"]=$data_notification['maximum_pass'].'';
                        $event["maximum_pass_adult"]=$data_notification['maximum_pass_adult'].'';
                        $event["maximum_pass_children"]=$data_notification['maximum_pass_children'].'';
                        $event["maximum_pass_guests"]=$data_notification['maximum_pass_guests'].'';
                        $event["booking_open"]=$data_notification['booking_open'].'';

                         $count=$d->sum_data("going_person","event_attend_list","event_id='$id'");
                         while($row=mysqli_fetch_array($count))
                        {
                             $asif=$row['SUM(going_person)'];
                             $totalMain=number_format($asif);

                         }
                         $count1=$d->sum_data("going_child","event_attend_list","event_id='$id'");
                         while($row1=mysqli_fetch_array($count1))
                        {
                             $asif1=$row1['SUM(going_child)'];
                             $totalMain1=number_format($asif1);

                         }
                          $count2=$d->sum_data("going_guest","event_attend_list","event_id='$id'");
                         while($row2=mysqli_fetch_array($count2))
                        {
                             $asif2=$row2['SUM(going_guest)'];
                             $totalMain2=number_format($asif2);

                         }
                        $event["total_population"]=$totalMain+$totalMain1+$totalMain2.'';
                         $fq=$d->select("users_master","user_id='$user_id' AND society_id='$society_id'");
                        $ownerData=mysqli_fetch_array($fq);
                        $userType= $ownerData['user_type'];

                        $goinginfo=$d->select("event_attend_list,users_master","event_attend_list.unit_id = '$unit_id' and event_attend_list.event_id='$id' AND event_attend_list.user_id=users_master.user_id AND event_attend_list.userType=users_master.user_type AND event_attend_list.userType='$userType'");


                        if(mysqli_num_rows($goinginfo)>0)
                        {
                            $event["going_person"]= "0";
                            $data = mysqli_fetch_array($goinginfo);
                            $totalNumPerson= $data['going_person']+$data['going_child']+$data['going_guest'];
                            $event["numberof_person"] =$totalNumPerson.'';
                            $event["going_adult"] =$data['going_person'];
                            $event["going_child"] = $data['going_child'];
                            $event["going_guest"] = $data['going_guest'];
                            $event["user_id"] = $data['user_id'];
                            $event["recived_amount"] = $data['recived_amount'];
                            $event["notes_person"] = $data['notes'];
                            $event["invoice_url"] =  $base_url."apAdmin/invoice.php?user_id=$data[user_id]&unit_id=$data[unit_id]&type=E&societyid=$data[society_id]&id=$data[event_id]&event_attend_id=$data[event_attend_id]";

                            if($data_notification['event_type']==0) {
                            $event["booked_by"] = "Already Booked By ".$data['user_full_name'];
                            }  else {
                                if ($data['going_guest']>0) {
                            $event["booked_by"] = "Already Booked By ".$data['user_full_name']."\n Adult : $data[going_person], Child: $data[going_child] & Guest: $data[going_guest] \nTotal Paid Amount: $data[recived_amount]";
                                } else {
                                $event["booked_by"] = "Already Booked By ".$data['user_full_name']."\n Adult : $data[going_person] & Child: $data[going_child] \nTotal Paid Amount: $data[recived_amount]";

                                }
                            }
                                
                        }
                        else
                        {
                            $event["going_person"]= "1";
                            $event["recived_amount"] = "0.00";
                        }


                        
                            $event["gallery"] = array();
                            $gallary_data=$d->select("gallery_master","society_id ='$society_id' AND event_id='$event_id' AND society_id!=0");
                             while($data_gallary_list=mysqli_fetch_array($gallary_data)) {

                                $gallery = array(); 
                                $gallery["gallery_id"]=$data_gallary_list['gallery_id'];
                                $gallery["society_id"]=$data_gallary_list['society_id'];
                                $gallery["gallery_title"]=$data_gallary_list['gallery_title'];
                                $gallery["gallery_photo"]=$base_url."img/gallery/".$data_gallary_list['gallery_photo'];
                                $gallery["event_id"]=$data_gallary_list['event_id'];
                                $gallery["upload_date_time"]=$data_gallary_list['upload_date_time'];
                              
                                array_push($event["gallery"], $gallery); 
                            }
                            array_push($response["event"], $event);

                       
                   

                    $response["message"]="Get Event Success.";
                    $response["status"]="200";
                    echo json_encode($response);


                }else{

                    $response["message"]="No Event Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['getEventDetails']=="getEventDetailsNew" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($event_id, FILTER_VALIDATE_INT) == true ){

                $qnotification=$d->select("event_master","society_id = '$society_id' AND event_id='$event_id'","");

                if(mysqli_num_rows($qnotification)>0){

                    $response["event"] = array();

                    $data_notification=mysqli_fetch_array($qnotification);

                    print_r($data_notification);

                    $data_notification = array_map("html_entity_decode", $data_notification);
                         
                        $event_id=$data_notification['event_id'];

                        $event = array(); 
                        $today=date('Y-m-d');
                        $cTime= date("H:i:s");
                        $expire = strtotime($data_notification['event_end_date']);
                        $today = strtotime("today $cTime");
                        if($data_notification['active_status']==1 || $today > $expire){
                            $eventEnd=0;
                            $event["hide_status"]="0";
                          } else {
                            $eventEnd=1;
                            $event["hide_status"]="1";
                          }
                        $event["event_id"]=$data_notification['event_id'];
                        $id=$data_notification['event_id'];
                        $event["event_title"]=html_entity_decode($data_notification['event_title']);
                        if(file_exists("../img/event_image/$data_notification[event_image]")) { 
                            $event["event_image"]=$base_url."img/event_image/".$data_notification['event_image'];
                        } else {
                            $event["event_image"]=$base_url."img/eventDefault.png";
                        }
                        $event["event_upcoming_completed"]=$data_notification['event_upcoming_completed'];
                        $event["event_description"]=html_entity_decode($data_notification['event_description']);
                        $event["event_start_date"]=$data_notification['event_start_date'];
                        $event["event_start_date_view"]=date("d M Y ", strtotime($data_notification['event_start_date']));
                        $event["event_end_date"]=date("d M Y ", strtotime($data_notification['event_end_date']));
                        $event["event_location"]=$data_notification['eventMom'];
                        $event["event_type"]='0';
                       
                        $event["booking_open"]=$data_notification['booking_open'].'';

                        $qs=$d->select("event_days_master","event_id='$data_notification[event_id]' AND society_id='$society_id'");

                        if(mysqli_num_rows($qs)>0){

                        $event["event_days"] = array();
                          while($data=mysqli_fetch_array($qs)) {
                             $q3=$d->select("balancesheet_master,society_payment_getway","society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data[balancesheet_id]' AND balancesheet_master.society_payment_getway_id!=0 OR society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data[balancesheet_id]' AND balancesheet_master.society_payment_getway_id_upi!=0");


                            $event_days = array(); 
                            $today=date('Y-m-d');
                            // $cTime= date("H:i:s");
                            $expire = strtotime($data['event_date']);
                            $today = strtotime("$today midnight");
                            if($data['active_status']==1 || $today > $expire){
                                $event_days["day_booking_open"]=FALSE;
                              } else {
                                $event_days["day_booking_open"]=TRUE;
                              }

                            if ($user_type!='' && $user_type==0) {
                                   $adult_charge = $data['adult_charge'];
                                   $child_charge =   $data['child_charge'];
                                   $guest_charge =   $data['guest_charge'];
                              
                            } else if ($user_type!='' && $user_type==1 ) {
                                   $adult_charge  = $data['adult_charge_tenant'];
                                   $child_charge =   $data['child_charge_tenant'];
                                   $guest_charge =   $data['guest_charge_tenant'];
                            } else {
                                  $adult_charge = $data['adult_charge'];
                                   $child_charge =   $data['child_charge'];
                                   $guest_charge =   $data['guest_charge'];
                            }

                            $event_days["events_day_id"]=$data['events_day_id'];
                            $event_days["event_id"]=$data['event_id'];
                            $event_days["event_day_name"]=html_entity_decode($data['event_day_name']);
                            $event_days["event_date"]=date("d F Y", strtotime($data['event_date']));
                            $event_days["event_time"]=date("h:i A", strtotime($data['event_time']));
                            $event_days["event_type"]=$data['event_type'];
                            $event_days["balancesheet_id"]=$data['balancesheet_id'];

                             if (mysqli_num_rows($q3)>0) {
                                  $event_days["isPay"]=true;   
                              } else {
                                  $event_days["isPay"]=false;   
                              }

                            if ($data_notification['bill_type']=='GST' && $data_notification['gst_type']=='IGST' && $data["event_type"]==1) {
                                $gstValueAdult =  $adult_charge - ($adult_charge  * (100/(100+$data_notification['tax_slab']))); 
                                $gstValueChild = $child_charge - ($child_charge  * (100/(100+$data_notification['tax_slab']))); 
                                $gstValueGuest =$guest_charge - ($guest_charge  * (100/(100+$data_notification['tax_slab'])));
                                // find igst
                                // if ($data_notification['gst']=="0") {
                                   $adult_charge =   $adult_charge-$gstValueAdult;
                                   $child_charge =   $child_charge-$gstValueChild;
                                   $guest_charge =   $guest_charge-$gstValueGuest;
                                // } else {
                                //    $adult_charge =   $adult_charge;
                                //    $child_charge =   $child_charge;
                                //    $guest_charge =   $guest_charge;
                                // }
                                $event_days["adult_charge"]=sprintf("%.2f",$adult_charge);
                                $event_days["child_charge"]=sprintf("%.2f",$child_charge);
                                $event_days["guest_charge"]=sprintf("%.2f",$guest_charge);
                                   
                            } else if ($data_notification['bill_type']=='GST' && $data_notification['gst_type']=='CGST/SGST' && $data["event_type"]==1) {


                                $gstValueAdult =  $adult_charge - ($adult_charge  * (100/(100+$data_notification['tax_slab']))); 
                                $gstValueChild = $child_charge - ($child_charge  * (100/(100+$data_notification['tax_slab']))); 
                                $gstValueGuest =$guest_charge - ($guest_charge  * (100/(100+$data_notification['tax_slab'])));
                                // if ($data_notification['gst']=="0") {
                                   $adult_charge =   $adult_charge-$gstValueAdult;
                                   $child_charge =   $child_charge-$gstValueChild;
                                   $guest_charge =   $guest_charge-$gstValueGuest;
                                // } else {
                                //    $adult_charge =   $adult_charge;
                                //    $child_charge =   $child_charge;
                                //    $guest_charge =   $guest_charge;
                                // }
                                
                                $event_days["adult_charge"]=number_format($adult_charge,2,'.','');
                                $event_days["child_charge"]=number_format($child_charge,2,'.','');
                                $event_days["guest_charge"]=number_format($guest_charge,2,'.','');
                                // $penalty["sgst_amount"]=number_format($sepGst,2,'.','');
                            } else {

                                $event_days["adult_charge"]=$adult_charge;
                                $event_days["child_charge"]=$child_charge;
                                $event_days["guest_charge"]=$guest_charge;

                            } 

                            // if ($data['bill_type']=="GST") {
                            //  $event_days["amount_without_gst"] =sprintf("%.2f",$data['amount_without_gst']);
                            // } else {
                            //  $event_days["amount_without_gst"]=$data['penalty_amount'];
                            // }
                            $event_days["maximum_pass_adult"]=$data['maximum_pass_adult'];
                            $event_days["maximum_pass_children"]=$data['maximum_pass_children'];
                            $event_days["maximum_pass_guests"]=$data['maximum_pass_guests'];
                            $event_days["bill_type"]=$data_notification['bill_type'];
                            $event_days["gst"]=$data_notification['gst'];
                            $event_days["gst_type"]=$data_notification['gst_type'];
                            $event_days["gst_slab"]=$data_notification['tax_slab'];

                            $count=$d->sum_data("going_person","event_attend_list","event_id='$id' AND events_day_id='$data[events_day_id]' AND book_status!=3");
                             while($row=mysqli_fetch_array($count))
                            {
                                 $asif=$row['SUM(going_person)'];
                                 $totalAdult=number_format($asif);

                             }
                             // child Count
                             $count1=$d->sum_data("going_child","event_attend_list","event_id='$id'  AND events_day_id='$data[events_day_id]' AND book_status!=3");
                             while($row1=mysqli_fetch_array($count1))
                            {
                                 $asif1=$row1['SUM(going_child)'];
                                 $totalChild=number_format($asif1);

                             }

                             // guest count
                              $count2=$d->sum_data("going_guest","event_attend_list","event_id='$id'  AND events_day_id='$data[events_day_id]' AND book_status!=3");
                             while($row2=mysqli_fetch_array($count2))
                            {
                                 $asif2=$row2['SUM(going_guest)'];
                                 $totalGuest=number_format($asif2);

                             }

                            $event_days["adult_booked"]=$totalAdult.'';
                            $event_days["child_booked"]=$totalChild.'';
                            $event_days["guest_booked"]=$totalGuest.'';
                            

                            if ($data['maximum_user_pass_adult'] != 0) {
                                $event_days["remaining_pass_adult"]=$data['maximum_user_pass_adult'] - $totalAdult;
                            }else{
                                $remaining_pass_adult= $data['maximum_pass_adult']-$totalAdult.'';
                                $event_days["remaining_pass_adult"]=$remaining_pass_adult;
                            }

                            if ($data['maximum_user_pass_children'] != 0) {
                                $event_days["remaining_pass_children"]=$data['maximum_user_pass_children'] - $totalAdult;
                            }else{
                                $remaining_pass_children= $data['maximum_pass_children']-$totalChild.'';
                                $event_days["remaining_pass_children"]=$remaining_pass_children;
                            }

                            if ($data['maximum_user_pass_guests'] != 0) {
                                $event_days["remaining_pass_guests"]=$data['maximum_user_pass_guests'] - $totalGuest;
                            }else{
                                $remaining_pass_guests= $data['maximum_pass_guests']-$totalGuest.'';

                                $event_days["remaining_pass_guests"]=$remaining_pass_guests;

                            }

                            

                            if ($remaining_pass_adult==0 && $remaining_pass_children==0 && $remaining_pass_guests==0) {
                                $event_days["sold_out"]=TRUE;
                            } else {
                                $event_days["sold_out"]=FALSE;
                            }

                            $event_days["total_booked"]=$totalAdult+$totalChild+$totalGuest.'';
                           

                            array_push($event["event_days"],$event_days); 
                          }

                        }

                        
                            $event["gallery"] = array();
                            $gallary_data=$d->select("gallery_master","society_id ='$society_id' AND event_id='$event_id' AND society_id!=0");
                             while($data_gallary_list=mysqli_fetch_array($gallary_data)) {

                                $gallery = array(); 
                                $gallery["gallery_id"]=$data_gallary_list['gallery_id'];
                                $gallery["society_id"]=$data_gallary_list['society_id'];
                                $gallery["gallery_title"]=$data_gallary_list['gallery_title'];
                                $gallery["gallery_photo"]=$base_url."img/gallery/".$data_gallary_list['gallery_photo'];
                                $gallery["event_id"]=$data_gallary_list['event_id'];
                                $gallery["upload_date_time"]=$data_gallary_list['upload_date_time'];
                              
                                array_push($event["gallery"], $gallery); 
                            }
                            array_push($response["event"], $event);

                       
                   

                    $response["message"]="Get Event Success.";
                    $response["status"]="200";
                    echo json_encode($response);


                }else{

                    $response["message"]="No Event Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['getEventDetailsNew']=="getEventDetailsNew" && $society_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($event_id, FILTER_VALIDATE_INT) == true ){

                $qnotification=$d->select("event_master","society_id = '$society_id' AND event_id='$event_id'","");

                if(mysqli_num_rows($qnotification)>0){

                    $response["event"] = array();

                    $data_notification=mysqli_fetch_array($qnotification);

                    $data_notification = array_map("html_entity_decode", $data_notification);
                         
                        $event_id=$data_notification['event_id'];

                        $event = array(); 
                        $today=date('Y-m-d');
                        $cTime= date("H:i:s");
                        $expire = strtotime($data_notification['event_end_date']);
                        $today = strtotime("today $cTime");
                        if($data_notification['active_status']==1 || $today > $expire){
                            $eventEnd=0;
                            $event["hide_status"]="0";
                          } else {
                            $eventEnd=1;
                            $event["hide_status"]="1";
                          }
                        $event["event_id"]=$data_notification['event_id'];
                        $id=$data_notification['event_id'];
                        $event["event_title"]=html_entity_decode($data_notification['event_title']);
                        if(file_exists("../img/event_image/$data_notification[event_image]")) { 
                            $event["event_image"]=$base_url."img/event_image/".$data_notification['event_image'];
                        } else {
                            $event["event_image"]=$base_url."img/eventDefault.png";
                        }
                        $event["event_upcoming_completed"]=$data_notification['event_upcoming_completed'];
                        $event["event_description"]=html_entity_decode($data_notification['event_description']);
                        $event["event_start_date"]=$data_notification['event_start_date'];
                        $event["event_start_date_view"]=date("d M Y ", strtotime($data_notification['event_start_date']));
                        $event["event_end_date"]=date("d M Y ", strtotime($data_notification['event_end_date']));
                        $event["event_location"]=$data_notification['eventMom'];
                       
                       
                        $event["booking_open"]=$data_notification['booking_open'].'';

                        $qs=$d->select("event_days_master","event_id='$data_notification[event_id]' AND society_id='$society_id'");

                        if(mysqli_num_rows($qs)>0){

                        $event["event_days"] = array();
                          while($data=mysqli_fetch_array($qs)) {
                             $q3=$d->select("balancesheet_master,society_payment_getway","society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data[balancesheet_id]' AND balancesheet_master.society_payment_getway_id!=0 OR society_payment_getway.society_payment_getway_id=society_payment_getway.society_payment_getway_id AND society_payment_getway.society_id='$society_id' AND balancesheet_master.balancesheet_id='$data[balancesheet_id]' AND balancesheet_master.society_payment_getway_id_upi!=0");


                            $event_days = array(); 
                            $today=date('Y-m-d');
                            // $cTime= date("H:i:s");
                            $expire = strtotime($data['event_date']);
                            $today = strtotime("$today midnight");
                            if($data['active_status']==1 || $today > $expire){
                                $event_days["day_booking_open"]=FALSE;
                              } else {
                                $event_days["day_booking_open"]=TRUE;
                              }

                            if ($user_type!='' && $user_type==0) {
                                   $adult_charge = $data['adult_charge'];
                                   $child_charge =   $data['child_charge'];
                                   $guest_charge =   $data['guest_charge'];
                              
                            } else if ($user_type!='' && $user_type==1 ) {
                                   $adult_charge  = $data['adult_charge_tenant'];
                                   $child_charge =   $data['child_charge_tenant'];
                                   $guest_charge =   $data['guest_charge_tenant'];
                            } else {
                                  $adult_charge = $data['adult_charge'];
                                   $child_charge =   $data['child_charge'];
                                   $guest_charge =   $data['guest_charge'];
                            }

                            $event_days["events_day_id"]=$data['events_day_id'];
                            $event_days["event_id"]=$data['event_id'];
                            $event_days["event_day_name"]=html_entity_decode($data['event_day_name']);
                            $event_days["event_date"]=date("d F Y", strtotime($data['event_date']));
                            $event_days["event_time"]=date("h:i A", strtotime($data['event_time']));
                            $event_days["event_type"]=$data['event_type'];
                            $event_days["balancesheet_id"]=$data['balancesheet_id'];

                             if (mysqli_num_rows($q3)>0) {
                                  $event_days["isPay"]=true;   
                              } else {
                                  $event_days["isPay"]=false;   
                              }

                           

                            $event_days["adult_charge"]=$adult_charge;
                            $event_days["child_charge"]=$child_charge;
                            $event_days["guest_charge"]=$guest_charge;


                            $event_days["adult_charge_view"]=number_format($adult_charge,2);
                            $event_days["child_charge_view"]=number_format($child_charge,2);
                            $event_days["guest_charge_view"]=number_format($guest_charge,2);
                           
                            $event_days["maximum_pass_adult"]=$data['maximum_pass_adult'];
                            $event_days["maximum_pass_children"]=$data['maximum_pass_children'];
                            $event_days["maximum_pass_guests"]=$data['maximum_pass_guests'];

                            $event_days["maximum_user_pass_adult"]=$data['maximum_user_pass_adult'];
                            $event_days["maximum_user_pass_children"]=$data['maximum_user_pass_children'];
                            $event_days["maximum_user_pass_guests"]=$data['maximum_user_pass_guests'];

                            $event_days["event_allow"]=$data['event_allow'];

                            $event_days["is_taxble"]=$data_notification['is_taxble'];
                            $event_days["gst"]=$data_notification['gst'];
                            $event_days["taxble_type"]=$data_notification['taxble_type'];
                            $event_days["tax_slab"]=$data_notification['tax_slab'];

                            $count=$d->sum_data("going_person","event_attend_list","event_id='$id' AND events_day_id='$data[events_day_id]' AND book_status!=3");
                             while($row=mysqli_fetch_array($count))
                            {
                                 $asif=$row['SUM(going_person)'];
                                 $totalAdult=number_format($asif);

                             }

                             // child Count
                             $count1=$d->sum_data("going_child","event_attend_list","event_id='$id'  AND events_day_id='$data[events_day_id]' AND book_status!=3");
                             while($row1=mysqli_fetch_array($count1))
                            {
                                 $asif1=$row1['SUM(going_child)'];
                                 $totalChild=number_format($asif1);

                             }

                             // guest count
                              $count2=$d->sum_data("going_guest","event_attend_list","event_id='$id'  AND events_day_id='$data[events_day_id]' AND book_status!=3");
                             while($row2=mysqli_fetch_array($count2))
                            {
                                 $asif2=$row2['SUM(going_guest)'];
                                 $totalGuest=number_format($asif2);

                             }

                             /// My Booking count

                             // Adult

                            $countMyAdult=$d->sum_data("going_person","event_attend_list","event_id='$id' AND events_day_id='$data[events_day_id]' AND book_status!=3 AND user_id = '$user_id'");
                             while($rowMyAdult=mysqli_fetch_array($countMyAdult))
                            {
                                 $datMyAdult=$rowMyAdult['SUM(going_person)'];
                                 $totalMyAdult=number_format($datMyAdult);

                             }
                             
                             // child Count
                             $count1Child=$d->sum_data("going_child","event_attend_list","event_id='$id'  AND events_day_id='$data[events_day_id]' AND book_status!=3  AND user_id = '$user_id'");
                             while($row1Child=mysqli_fetch_array($count1Child))
                            {
                                 $asif1Child=$row1Child['SUM(going_child)'];
                                 $totalMyChild=number_format($asif1Child);

                             }

                             // guest count
                              $count2GUest=$d->sum_data("going_guest","event_attend_list","event_id='$id'  AND events_day_id='$data[events_day_id]' AND book_status!=3  AND user_id = '$user_id'");
                             while($row2Guest=mysqli_fetch_array($count2GUest))
                            {
                                 $asif2Guest=$row2Guest['SUM(going_guest)'];
                                 $totalMyGuest=number_format($asif2Guest);

                             }

                            $event_days["adult_booked"]=$totalAdult.'';
                            $event_days["child_booked"]=$totalChild.'';
                            $event_days["guest_booked"]=$totalGuest.'';
                            $remaining_pass_adult= $data['maximum_pass_adult']-$totalAdult.'';
                            $remaining_pass_children= $data['maximum_pass_children']-$totalChild.'';
                            $remaining_pass_guests= $data['maximum_pass_guests']-$totalGuest.'';

                            if ($data['maximum_user_pass_adult'] != 0) {
                                $remaining_pass_adult = "1";
                                $event_days["remaining_pass_adult"]=$remaining_pass_adult.'';
                            }else{
                                $remaining_pass_adult= "1";
                                $event_days["remaining_pass_adult"]=$remaining_pass_adult;
                            }

                            if ($data['maximum_user_pass_children'] != 0) {
                                $remaining_pass_children = $data['maximum_user_pass_children'] - $totalMyChild;
                                $event_days["remaining_pass_children"]=$remaining_pass_children.'';
                            }else{
                                $remaining_pass_children= $data['maximum_pass_children']-$totalChild.'';
                                $event_days["remaining_pass_children"]=$remaining_pass_children;
                            }

                            if ($data['maximum_user_pass_guests'] != 0) {
                                $remaining_pass_guests= $data['maximum_user_pass_guests']-$totalMyGuest.'';
                                $event_days["remaining_pass_guests"]=$remaining_pass_guests.'';
                            }else{
                                $remaining_pass_guests= $data['maximum_pass_guests']-$totalGuest.'';
                                $event_days["remaining_pass_guests"]=$remaining_pass_guests;
                            }

                            if ($remaining_pass_adult==0 && $remaining_pass_children==0 && $remaining_pass_guests==0) {
                                $event_days["sold_out"]=TRUE;
                            } else {
                                $event_days["sold_out"]=FALSE;
                            }

                            $event_days["total_booked"]=$totalAdult+$totalChild+$totalGuest.'';
                           

                            array_push($event["event_days"],$event_days); 
                          }

                        }

                        
                        $event["gallery"] = array();
                        $gallary_data=$d->select("gallery_master","society_id ='$society_id' AND event_id='$event_id' AND society_id!=0");
                         while($data_gallary_list=mysqli_fetch_array($gallary_data)) {

                            $gallery = array(); 
                            $gallery["gallery_id"]=$data_gallary_list['gallery_id'];
                            $gallery["society_id"]=$data_gallary_list['society_id'];
                            $gallery["gallery_title"]=$data_gallary_list['gallery_title'];
                            $gallery["gallery_photo"]=$base_url."img/gallery/".$data_gallary_list['gallery_photo'];
                            $gallery["event_id"]=$data_gallary_list['event_id'];
                            $gallery["upload_date_time"]=$data_gallary_list['upload_date_time'];
                          
                            array_push($event["gallery"], $gallery); 
                        }
                        array_push($response["event"], $event);

                    $response["message"]="Get Event Success.";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{
                    $response["message"]="No Event Found.";
                    $response["status"]="201";
                    echo json_encode($response);
                }
            }else if($_POST['cancelAttend']=="cancelAttend" && filter_var($event_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                $qqq=$d->select("event_days_master,event_master,event_attend_list","event_attend_list.events_day_id=event_days_master.events_day_id AND event_master.event_id=event_days_master.event_id AND event_master.event_id='$event_id' AND event_attend_list.event_attend_id='$event_attend_id'");
                
                $eventData=mysqli_fetch_array($qqq);
                $event_title=$eventData['event_title'];
                if ($eventData['event_type']==1) {
                    $response["message"]="Please contact to $societyLngName admin for cancel this event booking !";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }

                $a1= array (
                 'book_status'=> "3" 
                );
                   $qnotification=$d->update("event_attend_list",$a1,"society_id = '$society_id' AND unit_id = '$unit_id' AND event_attend_id='$event_attend_id'");

                if($qnotification>0){
                    // $d->delete("event_passes_master","unit_id = '$unit_id' AND event_id='$event_id'");
                     $d->insert_myactivity($user_id,"$society_id","0","$user_name","$event_title event cancelled by You","Events_1xxxhdpi.png");
                    $response["message"]="Event Booking Cancelled Successfully";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No event Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else if($_POST['getPasses']=="getPasses" && $event_id!='' && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($event_id, FILTER_VALIDATE_INT) == true ){

                $qcheck=$d->selectRow("user_type,member_status","users_master","user_id='$user_id'");
                $userData=mysqli_fetch_array($qcheck);
                $user_type=$userData['user_type'];

                $q=$d->select("event_passes_master,event_master","event_passes_master.event_id=event_master.event_id AND event_passes_master.event_id='$event_id' AND event_passes_master.unit_id='$unit_id' AND event_passes_master.userType='$user_type'" );
                if(mysqli_num_rows($q)>0){
                        $response["passes"] = array();

                    while($data=mysqli_fetch_array($q)) {
                        $passes = array(); 
                        
                        $qr_size = "300x300";
                        $qr_content = "FIN_" . $data['pass_id'];
                        $qr_correction = strtoupper('H');
                        $qr_encoding = 'UTF-8';
                        $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                        $passes["pass_id"]=$data['pass_id'];
                        $passes["event_id"]=$data['event_id'];
                        $passes["pass_no"]='TKT'.$data['user_id'].$data['pass_id'];
                        $passes["entry_status"]=$data['entry_status'];
                        $passes["event_title"]=$data['event_title'];
                        $passes["event_start_date"]=date("d M Y h:i A", strtotime($data['event_start_date']));
                        if ($data['pass_type']==0) {
                            if ($data['event_type']==0) {
                                $passes["pass_type"]="Member Pass";
                            } else {
                                $passes["pass_type"]="Member Adult";
                            }
                        } elseif ($data['pass_type']==1) {
                            $passes["pass_type"]="Child ";
                        } else if ($data['pass_type']==2){
                            $passes["pass_type"]="Guest";
                        }
                        $passes['qr_code'] = $qrImageUrl;
                        $passes['qr_code_ios'] = $qrImageUrl . '.png';
                        array_push($response["passes"], $passes);

                    }
                    $response["message"]="Get Passes Successfully";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No Passes Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['getPassesDayWise']=="getPassesDayWise" && $event_id!='' && filter_var($user_id, FILTER_VALIDATE_INT) == true && filter_var($event_id, FILTER_VALIDATE_INT) == true  && filter_var($event_attend_id, FILTER_VALIDATE_INT) == true ){

                $qcheck=$d->selectRow("user_type,member_status","users_master","user_id='$user_id'");
                $userData=mysqli_fetch_array($qcheck);
                $user_type=$userData['user_type'];

                $q=$d->select("event_passes_master,event_master,event_days_master,event_attend_list","event_passes_master.events_day_id=event_days_master.events_day_id  AND event_attend_list.event_attend_id=event_passes_master.event_attend_id AND event_days_master.event_id=event_master.event_id AND event_passes_master.event_id=event_master.event_id AND event_passes_master.event_id='$event_id' AND event_passes_master.unit_id='$unit_id' AND event_passes_master.userType='$user_type' AND event_passes_master.event_attend_id='$event_attend_id'" );
                if(mysqli_num_rows($q)>0){
                        $response["passes"] = array();

                    while($data=mysqli_fetch_array($q)) {
                        $passes = array(); 
                        
                        $qr_size = "300x300";
                        $qr_content = "FIN_" . $data['pass_id'];
                        $qr_correction = strtoupper('H');
                        $qr_encoding = 'UTF-8';
                        $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                        $passes["pass_id"]=$data['pass_id'];
                        $passes["event_id"]=$data['event_id'];
                        $passes["pass_no"]='TKT'.$data['user_id'].$data['pass_id'];
                        $passes["entry_status"]=$data['entry_status'];
                        $passes["event_title"]=html_entity_decode($data['event_title'].'-'.$data['event_day_name']);
                        $passes["event_image"]=$base_url."img/event_image/".$data['event_image'];
                        
                        $passes["event_start_date"]=date("d M Y h:i A", strtotime($data['event_date'].' '.$data['event_time']));
                        if ($data['pass_type']==0) {
                                $passes["pass_type"]="Adult";
                        } elseif ($data['pass_type']==1) {
                            $passes["pass_type"]="Child ";
                        } else if ($data['pass_type']==2){
                            $passes["pass_type"]="Guest";
                        }
                        $passes['qr_code'] = $qrImageUrl;
                        $passes['qr_code_ios'] = $qrImageUrl . '.png';
                        array_push($response["passes"], $passes);

                    }
                    $response["message"]="Get Passes Successfully";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="No Passes Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>