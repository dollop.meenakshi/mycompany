<?php
include_once 'lib.php';

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb && $auth_check=='true') {
        
    $response = array();
    extract(array_map("test_input" , $_POST));
            if($_POST['getNotice']=="getNotice" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

                $response["notice"] = array();
               
                $nq=$d->select("notice_board_master","society_id='$society_id' AND society_id!=0 AND block_id=0 AND noticeboard_type=0 AND active_status=0" ,"ORDER BY updated_at DESC LIMIT 500");
                $nqBlock=$d->select("notice_board_master","society_id='$society_id' AND society_id!=0 AND block_id!=0 AND noticeboard_type=0 AND active_status=0" ,"ORDER BY updated_at DESC LIMIT 500");

                $nqUnit=$d->select("notice_board_master,notice_board_unit_master","notice_board_master.society_id='$society_id' AND notice_board_master.society_id!=0  AND notice_board_master.noticeboard_type=1 AND notice_board_unit_master.notice_board_id=notice_board_master.notice_board_id AND notice_board_unit_master.unit_id='$unit_id' AND notice_board_master.active_status=0" ,"ORDER BY notice_board_master.updated_at DESC LIMIT 500");

                $array1 = array();
                $array2 = array();
                $array3 = array();
                if(mysqli_num_rows($nqBlock)>0){

                    while($data_complain_list=mysqli_fetch_array($nqBlock)) {

                        $blockAray= explode(",",$data_complain_list['block_id']);

                        if(in_array($block_id, $blockAray)){
                            
                            array_push($array1, $data_complain_list);
                        }
                    }
                   
                }

                if(mysqli_num_rows($nqUnit)>0){

                     while($data_complain_list=mysqli_fetch_array($nqUnit)) {

                        $blockAray= explode(",",$data_complain_list['block_id']);
                        array_push($array3, $data_complain_list);
                    }
                   
                }

                if(mysqli_num_rows($nq)>0){

                    while($data_complain_list=mysqli_fetch_array($nq)) {

                            array_push($array2, $data_complain_list);
                    }
                   
                }

                
                $newArray = array_merge($array1,$array2,$array3);
                foreach ($newArray as $key => $part) {
                       $sort[$key] = strtotime($part['updated_at']);
                  }
                 array_multisort($sort, SORT_DESC, $newArray);


                for ($i=0; $i <count($newArray) ; $i++) { 
                     $notice = array(); 

                     $notice_board_id= $newArray[$i]['notice_board_id'];

                    $notice["notice_board_id"]=$newArray[$i]['notice_board_id'];
                    $notice["society_id"]=$newArray[$i]['society_id'];
                    $notice["notice_title"]=$newArray[$i]['notice_title'];
                    $notice["notice_description"]=$newArray[$i]['notice_description'];
                    $notice["notice_time"]=$newArray[$i]['notice_time'];
                    $notice["updated_time"]=$newArray[$i]['updated_at'];
                    if ($newArray[$i]['notice_attachment']!='') {
                    $notice["notice_attachment"]=$base_url."img/noticeBoard/".$newArray[$i]['notice_attachment'];
                    } else {
                    $notice["notice_attachment"]="";
                    }

                    // change readStatus 
                    $cqqq=$d->selectRow("read_id","notice_board_read_status","notice_board_id='$notice_board_id' AND user_id='$user_id'");
                        $aa = array(
                            'society_id' => $society_id,
                            'user_id' => $user_id,
                            'notice_board_id' => $notice_board_id,
                            'read_time' => date("Y-m-d H:i:s"),
                            'unit_id' => $unit_id,
                        );
                    if (mysqli_num_rows($cqqq)>0) {
                        $oldData=mysqli_fetch_array($cqqq);
                        $read_id=$oldData['read_id'];


                       $d->update("notice_board_read_status",$aa,"notice_board_id='$notice_board_id' AND user_id='$user_id'");
                    } else {
                       $d->insert("notice_board_read_status",$aa);
                    }

                    array_push($response["notice"], $notice);
                    
                }

                if (count($newArray)>0) {
                    $response["message"]="Get Notice Board Success.";
                    $response["status"]="200";
                    echo json_encode($response);
                } else {

                    $response["message"]="No Notice Board Found.";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            } else if($_POST['getNoticeFilter']=="getNoticeFilter" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

                $squ=$d->selectRow("zone_id,level_id","users_master","user_id='$user_id'");
                $userData = mysqli_fetch_array($squ);
                $zone_id = $userData['zone_id'];
                $level_id = $userData['level_id'];
                
                $appnedFilterQuery = "";
                switch ($filter_type) {
                    case '1':
                    // this month
                    $nFrom= date("Y-m-1 00:00:00");
                    $nTo= date("Y-m-t 23:59:59");
                    $appnedFilterQuery = " AND notice_board_master.updated_at BETWEEN '$nFrom' AND '$nTo'";
                        break;
                    case '2':
                    // prev month
                    $currentMonth = date('Y-m-d');
                    $nFrom = Date('Y-m-01 00:00:00', strtotime($currentMonth . " -1 month"));
                    $nTo = Date('Y-m-t 23:59:59', strtotime($currentMonth . " -1 month"));
                    $appnedFilterQuery = " AND notice_board_master.updated_at BETWEEN '$nFrom' AND '$nTo'";
                        break;
                    case '3':
                        // date range
                    $nFrom = date("Y-m-d 00:00:00", strtotime($from_date));
                    $nTo = date("Y-m-d 23:59:59", strtotime($to_date));
                    $appnedFilterQuery = " AND notice_board_master.updated_at BETWEEN '$nFrom' AND '$nTo'";
                        break;
                    default:
                    $limitQiryAll = 'LIMIT 15';
                    $limitQiryBlock = 'LIMIT 5';
                    $limitQiryUnit = 'LIMIT 5';
                        break;
                }

                $response["notice"] = array();
                
                // common
                $nq=$d->selectRow("notice_board_master.*,notice_board_read_status.read_id","notice_board_master LEFT JOIN notice_board_read_status ON notice_board_master.notice_board_id=notice_board_read_status.notice_board_id AND notice_board_read_status.user_id='$user_id' ","notice_board_master.society_id='$society_id' AND notice_board_master.society_id!=0 AND notice_board_master.noticeboard_zone_type=0 AND notice_board_master.noticeboard_level_type=0 AND notice_board_master.block_id=0 AND notice_board_master.noticeboard_type=0 AND notice_board_master.active_status=0  $appnedFilterQuery" ,"ORDER BY notice_board_master.updated_at DESC $limitQiryAll");
                
                // branch wise
                $nqBlock=$d->selectRow("notice_board_master.*,notice_board_read_status.read_id","notice_board_master LEFT JOIN notice_board_read_status ON notice_board_master.notice_board_id=notice_board_read_status.notice_board_id AND notice_board_read_status.user_id='$user_id'","notice_board_master.society_id='$society_id' AND notice_board_master.society_id!=0 AND notice_board_master.block_id!=0 AND notice_board_master.noticeboard_type=0 AND notice_board_master.noticeboard_zone_type=0 AND notice_board_master.noticeboard_level_type=0 AND  notice_board_master.active_status=0 AND FIND_IN_SET('$block_id',block_id) $appnedFilterQuery" ,"ORDER BY notice_board_master.updated_at DESC $limitQiryBlock");
                
                // department wise
                    $nqUnit=$d->selectRow("notice_board_master.*,notice_board_read_status.read_id","notice_board_unit_master,notice_board_master LEFT JOIN notice_board_read_status ON notice_board_master.notice_board_id=notice_board_read_status.notice_board_id AND notice_board_read_status.user_id='$user_id'","notice_board_master.society_id='$society_id' AND notice_board_master.society_id!=0  AND notice_board_master.noticeboard_type=1 AND notice_board_master.noticeboard_zone_type=0 AND notice_board_master.noticeboard_level_type=0  AND notice_board_unit_master.notice_board_id=notice_board_master.notice_board_id AND notice_board_unit_master.floor_id='$floor_id' AND notice_board_master.active_status=0  $appnedFilterQuery" ,"ORDER BY notice_board_master.updated_at DESC $limitQiryUnit");

                // zone wise
                $nqZone=$d->selectRow("notice_board_master.*,notice_board_read_status.read_id","notice_board_zone_master,notice_board_master LEFT JOIN notice_board_read_status ON notice_board_master.notice_board_id=notice_board_read_status.notice_board_id AND notice_board_read_status.user_id='$user_id'","notice_board_master.society_id='$society_id' AND notice_board_master.society_id!=0  AND notice_board_master.noticeboard_zone_type=1 AND notice_board_master.noticeboard_level_type=0 AND notice_board_zone_master.notice_board_id=notice_board_master.notice_board_id AND notice_board_zone_master.zone_id='$zone_id' AND notice_board_master.active_status=0  $appnedFilterQuery" ,"ORDER BY notice_board_master.updated_at DESC $limitQiryUnit");

                // level wise
                $nqLevel=$d->selectRow("notice_board_master.*,notice_board_read_status.read_id","notice_board_level_master,notice_board_master LEFT JOIN notice_board_read_status ON notice_board_master.notice_board_id=notice_board_read_status.notice_board_id AND notice_board_read_status.user_id='$user_id'","notice_board_master.society_id='$society_id' AND notice_board_master.society_id!=0  AND notice_board_master.noticeboard_level_type=1 AND notice_board_level_master.notice_board_id=notice_board_master.notice_board_id AND notice_board_level_master.level_id='$level_id' AND notice_board_master.active_status=0  $appnedFilterQuery" ,"ORDER BY notice_board_master.updated_at DESC $limitQiryUnit");

                $array1 = array();
                $array2 = array();
                $array3 = array();
                $array4 = array();
                $array5 = array();
                if(mysqli_num_rows($nqBlock)>0){

                    while($data_complain_list=mysqli_fetch_array($nqBlock)) {

                        $blockAray= explode(",",$data_complain_list['block_id']);

                        // if(in_array($block_id, $blockAray)){
                            
                            array_push($array1, $data_complain_list);
                        // }
                    }
                   
                }

                if(mysqli_num_rows($nqUnit)>0){

                     while($data_complain_list=mysqli_fetch_array($nqUnit)) {

                        $blockAray= explode(",",$data_complain_list['block_id']);
                        array_push($array3, $data_complain_list);
                    }
                   
                }

                if(mysqli_num_rows($nq)>0){

                    while($data_complain_list=mysqli_fetch_array($nq)) {

                            array_push($array2, $data_complain_list);
                    }
                   
                }

                if(mysqli_num_rows($nqZone)>0){

                     while($data_complain_list=mysqli_fetch_array($nqZone)) {

                        array_push($array4, $data_complain_list);
                    }
                   
                }
                if(mysqli_num_rows($nqLevel)>0){

                     while($data_complain_list=mysqli_fetch_array($nqLevel)) {

                        array_push($array5, $data_complain_list);
                    }
                   
                }


                
                $newArray = array_merge($array1,$array2,$array3,$array4,$array5);
                foreach ($newArray as $key => $part) {
                       $sort[$key] = strtotime($part['updated_at']);
                  }
                 array_multisort($sort, SORT_DESC, $newArray);


                for ($i=0; $i <count($newArray) ; $i++) { 
                     $notice = array(); 

                    $notice_board_id= $newArray[$i]['notice_board_id'];
                    $plainText = strip_tags($newArray[$i]['notice_description']);
                    // $plainTextTemp = test_input($plainText);
                    // $plainTextTempTmp = str_replace("&amp;nbsp;","",$plainTextTemp);
                    // $plainTextTempTmp11 = str_replace("&nbsp;","",$plainTextTempTmp);
                   

                    if (strlen($plainText)>105) {
                        $plainText = mb_substr($plainText,0,105) . '...';
                    } elseif (strlen(trim($plainText))==0 ) {
                        $plainText = "Click here to view";
                    }

                    $notice["notice_board_id"]=$newArray[$i]['notice_board_id'];
                    $notice["society_id"]=$newArray[$i]['society_id'];
                    $notice["notice_title"]=$newArray[$i]['notice_title'];
                    $notice["notice_description"]= html_entity_decode($plainText);
                    $notice["notice_time"]=date("h:i A, dS M Y", strtotime($newArray[$i]['updated_at']));
                    $notice["updated_time"]=$newArray[$i]['updated_at'];
                    if ($newArray[$i]['notice_attachment']!='') {
                    $notice["notice_attachment"]=$base_url."img/noticeBoard/".$newArray[$i]['notice_attachment'];
                    } else {
                    $notice["notice_attachment"]="";
                    }

                    // $cqqq=$d->selectRow("read_id","notice_board_read_status","notice_board_id='$notice_board_id' AND user_id='$user_id'");
                    if ($newArray[$i]['read_id']>0) {
                        $notice["read_status"] = true;
                    } else {
                        $notice["read_status"] = false;
                    }

                    array_push($response["notice"], $notice);
                    
                }

                if (count($newArray)>0) {
                   
                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);
                } else {

                    $response["message"]="$noDatafoundMsg ";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['getNoticeSingle']=="getNoticeSingle" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($notice_board_id, FILTER_VALIDATE_INT) == true){
                
               

                $response["notice"] = array();
               
                $nq=$d->select("notice_board_master","society_id='$society_id' AND society_id!=0 AND active_status=0  AND notice_board_id='$notice_board_id'" ,"ORDER BY updated_at DESC");

                $newArray = array();
                if(mysqli_num_rows($nq)>0){
                    while($data_complain_list=mysqli_fetch_array($nq)) {

                            array_push($newArray, $data_complain_list);
                    }
                   
                }

                

                for ($i=0; $i <count($newArray) ; $i++) { 
                     $notice = array(); 

                    $notice_board_id= $newArray[$i]['notice_board_id'];
                   
                    $notice["notice_board_id"]=$newArray[$i]['notice_board_id'];
                    $notice["society_id"]=$newArray[$i]['society_id'];
                    $notice["notice_title"]=$newArray[$i]['notice_title'];
                    $notice["notice_description"]= $newArray[$i]['notice_description'];
                    $notice["notice_time"]=$newArray[$i]['notice_time'];
                    $notice["updated_time"]=$newArray[$i]['updated_at'];
                    if ($newArray[$i]['notice_attachment']!='') {
                    $notice["notice_attachment"]=$base_url."img/noticeBoard/".$newArray[$i]['notice_attachment'];
                    } else {
                    $notice["notice_attachment"]="";
                    }

                    // change readStatus 
                    $cqqq=$d->selectRow("read_id","notice_board_read_status","notice_board_id='$notice_board_id' AND user_id='$user_id'");
                        $aa = array(
                            'society_id' => $society_id,
                            'user_id' => $user_id,
                            'notice_board_id' => $notice_board_id,
                            'read_time' => date("Y-m-d H:i:s"),
                            'unit_id' => $unit_id,
                        );
                    if (mysqli_num_rows($cqqq)>0) {
                        $oldData=mysqli_fetch_array($cqqq);
                        $read_id=$oldData['read_id'];


                       $d->update("notice_board_read_status",$aa,"notice_board_id='$notice_board_id' AND user_id='$user_id'");
                    } else {
                       $d->insert("notice_board_read_status",$aa);
                    }
                    
                    array_push($response["notice"], $notice);
                    
                }

                if (count($newArray)>0) {
                   
                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);
                } else {

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>