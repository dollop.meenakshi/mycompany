<?php
include_once 'lib.php';
 
if (isset($_POST) && !empty($_POST)) {
	$societyLngName=  $xml->string->society;
	if ($key == $keydb ) {
		$response = array();
		extract(array_map("test_input", $_POST));
		$today = date('Y-m-d');
		$todayTime = date('Y-m-d H:i:s');

		if (isset($user_login_new) && $user_login_new == 'user_login_new' && $user_mobile !="" && filter_var($society_id, FILTER_VALIDATE_INT) == true) {
			$user_mobile = mysqli_real_escape_string($con, $user_mobile);
			// $user_password=mysqli_real_escape_string($con, $user_password);

			if ($login_via==1) {
				
				$q = $d->select("users_master,block_master,floors_master,unit_master",
				"users_master.delete_status=0 AND users_master.user_email ='$user_mobile' AND users_master.user_email !=''
                AND users_master.block_id=block_master.block_id
                AND users_master.floor_id=floors_master.floor_id
                AND users_master.unit_id=unit_master.unit_id AND users_master.society_id ='$society_id' AND unit_master.unit_status!=4 AND users_master.user_status=1");

				$qPending = $d->select("users_master,block_master,floors_master,unit_master",
				"users_master.user_email ='$user_mobile' AND users_master.user_email !=''
                AND users_master.block_id=block_master.block_id
                AND users_master.floor_id=floors_master.floor_id
                AND users_master.unit_id=unit_master.unit_id AND users_master.society_id ='$society_id'
              AND  users_master.user_status=0 ");
			} else {

				if (strlen($user_mobile) < 8) {
					$response["message"] = "Please Enter Valid Mobile Number";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				}

				$q = $d->select("users_master,block_master,floors_master,unit_master",
					"users_master.delete_status=0 AND users_master.user_mobile ='$user_mobile' AND users_master.user_mobile !=0
	                AND users_master.block_id=block_master.block_id
	                AND users_master.floor_id=floors_master.floor_id
	                AND users_master.unit_id=unit_master.unit_id AND users_master.society_id ='$society_id'
	              AND unit_master.unit_status!=4 AND users_master.user_status=1 AND users_master.country_code='$country_code'");

				$qPending = $d->select("users_master,block_master,floors_master,unit_master",
					"users_master.user_mobile ='$user_mobile' AND users_master.user_mobile !=0
	                AND users_master.block_id=block_master.block_id
	                AND users_master.floor_id=floors_master.floor_id
	                AND users_master.unit_id=unit_master.unit_id AND users_master.society_id ='$society_id'
	              AND  users_master.user_status=0 AND users_master.country_code='$country_code'");

			}

			$user_data = mysqli_fetch_array($q);

			$socQry = $d->selectRow("bind_mac_address","society_master","society_id = '$society_id'");

			$socData = mysqli_fetch_array($socQry);

			if ($socData['bind_mac_address'] == 1 && $user_data['user_mac_address'] != '' && $user_data['mac_bind_on']!=1) {
				if ($user_data['user_mac_address'] != $user_mac_address) {
					$response["message"] = "You were registered with ".$user_data['phone_brand'].' - '.$user_data['phone_model'].", please contact to admin or request for new phone registration.";
					$response["status"] = "202";
					$response["user_id"] = $user_data['user_id'];
					$response["user_mobile"] = $user_data['user_mobile'];
					echo json_encode($response);
					exit();
				}
			}

			if ($user_data == TRUE) {

				$old_otp = $user_data['otp'];

				$user_id = $user_data['user_id'];
				$digits = 6;

				$to_time = strtotime($todayTime);
				$from_time = strtotime($user_data['otp_attempt_time']);
				$minitues = round(abs($to_time - $from_time) / 60, 2);

				if ($user_data['is_demo']==1 && $old_otp!='') {
					$otp = $old_otp;
				} else if ($old_otp != "" && strlen($old_otp) == 6 && $minitues < 60) {
					$otp = $old_otp;
				} else {
					$otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
					$otp = $otp;
				}


				if ($minitues < 60 && $user_data['opt_attempt'] > 3) {
					$response["message"] = "Too many attempts by you, Please  try again after 1 hour !";
					$response["status"] = "201";
					echo json_encode($response);
					exit();

				}

				

				if ($minitues > 60) {
					$opt_attempt = 1;
				} else if ($user_data['opt_attempt'] == "" || $opt_attempt < 1) {
					$opt_attempt = $user_data['opt_attempt'] + 1;
				} else {
					$opt_attempt = $user_data['opt_attempt'] + 1;
				}

				$m->set_data('otp', $otp);
				$a = array(
					'otp' => $m->get_data('otp'),
					'opt_attempt' => $opt_attempt,
					'otp_attempt_time' => $todayTime,
				);

				$d->update("users_master", $a, "user_id='$user_id'");
				$qSociety = $d->select("society_master", "society_id ='$society_id'");
				$user_data_society = mysqli_fetch_array($qSociety);
				$society_name = $user_data_society['society_name'];
				if ($user_data_society['society_status'] == '0') {


					if ($otp_type == 1 && $country_code=="+91" && $login_via!=1) {
						$smsObj->send_voice_otp($user_mobile, $otp,$country_code);
						$d->add_sms_log($user_mobile,"User App Voice OTP SMS",$society_id,$country_code,1);
						$you_will_receive_your_otp_on_call = $xml->string->you_will_receive_your_otp_on_call;
						$response["message"] = "$you_will_receive_your_otp_on_call";
						$response["is_firebase"] = false;

					}else if ($user_data['user_email']!='' && $otp_type == 2) {
						$to = $user_data['user_email'];
					 	$subject= "OTP Verification for MyCo App.";
						$app = "User application";
						$admin_name = $user_data['user_full_name'];
						$msg= "$otp"; 
						include '../apAdmin/mail/adminPanelLoginOTP.php';
						include '../apAdmin/mail.php';
						$otp_on_email = $xml->string->otp_on_email;
						$response["message"] = "$otp_on_email";
						$response["is_firebase"] = false;
					} else {
						$msgRespone = $smsObj->send_otp($user_mobile, $otp,$country_code);
						$d->add_sms_log($user_mobile,"User App OTP SMS",$society_id,$country_code,1);
						$otp_send_successfully = $xml->string->otp_send_successfully;
						$response["message"] = "$otp_send_successfully";

						if ($msgRespone==true) {
							$d->add_sms_log($user_mobile,"User App OTP SMS",$society_id,$country_code,1);
							$response["is_firebase"] = false;
						} else {
							$response["is_firebase"] = true;
						}
					}

					$a = mt_rand(100000,999999); 

					$response["trx_id"] = $a.$user_id.'';
					if ($user_data['user_email']!='' && $login_via!=1) {
						$response["is_email_otp"] = true;
					} else {
						$response["is_email_otp"] = false;
					}
					if ($country_code != "+91" ) {
						$response["is_voice_otp"] = false;
					} else if($login_via!=1) {
						$response["is_voice_otp"] = true;
					} else {
						$response["is_voice_otp"] = false;
					}
					$response["status"] = "200";
					echo json_encode($response);

					

				} else {
					$response["message"] = "Your $societyLngName is Deactive.";
					$response["status"] = "204";
					echo json_encode($response);
				}

			} else {

				if (mysqli_num_rows($qPending) > 0) {
					$response["message"] = "Account Not Activated Please Contact to $societyLngName Admin";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				} else {
					if($login_via==1) {
						$mobile_number_is_not_register = $xml->string->email_is_not_register;
					} else {
						$mobile_number_is_not_register = $xml->string->mobile_number_is_not_register;
					}
					$response["message"] = "$mobile_number_is_not_register";
					$response["status"] = "201";
					echo json_encode($response);
					exit();
				}

			}

		} else if (isset($user_verify_new_country) && $user_verify_new_country == 'user_verify_new_country' && filter_var($society_id, FILTER_VALIDATE_INT) == true ) {


				
			$user_mobile = mysqli_real_escape_string($con, $user_mobile);
			$otp = mysqli_real_escape_string($con, $otp);

			if ($user_mobile == "9737564998" && $society_id==2) {
				$is_firebase = "true";
			}

			if($login_via==1) {
				$q11 = $d->select("users_master", "user_email ='$user_mobile' AND delete_status=0");
			} else {
				$q11 = $d->select("users_master", "user_mobile ='$user_mobile' AND country_code='$country_code' AND delete_status=0");
			}

			$userData = mysqli_fetch_array($q11);

			$to_time = strtotime($todayTime);
			$from_time = strtotime($userData['otp_verfiy_time']);
			$minitues = round(abs($to_time - $from_time) / 60, 2);

			$socQry = $d->selectRow("bind_mac_address","society_master","society_id = '$society_id'");

			$socData = mysqli_fetch_array($socQry);

			if ($socData['bind_mac_address'] == 1 && $userData['user_mac_address'] != '' && $userData['mac_bind_on']!=1) {
				if ($userData['user_mac_address'] != $user_mac_address) {
					$response["message"] = "You were registered with ".$userData['phone_brand'].' - '.$userData['phone_model'].", please contact to admin or request for new phone registration.";
					$response["status"] = "202";
					echo json_encode($response);
					exit();
				}
			}

			if ($minitues < 60 && $userData['otp_verify_attempt'] > 5) {
				$response["message"] = "Too many attempts by you, Please try again after 1 hour !";
				$response["status"] = "201";
				echo json_encode($response);
				exit();

			}
			if($login_via==1) {
				$q = $d->select("users_master,block_master,floors_master,unit_master",
						"users_master.user_email ='$user_mobile' AND users_master.user_email !=''  AND users_master.otp='$otp'
	                AND users_master.block_id=block_master.block_id
	                AND users_master.floor_id=floors_master.floor_id
	                AND users_master.unit_id=unit_master.unit_id AND users_master.society_id ='$society_id' ");
			} else {

				if($is_firebase == "true" ) {
					$q = $d->select("users_master,block_master,floors_master,unit_master",
						"users_master.user_mobile ='$user_mobile' AND users_master.country_code='$country_code' AND users_master.user_mobile !=0 AND users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id  AND users_master.unit_id=unit_master.unit_id AND users_master.society_id ='$society_id'
	             ","LIMIT 1");

				} else {
					$q = $d->select("users_master,block_master,floors_master,unit_master",
						"users_master.user_mobile ='$user_mobile' AND users_master.user_mobile !=0  AND users_master.otp='$otp' AND users_master.country_code='$country_code'
	                AND users_master.block_id=block_master.block_id
	                AND users_master.floor_id=floors_master.floor_id
	                AND users_master.unit_id=unit_master.unit_id AND users_master.society_id ='$society_id'
	             ");

				}
			}

			$user_data = mysqli_fetch_array($q);

			if ($user_data == TRUE ) {
				if($login_via==1) {
					$user_mobile = $user_data['user_mobile'];
					$country_code = $user_data['country_code'];
				}

				if ($user_data['unit_status'] != '4' && $user_data['user_status'] != '2') {

					$qSociety = $d->select("society_master", "society_id ='$society_id'");
					$user_data_society = mysqli_fetch_array($qSociety);

					$response["user_id"] = $user_data['user_id'];
					$user_id = $user_data['user_id'];
					$olddevice = $user_data['device'];

					$token = $user_data['user_token'];
					if ($user_token != $token) {
						if ($olddevice == 'android') {
							$nResident->noti("", "", $society_id, $token, "Logout", "Logout", "Logout");
						} else if ($olddevice == 'ios') {
							$nResident->noti_ios("", "", $society_id, $token, "Logout", "Logout", "Logout");
						}
					}

					$m->set_data('user_token', $user_token);
					$m->set_data('device', $device);
					$m->set_data('android_version', $android_version);
					$m->set_data('otp', '');
					$m->set_data('opt_attempt', '');
					$m->set_data('otp_attempt_time', '');
					$m->set_data('otp_verify_attempt', '');
					$m->set_data('otp_verfiy_time', '');

					if ($user_data_society['bind_mac_address'] == 1) {
						// code...

						if ($userData['user_mac_address'] == '') {
							$m->set_data('user_mac_address', $user_mac_address);

							$a = array(
								'user_token' => $m->get_data('user_token'),
								'device' => $m->get_data('device'),
								'otp' => $m->get_data('otp'),
								'opt_attempt' => $m->get_data('opt_attempt'),
								'otp_attempt_time' => $m->get_data('otp_attempt_time'),
								'otp_verify_attempt' => $m->get_data('otp_verify_attempt'),
								'otp_verfiy_time' => $m->get_data('otp_verfiy_time'),
								'user_mac_address' => $m->get_data('user_mac_address'),
							);

							$m->set_data('user_id', $user_id);
							$m->set_data('society_id', $society_id);
							$m->set_data('mac_address_device', $mac_address_device);
							$m->set_data('mac_address_phone_modal', $mac_address_phone_modal);
							$m->set_data('mac_address_change_status', "1");
							$m->set_data('mac_address_change_reason', "First Time Binding");
							$m->set_data('mac_address_status_change_date', date('Y-m-d'));

							$aa = array(
								'user_id' => $m->get_data('user_id'),
								'society_id' => $m->get_data('society_id'),
								'mac_address_device' => $m->get_data('mac_address_device'),
								'mac_address_phone_modal' => $m->get_data('mac_address_phone_modal'),
								'mac_address_change_status' => $m->get_data('mac_address_change_status'),
								'mac_address_change_reason' => $m->get_data('mac_address_change_reason'),
								'mac_address' => $m->get_data('user_mac_address'),
								'mac_address_status_change_date' => $m->get_data('mac_address_status_change_date'),
							);

							$qqq = $d->insert("user_mac_address_master",$aa);

						}else{
							$a = array(
								'user_token' => $m->get_data('user_token'),
								'device' => $m->get_data('device'),
								'otp' => $m->get_data('otp'),
								'opt_attempt' => $m->get_data('opt_attempt'),
								'otp_attempt_time' => $m->get_data('otp_attempt_time'),
								'otp_verify_attempt' => $m->get_data('otp_verify_attempt'),
								'otp_verfiy_time' => $m->get_data('otp_verfiy_time'),
							);
						}
					}else{
						$a = array(
							'user_token' => $m->get_data('user_token'),
							'device' => $m->get_data('device'),
							'otp' => $m->get_data('otp'),
							'opt_attempt' => $m->get_data('opt_attempt'),
							'otp_attempt_time' => $m->get_data('otp_attempt_time'),
							'otp_verify_attempt' => $m->get_data('otp_verify_attempt'),
							'otp_verfiy_time' => $m->get_data('otp_verfiy_time'),
						);
					}

					$d->update("users_master", $a, "user_mobile='$user_mobile' AND user_mobile!='' AND user_mobile!='0'");

					if ($device == "android") {
						$m->set_data('android_sdk_version', $android_sdk_version);
						$m->set_data('mobile_os_version', $android_version);
						$m->set_data('android_os_name', $android_os_name);

						$andData = array(
							'android_sdk_version' => $m->get_data('android_sdk_version'),
							'mobile_os_version' => $m->get_data('mobile_os_version'),
							'android_os_name' => $m->get_data('android_os_name'),
						);
					}else if ($device == "ios"){
						$m->set_data('mobile_os_version', $ios_version);

						$andData = array(
							'mobile_os_version' => $m->get_data('mobile_os_version'),
						);
					}


					$d->update("users_master", $andData, "user_mobile='$user_mobile' AND user_mobile!='' AND user_mobile!='0'");

					$ip_address=$d->get_client_ip(); # Save The IP Address
					$browser=$mac_address_phone_modal.'-'.$mac_address_device.' ('.$device.')'; # Save The User Agent
					$loginTime=date("d M,Y h:i:sa");//Login Time
					$m->set_data('user_id',$user_id);
					$m->set_data('name',$user_data['user_full_name']);
					$m->set_data('role_name',$user_data['user_designation']);
					$m->set_data('ip_address',$ip_address);
					$m->set_data('browser',$browser);
					$m->set_data('loginTime',$loginTime);
					$aSession= array ('user_id'=> $m->get_data('user_id'),
						'name'=> $m->get_data('name'),
						'role_name'=> $m->get_data('role_name'),
						'ip_address'=> $m->get_data('ip_address'),
						'browser'=> $m->get_data('browser'),
						'loginTime'=> $m->get_data('loginTime'),
						'login_type'=> 0,
					);
					$d->insert('session_log',$aSession); 

					$response["society_id"] = $user_data['society_id'];
					$response["shift_time_id"] = $user_data['shift_time_id'];
					$society_id = $user_data['society_id'];
					$response["user_full_name"] = $user_data['user_full_name'];
					$response["user_first_name"] = $user_data['user_first_name'];
					$response["user_last_name"] = $user_data['user_last_name'];
					$response["user_mobile"] = $user_data['user_mobile'];
					$response["user_email"] = $user_data['user_email'];
					$response["user_id_proof"] = $user_data['user_id_proof'];
					$response["user_type"] = $user_data['user_type'];
					$response["block_id"] = $user_data['block_id'];
					$response["block_name"] = $user_data['block_name'];
					$response["floor_name"] = $user_data['floor_name'];
					$response["unit_name"] = $user_data['unit_name'];
					$response["base_url"] = $base_url;
					$response["floor_id"] = $user_data['floor_id'];
					$response["gender"] = $user_data['gender'];
					$response["unit_id"] = $user_data['unit_id'];
					$response["zone_id"] = $user_data['zone_id'];
					$response["level_id"] = $user_data['level_id'];
					$response["unit_status"] = $user_data['unit_status'];
					$response["user_status"] = $user_data['user_status'];
					$response["member_status"] = $user_data['member_status'];
					$response["public_mobile"] = $user_data['public_mobile'];
					$response["mobile_for_gatekeeper"] = $user_data['mobile_for_gatekeeper'];
					$response["visitor_approved"] = $user_data['visitor_approved'];
					$response["member_date_of_birth"] = "" . $user_data['member_date_of_birth'];
					$response["wedding_anniversary_date"] = "" . $user_data['wedding_anniversary_date'];
					$response["facebook"] = $user_data['facebook'];
					$response["instagram"] = $user_data['instagram'];
					$response["linkedin"] = $user_data['linkedin'];
					$response["alt_mobile"] = $user_data['alt_mobile'];
					$response["country_code"] = $user_data['country_code'];
					$response["country_code_alt"] = $user_data['country_code_alt'];
					$response["child_gate_approval"] = $user_data['child_gate_approval'];
					$response["company_name"] = html_entity_decode($user_data['company_name']);
					$response["company_address"] = html_entity_decode($user_data['company_address']);
					$response["plot_lattitude"] = $user_data['plot_lattitude'];
					$response["plot_longitude"] = $user_data['plot_longitude'];

					
					$response["get_business_data"] = false;


					if ($user_data['blood_group']!="") {
						$response["blood_group"] = $user_data['blood_group'];
					} else {
						$response["blood_group"] ="Not Available";
					}

					if ($user_data['last_login']=='0000-00-00 00:00:00') {
						$response["is_new_user"] = true;
					} else {
						$response["is_new_user"] = false;
					}

					if ($user_data['expense_on_off']=='0') {
						$response["is_expense_on"] = true;
						$response["max_expense_amount"] = $user_data['max_expense_amount'];
					} else {
						$response["is_expense_on"] = false;
						$response["max_expense_amount"] = "0.00";
					}

					$q1=$d->select("user_employment_details","user_id='$user_data[user_id]'");
                    $proData=mysqli_fetch_array($q1);
					$response["designation"] = $user_data['user_designation'];
					$response["business_categories"] = $proData['business_categories'];
					$response["business_categories_sub"] = $proData['business_categories_sub'];
					$response["business_categories_other"] = $proData['business_categories_other'];
					$response["professional_other"] = $proData['professional_other'];

					// For if society

					$is_society = true;

					if ($user_data_society['society_type'] == 0) {
						$response["is_society"] = true;
						$response["label_member_type"] = "Family Members";
						$response["label_setting_apartment"] = "Apartment closed";
						$response["label_setting_resident"] = "Contact Number Privacy for Residents";
					} else if ($user_data_society['society_type'] == 1) {
						$response["is_society"] = false;
						$response["label_member_type"] = "Team Members";
						$response["label_setting_apartment"] = "Unit closed";
						$response["label_setting_resident"] = "Contact Number Privacy for Members";
					} else {
						$response["is_society"] = true;
						$response["label_member_type"] = "Family Members";
						$response["label_setting_apartment"] = "Apartment closed";
						$response["label_setting_resident"] = "Contact Number Privacy for Residents";
					}


					$response["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $user_data['user_profile_pic'];

					if ($user_data['user_type'] == 1) {
						$fq = $d->select("users_master", "user_type=0 AND member_status=0 AND unit_id='$user_data[unit_id]'");
						$ownerData = mysqli_fetch_array($fq);
						$response["owner_name"] = $ownerData['user_full_name'] . "";
						$response["owner_email"] = $ownerData['user_email'] . "";
						$response["owner_mobile"] = $ownerData['user_mobile'] . "";
					} else {
						$response["owner_name"] = "";
						$response["owner_email"] = "";
						$response["owner_mobile"] = "";
					}

					

					if ($user_data_society['society_status'] == '0') {

						

						$response["society_address"] = html_entity_decode($user_data_society['society_address']). "";
						$response["society_latitude"] = $user_data_society['society_latitude'] . "";
						$response["society_longitude"] = $user_data_society['society_longitude'] . "";
						$response["society_name"] = html_entity_decode($user_data_society['society_name']) . "";
						$response["country_id"] = $user_data_society['country_id'] . "";
						$response["state_id"] = $user_data_society['state_id'] . "";
						$response["city_id"] = $user_data_society['city_id'] . "";
						$response["city_name"] = ', ' . $user_data_society['city_name'];
						$response["api_key"] = $keydb . "";
						$response["socieaty_logo"] = $base_url . 'img/society/' . $user_data_society['socieaty_logo'] . "";

						
						$response["currency"] =$user_data_society['currency']."";

						$unit_id = $user_data['unit_id'];
						$block_id = $user_data['block_id'];
						$floor_id = $user_data['floor_id'];

						$qfamily = $d->select("users_master", "society_id ='$society_id'and block_id='$block_id' AND floor_id='$floor_id' AND unit_id='$unit_id' AND member_status='1'");

						$qemergencynum = $d->select("user_emergencycontact_master", "society_id ='$society_id' and user_id='$user_id'");

						$response["member"] = array();
						$response["emergency"] = array();

						while ($datafamily = mysqli_fetch_array($qfamily)) {

							$member = array();
							$member["user_id"] = $datafamily['user_id'];
							$member["user_first_name"] = $datafamily['user_first_name'];
							$member["user_last_name"] = $datafamily['user_last_name'];
							$member["user_mobile"] = $datafamily['user_mobile'];
							$member["member_date_of_birth"] = $datafamily['member_date_of_birth'] . ' ';
							$dateOfBirth = "" . $datafamily['member_date_of_birth'];
							$today = date("Y-m-d");
							// $diff = date_diff(date_create($dateOfBirth), date_create($today));
							$member["member_age"] = "";
							$member["member_relation_name"] = $datafamily['member_relation_name'];
							$member["user_status"] = $datafamily['user_status'];
							$member["member_status"] = $datafamily['member_status'];

							array_push($response["member"], $member);

						}
						while ($dataemergency = mysqli_fetch_array($qemergencynum)) {
							$emergency = array();

							$emergency["emergencyContact_id"] = $dataemergency['emergencyContact_id'];
							$emergency["person_name"] = $dataemergency['person_name'];
							$emergency["person_mobile"] = $dataemergency['person_mobile'];
							$emergency["relation_id"] = $dataemergency['relation_id'].'';
							$emergency["relation"] = $dataemergency['relation'];

							array_push($response["emergency"], $emergency);
						}

						$login_successfully = $xml->string->login_successfully;
						$response["message"] = "$login_successfully";
						$response["status"] = "200";
						echo json_encode($response);

					} else {
						$response["message"] = "Society deactive.";
						$response["status"] = "204";
						echo json_encode($response);
					}

				} else {
					$response["message"] = "Account Not activated.";
					$response["status"] = "201";
					echo json_encode($response);
				}

			} else {

				if ($minitues > 60) {
					$otp_verify_attempt = 1;
				} else {

					$otp_verify_attempt = $userData['otp_verify_attempt'] + 1;
				}

				$otp_verfiy_time = date("Y-m-d H:i:s");

				$a = array(
					'otp_verify_attempt' => $otp_verify_attempt,
					'otp_verfiy_time' => $otp_verfiy_time,
				);

				$d->update("users_master", $a, "user_mobile='$user_mobile'");

				$otp_not_match = $xml->string->otp_not_match;

				$response["message"] = "$otp_not_match ";
				$response["status"] = "201";
				echo json_encode($response);

			}

		}   else if (isset($user_logout) && $user_logout == 'user_logout' && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$m->set_data('user_token', '');

			$a = array(
				'user_token' => $m->get_data('user_token'),
			);

			$qdelete = $d->update("users_master", $a, "user_id='$user_id'");

			if ($qdelete == TRUE) {
				$response["message"] = "Log out Sucessfully";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "Something wrong..! Try Again";
				$response["status"] = "201";
				echo json_encode($response);
			}
		}  else if (isset($changeMacRequest) && $changeMacRequest == 'changeMacRequest' && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

			$oldReqQry = $d->select("user_mac_address_master","user_id = '$user_id' AND society_id = '$society_id' AND mac_address_change_status = 0 AND mac_address = '$user_mac_address'");

			if (mysqli_num_rows($oldReqQry) > 0) {
				$response["message"] = "You have already added request for same device, please wait untill admin approve your request.";
				$response["status"] = "203";
				echo json_encode($response);
				exit();
			}

			$m->set_data('user_id', $user_id);
			$m->set_data('society_id', $society_id);
			$m->set_data('mac_address_device', $mac_address_device);
			$m->set_data('mac_address', $user_mac_address);
			$m->set_data('mac_address_phone_modal', $mac_address_phone_modal);
			$m->set_data('mac_address_change_status', "0");
			$m->set_data('mac_address_change_reason', $mac_address_change_reason);
			$m->set_data('mac_address_status_change_date', date('Y-m-d'));

			$aa = array(
				'user_id' => $m->get_data('user_id'),
				'society_id' => $m->get_data('society_id'),
				'mac_address_device' => $m->get_data('mac_address_device'),
				'mac_address' => $m->get_data('mac_address'),
				'mac_address_phone_modal' => $m->get_data('mac_address_phone_modal'),
				'mac_address_change_status' => $m->get_data('mac_address_change_status'),
				'mac_address_change_reason' => $m->get_data('mac_address_change_reason'),
				'mac_address_status_change_date' => $m->get_data('mac_address_status_change_date'),
			);

			$qqq = $d->insert("user_mac_address_master",$aa);


			if ($qqq == TRUE) {

				$user_nameQry = $d->selectRow("user_full_name","users_master","user_id = '$user_id'");

				$userData = mysqli_fetch_array($user_nameQry);

				$title = "New Device Change Request";
                $description = "Requested by, ".$userData['user_full_name'];

                $access_type = $mobile_device_bind_Access;

                include 'check_access_data_user.php';

                // To Employee App
                if (count($userIDArray) > 0) {

                    $fcmArrayPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='android' AND user_id IN ('$userFcmIds')");
                    $fcmArrayIosPA=$d->get_android_fcm("users_master","delete_status=0 AND user_token!='' AND society_id='$society_id' AND device='ios' AND user_id IN ('$userFcmIds')");

                    $dataAry = array(
                        "block_id"=>$block_id,
                        "floor_id"=>$floor_id,
                        "user_id"=>$user_id,
                    );

                    $dataJson = json_encode($dataAry);

                    $nResident->noti("device_change","",$society_id,$fcmArrayPA,$title,$description,$dataJson);
                    $nResident->noti_ios("device_change","",$society_id,$fcmArrayIosPA,$title,$description,$dataJson);

                    $d->insertUserNotificationWithJsonData($society_id,$title,$description,"device_change","smartphone.png",$dataJson,"users_master.user_id IN ('$userFcmIds')");
                }

                $block_id=$d->getBlockid($user_id);
                $fcmArray=$d->selectAdminBlockwise("20",$block_id,"android");
                $fcmArrayIos=$d->selectAdminBlockwise("20",$block_id,"ios");
          
                $nAdmin->noti_new($society_id,$notiImg,$fcmArray,$title, $description,"macAddress");
                $nAdmin->noti_ios_new($society_id,$notiImg,$fcmArrayIos,$title, $description,"macAddress");

                $notiAry = array(
                  'society_id'=>$society_id,
                  'notification_tittle'=>$title,
                  'notification_description'=>$description,
                  'notifiaction_date'=>date('Y-m-d H:i'),
                  'notification_action'=>"macAddress",
                  'admin_click_action '=>"macAddress",
                  'notification_logo'=>'smartphone.png',
                );
                            
                $d->insert("admin_notification",$notiAry);

				$response["message"] = "Request Sent";
				$response["status"] = "200";
				echo json_encode($response);
			} else {
				$response["message"] = "Something wrong..! Try Again";
				$response["status"] = "201";
				echo json_encode($response);
			}
		} else {
			$response["message"] = "Invalid Request";
			$response["status"] = "201";
			echo json_encode($response);
		}
	} else {
		$response["message"] = "wrong api key.";
		$response["status"] = "201";
		echo json_encode($response);
	}
}

?>