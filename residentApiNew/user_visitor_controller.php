<?php
include_once 'lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){


    if ($key==$keydb && $auth_check=='true') {
    $response = array();
    extract(array_map("test_input" , $_POST));
        
    if ($_POST['addNewDailyVisitor'] == "addNewDailyVisitor"  && $society_id!='' && $unit_id!='' && $user_id!='' && $visitor_name!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
           
            $valid_till = date("Y-m-d", strtotime($valid_till));
            $in_time = date("H:i:s", strtotime($in_time));
            $out_time = date("H:i:s", strtotime($out_time));

            if ($country_code!="") {
                $appendQuery = " AND country_code='$country_code'";
            }

            $cm=$d->selectRow("visitor_mobile,visitor_id","daily_visitors_master","visitor_mobile='$visitor_mobile' AND society_id='$society_id' $appendQuery");
            if (mysqli_num_rows($cm)>0) {
                $data=mysqli_fetch_array($cm);

                $m->set_data('daily_visitor_id', $data['visitor_id']);
                $m->set_data('society_id', $society_id);
                $m->set_data('user_id', $user_id);
                $m->set_data('unit_id', $unit_id);
                $m->set_data('valid_till', $valid_till);
                $m->set_data('in_time', $in_time);
                $m->set_data('out_time', $out_time);
                $m->set_data('week_days', $week_days);

                 $aUnitId = array(
                    'daily_visitor_id' => $m->get_data('daily_visitor_id'),
                    'society_id' => $m->get_data('society_id'),
                    'unit_id' => $m->get_data('unit_id'),
                    'user_id' => $m->get_data('user_id'),
                    'valid_till_user' => $m->get_data('valid_till'),
                    'week_days_user' => $m->get_data('week_days'),
                    'in_time_user' => $m->get_data('in_time'),
                    'out_time_user' => $m->get_data('out_time'),   
                    'week_days_user' => $m->get_data('week_days'),   
                );

                $qca=$d->select("daily_visitor_unit_master","society_id='$society_id' AND unit_id='$unit_id' AND daily_visitor_id='$data[visitor_id]'");
                if (mysqli_num_rows($qca)>0) {
                    $response["message"] = "This visitor already added in your unit";
                    $response["status"] = "201";
                    echo json_encode($response);
                    exit();
                 }  else {
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","$visitor_name daily visitor added","Visitors-mgmtxxxhdpi.png");
                    $d->insert("daily_visitor_unit_master", $aUnitId);
                    $response["message"] = "$addMsg";
                    $response["status"] = "200";
                    echo json_encode($response);
                    exit();
                 }
                $mobile_already_register = $xml->string->mobile_already_register;
                $response["message"] = "$mobile_already_register";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }

         
            $uploadedFile=$_FILES["visitor_profile_photo"]["tmp_name"];
            $ext = pathinfo($_FILES['visitor_profile_photo']['name'], PATHINFO_EXTENSION);
                if(file_exists($uploadedFile)) {
                   
                  $sourceProperties = getimagesize($uploadedFile);
                  $newFileName = rand().$user_id;
                  $dirPath = "../img/visitor/";
                  $imageType = $sourceProperties[2];
                  $imageHeight = $sourceProperties[1];
                  $imageWidth = $sourceProperties[0];
                  if ($imageWidth>1000) {
                    $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage /100;
                    $newImageHeight = $imageHeight * $newWidthPercentage /100;
                  } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                  }

                  switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagepng($tmp,$dirPath. $newFileName. "daily_visitor.". $ext);
                        break;           

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagejpeg($tmp,$dirPath. $newFileName. "daily_visitor.". $ext);
                        break;
                    
                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagegif($tmp,$dirPath. $newFileName. "daily_visitor.". $ext);
                        break;

                    default:
                      
                        break;
                    }
                $profile_name= $newFileName."daily_visitor.".$ext;

            } else {
             $profile_name= "visitor_default.png";
            }

           


            
            $m->set_data('society_id', $society_id);
            $m->set_data('visitor_type', 4); //for daily visitor
            $m->set_data('visit_from', $visit_from);
            $m->set_data('visitor_name', $visitor_name);
            $m->set_data('visitor_mobile', $visitor_mobile);
            $m->set_data('country_code', $country_code);
            $m->set_data('profile_name', $profile_name);
            $m->set_data('valid_till', $valid_till);
            $m->set_data('in_time', $in_time);
            $m->set_data('out_time', $out_time);
            $m->set_data('week_days', $week_days);
            $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
            $m->set_data('vehicle_number', $vehicle_number);
            $m->set_data('number_of_visitor',1 );
            $m->set_data('visitor_status', '0');
            $m->set_data('active_status', '0');

            

            $a = array(
                'society_id' => $m->get_data('society_id'),
                'visitor_type' => $m->get_data('visitor_type'),
                'visit_from' => $m->get_data('visit_from'),
                'visitor_name' => $m->get_data('visitor_name'),
                'visitor_mobile' => $m->get_data('visitor_mobile'),
                'country_code' => $m->get_data('country_code'),
                'visitor_profile' => $m->get_data('profile_name'),
                'number_of_visitor' => $m->get_data('number_of_visitor'),
                'valid_till' => $m->get_data('valid_till'),
                'in_time' => $m->get_data('in_time'),
                'out_time' => $m->get_data('out_time'),
                'week_days' => $m->get_data('week_days'),
                'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                'visitor_id_proof' => $m->get_data('visitor_id_proof'),
                'vehicle_number' => $m->get_data('vehicle_number'),
                'visitor_status' => $m->get_data('visitor_status'),
                'daily_active_status' => $m->get_data('active_status'),
            );

            $quserDataUpdate = $d->insert("daily_visitors_master", $a);
            $daily_visitor_id = $con->insert_id;

            $m->set_data('daily_visitor_id', $daily_visitor_id);
            if ($quserDataUpdate == true) {
               

                    $m->set_data('user_id', $user_id);
                    $m->set_data('unit_id', $unit_id);

                     $aUnitId = array(
                        'daily_visitor_id' => $m->get_data('daily_visitor_id'),
                        'society_id' => $m->get_data('society_id'),
                        'unit_id' => $m->get_data('unit_id'),
                        'user_id' => $m->get_data('user_id'),
                        'valid_till_user' => $m->get_data('valid_till'),
                        'in_time_user' => $m->get_data('in_time'),
                        'out_time_user' => $m->get_data('out_time'),   
                        'week_days_user' => $m->get_data('week_days'),   
                    );

                    $d->insert("daily_visitor_unit_master", $aUnitId);


                $d->insert_myactivity($user_id,"$society_id","0","$user_name","$visitor_name daily visitor added","Visitors-mgmtxxxhdpi.png");
                $response["message"] = "$addMsg";
                $response["status"] = "200";
                echo json_encode($response);


                $fcmArray = $d->get_emp_fcm("employee_master", "emp_token!='' AND emp_type_id='0'  AND society_id='$society_id' AND entry_status=1");
                $nGaurd->noti_new("visitor",$fcmArray,"$visitor_name-$visit_from","New Daily Visitor Added","dailyvisitor");


            } else {

                $response["message"] = "$somethingWrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if(isset($muteDailyVisitorNotification) &&  filter_var($visitor_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true) {
                if ($active_status==0) {
                    $active_status = 1;
                    $notification_off = $xml->string->notification_off;
                    $response["message"] = "$notification_off";
                } else {
                    $notification_on = $xml->string->notification_on;
                    $active_status = 0;
                    $response["message"] = "$notification_on";
                }

                $a = array(
                  'active_status'=>$active_status,
                  );

              $q=$d->update("daily_visitor_unit_master",$a,"unit_id='$unit_id' AND daily_visitor_id='$visitor_id' AND user_id='$user_id'");
              if($q==TRUE) {
                
                $response["status"] = "200";
                echo json_encode($response);
                exit();
              } else { 
                $response["message"] = "$somethingWrong";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
              }
        }else if ($_POST['editDailyVisitor'] == "editDailyVisitor"  && $society_id!='' && $unit_id!='' && $user_id!='' && $visitor_name!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true) {

            if ($country_code!="") {
                $appendQuery = " AND country_code='$country_code'";
            }
            
            $cm=$d->selectRow("visitor_mobile,visitor_id","daily_visitors_master","visitor_mobile='$visitor_mobile' AND society_id='$society_id' AND  visitor_id!='$visitor_id'  $appendQuery ");
            if (mysqli_num_rows($cm)>0) {
                $mobile_already_register = $xml->string->mobile_already_register;
                $response["message"] = "$mobile_already_register";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
           
            $valid_till = date("Y-m-d", strtotime($valid_till));
            $in_time = date("H:i:s", strtotime($in_time));
            $out_time = date("H:i:s", strtotime($out_time));

            $uploadedFile=$_FILES["visitor_profile_photo"]["tmp_name"];
            $ext = pathinfo($_FILES['visitor_profile_photo']['name'], PATHINFO_EXTENSION);
                if(file_exists($uploadedFile)) {
                   
                  $sourceProperties = getimagesize($uploadedFile);
                  $newFileName = rand().$user_id;
                  $dirPath = "../img/visitor/";
                  $imageType = $sourceProperties[2];
                  $imageHeight = $sourceProperties[1];
                  $imageWidth = $sourceProperties[0];
                  if ($imageWidth>1000) {
                    $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage /100;
                    $newImageHeight = $imageHeight * $newWidthPercentage /100;
                  } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                  }

                  switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagepng($tmp,$dirPath. $newFileName. "daily_visitor.". $ext);
                        break;           

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagejpeg($tmp,$dirPath. $newFileName. "daily_visitor.". $ext);
                        break;
                    
                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagegif($tmp,$dirPath. $newFileName. "daily_visitor.". $ext);
                        break;

                    default:
                      
                        break;
                    }
                $profile_name= $newFileName."daily_visitor.".$ext;

            } else {
             $qcd=$d->selectRow("visitor_profile","daily_visitors_master","visitor_id='$visitor_id'");
             $oldData=mysqli_fetch_array($qcd);
             $profile_name= $oldData['visitor_profile'];
            }
            


            $m->set_data('daily_visitor_id', $visitor_id);
            $m->set_data('society_id', $society_id);
            $m->set_data('visitor_type', 4); //for daily visitor
            $m->set_data('visit_from', $visit_from);
            $m->set_data('visitor_name', $visitor_name);
            $m->set_data('visitor_mobile', $visitor_mobile);
            $m->set_data('country_code', $country_code);
            $m->set_data('profile_name', $profile_name);
            $m->set_data('valid_till', $valid_till);
            $m->set_data('in_time', $in_time);
            $m->set_data('out_time', $out_time);
            $m->set_data('week_days', $week_days);
            $m->set_data('visitor_sub_type_id', $visitor_sub_type_id);
            $m->set_data('vehicle_number', $vehicle_number);
            $m->set_data('number_of_visitor',1 );
            $m->set_data('daily_active_status', '0');

            

            $a = array(
                'society_id' => $m->get_data('society_id'),
                'visitor_type' => $m->get_data('visitor_type'),
                'visit_from' => $m->get_data('visit_from'),
                'visitor_name' => $m->get_data('visitor_name'),
                'visitor_mobile' => $m->get_data('visitor_mobile'),
                'country_code' => $m->get_data('country_code'),
                'visitor_profile' => $m->get_data('profile_name'),
                'number_of_visitor' => $m->get_data('number_of_visitor'),
                'valid_till' => $m->get_data('valid_till'),
                'in_time' => $m->get_data('in_time'),
                'out_time' => $m->get_data('out_time'),
                'week_days' => $m->get_data('week_days'),
                'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                'vehicle_number' => $m->get_data('vehicle_number'),
                'daily_active_status' => $m->get_data('active_status'),
            );


            $qca=$d->select("daily_visitor_unit_master","society_id='$society_id'  AND daily_visitor_id='$visitor_id' AND unit_id!='$unit_id' ");

            $cqaData= mysqli_fetch_array($qca);
            if (mysqli_num_rows($qca)==0) {
                $quserDataUpdate = $d->update("daily_visitors_master", $a,"visitor_id='$visitor_id'");

                 $response["message"] = "$updateMsg";
                $d->insert_myactivity($user_id,"$society_id","0","$user_name","$visitor_name daily visitor data updated","Visitors-mgmtxxxhdpi.png");

            }else {
                $quserDataUpdate= "Update Only Time";
                $this_visitor_assign_in_multiple_units_in_out_week_days_details_updated_ = $xml->string->this_visitor_assign_in_multiple_units_in_out_week_days_details_updated_;
                $response["message"] = "$this_visitor_assign_in_multiple_units_in_out_week_days_details_updated_";
            }


            if ($quserDataUpdate == true) {
               

                    $m->set_data('user_id', $user_id);
                    $m->set_data('unit_id', $unit_id);

                     $aUnitId = array(
                        'unit_id' => $m->get_data('unit_id'),
                        'user_id' => $m->get_data('user_id'),
                        'valid_till_user' => $m->get_data('valid_till'),
                        'in_time_user' => $m->get_data('in_time'),
                        'out_time_user' => $m->get_data('out_time'),   
                        'week_days_user' => $m->get_data('week_days'),   
                    );

                    $d->update("daily_visitor_unit_master", $aUnitId,"daily_visitor_id='$visitor_id' AND unit_id='$unit_id' AND user_id='$user_id'");


               
                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "$somethingWrong";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if($_POST['addExVisitor']=="addExVisitorNew" && $unit_id!='' && $user_id!='' && $visitor_name!='' && $visit_date!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($block_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true ){

            if ($country_code!="") {
                $appendQuery = " AND country_code='$country_code'";
            }


            $visit_date = str_replace('/', '-',  $visit_date );
            $visit_date = date("Y-m-d", strtotime($visit_date));
            $visit_time = date("H:i:s", strtotime($visit_time));
            $expire = strtotime("$visit_date $visit_time");
            $cTime= date("H:i:s");

            $cTime= date("H:i:s");
            $currentDate = strtotime($cTime);
            $futureDate = $currentDate+(-60*5);
            $cTime = date("H:i:s", $futureDate);

            $cDate= date("Y-m-d");
            $today = strtotime("$cDate $cTime");
            if($today > $expire){
                $visit_date_expire = $xml->string->visit_date_expire;
                $response["message"] = "$visit_date_expire";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
              } 

            if($update=="1"){
                 $qq=$d->select("visitors_master","((visitor_mobile='$visitor_mobile' AND visitor_status=0  AND visitor_status!='3'
                     AND society_id='$society_id') 
                     OR (visitor_mobile='$visitor_mobile' AND visitor_status=1 AND society_id='$society_id') 
                     OR (visitor_mobile='$visitor_mobile' AND visitor_status=2 AND society_id='$society_id') 
                     OR (visitor_mobile='$visitor_mobile' AND visitor_status=6 AND society_id='$society_id') 
                     )  AND visitors_master.visit_date='$visit_date' AND visitors_master.unit_id='$unit_id'  AND visitor_id!='$visitor_id' $appendQuery");
            } else {
                 $qq=$d->select("visitors_master","((visitor_mobile='$visitor_mobile' AND visitor_status=0 AND visitor_status!='3'
                     AND society_id='$society_id') 
                     OR (visitor_mobile='$visitor_mobile' AND visitor_status=1 AND society_id='$society_id') 
                     OR (visitor_mobile='$visitor_mobile' AND visitor_status=2 AND society_id='$society_id') 
                     OR (visitor_mobile='$visitor_mobile' AND visitor_status=6 AND society_id='$society_id') 
                     )  AND visitors_master.visit_date='$visit_date' AND visitors_master.unit_id='$unit_id' $appendQuery ");
            }

            if (mysqli_num_rows($qq)>0) {
                $oldData=mysqli_fetch_array($qq);
                if ($oldData['visitor_type']==1 && $oldData['visitor_status']==1) {
                    $response["message"] = "Mobile number is already added in Expected Visitors List";
                } else if ($oldData['visitor_status']==2) {
                    $visitor_already_enter = $xml->string->visitor_already_enter;
                    $response["message"] = "$visitor_already_enter";
                } else {
                    $mobile_already_register = $xml->string->mobile_already_register;
                    $response["message"] = "$mobile_already_register";
                }
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }


            
            $digits = 4;
            $otp= rand(pow(10, $digits-1), pow(10, $digits)-1);

           
            $visit_time  = date("H:i:s", strtotime($visit_time));
            $visitTime  = date("h:i A", strtotime($visit_time));

            $visitDate =  $visit_date.' '.$visit_time;
            $valid_till = (int)$valid_till;
            $nextHours= $valid_till *60;
            $timestamp = strtotime($visitDate) + 60*$nextHours;

            $valid_till_date = date('Y-m-d H:i:s', $timestamp);

            $m->set_data('parent_visitor_id',$parent_visitor_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('floor_id',$floor_id);
            $m->set_data('block_id',$block_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('visitor_type','1');
            $m->set_data('expected_type','1');
            $m->set_data('visitor_name',$visitor_name);
            $m->set_data('visitor_mobile',$visitor_mobile);
            $m->set_data('country_code',$country_code);
            $m->set_data('number_of_visitor',$number_of_visitor);
            $m->set_data('visit_date',$visit_date);
            $m->set_data('visit_time',$visit_time);
            $m->set_data('valid_till_date',$valid_till_date);
            $m->set_data('visitor_status','1');
            $m->set_data('visiting_reason',$visiting_reason);
            $m->set_data('otp',$otp);
            $m->set_data('visitor_sub_type_id',$visitor_sub_type_id);
          
            
            $uploadedFile=$_FILES["visitor_profile_photo"]["tmp_name"];
            $ext = pathinfo($_FILES['visitor_profile_photo']['name'], PATHINFO_EXTENSION);
                if(file_exists($uploadedFile)) {
                   
                  $sourceProperties = getimagesize($uploadedFile);
                  $newFileName = rand().$user_id;
                  $dirPath = "../img/visitor/";
                  $imageType = $sourceProperties[2];
                  $imageHeight = $sourceProperties[1];
                  $imageWidth = $sourceProperties[0];
                  if ($imageWidth>1000) {
                    $newWidthPercentage= 1000*100 / $imageWidth;  //for maximum 1000 widht
                    $newImageWidth = $imageWidth * $newWidthPercentage /100;
                    $newImageHeight = $imageHeight * $newWidthPercentage /100;
                  } else {
                    $newImageWidth = $imageWidth;
                    $newImageHeight = $imageHeight;
                  }

                  switch ($imageType) {

                    case IMAGETYPE_PNG:
                        $imageSrc = imagecreatefrompng($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagepng($tmp,$dirPath. $newFileName. "_visitor.". $ext);
                        break;           

                    case IMAGETYPE_JPEG:
                        $imageSrc = imagecreatefromjpeg($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagejpeg($tmp,$dirPath. $newFileName. "_visitor.". $ext);
                        break;
                    
                    case IMAGETYPE_GIF:
                        $imageSrc = imagecreatefromgif($uploadedFile); 
                        $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$newImageWidth,$newImageHeight);
                        imagegif($tmp,$dirPath. $newFileName. "_visitor.". $ext);
                        break;

                    default:
                      
                        break;
                    }
                $profile_name= $newFileName."_visitor.".$ext;

            } else if ($visitor_photo_old!='') {
                  $profile_name= $visitor_photo_old;
            } else {
                $qOld = $d->select("visitors_master", "visitor_id='$visitor_id'","");
                $oldData = mysqli_fetch_array($qOld);
                $oldProfile = $oldData['visitor_profile'];
                if ($oldProfile!='') {
                    $profile_name= $oldProfile;
                } else {
                    $profile_name= "visitor_default.png";
                }
            }

           
    
                if($update=="1"){
                     $a = array(
                        'society_id'=>$m->get_data('society_id'),
                        'floor_id'=>$m->get_data('floor_id'),
                        'block_id'=>$m->get_data('block_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'visitor_type'=>$m->get_data('visitor_type'),
                        'expected_type'=>$m->get_data('expected_type'),
                        'visitor_name'=>$m->get_data('visitor_name'),
                        'visitor_mobile'=>$m->get_data('visitor_mobile'),
                        'country_code'=>$m->get_data('country_code'),
                        'visitor_profile'=>$profile_name,
                        'number_of_visitor'=>$m->get_data('number_of_visitor'),
                        'visit_date'=>$m->get_data('visit_date'),
                        'visit_time'=>$m->get_data('visit_time'),
                        'valid_till_date'=>$m->get_data('valid_till_date'),
                        'visitor_status'=>$m->get_data('visitor_status'),
                        'visiting_reason'=>$m->get_data('visiting_reason'),
                        'otp'=>$m->get_data('otp'),
                        'entry_time'=>date("Y-m-d H:i:s"),
                    );

                    $quserDataUpdate = $d->update("visitors_master",$a,"visitor_id='$visitor_id'");
                    $response["message"]="$updateMsg";
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","Expected visitor data updated","Visitors-mgmtxxxhdpi.png");
                }else{
                    $a = array(
                        // 'parent_visitor_id'=>$m->get_data('parent_visitor_id'),
                        'society_id'=>$m->get_data('society_id'),
                        'floor_id'=>$m->get_data('floor_id'),
                        'block_id'=>$m->get_data('block_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'visitor_type'=>$m->get_data('visitor_type'),
                        'expected_type'=>$m->get_data('expected_type'),
                        'visitor_name'=>$m->get_data('visitor_name'),
                        'visitor_mobile'=>$m->get_data('visitor_mobile'),
                        'country_code'=>$m->get_data('country_code'),
                        'visitor_profile'=>$profile_name,
                        'number_of_visitor'=>$m->get_data('number_of_visitor'),
                        'visit_date'=>$m->get_data('visit_date'),
                        'visit_time'=>$m->get_data('visit_time'),
                        'valid_till_date'=>$m->get_data('valid_till_date'),
                        'visitor_status'=>$m->get_data('visitor_status'),
                        'visiting_reason'=>$m->get_data('visiting_reason'),
                        'otp'=>$m->get_data('otp'),
                        'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                        'entry_time'=>date("Y-m-d H:i:s"),
                    );

                    $quserDataUpdate = $d->insert("visitors_master",$a);
                    $parent_visitor_id = $con->insert_id;
                    $aPrnt = array(
                        'parent_visitor_id'=>$parent_visitor_id,
                    );
                    $d->update("visitors_master",$aPrnt,"visitor_id='$parent_visitor_id'");
                    $response["message"]="$addMsg";
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","Expected visitor added","Visitors-mgmtxxxhdpi.png");
                }

            if($quserDataUpdate==TRUE){

                $visitDate= date("d M Y", strtotime($visit_date." ".$data2['visit_time']));
                if($update=="1"){
                    $title= "Expected visitor data updated by $user_name";
                } else {
                    $title= "Expected visitor added by $user_name";
                }
                $description= "Expected visitor arrive on $visitDate $visitTime $visitor_name ($number_of_visitor person)";

                $d->insertGuardNotification("Visitors-mgmtxxxhdpi.png",$title,$description,"in~3",$society_id,$block_id);

                $fcmToken=$d->get_emp_fcm("employee_master,employee_block_master","employee_master.emp_id=employee_block_master.emp_id AND employee_master.society_id='$society_id' AND employee_block_master.block_id='$block_id' AND employee_master.emp_token!=''");
                $nGaurd->noti_new("visitor",$fcmToken,$title,$description,"in~3");

                


                  $sq=$d->selectRow("society_name,society_latitude,society_longitude","society_master","society_id='$society_id'");
                  $societyData=mysqli_fetch_array($sq);
                  $society_name=$societyData['society_name'];
                  $society_latitude=$societyData['society_latitude'];
                  $society_longitude=$societyData['society_longitude'];
                  $mapLink= "https://maps.google.com/?q=$society_latitude,$society_longitude";
                  $visit_time=date("h:i A", strtotime($visit_time));

                  $society_name = html_entity_decode($society_name);
                  if ($country_code=='') {
                      $country_code = "+91";
                  }
                
                $smsObj->send_exp_visitor_otp($society_id,$visitor_mobile,$visitor_name,$society_name,$user_name,$visitDate,$visit_time,$otp,$mapLink,$country_code);
                $d->add_sms_log($visitor_mobile,"Expected Visitor OTP SMS",$society_id,$country_code,3);


                $response["status"]="200";
                echo json_encode($response);

            }else{

                $response["message"]="$somethingWrong";
                $response["status"]="201";
                echo json_encode($response);

            }

        }  else if($_POST['addExCabDelivery']=="addExCabDelivery" && $unit_id!='' && $user_id!=''  && $visit_date!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($block_id, FILTER_VALIDATE_INT) == true && filter_var($floor_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true ){

            $clickAction= "in~3";

            if($visitor_type==2) {
                $profile_name = "delivery_boy.png";
                $expected_type =2;
              } elseif ($visitor_type==3) {
                $profile_name = "taxi.png";
                $expected_type=3;
              } else {
                $profile_name = "visitor_default.png";

              }

            $visit_date = str_replace('/', '-',  $visit_date );
            
            $visit_date = date("Y-m-d", strtotime($visit_date));
            $visit_time  = date("H:i:s", strtotime($visit_time));
            
            $cTime= date("H:i:s");
            $currentDate = strtotime($cTime);
            $futureDate = $currentDate+(-60*5);
            $cTime = date("H:i:s", $futureDate);


            $todayDate= date("Y-m-d");
            $today = strtotime("$todayDate $cTime");
            $expire = strtotime($visit_date.' '.$visit_time);
            // $today = strtotime("today midnight");
            if($today > $expire){
                $visit_date_expire = $xml->string->visit_date_expire;
                $response["message"] = "$visit_date_expire";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
              } 

            

            $visitDate =  $visit_date.' '.$visit_time;
            $valid_till = (int)$valid_till;
            $nextHours= $valid_till *60;
            $timestamp = strtotime($visitDate) + 60*$nextHours;

            $valid_till_date = date('Y-m-d H:i:s', $timestamp);
            $vehicle_no = $vehicle_no;
            $m->set_data('parent_visitor_id',$parent_visitor_id);
            $m->set_data('society_id',$society_id);
            $m->set_data('floor_id',$floor_id);
            $m->set_data('block_id',$block_id);
            $m->set_data('unit_id',$unit_id);
            $m->set_data('user_id',$user_id);
            $m->set_data('visitor_type','1');
            $m->set_data('expected_type',$expected_type);
            $m->set_data('visit_from',$visit_from);
            $m->set_data('visitor_name',$visitor_name);
            $m->set_data('visitor_mobile',$visitor_mobile);
            $m->set_data('country_code',$country_code);
            $m->set_data('number_of_visitor',$number_of_visitor);
            $m->set_data('visit_date',$visit_date);
            $m->set_data('visit_time',$visit_time);
            $m->set_data('valid_till_date',$valid_till_date);
            $m->set_data('visitor_status','1');
            $m->set_data('number_of_visitor', 1);
            $m->set_data('vehicle_no',$vehicle_no);
            $m->set_data('profile_name',$profile_name);
            $m->set_data('visitor_sub_type_id',$visitor_sub_type_id);
            $m->set_data('leave_parcel_at_gate',$leave_parcel_at_gate);
            

           
    
                if($update=="1"){
                    $qold=$d->selectRow("leave_parcel_at_gate,parcel_collect_master_id","visitors_master","visitor_id='$visitor_id'");
                    $oldData=mysqli_fetch_array($qold);
                    $parcel_collect_master_id= $oldData['parcel_collect_master_id'];
                    if ($oldData['leave_parcel_at_gate']==0 && $leave_parcel_at_gate==1) {
                        

                        $parcel_collected_date = $visit_date.' '.$visit_time;
                        $parcelAry = array(
                            'society_id'=>$m->get_data('society_id'),
                            'parcel_photo'=>"my-parcelxxxxhdpi.png",
                            'no_of_parcel'=>$no_of_parcel,
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'collected_by'=>"",
                            'appoval_by' => $m->get_data('user_id'),
                            'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                            'parcel_status'=>"0",
                            'delivery_company'=>$m->get_data('visit_from'),
                            'parcel_collected_date'=>$parcel_collected_date,
                            'valid_till_date_parcel'=>$valid_till_date,
                        );

                        $d->insert("parcel_collect_master", $parcelAry);
                        $parcel_collect_master_id =  $con->insert_id;
                        $clickAction= "parcel~2";
                    } else  if ($oldData['leave_parcel_at_gate']==1 && $leave_parcel_at_gate==1) {
                        $parcel_collected_date = $visit_date.' '.$visit_time;
                        if ($visitor_sub_type_id!=0) {
                            $parcelAry = array(
                            'society_id'=>$m->get_data('society_id'),
                            'parcel_photo'=>"my-parcelxxxxhdpi.png",
                            'no_of_parcel'=>$no_of_parcel,
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'collected_by'=>"",
                            'appoval_by' => $m->get_data('user_id'),
                            'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                            'parcel_status'=>"0",
                            'delivery_company'=>$m->get_data('visit_from'),
                            'parcel_collected_date'=>$parcel_collected_date,
                            'valid_till_date_parcel'=>$valid_till_date,
                            );
                        } else {
                            $parcelAry = array(
                            'society_id'=>$m->get_data('society_id'),
                            'parcel_photo'=>"my-parcelxxxxhdpi.png",
                            'no_of_parcel'=>$no_of_parcel,
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'collected_by'=>"",
                            'appoval_by' => $m->get_data('user_id'),
                            'parcel_status'=>"0",
                            'delivery_company'=>$m->get_data('visit_from'),
                            'parcel_collected_date'=>$parcel_collected_date,
                            'valid_till_date_parcel'=>$valid_till_date,
                            );
                        }

                        

                        $d->update("parcel_collect_master", $parcelAry,"parcel_collect_master_id='$parcel_collect_master_id'");
                        $clickAction= "parcel~2";
                    }else if ($oldData['leave_parcel_at_gate']==1 && $leave_parcel_at_gate==0) {

                        $d->delete("parcel_collect_master","parcel_collect_master_id='$parcel_collect_master_id'");
                        $parcel_collect_master_id=0;
                    }


                     $a = array(
                        'society_id'=>$m->get_data('society_id'),
                        'floor_id'=>$m->get_data('floor_id'),
                        'block_id'=>$m->get_data('block_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'visitor_type'=>$m->get_data('visitor_type'),
                        'expected_type'=>$m->get_data('expected_type'),
                        'visit_from'=>$m->get_data('visit_from'),
                        'visitor_name'=>$m->get_data('visitor_name'),
                        'visitor_mobile'=>$m->get_data('visitor_mobile'),
                        'country_code'=>$m->get_data('country_code'),
                        'visitor_profile'=>$m->get_data('profile_name'),
                        'number_of_visitor'=>$m->get_data('number_of_visitor'),
                        'visit_date'=>$m->get_data('visit_date'),
                        'visit_time'=>$m->get_data('visit_time'),
                        'valid_till_date'=>$m->get_data('valid_till_date'),
                        'visitor_status'=>$m->get_data('visitor_status'),
                        'number_of_visitor'=>$m->get_data('number_of_visitor'),
                        'vehicle_no'=>$m->get_data('vehicle_no'),
                         'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                        'leave_parcel_at_gate'=>$m->get_data('leave_parcel_at_gate'),
                        'parcel_collect_master_id'=>$parcel_collect_master_id,
                        'entry_time'=>date("Y-m-d H:i:s"),
                    );

                   
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","Expected visitor data updated","Visitors-mgmtxxxhdpi.png");


                    $quserDataUpdate = $d->update("visitors_master",$a,"visitor_id='$visitor_id'");
                    $response["message"]="$updateMsg";

                }else{
                    if ($leave_parcel_at_gate==1) {
                       
                        $parcel_collected_date = $visit_date.' '.$visit_time;
                        $parcelAry = array(
                            'society_id'=>$m->get_data('society_id'),
                            'parcel_photo'=>"my-parcelxxxxhdpi.png",
                            'parcel_id'=>$parcel_id,
                            'no_of_parcel'=>$no_of_parcel,
                            'unit_id'=>$m->get_data('unit_id'),
                            'user_id'=>$m->get_data('user_id'),
                            'collected_by'=>"",
                            'appoval_by' => $m->get_data('user_id'),
                            'visitor_sub_type_id' => $m->get_data('visitor_sub_type_id'),
                            'parcel_status'=>"0",
                            'delivery_company'=>$m->get_data('visit_from'),
                            'parcel_collected_date'=>$parcel_collected_date,
                            'valid_till_date_parcel'=>$valid_till_date,
                        );
                        $clickAction= "parcel~2";
                        $d->insert("parcel_collect_master", $parcelAry);
                        $parcel_collect_master_id =  $con->insert_id;
                    }

                    $a = array(
                        'society_id'=>$m->get_data('society_id'),
                        'floor_id'=>$m->get_data('floor_id'),
                        'block_id'=>$m->get_data('block_id'),
                        'unit_id'=>$m->get_data('unit_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'visitor_type'=>$m->get_data('visitor_type'),
                        'expected_type'=>$m->get_data('expected_type'),
                        'visit_from'=>$m->get_data('visit_from'),
                        'visitor_name'=>$m->get_data('visitor_name'),
                        'visitor_mobile'=>$m->get_data('visitor_mobile'),
                        'country_code'=>$m->get_data('country_code'),
                        'visitor_profile'=>$m->get_data('profile_name'),
                        'number_of_visitor'=>$m->get_data('number_of_visitor'),
                        'visit_date'=>$m->get_data('visit_date'),
                        'visit_time'=>$m->get_data('visit_time'),
                        'valid_till_date'=>$m->get_data('valid_till_date'),
                        'visitor_status'=>$m->get_data('visitor_status'),
                        'number_of_visitor'=>$m->get_data('number_of_visitor'),
                        'vehicle_no'=>$m->get_data('vehicle_no'),
                        'visitor_sub_type_id'=>$m->get_data('visitor_sub_type_id'),
                        'leave_parcel_at_gate'=>$m->get_data('leave_parcel_at_gate'),
                        'parcel_collect_master_id'=>$parcel_collect_master_id,
                        'entry_time'=>date("Y-m-d H:i:s"),
                    );

                   

                    $quserDataUpdate = $d->insert("visitors_master",$a);
                     $parent_visitor_id = $con->insert_id;

                     $aPrnt = array(
                        'parent_visitor_id'=>$parent_visitor_id,
                     );
                     $d->update("visitors_master",$aPrnt,"visitor_id='$parent_visitor_id'");

                    if($visitor_type==2) {
                    $response["message"]="$addMsg";
                        $d->insert_myactivity($user_id,"$society_id","0","$user_name","Expected delivery added","my-parcelxxxxhdpi.png");

                    }else {
                        $d->insert_myactivity($user_id,"$society_id","0","$user_name","Expected cab added","taxi.png");
                    
                    $response["message"]="$addMsg";
                    }


                }

            if($quserDataUpdate==TRUE){

                $visitDate= date("d M Y", strtotime($visit_date." ".$data2['visit_time']));
                if($visitor_type==2) {
                    if($update=="1"){
                        if ($leave_parcel_at_gate==1) {
                            $title= "Parcel collect on gate data updated by $user_name ";
                            $notIcon= "my-parcelxxxxhdpi.png";
                        } else {
                            $title= "Expected Delivery boy data updated by $user_name";
                            $notIcon= "Visitors-mgmtxxxhdpi.png";
                        }
                    } else {
                        if ($leave_parcel_at_gate==1) {
                            $title= "Parcel collect on gate Request added by $user_name";
                            $notIcon= "my-parcelxxxxhdpi.png";
                        } else {
                            $title= "Expected Delivery boy Added by $user_name";
                            $notIcon= "Visitors-mgmtxxxhdpi.png";
                        }
                    }
                    $description="Expected Delivery boy arrive on $visitDate";
                } else  if($visitor_type==3) {
                    if($update=="1"){
                        $title= "Expected Cab data updated by $user_name";
                        $notIcon= "Visitors-mgmtxxxhdpi.png";
                    } else {
                        $title= "Expected Cab Added by $user_name";
                        $notIcon= "Visitors-mgmtxxxhdpi.png";
                    }
                    $description="Cab arrive on $visitDate";
                }

                $d->insertGuardNotification($notIcon,$title,$description,"in~3",$society_id,$block_id);

                $fcmToken=$d->get_emp_fcm("employee_master,employee_block_master","employee_master.emp_id=employee_block_master.emp_id AND employee_master.society_id='$society_id' AND employee_block_master.block_id='$block_id' AND employee_master.emp_token!=''");
                $nGaurd->noti_new("visitor",$fcmToken,$title,$description,"in~3");


                $response["status"]="200";
                echo json_encode($response);

            }else{

                $response["message"]="$somethingWrong";
                $response["status"]="201";
                echo json_encode($response);

            }

        } else if($_POST['getExVisitorList']=="getExVisitorList" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  ){

                $qnotification=$d->select("users_master,visitors_master","visitors_master.user_id=users_master.user_id AND visitors_master.user_id = '$user_id' AND visitors_master.visitor_status != '5' AND visitors_master.visitor_type = '1' AND visitors_master.society_id='$society_id'","ORDER BY visitors_master.visitor_id DESC LIMIT 100");


                if(mysqli_num_rows($qnotification)>0){

                    $response["visitor"] = array();

                    while($data_notification=mysqli_fetch_array($qnotification)) {
                        $data_notification = array_map("html_entity_decode", $data_notification);
                        $sq=$d->selectRow("society_name,society_latitude,society_longitude","society_master","society_id='$data_notification[society_id]'");
                        $societyData=mysqli_fetch_array($sq);
                        $society_name=$societyData['society_name'];
                        $society_latitude=$societyData['society_latitude'];
                        $society_longitude=$societyData['society_longitude'];
                        $mapLink= "https://maps.google.com/?q=$society_latitude,$society_longitude";
                        $visitDate = date("d M Y h:i A", strtotime($data_notification['visit_date']." ".$data_notification['visit_time']));
                        if ($data_notification['exit_date']!='') {
                        $exitDate = date("d M Y h:i A", strtotime($data_notification['exit_date']." ".$data_notification['exit_time']));
                        } else {
                            $exitDate="$not_available";
                        }

                        $visit_date = date("d M Y", strtotime($data_notification['visit_date']));
                        $visit_time = date("h:i A", strtotime($data_notification['visit_time']));

                        $visitor = array(); 
                        $otp = $data_notification['otp'];

                        $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data_notification[visitor_sub_type_id]'");
                        $vistLogo=mysqli_fetch_array($fd);
                        if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }

                        $visitor["visitor_id"]=$data_notification['visitor_id'];
                        $visitor["society_id"]=$data_notification['society_id'];
                        $visitor["unit_id"]=$data_notification['unit_id'];
                        $visitor["user_id"]=$data_notification['user_id'];
                        $visitor["visitor_type"]=$data_notification['visitor_type'];
                        $visitor["expected_type"]=$data_notification['expected_type'];
                        $visitor["visitor_name"]=$data_notification['visitor_name'].'';
                        $visitor["visit_from"]=''.$data_notification['visit_from'];
                        $visitor["visit_from"]=''.$data_notification['visit_from'];
                        $visitor["visit_logo"]=$visit_logo;
                        if ($data_notification['visitor_profile']!='') {
                            $visitor["visitor_profile"]=$base_url."img/visitor/".$data_notification['visitor_profile'];
                        } else {
                            $visitor["visitor_profile"]=$base_url."img/visitor/visitor_default.png";
                        }

                        if ($data_notification['visitor_mobile']!="0") {
                            $visitor["visitor_mobile"]=$data_notification['visitor_mobile'].'';
                        } else {
                            $visitor["visitor_mobile"]="$not_available";
                        }
                        $visitor["vistor_number"]=$data_notification['number_of_visitor'];
                        $visitor["visiting_reason"]="".$data_notification['visiting_reason'];
                        $visitor["visit_date"]=$data_notification['visit_date'];
                        $time1 = strtotime($data_notification['visit_date'].' '.$data_notification['visit_time']);
                        $time2 = strtotime($data_notification['valid_till_date']);
                        $difference = round(abs($time2 - $time1) / 3600);
                        if ($difference==1) {
                           $difference= $difference.' Hour';
                        } else {
                           $difference= $difference.' Hours';
                        }
                        $visitor["valid_till_date"]=$difference;
                        $visitor["visit_date_view"]=date("d-m-Y", strtotime($data_notification['visit_date']));
                        $visitor["otp"]="Dear $data_notification[visitor_name],\n\nYou are registered as prospective visitor at $society_name by $data_notification[user_full_name]\n\nVisiting Date: $visit_date, $visit_time.\n\nPlease share OTP *$otp* with security guard.\n\n$mapLink \n\nThank You,\nTeam MyCo";
                        $visitor["only_otp"]=$otp;

                        $visitor["visit_time"]=$data_notification['visit_time'];
                        $visitor["visit_time_view"]=date("h:i A", strtotime($data_notification['visit_time']));
                        $visitor["exit_date"]="".$data_notification['exit_date'];
                        $visitor["exit_time"]="".$data_notification['exit_time'];
                        $visitor["visitDate"]="".$visitDate;
                        $visitor["exitDate"]="".$exitDate;
                        $visitor["visitor_status"]=$data_notification['visitor_status'];
                        $visitor["vehicle_no"]=$data_notification['vehicle_no'].'';
                        $visitor["visitor_sub_type_id"]=$data_notification['visitor_sub_type_id'].'';
                        $visitor["leave_parcel_at_gate"]=$data_notification['leave_parcel_at_gate'].'';
                        $qqq=$d->select("parcel_collect_master","parcel_collect_master_id='$data_notification[parcel_collect_master_id]'");
                        $parcelData=mysqli_fetch_array($qqq);
                        $no_of_parcel = $parcelData['no_of_parcel'];
                        if ($no_of_parcel>0) {
                        $visitor["no_of_parcel"]=''.$no_of_parcel;
                        }else {
                        $visitor["no_of_parcel"]='1';
                        }

                        $qr_size = "300x300";
                        $qr_content ="expected_".$data_notification['visitor_id'];
                        $qr_correction = strtoupper('H');
                        $qr_encoding = 'UTF-8';
                        $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                        $visitor['qr_code'] = $qrImageUrl;
                        $visitor['qr_code_ios'] = $qrImageUrl . '.png';

                        if ($data_notification['visitor_status']==2) {
                            if ($data_notification['in_temperature']>0) {
                                $in_temperature=$data_notification['in_temperature'];
                            } else {
                                $in_temperature = "";
                            }
                            $visitor["temperature"]=$in_temperature.'';
                            $visitor["with_mask"]=$data_notification['in_with_mask'].'';
                        } else if ($data_notification['visitor_status']==3) {
                             if ($data_notification['out_temperature']>0) {
                                $out_temperature=$data_notification['out_temperature'];
                            } else {
                                $out_temperature = "";
                            }
                            $visitor["temperature"]=$out_temperature.'';
                            $visitor["with_mask"]=$data_notification['out_with_mask'].'';
                        } else {
                            $visitor["temperature"]='';
                            $visitor["with_mask"]='';
                        }

                        

                        array_push($response["visitor"], $visitor); 
                    }

                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['getNewVisitorList']=="getNewVisitorList" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

                $qnotification=$d->select("visitors_master,users_master","(users_master.user_id=visitors_master.user_id AND  (visitors_master.user_id='$user_id' AND visitors_master.visitor_status != 5 AND visitors_master.society_id='$society_id') AND  (visitors_master.visitor_type = 0 OR visitors_master.visitor_type =2 or visitors_master.visitor_type =3))","ORDER BY visitors_master.visitor_id DESC LIMIT 200");

                $response["visitor"] = array();

                if(mysqli_num_rows($qnotification)>0){

                    while($data_notification=mysqli_fetch_array($qnotification)) {
                         $data_notification = array_map("html_entity_decode", $data_notification);
                         $visitDate = date("d M Y h:i A", strtotime($data_notification['visit_date']." ".$data_notification['visit_time']));
                         if ($data_notification['exit_date']!='') {
                             $exitDate = date("d M Y h:i A", strtotime($data_notification['exit_date']." ".$data_notification['exit_time']));
                         } else {
                            $exitDate="$not_available";
                         }


                        $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data_notification[visitor_sub_type_id]'");
                        $vistLogo=mysqli_fetch_array($fd);
                        if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }

                        $visit_from = html_entity_decode($data_notification['visit_from']);
                        $visitFrom = explode('-', $visit_from);
                        $visitor = array(); 
                        $visitor["visitor_id"]=$data_notification['visitor_id'];
                        $visitor["parent_visitor_id"]=$data_notification['parent_visitor_id'];
                        $visitor["society_id"]=$data_notification['society_id'];
                        $visitor["unit_id"]=$data_notification['unit_id'];
                        $visitor["user_id"]=$data_notification['user_id'];
                        $visitor["visitor_type"]=$data_notification['visitor_type'];
                        $visitor["expected_type"]=$data_notification['visitor_type'];
                        $visitor["visitor_name"]=$data_notification['visitor_name'];
                        $visitor["visit_from"]=$visit_from.'';
                        $visitor["visit_logo"]=$visit_logo.'';
                        $visitor["visitor_mobile"]="".$data_notification['visitor_mobile'];
                        $visitor["visitor_profile"]=$base_url."img/visitor/".$data_notification['visitor_profile'];
                        if ($data_notification['visitor_type']==0) {
                            $visitor["vistor_number"]=$data_notification['number_of_visitor'].' Person';
                        }else if ($data_notification['visitor_type']==2) {
                            $visitor["vistor_number"]='1 Person';
                        }else if ($data_notification['visitor_type']==3) {
                            $visitor["vistor_number"]='1 Person';
                        }
                        $visitor["visiting_reason"]="".$data_notification['visiting_reason'];
                        $visitor["visit_date"]=$data_notification['visit_date'];
                        $visitor["visit_time"]=$data_notification['visit_time'];
                        $visitor["exit_date"]="".$data_notification['exit_date'];
                        $visitor["exit_time"]="".$data_notification['exit_time'];
                        $visitor["visitDate"]="".$visitDate;
                        $visitor["exitDate"]="".$exitDate;
                        $visitor["visitor_status"]=$data_notification['visitor_status'];
                        $visitor["visitor_approved"]=$data_notification['visitor_approved'];
                        $visitor["delivery_cab_approval"]=$data_notification['Delivery_cab_approval'];
                        $visitor["visitor_sub_type_id"]=$data_notification['visitor_sub_type_id'].'';
                        $visitor["leave_parcel_at_gate"]=$data_notification['leave_parcel_at_gate'].'';
                        if ($data_notification['visitor_status']==2) {
                            if ($data_notification['in_temperature']>0) {
                                $in_temperature=$data_notification['in_temperature'];
                            } else {
                                $in_temperature = "";
                            }
                            $visitor["temperature"]=$in_temperature.'';
                            $visitor["with_mask"]=$data_notification['in_with_mask'].'';
                        } else if ($data_notification['visitor_status']==3) {
                             if ($data_notification['out_temperature']>0) {
                                $out_temperature=$data_notification['out_temperature'];
                            } else {
                                $out_temperature = "";
                            }
                            $visitor["temperature"]=$out_temperature.'';
                            $visitor["with_mask"]=$data_notification['out_with_mask'].'';
                        } else {
                            $visitor["temperature"]='';
                            $visitor["with_mask"]='';
                        }

                        $intTime=$data_notification['visit_date']." ".$data_notification['visit_time'];
                        $outTime=date("Y-m-d H:i:s");
                        $date_a = new DateTime($intTime);
                        $date_b = new DateTime($outTime);
                        $interval = date_diff($date_a,$date_b);
                        $totalDays= $interval->days;
                         $visitor["test"]=$totalDays;
                        if ($data_notification['exit_date']=='' && $totalDays>30 && $data_notification['visitor_type']==0 || $data_notification['exit_date']=='' && $totalDays>30 && $data_notification['visitor_type']==1 || $data_notification['exit_date']=='' && $totalDays>1 && $data_notification['visitor_type']==2 || $data_notification['exit_date']=='' && $totalDays>1 && $data_notification['visitor_type']==3) {
                            
                            $visitor["exitDate"]="Auto Exit";

                        }


                        
                        array_push($response["visitor"], $visitor); 
                    }

                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }  else if($_POST['getDailyVisitorList']=="getDailyVisitorList" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  ){

                $qnotification = $d->select("daily_visitors_master,daily_visitor_unit_master", "daily_visitor_unit_master.daily_visitor_id=daily_visitors_master.visitor_id AND  daily_visitors_master.visitor_type  = '4' AND daily_visitors_master.society_id='$society_id' AND daily_visitors_master.daily_active_status =0 AND daily_visitor_unit_master.user_id='$user_id'", "");


                if(mysqli_num_rows($qnotification)>0){

                    $response["visitor"] = array();

                    while($data_notification=mysqli_fetch_array($qnotification)) {
                         $data_notification = array_map("html_entity_decode", $data_notification);
                        

                        $fd=$d->selectRow("visitor_sub_image,visitor_sub_type_name","visitorSubType","visitor_sub_type_id='$data_notification[visitor_sub_type_id]'");
                        $vistLogo=mysqli_fetch_array($fd);
                        if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                            $visit_from = $vistLogo['visitor_sub_type_name'].'';
                        } else {
                            $visit_logo="";
                        }

                        $visitor["visitor_id"]=$data_notification['visitor_id'];
                        $visitor["active_status"]=$data_notification['active_status'];
                        $visitor["society_id"]=$data_notification['society_id'];
                        $visitor["unit_id"]=$data_notification['unit_id'];
                        $visitor["user_id"]=$data_notification['user_id'];
                        $visitor["visitor_type"]=$data_notification['visitor_type'];
                        $visitor["in_time"]= date("h:i A", strtotime($data_notification['in_time_user']));$data_notification['in_time_user'];
                        $visitor["out_time"]=date("h:i A", strtotime($data_notification['out_time_user']));$data_notification['in_time_user'];
                        $visitor["week_days"]=$data_notification['week_days_user'];
                        $visitor["valid_till"]=$data_notification['valid_till'];
                        $visitor["visitor_name"]=html_entity_decode($data_notification['visitor_name']);
                        $visitor["visit_from"]=$visit_from.'';
                        $visitor["visit_logo"]=$visit_logo;
                        $visitor["visitor_profile"]=$base_url."img/visitor/".$data_notification['visitor_profile'];
                        $visitor["visitor_mobile"]=$data_notification['visitor_mobile'].'';
                        $visitor["country_code"]=$data_notification['country_code'];
                        $visitor["vistor_number"]=$data_notification['number_of_visitor'];
                        $visitor["visiting_reason"]=html_entity_decode($data_notification['visit_from']);
                        // $visitor["visitor_status"]=$data_notification['visitor_status'];
                        $vehicle_number = html_entity_decode($data_notification['vehicle_number']);
                        $visitor["vehicle_no"]=strtoupper($vehicle_number).'';
                        $visitor["visitor_sub_type_id"]=$data_notification['visitor_sub_type_id'].'';
                        $visitor["visitor_status"]=$data_notification['visitor_status'].'';
                        $visitor["visitor_status_view"]=$data_notification['visitor_status'].'';
                        $visitor["vehicle_number"]=strtoupper($data_notification['vehicle_number']).'';

                        $qc=$d->select("visitors_master","daily_visitor_id='$data_notification[visitor_id]' AND visitor_status=2 OR daily_visitor_id='$data_notification[visitor_id]' AND visitor_status=3","ORDER BY visitor_id DESC");
                        $visitData=mysqli_fetch_array($qc);
                         $visitor["visitor_status"]=$visitData['visitor_status'].'';
                         // $visitDate = date("d M Y h:i A", strtotime($visitData['visit_date']." ".$visitData['visit_time']));
                         if ($visitData['exit_date']!='') {
                             $exitDate = date("d M Y h:i A", strtotime($visitData['exit_date']." ".$visitData['exit_time']));
                         } else {
                            $exitDate="$not_available";
                         }

                         if ($visitData['visit_date']!='') {
                             $visitDate = date("d M Y h:i A", strtotime($visitData['visit_date']." ".$visitData['visit_time']));
                         } else {
                            $visitDate="$not_available";
                         }

                        $visitor["visitDate"]="".$visitDate;
                        $visitor["exitDate"]="".$exitDate;

                        if ($visitData['visitor_status']==2 && $visitData['in_with_mask']!='') {
                            $visitor["temperature"]=$visitData['in_temperature'].'';
                            $visitor["with_mask"]=$visitData['in_with_mask'].'';
                        } else if ($visitData['visitor_status']==3 && $visitData['in_with_mask']!='') {
                            $visitor["temperature"]=$visitData['out_temperature'].'';
                            $visitor["with_mask"]=$visitData['out_with_mask'].'';
                        }  else if ($visitData['in_with_mask']=='') {
                            $visitor["temperature"]=$visitData['out_temperature'].'';
                            $visitor["with_mask"]='';
                        } else {
                            $visitor["temperature"]='';
                            $visitor["with_mask"]='';
                        }
                       
                        array_push($response["visitor"], $visitor); 
                    }

                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['rejectVisitor']=="rejectVisitor"){

                $qq=$d->select("visitors_master,users_master,block_master,unit_master","block_master.block_id=unit_master.block_id AND users_master.block_id=block_master.block_id AND users_master.unit_id=unit_master.unit_id AND visitors_master.user_id=users_master.user_id AND visitors_master.visitor_id='$visitor_id' ");
                $visitorData=mysqli_fetch_array($qq);
                $unit_name=$visitorData['block_name'].'-'.$visitorData['unit_name'];

                if ($visitorData['visitor_status']==4) {
                    $already_rejected = $xml->string->already_rejected;
                    
                    $response["message"]="$already_rejected";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                } else if ($visitorData['visitor_status']==1) {
                    $already_approved = $xml->string->already_approved;
                    $response["message"]="$already_approved";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }else if ($visitorData['visitor_status']==2) {
                    $already_enter = $xml->string->already_enter;

                    $response["message"]="$already_enter";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                } else if ($visitorData['visitor_status']==3) {

                    $already_exited = $xml->string->already_exited;
                    $response["message"]="$already_exited";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                } else if ($visitor_status==5) {

                        $visitor_already_deleted = $xml->string->visitor_already_deleted;
                        $response["message"]="$visitor_already_deleted";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit();
                }

                $m->set_data('visitor_id',$visitor_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('visitor_status','4');
                $m->set_data('reject_by_id',$user_id);
               
                $a = array(
                    'visitor_status'=>$m->get_data('visitor_status'),
                    'reject_by_id'=>$m->get_data('reject_by_id')
                );


                $qdelete = $d->update("visitors_master",$a,"parent_visitor_id='$visitor_id' OR visitor_id='$visitor_id' ");
        
                if($qdelete==TRUE){

                    $title= "❌ $visitor_name request rejected by $user_name";
                    $description="Do not allow this visitor to enter $unit_name ";
                    

                    if ($block_id!='' && $block_id!=0) {
                        $d->insertGuardNotification("Visitors-mgmtxxxhdpi.png",$title,$description,"in~1",$society_id,$block_id);
                        $fcmToken=$d->get_emp_fcm("employee_master,employee_block_master","employee_master.emp_id=employee_block_master.emp_id AND employee_master.society_id='$society_id' AND  employee_master.emp_token!='' AND employee_block_master.block_id='$block_id'");
                    } else {
                        $fcmToken=$d->get_emp_fcm("employee_master","society_id='$society_id' AND  emp_token!=''");
                    }

                    $nGaurd->noti_new("visitor",$fcmToken,$title,$description,"in~1");

                    $m->set_data('vehicle_no', "");
                    $m->set_data('parking_status', '0');
                    $m->set_data('visitor', "0");
                    $m->set_data('vehicle_type', "0");

                    $a2 = array(
                    // 'parking_status' => $m->get_data('parking_status'),
                    'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                    'visitor' => $m->get_data('visitor'));
                    
                    $d->update("parking_master",$a2,"visitor='$visitor_id'");
                    

                    $request_rejected_succesfully = $xml->string->request_rejected_succesfully;
                    $response["message"]="$request_rejected_succesfully";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$somethingWrong";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if($_POST['acceptVisitor']=="acceptVisitor" && filter_var($visitor_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

                $qSociety = $d->selectRow("auto_reject_vistor_minutes","society_master","society_id ='$society_id'");
                $societyData = mysqli_fetch_array($qSociety);
                $auto_reject_vistor_minutes= $societyData['auto_reject_vistor_minutes'];

                $qq=$d->select("visitors_master,users_master,block_master,unit_master","block_master.block_id=unit_master.block_id AND users_master.block_id=block_master.block_id AND users_master.unit_id=unit_master.unit_id AND visitors_master.user_id=users_master.user_id AND visitors_master.visitor_id='$visitor_id' ");
                $visitorData=mysqli_fetch_array($qq);
                $visitor_status = $visitorData['visitor_status'];
                
                $intTime = $visitorData['visit_date'].' '.$visitorData['visit_time'];
                $outTime=date("Y-m-d H:i:s");
                $diffSeconds = strtotime($outTime) - strtotime($intTime);
                if ($diffSeconds/60 >$auto_reject_vistor_minutes && $visitor_status==0) {
                    $visitor_status = '4';
                    $aReject = array(
                        // 'visit_date' => $m->get_data('visit_date'),
                        // 'visit_time' => $m->get_data('visit_time'),
                        'visitor_status' => 4
                    );
                    $d->update("visitors_master", $aReject, "parent_visitor_id='$visitorData[parent_visitor_id]' ");

                } 

                if (mysqli_num_rows($qq)>0) {
                    $unit_name=$visitorData['block_name'].'-'.$visitorData['unit_name'];
                    if ($visitor_status==1) {
                        $already_approved = $xml->string->already_approved;
                        $response["message"]="$already_approved";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit();
                    }  else if ($visitor_status==3) {

                        $already_exited = $xml->string->already_exited;
                        $response["message"]="$already_exited";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit();
                    } else if ($visitor_status==4) {

                        $already_rejected = $xml->string->already_rejected;
                        $response["message"]="$already_rejected";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit();
                    } else if ($visitor_status==5) {

                        $visitor_already_deleted = $xml->string->visitor_already_deleted;
                        $response["message"]="$visitor_already_deleted";
                        $response["status"]="201";
                        echo json_encode($response);
                        exit();
                    }
                    
                        $m->set_data('visitor_id',$visitor_id);
                        $m->set_data('user_id',$user_id);
                        $m->set_data('visitor_status','1');
                        $m->set_data('approve_by_id',$user_id);
                       
                        $a = array(
                            'visitor_status'=>$m->get_data('visitor_status'),
                            'approve_by_id'=>$m->get_data('approve_by_id'),
                        );

                    $qdelete = $d->update("visitors_master",$a,"parent_visitor_id='$visitor_id' OR visitor_id='$visitor_id'");
            
                    if($qdelete==TRUE){

                        $title= "✔️ $visitor_name request accepted by $user_name";
                        $description="Allow this visitor to enter $unit_name ";

                        if ($block_id!='' && $block_id!=0) {
                            $d->insertGuardNotification("Visitors-mgmtxxxhdpi.png",$title,$description,"in~1",$society_id,$block_id);
                            $fcmToken=$d->get_emp_fcm("employee_master,employee_block_master","employee_master.emp_id=employee_block_master.emp_id AND employee_master.society_id='$society_id' AND  employee_master.emp_token!='' AND employee_block_master.block_id='$block_id'");
                        } else {
                            $fcmToken=$d->get_emp_fcm("employee_master","society_id='$society_id' AND  emp_token!=''");
                        }

                        $nGaurd->noti_new("visitor",$fcmToken,$title,$description,"in~1");


                        
                        $d->insert_myactivity($user_id,"$society_id","0","$user_name","Visitor request accepted","Visitors-mgmtxxxhdpi.png");

                        $request_approved_succesfully = $xml->string->request_approved_succesfully;
                        $response["message"]="$request_approved_succesfully";
                        $response["status"]="200";
                        echo json_encode($response);

                    }else{

                        $response["message"]="$somethingWrong";
                        $response["status"]="201";
                        echo json_encode($response);

                    }
                } else {
                    $response["message"]="$datafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);
                }
            }else if($_POST['deleteVisitor']=="deleteVisitor" && filter_var($visitor_id, FILTER_VALIDATE_INT) == true ){
               
                        $a = array('visitor_status'=>5);
                        $q=$d->select("visitors_master,users_master","visitors_master.user_id=users_master.user_id AND visitors_master.parent_visitor_id='$visitor_id' OR visitors_master.visitor_id = '$visitor_id' ");
                        $vDdata=mysqli_fetch_array($q);
                        $block_id=$vDdata['block_id'];
                        $visit_from=$vDdata['visit_from'];
                        $visitor_name=$vDdata['visitor_name'];

                        $fcmToken=$d->get_emp_fcm("employee_master,employee_block_master","employee_master.emp_id=employee_block_master.emp_id AND employee_master.society_id='$society_id' AND employee_block_master.block_id='$block_id' AND employee_master.emp_token!=''");
                        
                        $parcel_collect_master_id = $vDdata['parcel_collect_master_id'];
                        $pq = $d->selectRow("parcel_status","parcel_collect_master","parcel_collect_master_id='$parcel_collect_master_id' AND parcel_status=1");
                        if (mysqli_num_rows($pq)>0 && $vDdata['leave_parcel_at_gate']==1) {
                            $collect_parcel_from_security = $xml->string->collect_parcel_from_security;

                            $response["message"]="$collect_parcel_from_security";
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                        }

                       
                        if ($vDdata['leave_parcel_at_gate']==1) {
                            $d->delete("parcel_collect_master","parcel_collect_master_id='$vDdata[parcel_collect_master_id]'");
                            if ($vDdata['visitor_status']==1) {
                             $nGaurd->noti_new("visitor",$fcmToken,"Parcel from $visit_from Deleted","","in~1");
                            }
                        } else if($vDdata['leave_parcel_at_gate']==0 && $vDdata['visitor_type']==2 && $vDdata['visitor_status']==1) {
                            $nGaurd->noti_new("visitor",$fcmToken,"Delivery boy $visit_from Deleted","","in~1");
                        } else if($vDdata['visitor_type']==3 && $vDdata['visitor_status']==1){
                            $nGaurd->noti_new("visitor",$fcmToken,"Cab $visit_from Deleted","","in~1");
                        } else if($vDdata['visitor_status']==1){
                            $nGaurd->noti_new("visitor",$fcmToken,"Visitor $visitor_name Deleted","","in~1");
                        }

                        if ($vDdata['visitor_status']==0) {
                            $after_reject_delete = $xml->string->after_reject_delete;

                             $response["message"]="$after_reject_delete ";
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                        }else if ($vDdata['visitor_status']==2) {
                            $after_exit_delete = $xml->string->after_exit_delete;
                             $response["message"]="$after_exit_delete";
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                        }else  if ($vDdata['visitor_status']==0 || $vDdata['visitor_status']==1)  {
                            $qdelete = $d->delete("visitors_master","parent_visitor_id='$visitor_id' OR visitor_id = '$visitor_id' AND visitor_status!='2' ");
                        } else {
                            $qdelete = $d->update("visitors_master",$a,"parent_visitor_id='$visitor_id' OR visitor_id = '$visitor_id' AND visitor_status!='2' ");
                        }
            
                
                        if($qdelete==TRUE){

                             $m->set_data('vehicle_no', "");
                            $m->set_data('parking_status', '0');
                            $m->set_data('visitor', "0");
                            $m->set_data('vehicle_type', "0");

                            $a2 = array(
                            // 'parking_status' => $m->get_data('parking_status'),
                            'visitor_vehicle_no' => $m->get_data('vehicle_no'),
                            'visitor' => $m->get_data('visitor'));
                            
                            $d->update("parking_master",$a2,"visitor='$visitor_id'");
                            
                        $d->insert_myactivity($user_id,"$society_id","0","$user_name","Visitor deleted","Visitors-mgmtxxxhdpi.png");

                        
                            
                            $response["message"]="$deleteMsg";
                            $response["status"]="200";

                            echo json_encode($response);
        
                        }else{
        
                            $response["message"]="$somethingWrong";
                            $response["status"]="201";
                            echo json_encode($response);
        
                        }
        
            } else if ($_POST['deleteDailyVisitor']=="deleteDailyVisitor" && filter_var($visitor_id, FILTER_VALIDATE_INT) == true && filter_var($unit_id, FILTER_VALIDATE_INT) == true && filter_var($society_id, FILTER_VALIDATE_INT) == true ){

                         $acheckkk=$d->select("daily_visitors_master","society_id='$society_id' AND  visitor_id='$visitor_id'");
                        $visData=mysqli_fetch_array($acheckkk);
                        $visitor_name=$visData['visitor_name'];
                        if ($visData['visitor_status']==1) {
                            $after_exit_delete = $xml->string->after_exit_delete;
                             $response["message"]="$after_exit_delete";
                            $response["status"]="201";
                            echo json_encode($response);
                            exit();
                        }

                        $qca=$d->select("daily_visitor_unit_master","society_id='$society_id' AND daily_visitor_id='$visitor_id'");
                        $totalUnits= mysqli_num_rows($qca);
                        if (mysqli_num_rows($qca)==1) {
                            $qdelete=$d->delete("daily_visitor_unit_master","society_id='$society_id' AND unit_id='$unit_id' AND daily_visitor_id='$visitor_id' AND user_id='$user_id'");
                            $qdelete=$d->delete("daily_visitors_master","society_id='$society_id' AND visitor_id='$visitor_id'");
                        } else {
                            $qdelete=$d->delete("daily_visitor_unit_master","society_id='$society_id' AND unit_id='$unit_id' AND daily_visitor_id='$visitor_id' AND user_id='$user_id'");
                        }
                
                        if($qdelete==TRUE){


                            $d->insert_myactivity($user_id,"$society_id","0","$user_name","$visitor_name daily visitor deleted","Visitors-mgmtxxxhdpi.png");
                            $response["message"]="$deleteMsg";
                            $response["status"]="200";

                            echo json_encode($response);
        
                        }else{
        
                            $response["message"]="$somethingWrong";
                            $response["status"]="201";
                            echo json_encode($response);
        
                        }
        
            }else if($_POST['holdVisitor']=="holdVisitor" && filter_var($visitor_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

                $qSociety = $d->selectRow("auto_reject_vistor_minutes","society_master","society_id ='$society_id'");
                $societyData = mysqli_fetch_array($qSociety);
                $auto_reject_vistor_minutes= $societyData['auto_reject_vistor_minutes'];

                
                $qq=$d->select("visitors_master,users_master,block_master,unit_master","block_master.block_id=unit_master.block_id AND users_master.block_id=block_master.block_id AND users_master.unit_id=unit_master.unit_id AND visitors_master.user_id=users_master.user_id AND visitors_master.visitor_id='$visitor_id' ");
                $visitorData=mysqli_fetch_array($qq);
                $visitor_status = $visitorData['visitor_status'];
                
                $intTime = $visitorData['visit_date'].' '.$visitorData['visit_time'];
                $outTime=date("Y-m-d H:i:s");
                $diffSeconds = strtotime($outTime) - strtotime($intTime);
                if ($diffSeconds/60 >$auto_reject_vistor_minutes && $visitor_status==0) {
                    $visitor_status = '4';
                    $aReject = array(
                        // 'visit_date' => $m->get_data('visit_date'),
                        // 'visit_time' => $m->get_data('visit_time'),
                        'visitor_status' => 4
                    );
                    $d->update("visitors_master", $aReject, "parent_visitor_id='$visitorData[parent_visitor_id]' ");

                } 



                $unit_name=$visitorData['block_name'].'-'.$visitorData['unit_name'];

                if ($visitor_status==2) {
                    $already_enter = $xml->string->already_enter;
                    $response["message"]="$already_enter";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }else if ($visitor_status==4) {
                    $visitor_already_rejected = $xml->string->visitor_already_rejected;
                    $response["message"]="$visitor_already_rejected";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                } else if ($visitor_status==6) {
                    $visitor_already_hold = $xml->string->visitor_already_hold;
                    $response["message"]="$visitor_already_hold";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }else if ($visitor_status==1) {
                    $already_approved = $xml->string->already_approved;
                    $response["message"]="$already_approved";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }else if ($visitor_status==3) {
                    $already_exited = $xml->string->already_exited;
                    $response["message"]="$already_exited";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                } else if ($visitor_status==5) {
                    $visitor_already_deleted = $xml->string->visitor_already_deleted;
                    $response["message"]="$visitor_already_deleted";
                    $response["status"]="201";
                    echo json_encode($response);
                    exit();
                }


                $m->set_data('visitor_id',$visitor_id);
                $m->set_data('user_id',$user_id);
                $m->set_data('visitor_status','6');
                $m->set_data('hold_by_id',$user_id);
               
               
                $a = array(
                    'visitor_status'=>$m->get_data('visitor_status'),
                    'hold_by_id'=>$m->get_data('hold_by_id'),
                );
        
                        $qdelete = $d->update("visitors_master",$a,"parent_visitor_id='$visitor_id' OR visitor_id='$visitor_id'");
                
                        if($qdelete==TRUE){

                            $title= "Entry of $visitor_name hold by $user_name";
                            // $description="Please do not allow the visitor to enter in $unit_name ";
                            $description="Please hold the visitor for some time";
                    

                            if ($block_id!='' && $block_id!=0) {
                                $d->insertGuardNotification("Visitors-mgmtxxxhdpi.png",$title,$description,"in~1",$society_id,$block_id);
                                $fcmToken=$d->get_emp_fcm("employee_master,employee_block_master","employee_master.emp_id=employee_block_master.emp_id AND employee_master.society_id='$society_id' AND  employee_master.emp_token!='' AND employee_block_master.block_id='$block_id'");
                            } else {
                                $fcmToken=$d->get_emp_fcm("employee_master","society_id='$society_id' AND  emp_token!=''");
                            }
                            
                            $nGaurd->noti_new("visitor",$fcmToken,$title,$description,"in~1");
        
                            

                        $d->insert_myactivity($user_id,"$society_id","0","$user_name","Visitor on hold","Visitors-mgmtxxxhdpi.png");
                            $request_on_hold = $xml->string->request_on_hold;
                            $response["message"]="$request_on_hold";
                            $response["status"]="200";
                            echo json_encode($response);
        
                        }else{
        
                            $response["message"]="$somethingWrong";
                            $response["status"]="201";
                            echo json_encode($response);
        
                        }


            }else if (isset($getVisitorTypeList) && $getVisitorTypeList == 'getVisitorTypeList'  && $society_id!='') {

                $qSosEvent=$d->select("visitorMainType","active_status=0");

                if(mysqli_num_rows($qSosEvent)>0)
                {

                    $response["visitor_main_type"] = array();

                    while($data_notification=mysqli_fetch_array($qSosEvent)) {
                    $visitor_main_type = array(); 
                    $visitor_main_type["visitor_main_type_id"]=$data_notification['visitor_main_type_id'];
                    $visitor_main_type["main_type_name"]=$data_notification['main_type_name'];
                    $visitor_main_type["main_type_image"]=$base_url.'img/visitor_company/'.$data_notification['main_type_image'];
                    $visitor_main_type["visitor_main_full_img"]=$base_url.'img/visitor_company/'.$data_notification['visitor_main_full_img'];
                    $visitor_main_type["visitor_type"]=$data_notification['visitor_type'];

                    if ($country_id!="") {
                        $qs=$d->select("visitorSubType","country_id='$country_id' AND active_status=0 AND visitor_main_type_id='$data_notification[visitor_main_type_id]' AND visitor_sub_type_name!='Other' OR country_id='0' AND active_status=0 AND visitor_main_type_id='$data_notification[visitor_main_type_id]' AND visitor_sub_type_name!='Other'");
                    } else {
                        $qs=$d->select("visitorSubType","active_status=0 AND visitor_main_type_id='$data_notification[visitor_main_type_id]' AND visitor_sub_type_name!='Other'");
                    }


                    $visitor_main_type["visitor_sub_type"] = array();
                    if(mysqli_num_rows($qs)>0){

                        while($data=mysqli_fetch_array($qs)) {
                            $visitor_sub_type = array(); 
                            $visitor_sub_type["visitor_sub_type_id"]=$data['visitor_sub_type_id'];
                            $visitor_sub_type["visitor_sub_type_name"]=$data['visitor_sub_type_name'];
                            $visitor_sub_type["visitor_sub_image"]=$base_url.'img/visitor_company/'.$data['visitor_sub_image'];
                            array_push($visitor_main_type["visitor_sub_type"],$visitor_sub_type); 
                        }
                        $temp = true;
                    } else {
                        $temp = false;
                    }

                    
                    $qOther=$d->select("visitorSubType","active_status=0 AND visitor_main_type_id='$data_notification[visitor_main_type_id]' AND visitor_sub_type_name='Other'");


                    if(mysqli_num_rows($qOther)>0){

                        while($data=mysqli_fetch_array($qOther)) {
                            $visitor_sub_type = array(); 
                            $visitor_sub_type["visitor_sub_type_id"]=$data['visitor_sub_type_id'];
                            $visitor_sub_type["visitor_sub_type_name"]=$data['visitor_sub_type_name'];
                            $visitor_sub_type["visitor_sub_image"]=$base_url.'img/visitor_company/'.$data['visitor_sub_image'];
                            array_push($visitor_main_type["visitor_sub_type"],$visitor_sub_type); 
                        }
                        $temp1 = true;
                    } else {
                         $temp1 = true;
                    }
                    if ($temp==true || $temp1 ==true ) {
                        array_push($response["visitor_main_type"],$visitor_main_type); 
                    }

                }
                        
                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);
                        
                }
                else
                {

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);
                }
            }else if($_POST['getFilterData']=="getFilterData" && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  ){

                if ($filterType ==0) {
                    $qnotification=$d->select("users_master,visitors_master","visitors_master.user_id=users_master.user_id AND visitors_master.user_id = '$user_id' AND visitors_master.visitor_status != '5'  AND visitors_master.society_id='$society_id' AND visit_date BETWEEN '$start_date' AND '$end_date' ","ORDER BY visitors_master.visitor_id DESC LIMIT 500"); 
                } else {
                    $qnotification=$d->select("users_master,visitors_master","visitors_master.user_id=users_master.user_id AND visitors_master.user_id = '$user_id' AND visitors_master.visitor_status != '5'  AND visitors_master.society_id='$society_id' AND visit_date BETWEEN '$start_date' AND '$end_date' ","ORDER BY visitors_master.visitor_id DESC LIMIT 500");

                }


                if(mysqli_num_rows($qnotification)>0){

                    $response["visitor"] = array();

                    while($data_notification=mysqli_fetch_array($qnotification)) {
                         $data_notification = array_map("html_entity_decode", $data_notification);
                        $sq=$d->selectRow("society_name","society_master","society_id='$data_notification[society_id]'");
                        $societyData=mysqli_fetch_array($sq);
                        $society_name=$societyData['society_name'];
                        $visitDate = date("d M Y h:i A", strtotime($data_notification['visit_date']." ".$data_notification['visit_time']));
                        if ($data_notification['exit_date']!='') {
                        $exitDate = date("d M Y h:i A", strtotime($data_notification['exit_date']." ".$data_notification['exit_time']));
                        } else {
                           $exitDate="$not_available";
                        }

                        
                        $visit_date = date("d M Y", strtotime($data_notification['visit_date']));
                        $visit_time = date("h:i A", strtotime($data_notification['visit_time']));

                        $visitor = array(); 
                        $otp = $data_notification['otp'];

                        $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_name='$data_notification[visit_from]'");
                        $vistLogo=mysqli_fetch_array($fd);
                        if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }

                        $visitor["visitor_id"]=$data_notification['visitor_id'];
                        $visitor["society_id"]=$data_notification['society_id'];
                        $visitor["unit_id"]=$data_notification['unit_id'];
                        $visitor["user_id"]=$data_notification['user_id'];
                        $visitor["visitor_type"]=$data_notification['visitor_type'];
                        $visitor["expected_type"]=$data_notification['expected_type'];
                        $visitor["visitor_name"]=$data_notification['visitor_name'].'';
                        $visitor["visit_from"]=''.$data_notification['visit_from'];
                        $visitor["visit_from"]=''.$data_notification['visit_from'];
                        $visitor["country_code"]=$data_notification['country_code'].'';
                        $visitor["visitor_mobile"]=$data_notification['visitor_mobile'].'';
                        $visitor["visit_logo"]=$visit_logo;
                        $visitor["visitor_profile"]=$base_url."img/visitor/".$data_notification['visitor_profile'];
                        $visitor["vistor_number"]=$data_notification['number_of_visitor'];
                        $visitor["visiting_reason"]="".$data_notification['visiting_reason'];
                        $visitor["visit_date"]=$data_notification['visit_date'];
                        $time1 = strtotime($data_notification['visit_date'].' '.$data_notification['visit_time']);
                        $time2 = strtotime($data_notification['valid_till_date']);
                        $difference = round(abs($time2 - $time1) / 3600);
                        if ($difference==1) {
                           $difference= $difference.' Hour';
                        } else {
                           $difference= $difference.' Hours';
                        }
                        $visitor["valid_till_date"]=$difference;
                        $visitor["visit_date_view"]=date("d-m-Y", strtotime($data_notification['visit_date']));
                        $visitor["otp"]="Dear $data_notification[visitor_name],\nyou are registered as prospective visitor at $society_name by $data_notification[user_full_name]\nYour date of visit is $visit_date and time of entry $visit_time\nPlease share OTP $otp to the security individual while entering the premises\n\nThank you, Team MyCo";
                        $visitor["only_otp"]=$otp;

                        $visitor["visit_time"]=$data_notification['visit_time'];
                        $visitor["visit_time_view"]=date("h:i A", strtotime($data_notification['visit_time']));
                        $visitor["exit_date"]="".$data_notification['exit_date'];
                        $visitor["exit_time"]="".$data_notification['exit_time'];
                        $visitor["visitDate"]="".$visitDate;
                        $visitor["exitDate"]="".$exitDate;
                        $visitor["visitor_status"]=$data_notification['visitor_status'];
                        $visitor["visitor_sub_type_id"]=$data_notification['visitor_sub_type_id'];
                        $visitor["leave_parcel_at_gate"]=$data_notification['leave_parcel_at_gate'].'';
                        $visitor["vehicle_no"]=$data_notification['vehicle_no'].'';
                        $qqq=$d->select("parcel_collect_master","parcel_collect_master_id='$data_notification[parcel_collect_master_id]'");
                        $parcelData=mysqli_fetch_array($qqq);
                        $no_of_parcel = $parcelData['no_of_parcel'];
                        if ($no_of_parcel>0) {
                        $visitor["no_of_parcel"]=''.$no_of_parcel;
                        }else {
                        $visitor["no_of_parcel"]='1';
                        }

                        $qr_size = "300x300";
                        $qr_content =$data_notification['visitor_mobile'];
                        $qr_correction = strtoupper('H');
                        $qr_encoding = 'UTF-8';
                        $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                        $visitor['qr_code'] = $qrImageUrl;
                        $visitor['qr_code_ios'] = $qrImageUrl . '.png';

                        $visitor["visitor_approved"]=$data_notification['visitor_approved'];
                        $visitor["delivery_cab_approval"]=$data_notification['Delivery_cab_approval'];

                        if ($data_notification['visitor_status']==2) {
                            if ($data_notification['in_temperature']>0) {
                                $in_temperature=$data_notification['in_temperature'];
                            } else {
                                $in_temperature = "";
                            }
                            $visitor["temperature"]=$in_temperature.'';
                            $visitor["with_mask"]=$data_notification['in_with_mask'].'';
                        } else if ($data_notification['visitor_status']==3) {
                             if ($data_notification['out_temperature']>0) {
                                $out_temperature=$data_notification['out_temperature'];
                            } else {
                                $out_temperature = "";
                            }
                            $visitor["temperature"]=$out_temperature.'';
                            $visitor["with_mask"]=$data_notification['out_with_mask'].'';
                        } else {
                            $visitor["temperature"]='';
                            $visitor["with_mask"]='';
                        }

                        array_push($response["visitor"], $visitor); 
                    }

                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }else if (isset($getParcelList) && $getParcelList == 'getParcelList'  && $society_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  ) {

            $qSosEvent = $d->select("parcel_collect_master,visitors_master", "parcel_collect_master.parcel_collect_master_id=visitors_master.parcel_collect_master_id AND  parcel_collect_master.society_id ='$society_id' AND parcel_collect_master.user_id='$user_id'","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 100");

            // echo "string";

            if (mysqli_num_rows($qSosEvent) > 0) {

                $response["parcel"] = array();

                while ($data = mysqli_fetch_array($qSosEvent)) {
                     $data = array_map("html_entity_decode", $data);
                    $parcel = array();

                    $parcel["parcel_collect_master_id"] = $data['parcel_collect_master_id'];
                    $parcel["society_id"] = $data['society_id'];
                    if ($data['parcel_photo']=="my-parcelxxxxhdpi.png" || $data['parcel_photo']=='') {
                    $parcel["parcel_photo"] = $base_url . 'img/my-parcelxxxxhdpi.png';
                    } else {
                    $parcel["parcel_photo"] = $base_url . 'img/parcel/' . $data['parcel_photo'];
                    }
                    $parcel["parcel_id"] = 'PAR0'.$data['parcel_collect_master_id'];
                    $parcel["unit_id"] = $data['unit_id'];
                    $parcel["user_id"] = $data['user_id'];

                    $fdu1=$d->selectRow("emp_name,emp_mobile,country_code","employee_master","emp_id='$data[collected_by]'");
                    $gatData=mysqli_fetch_array($fdu1);
                     if ($gatData['emp_name']!='') {
                        $collected_by = $gatData['emp_name'].'';
                        $gatekeeper_mobile = $gatData['country_code'].' '.$gatData['emp_mobile'].'';
                    } else {
                        $collected_by="$not_available";
                        $gatekeeper_mobile ='';
                    }

                    $fdu2=$d->selectRow("emp_name,emp_mobile,country_code","employee_master","emp_id='$data[deliverd_by]'");
                    $gatData1=mysqli_fetch_array($fdu2);
                     if ($gatData1['emp_name']!='') {
                        $deliverd_by_gatekeeper = $gatData1['emp_name'].'';
                        $deliverd_by_gatekeeper_mobile = $gatData1['country_code'].' '.$gatData1['emp_mobile'].'';
                    } else {
                        $deliverd_by_gatekeeper="";
                        $deliverd_by_gatekeeper_mobile ='';
                    }
                        
                    $parcel["collected_by"] = $collected_by;
                    $parcel["gatekeeper_mobile"] = $gatekeeper_mobile;

                    $parcel["deliverd_by_gatekeeper"] = $deliverd_by_gatekeeper;
                    $parcel["deliverd_by_gatekeeper_mobile"] = $deliverd_by_gatekeeper_mobile;
                    
                    $fdu=$d->selectRow("user_first_name","users_master","user_id='$data[appoval_by]'");
                    $userData=mysqli_fetch_array($fdu);
                     if ($userData['user_first_name']!='') {
                            $appoval_by = $userData['user_first_name'].'';
                        } else {
                            $appoval_by="";
                        }
                    $parcel["appoval_by"] = $appoval_by;
                    $parcel["remark"] = $data['remark'];
                    $parcel["visitor_sub_type_id"] = $data['visitor_sub_type_id'];
                    $parcel["parcel_status"] = $data['parcel_status'];
                    if ($data['delivery_boy_name']=='') {
                    $parcel["delivery_boy_name"] ="$not_available";
                    }else {
                    $parcel["delivery_boy_name"] = $data['delivery_boy_name'];

                    }
                    if ($data['delivery_boy_number']!=0) {
                        $parcel["delivery_boy_number"] = $data['db_country_code'].' '.$data['delivery_boy_number'];
                    }else {
                        $parcel["delivery_boy_number"] = "";
                    }
                    $parcel["delivery_company"] = $data['delivery_company'];
                    $parcel["no_of_parcel"] = $data['no_of_parcel'];
                    $parcel["pass_code"] = 'Access Code: '.$data['pass_code'];
                    if ($data['pass_code']==0 || $data['pass_code']=='') {
                        $parcel["pass_code_new"] = "Not Generated";
                    } else {

                    $parcel["pass_code_new"] = $data['pass_code'];
                    }
                    
                    $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                    $vistLogo=mysqli_fetch_array($fd);
                     if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $parcel["delivery_companylogo"] = $visit_logo.'';
                    
                    $parcel["parcel_collected_date"] =  date("d M Y h:i A", strtotime($data['parcel_collected_date']));
                    if ($data['parcel_deliverd_date']=="0000-00-00 00:00:00") {
                        $parcel["parcel_deliverd_date"] =  "Yet not collected";
                        
                    } else {
                        $parcel["parcel_deliverd_date"] =  date("d M Y h:i A", strtotime($data['parcel_deliverd_date']));

                    }

                    $qr_size = "300x300";
                    $qr_content ='PAR0'.$data['parcel_collect_master_id'];
                    $qr_correction = strtoupper('H');
                    $qr_encoding = 'UTF-8';
                    $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                 if ($data['pass_code']==0 || $data['pass_code']=='') {
                     $parcel['qr_code'] = "";
                    $parcel['qr_code_ios'] = "";
                 } else {
                    $parcel['qr_code'] = $qrImageUrl;
                    $parcel['qr_code_ios'] = $qrImageUrl . '.png';
                 }

                    $visitDate = date("d M Y h:i A", strtotime($data['visit_date']." ".$data['visit_time']));
                    if ($data['exit_date']!='') {
                    $exitDate = date("d M Y h:i A", strtotime($data['exit_date']." ".$data['exit_time']));
                    } else {
                        $exitDate=" ";
                    }

                    $visit_date = date("d M Y", strtotime($data['visit_date']));
                    $visit_time = date("h:i A", strtotime($data['visit_time']));

                    // $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_name='$data[visit_from]'");
                    // $vistLogo=mysqli_fetch_array($fd);
                    // if ($vistLogo['visitor_sub_image']!='') {
                    //     $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                    // } else {
                    //     $visit_logo="";
                    // }

                    $parcel["visitor_id"]=$data['visitor_id'];
                    $parcel["society_id"]=$data['society_id'];
                    $parcel["unit_id"]=$data['unit_id'];
                    $parcel["user_id"]=$data['user_id'];
                    $parcel["visitor_type"]=$data['visitor_type'];
                    $parcel["expected_type"]=$data['expected_type'];
                    $parcel["visitor_name"]=$data['visitor_name'].'';
                    $parcel["visit_from"]=''.$data['visit_from'];
                    $parcel["visit_from"]=''.$data['visit_from'];
                    $parcel["visit_logo"]=$visit_logo;
                    $parcel["visitor_profile"]=$base_url."img/visitor/".$data['visitor_profile'];
                    $parcel["visitor_mobile"]=$data['visitor_mobile'].'';
                    $parcel["vistor_number"]=$data['number_of_visitor'];
                    $parcel["visiting_reason"]="".$data['visiting_reason'];
                    $parcel["visit_date"]=$data['visit_date'];
                    $time1 = strtotime($data['visit_date'].' '.$data['visit_time']);
                    $time2 = strtotime($data['valid_till_date']);
                    $difference = round(abs($time2 - $time1) / 3600);
                    if ($difference==1) {
                       $difference= $difference.' Hour';
                    } else {
                       $difference= $difference.' Hours';
                    }
                    $parcel["valid_till_date"]=$difference;
                    

                    $parcel["visit_time"]=$data['visit_time'];
                    $parcel["visit_time_view"]=date("h:i A", strtotime($data['visit_time']));
                    $parcel["exit_date"]="".$data['exit_date'];
                    $parcel["exit_time"]="".$data['exit_time'];
                    // $parcel["visitDate"]="".$visitDate;
                    // $parcel["exitDate"]="".$exitDate;
                    $parcel["visitor_status"]=$data['visitor_status'];
                    $parcel["vehicle_no"]=$data['vehicle_no'].'';
                    $parcel["visitor_sub_type_id"]=$data['visitor_sub_type_id'].'';
                    $parcel["leave_parcel_at_gate"]=$data['leave_parcel_at_gate'].'';

                      

                    array_push($response["parcel"], $parcel);
                }

                $response["message"] = "$datafoundMsg";
                $response["status"] = "200";
                echo json_encode($response);
                } else {
                    $response["message"] = "$noDatafoundMsg";
                    $response["status"] = "201";
                    echo json_encode($response);
                }
            }else if (isset($parcelReceived) && $parcelReceived == 'parcelReceived'  && $society_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($parcel_collect_master_id, FILTER_VALIDATE_INT) == true  ) { 

                $m->set_data('parcel_status', 2);
                $m->set_data('parcel_deliverd_date', date("Y-m-d H:i"));

                 $a = array(
                    'parcel_status'=>$m->get_data('parcel_status'),
                    'parcel_deliverd_date'=>$m->get_data('parcel_deliverd_date'),
                );

                $q = $d->update("parcel_collect_master", $a, "parcel_collect_master_id='$parcel_collect_master_id' AND user_id='$user_id'");
                if ($q>0) {

                    $qGaurdToken=$d->select("employee_master","emp_mobile='$gatekeeper_mobile' AND society_id='$society_id'");  
                    $data_Gaurd=mysqli_fetch_array($qGaurdToken);
                    $sos_Gaurd_token= $data_Gaurd['emp_token'];

                    if ($sos_Gaurd_token!='') {
                        # code...
                        $title = "Your Collected parcel Received by $user_name";
                        $des = "at ". date("Y-m-d H:i");
                        $nGaurd->noti_new("visitor",$sos_Gaurd_token,$title,$des,"parcel~1");
                       
                        $notiAry = array(
                            'society_id'=>$society_id,
                            'guard_notification_title'=>$title,
                            'guard_notification_desc'=>$des,
                            'employee_id'=>$data_Gaurd['emp_id'],
                            'guard_notification_date'=>date('Y-m-d H:i'),
                            'click_action'=>'parcel~1',
                            'notification_logo'=>'my-parcelxxxxhdpi.png',
                          );
                          
                        $d->insert("guard_notification_master",$notiAry);
                    }
                        $d->insert_myactivity($user_id,"$society_id","0","$user_name","Parcel received from gate","my-parcelxxxxhdpi.png");

                    $parcel_received = $xml->string->parcel_received;

                    $response["message"] = "$parcel_received";
                    $response["status"] = "200";
                    echo json_encode($response);
                }else {
                    $response["message"] = "$somethingWrong";
                    $response["status"] = "201";
                    echo json_encode($response);
                }


            }else if (isset($getVisitorStatus) && $getVisitorStatus == 'getVisitorStatus'  && $society_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true && filter_var($user_id, FILTER_VALIDATE_INT) == true  && filter_var($visitor_id, FILTER_VALIDATE_INT) == true  ) { 

                $qq=$d->select("users_master,visitors_master","visitors_master.user_id=users_master.user_id AND visitors_master.visitor_id='$visitor_id'");
                $visitorData=mysqli_fetch_array($qq);
                if (mysqli_num_rows($qq)>0) {
                    
                      
                    $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$visitorData[visitor_sub_type_id]'");
                    $vistLogo=mysqli_fetch_array($fd);
                     if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                        } else {
                            $visit_logo="";
                        }
                    $visit_time= date("d M Y h:i A", strtotime($visit_date.' '.$visit_time));

                    $qUserToken=$d->select("users_master,block_master,unit_master","block_master.block_id=block_master.block_id AND block_master.block_id=users_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.user_id='$user_id' ");
                    $data_notification=mysqli_fetch_array($qUserToken);
                    $unit_name=$data_notification['block_name'].'-'.$data_notification['unit_name'];
                    $user_name=$data_notification['full_name'];


                        $response["visitor_id"] = $visitorData['parent_visitor_id'];
                        $response["visitor_name"] = $visitorData['visitor_name'];
                        $response["visitor_profile"] =  $base_url.'img/visitor/'.$visitorData['visitor_profile'];
                        $response["visitor_type"] = $visitorData['visitor_type'];
                        $response["visitor_mobile"] =$visitorData['country_code'].' '.$visitorData['visitor_mobile'];
                        $response["visit_from"] = $visitorData['visit_from'];
                        $response["visit_logo"] = $visit_logo;
                        $response["visit_time"] = $visit_time;
                        $response["visitor_status"] =$visitorData['visitor_status'];
                        $response["unit_name"] =$unit_name;
                        $response["user_name"] =$user_name;
                        $response["society_id"] =$society_id;

                        if ($visitorData['visitor_status']==1) {
                            $ap=$d->selectRow("user_full_name","users_master","user_id='$visitorData[approve_by_id]' ");
                            $userData=mysqli_fetch_array($ap);
                            $user_full_name= $userData['user_full_name'];
                            $approved_by = $xml->string->approved_by;
                            $approved_by = str_replace("{user_full_name}", $user_full_name, $approved_by);
                            $view_message = "$approved_by";
                        } else if ($visitorData['visitor_status']==4) {
                            $ap=$d->selectRow("user_full_name","users_master","user_id='$visitorData[reject_by_id]' ");
                            $userData=mysqli_fetch_array($ap);
                            $user_full_name= $userData['user_full_name'];
                            if ($user_full_name=='') {
                            $auto_rejected_view = $xml->string->auto_rejected_view;
                            $view_message = "$auto_rejected_view";
                            } else {
                            $rejected_by = $xml->string->rejected_by;
                            $rejected_by = str_replace("{user_full_name}", $user_full_name, $rejected_by);
                            $view_message = "$rejected_by";
                            }
                        } else if ($visitorData['visitor_status']==6) {
                            $ap=$d->selectRow("user_full_name","users_master","user_id='$visitorData[hold_by_id]' ");
                            $userData=mysqli_fetch_array($ap);
                            $user_full_name= $userData['user_full_name'];
                            $hold_by = $xml->string->hold_by;
                            $hold_by = str_replace("{user_full_name}", $user_full_name, $hold_by);
                            $view_message = "$hold_by";
                        }else if ($visitorData['visitor_status']==3) {
                            $already_exited = $xml->string->already_exited;
                            $view_message = "$already_exited";
                        } else if ($visitorData['visitor_status']==5) {
                            $visitor_already_deleted = $xml->string->visitor_already_deleted;

                         $view_message ="$visitor_already_deleted";
                        }

                        $response["view_message"] =$view_message.'';
                        
                        $response["message"] = "$datafoundMsg";
                        $response["status"] = "200";
                        echo json_encode($response);
                   
                } else {
                    $response["message"] = "$noDatafoundMsg";
                    $response["status"] = "201";
                    echo json_encode($response);
                }
                


            }else if($_POST['getNewVisitorListCombo']=="getNewVisitorListCombo" && filter_var($society_id, FILTER_VALIDATE_INT) == true  && filter_var($user_id, FILTER_VALIDATE_INT) == true ){

                $qnotification=$d->select("users_master,visitors_master","(users_master.user_id=visitors_master.user_id AND  (visitors_master.user_id='$user_id' AND visitors_master.visitor_status != 5 AND visitors_master.society_id='$society_id') AND  (visitors_master.visitor_type = 1 OR visitors_master.visitor_type = 0 OR visitors_master.visitor_type =2 or visitors_master.visitor_type =3))","ORDER BY visitors_master.visitor_id DESC LIMIT 100");

                $response["visitor"] = array();

                if(mysqli_num_rows($qnotification)>0){

                    while($data_notification=mysqli_fetch_array($qnotification)) {
                     $data_notification = array_map("html_entity_decode", $data_notification);
                        
                        $sq=$d->selectRow("society_name,society_latitude,society_longitude","society_master","society_id='$data_notification[society_id]'");
                        $societyData=mysqli_fetch_array($sq);
                        $society_name=$societyData['society_name'];
                        $society_latitude=$societyData['society_latitude'];
                        $society_longitude=$societyData['society_longitude'];
                        $mapLink= "https://maps.google.com/?q=$society_latitude,$society_longitude";
                       

                        $visit_date = date("d M Y", strtotime($data_notification['visit_date']));
                        $visit_time = date("h:i A", strtotime($data_notification['visit_time']));

                        $otp = $data_notification['otp'];

                        
                         $visitDate = date("d M Y h:i A", strtotime($data_notification['visit_date']." ".$data_notification['visit_time']));
                         if ($data_notification['exit_date']!='') {
                             $exitDate = date("d M Y h:i A", strtotime($data_notification['exit_date']." ".$data_notification['exit_time']));
                         } else {
                            $exitDate="$not_available";
                         }

                         if ($data_notification['visitor_added_from_face_app'] != 1) {
                             // code...

                            $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data_notification[visitor_sub_type_id]'");
                            $vistLogo=mysqli_fetch_array($fd);
                            if ($vistLogo['visitor_sub_image']!='') {
                                $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                            } else {
                                $visit_logo="";
                            }
                        }else{
                            $fd=$d->selectRow("main_type_image","visitorMainType","visitor_main_type_id='$data_notification[visitor_sub_type_id]'");
                            $vistLogo=mysqli_fetch_array($fd);
                            if ($vistLogo['main_type_image']!='') {
                                $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['main_type_image'].'';
                            } else {
                                $visit_logo="";
                            }
                        }
                       

                        $visit_from = html_entity_decode($data_notification['visit_from']);
                        $visitFrom = explode('-', $visit_from);
                        $visitor = array(); 
                        $visitor["visitor_id"]=$data_notification['visitor_id'];
                        $visitor["parent_visitor_id"]=$data_notification['parent_visitor_id'];
                        $visitor["society_id"]=$data_notification['society_id'];
                        $visitor["unit_id"]=$data_notification['unit_id'];
                        $visitor["user_id"]=$data_notification['user_id'];
                        $visitor["visitor_type"]=$data_notification['visitor_type'];
                        $visitor["expected_type"]=$data_notification['expected_type'];
                        $visitor["visitor_name"]=$data_notification['visitor_name'];
                        $visitor["visit_from"]=$visit_from.'';
                        $visitor["visit_logo"]=$visit_logo.'';
                        $visitor["visitor_mobile"]="".$data_notification['visitor_mobile'];
                        $visitor["country_code"]=$data_notification['country_code'];
                        $visitor["visitor_profile"]=$base_url."img/visitor/".$data_notification['visitor_profile'];
                        if ($data_notification['visitor_type']==0 || $data_notification['visitor_type']==1) {
                            $visitor["vistor_number"]=$data_notification['number_of_visitor'];
                        }else if ($data_notification['visitor_type']==2) {
                            $visitor["vistor_number"]='1';
                        }else if ($data_notification['visitor_type']==3) {
                            $visitor["vistor_number"]='1';
                        }
                        $visitor["visiting_reason"]="".$data_notification['visiting_reason'];
                        $visitor["visit_date"]=$data_notification['visit_date'];
                        $visitor["visit_date_view"]=date("d-m-Y", strtotime($data_notification['visit_date']));
                        $visitor["visit_time"]=$data_notification['visit_time'];
                        $visitor["visit_time_view"]=date("h:i A", strtotime($data_notification['visit_time']));
                        $visitor["exit_date"]="".$data_notification['exit_date'];
                        $visitor["exit_time"]="".$data_notification['exit_time'];
                        $visitor["visitDate"]="".$visitDate;
                        $visitor["exitDate"]="".$exitDate;
                        $visitor["vehicle_no"]=$data_notification['vehicle_no'].'';
                        $visitor["visitor_status"]=$data_notification['visitor_status'];
                        $visitor["visitor_approved"]=$data_notification['visitor_approved'];
                        $visitor["delivery_cab_approval"]=$data_notification['Delivery_cab_approval'];
                        $visitor["visitor_sub_type_id"]=$data_notification['visitor_sub_type_id'].'';
                        $visitor["visitor_added_from_face_app"]=$data_notification['visitor_added_from_face_app'].'';


                        $visitor["leave_parcel_at_gate"]=$data_notification['leave_parcel_at_gate'].'';
                        if ($data_notification['visitor_type']==1) {
                            $visitor["only_otp"]=$data_notification['otp'];
                            
                            $visitor["otp"]="Invitation Dear $data_notification[visitor_name],\nYou are registered as a prospective visitor to $society_name by $data_notification[user_full_name]\nVisiting Date: $visit_date, $visit_time.\nPlease share OTP $otp with security guard.\n\n$mapLink\n\nThank You,\n Team MyCo";

                            $qr_size = "300x300";
                            $qr_content ="expected_".$data_notification['visitor_id'];
                            $qr_correction = strtoupper('H');
                            $qr_encoding = 'UTF-8';
                            $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                            $visitor['qr_code'] = $qrImageUrl;
                            $visitor['qr_code_ios'] = $qrImageUrl . '.png';
                        } else {
                            $visitor["only_otp"]="";
                            $visitor["otp"] ="";
                            $visitor['qr_code'] = "";
                            $visitor['qr_code_ios'] = "";
                        }

                        $qqq=$d->select("parcel_collect_master","parcel_collect_master_id='$data_notification[parcel_collect_master_id]'");
                        $parcelData=mysqli_fetch_array($qqq);
                        $no_of_parcel = $parcelData['no_of_parcel'];
                        if ($no_of_parcel>0) {
                        $visitor["no_of_parcel"]=''.$no_of_parcel;
                        }else {
                        $visitor["no_of_parcel"]='1';
                        }
                        
                        if ($data_notification['visitor_status']==2) {
                            if ($data_notification['in_temperature']>0) {
                                $in_temperature=$data_notification['in_temperature'];
                            } else {
                                $in_temperature = "";
                            }
                            $visitor["temperature"]=$in_temperature.'';
                            $visitor["with_mask"]=$data_notification['in_with_mask'].'';
                        } else if ($data_notification['visitor_status']==3) {
                             if ($data_notification['out_temperature']>0) {
                                $out_temperature=$data_notification['out_temperature'];
                            } else {
                                $out_temperature = "";
                            }
                            $visitor["temperature"]=$out_temperature.'';
                            $visitor["with_mask"]=$data_notification['out_with_mask'].'';
                        } else {
                            $visitor["temperature"]='';
                            $visitor["with_mask"]='';
                        }

                        $time1 = strtotime($data_notification['visit_date'].' '.$data_notification['visit_time']);
                        $time2 = strtotime($data_notification['valid_till_date']);
                        $difference = round(abs($time2 - $time1) / 3600);
                        if ($difference==1) {
                           $difference= $difference.' Hour';
                        } else {
                           $difference= $difference.' Hours';
                        }
                        $visitor["valid_till_date"]=$difference;

                        $intTime=$data_notification['visit_date']." ".$data_notification['visit_time'];
                        $outTime=date("Y-m-d H:i:s");
                        $date_a = new DateTime($intTime);
                        $date_b = new DateTime($outTime);
                        $interval = date_diff($date_a,$date_b);
                        $totalDays= $interval->days;
                        if ($data_notification['exit_date']=='' && $totalDays>30 && $data_notification['visitor_type']==0 || $data_notification['exit_date']=='' && $totalDays>30 && $data_notification['visitor_type']==1 || $data_notification['exit_date']=='' && $totalDays>1 && $data_notification['visitor_type']==2 || $data_notification['exit_date']=='' && $totalDays>1 && $data_notification['visitor_type']==3) {
                            
                            $visitor["exitDate"]="Auto Exit";

                        }




                        
                        array_push($response["visitor"], $visitor); 
                    }

                    $response["message"]="$datafoundMsg";
                    $response["status"]="200";
                    echo json_encode($response);

                }else{

                    $response["message"]="$noDatafoundMsg";
                    $response["status"]="201";
                    echo json_encode($response);

                }

            }  else{
                $response["message"]="wrong tag.";
                $response["status"]="201";
                echo json_encode($response);

            }

        

    }else{

         $response["message"]="wrong api key.";
        $response["status"]="201";
        echo json_encode($response);

    }

}?>
