<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    // for unit close & Open
    if ($key == $keydb && $auth_check=='true') {

        $response = array();
        extract(array_map("test_input", $_POST));

        if (isset($_POST['switchClose']) && $unit_id != '' && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {

            $gu=$d->select("users_master","delete_status=0 AND unit_id='$unit_id' AND society_id='$society_id' AND member_status=0 AND user_type=1","ORDER BY user_id DESC");
            $tenantData=mysqli_fetch_array($gu);
            if ($tenantData>0 && $unit_status != 5) {
                $unit_status=3;
            } 

            $m->set_data('unit_status', $unit_status);

            $a = array('unit_status' => $m->get_data('unit_status'));

            $quserDataUpdate = $d->update("unit_master", $a, "unit_id ='$unit_id' AND unit_id!=0");

            if ($quserDataUpdate == TRUE) {


                // get unit name 
                $qc=$d->select("block_master,unit_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id='$unit_id'");
                $unitData=mysqli_fetch_array($qc);
                $unit_name= $unitData['block_name'].'-'.$unitData['unit_name'];

                if ($unit_status == 5) {
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","$unit_name unit close","home.png");

                    $response["message"] = "$unit_name unit is close";
                } else {
                     $d->insert_myactivity($user_id,"$society_id","0","$user_name","$unit_name unit open","home.png");

                    $response["message"] = "$unit_name unit is open";
                }

                $fcmArray = $d->get_emp_fcm("employee_master", "emp_token!='' AND emp_type_id='0'  AND society_id='$society_id'");
                $nGaurd->noti_new("",$fcmArray, $response['message'],"By $user_name ", 'member');


                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Switch close faild.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        } else if (isset($_POST['switchCloseGatekeeper']) && $unit_id != '' && filter_var($unit_id, FILTER_VALIDATE_INT) == true) {

            if ($unit_status == 5) {
                $unit_close_for_gatekeeper=1;
            } else {
                $unit_close_for_gatekeeper=0;
            } 

            $m->set_data('unit_close_for_gatekeeper', $unit_close_for_gatekeeper);

            $a = array('unit_close_for_gatekeeper' => $m->get_data('unit_close_for_gatekeeper'));

            $quserDataUpdate = $d->update("unit_master", $a, "unit_id ='$unit_id' AND unit_id!=0");

            if ($quserDataUpdate == TRUE) {


                // get unit name 
                $qc=$d->select("block_master,unit_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id='$unit_id'");
                $unitData=mysqli_fetch_array($qc);
                $unit_name= $unitData['block_name'].'-'.$unitData['unit_name'];

                if ($unit_close_for_gatekeeper == 1) {
                    $d->insert_myactivity($user_id,"$society_id","0","$user_name","$unit_name unit close","home.png");

                    $response["message"] = "$unit_name unit is close";
                } else {
                     $d->insert_myactivity($user_id,"$society_id","0","$user_name","$unit_name unit open","home.png");

                    $response["message"] = "$unit_name unit is open";
                }

                $fcmArray = $d->get_emp_fcm("employee_master", "emp_token!='' AND emp_type_id='0'  AND society_id='$society_id'");
                $nGaurd->noti_new("",$fcmArray, $response['message'],"By $user_name ", 'member');


                $response["status"] = "200";
                echo json_encode($response);
            } else {

                $response["message"] = "Switch close faild.";
                $response["status"] = "201";
                echo json_encode($response);
            }
        }  else {
            $response["message"] = "wrong tag";
            $response["status"] = "201";
            echo json_encode($response);
        }
    } else {
        $response["message"] = "wrong api key";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
