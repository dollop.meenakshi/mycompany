<?php
include_once 'lib.php';

if (isset($_POST) && !empty($_POST)) {

    if ($key == $keydb && $auth_check=='true') {
        $response = array();
        extract(array_map("test_input", $_POST));
        $date = date('Y-m-d H:i:s');

        if ($_POST['addChatDoc'] == "addChatDoc" && filter_var($society_id, FILTER_VALIDATE_INT) == true) {

            $imgAray =array();
            $total = count($_FILES['chat_doc']['tmp_name']);
            for ($i = 0; $i < $total; $i++) {
              $uploadedFile = $_FILES['chat_doc']['name'][$i];
              if ($uploadedFile != "") {
                $temp = explode(".", $uploadedFile);
                $feed_img = "Chat_".$user_id . round(microtime(true)) .$i.'.' . end($temp);
                move_uploaded_file($_FILES['chat_doc']['tmp_name'][$i], "../img/chatImg/" . $feed_img);
                $file_original_name = $uploadedFile;
                $bytesSize = $_FILES['chat_doc']['size'][$i];
                $file_size= $d->formatSizeUnits($bytesSize);
                $imagePath= $base_url.'img/chatImg/'.$feed_img;

                array_push($imgAray, $imagePath);
              }
            }

            $response["image_array"] = $imgAray;
            $response["message"] = "Photo Uploaded";
            $response["status"] = "200";
            echo json_encode($response);
            exit();

        }else  if ($_POST['deleteDoc'] == "deleteDoc" && filter_var($society_id, FILTER_VALIDATE_INT) == true) {
            $total = count($chat_doc_url);
            for ($i = 0; $i < $total; $i++) {
                
                unlink($chat_doc_url[$i]);   

            }

            $response["message"] = "Photo Deleted";
            $response["status"] = "200";
            echo json_encode($response);

            exit(); 
        }  else {
            $response["message"] = "wrong tag.";
            $response["status"] = "201";
            echo json_encode($response);
        }
    }  else {

        $response["message"] = "wrong api key.";
        $response["status"] = "201";
        echo json_encode($response);
    }
}
