<?php

include_once '../residentApiNew/lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getAllProducts']=="getAllProducts"  && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $response["product_category"] = array();

        $response["products"] = array();

        $cartCount = $d->count_data_direct("purchase_cart_id","purchase_cart_master","user_id = '$user_id'");

        $response['cart_count'] = $cartCount;


        $qry = $d->select("product_category_master","society_id = '$society_id' AND product_category_status = '0' AND product_category_delete = '0'");

        if(mysqli_num_rows($qry)>0){
            

            while($data = mysqli_fetch_array($qry)) {

                $data = array_map("html_entity_decode", $data);

                $product_category = array();

                $product_category["product_category_id"] = $data['product_category_id'];
                $product_category["category_name"] = $data['category_name'];
                     
                array_push($response["product_category"], $product_category);
            }            
        }

        $qry = $d->selectRow("
            product_category_master.product_category_id,
            product_category_master.category_name,
            unit_measurement_master.unit_measurement_id,
            unit_measurement_master.unit_measurement_name,
            product_master.*,
            product_variant_master.product_variant_id,
            product_variant_master.product_variant_name,
            product_price_master.product_price_id,
            product_price_master.minimum_order_quantity
            ","
            product_category_master,
            unit_measurement_master,
            product_variant_master,
            product_master,
            product_price_master
            ","
            product_master.society_id = '$society_id' 
            AND product_master.product_status = '0' 
            AND product_master.product_delete = '0' 
            AND product_master.product_id = product_variant_master.product_id  
            AND product_master.unit_measurement_id = unit_measurement_master.unit_measurement_id 
            AND product_master.product_category_id = product_category_master.product_category_id 
            AND product_price_master.product_variant_id = product_variant_master.product_variant_id 
            AND product_price_master.product_id = product_master.product_id 
            AND product_price_master.product_id = product_variant_master.product_id 
            AND product_price_master.product_price_delete_status = '0' 
            AND product_price_master.product_active_status = '0'","");

        if(mysqli_num_rows($qry)>0){
            

            while($data = mysqli_fetch_array($qry)) {


                $data = array_map("html_entity_decode", $data);

                $products = array();

                $products["product_id"] = $data['product_id'];
                $products["product_code"] = $data['product_code'];
                $products["product_name"] = $data['product_name'];
                $products["product_variant_name"] = $data["product_variant_name"];
                $products["product_alias_name"] = $data['product_alias_name'];
                $products["product_variant_id"] = $data['product_variant_id'];
                $products["product_variant_name"] = $data['product_variant_name'];
                $products["product_short_name"] = $data['product_short_name'];
                $products["product_category_id"] = $data['product_category_id'];
                $products["category_name"] = $data['category_name'];
                $products["product_sub_category_id"] = $data['product_sub_category_id'].'';
                $products["sub_category_name"] = $data['sub_category_name'].'';
                $products["product_brand"] = $data['product_brand'];
                $products["product_brand"] = $data['product_brand'];
                $products["unit_measurement_id"] = $data['unit_measurement_id'];
                $products["unit_measurement_name"] = $data['unit_measurement_name'];
                $products["product_price_id"] = $data['product_price_id'];
                $products["minimum_order_quantity"] = $data['minimum_order_quantity'];

                $qq = $d->selectRow("purchase_cart_id,cart_quantity","purchase_cart_master","product_id = '$data[product_id]' AND user_id = '$user_id' AND product_variant_id = '$data[product_variant_id]' AND product_price_id = '$data[product_price_id]'");

                if (mysqli_num_rows($qq) > 0) {
                    $dataCart = mysqli_fetch_array($qq);
                    $products["purchase_cart_id"] = $dataCart['purchase_cart_id']."";
                    $products["no_of_item_in_cart"] = $dataCart['cart_quantity']."";
                }else{
                    $products["purchase_cart_id"] = "0";
                    $products["no_of_item_in_cart"] = "0";
                }

                if ($data['product_image'] != '') {
                    $products["product_image"] = $base_url.'img/product/'.$data['product_image'];
                }else{
                    $products["product_image"] = '';
                }

                $products["other"] = $data['other'];
                $products["product_status"] = $data['product_status'];

                if ($data['product_created_date'] == '0000-00-00 00:00:00') {
                    $data['product_created_date'] = "";
                }else{
                    $products["product_created_date"] = date("d M Y, h:i A",strtotime($data['product_created_date']));
                }
                     
                array_push($response["products"], $products);
            }
        }

        $response["message"]="Success";
        $response["status"]="200";
        echo json_encode($response);

    }else if($_POST['addPurchase']=="addPurchase" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $PONumber = "PO_".date('ymdHis');

        $m->set_data('society_id',$society_id);
        $m->set_data('site_id',$site_id);
        $m->set_data('user_id',$user_id);
        $m->set_data('po_number',$PONumber);
        $m->set_data('purchase_date',$dateTime);
        $m->set_data('created_by',$user_id);
        $m->set_data('created_date',$dateTime);
        $m->set_data('purchase_stage',"1");

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'site_id'=>$m->get_data('site_id'),
            'user_id'=>$m->get_data('user_id'),
            'po_number'=>$m->get_data('po_number'),
            'purchase_date'=>$m->get_data('purchase_date'),
            'created_by'=>$m->get_data('created_by'),
            'created_date'=>$m->get_data('created_date'),
            'purchase_stage'=>$m->get_data('purchase_stage'),
        );

        $qry = $d->insert("purchase_master",$a);

        $purchase_id = $con->insert_id;

        if ($qry == true) {

            $productIdAry = explode(",",$productIds);
            $quantitiesAry = explode(",",$quantities);
            $variantListAry = explode(",",$variant_list);
            $productPriceListAry = explode(",",$product_price_list);

            for ($i=0; $i < count($productIdAry) ; $i++) { 
                $m->set_data('society_id',$society_id);
                $m->set_data('purchase_id',$purchase_id);
                $m->set_data('product_id',$productIdAry[$i]);
                $m->set_data('product_variant_id',$variantListAry[$i]);
                $m->set_data('product_price_id',$productPriceListAry[$i]);
                $m->set_data('quantity',$quantitiesAry[$i]);

                $a1 = array(
                    'society_id'=>$m->get_data('society_id'),
                    'purchase_id'=>$m->get_data('purchase_id'),
                    'product_id'=>$m->get_data('product_id'),
                    'product_variant_id'=>$m->get_data('product_variant_id'),
                    'product_price_id'=>$m->get_data('product_price_id'),
                    'quantity'=>$m->get_data('quantity'),
                );

                $qry = $d->insert("purchase_product_master",$a1);

                $puchase_product_id = $con->insert_id;

                $m->set_data('puchase_product_id',$puchase_product_id);
                $m->set_data('purchase_id',$purchase_id);
                $m->set_data('new_quantity',$quantitiesAry[$i]);
                $m->set_data('changed_by',$user_id);
                $m->set_data('changed_date',date('Y-m-d H:i:s'));

                $aryHis = array(
                    'puchase_product_id'=>$m->get_data('puchase_product_id'),
                    'purchase_id'=>$m->get_data('purchase_id'),
                    'new_quantity'=>$m->get_data('new_quantity'),
                    'changed_by'=>$m->get_data('changed_by'),
                    'changed_date'=>$m->get_data('changed_date'),
                );

                $qryHistory = $d->insert("purchase_product_change_history",$aryHis);
            }

            $d->delete("purchase_cart_master","society_id = '$society_id' AND user_id = '$user_id'");

            $siteUsersQry = $d->selectRow("user_id,user_token,device","users_master,site_master,site_manager_master","
                site_master.site_id = site_manager_master.site_id 
                AND site_manager_master.site_id = '$site_id' 
                AND users_master.user_id = site_manager_master.site_manager_id 
                AND site_manager_master.manager_type = '1'");

            if (mysqli_num_rows($siteUsersQry) > 0) {
                
                $fcmArrayAndroid=array();
                $fcmArrayiOS=array();
                $userFcmIds=array();

                while ($row=mysqli_fetch_array($siteUsersQry)) {
                    if ($row['device'] == "android" || $row['device'] == "Android") {
                        array_push($fcmArrayAndroid, $row['user_token']);
                    }else if ($row['device'] == "ios" || $row['device'] == "iOS") {
                        array_push($fcmArrayiOS, $row['user_token']);
                    }
                    array_push($userFcmIds, $row['user_id']);
                }

                $userFcmIds = implode("','",$userFcmIds);

                $title = "New Purchase Request";
                $description = "By, ".$user_name;

                $nResident->noti("purchase_request","",$society_id,$fcmArrayAndroid,$title,$description,"");
                $nResident->noti_ios("purchase_request","",$society_id,$fcmArrayiOS,$title,$description,"");

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"purchase_request","purchase.png","","users_master.user_id IN ('$userFcmIds')");
            }

            $response["message"]="Purchase added";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['addToCart']=="addToCart" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('society_id',$society_id);
        $m->set_data('user_id',$user_id);
        $m->set_data('product_id',$product_id);
        $m->set_data('product_variant_id',$product_variant_id);
        $m->set_data('product_price_id',$product_price_id);
        $m->set_data('cart_quantity',$cart_quantity);
        $m->set_data('created_date',$dateTime);

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'user_id'=>$m->get_data('user_id'),
            'product_id'=>$m->get_data('product_id'),
            'product_variant_id'=>$m->get_data('product_variant_id'),
            'product_price_id'=>$m->get_data('product_price_id'),
            'cart_quantity'=>$m->get_data('cart_quantity'),
            'created_date'=>$m->get_data('created_date'),
        );

        if ($purchase_cart_id == '0') {
            $qry = $d->insert("purchase_cart_master",$a);

            $purchase_cart_id = $con->insert_id;
        }else{

            if ($cart_quantity == 0) {
                $qry = $d->delete("purchase_cart_master","purchase_cart_id = '$purchase_cart_id' AND user_id = '$user_id'");
            }else{
                $m->set_data('cart_quantity',$cart_quantity);
                $m->set_data('updated_date',$dateTime);

                $aa = array(
                    'cart_quantity'=>$m->get_data('cart_quantity'),
                    'updated_date'=>$m->get_data('updated_date'),
                );

                $qry = $d->update("purchase_cart_master",$aa,"purchase_cart_id = '$purchase_cart_id' AND user_id = '$user_id' AND product_variant_id = '$product_variant_id'");
            }
        }

        if ($qry == true) {

            $cartCount = $d->count_data_direct("purchase_cart_id","purchase_cart_master","user_id = '$user_id'");

            $response['cart_count'] = $cartCount;
            $response["purchase_cart_id"]=$purchase_cart_id."";

            if ($purchase_cart_id == '0') {
                $response["message"]="Added to cart";
            }else{
                $response["message"]="Cart Updated";
            }
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updateCart']=="updateCart" && $purchase_cart_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('cart_quantity',$cart_quantity);
        $m->set_data('updated_date',$dateTime);

        $a = array(
            'cart_quantity'=>$m->get_data('cart_quantity'),
            'updated_date'=>$m->get_data('updated_date'),
        );

        $qry = $d->update("purchase_cart_master",$a,"purchase_cart_id = '$purchase_cart_id' AND user_id = '$user_id'");

        if ($qry == true) {

            $response["message"]="Cart Updated";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['removeCartItem']=="removeCartItem" && $purchase_cart_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->delete("purchase_cart_master","purchase_cart_id = '$purchase_cart_id' AND user_id = '$user_id'");

        if ($qry == true) {

            $response["message"]="Product Removed";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getMyCart']=="getMyCart"  && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->selectRow("
            product_category_master.product_category_id,
            product_category_master.category_name,
            unit_measurement_master.unit_measurement_id,
            unit_measurement_master.unit_measurement_name,
            purchase_cart_master.purchase_cart_id,
            purchase_cart_master.cart_quantity,
            product_master.*,
            product_variant_master.product_variant_id,
            product_variant_master.product_variant_name,
            product_price_master.product_price_id,
            product_price_master.minimum_order_quantity
            ","
            product_category_master,
            unit_measurement_master,
            product_master,
            purchase_cart_master,
            product_variant_master,
            product_price_master
            ","
            product_master.society_id = '$society_id' 
            AND product_master.product_status = '0' 
            AND product_master.product_delete = '0' 
            AND product_variant_master.product_id = product_master.product_id
            AND product_variant_master.product_variant_id = purchase_cart_master.product_variant_id
            AND product_master.unit_measurement_id = unit_measurement_master.unit_measurement_id 
            AND product_master.product_category_id = product_category_master.product_category_id 
            AND product_master.product_id = purchase_cart_master.product_id 
            AND product_price_master.product_variant_id = product_variant_master.product_variant_id 
            AND product_price_master.product_id = product_master.product_id 
            AND product_price_master.product_id = product_variant_master.product_id 
            AND purchase_cart_master.user_id = '$user_id'
            AND purchase_cart_master.product_variant_id = product_variant_master.product_variant_id
            AND purchase_cart_master.product_id = product_master.product_id 
            AND purchase_cart_master.product_price_id = product_price_master.product_price_id","");

        if(mysqli_num_rows($qry)>0){
            
            $response["products"] = array();

            while($data = mysqli_fetch_array($qry)) {


                $data = array_map("html_entity_decode", $data);

                $products = array();

                $products["product_id"] = $data['product_id'];
                $products["product_code"] = $data['product_code'];
                $products["product_name"] = $data['product_name'];
                $products["product_id"] = $data['product_id'];
                $products["product_alias_name"] = $data['product_alias_name'];
                $products["product_variant_id"] = $data['product_variant_id'];
                $products["product_variant_name"] = $data['product_variant_name'];
                $products["product_short_name"] = $data['product_short_name'];
                $products["product_category_id"] = $data['product_category_id'];
                $products["category_name"] = $data['category_name'];
                $products["product_sub_category_id"] = $data['product_sub_category_id'];
                $products["sub_category_name"] = $data['sub_category_name'].'';
                $products["product_brand"] = $data['product_brand'];
                $products["unit_measurement_id"] = $data['unit_measurement_id'];
                $products["unit_measurement_name"] = $data['unit_measurement_name'];
                $products["minimum_order_quantity"] = $data['minimum_order_quantity'];
                $products["product_price_id"] = $data['product_price_id'];

                $products["purchase_cart_id"] = $data['purchase_cart_id']."";
                $products["no_of_item_in_cart"] = $data['cart_quantity']."";

                if ($data['product_image'] != '') {
                    $products["product_image"] = $base_url.'img/product/'.$data['product_image'];
                }else{
                    $products["product_image"] = '';
                }

                $products["other"] = $data['other'];
                $products["product_status"] = $data['product_status'];

                if ($data['product_created_date'] == '0000-00-00 00:00:00') {
                    $data['product_created_date'] = "";
                }else{
                    $products["product_created_date"] = date("d M Y, h:i A",strtotime($data['product_created_date']));
                }
                     
                array_push($response["products"], $products);
            }
        }

        $response["message"]="Success";
        $response["status"]="200";
        echo json_encode($response);

    }else if($_POST['getPurchase']=="getPurchase" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("purchase_master,site_master","purchase_master.society_id='$society_id' AND purchase_master.user_id = '$user_id' AND site_master.site_id = purchase_master.site_id","ORDER BY purchase_master.purchase_id DESC");

        if(mysqli_num_rows($qry)>0){
            
            $response["purchase_orders"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $purchase_orders = array();

                $purchase_orders["purchase_id"] = $data['purchase_id'];
                $purchase_orders["site_id"] = $data['site_id'];
                $purchase_orders["site_name"] = $data['site_name'];
                $purchase_orders["site_address"] = $data['site_address'];
                $purchase_orders["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
                $purchase_orders["po_number"] = $data['po_number'];
                $purchase_orders["purchase_descripion"] = $data['purchase_descripion'];

                if($data['purchase_status'] == '0'){ 
                    $status = "Pending";
                } else if($data['purchase_status'] == '1'){
                    $status = "Accepted";
                }else{
                    $status = "Rejected";
                }

                $totalItemCount = $d->count_data_direct("puchase_product_id","purchase_product_master","purchase_id = '$data[purchase_id]'");

                $purchase_orders["purchase_status"] = $data['purchase_status'];
                $purchase_orders["quantity"] = $totalItemCount.'';
                $purchase_orders["purchase_status_view"] = $status;
   
                array_push($response["purchase_orders"], $purchase_orders);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getPurchaseDetails']=="getPurchaseDetails" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("purchase_master,site_master","site_master.site_id=purchase_master.site_id AND purchase_master.society_id='$society_id'  AND purchase_master.purchase_id = '$purchase_id'");

        if(mysqli_num_rows($qry)>0){

            $data = mysqli_fetch_array($qry);

            $data = array_map("html_entity_decode", $data);

            $response["purchase_id"] = $data['purchase_id'];
            $response["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
            $response["po_number"] = $data['po_number'];
            $response["site_id"] = $data['site_id'];
            $response["site_name"] = $data['site_name'];
            $response["site_address"] = $data['site_address'];
            $response["purchase_description"] = $data['purchase_descripion'];
           
            if ($data['purchase_invoice'] != '') {
                $response["purchase_invoice"] =  $base_url."img/purchase_invoice/".  $data['purchase_invoice'];
            }else{
                $response["purchase_invoice"] =  "";
            }

            if($data['purchase_status'] == '0'){ 
                $status = "Pending";
            } else if($data['purchase_status'] == '1'){
                $status = "Accepted";
            }else{
                $status = "Rejected";
            }

            $response["purchase_status"] = $data['purchase_status'];
            $response["purchase_status_view"] = $status;

            if ($purchase_id!='') {
                $q = $d->select("
                    purchase_product_master,
                    product_master,
                    product_category_master,
                    product_variant_master,
                    unit_measurement_master
                    ","
                    purchase_product_master.product_id=product_master.product_id 
                    AND product_master.product_category_id=product_category_master.product_category_id 
                    AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id 
                    AND purchase_product_master.product_variant_id=product_variant_master.product_variant_id 
                    AND product_master.product_id=product_variant_master.product_id 
                    AND purchase_product_master.purchase_id='$purchase_id'");

                $response['products'] = array();

                if (mysqli_num_rows($q) > 0) {
                    while($purchaseData = mysqli_fetch_array($q)){

                        $purchaseData = array_map("html_entity_decode", $purchaseData);

                        $products = array();

                        $products['purchase_product_id'] = $purchaseData['puchase_product_id'];
                        $products['product_code'] = $purchaseData['product_code'];
                        $products['product_name'] = $purchaseData['product_name'];
                        $products['product_variant_id'] = $purchaseData['product_variant_id'];
                        $products['product_variant_name'] = $purchaseData['product_variant_name'];
                        $products['product_brand'] = $purchaseData['product_brand'];
                        $products['other'] = $purchaseData['other'];
                        $products['category_name'] = $purchaseData['category_name'];
                        $products['sub_category_name'] = $purchaseData['sub_category_name'].'';
                        $products['unit_measurement_name'] = $purchaseData['unit_measurement_name'];
                        $products['price'] = $purchaseData['price'].'';

                        $totalPrice = $purchaseData['price'] * $purchaseData['quantity'];

                        $totalPrice = number_format($totalPrice,2,'.','');

                        $products['total_price'] = $totalPrice."";
                        $products['quantity'] = $purchaseData['quantity'];
                        $products['availability_status'] = $purchaseData['availablity_status'];

                        array_push($response['products'],$products);
                    }
                }
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getCartCount']=="getCartCount" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $cartCount = $d->count_data_direct("purchase_cart_id","purchase_cart_master","user_id = '$user_id'");

        $response['cart_count'] = $cartCount;

        $isProcurement = $d->count_data_direct("manager_id","site_manager_master","site_manager_id = '$user_id' AND manager_type = '1'");

        if ($isProcurement > 0) {
            $response['is_procurement'] = true;
        }else{
            $response['is_procurement'] = false;
        }

        $isFinance = $d->count_data_direct("manager_id","site_manager_master","site_manager_id = '$user_id' AND manager_type = '2'");

        if ($isFinance > 0) {
            $response['is_finance'] = true;
        }else{
            $response['is_finance'] = false;
        }

        $isSiteManager = $d->count_data_direct("manager_id","site_manager_master","site_manager_id = '$user_id' AND manager_type = '0'");

        if ($isFinance > 0) {
            $response['is_site_manager'] = true;
        }else{
            $response['is_site_manager'] = false;
        }

        $response["message"]="Success";
        $response["status"]="200";
        echo json_encode($response);
    }else{
        $response["message"]="Wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>