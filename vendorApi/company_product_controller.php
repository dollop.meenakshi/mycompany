<?php
include_once '../residentApiNew/lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getProducts']=="getProducts" && $vendor_id != ''  && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        if ($product_sub_category_id !='0' && $product_category_id !='0') {
            $qry = $d->select("product_master,
                product_category_master,
                product_sub_category_master,
                unit_measurement_master,
                product_price_master,
                product_variant_master
                ","
                product_master.society_id = '$society_id' 
                AND product_master.product_status = '0' 
                AND product_master.product_delete = '0' 
                AND product_price_master.product_id = product_master.product_id 
                AND product_price_master.vendor_id = '$vendor_id'
                AND product_price_master.product_id = product_variant_master.product_id
                AND product_price_master.product_variant_id = product_variant_master.product_variant_id
                AND product_master.product_id = product_variant_master.product_id
                AND product_master.unit_measurement_id = unit_measurement_master.unit_measurement_id 
                AND product_master.product_category_id = product_category_master.product_category_id 
                AND product_master.product_sub_category_id = product_sub_category_master.product_sub_category_id
                AND product_master.product_category_id = '$product_category_id' 
                AND product_master.product_sub_category_id = '$product_sub_category_id'");
        }else if($product_category_id !='0' && $product_sub_category_id =='0'){
            $qry = $d->select("product_master,
                product_category_master,
                unit_measurement_master,
                product_price_master,
                product_variant_master
                ","
                product_master.society_id = '1' 
                AND product_master.product_status = '0' 
                AND product_master.product_delete = '0' 
                AND product_price_master.vendor_id = '$vendor_id' 
                AND product_price_master.product_id = product_master.product_id 
                AND product_price_master.product_id = product_variant_master.product_id 
                AND product_master.product_id = product_variant_master.product_id 
                AND product_price_master.product_variant_id = product_variant_master.product_variant_id 
                AND product_master.unit_measurement_id = unit_measurement_master.unit_measurement_id 
                AND product_master.product_category_id = product_category_master.product_category_id 
                AND product_master.product_category_id = '$product_category_id'");


        }else{
            $qry = $d->select("product_master,
                product_category_master,
                unit_measurement_master,
                product_price_master,
                product_variant_master
                ","
                product_master.society_id = '$society_id' 
                AND product_master.product_status = '0' 
                AND product_master.product_delete = '0' 
                AND product_price_master.product_id = product_master.product_id 
                AND product_price_master.vendor_id = '$vendor_id'
                AND product_price_master.product_id = product_variant_master.product_id
                AND product_price_master.product_variant_id = product_variant_master.product_variant_id
                AND product_master.product_id = product_variant_master.product_id
                AND product_master.unit_measurement_id = unit_measurement_master.unit_measurement_id 
                AND product_master.product_category_id = product_category_master.product_category_id");
        }

    
        if(mysqli_num_rows($qry)>0){
            
            $response["products"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $data = array_map("html_entity_decode", $data);

                $products = array();

                $products["product_id"] = $data['product_id'];
                $products["vendor_id"] = $data['vendor_id'];
                $products["product_code"] = $data['product_code'];
                $products["product_name"] = $data['product_name'];
                $products["product_alias_name"] = $data['product_alias_name'];
                $products["product_price_id"] = $data['product_price_id'];
                $products["product_price"] = $data['product_price'];
                $products["product_variant_id"] = $data['product_variant_id'];
                $products["product_variant_name"] = $data['product_variant_name'];
                $products["product_short_name"] = $data['product_short_name'];
                $products["product_category_id"] = $data['product_category_id'];
                $products["category_name"] = $data['category_name'];
                $products["product_sub_category_id"] = $data['product_sub_category_id'].'';
                $products["sub_category_name"] = $data['sub_category_name'].'';
                $products["product_brand"] = $data['product_brand'];
                $products["unit_measurement_id"] = $data['unit_measurement_id'];
                $products["unit_measurement_name"] = $data['unit_measurement_name'];

                if ($data['product_image'] != '') {
                    $products["product_image"] = $base_url.'img/product/'.$data['product_image'];
                }else{
                    $products["product_image"] = '';
                }

                $products["product_image_name"] = $data['product_image'];

                $products["other"] = $data['other'];
                $products["product_status"] = $data['product_status'];
                $products["minimum_order_quantity"] = $data['minimum_order_quantity'];
                $products["product_delete"] = $data['product_delete'];
                $products["product_created_date"] = date("d M Y, h:i A",strtotime($data['product_created_date']));
                     
                array_push($response["products"], $products);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['searchProducts']=="searchProducts"  && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        if ($product_sub_category_id !='0' && $product_category_id !='0') {
            $qry = $d->select("
                product_master,
                product_category_master,
                product_sub_category_master,
                unit_measurement_master
                ","
                product_master.society_id = '$society_id' 
                AND product_master.product_delete = '0' 
                AND product_master.product_status = '0' 
                AND product_master.unit_measurement_id = unit_measurement_master.unit_measurement_id 
                AND product_master.product_category_id = product_category_master.product_category_id 
                AND product_category_master.product_category_id = product_sub_category_master.product_category_id 
                AND product_master.product_sub_category_id = product_sub_category_master.product_sub_category_id
                AND product_master.product_category_id = '$product_category_id' AND product_master.product_sub_category_id = '$product_sub_category_id'");
            
        }else if($product_category_id !='0' && $product_sub_category_id =='0'){
            $qry = $d->select("
                product_master,
                product_category_master,
                unit_measurement_master
                ","
                product_master.society_id = '$society_id' 
                AND product_master.product_delete = '0' 
                AND product_master.product_status = '0' 
                AND product_master.unit_measurement_id = unit_measurement_master.unit_measurement_id 
                AND product_master.product_category_id = product_category_master.product_category_id 
                AND product_master.product_category_id = '$product_category_id'");
        }else{
            $qry = $d->select("
                product_master,
                product_category_master,
                unit_measurement_master,
                product_category_vendor_master
                ","
                product_master.society_id = '$society_id' 
                AND product_master.product_delete = '0' 
                AND product_master.product_status = '0' 
                AND product_master.unit_measurement_id = unit_measurement_master.unit_measurement_id 
                AND product_master.product_category_id = product_category_master.product_category_id 
                AND product_category_vendor_master.product_category_id = product_category_master.product_category_id 
                AND product_category_vendor_master.product_category_id = product_master.product_category_id 
                AND product_category_vendor_master.vendor_id = '$vendor_id'");
        }


        if(mysqli_num_rows($qry)>0){
            
            $response["products"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $data = array_map("html_entity_decode", $data);

                $products = array();

                $products["product_id"] = $data['product_id'];
                $products["vendor_id"] = $data['vendor_id'];
                $products["product_code"] = $data['product_code'];
                $products["product_name"] = $data['product_name'];
                $products["product_alias_name"] = $data['product_alias_name'];
                $products["product_short_name"] = $data['product_short_name'];
                $products["product_category_id"] = $data['product_category_id'];
                $products["category_name"] = $data['category_name'];
                $products["product_sub_category_id"] = $data['product_sub_category_id'];
                $products["sub_category_name"] = $data['sub_category_name'].'';
                $products["product_brand"] = $data['product_brand'];
                $products["unit_measurement_id"] = $data['unit_measurement_id'];
                $products["unit_measurement_name"] = $data['unit_measurement_name'];

                if ($data['product_image'] != '') {
                    $products["product_image"] = $base_url.'img/product/'.$data['product_image'];
                }else{
                    $products["product_image"] = '';
                }

                $products["product_image_name"] = $data['product_image'];

                $products["other"] = $data['other'];
                $products["product_status"] = $data['product_status'];
                $products["product_delete"] = $data['product_delete'];
                $products["product_created_date"] = date("d M Y, h:i A",strtotime($data['product_created_date']));
                     
                $products["product_variant_list"] = array();

                /*$variantQry = $d->selectRow("
                    product_price_master.product_price_id,
                    product_variant_master.product_variant_name,
                    product_price_master.product_price
                    ","
                    product_price_master,
                    product_variant_master
                    ","
                    product_price_master.product_id = '$data[product_id]' 
                    AND product_price_master.vendor_id = '$vendor_id'
                    AND product_variant_master.product_id = '$data[product_id]'
                    AND product_price_master.product_variant_id = product_variant_master.product_variant_id");*/

                $variantQry = $d->selectRow("
                    product_variant_master.product_id,
                    product_variant_master.product_variant_id,
                    product_variant_master.product_variant_name,
                    ppm.product_price_id,
                    ppm.product_price
                    ","
                    product_variant_master LEFT JOIN product_price_master AS ppm ON ppm.product_id = '$data[product_id]' 
                    AND ppm.product_variant_id = product_variant_master.product_variant_id 
                    AND ppm.vendor_id = '$vendor_id'
                    ","
                    product_variant_master.product_id = '$data[product_id]' AND product_variant_master.variant_delete_status = '0' AND product_variant_master.variant_active_status = '0'");

                if (mysqli_num_rows($variantQry) > 0) {
                    while($variantData = mysqli_fetch_array($variantQry)){
                        $varaintList = array();

                        $varaintList['product_variant_id'] = $variantData['product_variant_id'];
                        $varaintList['product_variant_name'] = $variantData['product_variant_name'];

                        if ($variantData['product_price_id'] != null) {
                            $varaintList['has_my_price'] = true;
                            $varaintList['product_price_id'] = $variantData['product_price_id'];
                            $varaintList['product_price'] = $variantData['product_price'];
                        }else{
                            $varaintList['has_my_price'] = false;
                            $varaintList['product_price_id'] = "";
                            $varaintList['product_price'] = "";
                        }
                        
                        array_push($products['product_variant_list'],$varaintList);
                    }
                }

                array_push($response["products"], $products);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['addProduct']=="addProduct" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $product_image = "";

        if ($_FILES["product_image"]["tmp_name"] != null) {
            // code...
            $file11=$_FILES["product_image"]["tmp_name"];
            $extId = pathinfo($_FILES['product_image']['name'], PATHINFO_EXTENSION);
            $extAllow=array("pdf","png","jpg","jpeg","PDF","JPEG","PNG","JPG");

            if(in_array($extId,$extAllow)) {
                $temp = explode(".", $_FILES["product_image"]["name"]);
                $product_image = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["product_image"]["tmp_name"], "../img/product/" . $product_image);
            } else {
            
                $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
        }

        $m->set_data('society_id',$society_id);
        $m->set_data('product_code',$product_code);
        $m->set_data('product_name',$product_name);
        $m->set_data('product_alias_name',$product_alias_name);
        $m->set_data('product_short_name',$product_short_name);
        $m->set_data('product_category_id',$product_category_id);
        $m->set_data('product_sub_category_id',$product_sub_category_id);
        $m->set_data('product_brand',$product_brand);
        $m->set_data('unit_measurement_id',$unit_measurement_id);
        $m->set_data('product_image',$product_image);
        $m->set_data('other',$other);
        $m->set_data('product_created_date',$dateTime);

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'product_code'=>$m->get_data('product_code'),
            'product_name'=>$m->get_data('product_name'),
            'product_alias_name'=>$m->get_data('product_alias_name'),
            'product_short_name'=>$m->get_data('product_short_name'),
            'product_category_id'=>$m->get_data('product_category_id'),
            'product_sub_category_id'=>$m->get_data('product_sub_category_id'),
            'product_brand'=>$m->get_data('product_brand'),
            'unit_measurement_id'=>$m->get_data('unit_measurement_id'),
            'product_image'=>$m->get_data('product_image'),
            'other'=>$m->get_data('other'),
            'product_created_date'=>$m->get_data('product_created_date'),
        );

        $qry = $d->insert("product_master",$a);

        $product_id = $con->insert_id;

        if ($qry == true) {

            $m->set_data('product_id',$product_id);
            $m->set_data('vendor_id',$vendor_id);
            $m->set_data('product_price',$product_price);
            $m->set_data('price_added_date',$dateTime);
            $m->set_data('minimum_order_quantity',$minimum_order_quantity);

            $a1 = array(
                'product_id'=>$m->get_data('product_id'),
                'vendor_id'=>$m->get_data('vendor_id'),
                'product_price'=>$m->get_data('product_price'),
                'price_added_date'=>$m->get_data('price_added_date'),
                'minimum_order_quantity'=>$m->get_data('minimum_order_quantity'),
            );

            $qryPrice = $d->insert("product_price_master",$a1);

            $response["message"]="New Product Added";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['addProductPrice']=="addProductPrice" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $q = $d->select("product_price_master","product_id = '$product_id' AND vendor_id = '$vendor_id' AND product_variant_id = '$product_variant_id'");

        if (mysqli_num_rows($q) > 0) {
            $response["message"]="Price already added";
            $response["status"]="201";
            echo json_encode($response);
            exit();
        }

        if ($otherVariant == "1") {
            
            $m->set_data('product_id',$product_id);
            $m->set_data('product_variant_name',$product_variant_name);
            $m->set_data('created_date',$dateTime);

            $a = array(
                'product_id'=>$m->get_data('product_id'),
                'product_variant_name'=>$m->get_data('product_variant_name'),
                'created_date'=>$m->get_data('created_date'),
            );

            $d->insert("product_variant_master",$a);

            $product_variant_id = $con->insert_id;
        }

        $m->set_data('product_id',$product_id);
        $m->set_data('vendor_id',$vendor_id);
        $m->set_data('product_price',$product_price);
        $m->set_data('product_variant_id',$product_variant_id);
        $m->set_data('price_added_date',$dateTime);
        $m->set_data('minimum_order_quantity',$minimum_order_quantity);

        $a1 = array(
            'product_id'=>$m->get_data('product_id'),
            'vendor_id'=>$m->get_data('vendor_id'),
            'product_price'=>$m->get_data('product_price'),
            'product_variant_id'=>$m->get_data('product_variant_id'),
            'price_added_date'=>$m->get_data('price_added_date'),
            'minimum_order_quantity'=>$m->get_data('minimum_order_quantity'),
        );

        $qryPrice = $d->insert("product_price_master",$a1);

        $product_price_id = $con->insert_id;

        $m->set_data('product_price_id',$product_price_id);

        $a2 = array(
            'product_price_id'=>$m->get_data('product_price_id'),
            'product_id'=>$m->get_data('product_id'),
            'vendor_id'=>$m->get_data('vendor_id'),
            'product_price'=>$m->get_data('product_price'),
            'product_variant_id'=>$m->get_data('product_variant_id'),
            'price_added_date'=>$m->get_data('price_added_date'),
            'minimum_order_quantity'=>$m->get_data('minimum_order_quantity'),
        );

        $qryPriceHis = $d->insert("product_price_history",$a2);

        if ($qryPrice == true) {

            $response["message"]="New Product Added";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updateProductPrice']=="updateProductPrice" && filter_var($society_id, FILTER_VALIDATE_INT) == true){


        $m->set_data('product_price',$product_price);
        $m->set_data('minimum_order_quantity',$minimum_order_quantity);

        $a1 = array(
            'product_price'=>$m->get_data('product_price'),
            'minimum_order_quantity'=>$m->get_data('minimum_order_quantity'),
        );

        $qryPrice = $d->update("product_price_master",$a1,"product_id = '$product_id' AND product_variant_id = '$product_variant_id' AND vendor_id = '$vendor_id' AND product_price_id = '$product_price_id'");

        if ($qryPrice == true) {

            $pData = $d->select("product_price_master","product_price_id = '$product_price_id'");

            $priceData = mysqli_fetch_array($pData);
        
            $a2 = array(
                'product_price_id'=>$priceData['product_price_id'],
                'product_id'=>$priceData['product_id'],
                'vendor_id'=>$priceData['vendor_id'],
                'product_price'=>$priceData['product_price'],
                'product_variant_id'=>$priceData['product_variant_id'],
                'price_added_date'=>$priceData['price_added_date'],
                'minimum_order_quantity'=>$priceData['minimum_order_quantity'],
            );

            $qryPriceHis = $d->insert("product_price_history",$a2);

            $response["message"]="Price Updated";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getPriceHistory']=="getPriceHistory" && $product_price_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $response['price_history'] = array();

        $priceHistoryQry = $d->select("product_price_history","product_price_id = '$product_price_id' AND vendor_id = '$vendor_id' AND product_id = '$product_id'","ORDER BY product_price_history_id DESC");

        if (mysqli_num_rows($priceHistoryQry) > 0) {

            while($priceData = mysqli_fetch_array($priceHistoryQry)){
                $price = array();

                $price['product_price_history_id'] = $priceData['product_price_history_id'];
                $price['product_price_id'] = $priceData['product_price_id'];
                $price['vendor_id'] = $priceData['vendor_id'];
                $price['product_id'] = $priceData['product_id'];
                $price['product_variant_id'] = $priceData['product_variant_id'];
                $price['product_price'] = $priceData['product_price'];
                $price['price_added_date'] = $priceData['price_added_date'];
                $price['minimum_order_quantity'] = $priceData['minimum_order_quantity'];
                
                array_push($response['price_history'],$price);
            }
            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
       
    }else if($_POST['deleteProductPrice']=="deleteProductPrice" && filter_var($society_id, FILTER_VALIDATE_INT) == true){


        $qryPrice = $d->delete("product_price_master","vendor_id = '$vendor_id' AND product_price_id = '$product_price_id'");

        if ($qryPrice == true) {

            $response["message"]="Product removed from your list";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updateProduct']=="updateProduct" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $product_image = "";

        if ($_FILES["product_image"]["tmp_name"] != null) {
            // code...
            $file11=$_FILES["product_image"]["tmp_name"];
            $extId = pathinfo($_FILES['product_image']['name'], PATHINFO_EXTENSION);
            $extAllow=array("pdf","png","jpg","jpeg","PDF","JPEG","PNG","JPG");

            if(in_array($extId,$extAllow)) {
                $temp = explode(".", $_FILES["product_image"]["name"]);
                $product_image = "User_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["product_image"]["tmp_name"], "../img/product/" . $product_image);
            } else {
            
                $response["message"] = "Invalid Document. Only JPG,PNG & PDF files are allowed.";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
        }

        if ($product_image == '') {
            $product_image = $product_image_name;
        }

        $m->set_data('product_code',$product_code);
        $m->set_data('product_name',$product_name);
        $m->set_data('product_base_price',$product_base_price);
        $m->set_data('product_alias_name',$product_alias_name);
        $m->set_data('product_short_name',$product_short_name);
        $m->set_data('product_category_id',$product_category_id);
        $m->set_data('product_sub_category_id',$product_sub_category_id);
        $m->set_data('product_brand',$product_brand);
        $m->set_data('minimum_order_quantity',$minimum_order_quantity);
        $m->set_data('unit_measurement_id',$unit_measurement_id);
        $m->set_data('product_image',$product_image);
        $m->set_data('other',$other);

        $a = array(
            'product_code'=>$m->get_data('product_code'),
            'product_name'=>$m->get_data('product_name'),
            'product_base_price'=>$m->get_data('product_base_price'),
            'product_alias_name'=>$m->get_data('product_alias_name'),
            'product_short_name'=>$m->get_data('product_short_name'),
            'product_category_id'=>$m->get_data('product_category_id'),
            'product_sub_category_id'=>$m->get_data('product_sub_category_id'),
            'product_brand'=>$m->get_data('product_brand'),
            'minimum_order_quantity'=>$m->get_data('minimum_order_quantity'),
            'unit_measurement_id'=>$m->get_data('unit_measurement_id'),
            'product_image'=>$m->get_data('product_image'),
            'other'=>$m->get_data('other'),
        );

        $qry = $d->update("product_master",$a,"product_id = '$product_id' AND vendor_id = '$vendor_id'");

        if ($qry == true) {
            $response["message"]="Product Updated";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['deleteProduct']=="deleteProduct" && $product_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('product_delete',"1");

        $a = array(
            'product_delete'=>$m->get_data('product_delete'),
        );

        $qry = $d->update("product_master",$a,"product_id = '$product_id' AND vendor_id = '$vendor_id'");

        $msg = "Product Deleted";
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['activeDeactiveProduct']=="activeDeactiveProduct" && $product_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('product_status',$product_status);

        $a = array(
            'product_status'=>$m->get_data('product_status'),
        );

        $qry = $d->update("product_master",$a,"product_id = '$product_id' AND vendor_id = '$vendor_id'");

        if ($product_status == 0) {
            $msg = "Product Activated";
        }else{
            $msg = "Product Deactivated";
        }
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>