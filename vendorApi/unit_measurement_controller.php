<?php
include_once '../residentApiNew/lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getUnitMeasurement']=="getUnitMeasurement"  && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("unit_measurement_master","society_id = '$society_id' AND unit_measurement_delete = '0' AND unit_measurement_status = '0'");

        if(mysqli_num_rows($qry)>0){
            
            $response["measurements"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $data = array_map("html_entity_decode", $data);

                $measurements = array();

                $measurements["unit_measurement_id"] = $data['unit_measurement_id'];
                $measurements["unit_measurement_name"] = $data['unit_measurement_name'];
                $measurements["unit_measurement_description"] = $data['unit_measurement_description'];
                $measurements["unit_measurement_status"] = $data['unit_measurement_status'];
                $measurements["unit_measurement_delete"] = $data['unit_measurement_delete'];
                     
                array_push($response["measurements"], $measurements);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['addMeasurement']=="addMeasurement" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('society_id',$society_id);
        $m->set_data('vendor_id',$vendor_id);
        $m->set_data('unit_measurement_name',$unit_measurement_name);
        $m->set_data('unit_measurement_description',$unit_measurement_description);
        $m->set_data('unit_measurement_created_date',$dateTime);

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'vendor_id'=>$m->get_data('vendor_id'),
            'unit_measurement_name'=>$m->get_data('unit_measurement_name'),
            'unit_measurement_description'=>$m->get_data('unit_measurement_description'),
            'unit_measurement_created_date'=>$m->get_data('unit_measurement_created_date'),
        );

        $qry = $d->insert("unit_measurement_master",$a);

        if ($qry == true) {
            $response["message"]="New Unit Measurement Added";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updateUnitMeasurement']=="updateUnitMeasurement" && $unit_measurement_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('unit_measurement_name',$unit_measurement_name);
        $m->set_data('unit_measurement_description',$unit_measurement_description);

        $a = array(
            'unit_measurement_name'=>$m->get_data('unit_measurement_name'),
            'unit_measurement_description'=>$m->get_data('unit_measurement_description'),
        );

        $qry = $d->update("unit_measurement_master",$a,"unit_measurement_id = '$unit_measurement_id' AND vendor_id = '$vendor_id'");

        if ($qry == true) {
            $response["message"]="Unit Measurement Updated";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['deleteUnitMeasurement']=="deleteUnitMeasurement" && $unit_measurement_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('unit_measurement_delete',"1");

        $a = array(
            'unit_measurement_delete'=>$m->get_data('unit_measurement_delete'),
        );

        $qry = $d->update("unit_measurement_master",$a,"unit_measurement_id = '$unit_measurement_id' AND vendor_id = '$vendor_id'");

        $msg = "Unit Measurement Deleted";
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['activeDeactiveUnitMeasurement']=="activeDeactiveUnitMeasurement" && $unit_measurement_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('unit_measurement_status',$unit_measurement_status);

        $a = array(
            'unit_measurement_status'=>$m->get_data('unit_measurement_status'),
        );

        $qry = $d->update("unit_measurement_master",$a,"unit_measurement_id = '$unit_measurement_id' AND vendor_id = '$vendor_id'");

        if ($unit_measurement_status == 0) {
            $msg = "Measurement Activated";
        }else{
            $msg = "Measurement Deactivated";
        }
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>