<?php

include_once '../residentApiNew/lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getPurchaseFinance']=="getPurchaseFinance" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("
            purchase_master,
            site_master,
            site_manager_master,
            users_master,
            floors_master,
            block_master
            ","
            purchase_master.society_id='$society_id' 
            AND purchase_master.purchase_stage = '2' 
            AND site_master.site_id = purchase_master.site_id 
            AND site_master.site_id = site_manager_master.site_id 
            AND site_manager_master.site_manager_id = '$user_id' 
            AND site_manager_master.manager_type = '2' 
            AND users_master.user_id = purchase_master.user_id 
            AND users_master.block_id = block_master.block_id 
            AND users_master.floor_id = floors_master.floor_id","ORDER BY purchase_master.purchase_id DESC");


        if(mysqli_num_rows($qry)>0){
            
            $response["procurement_orders"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $purchase_orders = array();

                $purchase_orders["purchase_id"] = $data['purchase_id'];
                $purchase_orders["site_id"] = $data['site_id'];
                $purchase_orders["site_name"] = $data['site_name'];
                $purchase_orders["site_address"] = $data['site_address'];
                $purchase_orders["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
                $purchase_orders["po_number"] = $data['po_number'];
                $purchase_orders["user_id"] = $data['user_id'];
                $purchase_orders["user_full_name"] = $data['user_full_name'];
                $purchase_orders["user_designation"] = $data['user_designation'];
                $purchase_orders["floor_id"] = $data['floor_id'];
                $purchase_orders["department_name"] = $data['floor_name'];
                $purchase_orders["block_id"] = $data['block_id'];
                $purchase_orders["branch_name"] = $data['block_name'];
                
                if ($data['user_profile_pic'] != '') {
                    $purchase_orders["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                } else {
                    $purchase_orders["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }

                if($data['purchase_status'] == '0'){ 
                    $status = "Pending";
                } else if($data['purchase_status'] == '1'){
                    $status = "Accepted";
                }else{
                    $status = "Rejected";
                }

                $totalItemCount = $d->count_data_direct("puchase_product_id","purchase_product_master","purchase_id = '$data[purchase_id]'");

                $purchase_orders["purchase_status"] = $data['purchase_status'];
                $purchase_orders["quantity"] = $totalItemCount.'';
                $purchase_orders["purchase_status_view"] = $status;

                     
                array_push($response["procurement_orders"], $purchase_orders);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getPurchaseDetailsFinance']=="getPurchaseDetailsFinance" && filter_var($society_id, FILTER_VALIDATE_INT) == true){


        $qry = $d->select("purchase_master,site_master,site_manager_master","site_master.site_id=purchase_master.site_id AND purchase_master.society_id='$society_id'  AND purchase_master.purchase_id = '$purchase_id' AND site_manager_master.site_id = site_master.site_id AND site_manager_master.manager_type = '2' AND site_manager_master.site_manager_id = '$user_id'");

        if(mysqli_num_rows($qry)>0){

            $data = mysqli_fetch_array($qry);

            $data = array_map("html_entity_decode", $data);

            $response["purchase_id"] = $data['purchase_id'];
            $response["puchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
            $response["po_number"] = $data['po_number'];
            $response["site_id"] = $data['site_id'];
            $response["site_name"] = $data['site_name'];
            $response["site_address"] = $data['site_address'];
            $response["purchase_descripion"] = $data['purchase_descripion'];
           
            if ($data['purchase_invoice'] != '') {
                $response["purchase_invoice"] =  $base_url."img/purchase_invoice/".  $data['purchase_invoice'];
            }else{
                $response["purchase_invoice"] =  "";
            }

            if($data['purchase_status'] == '0'){ 
                $status = "Pending";
            } else if($data['purchase_status'] == '1'){
                $status = "Accepted";
            }else{
                $status = "Rejected";
            }

            $response["purchase_status"] = $data['purchase_status'];
            $response["purchase_status_view"] = $status;

        }

        $response['products'] = array();

        $q = $d->selectRow("
            purchase_product_master.*,
            product_master.*,
            product_category_master.product_category_id,
            product_category_master.category_name,
            product_variant_master.product_variant_id,
            product_variant_master.product_variant_name,
            unit_measurement_master.unit_measurement_name,
            unit_measurement_master.unit_measurement_id,
            product_price_master.product_price_id,
            product_price_master.minimum_order_quantity
            ","
            purchase_product_master,
            product_master,
            product_category_master,
            product_variant_master,
            unit_measurement_master,
            product_price_master
            ","
            purchase_product_master.product_id=product_master.product_id 
            AND product_master.product_id=product_variant_master.product_id 
            AND purchase_product_master.product_variant_id=product_variant_master.product_variant_id 
            AND product_master.product_category_id=product_category_master.product_category_id 
            AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id 
            AND purchase_product_master.purchase_id='$purchase_id' 
            AND product_price_master.product_variant_id = purchase_product_master.product_variant_id
            AND product_price_master.product_id = purchase_product_master.product_id
            AND product_price_master.product_price_id = purchase_product_master.product_price_id","");


        if (mysqli_num_rows($q) > 0) {

            while($purchaseData = mysqli_fetch_array($q)){

                $purchaseData = array_map("html_entity_decode", $purchaseData);

                $products = array();

                $products['purchase_product_id'] = $purchaseData['puchase_product_id'];
                $products['product_id'] = $purchaseData['product_id'];
                $products['purchase_id'] = $purchaseData['purchase_id'];
                $products['product_code'] = $purchaseData['product_code'];
                $products['product_name'] = $purchaseData['product_name'];
                $products['category_name'] = $purchaseData['category_name'];
                $products['sub_category_name'] = $purchaseData['sub_category_name'].'';
                $products['unit_measurement_name'] = $purchaseData['unit_measurement_name'];
                $products["product_brand"] = $purchaseData['product_brand'];
                $products["other"] = $purchaseData['other'];
                $products['product_variant_id'] = $purchaseData['product_variant_id'];
                $products['product_variant_name'] = $purchaseData['product_variant_name'];
                $products['minimum_order_quantity'] = $purchaseData['minimum_order_quantity'];
                $products['product_price_id'] = $purchaseData['product_price_id'];

                $products['quantity'] = $purchaseData['quantity'];
                $products['availability_status'] = $purchaseData['availablity_status'];

                array_push($response['products'],$products);
            }
        }

        $response["message"]="Success";
        $response["status"]="200";
        echo json_encode($response);
    }else if($_POST['getProductVendors']=="getProductVendors" && filter_var($society_id, FILTER_VALIDATE_INT) == true){


        $q = $d->selectRow("
            purchase_product_master.*,
            local_service_provider_users.service_provider_users_id,
            local_service_provider_users.service_provider_name,
            local_service_provider_users.contact_person_name,
            local_service_provider_users.service_provider_address,
            local_service_provider_users.service_provider_phone,
            product_price_master.product_price_id,
            product_price_master.vendor_id,
            product_price_master.product_price,
            product_price_master.minimum_order_quantity,
            product_variant_master.product_variant_id,
            product_variant_master.product_variant_name
            ","
            purchase_product_master,
            product_price_master,
            product_variant_master,
            local_service_provider_users
            ","
            product_price_master.product_id = '$product_id' 
            AND purchase_product_master.purchase_id = '$purchase_id' 
            AND purchase_product_master.puchase_product_id = '$puchase_product_id' 
            AND purchase_product_master.product_variant_id = '$product_variant_id'
            AND product_price_master.product_id = purchase_product_master.product_id 
            AND product_price_master.product_variant_id = product_variant_master.product_variant_id 
            AND product_price_master.product_variant_id = purchase_product_master.product_variant_id 
            AND product_price_master.vendor_id = local_service_provider_users.service_provider_users_id 
            AND product_variant_master.product_id = purchase_product_master.product_id
            ");


        if (mysqli_num_rows($q) > 0) {

            $response['product_vendors'] = array();

            while($purchaseData = mysqli_fetch_array($q)){

                $purchaseData = array_map("html_entity_decode", $purchaseData);

                $vendors = array();

                $vendors['vendor_id'] = $purchaseData['service_provider_users_id'];
                $vendors['service_provider_name'] = $purchaseData['service_provider_name'];
                $vendors['contact_person_name'] = $purchaseData['contact_person_name'];
                $vendors['service_provider_address'] = $purchaseData['service_provider_address'];
                $vendors['service_provider_phone'] = $purchaseData['service_provider_phone'];
                $vendors['product_price'] = $purchaseData['product_price'];
                $vendors['minimum_order_quantity'] = $purchaseData['minimum_order_quantity'];

                array_push($response['product_vendors'],$vendors);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No vendors found for this product";
            $response["status"]="201";
            echo json_encode($response);
        }

        
    }else if($_POST['deliverToVendor']=="deliverToVendor" && $purchase_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('purchase_stage',"3");
        $m->set_data('finance_by_id',$user_id);

        $a = array(
            'purchase_stage'=>$m->get_data('purchase_stage'),
            'finance_by_id'=>$m->get_data('finance_by_id'),
        );

        $qry = $d->update("purchase_master",$a,"purchase_id = '$purchase_id'");

        if ($qry == true) {

            $response['vendors'] = array();

            $vendor_ids = explode(",",$vendor_ids);
            $product_ids = explode(",",$product_ids);
            $variant_ids = explode(",",$variant_ids);

            $vendorTempAry = array_unique($vendor_ids);
            
            for ($i=0; $i < count($vendorTempAry); $i++) { 
                $qry = $d->select("purchase_master,site_master","purchase_master.purchase_id = '$purchase_id' AND purchase_master.site_id = site_master.site_id");

                if(mysqli_num_rows($qry)>0){

                    $data = mysqli_fetch_array($qry);

                    $data = array_map("html_entity_decode", $data);

                    $vendor_id = $vendorTempAry[$i];

                    $m->set_data('purchase_id',$data['purchase_id']);
                    $m->set_data('society_id',$data['society_id']);
                    $m->set_data('user_id',$data['user_id']);
                    $m->set_data('purchase_date',$data['purchase_date']);
                    $m->set_data('site_id',$data['site_id']);
                    $m->set_data('vendor_id',$vendor_id);
                    $m->set_data('purchase_descripion',$data['purchase_descripion']);
                    $m->set_data('po_number',$po_no);
                    $m->set_data('expected_date',$data['expected_date']);
                    $m->set_data('created_by',$data['finance_by_id']);
                    $m->set_data('created_date',$dateTime);

                    $purchaseAry = array(
                        'purchase_id'=>$m->get_data('purchase_id'),
                        'society_id'=>$m->get_data('society_id'),
                        'user_id'=>$m->get_data('user_id'),
                        'purchase_date'=>$m->get_data('purchase_date'),
                        'site_id'=>$m->get_data('site_id'),
                        'vendor_id'=>$m->get_data('vendor_id'),
                        'purchase_descripion'=>$m->get_data('purchase_descripion'),
                        'po_number'=>$m->get_data('po_number'),
                        'expected_date'=>$m->get_data('expected_date'),
                        'created_by'=>$m->get_data('created_by'),
                        'created_date'=>$m->get_data('created_date'),
                    );

                    $qryVendorOrder = $d->insert("purchase_vendor_order_master",$purchaseAry);

                    $vendor_order_id = $con->insert_id;

                    if ($qryVendorOrder == true) {

                        $title = "New Order";
                        $description = "From Site: ".$data['site_name'];

                        $fcmAccessAry=$d->get_android_fcm_sp("local_service_provider_users","service_provider_delete_status=0 AND token!='' AND society_id='$society_id' AND sp_device='android' AND service_provider_users_id = '$vendor_id'");
                        $fcmAccessAryiOS=$d->get_android_fcm_sp("local_service_provider_users","service_provider_delete_status=0 AND token!='' AND society_id='$society_id' AND sp_device='ios' AND service_provider_users_id = '$vendor_id'");

                        $nResident->noti("new_order","",$society_id,$fcmAccessAry,$title,$description,"");
                        $nResident->noti_ios("new_order","",$society_id,$fcmAccessAryiOS,$title,$description,"");

                        for ($j=0; $j < count($vendor_ids); $j++) { 
                            
                            if ($vendor_ids[$j] == $vendor_id) {


                                $productId = $product_ids[$j];
                                $variantId = $variant_ids[$j];
                               
                                
                                $q = $d->selectRow("
                                    purchase_product_master.*,
                                    product_master.*,
                                    product_category_master.product_category_id,
                                    product_category_master.category_name,
                                    unit_measurement_master.unit_measurement_name,
                                    purchase_product_master.product_price_id,
                                    product_price_master.product_price_id,
                                    unit_measurement_master.unit_measurement_id
                                    ","
                                    purchase_product_master,
                                    product_master,
                                    product_category_master,
                                    product_price_master,
                                    unit_measurement_master
                                    ","purchase_product_master.product_id=product_master.product_id 
                                    AND product_master.product_category_id=product_category_master.product_category_id 
                                    AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id 
                                    AND purchase_product_master.purchase_id='$purchase_id'
                                    AND purchase_product_master.product_id = '$productId'
                                    AND purchase_product_master.product_variant_id = '$variantId'
                                    AND product_price_master.product_variant_id = purchase_product_master.product_variant_id
                                    AND product_price_master.product_id = purchase_product_master.product_id 
                                    AND product_price_master.product_price_id = purchase_product_master.product_price_id");

                                if (mysqli_num_rows($q) > 0) {

                                    while($purchaseData = mysqli_fetch_array($q)){

                                        $purchaseData = array_map("html_entity_decode", $purchaseData);

                                        $priceQry = $d->selectRow("product_price,product_price_id","product_price_master","vendor_id = '$vendor_id' AND product_id = '$productId' AND product_variant_id = '$variantId'");

                                        $priceData = mysqli_fetch_array($priceQry);

                                        $m->set_data('society_id',$purchaseData['society_id']);
                                        $m->set_data('vendor_order_id',$vendor_order_id);
                                        $m->set_data('product_id',$purchaseData['product_id']);
                                        $m->set_data('product_variant_id',$purchaseData['product_variant_id']);
                                        $m->set_data('quantity',$purchaseData['quantity']);
                                        $m->set_data('product_price_id_temp',$purchaseData['product_price_id']);
                                        $m->set_data('price',$priceData['product_price']);
                                        $m->set_data('availability_status',$purchaseData['availability_status']);

                                        $productAry = array(
                                            'society_id'=>$m->get_data('society_id'),
                                            'vendor_order_id'=>$m->get_data('vendor_order_id'),
                                            'product_id'=>$m->get_data('product_id'),
                                            'product_variant_id'=>$m->get_data('product_variant_id'),
                                            'quantity'=>$m->get_data('quantity'),
                                            'product_price_id_temp'=>$m->get_data('product_price_id_temp'),
                                            'price'=>$m->get_data('price'),
                                            'availability_status'=>$m->get_data('availability_status'),
                                        );

                                        $qryVendorProductOrder = $d->insert("purchase_vendor_order_product_master",$productAry);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $response["message"]="Order Transfered to Finance Team";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getConfirmFinance']=="getConfirmFinance" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("
            purchase_vendor_order_master,
            site_master,
            local_service_provider_users
            ","
            purchase_vendor_order_master.society_id='$society_id' 
            AND site_master.site_id = purchase_vendor_order_master.site_id
            AND purchase_vendor_order_master.created_by = '$user_id' AND local_service_provider_users.service_provider_users_id = purchase_vendor_order_master.vendor_id","ORDER BY purchase_vendor_order_master.vendor_order_id DESC");


        if(mysqli_num_rows($qry)>0){
            
            $response["confirm_orders"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $confirmOrder = array();

                $confirmOrder["vendor_order_id"] = $data['vendor_order_id'];
                $confirmOrder["site_id"] = $data['site_id'];
                $confirmOrder["site_name"] = $data['site_name'];
                $confirmOrder["site_address"] = $data['site_address'];
                $confirmOrder["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
                $confirmOrder["po_number"] = $data['po_number'];
                $confirmOrder["service_provider_name"] = $data['service_provider_name'];
                $confirmOrder["contact_person_name"] = $data['contact_person_name'];
                $confirmOrder["service_provider_address"] = $data['service_provider_address'];
                $confirmOrder["service_provider_phone"] = $data['service_provider_phone'];
                $confirmOrder["service_provider_email"] = $data['service_provider_email'];
                $confirmOrder["created_by"] = $data['created_by'];

                if ($data['purchase_invoice'] != '') {
                    $confirmOrder["purchase_invoice"] =  $base_url."img/purchase_invoice/".  $data['purchase_invoice'];
                }else{
                    $confirmOrder["purchase_invoice"] =  "";
                }

                if($data['purchase_status'] == '0'){ 
                    $status = "Pending";
                } else if($data['purchase_status'] == '1'){
                    $status = "Accepted";
                }else{
                    $status = "Rejected";
                }

                $totalItemCount = $d->count_data_direct("vendor_order_product_id","purchase_vendor_order_product_master","vendor_order_id = '$data[vendor_order_id]'");

                $confirmOrder["purchase_status"] = $data['purchase_status'];
                $confirmOrder["purchase_reject_reason"] = $data['purchase_reject_reason'].'';
                $confirmOrder["quantity"] = $totalItemCount.'';
                $confirmOrder["purchase_status_view"] = $status;

                     
                array_push($response["confirm_orders"], $confirmOrder);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getConfirmFinanceDetails']=="getConfirmFinanceDetails" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $response['confirm_products'] = array();

        $qry = $d->select("
            purchase_vendor_order_master,
            site_master
            ","
            site_master.site_id=purchase_vendor_order_master.site_id 
            AND purchase_vendor_order_master.society_id='$society_id' 
            AND purchase_vendor_order_master.vendor_order_id = '$vendor_order_id' 
            AND purchase_vendor_order_master.created_by = '$user_id'");

        if(mysqli_num_rows($qry)>0){

            $data = mysqli_fetch_array($qry);

            $data = array_map("html_entity_decode", $data);

            $response["vendor_order_id"] = $data['vendor_order_id'];
            $response["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
            $response["po_number"] = $data['po_number'];
            $response["site_id"] = $data['site_id'];
            $response["site_name"] = $data['site_name'];
            $response["site_address"] = $data['site_address'];
            $response["purchase_descripion"] = $data['purchase_descripion'];
            $response["created_by"] = $data['created_by'];
           
            if ($data['purchase_invoice'] != '') {
                $response["purchase_invoice"] =  $base_url."img/purchase_invoice/".  $data['purchase_invoice'];
            }else{
                $response["purchase_invoice"] =  "";
            }

            if($data['purchase_status'] == '0'){ 
                $status = "Pending";
            } else if($data['purchase_status'] == '1'){
                $status = "Accepted";
            }else{
                $status = "Rejected";
            }

            $totalItemCount = $d->count_data_direct("vendor_order_id","purchase_vendor_order_product_master","vendor_order_id = '$data[vendor_order_id]'");

            $purchase_orders["quntity"] = $totalItemCount.'';

            $response["purchase_status"] = $data['purchase_status'];
            $response["purchase_status_view"] = $status;

            $q = $d->selectRow("
                purchase_vendor_order_product_master.*,
                product_master.*,
                product_category_master.product_category_id,
                product_category_master.category_name,
                unit_measurement_master.unit_measurement_name,
                unit_measurement_master.unit_measurement_id,
                product_variant_master.product_variant_name,
                product_variant_master.product_variant_id,
                product_price_master.product_price_id
                ","
                purchase_vendor_order_product_master,
                product_master,
                product_category_master,
                unit_measurement_master,
                product_variant_master,
                product_price_master
                ","
                purchase_vendor_order_product_master.product_id=product_master.product_id 
                AND product_master.product_category_id=product_category_master.product_category_id 
                AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id 
                AND purchase_vendor_order_product_master.vendor_order_id='$data[vendor_order_id]' 
                AND purchase_vendor_order_product_master.product_variant_id = product_variant_master.product_variant_id 
                AND product_price_master.product_variant_id = purchase_vendor_order_product_master.product_variant_id
                AND product_price_master.product_id = purchase_vendor_order_product_master.product_id
                AND product_price_master.product_price_id = purchase_vendor_order_product_master.product_price_id_temp");


            if (mysqli_num_rows($q) > 0) {

                while($purchaseData = mysqli_fetch_array($q)){

                    $purchaseData = array_map("html_entity_decode", $purchaseData);

                    $products = array();

                    $products['vendor_order_product_id'] = $purchaseData['vendor_order_product_id'];
                    $products['product_id'] = $purchaseData['product_id'];
                    $products['product_variant_id'] = $purchaseData['product_variant_id'];
                    $products['product_variant_name'] = $purchaseData['product_variant_name'];
                    $products['product_code'] = $purchaseData['product_code'];
                    $products['product_name'] = $purchaseData['product_name'];
                    $products['product_brand'] = $purchaseData['product_brand'];
                    $products['other'] = $purchaseData['other'];
                    $products['category_name'] = $purchaseData['category_name'];
                    $products['sub_category_name'] = $purchaseData['sub_category_name'];
                    $products['unit_measurement_name'] = $purchaseData['unit_measurement_name'];
                    $products['price'] = $purchaseData['price'];

                    $totalPrice = $purchaseData['price'] * $purchaseData['quantity'];

                    $totalPrice = number_format($totalPrice,2,'.','');

                    $products['total_price'] = $totalPrice."";
                    $products['quantity'] = $purchaseData['quantity'];
                    $products['availability_status'] = $purchaseData['availability_status'];
                    $products['vendor_id'] = $purchaseData['vendor_id'];

                    array_push($response['confirm_products'],$products);
                }
            }

        }

        $response["message"]="Success";
        $response["status"]="200";
        echo json_encode($response);
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>