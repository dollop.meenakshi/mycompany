<?php

include_once '../residentApiNew/lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getOrders']=="getOrders" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("
            purchase_vendor_order_master,
            site_master
            ","
            purchase_vendor_order_master.society_id='$society_id' 
            AND site_master.site_id = purchase_vendor_order_master.site_id
            AND purchase_vendor_order_master.vendor_id = '$vendor_id'","ORDER BY purchase_vendor_order_master.vendor_order_id DESC");


        if(mysqli_num_rows($qry)>0){
            
            $response["orders"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $purchase_orders = array();

                $purchase_orders["vendor_order_id"] = $data['vendor_order_id'];
                $purchase_orders["site_id"] = $data['site_id'];
                $purchase_orders["site_name"] = $data['site_name'];
                $purchase_orders["site_address"] = $data['site_address'];
                $purchase_orders["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
                $purchase_orders["po_number"] = $data['po_number'];
                $purchase_orders["created_by"] = $data['created_by'];
                $purchase_orders["user_id"] = $data['user_id'];

                if($data['purchase_status'] == '0'){ 
                    $status = "Pending";
                } else if($data['purchase_status'] == '1'){
                    $status = "Accepted";
                }else{
                    $status = "Rejected";
                }

                $totalItemCount = $d->count_data_direct("vendor_order_product_id","purchase_vendor_order_product_master","vendor_order_id = '$data[vendor_order_id]'");

                $purchase_orders["purchase_status"] = $data['purchase_status'];
                $purchase_orders["quantity"] = $totalItemCount.'';
                $purchase_orders["purchase_status_view"] = $status;

                $purchase_orders['delivery_message'] = "Delivery Pending";

                $isDelivered = $d->count_data_direct("vendor_order_product_id","purchase_vendor_order_product_master","vendor_order_id = '$data[vendor_order_id]' AND total_received_product = quantity");

                if ($isPartialDelivery == $totalItemCount) {
                    $purchase_orders['is_delivered'] = true;
                    $purchase_orders['delivery_message'] = "Delivered";
                }else{
                    $purchase_orders['is_delivered'] = false;                
                }

                $isDeliveryPending = $d->count_data_direct("vendor_order_product_id","purchase_vendor_order_product_master","vendor_order_id = '$data[vendor_order_id]' AND total_received_product = 0");

                if ($isPartialDelivery == $totalItemCount) {
                    $purchase_orders['is_delivery_pending'] = true;
                    $purchase_orders['delivery_message'] = "Delivery Pending";
                }else{                
                    $purchase_orders['is_delivery_pending'] = false;
                }

                $isPartiallyDelivered = $d->count_data_direct("vendor_order_product_id","purchase_vendor_order_product_master","vendor_order_id = '$data[vendor_order_id]' AND total_received_product > 0 ");

                if ($isPartiallyDelivered > 0) {
                    $purchase_orders['is_partially_delivered'] = true;
                    $purchase_orders['delivery_message'] = "Partially Delivered";
                }else{                
                    $purchase_orders['is_partially_delivered'] = false;
                }

                     
                array_push($response["orders"], $purchase_orders);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getOrderDetails']=="getOrderDetails" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $response['products'] = array();

        $qry = $d->select("purchase_vendor_order_master,site_master","site_master.site_id=purchase_vendor_order_master.site_id AND purchase_vendor_order_master.society_id='$society_id' AND purchase_vendor_order_master.vendor_order_id = '$vendor_order_id' AND purchase_vendor_order_master.vendor_id = '$vendor_id'");

        if(mysqli_num_rows($qry)>0){

            $data = mysqli_fetch_array($qry);

            $data = array_map("html_entity_decode", $data);

            $response["vendor_order_id"] = $data['vendor_order_id'];
            $response["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
            $response["po_number"] = $data['po_number'];
            $response["site_id"] = $data['site_id'];
            $response["site_name"] = $data['site_name'];
            $response["site_address"] = $data['site_address'];
            $response["purchase_descripion"] = $data['purchase_descripion'];
            $response["created_by"] = $data['created_by'];
            $response["user_id"] = $data['user_id'];
           
            if ($data['purchase_invoice'] != '') {
                $response["purchase_invoice"] =  $base_url."img/purchase_invoice/".  $data['purchase_invoice'];
            }else{
                $response["purchase_invoice"] =  "";
            }

            if($data['purchase_status'] == '0'){ 
                $status = "Pending";
            } else if($data['purchase_status'] == '1'){
                $status = "Accepted";
            }else{
                $status = "Rejected";
            }

            $response["purchase_status"] = $data['purchase_status'];
            $response["purchase_status_view"] = $status;

            $q = $d->selectRow("
                purchase_vendor_order_product_master.*,
                product_master.*,
                product_category_master.product_category_id,
                product_category_master.category_name,
                unit_measurement_master.unit_measurement_name,
                unit_measurement_master.unit_measurement_id
                ","
                purchase_vendor_order_product_master,
                product_master,
                product_category_master,
                unit_measurement_master
                ","
                purchase_vendor_order_product_master.product_id=product_master.product_id 
                AND product_master.product_category_id=product_category_master.product_category_id 
                AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id 
                AND purchase_vendor_order_product_master.vendor_order_id='$data[vendor_order_id]'");


            if (mysqli_num_rows($q) > 0) {

                while($purchaseData = mysqli_fetch_array($q)){

                    $purchaseData = array_map("html_entity_decode", $purchaseData);

                    $products = array();

                    $products['vendor_order_product_id'] = $purchaseData['vendor_order_product_id'];
                    $products['product_id'] = $purchaseData['product_id'];
                    $products['product_code'] = $purchaseData['product_code'];
                    $products['product_name'] = $purchaseData['product_name'];
                    $products['category_name'] = $purchaseData['category_name'];
                    $products['sub_category_name'] = $purchaseData['sub_category_name']."";
                    $products['unit_measurement_name'] = $purchaseData['unit_measurement_name'];
                    $products['price'] = $purchaseData['price'];

                    $products['reason_of_return_product'] = $purchaseData['reason_of_return_product'];
                    $products['total_received_product'] = $purchaseData['total_received_product'];
                    $products['total_returned_products'] = $purchaseData['total_returned_products'];
                    $products['remaining_product_to_be_received'] = $purchaseData['quantity'] - $purchaseData['total_received_product'].'';
                    $products['remaining_product'] = $purchaseData['quantity'] - ($purchaseData['total_received_product'] + $purchaseData['total_returned_products']);

                    if ($purchaseData['total_received_product'] == $purchaseData['quantity']) {
                        $products['all_received'] = true;
                    }else{
                        $products['all_received'] = false;
                    }

                    $totalPrice = $purchaseData['price'] * $purchaseData['quantity'];

                    $totalPrice = number_format($totalPrice,2,'.','');

                    $products['total_price'] = $totalPrice."";
                    $products['quantity'] = $purchaseData['quantity'];
                    $products['availability_status'] = $purchaseData['availability_status'];
                    $products['vendor_id'] = $data['vendor_id'];

                    array_push($response['products'],$products);
                }
            }
        }

        $response["message"]="Success";
        $response["status"]="200";
        echo json_encode($response);
    }else if($_POST['sendPurchaseInvoice']=="sendPurchaseInvoice" && $vendor_order_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $purchase_invoice = "";

        if ($_FILES["purchase_invoice"]["tmp_name"] != null) {
            // code...
            $file11=$_FILES["purchase_invoice"]["tmp_name"];
            $extId = pathinfo($_FILES['purchase_invoice']['name'], PATHINFO_EXTENSION);
            $extAllow=array("pdf","doc","docx","png","jpg","jpeg","JPEG","PNG","JPG","PDF");

            if(in_array($extId,$extAllow)) {
                $temp = explode(".", $_FILES["purchase_invoice"]["name"]);
                $purchase_invoice = "p_invoice_".$user_id.round(microtime(true)) . '.' . end($temp);
                move_uploaded_file($_FILES["purchase_invoice"]["tmp_name"], "../img/purchase_invoice/" . $purchase_invoice);
            } else {
            
                $response["message"] = "Invalid Document. Only JPG,PNG, JPEG, Doc & PDF files are allowed.";
                $response["status"] = "201";
                echo json_encode($response);
                exit();
            }
        }

        $m->set_data('purchase_invoice',$purchase_invoice);

        $a = array(
            'purchase_invoice'=>$m->get_data('purchase_invoice'),
        );

        $qry = $d->update("purchase_vendor_order_master",$a,"vendor_order_id = '$vendor_order_id'");

        if ($qry == true) {

            $title = "Invoice Added";
            $description ="For Site: ".$site_name."\nBy, ".$user_name;

            $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$financeById'");
            $data_notification = mysqli_fetch_array($qsm);
            $user_token = $data_notification['user_token'];
            $device = $data_notification['device'];

            if ($device == 'android') {
                $nResident->noti("purchase_invoice","",$society_id,$user_token,$title,$description,"");
            } else if ($device == 'ios') {
                $nResident->noti_ios("purchase_invoice","",$society_id,$user_token,$title,$description,"");
            }

            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"purchase_invoice","purchase.png","","user_id = '$financeById'");

            $response["message"]="Invoice Uploaded";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['markAsNotAvailable']=="markAsNotAvailable" && $vendor_order_product_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('availability_status',$availability_status);

        $a = array(
            'availability_status'=>$m->get_data('availability_status'),
        );

        $qry = $d->update("purchase_vendor_order_product_master",$a,"vendor_order_product_id = '$vendor_order_product_id'");

        if ($qry == true) {
            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['changePurchaseStatus']=="changePurchaseStatus" && $vendor_order_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('purchase_status',$purchase_status);
        $m->set_data('purchase_reject_reason',$purchase_reject_reason);

        $a = array(
            'purchase_status'=>$m->get_data('purchase_status'),
            'purchase_reject_reason'=>$m->get_data('purchase_reject_reason'),
        );

        $qry = $d->update("purchase_vendor_order_master",$a,"vendor_order_id = '$vendor_order_id' AND vendor_id = '$vendor_id'");

        if ($qry == true) {

            if ($purchase_status == "1") {
                $title = "Order Approved";
            }else{
                $title = "Order Cancelled";
            }

            $description ="For Site: ".$site_name."\nBy, ".$user_name;

            $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$financeById'");
            $data_notification = mysqli_fetch_array($qsm);
            $user_token = $data_notification['user_token'];
            $device = $data_notification['device'];

            if ($device == 'android') {
                $nResident->noti("purchase_order_status","",$society_id,$user_token,$title,$description,"");
            } else if ($device == 'ios') {
                $nResident->noti_ios("purchase_order_status","",$society_id,$user_token,$title,$description,"");
            }

            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"purchase_order_cancel","purchase.png","","user_id = '$financeById'");

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Something went wrong";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updateProduct']=="updateProduct" && $productIds != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $puchaseProductIds = explode(",", $productIds);
        $puchaseProductPrice = explode(",", $productPrice);

        $totalIds = COUNT($puchaseProductIds);

        for ($i=0; $i < $totalIds; $i++) { 
            $m->set_data('price',$puchaseProductPrice[$i]);

            $a = array(
                'price'=>$m->get_data('price'),
            );

            $purchaseProductId = $puchaseProductIds[$i];

            $qry = $d->update("purchase_vendor_order_product_master",$a,"vendor_order_product_id = '$purchaseProductId'");
        }

        if ($qry == true) {
            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>