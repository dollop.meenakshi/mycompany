<?php
include_once '../residentApiNew/lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getProductCategory']=="getProductCategory"  && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("product_category_master,product_category_vendor_master","product_category_master.society_id = '$society_id' AND product_category_master.product_category_delete = '0' AND product_category_master.product_category_id = product_category_vendor_master.product_category_id AND product_category_vendor_master.vendor_id = '$vendor_id'");

        if(mysqli_num_rows($qry)>0){
            
            $response["product_category"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $data = array_map("html_entity_decode", $data);

                $product_category = array();

                $product_category["product_category_id"] = $data['product_category_id'];
                $product_category["category_name"] = $data['category_name'];
                $product_category["category_description"] = $data['category_description'];
                $product_category["product_category_status"] = $data['product_category_status'];
                $product_category["product_category_delete"] = $data['product_category_delete'];
                     
                array_push($response["product_category"], $product_category);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getProductSubCategory']=="getProductSubCategory" && $product_category_id!='' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("product_category_master,product_sub_category_master,product_sub_category_vendor_master","product_category_master.product_category_id = product_sub_category_master.product_category_id 
            AND product_sub_category_master.society_id = '$society_id' 
            AND product_sub_category_master.product_category_id = '$product_category_id'
            AND product_sub_category_master.product_sub_category_delete = '0' 
            AND product_sub_category_vendor_master.vendor_id = '$vendor_id' 
            AND product_sub_category_vendor_master.product_sub_category_id =product_sub_category_master.product_sub_category_id");

        if(mysqli_num_rows($qry)>0){
            
            $response["product_sub_category"] = array();

            while($data = mysqli_fetch_array($qry)) {
                
                $data = array_map("html_entity_decode", $data);

                $product_sub_category = array();

                $product_sub_category["product_category_id"] = $data['product_category_id'];
                $product_sub_category["product_sub_category_id"] = $data['product_sub_category_id'];
                $product_sub_category["category_name"] = $data['category_name'];
                $product_sub_category["sub_category_name"] = $data['sub_category_name'];
                $product_sub_category["sub_category_description"] = $data['sub_category_description'];
                $product_sub_category["product_sub_category_status"] = $data['product_sub_category_status'];
                $product_sub_category["product_sub_category_delete"] = $data['product_sub_category_delete'];

                     
                array_push($response["product_sub_category"], $product_sub_category);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['addProductCategory']=="addProductCategory" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('society_id',$society_id);
        $m->set_data('category_name',$category_name);
        $m->set_data('category_description',$category_description);
        $m->set_data('product_category_created_date',$dateTime);

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'category_name'=>$m->get_data('category_name'),
            'category_description'=>$m->get_data('category_description'),
            'product_category_created_date'=>$m->get_data('product_category_created_date'),
        );

        $qry = $d->insert("product_category_master",$a);

        if ($qry == true) {
            $response["message"]="Product Category Added";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['addProductSubCategory']=="addProductSubCategory" && $product_category_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('society_id',$society_id);
        $m->set_data('product_category_id',$product_category_id);
        $m->set_data('sub_category_name',$sub_category_name);
        $m->set_data('sub_category_description',$sub_category_description);
        $m->set_data('product_sub_category_created_date',$dateTime);

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'product_category_id'=>$m->get_data('product_category_id'),
            'sub_category_name'=>$m->get_data('sub_category_name'),
            'sub_category_description'=>$m->get_data('sub_category_description'),
            'product_sub_category_created_date'=>$m->get_data('product_sub_category_created_date'),
        );

        $qry = $d->insert("product_sub_category_master",$a);

        if ($qry == true) {
            $response["message"]="Product Sub Category Added";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updateProductCategory']=="updateProductCategory" && $product_category_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('category_name',$category_name);
        $m->set_data('category_description',$category_description);

        $a = array(
            'category_name'=>$m->get_data('category_name'),
            'category_description'=>$m->get_data('category_description'),
        );

        $qry = $d->update("product_category_master",$a,"product_category_id = '$product_category_id'");

        if ($qry == true) {
            $response["message"]="Category Updated";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updateProductSubCategory']=="updateProductSubCategory" && $product_sub_category_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('sub_category_name',$sub_category_name);
        $m->set_data('sub_category_description',$sub_category_description);

        $a = array(
            'sub_category_name'=>$m->get_data('sub_category_name'),
            'sub_category_description'=>$m->get_data('sub_category_description'),
        );

        $qry = $d->update("product_sub_category_master",$a,"product_sub_category_id = '$product_sub_category_id'");

        if ($qry == true) {
            $response["message"]="Category Updated";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['deleteProductCategory']=="deleteProductCategory" && $product_category_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('product_category_delete',"1");

        $a = array(
            'product_category_delete'=>$m->get_data('product_category_delete'),
        );

        $qry = $d->update("product_category_master",$a,"product_category_id = '$product_category_id'");

        $msg = "Product Category Deleted";
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['deleteProductSubCategory']=="deleteProductSubCategory" && $product_sub_category_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('product_sub_category_delete',"1");

        $a = array(
            'product_sub_category_delete'=>$m->get_data('product_sub_category_delete'),
        );

        $qry = $d->update("product_sub_category_master",$a,"product_sub_category_id = '$product_sub_category_id'");

        $msg = "Product Sub Category Deleted";
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['activeDeactiveProductCategory']=="activeDeactiveProductCategory" && $product_category_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('product_category_status',$product_category_status);

        $a = array(
            'product_category_status'=>$m->get_data('product_category_status'),
        );

        $qry = $d->update("product_category_master",$a,"product_category_id = '$product_category_id'");

        if ($product_category_status == 0) {
            $msg = "Category Activated";
        }else{
            $msg = "Category Deactivated";
        }

        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['activeDeactiveProductSubCategory']=="activeDeactiveProductSubCategory" && $product_sub_category_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('product_sub_category_status',$product_sub_category_status);

        $a = array(
            'product_sub_category_status'=>$m->get_data('product_sub_category_status'),
        );

        $qry = $d->update("product_sub_category_master",$a,"product_sub_category_id = '$product_sub_category_id'");

        if ($product_sub_category_status == 0) {
            $msg = "Category Activated";
        }else{
            $msg = "Category Deactivated";
        }
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>