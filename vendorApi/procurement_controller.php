<?php

include_once '../residentApiNew/lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getPurchaseProcurement']=="getPurchaseProcurement" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("
            purchase_master,
            site_master,
            site_manager_master,
            users_master,
            floors_master,
            block_master
            ","
            purchase_master.society_id='$society_id' 
            AND purchase_master.purchase_stage = '1' 
            AND site_master.site_id = purchase_master.site_id 
            AND site_master.site_id = site_manager_master.site_id 
            AND site_manager_master.site_manager_id = '$user_id' 
            AND site_manager_master.manager_type = '1' 
            AND users_master.user_id = purchase_master.user_id 
            AND users_master.block_id = block_master.block_id 
            AND users_master.floor_id = floors_master.floor_id","ORDER BY purchase_master.purchase_id DESC");

        if(mysqli_num_rows($qry)>0){
            
            $response["procurement_orders"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $purchase_orders = array();

                $purchase_orders["purchase_id"] = $data['purchase_id'];
                $purchase_orders["site_id"] = $data['site_id'];
                $purchase_orders["site_name"] = $data['site_name'];
                $purchase_orders["site_address"] = $data['site_address'];
                $purchase_orders["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
                $purchase_orders["po_number"] = $data['po_number'];
                $purchase_orders["user_id"] = $data['user_id'];
                $purchase_orders["user_full_name"] = $data['user_full_name'];
                $purchase_orders["user_designation"] = $data['user_designation'];
                $purchase_orders["floor_id"] = $data['floor_id'];
                $purchase_orders["department_name"] = $data['floor_name'];
                $purchase_orders["block_id"] = $data['block_id'];
                $purchase_orders["branch_name"] = $data['block_name'];
                
                if ($data['user_profile_pic'] != '') {
                    $purchase_orders["user_profile_pic"] = $base_url . "img/users/recident_profile/" . $data['user_profile_pic'];
                } else {
                    $purchase_orders["user_profile_pic"] = $base_url . "img/users/owner/user_default.png";
                }

                if($data['purchase_status'] == '0'){ 
                    $status = "Pending";
                } else if($data['purchase_status'] == '1'){
                    $status = "Accepted";
                }else{
                    $status = "Rejected";
                }

                $totalItemCount = $d->count_data_direct("puchase_product_id","purchase_product_master","purchase_id = '$data[purchase_id]'");

                $purchase_orders["purchase_status"] = $data['purchase_status'];
                $purchase_orders["quantity"] = $totalItemCount.'';
                $purchase_orders["purchase_status_view"] = $status;

                     
                array_push($response["procurement_orders"], $purchase_orders);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getPurchaseDetailsProcurement']=="getPurchaseDetailsProcurement" && filter_var($society_id, FILTER_VALIDATE_INT) == true){



        $qry = $d->select("
            purchase_master,
            site_master,
            site_manager_master
            ","
            site_master.site_id=purchase_master.site_id 
            AND purchase_master.society_id='$society_id' 
            AND purchase_master.purchase_id = '$purchase_id' 
            AND site_manager_master.site_id = site_master.site_id 
            AND site_manager_master.manager_type = '1' 
            AND site_manager_master.site_manager_id = '$user_id'");

        if(mysqli_num_rows($qry)>0){

            $data = mysqli_fetch_array($qry);

            $data = array_map("html_entity_decode", $data);

            $response["purchase_id"] = $data['purchase_id'];
            $response["puchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
            $response["po_number"] = $data['po_number'];
            $response["site_id"] = $data['site_id'];
            $response["site_name"] = $data['site_name'];
            $response["site_address"] = $data['site_address'];
            $response["purchase_descripion"] = $data['purchase_descripion'];
           
            if ($data['purchase_invoice'] != '') {
                $response["purchase_invoice"] =  $base_url."img/purchase_invoice/".  $data['purchase_invoice'];
            }else{
                $response["purchase_invoice"] =  "";
            }

            if($data['purchase_status'] == '0'){ 
                $status = "Pending";
            } else if($data['purchase_status'] == '1'){
                $status = "Accepted";
            }else{
                $status = "Rejected";
            }

            $response["purchase_status"] = $data['purchase_status'];
            $response["purchase_status_view"] = $status;

        }

    
        $response['products'] = array();

        $q = $d->selectRow("
            purchase_product_master.*,
            product_master.*,
            product_category_master.product_category_id,
            product_category_master.category_name,
            product_variant_master.product_variant_id,
            product_variant_master.product_variant_name,
            unit_measurement_master.unit_measurement_name,
            unit_measurement_master.unit_measurement_id,
            product_price_master.product_price_id,
            product_price_master.minimum_order_quantity
            ","
            purchase_product_master,
            product_master,
            product_category_master,
            product_variant_master,
            unit_measurement_master,
            product_price_master
            ","
            purchase_product_master.product_id=product_master.product_id 
            AND product_master.product_id=product_variant_master.product_id 
            AND purchase_product_master.product_variant_id=product_variant_master.product_variant_id 
            AND product_master.product_category_id=product_category_master.product_category_id 
            AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id 
            AND purchase_product_master.purchase_id='$purchase_id' 
            AND product_price_master.product_variant_id = purchase_product_master.product_variant_id
            AND product_price_master.product_id = purchase_product_master.product_id 
            AND product_price_master.product_price_id = purchase_product_master.product_price_id","");


        if (mysqli_num_rows($q) > 0) {

            while($purchaseData = mysqli_fetch_array($q)){

                $purchaseData = array_map("html_entity_decode", $purchaseData);

                $products = array();

                $products['purchase_product_id'] = $purchaseData['puchase_product_id'];
                $products['product_id'] = $purchaseData['product_id'];
                $products['purchase_id'] = $purchaseData['purchase_id'];
                $products['product_code'] = $purchaseData['product_code'];
                $products['product_name'] = $purchaseData['product_name'];
                $products['category_name'] = $purchaseData['category_name'];
                $products['sub_category_name'] = $purchaseData['sub_category_name'].'';
                $products['unit_measurement_name'] = $purchaseData['unit_measurement_name'];
                $products["product_brand"] = $purchaseData['product_brand'];
                $products["other"] = $purchaseData['other'];
                $products['product_variant_id'] = $purchaseData['product_variant_id'];
                $products['product_variant_name'] = $purchaseData['product_variant_name'];
                $products['minimum_order_quantity'] = $purchaseData['minimum_order_quantity'];
                $products['product_price_id'] = $purchaseData['product_price_id'];

                $products['quantity'] = $purchaseData['quantity'];
                $products['availability_status'] = $purchaseData['availablity_status'];

                array_push($response['products'],$products);
            }
        }

        $response["message"]="Success";
        $response["status"]="200";
        echo json_encode($response);
    }else if($_POST['updateProductQty']=="updateProductQty" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('quantity',$quantity);

        $a = array(
            'quantity'=>$m->get_data('quantity'),
        );

        $qry = $d->update("purchase_product_master",$a,"puchase_product_id = '$puchase_product_id' AND purchase_id = '$purchase_id' AND product_id = '$product_id' AND product_variant_id = '$product_variant_id'");
            
        if ($qry == true) {

            $m->set_data('puchase_product_id',$puchase_product_id);
            $m->set_data('purchase_id',$purchase_id);
            $m->set_data('new_quantity',$quantity);
            $m->set_data('changed_by',$user_id);
            $m->set_data('changed_date',date('Y-m-d H:i:s'));

            $aryHis = array(
                'puchase_product_id'=>$m->get_data('puchase_product_id'),
                'purchase_id'=>$m->get_data('purchase_id'),
                'new_quantity'=>$m->get_data('new_quantity'),
                'changed_by'=>$m->get_data('changed_by'),
                'changed_date'=>$m->get_data('changed_date'),
            );

            $qryHistory = $d->insert("purchase_product_change_history",$aryHis);

            $title = "Purchase Request Updated";
            $description ="Site: ".$site_name."\nProduct: ".  $product_name. ", quantity($quantity) updated in your purchase request.\n"."By, ".$user_name;

            $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$purchase_user_id'");
            $data_notification = mysqli_fetch_array($qsm);
            $user_token = $data_notification['user_token'];
            $device = $data_notification['device'];

            if ($device == 'android') {
                $nResident->noti("my_purchase_request","",$society_id,$user_token,$title,$description,$tabPosition);
            } else if ($device == 'ios') {
                $nResident->noti_ios("my_purchase_request","",$society_id,$user_token,$title,$description,$tabPosition);
            }

            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"my_purchase_request","purchase.png",$tabPosition,"user_id = '$purchase_user_id'");

            $response["message"]="Product Quantity Updated";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['deletePurchaseProduct']=="deletePurchaseProduct" && filter_var($society_id, FILTER_VALIDATE_INT) == true){


        $qry = $d->delete("purchase_product_master","puchase_product_id = '$puchase_product_id' AND purchase_id = '$purchase_id' AND product_id = '$product_id' AND product_variant_id = '$product_variant_id'");
            
        if ($qry == true) {

            $title = "Purchase Request Updated";
            $description ="Site: ".$site_name."\nProduct: ". $product_name. ", removed from your purchase request.\n"."By, ".$user_name;

            $qsm = $d->select("users_master", "society_id='$society_id' AND user_id='$purchase_user_id'");
            $data_notification = mysqli_fetch_array($qsm);
            $user_token = $data_notification['user_token'];
            $device = $data_notification['device'];

            if ($device == 'android') {
                $nResident->noti("my_purchase_request","",$society_id,$user_token,$title,$description,"");
            } else if ($device == 'ios') {
                $nResident->noti_ios("my_purchase_request","",$society_id,$user_token,$title,$description,"");
            }

            $d->insertUserNotificationWithJsonData($society_id,$title,$description,"my_purchase_request","purchase.png",$tabPosition,"user_id = '$purchase_user_id'");

            $response["message"]="Requested Product Deleted";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Failed";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['transferToFinance']=="transferToFinance" && $purchase_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('purchase_stage',"2");
        $m->set_data('procurement_by_id',$user_id);

        $a = array(
            'purchase_stage'=>$m->get_data('purchase_stage'),
            'procurement_by_id'=>$m->get_data('procurement_by_id'),
        );

        $qry = $d->update("purchase_master",$a,"purchase_id = '$purchase_id'");

        if ($qry == true) {

            $siteUsersQry = $d->selectRow("user_id,user_token,device","users_master,site_master,site_manager_master","
                site_master.site_id = site_manager_master.site_id 
                AND site_manager_master.site_id = '$site_id' 
                AND users_master.user_id = site_manager_master.site_manager_id 
                AND site_manager_master.manager_type = '2'");

            if (mysqli_num_rows($siteUsersQry) > 0) {
                
                $fcmArrayAndroid=array();
                $fcmArrayiOS=array();
                $userFcmIds=array();

                while ($row=mysqli_fetch_array($siteUsersQry)) {
                    if ($row['device'] == "android" || $row['device'] == "Android") {
                        array_push($fcmArrayAndroid, $row['user_token']);
                    }else if ($row['device'] == "ios" || $row['device'] == "iOS") {
                        array_push($fcmArrayiOS, $row['user_token']);
                    }
                    array_push($userFcmIds, $row['user_id']);
                }

                $userFcmIds = implode("','",$userFcmIds);

                $title = "New Finance Request";
                $description = "By, ".$user_name;

                $nResident->noti("finance_request","",$society_id,$fcmArrayAndroid,$title,$description,"");
                $nResident->noti_ios("finance_request","",$society_id,$fcmArrayiOS,$title,$description,"");

                $d->insertUserNotificationWithJsonData($society_id,$title,$description,"finance_request","purchase.png","","users_master.user_id IN ('$userFcmIds')");
            }

            $response["message"]="Purchase Request Transfered to Finance Team";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>