<?php

include_once '../residentApiNew/lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getConfirmOrders']=="getConfirmOrders" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("
            purchase_vendor_order_master,
            site_master,
            local_service_provider_users
            ","
            purchase_vendor_order_master.society_id='$society_id' 
            AND site_master.site_id = purchase_vendor_order_master.site_id
            AND purchase_vendor_order_master.user_id = '$user_id' AND purchase_vendor_order_master.purchase_status = '1' AND local_service_provider_users.service_provider_users_id = purchase_vendor_order_master.vendor_id","ORDER BY purchase_vendor_order_master.vendor_order_id DESC");


        if(mysqli_num_rows($qry)>0){
            
            $response["confirm_orders"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $confirmOrder = array();

                $confirmOrder["vendor_order_id"] = $data['vendor_order_id'];
                $confirmOrder["site_id"] = $data['site_id'];
                $confirmOrder["site_name"] = $data['site_name'];
                $confirmOrder["site_address"] = $data['site_address'];
                $confirmOrder["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
                $confirmOrder["po_number"] = $data['po_number'];
                $confirmOrder["service_provider_name"] = $data['service_provider_name'];
                $confirmOrder["contact_person_name"] = $data['contact_person_name'];
                $confirmOrder["service_provider_address"] = $data['service_provider_address'];
                $confirmOrder["service_provider_phone"] = $data['service_provider_phone'];
                $confirmOrder["service_provider_email"] = $data['service_provider_email'];

                if ($data['purchase_invoice'] == "") {
                    $confirmOrder["purchase_invoice"] = "";
                }else{                    
                    $confirmOrder["purchase_invoice"] = $base_url.'img/purchase_invoice/'.$data['purchase_invoice'];
                }

                if($data['purchase_status'] == '0'){ 
                    $status = "Pending";
                } else if($data['purchase_status'] == '1'){
                    $status = "Accepted";
                }else{
                    $status = "Rejected";
                }

                $totalItemCount = $d->count_data_direct("vendor_order_product_id","purchase_vendor_order_product_master","vendor_order_id = '$data[vendor_order_id]'");

                $confirmOrder["purchase_status"] = $data['purchase_status'];
                $confirmOrder["quantity"] = $totalItemCount.'';
                $confirmOrder["purchase_status_view"] = $status;

                     
                array_push($response["confirm_orders"], $confirmOrder);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['getConfirmOrderDetails']=="getConfirmOrderDetails" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $response['confirm_products'] = array();

        $qry = $d->select("purchase_vendor_order_master,site_master","site_master.site_id=purchase_vendor_order_master.site_id AND purchase_vendor_order_master.society_id='$society_id' AND purchase_vendor_order_master.vendor_order_id = '$vendor_order_id' AND purchase_vendor_order_master.user_id = '$user_id'");

        if(mysqli_num_rows($qry)>0){

            $data = mysqli_fetch_array($qry);

            $data = array_map("html_entity_decode", $data);

            $response["vendor_order_id"] = $data['vendor_order_id'];
            $response["purchase_date"] = date('d-m-Y h:i A',strtotime($data['purchase_date']));
            $response["po_number"] = $data['po_number'];
            $response["site_id"] = $data['site_id'];
            $response["site_name"] = $data['site_name'];
            $response["site_address"] = $data['site_address'];
            $response["purchase_descripion"] = $data['purchase_descripion'];
           
            if ($data['purchase_invoice'] != '') {
                $response["purchase_invoice"] =  $base_url."img/purchase_invoice/".  $data['purchase_invoice'];
            }else{
                $response["purchase_invoice"] =  "";
            }

            if($data['purchase_status'] == '0'){ 
                $status = "Pending";
            } else if($data['purchase_status'] == '1'){
                $status = "Accepted";
            }else{
                $status = "Rejected";
            }

            $response["purchase_status"] = $data['purchase_status'];
            $response["purchase_status_view"] = $status;

            $q = $d->selectRow("
                purchase_vendor_order_product_master.*,
                product_master.*,
                product_category_master.product_category_id,
                product_category_master.category_name,
                unit_measurement_master.unit_measurement_name,
                unit_measurement_master.unit_measurement_id,
                product_variant_master.product_variant_name,
                product_variant_master.product_variant_id
                ","
                purchase_vendor_order_product_master,
                product_master,
                product_category_master,
                unit_measurement_master,
                product_variant_master
                ","
                purchase_vendor_order_product_master.product_id=product_master.product_id 
                AND product_master.product_category_id=product_category_master.product_category_id 
                AND product_master.unit_measurement_id=unit_measurement_master.unit_measurement_id 
                AND purchase_vendor_order_product_master.vendor_order_id='$data[vendor_order_id]' 
                AND purchase_vendor_order_product_master.product_variant_id = product_variant_master.product_variant_id");


            if (mysqli_num_rows($q) > 0) {

                while($purchaseData = mysqli_fetch_array($q)){

                    $purchaseData = array_map("html_entity_decode", $purchaseData);

                    $products = array();

                    $products['vendor_order_product_id'] = $purchaseData['vendor_order_product_id'];
                    $products['product_id'] = $purchaseData['product_id'];
                    $products['product_variant_id'] = $purchaseData['product_variant_id'];
                    $products['product_variant_name'] = $purchaseData['product_variant_name'];
                    $products['product_code'] = $purchaseData['product_code'];
                    $products['product_name'] = $purchaseData['product_name'];
                    $products['product_brand'] = $purchaseData['product_brand'];
                    $products['other'] = $purchaseData['other'];
                    $products['category_name'] = $purchaseData['category_name'];
                    $products['sub_category_name'] = $purchaseData['sub_category_name'];
                    $products['unit_measurement_name'] = $purchaseData['unit_measurement_name'];
                    $products['price'] = $purchaseData['price'];

                    $totalPrice = $purchaseData['price'] * $purchaseData['quantity'];

                    $totalPrice = number_format($totalPrice,2,'.','');

                    $products['total_price'] = $totalPrice."";
                    $products['quantity'] = $purchaseData['quantity'];
                    $products['availability_status'] = $purchaseData['availability_status'];
                    $products['vendor_id'] = $purchaseData['vendor_id'];

                    array_push($response['confirm_products'],$products);
                }
            }

        }

        $response["message"]="Success";
        $response["status"]="200";
        echo json_encode($response);
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>