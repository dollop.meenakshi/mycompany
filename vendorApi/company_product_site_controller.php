<?php
include_once '../residentApiNew/lib.php';

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

if(isset($_POST) && !empty($_POST)){
  
    $response = array();
    extract(array_map("test_input" , $_POST));
    $dateTime = date("Y-m-d H:i:s");
    $temDate = date("Y-m-d h:i A");

    if($_POST['getSites']=="getSites" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $qry = $d->select("site_master","society_id = '$society_id' AND site_delete = '0' AND site_active_status = '0'");

        if(mysqli_num_rows($qry)>0){
            
            $response["sites"] = array();

            while($data = mysqli_fetch_array($qry)) {

                $data = array_map("html_entity_decode", $data);

                $sites = array();

                $sites["site_id"] = $data['site_id'];
                $sites["site_name"] = $data['site_name'];
                $sites["site_address"] = $data['site_address'];
                     
                array_push($response["sites"], $sites);
            }

            $response["message"]="Success";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="No data found";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['addSite']=="addSite" && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('society_id',$society_id);
        $m->set_data('site_manager_id',$vendor_id);
        $m->set_data('site_name',$site_name);
        $m->set_data('site_address',$site_address);
        $m->set_data('site_description',$site_description);
        $m->set_data('site_created_date',$dateTime);

        $a = array(
            'society_id'=>$m->get_data('society_id'),
            'site_manager_id'=>$m->get_data('site_manager_id'),
            'site_name'=>$m->get_data('site_name'),
            'site_address'=>$m->get_data('site_address'),
            'site_description'=>$m->get_data('site_description'),
            'site_created_date'=>$m->get_data('site_created_date'),
        );

        $qry = $d->insert("site_master",$a);

        if ($qry == true) {
            $response["message"]="New Site Added";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['updateSite']=="updateSite" && $site_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('site_name',$site_name);
        $m->set_data('site_address',$site_address);
        $m->set_data('site_description',$site_description);

        $a = array(
            'site_name'=>$m->get_data('site_name'),
            'site_address'=>$m->get_data('site_address'),
            'site_description'=>$m->get_data('site_description'),
        );

        $qry = $d->update("site_master",$a,"site_id = '$site_id' AND site_manager_id = '$vendor_id'");

        if ($qry == true) {
            $response["message"]="Site Updated";
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['deleteSite']=="deleteSite" && $site_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('site_delete',"1");

        $a = array(
            'site_delete'=>$m->get_data('site_delete'),
        );

        $qry = $d->update("site_master",$a,"site_id = '$site_id' AND site_manager_id = '$vendor_id'");

        $msg = "Site Deleted";
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else if($_POST['activeDeactiveSite']=="activeDeactiveSite" && $site_id != '' && filter_var($society_id, FILTER_VALIDATE_INT) == true){

        $m->set_data('site_active_status',$site_active_status);

        $a = array(
            'site_active_status'=>$m->get_data('site_active_status'),
        );

        $qry = $d->update("site_master",$a,"site_id = '$site_id' AND site_manager_id = '$vendor_id'");

        if ($site_active_status == 0) {
            $msg = "Site Activated";
        }else{
            $msg = "Site Deactivated";
        }
    
        if ($qry == true) {
            $response["message"]=$msg;
            $response["status"]="200";
            echo json_encode($response);
        }else{
            $response["message"]="Error";
            $response["status"]="201";
            echo json_encode($response);
        }
    }else{
        $response["message"]="wrong tag.";
        $response["status"]="201";
        echo json_encode($response);
    }
}
?>