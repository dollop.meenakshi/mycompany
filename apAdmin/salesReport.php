<?php
if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
}
if(isset($_GET['Df']) && !empty($_GET['Df']) && $_GET['Df'] != "0000-00-00 00:00:00")
{
    $date_from = $_GET['Df'];
}
elseif(isset($_COOKIE['Df']) && !empty($_COOKIE['Df']) && $_COOKIE['Df'] != "0000-00-00 00:00:00")
{
    $date_from = $_COOKIE['Df'];
}
else
{
    $date_from = date("Y-m-d");
}

if(isset($_GET['Dt']) && !empty($_GET['Dt']) && $_GET['Dt'] != "0000-00-00 00:00:00")
{
    $date_to = $_GET['Dt'];
}
elseif(isset($_COOKIE['Dt']) && !empty($_COOKIE['Dt']) && $_COOKIE['Dt'] != "0000-00-00 00:00:00")
{
    $date_to = $_COOKIE['Dt'];
}
else
{
    $date_to = date("Y-m-d");
}

$format = 'Y-m-d';
if(isset($date_from) && !empty($date_from))
{
    $df = DateTime::createFromFormat($format, $date_from);
    $result = $df && $df->format($format) === $date_from;
    if($result != 1)
    {
        $_SESSION['msg1'] = "Invalid From Date!";
    ?>
        <script>
            window.location = "salesReport";
        </script>
    <?php
        exit;
    }
}

if(isset($date_to) && !empty($date_to))
{
    $dt = DateTime::createFromFormat($format, $date_to);
    $result = $dt && $dt->format($format) === $date_to;
    if($result != 1)
    {
        $_SESSION['msg1'] = "Invalid To Date!";
    ?>
        <script>
            window.location = "salesReport";
        </script>
    <?php
        exit;
    }
}

if(isset($_GET['UId']) && !empty($_GET['UId']))
{
    $user_id = $_GET['UId'];
}
/*elseif(isset($_COOKIE['UId']) && !empty($_COOKIE['UId']))
{
    $user_id = $_COOKIE['UId'];
}*/

if(isset($user_id) && !empty($user_id) && !ctype_digit($user_id))
{
    $_SESSION['msg1'] = "Invalid User Id!";
    ?>
    <script>
        window.location = "salesReport";
    </script>
<?php
    exit;
}

if(isset($_GET['RId']) && !empty($_GET['RId']))
{
    $retailer_id = $_GET['RId'];
}
/*elseif(isset($_COOKIE['RId']) && !empty($_COOKIE['RId']))
{
    $retailer_id = $_COOKIE['RId'];
}*/

if(isset($retailer_id) && !empty($retailer_id) && !ctype_digit($retailer_id))
{
    $_SESSION['msg1'] = "Invalid Retailer Id!";
    ?>
    <script>
        window.location = "salesReport";
    </script>
<?php
    exit;
}

if(isset($_GET['DId']) && !empty($_GET['DId']))
{
    $distributor_id = $_GET['DId'];
}
/*elseif(isset($_COOKIE['DId']) && !empty($_COOKIE['DId']))
{
    $distributor_id = $_COOKIE['DId'];
}*/

if(isset($distributor_id) && !empty($distributor_id) && !ctype_digit($distributor_id))
{
    $_SESSION['msg1'] = "Invalid Distributor Id!";
    ?>
    <script>
        window.location = "salesReport";
    </script>
<?php
    exit;
}

if(isset($_GET['Os']))
{
    $order_status = $_GET['Os'];
}
/*elseif(isset($_COOKIE['Os']) && !empty($_COOKIE['Os']))
{
    $order_status = $_COOKIE['Os'];
}*/

if(isset($order_status) && !empty($order_status) && !ctype_digit($order_status))
{
    $_SESSION['msg1'] = "Invalid Order Status!";
    ?>
    <script>
        window.location = "salesReport";
    </script>
<?php
    exit;
}

if(isset($_GET['AId']) && !empty($_GET['AId']))
{
    $area_id = $_GET['AId'];
}
/*elseif(isset($_COOKIE['AId']) && !empty($_COOKIE['AId']))
{
    $area_id = $_COOKIE['AId'];
}*/

if(isset($area_id) && !empty($area_id) && !ctype_digit($area_id))
{
    $_SESSION['msg1'] = "Invalid Area Id!";
    ?>
    <script>
        window.location = "salesReport";
    </script>
<?php
    exit;
}

if(isset($_GET['PId']) && !empty($_GET['PId']))
{
    $product_id = $_GET['PId'];
}
/*elseif(isset($_COOKIE['PId']) && !empty($_COOKIE['PId']))
{
    $product_id = $_COOKIE['PId'];
}*/

if(isset($product_id) && !empty($product_id) && !ctype_digit($product_id))
{
    $_SESSION['msg1'] = "Invalid Product Id!";
    ?>
    <script>
        window.location = "salesReport";
    </script>
<?php
    exit;
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Sales Report</h4>
            </div>
        </div>
        <form id="filterFormOrders">
            <div class="row pt-2 pb-2">
                <div class="col-lg-3 col-6">
                    <label  class="form-control-label">User </label>
                    <select class="form-control single-select-new" id="UId" name="UId">
                        <option value="">-- Select --</option>
                        <?php
                            $qu = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","delete_status = 0 AND active_status = 0");
                            while ($ud = $qu->fetch_assoc())
                            {
                            ?>
                        <option <?php if($user_id == $ud['user_id']) { echo 'selected'; } ?> value="<?php echo $ud['user_id']; ?>"><?php echo $ud['user_full_name'] . " (" . $ud['block_name'] . "-" . $ud['floor_name'] . ")"; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Retailer </label>
                    <select class="form-control single-select" id="RId" name="RId">
                        <option value="">-- Select --</option>
                        <?php
                            $rt = $d->selectRow("rm.retailer_id,rm.retailer_name,c.city_name,amn.area_name","retailer_master AS rm JOIN cities AS c ON c.city_id = rm.city_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id");
                            while ($rd = $rt->fetch_assoc())
                            {
                        ?>
                        <option <?php if(isset($retailer_id) && $retailer_id == $rd['retailer_id']) { echo 'selected'; } ?> value="<?php echo $rd['retailer_id'];?>"><?php echo $rd['retailer_name'] . " (" . $rd['area_name'] . "-" . $rd['city_name'] . ")";?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Distributor </label>
                    <select class="form-control single-select" id="DId" name="DId">
                        <option value="">-- Select --</option>
                        <?php
                            $qd = $d->selectRow("dm.distributor_id,dm.distributor_name,c.city_name,amn.area_name","distributor_master AS dm JOIN cities AS c ON c.city_id = dm.city_id JOIN area_master_new AS amn ON amn.area_id = dm.distributor_area_id");
                            while ($qdd = $qd->fetch_assoc())
                            {
                        ?>
                        <option <?php if(isset($distributor_id) && $distributor_id == $qdd['distributor_id']) { echo 'selected'; } ?> value="<?php echo $qdd['distributor_id']; ?>"><?php echo $qdd['distributor_name'] . " (" . $qdd['area_name'] . "-" . $qdd['city_name'] . ")"; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Order Status </label>
                    <select class="form-control single-select" id="Os" name="Os">
                        <option value="">-- Select --</option>
                        <option <?php if(isset($order_status) && $order_status == 0) { echo 'selected'; } ?> value="0">Pending</option>
                        <option <?php if(isset($order_status) && $order_status == 1) { echo 'selected'; } ?> value="1">Approved</option>
                        <option <?php if(isset($order_status) && $order_status == 2) { echo 'selected'; } ?> value="2">Rejected</option>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Start Date </label>
                    <input type="text" class="form-control" autocomplete="off" id="date_from_new" name="Df" value="<?php if(isset($date_from) && $date_from != ""){ echo $date_from; } ?>">
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">End Date </label>
                    <input  type="text" class="form-control" autocomplete="off" id="date_to_new" name="Dt" value="<?php if(isset($date_to) && $date_to != ""){ echo $date_to; } ?>">
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Area </label>
                    <select class="form-control single-select-new" id="AId" name="AId">
                        <option value="">-- Select --</option>
                        <?php
                            $qar = $d->selectRow("area_id,area_name","area_master_new","area_flag = 1");
                            while ($qdar = $qar->fetch_assoc())
                            {
                        ?>
                        <option <?php if(isset($area_id) && $area_id == $qdar['area_id']) { echo 'selected'; } ?> value="<?php echo $qdar['area_id']; ?>"><?php echo $qdar['area_name']; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Product </label>
                    <select class="form-control single-select" id="PId" name="PId">
                        <option value="">-- Select --</option>
                        <?php
                            $qp = $d->selectRow("rpm.product_id,rpm.product_name,pcm.category_name","retailer_product_master AS rpm JOIN product_category_master AS pcm ON pcm.product_category_id = rpm.product_category_id","rpm.product_status = 0 AND rpm.product_delete = 0");
                            while ($qdp = $qp->fetch_assoc())
                            {
                        ?>
                        <option <?php if(isset($product_id) && $product_id == $qdp['product_id']) { echo 'selected'; } ?> value="<?php echo $qdp['product_id']; ?>"><?php echo $qdp['product_name'] . " (" . $qdp['category_name'] . ")"; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="button" onclick="submitFilterForm();">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example10" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Order</th>
                                        <th>Order Date</th>
                                        <th>Order Time</th>
                                        <th>Server Date</th>
                                        <th>Server Time</th>
                                        <th>User</th>
                                        <th>Category</th>
                                        <th>Product</th>
                                        <th>SKU</th>
                                        <th>Packing Type</th>
                                        <th>Case</th>
                                        <th>Unit</th>
                                        <th>Total Unit</th>
                                        <th>Total Weight</th>
                                        <th>Product Price</th>
                                        <th>Sales</th>
                                        <th>Route</th>
                                        <th>Distributor</th>
                                        <th>Retailer</th>
                                        <th>Retailer Contact</th>
                                        <th>Area</th>
                                        <th>Retailer Address</th>
                                        <th>Order Latitude</th>
                                        <th>Order Longitude</th>
                                        <th>Remarks</th>
                                        <th>Status</th>
                                        <th>Order Placed Via</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script>
function submitFilterForm()
{
    var from_d = $('#date_from_new').val();
    var to_d = $('#date_to_new').val();
    if(from_d != "" && to_d == "")
    {
        swal("Please select end date to use filter!", {icon: "error",});
    }
    else
    {
        loaddata();
    }
}

$(document).ready(function()
{
    var user_id = '<?php echo $user_id ?>';
    var retailer_id = '<?php echo $retailer_id ?>';
    var distributor_id = '<?php echo $distributor_id ?>';
    var order_status = '<?php echo $order_status ?>';
    var date_from = '<?php echo $date_from ?>';
    var date_to = '<?php echo $date_to ?>';
    var area_id = '<?php echo $area_id ?>';
    var product_id = '<?php echo $product_id ?>';
    loaddata(user_id,retailer_id,distributor_id,order_status,date_from,date_to,area_id,product_id);
});

function loaddata(user_id,retailer_id,distributor_id,order_status,date_from,date_to,area_id,product_id)
{
    $('#spinner').fadeIn(1);
    if($.fn.DataTable.isDataTable('#example10'))
    {
        $('#example10').DataTable().destroy();
    }
    if(user_id == "" || user_id == null || user_id == 0)
    {
        user_id = $('#UId').val();
    }
    if(retailer_id == "" || retailer_id == null || retailer_id == 0)
    {
        retailer_id = $('#RId').val();
    }
    if(distributor_id == "" || distributor_id == null || distributor_id == 0)
    {
        distributor_id = $('#DId').val();
    }
    if(order_status == "" || order_status == null)
    {
        order_status = $('#Os').val();
    }
    if(date_from == "" || date_from == null || date_from == "0000-00-00 00:00:00")
    {
        date_from = $('#date_from_new').val();
    }
    if(date_to == "" || date_to == null || date_to == "0000-00-00 00:00:00")
    {
        date_to = $('#date_to_new').val();
    }
    if(area_id == "" || area_id == null || area_id == 0)
    {
        area_id = $('#AId').val();
    }
    if(product_id == "" || product_id == null || product_id == 0)
    {
        product_id = $('#PId').val();
    }
    pintable = $('#example10').DataTable({
        <?php
        if($adminData['report_download_access'] != 0)
        {
        ?>
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false,
        "dom": 'Blfrtip',
        "buttons": [
        {
            extend: 'copyHtml5',
            title: $("#reportName").val(),
            exportOptions: {
              columns: ':visible'
            }
        },
        {
            extend: 'csv',
            title: $("#reportName").val(),
            exportOptions: {
              columns: ':visible'
            }
        },
        {
            extend: 'excelHtml5',
            title: $("#reportName").val(),
            exportOptions: {
              columns: ':visible'
            }
        },
        {
            extend : 'pdfHtml5',
            title: $("#reportName").val(),
            orientation : 'landscape',
            pageSize : 'LEGAL',
            titleAttr : 'PDF',
            exportOptions: {
              columns: ':visible'
            }
        },'colvis'],
        <?php
        }
        ?>
        "columnDefs": [{
            "defaultContent": "",
            "targets": "_all"
        }],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "ajax": {
            url: "controller/orderProductController.php",
            type: "post",
            data: {getProductSalesListReport:"getProductSalesListReport",user_id:user_id,retailer_id:retailer_id,distributor_id:distributor_id,order_status:order_status,date_from:date_from,date_to:date_to,area_id:area_id,product_id:product_id,csrf:csrf},
            dataSrc: ""
        },
        "columns": [
            { "data": "retailer_order_id" },
            { "data": "o_date" },
            { "data": "o_time" },
            { "data": "s_date" },
            { "data": "s_time" },
            { "data": "user_full_name" },
            { "data": "category_name" },
            { "data": "product_variant" },
            { "data": "sku" },
            { "data": "variant_packing_type" },
            { "data": "case" },
            { "data": "unit" },
            { "data": "product_quantity" },
            { "data": "total_weight_in_kg" },
            { "data": "product_price" },
            { "data": "product_total_price" },
            { "data": "order_route_name" },
            { "data": "distributor_name" },
            { "data": "retailer_name" },
            { "data": "retailer_num" },
            { "data": "area_name" },
            { "data" : "retailer_address" , render : function ( data, type, row, meta ) {
                return data.replace(/.{40}/g, '$&<br>');
            }},
            { "data": "order_latitude" },
            { "data": "order_longitude" },
            { "data" : "order_remark" , render : function ( data, type, row, meta ) {
                return data.replace(/.{40}/g, '$&<br>');
            }},
            { "data": "order_status",render : function ( data, type, row, meta ) {
                if (data == 0)
                {
                    return '<label class="badge ml-2 badge-warning">Pending</label>';
                }
                else if(data == 1)
                {
                    return '<label class="badge ml-2 badge-success">Approved</label>';
                }
                else if(data == 2)
                {
                    return '<label class="badge ml-2 badge-danger">Rejected</label>';
                }
                else
                {
                    return '<label class="badge ml-2 badge-danger">Cancelled</label>';
                }
            }},
            { "data": "order_placed_via" }
        ]
    });
    $('#spinner').fadeOut("slow");
}

function destroydata()
{
    $('#example10').html('');
    pintable.destroy();
}
</script>