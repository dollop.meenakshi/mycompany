<?php error_reporting(0);
$vendor_order_id = $_GET['id'];
if(isset($_GET)){
   
    $q2 = $d->select("purchase_vendor_order_master,site_master,local_service_provider_users","site_master.site_id=purchase_vendor_order_master.site_id AND local_service_provider_users.service_provider_users_id=purchase_vendor_order_master.vendor_id AND purchase_vendor_order_master.vendor_order_id='$_GET[id]'");
    $data = mysqli_fetch_array($q2);
	extract($data);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
          <h4 class="page-title">Purchase Order Products</h4>
        </div>
        <div class="col-sm-3 col-6 text-right">
       <!--  <?php if($data['purchase_status']==0){?>
        <a href="addSiteBtn" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-check" aria-hidden="true"></i> Approve </a>
        <a href="javascript:void(0)" onclick="DeleteAll('deleteSite');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-times" aria-hidden="true"></i> Reject </a>
        <?php } ?> -->
        <!--  <a href="javascript:void(0)" onclick="DeleteAll('deleteSite');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-times" aria-hidden="true"></i> Reject </a> -->
        </div>
      </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              
                <div class="row">
                    <!-- <div class="col-md-6">
                        <h4 class="">Purchase Order Products</h4>
                    </div> -->
                    <div class="col-md-12 col-sm-3">
                    <?php if($data['purchase_status']==0){
                        $st = "Pending";
                        $cls = "badge-primary";
                        }else if($data['purchase_status']==1){
                            $st = "Accepted";
                            $cls = "badge-success";
                        }else
                        {
                            $st = "Rejected";
                            $cls = "badge-danger";
                        }?>
                    
                        <h6 class="text-right">
                            <span class=" badge badge-sm <?php echo $cls; ?> "><?php echo $st; ?></span>
                        </h6>
                       
                    </div>
                    <div class="col-md-4 pt-4">
                       <label>PO Number:</label> <span> <?php echo $data['po_number']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                        <label>Site Name:</label><span> <?php echo $data['site_name']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                    <label>Vandor Name:</label><span> <?php echo $data['service_provider_name']; ?> - <?php echo $data['contact_person_name']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                    <label>Date:</label> <span> <?php if(isset($data['purchase_date']) &&  $data['purchase_date']!="0000-00-00 00:00:00"){ echo date("d M Y h:i A", strtotime($data['purchase_date']));} ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                    <label>Invoice : </label><span> <?php if(isset($data['purchase_invoice'])){ ?><a target="_blank" href="../img/purchase_invoice/<?php echo $data['purchase_invoice'];?>"><?php  echo $data['purchase_invoice'];  ?></a><?php }?></span>
                    </div>
                    
                </div>
                  <div class="table-responsive pt-4">
                    <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Sr. No.</th>
                              
                              <th>Product Name</th>
                              <th>Category</th>
                              <th>Sub Category</th>
                              <th>Verient</th>
                              <th>Price</th>
                              <th>Measure Unit</th>
                              <th>Order QTY</th>
                              <th>Availability</th>
                          </tr>
                      </thead>
                      <tbody id="showFilterData" class="purchase_requirements">
                         <?php 


        /* $sql =  "SELECT purchase_vendor_order_product_master.*,product_master.product_code,product_master.unit_measurement_id,product_master.product_category_id,product_master.product_sub_category_id,product_master.product_image,unit_measurement_master.unit_measurement_name
            FROM `purchase_vendor_order_product_master` 
            LEFT JOIN product_master ON  product_master.product_id = purchase_vendor_order_product_master.product_id
            LEFT JOIN product_category_master ON product_category_master.product_category_id = product_master.product_category_id
            LEFT JOIN unit_measurement_master ON unit_measurement_master.unit_measurement_id = product_master.unit_measurement_id
            LEFT JOIN product_sub_category_master ON  product_sub_category_master.product_sub_category_id = product_master.product_sub_category_id
            WHERE purchase_vendor_order_product_master.vendor_order_id = 1;";
                            $purchaseItem = $d->executeSql($sql,'result_array'); */
                           // print_r($purchaseItem);
                            $q=$d->selectRow("purchase_vendor_order_product_master.*,product_variant_master.product_variant_name,product_category_master.category_name,product_sub_category_master.sub_category_name,product_master.product_code,product_master.unit_measurement_id,product_master.product_name,product_master.product_category_id,product_master.product_sub_category_id,product_master.product_image,unit_measurement_master.unit_measurement_name","purchase_vendor_order_product_master LEFT JOIN product_master ON  product_master.product_id = purchase_vendor_order_product_master.product_id LEFT JOIN product_category_master ON product_category_master.product_category_id = product_master.product_category_id
                            LEFT JOIN product_sub_category_master ON product_sub_category_master.product_sub_category_id = product_master.product_sub_category_id LEFT JOIN unit_measurement_master ON unit_measurement_master.unit_measurement_id = product_master.unit_measurement_id LEFT JOIN product_variant_master ON product_variant_master.product_variant_id = purchase_vendor_order_product_master.product_variant_id",
                            "purchase_vendor_order_product_master.vendor_order_id=$vendor_order_id");
                            $counter = 1;
                            while ($purchaseItem=mysqli_fetch_array($q)) {
                             
                          ?>
                            <tr>
                                <td><?php echo $counter++; ?></td>
                                
                                <td><?php echo $purchaseItem['product_name']; ?></td>
                                <td><?php echo $purchaseItem['category_name']; ?></td>
                                <td><?php echo $purchaseItem['sub_category_name']; ?></td>
                                <td><?php echo $purchaseItem['product_variant_name']; ?></td>
                                <td><?php echo $purchaseItem['price']; ?></td>
                                <td><?php echo $purchaseItem['unit_measurement_name']; ?></td>
                               <td><?php echo $purchaseItem['quantity']; ?></td>
                               <td><?php if($purchaseItem['availability_status']==0){?>
                                <span class="badge badge-success">Available</span>
                                <?php }else{ ?>
                                <span class="badge badge-warning ">Unavailable</span>
                                <?php } ?></td>
                                
                            </tr>
                          <?php } ?>     
                      </tbody>
                      
                    </table>
                  </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div><!--End content-wrapper-->

<div class="modal fade" id="addRequirementsModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Requirements</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
        <form id="addRequirements">
           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Category <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <select  type="text" required="" class="form-control single-select" id="product_category_id" name="product_category_id" onchange="getSubCategoryById(this.value);">
                        <option value="">-- Select --</option> 
                        <?php 
                            $floor=$d->select("product_category_master","society_id='$society_id' ");  
                            while ($floorData=mysqli_fetch_array($floor)) {
                        ?>
                        <option <?php if(isset($data['product_category_id']) && $data['product_category_id'] ==$floorData['product_category_id']){ echo "selected"; } ?> value="<?php if(isset($floorData['product_category_id']) && $floorData['product_category_id'] !=""){ echo $floorData['product_category_id']; } ?>"><?php if(isset($floorData['category_name']) && $floorData['category_name'] !=""){ echo $floorData['category_name']; } ?></option> 
                        <?php } ?>
                    </select>                  
                </div>
           </div> 

           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Sub Category <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <select  type="text" required="" class="form-control single-select" id="product_sub_category_id" name="product_sub_category_id" onchange="getProductByCategoryAndSubCategoryId(this.value);">
                        <option value="">-- Select --</option> 
                        
                    </select>                  
                </div>
           </div> 
           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Product <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <select  type="text" required="" class="form-control single-select" id="product_id" name="product_id">
                        <option value="">-- Select --</option> 
                        
                    </select>                  
                </div>
           </div>                
           
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">QTY <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
               <input type="text" class="form-control" placeholder="QTY" id="quantity" name="quantity" value="<?php if($data['product_brand'] !=""){ echo $data['product_brand']; } ?>">
               </div>                   
           </div> 
                             
           <div class="form-footer text-center">
             <button id="addSiteBtn" name="addSiteBtn" type="submit" class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
             <input type="hidden" id="key" value="">
             <input type="hidden" id="product_old_id" value="">
             <button id="addSiteBtn" type="submit"  class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
           </div>

        </form>
      
       </div>
      </div>
      
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">

function addRequirements(){
  var site_id = $('#site_id').val();
  var vendor_id = $('#vendor_id').val();
  var po_number = $('#po_number').val();
  if(site_id != '' && vendor_id != '' && po_number != ''){
    $('#key').val('add');
  $('#addRequirements')[0].reset();
  //$('#product_category_id').select2('refresh');
  $("#product_category_id").val('').trigger('change.select2'); 
  $("#product_sub_category_id").val('').trigger('change.select2'); 
  $("#product_id").val('').trigger('change.select2');
  $('#addRequirementsModal').modal();
  }else{
    swal(
      'Warning !',
      'Please Select Site, Vendor and PO Number !',
      'warning'
    );
  }
}

function getSubCategoryById(product_category_id, product_sub_category_id='')
{
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getSubCategory",
                product_category_id:product_category_id,
              },
            success: function(response){
              var main_content ="";
              main_content=`<option value="">-- Select --</option>`;
              $.each(response.sub_category, function( index, value ) {
                var selected = "";
                if(product_sub_category_id==value.product_sub_category_id)
                {
                  var selected = "selected";
                }
                main_content +=`<option `+selected+` value="`+value.product_sub_category_id+`">`+value.sub_category_name+`</option>`;
              });
              
             $('#product_sub_category_id').html(main_content);
            }
    })
}


function getProductByCategoryAndSubCategoryId(product_sub_category_id, product_id='')
{
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getProductByCategoryAndSubCategoryId",
                //product_category_id:product_category_id,
                product_sub_category_id:product_sub_category_id,
              },
            success: function(response){
              var main_content ="";
              main_content=`<option value="">-- Select --</option>`;
              $.each(response.product, function( index, value ) {
                var selected = "";
                 if(value.product_id == product_id)
                {
                  var selected = "selected";
                } 
                main_content +=`<option `+selected+` value="`+value.product_id+`">`+value.product_name+`</option>`;
              });
              
             $('#product_id').html(main_content);
            }
  })
}

function editRequirements(id,quantity){
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getProduct",
                product_id :id,
              },
            success: function(response){
                $('#addRequirementsModal').modal();
                $('#quantity').val(quantity);

                $('#product_category_id').val(response.product.product_category_id);
                $('#product_category_id').select2();
                getSubCategoryById(response.product.product_category_id, response.product.product_sub_category_id);
                $('#product_sub_category_id').select2();
                getProductByCategoryAndSubCategoryId(response.product.product_sub_category_id, response.product.product_id);
                $('#product_id').select2();
                $('#key').val('edit');
                $('#product_old_id').val(id);
              }
            });
  
}




function popitup(url) {
  newwindow=window.open(url,'name','height=800,width=900, location=0');
  if (window.focus) {newwindow.focus()}
    return false;
}
</script>
<style>
.hideupdate{
  display:none;
}

</style>