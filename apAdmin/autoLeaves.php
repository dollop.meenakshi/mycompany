<?php error_reporting(0);
  $currentYear = date('Y');

  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  if(isset($_GET['year']) && isset($_GET['month'])){
    $year = (int)$_REQUEST['year'];
    $month = (int)$_REQUEST['month'];
  }else {
    $month  =  date('m');
    $year  = date('Y');
  }
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12 col-md-12">
          <h4 class="page-title">Auto Leaves For Late In / Early Out</h4>
        </div>
     </div>
     <form class="branchDeptFilter" action="" method="get">
        <div class="row pt-2 pb-2">
          <?php include 'selectBranchDeptEmpForFilterAll.php' ?>
            <div class="col-md-2 col-6 form-group">
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-60 days'));} ?>">  
          </div>
          <div class="col-md-2 col-6 form-group">
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
          </div>
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
            </div>
        </div>
    </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <?php
                      $i = 1;
                      $yearFilterQuery = " AND DATE_FORMAT(leave_start_date,'%Y') = '$currentYear'";
                      if(isset($bId) && $bId>0) {
                        $blockFilterQuery = " AND users_master.block_id='$bId'";
                      }
                      if(isset($dId) && $dId>0) {
                          $deptFilterQuery = " AND users_master.floor_id='$dId'";
                      }
                      if(isset($uId) && $uId>0) {
                          $userFilterQuery = " AND users_master.user_id='$uId'";
                      }
                      
                      if(isset($from) && isset($from) && $toDate != '' && $toDate) {
                          $dateFilterQuery = " AND leave_master.leave_start_date BETWEEN '$from' AND '$toDate'";
                      }

                      // if(isset($year) && $year>0) {
                      //   $yearFilterQuery = " AND DATE_FORMAT(leave_start_date,'%Y') = '$year'";
                      // }
                      
                      $q = $d->selectRow("users_master.user_full_name,users_master.floor_id,users_master.block_id,leave_type_master.leave_type_name,bms_admin_master.admin_name,leave_master.*,u.user_full_name AS approve_by,block_master.block_name,floors_master.floor_name,users_master.user_designation","block_master,floors_master,users_master,attendance_master,leave_master LEFT JOIN leave_type_master ON leave_type_master.leave_type_id=leave_master.leave_type_id LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=leave_master.leave_approved_by LEFT JOIN users_master AS u ON u.user_id=leave_master.leave_approved_by","leave_master.leave_type_id=0 AND  users_master.user_id=leave_master.user_id AND users_master.delete_status=0 AND leave_master.society_id='$society_id' AND users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND attendance_master.user_id=leave_master.user_id AND attendance_master.attendance_date_start=leave_master.leave_start_date AND attendance_master.auto_leave=1 $blockFilterQuery $dateFilterQuery $deptFilterQuery $userFilterQuery  $blockAppendQueryUser", "ORDER BY leave_master.leave_id DESC LIMIT 100");
                      $counter = 1;                      
                    ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Action</th>
                        <th>Employee</th>
                        <th>Branch (Department)</th>
                        <th>Leave Date</th>
                        <th>Type</th>
                        <th>Day Type</th>
                        <th>Paid/Unpaid</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      while ($data = mysqli_fetch_array($q)) {
                    ?>
                    <tr>
                       <td><?php echo $counter++; ?></td>
                       <td>
                         <div class="d-flex align-items-center">
                            <a data-toggle="tooltip" title="Leave Balance" href="leaveAssign?bId=<?php echo $data['block_id'];?>&dId=<?php echo $data['floor_id'];?>&uId=<?php echo $data['user_id'];?>&ltId=&year=<?php echo date("Y", strtotime($data['leave_start_date']));?>" class="btn btn-sm btn-primary ml-1"><i class="fa fa-angellist"></i></a>

                            <button type="button" class="btn btn-sm btn-primary ml-1" onclick="LeaveModal(<?php echo $data['leave_id']; ?>, <?php echo $data['society_id']; ?>)" >
                            <i class="fa fa-eye"></i>
                            </button>
                            <?php if($data['is_salary_generated'] == 0){ ?>
                            <button type="button" class="btn btn-sm btn-primary ml-1" onclick="updateAutoLeave(`<?php echo $data['user_full_name'].' ('.$data['user_designation'].')'; ?>`,'<?php echo $data['leave_id']; ?>', '<?php echo $data['user_id']; ?>', '<?php echo $data['leave_start_date']; ?>', '<?php echo $data['paid_unpaid']; ?>', '<?php echo date('Y', strtotime($data['leave_start_date'])); ?>','<?php echo date('d M Y', strtotime($data['leave_start_date'])); ?>','<?php echo $data['leave_day_type']; ?>');"><i class="fa fa-pencil"></i></button>
                            <?php } ?>
                         </div>
                        </td>
                        <td><?php echo $data['user_full_name']; ?> (<?php custom_echo($data['user_designation'],20); ?>)</td>
                       <td><?php echo $data['block_name']; ?> (<?php echo $data['floor_name']; ?>)</td>
                       <td><?php echo date("d M Y", strtotime($data['leave_start_date'])); ?> (<?php echo date("D", strtotime($data['leave_start_date'])); ?>)</td>
                       <td><?php echo "Auto"; ?></td>
                       <td><?php 
                          if($data['leave_day_type'] == 0){
                              echo "Full Day";
                          }else{
                              echo "Half Day";
                          }
                        ?></td>
                        <td><?php 
                          if($data['paid_unpaid'] == 0){
                              echo "Paid";
                          }else{
                              echo "Unpaid";
                          }
                        ?></td>
                       
                        <td><?php if($data['auto_leave_reason']!=""){ echo $data['auto_leave_reason']; } else { echo $data['leave_reason'];  } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>

            </table>
            
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

</div>


<div class="modal fade" id="leaveDetailModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Leave Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="leaveData" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="updateAutoLeaveModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Update Leave Type</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
          <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="updateAutoLeaveForm" action="controller/leaveController.php" enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Leave Type <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <select name="leave_type_id" id="leave_type_id" class="form-control single-select" onchange="getLeaveBalanceForAutoLeave(this.value)">
                            <option value="">-- Select Leave Type --</option> 
                              <?php 
                                $qLeaveType=$d->select("leave_type_master","society_id='$society_id' AND leave_type_delete=0");  
                                while ($qLeaveTypeData=mysqli_fetch_array($qLeaveType)) {
                              ?>
                              <option  <?php if($ltId==$qLeaveTypeData['leave_type_id']) { echo 'selected';} ?> value="<?php echo  $qLeaveTypeData['leave_type_id'];?>" ><?php echo $qLeaveTypeData['leave_type_name'];?></option>
                            <?php } ?>
                          </select>
                        </div>                   
                    </div>  
                    <div class="form-group row">
                      <p class="col-sm-12 text-danger">Total Leave Balance (Year): <span id="balance_leave"></span></p>
                      <p class="col-sm-12 text-danger">Applicable Leave Till Date <span class="leave_date"></span>: <span id="applicable_leave"></span></p>
                    </div>  
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Mode <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <select name="paid_unpaid" class="form-control paid_unpaid">
                              <option value="">-- Select Mode --</option> 
                              <option value="0">Paid Leave</option> 
                              <option value="1">Unpaid Leave</option> 
                          </select>
                        </div>                   
                    </div>               
                    <div class="form-footer text-center">
                      <input type="hidden" name="updateAutoLeave"  value="updateAutoLeave">
                      <input type="hidden" name="user_full_name" class="user_full_name">
                      <input type="hidden" name="leave_id" class="leave_id">
                      <input type="hidden" name="leave_day_type" class="leave_day_type">
                      <input type="hidden" name="leave_date" id="leave_date">
                      <input type="hidden" name="show_leave_date" id="show_leave_date">
                      <input type="hidden" name="user_id" id="user_id">
                      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Update </button> 
                      <button type="button" class="btn btn-sm btn-danger leave_id" onclick="removeUserApprovedLeave()"><i class="fa fa-trash-o fa-lg"></i> Remove Leave </button>
                    </div>
                </form>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

