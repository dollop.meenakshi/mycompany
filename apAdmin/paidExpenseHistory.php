<?php
error_reporting(0);
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$ueMonth = $_REQUEST['ueMonth'];
$ueYear = $_REQUEST['ueYear'];
$hId = $_REQUEST['hId'];
$q = $d->selectRow('user_expense_history.*','user_expense_history',"user_expense_history_id='$hId'");
$hdata = mysqli_fetch_assoc($q);
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-md-12">
                <h4 class="page-title">Paid Expenses History</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="card-body">
                        <label>Total Amount :</label><i><?php echo $hdata['amount']; ?></i>
                        <div class="table-responsive">
                            <?php
                            $i = 1;
                            if(isset($dId) && $dId > 0)
                            {
                                $deptFilterQuery = " AND user_expenses.floor_id = '$dId'";
                            }
                            if(isset($ueYear) && $ueYear > 0)
                            {
                                $yearFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%Y') = '$ueYear'";
                            }
                            if(isset($ueMonth) && $ueMonth > 0)
                            {
                                $monthFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%m') = '$ueMonth'";
                            }
                            $q = $d->selectRow("user_expenses.*,floors_master.*,users_master.*,ecm.expense_category_name,ecm.expense_category_type,ecm.expense_category_unit_name,ecm.expense_category_unit_price","user_expenses JOIN floors_master ON user_expenses.floor_id = floors_master.floor_id JOIN users_master ON users_master.user_id = user_expenses.user_id LEFT JOIN expense_category_master AS ecm ON FIND_IN_SET(ecm.expense_category_id, user_expenses.expense_category_id) > 0","user_expenses.society_id = '$society_id' AND user_expenses.expense_paid_status = 1 AND users_master.delete_status = 0 AND user_expenses.user_expense_id IN ($hdata[expense_id]) $deptFilterQuery $yearFilterQuery $monthFilterQuery $blockAppendQueryUser","ORDER BY user_expenses.user_expense_id ASC");
                            $counter = 1;
                            ?>
                            <table id="exampleReport" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Department</th>
                                        <th>Employee</th>
                                        <th>Expense Title</th>
                                        <th>Amount</th>
                                        <th>Expense Category Name</th>
                                        <th>Expense Category Type</th>
                                        <th>Unit</th>
                                        <th>Unit Name</th>
                                        <th>Unit Price</th>
                                        <th>Date</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php while ($data = mysqli_fetch_array($q)) { ?>
                                    <tr>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['floor_name']; ?> </td>
                                        <td><?php echo $data['user_full_name']; ?>(<?php echo $data['user_designation']; ?>)</td>
                                        <td><?php echo $data['expense_title']; ?></td>
                                        <td><?php echo $data['amount']; ?></td>
                                        <td><?php echo $data['expense_category_name']; ?></td>
                                        <td><?php echo ($data['expense_category_type'] == 1) ? 'Unit Wise' : 'Amount Wise'; ?></td>
                                        <td><?php echo ($data['unit'] != "0.00") ? $data['unit'] : ""; ?></td>
                                        <td><?php echo $data['expense_category_unit_name']; ?></td>
                                        <td><?php echo ($data['expense_category_unit_price'] != "0.00") ? $data['expense_category_unit_price'] : ""; ?></td>
                                        <td><?php echo date("d M Y", strtotime($data['date'])); ?></td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-primary mr-1 pd-1" onclick="employeeExpensesDetail(<?php echo $data['user_expense_id']; ?>)" >
                                            <i class="fa fa-eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<div class="modal fade" id="employeeExpensesModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Employee Expenses</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="employeeExpensesData" style="align-content: center;">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expensePayModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Expense Pay Detail</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="billPayDiv" style="align-content: center;">
                <div class="card-body">
                    <form id="payExpenseAmountForm" action="controller/ExpenseSettingController.php" enctype="multipart/form-data" method="post">
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Expense Payment Mode <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" class="form-control" placeholder="Expense Payment Mode" id="expense_payment_mode" name="expense_payment_mode" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Reference Number <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" class="form-control" placeholder="Reference Number" id="reference_no" name="reference_no" value="">
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" id="user_expense_id" name="user_expense_id" value="" >
                            <input type="hidden" name="payExpenseAmount"  value="payExpenseAmount">
                            <button id="payExpenseAmountBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                            <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('payExpenseAmountForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function expensePay(id){
$('#user_expense_id').val(id);
//$('#addAttendaceModal').modal();
$('#expensePayModal').modal();
}
</script>