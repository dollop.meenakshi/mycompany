<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="signupForm" action="controller/menuController.php" method="post" >
                <?php 
                   if(isset($_POST['editRole'])) {
                    extract(array_map("test_input" , $_POST));
                    $q=$d->select("role_master","role_id='$role_id'");
                    $data=mysqli_fetch_array($q);
                    }
                   ?>
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-lock"></i>
                  Role
                </h4>                   
                <input type="hidden" name="societyId" value="<?php echo $data['society_id']; ?>">
                <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label">Role Name  <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <?php if(isset($_POST['editRole'])) { ?>
                        <input type="hidden" name="role_id" value="<?php echo $data['role_id']; ?>">
                        <input maxlength="30" type="text" name="role_nameEdit" class="form-control" required data-validation-required-message="Enter Role  Name"  value="<?php echo $data['role_name']; ?>">
                    <?php } else { ?>
                        <input maxlength="30" type="text" name="role_name" class="form-control" required data-validation-required-message="Enter Role Name">
                    <?php } ?>
                  </div>
                  <label for="input-11" class="col-sm-2 col-form-label"> Description</label>
                  <div class="col-sm-4" >
                    <input maxlength="100" type="text" name="role_description" class="form-control"    value="<?php echo $data['role_description']; ?>">
                  </div>
                </div>
                
                <div class="form-group row">
                     
                  <?php //if ($adminData['admin_type']==1  || $adminData['role_id']==2) { ?>
                 
                  <div class="col-sm-6 offset-3" >
                    <div class="controls">
                    <div class="text-center">
                      <h6>
                          <?php echo $xml->string->main_menu; ?> <?php echo $xml->string->permissions; ?>
                      </h6>
                        <input type="checkbox" id="user-checkbox" class="chk_boxes pagePrivilege" />
                          <label for="user-checkbox"><?php echo $xml->string->check_uncheck; ?> </label>
                          
                    </div>
                      
                      <?php
                     
                        $q11=$d->select("role_master","role_id='$adminData[role_id]' AND admin_type=1 OR role_id=2");
                        $privilegeData=mysqli_fetch_array($q11);
                        $menuPrivilege=$privilegeData['menu_id'];
                        $pagePrivilege=$privilegeData['pagePrivilege'];
                        $menuPrivilege=explode(",", $menuPrivilege);
                        $pagePrivilege=explode(",", $pagePrivilege);
                        
                      $menuId=$data['menu_id'];
                      $menuId=explode(",", $menuId);
                      $pagePrivilegeId=$data['pagePrivilege'];
                      $pagePrivilegeId=explode(",", $pagePrivilegeId);

                    $i=1;
                    $q1=$d->select("master_menu","","ORDER BY order_no ASC");
                   
                    while ($dataMenu=mysqli_fetch_array($q1)) {
                      $menu_id=$dataMenu['menu_id'];
                      //print_r($menu_id);
                      if($dataMenu['sub_menu']==0) { 
                        if($dataMenu['parent_menu_id']==0 && $dataMenu['page_status'] == 0){ ?>
                        <div class="card-body">
                        <ul class="list-group">
                          <?php
                          if(in_array($menu_id, $menuPrivilege)){?>
                          <!-- Menu Name that do not have any sub menu -->
                          <li class="list-group-item list-group-item-primary">
                          <label  style="margin-left: -30px;" class="custom-control custom-checkbox error_color">
                            <input <?php if(in_array($menu_id, $menuId)){ echo "checked"; } ?> type="checkbox" class="pagePrivilege" value="<?php echo $dataMenu['menu_id']; ?>" name="menu_id[]">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description"><b><?php echo $dataMenu['menu_name']; ?></b></span>
                          </label>
                          </li>
                        <?php }else if($adminData['role_id']==1){ ?>
                          <li class="list-group-item list-group-item-primary"> 
                          <label  style="margin-left: -30px;" class="custom-control custom-checkbox error_color">
                            <input <?php if(in_array($menu_id, $menuId)){ echo "checked"; } ?> type="checkbox" class="pagePrivilege" value="<?php echo $dataMenu['menu_id']; ?>" name="menu_id[]">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description"><b><?php echo $dataMenu['menu_name']; ?></b></span>
                          </label>
                          </li>
                        <?php } //} if($dataMenu['parent_menu_id']==0 && $dataMenu['page_status'] == 0){ 
                          $pageq=$d->select("master_menu","parent_menu_id='$menu_id' AND page_status=1"); ?>
                          <?php while ($pageData=mysqli_fetch_array($pageq)) {
                            $page_id=$pageData['menu_id'];
                            if(in_array($page_id, $pagePrivilege)){
                            ?>
                            <!-- page name -->
                            <li class="list-group-item list-group-item-secondary">
                            <label class="custom-control custom-checkbox error_color">
                              <input <?php if(in_array($page_id, $pagePrivilegeId)){ echo "checked"; } ?> type="checkbox" class="pagePrivilege" value="<?php echo $pageData['menu_id']; ?>" name="pagePrivilege[]">
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description"><b><?php echo $pageData['menu_name']; ?></b></span>
                            </label>
                            </li>
                          <?php }else if($adminData['role_id']==1){ ?> 
                            <li class="list-group-item list-group-item-secondary">
                            <label class="custom-control custom-checkbox error_color">
                              <input <?php if(in_array($page_id, $pagePrivilegeId)){ echo "checked"; } ?> type="checkbox" class="pagePrivilege" value="<?php echo $pageData['menu_id']; ?>" name="pagePrivilege[]">
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description"><b><?php echo $pageData['menu_name']; ?></b></span>
                            </label>
                            </li>
                          <?php } } ?> 
                          </ul>
                          </div>
                        <?php }?>
                          
                      <?php  } else{ 
                        if($dataMenu['sub_menu']==1) { 
                          $ids = join("','",$menuPrivilege); 
                          $pageIds = join("','",$pagePrivilege); 
                          if($adminData['role_id'] != 1){
                            $appendMenuQuery = " AND menu_id IN ('$ids')";
                            $appendPageQuery = " AND menu_id IN ('$pageIds')";
                          }
                          $subMenuq=$d->select("master_menu","parent_menu_id='$menu_id' AND page_status=0 $appendMenuQuery");
                          $s=$d->select("master_menu","parent_menu_id='$menu_id' AND page_status=1 $appendPageQuery");
                          
                         
                          if(mysqli_num_rows($subMenuq) || mysqli_num_rows($s)){
                          ?>
                        <div class="card-body">
                          <ul class="list-group">
                          <!-- Menu Name that have any sub menu -->
                          <li class="list-group-item list-group-item-primary">                          
                            <label   class=" custom-checkbox error_color">
                            <span class="custom-control-description"><b><?php echo $dataMenu['menu_name']; ?></b></span>
                          </label>
                          </li>
                          <!-- sub menu name -->
                          <?php
                          
                          while ($subMeneData=mysqli_fetch_array($subMenuq)) {
                          $sub_menu_id=$subMeneData['menu_id']; 
                          if(in_array($sub_menu_id, $menuPrivilege)){ ?>
                          <li class="list-group-item list-group-item-success">
                            <label class="custom-control custom-checkbox error_color">
                              <input <?php if(in_array($sub_menu_id, $menuId)){ echo "checked"; } ?> type="checkbox" class="pagePrivilege" value="<?php echo $subMeneData['menu_id']; ?>" name="menu_id[]">
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description"><?php echo $subMeneData['menu_name']; ?></span>
                            </label>
                          </li>
                          <?php }else if($adminData['role_id']==1){?>
                            <li class="list-group-item list-group-item-success">
                            <label class="custom-control custom-checkbox error_color">
                              <input <?php if(in_array($sub_menu_id, $menuId)){ echo "checked"; } ?> type="checkbox" class="pagePrivilege" value="<?php echo $subMeneData['menu_id']; ?>" name="menu_id[]">
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description"><?php echo $subMeneData['menu_name']; ?></span>
                            </label>
                            </li>
                          <?php } }?>
                          <!-- page name -->
                          <?php
                          while ($sdata=mysqli_fetch_array($s)) {
                          $page_id=$sdata['menu_id']; 
                          if(in_array($page_id, $pagePrivilege)){
                          ?>
                          <li class="list-group-item list-group-item-secondary">
                            <label class="custom-control custom-checkbox error_color">
                              <input <?php if(in_array($page_id, $pagePrivilegeId)){ echo "checked"; } ?>  type="checkbox" class="pagePrivilege" value="<?php echo $sdata['menu_id']; ?>" name="pagePrivilege[]">
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description"><?php echo $sdata['menu_name']; ?></span>
                            </label>
                            </li>
                          <?php }else if($adminData['role_id']==1){?>
                            <li class="list-group-item list-group-item-secondary">
                            <label class="custom-control custom-checkbox error_color">
                              <input <?php if(in_array($page_id, $pagePrivilegeId)){ echo "checked"; } ?>  type="checkbox" class="pagePrivilege" value="<?php echo $sdata['menu_id']; ?>" name="pagePrivilege[]">
                              <span class="custom-control-indicator"></span>
                              <span class="custom-control-description"><?php echo $sdata['menu_name']; ?></span>
                            </label>
                          </li>
                          <?php } }?>
                        </ul>
                      </div>
                      <?php } }?>
                      <?php  }?>
                    <?php } ?>
                    </div>
                  </div>
                
                  

                 
                  <?php  //}?>
                  

                </div>
                
                <div class="form-footer text-center">
                   <?php if(isset($_POST['editRole'])) { ?>
                    <button type="submit" name="updateRole" class="btn btn-success"><i class="fa fa-check-square-o"></i> UPDATE</button>
                  <?php } else { ?>
                    <button name="addRole" value="add Role" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> ADD</button>
                  <?php } ?>
                    <button  type="reset" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
  <!--select icon modal -->
    <script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">

  $(function() {

    $('.chk_boxes').click(function() {

        $('.pagePrivilege').prop('checked', this.checked);

    });

});

</script>