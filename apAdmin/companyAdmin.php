<style>
  .photoInput ,.idInput  {
    display: none;
  }  
</style>

<div class="content-wrapper">
    <div class="container-fluid">
   
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <?php 


              if(isset($_POST['admin_id_edit'])) {
              extract(array_map("test_input" , $_POST));
              $q=$d->select("bms_admin_master,role_master","role_master.role_id=bms_admin_master.role_id AND  bms_admin_master.admin_id='$admin_id_edit' AND bms_admin_master.society_id='$society_id' AND bms_admin_master.admin_active_status=0");
              $data=mysqli_fetch_array($q);
              $role_id=$data['role_id'];
              $access_branchs=$data['access_branchs'];
              $access_departments=$data['access_departments'];

               $blockAry = explode(",", $access_branchs);
               $block_ids = join("','",$blockAry); 
               $departmentAry = explode(",", $access_departments);
              
               $notAccAry = array();
              $notCheck1=$d->select("bms_admin_notification_master","admin_id='$admin_id_edit' ");
              while ($oldBlock=mysqli_fetch_array($notCheck1)) {
                  array_push($notAccAry , $oldBlock['admin_notification_id']);
              }

               ?>  
              <form id="addAdmin" action="controller/buildingController.php" method="post" enctype="multipart/form-data">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-building"></i>
                   <?php echo $xml->string->society; ?> <?php echo $xml->string->committee; ?> <?php echo $xml->string->add; ?>
                </h4>
                <input type="hidden" name="admin_id_edit" value="<?php echo $data['admin_id']; ?>">
                <!-- <input type="hidden" name="role_id_edit" value="<?php echo $data['role_id']; ?>"> -->
                <input type="hidden" name="admin_profile_old" value="<?php echo $data['admin_profile']; ?>">
                <div class="form-group row">
                   <div class="col-sm-12 text-center">
                    <label for="imgInp">
                       <img id="blah" style="border: 1px solid gray;" src="<?php if(file_exists("../img/society/$data[admin_profile]"))  { echo '../img/society/'.$data['admin_profile']; }  else { echo 'img/user.png'; } ?>"  width="100" height="100"   src="#" alt="your image" class='rounded-circle' />
                       <i style="position: absolute;font-size: 20px;margin-left: -16px;margin-top: 66px;" class="fa fa-camera"></i>
                     <input accept="image/*" class="photoInput photoOnly" id="imgInp" type="file" name="admin_profile">
                   </label>
                   </div>
                </div>
                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label"><?php echo $xml->string->full_name; ?> <span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" maxlength="50" required="" class="form-control  onlyName" id="input-12" name="admin_name_edit" value="<?php echo $data['admin_name'] ?>">
                  </div>
                  
                  
                </div>
                <div class="form-group row">
                  <label for="Country Code " class="col-sm-2 col-form-label"> <?php echo $xml->string->country_code; ?>  <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="hidden" value="<?php echo $data['country_code']; ?>" id="country_code_get" name="">
                    <select name="country_code" class="form-control single-select" id="country_code" required="">
                    <?php include 'country_code_option_list.php'; ?>
                    </select>
                  </div>
                  <label for="no_of_option" class="col-sm-2 col-form-label"> <?php echo $xml->string->mobile_no; ?> <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" maxlength="10" required="" id="secretary_mobile" inputmode="numeric" onblur="checkMobileSocietyEdit()" class="form-control"  name="admin_mobile"  value="<?php echo $data['admin_mobile'] ?>">
                    <input type="hidden" id="secretary_mobile_old"  maxlength="12" value="<?php echo $data['admin_mobile']; ?>" required="" class="form-control" >

                  </div>
                </div>
               <div class="form-group row">
                 
                  <label for="input-13" class="col-sm-2 col-form-label"> <?php echo $xml->string->email_id; ?>  <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="email" maxlength="60" id="secretary_email" required="" onblur="checkemailSocietyEdit()" class="form-control" id="input-13" name="admin_email"  value="<?php echo $data['admin_email'] ?>">
                    <input type="hidden" maxlength="80" id="secretary_email_old" required="" value="<?php echo $data['admin_email']; ?>" class="form-control" id="input-13" >
                  </div>
                  <label for="input-role" class="col-sm-2 col-form-label"><?php echo $xml->string->role_name; ?> <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <!-- <input type="text" maxlength="20" required="" value="<?php echo $data['role_name'] ?>" class="form-control text-capitalize" id="input-role" name="role_name"> -->
                    <select name="role_id" id="role_id" class="form-control single-select" required="">
                      <option value="">-- Select Role --</option> 
                        <?php 
                          $qRole=$d->select("role_master","society_id='$society_id' AND role_status=0 AND role_id>2");  
                          while ($roleData=mysqli_fetch_array($qRole)) {
                        ?>
                      <option  <?php if($data['role_id']==$roleData['role_id']) { echo 'selected';} ?> value="<?php echo $roleData['role_id'];?>" ><?php echo $roleData['role_name'];?> <?php if($roleData['role_id']==2) { echo " (Common Role)"; } else if ($roleData['admin_type']==1) {
                      echo " (Main Role)";
                    }?></option>
                      <?php } ?>
                    </select>
                  </div>
                  
                </div>
                 <div class="form-group row" >
                 
                  <label for="password-field" class="col-sm-2 col-form-label"><?php echo $xml->string->access; ?> <?php echo $xml->string->blocks; ?> </label>
                  <div class="col-sm-10">
                      <select  type="text"  onchange="getDepartmentForAdmin();" class="form-control multiple-select-block accessAdminBlocks" name="access_branchs[]" multiple="multiple">
                        <option value="">-- <?php echo $xml->string->all; ?> --</option>
                        <?php 
                          $q3=$d->select("block_master","society_id='$society_id'","");
                           while ($blockRow=mysqli_fetch_array($q3)) {
                          if(isset($_POST['admin_id_edit'])) {
                         ?>
                          <option <?php if(in_array($blockRow['block_id'], $blockAry)){ echo 'selected'; } ?>   value="<?php echo $blockRow['block_id'];?>"><?php echo $blockRow['block_name'];?> </option>
                        <?php }  else {?>
                          <option  value="<?php echo $blockRow['block_id'];?>"><?php echo $blockRow['block_name'];?> </option>
                          <?php } }?>
                        </select>
                        <i><?php echo $xml->string->keep_blank_if_you_want_to_all; ?> <?php echo $xml->string->blocks; ?> <?php echo $xml->string->access; ?> </i>
                  </div>
                  
                </div>

                <div class="form-group row all-access department-wise ">    
                    <label for="input-10" class="col-lg-2  col-form-label"><?php echo $xml->string->access; ?> <?php echo $xml->string->floors; ?> </label>
                    <div class="col-lg-10 col-md-10" id="">
                        <select type="text" id="dwfloorId" class="form-control multiple-select-block dwfloorId" multiple name="access_departments[]">
                           <option value=""><?php echo $xml->string->all; ?> <?php echo $xml->string->floors; ?></option>
                            <?php 
                                $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') ");  
                                while ($depaData=mysqli_fetch_array($qd)) {
                            ?>
                            <option <?php if(in_array($depaData['floor_id'],$departmentAry)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
                            <?php } ?>
                        </select>
                    </div>                   
                </div>
                <div class="form-group row">
                  <label for="input-role" class="col-sm-2 col-form-label"><?php echo $xml->string->complaint; ?> <?php echo $xml->string->category; ?>  </label>
                  <div class="col-sm-10">
                    <select  type="text"  class="form-control multiple-select" name="complaint_category_id[]" multiple="multiple">
                      <option value="">-- <?php echo $xml->string->all; ?> --</option>
                      <?php 
                       $comCatIdAry = explode(",", $data['complaint_category_id']);
                        $q3=$d->select("complaint_category","active_status=0","");
                         while ($blockRow=mysqli_fetch_array($q3)) {
                       ?>
                        <option <?php if(in_array($blockRow['complaint_category_id'], $comCatIdAry)){ echo 'selected'; } ?>  value="<?php echo $blockRow['complaint_category_id'];?>"><?php echo $blockRow['category_name'];?> </option>
                        <?php }?>
                      </select>
                      <i>Selected category receive notification </i>
                  </div>
                  
                </div>
                <div class="form-group row">
                  <label for="input-role" class="col-sm-2 col-form-label"><?php echo $xml->string->members; ?> <?php echo $xml->string->block_type; ?> </label>
                  <div class="col-sm-4">
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" checked=""  <?php if($data['user_type']=='0'){echo "checked";} ?> class="form-check-input" value="0" name="user_type"> <?php echo $xml->string->committee; ?>
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio"  <?php if($data['user_type']=='1'){echo "checked";} ?>  class="form-check-input" value="1" name="user_type"> <?php echo $xml->string->representative; ?>
                      </label>
                    </div>
                  </div>
                  <label for="input-role" class="col-sm-2 col-form-label">Mobile Number Private</label>
                  <div class="col-sm-4">
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" checked="" <?php if($data['mobile_private']=='0'){echo "checked";} ?> class="form-check-input" value="0" name="mobile_private"> No
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" <?php if($data['mobile_private']=='1'){echo "checked";} ?> class="form-check-input" value="1" name="mobile_private"> Yes
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-role" class="col-sm-2 col-form-label">Report Download Access</label>
                  <div class="col-sm-4">
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" checked="" <?php if($data['report_download_access']=='0'){echo "checked";} ?> class="form-check-input" value="0" name="report_download_access"> No
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" <?php if($data['report_download_access']=='1'){echo "checked";} ?> class="form-check-input" value="1" name="report_download_access"> Yes
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-notificationAccess-access" class="col-lg-2 col-form-label">Admin App Notifications</label>
                  <div class="col-lg-10">
                    <select class="form-control multiple-select" multiple name="notificationAccess[]">
                      <?php $ns=$d->select("admin_notification_master","active_status=0");
                      while ($ndata=mysqli_fetch_array($ns)) {
                      ?>
                      <option <?php if(in_array($ndata['admin_notification_id'], $notAccAry)){ echo "selected"; } ?> value="<?php echo $ndata['admin_notification_id']; ?>"><?php echo $ndata['title']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  
                </div>

                <div class="form-footer text-center">

                    <button type="submit" id="socAddBtn" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                </div>
              </form>
            <?php } else { 
                  extract($_POST);
                  if(isset($user_mobile)) {
                    $q11=$d->select("bms_admin_master","admin_mobile='$user_mobile'  ");
                    $data11=mysqli_fetch_array($q11);
                    if ($data11>0) {
                      $_SESSION['msg1']="This Mobile Number is Already Used";
                    }

                 }
              ?>

              <form id="addAdmin" action="controller/buildingController.php" method="post" enctype="multipart/form-data">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-building"></i>
                  <?php echo $xml->string->society; ?> <?php echo $xml->string->committee; ?>
                </h4>
                <div class="form-group row">
                   <div class="col-sm-12 text-center">
                    <label for="imgInp">
                       <img id="blah" style="border: 1px solid gray;" src="<?php if(isset($emp_profile)) { echo '../img/emp/'.$emp_profile; }  else { echo 'img/user.png'; } ?>"  width="100" height="100"   src="#" alt="your image" class='rounded-circle' />
                       <i style="position: absolute;font-size: 20px;margin-left: -16px;margin-top: 66px;" class="fa fa-camera"></i>
                     <input accept="image/*" class="photoInput photoOnly" id="imgInp" type="file" name="admin_profile">
                   </label>
                   </div>
                </div>
                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label">  <?php echo $xml->string->full_name; ?> <span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" maxlength="50" value="<?php if(isset($user_full_name)) { echo $user_full_name;} ?>" required="" class="form-control text-capitalize onlyName"  id="input-12" name="admin_name_add">
                  </div>
                  
                </div>
                <div class="form-group row">
                  <label for="Country Code " class="col-sm-2 col-form-label">  <?php echo $xml->string->country_code; ?>  <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <select name="country_code" class="form-control single-select" id="country_code" required="">
                    <?php include 'country_code_option_list.php'; ?>
                    </select>
                  </div>
                  <label for="no_of_option" class="col-sm-2 col-form-label">  <?php echo $xml->string->mobile_no; ?> <span class="required">*</span></label>

                  <div class="col-sm-4">
                    <input type="text" maxlength="10" required="" value="<?php if(isset($user_mobile)) { echo $user_mobile;} ?>" inputmode="numeric" id="secretary_mobile" onblur="checkMobileSociety()" class="form-control" id="secretary_mobile" name="admin_mobile">
                  </div>
                </div>
               <div class="form-group row">
                 
                  <label for="input-13" class="col-sm-2 col-form-label">  <?php echo $xml->string->email_id; ?> <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="email" maxlength="60" id="secretary_email" value="<?php if(isset($user_email)) { echo $user_email;} ?>" required="" onblur="checkemailSociety()" class="form-control" id="input-13" name="admin_email">
                  </div>
                
                  <label for="input-role" class="col-sm-2 col-form-label"> <?php echo $xml->string->role_name; ?> <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <!-- <input type="text" maxlength="20" required="" class="form-control text-capitalize" id="input-role" name="role_name"> -->
                    <select name="role_id" id="role_id" class="form-control single-select" required="">
                      <option value="">-- Select Role --</option> 
                        <?php 
                          $qRole=$d->select("role_master","society_id='$society_id' AND role_status=0 AND role_id>2");  
                          while ($roleData=mysqli_fetch_array($qRole)) {
                        ?>
                      <option  <?php if($data['role_id']==$roleData['role_id']) { echo 'selected';} ?> value="<?php echo $roleData['role_id'];?>" ><?php echo $roleData['role_name'];?> <?php if($roleData['role_id']==2) { echo " (Common Role)"; } else if ($roleData['admin_type']==1) {
                      echo " (Main Role)";
                    }?></option>
                      <?php } ?>
                    </select>
                  </div>
                  
                </div>

                 <div class="form-group row" >
                 
                  <label for="password-field" class="col-sm-2 col-form-label"> <?php echo $xml->string->access; ?> <?php echo $xml->string->blocks; ?> </label>
                  <div class="col-sm-10">
                      <select  type="text"  onchange="getDepartmentForAdmin();" class="form-control multiple-select-block accessAdminBlocks" name="access_branchs[]" multiple="multiple">
                        <option value=""><?php echo $xml->string->all; ?> <?php echo $xml->string->blocks; ?></option>
                        <?php 
                          $q3=$d->select("block_master","society_id='$society_id'","");
                           while ($blockRow=mysqli_fetch_array($q3)) {
                          ?>
                          <option  value="<?php echo $blockRow['block_id'];?>"><?php echo $blockRow['block_name'];?> </option>
                          <?php }?>
                        </select>
                        <i> <?php echo $xml->string->keep_blank_if_you_want_to_all; ?> <?php echo $xml->string->blocks; ?> <?php echo $xml->string->access; ?> </i>
                  </div>
                  
                </div>

                <div class="form-group row all-access department-wise ">    
                    <label for="input-10" class="col-lg-2  col-form-label"><?php echo $xml->string->access; ?> <?php echo $xml->string->floors; ?> </label>
                    <div class="col-lg-10 col-md-10" id="">
                        <select type="text" id="dwfloorId" class="form-control multiple-select-block dwfloorId" multiple name="access_departments[]">
                           <option value=""><?php echo $xml->string->all; ?> <?php echo $xml->string->floors; ?></option>
                            <?php 
                                $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') $blockAppendQuery");  
                                while ($depaData=mysqli_fetch_array($qd)) {
                            ?>
                            <option <?php if(in_array($depaData['floor_id'],$floorReportArray)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
                            <?php } ?>
                        </select>
                    </div>                   
                  </div>

                <div class="form-group row">
                  <label for="input-role" class="col-sm-2 col-form-label"> <?php echo $xml->string->complaint; ?> <?php echo $xml->string->category; ?> </label>
                  <div class="col-sm-10">
                    <select  type="text"  class="form-control multiple-select" name="complaint_category_id[]" multiple="multiple">
                      <option value="">-- <?php echo $xml->string->select; ?> --</option>
                      <?php 
                        $q3=$d->select("complaint_category","active_status=0","");
                         while ($blockRow=mysqli_fetch_array($q3)) {
                       ?>
                        <option selected="" value="<?php echo $blockRow['complaint_category_id'];?>"><?php echo $blockRow['category_name'];?> </option>
                        <?php }?>
                      </select>
                      <i>Selected category receive notification </i>
                      
                  </div>
                  
                </div>
                <div class="form-group row">
                  <label for="input-role" class="col-sm-2 col-form-label"><?php echo $xml->string->members; ?> <?php echo $xml->string->block_type; ?> </label>
                  <div class="col-sm-4">
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" checked=""  class="form-check-input" value="0" name="user_type"> <?php echo $xml->string->committee; ?>
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio"  class="form-check-input" value="1" name="user_type"> <?php echo $xml->string->representative; ?>
                      </label>
                    </div>
                  </div>
                  <label for="input-role" class="col-sm-2 col-form-label">Mobile Number Private</label>
                  <div class="col-sm-4">
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" checked=""  class="form-check-input" value="0" name="mobile_private"> No
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio"  class="form-check-input" value="1" name="mobile_private"> Yes
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-role" class="col-sm-2 col-form-label">Report Download Access</label>
                  <div class="col-sm-4">
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" checked="" <?php if($data['report_download_access']=='0'){echo "checked";} ?> class="form-check-input" value="0" name="report_download_access"> No
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" <?php if($data['report_download_access']=='1'){echo "checked";} ?> class="form-check-input" value="1" name="report_download_access"> Yes
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-notificationAccess-access" class="col-lg-2 col-form-label">Admin App Notifications</label>
                  <div class="col-lg-10">
                    <select class="form-control multiple-select" multiple name="notificationAccess[]">
                      <?php $ns=$d->select("admin_notification_master","active_status=0");
                      while ($ndata=mysqli_fetch_array($ns)) {
                      ?>
                      <option <?php if(isset($notAccAry) && in_array($ndata['admin_notification_id'], $notAccAry)){ echo "selected"; } ?> value="<?php echo $ndata['admin_notification_id']; ?>"><?php echo $ndata['title']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  
                </div>
                

                <div class="form-footer text-center">
                  
                    <button type="submit" id="socAddBtn" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->save; ?></button>
                </div>
              </form>
            <?php } ?>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->

       <script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">

  $(function() {

    $('.chk_boxes').click(function() {

        $('.pagePrivilege').prop('checked', this.checked);

    });

});

</script>

 <script type="text/javascript">
      var fileExtension = ['jpeg', 'jpg', 'png', 'jpeg'];

  function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}


$("#imgInp").change(function() {
  readURL(this);
});

</script>