<?php error_reporting(0);
$cId = (int)$_REQUEST['cId'];
$sId = (int)$_REQUEST['sId'];
$dId = (int)$_REQUEST['dId'];
$bId = (int)$_REQUEST['bId'];
$emp = (int)$_REQUEST['emp'];
$dpt = (int)$_REQUEST['dpt'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-3 col-md-6 col-6 ">
        <h4 class="page-title">Company Documents </h4>
      </div>
      <div class="col-sm-3 col-md-6 col-6 text-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <a href="addHrDoc" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
      </div>
    </div>
    <form action="" class="branchDeptFilter">
      <div class="row pt-2 pb-2">
        <?php include('selectBranchDeptForFilter.php'); ?>
        <div class="col-md-3 form-group">
          <select name="cId" class="form-control single-select" onchange="this.form.submit()">
            <option value="">All Category </option>
            <?php
            $hrcate = $d->select("hr_document_category_master", "society_id='$society_id' AND hr_document_category_deleted=0");
            while ($HrCatdata = mysqli_fetch_array($hrcate)) {
            ?>
              <option <?php if ($cId == $HrCatdata['hr_document_category_id']) {
                        echo 'selected';
                      } ?> value="<?php if (isset($HrCatdata['hr_document_category_id']) && $HrCatdata['hr_document_category_id'] != "") {
                                                                                                            echo $HrCatdata['hr_document_category_id'];
                                                                                                          } ?>"><?php if (isset($HrCatdata['hr_document_category_id']) && $HrCatdata['hr_document_category_id'] != "") {
                                                                                                                                                                                                                                                                    echo $HrCatdata['hr_document_category_name'];
                                                                                                                                                                                                                                                                  } ?></option>
            <?php } ?>

          </select>
        </div>
        <div class="col-md-1 form-group">
          <input class="btn btn-success btn-sm " type="submit" name="getReport" value="Get Data">
        </div>
      </div>

    </form>

    <div class="row">

      <div class="col-12 col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php if ($sId < 1) { ?>
              <div class="row mt-3">
                <?php

                $q1 = $d->selectRow("hr_document_sub_category_master.*,(SELECT COUNT(*) FROM hr_document_master WHERE hr_document_master.hr_document_sub_category_id = hr_document_sub_category_master.hr_document_sub_category_id AND hr_document_master.hr_doc_deleted=0) AS total_files", "hr_document_sub_category_master", "society_id='$society_id' AND hr_document_sub_category_deleted=0  AND hr_document_sub_category_master.hr_document_category_id='$cId' ", "ORDER BY hr_document_sub_category_master.hr_document_sub_category_name ASC");
                $counter = 1;
                while ($catSubdata = mysqli_fetch_array($q1)) {

                ?>
                  <div class="col-12 col-lg-3">
                    <a href="hrDoc?cId=<?php echo $cId; ?>&sId=<?php echo $catSubdata['hr_document_sub_category_id']; ?>">
                      <div class="card shadow-none border radius-15">
                        <div class="card-body">
                          <div class="d-flex align-items-center">
                            <div class="font-30 text-primary"><i class="fa fa-folder  fa-4x"></i>
                            </div>

                          </div>
                          <h6 class="mb-0 text-primary"><?php echo $catSubdata['hr_document_sub_category_name']; ?></h6>
                          <small><?php echo $catSubdata['total_files']; ?> files</small>
                        </div>
                      </div>
                    </a>
                  </div>
                <?php } ?>
              </div>
            <?php } ?>
            <!--end row-->
            <div class="row mt-3">
              <?php
              if (isset($cId) && $cId > 0) {

                $categoryFilterQuery = " AND hr_document_master.hr_document_category_id='$cId'";

                if (isset($dId) && $dId > 0) {
                  $departmentFilterQuery = " AND hr_document_master.floor_id='$dId'";
                }
                if (isset($dpt) && $dpt > 0) {
                  $departmentqFilterQuery = "  AND hr_document_master.user_id =0 ";
                }

                $q1 = $d->selectRow("hr_document_sub_category_master.hr_document_sub_category_name,users_master.user_full_name,users_master.user_designation,block_master.block_name,floors_master.floor_name,hr_document_category_master.hr_document_category_name,hr_document_master.*", "hr_document_category_master, hr_document_master LEFT JOIN hr_document_sub_category_master ON hr_document_sub_category_master.hr_document_sub_category_id=hr_document_master.hr_document_sub_category_id LEFT JOIN block_master ON block_master.block_id=hr_document_master.block_id AND hr_document_master.block_id!=0 LEFT JOIN floors_master ON floors_master.floor_id=hr_document_master.floor_id AND hr_document_master.floor_id!=0 LEFT JOIN users_master ON users_master.user_id= hr_document_master.user_id AND hr_document_master.user_id!=0", " hr_document_category_master.hr_document_category_id=hr_document_master.hr_document_category_id AND hr_document_master.society_id='$society_id' AND hr_document_master.hr_doc_deleted=0  $categoryFilterQuery $departmentFilterQuery", "ORDER BY hr_document_master.hr_document_id DESC");
                while ($catdata = mysqli_fetch_array($q1)) {

                  if ($catdata['document_type'] == 2) {
                    $icon  = "<i class='fa fa-youtube fa-4x'></i>";
                  } else if ($catdata['document_type'] == 0 || $catdata['document_type'] == 1) {
                    $ext = pathinfo($catdata['hr_document_file'], PATHINFO_EXTENSION);
                    $ext = strtolower($ext);

                    if ($ext == 'pdf') {
                      $icon  = "<i class='fa fa-file-pdf-o fa-4x'></i>";
                    } else if ($ext == 'jpg' || $ext == 'jpeg') {
                      $icon  = "<i class='fa fa-file-image-o fa-4x'></i>";
                    } else if ($ext == 'png') {
                      $icon  = "<i class='fa fa-file-image-o fa-4x'></i>";
                    } else if ($ext == 'doc' || $ext == 'docx') {
                      $icon  = "<i class='fa fa-file-word-o fa-4x'></i>";
                    } else if ($ext == 'MP4' || $ext == 'mp4') {
                      $icon  = "<i class='fa fa-file-video-o fa-4x'></i>";
                    } else if ($catdata['document_type'] == 1 && $ext == '') {
                      $icon  = "<i class='fa fa-firefox fa-4x'></i>";
                    } else if ($catdata['document_type'] == 1 && $ext == 'com') {
                      $icon  = "<i class='fa fa-firefox fa-4x'></i>";
                    } else {
                      $icon  = "<i class='fa fa-file-o fa-4x'></i>";
                    }
                  } else {
                    $icon  = "<i class='fa fa-folder fa-4x'></i>";
                  }
              ?>
                  <div class="col-12 col-lg-3">
                    <?php if ($catdata['document_type'] == 0) { ?>
                      <a href="<?php if ($catdata['hr_document_file'] != "") {
                                  echo $base_url . '/img/hrdoc/' . $catdata['hr_document_file'];
                                } ?>">
                      <?php } else { ?>
                        <a href="<?php if ($catdata['hr_document_file'] != "") {
                                    echo $catdata['hr_document_file'];
                                  } ?>">
                        <?php } ?>
                        <div class="card shadow-none border radius-15">
                          <div class="card-body">
                            <div class=" align-items-center">
                              <div class="font-30 text-primary">
                                <div class="row">
                                  <div class="col-sm-3">
                                    <?php echo $icon; ?>
                                  </div>
                                  <div class="col-sm-9">
                                    <h6 class="mb-0 text-primary"><?php echo $catdata['hr_document_name']; ?></h6>
                                    <small><?php if ($catdata['user_full_name'] != "") {
                                              echo $catdata['user_full_name'] . ' (' . $catdata['user_designation'] . ')';
                                            } else if ($catdata['block_name'] != "") {
                                              echo $catdata['floor_name']; ?> - <?php echo $catdata['block_name'];
                                                                                                                                                                                                                                      } else {
                                                                                                                                                                                                                                        echo "Common";
                                                                                                                                                                                                                                      } ?> <br> <?php echo date("d M Y h:i A", strtotime($catdata['hr_document_uploaded_date'])); ?></small>
                                  </div>
                                </div>


                                <div class="row">
                                  <div class="col-sm-4">
                                    <span class="badge badge-success"><?php echo $catdata['hr_document_category_name']; ?></span>
                                    <?php if ($catdata['hr_document_sub_category_name'] != "") { ?>
                                      <br>
                                      <span class="badge badge-success"><?php echo $catdata['hr_document_sub_category_name'];  ?></span>
                                    <?php } ?>
                                  </div>
                                  <div class="col-sm-4 text-right">
                                    <form action="addHrDoc" method="post">
                                      <input type="hidden" name="hr_document_id" value="<?php echo $catdata['hr_document_id']; ?>">
                                      <input type="hidden" name="edit_hr_doc" value="edit_hr_doc">
                                      <button type="submit" class="btn btn-sm btn-primary mr-1"> <i class="fa fa-pencil"></i></button>
                                      <?php if ($catdata['hr_document_status'] == "0") {
                                      ?>
                                        <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $catdata['hr_document_id']; ?>','hrdocDeactive');" data-size="small" />
                                      <?php } else { ?>
                                        <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $catdata['hr_document_id']; ?>','hrdocActive');" data-size="small" />
                                      <?php } ?>
                                    </form>
                                  </div>
                                  <div class="col-sm-2 text-right">
                                    <form action="documentReadStatus" method="post">
                                      <input type="hidden" name="hr_document_id" value="<?php echo $catdata['hr_document_id']; ?>">
                                      <input type="hidden" name="floor_id" value="<?php echo $catdata['floor_id']; ?>">
                                      <input type="hidden" name="block_id" value="<?php echo $catdata['block_id']; ?>">
                                      <input type="hidden" name="user_id" value="<?php echo $catdata['user_id']; ?>">
                                      <button class="btn btn-sm btn-info mr-1"><i class="fa fa-eye"></i> </button>
                                    </form>
                                  </div>
                                  <div class="col-sm-2 text-right">
                                    <form action="controller/hrDocController.php" method="post">
                                      <input type="hidden" name="hr_document_id" value="<?php echo $catdata['hr_document_id']; ?>">
                                      <input type="hidden" name="cId" value="<?php echo $cId; ?>">
                                      <input type="hidden" name="deleteDocument" value="deleteDocument">
                                      <button type="submit" class="btn form-btn btn-sm btn-danger mr-1"> <i class="fa fa-trash-o"></i></button>
                                    </form>
                                  </div>
                                </div>

                              </div>
                            </div>


                          </div>
                        </div>
                        </a>
                  </div>

                <?php }
              } else {
                $q1 = $d->selectRow("hr_document_category_master.*,(SELECT COUNT(*) FROM hr_document_master WHERE hr_document_master.hr_document_category_id = hr_document_category_master.hr_document_category_id AND hr_document_master.hr_doc_deleted=0) AS total_files", "hr_document_category_master", "society_id='$society_id' AND hr_document_category_deleted=0 ", "ORDER BY hr_document_category_master.hr_document_category_name ASC");
                $counter = 1;
                while ($catdata = mysqli_fetch_array($q1)) {

                ?>
                  <div class="col-12 col-lg-3">
                    <a href="hrDoc?cId=<?php echo $catdata['hr_document_category_id']; ?>">
                      <div class="card shadow-none border radius-15">
                        <div class="card-body">
                          <div class="d-flex align-items-center">
                            <div class="font-30 text-primary"><i class="fa fa-folder  fa-4x"></i>
                            </div>

                          </div>
                          <h6 class="mb-0 text-primary"><?php echo $catdata['hr_document_category_name']; ?></h6>
                          <small><?php echo $catdata['total_files']; ?> files</small>
                        </div>
                      </div>
                    </a>
                  </div>
              <?php }
              } ?>
            </div>
            <!--end row-->
            <div class="d-flex align-items-center">
              <div>
                <h5 class="mb-0">Recent Added Documents</h5>
              </div>

            </div>
            <div class="table-responsive mt-3">
              <table class="table table-striped table-hover table-sm mb-0">
                <thead>
                  <tr>
                    <th>Category <i class="bx bx-up-arrow-alt ms-2"></i>
                    </th>
                    <th>Name</th>
                    <th>File</th>
                    <th>Last Modified</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  $qR = $d->select("hr_document_category_master, hr_document_master LEFT JOIN hr_document_sub_category_master ON hr_document_sub_category_master.hr_document_sub_category_id=hr_document_master.hr_document_sub_category_id", " hr_document_category_master.hr_document_category_id=hr_document_master.hr_document_category_id AND hr_document_master.society_id='$society_id' AND hr_document_master.hr_doc_deleted=0   $categoryFilterQuery   $departmentFilterQuery", "ORDER BY hr_document_master.hr_document_id DESC LIMIT 5");
                  while ($recdata = mysqli_fetch_array($qR)) {
                  ?>
                    <tr>
                      <td>
                        <div class="d-flex align-items-center">
                          <div><i class="bx bxs-file me-2 font-24 text-primary"></i>
                          </div>
                          <div class="font-weight-bold text-primary"><?php echo $recdata['hr_document_category_name']; ?></div>
                        </div>
                      </td>
                      <td>
                        <?php echo $recdata['hr_document_name']; ?>
            </div>

            </td>
            <td><?php if ($recdata['document_type'] == 0) { ?>
                <a class="btn btn-sm btn-warning" href="<?php if ($recdata['hr_document_file'] != "") {
                                                          echo $base_url . '/img/hrdoc/' . $recdata['hr_document_file'];
                                                        } ?>">View</a>
              <?php } else { ?>
                <a class="btn btn-sm btn-warning" href="<?php if ($recdata['hr_document_file'] != "") {
                                                          echo $recdata['hr_document_file'];
                                                        } ?>">View</a>
              <?php } ?>
            </td>
            <td><?php echo date("M d, Y", strtotime($recdata['hr_document_uploaded_date'])); ?></td>
            <td><i class="bx bx-dots-horizontal-rounded font-24"></i>
            </td>
            </tr>
          <?php } ?>
          </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>





</div>
<!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->
<script type="text/javascript">
  function vieComplain(complain_id) {
    $.ajax({
      url: "getComplaineDetails.php",
      cache: false,
      type: "POST",
      data: {
        complain_id: complain_id
      },
      success: function(response) {
        $('#comResp').html(response);
      }
    });
  }
</script>




<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Company Documents</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body">
          <form id="addHrDocumentAdd" action="controller/hrDocController.php" enctype="multipart/form-data" method="post">
            <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">HR Category <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <select class="form-control single-select restFrm" name="hr_document_category_id" id="hr_document_category_id" onchange="getHRDocSubCategory(this.value)">
                  <option value="">-- Select --</option>
                  <?php
                  $hrcate = $d->select("hr_document_category_master", "society_id='$society_id' AND hr_document_category_deleted=0");
                  while ($HrCatdata = mysqli_fetch_array($hrcate)) {
                  ?>
                    <option <?php if (isset($data['hr_document_category_id']) && $data['hr_document_category_id'] == $HrCatdata['hr_document_category_id']) {
                              echo "selected";
                            } ?> value="<?php if (isset($HrCatdata['hr_document_category_id']) && $HrCatdata['hr_document_category_id'] != "") {
                                                                                                                                                                                          echo $HrCatdata['hr_document_category_id'];
                                                                                                                                                                                        } ?>"><?php if (isset($HrCatdata['hr_document_category_id']) && $HrCatdata['hr_document_category_id'] != "") {
                                                                                                                                                                                                                                                                                                                                                  echo $HrCatdata['hr_document_category_name'];
                                                                                                                                                                                                                                                                                                                                                } ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">HR Sub Category</label>
              <div class="col-lg-8 col-md-8" id="">
                <select class="form-control single-select restFrm" name="hr_document_sub_category_id" id="hr_document_sub_category_id">
                  <option value="">-- Select --</option>
                  <?php
                  $hrSubcate = $d->select("hr_document_sub_category_master", "society_id='$society_id' AND hr_document_sub_category_deleted=0");
                  while ($HrSubCatdata = mysqli_fetch_array($hrSubcate)) {
                  ?>
                    <option <?php if (isset($data['hr_document_sub_category_id']) && $data['hr_document_sub_category_id'] == $HrSubCatdata['hr_document_sub_category_id']) {
                              echo "selected";
                            } ?> value="<?php if (isset($HrSubCatdata['hr_document_sub_category_id']) && $HrSubCatdata['hr_document_sub_category_id'] != "") {
                                                                                                                                                                                                          echo $HrSubCatdata['hr_document_sub_category_id'];
                                                                                                                                                                                                        } ?>"><?php if (isset($HrSubCatdata['hr_document_sub_category_id']) && $HrSubCatdata['hr_document_sub_category_id'] != "") {
                                                                                                                                                                                                                                                                                                                                                                                      echo $HrSubCatdata['hr_document_sub_category_name'];
                                                                                                                                                                                                                                                                                                                                                                                    } ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <input type="hidden" name="is_dpt" value="<?php if (isset($_GET['dpt'])) {
                                                        echo 1;
                                                      } else {
                                                        echo 0;
                                                      } ?>">

            <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Department <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <select name="floor_id" id="floor_id" class="form-control single-select restFrm" onchange="getHrDocUser(this.value)">

                  <option value="All"> All Department </option>
                  <?php
                  $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id $blockAppendQuery");
                  while ($depaData = mysqli_fetch_array($qd)) {
                  ?>
                    <option <?php if ($dId == $depaData['floor_id']) {
                              echo 'selected';
                            } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name'] . '-' . $depaData['block_name']; ?></option>
                  <?php } ?>

                </select>
              </div>
            </div>
            <?php if (!isset($_GET['dpt'])) { ?>
              <div class="form-group row employee_select d-none">
                <label for="input-10" class="col-sm-4 col-form-label">Employee <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                  <select type="text" id="user_id" class="form-control single-select restFrm" name="user_id">

                  </select>
                </div>
              </div>
            <?php } ?>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Document <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8 ">
                <input type="hidden" name="hr_document_file_old" id="hr_document_file_old" value="<?php if (isset($data['hr_document_file']) && $data['hr_document_file'] != "") {
                                                                                                    echo $data['hr_document_file'];
                                                                                                  } ?>">
                <input type="file" accept=".pdf" maxlength="250" name="hr_document_file" class="form-control-file border ">

              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Document Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8 ">
                <input type="text" name="hr_document_name" id="hr_document_name" class="form-control" value="<?php if (isset($data['hr_document_name']) && $data['hr_document_name'] != "") {
                                                                                                                echo $data['hr_document_name'];
                                                                                                              } ?>">
              </div>
            </div>
            <div class="form-footer text-center">

              <input type="hidden" id="hr_document_id" name="hr_document_id" value="">
              <button id="addHrDocumentBtn" type="submit" class="btn btn-success sbmitbtn hrDocSubmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
              <input type="hidden" name="addHrDocument" value="addHrDocument">
              <input type="hidden" name="user_id_old" id="user_id_old" value="">
              <input type="hidden" name="floor_id_old" id="floor_id_old" value="">

              <button id="addHrDocumentBtn" type="submit" class="btn btn-success sbmitbtn hrDocSubmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>

              <!-- <button type="button"  value="add" class="btn btn-danger " onclick="resetForm();"><i class="fa fa-check-square-o"></i> Reset</button> -->

            </div>

          </form>

        </div>
      </div>

    </div>
  </div>
</div>



<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
  function buttonSettingForHrDoc() {


    $('#hr_document_id').val('');
    $('.hideAdd').show('');
    $('.hideupdate').hide('');
    $('#hr_document_category_id').val('');
    $('#hr_document_file').val('');
    $('#hr_document_name').val('');
    $('#floor_id').val('');
    $('#user_id').val('');
    $('#hr_document_category_id').attr("disabled", false);
    $('#floor_id').attr("disabled", false);
    $('#user_id').attr("disabled", false);
  }

  function resetForm() {
    $(".restFrm").val('').trigger('change');
    //$('#hr_document_file_old').val('');
    $('#hr_document_file').val('');
    $('#hr_document_id').val('');
  }

  function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
    }
    return false;
  }
</script>
<style>
  .hideupdate {
    display: none;
  }
</style>