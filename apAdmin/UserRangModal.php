<?php 
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

$floor_id = $_POST['floor_id'];
$uId = $_POST['uId'];
$csrf = $_POST['csrf'];
$society_id=$_COOKIE['society_id'];
$q = $d->selectRow('*', "society_master", "society_id='$society_id'");
$data = mysqli_fetch_assoc($q);
//print_r($data);
$user_latitud = $data['society_latitude'];
$user_longitude = $data['society_longitude'];

?>
<!-- <form id="UsercmpAtnType"  action="controller/updateUserGeoController.php" enctype="multipart/form-data" method="post"> -->
    <div class="form-group row">
        <input type="hidden" name="is_app_attendance_on" id="is_app_attendance_on">
            <label for="input-10" class="col-lg-4 col-md-4 col-form-label"> Range(Meter) <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                    <input  type ="text" value="<?php // echo $rangeValue; ?>" onkeyup="chnageUserRange()"  min="5"  id="user_geofence_range" name="user_geofence_range" class="form-control">
                    <input  type="hidden" id="csrf" value="<?php echo $csrf; ?>">
              </div>
            </div>
            <div class="form-group row blockDiv">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch <span class="required">*</span></label>
                    <div class="col-lg-8 col-md-8" id="">
                        <select name="br_id" id="br_id" class="form-control single-select " onchange="getVisitorFloorByBlockId(this.value)">
                             <?php 
                            $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                            while ($blockData=mysqli_fetch_array($qb)) {
                            ?>
                            <option  <?php // if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                            <?php } ?>

                        </select>
                    </div>
                </div>
            <div class="form-group row blockDiv">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Department <span class="required">*</span></label>
                    <div class="col-lg-8 col-md-8" id="">
                        <select name="floor_id" id="floor_id" class="form-control single-select floor_idGeo" onchange="getCompnayAttendaceUser(this.value)">
                            <option value="">All Department</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row userDiv">
                <input type="hidden" name="user_id_old" id="user_id_old">
                    <label for="input-10" class="col-sm-4 col-form-label">Employee <span class="required">*</span></label>
                    <div class="col-lg-8 col-md-8" id="">
                    <select id="user_id" class="form-control single-select" name="user_id" >
                        <option value="">Select Employee</option>
                    </select>
                    </div>
                </div>
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label"> User Latitude <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                <input type ="text" value="<?php  echo $user_latitud; ?>" readonly id="user_latitude" name="user_latitude" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label"> User Longitude <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                <input type ="text"  value="<?php  echo $user_longitude; ?>"  readonly id="user_longitude" name="user_longitude" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <input id="searchInput6" name="location_name" class="form-control" type="text" placeholder="Enter location" >
                    <div class="map" id="map2" style="width: 100%; height: 300px;"></div>
                </div>
            </div>
            <input type="hidden" name="updateUser" value="updateUser">
            <div class="text-center">
                <button id="" type="submit"   class="btn btn-secondary btn-sm sbmitbtnUserCmpAtnd "><i class="fa fa-check-square-o"></i> Update </button>
            </div>
        </div>
    </div>
<!-- </form> -->

<script>
$().ready(function()
  {
    var latitude = '<?php echo $user_latitud; ?>';
    var longitude = '<?php echo $user_longitude; ?>';

    initialize2(latitude,longitude);
  });
  function initialize2(d_lat,d_long)
  {
    
    var latlng = new google.maps.LatLng(d_lat,d_long);
    var latitute = 23.037786;
    var longitute = 72.512043;
    var map = new google.maps.Map(document.getElementById('map2'), {
      center: latlng,
      zoom: 17
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });
     // Add circle overlay and bind to marker
     user_geofence_range = $('#user_geofence_range').val();
     if(user_geofence_range=="")
     {
      user_geofence_range=10;
     }
     var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(user_geofence_range),    // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker, 'position');
    var parkingRadition = 5;
    var citymap = {
        newyork: {
          center: {lat: latitute, lng: longitute},
          population: parkingRadition
        }
    };

    var input = document.getElementById('searchInput6');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    autocomplete10.addListener('place_changed', function()
    {
      infowindow.close();
      marker.setVisible(false);
      var place5 = autocomplete10.getPlace();
      if (!place5.geometry) {
          window.alert("Autocomplete's returned place5 contains no geometry");
          return;
      }

      // If the place5 has a geometry, then present it on a map.
      if (place5.geometry.viewport) {
          map.fitBounds(place5.geometry.viewport);
      } else {
          map.setCenter(place5.geometry.location);
          map.setZoom(17);
      }

      marker.setPosition(place5.geometry.location);
      marker.setVisible(true);

      var pincode="";
      for (var i = 0; i < place5.address_components.length; i++) {
        for (var j = 0; j < place5.address_components[i].types.length; j++) {
          if (place5.address_components[i].types[j] == "postal_code") {
            pincode = place5.address_components[i].long_name;
          }
        }
      }
      bindDataToForm2(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
      infowindow.setContent(place5.formatted_address);
      infowindow.open(map, marker);

    });
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function()
    {
      geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
      {
        if (status == google.maps.GeocoderStatus.OK)
        {
          if (results[0])
          {
            var places = results[0];
            var pincode="";
            var serviceable_area_locality= places.address_components[4].long_name;
            for (var i = 0; i < places.address_components.length; i++)
            {
              for (var j = 0; j < places.address_components[i].types.length; j++)
              {
                if (places.address_components[i].types[j] == "postal_code")
                {
                  pincode = places.address_components[i].long_name;
                }
              }
            }
            bindDataToForm2(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
          }
        }
      });
    });
  }
  function bindDataToForm2(address,lat,lng,pin_code,serviceable_area_locality)
  {
    document.getElementById('user_latitude').value = lat;
    document.getElementById('user_longitude').value = lng;
  }
</script>