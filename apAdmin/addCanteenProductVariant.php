
<?php
extract(array_map("test_input", $_POST));
if (isset($edit_canteen_product_variant)) {
    $q = $d->select("vendor_product_variant_master","vendor_product_variant_id='$vendor_product_variant_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Canteen Product Variant</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addCanteenProductVariantFrom" action="controller/VendorProductVariantController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Product <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text" required="" class="form-control single-select" name="vendor_product_id">
                                        <option value="">-- Select Product --</option> 
                                        <?php 
                                            $product=$d->select("vendor_product_master,vendor_product_category_master","vendor_product_master.vendor_product_category_id=vendor_product_category_master.vendor_product_category_id AND vendor_product_master.vendor_category_id='1' ");  
                                            while ($productData=mysqli_fetch_array($product)) {
                                        ?>
                                        <option <?php if(isset($data['vendor_product_id']) && $data['vendor_product_id'] ==$productData['vendor_product_id']){ echo "selected"; } ?> value="<?php if(isset($productData['vendor_product_id']) && $productData['vendor_product_id'] !=""){ echo $productData['vendor_product_id']; } ?>"><?php if(isset($productData['vendor_product_name']) && $productData['vendor_product_name'] !=""){ echo $productData['vendor_product_name'] .' ('. $productData['vendor_product_category_name'] .')'; } ?></option> 
                                        <?php } ?>
                                    </select>                    
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Product Variant Name <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Product Variant Name" name="vendor_product_variant_name" value="<?php if($data['vendor_product_variant_name'] !=""){ echo $data['vendor_product_variant_name']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Description </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                <textarea id="purchase_descripion" name="vendor_product_variant_description" class="form-control "><?php if($data['vendor_product_variant_description'] !=""){ echo $data['vendor_product_variant_description']; } ?></textarea>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Price <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" min="0" class="form-control onlyNumber" placeholder="Price" name="vendor_product_variant_price" value="<?php if($data['vendor_product_variant_price'] !=""){ echo $data['vendor_product_variant_price']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Quantity <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Quantity" name="vendor_product_quantity" value="<?php if($data['vendor_product_quantity'] !=""){ echo $data['vendor_product_quantity']; } ?>">
                                </div>
                            </div> 
                        </div> -->
                        
                        <div class="col-md-6 <?php if (isset($edit_canteen_product_variant)) { echo 'd-none';} ?>">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Product Image <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="file" class="form-control" name="vendor_product_image_name[]" value="" multiple>
                                </div>
                            </div> 
                        </div>
                        <!-- <div class="col-md-12">
                            <div class="col-md-6" id="blah">
                            <?php if (isset($data['vendor_product_image_name']) && $data['vendor_product_image_name'] != null && $data['vendor_product_image_name'] != '') { ?>
                                <img src="../img/vendor_logo/<?php echo $data['vendor_logo']; ?>" width="75%" height="50%">
                            <?php } ?>
                            </div>
                        </div> -->
                    </div>    
                                
                    <div class="form-footer text-center">
                    <?php //IS_1019 addPenaltiesBtn 
                    if (isset($edit_canteen_product_variant)) {                    
                    ?>
                    <input type="hidden" id="vendor_product_variant_id" name="vendor_product_variant_id" value="<?php if($data['vendor_product_variant_id'] !=""){ echo $data['vendor_product_variant_id']; } ?>" >
                    <button id="addCanteenProductVariantBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addCanteenProductVariant"  value="addCanteenProductVariant">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCanteenProductVariantFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                        <?php }
                        else {
                        ?>
                    
                    <button id="addCanteenProductVariantBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addCanteenProductVariant"  value="addCanteenProductVariant">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCanteenProductVariantFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
    document.querySelector(".numberEClass").addEventListener("keypress", function (evt) {
      if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
      {
          evt.preventDefault();
      }
    });
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //$('#blah').attr('src', e.target.result);
            $('#blah').html('<img src="'+e.target.result+'" width="75%" height="50%">');
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
<style>
    input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	  -webkit-appearance: none;
	  margin: 0;
	}
</style>