<?php
$ends = array('th','st','nd','rd','th','th','th','th','th','th');
$bId = "";
$dId = "";
$Query = "";
$csrf = "";
$currentDay = date('m-d');

if (isset($_GET['monthWise']) && $_GET['monthWise']=='yes')
{
  if (!isset($_REQUEST['month']) && $_REQUEST['month']=='')
  {
   $month = date('m');
  }
  else
  {
   $month = (int)$_REQUEST['month'];
  }

  if(isset($_GET) && isset($_GET['bId']) || isset($_GET['dId']) || isset($_GET['uId']))
{
  $bId = (int)$_GET['bId'];
  $dId = (int)$_GET['dId'];
  $csrf = $_GET['csrf'];
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-md-9">
        <h4 class="page-title">Birthdays, Wedding & Work anniversary (<?php echo date("F",strtotime("2022-".$month.'-01')); ?>)</h4>
      </div>
      <div class="col-md-3 text-right">
        <a class="btn btn-primary btn-sm" href="celebrationsList">View Upcoming</a>
      </div>
    </div>
    <form class="" action="celebrationsList" >
      <div class="row pt-2 pb-2">
        <?php include 'selectBranchDeptForFilter.php' ?>
        <div class="col-md-3 form-group">
          <input type="hidden" name="monthWise" value="yes">
          <select class="form-control single-select"  name="month" id="month" onchange="this.form.submit()">
              <option value="0">-- Select Month --</option> 
              <?php
              $selected = "";
              for ($m = 1; $m <= 12; $m++) {
                $monthName = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
                if (isset($month)  && $month !="") {
                  if($month == $m)
                  {
                    $selected = "selected";
                  }else{
                    $selected = "";
                  }
                  
                } else {
                  $selected = "";
                }

              ?>
              <option  <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $monthName; ?></option> 
            <?php }?>
            </select> 
          
        </div>
        <div class="col-md-2 form-group">
          <input class="btn btn-success" type="submit" name="getList" class="form-control" value="getList">
        </div>
      </div>
    </form>
    <div class="row">
      <div class="col-lg-12">
        <div class="">
          <div class="card-body">
            <?php
            if(isset($bId) && $bId > 0)
            {
              $Query .= " AND block_id = '$bId'";
            }
            if(isset($dId) && $dId > 0)
            {
              $Query .= " AND floor_id = '$dId'";
            }
            if(isset($uId) && $uId > 0)
            {
              $Query .= " AND users_master.user_id = '$uId'";
            }
            $q = $d->selectRow("user_id,user_profile_pic,member_date_of_birth,user_full_name,user_designation","users_master","delete_status = 0 AND user_status = '1' AND active_status = '0' AND DATE_FORMAT(member_date_of_birth,'%m') = $month $Query","ORDER BY Month(member_date_of_birth) ASC,day( member_date_of_birth) ASC");

            $wa = $d->selectRow("user_id,user_profile_pic,user_full_name,user_designation,wedding_anniversary_date","users_master","delete_status = 0 AND user_status = '1' AND active_status = '0' AND  DATE_FORMAT(wedding_anniversary_date,'%m') = $month $Query","ORDER BY Month(wedding_anniversary_date) ASC,day( wedding_anniversary_date) ASC");

            $jd = $d->selectRow("users_master.user_id,user_profile_pic,user_full_name,user_designation,joining_date","users_master JOIN user_employment_details ON user_employment_details.user_id = users_master.user_id","delete_status = 0 AND user_status = '1' AND active_status = '0' AND  DATE_FORMAT(joining_date,'%m') = $month $Query","ORDER BY Month(joining_date) ASC,day( joining_date) ASC");

            ?>

            <h5 class="bdayDiv">Birthdays <i class='fa fa-birthday-cake' aria-hidden='true'></i></h5>
            <div class="row bdayDiv">
              <?php
              if(mysqli_num_rows($q) > 0)
              {
                while ($bd = $q->fetch_assoc())
                {
                  ?>
                  <div class="col-lg-2 col-md-2 col-6 col-xl-2">
                    <a href="employeeDetails?id=<?php echo $bd['user_id']; ?>" >
                      <div class="card radius-10 border-1px p-2">
                        <div class="">
                          <?php
                          if(date("m-d", strtotime($bd['member_date_of_birth'])) == $currentDay) { ?>
                            <a href="javascript:void(0)" onclick="birthdayNotification(<?php echo $bd['user_id']; ?>)" >
                              <?php }  ?>
                              <div class="text-center">
                                  <?php if(file_exists('../img/users/recident_profile/'.$bd['user_profile_pic']) && $bd['user_profile_pic']!="user_default.png" && $bd['user_profile_pic']!="user.png") { ?>
                        
                                  <img class="myIcon lazyload rounded-circle" onerror="this.onerror=null; this.src='../img/users/recident_profile/user_default.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $bd['user_profile_pic']; ?>">
                                  <?php
                                  }
                                  else
                                  {
                                  ?>
                                  <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
                                    <?php $shortChar=$d->get_sort_name($bd['user_full_name']); echo trim($shortChar); ?>
                                  </div>
                                  <?php
                                  }
                                  ?>
                                
                                <div class="w-100 pl-1">
                                  <span class="mb-0 text-primary" style="font-size: 14px;"><b><?php echo $bd['user_full_name']; ?></b></span><br>
                                  <span class="mb-0 text-dark" style="font-size: 14px;"><?php echo custom_echo($bd['user_designation'],17); ?></span>
                                  <?php
                                  $dateOfbirth = $bd['member_date_of_birth'];
                                  $wed = date("d",strtotime($bd['member_date_of_birth']));
                                  $curd = date("d");
                                  $totd = $wed - $curd;
                                  $diff = date_diff(date_create($dateOfbirth), date_create(date("Y-m-d")));
                                  if($totd > 0)
                                  {
                                    $number = $diff->format('%y') + 1;
                                  }
                                  else
                                  {
                                    $number = $diff->format('%y');
                                  }
                                  if (($number %100) >= 11 && ($number%100) <= 13)
                                  {
                                    $abbreviation = $number. 'th';
                                  }
                                  else
                                  {
                                    if ($number==0) {
                                       $number=1;
                                    }
                                    $abbreviation = $number. $ends[$number % 10];
                                  }
                                  ?>
                                  <p class="text-dark mb-0"><?php echo date("d M Y", strtotime($bd['member_date_of_birth'])); echo $bdDesc; ?></p>
                                  <span class="text-dark"><?php echo "(".$abbreviation." birthday)"; ?>
                                </span>
                              </div>
                            </div>
                            <?php
                            if(date("m-d", strtotime($bd['member_date_of_birth'])) == $currentDay) { ?> </a>
                            <?php } ?>
                      </div>
                    </div>
                    </a>
                </div>
              <?php }
            }
            else
            {
              ?>
              <div class="col text-danger ml-5"><b>No birthdays on this month</b></div>
              <?php
            }
            ?>
          </div>
          <h5 class="mt-3 weddDiv">Wedding Anniversary <i class='fa fa-ravelry' aria-hidden='true'></i></h5>
          <div class="row weddDiv">
            <?php
            if(mysqli_num_rows($wa) > 0)
            {
              while ($wad = $wa->fetch_assoc())
              {
                ?>
                <div class="col-lg-2 col-md-2 col-6 col-xl-2">
                  <a href="employeeDetails?id=<?php echo $wad['user_id']; ?>" >
                    <div class="card radius-10 border-1px p-2">
                        <?php
                        if(date("m-d", strtotime($wad['wedding_anniversary_date'])) == $currentDay)
                        {
                          ?>
                          <a href="javascript:void(0)" onclick="weddingNotification(<?php echo $wad['user_id']; ?>)" >
                            <?php  } ?>
                              <div class="text-center">
                              <?php if(file_exists('../img/users/recident_profile/'.$wad['user_profile_pic']) && $wad['user_profile_pic']!="user_default.png" && $wad['user_profile_pic']!="user.png") { ?>
                      
                                <img class="myIcon lazyload rounded-circle" onerror="this.onerror=null; this.src='../img/users/recident_profile/user_default.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $wad['user_profile_pic']; ?>">
                                <?php
                                }
                                else
                                {
                                ?>
                                <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
                                  <?php $shortChar=$d->get_sort_name($wad['user_full_name']); echo trim($shortChar); ?>
                                </div>
                                <?php
                                }
                                ?>
                             
                              <div class="w-100 pl-1">
                                <p class="mb-0 text-primary"><b><?php echo $wad['user_full_name']; ?></b></p>
                                <span class="mb-0 text-dark" style="font-size: 14px;"><?php echo custom_echo($wad['user_designation'],17); ?></span>
                                <?php

                                

                                $dateOfWedding = $wad['wedding_anniversary_date'];
                                $wed = date("d",strtotime($wad['wedding_anniversary_date']));
                                $curd = date("d");
                                $totd = $wed - $curd;
                                $diff = date_diff(date_create($dateOfWedding), date_create(date("Y-m-d")));
                                if($totd > 0)
                                {
                                  $number = $diff->format('%y') + 1;
                                }
                                else
                                {
                                  $number = $diff->format('%y');
                                }
                                if (($number %100) >= 11 && ($number%100) <= 13)
                                {
                                  $abbreviation = $number. 'th';
                                }
                                else
                                {
                                  $abbreviation = $number. $ends[$number % 10];
                                }
                                ?>
                                <p class="text-dark mb-0"><?php echo date("d M Y", strtotime($wad['wedding_anniversary_date'])); echo $wdDesc; ?>
                              </p>
                              <span class="text-dark"><?php echo "(".$abbreviation." anniversary)"; ?>
                            </span>
                          </div>
                          </div>
                        <?php
                        if(date("m-d", strtotime($wad['wedding_anniversary_date'])) == $currentDay)
                        {
                          ?>
                        </a>
                        <?php
                      }  ?>
                </div>
              </a>
            </div>
          <?php }
        }
        else
        {
          ?>
          <div class="col text-danger ml-5"><b>No Wedding Anniversary on this month</b></div>
          <?php
        }
        ?>
      </div>
      <h5 class="mt-3 workDiv">Work Anniversary <i class='fa fa-briefcase' aria-hidden='true'></i></h5>
      <div class="row workDiv">
        <?php
        if(mysqli_num_rows($jd) > 0)
        {
          while ($jdd = $jd->fetch_assoc())
          {
            ?>
            <div class="col-lg-2 col-md-2 col-6 col-xl-2">
              <a href="employeeDetails?id=<?php echo $jdd['user_id']; ?>" >
                  <div class="card radius-10 border-1px p-2">
                    <?php
                    if(date("m-d", strtotime($jdd['joining_date'])) == $currentDay)
                    {
                      ?>
                      <a href="javascript:void(0)" onclick="workNotification(<?php echo $jdd['user_id']; ?>)" >
                        <?php }  ?>
                          <div class="text-center">
                          <?php if(file_exists('../img/users/recident_profile/'.$jdd['user_profile_pic']) && $jdd['user_profile_pic']!="user_default.png" && $jdd['user_profile_pic']!="user.png") { ?>
                      
                            <img class="myIcon lazyload rounded-circle" onerror="this.onerror=null; this.src='../img/users/recident_profile/user_default.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $jdd['user_profile_pic']; ?>">
                            <?php
                            }
                            else
                            {
                            ?>
                            <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
                              <?php $shortChar=$d->get_sort_name($jdd['user_full_name']); echo trim($shortChar); ?>
                            </div>
                            <?php
                            }
                            ?>
                          
                          <div class="w-100 pl-1">
                            <p class="mb-0 text-primary"><b><?php echo $jdd['user_full_name']; ?></b></p>
                            <span class="mb-0 text-dark" style="font-size: 14px;"><?php echo custom_echo($jdd['user_designation'],17); ?></span>
                            <?php
                            

                            $dateOfJoin = $jdd['joining_date'];
                            $wed = date("d",strtotime($jdd['joining_date']));
                            $curd = date("d");
                            $totd = $wed - $curd;
                            $diff = date_diff(date_create($dateOfJoin), date_create(date("Y-m-d")));
                            if($totd > 0)
                            {
                              $number = $diff->format('%y') + 1;
                            }
                            else
                            {
                              $number = $diff->format('%y');
                            }
                            if (($number %100) >= 11 && ($number%100) <= 13)
                            {
                              $abbreviation = $number. 'th';
                            }
                            else
                            {
                              $abbreviation = $number. $ends[$number % 10];
                            }
                            ?>
                            <p class="text-dark mb-0"><?php echo date("d M Y", strtotime($jdd['joining_date'])); echo $jdDesc; ?>
                          </p>

                          <span class="text-dark"><?php  if ($number==0) { echo "(New Joining)"; } else { echo "(".$abbreviation." anniversary)";  } ?>
                        </span>
                      </div>
                    <?php
                    if(date("m-d", strtotime($jdd['joining_date'])) == $currentDay)
                    { ?> </a>  <?php  }  ?>
              </div>
            </div>
            </a>
        </div>
        <?php
      }
    }
    else
    {
      ?>
      <div class="col text-danger ml-5"><b>No Work Anniversary on this month</b></div>
      <?php
    }
    ?>
  </div>
</div>
</div><!-- End Row-->
</div><!-- End container-fluid-->
</div><!--End content-wrapper-->





<?php } else {

if(isset($_GET) && isset($_GET['bId']) || isset($_GET['dId']) || isset($_GET['uId']))
{
  $bId = (int)$_GET['bId'];
  $dId = (int)$_GET['dId'];
  $csrf = $_GET['csrf'];
}
$birthday_notification_type = $sData['birthday_notification_type'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-md-6">
        <h4 class="page-title">Upcoming Birthdays, Wedding & Work anniversary (15 Days)</h4>
      </div>
      <div class="col-md-4">
        <li class="list-group-item d-flex justify-content-between align-items-center">
          Daily Birthday Notification
          <span>
          <form>
            <select name="birthday_notification_type" onchange="changeBirthdayNotificationSetting(this.value);" id="birthday_notification_type" class="form-control">
                <option <?php if($birthday_notification_type==0){echo 'selected';}?> value="0"> OFF </option>
                <option <?php if($birthday_notification_type==1){echo 'selected';}?> value="1">Branch Wise</option>
                <option <?php if($birthday_notification_type==2){echo 'selected';}?> value="2">Department Wise</option>
                <option <?php if($birthday_notification_type==3){echo 'selected';}?> value="3">Company Wise</option>
            </select>
          </form>
           
          </span>
          </li>
      </div>
      <div class="col-md-2 text-right">
        <a class="btn btn-primary btn-sm" href="celebrationsList?monthWise=yes">Month Wise</a>
      </div>
    </div>
    <form class="branchDeptFilter" action="" >
      <div class="row pt-2 pb-2">
        <?php include 'selectBranchDeptForFilter.php' ?>
        <div class="col-md-3 form-group">
          <select class="form-control single-select" name="celebrationTypeFilter" id="celebrationTypeFilter" onchange="celebrationsFilter(this.value)">
            <option value="">--SELECT--</option>
            <option value="1">Birthdays</option>
            <option value="2">Wedding Anniversary</option>
            <option value="3">Work Anniversary</option>
          </select>
        </div>
        <div class="col-md-2 form-group">
          <input class="btn btn-success" type="submit" name="getList" class="form-control" value="getList">
        </div>
      </div>
    </form>
    <div class="row">
      <div class="col-lg-12">
        <div class="">
          <div class="card-body">
            <?php
            $today_full_date = date("m-d");
            $fifteen_days = date("m-d",strtotime("15 Days"));
            if(isset($bId) && $bId > 0)
            {
              $Query .= " AND block_id = '$bId'";
            }
            if(isset($dId) && $dId > 0)
            {
              $Query .= " AND floor_id = '$dId'";
            }
            if(isset($uId) && $uId > 0)
            {
              $Query .= " AND users_master.user_id = '$uId'";
            }
            $q = $d->selectRow("user_first_name,user_id,user_profile_pic,member_date_of_birth,user_full_name,user_designation","users_master","delete_status = 0 AND user_status = '1' AND active_status = '0' AND (DATE_FORMAT(member_date_of_birth, '%m-%d') BETWEEN '$today_full_date' AND '$fifteen_days') $Query","ORDER BY Month(member_date_of_birth) ASC,day( member_date_of_birth) ASC");

            $wa = $d->selectRow("user_id,user_profile_pic,user_full_name,user_designation,wedding_anniversary_date","users_master","delete_status = 0 AND user_status = '1' AND active_status = '0' AND (DATE_FORMAT(wedding_anniversary_date, '%m-%d') BETWEEN '$today_full_date' AND '$fifteen_days') $Query","ORDER BY Month(wedding_anniversary_date) ASC,day( wedding_anniversary_date) ASC");

            $jd = $d->selectRow("users_master.user_id,user_profile_pic,user_full_name,user_designation,joining_date","users_master JOIN user_employment_details ON user_employment_details.user_id = users_master.user_id","delete_status = 0 AND user_status = '1' AND active_status = '0' AND (DATE_FORMAT(joining_date, '%m-%d') BETWEEN '$today_full_date' AND '$fifteen_days') $Query","ORDER BY Month(joining_date) ASC,day( joining_date) ASC");
            ?>

            <h5 class="bdayDiv">Birthdays <i class='fa fa-birthday-cake' aria-hidden='true'></i></h5>
            <div class="row bdayDiv">
              <?php
              if(mysqli_num_rows($q) > 0)
              {
                while ($bd = $q->fetch_assoc())
                {
                  ?>
                  <div class="col-lg-2 col-md-2 col-6 col-xl-2">
                    <a href="employeeDetails?id=<?php echo $bd['user_id']; ?>" >
                      <div class="card radius-10 border-1px p-2">
                        <div class="">
                          <?php
                          if(date("m-d", strtotime($bd['member_date_of_birth'])) == $currentDay) { ?>
                            <a href="javascript:void(0)" onclick="birthdayNotification(<?php echo $bd['user_id']; ?>)" >
                              <?php }  ?>
                              <div class="text-center">
                                  <?php if(file_exists('../img/users/recident_profile/'.$bd['user_profile_pic']) && $bd['user_profile_pic']!="user_default.png" && $bd['user_profile_pic']!="user.png") { ?>
                        
                                  <img class="myIcon lazyload rounded-circle" onerror="this.onerror=null; this.src='../img/users/recident_profile/user_default.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $bd['user_profile_pic']; ?>">
                                  <?php
                                  }
                                  else
                                  {
                                  ?>
                                  <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
                                    <?php $shortChar=$d->get_sort_name($bd['user_full_name']); echo trim($shortChar); ?>
                                  </div>
                                  <?php
                                  }
                                  ?>
                                
                                <div class="w-100 pl-1">
                                  <span class="mb-0 text-primary" style="font-size: 14px;"><b><?php echo $bd['user_full_name']; ?></b></span><br>
                                  <span class="mb-0 text-dark" style="font-size: 14px;"><?php echo custom_echo($bd['user_designation'],17); ?></span>
                                  <?php
                                  $dateOfbirth = $bd['member_date_of_birth'];
                                  $wed = date("d",strtotime($bd['member_date_of_birth']));
                                  $curd = date("d");
                                  $totd = $wed - $curd;
                                  $diff = date_diff(date_create($dateOfbirth), date_create(date("Y-m-d")));
                                  if($totd > 0)
                                  {
                                    $number = $diff->format('%y') + 1;
                                  }
                                  else
                                  {
                                    $number = $diff->format('%y');
                                  }
                                  if (($number %100) >= 11 && ($number%100) <= 13)
                                  {
                                    $abbreviation = $number. 'th';
                                  }
                                  else
                                  {
                                    if ($number==0) {
                                       $number=1;
                                    }
                                    $abbreviation = $number. $ends[$number % 10];
                                  }
                                  ?>
                                  <p class="text-dark mb-0"><?php echo date("d M Y", strtotime($bd['member_date_of_birth'])); echo $bdDesc; ?></p>
                                  <span class="text-dark"><?php echo "(".$abbreviation." birthday)"; ?>
                                </span>
                              </div>
                            </div>
                            <?php
                            if(date("m-d", strtotime($bd['member_date_of_birth'])) == $currentDay) { ?> </a>
                            <?php } ?>
                      </div>
                    </div>
                    </a>
                </div>
              <?php }
            }
            else
            {
              ?>
              <div class="col text-danger ml-5"><b>No Upcoming Birthdays</b></div>
              <?php
            }
            ?>
          </div>
          <h5 class="mt-3 weddDiv">Wedding Anniversary <i class='fa fa-ravelry' aria-hidden='true'></i></h5>
          <div class="row weddDiv">
            <?php
            if(mysqli_num_rows($wa) > 0)
            {
              while ($wad = $wa->fetch_assoc())
              {
                ?>
                <div class="col-lg-2 col-md-2 col-6 col-xl-2">
                  <a href="employeeDetails?id=<?php echo $wad['user_id']; ?>" >
                    <div class="card radius-10 border-1px p-2">
                        <?php
                        if(date("m-d", strtotime($wad['wedding_anniversary_date'])) == $currentDay)
                        {
                          ?>
                          <a href="javascript:void(0)" onclick="weddingNotification(<?php echo $wad['user_id']; ?>)" >
                            <?php  } ?>
                              <div class="text-center">
                              <?php if(file_exists('../img/users/recident_profile/'.$wad['user_profile_pic']) && $wad['user_profile_pic']!="user_default.png" && $wad['user_profile_pic']!="user.png") { ?>
                      
                                <img class="myIcon lazyload rounded-circle" onerror="this.onerror=null; this.src='../img/users/recident_profile/user_default.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $wad['user_profile_pic']; ?>">
                                <?php
                                }
                                else
                                {
                                ?>
                                <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
                                  <?php $shortChar=$d->get_sort_name($wad['user_full_name']); echo trim($shortChar); ?>
                                </div>
                                <?php
                                }
                                ?>
                             
                              <div class="w-100 pl-1">
                                <p class="mb-0 text-primary"><b><?php echo $wad['user_full_name']; ?></b></p>
                                <span class="mb-0 text-dark" style="font-size: 14px;"><?php echo custom_echo($wad['user_designation'],17); ?></span>
                                <?php

                                

                                $dateOfWedding = $wad['wedding_anniversary_date'];
                                $wed = date("d",strtotime($wad['wedding_anniversary_date']));
                                $curd = date("d");
                                $totd = $wed - $curd;
                                $diff = date_diff(date_create($dateOfWedding), date_create(date("Y-m-d")));
                                if($totd > 0)
                                {
                                  $number = $diff->format('%y') + 1;
                                }
                                else
                                {
                                  $number = $diff->format('%y');
                                }
                                if (($number %100) >= 11 && ($number%100) <= 13)
                                {
                                  $abbreviation = $number. 'th';
                                }
                                else
                                {
                                  $abbreviation = $number. $ends[$number % 10];
                                }
                                ?>
                                <p class="text-dark mb-0"><?php echo date("d M Y", strtotime($wad['wedding_anniversary_date'])); echo $wdDesc; ?>
                              </p>
                              <span class="text-dark"><?php echo "(".$abbreviation." anniversary)"; ?>
                            </span>
                          </div>
                          </div>
                        <?php
                        if(date("m-d", strtotime($wad['wedding_anniversary_date'])) == $currentDay)
                        {
                          ?>
                        </a>
                        <?php
                      }  ?>
                </div>
              </a>
            </div>
          <?php }
        }
        else
        {
          ?>
          <div class="col text-danger ml-5"><b>No Upcoming Wedding Anniversary</b></div>
          <?php
        }
        ?>
      </div>
      <h5 class="mt-3 workDiv">Work Anniversary <i class='fa fa-briefcase' aria-hidden='true'></i></h5>
      <div class="row workDiv">
        <?php
        if(mysqli_num_rows($jd) > 0)
        {
          while ($jdd = $jd->fetch_assoc())
          {
            ?>
            <div class="col-lg-2 col-md-2 col-6 col-xl-2">
              <a href="employeeDetails?id=<?php echo $jdd['user_id']; ?>" >
                  <div class="card radius-10 border-1px p-2">
                    <?php
                    if(date("m-d", strtotime($jdd['joining_date'])) == $currentDay)
                    {
                      ?>
                      <a href="javascript:void(0)" onclick="workNotification(<?php echo $jdd['user_id']; ?>)" >
                        <?php }  ?>
                          <div class="text-center">
                          <?php if(file_exists('../img/users/recident_profile/'.$jdd['user_profile_pic']) && $jdd['user_profile_pic']!="user_default.png" && $jdd['user_profile_pic']!="user.png") { ?>
                      
                            <img class="myIcon lazyload rounded-circle" onerror="this.onerror=null; this.src='../img/users/recident_profile/user_default.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $jdd['user_profile_pic']; ?>">
                            <?php
                            }
                            else
                            {
                            ?>
                            <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
                              <?php $shortChar=$d->get_sort_name($jdd['user_full_name']); echo trim($shortChar); ?>
                            </div>
                            <?php
                            }
                            ?>
                          
                          <div class="w-100 pl-1">
                            <p class="mb-0 text-primary"><b><?php echo $jdd['user_full_name']; ?></b></p>
                            <span class="mb-0 text-dark" style="font-size: 14px;"><?php echo custom_echo($jdd['user_designation'],17); ?></span>
                            <?php
                            

                            $dateOfJoin = $jdd['joining_date'];
                            $wed = date("d",strtotime($jdd['joining_date']));
                            $curd = date("d");
                            $totd = $wed - $curd;
                            $diff = date_diff(date_create($dateOfJoin), date_create(date("Y-m-d")));
                            if($totd > 0)
                            {
                              $number = $diff->format('%y') + 1;
                            }
                            else
                            {
                              $number = $diff->format('%y');
                            }
                            if (($number %100) >= 11 && ($number%100) <= 13)
                            {
                              $abbreviation = $number. 'th';
                            }
                            else
                            {
                              $abbreviation = $number. $ends[$number % 10];
                            }
                            ?>
                            <p class="text-dark mb-0"><?php echo date("d M Y", strtotime($jdd['joining_date'])); echo $jdDesc; ?>
                          </p>

                          <span class="text-dark"><?php  if ($number==0) { echo "(New Joining)"; } else { echo "(".$abbreviation." anniversary)";  } ?>
                        </span>
                      </div>
                    <?php
                    if(date("m-d", strtotime($jdd['joining_date'])) == $currentDay)
                    { ?> </a>  <?php  }  ?>
              </div>
            </div>
            </a>
        </div>
        <?php
      }
    }
    else
    {
      ?>
      <div class="col text-danger ml-5"><b>No Upcoming Work Anniversary</b></div>
      <?php
    }
    ?>
  </div>
</div>
</div>
</div>
</div><!-- End Row-->
</div><!-- End container-fluid-->
</div><!--End content-wrapper-->


<div class="modal fade" id="birthNotiModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white modal-title-noti">Send Birthday Notification</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <form id="birthdayNotificationForm" action="controller/statusController.php" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <label for="input-10" class="col-sm-12 col-form-label">Title <span class="text-danger">*</span></label>
                  <div class="col-lg-12 col-md-12">
                    <input type="text" class="form-control" id="title" name="title" value="Happy Birthday">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-12 col-form-label">Description <span class="text-danger">*</span></label>
                  <div class="col-lg-12 col-md-12">
                    <textarea class="form-control" id="description" name="description"></textarea>
                  </div>
                </div>
                <div class="form-footer text-center">
                  <input type="hidden" name="carf" value="<?php echo $csrf; ?>">
                  <input type="hidden" name="status" value="birthdayNotification">
                  <input type="hidden" name="celebrationType" id="celebrationType">
                  <input type="hidden" name="user_id" id="user_id">
                  <input type="hidden" name="url" id="url" value="celebrationsList">
                  <input type="hidden" name="block_id" id="block_id" value="<?php echo $bId; ?>">
                  <input type="hidden" name="department_id" id="department_id" value="<?php echo $dId; ?>">
                  <button type="submit" class="btn btn-success">Send </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php } ?>