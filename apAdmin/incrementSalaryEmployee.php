<?php
error_reporting(0);
/* $currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year')); */

$uId = (int) $_REQUEST['uId'];
$dId = (int) $_REQUEST['dId'];
$bId = (int) $_REQUEST['bId'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
     <div class="row pt-2 pb-2">
          <div class="col-sm-3 col-md-6 col-6">
            <h4 class="page-title">Upcoming Increment Salary</h4>
          </div>
          <div class="col-sm-3 col-md-6 col-6">
            
        </div>
     </div>
     <form action="" >

      <div class="row pt-2 pb-2">
        <?php include('selectBranchDeptEmpForFilterAll.php'); ?>
        <div class="col-md-3 form-group ">
              <input class="btn btn-sm btn-success " type="submit" name="getReport"  value="Get Data">
          </div>
            
      </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Department</th>
                        <th>Employer</th>
                        <th>Net Salary</th>
                        <th>Type</th>
                        <th>Start Date</th>
                        <th>Increment Date</th>
                        <th>View</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                      $i = 1;
                      if(isset($bId) && $bId>0) {
                        $blockFilterQuery = " AND users_master.block_id='$bId'";
                      }

                      if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                          $deptFilterQuery = " AND salary.floor_id='$_GET[dId]'";
                      }
                      if (isset($uId) && $uId > 0) {
                          $UserFilterQuery = "AND salary.user_id='$user_id'";
                      }
                      $crDate = date('Y-m-d');
                       $q = $d->select("salary,users_master,floors_master", "salary.is_preivous_salary=0 AND  salary.user_id=users_master.user_id AND salary.floor_id=floors_master.floor_id AND salary.society_id='$society_id'  AND salary.is_delete='0' AND users_master.delete_status=0 AND DATEDIFF(salary.salary_increment_date,'$crDate') <=30 $blockFilterQuery $deptFilterQuery  $UserFilterQuery $blockAppendQueryUser", "ORDER BY salary.salary_id ASC");
                      $counter = 1;
                      while ($data = mysqli_fetch_array($q)) {
                          ?>
                    <tr>
                       
                       <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['floor_name']; ?></td>
                       <td><?php echo $data['user_full_name']; ?></td>
                       <td><?php echo $data['net_salary']; ?></td>
                       <td><?php 
                          if($data['salary_type']=="0"){
                            $typ = "Fixed";
                          }else if($data['salary_type']=="1")
                          {
                            $typ = "Per Day";
                          }else
                          {
                            $typ = "Per Hour";
                          }
                        ?><span class="badge badge-info"><?php echo $typ; ?></span></td>
                       <td><?php echo date("d M Y ", strtotime($data['salary_start_date'])); ?></td>
                       <td><?php echo date("d M Y ", strtotime($data['salary_increment_date'])); ?></td>
                       <td> <button type="button" class="btn btn-primary btn-sm salary_Modal" data-id="<?php echo $data['salary_id']; ?>">View</button></td>
                       <td>
                       <div class="d-flex align-items-center">
                      
                         <a href="addSalary?is_increment=1&sid=<?php echo $data['salary_id']; ?>">
                          <button type="submit" class="btn btn-sm btn-primary mr-2"><i class="fa fa-plus"></i></button>
                          </a>
                         
                          <?php if ($data['is_active'] == "0") {
                                    $status = "salaryStatusDeactive";
                                    $active = "checked";
                                } else {
                                    $status = "salaryStatusActive";
                                    $active = "";
                                }?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['salary_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                         
                        </div>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
   </div>
   <div class="modal fade" id="salaryModal">
  <div class="modal-dialog" style="max-width: 90%">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Salary Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div id="salaryData"></div>
        <div class="col-md-12 row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Earning</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody id="earnData">
                    </tbody>
                  </table>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th> Deduction Name</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody id="deductData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 text-center">
                  <label for="exampleInputEmail1">Net Salary : </label>
                    <span id="netSalary"></span>
                  </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
//////////////////////holiday data
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }

</script>
