<?php error_reporting(0);
   $currentYear = date('Y');
   $currentMonth = date('m');
   $nextYear = date('Y', strtotime('+1 year'));
   $onePreviousYear = date('Y', strtotime('-1 year'));
   $twoPreviousYear = date('Y', strtotime('-2 year'));
   $bId = (int)$_REQUEST['bId'];
   $dId = (int)$_REQUEST['dId'];
   $ueMonth = $_REQUEST['ueMonth'];
   $ueYear = $_REQUEST['ueYear'];
   $hId = $_REQUEST['hId'];
   $uId = $_REQUEST['uId'];
   $q2 = $d->selectRow('user_expense_history.*','user_expense_history',"user_expense_history_id='$hId'");
   $hdata = mysqli_fetch_assoc($q2);

   if(isset($dId) && $dId>0) {
    $deptFilterQuery = " AND user_expenses.floor_id='$dId'";
  }
   if(isset($uId) && $uId>0) {
    $userFilterQuery = " AND user_expenses.user_id='$uId'";
  }
  if(isset($ueYear) && $ueYear>0) {
    $yearFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%Y') = '$ueYear'";
  }
  if(isset($ueMonth) && $ueMonth>0) {
    $monthFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%m') = '$ueMonth'";
  }else
  {
    $ueMonth = date('m');
    $monthFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%m') = '$ueMonth'";

  }
 /// print_R($hdata['expense_id']);
  $q = $d->selectRow("user_expenses.*,floors_master.*,users_master.*","user_expenses,floors_master,users_master", "users_master.user_id=user_expenses.user_id AND user_expenses.floor_id=floors_master.floor_id AND user_expenses.society_id='$society_id' AND user_expenses.expense_paid_status=1 AND users_master.delete_status=0 $userFilterQuery  $deptFilterQuery $yearFilterQuery $monthFilterQuery $blockAppendQueryUser","ORDER BY user_expenses.user_expense_id ASC");
  $counter = 1;

  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-md-6">
          <h4 class="page-title">Employee Paid Expenses History</h4>
        </div>
        <div class="col-md-6 text-right">
            <?php if(mysqli_num_rows($q)>0){ ?>
          <h4 class="page-title">
          <a href="unpaidUserExpenseInvoice.php?bId=<?php echo $dId; ?>&dId=<?php echo $dId; ?>&uId=<?php echo $uId; ?>&ueMonth=<?php echo $ueMonth; ?>&ueYear=<?php echo $ueYear; ?>"  class="btn btn-sm  btn-secondary mr-1"><i class="text-white fa fa-file-pdf-o"></i>PDF</a>
            <?php } ?>
        </div>
     </div>
     <form class="branchDeptFilter" action="" >
        <div class="row pt-2 pb-2">
            <?php  include 'selectBranchDeptEmpForFilter.php' ?>
            <div class="col-md-2 form-group">
            <select required name="ueMonth" class="form-control single-select">
                  <option <?php if($ueMonth=="0") { echo 'selected';} ?> value="">Select Month</option>
                  <option <?php if($ueMonth=="01") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '01') { echo 'selected';}?> value="01">January</option>
                  <option <?php if($ueMonth=="02") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '02') { echo 'selected';}?> value="02">February</option>
                  <option <?php if($ueMonth=="03") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '03') { echo 'selected';}?> value="03">March</option>
                  <option <?php if($ueMonth=="04") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '04') { echo 'selected';}?> value="04">April</option>
                  <option <?php if($ueMonth=="05") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '05') { echo 'selected';}?> value="05">May</option>
                  <option <?php if($ueMonth=="06") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '06') { echo 'selected';}?> value="06">June</option>
                  <option <?php if($ueMonth=="07") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '07') { echo 'selected';}?> value="07">July</option>
                  <option <?php if($ueMonth=="08") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '08') { echo 'selected';}?> value="08">August</option>
                  <option <?php if($ueMonth=="09") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '09') { echo 'selected';}?> value="09">September</option>
                  <option <?php if($ueMonth=="10") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '10') { echo 'selected';}?> value="10">October</option>
                  <option <?php if($ueMonth=="11") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '11') { echo 'selected';}?> value="11">November</option>
                  <option <?php if($ueMonth=="12") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '12') { echo 'selected';}?> value="12">December</option>
                
                </select>
            </div>
            <div class="col-md-2 form-group">
            <select name="ueYear" class="form-control">
                  <option <?php if($ueYear=="0") { echo 'selected';} ?> value="0">All Year</option>
                  <option <?php if($ueYear==$twoPreviousYear) { echo 'selected';} ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                  <option <?php if($ueYear==$onePreviousYear) { echo 'selected';} ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                  <option <?php if($ueYear==$currentYear) { echo 'selected';}?> <?php if($ueYear == '') { echo 'selected';}?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                  <option <?php if($ueYear==$nextYear) { echo 'selected';} ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
                </select>
            </div>
            <div class="col-md-2 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
            </div>
                  
        </div>
    </form>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
           
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <?php if($hdata['amount']>0) { ?>
                <label>Total Amount :</label><i><?php echo $hdata['amount']; ?></i>
              <?php } ?>
              <div class="table-responsive">
              <?php
                      $i = 1;
                      
                      
                    ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Department</th>
                        <th>Employee</th>
                        <th>Expense Title</th>
                        <!-- <th>Paid By</th> -->
                        <th>Amount</th>
                        <th>Date</th>
                        <th>View</th>

                    </tr>
                </thead>
                <tbody>
                   <?php while ($data = mysqli_fetch_array($q)) { ?>
                    <tr>
                       <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['floor_name']; ?> </td>
                       <td><?php echo $data['user_full_name']; ?>(<?php echo $data['user_designation']; ?>)</td>
                       <td><?php echo $data['expense_title']; ?></td>
                       <!-- <td><?php if($data['expense_paid_by_type']==1){ echo $data['paid_by_admin_name'];}else { echo  $data['paid_user_name'];} ?></td> -->
                       <td><?php echo $data['amount']; ?></td>
                       <td><?php echo date("d M Y", strtotime($data['date'])); ?></td>
                        <td>
                            <button type="button" class="btn btn-sm btn-primary mr-1 pd-1" onclick="employeeExpensesDetail(<?php echo $data['user_expense_id']; ?>)" >
                            <i class="fa fa-eye"></i>
                            </button>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
            
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->

<div class="modal fade" id="employeeExpensesModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Employee Expenses</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="employeeExpensesData" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>
<div class="modal fade" id="expensePayModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Expense Pay Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="payExpenseAmountForm" action="controller/ExpenseSettingController.php" enctype="multipart/form-data" method="post"> 
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Expense Payment Mode <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <input type="text" class="form-control" placeholder="Expense Payment Mode" id="expense_payment_mode" name="expense_payment_mode" value="">
                </div> 
            </div>
             
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Reference Number <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <input type="text" class="form-control" placeholder="Reference Number" id="reference_no" name="reference_no" value="">
                </div> 
            </div>                    
            <div class="form-footer text-center">
             <input type="hidden" id="user_expense_id" name="user_expense_id" value="" >
              <input type="hidden" name="payExpenseAmount"  value="payExpenseAmount">
             <button id="payExpenseAmountBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('payExpenseAmountForm');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>


<script type="text/javascript">

function expensePay(id){
  $('#user_expense_id').val(id);
  //$('#addAttendaceModal').modal();
  $('#expensePayModal').modal();
}



</script>
