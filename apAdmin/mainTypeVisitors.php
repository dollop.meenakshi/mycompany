
<div class="content-wrapper">
	<div class="container-fluid">
	<div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">  Visitores Main Type </h4>
      </div>
      <div class="col-sm-3">
        <div class="btn-group float-sm-right">
         	<button class="btn  btn-sm btn-success " data-toggle="modal" data-target="#addModal"><i class="fa fa-plus mr-1"></i> Add Visitor</button>	
          
        </div>
      </div>
    </div>

	
		<div class="row"><!-- start view -->
			<?php 
			$query=$d->select("visitorMainType"," active_status = 0","");
			while ($response=mysqli_fetch_array($query)) {
			  extract($response); 
				 ?>
				<div class="col-md-4">
					<div class="card mb-2">
						<div id="collapse-1" class="collapse show" data-parent="#accordion1" style="">
							<div class="p-2">
								<div class="card-body">

		<a href="visitorsubtype?visitorMainTypeId=<?php echo $visitor_main_type_id;?>"  title="Sub Types"> <img src="../img/visitor_company/<?php echo $visitor_main_full_img; ?>" height="200px" width="100%"> </a>


									 
								</div>
								<div class="card-title mt-2 text-uppercase"> 
									<?php echo $response['main_type_name']; ?>
								</div>
								
								<ul class="list-inline text-center">
									<li class="list-inline-item">


											<!-- <button type="submit"name="update" data-target="#updateModal" data-toggle="modal" onclick="editMainType('<?= $response['visitor_main_type_id'] ?>','<?= $response['main_type_name']; ?>','<?= $response['visitor_type']; ?>','<?= $response['visitor_main_full_img']; ?>','<?= $response['main_type_image']; ?>',)" class="btn btn-primary btn-sm">Edit</button> -->


											 <a  class="btn btn-primary btn-sm" onclick="editMainType('<?php echo $visitor_main_type_id; ?>');" data-toggle="modal" data-target="#updateModal" href="javascript:void();">
					                          <span class=" "><i class="fa fa-pencil-square-o"></i></span>

					                           </a>



									</li>
									<li class="list-inline-item">


 <?php   $cnt= $d->count_data_direct("visitor_sub_type_id","visitorSubType"," visitor_main_type_id='$visitor_main_type_id'"); 
                      if( $cnt == 0 ) {?>
                        <form action="controller/addMainTypeVisitorsController.php" method="post">    
                          <input type="hidden" name="visitor_main_type_id" value="<?php echo $visitor_main_type_id; ?>">    
                          <input type="hidden" name="deletemainvisitor" value="deletemainvisitor">                 
                          <button type="submit" name="" class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o fa-lg"> </i></button>
                        </form>

                      <?php } else {?>  <button type="butto" name="" class="btn btn-danger btn-sm " onclick="swal('Visitor sub type has records with this Main Type, Please Delete Those records before you delete main visitor type...!');" ><i class="fa fa-trash-o fa-lg"> </i></button> <?php }?>



										  
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div><!-- end view -->

<div class="modal fade" id="updateModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Visitors Main Type</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="updateModalDiv" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>


		 
		<div class="modal fade" id="addModal"><!-- Edit Modal -->
			<div class="modal-dialog">
				<div class="modal-content border-primary">
					<div class="modal-header bg-primary">
						<h5 class="modal-title text-white">Add Visitors Main Type</h5>
						<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form id="addMainVisitorFrm" action="controller/addMainTypeVisitorsController.php" method="post" enctype="multipart/form-data">
							<div class="form-group row">
								<label for="main_type_name" class="col-sm-4 col-form-label">Visitor Main Type Name <span class="required">*</span></label>
								<div class="col-sm-8" id=" ">
									<input type="text" class="form-control" name="main_type_name" id="main_type_name">
								</div>
							</div>

					 <div class="form-group row">
		                <label for="visitor_type" class="col-sm-4 col-form-label">Visitor Type <span class="required">*</span></label>
		                <div class="col-sm-8" id="">
		                 
		                  <input type="text" class="form-control onlyNumber" inputmode="numeric" name="visitor_type" id="visitor_type"  >
		                </div>
		              </div>
							 
							<div class="form-group row">
								<label for="main_type_image" class="col-sm-4 col-form-label">Main image <span class="required">*</span></label>
								<div class="col-sm-8">
									<input type="file" class="form-control-file photoOnly" name="main_type_image" accept="image/*" id="main_type_image">
								</div>
							</div>
							<div class="form-group row">
								<label for="visitor_main_full_img" class="col-sm-4 col-form-label">Full image <span class="required">*</span></label>
								<div class="col-sm-8">
									<input type="file" class="form-control-file photoOnly" name="visitor_main_full_img" accept="image/*" id="visitor_main_full_img">
								</div>
							</div>
							<div class="form-footer text-center">
								<button type="submit" name="addMainTypeVisitor" value="addMainTypeVisitor" class="btn btn-success"><i class="fa fa-check-square-o"></i>Add</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><!--end modal -->
	</div>
</div>
 
