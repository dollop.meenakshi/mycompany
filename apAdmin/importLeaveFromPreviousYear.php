<?php
extract(array_map("test_input", $_POST));
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
extract($data);
$from_year = (int)$_REQUEST['from_year'];
$to_year = (int)$_REQUEST['to_year'];
$block_id = (int)$_REQUEST['block_id'];
$floor_id = (int)$_REQUEST['floor_id'];
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-6">
        <h4 class="page-title"> Add Leave From Previous Year</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6 ">
        <div class="btn-group float-sm-right">
            <a href="leaveDefaultCount" class="btn mr-1 btn-sm btn-primary waves-effect waves-light">Back </a>
        </div>
     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="getLeavesFromPreviousYearForm" action="" enctype="" method="GET">
                <div class="row">
                    <div class="col-md-3 ">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">From Year <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select  type="text" id="from_year" required="" class="form-control" name="from_year" onchange="changeYear(this.value)">
                                    <option value="">-- Select From Year --</option>
                                    <option <?php if ($from_year == $onePreviousYear) {
                                                echo 'selected';
                                            } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                                    <option <?php if ($from_year == $currentYear) {
                                                echo 'selected';
                                            } ?>  value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                                    <option <?php if ($from_year == $nextYear) {
                                                echo 'selected';
                                            } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-3 ">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">To Year <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select  type="text" id="to_year" required="" class="form-control" name="to_year" >
                                    <option value="">-- Select To Year --</option>
                                    <?php if($from_year != '' && $from_year>0){
                                        $nextYear = $from_year+1;
                                        $twoNextYear = $nextYear+1;
                                        ?>
                                        <option <?php if($to_year == $nextYear){ echo "selected";} ?> value="<?php echo $nextYear; ?>"><?php echo $nextYear; ?></option>
                                        <option <?php if($to_year == $twoNextYear){ echo "selected";} ?> value="<?php echo $twoNextYear; ?>"><?php echo $twoNextYear; ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-3 ">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                            <select name="block_id" class="form-control single-select" required="" onchange="getVisitorFloorByBlockId(this.value)">
                                <option value="">-- Select Branch --</option> 
                                <?php 
                                    $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                                    while ($blockData=mysqli_fetch_array($qb)) {
                                ?>
                                <option <?php if($block_id==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-3 ">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                            <select  type="text" id="floor_id" required="" class="form-control single-select" name="floor_id" onchange="getLeaveTypeByFloorid(this.value)">
                                <option value="">-- Select Department --</option>
                                <?php
                                $floors=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$block_id' $blockAppendQuery");
                                while ($floorsData=mysqli_fetch_array($floors)) {
                                ?>
                                <option <?php if($floor_id==$floorsData['floor_id']) { echo 'selected';} ?> value="<?php echo $floorsData['floor_id'];?>"> <?php echo $floorsData['floor_name']; ?></option>
                                <?php }?>
                            </select>
                            </div>
                        </div> 
                    </div>
                  </div>     
                  <div class="form-footer text-center">
                    <button type="submit" class="btn btn-success"> GET </button>
                  </div>
              </form>
           
            </div>
          </div>
        </div>
      </div><!--End Row-->

      <?php if($from_year>0 && $to_year>0 && $block_id>0 && $floor_id>0){ ?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <?php                 
                            $q = $d->select("leave_default_count_master,floors_master","leave_default_count_master.society_id='$society_id' AND leave_default_count_master.floor_id='$floor_id' AND leave_default_count_master.leave_year='$from_year' AND leave_default_count_master.floor_id=floors_master.floor_id $blockAppendQueryFloor");
                            if(mysqli_num_rows($q)>0){
                        ?>
                        <form id="" action="controller/LeaveCountController.php" enctype="multipart/form-data" method="post">
                        <?php 
                        $i=0;
                        while ($data = mysqli_fetch_array($q)) { 
                            $ltq = $d->selectRow('leave_type_name',"leave_type_master","leave_type_id='$data[leave_type_id]'");
                            $leaveTypeData = mysqli_fetch_assoc($ltq);
                            ?>
                            <div class="row m-0">
                                <div class="col-md-12">
                                    <div class="form-group row w-100 mx-0">
                                        <div class="col-lg-4  col-md-4">
                                            <input type="text" class="form-control" disabled value="<?php echo $leaveTypeData['leave_type_name']; ?>">
                                            <input type="hidden" value="<?php echo $data['leave_type_id']; ?>" name="leave_type_id[<?php echo $i;?>]" id="leave_type_id_<?php echo $i;?>">
                                            <input type="hidden" value="<?php echo $to_year; ?>" name="leave_year">
                                            <input type="hidden" value="<?php echo $block_id; ?>" name="block_id">
                                            <input type="hidden" value="<?php echo $floor_id; ?>" name="floor_id">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">No Of Leaves <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12">
                                            <input step="0.5" min="0" type="text" placeholder="No Of Leaves " required="" value="<?php echo $data['number_of_leaves']; ?>" name="number_of_leaves[<?php echo $i;?>]"  id="number_of_leaves_<?php echo $i;?>" class="form-control onlyNumber only5DecimalPoint" onkeyup="setUseInMonthLeaves(<?php echo $i;?>)">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Use In Month<span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12">
                                            <select class="form-control single-select" name="leaves_use_in_month[<?php echo $i;?>]" id="leaves_use_in_month_<?php echo $i;?>">
                                                <?php $x= 1;
                                                    for($a=($x-0.5); $a<=$data['number_of_leaves']; $a+=0.5){ ?>
                                                        <option <?php if($data['leaves_use_in_month']==$a){echo 'selected';} ?> value="<?php echo $a; ?>"><?php echo $a; ?></option>
                                                <?php } ?>
                                            </select>
                                            <!-- <input type="text" placeholder="Use In Month" required="" value="<?php if($data['leaves_use_in_month']>0){echo $data['leaves_use_in_month'];} ?>" name="leaves_use_in_month[]" class="form-control onlyNumber"> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Carry Forward</label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select  type="text" required="" class="form-control" id="carry_forward_to_next_year_<?php echo $i;?>" onchange="carryForward(<?php echo $i;?>)" name="carry_forward_to_next_year[<?php echo $i;?>]" id="carry_forward_to_next_year_<?php echo $i;?>">
                                                <option <?php if(isset($data) && $data['carry_forward_to_next_year'] == 1){echo "selected";} ?> value="1">No</option>
                                                <option <?php if(isset($data) && $data['carry_forward_to_next_year'] == 0){echo "selected";} ?> value="0">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-4 <?php if(isset($data) && $data['carry_forward_to_next_year'] == 0){echo '';}else{echo 'd-none';} ?> carryForward<?php echo $i;?>">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Maximum Carry Forward</label>
                                        <div class="col-lg-12 col-md-12" id="">
                                            <select class="form-control single-select" name="max_carry_forward[<?php echo $i;?>]" id="max_carry_forward_<?php echo $i;?>" onchange="setMinCarryForward(<?php echo $i;?>)">
                                                <?php for($j=$data['number_of_leaves']; $j>=0.5; $j-=0.5){ ?>
                                                    <option <?php if($data['max_carry_forward']==$j){echo 'selected';} ?> value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-4 <?php if(isset($data) && $data['carry_forward_to_next_year'] == 0){echo '';}else{echo 'd-none';} ?> carryForward<?php echo $i;?>">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Minimum Carry Forward</label>
                                        <div class="col-lg-12 col-md-12" id="">
                                            <select class="form-control single-select" name="min_carry_forward[<?php echo $i;?>]" id="min_carry_forward_<?php echo $i;?>">
                                                <?php $y= 1;
                                                    for($k=($y-0.5); $k<=$data['max_carry_forward']; $k+=0.5){ ?>
                                                    <option <?php if($data['min_carry_forward']==$k){echo 'selected';} ?> value="<?php echo $k; ?>"><?php echo $k; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Pay Out</label>
                                        <div class="col-lg-12 col-md-12" id="">
                                            <select  type="text" required="" class="form-control" name="pay_out[<?php echo $i;?>]" id="pay_out_<?php echo $i;?>">
                                                <option <?php if(isset($data) && $data['pay_out'] == 0){echo "selected";} ?> value="0">Yes</option>
                                                <option <?php if(isset($data) && $data['pay_out'] == 1){echo "selected";} ?> value="1">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Remark</label>
                                        <div class="col-lg-12 col-md-12" id="">
                                            <textarea type="text" class="form-control" name="pay_out_remark[<?php echo $i;?>]" id="pay_out_remark_<?php echo $i;?>" placeholder="Remark"><?php echo $data['pay_out_remark']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } ?>
                        <div class="form-footer text-center">
                            <input type="hidden" name="addLeaveFromPreviousYear" value="addLeaveFromPreviousYear">
                            <button type="submit" class="btn btn-success"><i class="fa fa-plus mr-1"></i> Add </button>
                        </div>
                        </form>
                        <?php }else{ ?>
                            <h4>No Data Found!</h4>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
      <?php } ?> 

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script type="text/javascript">
    function changeYear(year)
    {
        const currentYear = new Date().getFullYear(); // 2020
        const nextYear =  parseInt(year)+1;
        const twoNextYear =  nextYear+1;
        showHtml =`<option value="">-- Select To Year --</option>
                    <option value="`+nextYear+`">`+nextYear+`</option>
                    <option value="`+twoNextYear+`">`+twoNextYear+`</option>`;
        ;
        $('#to_year').html(showHtml);
    }

    function carryForward(id){
        value = $('#carry_forward_to_next_year_'+id).val();
        if(value == 0){
            $('.carryForward'+id).removeClass('d-none');
        }else{
            $('.carryForward'+id).addClass('d-none');
        }
    }

    function setUseInMonthLeaves(id){
        value = $('#number_of_leaves_'+id).val();
        var splitVal = value.split('.'); 
        var number = splitVal[0];
        var decimal = splitVal[1];
        use_in_month_content = ``;
        max_carry_forward_content = ``;
        min_carry_forward_content = ``;
        month_use_value = number>=31?31:number;
        var x = 1;
        for (var i = (x-0.5); i <=month_use_value; i += 0.5) {
            use_in_month_content += `<option value="`+i+`">`+i+`</option>`;
        }
        for (var j = number; j >=0.5; j -= 0.5) {
            max_carry_forward_content += `<option value="`+j+`">`+j+`</option>`;
        }
        for (var k = (x-0.5); k <=number; k += 0.5) {
            min_carry_forward_content += `<option value="`+k+`">`+k+`</option>`;
        }
        $('#leaves_use_in_month_'+id).html(use_in_month_content);
        $('#max_carry_forward_'+id).html(max_carry_forward_content);
        $('#min_carry_forward_'+id).html(min_carry_forward_content);
    }

    function setMinCarryForward(id){
        value = $('#max_carry_forward_'+id).val();
        min_carry_forward_content = ``;
        var x = 1;
        for (var l = (x-0.5); l <value; l += 0.5) {
            min_carry_forward_content += `<option value="`+l+`">`+l+`</option>`;
        }
        $('#min_carry_forward_'+id).html(min_carry_forward_content);
    }
    </script>

<style>
    input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	  -webkit-appearance: none;
	  margin: 0;
	}
</style>