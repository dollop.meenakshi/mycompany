<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" />
<?php
extract(array_map("test_input", $_POST));
if (isset($edit_advance_salary)) {
    $q = $d->select("advance_salary", "advance_salary_id='$advance_salary_id'");
    $data = mysqli_fetch_array($q);
?>
<?php extract($data);
}

?>

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-6">
                <h4 class="page-title"> Add Advance Salary</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                    <form id="addAdvanceSalary" action="controller/AdvanceSalaryController.php" enctype="multipart/form-data" method="post">
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select id="block_id" name="block_id" class="form-control inputSl single-select" onchange="getFloorByBlockIdAddSalary(this.value)" required="">
                                        <option value="">Select Branch</option>
                                        <?php
                                        $qd = $d->select("block_master", "block_master.society_id='$society_id' AND block_master.block_status=0 $blockAppendQueryOnly ");
                                        while ($depaData = mysqli_fetch_array($qd)) {
                                        ?>
                                            <option <?php if ($data['block_id'] == $depaData['block_id']) { echo 'selected';} ?> <?php if ($block_id == $depaData['block_id']) {echo 'selected';} ?> value="<?php echo  $depaData['block_id']; ?>"><?php echo $depaData['block_name']; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-4 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select type="text" id="floor_id" required="" onchange="getUserByFloorId(this.value)" class="form-control single-select floor_id inputSl" name="floor_id">
                                        <option value="">-- Select --</option>
                                        <?php
                                        if (isset($data)) {
                                            
                                        $floors = $d->select("floors_master,block_master", "block_master.block_id =floors_master.block_id AND  floors_master.society_id='$society_id' AND floors_master.block_id = '$data[block_id]' $blockAppendQuery");
                                        while ($floorsData = mysqli_fetch_array($floors)) {
                                        ?>
                                            <option <?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] == $data['floor_id']) {
                                                        echo "selected";
                                                    } ?> value="<?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] != "") {
                                                                    echo $floorsData['floor_id'];
                                                                } ?>"> <?php if (isset($floorsData['floor_name']) && $floorsData['floor_name'] != "") {
                                                                            echo $floorsData['floor_name'] . "(" . $floorsData['block_name'] . ")";
                                                                        } ?></option>
                                        <?php } }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-4 col-form-label">Employee <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select name="user_id" id="user_id" class="inputSl form-control single-select">
                                        <option value="">-- Select Employee --</option>
                                        <?php
                                         if (isset($data)) {
                                        $user = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND user_status !=0 AND floor_id = '$data[floor_id]' $blockAppendQueryUser");
                                        while ($userdata = mysqli_fetch_array($user)) {
                                        ?>
                                            <option <?php if ($data['user_id'] == $userdata['user_id']) {
                                                        echo 'selected';
                                                    } ?> value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name']; ?>
                                            </option>
                                        <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Amount<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <input autocomplete="off" type="text"  name="advance_salary_amount" id="advance_salary_amount" value="<?php if(isset($data['advance_salary_amount']) && $data['advance_salary_amount'] !=""){ echo $data['advance_salary_amount']; } ?>" class="form-control rstFrm onlyNumber rstFrm">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Advance Salary Date<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <input autocomplete="off"  type="text" value="<?php if(isset($data['advance_salary_date']) && $data['advance_salary_date'] !=""){ echo $data['advance_salary_date']; } ?>" name="advance_salary_date" id="advance_salary_date" class="form-control rstFrm onlyNumber rstFrm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Mode<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select class="form-control inputSl" required name="advance_salary_mode" id="advance_salary_mode">
                                        <option value="">-- Select --</option>
                                        <option <?php if (isset($data) && $data['advance_salary_mode'] == 0) {
                                                    echo "selected";
                                                } ?> value="0">Bank Transaction</option>
                                        <option <?php if (isset($data) && $data['advance_salary_mode'] == 1) {
                                                    echo "selected";
                                                } ?> value="1">Cash</option>
                                        <option <?php if (isset($data) && $data['advance_salary_mode'] == 2) {
                                                    echo "selected";
                                                } ?> value="2">Cheque</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4  col-md-4 col-form-label">Remark</label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <textarea name="advance_salary_remark" value="<?php if(isset($data['advance_salary_remark']) && $data['advance_salary_remark'] !=""){ echo $data['advance_salary_remark']; } ?>" id="advance_salary_remark" class="form-control rstFrm"><?php if(isset($data['advance_salary_remark']) && $data['advance_salary_remark'] !=""){ echo $data['advance_salary_remark']; } ?></textarea>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <input type="hidden" id="advance_salary_id" name="advance_salary_id"  class="rstFrm" value="<?php if(isset($data['advance_salary_id']) && $data['advance_salary_id'] !=""){ echo $data['advance_salary_id']; } ?>">
                                <button type="submit" class="btn btn-success hideAdd course_lesson_button"><i class="fa fa-check-square-o"></i><?php if (isset($edit_advance_salary)) { echo "Update"; } else { echo "Add"; } ?> </button>
                                <input type="hidden" name="addAdvanceSalary" value="addAdvanceSalary">
                                <button type="button" value="add" class="btn btn-danger " onclick="resetForm()"><i class="fa fa-check-square-o"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--End Row-->
    </div>
</div>
<!--End content-wrapper-->
