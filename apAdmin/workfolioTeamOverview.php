<?php error_reporting(0);
$tId = $_REQUEST['tId'];
$date = $_REQUEST['date'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Workfolio Team </h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
            <a href="javascript:void(0)" onclick="DeleteAll('deleteChatGroupMember');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
      </div>

        <form class="workfolioFilterForm" action="">
            <div class="row pt-2 pb-2">
                <div class="col-md-3 form-group">
                    <select name="tId" class="form-control single-select" required="" onchange="getWorkfolioEmployees(this.value)">
                        <option value="">-- Select Team --</option> 
                        <?php 
                            $qt=$d->select("workfolio_team","");  
                            while ($teamData=mysqli_fetch_array($qt)) {
                        ?>
                        <option  <?php if($tId==$teamData['workfolio_team_id']) { echo 'selected';} ?> value="<?php echo $teamData['workfolio_team_id'];?>" ><?php echo $teamData['teamName'];?></option>
                        <?php } ?>
                    </select>
                </div>
				<div class="col-md-3 form-group">
					<input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" readonly name="date" value="<?php if ($date!='') { echo $date;} else{echo date('Y-m-d');} ?>">
                </div>
                
                <div class="col-md-3 form-group">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
		<div class="row">
			<?php
				if (isset($tId) && $tId>0) {

			  	$q = $d->select("workfolio_timesheet, workfolio_team, workfolio_employees", "workfolio_timesheet.teamId=workfolio_team.teamId AND workfolio_team.workfolio_team_id='$tId' AND workfolio_employees.email=workfolio_timesheet.email AND workfolio_employees.teamId=workfolio_timesheet.teamId AND workfolio_timesheet.date='$date'", "");
			  	//$q = $d->select("workfolio_team,workfolio_employees,workfolio_timesheet LEFT JOIN workfolio_appsandwebsiteshistory ON workfolio_timesheet.email=workfolio_appsandwebsiteshistory.email AND workfolio_timesheet.teamId=workfolio_appsandwebsiteshistory.teamId AND DATE_FORMAT(workfolio_appsandwebsiteshistory.date,'%Y-%m-%d')='$date'", "workfolio_timesheet.teamId=workfolio_team.teamId AND workfolio_team.workfolio_team_id='$tId' AND workfolio_employees.email=workfolio_timesheet.email AND workfolio_employees.teamId=workfolio_timesheet.teamId AND workfolio_timesheet.date='$date'", "GROUP BY workfolio_appsandwebsiteshistory.title");
			 	
				if(mysqli_num_rows($q)>0){
					$counter = 1;
			  	while ($data = mysqli_fetch_array($q)) {
			?>
				<div class="col-md-6 col-lg-4">
					<div class="card radius-10 p-2">
						<div class="d-flex align-items-center">
							<div class="text-success">
								<img width="50" height="50" class="rounded-circle shadow lazyload" onerror="this.src='img/user.png'" src="<?php echo $data['profilePictureUrl']; ?>">
							</div>
							<div class="pl-2">
								<p class="mb-0"><?php echo $data['displayName']; ?></p>
								<?php if($data['dayType']=="present"){
									$textColorClass = "text-success";
									$badgeColorClass = "badge-success";
								}else{
									$textColorClass = "text-danger";
									$badgeColorClass = "badge-danger";
								} ?>
								<p class="mb-0 badge <?php echo $badgeColorClass; ?>" style="font-size: 12px;"><?php echo ucfirst($data['dayType']); ?></p>
							</div>
							<div class="pl-2  ms-auto">
								<?php if($data['productiveSec']>0){
									$productive_pre = round((($data['productiveSec']+$data['untrackedSec'])/$data['workedSec'])*100);
									}else{
									$productive_pre = 0;
									}  
								?>
								<h5 class="mb-0 <?php echo $textColorClass; ?> text-right"><?php echo $productive_pre; ?>%</h5>
								<p class="mb-0" style="font-size: 12px;">Productive</p>
							</div>
						</div>
						<div class="text-center pt-2">
							<?php $totalApps = $d->selectRow("title","workfolio_appsandwebsiteshistory","teamId='$data[teamId]' AND email ='$data[email]' AND DATE_FORMAT(date,'%Y-%m-%d') = '$date'","GROUP BY title");?>
							<span class="bg-light-primary rounded p-1 text-dark"><?php echo mysqli_num_rows($totalApps); ?> Apps</span>
						</div>
						<div class="text-center p-2">
							<div class="progress" style="height:10px;">
								<div class="progress-bar bg-success progress-bar-striped" style="width:<?php echo $productive_pre; ?>%"></div>
							</div>
						</div>
						<div class="d-flex align-items-center">
							<div class="pl-2">
								<p class="mb-0"><?php if($data['in_time'] != '0000-00-00 00:00:00'){ echo date("h:i A", strtotime($data['in_time']));}else{echo '00:00';} ?></p>
								<p class="mb-0" style="font-size: 12px;">Checked-in</p>
							</div>
							
							<div class="pl-2  ms-auto">
								<?php
									$init = $data['workedSec'];
									$hours = floor($init / 3600);
									$minutes = floor(($init / 60) % 60);
									$seconds = $init % 60;
								?>
								<p class="mb-0"><?php echo sprintf('%02d', $hours).'h:'.sprintf('%02d', $minutes).'m'; ?></p>
								<p class="mb-0" style="font-size: 12px;">Hours Worked</p>
							</div>
						</div>
					</div>
				</div> 
			<?php } }else{ ?>
				<div class="card col-md-12" role="alert">
					<div class="card-body">
						<span><strong>Note :</strong> No Data Found!</span>
					</div>
				</div>
			 <?php } } else {  ?>
				<div class="card col-md-12" role="alert">
					<div class="card-body">
						<span><strong>Note :</strong> Please Select Team Employee</span>
					</div>
				</div>
			<?php } ?>
	  	</div><!-- End Row-->

  </div>
    <!-- End container-fluid-->
    
</div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
