
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">MyCo  Package Transactions </h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="welcome">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page"> Package Transactions</li>
        </ol>
      </div>
      <div class="col-sm-3">
        <div class="btn-group float-sm-right">
          <a href="buyPlan" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Renew  Plan </a>

          
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Society</th>
                    <th>Package</th>
                    <th>Mode</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Txt Id</th>
                    <th>Invoice No</th>
                    <th>Action</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php
                   if ($adminData['role_id']==1) {
                  $q=$d->select("transection_master,society_master","society_master.society_id=transection_master.society_id AND  transection_master.payment_status='success'","");
                  } else {
                    $q=$d->select("transection_master,society_master","society_master.society_id=transection_master.society_id AND  transection_master.payment_status='success' AND transection_master.society_id='$society_id'","");
                  }
                  $i = 0;
                  while($row=mysqli_fetch_array($q))
                  {
                  // extract($row);
                  $i++;
                  
                  ?>
                  <tr>
                    
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['society_name']; ?></td>
                    <td><?php echo $row['package_name']; ?></td>
                    <td><?php echo $row['payment_mode']; ?>-<?php echo $row['bankcode']; ?></td>
                    <td><?php echo $row['transection_amount']; ?></td>
                    <td><?php echo $row['transection_date']; ?></td>
                    <td><?php echo $row['payment_txnid']; ?></td>
                    <td><?php echo $row['invoice_no']; ?></td>
                    <td><form action="viewPkgReceipt" method="post">
                          <input type="hidden" name="transection_id" id="votingData" value="<?php echo $row['transection_id'] ?>">
                      <button type="submit" class="btn btn-primary btn-sm" name="pullingReport">View </button>
                        
                      </form>
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
                
              </div>
            </table>
          </div>
        </div>
      </div>
    </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->
  
  </div><!--End content-wrapper-->
  <!--Start Back To Top Button-->
