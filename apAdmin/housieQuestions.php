<?php error_reporting(0);
extract($_REQUEST);
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-6 col-6">
          <h4 class="page-title">Housie Questions</h4>
        </div>
        <div class="col-sm-3 col-6"> 
          <!-- Status:  -->
           <!-- <?php if($survey_master_result['survey_status']=="0"){
                      ?>
                <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $survey_master_result['survey_id']; ?>','SurveyClose');" data-size="small"/>
                <?php } else { 
                if (mysqli_num_rows($q)>0) {
                  ?>
               <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $survey_master_result['survey_id']; ?>','SurveyActive');" data-size="small"/>
              <?php } else {
                echo "Add Questions to start ";
              } }  ?>  -->
        </div>
        <div class="col-sm-3 col-6">
          <div class="btn-group float-sm-right">
            <a href="#" data-toggle="modal" data-target="#billCategoryModal" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Question</a>
            <a href="javascript:void(0)" onclick="DeleteAll('deleteHousieQuetion');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
            
          </div>
        </div>
      </div>
      <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>#</th>
                      <th>Question</th>
                      <th>Answer</th>
                      <th>Action</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $room_id = (int)$room_id;
                        $i = 0;
                   $q=$d->select("housie_questions_master","room_id='$room_id'","ORDER BY que_id DESC");
                   
                    while($row=mysqli_fetch_array($q))
                    {
                    extract($row);
                    $i++;
                    ?>
                    <tr>
                      <td class='text-center'>
                        <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['que_id']; ?>">
                      </td>
                      <td><?php echo $i; ?></td>
                      <td><?php echo custom_echo($row['question'],50); ?></td>
                      <td><?php echo custom_echo($row['answer'],50); ?></td>

                      <td>
                        <?php  if($result_found ==0) { ?>
                        <div style="display: inline-block;">
                          <form   class="mr-2" action="addHousieGame" method="get">
                            <input type="hidden" name="id" value="<?php echo $row['room_id']; ?>">
                            <input type="hidden" name="que_id" value="<?php echo $row['que_id']; ?>">
                            <input type="hidden" name="que" value="<?php echo 1; ?>">
                            <input type="hidden" name="editQuestion" value="true">
                            <button type="submit" class="btn btn-sm btn-primary"> Edit</button>
                          </form>
                        </div>
                        
                        <?php } ?>
                      </td>
                      
                    </tr>
                    <?php }?>
                  </tbody>
                </div>
              </table>
            </div>
          </div>
        </div>
      </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <!--Start Back To Top Button-->


<div class="modal fade" id="billCategoryModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Question</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="billCategoryValidation" method="GET" action="addHousieGame" enctype="multipart/form-data">
            <div class="row form-group">
              <label class="col-sm-6 form-control-label">Number Of  Question<span class="text-danger">*</span></label>
              <div class="col-sm-6">
                <input type="hidden" maxlength="2"  class="form-control" value="<?php echo $room_id;?>" required="" name="id">
                <input type="text" maxlength="2"  class="form-control onlyNumber" inputmode="numeric" required="" name="que">
              </div>
            </div>
            
            <div class="form-footer text-center">
              <button class="btn btn-sm btn-success"><i class="fa fa-check"></i> Next</button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>