<?php
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
if (isset($_COOKIE['bms_admin_id'])) {
  $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
  $society_id = $_COOKIE['society_id'];
}
include 'common/checkLanguage.php';
if ($_COOKIE['country_id']==101) { 
  $cgstName = $xml->string->cgst;
  $sgstName = $xml->string->sgst;
  $igstName = $xml->string->igst;
} else if ($_COOKIE['country_id']==161) {
  $igstName = $xml->string->igst;
}

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract(array_map("test_input" , $_POST));
$checS=$d->selectRow("virtual_wallet,currency","society_master","society_id='$society_id'");
$sData=mysqli_fetch_array($checS);
$virtual_wallet= $sData['virtual_wallet'];
$currency= $sData['currency'];


$q=$d->select("penalty_master,unit_master,users_master,block_master,payment_paid_request,floors_master","users_master.user_id=payment_paid_request.user_id AND payment_paid_request.penalty_id=penalty_master.penalty_id  AND block_master.block_id=users_master.block_id and penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND floors_master.floor_id = users_master.floor_id AND penalty_master.society_id='$society_id' AND penalty_master.penalty_id='$penalty_id'","ORDER BY penalty_master.penalty_date DESC");
$data=mysqli_fetch_array($q);
extract($data);
//IS_605
switch ($payment_type) {
  case '1':
   $payment_type1=0;
   $paymentView='Cash';
   $cheque_number='';
   $utr_id='';
   $bank_name='';
    break;
  case '2':
   $payment_type1=1;
   $paymentView='Cheque';
   $utr_id="";
   $bank_name=$bank_name;
   $cheque_number=$cheque_number;
    # code...
    break;
  case '3':
    # code...
   $payment_type1=2;
   $paymentView='Online';
   $bank_name='';
   $utr_id="";
   $transaction_id=$cheque_number;
   $cheque_number='';
    break;
  case '4':
    # code...
   $paymentView='UPI';
   $payment_type1=2;
   $bank_name='';
   $utr_id=$cheque_number;
   $transaction_id='';
   $cheque_number='';
    break;
  default:
    # code...
    break;
}

?>
  <form id="menApporvalReq" action="controller/penaltyController.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
    <input type="hidden" name="penalty_id" id="penalty_id" value="<?php echo $penalty_id;?>">
    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
    <input type="hidden" name="user_name" id="user_id" value="<?php echo $user_full_name;?>">
    <input type="hidden" name="paidPanalty" value="paidPanalty">
    <input type="hidden" name="payment_request_id" id="user_id" value="<?php echo $payment_request_id ;?>">
    <input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id;?>" id="balancesheet_id">
    <input type="hidden" name="payment_type" value="<?php echo $payment_type1;?>" id="payment_type">
    <input type="hidden" name="bank_name" value="<?php echo $bank_name; ?>">
    <input type="hidden" name="payment_ref_no" value="<?php echo $data['cheque_number']; ?>">
    <input type="hidden" name="unit_name" value="<?php echo $block_name.'-'.$unit_name; ?>">
    <input type="hidden" id="org_penalty_amount" name="penalty_amount_org" value="<?php echo $data['penalty_amount']; ?>">
    <input type="hidden" id="currency" name="currency" value="<?php echo $currency; ?>">
    <div class="row">
      <label for="input-10" class="col-sm-5 col-form-label">Requested By </label>
      <div class="col-sm-7">
        <?php echo $user_full_name.' - '.$user_designation.' ('.$block_name. ' - '.$floor_name.')'; ?>
      </div>
      
    </div>
    <div class="row">
      <label for="payment_type" class="col-sm-5 col-form-label">Payment Method </label>
      <div class="col-sm-7">
        <?php echo $paymentView; ?>
      </div>
      
    </div>
     <?php if($bank_name!='') { ?>
     <div class="row">
      <label for="input-10" class="col-md-5 col-form-label">Bank </label>
      <div class="col-sm-7">
         <?php echo $data['bank_name'];  ?>
      </div>
    </div>
    <?php } ?>
    <?php if($cheque_number!='') { ?>
     <div class="row">
      <label for="input-10" class="col-md-5 col-form-label">Cheque Number </label>
       <div class="col-sm-7">
        <?php echo $data['cheque_number'];  ?>
      </div>
    </div>
    <?php } ?>
     <?php if($transaction_id!='') { ?>
     <div class="row">
      <label for="input-10" class="col-md-5 col-form-label">Transaction Id </label>
       <div class="col-sm-7">
        <?php echo $data['cheque_number'];  ?>
      </div>
    </div>
    <?php } ?>
     <?php if($utr_id!='') { ?>
     <div class="row">
      <label for="input-10" class="col-md-5 col-form-label">UTR Id </label>
       <div class="col-sm-7">
         <?php echo $data['cheque_number'];  ?>
      </div>
    </div>
    <?php } ?>
    <?php //IS_846 ?>
    <div id="addBankDetails"></div>
    <input type="hidden" name="isCash" id="isCash" value="">
    <div class="form-group row">
      <label for="date-time-picker4" class="col-sm-5 col-form-label">Payment Date </label>
      <div class="col-sm-7">
        <?php echo $payment_date;?>
        <!--  <input required="" readonly="" type="text" class="form-control"  id="received_date" name="received_date"> -->
        <input type="hidden" id="date-time-picker4" readonly="" maxlength="250" required="" name="received_date" class="form-control" value="<?php echo $payment_date;?> <?php echo date("H:i:s");?>" min="<?php echo $payment_date;?>" max="<?php echo date("Y-m-d H:i");?>">
        
      </div>
      
    </div>
  
  <?php
  $now = time();
  $your_date = strtotime($penalty_date);
  $datediff = $now - $your_date;

  ?>
  <input type="hidden" id="penaltydatediff" value="<?php echo round($datediff / (60 * 60 * 24)); ?>">
  
   <?php  if($is_taxble=='1'){ ?>
  <div class="form-group row">
    <label for="penalty_amount" class="col-sm-5 col-form-label">Penalty <?php if($is_taxble=="1") { ?>Without <?php echo $xml->string->tax; } ?></label>
    
    <div class="col-sm-7">
     <?php echo $currency;?> <?php echo $amount_without_gst; ?>
    </div>
  </div>
  <div class="form-group row">
    <label for="penalty_amount" class="col-sm-5 col-form-label">Tax Amount </label>
    
    <div class="col-sm-7">
      <?php
      if ($taxble_type=="1") {
        $gstValue = $penalty_amount - ($penalty_amount  * (100/(100+$tax_slab))); 
          echo $igstName." ($tax_slab%): ".$currency.' '.number_format($gstValue,2,'.','');
      } else {
         $gstValue = $penalty_amount - ($penalty_amount * (100/(100+$tax_slab))); 
          $sepGst = $gstValue/2;
          $gstPer = $tax_slab/2;
        echo $cgstName." ($gstPer%): " .$currency.' '.number_format($sepGst,2,'.','');
        echo "<br>";
        echo $sgstName." ($gstPer%): " .$currency.' '.number_format($sepGst,2,'.','');
      }
      
      ?>
    </div>
  </div>
  
  
  <?php } //IS_1470 ?>
  
  <!-- <div class=" row">
    <label for="penalty_amount" class="col-sm-5 col-form-label">Penalty Amount  </label>
    
    <div class="col-sm-7">
      <?php echo $currency .' '.$penalty_amount;?>
    </div>
  </div> -->
  <div class=" row">
    <label for="penalty_amount" class="col-sm-5 col-form-label">Requested Amount  <span class="required">*</span></label>
    
    <div class="col-sm-7">
      <input maxlength="15" type="text" autocomplete="off" readonly="" required="" min="<?php echo $penalty_amount;?>" class="form-control onlyNumber" inputmode="numeric" id="penalty_amount" value="<?php echo $payment_amount;?>"  name="penalty_amount">
    </div>
  </div>
  <div class="form-group row" >
    <label for="" class="col-sm-5 col-form-label">  </label> 
     <div class="col-sm-7 text-danger"  id="walletMsg">
      <?php  
      if ($penalty_amount<$payment_amount) {
          $difAmount = number_format($payment_amount-$penalty_amount,2,'.','');
          echo $currency.' '. $difAmount.' Credited in user wallet for overpaid';
      }

      ?>
     </div>
  </div>
  
  <div class="row">
    <label for="penalty_amount" class="col-sm-5 col-form-label">Remark  </label>
    
    <div class="col-sm-7">
      <?php echo $remark; ?>
    </div>
  </div>
   <?php if ($payment_photo!='') { ?>
  <div class="row">
    <label for="penalty_amount" class="col-sm-5 col-form-label">Photo  </label>
    
    <div class="col-sm-7">
       <a href="../img/request/<?php echo $payment_photo;?>" data-fancybox="images" data-caption="Photo Name : Photo ?>"> 
      <img width="100%" src="../img/request/<?php echo $payment_photo; ?>" alt="">
      </a>
    </div>
  </div>
    
  <?php } ?>
  <div class="form-footer text-center">
    <button id="PayPaneltyBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i>  Approve  & Paid </button>
  </div>
</form>
 <form id="payPenaltyFrm" action="controller/penaltyController.php" method="post" enctype="multipart/form-data">
   <div class=" text-center">
    <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
    <input type="hidden" name="user_email" id="user_email" value="<?php echo $user_email;?>">
    <input type="hidden" name="device" id="device" value="<?php echo $device;?>">
    <input type="hidden" name="user_token" id="user_token" value="<?php echo $user_token;?>">
    <input type="hidden" name="payment_request_id" id="user_id" value="<?php echo $payment_request_id ;?>">
    <input type="hidden" name="penalty_id" id="penalty_id" value="<?php echo $penalty_id;?>">
    <input type="hidden" name="unit_id" id="unit_id" value="<?php echo $unit_id;?>">
    <input type="hidden" name="payment_amount" id="payment_amount" value="<?php echo $payment_amount;?>">
    <input type="hidden" name="rejectPenaltyRequest" value="rejectPenaltyRequest">
    <button type="submit"  class="btn btn-danger form-btn"><i class="fa fa-times"></i> Reject Request</button>
  </div>
</form>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  jQuery(document).ready(function($){
    
    $.validator.addMethod("noSpace", function(value, element) {
      return value == '' || value.trim().length != 0;
    }, "No space please and don't leave it empty");
     $("#menApporvalReq").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules:{
          
            payment_amount:{
                required:true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
           
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                form.submit(); 
          }
    });
  });

  $(".photoOnly").change(function () {
     // alert(this.files[0].size);
     var fileExtension = ['jpeg', 'jpg', 'png'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
        swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
        $('.photoOnly').val('');
    }
});
//IS_1130
  $(".onlyNumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function calcNewPriceFinal() {
  var currency =$('#currency').val();
  var penalty_amount = parseFloat($('#penalty_amount').val());
  var org_penalty_amount = parseFloat($('#org_penalty_amount').val());
  if(org_penalty_amount<penalty_amount) {
    var difAmount = parseFloat(penalty_amount-org_penalty_amount).toFixed(2);
    $('#walletMsg').html(currency+' '+difAmount +' Credited in user wallet for overpaid');
  } else {
      $('#walletMsg').html('');
  }
}
</script>
