<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-7">
        <h4 class="page-title">SOS</h4>
      </div>
      <div class="col-sm-3 col-4">
        <div class="btn-group float-sm-right">
          <a href="sos" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
     <div class="row" id="searchedEmployees">
      <?php
        $i++;
         $q=$d->select("sos_events_master","society_id='$society_id'","");
        if(mysqli_num_rows($q)>0) {

          while ($row=mysqli_fetch_array($q)) {
            extract($row);
          
      ?>  
      <div class="col-lg-3 col-6">
        <div id="accordion1">
          <div class="card mb-2">
            <div id="collapse-1" class="collapse show" data-parent="#accordion1" style="">
              <div class="p-2">
                <div class="text-center">
                  <img style="height: 150px !important;width: 80;" onerror="this.src='../img/sos/sos_default.png'"  src="../img/sos/<?php echo $sos_image; ?>" class="img-fluid rounded "  alt="card img">
                </div>
                <div class="card-title text-uppercase text-primary text-center"> <?php echo $event_name; ?>
                </div>
                <ul class="list-unstyled">
                  <li><i class="fa fa-clock-o"></i> <?php echo $sos_duration." mins"; ?></li>
                  <!-- <li>Email : <?php echo $emp_email; ?></li> -->
                  <!-- <li>Salary : <?php if ($emp_sallary>0) { echo $emp_sallary; } ?></li> -->
                  <li><i class="fa fa-users"></i> For : <?php
                  if ($sos_for==0) {
                    echo "All";
                  } elseif ($sos_for==1) {
                    echo "Resident";
                  } elseif ($sos_for==2) {
                    echo "Security Guard";
                  } elseif ($sos_for==3) {
                    echo "Secretary";
                  } elseif ($sos_for==4) {
                    echo "Security Guard & Secretary";
                  } elseif ($sos_for==5) {
                    echo "Security Guard & Resident";
                  }
                  ?>
                  
                </ul>
                <ul  class="list-inline text-center">
                  
                  <li class="list-inline-item">
                     <form action="sos" method="post">
                      <input type="hidden" name="sos_event_id" value="<?php echo $sos_event_id; ?>">
                      <button class="btn btn-sm btn-primary" name="updateSos" value="Edit"><i class="fa fa-pencil"></i></button>
                    </form>
                  </li>

                  <li class="list-inline-item">
                    <form title="Delete" action="controller/sosController.php" method="post"  >
                      <input type="hidden" name="sos_event_id" value="<?php echo $sos_event_id; ?>">
                      <input type="hidden" value="sosDelete" name="sosDelete">
                      <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i></button> 
                    </form>
                  </li>
                 
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <?php } } else {
        echo "<img src='img/no_data_found.png'>";
      } ?>
    </div>

   
  </div>
</div>