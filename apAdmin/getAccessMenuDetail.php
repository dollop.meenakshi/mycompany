<?php //IS_1291 whole file
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
if (isset($_COOKIE['bms_admin_id'])) {
$bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
extract(array_map("test_input" , $_POST));

if(isset($app_menu_id)  ){
$q=$d->select("resident_app_menu","app_menu_id='$app_menu_id' ");
$row=mysqli_fetch_array($q);
extract($row);
?>

<form id="EditAccessMenuFrm" action="controller/statusController.php" method="POST" enctype="multipart/form-data" >
  <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
  
  <div class="form-group row">
    
    <label for="input-13" class="col-sm-2 col-form-label">Menu Name <span class="required">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control txtNumeric" name="menu_title" id="menu_title" value="<?php if(isset($app_menu_id)) {echo $menu_title;} ?> " maxlength="25" size="25" required>
    </div>
  </div>
  <?php //27march2020 ?>
  <div class="form-group row">
    
    <label for="menu_sequence" class="col-sm-2 col-form-label">Menu Sequence <span class="required">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control txtNumeric onlyNumber" inputmode="numeric" name="menu_sequence" id="menu_sequence" value="<?php if(isset($app_menu_id)) {echo $menu_sequence;} ?>" maxlength="25" size="25" required>
    </div>
  </div>
  <?php //27march2020 ?>
  <div class="form-footer text-center">
    <input type="Hidden" name="app_menu_id" value="<?php echo $app_menu_id;?>">
    <input type="submit" id="updateAccessMenuBtn" class="btn btn-primary" name="updateAccessMenuBtn" value="update">
  </div>
  
</form>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript">

$(".onlyNumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});


jQuery(document).ready(function($){
$.validator.addMethod("noSpace", function(value, element) {
return value == '' || value.trim().length != 0;
}, "No space please and don't leave it empty");
$.validator.addMethod("alpha", function(value, element) {
return this.optional(element) || value == value.match(/^[a-zA-Z ]*$/);
}, jQuery.validator.format("Please enter a Valid Name."));
$('.txtNumeric').keypress(function(event)
{
var kcode = event.keyCode;
if (kcode == 8 ||
kcode == 9 ||

kcode == 95 ||
kcode == 45 ||
kcode == 32 ||
(kcode > 47 && kcode < 58) ||
(kcode > 64 && kcode < 91) ||
(kcode > 96 && kcode < 123))
{
return true;
}
else
{
return false;
}
});

$("#EditAccessMenuFrm").validate({
errorPlacement: function (error, element) {
  if (element.parent('.input-group').length) {
  error.insertAfter(element.parent());      // radio/checkbox?
  } else if (element.hasClass('select2-hidden-accessible')) {
  error.insertAfter(element.next('span'));  // select2
  element.next('span').addClass('error').removeClass('valid');
  } else {
  error.insertAfter(element);               // default
  }
  },
  rules: {
  menu_title:
    {
    required: true,
    noSpace: true,
    alpha:true,
    },
    menu_sequence:
    {
    required: true,
    noSpace: true,
    }
  },
  messages: {
  menu_title: {
  required: "Please enter Menu Name",
  },
  menu_sequence: {
  required: "Please enter sequence number",
  }
  }
  });
});

</script>

<?php }   ?>