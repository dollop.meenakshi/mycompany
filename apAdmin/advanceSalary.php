<?php

error_reporting(0);
$lsId = (int) $_REQUEST['lsId'];
$cpId = (int) $_REQUEST['cpId'];
$bId = (int) $_REQUEST['bId'];
$dId = (int) $_REQUEST['dId'];
$uId = (int)$_REQUEST['uId'];
$status = $_REQUEST['status'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-6 col-md-6 col-6">
                <h4 class="page-title">Advance Salary</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
                <div class="btn-group float-sm-right">
                    <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
                    <a href="addAdvanceSalary"  onclick="buttonSettingForLmsLesson()" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteAdvanceSalary');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <form action="" class="branchDeptFilterWithUser">
            <div class="row pt-2 pb-2">
                <?php  include('selectBranchDeptEmpForFilterAll.php'); ?> 
                <div class="col-lg-2 col-6">
                    <select name="status"  class="form-control ">
                        <option value="">All</option> 
                        <option <?php if($status=='1') { echo 'selected';} ?> value="1"  >Paid</option>
                        <option <?php if($status=='0') { echo 'selected';} ?>  value="0" >Unpaid</option>
                    </select>
                </div>
                <div class="col-md-2 col-6 form-group">
                    <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-31 days'));} ?>">   
                  </div>
                  <div class="col-md-2 col-6 form-group">
                    <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
                  </div> 
                <div class="col-md-2 form-group mt-auto">
                    <input class="btn btn-success btn-sm " type="submit" name="getReport"  value="Get Data">
                </div>
            </div>

        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-body">
                        <div class="table-responsive">
                            <?php 
                            $i = 1;
                            if($dId>0){
                                $dptFilter = " AND users_master.floor_id='$dId'";
                            }
                            if($bId>0){
                                $blkFilter = " AND users_master.block_id='$bId'";
                            }else {
                                $limit = " LIMIT 1000";
                            }

                            if (isset($uId) && $uId > 0) {
                              $userFilterQuery = "AND advance_salary.user_id='$uId'";
                            }
                            if (isset($status) && $status!='') {
                              $paidStatus = "AND advance_salary.is_paid='$status'";
                            }

                            if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                              $dateFilterQuery = " AND advance_salary.advance_salary_date BETWEEN '$from' AND '$toDate'";
                            }

                            $q = $d->selectRow('paid_by_admin.admin_name AS paid_by_admin_name,advance_salary.*,users_master.*,floors_master.floor_id,bms_admin_master.admin_id,bms_admin_master.admin_name,floors_master.floor_name,block_master.block_id,block_master.block_name',"advance_salary LEFT JOIN bms_admin_master AS paid_by_admin ON paid_by_admin.admin_id= advance_salary.amount_receive_by LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=advance_salary.advance_salary_created_by,users_master LEFT JOIN floors_master ON floors_master.floor_id=users_master.floor_id LEFT JOIN block_master ON block_master.block_id=users_master.block_id ","advance_salary.society_id=$society_id AND users_master.user_id=advance_salary.user_id AND advance_salary.advance_salary_delete_status=0  $dptFilter $blkFilter $userFilterQuery  $dateFilterQuery $paidStatus","ORDER BY advance_salary.advance_salary_id DESC $limit" );
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>Return</th>
                                        <th>Action</th>
                                        <th>Employee Name</th>
                                        <th>Department</th>
                                        <th>Given Date</th>
                                        <th>Amount</th>
                                        <th>Created By</th>
                                        <th>Reason</th>
                                        <th>Retun Date</th>
                                        <th>Retun By</th>
                                        <th>Retun Remark</th>
                                        <th>Given Mode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    while ($data=mysqli_fetch_assoc($q)) {
                                       ?>
                                       <tr>
                                           <td>
                                               <?php 
                                                    if(isset($data['is_paid']) && $data['is_paid'] ==0){ ?>
                                                    <input type="hidden" name="id" id="id"  value="<?php echo $data['advance_salary_id']; ?>">
                                                    <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['advance_salary_id']; ?>">
                                                    <?php } else { ?>
                                                        <input type="checkbox" disabled name="" class="multiDelteCheckbox" value="">
                                                    <?php } ?>
                                                    </td>
                                                    <td><?php echo $i++; ?></td>
                                                    <td> <?php if(isset($data['is_paid']) && $data['is_paid'] ==0){ ?>
                                                        <button data-toggle="toolip" title="Return Mark as paid" type="button" class="btn btn-sm btn-danger " onclick="paidAdvanceSalary(<?php echo $data['advance_salary_id']; ?>)"> <i class="fa fa-money"></i></button>
                                                        <?php }else { ?>
                                                            <span  class="badge badge-info">Paid</span>
                                                        <?php } ?></td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <?php 
                                                        if(isset($data['is_paid']) && $data['is_paid'] ==0){
                                                        if(isset($data['advance_salary_created_by']) && $data['advance_salary_created_by'] ==$_COOKIE['bms_admin_id']){ ?>
                                                        <form action="addAdvanceSalary" method="post" >
                                                            <input type = "hidden" name="advance_salary_id" value="<?php echo $data['advance_salary_id']; ?>">
                                                            <input type = "hidden" name="edit_advance_salary" value="edit_advance_salary">
                                                            <button type="submit" class="btn btn-sm btn-primary mr-1" > <i class="fa fa-pencil"></i></button>
                                                        </form>
                                                     <?php } }
                                                     ?>
                                                     <button onclick="getAdvaceSalaryDetail( <?php echo $data['advance_salary_id'];?>)" class="btn btn-primary btn-sm "><i class="fa fa-eye" ></i></button>
                                                    
                                                     </div>
                                            </td>
                                           <td><?php echo $data['user_full_name'] ?></td>
                                           <td><?php echo $data['floor_name']."(". $data['block_name'].")";?></td>
                                           <td><?php echo date("d M Y",strtotime($data['advance_salary_date']));?></td>
                                           <td><?php echo $data['advance_salary_amount'];?></td>
                                           <td><?php echo $data['admin_name'];?></td>
                                           <td><?php custom_echo($data['advance_salary_remark'],30);?></td>
                                            <td><?php  if($data['paid_date']!='' ) { echo date("d M Y",strtotime($data['paid_date'])); }?></td>
                                             <td><?php custom_echo($data['paid_by_admin_name'],30);?></td>
                                            <td><?php custom_echo($data['advance_salary_paid_remark'],30);?></td>
                                           <td>
                                           <?php
                                           if($data['advance_salary_mode']==0)
                                            {
                                            $data['advance_salary_mode'] = "Bank Transaction";
                                            }
                                            else if($data['advance_salary_mode'] == 1)
                                            {
                                            $data['advance_salary_mode'] = "Cash";
                                            }
                                            else
                                            {
                                            $data['advance_salary_mode'] = "Cheque";
                                            }
                                            echo $data['advance_salary_mode'];
                                                ?>
                                           </td>
                                       </tr>
                                       <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

</div>
<div class="modal fade" id="addAdvanceSalaryModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Advance Salary</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-2 " style="align-content: center;">
                <div class="row">
                    <div class="card-body">
                        <form id="addAdvanceSalary" action="controller/AdvanceSalaryController.php" enctype="multipart/form-data" method="post">
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select id="block_id" name="block_id" class="form-control inputSl single-select" onchange="getFloorByBlockIdAddSalary(this.value)" required="">
                                        <option value="">Select Branch</option>
                                        <?php
                                        $qd = $d->select("block_master", "block_master.society_id='$society_id' AND block_master.block_status=0 $blockAppendQueryOnly ");
                                        while ($depaData = mysqli_fetch_array($qd)) {
                                        ?>
                                            <option <?php if ($data['block_id'] == $depaData['block_id']) { echo 'selected';} ?> <?php if ($block_id == $depaData['block_id']) {echo 'selected';} ?> value="<?php echo  $depaData['block_id']; ?>"><?php echo $depaData['block_name']; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-4 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select type="text" id="floor_id" required="" onchange="getUserByFloorId(this.value)" class="form-control single-select floor_id inputSl" name="floor_id">
                                        <option value="">-- Select --</option>
                                       <!--  <?php
                                        if (isset($sid) && $sid > 0) {
                                            $fblock_id = $data['block_id'];
                                        } else {
                                            $fblock_id = $block_id;
                                        }
                                        $floors = $d->select("floors_master,block_master", "block_master.block_id =floors_master.block_id AND  floors_master.society_id='$society_id' AND floors_master.block_id = '$fblock_id' $blockAppendQuery");
                                        while ($floorsData = mysqli_fetch_array($floors)) {
                                        ?>
                                            <option <?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] == $fid) {
                                                        echo "selected";
                                                    } ?> value="<?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] != "") {
                                                                    echo $floorsData['floor_id'];
                                                                } ?>"> <?php if (isset($floorsData['floor_name']) && $floorsData['floor_name'] != "") {
                                                                            echo $floorsData['floor_name'] . "(" . $floorsData['block_name'] . ")";
                                                                        } ?></option>
                                        <?php } ?> -->
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-4 col-form-label">Employee <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select name="user_id" id="user_id" class="inputSl form-control single-select">
                                        <option value="">-- Select Employee --</option>
                                        <?php
                                        $user = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND user_status !=0 AND floor_id = '$dId' $blockAppendQueryUser");
                                        while ($userdata = mysqli_fetch_array($user)) {
                                        ?>
                                            <option <?php if ($uId == $userdata['user_id']) {
                                                        echo 'selected';
                                                    } ?> value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Amount<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <input type="text" value="" name="advance_salary_amount" id="advance_salary_amount" class="form-control rstFrm onlyNumber rstFrm">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Advance Salary Date<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <input type="text" value="" name="advance_salary_date" id="advance_salary_date" class="form-control rstFrm onlyNumber rstFrm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Mode<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select class="form-control inputSl" required name="advance_salary_mode" id="advance_salary_mode">
                                        <option value="">-- Select --</option>
                                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['advance_salary_mode'] == 0) {
                                                    echo "selected";
                                                } ?> value="0">Bank Transaction</option>
                                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['advance_salary_mode'] == 1) {
                                                    echo "selected";
                                                } ?> value="1">Cash</option>
                                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['advance_salary_mode'] == 2) {
                                                    echo "selected";
                                                } ?> value="2">Cheque</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4  col-md-4 col-form-label">Remark<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <textarea name="advance_salary_remark" id="advance_salary_remark" class="form-control rstFrm"></textarea>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <input type="hidden" id="advance_salary_id" name="advance_salary_id" value="" class="rstFrm">
                                <button type="submit" class="btn btn-success hideAdd course_lesson_button"><i class="fa fa-check-square-o"></i> </button>
                                <input type="hidden" name="addAdvanceSalary" value="addAdvanceSalary">
                                <button type="button" value="add" class="btn btn-danger " onclick="resetForm()"><i class="fa fa-check-square-o"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="advanceSalaryModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Advance Salary Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="advanceSalaryData">

        </div>
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css" />
<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
   

    function resetForm() {
        $('.rstFrm').val('');
        $('.inputSl').val('');
        $('.inputSl').select2('');
    }

    function buttonSettingForLmsLesson() {
        $('.hideupdate').hide();
        $('.hideAdd').text('Add');
        $('.rstFrm').val('');
        $('.inputSl').val('');
        $('.inputSl').select2('');
    }

   


    function popitup(url) {
        newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    }
</script>