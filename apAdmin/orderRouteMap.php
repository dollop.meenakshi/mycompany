<?php
error_reporting(0);
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}
if (isset($_GET['date_filter']) && $_GET['date_filter'] != '')
{
    $ch = validateDate($_GET['date_filter']);
    if($ch != 1)
    {
        $_SESSION['msg1'] = "Invalid Date";
    ?>
        <script>
            window.history.go(-1);
        </script>
    <?php
        exit;
    }
    $date_filter  = $_GET['date_filter'];
}
else
{
    $date_filter  = date('Y-m-d');
}
if(isset($_GET['UId']))
{
    if($_GET['UId'] != "" && !ctype_digit($_GET['UId']))
    {
        $_SESSION['msg1'] = "Invalid User Id";
    ?>
        <script>
            window.history.go(-1);
        </script>
    <?php
        exit;
    }
    $user_id = (int)$_GET['UId'];
}
else
{
    $user_id = "";
}
if(isset($_GET['RId']))
{
    if($_GET['RId'] != "" && !ctype_digit($_GET['RId']))
    {
        $_SESSION['msg1'] = "Invalid Route Id";
    ?>
        <script>
            window.history.go(-1);
        </script>
    <?php
        exit;
    }
    $route_id = (int)$_GET['RId'];
}
else
{
    $route_id = "";
}
?>
<script>
    var step = 1000;
    function abc(aa)
    {
        aa = parseInt(aa)
        step = aa;
    }
</script>
<style type="text/css">
    #map-canvas1
    {
        height: 500px;
        width:100%;
    }
    @media screen and (max-width: 480px)
    {
        #map-canvas1{
            height: 400px;
            width:100%;
        }
    }
</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row ">
            <div class="col-sm-3 col-md-6 col-6 col-12">
                <h4 class="page-title">Order Route Map</h4>
            </div>
            <div class="col-sm-6 col-md-6 col-6  col-12 text-right">
            </div>
        </div>
        <form class="branchDeptFilterWithUser">
            <div class="row  ">
                <div class="col-md-3 form-group">
                    <label class="form-control-label">Route </label>
                    <select name="RId" id="RId" class="form-control single-select" onchange="getEmpFromRoute(this.value);" required>
                        <option value="">--select--</option>
                        <?php
                        $qr = $d->selectRow("rm.route_id,rm.route_name,c.city_name","route_master AS rm JOIN cities AS c ON c.city_id = rm.city_id","rm.society_id = '$society_id' AND rm.route_active_status = 0");
                        while ($rdata = $qr->fetch_assoc())
                        {
                        ?>
                        <option <?php if ($_GET['RId'] == $rdata['route_id']) { $routeName =$rdata['route_name'] ; echo 'selected';
                        } ?> value="<?php echo $rdata['route_id']; ?>"><?php echo $rdata['route_name'] . " (" . $rdata['city_name'] . ")"; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label class="form-control-label">Employee </label>
                    <select id="UId" class="form-control single-select" name="UId" required>
                        <option value="">--Select--</option>
                        <?php
                        if(isset($route_id) && $route_id != 0)
                        {
                            $userData = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","route_employee_assign_master AS ream JOIN users_master AS um ON um.user_id = ream.user_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","ream.route_id = '$route_id' AND um.society_id = '$society_id' AND um.delete_status = 0");
                            while ($user = $userData->fetch_assoc())
                            {
                            ?>
                        <option <?php if (isset($user_id) && $user_id == $user['user_id']) { $empName= $user['user_full_name']; echo "selected";} ?> value="<?php echo $user['user_id']; ?>"> <?php echo $user['user_full_name'] . " (" . $user['block_name'] . "-" .$user['floor_name'] . ")"; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-3 col-6 form-group">
                    <label class="form-control-label">Date </label>
                    <input type="text" class="form-control default-datepicker" autocomplete="off" id="default-datepicker" name="date_filter" value="<?php if(isset($_GET['date_filter']) && $_GET['date_filter'] != ''){ echo $date_filter = $_GET['date_filter']; }else{ echo $date_filter = date('Y-m-d'); } ?>">
                </div>
                <div class="col-md-2 form-group mt-auto">
                    <input class="btn btn-success btn-sm" type="submit" name="getReport" value="Get Data">
                </div>
            </div>
        </form>
        <?php
        if(isset($_GET['RId']) && isset($_GET['UId']) && $_GET['date_filter'])
        {
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-lg-12">
                            <?php
                            if ($user_id > 0)
                            {
                                $uq = $d->selectRow("last_tracking_gps_accuracy,last_tracking_battery_status,last_tracking_latitude,last_tracking_longitude,last_tracking_address,last_tracking_location_time,last_tracking_area,last_tracking_locality,last_tracking_mock_location,last_tracking_mock_location_app_name","users_master","user_id='$user_id' AND DATE_FORMAT(last_tracking_location_time,'%Y-%m-%d') = '$date_filter'");
                                $lastUserData= mysqli_fetch_array($uq);
                                $last_tracking_latitude = $lastUserData['last_tracking_latitude'];
                                $last_tracking_longitude = $lastUserData['last_tracking_longitude'];
                                $last_tracking_address = $lastUserData['last_tracking_address'];
                                $last_tracking_location_time = $lastUserData['last_tracking_location_time'];
                                $last_tracking_area = $lastUserData['last_tracking_area'];
                                $last_tracking_locality = $lastUserData['last_tracking_locality'];
                                $last_tracking_battery_status = $lastUserData['last_tracking_battery_status'];
                                $last_tracking_gps_accuracy = $lastUserData['last_tracking_gps_accuracy'];
                            }
                            if($lastUserData > 0)
                            {
                            ?>
                            <div><h6>Last Location: <?php echo $lastUserData['last_tracking_address'];?></h6>
                                <h6>Last Location Time: <?php if($lastUserData['last_tracking_location_time'] !="0000-00-00 00:00:00"){ echo date('d M Y h:i A',strtotime($lastUserData['last_tracking_location_time'])); } echo " (".time_elapsed_string(date("d M Y h:i A", strtotime($lastUserData['last_tracking_location_time']))); ?>)  </h6>
                                <h6>Phone Battery Level : <i class="fa fa-battery-three-quarters"></i> <?php echo $last_tracking_battery_status;?> % </h6>
                                <h6> GPS Accuracy : <i class="fa fa-compass" aria-hidden="true"></i> <?php echo $last_tracking_gps_accuracy;?> M</h6>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <div id="map-canvas1"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        else
        {
        ?>
        <div class="row">
            <label class="text-danger">Please use filters to get data</label>
        </div>
        <?php
        }
        ?>

        <?php
        if(isset($_GET['RId']) && isset($_GET['UId']) && $_GET['date_filter'])
        {
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <div class="text-center"><h6><?php echo $routeName; ?> Route Summary (<?php echo $empName; ?>)</h6></div>
                            <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Retailer</th>
                                        <th>Date</th>
                                        <th>Start Visit</th>
                                        <th>End Visit</th>
                                        <th>Total Time</th>
                                        <th>Status</th>
                                        <th>In Range</th>
                                        <th>Route</th>
                                        <th>Employee</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    $q = $d->selectRow("nom.retailer_no_order_id,retailer_master.retailer_name ,rdvtm.retailer_visit_created_date,rom.order_status,rdvtm.out_of_range,rom.retailer_order_id,rdvtm.retailer_visit_start_datetime,rdvtm.retailer_visit_end_datetime,rdvtm.retailer_visit_duration","retailer_master,retailer_daily_visit_timeline_master AS rdvtm LEFT JOIN retailer_order_master AS rom ON rom.retailer_visit_id = rdvtm.retailer_daily_visit_timeline_id
                                        LEFT JOIN retailer_no_order_master AS nom ON nom.retailer_visit_id = rdvtm.retailer_daily_visit_timeline_id 
                                        ","retailer_master.retailer_id=rdvtm.retailer_id AND rdvtm.retailer_visit_by_id = '$user_id' AND DATE_FORMAT(rdvtm.retailer_visit_created_date,'%Y-%m-%d') = '$date_filter' AND rdvtm.route_id = '$route_id'","GROUP BY rdvtm.retailer_daily_visit_timeline_id");
                                    $iNo = 1;
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td><?php echo $iNo++; ?></td>
                                        <td><?php echo $retailer_name; ?></td>
                                        <td><?php echo $date_filter; ?></td>
                                        <td><?php if($retailer_visit_start_datetime!="") { echo date('h:i A', strtotime($retailer_visit_start_datetime)); } ?></td>
                                        <td><?php  if($retailer_visit_end_datetime!="") {   echo date('h:i A', strtotime($retailer_visit_end_datetime)); } ?></td>
                                        <td><?php if($retailer_visit_start_datetime!="" && $retailer_visit_end_datetime!="") {
                                            echo $d->getTotalHoursWithNames(date('Y-m-d', strtotime($retailer_visit_start_datetime)),date('Y-m-d', strtotime($retailer_visit_end_datetime)),date('H:i:s', strtotime($retailer_visit_start_datetime)),date('H:i:s', strtotime($retailer_visit_end_datetime)));
                                        } ?>
                                         </td>
                                        <td>
                                        <?php
                                            if(!empty($retailer_order_id))
                                            {
                                                echo "<span class='badge badge-success'>Order Taken</span>";
                                            } else if ($retailer_no_order_id>0 && $retailer_visit_start_datetime!="") {
                                                 echo "<span class='badge badge-warning'>No Order</span>";
                                            }
                                            else if($retailer_visit_start_datetime=="")
                                            {
                                                echo "<span class='badge badge-danger'>Not Visited Yet</span>";
                                            }
                                        ?>
                                        </td>
                                        <td>
                                        <?php
                                            if($out_of_range == 0 && $retailer_order_id>0 || $out_of_range == 0 && $retailer_no_order_id>0)
                                            {
                                                echo "<span class='badge badge-success'>In Range</span>";
                                            }
                                            else if($out_of_range == 1 && $retailer_order_id>0 || $out_of_range == 1 && $retailer_no_order_id>0)
                                            {
                                                echo "<span class='badge badge-danger'>Out Of Range</span>";
                                            }
                                        ?>
                                        </td>
                                        <td><?php echo $routeName; ?></td>
                                        <td><?php echo $empName; ?></td>
                                        
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
        <?php
        }
        ?>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<?php
if(isset($_GET['RId']) && isset($_GET['UId']) && $_GET['date_filter'])
{
?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $d->map_key(); ?>"></script>
<?php
$dateFilterQuery = "";
if (isset($date_filter) && $date_filter != '')
{
    $dateFilterQuery .= " AND DATE_FORMAT(rom.order_date,'%Y-%m-%d') = '$date_filter' AND DATE_FORMAT(ubtm.user_back_track_date,'%Y-%m-%d') = '$date_filter'";
}
else
{
    $limitSql  = " LIMIT 100";
}
if (isset($route_id) && $route_id != '')
{
    $dateFilterQuery .= " AND ream.route_id = '$route_id'";
}
$query1 = $d->select('block_master AS bm JOIN users_master AS um ON um.block_id = bm.block_id',"um.user_id = '$user_id' AND bm.society_id = '$society_id'");
$row1 = mysqli_fetch_assoc($query1);
$lat = $sData['society_latitude'];
$lon = $sData['society_longitude'];
$lat2 = $row1['block_geofence_latitude'];
$lon2 = $row1['block_geofence_longitude'];
$user_full_name = $row1['user_full_name'];
$dump_lat= $lat2;
$dump_lng= $lon2;
$trackingArray = array();
$query = $d->selectRow("ubtm.*,rom.order_latitude,rom.order_longitude,um.user_full_name,um.country_code,um.user_mobile",'user_back_track_master AS ubtm JOIN retailer_order_master AS rom ON rom.order_by_user_id = ubtm.user_id JOIN route_employee_assign_master AS ream ON ream.user_id = ubtm.user_id JOIN users_master AS um ON um.user_id = ubtm.user_id',"ubtm.gps_accuracy < 100 AND ubtm.user_id = '$user_id' $dateFilterQuery","GROUP BY ubtm.user_back_track_id ORDER BY ubtm.user_back_track_date ASC");
while($row = mysqli_fetch_assoc($query))
{
    array_push($trackingArray,$row);
}

$get_ll = $d->selectRow("rm.retailer_name,rm.retailer_address,rm.retailer_contact_person,rm.retailer_contact_person_country_code,rm.retailer_contact_person_number,DATE_FORMAT(rom.order_date,'%H:%i:%s') AS order_time,rom.retailer_order_id,rom.order_date,rom.order_latitude,rom.order_longitude,um.user_full_name,um.user_mobile,um.country_code","retailer_order_master AS rom JOIN users_master AS um ON um.user_id = rom.order_by_user_id JOIN retailer_master AS rm ON rm.retailer_id = rom.retailer_id","rom.order_by_user_id = '$user_id' AND DATE_FORMAT(rom.order_date,'%Y-%m-%d') = '$date_filter' AND rom.order_latitude != '' AND rom.order_longitude != ''");
$ll_arr = [];
while($data = $get_ll->fetch_assoc())
{
    $ll_arr[] = $data;
}
if(count($trackingArray) > 0)
{
    $startLatitude = $trackingArray[0]['back_user_lat'];
    $startLongitude = $trackingArray[0]['back_user_long'];
    $order_latitude = $trackingArray[0]['order_latitude'];
    $order_longitude = $trackingArray[0]['order_longitude'];
}
else
{
    $startLatitude= $lat2;
    $startLongitude= $lon2;
    $order_latitude = $trackingArray[0]['order_latitude'];
    $order_longitude = $trackingArray[0]['order_longitude'];
}
?>
<script>
    function initialize()
    {
        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(<?php echo $startLatitude; ?>,<?php echo $startLongitude; ?>),
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };
        var bounds = new google.maps.LatLngBounds();
        var map = new google.maps.Map(document.getElementById('map-canvas1'),mapOptions);
        //For company Marker ANd Infowindow
        var infoWindow = new google.maps.InfoWindow();
        PoiPointP = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lon; ?>);
        // Function for adding a marker to the page.
        addPoiMarkerRedParking(PoiPointP);
        function addPoiMarkerRedParking(location) {
            marker = new google.maps.Marker({
                position: location,
                map: map,
                icon: '../img/building.png'
            });
        }
        //Attach click event to the marker.
        (function (marker) {
            google.maps.event.addListener(marker, "click", function (e) {
                infoWindow.setContent("<div  style = 'width:200px;min-height:40px;text-align:center;'><b><?php echo $sData['society_name']; ?></b><Br><i><?php echo $society_address; ?></i></div>");
                infoWindow.open(map, marker);
            });
        })(marker);
        // For branch Marker
        var infoWindow = new google.maps.InfoWindow();
        PoiPointD = new google.maps.LatLng(<?php echo $lat2; ?>, <?php echo $lon2; ?>);
        // Function for adding a marker to the page.
        addPoiMarkerRedDump(PoiPointD);
        function addPoiMarkerRedDump(location)
        {
            marker = new google.maps.Marker({
                position: location,
                map: map,
                icon: '../img/location.png'
            });
        }
        //Attach click event to the marker.
        (function (marker) {
            google.maps.event.addListener(marker, "click", function (e) {
                infoWindow.setContent("<div  style = 'width:200px;min-height:40px;text-align:center;'>Branch:  <?php echo $row1['block_name']; ?></div>");
                infoWindow.open(map, marker);
            });
        })(marker);
        var flightPlanCoordinates = [
            <?php
            for ($iNew=0; $iNew <count($trackingArray) ; $iNew++)
            {
                $lat = $trackingArray[$iNew]['back_user_lat'];
                $lon = $trackingArray[$iNew]['back_user_long'];
                echo 'new google.maps.LatLng('.$lat.', '.$lon.'),';
            }
            ?>
        ];

        <?php
        if(count($ll_arr) > 0 && !empty($ll_arr))
        {
        ?>
        var markers = [
        <?php
            foreach($ll_arr AS $key => $value)
            {
        ?>
            ["<?php echo $value['retailer_name']; ?>","<?php echo $value['order_latitude']; ?>","<?php echo $value['order_longitude']; ?>"],
            <?php
            }
            ?>
        ];

        var infoWindowContent = [
        <?php
            foreach($ll_arr AS $key => $value)
            {
        ?>
            ["<div class='info_content'><h6><?php echo $value['retailer_name']; ?></h6><p><?php echo $value['retailer_address']; ?></p><p><?php echo $value['retailer_contact_person']; ?></p><p><?php echo $value['retailer_contact_person_country_code'] . " " . $value['retailer_contact_person_number']; ?></p><p>Order Time : <?php echo $value['order_time']; ?></p><p>Order : <?php echo '#'.$value['retailer_order_id']; ?></p></div>"],
            <?php
            }
            ?>
        ];

        var infoWindow1 = new google.maps.InfoWindow(), marker1, i;

        // Place each marker on the map
        for( i = 0; i < markers.length; i++ )
        {
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker1 = new google.maps.Marker({
                position: position,
                map: map,
                icon: '../img/green-dot.png',
                title: markers[i][0]
            });

            // Add info window to marker
            google.maps.event.addListener(marker1, 'click', (function(marker1, i) {
                return function() {
                    infoWindow1.setContent(infoWindowContent[i][0]);
                    infoWindow1.open(map, marker1);
                }
            })(marker1, i));

            // Center the map to fit all markers on the screen
            map.fitBounds(bounds);
        }

        <?php
        }
        ?>

        var userCoor = [
            <?php
            for ($iNew1=1; $iNew1 <count($trackingArray) ; $iNew1++)
            {
                $lat = $trackingArray[$iNew1]['back_user_lat'];
                $lon = $trackingArray[$iNew1]['back_user_long'];
                $trakAddress = str_replace("'", "", $trackingArray[$iNew1]['back_address']);
                $trakAddress = $trakAddress;
                $gps_accuracy = $trackingArray[$iNew1]['gps_accuracy'].' M';
                $user_back_track_date = date('d M Y h:i A',strtotime($trackingArray[$iNew1]['user_back_track_date']));
                echo "['".$trakAddress."', $lat, $lon,'".$user_back_track_date."','".$gps_accuracy."'],";
            }
            ?>
        ];
        var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
        lineSymbol = {
            path: car,
            scale: .7,
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'red',
            offset: '5%',
            anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
        };
        var flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: '#2f648e',
            strokeOpacity: 1.0,
            strokeWeight: 2,
            icons: [{
                icon: lineSymbol,
                offset: '100%'
            }],
            map: map
        });
        var markerPoly, i;
        var infowindow = new google.maps.InfoWindow();
        for (i = 0; i < userCoor.length; i++)
        {
            markerPoly = new google.maps.Marker({
                position: new google.maps.LatLng(userCoor[i][1], userCoor[i][2]),
                map: map,
                icon: '../img/record-button.png'
            });
            google.maps.event.addListener(markerPoly, 'click', (function(markerPoly, i) {
                return function()
                {
                    infowindow.setContent("<div  style = 'width:200px;min-height:40px;text-align:center;'><b>Address: </b> "+userCoor[i][0]+"<br><b>Time: </b> "+userCoor[i][3]+"<br><b>GPS Accuracy : </b> "+userCoor[i][4]+"</div>");
                    infowindow.open(map, markerPoly);
                }
            })(markerPoly, i));
        }
        if (timerHandle)
        {
            clearTimeout(timerHandle);
        }
        flightPath.setMap(map);
        abc(1000);
        setTimeout(animateCircle, step, flightPath);
    }
    var tstep = 500;
    var count = 0;
    var timerHandle = null;
    function animateCircle(flightPath)
    {
        var latlong;
        count = (count + 1) % 200;
        var icons = flightPath.get('icons');
        icons[0].offset = (count / 2) + '%';
        flightPath.set('icons', icons);
        latlong = flightPath.getPath();
        var p = flightPath.GetPointAtDistance(count);
        timerHandle = setTimeout(animateCircle, step,flightPath);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script type="text/javascript">
    google.maps.LatLng.prototype.distanceFrom = function (newLatLng)
    {
        var EarthRadiusMeters = 6378137.0; // meters
        var lat1 = this.lat();
        var lon1 = this.lng();
        var lat2 = newLatLng.lat();
        var lon2 = newLatLng.lng();
        var dLat = (lat2 - lat1) * Math.PI / 180;
        var dLon = (lon2 - lon1) * Math.PI / 180;
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = EarthRadiusMeters * c;
        return d;
    }
    google.maps.LatLng.prototype.latRadians = function ()
    {
        return this.lat() * Math.PI / 180;
    }
    google.maps.LatLng.prototype.lngRadians = function ()
    {
        return this.lng() * Math.PI / 180;
    }
    google.maps.Polygon.prototype.Distance = function ()
    {
        var dist = 0;
        for (var i = 1; i < this.getPath().getLength(); i++)
        {
            dist += this.getPath().getAt(i).distanceFrom(this.getPath().getAt(i - 1));
        }
        return dist;
    }
    google.maps.Polygon.prototype.GetPointAtDistance = function (metres)
    {
        // some awkward special cases
        if (metres == 0) return this.getPath().getAt(0);
        if (metres < 0) return null;
        if (this.getPath().getLength() < 2) return null;
        var dist = 0;
        var olddist = 0;
        for (var i = 1;
        (i < this.getPath().getLength() && dist < metres); i++) {
        olddist = dist;
        dist += this.getPath().getAt(i).distanceFrom(this.getPath().getAt(i - 1));
        }
        if (dist < metres) {
        return null;
        }
        var p1 = this.getPath().getAt(i - 2);
        var p2 = this.getPath().getAt(i - 1);
        var m = (metres - olddist) / (dist - olddist);
        return new google.maps.LatLng(p1.lat() + (p2.lat() - p1.lat()) * m, p1.lng() + (p2.lng() - p1.lng()) * m);
    }
    google.maps.Polygon.prototype.GetPointsAtDistance = function (metres)
    {
        var next = metres;
        var points = [];
        // some awkward special cases
        if (metres <= 0) return points;
        var dist = 0;
        var olddist = 0;
        for (var i = 1;(i < this.getPath().getLength()); i++)
        {
            olddist = dist;
            dist += this.getPath().getAt(i).distanceFrom(this.getPath().getAt(i - 1));
            while (dist > next)
            {
                var p1 = this.getPath().getAt(i - 1);
                var p2 = this.getPath().getAt(i);
                var m = (next - olddist) / (dist - olddist);
                points.push(new google.maps.LatLng(p1.lat() + (p2.lat() - p1.lat()) * m, p1.lng() + (p2.lng() - p1.lng()) * m));
                next += metres;
            }
        }
        return points;
    }
    google.maps.Polygon.prototype.GetIndexAtDistance = function (metres)
    {
        if (metres == 0) return this.getPath().getAt(0);
        if (metres < 0) return null;
        var dist = 0;
        var olddist = 0;
        for (var i = 1;(i < this.getPath().getLength() && dist < metres); i++)
        {
            olddist = dist;
            dist += this.getPath().getAt(i).distanceFrom(this.getPath().getAt(i - 1));
        }
        if (dist < metres)
        {
            return null;
        }
        return i;
    }
    google.maps.Polyline.prototype.Distance = google.maps.Polygon.prototype.Distance;
    google.maps.Polyline.prototype.GetPointAtDistance = google.maps.Polygon.prototype.GetPointAtDistance;
    google.maps.Polyline.prototype.GetPointsAtDistance = google.maps.Polygon.prototype.GetPointsAtDistance;
    google.maps.Polyline.prototype.GetIndexAtDistance = google.maps.Polygon.prototype.GetIndexAtDistance;
</script>
<?php
}
?>
<script>
    function getEmpFromRoute(route_id)
    {
        $.ajax({
            url: "controller/routeController.php",
            cache: false,
            type: "POST",
            data: {route_id : route_id,getEmpFromRoute:"getEmpFromRoute",csrf:csrf},
            success: function(response)
            {
                response = JSON.parse(response);
                $('#UId').empty();
                $('#UId').append("<option value=''>--Select--</option>");
                $.each(response, function(key, value)
                {
                    $('#UId').append("<option value='"+value.user_id+"'>"+value.u_name+"</option>");
                });
            }
        });
    }
</script>