<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');

$uId = (int) $_REQUEST['uId'];
$dId = (int) $_REQUEST['dId'];
$bId = (int) $_REQUEST['bId'];
$mnth = (int) $_REQUEST['month_year'];
if (isset($_GET['laYear']) && $_GET['laYear'] != "") {

  extract(array_map("test_input", $_GET));
  $salary_month_name = $_GET['month_year'] . "-" . $_GET['laYear'];
  $crDate = date('Y-m-d');
  $userIds = array();
  // $userIds = join("','",$userIds); 
  $i = 1;

}

$checkMenu = $d->selectRow('master_menu.*','master_menu',"menu_link='salaryChecked'");
$getMenu = mysqli_fetch_assoc($checkMenu);

$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));

if (isset($mnth) && ($mnth > 0) && ($laYear > 0)) {
    $MonthFilterTemp = $mnth;
    $yearData  = $mnth . "-" . $laYear;
    $yearDataQuery = " AND salary_slip_master.salary_month_name='$yearData'";
  } else {
    $MonthFilterTemp = date('n') - 1;
    $yearData  = date('n',strtotime('last month')) . "-" . $currentYear;
    $yearDataQuery = " AND salary_slip_master.salary_month_name='$yearData'";
  }

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-6">
        <h4 class="page-title">Generated Salary Slip</h4> 
      </div>
      <div class="col-sm-3 col-md-6 col-6">
        <div class="btn-group float-sm-right">
          <a href="generatedSalaryReport?bId=<?php echo $bId;?>&dId=<?php echo $dId;?>&laYear=<?php echo $laYear;?>&month=<?php echo $MonthFilterTemp;?>&is_failed=0" class="btn btn-sm btn-warning waves-effect waves-light mr-1"><i class="fa fa-file-o"></i> View Report </a>
        </div>
      </div>
    </div>
    <form action="" method="get" class="branchDeptFilter">
      <div class="row pb-2">
        <?php include('selectBranchDeptForFilterAll.php'); ?>

        <div class="col-md-2 col-6">
          <select name="laYear" class="form-control" id="year" onchange="yearChange(this.value)">
            <option value="">Year</option>
            <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $onePreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $currentYear) {
                      echo 'selected';
                    } ?> <?php if ($_GET['laYear'] == '') {
                            echo 'selected';
                          } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
            <option <?php if ($_GET['laYear'] == $nextYear) {
                      echo 'selected';
                    } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear; ?></option>
          </select>
        </div>
        <div class="col-md-2 col-6">
          <select required class="form-control month_year single-select" id="month" name="month_year">
            <option value="">-- Select --</option>
            <?php
            $selected = "";
            $crMnt = date('m');
            if(isset($laYear) && $laYear !=""){
              if($laYear==date('Y')){
                 $crMnt = date('m');
                // $cntMonth = $cntMonth;
              }else if($laYear<date('Y')){
                 $crMnt = 13;
              }
           }else{
              $crMnt = date('m');
           }
            for ($m = 1; $m < $crMnt; $m++) {
              $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
              $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
              /* if (isset($_GET['month_year'])  && $_GET['month_year'] != "") {
                if ($_GET['month_year'] == $m) {
                  $selected = "selected";
                } else {
                  $selected = "";
                }
              } else {
                $selected = "";
                $k = date('n') - 1;
                if ($m == $k) {
                  $selected = "selected";
                } else {
                  $selected = "";
                }
              } */

            ?>
              <option <?php if(isset($_GET['month_year']) && $_GET['month_year']== $m || $m==(int)$_GET['month_year']){ echo "selected"; }; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $month; ?></option>
            <?php } ?>
          </select>
        </div>
        <input type="hidden" name="">
        <div class="col-lg-1 from-group col-6">
          <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get">
        </div>
      </div>
    </form>
    <!-- End Breadcrumb-->
    <input type="hidden" name="floor_id_old" id="floor_id_old" value="<?php if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                                                                        echo $_GET['dId'];
                                                                      } ?>">

    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <?php
            if (isset($bId) && $bId > 0) {
              $brnchFilterQuery = " AND users_master.block_id='$bId'";
            }
            if (isset($dId) && $dId > 0) {
              $deptFilterQuery = " AND users_master.floor_id='$dId'";
            }
            if (isset($uId) && $uId > 0) {
              $UserFilterQuery = " AND salary_slip_master.user_id='$uId'";
            }

            

            $q = $d->select("salary_slip_master LEFT JOIN bms_admin_master AS gen ON gen.admin_id = salary_slip_master.prepared_by,users_master,floors_master,block_master", "block_master.block_id=users_master.block_id AND salary_slip_master.user_id=users_master.user_id AND salary_slip_master.floor_id=floors_master.floor_id AND salary_slip_master.society_id='$society_id' AND user_status !=0 AND salary_slip_master.salary_slip_status=0  $deptFilterQuery  $UserFilterQuery   $yearDataQuery $statusFilterQuery $brnchFilterQuery", "ORDER BY salary_slip_master.salary_month_name,users_master.user_full_name ASC");
            
            $count = mysqli_num_rows($q);
            
              if ($count > 0) { ?>
                <div class="col-md-6 gneClass float-left">
                  <label> Select All</label>
                  <input type="checkbox" name="" class="selectAll ml-2" value="">
                </div>
                <div class="col-md-6 gneClass float-left">
                  <button onclick="OpenStatusModal();" class="btn btn-sm btn-primary float-right mb-2" > Checked All</button>
                </div>
            <?php 
            } ?>
            <div class="table-responsive">
            
                <table id="bulkSalary" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Status</th>
                      <th>Action</th>
                      <th>Employee</th>
                      <th>designation</th>
                      <th>Department</th>
                      <th>Gross Salary</th>
                      <th>Net Salary</th>
                      <th>Total CTC</th>
                      <th>Month</th>
                      <th>Created By</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    
                    $finalData = array();
                    while ($data = mysqli_fetch_array($q)) {
                     
                    ?>
                      <tr>
                        <td>
                          <input id="Chekbox_<?php echo $data['salary_slip_id']; ?>" type="checkbox" data-id="<?php echo $data['salary_slip_id']; ?>" name="bulkSLryCkbx[]" value="<?php echo $data['salary_slip_id']; ?>" class="blkChkcls">
                          
                        </td>
                        <td id="processKey_<?php echo $data['salary_slip_id']; ?>">
                          <span class="badge badge-primary" >Generated</span>
                        </td>
                        <td>
                          <div class="d-flex align-items-center">
                            <?php if ($data['salary_slip_status'] == 0) {
                              if($getMenu){
                              if(in_array($getMenu['menu_id'],$accessMenuIdArr)){ ?>
                              <button onclick="SalarySlipStatusChange(1, '<?php echo $data['salary_slip_id']; ?>',<?php echo $_COOKIE['bms_admin_id']; ?>)" class="btn btn-sm btn-primary mr-1 medium" aria-hidden="true" data-toggle="tooltip" title="Checked">Mark As Checked</button>
                             

                            <?php } } } ?>
                            

                            <button type="button" onclick="salarySlipGeneration(<?php echo $data['salary_slip_id']; ?>)" class="btn btn-info btn-sm salary_slip_modal" data-id="<?php echo $data['salary_id']; ?>"><i class="fa fa-eye"></i></button>
                            <?php if ($data['salary_slip_status'] == 0) {
                              $darray = explode('-', $data['salary_month_name']);
                            ?>
                              <a href="salarySlip?sId=<?php echo $data['salary_slip_id']; ?>&block_id=<?php echo $data['block_id']; ?>&floor_id=<?php echo $data['floor_id']; ?>&user_id=<?php echo $data['user_id']; ?>&year=<?php echo $darray['1']; ?>&month=<?php echo $darray['0']; ?>">
                                <button type="submit" class="btn ml-1 btn-sm btn-warning mr-2"><i class="fa fa-pencil"></i></button>
                                </a>
                          </div>
                          <?php } ?>
                        </td>
                        <td><?php echo $data['user_full_name'] ;
                         ?></td>
                        <td><?php echo $data['user_designation'];
                         ?></td>
                         <td><?php echo $data['floor_name'] .'-'.$data['block_name']; ?></td>
                         <td><?php echo $data['total_earning_salary']; ?></td>
                         <td><?php echo $data['total_net_salary']; ?></td>
                         <td><?php echo $data['total_ctc_cost']; ?></td>
                         <td><?php echo date(" M  ", strtotime($data['salary_start_date'])) . "-" . date("Y ", strtotime($data['salary_start_date'])); ?></td>
                        <td>
                          <?php echo $data['admin_name']; ?></span>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
             

            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->
  </div>
</div>

<div class="modal fade" id="bulksalaryModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Salary Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="bulkModalForm" method="post">

        <div class="modal-body" style="align-content: center;">
          
          <div class="form-group row">
            <label for="input-10" class="col-sm-6 col-form-label">Status Action </label>
            <div class="col-lg-6 col-md-6" id="">
              <?php
              $checkMenu = $d->selectRow('master_menu.*','master_menu',"menu_link='salaryChecked'");
              $getMenu = mysqli_fetch_assoc($checkMenu);
              $checkMenu2 = $d->selectRow('master_menu.*','master_menu',"menu_link='salaryPublished'");
              $getMenu2 = mysqli_fetch_assoc($checkMenu2);
              ?>
              <select class="form-control " required name="status_action" id="status_action" onclick="changeStatusAction(this.value)">
                <option value="">-- Select --</option>
                
                <?php
                     if($getMenu){
                     if(in_array($getMenu['menu_id'],$accessMenuIdArr)){ ?>
                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_slip_status'] == 1) {
                                 echo "selected";
                                 } ?> value="1"> Checked</option>
                     <?php } } ?>
                     <?php 
                     if($getMenu2){
                     if(in_array($getMenu2['menu_id'],$accessMenuIdArr)){ ?>
                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_slip_status'] == 2) {
                                 echo "selected";
                                 } ?> value="2">Checked & Published</option>
                     <?php } } ?>
              </select>
            </div>
          </div>
          <div class="sharWUser">
            <div class="form-group row ">
              <label for="input-10" class="col-sm-6 col-form-label">Share With User </label>
              <div class="col-lg-6 col-md-6" id="">
                <input type="radio" checked name="share_with_user" class="mr-2" value="1"><label> Yes</label>
                <input type="radio" name="share_with_user" class="mr-2" value="0"><label> No</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="col-md-12 text-center">
            <button type="button" class="btn btn-primary bulkModal">Save changes</button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>


<div class="modal fade" id="salarySLipModal">
  <div class="modal-dialog" style="max-width: 60%">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Salary Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div id="salarySlipData"></div>
        <div class="col-md-12 row">
          <div class="col-md-4">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Earning</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody id="earnData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>

                        <th>Deduction</th>
                        <th>Value</th>

                      </tr>
                    </thead>
                    <tbody id="deductData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
          <div class="col-md-4 contributionClass">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>

                        <th>Name</th>
                        <th>Employer Contribution</th>

                      </tr>
                    </thead>
                    <tbody id="contributionData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="row">

          <div class="col-md-6 text-center">
            <label for="exampleInputEmail1">Net Salary  (Earning - Deduction)+Reimbursement </label>
              <h6 id="netSalary"></h6>
          </div>
          <div class="col-md-6 text-center contributionClass">
            <label for="exampleInputEmail1">Total CTC (Earning+Total Contribution) </label>
              <h6 id="totalCtc"></h6>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>
<script>
  function OpenStatusModal() {

    var oTable = $("#bulkSalary").dataTable();
      var val = [];
            $(".blkChkcls:checked", oTable.fnGetNodes()).each(function(i) {
              val[i] = $(this).val();
            });
      if(val=="") {
        swal(
          'Warning !',
          'Please select at least 1 employee !',
          'warning'
        );
      } else {
        $('#bulksalaryModal').modal("show");
      }
  }

  $('.selectAll').click(function() {

    if ($(this).prop("checked") == true) {
      $('.blkChkcls').prop('checked', true);
    } else if ($(this).prop("checked") == false) {
      $('.blkChkcls').prop('checked', false);
    }
  });

  cnt = $('.blkChkcls').length;
  if (cnt <= 0) {
    $('.gneClass').hide();
  }

  function changeStatusAction(value) {
    ///sharWUser
    if (value == 2) {
      $('.sharWUser').show();
    } else {
      $('.sharWUser').hide();
    }

  }

  /* function generateSalary() {
    var val = [];
    $('#bulksalaryModal').modal();
  } */
  $('.bulkModal').click(function(e) {
      var oTable = $("#bulkSalary").dataTable();
      var val = [];
            $(".blkChkcls:checked", oTable.fnGetNodes()).each(function(i) {
              val[i] = $(this).val();
            });
      if(val=="") {
        swal(
          'Warning !',
          'Please Select at least 1 employee !',
          'warning'
        );
      } else {

        status_action = $('#status_action').val();

        if (status_action == 2) {
          share_with_user = $('input[name="share_with_user"]:checked').val();
        } else {
          share_with_user = 0
        }
        if (status_action != "") {

          $('#bulksalaryModal').modal("hide");
          var arr = $('.blkChkcls:checked').map(function() {
            var salary_slip_id = this.value;
            salary_month = '<?php echo $_GET['month_year']; ?>';
            salary_year = '<?php echo $_GET['laYear']; ?>';
            block_id = '<?php echo $_GET['bId']; ?>';
            floor_id = '<?php echo $_GET['dId']; ?>';
            $(".ajax-loader").show();
            var res = syncFUnction(salary_slip_id, salary_month, salary_year, block_id, floor_id, status_action, share_with_user);
           
            if(res!=""){
              $('#processKey_' + salary_slip_id).html("<span class='badge badge-success'>"+res+"<span>");
               $("#Chekbox_" + salary_slip_id).attr("disabled", true);
            }
          }).get();
          
        } else {
          swal("Please select status!");
        }
      }
    })
  function syncFUnction(salary_slip_id, salary_month, salary_year, block_id, floor_id, status_action, share_with_user) {
    msg= "";
    var m = 1;
    if (m == 1) {
      m = 0;
      $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
         async: false,
        data: {
          action: "bulkSalaryChecked",
          salary_slip_id: salary_slip_id,
          salary_month: salary_month,
          salary_year: salary_year,
          status_action: status_action,
          share_with_user: share_with_user,
          block_id: block_id,
          bms_admin_id: '<?php echo $_COOKIE['bms_admin_id']; ?>',
          society_id: '<?php echo $society_id; ?>',
          floor_id: floor_id,
        },
        success: function(response) {
          $(".ajax-loader").hide();
          m = 1;
          
          msg = response.message;
          if (response.status == '200') {
           // $('#processKey_' + user_id).text('Generated');
          }
          if (response.status == '201') {
          //  $('#processKey_' + user_id).text(response.message);
          //  $('#processKey_' + user_id).removeClass('badge-success');
          //  $('#processKey_' + user_id).addClass('badge-warning');
          }

        }
        
      });
      return msg; 
    }
  }

  function yearChange(yearValue){
        // alert("ldfj");
         // yearValue = $(this).val();
          crntYear = '<?php echo date('Y');?>';
          crntM = '<?php echo date('m');?>';
          monthHtml = "";
         var  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
         var d = new Date();
         var monthName=months[d.getMonth()];
         console.log(monthName)
         if(crntYear==yearValue)
         {

            for (let index = 0; index < ((months.length)); index++) {
               const element = months[index];
               console.log(index);
               console.log(crntM);
            //   crntM = 2;
               if(index<crntM)
               {
                  monthHtml += `<option value="`+(index+1)+`" >`+element+`</option>`;
               }
            }
            $('#month').html(monthHtml);
         }
         else
         {
            for (let index = 0; index < ((months.length)); index++) {
               const element = months[index];
               monthHtml += `<option value="`+(index+1)+`" >`+element+`</option>`;
            }
            $('#month').html(monthHtml);
         }
      };
</script>
<style>
  .sharWUser {
    display: none;
  }
</style>