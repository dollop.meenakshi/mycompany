<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Add /Update Service Provider</h4>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="serviceProviderFrm" action="controller/serviceProviderController.php" method="post" enctype="multipart/form-data">
             <?php 
             if(isset($_REQUEST['editSp'])) {
                $service_provider_users_id = (int)$_REQUEST['service_provider_users_id'];
               extract(array_map("test_input" , $_REQUEST));
               $q=$d->select("local_service_provider_users","service_provider_users_id='$service_provider_users_id'");
               $data=mysqli_fetch_array($q);
               $service_provider_phone = $data['service_provider_phone'];
               $id = $service_provider_users_id;
             }else{
              $id = 0;
             }
             ?>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Company Name  <span class="required">*</span></label>
                <div class="col-sm-4">
                  <?php  if(isset($_REQUEST['editSp'])) { ?>
                    <?php //IS_618 id="input-10" to id="service_provider_name" ?>
                    <input type="text" value="<?php echo $data['service_provider_name']; ?>"  required="" class="form-control" maxlength="80" id="service_provider_name" name="service_provider_name_edit">
                    <input type="hidden" value="<?php echo $data['service_provider_users_id']; ?>" required="" class="form-control" id="input-10" name="service_provider_users_id">
                  <?php } else { ?>
                    <input type="text" maxlength="80" value="" required="" class="form-control" id="service_provider_name" name="service_provider_name">
                  <?php } ?>
                </div>
                 <label for="input-10" class="col-sm-2 col-form-label">Contact Person Name  <span class="required">*</span></label>
                <div class="col-sm-4">
                    <input type="text" maxlength="50"  required="" class="form-control" id="contact_person_name" name="contact_person_name" value="<?php echo $data['contact_person_name']; ?>" >
                </div>
              </div>
              <div class="form-group row">
                <label for="secretary_mobile" class="col-sm-2 col-form-label"> Mobile <span class="required">*</span></label>
                <div class="col-sm-4">
                  <?php //IS_618 onlyNumber id="service_provider_phone" ?> 
                  <input type="text" id="service_provider_phone" maxlength="12" value="<?php if(isset($_REQUEST['editSp'])) { echo $data['service_provider_phone']; } ?>" required="" class="form-control onlyNumber" inputmode="numeric" minlength="10" maxlength="10" name="service_provider_phone" id="service_provider_phone" onblur="checkspMobile(this.value,<?php echo $id ?>)">
                  <div id="servicePhoneError" class="text-danger font-weight-bold"></div>
                </div>
                <label for="input-13" class="col-sm-2 col-form-label"> Email </label>
                <div class="col-sm-4">

                  <input type="email" maxlength="80" id="service_provider_email"  value="<?php if(isset($_REQUEST['editSp'])) { echo $data['service_provider_email']; } ?>" class="form-control" id="input-13" name="service_provider_email" onblur="checkspEmail(this.value,<?php echo $id ?>)">
                  <!-- <div id="serviceEmailError" class="text-danger font-weight-bold"></div> -->
                </div>
              </div>
              <?php  if(!isset($_REQUEST['editSp'])) { ?>
              <div class="form-group row" >
                <label for="input-12" class="col-sm-2 col-form-label">Category <span class="required">*</span></label>
                <div class="col-sm-4">
                 <select onchange="getSubCategorySp();" required="" id="local_service_master_id" class="form-control single-select" name="local_service_master_id">
                   <option value="">-- Select --</option>
                   <?php $q4=$d->select("local_service_provider_master","");
                   while($rowSc=mysqli_fetch_array($q4)){ ?> 
                    <option <?php if( isset($data['local_service_master_id']) && $data['local_service_master_id']==$rowSc['local_service_provider_id']) {echo "selected";} ?> value="<?php echo $rowSc['local_service_provider_id'] ?>~<?php echo $rowSc['service_provider_category_name'] ?>"><?php echo $rowSc['service_provider_category_name'] ?></option>
                  <?php } ?>
                </select>
                </div>
                <label for="input-12" class="col-sm-2 col-form-label">Sub Category </label>
                <div class="col-sm-4">
                  <?php //IS_618   id="local_service_provider_sub_id" ?> 
                  <select id="local_service_provider_sub_id" class="form-control single-select" name="local_service_provider_sub_id" >
                   <option value="0">-- Select --</option>
                   <?php if(isset($_REQUEST['editSp'])) { ?>
                    <?php
                    $q3=$d->select("local_service_provider_sub_master","local_service_provider_id='$data[local_service_master_id]'","");
                    if (mysqli_num_rows($q3)>0) {
                      while ($blockRow=mysqli_fetch_array($q3)) {
                       ?>
                       <option <?php if( isset($data['local_service_provider_sub_id']) && $data['local_service_provider_sub_id']==$blockRow['local_service_provider_sub_id']) {echo "selected";} ?> value="<?php echo $blockRow['local_service_provider_sub_id'];?>"><?php echo $blockRow['service_provider_sub_category_name'];?></option>

                     <?php } } else  {?>
                       <option selected value="0">No Sub Category Availble</option>

                     <?php } ?>
                   <?php } ?>
                 </select>
               </div>
              </div>
            <?php }?>
             
              <div class="form-group row">
               
                <label for="input-101" class="col-sm-2 col-form-label"> Website </label>
                <div class="col-sm-4">
                  <input type="url" class="form-control" id="input-12" name="sp_webiste" id="sp_webiste" value="<?php echo $data['sp_webiste']; ?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-101" class="col-sm-2 col-form-label"> Address <span class="required">*</span></label>
                <div class="col-sm-4">
                  <?php //IS_618 id="input-10" to id="service_provider_address" ?>
                  <textarea maxlength="250" required="" class="form-control" id="service_provider_address" name="service_provider_address"><?php if(isset($_REQUEST['editSp'])) { echo $data['service_provider_address']; } ?></textarea>
                  <?php  if(isset($_REQUEST['editSp'])) { ?>
                    <input  type="hidden" class="form-control" id="lat" name="service_provider_latitude"  placeholder="Lattitude"  value="<?php echo $data['service_provider_latitude']; ?>">
                    <input  type="hidden" class="form-control" id="lng"  name="service_provider_logitude"  placeholder="sociaty_longitude" value="<?php echo $data['service_provider_logitude']; ?>">
                  <?php } else { ?>
                    <input  type="hidden" class="form-control" id="lat" name="service_provider_latitude"  placeholder="Lattitude"  value="23.0242625">
                    <input  type="hidden" class="form-control" id="lng"  name="service_provider_logitude"  placeholder="sociaty_longitude" value="72.5720625">
                  <?php } ?>
                </div>
                <label for="input-101" class="col-sm-2 col-form-label"> Work Description </label>
                <div class="col-sm-4">
                  <textarea maxlength="250"  class="form-control" id="work_description" name="work_description"><?php if(isset($_REQUEST['editSp'])) { echo $data['work_description']; } ?></textarea>
                </div>
              </div>
              <div class="form-group row">

                <label for="service_provider_zipcode" class="col-sm-2 col-form-label">Zipcode </label>
                <div class="col-sm-4">
                 <input type="text" class="form-control number" value="<?php if(isset($_REQUEST['editSp']) && $data['service_provider_zipcode']!=0) { echo $data['service_provider_zipcode']; }?>" name="service_provider_zipcode">
               </div>
               <label for="service_provider_user_image" class="col-sm-2 col-form-label">Logo <span class="required">*</span></label>

               <div class="col-sm-4">

                <input type="hidden"  class="form-control-file border" name="service_provider_user_image_old" value="<?php if(isset($_REQUEST['editSp'])) { echo $data['service_provider_user_image']; } ?>"  id="service_provider_user_image_old">
                <?php  if(isset($_REQUEST['editSp'])) { ?>
                 <input type="file"  class="form-control-file border onlyPhoto" accept="image/*" name="service_provider_user_image" id="service_provider_user_image" >

               <?php } else { ?>
                 <input type="file" required=""  class="form-control-file border" accept="image/*" name="service_provider_user_image" id="service_provider_user_image">
               <?php } ?>
               </div>
              </div>
              <div class="form-group row">
                <label for="service_provider_start_time" class="col-sm-2 col-form-label">Opening Time <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text" class="form-control " value="<?php if(isset($_REQUEST['editSp'])){ echo $data['open_time'];} ?>" name="start_time" id="start_time">
                </div>
                <label for="service_provider_end_time" class="col-sm-2 col-form-label">Closing Time <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text" class="form-control " value="<?php if(isset($_REQUEST['editSp'])){ echo $data['close_time'];} ?>" name="end_time" id="end_time">
                </div>
              </div>  
                            
              <div class="form-group row">
                <label for="id_proof" class="col-sm-2 col-form-label">ID Proof </label>
                <div class="col-sm-4">
                 <?php  if(isset($_REQUEST['editSp'])) { ?>
                   <input type="file" accept="image/*,.pdf" class="form-control-file border" name="id_proof">
                   <input type="hidden"  class="form-control-file border" name="id_proof_old" value="<?php echo $data['id_proof']; ?>">
                 <?php } else { ?>
                  <input maxlength="100" type="file" class="form-control-file border" accept="image/*,.pdf" value="<?php echo $data['id_proof']; ?>" id="id_proof" name="id_proof">
                  <?php } ?>
                </div>
                <label for="location_proof" class="col-sm-2 col-form-label">Location Proof/Visiting Card </label>
                <div class="col-sm-4">
                 <?php  if(isset($_REQUEST['editSp'])) { ?>
                   <input type="file" accept="image/*,.pdf" class="form-control-file border" id="location_proof" name="location_proof">
                   <input type="hidden"  class="form-control-file border" id="location_proof" name="location_proof_old" value="<?php echo $data['location_proof']; ?>">
                 <?php } else { ?>
                   <input type="file" accept="image/*,.pdf"  class="form-control-file border" id="input-12" name="location_proof" id="location_proof">
                 <?php } ?>
                </div>
              </div>

              <div class="form-group row">
                <label for="id_proof" class="col-sm-2 col-form-label">GST Number </label>
                <div class="col-sm-4">
                  <input type="text" class="form-control text-uppercase" id="input-12" name="gst_no" id="gst_no" value="<?php echo $data['gst_no']; ?>">
                </div>
                <label for="id_proof" class="col-sm-2 col-form-label">Company Profile/Brochure </label>
                <div class="col-sm-4">
                  <?php  if(isset($_REQUEST['editSp'])) { ?>
                   <input type="file" accept="image/*,.pdf" class="form-control-file border" id="brochure_profile" name="brochure_profile">
                   <input type="hidden"  class="form-control-file border" id="brochure_profile" name="brochure_profile_old" value="<?php echo $data['brochure_profile']; ?>">
                 <?php } else { ?>
                   <input type="file" accept="image/*,.pdf"  class="form-control-file border" id="input-12" name="brochure_profile" id="brochure_profile">
                 <?php } ?>
                </div>
              </div>

              <div class="form-group row">
                <input id="searchInput5" class="form-control" type="text" placeholder="Enter a Google location" >
                <div class="map" id="map" style="width: 100%; height: 400px;"></div>
              </div>
              <div class="form-footer text-center">
                <button type="submit" id="socAddBtn" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> RESET</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <?php  if(isset($_REQUEST['editSp'])) { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <h5>Manage Category</h5>
            <div class="table-responsive">
              <a href="javascript:void(0)" data-toggle="modal"  data-target="#addMoreParking" class="btn btn-sm btn-primary pull-right">Add New </a>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Category</th>
                  <th>Sub Category</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $qc=$d->select("local_service_provider_users_category,local_service_provider_master","local_service_provider_users_category.category_id=local_service_provider_master.local_service_provider_id AND local_service_provider_users_category.service_provider_users_id='$service_provider_users_id'");
                $i111=1;
                $totalCategory = mysqli_num_rows($qc);
                while ($catData=mysqli_fetch_array($qc)) {
                  ?>
                <tr>
                  <td><?php echo $i2= $i111++;?></td>
                  <td><?php echo $catData['service_provider_category_name']; ?></td>
                  <td><?php if ($catData['sub_category_id']!=0) {
                    $sq=$d->select("local_service_provider_sub_master","local_service_provider_sub_id='$catData[sub_category_id]'");
                    $subData=mysqli_fetch_array($sq);
                    echo $subData['service_provider_sub_category_name'];
                  } ?></td>
                  <td>
                  <?php
                   
                   if($totalCategory>1 ) {

                    ?>
                    <form action="controller/serviceProviderController.php" method="post" accept-charset="utf-8">
                      <input type="hidden" name="users_category_id" value="<?php echo $catData['users_category_id'];?>">
                      <input type="hidden" name="service_provider_phone" value="<?php echo $service_provider_phone;?>">
                      <input type="hidden" name="service_provider_users_id" value="<?php echo $service_provider_users_id;?>">
                      <input type="hidden" name="deletSpCategory" value="<?php echo 'deletSpCategory';?>">
                      <button type="submit" class="btn btn-danger form-btn btn-sm">Delete</button>
                    </form>
                  <?php } ?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  <?php } ?>
  </div>
</div>


 <div class="modal fade" id="addMoreParking">
  <div class="modal-dialog">
    <div class="modal-content border-myassociation">
      <div class="modal-header bg-myassociation">
        <h5 class="modal-title text-white">Add More Category </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div >
          <form class="d-inline" id="addMoreParkingForm" action="controller/serviceProviderController.php" method="post">
            <input type="hidden" name="service_provider_users_id" value="<?php echo $service_provider_users_id;?>">
            <input type="hidden" name="service_provider_phone" value="<?php echo $service_provider_phone;?>">
            <div class="form-group row">
              <label for="input-12" class="col-sm-3 col-form-label">Category <span class="required">*</span></label>
                <div class="col-sm-9">
                 <select onchange="getSubCategorySp();" required="" id="local_service_master_id" class="form-control single-select" name="category_id">
                   <option value="">-- Select --</option>
                   <?php $q4=$d->select("local_service_provider_master","");
                   while($rowSc=mysqli_fetch_array($q4)){ ?> 
                    <option  value="<?php echo $rowSc['local_service_provider_id'] ?>~<?php echo $rowSc['service_provider_category_name'] ?>"><?php echo $rowSc['service_provider_category_name'] ?></option>
                  <?php } ?>
                </select>
                </div>
            </div>
            <div class="form-group row">
               <label for="input-12" class="col-sm-3 col-form-label">Sub Category </label>
                <div class="col-sm-9">
                  <?php //IS_618   id="local_service_provider_sub_id" ?> 
                  <select id="local_service_provider_sub_id" class="form-control single-select" name="sub_category_id" >
                   <option value="0">-- Select --</option>

                  </select>
                </div>
            </div>
            
            <div class="text-center">
              <input type="hidden" value="addMoreCategory" name="addMoreCategory">
                <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
            </div>
            </form>
            
        </div>

      </div>

    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDZnHnzKIMxuaw5v1m-4Ctvt6ymhtCmVhg"></script>
<script>

  /* script */
  function initialize() {
   // var latlng = new google.maps.LatLng(23.05669,72.50606);
    var latitute =document.getElementById('lat').value;
    var longitute =document.getElementById('lng').value;
    var latlng = new google.maps.LatLng(latitute,longitute);

    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
      // icon:'img/direction/'+dirction+'.png'
    });
    var parkingRadition = 5;
    var citymap = {
      newyork: {
        center: {lat: latitute, lng: longitute},
        population: parkingRadition
      }
    };

    var input = document.getElementById('searchInput5');
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var geocoder = new google.maps.Geocoder();
        var autocomplete10 = new google.maps.places.Autocomplete(input);
        autocomplete10.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();   
        autocomplete10.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place5 = autocomplete10.getPlace();
          if (!place5.geometry) {
            window.alert("Autocomplete's returned place5 contains no geometry");
            return;
          }

            // If the place5 has a geometry, then present it on a map.
            if (place5.geometry.viewport) {
              map.fitBounds(place5.geometry.viewport);
            } else {
              map.setCenter(place5.geometry.location);
              map.setZoom(17);
            }

            marker.setPosition(place5.geometry.location);
            marker.setVisible(true);          
            
            var pincode="";
            for (var i = 0; i < place5.address_components.length; i++) {
              for (var j = 0; j < place5.address_components[i].types.length; j++) {
                if (place5.address_components[i].types[j] == "postal_code") {
                  pincode = place5.address_components[i].long_name;
                  // alert(pincode);
                }
              }
            }
            bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
            infowindow.setContent(place5.formatted_address);
            infowindow.open(map, marker);

          });
        // this function will work on marker move event into map 
        google.maps.event.addListener(marker, 'dragend', function() {
          geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) { 
               var places = results[0]       ;
               console.log(places);
               var pincode="";
               var serviceable_area_locality= places.address_components[4].long_name;
               // alert(serviceable_area_locality);
               for (var i = 0; i < places.address_components.length; i++) {
                for (var j = 0; j < places.address_components[i].types.length; j++) {
                  if (places.address_components[i].types[j] == "postal_code") {
                    pincode = places.address_components[i].long_name;
                  // alert(pincode);
                }
              }
            }
            bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
                  // infowindow.setContent(results[0].formatted_address);
                  // infowindow.open(map, marker);
                }
              }
            });
        });
  }
  function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality){
   // document.getElementById('poi_point_address').value = address;
   document.getElementById('lat').value = lat;
   document.getElementById('lng').value = lng;
   // document.getElementById('collection_center_pincode').value = pin_code;
   // document.getElementById('serviceable_area_locality').value = serviceable_area_locality;

    // document.getElementById("POISubmitBtn").removeAttribute("hidden"); 
    // initialize();
  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script src="assets/plugins/material-datepicker/js/moment.min.js"></script>
<script src="assets/plugins/material-datepicker/js/bootstrap-material-datetimepicker.min.js"></script>
<script src="assets/plugins/material-datepicker/js/ja.js"></script>
<script type="text/javascript">

  $('#start_time').bootstrapMaterialDatePicker({
    format: 'HH:mm',
    date: false,
  });

  $('#end_time').focus(function() {
    var startTime = $('#start_time').val();
    if (startTime=='') {
      swal('Please select Start Time first');
    } 
  });
  <?php if(isset($_REQUEST['editSp']) && $data['open_time']!=''){ ?>
    var startTime = $('#start_time').val();
    $('#end_time').bootstrapMaterialDatePicker({
      format: 'HH:mm',
      minDate: startTime,
      date: false,
    });
  <?php } ?>
  $('#start_time').change(function() {
    var startTime = $('#start_time').val();
    $('#end_time').bootstrapMaterialDatePicker({
      format: 'HH:mm',
      date: false,
      minDate: startTime,
    });
    $('#end_time').on('change', function() {  // when the value changes
      $(this).valid(); // trigger validation on this element
    });
  });

  $('#start_time').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
  });

  $('#start-date').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD',
    time: false,
  });
  <?php if(isset($_REQUEST['editSp']) && $data['service_provider_start_date']!=''){ ?>
    var startDate = $('#start-date').val();
    $('#end-date').bootstrapMaterialDatePicker({
      format: 'YYYY-MM-DD',
      minDate: startDate,
      time: false,
    });
  <?php } ?>

  $('#end-date').focus(function() {
    var startDate = $('#start-date').val();
    if (startDate=='') {
     swal("Please select Start Date first", {
      icon: "warning",
    });
   } 
 });
  $('#start-date').change(function() {
    var startDate = $('#start-date').val();
    $('#end-date').bootstrapMaterialDatePicker({
      format: 'YYYY-MM-DD',
      minDate: startDate,
      time: false,
    });
    $('#end-date').on('change', function() {  // when the value changes
      $(this).valid(); // trigger validation on this element
    });
  });

  $('#start-date').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
  });
</script>
<script type="text/javascript">
  function checkspMobile(mobile,id) {
    $.ajax({
      url: "spDetailsVerify.php",
      cache: false,
      type: "POST",
      data: {mobile : mobile,id:id},
      success: function(response){
         if (response==1) {
          $('#servicePhoneError').html('Mobile Number is already Registered');
          swal("This mobile is already registered");
          $('#service_provider_phone').val('');
        } else{
          $('#servicePhoneError').empty();
          $( "#service_provider_email" ).blur();
        }
      }
    });
  }
</script>
<script type="text/javascript">
  function checkspEmail(email,id) {
    $.ajax({
      url: "spDetailsVerify.php",
      cache: false,
      type: "POST",
      data: {email : email,id:id},
      success: function(response){
        $('#serviceEmailError').html(response);
      }
    });
  }
</script>