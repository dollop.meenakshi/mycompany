<?php
$id = (int)$_GET['id'];
$mt = $_GET['mt'];
$currentDate = date('Y-m-d');
/* $day = '0,2,5';
$date = date('Y-m-d');
$week_day = explode(',', $day);
echo "Shubham_Heloo";
print_r($week_day);
$days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
for ($i=0; $i <= 6; $i++) { 
    date('Y-m-d', strtotime($days[$i], strtotime($date))); 
    
} */
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<h4 class="page-title">Completed Assets Maintenance</h4>
			</div>
		</div>

		<form class="branchDeptFilter" action="" method="get">
			<div class="row pt-2 pb-2">
				<div class="col-md-3 form-group" >
					<select type="text" name="id" id="id" class="form-control single-select" style="width: 100%">
						<option value="">All Category</option>
						<?php $q12 = $d->select("assets_category_master", "", "order by assets_category ASC");
						while ($row12 = mysqli_fetch_array($q12)) { ?>
						<option value="<?php echo $row12['assets_category_id']; ?>" <?php if ($id == $row12['assets_category_id']) {echo "selected";} ?>><?php echo $row12['assets_category']; ?> </option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3 form-group" >
					<select class="form-control single-select" name="mt">
						<option <?php if(isset($mt) && $mt == 'all'){echo 'selected';} ?> value="all">All Maintenance Type</option>
						<option <?php if(isset($mt) && $mt == '0'){echo 'selected';} ?> value="0">Custom Date</option>
						<option <?php if(isset($mt) && $mt == '1'){echo 'selected';} ?> value="1">Weekly</option>
						<option <?php if(isset($mt) && $mt == '2'){echo 'selected';} ?> value="2">Month Days</option>
						<option <?php if(isset($mt) && $mt == '3'){echo 'selected';} ?> value="3">Monthly</option>
						<option <?php if(isset($mt) && $mt == '4'){echo 'selected';} ?> value="4">Quarterly</option>
						<option <?php if(isset($mt) && $mt == '5'){echo 'selected';} ?> value="5">Half Yearly</option>
						<option <?php if(isset($mt) && $mt == '6'){echo 'selected';} ?> value="6">Yearly</option>
					</select>
				</div>
				<div class="col-md-3 form-group">
					<input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-bordered">
								<thead>
									<tr>
										<th>S. No</th>
										<th>Category</th>
										<th>Item Name</th>
										<th>Brand Name</th>
										<th>Maintenance Type</th>
										<th>Maintenance Amount</th>
										<th>Schedule Date</th>
										<th>Complete Date</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$counter = 1;
									if (isset($id) && $id > 0) {
										$appendQery = " AND assets_item_detail_master.assets_category_id='$id'";
									}
									if (isset($mt) && $mt != 'all') {
										$typeAppendQery = " AND assets_maintenance_master.maintenance_type='$mt'";
									}
									
									$q = $d->select("assets_category_master,assets_item_detail_master,assets_maintenance_master,assets_maintenance_complete","assets_category_master.assets_category_id=assets_maintenance_complete.assets_category_id AND assets_item_detail_master.assets_id=assets_maintenance_complete.assets_id AND assets_maintenance_complete.assets_maintenance_id=assets_maintenance_master.assets_maintenance_id AND assets_maintenance_complete.society_id='$society_id' AND assets_maintenance_complete.is_maintenance_complete=1  $appendQery $typeAppendQery");
									while ($row = mysqli_fetch_array($q)) { ?>
										<tr>
											<td><?php echo $counter++; ?></td>
											<td><?php echo $row['assets_category']; ?></td>
											<td><?php echo $row['assets_name']; ?></td>
											<td><?php echo $row['assets_brand_name']; ?></td>
											<td><?php if($row['maintenance_type'] == 0){
													echo 'Custom Date';
												}
												elseif($row['maintenance_type'] == 1){
													echo 'Weekly';
												}
												elseif($row['maintenance_type'] == 2){
													echo 'Month Days';
												}
												elseif($row['maintenance_type'] == 3){
													echo 'Monthly';
												}
												elseif($row['maintenance_type'] == 4){
													echo 'Quarterly';
												}
												elseif($row['maintenance_type'] == 5){
													echo 'Half Yearly';
												}
												elseif($row['maintenance_type'] == 6){
													echo 'Yearly';
												}
										 	?></td>
											<td><?php echo number_format($row['maintenance_amount'],2,'.',''); ?></td>
											<td><?php echo date("d M Y", strtotime($row['maintenance_schedule_date'])); ?></td>
											<td><?php echo date("d M Y", strtotime($row['maintenance_complete_at'])); ?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
