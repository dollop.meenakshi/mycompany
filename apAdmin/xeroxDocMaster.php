<?php error_reporting(0);
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-md-9">
          <h4 class="page-title">Xerox Orders</h4>
        </div>
     </div>
     <div class="row pt-2 pb-2">
        <div class="col-sm-12 col-12">
          <ol class="breadcrumb">
            <li>
                <a href ="xeroxDocMaster"><span class="badge badge-pill badge-primary m-1"> All(<?php echo $d->count_data_direct("xerox_doc_id","xerox_doc_master","society_id='$society_id'"); ?>)</span></a>
                <a href ="xeroxDocMaster?status=0"><span class="badge badge-pill badge-secondary m-1">Pending (<?php echo $d->count_data_direct("xerox_doc_id","xerox_doc_master","xerox_doc_master.xerox_doc_status = 0 AND xerox_doc_master.society_id='$society_id'"); ?>)</span></a>
                <a href ="xeroxDocMaster?status=1"><span class="badge badge-pill badge-success m-1">Approved (<?php echo $d->count_data_direct("xerox_doc_id","xerox_doc_master","xerox_doc_master.xerox_doc_status = 1 AND xerox_doc_master.society_id='$society_id'"); ?>)</span></a>
                <a href ="xeroxDocMaster?status=2" ><span class="badge badge-pill badge-info m-1">Inprogress  (<?php echo $d->count_data_direct("xerox_doc_id","xerox_doc_master","xerox_doc_master.xerox_doc_status = 2 AND xerox_doc_master.society_id='$society_id'"); ?>)</span></a>
                <a href ="xeroxDocMaster?status=3"><span class="badge badge-pill badge-warning m-1">Ready (<?php echo $d->count_data_direct("xerox_doc_id","xerox_doc_master","xerox_doc_master.xerox_doc_status = 3 AND xerox_doc_master.society_id='$society_id'"); ?>)</span></a>
                <a href ="xeroxDocMaster?status=4"><span class="badge badge-pill badge-primary m-1">Delivered (<?php echo $d->count_data_direct("xerox_doc_id","xerox_doc_master","xerox_doc_master.xerox_doc_status = 4 AND xerox_doc_master.society_id='$society_id'"); ?>)</span></a>
                <a href ="xeroxDocMaster?status=5"><span class="badge badge-pill badge-danger m-1">Cancel (<?php echo $d->count_data_direct("xerox_doc_id","xerox_doc_master","xerox_doc_master.xerox_doc_status = 5 AND xerox_doc_master.society_id='$society_id'"); ?>)</span></a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
               
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>User Name</th>
                        <th>Department</th>
                        <th>Xerox Type</th>
                        <th>Paper Size</th>
                        <th>Date</th>
                        <th>Print</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody id="showFilterData">
                   <?php 
                    $i=1;
                                           
                      if(isset($dId) && $dId>0) {
                        $departmentFilterQuery = " AND users_master.floor_id='$dId'";
                       }
                       if(isset($_GET['status']) && $_GET['status'] !="")
                       {
                        $status = $_GET['status'];
                        $statusFIlter= " AND xerox_doc_master.xerox_doc_status='$status'";
                       }
                     
                      $q=$d->select("xerox_doc_master,users_master,floors_master,block_master,xerox_type_category,xerox_paper_size","block_master.block_id = users_master.block_id AND users_master.floor_id = floors_master.floor_id AND xerox_doc_master.user_id = users_master.user_id AND xerox_doc_master.xerox_type_id = xerox_type_category.xerox_type_id AND xerox_doc_master.xerox_paper_size_id = xerox_paper_size.xerox_paper_size_id AND users_master.delete_status=0 AND xerox_doc_master.society_id= $society_id  $statusFIlter ORDER BY xerox_doc_master.xerox_doc_id DESC LIMIT 1000 ");
                        $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                      <?php  ?>
                      <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['xerox_doc_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['xerox_doc_id']; ?>">                      
                      </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['user_full_name']; ?>(<?php echo $data['block_name']; ?>)</td>
                      <td><?php echo $data['floor_name']; ?></td>
                      <td><?php echo $data['xerox_type_name']; ?></td>
                      <td><?php echo $data['xerox_paper_size_name']; ?></td>
                      <td><?php echo date("d M Y", strtotime($data['uploade_date'])); ?></td>
                      <td>
                        <a href="../img/documents/<?php echo $data['xerox_doc_name'] ?>" download="<?php echo $pData['gallery_photo'] ?>">
                          <button class="btn btn-sm btn-primary" style="margin-right: 5px;">
                            <i class="fa fa-download" aria-hidden="true"></i>
                          </button>
                        </a>
                        <a target="_blank" href="printXeroxDoc.php?doc=<?php echo $data['xerox_doc_name'] ?>">
                          <button class="btn btn-sm btn-primary" style="margin-right: 5px;">
                            <i class="fa fa-print" aria-hidden="true"></i>
                          </button>
                        </a>
                      </td>

                      <td>
                      <div class="d-flex align-items-center">
                        <form  method="post" accept-charset="utf-8">
                          <input type="hidden" name="xerox_doc_id" value="<?php echo $data['xerox_doc_id']; ?>">
                          <input type="hidden" name="edit_xerox" value="edit_xerox">
                        </form>
 
                          <select <?php if(($data['xerox_doc_status']==5) || ($data['xerox_doc_status']==4)){ echo "disabled"; }?> id="xerox_doc_status" class="form-control" name="xerox_doc_status"  onchange = "changeXeroxStatus(this.value,<?php echo $data['xerox_doc_id']; ?>,<?php echo $data['user_id']; ?>)">
                            <?php if($data['xerox_doc_status']==0){ ?>
                            <option <?php if( $data['xerox_doc_status']==0){ echo "selected";} ?> value="0">Pending</option>
                            <option <?php if( $data['xerox_doc_status']==1){ echo "selected";} ?> value="1">Approved</option>
                            <?php } else if($data['xerox_doc_status']<=1){ ?>
                           
                              <option <?php if( $data['xerox_doc_status']==1){ echo "selected";} ?> value="1">Approved</option>
                              <option <?php if( $data['xerox_doc_status']==2){ echo "selected";} ?> value="2">Inprogress</option>
                              <option <?php if( $data['xerox_doc_status']==3){ echo "selected";} ?> value="3">Ready</option>
                              <option <?php if( $data['xerox_doc_status']==4){ echo "selected";} ?> value="4">Delivered</option>

                              <?php } else if($data['xerox_doc_status']<=2){ ?>
                                <option <?php if( $data['xerox_doc_status']==2){ echo "selected";} ?> value="2">Inprogress</option>
                                <option <?php if( $data['xerox_doc_status']==3){ echo "selected";} ?> value="3">Ready</option>
                                <option <?php if( $data['xerox_doc_status']==4){ echo "selected";} ?> value="4">Delivered</option>                            
                            <?php } else if($data['xerox_doc_status']<=3){ ?>
                              <option <?php if( $data['xerox_doc_status']==3){ echo "selected";} ?> value="3">Ready</option>
                                <option <?php if( $data['xerox_doc_status']==4){ echo "selected";} ?> value="4">Delivered</option>                           
                             <?php  } 
                             else if($data['xerox_doc_status']<=4){
                               ?>
                                <option <?php if( $data['xerox_doc_status']==4){ echo "selected";} ?> value="4">Delivered</option>                           

                               <?php 
                             }
                            if($data['xerox_doc_status']==0 ||  $data['xerox_doc_status']==5){ ?>
                            <option <?php if( $data['xerox_doc_status']==5){ echo "selected";} ?> value="5">Cancel</option>
                            <?php } ?>
                        </select>
                         <!--  <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['xerox_doc_id']; ?>','xeroxDocDeactive');" data-size="small"/>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['xerox_doc_id']; ?>','xeroxDocActive');" data-size="small"/>
                           --><button type="button" class="btn btn-sm btn-primary ml-1" onclick="xeroxDocShowDetails(<?php echo $data['xerox_doc_id']; ?>)" data-toggle="modal" data-target="#xeroxDocModel" ><i class="fa fa-eye"></i></button> 
                        </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
<!-- <script type="text/javascript">
  function vieComplain(complain_id) {
      $.ajax({
          url: "getComplaineDetails.php",
          cache: false,
          type: "POST",
          data: {complain_id : complain_id},
          success: function(response){
              $('#comResp').html(response);
            
              
          }
       });
  } 
</script> -->




<div class="modal fade" id="xeroxDocModel">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Xerox Document Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="xeroxDocModelDiv" style="align-content: center;">

      </div>
      
    </div>
  </div>
</div>
<!-- <script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
.hideupdate{
  display:none;
}

</style> -->