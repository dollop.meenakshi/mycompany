<?php
if(isset($_GET['CId']) && !empty($_GET['CId']))
{
    $country_id = $_GET['CId'];
}
elseif((isset($_COOKIE['CId']) && !empty($_COOKIE['CId'])))
{
    $country_id = $_COOKIE['CId'];
}
if(isset($country_id) && !empty($country_id) && !ctype_digit($country_id))
{
    $_SESSION['msg1'] = "Invalid Country Id!";
    ?>
    <script>
        window.location = "manageDistributor";
    </script>
<?php
exit;
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Manage Distributor</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form action="addDistributor" method="post" id="addDistributorBtnForm">
                        <input type="hidden" name="addDistributorBtn" value="addDistributorBtn">
                        <button type="button" onclick="submitAddBtnForm();" class="btn btn-primary btn-sm waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add</button>
                    </form>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteDistributor');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <form id="filterFormDistributor">
            <div class="row pt-2 pb-2">
                <div class="col-lg-3 col-6">
                    <label  class="form-control-label">Country </label>
                    <select name="CId" id="CId" class="form-control single-select" required onchange="getStateCity(this.value);">
                        <option value="">--Select--</option>
                        <?php
                        $cq = $d->select("countries");
                        while ($cd = $cq->fetch_assoc())
                        {
                        ?>
                        <option <?php if($country_id == $cd['country_id']) { echo 'selected';} ?> value="<?php echo $cd['country_id']; ?>"><?php echo $cd['country_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="submit">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            if(isset($country_id) && !empty($country_id))
                            {
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>Action</th>
                                        <th>Distributor Name</th>
                                        <th>Contact Person </th>
                                        <th>Contact Number</th>
                                        <th>Order Email</th>
                                        <th>Photo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $append = "";
                                    if(!empty($country_id))
                                    {
                                        $append .= " AND dm.country_id = '$country_id'";
                                    }
                                    $q = $d->selectRow("dm.*,rom.retailer_order_id,amn.area_name","distributor_master AS dm LEFT JOIN retailer_order_master AS rom ON rom.distributor_id = dm.distributor_id LEFT JOIN area_master_new AS amn ON amn.area_id = dm.distributor_area_id","dm.society_id = '$society_id'".$append,"GROUP BY dm.distributor_id ORDER BY distributor_id DESC");
                                    $iNo = 1;
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td class='text-center'>
                                            <?php
                                            if($retailer_order_id == "" && empty($retailer_order_id))
                                            {
                                            ?>
                                            <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['distributor_id']; ?>">
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <input type="hidden"/>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $iNo++; ?></td>
                                        <td>
                                            <div style="display: inline-block;">
                                                <form action="addDistributor" method="post">
                                                    <input type="hidden" name="distributor_id" value="<?php echo $row['distributor_id']; ?>">
                                                    <input type="hidden" name="editDistributor" value="editDistributor">
                                                    <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
                                                </form>
                                            </div>
                                            <div style="display: inline-block;">
                                                <form action="distributorDetails">
                                                    <input type="hidden" name="DId" value="<?php echo $row['distributor_id']; ?>">
                                                    <input type="hidden" name="CId" value="<?php echo $country_id; ?>">
                                                    <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            if($retailer_order_id == "" && empty($retailer_order_id))
                                            {
                                            ?>
                                            <div style="display: inline-block;">
                                                <form action="controller/distributorController.php" method="POST">
                                                    <input type="hidden" name="distributor_id" value="<?php echo $row['distributor_id']; ?>">
                                                    <input type="hidden" name="country_id" value="<?php echo $country_id; ?>">
                                                    <input type="hidden" name="state_id" value="<?php echo $state_id; ?>">
                                                    <input type="hidden" name="city_id" value="<?php echo $city_id; ?>">
                                                    <input type="hidden" name="area_id" value="<?php echo $area_id; ?>">
                                                    <input type="hidden" name="distributor_photo" value="<?php echo $distributor_photo; ?>">
                                                    <input type="hidden" name="deleteDistributor" value="deleteDistributor">
                                                    <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $distributor_name; ?></td>
                                        <td><?php echo $distributor_contact_person; ?></td>
                                        <td><?php echo $distributor_contact_person_country_code . " " . $distributor_contact_person_number; ?></td>
                                        <td><?php echo $distributor_email; ?></td>
                                        <td>
                                        <?php
                                        if($distributor_photo != "" && !empty($distributor_photo) && file_exists("../img/users/recident_profile/".$distributor_photo))
                                        {
                                        ?>
                                        <a href="../img/users/recident_profile/<?php echo $distributor_photo; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $distributor_name; ?>">
                                            <img src="../img/users/recident_profile/<?php echo $distributor_photo; ?>" width="100" height="100" alt="Distributor Image"/>
                                        </a>
                                        <?php
                                        }
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            else
                            {
                            ?>
                            <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select County</span>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script>
function getStateCity(country_id)
{
    $.ajax({
        url: 'controller/distributorController.php',
        data: {getStateCityTag:"getStateCityTag",country_id:country_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#SCId').empty();
            response = JSON.parse(response);
            $('#SCId').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#SCId').append("<option value='"+value.city_id+"-"+value.state_id+"'>"+value.city_name+" - "+value.state_name+"</option>");
            });
        }
    });
}

function getAreaList(city_state_id)
{
    const country_id = $('#CId').val();
    const city_id = city_state_id.substring(0, city_state_id.indexOf('-'));
    const state_id = city_state_id.substring(city_state_id.indexOf('-') + 1);
    $.ajax({
        url: 'controller/distributorController.php',
        data: {getAreaTag:"getAreaTag",country_id:country_id,state_id:state_id,city_id:city_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#AId').empty();
            response = JSON.parse(response);
            $('#AId').append("<option value=''>--Select--</option>");
            $('#AId').append("<option value='0'>All</option>");
            $.each(response, function(key, value)
            {
                $('#AId').append("<option value='"+value.area_id+"'>"+value.area_name+"</option>");
            });
        }
    });
}

function submitAddBtnForm()
{
    var country_id = $('#CId').val();
    if(country_id != "" && country_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'country_id',
            value: country_id
        }).appendTo('#addDistributorBtnForm');
    }
    $("#addDistributorBtnForm").submit()
}
</script>