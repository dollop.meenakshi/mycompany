 <div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-6">
        <h4 class="page-title"> <?php echo $xml->string->blocks; ?></h4>
      </div>
      <div class="col-sm-3 col-6">
       <div class="btn-group float-sm-right">
        <?php $totalBlock= $d->count_data_direct("block_id","block_master","society_id='$society_id'");
        if ($totalBlock==0) {
         
         ?>
         <a href="block" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> <?php echo $xml->string->multiple; ?> <?php echo $xml->string->block; ?></a>
       <?php } else { ?>
        <a href="block" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> <?php echo $xml->string->single; ?> <?php echo $xml->string->block; ?></a>

      <?php } ?>
    </div>
  </div>
</div>
<!-- End Breadcrumb-->
<div class="row">
 <?php 
 $i=1;
 $q = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_sort ASC");
 if(mysqli_num_rows($q)>0) {

  while ($data=mysqli_fetch_array($q)) {
    extract($data);
    $countUsers=$d->count_data_direct("block_id","users_master","society_id!=0 AND society_id='$society_id' AND block_id='$block_id'");
    ?>  
    
    <div class="col-lg-3  col-6">
      <div class="card bg-primary">
       <div class="card-body text-center text-white">
        <a href="floors" class="text-white">
         <?php echo $xml->string->blocks; ?>
         <h5 class="mt-2 text-white"><?php echo $block_name; ?> </h5>
       </a>
        <i title="Edit Block Name" class="fa fa-pencil pull-right"  data-toggle="modal" data-target="#editFloor" onclick="editBlock('<?php echo $data['block_id']; ?>','<?php echo $data['block_name']; ?>','<?php echo $data['block_sort'] ?>');"></i>
          <?php if ($countUsers==0){ ?><i title="Delete Block" onclick="DeleteBlock('<?php echo $block_id ?>');" class="fa fa-trash-o pull-right mr-2"></i>
        <?php } else { ?>
          <i title="Delete Block" class="fa fa-trash-o pull-right" onclick="showError('Delete This <?php echo $xml->string->block; ?> All users then delete this <?php echo $xml->string->block; ?>..!')"></i>
        <?php } ?>



       
       <div class="row">
        <div class="col-12 border-right border-light-2">  



          

    </div>
  </div>
  <?php  ?>

</div>

</div>
</div>




<?php // ?>
<?php } } else {
  echo "<img src='img/no_data_found.png'>";
} 


?>
   <?php //IS_1045 
   $q = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_sort ASC");
   if(mysqli_num_rows($q)>0) {

     
     ?>
     <div class="col-lg-12">
      <div class="card">
        <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
        <div class="card-body">
          <div class="table-responsive">
            <?php //IS_675  example to examplePenaltiesTbl?>
            <table id="example" class="table table-bordered">
              <thead>
                <tr><th>#</th>
                 <th><?php echo $xml->string->blocks; ?> <?php echo $xml->string->name; ?></th>
                 <th><?php echo $xml->string->order_no; ?></th>
                 
               </tr>
             </thead>
             <tbody>
              <?php 
              $cnt = 0 ;
              while ($data=mysqli_fetch_array($q)) {
                extract($data);
                ?>
                <tr id="<?php echo $block_id.'~'.$society_id; ?>" >
                  <td> <?php $cnt=$cnt+1; echo $cnt; ?> </td>
                  <td><?php echo $xml->string->block; ?> - <?php echo $block_name; ?> </td>
                  <td> 
                    <span style="display: none;"><?php echo $block_sort; ?></span>
                     
                    <input class="form-control block-id-cls onlyNumber" inputmode="numeric" type="text" name="block_sort" id="block_sort_val" value="<?php echo $block_sort; ?>" min="0" max="5555" maxlength="4" size="4" />
                    <span id="block_id_<?php echo $block_id; ?>"></span>
                  </td>
                </tr>
              <?php }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <?php
} // IS_1045?> 




</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->


<div class="modal fade" id="editFloor">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->edit; ?> <?php echo $xml->string->blocks; ?> <?php echo $xml->string->name; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="blockAdd" action="controller/blockController.php" method="post">
          <input type="hidden" id="society_id" name="society_id" value="<?php echo $society_id;?>">
          <input type="hidden" id="floorId" name="block_id">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->blocks; ?> <?php echo $xml->string->name; ?> <span class="required">*</span></label>
            <div class="col-sm-8" id="PaybleAmount">
              <input type="text" id="oldFloorname" class="form-control text-capitalize txtNumeric" name="block_name" required="" maxlength="30" autocomplete="off">
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->order_no; ?> <span class="required">*</span></label>
            <div class="col-sm-8" id="PaybleAmount">
              <input type="text" class="form-control number" id="block_sort" name="block_sort" required="">
            </div>
          </div>
          
          <div class="form-footer text-center">
            <input type="hidden" name="updateBlocks" value="updateBlocks">
            <button type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->update; ?></button>
          </div>

        </form>
      </div>
      
    </div>
  </div>
</div><!--End Modal -->
 