<?php
if(isset($_GET['CId']) && !empty($_GET) && isset($_GET['SCId']))
{
    $country_id = $_GET['CId'];
    $area_id = $_GET['AId'];
    $ci_st_id = explode("-",$_GET['SCId']);
    $city_id = $ci_st_id[0];
    $state_id = $ci_st_id[1];
}
elseif((isset($_COOKIE['CId']) && !empty($_COOKIE['CId'])) || (isset($_COOKIE['SId']) && !empty($_COOKIE['SId'])) || (isset($_COOKIE['CIId']) && !empty($_COOKIE['CIId'])) || (isset($_COOKIE['AId']) && !empty($_COOKIE['AId'])))
{
    if(ctype_digit($_COOKIE['CId']))
    {
        $country_id = $_COOKIE['CId'];
    }
    if(ctype_digit($_COOKIE['SId']))
    {
        $state_id = $_COOKIE['SId'];
    }
    if(ctype_digit($_COOKIE['CIId']))
    {
        $city_id = $_COOKIE['CIId'];
    }
    if(ctype_digit($_COOKIE['AId']))
    {
        $area_id = $_COOKIE['AId'];
    }
}

if(isset($city_id) && !empty($city_id) && $city_id != "" && !ctype_digit($city_id))
{
    $_SESSION['msg1'] = "Invalid City Id!";
    unset($_COOKIE['CIId']);
    unset($_COOKIE['SId']);
    setcookie('CIId', '', time()-1000);
    setcookie('CIId', '', time()-1000, '/');
    setcookie('CIId', '', time()-1000, "/$cookieUrl");
    setcookie('SId', '', time()-1000);
    setcookie('SId', '', time()-1000, '/');
    setcookie('SId', '', time()-1000, "/$cookieUrl");
?>
    <script>
        window.location = "manageRetailer";
    </script>
<?php
exit;
}

if(isset($state_id) && !empty($state_id) && $state_id != "" && !ctype_digit($state_id))
{
    $_SESSION['msg1'] = "Invalid State Id!";
    unset($_COOKIE['CIId']);
    unset($_COOKIE['SId']);
    setcookie('SId', '', time()-1000);
    setcookie('SId', '', time()-1000, '/');
    setcookie('SId', '', time()-1000, "/$cookieUrl");
    setcookie('CIId', '', time()-1000);
    setcookie('CIId', '', time()-1000, '/');
    setcookie('CIId', '', time()-1000, "/$cookieUrl");
?>
    <script>
        window.location = "manageRetailer";
    </script>
<?php
exit;
}
if(isset($country_id) && !empty($country_id) && $country_id != "" && !ctype_digit($country_id))
{
    $_SESSION['msg1'] = "Invalid Country Id!";
    unset($_COOKIE['CId']);
    setcookie('CId', '', time()-1000);
    setcookie('CId', '', time()-1000, '/');
    setcookie('CId', '', time()-1000, "/$cookieUrl");
    ?>
    <script>
        window.location = "manageRetailer";
    </script>
<?php
exit;
}

if(isset($area_id) && !empty($area_id) && $area_id != "" && !ctype_digit($area_id))
{
    $_SESSION['msg1'] = "Invalid Area Id!";
    unset($_COOKIE['AId']);
    setcookie('AId', '', time()-1000);
    setcookie('AId', '', time()-1000, '/');
    setcookie('AId', '', time()-1000, "/$cookieUrl");
    ?>
    <script>
        window.location = "manageRetailer";
    </script>
<?php
exit;
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Manage Retailer</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form action="addRetailer" method="post" id="addRetailerBtnForm">
                        <input type="hidden" name="addRetailerBtn" value="addRetailerBtn">
                        <button type="button" onclick="submitAddBtnForm();" class="btn btn-primary btn-sm waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add</button>
                    </form>
                    <a href="javascript:void(0)" onclick="DeleteAllNew('deleteRetailer');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <form id="filterFormRetailer">
            <div class="row pt-2 pb-2">
                <div class="col-lg-3 col-6">
                    <label  class="form-control-label">Country </label>
                    <select name="CId" id="CId" class="form-control single-select" required onchange="getStateCity(this.value);">
                        <option value="">--Select--</option>
                        <?php
                        $cq = $d->select("countries");
                        while ($cd = $cq->fetch_assoc())
                        {
                        ?>
                        <option <?php if($country_id == $cd['country_id']) { echo 'selected';} ?> value="<?php echo $cd['country_id']; ?>"><?php echo $cd['country_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">State-City </label>
                    <select class="form-control single-select" id="SCId" name="SCId" required onchange="getAreaList(this.value);">
                        <option value="">-- Select --</option>
                        <?php
                        if(isset($country_id) && $country_id != 0 && $country_id != "")
                        {
                            $qt = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id' AND c.flag = 1");
                            while ($qd = $qt->fetch_assoc())
                            {
                        ?>
                        <option <?php if($city_id == $qd['city_id']) { echo 'selected'; } ?> value="<?php echo $qd['city_id']."-".$qd['state_id'];?>"><?php echo $qd['city_name'];?>-<?php echo $qd['state_name'];?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Area </label>
                    <select class="form-control single-select" id="AId" name="AId">
                        <option value="">-- Select --</option>
                        <option value="0" <?php if(isset($area_id) && $area_id == 0){ echo "selected"; } ?>>All</option>
                        <?php
                        if(isset($country_id) && $country_id != 0 && $country_id != "" && isset($state_id) && $state_id != 0 && $state_id != "" && isset($city_id) && $city_id != 0 && $city_id != "")
                        {
                            $qa = $d->selectRow("area_id,area_name","area_master_new","country_id = '$country_id' AND state_id = '$state_id' AND city_id = '$city_id'");
                            while ($qda = $qa->fetch_assoc())
                            {
                        ?>
                        <option <?php if($area_id == $qda['area_id']) { echo 'selected'; } ?> value="<?php echo $qda['area_id']; ?>"><?php echo $qda['area_name']; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="button" onclick="loaddata();">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example10" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>Action</th>
                                        <th>Status</th>
                                        <th>Retailer Name</th>
                                        <th>Area</th>
                                        <th>City</th>
                                        <th>Retailer Code</th>
                                        <th>Contact Person Name</th>
                                        <th>Contact Person Number</th>
                                        <th>Photo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script>
function getStateCity(country_id)
{
    $.ajax({
        url: 'controller/retailerController.php',
        data: {getStateCityTag:"getStateCityTag",country_id:country_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#SCId').empty();
            response = JSON.parse(response);
            $('#SCId').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#SCId').append("<option value='"+value.city_id+"-"+value.state_id+"'>"+value.city_name+" - "+value.state_name+"</option>");
            });
        }
    });
}

function getAreaList(city_state_id)
{
    const country_id = $('#CId').val();
    const city_id = city_state_id.substring(0, city_state_id.indexOf('-'));
    const state_id = city_state_id.substring(city_state_id.indexOf('-') + 1);
    $.ajax({
        url: 'controller/retailerController.php',
        data: {getAreaTag:"getAreaTag",country_id:country_id,state_id:state_id,city_id:city_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#AId').empty();
            response = JSON.parse(response);
            $('#AId').append("<option value=''>--Select--</option>");
            $('#AId').append("<option value='0'>All</option>");
            $.each(response, function(key, value)
            {
                $('#AId').append("<option value='"+value.area_id+"'>"+value.area_name+"</option>");
            });
        }
    });
}

function submitAddBtnForm()
{
    var country_id = $('#CId').val();
    var state_city_id = $('#SCId').val();
    var city_id = state_city_id.substr(0, state_city_id.indexOf('-'));
    var state_id = state_city_id.split("-")[1];
    var area_id = $('#AId').val();
    if(country_id != "" && country_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'country_id',
            value: country_id
        }).appendTo('#addRetailerBtnForm');
    }
    if(city_id != "" && city_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'city_id',
            value: city_id
        }).appendTo('#addRetailerBtnForm');
    }if(state_id != "" && state_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'state_id',
            value: state_id
        }).appendTo('#addRetailerBtnForm');
    }
    if(area_id != "" && area_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'area_id',
            value: area_id
        }).appendTo('#addRetailerBtnForm');
    }
    $("#addRetailerBtnForm").submit()
}

$(document).ready(function()
{
    var country_id = '<?php echo $country_id ?>';
    var state_id = '<?php echo $state_id ?>';
    var city_id = '<?php echo $city_id ?>';
    var area_id = '<?php echo $area_id ?>';
    loaddata(country_id,state_id,city_id,area_id);
});

function loaddata(country_id,state_id,city_id,area_id)
{
    if(state_id == null || state_id == "")
    {
        area_id = "";
    }
    if(city_id == null || city_id == "")
    {
        area_id = "";
    }
    $('#spinner').fadeIn(1);
    if($.fn.DataTable.isDataTable('#example10'))
    {
        $('#example10').DataTable().destroy();
    }
    if(country_id == null || country_id == "" || country_id == 0)
    {
        country_id = $("#CId").val();
    }
    if(state_id == null || city_id == null || state_id == "" || city_id == "" || state_id == 0 || city_id == 0 || state_id == "undefined" || city_id == "undefined")
    {
        var city_state_id = $('#SCId').val();
        var city_id = city_state_id.substring(0, city_state_id.indexOf('-'));
        var state_id = city_state_id.substring(city_state_id.indexOf('-') + 1);
    }
    if(area_id == null || area_id == "" || area_id == 0)
    {
        area_id = $("#AId").val();
    }
    pintable = $('#example10').DataTable({
        "columnDefs": [{
            "defaultContent": "",
            "targets": "_all"
        }],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "ajax": {
            url: "controller/retailerControllerNew.php",
            type: "post",
            data: {getRetailerList:"getRetailerList",country_id:country_id,state_id:state_id,city_id:city_id,area_id:area_id,csrf:csrf},
            dataSrc: ""
        },
        "columns": [
            { "data": "action", render : function ( data, type, row, meta ) {
                if(data.retailer_order_id == 0 || data.retailer_order_id == null || data.retailer_order_id == "")
                {
                    return '<input type="checkbox" class="multiDelteCheckbox" value="'+data.retailer_id+'">';
                }
                else
                {
                    return '<input type="hidden"/>';
                }
            }},
            { "data": "no" },
            { "data": "action",render : function ( data, type, row, meta ) {
                if (type === 'display')
                {
                    var append = "";
                    if(data.retailer_order_id == 0 || data.retailer_order_id == null || data.retailer_order_id == "")
                    {
                        append += '<div style="display: inline-block;"><form id="'+data.retailer_id+'" class="mr-2" action="controller/retailerController.php" method="POST"><input type="hidden" name="csrf" value="'+csrf+'"><input type="hidden" name="retailer_id" value="'+data.retailer_id+'"><input type="hidden" name="country_id" value="'+data.country_id+'"><input type="hidden" name="state_id" value="'+data.state_id+'"><input type="hidden" name="city_id" value="'+data.city_id+'"><input type="hidden" name="area_id" value="'+data.area_id+'"><input type="hidden" name="retailer_photo" value="'+data.retailer_photo+'"><input type="hidden" name="deleteRetailer" value="deleteRetailer"><button type="button" onclick="confirmDelete('+data.retailer_id+');" title="Delete Retailer" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash"></i></button></form></div>';
                    }
                    return '<div style="display: inline-block;"><form class="mr-2" action="addRetailer" method="post"><input type="hidden" name="retailer_id" value="'+data.retailer_id+'"><input type="hidden" name="editRetailer" value="editRetailer"><button type="submit" title="Edit Retailer" class="btn btn-sm btn-warning"> <i class="fa fa-edit"></i></button></form></div><div style="display: inline-block;"><form action="retailerDetails" class="mr-2"><input type="hidden" name="RId" value="'+data.retailer_id+'"><input type="hidden" name="CId" value="'+data.country_id+'"><input type="hidden" name="SId" value="'+data.state_id+'"><input type="hidden" name="CIId" value="'+data.city_id+'"><input type="hidden" name="AId" value="'+data.area_id+'"><button type="submit" title="Retailer Details" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></button></form></div>'+append;
                }
                else
                {
                    return data;
                }
            }},
            { "data" : "status" , render : function ( data, type, row, meta ) {
                if (type === 'display')
                {
                    if (data.retailer_status == 1)
                    {
                         return '<button title="Deactivate Retailer" type="button" class="btn btn-success bg-success btn-sm mmw-80" onclick="changeStatusNew(\''+data.retailer_id+'\',\'retailerDeactive\')">Active</button>';
                    }
                    else
                    {
                        return '<button title="Activate Retailer" type="button"  class="btn btn-danger btn-sm mmw-80" onclick="changeStatusNew(\''+data.retailer_id+'\',\'retailerActive\')"/>Deactive</button>';
                    }
                }
                else
                {
                    return data;
                }
            }},
            { "data": "retailer_name" },
            { "data": "area_name" },
            { "data": "city_name" },
            { "data": "action", render : function ( data, type, row, meta ) {
                    return 'R'+data.retailer_id;
            }},
            { "data": "retailer_contact_person" },
            { "data": "retailer_contact_person_number" },
            { "data": "photo_new" , render : function ( data, type, row, meta ) {
                if (data.retailer_photo != "" && data.retailer_photo != null)
                {
                    if (data.photo_exists == 1)
                    {
                        return '<a href="../img/users/recident_profile/'+data.retailer_photo+'" data-fancybox="images" data-caption="Photo Name : '+data.retailer_name+'"><img src="../img/users/recident_profile/'+data.retailer_photo+'" width="100" height="100" alt="Retailer Image"/></a>';
                    }
                }
            }},
        ]
    });
    $('#spinner').fadeOut("slow");
}

function confirmDelete(id)
{
    swal({
        title: "Are you sure?",
        text: "You want to delete retailer!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) =>
    {
        if (willDelete)
        {
            $('#'+id).submit();
        }
    });
}

function changeStatusNew(id,status)
{
    swal({
        title: "Are you sure?",
        text: "You want to change status!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) =>
    {
        if (willDelete)
        {
            var csrf = "<?php echo $_SESSION['token']; ?>";
            $.ajax({
                url: "controller/statusController.php",
                cache: false,
                type: "POST",
                data: {id : id,status
                  :status,csrf : csrf},
                success: function(response)
                {
                    if(response==1)
                    {
                        Lobibox.notify('success',
                        {
                            pauseDelayOnHover: true,
                            continueDelayOnInactiveTab: true,
                            position: 'top right',
                            icon: 'fa fa-check-circle',
                            msg: 'Status Successfully Changed.'
                        });
                        destroydata(); loaddata();
                    }
                    else
                    {
                        Lobibox.notify('error',
                        {
                            pauseDelayOnHover: true,
                            continueDelayOnInactiveTab: true,
                            position: 'top right',
                            icon: 'fa fa-times-circle',
                            msg: 'Something Wrong.'
                        });
                    }
                }
            });
        }
    });
}

function destroydata()
{
    $('#example10').html('');
    pintable.destroy();
}

function DeleteAllNew(deleteValue)
{
    var oTable = $("#example10").dataTable();
    var val = [];
    $(".multiDelteCheckbox:checked", oTable.fnGetNodes()).each(function(i) {
        val[i] = $(this).val();
    });
    if(val == "")
    {
        swal(
            'Warning !',
            'Please Select at least 1 item !',
            'warning'
        );
    }
    else
    {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) =>
        {
            if(willDelete)
            {
                $.ajax({
                    url: "controller/deleteController.php",
                    cache: false,
                    type: "POST",
                    data: {ids : val,deleteValue:deleteValue},
                    success: function(response)
                    {
                        if(response == 1)
                        {
                            document.location.reload(true);
                        }
                        else
                        {
                            document.location.reload(true);
                        }
                    }
                });
            }
        });
    }
}
</script>