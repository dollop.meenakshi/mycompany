<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php //IS_572  signupForm to addGallary ?> 
            <form id="addGallary" method="post" action="controller/galleryController.php" enctype="multipart/form-data">
              <h4 class="form-header text-uppercase">
              <i class="fa fa-picture-o"></i>
              Photo Gallery
              </h4>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label"> Event <span class="required">*</span></label>
                <div class="col-sm-6">
                  <?php //IS_572  id="event" to id="event_id" ?> 
                  <select  required="" id="event_id" name="event_id" class="form-control single-select  check">
                    <option value="">---Select---</option>
                     
                    <?php
                    //IS_1015 and event_status=0 to 1
                    $q = $d->select("event_master","society_id='$society_id' and event_status=1  ","");




                    if(mysqli_num_rows($q)>0) {
                    ?>
                    <optgroup label="Upcoming">
                   <?php 
                    while($data = mysqli_fetch_array($q)){

                       $today=date('Y-m-d');
                      $eData= date('Y-m-d',strtotime($data['event_end_date']));


                      if (strtotime($today) < strtotime($eData) ) { 


                    ?>
                    <option  value="<?php echo $data['event_id'] ?>"><?php echo $data['event_title']; ?></option>
                    <?php }
                    } ?>
                    </optgroup>
                    <?php } ?> 
                

                    <?php   


                    $qc = $d->select("event_master","society_id='$society_id' and event_status=1 ","");
                    if(mysqli_num_rows($qc)>0) {
                    ?>
                    <optgroup label="Completed">
                    <?php  while($dataComp = mysqli_fetch_array($qc)){ 
                    /* echo "<pre>";print_r($dataComp);echo "</pre>";*/
                        $today=date('Y-m-d h:i A');
                        $eData= date('Y-m-d h:i A',strtotime($dataComp['event_end_date']));

                      
                        if ( strtotime($today) > strtotime($eData))  { 
                          ?>
                      <option value="<?php echo $dataComp['event_id'] ?>"><?php echo $dataComp['event_title']; ?></option>
                      <?php } } ?> 
                    </optgroup>
                    <?php }?>
                   <option  value="0">Other</option>
                </select>
                  Note: For Create New Folder Select Other Option in Event List
                </div>
                 
             
                <div class="col-sm-4" id="otherDiv">
                    <input maxlength="30" required type="text" class="form-control mb-2" id="input-dd" name="album_title" value="" placeholder="Title">
                    <input maxlength="30" required type="text" readonly class="form-control" id="default-datepicker" name="event_date" value="" placeholder="Date">
                  </div>
              </div>
              <div class="form-group row">
              <label for="input-13333" class="col-sm-2 col-form-label">Branch <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                  <select required="" multiple class="form-control multiple-select galleryForBranchMultiSelectCls"  name="block_id[]" id="block_id" onchange="getFloorListGallery();" >
                  <?php if ($access_branchs=="") { ?>
                  <option value="0">All Branch</option>
                    <?php }
                    $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                      while ($blockData=mysqli_fetch_array($bq)) { ?>
                      <option value="<?php echo $blockData['block_id'];?>"><?php echo $blockData['block_name'];?></option>
                    <?php  }?>
                    
                  </select>
                </div>
                <label for="input-13333" class="col-sm-2 col-form-label">Department <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                 
                  <select required="" multiple  class="form-control multiple-select galleryForDepartmentMultiSelectCls"  name="floor_id[]" id="floor_id">
                  <?php if ($access_branchs=="") { ?>
                  <option value="0">All Department</option>
                  <?php } ?>
                    <!-- <?php 
                    $fq=$d->select("floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.society_id='$society_id' ");
                      while ($floorData=mysqli_fetch_array($fq)) { ?>
                      <option <?php if(isset($voting_id) && $poll_for==$floorData['floor_id']  ) { echo "selected";} ?> value="<?php echo $floorData['floor_id'];?>"><?php echo $floorData['floor_name'].' ('.$floorData['block_name'].')';?></option>
                    <?php  }?> -->
                    
                  </select>
                </div>
                
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label"> Photo <span class="required">*</span></label>
                <div class="col-sm-10">
                   
                  <input class="form-control-file border photoOnly" type="file" accept="image/*" name="gallery_photo[]" ><br>
                  <input class="form-control-file border photoOnly" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <input class="form-control-file border photoOnly" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <input class="form-control-file border photoOnly" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <input class="form-control-file border photoOnly" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <input class="form-control-file border photoOnly" type="file" accept="image/*" name="gallery_photo[]"><br>
                  <?php //IS_988 galary_photos ?>
                  <div id="galary_photos_div"   ></div>
                  <br> <?php //IS_653 , 20 Files at a time added?> 
                  <!-- <h6>Note: Press CTRL key to Upload Multiple Photos(Max Size 2MB Per Photo, 20 Files at a time)</h6> -->
                </div>
                
              </div>
              
              <input type="hidden" name="society_id" value="<?php echo $society_id ?>">
              <div class="form-footer text-center">
                <input type="hidden" name="addGallery" value="addGallery">
                <button type="submit" class="btn btn-success" name = ""><i class="fa fa-check-square-o"></i> SAVE</button>
              </div>
            </form>
           
          </div>
        </div>
      </div>
      </div><!--End Row-->
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->

<script src="assets/js/jquery.min.js"></script>

      <script type="text/javascript">
//IS_988
         $( "#addGallary" ).submit(function( event ) {
  var error = 0;
  
 var fp = $("#galary_photos");
               var lg = fp[0].files.length; // get length
               var items = fp[0].files;
               var fileSize = 0;
           
           if (lg > 0) {
               for (var i = 0; i < lg; i++) {
                   fileSize = fileSize+items[i].size; // get file size

                   if(  items[i].size > 2097152) {
                    error++;
                    $('#galary_photos_div').css('color','#ff0000');
                    $('#galary_photos_div').css('text-transform','uppercase');
                    $('#galary_photos_div').css('font-size','.75rem');
                    $('#galary_photos_div').css('font-weight','600');
                    
                    $('#galary_photos_div').text('each file size must be less than or equal to 2MB.');
                  }
               }
               
           }
   if(error==0){
    $('#galary_photos_div').text('');
   }
  if(error > 0 ){
    event.preventDefault();
    $('#galary_photos_div').css('color','#ff0000');
    $('#galary_photos_div').css('text-transform','uppercase');
    $('#galary_photos_div').css('font-size','.75rem');
    $('#galary_photos_div').css('font-weight','600');
    
    $('#galary_photos_div').text('each file size must be less than or equal to 2MB.');
  } else {
    $( "#addGallary" ).submit();
  }
  
});
// /IS_988

        


      $(document).ready(function() {
          $('#otherDiv').hide();
          
          $('.check').change(function(){
            var data= $(this).val();
             if(data==0) {
              $('#otherDiv').show();
             } else {
              $('#otherDiv').hide();
             }          
          });
        
      });
  </script>