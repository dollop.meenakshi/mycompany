<?php 
error_reporting(0);
if (filter_var($_GET['id'], FILTER_VALIDATE_INT) == true  && filter_var($_GET['user_id'], FILTER_VALIDATE_INT) == true && filter_var($_GET['unit_id'], FILTER_VALIDATE_INT) == true && filter_var($_GET['societyid'], FILTER_VALIDATE_INT) == true) {
  $society_id = $_GET['societyid'];
  $url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
  $activePage = basename($_SERVER['PHP_SELF'], ".php");
  include_once 'lib/dao.php';
  include 'lib/model.php';
  $d = new dao();
  $m = new model();
  $con=$d->dbCon();
  extract(array_map("test_input" , $_GET));
  if (!isset($societyid)) {
    echo "Invalid Url !";
    exit();
  }
  include 'common/checkLanguage.php';

  
  $society_id =  mysqli_real_escape_string($con, $societyid);
  $sp=$d->select("society_master","society_id='$society_id'");
  $socData=mysqli_fetch_array($sp);
  $society_name=$socData['society_name'];
  $secretary_email= $socData['secretary_email'];
  $society_address= $socData['society_address'].', '.$socData['city_name'];
  $currency = $socData['currency'];
  $pan_number = $socData['pan_number'];
  $gst_no = $socData['gst_no'];
  $country_id = $socData['country_id'];
  $socieaty_logo = $socData['socieaty_logo'];
  $state_id = $socData['state_id'];

  if ($country_id==101) { 
    $cgstName = $xml->string->cgst;
    $sgstName = $xml->string->sgst;
    $igstName = $xml->string->igst;
  } else if ($country_id==161) { 
    $igstName = $xml->string->igst;
  } 

    $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,
                  "getCountriesSingle=getCountriesSingle&country_id=$country_id&language_id=1");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'key: bmsapikey'
      ));
      $server_output = curl_exec($ch);
      curl_close ($ch);
      $server_output=json_decode($server_output,true);
   $country_name= $server_output['name'];

 $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                "getStateSingle=getStateSingle&state_id=$state_id&language_id=1");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'key: bmsapikey'
    ));
    $server_output = curl_exec($ch);
    curl_close ($ch);
    $server_output=json_decode($server_output,true);
 $state_name= $server_output['name'];
 $invoiceNumber= "#INV-GST-006081";
 $invoice_date= "";
 $due_date= "";
 $qty = "";
 $user_name= "";
 $attention = "";
 $user_address = "";
 $user_gst = "";

 $rate_amount = "";
 $rate_amount_total = "";
 $discount = "";
 $sub_total = "";
 $gst_value = "";
 $gst_percenage = "";
 $late_fees = "";
 $grand_total = "";
 $grand_total_word = "";
 $igstDiv= "false";
 $cgstDiv= "false";
 $transaction_charges = '';
 $fixed_charge = "";
  if ($type=='P') {
    // Penalty Data
    $category = 'Penalty';
    $qb=$d->select("penalty_master,unit_master,users_master,block_master","unit_master.block_id=block_master.block_id AND penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$societyid ' AND penalty_master.penalty_id='$id' AND penalty_master.paid_status=0","");
    $data=mysqli_fetch_array($qb);
    extract($data);
    $invoiceNumber= "#INVPN$id";
    if($penalty_receive_date !="0000-00-00 00:00:00") { $invoice_date= date("d-m-Y", strtotime($penalty_receive_date)); } 
    if ($penalty_payment_type==0) {
      $payment_mode="Cash";
    } else if ($penalty_payment_type==1) {
      $payment_mode="Cheque";
      $payment_description = "Bank: $bank_name - $payment_ref_no ";
    } else if ($penalty_payment_type==2) {
      $payment_mode="Online";
      $payment_description = "Transaction Id: $payment_ref_no ";;
    } else {
      $payment_mode="";
      $payment_description = "";
    }
   $invoice_description =  "Penalty for $penalty_name (Penalty Date : $penalty_date)";
   $qty = "1";
   $user_name=$user_full_name;
   $attention = "";
   $user_address = $socData['society_address'].', '.$socData['city_name'];
   $user_gst = "";
   $transaction_charges = $transaction_charges;
   $rateTotal = $penalty_amount;
   $rateTota1 = $penalty_amount;
   $rateAmountSingle = $penalty_amount;
   $taxAmount = $penalty_amount;
   $grand_total =$penalty_amount+$transaction_charges;
  }

  if($is_taxble=="1"){
     $gst_amount = $taxAmount - ($taxAmount  * (100/(100+$tax_slab))); 
     $amount_without_gst_single=$taxAmount-$gst_amount;
     $amount_without_gst=$taxAmount-$gst_amount;
     $amount_subTotal=$taxAmount-$transaction_charges;
     // $rateTotal = $rateTotal-$gst_amount;
    if($taxble_type=="0"){
      $cgstDiv ="true";
      $gst_amount =  number_format($gst_amount/2,2,'.','');
      $tax_slab =  number_format($tax_slab/2,2,'.','');
    } else {
      $igstDiv ="true";
      $gst_amount =  number_format($gst_amount,2,'.','');
      $tax_slab =  number_format($tax_slab,2,'.','');
    }

  } else {
    $gst_amount = 0;

    $amount_without_gst_single=$taxAmount-$transaction_charges;
    $amount_without_gst=$taxAmount-$transaction_charges;
  }

  if($type=="B"){
     $amount_without_gst_single=$unit_price;
  }
   $amount_without_gst;
?>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $invoiceNumber; ?> Invoice</title>
<link rel="icon" href="../img/fav.png" type="image/png">
<meta name="author" content="harnishdesign.net">
<meta name="viewport" content="width=1024">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>
<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="assets/css/invoice.css"/>

</head>
<body id="printableArea">
<div class="container-fluid invoice-container">
  <div class="row no-print" id="printPageButton">
    <div class="col-lg-12 text-center">
      <a href="#" onclick="window.history.go(-1); return false;"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Back</a>
      <a href="#" onclick="printDiv('printableArea')"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
    </div>
  </div>
  <div >
    <header>
    <div class="row align-items-center">
      <div class="col-sm-7 text-center text-sm-left mb-3 mb-sm-0"><br />
        <?php if ($socieaty_logo !='') { ?>
        <img width="200" id="logo" src="<?php if(file_exists("../img/society/$socieaty_logo")) { echo '../img/society/'.$socieaty_logo; }  ?>" title="invoice" alt="" />
        <?php } ?>
      </div>
      <div class="col-sm-5 text-center text-sm-right">
        <h2 class="text-primary mb-0">Proforma Invoice</h2>
        <h6 class="text-secondary mb-0">Invoice No.: <i class="text-danger"><?php echo $invoiceNumber; ?></i></h6>
        <h6 class="text-secondary mb-0">Category : <?php echo $category; ?></h6>
        <h6 class="text-secondary mb-0">Status : Unpaid</h6>
      </div>
    </div>
    </header>
    <main>
  
    <div class="row">
      <div class="col-sm-6 "> <strong><?php echo $society_name; ?></strong>
        <address>
        <?php echo $society_address; ?><br>
        <?php echo $state_name; ?> - <?php echo $country_name;?> (<?php echo $socData['society_pincode'];?>)
  	   <br />
       <br />
       <?php if ($pan_number!='') { 
        echo "PAN #: ". $pan_number.'<br />';
       } if ($gst_no!='') { 
        echo $xml->string->tax_number." #: ". $gst_no.'<br />';
       }  ?>
        </address>
      </div>
      <!-- <div class="col-sm-6 text-sm-right"> </div> -->
      
    </div> 

     <div class="row">
      <div class="col-sm-6 "> Bill To
        <address>
        <strong><?php echo $user_name; ?> (<?php echo $user_designation; ?>)</strong> <br />
      <?php echo $last_address; ?><br>
     
       <br />
      <?php if ($user_gst!='') { 
        echo $xml->string->tax_number." #: ". $user_gst.'<br />';
       }  ?>
        </address>
      </div>
      <div class="col-sm-6 text-sm-right">
           <b>  Date :</b> <?php echo date("d-m-Y"); ?><br />
            <?php if($due_date!='') { 
              echo "<b>Due Date : </b>". date("d-m-Y", strtotime($due_date))." <br />";
            } ?>
            
            
    </div> 


  <div class="table-responsive">
  <table class="table">
    <thead style='background-color:#6C6B6C;color: white;'>
      <tr>
        <th scope="col">#</th>
        <th style="max-width: 280px;" scope="col">Item/Description</th>
        <th scope="col">Qty/Unit</th>
        <th scope="col">HSN/SAC</th>
        <th scope="col">Rate</th>
        <th scope="col" class="text-right">Amount</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">1</th>
        <td style="max-width: 280px;">
            <?php echo $invoice_description; ?>
        </td>
        <td class="text-center"><?php echo $qty;?></td>
        <td>-</td>
        <td><?php echo number_format($rateAmountSingle,2,'.','');?></td>
        <td align="right"><?php echo number_format($rateTota1,2,'.','');?></td>
      </tr>
      <?php  if ($minimum_charge>0 && $rateTota1<$minimum_charge) { ?>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td>Minimum Charge</td>
        <td align="right"><?php echo number_format($minimum_charge,2,'.','');?></td>
      </tr>
      
       <?php }  if ($fixed_charge>0) { ?>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td>Fixed Charge</td>
        <td align="right"><?php echo number_format($fixed_charge,2,'.','');?></td>
      </tr>

       <?php }
        if($discount>0) { ?>
       <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td>Discount(<?php echo number_format($discount_per,2,'.','');?>%) </td>
        <td align="right">-<?php echo  number_format($discount,2,'.',''); ?></td>
      </tr>
      <?php } if($sub_total>0 && $discount>0) { ?>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td>Sub Total</td>
        <td align="right"><?php echo  number_format($sub_total,2,'.',''); ?></td>
      </tr>
       <?php } if($cgstDiv=="true" || $igstDiv=="true") { ?>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td>Taxable Amount </td>
        <td align="right"><?php echo  number_format($amount_without_gst,2,'.',''); ?></td>
      </tr>
      <?php } if ($cgstDiv=="true") { ?>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td><?php echo $cgstName; ?> (<?php echo  $tax_slab.'%'; ?>)</td>
        <td align="right"><?php echo $gst_amount; ?></td>
      </tr>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td><?php echo $sgstName; ?> (<?php echo  $tax_slab.'%'; ?>)</td>
        <td align="right"><?php echo $gst_amount; ?></td>
      </tr>
      <?php } if ($igstDiv=="true") { ?>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td><?php echo $igstName;?> (<?php echo  $tax_slab.'%'; ?>)</td>
        <td align="right"><?php echo $gst_amount; ?></td>
      </tr>
      <?php } if ($late_fees>0) { ?>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td>Late Fees</td>
        <td align="right"><?php echo number_format($late_fees,2,'.','');?></td>
      </tr>
      <?php } 

        $orgValue =number_format((float)$grand_total,2,'.','');
        $roundOffValue =number_format((float)$grand_total,0,'.','');
        $roundOff =   abs($orgValue - $roundOffValue);
        if ($orgValue>$roundOffValue) {
           $symbol = "(-)";
        } else {
           $symbol = "(+)";
        }
        if ($roundOff>0) {
      ?>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td>Rounding Off(S)</td>
        <td align="right"><?php echo $symbol;?> <?php echo number_format($roundOff,2,'.','');?></td>
      </tr>
      <?php } ?>
      <tr>
        <th scope="row"></th>
        <td></td>
        <td></td>
        <td></td>
        <td><strong>Total Invoice Amount</strong></td>
        <td align="right"><strong><?php echo $currency;?> <?php echo number_format((float)$roundOffValue).'.00';?></strong></td>
      </tr>
      
     
     



      
      
    </tbody>
  </table>
</div>
   </main>
     
    <?php if ($wallet_amount_type==0 && $wallet_amount>0) { 
                echo "<i style='color: #df3636;'>Remark: ". $currency.' '.$wallet_amount. " Amount credited in wallet for Overpaid</i>";
              }else if ($wallet_amount_type==1 && $wallet_amount>0) { 
                echo "<i style='color: #df3636;'>Remark: ".  $currency.' '.$wallet_amount. " Amount debited from wallet</i>";
              }
              $invoice_term_conditions_master_qry=$d->select("invoice_term_conditions_master","society_id='$societyid' and status= '0'  "," ORDER BY invoice_term_condition_id DESC limit 0,1");
              if(mysqli_num_rows($invoice_term_conditions_master_qry) > 0 ){
                $invoice_term_conditions_master_data=mysqli_fetch_array( $invoice_term_conditions_master_qry);
                echo "<br> <h4><span  class='card-title' ><u>Terms & Conditions</u></span></h4>";
                echo  $invoice_term_conditions_master_data['condition_desc'];
              }
              ?>
    <footer class="text-center">
    <hr>
     <p class="text-gray-dark" align="center" style="color: #b5b5b5;">This is a computer generated proforma invoice, thus no signature is required.</p>
   
    </footer>

   
  </div>
  </div>
</body>
<script type="text/javascript">
  function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }

     window.onload = function() { printDiv('printableArea'); }

    Android.print(printContents);
</script>
</html>
<?php } else{
  echo "Invalid Request!!!";
}?>