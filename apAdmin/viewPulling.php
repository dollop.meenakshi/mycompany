
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Manage Pollings</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="welcome">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Pollings</li>
        </ol>
      </div>
      <div class="col-sm-3">
        <div class="btn-group float-sm-right">
          <a href="addPulling" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
         <a href="javascript:void(0)" onclick="DeleteAll('deletePoll');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

          
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
           
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>Question</th>
                    <th>Description</th>
                    <th>Stat Date</th>
                    <th>End Date</th>
                    <th>Status</th>
                    <th>Reports</th>
                    <!-- <th>Action</th> -->
                    
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $q=$d->select("voting_master","society_id='$society_id'","");
                  $i = 0;
                  while($row=mysqli_fetch_array($q))
                  {
                  // extract($row);
                  $i++;
                  
                  ?>
                  <tr>
                     <td class='text-center'>
                      <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['voting_id']; ?>">
                    </td>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['voting_question']; ?></td>
                    <td><?php echo $row['voting_description']; ?></td>
                    <td><?php echo $row['voting_start_date']; ?></td>
                    <td><?php echo $row['voting_end_date']; ?></td>
                    <td>
                      <?php
                            if($row['voting_status']=="0"){
                      ?>
                        <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['voting_id']; ?>','pollClose');" data-size="small"/>
                        <?php } else { ?>
                       <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['voting_id']; ?>','pollActive');" data-size="small"/>
                      <?php } ?>
                      
                    </td>
                    <td>
                     
                      <form action="pollResult" method="post">
                          <input type="hidden" name="voting_id" id="votingData" value="<?php echo $row['voting_id'] ?>">
                        <button type="submit" class="btn btn-primary btn-sm" name="pullingReport">View</button>
                        
                      </form>
                      </td>
                   
                    
                    
                  </tr>
                  <?php }?>
                </tbody>
                
              </div>
            </table>
          </div>
        </div>
      </div>
    </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->
  
  </div><!--End content-wrapper-->
  <!--Start Back To Top Button-->

<script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">
        function getVogingDetails(voting_id) {
           $('#pullingBox').modal('show');
             $.ajax({
              url: "getPullingReport.php",
              cache: false,
              type: "POST",
              data: {voting_id : voting_id},
              success: function(response){
                  $('#addMainDiv').html(response);
              }
           });
        }

</script>

  <div class="modal fade" id="pullingBox">
  <div class="modal-dialog">
    <div class="modal-content border-success">
      <div class="modal-header bg-success">
        <h5 class="modal-title text-white">Polling Reports</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="addMainDiv">

      </div>
    </div>
  </div>
</div>