<?php
$all = [];
$all_distributor = [];
$all_route = [];
if(isset($_GET['RId']) && !empty($_GET['RId']))
{
    $retailer_id = $_GET['RId'];
    $CId = $_GET['CId'];
    $SId = $_GET['SId'];
    $CIId = $_GET['CIId'];
    $AId = $_GET['AId'];
    if(!ctype_digit($CId) || !ctype_digit($SId) || !ctype_digit($CIId) || !ctype_digit($AId) || !ctype_digit($retailer_id))
    {
        $_SESSION['msg1'] = "Invalid Id!";
    ?>
    <script>
        window.history.go(-2);
    </script>
    <?php
        exit;
    }
    if ((int) $retailer_id != $_GET['RId'])
    {
        $_SESSION['msg1'] = "Invalid Retailer Id!";
    ?>
    <script>
        window.location = "manageRetailer?CId=<?php echo $CId; ?>&SCId=<?php echo $CIId; ?>-<?php echo $SId; ?>&AId=<?php echo $AId; ?>";
    </script>
    <?php
    exit;
    }
    $q = $d->selectRow("rm.*,dm.distributor_name,amn.area_name,rma.route_name","retailer_master AS rm LEFT JOIN retailer_distributor_relation_master AS rdrm ON rdrm.retailer_id = rm.retailer_id LEFT JOIN distributor_master AS dm ON dm.distributor_id = rdrm.distributor_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id LEFT JOIN route_retailer_master AS rrm ON rrm.retailer_id = rm.retailer_id LEFT JOIN route_master AS rma ON rma.route_id = rrm.route_id","rm.retailer_id = '$retailer_id' AND rm.society_id = '$society_id'");
    if(mysqli_num_rows($q) > 0)
    {
        while($data = $q->fetch_assoc())
        {
            $all_distributor[] = $data['distributor_name'];
            $all_route[] = $data['route_name'];
            $all[] = $data;
        }
    }
    else
    {
        $_SESSION['msg1'] = "Invalid Id!";
    ?>
    <script>
        window.location = "manageRetailer?CId=<?php echo $CId; ?>&SCId=<?php echo $CIId; ?>-<?php echo $SId; ?>&AId=<?php echo $AId; ?>";
    </script>
    <?php
    exit;
    }
    $all_distributor = array_unique($all_distributor);
    $all_route = array_unique($all_route);
}
else
{
    $CId = $_GET['CId'];
    $SId = $_GET['SId'];
    $CIId = $_GET['CIId'];
    $AId = $_GET['AId'];
    $_SESSION['msg1'] = "Invalid Id!";
?>
<script>
    window.location = "manageRetailer?CId=<?php echo $CId; ?>&SCId=<?php echo $CIId; ?>-<?php echo $SId; ?>&AId=<?php echo $AId; ?>";
</script>
<?php
exit;
}
?>
<style>
    .small-width{
        width: 50px;
    }
</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Retailer Details</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form class="mr-2" action="addRetailer" method="post">
                        <input type="hidden" name="retailer_id" value="<?php echo $retailer_id; ?>">
                        <input type="hidden" name="editRetailer" value="editRetailer">
                        <button type="submit" class="btn btn-sm btn-warning"> Edit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body wrapper">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="table-responsive">
                                    <table class="table w-100">
                                        <tr>
                                            <th class="small-width">Retailer Name</th>
                                            <td><?php echo $all[0]['retailer_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Retailer Code</th>
                                            <td><?php echo $all[0]['retailer_code']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Contact Person</th>
                                            <td><?php echo $all[0]['retailer_contact_person']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Contact Number</th>
                                            <td><?php echo $all[0]['retailer_contact_person_country_code'] . " " . $all[0]['retailer_contact_person_number']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Alternate Number</th>
                                            <td><?php echo ($all[0]['retailer_alt_contact_number'] == 0) ? "" : $all[0]['retailer_alt_country_code'] . " " . $all[0]['retailer_alt_contact_number']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Address</th>
                                            <td style="white-space: normal !important;"><?php echo $all[0]['retailer_address']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Pincode</th>
                                            <td><?php echo $all[0]['retailer_pincode']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Area</th>
                                            <td><?php echo $all[0]['area_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Route</th>
                                            <td style="white-space: normal !important;">
                                            <?php
                                            foreach($all_route AS $key => $value)
                                            {
                                            ?>
                                                <span class="badge badge-primary"><?php echo $value; ?></span>
                                            <?php
                                            }
                                            ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">GST</th>
                                            <td><?php echo $all[0]['retailer_gst_no']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Distributor</th>
                                            <td style="white-space: normal !important;">
                                            <?php
                                            foreach($all_distributor AS $key1 => $value1)
                                            {
                                            ?>
                                                <span class="badge badge-info"><?php echo $value1; ?></span>
                                            <?php
                                            }
                                            ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Credit Limit</th>
                                            <td><?php echo $all[0]['retailer_credit_limit']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Credit Days</th>
                                            <td><?php echo $all[0]['retailer_credit_days']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Retailer Verified</th>
                                            <td>
                                            <?php
                                            if($all[0]['retailer_verified'] == 0)
                                            {
                                            ?>
                                                <span class="text-danger">Unverified</span>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <span class="text-success">Verified</span>
                                            <?php
                                            }
                                            ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Last Order Date</th>
                                            <td><?php echo $all[0]['retailer_last_order_date']; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php
                                        if($all[0]['retailer_photo'] != "" && file_exists("../img/users/recident_profile/".$all[0]['retailer_photo']))
                                        {
                                        ?>
                                        <img class="img-fluid p-4" style="height:300px;" src="../img/users/recident_profile/<?php echo $all[0]['retailer_photo']; ?>"/>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="col-sm-12 p-3">
                                        <div class="map" id="map2" style="width: 100%; height: 300px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $d->map_key(); ?>"></script>
<script>
function initMap()
{
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };

    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("map2"), mapOptions);
    map.setTilt(50);

    // Multiple markers location, latitude, and longitude
    var markers = [
        ["<?php echo $all[0]['retailer_name'] ?>","<?php echo $all[0]['retailer_latitude'] ?>","<?php echo $all[0]['retailer_longitude'] ?>"]
    ];
    
    // Info window content
    var infoWindowContent = [
        ["<div class='info_content'><h6><?php echo $all[0]['retailer_name']; ?></h6><p><?php echo $all[0]['retailer_address']; ?></p><p><?php echo $all[0]['retailer_contact_person']; ?></p><p><?php echo $all[0]['retailer_contact_person_country_code'] . " " . $all[0]['retailer_contact_person_number']; ?></p></div>"]
    ];

    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Place each marker on the map
    for( i = 0; i < markers.length; i++ )
    {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });

        // Add info window to marker
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event)
    {
        this.setZoom(16);
        google.maps.event.removeListener(boundsListener);
    });
}
<?php
if($all[0]['retailer_latitude'] != "" && $all[0]['retailer_longitude'] != "")
{
?>
// Load initialize function
google.maps.event.addDomListener(window, 'load', initMap);
<?php
}
?>
</script>