<?php
// error_reporting(0);
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$month_year = (int) $_REQUEST['month'];

$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
$month= $_REQUEST['month'];
$laYear = $_REQUEST['laYear'];
if (!isset($month)) {
  $month = date("m");
  $laYear = date("Y");
}

$startDate = date('Y-m-d',strtotime(date("$laYear-$month-01")));
$month_end_date = date("Y-m-t", strtotime($startDate));

$time3 = new DateTime($startDate);
$time4 = new DateTime($month_end_date);
$interval = $time3->diff($time4);
$indays =  $interval->format('%a');

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Add Attendance  </h4>
      </div>
    </div>
    <form action="" method="get" class="branchDeptFilterWithUser">
      <div class="row pb-2">
        <?php include('selectBranchDeptEmpForFilter.php'); ?>
        <div class="col-md-3 col-6">
          <select name="laYear" class="form-control">
            <option value="">Year</option>
            <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
              echo 'selected';
            } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $onePreviousYear) {
              echo 'selected';
            } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $currentYear) {
              echo 'selected';
            } ?> <?php if ($_GET['laYear'] == '') {
              echo 'selected';
            } ?> value="<?php echo $currentYear; ?>"><?php echo $currentYear; ?></option>
            <option <?php if ($_GET['laYear'] == $nextYear) {
              echo 'selected';
            } ?> value="<?php echo $nextYear; ?>"><?php echo $nextYear; ?></option>
          </select>
        </div>
        <div class="col-md-3 col-6">
          <select required class="form-control single-select" name="month" id="month">
            <option value="">-- Select Month--</option>
            <?php
            $selected = "";
            for ($m = 1; $m <= 12; $m++) {
              $monthName = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
              $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
              if (isset($month)  && $month != "") {
                if ($month == $m) {
                  $selected = "selected";
                } else {
                  $selected = "";
                }
              } else {
                $selected = "";
                if ($m == date('n')) {
                  $selected = "selected";
                } else {
                  $selected = "";
                }
              }
              ?>
              <option <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $monthName; ?></option>
            <?php } ?>
          </select>
        </div>
        <input type="hidden" name="">
        <div class="col-lg-2 from-group col-6">
          <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Report">
        </div>
      </div>
    </form>
    <!-- End Breadcrumb-->
    <input type="hidden" name="floor_id_old" id="floor_id_old" value="<?php if (isset($_GET['dId']) && $_GET['dId'] > 0) {echo $_GET['dId'];} ?>">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <p class="info_message text-warning"></p>
            <div class="table-responsive">
             <?php
              $i = 1;
              if (isset($_GET['dId']) && isset($_GET['bId']) && isset($_GET['uId']) && $_GET['uId']>0) {
                ?>
                <table  id="" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Date</th>
                      <th>Day</th>
                      <th>Employee</th>
                      <th>Present</th>
                      <th>Punch In</th>
                      <th>Punch Out</th>
                      <th>Late In</th>
                      <th>Early Out</th>
                      <th>Leave</th>
                    </tr>
                  </thead>
                  <tbody class="monthCalendarView">
                   <?php 
                    $i1 = 1;
                    for ($j = 0; $j <= $indays; $j++) { 
                      $jtemp = $j+1;
                      $dateTemp = $laYear. "-" . $month.'-'.$jtemp;
                      $dateTemp = date('Y-m-d', strtotime($dateTemp));
                      $dateTemp = date('d-m-Y', strtotime($dateTemp));
                      $dayName = date('D', strtotime($dateTemp));
                    ?>
                    <tr>
                        <td><?php echo  $i1++; ?></td>
                        <td><?php echo  $dateTemp; ?></td>
                        <td><?php echo  $dayName; ?></td>
                        <td class="employee_name"></td>
                        <td class="c_<?php echo $j;?>"></td>
                        <td class="punchin_<?php echo $j;?>"></td>
                        <td class="punchout_<?php echo $j;?>"></td>
                        <td class="latein_<?php echo $j;?>"></td>
                        <td class="early_<?php echo $j;?>"></td>
                        <td class="leave_<?php echo $j;?>"></td>
                      </tr>
                  <?php } ?> 
                  </tbody>
                  </table>
                  <?php } else {  ?>
                      <div class="" role="alert">
                        <span><strong>Note :</strong> Please Select Employee</span>
                      </div>
                    <?php } ?>

                  </div>
                </div>
              </div>
            </div>
          </div><!-- End Row-->

          <?php if (isset($uId) && $uId>0) {  ?>
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <table id="" class="table table-bordered">
                        <thead>
                          <tr>
                            <th class="reportTitle">Description</th>
                            <th>Result</th>
                          </tr>
                        </thead>
                        <tbody>
                           <tr>
                            <td>Total Month Days</td>
                            <td class="total_month_days"><?php echo $indays+1;?></td>
                          </tr>
                          <tr>
                            <td>Total Working Days</td>
                            <td class="total_working_days"></td>
                          </tr>
                          <tr>
                            <td>Total Present Days</td>
                            <td class="total_present"></td>
                          </tr>

                          <tr>
                            <td>Extra Working Days</td>
                            <td class="total_extra_days"></td>
                        </tr>
                        <tr>
                          <td>Half Day Leave</td>
                          <td class="total_half_day"></td>
                      </tr>
                      <tr>
                        <td>Full Day Leave</td>
                        <td class="total_full_leave"></td>
                    </tr>
                    <tr>
                      <td>Total Leaves</td>
                      <td class="total_leave"></td>
                    </tr>
                    <tr>
                      <td>Total Working Hours</td>
                      <td class="total_month_hours_view"></td>
                    </tr>
                    <tr>
                      <td>Total Worked Hours</td>
                      <td class="total_month_hour_spent_view"></td>
                    </tr>
                  <tr>
                    <td>Total Late In </td>
                    <td class="late_punch_in"></td>
                  </tr>
                <tr>
                  <td>Total Early Out </td>
                  <td class="early_punch_out"></td>
                </tr>
                <tr>
                  <td>Total Punch Out Missing</td>
                  <td class="total_punch_out_missing"></td>
                </tr>
                <tr>
                  <td>Total Pending Attendance</td>
                  <td class="total_pending_attendance"></td>
                </tr>
                <tr>
                  <td>Total Rejected Attendance</td>
                  <td class="total_rejected_attendance"></td>
                </tr>
                <tr>
                  <td> Holidays </td>
                  <td class="total_holidays"></td>
                </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<?php } ?>

</div>
<!-- End container-    fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->
<div class="modal fade" id="addAttendaceModalNew">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Add Attendance</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addAttendanceFormNew" action="controller/leaveController.php" method="post">
                <div class="modal-body" style="align-content: center;">
                    <div class="form-group row punch_out_date_div d-none">
                        <label for="punch_out_date" class="col-sm-4 col-form-label">Punch Out Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-group single-select" required name="punch_out_date" id="punch_out_date_new">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="punch_in_time" class="col-sm-4 col-form-label">Punch In Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" required class="form-control time-picker-shft-Out_update" readonly id="punch_in_time" name="punch_in_time">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="punch_out_time" class="col-sm-4 col-form-label">Punch Out Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" required class="form-control time-picker-shft-Out_update" readonly id="punch_out_time_new" name="punch_out_time">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="attendance_type" class="col-sm-4 col-form-label">Attendance Type <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-control single-select attendance_drop" name="attendance_type" id="attendance_type" required>
                                <option value>--Select--</option>
                                <option value="0" selected>Full Day</option>
                                <option value="1">Half Day</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 text-center">
                        <input type="hidden" name="attendance_date" id="attendance_date_new">
                        <input type="hidden" name="max_shift_hour" id="max_shift_hour">
                        <input type="hidden" name="max_punch_out_time" id="max_punch_out_time">
                        <input type="hidden" name="shift_type" id="shift_type_new">
                        <input type="hidden" name="shift_time_id" id="shift_time_id_new">
                        <input type="hidden" name="per_day_hour" id="per_day_hour_new">
                        <input type="hidden" name="unit_id" id="unit_id_new">
                        <input type="hidden" name="late_time_start" id="late_time_start_new">
                        <input type="hidden" name="early_out_time" id="early_out_time_new">
                        <input type="hidden" name="shift_start_time" id="shift_start_time_new">
                        <input type="hidden" name="shift_end_time" id="shift_end_time_new">
                        <input type="hidden" name="block_id" value="<?php echo $_GET['bId']; ?>">
                        <input type="hidden" name="floor_id" value="<?php echo $_GET['dId']; ?>">
                        <input type="hidden" name="user_id" value="<?php echo $_GET['uId']; ?>">
                        <input type="hidden" name="month" value="<?php echo $_GET['month']; ?>">
                        <input type="hidden" name="laYear" value="<?php echo $_GET['laYear']; ?>">
                        <input type="hidden" name="addAttendance" value="addAttendance">
                        <button type="button" onclick="submitAddAttendanceForm();" class="btn btn-sm btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="attnSmryDateDataMdl">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Date Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">
          <div class="row col-md-12 ">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody id="attnSmryDateData">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>

<?php if(isset($uId)) { ?>
<script type="text/javascript">
  var month = '<?php echo $month; ?>';
  var year = '<?php echo $laYear; ?>';
  eventArray = [];
  WholeArray = [];
  modaljasonObAr = [];

getMonthData(month,year)
 function getMonthData(month,year)
{
  shift_time_id = $('#shift_time_id').val();
  $.ajax({
      url: "../residentApiNew/MonthAttendanceData.php",
      cache: false,
      type: "POST",
      async:false,
      data: {user_id :$('#user_attendance_id').val(),
            getMonthlyAttendanceHistory:"getMonthlyAttendanceHistory",
            //user_id: '<?php echo $uId; ?>',
            user_id: $('#uId').val(),
            month: month,
            year: year,
            shift_time_id:shift_time_id,
            society_id:'<?php echo $society_id; ?>'
        },
      success: function(response){
          if(response.monthly_history.length >0){
              var presentData;
              var lateInData;
              var earlyOutData;
              var leaveData;
              var punchInRangeData;
              var punchOutRangeData;
              $.each(response.monthly_history, function( index, value ) {

                  if (value.present==true && value.extra_day==true) {
                    presentData =`<i class="fa fa-check-circle text-success" aria-hidden="true"> <span class="badge badge-success"> Extra Day</span>`;
                  }else if (value.present==true && value.attendance_declined==true) {
                    presentData =`<i class="fa fa-times-circle text-danger" aria-hidden="true"> <span class="badge badge-danger"> Attendance Rejected</span>`;
                  }else if (value.present==true && value.half_day==true) {
                    presentData =`<i class="fa fa-check-circle text-success" aria-hidden="true">  <span class="badge badge-danger">Half Day Leave</span>`;
                  }else if (value.present==true) {
                    presentData =`<i class="fa fa-check-circle text-success" aria-hidden="true"> <span class="badge badge-success"> Present</span>`;
                  } else if (value.holiday==true) {
                    presentData =`<span class="badge badge-secondary"> Holiday (`+value.holiday_name+`)</span>`;
                    var selectedDate = value.date;
                    if(Date.parse(selectedDate)-Date.parse(new Date())<0) {
                     presentData =`
                     <div class="d-flex align-items-center">
                        <button class="btn btn-primary btn-sm" onclick="addAttendance(`+value.add_attendace_click+`)"><i class="fa fa-plus" aria-hidden="true"></i> Add Attendance</button>
                        </div>
                        <span class="badge badge-secondary"> Holiday (`+value.holiday_name+`)</span>`;

                     } else  {
                        presentData =`
                     <div class="d-flex align-items-center">
                        <span class="badge badge-secondary"> Holiday (`+value.holiday_name+`)</span>
                    </div>`;
                     }
                  } else if (value.week_off==true) {
                    presentData =`<span class="badge badge-info"> Week OFF</span>`;
                    var selectedDate = value.date;
                    if(Date.parse(selectedDate)-Date.parse(new Date())<0) {
                     presentData =`
                     <div class="d-flex align-items-center">
                        <button class="btn btn-primary btn-sm" onclick="addAttendance(`+value.add_attendace_click+`)"><i class="fa fa-plus" aria-hidden="true"></i> Add Attendance</button>
                        </div>
                        <span class="badge badge-info"> Week OFF</span>`;

                     } else  {
                        presentData =`
                     <div class="d-flex align-items-center">
                        <span class="badge badge-info"> Week OFF</span>
                    </div>`;
                     }
                  } else if (value.present==true){
                    if (value.leave==true && value.half_day==true) {
                    presentData =`<i class="fa fa-times-circle text-danger" aria-hidden="true"></i> <span class="badge badge-danger">Half Day Leave</span>`;
                    } else {
                    presentData =`<i class="fa fa-times-circle text-danger" aria-hidden="true"></i>`;
                    }
                  } else if (value.present==false && value.leave==true){
                    presentData =` <span class="badge badge-danger">Full Day Leave</span>`;
                  }else if (value.present==false && value.half_day==true){
                    presentData =` <span class="badge badge-danger">Half Day Leave</span>`;
                    var selectedDate = value.date;
                    if(Date.parse(selectedDate)-Date.parse(new Date())<0) {
                     presentData =`
                     <div class="d-flex align-items-center">
                        <button class="btn btn-primary btn-sm" onclick="addAttendance(`+value.add_attendace_click+`)"><i class="fa fa-plus" aria-hidden="true"></i> Add Attendance</button>

                        </div><span class="badge badge-danger">Half Day Leave</span>`;

                     } else  {
                        presentData =`
                     <div class="d-flex align-items-center">
                        <span class="badge badge-danger">Half Day Leave</span>
                    </div>`;
                     }

                  }else {
                    var selectedDate = value.date;
                    if(Date.parse(selectedDate)-Date.parse(new Date())<0) {
                     presentData =`
                     <div class="d-flex align-items-center">
                        <button class="btn btn-primary btn-sm" onclick="addAttendance(`+value.add_attendace_click+`)"><i class="fa fa-plus" aria-hidden="true"></i> Add Attendance</button>
                        </div>`;

                     } else  {
                        presentData =`
                     <div class="d-flex align-items-center">
                        
                    </div>`;
                     }
                  }

                  if (value.present==true && value.is_punch_out_missing==true) {
                    presentData +=` <span class="badge badge-danger">Punch Out Missing</span>`;
                  }

                  if (value.present==true && value.attendnace_pending==true) {
                    presentData +=` <span class="badge badge-warning">Attendance Pending</span>`;
                  }



                  $('.c_'+index).html(presentData);

                  if (value.present==true ) {
                    lateInData=value.punch_in_time;
                  }else{
                    lateInData=``;
                  }
                  $('.punchin_'+index).html(lateInData);

                  if (value.present==true ) {
                    lateInData=value.punch_out_time;
                  }else{
                    lateInData=``;
                  }
                  $('.punchout_'+index).html(lateInData);


                  if (value.late_in==true ) {
                    lateInData=`<i class="fa fa-check-circle text-success" aria-hidden="true"> `+value.punch_in_time+`</i>`;
                  }else{
                    lateInData=``;
                  }
                  $('.latein_'+index).html(lateInData);

                  if (value.early_out==true ) {
                    earlyOutData =`<i class="fa fa-check-circle text-success" aria-hidden="true"> `+value.punch_out_time+`</i>`;
                  }else{
                    earlyOutData =``;
                  }
                  $('.early_'+index).html(earlyOutData);

                  if (value.leave==true  && value.half_day==true) {
                    leaveData =`<span class="badge badge-danger">Half Day</span>`;
                  }else if (value.leave==true  && value.half_day==false) {
                    leaveData =`<span class="badge badge-danger">Full Day</span>`;
                  }else{
                    leaveData =``;
                  }
                  $('.leave_'+index).html(leaveData);

            });

            $('.employee_name').html(response.user_full_name);
            $('.total_working_days').html(response.total_working_days);
            $('.total_present').html(response.total_present);
            $('.total_extra_days').html(response.total_extra_days);
            $('.total_half_day').html(response.total_half_day);
            $('.total_full_leave').html(response.total_leave);
            $('.total_leave').html(parseFloat(response.total_leave)+parseFloat(response.total_half_day/2));
            $('.total_month_hours_view').html(response.total_month_hours_view);
            $('.total_month_hour_spent_view').html(response.total_month_hour_spent_view);
            $('.late_punch_in').html(response.late_punch_in);
            $('.early_punch_out').html(response.early_punch_out);
            $('.total_holidays').html(response.total_holidays);
            $('.total_punch_out_missing').html(response.total_punch_out_missing);
            $('.total_pending_attendance').html(response.total_pending_attendance);
            $('.total_rejected_attendance').html(response.total_rejected_attendance);
            $('.info_message').html(response.info_message);
            $('.reportTitle').html(response.user_full_name+ ` `+response.month+` Monthly Report`);


        }

      }
  });
       
        
}

function addAttendance(user_id,attendance_date,shift_type,shift_time_id,per_day_hour,unit_id,late_time_start,early_out_time,shift_start_time,shift_end_time,max_punch_out_time,max_shift_hour)
{
    $.ajax({
        url: "controller/leaveController.php",
        cache: false,
        type: "POST",
        data:
        {
            checkApprovedHalfDay:"checkApprovedHalfDay",
            user_id:user_id,
            attendance_date:attendance_date,
            csrf:csrf
        },
        success: function(response)
        {
            response = JSON.parse(response);
            if(response == 1)
            {
                $('.attendance_drop').empty();
                $('.attendance_drop').append(`<option value="">--Select--</option>`);
                $('.attendance_drop').append(`<option value="1" selected>Half Day</option>`);
            }
            else
            {
                $('.attendance_drop').empty();
                $('.attendance_drop').append(`<option value="">--Select--</option>`);
                $('.attendance_drop').append(`<option value="0" selected>Full Day</option>`);
                $('.attendance_drop').append(`<option value="1">Half Day</option>`);
            }
        }
    });
    $('#attendance_date_new').val(attendance_date);
    $('#shift_type_new').val(shift_type);
    $('#shift_time_id_new').val(shift_time_id);
    $('#per_day_hour_new').val(per_day_hour);
    $('#unit_id_new').val(unit_id);
    $('#late_time_start_new').val(late_time_start);
    $('#early_out_time_new').val(early_out_time);
    $('#shift_start_time_new').val(shift_start_time);
    $('#shift_end_time_new').val(shift_end_time);
    $('#max_shift_hour').val(max_shift_hour);
    $('#max_punch_out_time').val(max_punch_out_time);
    if(shift_type == "Night")
    {
        var dt = $.datepicker.parseDate('yy-mm-dd', attendance_date);
        dt.setDate(dt.getDate() + 1)
        var dtNew = $.datepicker.formatDate('yy-mm-dd', dt);
        $('#punch_out_date_new').empty();
        $('#punch_out_date_new').append($("<option></option>").attr("value","").text("--Select--"));
        $('#punch_out_date_new').append($("<option></option>").attr("value", attendance_date).text(attendance_date));
        $('#punch_out_date_new').append($("<option></option>").attr("value", dtNew).text(dtNew));
        $('.punch_out_date_div').removeClass('d-none');
    }
    else
    {
        $('.punch_out_date_div').addClass('d-none');
    }
    $('#addAttendaceModalNew').modal();
}

$('#addAttendaceModalNew').on('hidden.bs.modal', function ()
{
    $('#addAttendanceFormNew').trigger("reset");
    $("#addAttendanceFormNew").validate().resetForm();
});

function submitAddAttendanceForm()
{
    if($('#addAttendanceFormNew').valid())
    {
        var timefrom = new Date();
        temp1 = $('#punch_in_time').val().split(":");
        timefrom.setHours((parseInt(temp1[0]) - 1 + 24) % 24);
        timefrom.setMinutes(parseInt(temp1[1]));
        var timeto = new Date();
        temp2 = $('#punch_out_time_new').val().split(":");
        timeto.setHours((parseInt(temp2[0]) - 1 + 24) % 24);
        timeto.setMinutes(parseInt(temp2[1]));
        shift_type = $('#shift_type_new').val();
        attendance_date = $('#attendance_date_new').val();
        punch_out_date = $('#punch_out_date_new').val();

        max_punch_out_time=$('#max_punch_out_time').val();
        if(max_punch_out_time!=''){
          var max_punch_out_time = new Date();
          temp3 = $('#max_punch_out_time').val().split(":");
          max_punch_out_time.setHours((parseInt(temp3[0]) - 1 + 24) % 24);
          max_punch_out_time.setMinutes(parseInt(temp3[1]));
        }

        max_shift_hour=$('#max_shift_hour').val();
        if(max_shift_hour!=''){
          var max_shift_hour = new Date();
          temp4 = $('#max_shift_hour').val().split(":");
          max_shift_hour.setHours((parseInt(temp4[0])+parseInt(temp1[0])) % 24);
          max_shift_hour.setMinutes(parseInt(temp4[1]+parseInt(temp1[1])));
        }

        if(timeto < timefrom && shift_type == "Day")
        {
            swal("Punch in time should be smaller than punch out time!", {icon: "error",});
        }
        else if (timeto < timefrom && shift_type == "Night" && attendance_date == punch_out_date)
        {
            swal("Punch in time should be smaller than punch out time!", {icon: "error",});
        }
        else
        {
          if(max_punch_out_time > timeto || max_shift_hour > timeto){
            $('#addAttendanceFormNew').submit();
          }else{
            swal("Punch out time exceeds maximum punch out time!", {icon: "error",});
          }
        }
    }
    else
    {
        $('#addAttendanceFormNew').validate();
    }
}
</script>

<?php } ?>