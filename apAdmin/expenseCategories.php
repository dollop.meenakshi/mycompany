<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-5">
        <h4 class="page-title"><?php echo $xml->string->expense_category; ?></h4>
      </div>
      <div class="col-sm-3 col-7">
        <div class="btn-group float-sm-right">
          <button type="button" data-toggle="modal" data-target="#complainCategoryModal" class="float-right btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add</button>
          <a href="javascript:void(0)" onclick="DeleteAll('deleteExpenseCategory');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a> 
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>#</th>
                    <th><?php echo $xml->string->name; ?></th>
                    <th><?php echo $xml->string->block_type; ?></th>
                    <th>Action</th>

                  </tr>
                </thead>
                <tbody>
                 <?php 
                 $i=1;
                 $q=$d->select("expense_category_master","","");
                 while($row=mysqli_fetch_array($q))
                 {
                  extract($row);
                  ?>
                  <tr>
                    <td class='text-center'>
                      <?php 
                        $billExist = $d->selectArray("expenses_balance_sheet_master","expense_category_id='$expense_category_id'");
                        if(empty($billExist)){
                      ?>
                        <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['expense_category_id']; ?>">
                      <?php } ?>
                    </td>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $expense_category_name; ?></td>
                    <td><?php if ($category_type==1) {
                       echo $xml->string->income;
                      }else {
                         echo $xml->string->expense;
                      }  ?></td>
                    <td>
                      <button type="button" data-toggle="modal" data-target="#editcomplainCategoryModal" class="btn btn-info btn-sm" onclick="editExpenseCategory(<?php echo $expense_category_id ?>)"><i class="fa fa-pencil"></i></button>
                    </td>
                  </tr>
                  <?php }?>

                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<div class="modal fade" id="complainCategoryModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add; ?> <?php echo $xml->string->expense_category; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="addexplenseCategory" method="POST" action="controller/balancesheetController.php" enctype="multipart/form-data">
            <div class="row form-group">
              <label class="col-sm-3 form-control-label">Name <span class="text-danger">*</span></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" maxlength="200" autocomplete="off" required="" name="expense_category_name">
              </div>
            </div>
            <div class="row form-group">
              <label class="col-sm-3 form-control-label"><?php echo $xml->string->category;?> <span class="text-danger">*</span></label>
               <div class="col-sm-9">
                <select class="form-control" maxlength="200" autocomplete="off" required="" name="category_type">
                  <option value=""><?php echo $xml->string->select;?> </option>
                  <option value="0"><?php echo $xml->string->expense;?> </option>
                  <option value="1"><?php echo $xml->string->income;?> </option>
                </select>
              </div>
            </div>
              <div class="form-footer text-center">
                <input type="hidden" name="addExpenseCategory">
              <button class="btn btn-sm btn-success"><i class="fa fa-check"></i> Submit</button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>

<div class="modal fade" id="editcomplainCategoryModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->edit; ?> <?php echo $xml->string->expense_category; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="editcomplaintCatDetails">
      </div>
     
    </div>
  </div>
</div>