<?php
if(isset($_GET['CId']) && !empty($_GET) && isset($_GET['SCId']))
{
    $country_id = $_GET['CId'];
    $ci_st_id = explode("-",$_GET['SCId']);
    $city_id = $ci_st_id[0];
    $state_id = $ci_st_id[1];
}
elseif((isset($_COOKIE['CId']) && !empty($_COOKIE['CId'])) || (isset($_COOKIE['SId']) && !empty($_COOKIE['SId'])) || (isset($_COOKIE['CIId']) && !empty($_COOKIE['CIId'])))
{
    $country_id = $_COOKIE['CId'];
    $state_id = $_COOKIE['SId'];
    $city_id = $_COOKIE['CIId'];
}

if(isset($_GET['UId']) && !empty($_GET['UId']))
{
    $user_id = $_GET['UId'];
}
/*elseif(isset($_COOKIE['UId']) && !empty($_COOKIE['UId']))
{
    $user_id = $_COOKIE['UId'];
}*/

if(isset($city_id) && !empty($city_id) && !ctype_digit(strval($city_id)))
{
    $_SESSION['msg1'] = "Invalid City Id!";
?>
    <script>
        window.location = "manageRoute";
    </script>
<?php
exit;
}

if(isset($state_id) && !empty($state_id) && !ctype_digit($state_id))
{
    $_SESSION['msg1'] = "Invalid State Id!";
?>
    <script>
        window.location = "manageRoute";
    </script>
<?php
exit;
}
if(isset($country_id) && !empty($country_id) && !ctype_digit($country_id))
{
    $_SESSION['msg1'] = "Invalid Country Id!";
    ?>
    <script>
        window.location = "manageRoute";
    </script>
<?php
exit;
}
?>
<style>
    ul {
      list-style-type: none;
    }
</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Manage Route</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form action="addRoute" method="post" id="addRouteBtnForm">
                        <input type="hidden" name="addRouteBtn" value="addRouteBtn">
                        <button type="button" onclick="submitAddBtnForm();" class="btn btn-primary btn-sm waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add</button>
                    </form>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteRoute');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <form id="filterFormRoute">
            <div class="row pt-2 pb-2">
                <div class="col-lg-3 col-6">
                    <label  class="form-control-label">Country </label>
                    <select name="CId" id="CId" class="form-control single-select" required onchange="getStateCity(this.value);">
                        <option value="">--Select--</option>
                        <?php
                        $cq = $d->select("countries","");
                        while ($cd = $cq->fetch_assoc())
                        {
                        ?>
                        <option <?php if($country_id == $cd['country_id']) { echo 'selected';} ?> value="<?php echo $cd['country_id']; ?>"><?php echo $cd['country_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">State-City </label>
                    <select class="form-control single-select" id="SCId" name="SCId" required>
                        <option value="">-- Select --</option>
                        <?php
                        if(isset($country_id) && $country_id != 0 && $country_id != "")
                        {
                            $qt = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id' AND c.flag = 1");
                            while ($qd = $qt->fetch_assoc())
                            {
                        ?>
                        <option <?php if($city_id == $qd['city_id']) { echo 'selected'; } ?> value="<?php echo $qd['city_id']."-".$qd['state_id'];?>"><?php echo $qd['city_name'];?>-<?php echo $qd['state_name'];?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">User</label>
                    <select class="form-control single-select-new" id="UId" name="UId">
                        <option value="">-- Select --</option>
                        <?php
                            $qu = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","user_status = 1 AND active_status = 0 AND delete_status = 0");
                            while ($ud = $qu->fetch_assoc())
                            {
                        ?>
                        <option <?php if($user_id == $ud['user_id']) { echo 'selected'; } ?> value="<?php echo $ud['user_id']; ?>"><?php echo $ud['user_full_name'] . " (" . $ud['block_name'] . "-" . $ud['floor_name'] . ")" ; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="submit">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>Action</th>
                                        <th>Status</th>
                                        <th>Route Name</th>
                                        <th>City</th>
                                        <th>Employee Count</th>
                                        <th>Retailer Count</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $append = "";
                                    if(isset($country_id) && $country_id != "" && $country_id != 0)
                                    {
                                        $append .= " AND rm.country_id = '$country_id'";
                                    }
                                    if(isset($state_id) && $state_id != "" && $state_id != 0)
                                    {
                                        $append .= " AND rm.state_id = '$state_id'";
                                    }
                                    if(isset($city_id) && $city_id != "" && $city_id != 0)
                                    {
                                        $append .= " AND rm.city_id = '$city_id'";
                                    }
                                    if(isset($user_id) && $user_id != 0)
                                    {
                                        $append .= " AND ream.user_id = '$user_id'";
                                    }
                                    $q = $d->selectRow("rm.*,ci.city_name,s.state_name,co.country_name,COUNT(DISTINCT ream.user_id) AS empCnt,COUNT(DISTINCT rrm.retailer_id) AS retCount","route_master AS rm JOIN cities AS ci ON ci.city_id = rm.city_id JOIN states AS s ON s.state_id = rm.state_id JOIN countries AS co ON co.country_id = rm.country_id LEFT JOIN route_employee_assign_master AS ream ON ream.route_id = rm.route_id LEFT JOIN route_retailer_master AS rrm ON rrm.route_id = rm.route_id","rm.society_id = '$society_id'".$append,"GROUP BY rm.route_id ORDER BY route_name ASC LIMIT 1000");
                                    $iNo = 1;
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td class='text-center'>
                                            <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['route_id']; ?>">
                                        </td>
                                        <td><?php echo $iNo++; ?></td>
                                        <td>
                                            <div style="display: inline-block;">
                                                <form class="mr-2" action="addRoute" method="post">
                                                    <input type="hidden" name="route_id" value="<?php echo $row['route_id']; ?>">
                                                    <input type="hidden" name="editRoute" value="editRoute">
                                                    <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
                                                </form>
                                            </div>
                                            <div style="display: inline-block;">
                                                <form class="mr-2" action="controller/routeController.php" method="POST">
                                                    <input type="hidden" name="route_id" value="<?php echo $row['route_id']; ?>">
                                                    <input type="hidden" name="country_id" value="<?php echo $country_id; ?>">
                                                    <input type="hidden" name="state_id" value="<?php echo $state_id; ?>">
                                                    <input type="hidden" name="city_id" value="<?php echo $city_id; ?>">
                                                    <input type="hidden" name="deleteRoute" value="deleteRoute">
                                                    <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </div>
                                            <?php if($retCount>0) { ?>
                                            <div style="display: inline-block;" class="mr-2">
                                                <button type="button" onclick="changeRetailerOrder('<?php echo $route_id; ?>');" class="btn btn-sm btn-primary" title="Change Retailer Order"><i class="fa fa-sort"></i></button>
                                            </div>
                                            <?php  } ?>
                                            <div style="display: inline-block;">
                                                <form class="mr-2" action="routeDetails" method="POST">
                                                    <input type="hidden" name="route_id" value="<?php echo $row['route_id']; ?>">
                                                    <input type="hidden" name="country_id" value="<?php echo $country_id; ?>">
                                                    <input type="hidden" name="state_id" value="<?php echo $state_id; ?>">
                                                    <input type="hidden" name="city_id" value="<?php echo $city_id; ?>">
                                                    <input type="hidden" name="routeDetails" value="routeDetails">
                                                    <button type="submit" class="btn btn-sm btn-info" title="Route Details"><i class="fa fa-eye"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                        <td>
                                        <?php
                                        if($route_active_status == 0)
                                        {
                                        ?>
                                            <button title="Deactivate Route" type="button" class="btn btn-success btn-sm mmw-80 bg-success" onclick="changeStatus('<?php echo $route_id; ?>','routeDeactive')">Active</button>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <button title="Activate Route" type="button" class="btn btn-danger btn-sm mmw-80" onclick="changeStatus('<?php echo $route_id; ?>','routeActive')"/>Deactive</button>
                                        <?php
                                        }
                                        ?>
                                        </td>
                                        <td><?php echo $route_name; ?></td>
                                        <td><?php echo $city_name; ?></td>
                                        <td><?php echo $empCnt; ?></td>
                                        <td><?php echo $retCount; ?></td>
                                        <td><?php if($route_description != ""){ echo $route_description; } ?></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<div class="modal fade" id="retailerOrderModal">
    <div class="modal-dialog">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Change Retailer Order</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="retailerList">
                <form name="frmQA" id="changeOrderForm" action="controller/routeController.php" method="POST"/>
                    <input type="hidden" name="row_order" id="row_order" />
                    <input type="hidden" name="changeRetailerOrder" value="changeRetailerOrder" />
                    <ul id="sortable-row">
                    </ul>
                    <div class="form-footer text-center">
                        <input type="submit" class="btn btn-sm btn-primary btnSave" name="submit" value="Save Order" onClick="saveOrder();" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>

<script>
function getStateCity(country_id)
{
    $.ajax({
        url: 'controller/routeController.php',
        data: {getStateCityTagRoute:"getStateCityTagRoute",country_id:country_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#SCId').empty();
            response = JSON.parse(response);
            $('#SCId').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#SCId').append("<option value='"+value.city_id+"-"+value.state_id+"'>"+value.city_name+" - "+value.state_name+"</option>");
            });
        }
    });
}

function submitAddBtnForm()
{
    var country_id = $('#CId').val();
    var state_city_id = $('#SCId').val();
    var city_id = state_city_id.substr(0, state_city_id.indexOf('-'));
    var state_id = state_city_id.split("-")[1];
    if(country_id != "" && country_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'country_id',
            value: country_id
        }).appendTo('form');
    }
    if(city_id != "" && city_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'city_id',
            value: city_id
        }).appendTo('form');
    }if(state_id != "" && state_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'state_id',
            value: state_id
        }).appendTo('form');
    }
    $("#addRouteBtnForm").submit()
}

function changeRetailerOrder(route_id)
{
    $.ajax({
        url: 'controller/routeController.php',
        data: {getRetailerListTag:"getRetailerListTag",route_id:route_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#sortable-row').empty();
            response = JSON.parse(response);
            $.each(response, function(key, value)
            {
                $('#sortable-row').append("<li id='"+value.route_retailer_id+"' class='border m-2 bg-light'><i class='fa fa-sort mr-2'></i>"+value.retailer_name+"</li>");
            });
            $('<input>').attr({
                type: 'hidden',
                name: 'country_id',
                value: $('#country_id').val()
            }).appendTo('#changeOrderForm');
            $('<input>').attr({
                type: 'hidden',
                name: 'state_city_id',
                value: $('#state_city_id').val()
            }).appendTo('#changeOrderForm');
            $('#retailerOrderModal').modal();
        }
    });
}

$(function()
{
    $("#sortable-row").sortable();

    $("#CId").change(function()
    {
        setTimeout(
            function()
            {
                $("#filterFormRoute").removeAttr("novalidate");
            },700);
    });
});

function saveOrder()
{
    var selectedLanguage = new Array();
    $('ul#sortable-row li').each(function()
    {
        selectedLanguage.push($(this).attr("id"));
    });
    document.getElementById("row_order").value = selectedLanguage;
}
</script>