<?php
session_start();
$society_id=$_COOKIE['society_id'];
extract($_POST);
include_once 'lib/dao.php';
include 'lib/model.php';
include 'common/checkLogin.php';
$d = new dao();
$m = new model();
$base_url=$m->base_url();
$master_url=$d->master_url();
$keydb = $m->api_key();

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

$q=$d->select("bms_admin_master","admin_id='$_COOKIE[bms_admin_id]'");
$data=mysqli_fetch_array($q);
extract($data);

$q=$d->select("society_master","society_id='$society_id'");
$bData=mysqli_fetch_array($q);
// $pq=$d->select("package_master","package_id='$package_id' ");
// $pkgData=mysqli_fetch_array($pq);
// $amount = $pkgData['package_amount'];
// $package_id =$pkgData['package_id'];

$language_id = $_COOKIE['language_id'];
  $chLang = curl_init();
  curl_setopt($chLang, CURLOPT_URL,$master_url."commonApi/packageController.php");
  curl_setopt($chLang, CURLOPT_POST, 1);
  curl_setopt($chLang, CURLOPT_POSTFIELDS,
            "getPackage=getPackage&language_id=$language_id&society_id=$society_id&package_id=$package_id");

  curl_setopt($chLang, CURLOPT_RETURNTRANSFER, true);

  curl_setopt($chLang, CURLOPT_HTTPHEADER, array(
   'key: '.$keydb
  ));

  $server_output_lng = curl_exec($chLang);

  curl_close ($chLang);
  $server_output_lng=json_decode($server_output_lng,true);


  
$planName= $server_output_lng['cPlanName'];
$no_of_month = $server_output_lng['cPlanMonth'];

$razorpay_keyId = $server_output_lng['razorpay_keyId'];
$razorpay_keySecret = $server_output_lng['razorpay_keySecret'];

$amount =$server_output_lng['cPlanAmount'];
$package_id =$server_output_lng['cPlanPacakgeId'];
if ($amount<1) {
  $_SESSION['msg1']= "Something went wrong.";
  header("location:buyPlan");
  exit();
}
$keyId = $razorpay_keyId;
$keySecret = $razorpay_keySecret;
$displayCurrency = 'INR';
require('razorpay-php/Razorpay.php');

$userName = $data['admin_name'];
$userMobile = $data['admin_mobile'];
$userEmail = $data['admin_email'];
$cDate = date("YmdHi");
$merchant_order_id ="MYCO".$cDate.$data['admin_id'].$package_id;


use Razorpay\Api\Api;

$api = new Api($keyId, $keySecret);

//
// We create an razorpay order using orders api
// Docs: https://docs.razorpay.com/docs/orders
//
$orderData = [
    'receipt'         => 3456,
    'amount'          => $amount * 100, // 1 rupees in paise
    'currency'        => 'INR',
    'payment_capture' => 1 // auto capture
];

$razorpayOrder = $api->order->create($orderData);

$razorpayOrderId = $razorpayOrder['id'];

$_SESSION['razorpay_order_id'] = $razorpayOrderId;

$displayAmount = $amount = $orderData['amount'];
if ($displayCurrency !== 'INR')
{
    $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
    $exchange = json_decode(file_get_contents($url), true);

    $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
}


$paymentAry = array(
    'society_id' => $data['society_id'], 
    'package_id' => $package_id,
    'no_of_month' => $no_of_month,
    'package_name' => $planName,
    'user_mobile' => $admin_mobile,
    'payment_mode' => "razorpay",
    'transection_amount' => $displayAmount/100,
    'transection_date' => date("Y-m-d H:i:s"),
    'payment_status' => "pending",
    'payment_firstname' => $admin_name,
    'payment_email' => $admin_mobile,
    'order_id' => $razorpayOrderId,
    'receipt_no' => $merchant_order_id,
    'merchant_id' => $keyId,
    'merchant_key' => $keySecret,
 );

$address = $bData['society_address'];
$q3=$d->insert("transection_master",$paymentAry);

$data = [
    "key"               => $keyId,
    "amount"            => $amount,
    "name"              => "MyCo Plan",
    "description"       => $planName,
    "image"             => $base_url."img/logo.png",
    "prefill"           => [
    "name"              => $userName,
    "email"             => $userEmail,
    "contact"           => $userMobile,
    ],
    "notes"             => [
    "address"           => $address,
    "merchant_order_id" => $merchant_order_id,
    ],
    "theme"             => [
    "color"             => "#2f648e"
    ],
    "order_id"          => $razorpayOrderId,
];

if ($displayCurrency !== 'INR')
{
    $data['display_currency']  = $displayCurrency;
    $data['display_amount']    = $displayAmount;
}

$json = json_encode($data);

require("checkout/manual.php");

?>