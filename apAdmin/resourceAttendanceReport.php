<?php 
error_reporting(0);
if (isset($_GET['from']) && $_GET['emp_id']!='All') {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1 || filter_var($_GET['emp_id'], FILTER_VALIDATE_INT) != true){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='attendanceReport';
        </script>");
  }
} else if(isset($_GET['from']) && $_GET['emp_id']=='All'){
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='attendanceReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title"><?php echo $xml->string->employees; ?> Attendance Report</h4>
        </div>
     
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
          <div class="col-lg-3 col-6">
            <label  class="form-control-label"><?php echo $xml->string->employee; ?> Type</label>
            <select class="form-control select-sm single-select" name="empType" id="empType" onchange="this.form.submit()">
              <option value="All"> All </option>
              <?php
              if ($_GET['emp_type']=='0') {
                $q=$d->select("emp_type_master","society_id='$society_id'","");
              } else if ($_GET['emp_type']=='1') {
                $q=$d->select("emp_type_master","society_id='$society_id' OR society_id=0","");
              } else {
                $q=$d->select("emp_type_master","society_id='$society_id' OR society_id=0","");
              }
              while ($row=mysqli_fetch_array($q)) {
              ?>
              <option <?php if($_REQUEST['empType']==$row['emp_type_id']) { echo 'selected';} ?> value="<?php echo $row['emp_type_id'];?>"><?php echo $row['emp_type_name'];?></option>
              <?php }?>
            </select>
          </div>
          <div class="col-lg-3 col-6">
            <?php 
                  if ($_GET['empType']!="" && $_GET['empType']!="All") {
                      if ( $_GET['empType']==1) {
                         $empType=0;
                      }else {
                         $empType=$_GET['empType'];
                      }
                     $q=$d->select("employee_master","society_id='$society_id' AND emp_type_id='$empType'","");
                    } else {

                     $q=$d->select("employee_master","society_id='$society_id'","");
                    }
              ?>

            <label  class="form-control-label">Select <?php echo $xml->string->employee; ?> </label>
             <select required="" class="form-control single-select" name="emp_id">
                <option value="All">All</option>
                  <?php
                   
                  
                        while ($mData=mysqli_fetch_array($q)) {
                          $emp_id=$mData['emp_id']; ?>
                          <option <?php if($mData['emp_id']==$_GET['emp_id']) { echo 'selected';} ?> value="<?php echo $mData['emp_id'] ?>"><?php echo $mData['emp_name']; 
                           $EW=$d->select("emp_type_master","emp_type_id='$mData[emp_type_id]'");
                            $empTypeData=mysqli_fetch_array($EW);
                           if ($empTypeData['emp_type_name']!='') {
                  echo ' - '.$empTypeData['emp_type_name'];
                  } else {
                  echo " - Security Guard";
                  } ?> </option>
                  <?php } ?>
             </select>
          </div>
          
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get">
          </div>
          <div class="col-lg-2">
        

          </div>

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                  extract(array_map("test_input" , $_GET));

                    if ($_GET['empType']!="" && $_GET['empType']!="All") {
                      if ( $_GET['empType']==1) {
                         $appQeury= "employee_master.emp_type_id=0";
                      }else {
                          $appQeury= "employee_master.emp_type_id=$_GET[empType]";
                      }
                    } else {
                      $appQeury= "";
                    }

                    if ($appQeury!='') {
                     $appQeury = " AND $appQeury";
                    }
                  // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                    $emp_id=mysqli_real_escape_string($con, $_GET['emp_id']);
                  if ($emp_id=="All") {
                       $q=$d->select("employee_master,staff_visit_master","employee_master.emp_id =staff_visit_master.emp_id  AND employee_master.society_id='$society_id' AND staff_visit_master.filter_data BETWEEN '$from' AND '$toDate'  $appQeury","ORDER BY staff_visit_master.staff_visit_id");
                    } else {

                       $q=$d->select("employee_master,staff_visit_master","employee_master.emp_id =staff_visit_master.emp_id  AND employee_master.society_id='$society_id' AND staff_visit_master.filter_data BETWEEN '$from' AND '$toDate' AND staff_visit_master.emp_id='$emp_id'  $appQeury","ORDER BY staff_visit_master.staff_visit_id");
                      
                  }


                  $i=1;
                if (isset($_GET['from']) && $_GET['from'] !='') {
               ?>
              
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $xml->string->employee; ?></th>
                        <th>Mobile</th>
                        <th>In Date Time</th>
                        <th>Out Date Time</th>
                        <th>Duration</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q)) {

                      $EW=$d->select("emp_type_master","emp_type_id='$data[emp_type_id]'");
                      $empTypeData=mysqli_fetch_array($EW);
                    // echo '<pre>';
                    // print_r($data);
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['emp_name'].'-'; if ($empTypeData['emp_type_name']!='') {
                          echo custom_echo($empTypeData['emp_type_name'],10);
                          } else {
                          echo "Security Guard";
                          } ?></td>
                        <td><?php echo $data['country_code'] . " " . substr($data['emp_mobile'], 0, 2) . '*****' . substr($data['emp_mobile'], -3); ?></td>
                        <td><?php echo  date("Y-m-d h:i A", strtotime($data['visit_entry_date_time'])); ?></td>
                        <td><?php  if ($data['visit_status']!=0) { echo  date("Y-m-d h:i A", strtotime($data['visit_exit_date_time'])); }?></td>
                        <td><?php 
                          if ($data['visit_status']!=0) {
                            
                            $intTime=$data['visit_entry_date_time'];
                            $outTime=$data['visit_exit_date_time'];
                            $date_a = new DateTime($intTime);
                            $date_b = new DateTime($outTime);
                            $interval = date_diff($date_a,$date_b);
                            $totalDays= $interval->d;
                            $totalMinutes= $interval->i;
                            $totalHours= $interval->h;
                             if ($totalDays>0) {
                              echo  $interval->d.' days '. $interval->h.' hours '. $interval->i.' min';
                              } else {
                              echo $interval->format('%h:%i');
                              }
                          }
                           ?></td>
                        <td>
                         <?php if ($data['visit_status']==1) {
                          echo "Exit";
                         } elseif ($data['visit_status']==0) {
                          echo "In";
                         } 
                         ?>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            <?php } else {  ?>
              

                <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->