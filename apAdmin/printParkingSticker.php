<?php 
// session_cache_limiter(false);
session_start();
include_once 'lib/dao.php';
include_once 'lib/model.php';

$d = new dao();
$m = new model();
extract(array_map("test_input" , $_GET));
if (isset($_COOKIE['bms_admin_id'])) {
	$bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
	$society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

if ($_GET['type']!="qrcode") {
?>
<!DOCTYPE html>
<html>
<head>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
	<?php include 'common/colours.php'; ?>
	<link href="assets/css/app-style9.css" rel="stylesheet"/>

	<title>Parking Sticker Print</title>
	<style type="text/css">
		body {
			/*background-color: #d7d6d3;*/
			font-family: 'Open Sans', sans-serif;
		}
		h2 {
			font-size: 12px;
			margin: 5px 0;
		}
		h3 {
			font-size: 10px;
			margin: 2.5px 0;
			font-weight: 300;
		}
		.addressSoc {
			font-size: 7px;
			font-weight: bold;
		}
		.col-md-4 {
			-ms-flex: 0 0 33.333333%;
			flex: 0 0 33.333333%;
			/*margin: 5px !important;*/
			width:33.33% !important;
			float: left;
			display: inline-block;
			padding: 0px !important; 
		}

 .main-div{
 	border-radius: 50% !important; background-color: var(--primary) !important; color:#fff !important; padding-top:10px  !important; padding-left: 5px  !important; padding-right:5px  !important;  float: left;
 }
 .img-div{
 	width:110px !important; 
 	height: 110px !important; 
 	border-radius: 50% !important;
 	background-color: #fff !important;
 	opacity: 1 !important;
 	box-shadow: 3px 3px 20px
 	rgba(0, 0, 0, 0.5);
 	border: 2px solid
 	rgba(255, 255, 255, 1);
 }
 .sosa-name-div{
 	text-transform: uppercase; font-weight: 600 !important; padding: 10px !important;
 }
 .block-info{
 	text-transform: uppercase;
 	overflow-wrap: break-word;
 	min-height: 24px !important;
 	min-width: 60px !important;
 	max-height: 20px !important;
 	max-width: 90px !important;
 	background-color:#fff !important;
 	font-weight: 600 !important;
 	line-height: 25px !important;
 	color:var(--primary) !important;
 	box-shadow: 3px 3px 20px
 	rgba(0, 0, 0, 0.5);
 	border-top: 2px solid
 	rgba(255, 255, 255, 1);
 }
 .parking-permit{
 	text-transform: uppercase; padding: 10px !important;font-weight: 600;
 }
 .new{

 	padding: 0px !important;
 }	 
 .new2{
 	padding-top: 10px !important;
 }
 .img-new-div {
 	z-index: 10;
 	position: relative;

 }
 .detail-div
 {
 	position: relative;
 	height:255px;
 	width: 255px;
 	margin-top: 15px;
 	z-index: 20;
 	color: var(--primary) !important;
 	font-weight: bold;
 	/*margin-left: 22% !important;*/
 	margin: auto;
 	text-align: center;
 	clear: both;
 	padding-bottom: 10px;
 	/*background-image: url(../img/parking2.png);*/
 	background-repeat: no-repeat;
 	background-size: cover;
 	padding-top: 50px;
 	margin-bottom : 25px;
 }

 .sosa-text {
 	text-align: center;
 	color: var(--primary);
 	font-size: 14px;
 	margin-top: 52px;
 	letter-spacing: 0px;
 	font-weight: 600;
 	text-align: center;
 	/*line-height: 0px;*/
 	word-wrap: break-word;
 	width: 240px;
 	margin-bottom: 0px;
 	padding-left: 12px;
 }


 .sosa-name {
 	color: var(--primary);
 	font-size: 12px;
 	display: inline-block;
 	position: relative;
 	top: 0px;
 	letter-spacing: 0px;
 	font-weight: 600;
 	text-align: center;
 	/*line-height: 0px;*/
 }
 .type-img{
 	height: 30px !important;
 	width: 30px !important;
 }
 .main-img{
 	width: 200px !important;
 	height: 200px !important;
 }
 .parking-name{
 	font-weight: 600;
 	font-size: 35px;
 	/*margin-top: 45px;*/
 	margin-bottom: 0px;
 }

@page {
  size: A4;
  margin-bottom: 20mm !important;
}

@media print {
	.img-new-div {
 	z-index: 10;
 	position: relative;
 }
 .type-img{
 	height: 40px !important;
 	width: 40px !important;
 }
 .main-img{
 	width: 200px !important;
 	height: 200px !important;
 }
 .parking-name{
 	font-weight: 600;
 	font-size: 35px;
 	/*margin-top: 45px;*/
 	margin-bottom: 0px;	
 }
}
</style>


</head>

  <body onload="window.print()">
	<div class="row printable bg-white">
			<?php
			$qry ="";
			$parking_id = (int)$parking_id;
			$society_parking_id = (int)$society_parking_id;
			if(isset($parking_id) && $parking_id!=""){
				$qry = " and parking_master.parking_id='$parking_id'";
			}
			if(isset($society_parking_id ) && $society_parking_id !=""){
				$qry .= " and parking_master.society_parking_id='$society_parking_id'";
			}
			$parking_master_qry=$d->select("parking_master,unit_master,block_master,floors_master,society_parking_master ,users_master","users_master.unit_id=unit_master.unit_id AND users_master.member_status=0 AND users_master.delete_status=0 AND society_parking_master.society_parking_id=parking_master.society_parking_id AND unit_master.unit_id=parking_master.unit_id AND block_master.block_id=parking_master.block_id AND 
				floors_master.floor_id=parking_master.floor_id AND 
				parking_master.society_id='$society_id' AND 
				parking_master.parking_status!=0  $qry  order by parking_type asc ","");



			$i=0;
			while($parking_master_data=mysqli_fetch_array($parking_master_qry)){
						$socieaty_parking_name = html_entity_decode($parking_master_data['socieaty_parking_name']);
						$society_name = html_entity_decode($_COOKIE['society_name']);
 
			 ?>


				<div class="col-md-4  text-center"  style="margin-bottom: 13px !important;">
						
					<div  class="detail-div"  style="background-image: url(../img/<?php if($parking_master_data['parking_type']==0) { echo 'Car_Sticker.png'; } else { echo 'Bike_Sticker.png'; }?>);">
							<div class="sosa-text"><?php custom_echo($parking_master_data['user_first_name'],13);  ?> <?php    custom_echo($socieaty_parking_name,50);   ?></div>
							<p class="parking-name">
							<?php 
							$parking_name = (strlen($parking_master_data['parking_name']) > 6) ? substr($parking_master_data['parking_name'],0,6) : $parking_master_data['parking_name'];
							echo   $parking_name; ?>
							</p>
						<div style="width: 163px;margin: auto;">
							<h5 class="sosa-name"><?php 
							echo   custom_echo($society_name,50);   ?></h5>
						</div>

							
					</div>
				</div>
		 

 	<?php } ?> 
</div>

</body>
</html>
<?php } else {  ?>

<!DOCTYPE html>
<html>
<head>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">

	<title>Parking QR Code Sticker Print</title>
	<style type="text/css">
		body {
			/*background-color: #d7d6d3;*/
			font-family: 'Open Sans', sans-serif;
		}
		h2 {
			font-size: 12px;
			margin: 5px 0;
		}
		h3 {
			font-size: 10px;
			margin: 2.5px 0;
			font-weight: 300;
		}
		.addressSoc {
			font-size: 7px;
			font-weight: bold;
		}
		.col-md-4 {
			-ms-flex: 0 0 33.333333%;
			flex: 0 0 33.333333%;
			/*margin: 5px !important;*/
			width:33.33% !important;
			float: left;
			display: inline-block;
			padding: 0px !important; 
		}

 .main-div{
 	border-radius: 50% !important; background-color: var(--primary) !important; color:#fff !important; padding-top:10px  !important; padding-left: 5px  !important; padding-right:5px  !important;  float: left;
 }
 .img-div{
 	width:110px !important; 
 	height: 110px !important; 
 	border-radius: 50% !important;
 	background-color: #fff !important;
 	opacity: 1 !important;
 	box-shadow: 3px 3px 20px
 	rgba(0, 0, 0, 0.5);
 	border: 2px solid
 	rgba(255, 255, 255, 1);
 }
 .sosa-name-div{
 	text-transform: uppercase; font-weight: 600 !important; padding: 10px !important;
 }
 .block-info{
 	text-transform: uppercase;
 	overflow-wrap: break-word;
 	min-height: 24px !important;
 	min-width: 60px !important;
 	max-height: 20px !important;
 	max-width: 90px !important;
 	background-color:#fff !important;
 	font-weight: 600 !important;
 	line-height: 25px !important;
 	color:var(--primary) !important;
 	box-shadow: 3px 3px 20px
 	rgba(0, 0, 0, 0.5);
 	border-top: 2px solid
 	rgba(255, 255, 255, 1);
 }
 .parking-permit{
 	text-transform: uppercase; padding: 10px !important;font-weight: 600;
 }
 .new{

 	padding: 0px !important;
 }	 
 .new2{
 	padding-top: 10px !important;
 }
 .img-new-div {
 	z-index: 10;
 	position: relative;

 }
 .detail-div
 {
 	position: relative;
 	height:275px;
 	width: 255px;
 	margin-top: 15px;
 	z-index: 20;
 	color: var(--primary) !important;
 	font-weight: bold;
 	/*margin-left: 22% !important;*/
 	margin: auto;
 	text-align: center;
 	clear: both;
 	padding-bottom: 10px;
 	/*background-image: url(../img/parking2.png);*/
 	background-repeat: no-repeat;
 	background-size: cover;
 	padding-top: 5px;
 	margin-bottom : 25px;
 	border: 1px solid var(--primary);
 	border-radius: 15px;
	overflow: hidden;
 }

 .sosa-text {
 	text-align: center;
 	color: var(--primary);
 	font-size: 14px;
 	margin-top: 52px;
 	letter-spacing: 0px;
 	font-weight: 600;
 	text-align: center;
 	/*line-height: 0px;*/
 	word-wrap: break-word;
 	width: 240px;
 	margin-bottom: 0px;
 	padding-left: 12px;
 }


 .sosa-name {
 	color: var(--primary);
 	font-size: 12px;
 	display: inline-block;
 	position: relative;
 	top: -30px;
 	letter-spacing: 0px;
 	font-weight: 600;
 	text-align: center;
 	/*line-height: 0px;*/
 }
 .type-img{
 	height: 30px !important;
 	width: 30px !important;
 }
 .main-img{
 	width: 200px !important;
 	height: 200px !important;
 }
 .parking-name{
 	font-weight: 600;
 	font-size: 35px;
 	/*margin-top: 45px;*/
 	margin-bottom: 0px;
 }

@page {
  size: A4;
  margin-bottom: 20mm !important;
}

@media print {
	.img-new-div {
 	z-index: 10;
 	position: relative;
 }
 .type-img{
 	height: 40px !important;
 	width: 40px !important;
 }
 .main-img{
 	width: 200px !important;
 	height: 200px !important;
 }
 .parking-name{
 	font-weight: 600;
 	font-size: 35px;
 	/*margin-top: 45px;*/
 	margin-bottom: 0px;	
 }
}
</style>


</head>
<!-- <body> -->
<body onload="window.print()">
	<div class="row printable">
			<?php
			$qry ="";
			$parking_id = (int)$parking_id;
			$society_parking_id = (int)$society_parking_id;
			if(isset($parking_id) && $parking_id!=""){
				$qry = " and parking_master.parking_id='$parking_id'";
			}
			if(isset($society_parking_id ) && $society_parking_id !=""){
				$qry .= " and parking_master.society_parking_id='$society_parking_id'";
			}
			$parking_master_qry=$d->select("parking_master,unit_master,block_master,floors_master,society_parking_master ","society_parking_master.society_parking_id=parking_master.society_parking_id AND unit_master.unit_id=parking_master.unit_id AND block_master.block_id=parking_master.block_id AND 
				floors_master.floor_id=parking_master.floor_id AND 
				parking_master.society_id='$society_id' AND 
				parking_master.parking_status!=0  $qry  order by parking_type asc ","");



			$i=0;
			while($parking_master_data=mysqli_fetch_array($parking_master_qry)){
						$socieaty_parking_name = html_entity_decode($parking_master_data['socieaty_parking_name']);
						$society_name = html_entity_decode($_COOKIE['society_name']);
 
			 ?>


				<div class="col-md-4  text-center"  style="margin-bottom: 13px !important;">
						
					<div  class="detail-div" >
							<?php $qr_size          = "255x255";
						$qr_content       = "$parking_master_data[parking_id]";
						$qr_correction    = strtoupper('H');
						$qr_encoding      = 'UTF-8';

	    				//form google chart api link
						$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
						?>
						<img src="<?php echo $qrImageUrl; ?>" alt="">
						<div style="margin: auto;">
							<h5 class="sosa-name"><?php 
							$parking_name = (strlen($parking_master_data['parking_name']) > 6) ? substr($parking_master_data['parking_name'],0,6) : $parking_master_data['parking_name'];
							echo   $parking_name;   ?></h5>
						</div>

							
					</div>
				</div>
		 

 	<?php } ?> 
</div>

</body>
<script type="text/javascript">
        Android.print('print');
  </script>
</html>

<?php } ?>