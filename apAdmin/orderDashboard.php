<?php
function validateDate($date, $format = 'Y-m-d')
{
    $da = DateTime::createFromFormat($format, $date);
    return $da && $da->format($format) === $date;
}
$level_str = "";
if(isset($_GET['filter_data']) && $_GET['filter_data'] == "filter_data" && isset($_GET['UId']) && $_GET['UId'] != 0 && !empty($_GET['UId']) && ctype_digit($_GET['UId']))
{
    $user_id = $_GET['UId'];
    // $get_level = $d->selectRow("elm.level_id","users_master AS um JOIN employee_level_master AS elm ON elm.parent_level_id = um.level_id","um.user_id = '$user_id'");
    // $all = [];
    // while($fetch = $get_level->fetch_assoc())
    // {
        // $all[] = $fetch['level_id'];
    // }
    // $level_str = implode(",",$all);
}
elseif(isset($_GET['filter_data']) && $_GET['filter_data'] == "filter_data" && isset($_GET['UId']) && $_GET['UId'] != 0 && !empty($_GET['UId']) && !ctype_digit($_GET['UId']))
{
    $_SESSION['msg1'] = "Invalid User Id!";
?>
    <script>
        window.location = "orderDashboard";
    </script>
<?php
    exit;
}

/*if($level_str == "")
{
    $level_str = 0;
}*/

if(isset($_GET['filter_data']) && $_GET['filter_data'] == "filter_data" && isset($_GET['filter_date']) && !empty($_GET['filter_date']) && validateDate($_GET['filter_date']))
{
    $filter_date = $_GET['filter_date'];
}
elseif(isset($_GET['filter_data']) && $_GET['filter_data'] == "filter_data" && isset($_GET['filter_date']) && !empty($_GET['filter_date']))
{
    if(!var_dump(validateDate($_GET['filter_date'])))
    {
        $_SESSION['msg1'] = "Invalid Date!";
    ?>
        <script>
            window.location = "orderDashboard";
        </script>
    <?php
    exit;
    }
}
$appendQuery = "";
$appendQuery2 = "";
$total_orders = "";
$total_sales = "";
$total_quantity = "";
$total_visits = "";
$total_visit_duration = "";
$productivity = "";
if(!isset($filter_date))
{
    $filter_date = date('Y-m-d');
}
$appendQuery = " AND DATE_FORMAT(rom.order_date,'%Y-%m-%d') = '$filter_date'";
$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') = '$filter_date'";
if(isset($user_id) && !empty($user_id) && $user_id != 0)
{
    $totalAmount = $d->sum_data("order_total_amount","retailer_order_master AS rom JOIN users_master AS um ON um.user_id = rom.order_by_user_id","rom.order_status = '1' AND rom.society_id = '$society_id' $appendQuery");
    $totalDuration = $d->sum_data("retailer_visit_duration","retailer_daily_visit_timeline_master AS rdvtm JOIN users_master AS um ON um.user_id = rdvtm.retailer_visit_by_id","1=1 ".$appendQuery2);
    $totalQuantity = $d->sum_data("total_order_product_qty","retailer_order_master AS rom JOIN users_master AS um ON um.user_id = rom.order_by_user_id","rom.order_status = 1 AND rom.society_id = '$society_id' $appendQuery");
    $totalVisits = $d->count_data_direct("retailer_daily_visit_timeline_id","retailer_daily_visit_timeline_master AS rdvtm JOIN users_master AS um ON um.user_id = rdvtm.retailer_visit_by_id","rdvtm.retailer_visit_start_datetime IS NOT NULL AND rdvtm.order_taken != 0 $appendQuery2");
    $totalOrders = $d->count_data_direct("retailer_order_id","retailer_order_master AS rom JOIN users_master AS um ON um.user_id = rom.order_by_user_id","1=1 ".$appendQuery);
}
else
{
    $totalAmount = $d->sum_data("order_total_amount","retailer_order_master AS rom","rom.order_status = '1' AND rom.society_id = '$society_id' $appendQuery");
    $totalDuration = $d->sum_data("retailer_visit_duration","retailer_daily_visit_timeline_master","1=1" . $appendQuery2);
    $totalQuantity = $d->sum_data("total_order_product_qty","retailer_order_master AS rom","rom.order_status = 1 AND rom.society_id = '$society_id' $appendQuery");
    $totalVisits = $d->count_data_direct("retailer_daily_visit_timeline_id","retailer_daily_visit_timeline_master","retailer_visit_start_datetime IS NOT NULL AND order_taken != 0 $appendQuery2");
    $totalOrders = $d->count_data_direct("retailer_order_id","retailer_order_master AS rom","1=1".$appendQuery);
}

$total_orders = $totalOrders.'';
if ($totalOrders > 0 && $totalVisits > 0)
{
    $totalProductivity = ($totalOrders * 100) / $totalVisits;
}
else
{
    $totalProductivity = 0;
}

$totalAmountData = mysqli_fetch_array($totalAmount);
if ($totalAmountData['SUM(order_total_amount)'] != '')
{
    $total_sales = $totalAmountData['SUM(order_total_amount)'].'';
}
else
{
    $total_sales = "0";
}

$totalQuantityData = mysqli_fetch_array($totalQuantity);
if ($totalQuantityData['SUM(total_order_product_qty)'] != '')
{
    $total_quantity = $totalQuantityData['SUM(total_order_product_qty)'].'';
}
else
{
    $total_quantity = "0";
}

$totalDurationData = mysqli_fetch_array($totalDuration);
$totalSecounds = $totalDurationData['SUM(retailer_visit_duration)'].'';
if($totalSecounds == "" || empty($totalSecounds))
{
    $totalVisitDuration = gmdate("H:i:s", 0);
}
else
{
    $totalVisitDuration = gmdate("H:i:s", $totalSecounds);
}
$total_visits = $totalVisits.'';
$total_visit_duration = $totalVisitDuration.'';
$productivity = number_format($totalProductivity,2,'.','').'';

$dt = date("Y-m-d");
$fd = date("Y-m-01", strtotime($dt));
$ld = date("Y-m-t", strtotime($dt));
$total_days = date('t');
$total_days_arr = [];
$total_days_str = "";
$total_amount_mon_arr = [];
$total_amount_mon_str = "";
$total_qty_mon_arr = [];
$total_qty_mon_str = "";
$mwc = [];
$get_mcq = $d->selectRow("DATE_FORMAT(rom.order_date,'%e') AS date,SUM(rom.order_total_amount) AS total_amount,SUM(rom.total_order_product_qty) AS total_qty","retailer_order_master AS rom","rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59' AND rom.order_status = 1","GROUP BY DATE(rom.order_date)");
while($mcd = $get_mcq->fetch_assoc())
{
    $mwc[$mcd['date']] = $mcd;
}
for($i = 1;$i <= $total_days;$i++ )
{
    $total_days_arr[] = $i;
    if(!isset($mwc[$i]))
    {
        $mwc[$i]['date'] = $i;
        $mwc[$i]['total_amount'] = 0;
        $mwc[$i]['total_qty'] = 0;
        $total_amount_mon_arr[] = 0;
        $total_qty_mon_arr[] = 0;
    }
    else
    {
        $total_amount_mon_arr[] = $mwc[$i]['total_amount'];
        $total_qty_mon_arr[] = $mwc[$i]['total_qty'];

    }
}
$total_days_str = "'" .  implode("','",$total_days_arr) . "'";
$total_amount_mon_str = implode(",",$total_amount_mon_arr);
$total_qty_mon_str = implode(",",$total_qty_mon_arr);

$first_date_year = date("Y")."-01-01 00:00:01";
$last_date_year = date("Y")."-12-31 23:59:59";
$total_amount_year_arr = [];
$total_qty_year_arr = [];
$total_amount_year_str = "";
$total_qty_year_str = "";
$ywc = [];
$append3 = "";

$get_mcq = $d->selectRow("DATE_FORMAT(rom.order_date,'%c') AS month,SUM(rom.order_total_amount) AS total_amount,SUM(rom.total_order_product_qty) AS total_qty","retailer_order_master AS rom","rom.order_date BETWEEN '$first_date_year' AND '$last_date_year' AND rom.order_status = 1","GROUP BY MONTH(rom.order_date)");
while($ycd = $get_mcq->fetch_assoc())
{
    $ywc[$ycd['month']] = $ycd;
}
for($i = 1;$i <= 12;$i++)
{
    if(!isset($ywc[$i]))
    {
        $ywc[$i]['month'] = $i;
        $ywc[$i]['total_amount'] = 0;
        $ywc[$i]['total_qty'] = 0;
        $total_amount_year_arr[] = 0;
        $total_qty_year_arr[] = 0;
    }
    else
    {
        $total_amount_year_arr[] = $ywc[$i]['total_amount'];
        $total_qty_year_arr[] = $ywc[$i]['total_qty'];

    }
}
$total_amount_year_str = implode(",",$total_amount_year_arr);
$total_qty_year_str = implode(",",$total_qty_year_arr);
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-4 col-6">
                <h4 class="page-title">Statistics & Reports</h4>
            </div>
            <div class="col-sm-8 col-6">
                <form>
                    <input type="hidden" name="filter_data" value="filter_data">
                    <div class="row">
                        <div class="col-md-6 col-6 form-group">
                            <input type="hidden" id="UId" name="UId" >
                            <!-- <select class="form-control single-select-new" id="UId" name="UId" onchange="this.form.submit()">
                                <option value="">-- Select Employee--</option>
                                <?php
                                $qu = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","um.delete_status = 0 AND um.active_status = 0");
                                while ($ud = $qu->fetch_assoc())
                                {
                                ?>
                                <option <?php if(isset($user_id) && $user_id == $ud['user_id']) { echo 'selected'; } ?> value="<?php echo $ud['user_id']; ?>"><?php echo $ud['user_full_name'] . " (" . $ud['block_name'] . " - " . $ud['floor_name'] . ")"; ?></option>
                                <?php
                                }
                                ?>
                            </select> -->
                        </div>
                        <div class="col-md-6 col-6 form-group">
                            <input type="text" class="form-control default-datepicker-onchange" autocomplete="off" id="filter_date" name="filter_date" value="<?php if(isset($_GET['filter_date']) && $_GET['filter_date'] != ''){ echo $filter_date= $_GET['filter_date'];}else{ echo $filter_date= date('Y-m-d');} ?>">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-3">
                        <div class="card gradient-ibiza">
                            <div class="card-body">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="text-white">Total Orders</p>
                                        <h4 class="text-white line-height-5" id="total_orders_count"><?php echo $total_orders; ?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-3">
                        <div class="card gradient-branding">
                            <div class="card-body">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="text-white">Order Revenue</p>
                                        <h4 class="text-white line-height-5" id="total_sales_count"><?php echo $total_sales; ?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-12 col-xl-3">
                        <div class="card gradient-deepblue">
                            <div class="card-body">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="text-white">Order quantities</p>
                                        <h4 class="text-white line-height-5" id="total_quantity_count"><?php echo $total_quantity; ?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-3">
                        <div class="card gradient-ibiza">
                            <div class="card-body">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="text-white">Productivity</p>
                                        <h4 class="text-white line-height-5" id="productivity_count"><?php
                                        if($productivity > 100)
                                        {
                                            echo "100.00";
                                        }
                                        elseif($productivity > "100")
                                        {
                                            echo "100.00";
                                        }
                                        else
                                        {
                                            echo $productivity;
                                        }
                                        ?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-4">
                                <h6>Order Summary</h6>
                            </div>
                            <div class="col-8">
                                <ul class="nav nav-tabs-primary float-right">
                                    <li class="nav-item">
                                        <a class="nav-link active show" data-toggle="tab" href="#tabe-1">Today</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabe-2">This Week</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabe-3">This Month</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="" style="max-height: 400px;overflow:scroll;">
                        <div class="tab-content">
                            <div id="tabe-1" class="container tab-pane  active show">
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Order Values</th>
                                                <th>Qty</th>
                                                <th>No. of Orders</th>
                                            </tr>
                                        </thead>
                                        <tbody id="ostb">
                                            <?php
                                            $today_date = date("Y-m-d");
                                            $append4 = "";
                                            // if(isset($user_id) && !empty($user_id) && $user_id != 0)
                                            // {
                                                // $append4 .= " AND um.level_id IN ($level_str)";
                                            // }
                                            $get_product = $d->selectRow("SUM(rom.order_total_amount) AS total_amount,um.user_full_name,SUM(rom.total_order_product_qty) AS total_qty,COUNT(rom.order_by_user_id) AS total_orders","retailer_order_master AS rom JOIN users_master AS um ON rom.order_by_user_id = um.user_id","DATE(rom.order_date) = '$today_date'","GROUP BY rom.order_by_user_id ORDER BY total_amount DESC");
                                            $order_value_today = [];
                                            $user_name_today = [];
                                            $order_value_today_str = "";
                                            $user_name_today_str = "";
                                            while($row = $get_product->fetch_assoc())
                                            {
                                                $order_value_today[] = $row['total_amount'];
                                                $user_name_today[] = $row['user_full_name'];
                                            ?>
                                            <tr>
                                                <td><?php echo $row['user_full_name']; ?></td>
                                                <td class="ostat"><?php echo $row['total_amount']; ?></td>
                                                <td class="ostqt"><?php echo $row['total_qty']; ?></td>
                                                <td class="ostot"><?php echo $row['total_orders']; ?></td>
                                            </tr>
                                            <?php
                                            }
                                            $order_value_today_str = implode(",",$order_value_today);
                                            $user_name_today_str = "'" .  implode("','",$user_name_today) . "'";
                                            if(mysqli_num_rows($get_product) > 0)
                                            {
                                            ?>
                                            <tr class="font-weight-bold">
                                                <td>Total</td>
                                                <td id="ostat"></td>
                                                <td id="ostqt"></td>
                                                <td id="ostot"></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="tabe-2" class="container tab-pane fade">
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Order Values</th>
                                                <th>Qty</th>
                                                <th>No. of Orders</th>
                                            </tr>
                                        </thead>
                                        <tbody id="oswb">
                                            <?php
                                            $dt_min = new DateTime("last saturday");
                                            $dt_min->modify('+2 day');
                                            $dt_max = clone($dt_min);
                                            $dt_max->modify('+6 days');
                                            $start_date = $dt_min->format('Y-m-d');
                                            $end_date = $dt_max->format('Y-m-d');
                                            $get_product = $d->selectRow("SUM(rom.order_total_amount) AS total_amount,um.user_full_name,SUM(rom.total_order_product_qty) AS total_qty,COUNT(rom.order_by_user_id) AS total_orders","retailer_order_master AS rom JOIN users_master AS um ON rom.order_by_user_id = um.user_id","rom.order_date BETWEEN '$start_date 00:00:01' AND '$end_date 23:59:59'","GROUP BY rom.order_by_user_id ORDER BY total_amount DESC");
                                            while($row = $get_product->fetch_assoc())
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $row['user_full_name']; ?></td>
                                                <td class="oswat"><?php echo $row['total_amount']; ?></td>
                                                <td class="oswqt"><?php echo $row['total_qty']; ?></td>
                                                <td class="oswot"><?php echo $row['total_orders']; ?></td>
                                            </tr>
                                            <?php
                                            }
                                            if(mysqli_num_rows($get_product) > 0)
                                            {
                                            ?>
                                            <tr class="font-weight-bold">
                                                <td>Total</td>
                                                <td id="oswat"></td>
                                                <td id="oswqt"></td>
                                                <td id="oswot"></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="tabe-3" class="container tab-pane fade">
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Order Values</th>
                                                <th>Qty</th>
                                                <th>No. of Orders</th>
                                            </tr>
                                        </thead>
                                        <tbody id="osmb">
                                            <?php
                                            $get_product = $d->selectRow("SUM(rom.order_total_amount) AS total_amount,um.user_full_name,SUM(rom.total_order_product_qty) AS total_qty,COUNT(rom.order_by_user_id) AS total_orders","retailer_order_master AS rom JOIN users_master AS um ON rom.order_by_user_id = um.user_id","rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59'","GROUP BY rom.order_by_user_id ORDER BY total_amount DESC");
                                            $order_value_month = [];
                                            $user_name_month = [];
                                            $order_value_month_str = "";
                                            $user_name_month_str = "";
                                            while($row = $get_product->fetch_assoc())
                                            {
                                                $order_value_month[] = $row['total_amount'];
                                                $user_name_month[] = $row['user_full_name'];
                                            ?>
                                            <tr>
                                                <td><?php echo $row['user_full_name']; ?></td>
                                                <td class="osmat"><?php echo $row['total_amount']; ?></td>
                                                <td class="osmqt"><?php echo $row['total_qty']; ?></td>
                                                <td class="osmot"><?php echo $row['total_orders']; ?></td>
                                            </tr>
                                            <?php
                                            }
                                            $order_value_month_str = implode(",",$order_value_month);
                                            $user_name_month_str = "'" .  implode("','",$user_name_month) . "'";
                                            if(mysqli_num_rows($get_product) > 0)
                                            {
                                            ?>
                                            <tr class="font-weight-bold">
                                                <td>Total</td>
                                                <td id="osmat"></td>
                                                <td id="osmqt"></td>
                                                <td id="osmot"></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h6>Orders Summary Chart<span style="font-size: 12px;"> -This Month</span></h6>
                    </div>
                    <div class="card-body">
                        <div id="ordeSumeryChart"></div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-4">
                                <h6>Product Summary</h6>
                            </div>
                            <div class="col-8">
                                <ul class="nav nav-tabs-primary float-right">
                                    <li class="nav-item">
                                        <a class="nav-link active show" data-toggle="tab" href="#pso-1">Today</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#pso-2">This Week</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#pso-3">This Month</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="" style="max-height: 243px;overflow:scroll;">
                        <div class="tab-content">
                            <div id="pso-1" class="container tab-pane active show">
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Order Values</th>
                                                <th>Qty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_product = $d->selectRow("pcm.category_name,SUM(ropm.product_total_price) AS total_amount,SUM(ropm.product_quantity) AS total_qty","retailer_order_product_master AS ropm JOIN retailer_order_master AS rom ON rom.retailer_order_id = ropm.retailer_order_id JOIN retailer_product_master AS rpm ON rpm.product_id = ropm.product_id JOIN product_category_master AS pcm ON pcm.product_category_id = rpm.product_category_id","DATE(rom.order_date) = '$today_date'","GROUP BY pcm.product_category_id");
                                            while($row = $get_product->fetch_assoc())
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $row['category_name']; ?></td>
                                                <td class="pstta"><?php echo $row['total_amount']; ?></td>
                                                <td class="psttq"><?php echo $row['total_qty']; ?></td>
                                            </tr>
                                            <?php
                                            }
                                            if(mysqli_num_rows($get_product) > 0)
                                            {
                                            ?>
                                            <tr class="font-weight-bold">
                                                <td>Total</td>
                                                <td id="pstta"></td>
                                                <td id="psttq"></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="pso-2" class="container tab-pane fade">
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Order Values</th>
                                                <th>Qty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_product = $d->selectRow("pcm.category_name,SUM(ropm.product_total_price) AS total_amount,SUM(ropm.product_quantity) AS total_qty","retailer_order_product_master AS ropm JOIN retailer_order_master AS rom ON rom.retailer_order_id = ropm.retailer_order_id JOIN retailer_product_master AS rpm ON rpm.product_id = ropm.product_id JOIN product_category_master AS pcm ON pcm.product_category_id = rpm.product_category_id","rom.order_date BETWEEN '$start_date 00:00:01' AND '$end_date 23:59:59'","GROUP BY pcm.product_category_id");
                                            while($row = $get_product->fetch_assoc())
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $row['category_name']; ?></td>
                                                <td class="pswta"><?php echo $row['total_amount']; ?></td>
                                                <td class="pswtq"><?php echo $row['total_qty']; ?></td>
                                            </tr>
                                            <?php
                                            }
                                            if(mysqli_num_rows($get_product) > 0)
                                            {
                                            ?>
                                            <tr class="font-weight-bold">
                                                <td>Total</td>
                                                <td id="pswta"></td>
                                                <td id="pswtq"></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="pso-3" class="container tab-pane fade">
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Order Values</th>
                                                <th>Qty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_product = $d->selectRow("pcm.category_name,SUM(ropm.product_total_price) AS total_amount,SUM(ropm.product_quantity) AS total_qty","retailer_order_product_master AS ropm JOIN retailer_order_master AS rom ON rom.retailer_order_id = ropm.retailer_order_id JOIN retailer_product_master AS rpm ON rpm.product_id = ropm.product_id JOIN product_category_master AS pcm ON pcm.product_category_id = rpm.product_category_id","rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59'","GROUP BY pcm.product_category_id");
                                            $category_value_month = [];
                                            $category_name_month = [];
                                            $category_value_month_str = "";
                                            $category_name_month_str = "";
                                            while($row = $get_product->fetch_assoc())
                                            {
                                                $category_value_month[] = $row['total_amount'];
                                                $category_name_month[] = $row['category_name'] . " (" . $row['total_amount'] . ") ";
                                            ?>
                                            <tr>
                                                <td><?php echo $row['category_name']; ?></td>
                                                <td class="psmta"><?php echo $row['total_amount']; ?></td>
                                                <td class="psmtq"><?php echo $row['total_qty']; ?></td>
                                            </tr>
                                            <?php
                                            }
                                            $category_value_month_str = implode(",",$category_value_month);
                                            $category_name_month_str = "'" .  implode("','",$category_name_month) . "'";
                                            if(mysqli_num_rows($get_product) > 0)
                                            {
                                            ?>
                                            <tr class="font-weight-bold">
                                                <td>Total</td>
                                                <td id="psmta"></td>
                                                <td id="psmtq"></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h6>Product Category Order Chart<span style="font-size: 12px;"> -This Month</span></h6>
                    </div>
                    <div class="card-body">
                        <div id="prdouctPieChart"></div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h6>Daily Orders In <?php echo date("F Y"); ?></h6>
                    </div>
                    <div class="card-body">
                        <div id="monthlyChart"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h6>Monthly Orders In <?php echo date("Y"); ?></h6>
                    </div>
                    <div class="card-body">
                        <div id="YearlyChart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugins/apexcharts-bundle/js/apexcharts.min.js"></script>
<script>
var MCO = {
    series: [
    {
        name: 'Order Amount',
        data: [<?php echo $total_amount_mon_str; ?>]
    },
    {
        name: 'Quantity',
        data: [<?php echo $total_qty_mon_str; ?>]
    }],
    chart:
    {
        type: 'bar',
        height: 350
    },
    plotOptions:
    {
        bar:
        {
            horizontal: false,
            columnWidth: '55%'
        },
    },
    dataLabels:
    {
        enabled: false
    },
    stroke:
    {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    xaxis:
    {
        categories: [<?php echo $total_days_str; ?>],
    },
    fill:
    {
        opacity: 1
    }
};
var chart = new ApexCharts(document.querySelector("#monthlyChart"), MCO);
chart.render();

var YCO = {
    series: [
    {
        name: 'Order Amount',
        data: [<?php echo $total_amount_year_str; ?>]
    },
    {
        name: 'Quantity',
        data: [<?php echo $total_qty_year_str; ?>]
    }],
    chart:
    {
        type: 'bar',
        height: 350
    },
    plotOptions:
    {
        bar:
        {
            horizontal: false,
            columnWidth: '55%'
        },
    },
    dataLabels:
    {
        enabled: false
    },
    stroke:
    {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    xaxis:
    {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    },
    yaxis:
    {
        title:
        {
            text: 'Monthly Sales <?php echo date("Y"); ?>'
        }
    },
    fill:
    {
        opacity: 1
    }
};
var chart = new ApexCharts(document.querySelector("#YearlyChart"), YCO);
chart.render();

var PPCO = {
    series: [<?php echo $category_value_month_str; ?>],
    chart:
    {
        width: 480,
        type: 'pie',
    },
    labels: [<?php echo $category_name_month_str; ?>],
    responsive: [
    {
        breakpoint: 480,
        options:
        {
            chart:
            {
                width: 200
            },
            legend:
            {
                position: 'bottom'
            }
        }
    }]
};
var chart = new ApexCharts(document.querySelector("#prdouctPieChart"), PPCO);
chart.render();

var OSCO = {
    series: [
    {
        name: 'Order Value',
        data: [<?php echo $order_value_month_str; ?>]
    }],
    chart:
    {
        height: 350,
        type: 'bar',
    },
    plotOptions:
    {
        bar:
        {
            borderRadius: 10,
            columnWidth: '50%',
        }
    },
    dataLabels:
    {
        enabled: false
    },
    stroke:
    {
        width: 2
    },
    grid:
    {
        row:
        {
            colors: ['#fff', '#f2f2f2']
        }
    },
    xaxis:
    {
        labels:
        {
            rotate: -45
        },
        categories: [<?php echo $user_name_month_str; ?>],
        tickPlacement: 'on'
    },
    fill:
    {
        type: 'gradient',
        gradient:
        {
            shade: 'light',
            type: "horizontal",
            shadeIntensity: 0.25,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 0.85,
            opacityTo: 0.85,
            stops: [50, 0, 100]
        },
    }
};
var chartosc = new ApexCharts(document.querySelector("#ordeSumeryChart"), OSCO);
chartosc.render();

$('.default-datepicker-onchange').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
}).on("changeDate", function (e)
{
    var csrf = "<?php echo $_SESSION['token']; ?>";
    var filter_date = e.format();
    var user_id = $('#user_id').val();
    $.ajax({
        url: "controller/orderProductController.php",
        cache: false,
        type: "POST",
        data: {user_id : user_id,getFilterData:"getFilterData",filter_date:filter_date,csrf : csrf},
        success: function(response)
        {
            response = JSON.parse(response);
            $('#total_orders_count').empty();
            $('#total_orders_count').text(response.total_orders);

            $('#total_sales_count').empty();
            $('#total_sales_count').text(response.total_sales);

            $('#total_quantity_count').empty();
            $('#total_quantity_count').text(response.total_quantity);

            $('#productivity_count').empty();
            if(response.productivity > 100)
            {
                $('#productivity_count').text("100.00");
            }
            else if(response.productivity > "100")
            {
                $('#productivity_count').text("100.00");
            }
            else
            {
                $('#productivity_count').text(response.productivity);
            }
        }
    });
});

$(document).ready(function()
{
    var sum = 0;
    $(".ostat").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#ostat').text(sum);

    var sum = 0;
    $(".ostqt").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#ostqt').text(sum);

    var sum = 0;
    $(".ostot").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#ostot').text(sum);

    var sum = 0;
    $(".oswat").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#oswat').text(sum);

    var sum = 0;
    $(".oswqt").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#oswqt').text(sum);

    var sum = 0;
    $(".oswot").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#oswot').text(sum);

    var sum = 0;
    $(".osmat").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#osmat').text(sum);

    var sum = 0;
    $(".osmqt").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#osmqt').text(sum);

    var sum = 0;
    $(".osmot").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#osmot').text(sum);

    var sum = 0;
    $(".pstta").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#pstta').text(sum);

    var sum = 0;
    $(".psttq").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#psttq').text(sum);

    var sum = 0;
    $(".pswta").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#pswta').text(sum);

    var sum = 0;
    $(".pswtq").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#pswtq').text(sum);

    var sum = 0;
    $(".psmta").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#psmta').text(sum);

    var sum = 0;
    $(".psmtq").each(function()
    {
        sum += parseFloat($(this).text());
    });
    $('#psmtq').text(sum);
});
</script>