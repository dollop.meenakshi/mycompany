<?php error_reporting(0);
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$month_year = (int) $_REQUEST['month'];
$dId = (int) $_REQUEST['dId'];
$bId = (int) $_REQUEST['bId'];
$laYear = (int) $_REQUEST['laYear'];
$uId = (int) $_REQUEST['uId'];
$initDate = date("$laYear-$month_year-1");
if(isset($uId))
{
  $user_shift = $d->selectRow('*', "users_master", "user_id='$uId'"); 
  $user_shift_data = mysqli_fetch_assoc($user_shift);
  $shift_time_id=$user_shift_data['shift_time_id'];
}
else
{
  $shift_time_id="";
}
?>
  <input type = "hidden" id="crntData" value="<?php echo date('Y-m-d'); ?>">
  <input type = "hidden" id="total_month_hours"  >
  <input type = "hidden" id="total_month_hour_spent"  >
  <input type = "hidden" id="dfltDate" value="<?php echo date('Y-m-1'); ?>">
  <input type = "hidden" id="user_attendance_id" value="<?php echo $uId; ?>">
  <input type = "hidden" id="society_id" value="<?php echo $society_id; ?>">
  <input type = "hidden" id="shift_time_id" value="<?php echo $shift_time_id; ?>">
  <div class="content-wrapper">
    <div class="container-fluid">
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-8">
          <h4 class="page-title">Month Wise Attendances</h4>
        </div>
        <div class="col-sm-3 col-1">
          <div class="btn-group float-sm-right">
            <a href="weeklyWorkReport?bId=<?php echo $bId; ?>&dId=<?php echo $dId; ?>&laYear=<?php echo $laYear; ?>&month_year=<?php echo $month_year; ?>&uId=<?php echo $uId; ?>"><button class="btn btn-sm btn-primary">View Weekly</button></a>
          </div>
        </div>
     </div>
     <form action=""  class="branchDeptFilterWithUser">
      <div class="row ">
          <?php include('selectBranchDeptEmpForFilter.php');?>
          <div class="col-md-3 form-group col-6" >
            <select class="form-control single-select"  name="month" id="month">
              <option value="">-- Select --</option> 
              <?php
              $selected = "";
              for ($m = 1; $m <= 12; $m++) {
                $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
                if (isset($_GET['month'])  && $_GET['month'] !="") {
                  if($_GET['month'] == $m)
                  {
                    $selected = "selected";
                  }else{
                    $selected = "";
                  }
                  
                } else {
                  $selected = "";
                  if($m==date('n'))
                  { 
                    $selected = "selected";
                  }
                  else
                  {
                    $selected = "";
                  }
                }
              ?>
              <option  <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $month; ?></option> 
            <?php }?>
            </select> 
          </div>
          <div class="col-md-2 form-group col-6" >
              <select name="laYear" class="form-control single-select ">
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $twoPreviousYear) {
                          echo 'selected';
                        } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $onePreviousYear) {
                          echo 'selected';
                        } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $currentYear) {
                          echo 'selected';
                        }else{ echo ''; } ?> <?php if (!isset($_GET['laYear'])) {
                                echo 'selected';
                              } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $nextYear) {
                          echo 'selected';
                        } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
              </select>
          </div>
            <div class="col-md-1 form-group mt-auto col-6" >
                <input class="btn btn-success btn-sm mb-1" type="submit" name="getReport" class="form-control" value="Get Data">
            </div>
      </div>
      </form>
      <?php if(isset($uId) && $uId>0){?>
     <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <div class="container-fluid">
                    <div class="row">
                      <div class="col-md-2"> <h4 class="page-title">Calendar</h4></div>
                      <div class="col-md-6">
                        <i class="fa fa-circle" aria-hidden="true" style="color:green"></i>
                        <label>Holiday</label>
                        <i class="fa fa-circle" aria-hidden="true" style="color:yellow"></i>
                        <label>Half Day Leave</label>
                        <i class="fa fa-circle" aria-hidden="true" style="color:red"></i>
                        <label>Leave</label>
                        <i class="fa fa-circle" aria-hidden="true" style="color:blue"></i>
                        <label>Present</label>
                        <i class="fa fa-circle" aria-hidden="true" style="color:#11cdef"></i>
                        <label>Week Off</label>
                      </div>
                      <div class="col-md-3 text-center">
                        <h5 id="showMonth"></h5>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-8 pt-2 pb-2">
                        <div class="row mt-3 mx-0 month_att_crds_row align-items-center">
                          <!-- progress bar -->
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2">
                            <div class="col">
                             <div class="progress-bar" id="pgBar" data-percent="00" data-duration="1000" >
                               <div class="progresBarContent font-12">
                                 <span class="text-primary d-block">
                                   Total Time
                                 </span>
                                 
                                 <h5 class=" d-block totalTime">
                                   
                                 </h5>  

                                 <span class="text-primary d-block">
                                   Monthly Hour Spend
                                 </span>                    
                                 
                                  <h5 class="d-block spendHours">
                                  
                                 </h5>

                               </div>
                             </div>
                            </div>
                          </div>
                          <!-- progress bar -->
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary">Working</p>
                                        <h5 class="my-1" id="TotalWorking">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/ic_calendar_working_days.png" data-src="assets/images/ic_calendar_working_days.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary"> Present</p>
                                        <h5 class="my-1" id="Present">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/ic_present.png" data-src="assets/images/ic_present.png">
                                      </div>
                                     
                                    </div>
                                   <div id="extraDaysVIew" style="font-size: 10px; "></div>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>
                        <div class="row mt-3 mx-0 month_att_crds_row align-items-center">
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2 showModalForSingleDAta" data-id="Absent" style="cursor: pointer;">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary " >Absent </p>
                                        <h5 class="my-1 Absent" id="Absent">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/ic_absent.png" data-src="assets/images/ic_absent.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                           <div class="col-md-6 col-lg-6 col-xl-4 px-2 showModalForSingleDAta" data-id="ExtraDays" style="cursor: pointer;">
                                <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary " >Extra Days</p>
                                        <h5 class="my-1 ExtraDays" id="extraDays">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/ic_present.png" data-src="assets/images/ic_present.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2 showModalForSingleDAta" data-id="Pending" style="cursor: pointer;">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary " >Pending Att.</p>
                                        <h5 class="my-1 Pending" id="Pending">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/pending_attendance.png" data-src="assets/images/pending_attendance.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2 showModalForSingleDAta" data-id="Rejected" style="cursor: pointer;">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary " >Rejected Att.</p>
                                        <h5 class="my-1 Rejected" id="rejected">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/ic_absent.png" data-src="assets/images/ic_absent.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          
                         
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2 showModalForSingleDAta" data-id="halfDay" style="cursor: pointer;">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary " > Half Day Leave</p>
                                        <h5 class="my-1 halfDay" id="halfday">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/half_day.png" data-src="assets/images/half_day.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2 showModalForSingleDAta" data-id="Leaves" style="cursor: pointer;">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary " >Full Day Leaves</p>
                                        <h5 class="my-1 Leaves" id="leaves">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/leave.png" data-src="assets/images/leave.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2 showModalForSingleDAta" data-id="lateIn" style="cursor: pointer;">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary " >Late In</p>
                                        <h5 class="my-1 lateIn" id="LateIn">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/ic_late_in.png" data-src="assets/images/ic_late_in.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2 showModalForSingleDAta"  data-id="EarlyOut" style="cursor: pointer;">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary "> Early Out</p>
                                        <h5 class="my-1 EarlyOut" id="earlyOut">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/ic_early_out.png" data-src="assets/images/ic_early_out.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                         
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2 showModalForSingleDAta" data-id="MissPunch"  style="cursor: pointer;" >
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary ">Punch Out Missing</p>
                                        <h5 class="my-1 MissPunch" id="PunchMiss">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/time_missing.png" data-src="assets/images/time_missing.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2  showModalForSingleDAta" data-id="holidays" style="cursor: pointer;">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary "  >Holidays</p>
                                        <h5 class="my-1 holidays" id="Holiday">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/holiday.png" data-src="assets/images/holiday.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                           <div class="col-md-6 col-lg-6 col-xl-4 px-2">
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary">Week Off</p>
                                        <h5 class="my-1" id="WeekOff">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/week.png" data-src="assets/images/week.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2">
                              <div class="col px-0">
                              <div class="card radius-10 border-1px mb-3">
                                <div class="p-2">
                                  <div class="d-flex align-items-center">
                                    <div>
                                      <p class="mb-0 text-primary "  style="cursor: pointer;">Extra Day Working Hours</p>
                                      <h5 class="my-1" id="extraHours">0</h5>
                                    </div>
                                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/ic_count_time.png" data-src="assets/images/ic_count_time.png">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-xl-4 px-2 " >
                              <div class="col px-0">
                                <div class="card radius-10 border-1px mb-3">
                                  <div class="p-2">
                                    <div class="d-flex align-items-center">
                                      <div>
                                        <p class="mb-0 text-primary">Remaining Hours</p>
                                        <h5 class="my-1 " id="total_remaining_hours_view">0</h5>
                                      </div>
                                      <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="assets/images/ic_count_time.png" data-src="assets/images/ic_count_time.png">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 pt-2 pb-2">
                        <?php
                        if(isset($uId) && $uId !="")
                        {
                          $user = $d->select("users_master", "society_id='$society_id' AND user_id = '$uId'");
                          $userdata = mysqli_fetch_assoc($user);
                        }?>
                          <!-- <h4 class="page-title"><?php  if(isset($userdata['user_full_name']) && $userdata['user_full_name'] !=""){ echo $userdata['user_full_name']; } ?></h4> -->
                          <!-- <h5>Total Working Hours: <span id="total_month_hours"></span> Hours</h5> -->
  
                          <div id='calendar-1' class="calendar"></div>
                          <div class="col-md-12 mt-1 ">
                            <div class="card">
                                <div id='todayData'></div>
                            </div>
                          </div>
                          <div class="col-md-12 multiPunchInTable" style="display: none;">
                            Multi Punch In Details
                              <table class="table table-bordered">
                                <thead>
                                  <th>Punch In</th>
                                  <th>Punch Out</th>
                                  <th>Hours</th>
                                </thead>
                                <tbody id="MulitPunchIn">
                                </tbody>
                              </table>
                          </div>
                      </div>
                    </div>

                  </div>
                  <!-- End Breadcrumb-->
                
                </div>
              </div>
            </div>
        </div>
      
      <?php }else{ ?>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                Please Select Department, Branch & Employee
              </div>
            </div>
          </div>
        </div>
        <?php }?>
        
      </div>
    </div>
  </div>
<div class="modal fade" id="showSIngleData">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Leave Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="" style="align-content: center;">
          <div class="card-body" id="">
          <div class="table-responsive">
            <table id="" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>                        
                    </tr>
                </thead>
                <tbody class="ShowDateDetails">
                </tbody>
            </table>
          </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.full.js'></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.full.min.js'></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.js'></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.min.js'></script>
<link rel="stylesheet" href="assets/plugins/jQuery-plugin-progressbar-master/jQuery-plugin-progressbar.css">
<script src='assets/plugins/jQuery-plugin-progressbar-master/jQuery-plugin-progressbar.js'></script>


<script type="text/javascript">

 <?php if(isset($_REQUEST['month']) && $_REQUEST['month'] !=""){ ?>
  month = '<?php echo $_REQUEST['month']; ?>';
  <?php } else{?>
    month = $('#month').val();

  <?php } ?>
 
  year = '<?php echo $_REQUEST['laYear']; ?>';
  eventArray = [];
  WholeArray = [];
  modaljasonObAr = [];

getMonthData(month,year)
 function getMonthData(month,year)
{
  shift_time_id = $('#shift_time_id').val();
  $.ajax({
      url: "../residentApiNew/MonthAttendanceData.php",
      cache: false,
      type: "POST",
      async:false,
      data: {user_id :$('#user_attendance_id').val(),
            getMonthlyAttendanceHistoryNew:"getMonthlyAttendanceHistoryNew",
            //user_id: '<?php echo $uId; ?>',
            user_id: $('#uId').val(),
            month: month,
            year: year,
            shift_time_id:shift_time_id,
            society_id:'<?php echo $society_id; ?>'
        },
      success: function(response){
       
        if(response.monthly_history.length >0)
        {
          total_month_hours = parseInt(response.total_month_hours);
          total_month_hour_spent = parseInt(response.total_month_hour_spent);
          $('#total_month_hours').val(response.total_month_hours);
          $('#showMonth').text(response.month);
          $('#total_month_hour_spent').val(response.total_month_hour_spent);
          pgdata = (total_month_hour_spent*100)/total_month_hours;
         
          if(pgdata.toFixed(2)>100)
          {
            pgdata = 100;
          }
          else
          {
            pgdata= pgdata.toFixed(2)
          }
          $('.totalTime').text(response.total_month_hours_view);
          $('.spendHours').text(response.total_month_hour_spent_view);
          $('#pgBar').attr('data-percent',pgdata);
          
          $.each(response.monthly_history, function( index, value ) {
            if(value.holiday==true)
            {
              jasonOb = {
                            "name":"holiday",
                            "date":value.date,
                        }
              eventArray.push(jasonOb);
            }
            if(value.week_off==true)
            {
              jasonOb = {
                            "name":"weekoff",
                            "date":value.date,
                        }
              eventArray.push(jasonOb);
            }
            if(value.attendance_declined==true)
            {
              jasonOb = {
                            "name":"rejected",
                            "date":value.date,
                        }
              eventArray.push(jasonOb);
            }
            if(value.attendnace_pending==true)
            {
              jasonOb = {
                            "name":"pending",
                            "date":value.date,
                        }
              eventArray.push(jasonOb);
            }
            if(value.leave==true && value.present !=true)
            {
              jasonOb = {
                            "name":"leave",
                            "date":value.date,
                            "type":value.leave_type_name,
                        }
              eventArray.push(jasonOb);
            }
            if(value.present==true)
            {
              jasonOb = {
                            "name":"present",
                            "date":value.date,
                        }
              eventArray.push(jasonOb);
            }
            if(value.half_day==true)
            {
              
              jasonOb = {
                            "name":"halfday",
                            "date":value.date,
                            "type":value.leave_type_name,
                        }
              eventArray.push(jasonOb);
            }
            
          });
        }
        $('#TotalWorking').text(response.total_working_days);
        $('#Present').text(response.total_present);
       if(response.total_extra_days_view>0){

         $('#extraDaysVIew').text("(Including "+response.total_extra_days_view +" Days Extra)");
       }
        $('#total_remaining_hours_view').text(response.total_remaining_hours_view);
        $('#Pending').text(response.total_pending_attendance);
        $('#rejected').text(response.total_rejected_attendance);
        $('#Absent').text(response.total_absent);
        $('#halfday').text(response.total_half_day);
        $('#extraDays').text(response.total_extra_days);
        if(response.has_extra_hours !=false){
          $('#extraHours').text(response.total_extra_hours_view);
        }
        else
        {
          $('#extraHours').text(0);
        }
        $('#leaves').text(response.total_leave);
        $('#LateIn').text(response.late_punch_in);
        $('#earlyOut').text(response.early_punch_out);
        $('#WeekOff').text(response.total_week_off);
        $('#PunchMiss').text(response.total_punch_out_missing);
        $('#Holiday').text(response.total_holidays);
      },
      
    });
}
shift_time_id = $('#shift_time_id').val();
eventArray2 =[];
 $.ajax({
  url: "../residentApiNew/MonthAttendanceData.php",
      cache: false,
      type: "POST",
      async:false,
      data: {user_id :$('#user_attendance_id').val(),
            getMonthlyAttendanceHistoryNew:"getMonthlyAttendanceHistoryNew",
            user_id: $('#uId').val(),
            month: month,
            
            year: year,
            shift_time_id:shift_time_id,
            society_id:'<?php echo $society_id; ?>'
        },
      success: function(response){
       
        if(response.monthly_history.length >0)
        {
          WholeArray.push(response.monthly_history);
          $('#showMonth').text(response.month);
          if(response.total_extra_days_view>0){

            $('#extraDaysVIew').text("(Including "+response.total_extra_days_view +" Days Extra)");
          }

          $('#total_month_hours').val(response.total_month_hours);
          $('#total_month_hour_spent').val(response.total_month_hour_spent);
          $.each(response.monthly_history, function( index, value ) {
            if(value.is_date_gone==true && value.present !=true && value.week_off !=true && value.holiday !=true)
            {
              modaljasonOb = {
                            "name":"absent",
                            "date":value.date,
                        }
                modaljasonObAr.push(modaljasonOb);
            }
            if(value.holiday==true)
            {
              jasonOb = {
                            "name":"holiday",
                            "holiday_name":value.holiday_name,
                            "date":value.date,
                        }
              eventArray2.push(jasonOb);
              modaljasonObAr.push(jasonOb);
            }
            if(value.early_out==true)
            {
              jasonOb = {
                            "name":"early_out",
                            "date":value.date,
                            "time":value.punch_out_time,
                        }
              modaljasonObAr.push(jasonOb);
            }
            if(value.attendnace_pending==true)
            {
              jasonOb = {
                            "name":"pending",
                            "date":value.date,
                        }
                        modaljasonObAr.push(jasonOb);
            }
            if(value.attendance_declined==true)
            {
              jasonOb = {
                            "name":"rejected",
                            "date":value.date,
                        }
                        modaljasonObAr.push(jasonOb);
            }
            if(value.extra_day==true)
            {
              jasonOb = {
                            "name":"extra_day",
                            "date":value.date,
                        }
              modaljasonObAr.push(jasonOb);
            }
            if(value.late_in==true)
            {
              jasonOb = {
                            "name":"late_in",
                            "date":value.date,
                            "time":value.punch_in_time,

                        }
              modaljasonObAr.push(jasonOb);
            }
            if(value.week_off==true)
            {
              jasonOb = {
                            "name":"weekoff",
                            "date":value.date,
                        }
                        eventArray2.push(jasonOb);
            }
            if(value.leave==true)
            {
              jasonOb = {
                            "name":"leave",
                            "date":value.date,
                            "type":value.leave_type_name,
                            "leave_day_view":value.leave_day_view,
                        }
                        eventArray2.push(jasonOb);
                  modaljasonObAr.push(jasonOb);
            }
            if(value.present==true)
            {
              jasonOb = {
                            "name":"present",
                            "date":value.date,
                        }
                        eventArray2.push(jasonOb);
            }
            if(value.is_punch_out_missing==true)
            {
              jasonOb = {
                            "name":"punchMiss",
                            "date":value.date,
                        }
                modaljasonObAr.push(jasonOb);
            }
            if(value.half_day==true)
            {
              jasonOb = {
                            "name":"halfday",
                            "date":value.date,
                            "type":value.leave_type_name,
                            "leave_day_view":value.leave_day_view,
                        }
              eventArray2.push(jasonOb);
              modaljasonObAr.push(jasonOb);
            }
            
          });
        }
              main2(eventArray2);
    }
  });

function main2(eventArrayData)
{
  it= '<?php echo $initDate; ?>';
 
      var pignoseCalendar = $('.calendar').pignoseCalendar({
          format: 'YYYY-MM-DD',
          theme: 'blue',
          weeks: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
          date: it,
          scheduleOptions: {
            colors: {
                holiday: 'green',
                present: 'blue',
                halfday: 'yellow',
                fullday:'yellow',
                leave: 'red',
                weekoff:'#11cdef',
            }
          },
        schedules: eventArrayData,
        select: function(dates, context) {
          if(context.storage.schedules.length>0) {

            if(context.storage.schedules[0].name=='leave'){
             
              Lobibox.notify('warning', {
                size: 'mini',
                rounded: true,
                pauseDelayOnHover: true,
                continueDelayOnInactiveTab: false,
                position: 'top right',
                icon: 'fa fa-sign-out',
                msg: 'Leave'
              });
            }
            if(context.storage.schedules[0].name=='weekoff'){
             
              Lobibox.notify('info', {
                size: 'mini',
                rounded: true,
                pauseDelayOnHover: true,
                continueDelayOnInactiveTab: false,
                position: 'top right',
                icon: 'fa fa-power-off',
                msg: 'Week off'
              });
            }
            if(context.storage.schedules[0].name=='holiday'){
             
              Lobibox.notify('success', {
                size: 'mini',
                rounded: true,
                pauseDelayOnHover: true,
                continueDelayOnInactiveTab: false,
                position: 'top right',
                icon: 'fa fa-gift',
                msg: 'Holiday'
              });
            }
          }
          getTodayData(dates[0]['_i'])
          
        },
    
    });
}
 
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = yyyy + '-' + mm + '-' + dd;
//today="2021-12-28";
//getTodayData(today)

  function getTodayData(date){
    var showData ="";
    $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {user_id :$('#user_attendance_id').val(),
            date:date,
            action:"get_attendance_today_attendance_data",
            society_id: $('#society_id').val()},
      success: function(response){
       
        date =  new Date(date) ;
        let shortMonth = date.toLocaleString('en-us', { month: 'short' }); /* Jun */
        let shortYear = date.getFullYear(); /* Jun */
        let shortDate = date.getDate(); /* Jun */
        showDate =  shortDate + '-' + shortMonth + '-' + shortYear;
       if(response.status ==200)
       {
          if(response.data.punch_in_time !="00:00:00"){
            punch_in_time = response.data.punch_in_time;
          }
          if(response.data.punch_out_time !=""){
            punch_out_time = response.data.punch_out_time;
          }else
          {
            punch_out_time = "";
          }
          if(response.data.punch_out_time !="" && response.data.punch_out_time !='Not Punch Out' && response.data.punch_out_time !='Punch Out Missing' ){
            total_working = response.data.total_working;
          }
          else
          {
            total_working="";
          }
          if(response.data.workReport != null){
            response.data.workReport = $.trim(response.data.workReport) ;
            workReport =  response.data.workReport.substring(0, 20)
          }
          else
          {
            workReport ="";
          }
          showData = `<div class="row container-fluid" >
                        <div class="col-md-12">
                          <div class="col-md-6 mt-2 float-left"><h6>Date :</h6></div>
                          <div class="col-md-6 mt-2 float-right">`+showDate+` (`+response.data.day+`)</div>
                        </div>                      
                        <div class="col-md-12">
                          <div class="col-md-6 mt-2 float-left"><h6>Total Working Hours:</h6></div>
                          <div class="col-md-6 mt-2 float-right">`+total_working+`</div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-6 mt-2 float-left"><h6>Punch In</h6></div>
                          <div class="col-md-6 mt-2 float-right">`+punch_in_time+`</div>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-6 mt-2 float-left"><h6>Punch Out</h6></div>
                          <div class="col-md-6 mt-2 float-right">`+punch_out_time+`</div>
                        </div>
                          
                      </div>`;
                      if(response.data.attedance.length>0)
                      {
                        $.each(response.data.attedance, function( index, value ) {
                          showData +=` <div class="col-md-12">
                                        <div class="col-md-6 mt-2 float-left">
                                          <div class="col-md-12 mt-2 float-left">
                                          <span><b>`+value.attendance_type_name +` Start</b> </span>
                                          </div>
                                          <div class="col-md-12 mt-2 float-right">
                                          `+value.break_in_time+`
                                          </div>
                                        </div>
                                        <div class="col-md-6 mt-2 float-left ">
                                          <div class="col-md-12 mt-2 float-left">
                                          <span> <b>`+value.attendance_type_name +` End</b></span>
                                          </div>
                                          <div class="col-md-12 mt-2 float-right">
                                          `+value.break_out_time+`
                                          </div>
                                        </div>
                                        <div class="col-md-12 mt-2 ">
                                          <div class="col-md-12 mt-2 float-left">
                                          <span><b>Total `+value.attendance_type_name+` Time</b></span>
                                          </div>
                                          <div class="col-md-12 mt-2 float-right">
                                          `+value.total_break+`
                                          </div>
                                        </div>
                                    </div>`;
                          });
                      }
          
       }
       else
       {
        showData = ``;
       }
        
      $('#todayData').html(showData);

      var  main_content3 ;
        if (response.data.total_punch>1) {
          $('.multiPunchInTable').show();
          $.each(response.data.punch_in_data, function (index, value) {
             
             main_content3 += `<tr>
                                  <td>`+ value.punch_in_date +`</td>
                                  <td>`+ value.punch_out_date  +`</td>
                                  <td>`+ value.working_hour  +`</td>
                              </tr>`;
           })
          $('#MulitPunchIn').html(main_content3);
        }


      }
   });
}
$(".progress-bar").loading();
$('input').on('click', function () {
	 $(".progress-bar").loading();
});
$('.showModalForSingleDAta').click(function(){
  clikcName = $(this).attr('data-id');
 if($('.'+clikcName).text()>0){
  $('#showSIngleData').modal();
 }
 
    showHtml = "";
    showHtmlExtraDays  = "";
    showHtmlAbsent = "";
    showHtmlEarlyOut = "";
    showHtmlhalfDay = "";
    showHtmlLeaves = "";
    showHtmllateIn = "";
    showHtmlMissPunch = "";
    showHtmlholidays = "";
    showHtmlPending = "";
    showHtmlRejected = "";
  $.each(modaljasonObAr, function( index, value ) {
   
    date =  new Date(value.date) ;
    let shortMonth = date.toLocaleString('en-us', { month: 'short' }); /* Jun */
    let shortYear = date.getFullYear(); /* Jun */
    let shortDate = date.getDate(); /* Jun */
    let shortDay= date.getDay(); /* Jun */
    let shortDay2= date.toLocaleDateString('en-us', {  day: '2-digit' }); /* Jun */
    
    days =  ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    shortDay = days[shortDay];
    showDate =  shortDate + '-' + shortMonth + '-' + shortYear;
  if(clikcName=="ExtraDays")
  {
    if(value.name=="extra_day"){
     
      showHtmlExtraDays +=`<tr>
      <td>`+showDate+` (` +shortDay+ `)</td>
                </tr>`;
    }
   
  }
  if(clikcName=="Absent")
  {
    if(value.name=="absent"){
      showHtmlAbsent +=`<tr>
                        <td>`+showDate+` (` +shortDay+ `)</td>
                        </tr>`;
    }
  }
  if(clikcName=="EarlyOut")
  {
    if(value.name=="early_out"){
      showHtmlEarlyOut +=`<tr>
      <td>`+showDate+` (` +shortDay+ `) ( `+value.time+` )</td>
                  </tr>`;
    }
  }
  if(clikcName=="halfDay")
  {
    if(value.name=="halfday"){
    showHtmlhalfDay +=`<tr>
    <td>`+showDate+` ` +shortDay+ ` (`+ value.type +`) `+value.leave_day_view+`</td>
                </tr>`;
    }
  }
  if(clikcName=="Pending")
  {
    if(value.name=="pending"){
    showHtmlPending +=`<tr>
    <td>`+showDate+` (` +shortDay+ `)</td>
                </tr>`;
    }
  }
  if(clikcName=="Rejected")
  {
    if(value.name=="rejected"){
    showHtmlRejected +=`<tr>
    <td>`+showDate+` (` +shortDay+ `)</td>
                </tr>`;
    }
  }
  if(clikcName=="Leaves")
  {
    if(value.name=="leave"){
    showHtmlLeaves +=`<tr>
                        <td>`+showDate+` `+shortDay+ ` (`+value.type+`) `+value.leave_day_view+` </td>
                      </tr>`;
    }
  }
  if(clikcName=="lateIn")
  {
    if(value.name=="late_in"){
    showHtmllateIn +=`<tr>
    <td>`+showDate+` (` +shortDay+ `) ( `+value.time+` )</td>
                </tr>`;
    }
  }
  if(clikcName=="MissPunch")
  {
    if(value.name=="punchMiss"){
    showHtmlMissPunch +=`<tr>
    <td>`+showDate+` (` +shortDay+ `)</td>
                </tr>`;
    }
  }
  if(clikcName=="holidays")
  {
    if(value.name=="holiday"){
    showHtmlholidays +=`<tr>
                          <td>`+showDate+` (` +shortDay+ `) <span class=' badge badge-primary'>` +value.holiday_name+ `</span></td>
                        </tr>`;
    }
  }
})
  if(clikcName=="holidays")
  {
    $('.ShowDateDetails').html(showHtmlholidays);
  }
  if(clikcName=="MissPunch")
  {
    $('.ShowDateDetails').html(showHtmlMissPunch);
  }
  if(clikcName=="lateIn")
  {
    $('.ShowDateDetails').html(showHtmllateIn);
  }
  if(clikcName=="Leaves")
  {
    $('.ShowDateDetails').html(showHtmlLeaves);
  }
  if(clikcName=="halfDay")
  {
    $('.ShowDateDetails').html(showHtmlhalfDay);
  }
  if(clikcName=="EarlyOut")
  {
    $('.ShowDateDetails').html(showHtmlEarlyOut);
  }
  if(clikcName=="Absent")
  {
    $('.ShowDateDetails').html(showHtmlAbsent);
  }
  if(clikcName=="ExtraDays")
  {
    $('.ShowDateDetails').html(showHtmlExtraDays);
  }
  if(clikcName=="Rejected")
  {
    $('.ShowDateDetails').html(showHtmlRejected);
  }
  if(clikcName=="Pending")
  {
    $('.ShowDateDetails').html(showHtmlPending);
  }

})
</script>

<style>
.attendance_status
{
  min-width: 113px;
}

.myIcon{
  width: 36px !important;
  height: 36px !important;
}

.month_att_crds_row .card.radius-10.border-1px.mb-3 p.mb-0.text-primary{
  font-size: 12px;
}

.month_att_crds_row .card.radius-10.border-1px.mb-3 {
  height: 80px;
}

.progress-bar {
    background-color: #ffffff;
}

.progress-bar div span {
    position: absolute;
    font-family: Arial;
     font-size: 16px;
}
    
</style>
