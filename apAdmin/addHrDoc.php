<?php
extract(array_map("test_input", $_POST));
if (isset($edit_hr_doc)) {
  $q = $d->select("hr_document_master", "hr_document_id='$hr_document_id'");
  $data = mysqli_fetch_array($q);
  extract($data);
}
?>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Document</h4>
      </div>
      <div class="col-sm-3">
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="addHrDocumentAdd" action="controller/hrDocController.php" enctype="multipart/form-data" method="post">
              <div class="row">
                <div class="col-md-4 ">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Category <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12" id="">
                      <select class="form-control single-select restFrm" name="hr_document_category_id" id="hr_document_category_id" onchange="getHRDocSubCategory(this.value)" <?php if (isset($edit_hr_doc)) { echo 'disabled'; } ?>>
                        <option value="">-- Select --</option>
                        <?php
                        $hrcate = $d->select("hr_document_category_master", "society_id='$society_id' AND hr_document_category_deleted=0");
                        while ($HrCatdata = mysqli_fetch_array($hrcate)) {
                        ?>
                          <option <?php if (isset($data['hr_document_category_id']) && $data['hr_document_category_id'] == $HrCatdata['hr_document_category_id']) {
                                    echo "selected";
                                  } ?> value="<?php if (isset($HrCatdata['hr_document_category_id']) && $HrCatdata['hr_document_category_id'] != "") {echo $HrCatdata['hr_document_category_id']; } ?>"><?php if (isset($HrCatdata['hr_document_category_id']) && $HrCatdata['hr_document_category_id'] != "") { echo $HrCatdata['hr_document_category_name']; } ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 ">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Sub Category </label>
                    <div class="col-lg-12 col-md-12" id="">
                      <select class="form-control single-select restFrm" name="hr_document_sub_category_id" id="hr_document_sub_category_id" <?php if (isset($edit_hr_doc)) {echo 'disabled';} ?>>
                        <option value="">-- Select --</option>
                        <?php
                        $hrSubcate = $d->select("hr_document_sub_category_master", "society_id='$society_id' AND hr_document_sub_category_deleted=0 AND hr_document_category_id='$hr_document_category_id'");
                        while ($HrSubCatdata = mysqli_fetch_array($hrSubcate)) {
                        ?>
                          <option <?php if (isset($data['hr_document_sub_category_id']) && $data['hr_document_sub_category_id'] == $HrSubCatdata['hr_document_sub_category_id']) {
                                    echo "selected";
                                  } ?> value="<?php if (isset($HrSubCatdata['hr_document_sub_category_id']) && $HrSubCatdata['hr_document_sub_category_id'] != "") {echo $HrSubCatdata['hr_document_sub_category_id'];} ?>"><?php if (isset($HrSubCatdata['hr_document_sub_category_id']) && $HrSubCatdata['hr_document_sub_category_id'] != "") {echo $HrSubCatdata['hr_document_sub_category_name'];} ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-sm-12 col-form-label">Branch <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12" id="">
                      <select <?php if (isset($edit_hr_doc)) {echo 'disabled';} ?> name="block_id[]" multiple class="form-control HrFloor single-select"  required>
                        <option value="">-- Select Branch --</option>
                        <?php if ($access_branchs=="") { ?>
                        <option value="all_branch"> All Branch </option>
                        <?php }
                        $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQueryOnly");
                        while ($blockData = mysqli_fetch_array($qb)) {
                        ?>
                          <option <?php if ($block_id == $blockData['block_id']) {
                                    echo 'selected';
                                  } ?> value="<?php echo  $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?></option>
                        <?php } ?>

                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 ">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-sm-12 col-form-label">Department <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12" id="">
                      <select multiple name="floor_id[]" id="floor_id" class="form-control single-select restFrm HrDocUser"  <?php if (isset($edit_hr_doc)) {echo 'disabled';} ?>>
                        <?php if ($access_branchs=="") { ?>
                        <option value="All"> All Department </option>
                        <?php }
                        $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id $blockAppendQuery $blockAppendQueryFloor");
                        while ($depaData = mysqli_fetch_array($qd)) {
                        ?>
                          <option <?php if ($floor_id == $depaData['floor_id']) {
                                    echo 'selected';
                                  } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name'] . '-' . $depaData['block_name']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 ">
                  <div multiple class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-sm-12 col-form-label">Employee</label>
                    <div class="col-lg-12 col-md-12" id="">
                      <select type="text" multiple class="form-control single-select employees" name="user_id[]" id="user_id" <?php if (isset($edit_hr_doc)) { echo 'disabled'; } ?>>
                        <option value="">-- Select --</option>
                        <?php
                        $userData = $d->select("users_master", "society_id='$society_id' AND floor_id='$floor_id'");
                        while ($user = mysqli_fetch_array($userData)) {
                        ?>
                          <option <?php if (isset($data['user_id']) && $data['user_id'] == $user['user_id']) {
                                    echo "selected";
                                  } ?> value="<?php if (isset($user['user_id']) && $user['user_id'] != "") { echo $user['user_id']; } ?>"> <?php if (isset($user['user_full_name']) && $user['user_full_name'] != "") { echo $user['user_full_name']; } ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 ">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-sm-12 col-form-label">Document Name <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 ">
                      <input type="text" name="hr_document_name" id="hr_document_name" class="form-control" value="<?php if (isset($data['hr_document_name']) && $data['hr_document_name'] != "") { echo $data['hr_document_name'];} ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-4 ">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-sm-12 col-form-label">Document Type <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 ">
                      <select name="document_type" id="document_type" class="form-control" onchange="documentType(this.value);">
                        <option <?php if ($document_type == 0) {
                                  echo 'selected';
                                } ?> value="0">Local Storage</option>
                        <option <?php if ($document_type == 1) {
                                  echo 'selected';
                                } ?> value="1">Online Public URL</option>
                        <option <?php if ($document_type == 2) {
                                  echo 'selected';
                                } ?> value="2">YouTube URL</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 local-storage <?php if ($document_type == 1 || $document_type == 2) {echo 'd-none';} ?>">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-sm-12 col-form-label">Document <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 ">
                      <input type="hidden" name="hr_document_file_old" id="hr_document_file_old" value="<?php if (isset($data['hr_document_file']) && $data['hr_document_file'] != "" && $document_type == 0) { echo $data['hr_document_file'];} ?>">
                      <input type="file" accept=".pdf,.doc" required="" name="hr_document_file" class="form-control-file border ">
                    </div>
                  </div>
                </div>
                <div class="col-md-4 online-url <?php if ($document_type != 0) {
                                                } else {
                                                  echo 'd-none';
                                                } ?>">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-sm-12 col-form-label">Document URL <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 ">
                      <input type="text" required="" name="hr_document_url" value="<?php if (isset($data['hr_document_file']) && $data['hr_document_file'] != "") {echo $data['hr_document_file']; } ?>" class="form-control">

                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <label for="input-10" class="col-sm-12 col-form-label">Terms and Condition <span class="text-danger">*</span></label>

                  <div class="form-check form-check-inline">
                  <?php if (isset($data['is_terms_and_conditions']) && $data['is_terms_and_conditions'] != "") { if($data['is_terms_and_conditions']==0){ $checked="checked"; } else{ $checked=""; } }else{ $checked="checked"; } ?>
                    <input class="form-check-input" name="is_terms_and_conditions" type="radio" name="inlineRadioOptions" id="is_terms_and_conditions_no" value="0" <?php echo $checked ?> />
                    <label class="form-check-label" for="is_terms_and_conditions_no">No</label>
                  </div>
                  <div class="form-check form-check-inline">
                  <?php if (isset($data['is_terms_and_conditions']) && $data['is_terms_and_conditions'] != "") { if($data['is_terms_and_conditions']==1){ $checked="checked"; } else{ $checked=""; } }else{ $checked=""; } ?>
                    <input class="form-check-input" name="is_terms_and_conditions" type="radio" name="inlineRadioOptions" id="is_terms_and_conditions_yes" value="1" <?php echo $checked ?> />
                    <label class="form-check-label" for="is_terms_and_conditions_yes">Yes</label>
                  </div>
                </div>
              </div>
              <div class="form-footer text-center">
                <?php //IS_1019 addPenaltiesBtn 
                if (isset($edit_hr_doc)) {
                ?>

                  <input type="hidden" id="hr_document_id" name="hr_document_id" value="<?php if ($data['hr_document_id'] != "") { echo $data['hr_document_id'];} ?>">
                  <button id="addHrDocumentBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                  <input type="hidden" name="addHrDocument" value="addHrDocument">
                <?php } else {
                ?>

                  <button id="addHrDocumentBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                  <input type="hidden" name="addHrDocument" value="addHrDocument">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" 
                  ?>
                  <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addHrDocumentAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                <?php } ?>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
    <!--End Row-->

  </div>
  <!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script>
  function documentType(value) {
    if (value == 0) {
      $('.local-storage').removeClass('d-none');
      $('.online-url').addClass('d-none');
    } else if (value == 1 || value == 2) {
      $('.online-url').removeClass('d-none');
      $('.local-storage').addClass('d-none');
    } else {
      $('.online-url').addClass('d-none');
      $('.local-storage').addClass('d-none');
    }
  }
</script>