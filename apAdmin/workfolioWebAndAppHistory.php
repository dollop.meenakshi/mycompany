<link href="assets/plugins/vertical-timeline/css/vertical-timeline1.css" rel="stylesheet"/>
<?php error_reporting(0);
$tId = $_REQUEST['tId'];
$eId = $_REQUEST['eId'];
$date = $_REQUEST['date'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Workfolio Web And App History</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
            <a href="javascript:void(0)" onclick="DeleteAll('deleteChatGroupMember');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
     </div>

        <form class="workfolioFilterForm" action="">
            <div class="row pt-2 pb-2">
                <div class="col-md-3 form-group">
                    <select name="tId" class="form-control single-select" required="" onchange="getWorkfolioEmployees(this.value)">
                        <option value="">-- Select Team --</option> 
                        <?php 
                            $qt=$d->select("workfolio_team","");  
                            while ($teamData=mysqli_fetch_array($qt)) {
                        ?>
                        <option  <?php if($tId==$teamData['workfolio_team_id']) { echo 'selected';} ?> value="<?php echo $teamData['workfolio_team_id'];?>" ><?php echo $teamData['teamName'];?></option>
                        <?php } ?>
                    </select>
                </div>
				<div class="col-md-3 form-group">
                    <select name="eId" id="eId" class="form-control single-select" required="">
                        <option value="">-- Select Team Employee --</option> 
                        <?php 
                            $qte=$d->select("workfolio_employees,workfolio_team","workfolio_employees.teamId=workfolio_team.teamId AND workfolio_team.workfolio_team_id='$tId'");  
                            while ($teamEmpData=mysqli_fetch_array($qte)) {
                        ?>
                        <option  <?php if($eId==$teamEmpData['workfolio_employee_id']) { echo 'selected';} ?> value="<?php echo $teamEmpData['workfolio_employee_id'];?>" ><?php echo $teamEmpData['displayName'];?></option>
                        <?php } ?>
                    </select>
                </div>
				<div class="col-md-3 form-group">
					<input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" readonly name="date" value="<?php if ($date!='') { echo $date;} else{echo date('Y-m-d');} ?>">
                </div>
                
                <div class="col-md-3 form-group">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
		<div class="row">
			<div class="col-lg-12">
        <?php
          if (isset($tId) && $tId>0 && isset($eId) && $eId>0) {

            $q = $d->select("workfolio_appsandwebsiteshistory,workfolio_employees", "workfolio_appsandwebsiteshistory.email=workfolio_employees.email AND workfolio_employees.workfolio_employee_id='$eId' AND DATE_FORMAT(workfolio_appsandwebsiteshistory.date,'%Y-%m-%d') = '$date'", "ORDER BY workfolio_appsandwebsiteshistory.date ASC");
          
          if(mysqli_num_rows($q)>0){
            $counter = 1;
        ?>
          <section class="cd-timeline js-cd-timeline">
            <div class="cd-timeline__container">
              <?php while ($data = mysqli_fetch_array($q)) { ?>
              <div class="cd-timeline__block js-cd-block <?php if($counter % 2 == 0){ echo 'floatRight'; };?>">
                <div class="cd-timeline__img js-cd-img cd-timeline__img--bounce-in bg-white">
                  <img src="<?php echo $data['icon']; ?>">
                </div> <!-- cd-timeline__img -->
                <div class="cd-timeline__content js-cd-content cd-timeline__content--bounce-in card border">
                  <h6 class="mb-0 mt-0"><?php echo $data['title']; ?> <?php $counter++; ?></h6>
                  <p class="m-0"><?php echo $data['window_title']; ?></p>
                  <div class="pt-2 row" style="font-size: 12px;">
                    <div class="col pl-3">
                      <?php
                        $init = $data['totalSec'];
                        $hours = floor($init / 3600);
                        $minutes = floor(($init / 60) % 60);
                        $seconds = $init % 60;
                      ?>
                      <span><i class="fa fa-clock-o"></i> <?php echo sprintf('%02d', $hours).'h:'.sprintf('%02d', $minutes).'m:'.sprintf('%02d', $seconds).'s'; ?></span>
                    </div>
                    <div class="col pr-3">
                      <?php if($data['productivity_status']=="neutral"){
                        $statusClass = "text-dark";
                      }else if($data['productivity_status']=="productive"){
                        $statusClass = "text-success";
                      }else if($data['productivity_status']=="unproductive"){
                        $statusClass = "text-danger";
                      } ?>
                      <span class="<?php echo $statusClass; ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo $data['productivity_status']; ?></span>
                    </div>
                  </div>
                  <span class="cd-timeline__date"><?php echo date("d M Y h:i A", strtotime($data['date'])); ?></span>
                </div> <!-- cd-timeline__content -->
              </div>
              <?php } ?>
            </div>
          </section>
        <?php }else{ ?> 
          <div class="card" role="alert">
            <div class="card-body">
              <span><strong>Note :</strong> No Data Found!</span>
            </div>
          </div>
        <?php } }else{?> 
          <div class="card col-md-12" role="alert">
            <div class="card-body">
              <span><strong>Note :</strong> Please Select Team Employee</span>
            </div>
          </div>
        <?php } ?>
			</div>
		</div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
