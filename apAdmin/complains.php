  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Complains</h4>
         <ol class="breadcrumb">
          <li>
            <a href ="complains?type=1" class="btn btn-success btn-sm  waves-effect waves-light m-1">CLose (<?php echo $d->count_data_direct("complain_id","complains_master","society_id='$society_id' AND complain_status=1"); ?>)<i></i> </a>   
            <a href ="complains?type=2" class="btn btn-info btn-sm waves-effect waves-light m-1">Re Open  (<?php echo $d->count_data_direct("complain_id","complains_master","society_id='$society_id' AND complain_status=2"); ?>)<i></i> </a> 
            <a href ="complains?type=0" class="btn btn-warning btn-sm waves-effect waves-light m-1">Open (<?php echo $d->count_data_direct("complain_id","complains_master","society_id='$society_id' AND complain_status=0"); ?>)<i></i> </a>  
        </ol>
     </div>
     <div class="col-sm-3">
       <!-- <div class="btn-group float-sm-right">
        <a href="employee" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
       
        </button>
         -->
         <a href="javascript:void(0)" onclick="DeleteAll('deleteCompalaine');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                       <th>#</th>
                        <th> Status</th>
                        <th>No</th>
                        <th><?php echo $xml->string->unit; ?></th>
                        <th> Title</th>
                        <th> Description</th>
                        <th> Photo</th>
                        <th> Date</th>
                        <th>Last Status</th>                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                    if (isset($_GET['type'])) {
                      if ($_GET['type']==0) {
                        $q=$d->select("complains_master","society_id='$society_id' AND  complain_status=0","ORDER BY complain_id DESC");
                      }elseif ($_GET['type']==1) {
                        $q=$d->select("complains_master","society_id='$society_id'  AND complain_status=1","ORDER BY complain_id DESC");
                      } elseif ($_GET['type']==2) {
                        $q=$d->select("complains_master","society_id='$society_id'  AND complain_status=2","ORDER BY complain_id DESC");
                      } 
                    }
                    else {
                      $q=$d->select("complains_master","society_id='$society_id'","ORDER BY complain_id DESC");
                    }
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td class="text-center">
                        <?php if ($data['complain_status']==1 OR $data['flag_delete']==1): ?>
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['complain_id']; ?>">
                        <?php endif ?>
                        </td>
                        <td><?php
                        if ($data['flag_delete']==0) {
                         
                         if ($data['complain_status']==0) {  ?>
                          <span   data-toggle="modal" data-target="#vieComp"  onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-warning m-1 pointerCursor">OPEN</span>
                        <?php } elseif ($data['complain_status']==1) { ?>
                          <span   data-toggle="modal" data-target="#vieComp"  onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-success  m-1 pointerCursor">CLOSE</span>
                        <?php  } elseif ($data['complain_status']==2) { ?>
                          <span  data-toggle="modal" data-target="#vieComp"   onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-info  m-1 pointerCursor">REOPEN</span>
                        <?php } elseif ($data['complain_status']==3) { ?>
                          <span  data-toggle="modal" data-target="#vieComp"   onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-primary  m-1 pointerCursor">In Progress</span>
                        <?php } } else {
                          echo "Deleted by User";
                        }?></td>
                         <td><?php echo $i++; ?></td>
                        <td><?php echo $data['complain_assing_to']; ?></td>
                        <td><?php echo $data['compalain_title']; ?></td>
                        <td><?php echo $data['complain_description']; ?></td>
                        <td><?php if ($data['complain_photo']!='') { ?>
                           <a data-fancybox="images" data-caption="Photo Name : <?php echo $data["complain_photo"]; ?>" target="_blank" href="../img/complain/<?php echo $data['complain_photo']; ?>"><img width="50" height="50"  src="../img/complain/<?php echo $data['complain_photo']; ?>" ></a>
                        <?php } else {
                          echo "Not Uploaded";
                        } ?></td>
                        <td><?php echo $data['complain_date']; ?></td>
                        
                        <td><?php echo $data['complain_review_msg']; ?></td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
<script type="text/javascript">
  function vieComplain(complain_id) {
      $.ajax({
          url: "getComplaineDetails.php",
          cache: false,
          type: "POST",
          data: {complain_id : complain_id},
          success: function(response){
              $('#comResp').html(response);
            
              
          }
       });
  } 
</script>



<div class="modal fade" id="vieComp">
  <div class="modal-dialog">
    <div class="modal-content border-success">
      <div class="modal-header bg-success">
        <h5 class="modal-title text-white">View Complain </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="comResp">
          
      </div>
     
    </div>
  </div>
</div>