  <?php error_reporting(0);
  
  //error_reporting(0);
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year']= $_REQUEST['month_year'];
  $dId = (int)$_REQUEST['dId'];
  $bId = (int)$_REQUEST['bId'];
  $uId = (int)$_REQUEST['uId'];
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row ">
        <div class="col-md-12 ">
          <h4 class="page-title">Punch Out Missing Report</h4>
        </div>
     </div>
     <form action="" class="branchDeptFilter" >
      <div class="row ">
        <?php include('selectBranchDeptEmpForFilterAll.php'); ?>
     
        <div class="col-md-2 col-6 form-group">
          <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){echo $from= $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-31 days'));} ?>">  
        </div>
        <div class="col-md-2 col-6 form-group">
          <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate=  $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
        </div>
          <div class="col-md-1 text-center my-auto">
              <input class="btn btn-success btn-sm mb-3" type="submit" name="getReport" class="form-control" value="Get Data">
          </div>    
      </div>
    
      
      </form>
     
      <div class="row mt-2">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <input type="hidden" name="punchTypeDid" id="punchTypeDid" value="<?php if(isset($_GET['dId']) && $_GET['dId']>0 ){ echo 1; } else { echo 0; } ?>">
              <input type="hidden" name="floor_id_old" id="floor_id_old" value="<?php if(isset($_GET['dId']) && $_GET['dId']>0 ){ echo $_GET['dId']; }?>">

                  <?php 
                        $i=1;
                        if (isset($bId) && $bId> 0) {
                          $BranchFilterQuery = " AND users_master.block_id='$bId]'";
                        }

                        if(isset($_GET['dId']) && $_GET['dId']>0) {
                          $deptFilterQuery = " AND users_master.floor_id='$_GET[dId]'";
                        }
                        else
                        {
                          $limitSql = 'LIMIT 1000';
                        }
                       
                        $crDate = date('Y-m-d');
                        
                        if(isset($from) && isset($from) && $toDate != '' && $toDate) {
                          $dateFilterQuery = " AND attendance_master.attendance_date_start BETWEEN '$from' AND '$toDate'";
                        }
                        if(isset($_GET['uId']) && $_GET['uId']>0) {
                          $userFilterQuery = "AND attendance_master.user_id='$_GET[uId]'";
                        }
                        $curnTime  = date('H:i:s');
                        $q=$d->select("attendance_master LEFT JOIN shift_timing_master ON shift_timing_master.shift_time_id = attendance_master.shift_time_id ,users_master LEFT JOIN floors_master ON floors_master.floor_id=users_master.floor_id LEFT JOIN block_master ON block_master.block_id=users_master.block_id","users_master.user_id=attendance_master.user_id AND attendance_master.society_id='$society_id' AND attendance_master.punch_out_time ='00:00:00' AND attendance_master.attendance_date_start <' $crDate' AND users_master.delete_status=0 $BranchFilterQuery $deptFilterQuery $dateFilterQuery $userFilterQuery $blockAppendQueryUser","ORDER BY attendance_master.attendance_id DESC $limitSql");
                        $counter = 1;
                        //if(isset($_GET['dId'])){
                        ?>
                  <table id="example" class="table table-bordered">
                    <thead>
                        <tr>
                          <th>Sr.No</th>                        
                          <th>Action</th>
                          <th>Employee Name</th>
                          <th>Department</th>
                          <th>Punch In</th>
                          <th>Punch In Date</th>                      
                          <th>Punch Out</th>
                          <th>Punch Out Date</th>                      
                          <th>In Branch</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      while ($data=mysqli_fetch_array($q)) {
                       // print_r($data);
                      ?>
                        <tr>
                          
                          <td><?php echo $counter++; ?></td>
                          <td>
                              <div class="d-flex align-items-center">
                              <button type="button" class="btn btn-sm btn-primary ml-2" onclick="viewUserCheckInOutImage('<?php echo $data['attendance_id']; ?>')"> <i class="fa fa-picture-o"></i> <i class="fa fa-map-marker"></i></button>

                                <button type="button" class="btn btn-sm btn-primary ml-2" onclick="allAttendaceSet(<?php echo $data['attendance_id']; ?>)"> <i class="fa fa-eye"></i></button>
                                <input type="hidden" name="user_id_attendace" value="<?php if($data['user_id']!=""){ echo $data['user_id'];}   ?>">
                                 <!--  <a   href="attendanceCalendar?uid=<?php if($data['user_id']!=""){ echo $data['user_id'];} ?>">
                                    <button type="button" class="btn btn-sm btn-primary ml-2" > 
                                      <i class="fa fa-calendar"></i>
                                    </button>
                                  </a> -->
                                  <button type="button" class="btn btn-sm btn-primary ml-2" onclick="changeUserAttendace(<?php if($data['attendance_id']!=''){ echo $data['attendance_id']; }  ?>,<?php if($data['floor_id']!=''){ echo $data['floor_id']; }  ?>,'<?php if($data['attendance_id']!=''){ echo $data['shift_type']; }  ?>','<?php if($data['attendance_id']!=''){ echo $data['attendance_date_start']; }  ?>','<?php if ($data['punch_in_time'] != '') {echo $data['punch_in_time']; }  ?>')"> 
                                      <i class="fa fa-pencil"></i>
                                    </button>
                              </div>
                          </td>
                          <td><?php echo $data['user_full_name']; ?><?php if(	$data['user_designation'] !=""){ echo "(". $data['user_designation'] .")"; } ?></td>
                          <td><?php echo $data['floor_name']."(".$data['block_name'] .")"; ?></td>

                          <td><?php if($data['punch_in_time']!='00:00:00' && $data['punch_in_time']!='null') { echo date("h:i A", strtotime($data['punch_in_time'])); }?>
                         <!--  <?php if ($data['punch_in_image']!='') { ?>
                            <button type="button" class="btn btn-sm btn-primary ml-2" onclick="viewUserCheckInOutImage( '<?php echo $data['attendance_id']; ?>')"> <i class="fa fa-picture-o"></i></button>
                          <?php } ?>
                          <?php if($data['punch_in_latitude']!='' & $data['punch_in_longitude']!=''){ ?>
                          <button type="button" class="btn btn-sm btn-primary ml-2" onclick="viewUserCheckInOutLocation('<?php echo $data['punch_in_latitude']; ?>', '<?php echo $data['punch_in_longitude']; ?>','<?php echo $data['block_id']; ?>')"> <i class="fa fa-map-marker"></i>    </button>
                          <?php } ?> -->
                          </td>
                          
                          <td><?php if($data['attendance_date_start']!='0000-00-00' && $data['attendance_date_start']!='null') { echo date("d M Y", strtotime($data['attendance_date_start'])); }?></td>
                          
                          <td><?php if($data['punch_out_time']!='00:00:00' && $data['punch_out_time']!='null') { echo date("h:i A", strtotime($data['punch_out_time'])); }?>
                         <!--  <?php if ($data['punch_out_image']!='') { ?>
                            <button type="button" class="btn btn-sm btn-primary ml-2" onclick="viewUserCheckInOutImage( '<?php echo $data['attendance_id']; ?>')"> <i class="fa fa-picture-o"></i></button>
                          <?php } ?>
                          <?php if($data['punch_out_latitude']!='' & $data['punch_out_longitude']!=''){ ?>
                          <button type="button" class="btn btn-sm btn-primary ml-2" onclick="viewUserCheckInOutLocation('<?php echo $data['punch_out_latitude']; ?>', '<?php echo $data['punch_out_longitude']; ?>','<?php echo $data['block_id']; ?>')"> <i class="fa fa-map-marker"></i>    </button>
                          <?php } ?> -->
                          </td>
                          
                          <td><?php if($data['attendance_date_end']!='0000-00-00' && $data['attendance_date_end']!='null') { echo date("d M Y", strtotime($data['attendance_date_end'])); }?></td>
                          <td><?php  echo $data['punch_in_branch'];  ?></td>

                          
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- <?php //} else {  ?>
                <div class="" role="alert">
                  <span><strong>Note :</strong> Please Select Department</span>
                  </div>
                <?php // } ?> -->
            </div>
            </div>
          </div>
        </div>
     </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>
  <div class="modal fade" id="ChanngeAttendaceDataModal">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Update Attendance</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="modal-body" style="align-content: center;">
            <div class="card-body" > 
              
              <form id="changeAttendance" name="changeAttendance"  enctype="multipart/form-data" method="post">
                  <div class="form-group row" id="hidePunchDateForShift">
                      <label for="input-10" class="col-sm-6 col-form-label">Punch Out Date <span class="text-danger">*</span></label>
                      <div class="col-lg-6 col-md-6" id="">
                        <select name="punch_out_date"  readonly id="punch_out_date" class="form-control single-select">
                        </select>                 
                    </div>     
                </div>     
                <div class="form-group row">
                      <label for="input-10" class="col-sm-6 col-form-label">Punch Out Time <span class="text-danger">*</span></label>
                      <div class="col-lg-6 col-md-6" id="">
                      <input type="text" class="form-control time-picker-shft-Out_update" readonly id="punch_out_time" name="punch_out_time" >
                      </div>                   
                </div>              
                <div class="form-footer text-center">
                <input type="hidden" name="updateUserAttendance"  value="updateUserAttendance">
                <input type="hidden" name="attendance_id"  id="attendance_id">
                <input type="hidden" name="indate"  id="indate" class="indate">
                  <button id="addHrDocumentBtn" type="submit"  onclick="updateUserAttendanceData()" class="btn btn-success addUserBank hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                </div>
            </form>
            </div>
          </div>
      </div>
    </div>
  </div>
<div class="modal fade" id="addAttendaceModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Attendance Data</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
          <div class="card-body">
            <div class="row col-md-12 ">
              <div class="table-responsive">
                <table  class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody id="attendanceData">
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row col-md-12 multiPunchInTable" style="display: none;">
              <table class="table table-bordered">
                <thead>
                  <th>Punch In</th>
                  <th>Punch Out</th>
                  <th>Hours</th>
                </thead>
                <tbody id="MulitPunchIn">
                </tbody>
              </table>
            </div>
            <div class="row col-md-12 wrkRpt">

            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="UserCheckInLocation">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Attendance Data</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body" > 
            <div class="map" id="map" style="width: 100%; height: 300px;"></div>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="UserCheckInOutImage">
    <div class="modal-dialog " style="max-width: 75%">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Attendance Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-md-6">
                <div><b>Punch In :</b> <span id="in_time"></span></div>
                <div ><b>Distance:</b> <span class="Inkm"></span></div>
                <div ><b>Address:</b> <span class="InAddress"></span></div>
              </div>
              <div class="col-md-6" style="display: none;">
                <div><b>Punch Out :</b> <span id="out_time"></span></div>
                <div ><b>Distance:</b> <span class="Outkm"></span></div>
                <div ><b>Address:</b> <span class="OutAddress"></span></div>
               
              </div>
            </div>
             <div class="row">
                <div class="col-md-6 col-6 text-center hideInData">
                  <div class="userCheckInOutImageData " id="userCheckInOutImageData">
                  </div>
                </div>
                <div class="col-md-6 col-6 text-center hideOutData">
                  <div class="userCheckOutImageData " id="userCheckOutImageData">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-12 float-left mapDiv ">
                  <div class="map mr-1" id="map3" style="width: 100%; height: 280px;">
                  </div>
                </div>
                
              </div>
            </div>

        </div>
      </div>
    </div>
  </div>
<div class="modal fade" id="attendanceDeclineReason">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Declined Reason</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="holiday" style="align-content: center;">
        <textarea class="form-control" name="attendance_declined_reason" id="attendance_declined_reason"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary attendanceDeclineReasonClass"  data-dismiss="modal" aria-label="Close">Save changes</button>
      </div>

    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>

<script type="text/javascript">
 
 function initialize(d_lat,d_long,blockRange,blockLat,blockLOng)
  {
    var latlng = new google.maps.LatLng(d_lat,d_long);
    var Blatlng = {lat: parseFloat(blockLat), lng: parseFloat(blockLOng)};

    var latitute = d_lat;
    var longitute = d_long;

    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      icon:"assets/images/placeholder.png",
      position: latlng,
     // draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });
   ////Company marker
    var marker2 = new google.maps.Marker({
      map: map,
      
      position: Blatlng,
    //  draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });
     ////Company circle
    var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange),    // 10 miles in metres
        fillColor: '#AA0000'
      });
    circle.bindTo('center', marker2, 'position');
    var parkingRadition = 5;
    var citymap = {
        newyork: {
          center: {lat: latitute, lng: longitute},
          population: parkingRadition
        }
    };

      
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function()
    {
      geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
      {
        if (status == google.maps.GeocoderStatus.OK)
        {
          if (results[0])
          {
            var places = results[0];
            var pincode="";
            var serviceable_area_locality= places.address_components[4].long_name;
            for (var i = 0; i < places.address_components.length; i++)
            {
              for (var j = 0; j < places.address_components[i].types.length; j++)
              {
                if (places.address_components[i].types[j] == "postal_code")
                {
                  pincode = places.address_components[i].long_name;
                }
              }
            }
            bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
          }
        }
      });
    });
  }


  function initializeCommonMap(in_lat, in_long,out_lat,out_long) {

      var latlng = new google.maps.LatLng(in_lat, in_long);
      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize3(d_lat, d_long) {
       
      var latlng = new google.maps.LatLng(d_lat, d_long);
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
   

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize2(out_lat, out_long) {

      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlngOut,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));


      var infowindow = new google.maps.InfoWindow();
      
    }

  
<?php
 if(!isset($_GET['month_year']))
 { ?>
    $('#month_year').val(<?php echo $currentMonth; ?>);
<?php } ?>





  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
      }
      return false;
    }

</script>
<style>
.attendance_status
{  
  min-width: 113px;
}
</style>
