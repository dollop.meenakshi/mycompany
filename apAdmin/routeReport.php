<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title">Route Report</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $q = $d->selectRow("rm.*,c.city_name,um.user_full_name,ream.route_assign_date,bm.block_name,fm.floor_name","route_master AS rm JOIN route_employee_assign_master AS ream ON ream.route_id = rm.route_id JOIN cities AS c ON c.city_id = rm.city_id JOIN users_master AS um ON um.user_id = ream.user_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","rm.route_active_status = 0 ");
                            $i = 1;
                            ?>
                            <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Route</th>
                                        <th>City</th>
                                        <th>User Name</th>
                                        <th>Route Created Date</th>
                                        <th>Route Assigned Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($data = $q->fetch_assoc())
                                    {
                                        extract($data);
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $route_name; ?></td>
                                        <td><?php echo $city_name; ?></td>
                                        <td><?php echo $user_full_name . " (" . $block_name . "-" . $floor_name . ")"; ?></td>
                                        <td><?php echo date("Y-m-d",strtotime($route_created_date)); ?></td>
                                        <td><?php echo date("Y-m-d",strtotime($route_assign_date)) ?></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div>