<?php error_reporting(0);
if(isset($_GET['id'])){
    $id = (int)$_GET['id'];
  }
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">User Task Pause History</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="javascript:void(0)" onclick="DeleteAll();" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>                        
                        <th>User Name</th>
                        <th>Pause Start Time</th>
                        <th>Pause Resume Time</th>                    
                        <th>Pause Time</th>                    
                        <th>Pause Reason</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("task_pause_history,users_master","task_pause_history.user_id=users_master.user_id AND task_pause_history.society_id='$society_id' AND task_pause_history.task_user_history_id='$id'","ORDER BY task_pause_history.task_pause_history_id DESC");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                        <td><?php echo $counter++; ?></td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php echo date("d M Y h:i A", strtotime($data['task_pause_date'])); ?></td>
                        <td><?php if($data['task_resume_date'] != '0000-00-00 00:00:00' && $data['task_resume_date'] != null){ echo date("d M Y h:i A", strtotime($data['task_resume_date']));} ?></td>
                        <td><?php if($data['task_resume_date'] != '0000-00-00 00:00:00' && $data['task_resume_date'] != null){
                                $pause_time = new DateTime($data['task_pause_date']);
                                $resume_time = new DateTime($data['task_resume_date']);
                                $interval = $pause_time->diff($resume_time);
                                echo $interval->format('%H:%I'); 
                              } ?></td>
                        <td><?php custom_echo($data['task_pause_reason'], 30); ?></td>
                        <td>
                          <a href="javascript:void(0)" onclick="userTaskPauseHistoryDetail(<?php echo $data['task_pause_history_id'] ?>)" title="View Detail" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>

<div class="modal fade" id="PauseHistoryDetailModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">User Task Pause History Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="pauseHistoryDetailData" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
