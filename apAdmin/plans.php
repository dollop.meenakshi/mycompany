  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Building Package</h4>
        <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="welcome">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Package</li>
         </ol>
     </div>
     <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="plan" class="btn btn-primary waves-effect waves-light btn-sm"><i class="fa fa-plus mr-1"></i> Add New</a>
         <a href="javascript:void(0)" onclick="DeleteAll('deletePackage');" class="btn btn-danger waves-effect waves-light btn-sm"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
       
        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      <th>#</th>
                      <th>#</th>
                      <th>Package Name</th>
                      <th>Description</th>
                      <th>Amount</th>
                      <th>Duration</th>
                      <th>Assign Society</th>
                      <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  $i=1;
                  $q=$d->select("package_master","","");
                  while($row=mysqli_fetch_array($q))
                  {
                    extract($row);
                     $totalAsignPckg= $d->count_data_direct("package_id","society_master","package_id='$row[package_id]'");
                  ?>
                    <tr>
                       <td class='text-center'>
                        <?php if ($totalAsignPckg==0) {
                         ?>
                        <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['package_id']; ?>">
                        <?php } ?>
                    </td>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $package_name; ?></td>
                        <td><?php echo $packaage_description; ?></td>
                        <td><?php echo $package_amount; ?></td>
                        <td><?php echo $no_of_month; ?> Month</td>
                        <td><?php echo $totalAsignPckg; ?> </td>
                        <td>
                        <form action="plan" method="post">
                            <input type="hidden" name="package_id" value="<?php echo $package_id; ?>">
                            <input type="submit" class="btn btn-sm btn-primary" name="updatepackage" value="Edit">
                          </form>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



