<?php
$id = (int)$_GET['id'];
$mt = $_GET['mt'];
$currentDate = date('Y-m-d');
/* $day = '0,2,5';
$date = date('Y-m-d');
$week_day = explode(',', $day);
echo "Shubham_Heloo";
print_r($week_day);
$days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
for ($i=0; $i <= 6; $i++) { 
    date('Y-m-d', strtotime($days[$i], strtotime($date))); 
    
} */
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<h4 class="page-title">Upcoming Assets Maintenance</h4>
			</div>
		</div>

		<form class="branchDeptFilter" action="" method="get">
			<div class="row pt-2 pb-2">
				<div class="col-md-3 form-group" >
					<select type="text" name="id" id="id" class="form-control single-select" style="width: 100%">
						<option value="">All Category</option>
						<?php $q12 = $d->select("assets_category_master", "", "order by assets_category ASC");
						while ($row12 = mysqli_fetch_array($q12)) { ?>
						<option value="<?php echo $row12['assets_category_id']; ?>" <?php if ($id == $row12['assets_category_id']) {echo "selected";} ?>><?php echo $row12['assets_category']; ?> </option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3 form-group" >
					<select class="form-control single-select" name="mt">
						<option <?php if(isset($mt) && $mt == 'all'){echo 'selected';} ?> value="all">All Maintenance Type</option>
						<option <?php if(isset($mt) && $mt == '0'){echo 'selected';} ?> value="0">Custom Date</option>
						<option <?php if(isset($mt) && $mt == '1'){echo 'selected';} ?> value="1">Weekly</option>
						<option <?php if(isset($mt) && $mt == '2'){echo 'selected';} ?> value="2">Month Days</option>
						<option <?php if(isset($mt) && $mt == '3'){echo 'selected';} ?> value="3">Monthly</option>
						<option <?php if(isset($mt) && $mt == '4'){echo 'selected';} ?> value="4">Quarterly</option>
						<option <?php if(isset($mt) && $mt == '5'){echo 'selected';} ?> value="5">Half Yearly</option>
						<option <?php if(isset($mt) && $mt == '6'){echo 'selected';} ?> value="6">Yearly</option>
					</select>
				</div>
				<div class="col-md-3 form-group">
					<input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-bordered">
								<thead>
									<tr>
										<th>S. No</th>
										<th>Category</th>
										<th>Item Name</th>
										<th>Maintenance Type</th>
										<th>Maintenance Date</th>
										<th>Vendor Name</th>
										<th>Vendor Mobile</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$counter = 1;
									if (isset($id) && $id > 0) {
										$appendQery = " AND assets_item_detail_master.assets_category_id='$id'";
									}
									if (isset($mt) && $mt != 'all') {
										$typeAppendQery = " AND assets_maintenance_master.maintenance_type='$mt'";
									}
									
									$q = $d->select("assets_category_master,assets_item_detail_master,assets_maintenance_master","assets_category_master.assets_category_id=assets_maintenance_master.assets_category_id AND assets_item_detail_master.assets_id=assets_maintenance_master.assets_id AND assets_maintenance_master.society_id='$society_id' $appendQery $typeAppendQery");
									while ($row = mysqli_fetch_array($q)) {
                                        if($row['maintenance_type'] == 0){ 
                                            $custom_date = date("d F Y", strtotime($row['custom_date'])); 
                                            $end_date = new DateTime($custom_date);
                                            $start_date = new DateTime($currentDate);
                                            $diff=date_diff($start_date,$end_date);
                                            $diff_in_days = $diff->format("%a"); 
                                            
                                            if($diff_in_days < 30 && $row['custom_date'] >= $currentDate){ 
                                            $maintenance_schedule_date = date("Y-m-d", strtotime($custom_date)); 
                                            $isCompleted = $d->count_data_direct("assets_maintenance_complete_id","assets_maintenance_complete","assets_category_id='$row[assets_category_id]' AND assets_id='$row[assets_id]' AND assets_maintenance_id='$row[assets_maintenance_id]' AND maintenance_schedule_date='$maintenance_schedule_date'");
                                            if( $isCompleted==0 ) {?>
                                            <tr>
                                                <td><?php echo $counter++; ?> </td>
                                                <td><?php echo $row['assets_category']; ?> </td>
                                                <td><?php echo $row['assets_name']; ?> </td>
                                                <td><?php echo 'Custom Date'; ?>
                                                </td>
                                                <td>
                                                    <?php echo $custom_date; ?>
                                                </td>
                                                <td><?php echo $row['vendor_name']; ?> </td>
                                                <td><?php echo $row['vendor_mobile_no']; ?> </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <?php if(date('Y-m-d', strtotime($custom_date)) == $currentDate){ ?>
                                                        <button class="btn btn-primary btn-sm" title="Complete Maintenance" onclick="assetsMaintenanceCompleted(`<?php echo $row['assets_category_id'] ?>`, `<?php echo $row['assets_id'] ?>`, `<?php echo $row['assets_maintenance_id'] ?>`, `<?php echo $custom_date; ?>`);"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                           <?php } } 
                                        }
                                        elseif($row['maintenance_type'] == 1){
                                            $week_day = explode(',', $row['days']);

                                            $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
                                            for ($i=0; $i < count($week_day); $i++) { 
                                                $day = $week_day[$i]; 
                                                $to_date = date('Y-m-d');
                                                $weekly_date = date('d F Y', strtotime($days[$day], strtotime($to_date)));
                                                $maintenance_schedule_date = date("Y-m-d", strtotime($weekly_date)); 
                                                $isCompleted = $d->count_data_direct("assets_maintenance_complete_id","assets_maintenance_complete","assets_category_id='$row[assets_category_id]' AND assets_id='$row[assets_id]' AND assets_maintenance_id='$row[assets_maintenance_id]' AND maintenance_schedule_date='$maintenance_schedule_date'");
                                                if( $isCompleted==0 ) {?>
                                                <tr>
                                                    <td><?php echo $counter++; ?> </td>
                                                    <td><?php echo $row['assets_category']; ?> </td>
                                                    <td><?php echo $row['assets_name']; ?> </td>
                                                    <td><?php echo 'Weekly'; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $weekly_date; ?>
                                                    </td>
                                                    <td><?php echo $row['vendor_name']; ?> </td>
                                                    <td><?php echo $row['vendor_mobile_no']; ?> </td>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            <?php if(date('Y-m-d', strtotime($weekly_date)) == $currentDate){ ?>
                                                            <button class="btn btn-primary btn-sm" title="Complete Maintenance" onclick="assetsMaintenanceCompleted(`<?php echo $row['assets_category_id'] ?>`, `<?php echo $row['assets_id'] ?>`, `<?php echo $row['assets_maintenance_id'] ?>`, `<?php echo $weekly_date; ?>`);"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                           <?php } }
                                        }
                                        elseif($row['maintenance_type'] == 2){
                                            $month = date('F Y');
											$month_days = explode(',',$row['days']);
												
                                            for ($i = 0; $i < count($month_days); $i++) {
                                                $month_day = $month_days[$i].' '.$month; 
                                                if(date('Y-m-d', strtotime($month_day)) < $currentDate){
                                                    $month_day = date('d F Y', strtotime($month_day. ' + 1 months'));
                                                }
                                                $maintenance_schedule_date = date("Y-m-d", strtotime($month_day)); 
                                                $isCompleted = $d->count_data_direct("assets_maintenance_complete_id","assets_maintenance_complete","assets_category_id='$row[assets_category_id]' AND assets_id='$row[assets_id]' AND assets_maintenance_id='$row[assets_maintenance_id]' AND maintenance_schedule_date='$maintenance_schedule_date'");
                                                if( $isCompleted==0 ) {?>
                                                <tr>
                                                    <td><?php echo $counter++; ?> </td>
                                                    <td><?php echo $row['assets_category']; ?> </td>
                                                    <td><?php echo $row['assets_name']; ?> </td>
                                                    <td><?php echo 'Month Days'; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $month_day; ?>
                                                    </td>
                                                    <td><?php echo $row['vendor_name']; ?> </td>
                                                    <td><?php echo $row['vendor_mobile_no']; ?> </td>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            <?php if(date('Y-m-d', strtotime($month_day)) == $currentDate){ ?>
                                                            <button class="btn btn-primary btn-sm" title="Complete Maintenance" onclick="assetsMaintenanceCompleted(`<?php echo $row['assets_category_id'] ?>`, `<?php echo $row['assets_id'] ?>`, `<?php echo $row['assets_maintenance_id'] ?>`, `<?php echo $month_day; ?>`);"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                           <?php } }
                                        }
                                        elseif($row['maintenance_type'] == 3){ 
                                            $month = date('F Y');
                                            $month_date = $row['days'].' '.$month; 
                                            if(date('Y-m-d', strtotime($month_date)) < $currentDate){
                                                $month_date = date('d F Y', strtotime($month_date. ' + 1 months'));
                                            }
                                            $maintenance_schedule_date = date("Y-m-d", strtotime($month_date)); 
                                            $isCompleted = $d->count_data_direct("assets_maintenance_complete_id","assets_maintenance_complete","assets_category_id='$row[assets_category_id]' AND assets_id='$row[assets_id]' AND assets_maintenance_id='$row[assets_maintenance_id]' AND maintenance_schedule_date='$maintenance_schedule_date'");
                                            if( $isCompleted==0 ) {?>
                                            <tr>
                                                <td><?php echo $counter++; ?> </td>
                                                <td><?php echo $row['assets_category']; ?> </td>
                                                <td><?php echo $row['assets_name']; ?> </td>
                                                <td><?php echo 'Monthly'; ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    echo $month_date;
                                                    ?>
                                                </td>
                                                <td><?php echo $row['vendor_name']; ?> </td>
                                                <td><?php echo $row['vendor_mobile_no']; ?> </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <?php if(date('Y-m-d', strtotime($month_date)) == $currentDate){ ?>
                                                        <button class="btn btn-primary btn-sm" title="Complete Maintenance" onclick="assetsMaintenanceCompleted(`<?php echo $row['assets_category_id'] ?>`, `<?php echo $row['assets_id'] ?>`, `<?php echo $row['assets_maintenance_id'] ?>`, `<?php echo $month_date; ?>`);"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                           <?php }
                                        }
                                        elseif($row['maintenance_type'] == 4){ 
                                            $date = date('Y').'-'.$row['start_month'].'-'.$row['days'];
                                            if($currentDate <= date('Y-m-d', strtotime($date))){
                                                $monthNum  = $row['start_month'];
                                                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                                $monthName = $dateObj->format('F').' '.date('Y');
                                                $quarterly_date = $row['days'].' '.$monthName;
                                            }else{
                                                $newDate = date('m', strtotime($date. ' + 3 months'));
                                                $monthNum  = $newDate;
                                                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                                $monthName = $dateObj->format('F').' '.date('Y');
                                                $quarterly_date = $row['days'].' '.$monthName;
                                            }

                                            if(date('Y-m-d', strtotime($quarterly_date)) < $currentDate){
                                                $quarterly_date = date('d F Y', strtotime($quarterly_date. ' + 3 months'));
                                            }
                                            $end_date = new DateTime($quarterly_date);
                                            $start_date = new DateTime($currentDate);
                                            $diff=date_diff($start_date,$end_date);
                                            $diff_in_days = $diff->format("%a"); 
                                            if($diff_in_days < 30){
                                            $maintenance_schedule_date = date("Y-m-d", strtotime($quarterly_date)); 
                                            $isCompleted = $d->count_data_direct("assets_maintenance_complete_id","assets_maintenance_complete","assets_category_id='$row[assets_category_id]' AND assets_id='$row[assets_id]' AND assets_maintenance_id='$row[assets_maintenance_id]' AND maintenance_schedule_date='$maintenance_schedule_date'");
                                            if( $isCompleted==0 ) {?>
                                            <tr>
                                                <td><?php echo $counter++; ?> </td>
                                                <td><?php echo $row['assets_category']; ?> </td>
                                                <td><?php echo $row['assets_name']; ?> </td>
                                                <td><?php echo 'Quarterly'; ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    echo $quarterly_date;
                                                    ?>
                                                </td>
                                                <td><?php echo $row['vendor_name']; ?> </td>
                                                <td><?php echo $row['vendor_mobile_no']; ?> </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <?php if(date('Y-m-d', strtotime($quarterly_date)) == $currentDate){ ?>
                                                        <button class="btn btn-primary btn-sm" title="Complete Maintenance" onclick="assetsMaintenanceCompleted(`<?php echo $row['assets_category_id'] ?>`, `<?php echo $row['assets_id'] ?>`, `<?php echo $row['assets_maintenance_id'] ?>`, `<?php echo $quarterly_date; ?>`);"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                           <?php } }
                                        }
                                        elseif($row['maintenance_type'] == 5){ 
                                            $date = date('Y').'-'.$row['start_month'].'-'.$row['days'];
                                            if($currentDate <= date('Y-m-d', strtotime($date))){
                                                $monthNum  = $row['start_month'];
                                                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                                $monthName = $dateObj->format('F').' '.date('Y');
                                                $half_year_date = $row['days'].' '.$monthName;
                                            }else{
                                                $newDate = date('m', strtotime($date. ' + 6 months'));
                                                $monthNum  = $newDate;
                                                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                                $monthName = $dateObj->format('F').' '.date('Y');
                                                $half_year_date = $row['days'].' '.$monthName;
                                            }

                                            if(date('Y-m-d', strtotime($half_year_date)) < $currentDate){
                                                $half_year_date = date('d F Y', strtotime($half_year_date. ' + 6 months'));
                                            }
                                            $end_date = new DateTime($half_year_date);
                                            $start_date = new DateTime($currentDate);
                                            $diff=date_diff($start_date,$end_date);
                                            $diff_in_days = $diff->format("%a"); 
                                            if($diff_in_days<30){
                                            $maintenance_schedule_date = date("Y-m-d", strtotime($half_year_date)); 
                                            $isCompleted = $d->count_data_direct("assets_maintenance_complete_id","assets_maintenance_complete","assets_category_id='$row[assets_category_id]' AND assets_id='$row[assets_id]' AND assets_maintenance_id='$row[assets_maintenance_id]' AND maintenance_schedule_date='$maintenance_schedule_date'");
                                            if( $isCompleted==0 ) {?>
                                            <tr>
                                                <td><?php echo $counter++; ?> </td>
                                                <td><?php echo $row['assets_category']; ?> </td>
                                                <td><?php echo $row['assets_name']; ?> </td>
                                                <td><?php echo 'Half Yearly'; ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    echo $half_year_date;
                                                    ?>
                                                </td>
                                                <td><?php echo $row['vendor_name']; ?>
                                                </td>
                                                <td><?php echo $row['vendor_mobile_no']; ?> </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <?php if(date('Y-m-d', strtotime($half_year_date)) == $currentDate){ ?>
                                                        <button class="btn btn-primary btn-sm" title="Complete Maintenance" onclick="assetsMaintenanceCompleted(`<?php echo $row['assets_category_id'] ?>`, `<?php echo $row['assets_id'] ?>`, `<?php echo $row['assets_maintenance_id'] ?>`, `<?php echo $half_year_date; ?>`);"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                           <?php } }
                                        }
                                        elseif($row['maintenance_type'] == 6){ 
                                            $date = date('Y').'-'.$row['start_month'].'-'.$row['days'];
                                            $monthNum  = $row['start_month'];
                                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                            $monthName = $dateObj->format('F').' '.date('Y');
                                            $year_date = $row['days'].' '.$monthName;

                                            if(date('Y-m-d', strtotime($year_date)) < $currentDate){
                                                $year_date = date('d F Y', strtotime($year_date. ' + 1 year'));
                                            }
                                            $end_date = new DateTime($year_date);
                                            $start_date = new DateTime($currentDate);
                                            $diff=date_diff($start_date,$end_date);
                                            $diff_in_days = $diff->format("%a"); 
                                            if($diff_in_days <30 ){ 
                                            $maintenance_schedule_date = date("Y-m-d", strtotime($year_date)); 
                                            $isCompleted = $d->count_data_direct("assets_maintenance_complete_id","assets_maintenance_complete","assets_category_id='$row[assets_category_id]' AND assets_id='$row[assets_id]' AND assets_maintenance_id='$row[assets_maintenance_id]' AND maintenance_schedule_date='$maintenance_schedule_date'");
                                            if( $isCompleted==0 ) {?>
                                            <tr>
                                                <td><?php echo $counter++; ?> </td>
                                                <td><?php echo $row['assets_category']; ?> </td>
                                                <td><?php echo $row['assets_name']; ?> </td>
                                                <td><?php echo 'Yearly'; ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    echo $year_date;
                                                    ?>
                                                </td>
                                                <td><?php echo $row['vendor_name']; ?> </td>
                                                <td><?php echo $row['vendor_mobile_no']; ?> </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <?php if(date('Y-m-d', strtotime($year_date)) == $currentDate){ ?>
                                                        <button class="btn btn-primary btn-sm" title="Complete Maintenance" onclick="assetsMaintenanceCompleted(`<?php echo $row['assets_category_id'] ?>`, `<?php echo $row['assets_id'] ?>`, `<?php echo $row['assets_maintenance_id'] ?>`, `<?php echo $year_date; ?>`);"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                           <?php } }
                                        }
									?>
									
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="assetsMaintenanceCompletedModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Complete Assets Maintenance</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="completeAssetsMaintenanceForm" action="controller/assetsItemDetailsController.php" name="changeAttendance"  enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Amount <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <input type="text" required class="form-control onlyNumber" name="maintenance_amount" id="maintenance_amount" placeholder="Maintenance Amount">
                        </div>                   
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Remark </label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea class="form-control" name="remark"></textarea>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                        <input type="hidden" name="assets_category_id" id="assets_category_id"  value="">
                        <input type="hidden" name="assets_id"  id="assets_id">
                        <input type="hidden" name="assets_maintenance_id"  id="assets_maintenance_id">
                        <input type="hidden" name="date"  id="date">
                        <input type="hidden" name="completeAssetsMaintenance"  value="completeAssetsMaintenance">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>