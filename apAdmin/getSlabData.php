<?php //IS_1546
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

if (isset($_COOKIE['bms_admin_id'])) {
  $bms_admin_id = $_COOKIE['bms_admin_id'];
}
 
extract(array_map("test_input" , $_POST));

 

if(isset($EditFlg) && $EditFlg=="Yes" && isset($slab_id)){ 

$q=$d->select("gst_master","slab_id='$slab_id'","");
$data=mysqli_fetch_array($q);
extract($data);
?>



  <input type="hidden" name="slab_id" id="slab_id" value="<?php echo $slab_id; ?>">

 <div class="form-group row">
              <label for="slab_percentage" class="col-sm-3 col-form-label">Slab Percentage %<span class="required">*</span></label>
              <div class="col-sm-9 ">
                <input type="text" maxlength="5" class="form-control onlyNumber" inputmode="numeric" required="" name="slab_percentage" id="slab_percentage_edit" value="<?php echo $slab_percentage; ?>">  <span class="m-1 sameBlockEdit text-info">
              </div>
            </div>
            
           
            <div class="row form-group">
              <label for="description" class="col-sm-3 form-control-label">Description</label>
              <div class="col-sm-9">
                <textarea type="text" maxlength="100" class="form-control" name="description" id="description_edit"><?php echo $description; ?></textarea>
              </div>
            </div>
 <script src="assets/js/jquery.min.js"></script>           
 <script type="text/javascript">
  jQuery(document).ready(function($){
   $(".onlyNumber").keydown(function (e) {
     if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
});
 </script>
<?php }  else {
   $_SESSION['msg']="Something Wrong";
    header("Location: ../gstSlab");
} ?>