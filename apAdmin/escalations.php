<?php error_reporting(0);


$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$laYear = (int)$_REQUEST['laYear'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-3 col-md-6 col-6">
        <h4 class="page-title">Escalation</h4>
      </div>

      <div class="col-sm-3 col-md-6 col-6">
        <div class="btn-group float-sm-right">
<!--           <a href="holiday" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
 -->          <a href="javascript:void(0)" onclick="DeleteAll('deleteEscalation');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          <!--               <a href="controller/BulkHolidayController.php"  class="btn btn-sm btn-warning" >Add Last Year</a>
 -->
        </div>

      </div>

    </div>
    <!-- <form action=""  class="">
        <div class=" form-group">
          <select name="laYear" class="form-control" onchange="this.form.submit()">
              <option <?php if ($laYear == $twoPreviousYear) {
                        echo 'selected';
                      } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
              <option <?php if ($laYear == $onePreviousYear) {
                        echo 'selected';
                      } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
              <option <?php if ($laYear == $currentYear) {
                        echo 'selected';
                      } ?> <?php if (isset($laYear) && $laYear == '') {
                                                                              echo 'selected';
                                                                            } ?>  value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
              <option <?php if ($laYear == $nextYear) {
                        echo 'selected';
                      } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
            </select>
        </div>
      </form> -->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <div class="table-responsive">
              <?php //IS_675  example to examplePenaltiesTbl
              ?>
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Sr.No</th>
                    <th>Action</th>
                    <th>Sender</th>
                    <th>Reciver</th>
                    <th>Title</th>
                    <th>Date</th>
                    <th>Reply Date</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i = 1;
                  $idArray = array();


                //  $q = $d->select("escalation_master LEFT JOIN users_master AS sender ON sender.user_id= escalation_master.escalation_by_id ", " escalation_master.society_id='$society_id'");
                  $q = $d->selectRow("escalation_master.*,sender.user_full_name ,reciver.user_full_name AS reciverName","escalation_master LEFT JOIN users_master AS sender ON sender.user_id= escalation_master.escalation_by_id LEFT JOIN users_master AS reciver ON reciver.user_id= escalation_master.escalation_to_id ", " escalation_master.society_id='$society_id' AND escalation_master.escalation_is_delete = 0");
                  $counter = 1;
                  while ($data = mysqli_fetch_array($q)) {
                   /*  $q2 = $d->selectRow('user_full_name AS reciverName',"users_master","user_id='$data[escalation_to_id]'");
                    $data2 = mysqli_fetch_assoc($q2) */
                  ?>
                    <tr>

                      <td class="text-center">
                        <input type="hidden" name="id" id="id" value="<?php echo $data['escalation_id']; ?>">
                        <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['escalation_id']; ?>">
                      </td>
                      <td><?php echo $counter++; ?></td>
                      <td>
                        <div class="d-flex align-items-center">
                            <button type="button" onclick="showEscalationData(<?php echo $data['escalation_id']; ?>)" class="btn btn-sm btn-primary mr-1"> <i class="fa fa-eye"></i></button>

                          <?php if ($data['escalation_active_status'] == 0) {
                            $status = "esclationStatusDeactive";
                            $active = "checked";
                          } else {
                            $status = "esclationStatusActive";
                            $active = "";
                          } ?>
                          <input <?php echo $disabled; ?> type="checkbox" <?php echo $active; ?> class="js-switch mr-1" data-color="#15ca20" onchange="changeStatus('<?php echo $data['escalation_id']; ?>','<?php echo $status; ?>');" data-size="small" />
                        </div>
                      </td>
                      <td><?php echo $data['user_full_name']; ?></td>
                      <td><?php echo $data['reciverName']; ?></td>
                      <td><?php custom_echo($data['escalation_title'],20); ?></td>
                      <td><?php echo  date("d M Y h:i A", strtotime($data['escalation_created_date'])); ?></td>
                      <td><?php if($data['escalation_replay_date'] !="0000-00-00 00:00:00" && $data['escalation_replay_date'] !=null){ echo  date("d M Y h:i A", strtotime($data['escalation_replay_date'])); } ?></td>

                    </tr>
                  <?php } ?>
                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>

<div class="modal fade" id="EscalationData">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Escalation Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">

          <div class="row col-md-12"  id="escalationView">
          </div>

        </div>
      </div>

    </div>
  </div>
</div>




<script type="text/javascript">
  function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
    }
    return false;
  }
</script>