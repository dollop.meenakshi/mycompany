<?php if (isset($_GET['addNew']) && $_GET['addNew'] == "yes") { ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <h4 class="page-title">Add Assets</h4>
        </div>
        <div class="col-lg-4 col-md-6">

        </div>
        <div class="col-lg-4 col-md-2">

        </div>
      </div>
      <div class="row mt-3">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="additemDetails" method="POST" class="form-horizontal" action="controller/assetsItemDetailsController.php" enctype="multipart/form-data">
                <div class="modal-body">
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Category <span class="required">*</span></label>
                    <div class="col-lg-8">
                      <select class="form-control multiple-select" name="assets_category_id" id="assets_category_id">
                        <option value="">Select Category</option>
                        <?php $sql = $d->select("assets_category_master", "", "");
                        while ($data = mysqli_fetch_array($sql)) { ?>
                          <option value="<?= $data['assets_category_id']; ?>">
                            <?= $data['assets_category']; ?>
                          </option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Item Name <span class="required">*</span></label>
                    <div class="col-lg-8">
                      <input type="text" maxlength="200" class="form-control" name="assets_name" id="assets_name" required="" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Brand Name</label>
                    <div class="col-lg-8">
                      <input type="text" maxlength="200" class="form-control" name="assets_brand_name" id="assets_brand_name" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Image</label>
                    <div class="col-lg-8">
                      <input type="file" accept="image/*,.pdf" class="form-control" name="assets_file" id="assets_file" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Location </label>
                    <div class="col-lg-8">
                      <input type="text" class="form-control" name="assets_location" id="assets_location" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Item Code </label>
                    <div class="col-lg-8">
                      <input type="text" class="form-control" name="assets_item_code" id="assets_item_code" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Description </label>
                    <div class="col-lg-8">
                      <textarea class="form-control" name="assets_description" id="assets_description"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Purchase Date</label>
                    <div class="col-lg-8">
                      <input type="text" readonly="" class="form-control autoclose-datepicker-item" id="item_purchase_date" name="item_purchase_date">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Sr. No./ MAC/Sim</label>
                    <div class="col-lg-8">
                      <input type="text" maxlength="200" class="form-control" name="sr_no" id="sr_no" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Price</label>
                    <div class="col-lg-8">
                      <input type="text" maxlength="200" class="form-control onlyNumber" name="item_price" id="item_price" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Invoice</label>
                    <div class="col-lg-8">
                      <input type="file" accept="image/*,.pdf" class="form-control" name="assets_invoice" id="assets_invoice" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Branch</label>
                    <div class="col-lg-8">
                      <select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" >
                        <option value="">-- Select Branch --</option> 
                        <?php 
                        $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                        while ($blockData=mysqli_fetch_array($qb)) {
                        ?>
                        <option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                        <?php } ?>
                      </select>    
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Department</label>
                    <div class="col-lg-8">
                      <select class="form-control single-select" name="floor_id" id="floor_id" onchange="getCustodian();">
                        <option value="">--Select Department--</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Custodian</label>
                    <div class="col-lg-8">
                      <select class="form-control single-select" name="user_id" id="user_id">
                        <option value="">Select Employee</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-form-label">Handover Date</label>
                    <div class="col-lg-8">
                      <input type="text" readonly="" class="form-control autoclose-datepicker-add-item-handover-date" id="start_date" name="start_date">
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="col-md-12 text-center">
                    <input type="hidden" name="submit_assets" id="submit_assets" value="submit_assets" />
                    <button type="submit" class="btn btn-success" id="submit_assets" value="submit_assets">Add</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } else { ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <h4 class="page-title">Employee Assets items</h4>
        </div>
        <div class="col-lg-4 col-md-6">
          <form action="" method="get" id="searchbycatform">
            <select type="text" onchange="this.form.submit();" name="id" id="id" class="form-control single-select" style="width: 100%">
              <option value="">All Category</option>
              <?php $q12 = $d->select("assets_category_master", "", "order by assets_category ASC");
              while ($row12 = mysqli_fetch_array($q12)) { ?>
                <option value="<?php echo $row12['assets_category_id']; ?>" <?php if (isset($_REQUEST) && isset($_REQUEST['id'])) {
                  if ((int)$_REQUEST['id'] == $row12['assets_category_id']) {  echo "selected"; }
                    } ?>><?php echo $row12['assets_category']; ?> </option>
              <?php } ?>
            </select>
          </form>
        </div>
        <div class="col-lg-4 col-md-2">
          <div class="btn-group float-sm-right">
            <a href="itemDetails?addNew=yes" class="btn btn-sm btn-primary">
              +ADD
            </a>
            <a href="printAssetsQRCode.php?&type=qrcode" target="_blank" class="btn btn-sm btn-secondary ml-1" title="QR Code"><i class="fa fa-qrcode"></i> Print QR Code</a>
          </div>
        </div>
      </div>
      <div class="row mt-3">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Action</th>
                      <th>Category</th>
                      <th>Item Name</th>
                      <th>Brand </th>
                      <th>Item Image</th>
                      <th>Purchase Date</th>
                      <th>Custodian</th>
                      <th>Sr. No./MAC/Sim</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i1 = 1;
                    if (isset($_GET['id']) && $_GET['id'] > 0 && filter_var($_GET['id'], FILTER_VALIDATE_INT) == true) {
                      $appendQery = " AND assets_item_detail_master.assets_category_id='$_GET[id]'";
                    }

                    if ($access_branchs!="") {
                        $q = $d->select("assets_item_detail_master,assets_detail_inventory_master,users_master,assets_category_master","assets_detail_inventory_master.assets_id = assets_item_detail_master.assets_id AND assets_detail_inventory_master.end_date='0000-00-00' AND users_master.user_id =assets_detail_inventory_master.user_id AND assets_item_detail_master.assets_category_id=assets_category_master.assets_category_id AND assets_item_detail_master.move_to_scrap=0  $appendQery $blockAppendQueryUser", "ORDER BY assets_item_detail_master.assets_id DESC");
                    } else {

                    $q = $d->select1("assets_item_detail_master LEFT JOIN assets_detail_inventory_master ON assets_detail_inventory_master.assets_id = assets_item_detail_master.assets_id AND assets_detail_inventory_master.end_date='0000-00-00' LEFT JOIN users_master ON users_master.user_id =assets_detail_inventory_master.user_id ,assets_category_master","assets_item_detail_master.*,assets_category_master.*,users_master.user_full_name,users_master.user_designation,assets_detail_inventory_master.user_id,assets_detail_inventory_master.inventory_id" , "assets_item_detail_master.assets_category_id=assets_category_master.assets_category_id AND assets_item_detail_master.move_to_scrap=0  $appendQery ", "ORDER BY assets_item_detail_master.assets_id DESC");
                    } 
                    while ($row = mysqli_fetch_array($q)) {
                    ?>
                      <tr>
                        <td><?php echo $i1++; ?> </td>
                        <td>
                          <div class="d-flex align-items-center">
                            <form action="assetsInventory" method="get">
                              <input type="hidden" name="id" value="<?php echo $row['assets_id']; ?>">
                              <button class="btn btn-warning btn-sm " title="Summary"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            </form>
                          
                            <button type="button" title="Edit" class="btn btn-sm btn-primary ml-1 editItemsDetails" data-toggle="modal" data-target="#editItemsDetails" onclick="editItemsDetails(<?php echo $row['assets_id']; ?>,<?php echo $society_id; ?>)">
                              <i class="fa fa-edit"></i>
                            </button>
                            <?php if($row['user_full_name'] == ''){ ?>
                            <button class="btn btn-sm btn-info ml-1" title="Move To Scrap" onclick="moveToScrap(<?php echo $row['assets_id']; ?>)"><i class="fa fa-minus-circle"></i></button>

                            <button class="btn btn-sm btn-success  ml-1" title="Sold Out" onclick="itemSoldOut(<?php echo $row['assets_id']; ?>)"><i class="fa fa-tags"></i></button>
                            <?php } ?>
                            <a href="printAssetsQRCode.php?assets_id=<?php echo $row['assets_id']; ?>&type=qrcode" class="btn btn-sm btn-secondary ml-1" target="_blank" title="QR Code"><i class="fa fa-qrcode"></i></a>

                          </div>
                          <?php if($row['user_full_name'] != ''){ ?>
                            <a href="assetsInventory?id=<?php echo $row['assets_id']; ?>&inventory_id=<?php echo $row['inventory_id']; ?>&custodian=takeover" class="btn btn-sm btn-success mt-2">Takeover Assets</a>
                          <?php }else{ ?> 
                            <a href="assetsInventory?id=<?php echo $row['assets_id']; ?>&custodian=handover" class="btn btn-sm btn-success mt-2">Handover Assets</a>
                          <?php } ?>
                        </td>
                        <td><?php echo $row['assets_category']; ?> </td>
                        <td><?php echo $row['assets_name']; ?> </td>
                        <td><?php echo $row['assets_brand_name']; ?> </td>
                        <td class="text-center align-middle">
                          <?php if($row['assets_file'] != ''){ 
                            if(file_exists("../img/society/$row[assets_file]")) {
                            ?>
                            <a data-fancybox="images" data-caption="Photo Name : <?php echo $row['assets_file'] ?>" href="../img/society/<?php echo $row['assets_file'] ?>" target="_blank">
                            <img style="height:70px; width:70px; object-fit: cover;" class="lazyload" onerror="this.src='../img/banner_placeholder.png'" src='../img/banner_placeholder.png' id="<?php echo $row['assets_id']; ?>" data-src="../img/society/<?php echo $row['assets_file']; ?>"></a>
                          <?php } } ?>
                        </td>
                        <td>
                          <?php if ($row['item_purchase_date'] != "0000-00-00") {
                            echo date("d M Y", strtotime($row['item_purchase_date']));
                          } ?>
                        </td>
                        <td>
                          <?php if($row['user_full_name']!="") { echo $row['user_full_name'].'<br>('.$row['user_designation'] .')'; } ?>
                        </td>
                         <td>
                          <?php if($row['sr_no']!="") { echo $row['sr_no']; } ?>
                        </td>
                        
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

<!-- --------------Edit Data-------------------- -->
<div class="modal fade" id="editItemsDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white" id="exampleModalLabel">Edit Assets Item</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <span id="edit_items"></span>
    </div>
  </div>
</div>


<div class="modal fade" id="moveToScrapReasonModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Move To Scrap Reason</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form action="controller/assetsItemDetailsController.php"  enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Reason </label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea class="form-control" placeholder="Reason" name="move_to_scrap_reason"></textarea>
                        </div>                   
                    </div>     
                    <div class="form-footer text-center">
                      <input type="hidden" value="delete_assets" name="delete_assets">
                      <input type="hidden" name="assets_id"  class="assets_id">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="soldOutReasonAndPriceModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Sold Out</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="assetsSoldOutForm" action="controller/assetsItemDetailsController.php"  enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Reason </label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea class="form-control" placeholder="Reason" name="move_to_scrap_reason"></textarea>
                        </div>                   
                    </div>        
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Sold Out Price <span class="required">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <input class="form-control onlyNumber" placeholder="Sold Out Price" name="sold_out_price">
                        </div>                   
                    </div>     
                    <div class="form-footer text-center">
                      <input type="hidden" name="sold_out_assets" value="sold_out_assets">
                      <input type="hidden" name="assets_id"  class="assets_id">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>



<script>
  function onerrorFunction(val, id) {
    console.log(val);
    if (val != "") {
      // $('#'+id).attr('src','../img/ajax-loader.gif');
    }
  }
</script>