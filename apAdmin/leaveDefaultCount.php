<?php

error_reporting(0);

$lgId = (int)$_REQUEST['lgId'];
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$year = (int)$_REQUEST['year'];
$ltId = (int)$_REQUEST['ltId'];
$type = $_REQUEST['type'];
$i = 1;

if (isset($lgId) && $lgId > 0) {
    $leaveGroupCategory = "AND leave_default_count_master.leave_group_id='$lgId' ";
}

$q = $d->select("leave_group_master,leave_default_count_master,leave_type_master", "leave_group_master.leave_group_id=leave_default_count_master.leave_group_id AND leave_default_count_master.leave_type_id= leave_type_master.leave_type_id AND leave_default_count_master.society_id='$society_id' $leaveGroupCategory", "ORDER BY leave_default_count_master.leave_default_count_id DESC");
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title">Leave Group</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6 text-right">
                <a href="addLeaveDefaultCount" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Group</a>
                <!-- <a href="javascript:void(0)" onclick="DeleteAll('deleteSalaryCommonValue');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
            </div>
        </div>
        <form action="" class="branchDeptFilter">
            <div class="row pt-2 pb-2">
                <div class="col-md-6 form-group">
                    <select name="lgId" class="form-control single-select">
                        <option value="">Select Group</option>
                        <?php $qs = $d->select("leave_group_master", "");
                        while ($sgData = mysqli_fetch_array($qs)) { ?>
                            <option <?php if (isset($sgData) && $sgData['leave_group_id'] == "$lgId") {
                                        echo "selected";
                                    } ?> value="<?php echo $sgData['leave_group_id']; ?>"><?php echo $sgData['leave_group_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-1 form-group ">
                    <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
        <?php if (isset($lgId) && $lgId > 0 && $lgId != "" && (mysqli_num_rows($q) > 0)) { ?>
            <div class="row  ">
                <div class="col-md-12 text-right">
                    <a href="leaveDefaultCount"><button class="btn btn-sm btn-warning   mb-2"><i class="fa fa-eye"></i> View All Group</button></a>
                    <a href="addLeaveDefaultCount?lgId=<?php echo $lgId; ?>"><button class="btn btn-sm btn-success   mb-2"><i class="fa fa-pencil"></i> Edit This Group</button></a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <?php
                                $counter = 1;
                                if (isset($lgId) && $lgId > 0) {
                                ?>
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>Action</th>
                                                <th>Type</th>
                                                <th>Leaves</th>
                                                <th>Use in Month</th>
                                                <th>Carry Forward <br> To Next Year</th>
                                                <th>Pay Out</th>
                                                <th>Leave Calculation</th>
                                                <th>Remark</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while ($data = mysqli_fetch_array($q)) { ?>
                                                <tr>

                                                    <td><?php echo $counter++; ?></td>
                                                     <td>
                                                        <div class="d-flex align-items-center">
                                                            
                                                            <?php if ($data['active_status'] == "0") {
                                                                $status = "leaveDefaultCountDeactive";
                                                                $active = "checked";
                                                            } else {
                                                                $status = "leaveDefaultCountActive";
                                                                $active = "";
                                                            } ?>
                                                            <input type="checkbox" <?php echo $active; ?> class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['leave_default_count_id']; ?>','<?php echo $status; ?>');" data-size="small" />
                                                        </div>
                                                    </td>
                                                    <td><?php echo $data['leave_type_name']; ?></td>
                                                    <td><?php echo $data['number_of_leaves']; ?></td>
                                                    <td><?php echo $data['leaves_use_in_month']; ?></td>
                                                    <td><?php if($data['carry_forward_to_next_year']==0) {  echo "Yes (Min:".$data['min_carry_forward'].', Max:'.$data['max_carry_forward'].')';} else { echo "No";} ?></td>
                                                    <td><?php if($data['pay_out']==0) {  echo "Yes";} else { echo "No";} ?></td>
                                                    <td><?php if($data['leave_calculation']==0) {  echo "Manual";} else { echo "Average";} ?></td>
                                                    <td><?php echo $data['pay_out_remark']; ?></td>
                                                   

                                                </tr>
                                            <?php } ?>
                                        </tbody>

                                    </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End Row-->

        <?php } else { ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <?php
                                $counter = 1;
                                ?>
                                <table id="example" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Group Id</th>
                                            <th>Leave Group Name</th>
                                            <th>Leave Types in Group</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $q1 = $d->selectRow(
                                            "leave_group_master.*,(SELECT COUNT(*) FROM leave_default_count_master WHERE leave_default_count_master.leave_group_id = leave_group_master.leave_group_id AND leave_group_master.society_id='$society_id') AS total_group_used",
                                            "leave_group_master,leave_default_count_master",
                                            "leave_default_count_master.leave_group_id = leave_group_master.leave_group_id",
                                            "GROUP BY leave_group_master.leave_group_id");
                                        while ($data = mysqli_fetch_array($q1)) { ?>
                                            <tr>

                                                <td><?php echo $counter++; ?></td>
                                                <td><?php echo "L" . $data['leave_group_id']; ?></td>
                                                <td><?php echo $data['leave_group_name']; ?></td>
                                                <td><?php echo $data['total_group_used']; ?></td>
                                                <td>
                                                    <form method="post" action="">
                                                        <a href="leaveDefaultCount?lgId=<?php echo $data['leave_group_id']; ?>" onclick="ShowCommonValue(<?php echo $data['leave_default_count_id']; ?>)" class="btn btn-sm btn-warning mr-2"> <i class="fa fa-eye"></i> Manage</a>

                                                        <?php
                                                        if ($data['leave_group_active_status'] == "0") {
                                                            $status = "leaveGroupDeactive";
                                                            $active = "checked";
                                                        } else {
                                                            $status = "leaveGroupActive";
                                                            $active = "";
                                                        }
                                                        ?>
                                                        <input type="checkbox" <?php echo $active; ?> class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['leave_group_id']; ?>','<?php echo $status; ?>');" data-size="small" />

                                                        <input type="hidden" name="delete_group_name" value="<?php echo $data['leave_group_name']; ?>">
                                                        <input type="hidden" name="delete_leave_group_id" value="<?php echo $data['leave_group_id']; ?>">
                                                        <input type="hidden" name="deleteLeaveGroup" value="deleteLeaveGroup">
                                                        <?php if ($data['total_group_used'] < 1) { ?>
                                                            <button class="btn btn-danger btn-sm form-btn" type="submit"><i class="fa fa-trash-o"></i></button>
                                                        <?php } ?>
                                                    </form>
                                                </td>

                                            </tr>
                                        <?php } ?>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End Row-->

        <?php } ?>

    </div>
    <!-- End container-fluid-->

</div>

<div class="modal fade" id="leaveGroupModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Leave Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="billPayDiv" style="align-content: center;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Leaves Group Name</label>
                                <div class="col-lg-12 col-md-12">
                                    <p id="leave_group_name" name="leave_group_name"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Leaves Type</label>
                                <div class="col-lg-12 col-md-12">
                                    <p name="leave_type" id="leave_type"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Number of Leaves</label>
                                <div class="col-lg-12 col-md-12">
                                    <p name="number_of_leaves" id="number_of_leaves"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Leaves Use In Month</label>
                                <div class="col-lg-12 col-md-12">
                                    <p name="getLeaves_use_in_month" id="getLeaves_use_in_month"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Carry Forward</label>
                                <div class="col-lg-12 col-md-12">
                                    <p id="carry_forward_to_next_year" name="carry_forward_to_next_year">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 carry_forward">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Maximum Carry Forward</label>
                                <div class="col-lg-12 col-md-12">
                                    <p name="max_carry_forward" id="max_carry_forward"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 carry_forward">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Minimum Carry Forward</label>
                                <div class="col-lg-12 col-md-12">
                                    <p name="min_carry_forward" id="min_carry_forward"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Pay Out</label>
                                <div class="col-lg-12 col-md-12">
                                    <p name="pay_out" id="pay_out"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 leave_calculation">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Leave Calculation </label>
                                <div class="col-lg-12 col-md-12">
                                    <p name="leave_calculation" id="leave_calculation"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-sm-12 col-form-label">Remark</label>
                                <div class="col-lg-12 col-md-12">
                                    <p id="pay_out_remark" name="pay_out_remark">Per Month</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom16.js"></script>
<script src="assets/js/custom17.js"></script>
<script type="text/javascript" src="assets/js/select.js"></script>

<script type="text/javascript">
    function popitup(url) {
        newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    }

    function setUseInMonthLeaves() {
        $('#getLeaves_use_in_month').addClass('d-none');
        value = $('#number_of_leaves').val();
        use_in_month_content = ``;
        max_carry_forward_content = ``;
        min_carry_forward_content = ``;
        month_use_value = value >= 31 ? 31 : value;
        var x = 1;
        for (var i = (x - 0.5); i <= month_use_value; i += 0.5) {
            use_in_month_content += `<option value="` + i + `">` + i + `</option>`;
        }
        for (var j = value; j >= 0.5; j -= 0.5) {
            max_carry_forward_content += `<option value="` + j + `">` + j + `</option>`;
        }
        for (var k = (x - 0.5); k <= value; k += 0.5) {
            min_carry_forward_content += `<option value="` + k + `">` + k + `</option>`;
        }

        if (value >= 12) {
            $('.leaveCalculatio').removeClass('d-none');
        } else {
            $('.leaveCalculatio').addClass('d-none');
        }

        $('#leaves_use_in_month').html(use_in_month_content);
        $('#max_carry_forward').html(max_carry_forward_content);
        $('#min_carry_forward').html(min_carry_forward_content);
    }

    // function leaveCalculation(value, id) {
    //     if (value == 1) {
    //         $('.averageCalculation' + id).removeClass('d-none');
    //     } else {
    //         $('.averageCalculation' + id).addClass('d-none');
    //     }
    // }

    // function showAverageLeaveCalculation(id) {
    //     var number_of_leaves = $('#number_of_leaves_' + id).val();
    //     var leaves_use_in_month = $('#leaves_use_in_month_' + id).val();
    //     var avg_leave = number_of_leaves / 12;
    //     var total_leave_till_month = avg_leave * 4;
    //     var applicable_leave_till = total_leave_till_month - 1;
    //     if (applicable_leave_till > leaves_use_in_month) {
    //         applicable_paid_leave = leaves_use_in_month;
    //     } else {
    //         applicable_paid_leave = applicable_leave_till;
    //     }
    //     const el = document.createElement('div')
    //     el.innerHTML = `<div>
    //                   <h5 class="text-center">Here's a Average Leave Calculation Example</h5>
    //                   <p class="text-left">
    //                       Number of leaves = ` + number_of_leaves + ` <br>
    //                       Use in Month = ` + leaves_use_in_month + ` <br>
    //                       Previous Taken Paid Leave = 1 <br>
    //                       Leave Date = 30 April ` + current_year + ` <br>
    //                       Leave Month Number = 04 (April=04) <br>
    //                       ` + number_of_leaves + `/12 = ` + avg_leave + ` (Average Month Leave) <br>
    //                       ` + avg_leave + ` * 04 = ` + total_leave_till_month + ` (Total Leave Till Leave Month) <br>
    //                       ` + total_leave_till_month + ` - 1 = ` + applicable_leave_till + ` (Applicable Leave Till Leave Month) <br><br>
    //                       CASE 1: Applicable Leave Till Leave Month(` + applicable_leave_till + `) is greater than and equal to Use in Month(` + leaves_use_in_month + `) <br>
    //                       THEN, Applicable Paid Leave = ` + leaves_use_in_month + ` <br><br>
    //                       CASE 2:  Applicable Leave Till Leave Month(` + applicable_leave_till + `) is Less than Use in Month(` + leaves_use_in_month + `) <br>
    //                       THEN, Applicable Paid Leave = ` + applicable_leave_till + ` <br>
    //                       <br></p>
    //                       <p class="text-success"> Applicable Paid Leave = ` + applicable_paid_leave + `</p>
    //               </div>`
    //     swal({
    //         content: el,
    //     });
    // }

    function carryForward() {
        value = $('#carry_forward_to_next_year').val();
        if (value == 0) {
            $('.carryForward').removeClass('d-none');
        } else {
            $('.carryForward').addClass('d-none');
        }
    }

    function setMinCarryForward() {
        value = $('#max_carry_forward').val();
        min_carry_forward_content = ``;
        var x = 1;
        for (var l = (x - 0.5); l < value; l += 0.5) {
            min_carry_forward_content += `<option value="` + l + `">` + l + `</option>`;
        }
        $('#min_carry_forward').html(min_carry_forward_content);
    }
</script>
<style>
    .ernDedData {
        display: none;
    }
</style>