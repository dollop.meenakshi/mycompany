<?php 
  extract($_REQUEST);
$week_days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

 if(isset($_GET['pendingUser'])  && $_GET['pendingUser']=='yes')   {  ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    
    <!-- End Breadcrumb-->
     <?php 
        $nq=$d->select("users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND users_master.delete_status=0 AND block_master.block_id=unit_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.member_status=0 AND users_master.user_status=0 AND users_master.society_id='$society_id' $blockAppendQueryUser","ORDER BY users_master.user_id DESC ");
        echo "<h6>Employee Pending Request</h6>";
        if (mysqli_num_rows($nq)>0) {
        
      ?>
      <div class="row">
        <?php 
        while ($newUserData=mysqli_fetch_array($nq)) {
          extract($newUserData);
         ?>
        <div class="col-lg-2 col-6">
         <a href="pendingUser?id=<?php echo $newUserData['user_id']; ?>">
          <div class="card pt-3">
           
          <img height="160" width="160" src="img/user.png" onerror="this.src='img/user.png'" data-src="../img/users/recident_profile/<?php echo $newUserData['user_profile_pic']; ?>" class="rounded-circle shadow lazyload m-auto " alt="Card image cap">
          <div class="p-2 text-center">
            <h5 class="mb-0 mt-2"> <?php custom_echo($newUserData['user_full_name'],15); ?></h5>
            <?php if($newUserData['user_mobile']>0) { ?>
            <p class="text-dark"><?php custom_echo($newUserData['country_code'].' '.$newUserData['user_mobile'],16); ?></p>
            <?php } ?>
           
            <?php 
              echo "<span class='badge bg-primary text-white'>".$newUserData['floor_name']; ?>-<?php echo $newUserData['block_name']."</span>";
             ?>
          </div>
          <ul class="list-group list-group-flush list shadow-none">
            <?php
                $qq=$d->select("users_master","unit_id='$unit_id' AND society_id='$society_id' AND user_type=1 AND user_status=0");
                $tenData=mysqli_fetch_array($qq);
                if ($tenData>0 && $user_type==0) {
                  $tId=$tenData['user_id'];
                  echo "<li class='list-inline-item text-center pb-1'>";
                   echo  "Approve/Reject Tenant Request First";
                   echo "</li>";
                } else { ?>
            <li class="list-inline-item text-center pb-1">
              <div style="display: inline-block;">
              <form action="controller/userController.php" method="post">
              
              <input type="hidden" name="approve_user_id" value="<?php echo $user_id; ?>">
              <input type="hidden" name="owner_id" value="<?php echo $ownerData['user_id']; ?>">
              <input type="hidden" name="owner_name" value="<?php echo $ownerData['user_full_name']; ?>">
              <input type="hidden" name="owner_country_code" value="<?php echo $ownerData['country_code']; ?>">
              <input type="hidden" name="owner_mobile" value="<?php echo $ownerData['user_mobile']; ?>">

              <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
              <input type="hidden" name="country_code" value="<?php echo $country_code; ?>">
              <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
              <input type="hidden" name="unit_status" value="<?php echo $user_type; ?>">
              <input type="hidden" name="unit_name" value="<?php echo $floor_name; ?>-<?php echo $block_name; ?>">
              <input type="hidden" name="user_full_name" value="<?php echo $user_full_name ?>">
              <input type="hidden" name="user_mobile" value="<?php echo $user_mobile ?>">
              <button type="submit" class="btn btn-primary btn-sm form-btn" >Approve <i class="fa fa-check-square-o"> </i></button>
            </form>
             </div>
            <div style="display: inline-block;">
            <form action="controller/userController.php" method="post">
              <input type="hidden" name="unit_name" value="<?php echo $floor_name.'-'.$block_name; ?>">
              <input type="hidden" name="device" value="<?php echo $device; ?>">
              <input type="hidden" name="user_token" value="<?php echo $user_token; ?>">
               <input type="hidden" name="unit_name" value="<?php echo $floor_name; ?>-<?php echo $block_name; ?>">
              <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
              <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
              <input type="hidden" name="user_full_name" value="<?php echo $user_full_name; ?>">
              <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
              <input type="hidden" name="rejectUserId" value="rejectUserId">
              <button type="submit" name="" value="" class="btn btn-danger btn-sm form-btn" >Reject <i class="fa fa-times"></i></button>
            </form>
             
           
              
            </div>
            
          </li>
              <?php }?>
          </ul>
          <div class="text-center p-2 border-0 <?php if ($newUserData['user_type']==1){ echo 'bg-warning text-white'; } else { echo 'bg-success text-white'; }?>">
              <?php custom_echo($newUserData['user_designation'],17);?>
              
              <div class="card-action">
             </div>
            </div>
          
          </div>

          </a>
        </div>
        <?php }?>
      </div>
      <?php }  else {
         echo "<img width=250 src='img/no_data_found.png'>";
    } ?>

  </div>
</div>
<?php } else {
  if(filter_var($id, FILTER_VALIDATE_INT) != true){
  $_SESSION['msg1']='Invalid User';
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='owners';
      </script>");
}
  $qq=$d->select("users_master,unit_master,block_master,floors_master","users_master.user_id='$id' AND unit_master.unit_id=users_master.unit_id AND block_master.block_id=floors_master.block_id AND floors_master.floor_id=unit_master.floor_id");
  $userData=mysqli_fetch_array($qq);
  if ($userData>0) {
  extract($userData);

  $q1=$d->select("user_employment_details","user_id='$user_id'");
  $proData=mysqli_fetch_array($q1);

  $userType =$unit_status;
  $user_type =$user_type;
  if($user_status==1) {
     echo ("<script LANGUAGE='JavaScript'>
    window.location.href='viewOwner?id=$id';
    </script>");
     exit();
  }

   if ($_COOKIE['society_type']==1) {
       $q1=$d->select("user_employment_details","user_id='$id'");
       $proData=mysqli_fetch_array($q1);
    } 



?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Pending  <?php echo $floor_name; ?>-<?php echo $block_name; ?> </h4>
      </div>
    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-4">
        <div class="card profile-card-2">
          <div class="card-img-block">
            <img class="img-fluid" src="img/31.jpg" alt="Card image cap">
          </div>
          <div class="card-body pt-5">
            <?php if($user_profile_pic!="") { ?>
              <a href="../img/users/recident_profile/<?php echo $user_profile_pic;?>" data-fancybox="images" data-caption="Photo Name : Profile Photo ?>"> 
              <img id="blah"  onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $user_profile_pic; ?>"  width="75" height="75"   src="#" alt="your image" class='profile' /></a>
              <?php } else { ?>
            <img id="blah"  onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $user_profile_pic; ?>"  width="75" height="75"   src="#" alt="your image" class='profile' />
            <?php } ?>
            <h5 class="card-title"><?php echo $user_full_name; ?></h5>
            <p class="card-text"><?php echo $_COOKIE['society_name']; ?></p>

          </div>

          <div class="card-body border-top">
            <?php if($user_mobile>0) { ?>
            <div class="media align-items-center">
             <div>
               <i class="fa fa-mobile"></i>
             </div>
             <div class="media-body text-left">
               <div class="progress-wrapper">
                 <?php echo $country_code.' '.$user_mobile; ?>
               </div>                   
             </div>
            </div>
            <hr>
            <?php } ?>
            <div class="media align-items-center">
             <div>
               <i class="fa fa-envelope"></i>
             </div>
             <div class="media-body text-left">
               <div class="progress-wrapper">
                <?php echo custom_echo($user_email,28); ?>
              </div>                   
            </div>
            </div>

            <hr>
            <form style="float: left; margin-right: 5px;"  action="controller/userController.php" method="post">
              
              <input type="hidden" name="approve_user_id" value="<?php echo $user_id; ?>">
              <input type="hidden" name="owner_id" value="<?php echo $ownerData['user_id']; ?>">
              <input type="hidden" name="owner_id" value="<?php echo $ownerData['user_id']; ?>">
              <input type="hidden" name="owner_country_code" value="<?php echo $ownerData['country_code']; ?>">
               <input type="hidden" name="owner_name" value="<?php echo $ownerData['user_full_name']; ?>">
              <input type="hidden" name="owner_mobile" value="<?php echo $ownerData['user_mobile']; ?>">
              <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
              <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
              <input type="hidden" name="country_code" value="<?php echo $country_code; ?>">
              <input type="hidden" name="unit_status" value="<?php echo $user_type; ?>">
              <input type="hidden" name="unit_name" value="<?php echo $floor_name; ?>-<?php echo $block_name; ?>">
              <input type="hidden" name="user_full_name" value="<?php echo $userData['user_full_name'] ?>">
              <input type="hidden" name="user_mobile" value="<?php echo $userData['user_mobile'] ?>">
              <button type="submit" class="btn btn-success form-btn" >Approve </button>
            </form>
            <form action="controller/userController.php" method="post">
              <input type="hidden" name="unit_name" value="<?php echo $floor_name.'-'.$block_name; ?>">
              <input type="hidden" name="device" value="<?php echo $device; ?>">
              <input type="hidden" name="user_token" value="<?php echo $user_token; ?>">
               <input type="hidden" name="unit_name" value="<?php echo $floor_name; ?>-<?php echo $block_name; ?>">
              <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
              <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
              <input type="hidden" name="user_full_name" value="<?php echo $userData['user_full_name'] ?>">
              <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
              <input type="hidden" name="rejectUserId" value="rejectUserId">
              <button type="submit" name="" value="" class="btn btn-danger form-btn" >Reject </button>
            </form>
          </div>
        </div>
      </div>

      <div class="col-lg-8">
        <div class="card">
          <div class="card-body">
           
            <div class="tab-pane active" id="edit">
              <form id="pendingValidation" action="#" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                  <label class="col-lg-3 col-form-label form-control-label">Branch </label>
                  <div class="col-lg-9">
                    <?php echo $block_name; ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-form-label form-control-label">Department </label>
                  <div class="col-lg-9">
                    <?php echo $floor_name; ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-form-label form-control-label">Joining Date </label>
                  <div class="col-lg-9">
                    <?php if($proData['joining_date'] !="" && $proData['joining_date'] !="0000-00-00"){
                            echo $proData['joining_date']; 

                          }?>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-form-label form-control-label">Designation </label>
                  <div class="col-lg-9">
                    <?php echo $user_designation; ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-form-label form-control-label">First Name </label>
                  <div class="col-lg-9">
                    <?php echo $user_first_name; ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-form-label form-control-label">Last Name </label>
                  <div class="col-lg-9">
                    <?php echo $user_last_name; ?>
                  </div>
                </div>
                 
                <div class="form-group row">
                  <label class="col-lg-3 col-form-label form-control-label">Mobile </label>
                  <div class="col-lg-9">
                   <?php echo $country_code; ?> <?php echo $user_mobile; ?>
                 </div>
               </div>

               <div class="form-group row">
                <label class="col-lg-3 col-form-label form-control-label">Email </label>
                <div class="col-lg-9">
                 <?php echo $user_email; ?>
                </div>
              </div>
               <div class="form-group row">
                <label class="col-lg-3 col-form-label form-control-label">Gender</label>
                <div class="col-lg-9">
                  <?php echo $gender; ?>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-form-label form-control-label">Shift Time</label>
                <div class="col-lg-9">
                  <?php   $shiftq=$d->select("shift_timing_master","society_id='$_COOKIE[society_id]'  AND is_deleted = 0 AND shift_time_id='$shift_time_id'");
                      if (mysqli_num_rows($shiftq)>0) {
                         
                      $shiftData=mysqli_fetch_array($shiftq);

                      $arData = array();
                      for($i=0; $i<count($week_days); $i++){
                        if($shiftData['week_off_days'] !="")
                        {
                          $week_off_days = explode(',',$shiftData['week_off_days']);
                          if(in_array($i, $week_off_days)){
                            array_push($arData,$week_days[$i]);
                          } 
                        }
                      }
                      if(!empty($arData)){
                        $showOff = implode(',',$arData);
                      }
                      else
                      {
                        $showOff = "";
                      }

                      echo $shiftData['shift_name'] .' (S'.$shiftData['shift_time_id'];?>) <?php if($showOff !="") {echo "("  .$showOff.")"; } 
                    } else {
                      echo "Shift Not Assigned";
                    }
                  ?>
                </div>
              </div>
              
              
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>

        <!-- Agreement Section -->


       
        
  </div>
</div>
<?php } else{ ?>
<div class="content-wrapper">
  <div class="container-fluid text-center">
    <img width="250" src='img/no_data_found.png'>
  </div>
</div>
<?php } ?>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });
</script>


<?php } ?>
