<?php
extract($_POST);
session_start();
// error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

include 'common/checkLanguage.php';
$qb=$d->select("emergemcy_number_list","emergency_id='$emergency_id' ");
$data=mysqli_fetch_array($qb);
extract($data);
?>
    <form id="emergencyNumberEdit" action="controller/emerController.php" method="post">
       <input type="hidden" name="emergency_id" id="emergency_id" value="<?php echo $emergency_id; ?>">
     <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>">
        <div class="form-group row">
            <label for="inputTextBox" class="col-sm-4 col-form-label"><?php echo $xml->string->name; ?> <span class="text-danger">*</span></label>
            <div class="col-sm-8" id="parkingName1">
              <input type="text" id="onlyName" maxlength="20" name="name_edit" value="<?php echo $name; ?>" required="" class="form-control text-capitalize onlyName" >
            </div>
        </div>

        <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->description; ?> <span class="text-danger">*</span></label>
            <div class="col-sm-8" >
              <input type="text" maxlength="20"  value="<?php echo $designation; ?>" name="designation" required class="form-control  onlyName">
            </div>
        </div>
         <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->emergency_number; ?> <span class="text-danger">*</span></label>
            <div class="col-sm-8" >
              <input type="text"  value="<?php echo $mobile; ?>" maxlength="15" minlength="3" id="editMobile1" name="mobile" required class="form-control onlyNumber">
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->blocks; ?> <span class="text-danger">*</span></label>
            <div class="col-sm-8" id="PaybleAmount">
              <select    name="city_id" id="city_id" required class="form-control ">
                <option value="0"> All <?php echo $xml->string->blocks; ?></option>
                <?php $q = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_sort ASC");
                 while ($row=mysqli_fetch_array($q)) { ?>
                <option <?php if($row['block_id']==$city_id) { echo 'selected'; } ?>  value="<?php echo $row['block_id']; ?>"><?php echo $row['block_name']; ?></option>
                <?php } ?>
              </select>
            </div>
        </div>

        <div class="form-footer text-center">
          <input type="hidden" name="addEmergency" value="allocateParking">
          <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->update; ?></button>
        </div>
  </form>

<!-- <script src="assets/js/jquery.min.js"></script> -->
<!-- <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script> -->
<script type="text/javascript">

 $(".onlyNumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});


$().ready(function() {

  $.validator.addMethod("noSpace", function(value, element) {
        return value == '' || value.trim().length != 0;
    }, "No space please and don't leave it empty");

   $.validator.addMethod("alphaRestSpeChartor", function(value, element) {
        return this.optional(element) || value == value.match(/^[A-Za-z 0-9\d=!,\n\-@&()/?%._*]*$/);
    }, jQuery.validator.format("special characters not allowed"));

   $.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z ]*$/);
    }, jQuery.validator.format("Please enter a Valid Name."));


        $("#emergencyNumberEdit").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            name_edit: {
                required: true,
                noSpace:true,
                alpha:true,
            },
            designation : {
                required: true,
                noSpace:true,
                // alpha: true,
            },
            mobile: {
                noSpace:true,
                required: true,
                minlength: 3,
                maxlength:15,
            }
        },
        messages: {
            name_edit: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },
            designation: {
                required : "Please enter Description",
                noSpace: "No space please and don't leave it empty",
            },
            mobile: {
                minlength : "Please enter valid number",
                maxlength : "Please enter valid number",
                required : "Please enter valid number",
                noSpace: "No space please and don't leave it empty",
            },
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit();
          }
    });
});
</script>