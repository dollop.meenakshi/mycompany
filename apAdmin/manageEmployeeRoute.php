<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
if(isset($_GET['RId']) && !empty($_GET))
{
    $route_id = $_GET['RId'];
}
elseif(isset($_COOKIE['RId']) && !empty($_COOKIE['RId']))
{
    $route_id = $_COOKIE['RId'];
}

if(isset($_GET['UId']) && !empty($_GET['UId']))
{
    $user_id = $_GET['UId'];
}
/*elseif(isset($_COOKIE['UId']) && !empty($_COOKIE['UId']))
{
    $user_id = $_COOKIE['UId'];
}*/

if(isset($route_id) && !empty($route_id) && !ctype_digit($route_id))
{
    $_SESSION['msg1'] = "Invalid Route Id!";
?>
    <script>
        window.location = "manageEmployeeRoute";
    </script>
<?php
exit;
}
$week_arr = [
    0 => 'Sunday',
    1 => 'Monday',
    2 => 'Tuesday',
    3 => 'Wednesday',
    4 => 'Thursday',
    5 => 'Friday',
    6 => 'Saturday',
];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Manage Employee Route</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form action="addEmployeeRoute" method="post" id="addEmployeeRouteBtnForm">
                        <input type="hidden" name="addEmployeeRouteBtn" value="addEmployeeRouteBtn">
                        <button type="button" onclick="submitAddBtnForm();" class="btn btn-primary btn-sm waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add</button>
                    </form>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteEmployeeRoute');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <form id="filterFormArea">
            <div class="row pt-2 pb-2">
                <div class="col-lg-4 col-6">
                    <label class="form-control-label">User</label>
                    <select class="form-control single-select-new" id="UId" name="UId" onchange="getRoutes(this.value);">
                        <option value="">-- Select --</option>
                        <?php
                            $qu = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","user_status = 1 AND active_status = 0 AND delete_status = 0");
                            while ($ud = $qu->fetch_assoc())
                            {
                        ?>
                        <option <?php if(isset($user_id) && $user_id == $ud['user_id']) { echo 'selected'; } ?> value="<?php echo $ud['user_id']; ?>"><?php echo $ud['user_full_name'] . " (" . $ud['block_name'] . "-" . $ud['floor_name'] . ")" ; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-4 col-6">
                    <label  class="form-control-label">Route </label>
                    <select name="RId" id="RId" class="form-control single-select">
                        <option value="">--Select--</option>
                        <?php
                        if(isset($user_id) && !empty($user_id))
                        {
                            $cq = $d->selectRow("rm.route_id,rm.route_name,c.city_name","route_master AS rm JOIN cities AS c ON c.city_id = rm.city_id JOIN route_employee_assign_master AS ream ON ream.route_id = rm.route_id","ream.user_id = '$user_id'");
                            while ($cd = $cq->fetch_assoc())
                            {
                        ?>
                        <option <?php if($route_id == $cd['route_id']) { echo 'selected';} ?> value="<?php echo $cd['route_id']; ?>"><?php echo $cd['route_name'] . " (" . $cd['city_name'] . ")"; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="submit">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            if(isset($user_id) && !empty($user_id))
                            {
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>Action</th>
                                        <th>Route Name</th>
                                        <th>Route Assign Date</th>
                                        <th>Assign Week Days</th>
                                        <th>Route Assign By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $append = "";
                                    if(isset($route_id) && !empty($route_id))
                                    {
                                        $append .= " AND ream.route_id = '$route_id'";
                                    }
                                    $q = $d->selectRow("ream.*,um.user_full_name,bam.admin_name,bm.block_name,fm.floor_name,rm.route_name,c.city_name","route_employee_assign_master AS ream JOIN users_master AS um ON um.user_id = ream.user_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id LEFT JOIN bms_admin_master AS bam ON bam.admin_id = ream.route_assign_by JOIN route_master AS rm ON rm.route_id = ream.route_id JOIN cities AS c ON c.city_id = rm.city_id","ream.user_id = '$user_id'".$append,"ORDER BY route_employee_assign_id DESC");
                                    $iNo = 1;
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                        $route_assign_week_days_arr = explode(",",$route_assign_week_days);
                                    ?>
                                    <tr>
                                        <td class='text-center'>
                                            <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['route_employee_assign_id']; ?>">
                                        </td>
                                        <td><?php echo $iNo++; ?></td>
                                        <td>
                                            <div style="display: inline-block;">
                                                <form class="mr-2" action="addEmployeeRoute" method="post">
                                                    <input type="hidden" name="route_employee_assign_id" value="<?php echo $row['route_employee_assign_id']; ?>">
                                                    <input type="hidden" name="editEmployeeRoute" value="editEmployeeRoute">
                                                    <button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></button>
                                                </form>
                                            </div>
                                            <div style="display: inline-block;">
                                                <form class="mr-2" action="controller/employeeRouteController.php" method="POST">
                                                    <input type="hidden" name="route_employee_assign_id" value="<?php echo $row['route_employee_assign_id']; ?>">
                                                    <input type="hidden" name="route_id" value="<?php echo $route_id; ?>">
                                                    <input type="hidden" name="deleteEmployeeRoute" value="deleteEmployeeRoute">
                                                    <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                        <td><?php echo $route_name . " (" . $city_name . ")"; ?></td>
                                        <td><?php echo $route_assign_date; ?></td>
                                        <td>
                                            <?php
                                            $wd = "";
                                            foreach($route_assign_week_days_arr AS $key => $value)
                                            {
                                                if(isset($week_arr[$value]))
                                                {
                                                    $wd .= $week_arr[$value].",";
                                                }
                                            }
                                            echo rtrim($wd, ",");
                                            ?>
                                        </td>
                                        <td>
                                        <?php
                                        if(isset($admin_name))
                                        {
                                            echo $admin_name;
                                        }
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            else
                            {
                            ?>
                            <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select Route</span>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script>
function submitAddBtnForm()
{
    var route_id = $('#RId').val();
    if(route_id != "" && route_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'route_id',
            value: route_id
        }).appendTo('#addEmployeeRouteBtnForm');
    }
    $("#addEmployeeRouteBtnForm").submit()
}

function getRoutes(user_id)
{
    $.ajax({
        url: 'controller/employeeRouteController.php',
        data: {getRouteFromUser:"getRouteFromUser",user_id:user_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#RId').empty();
            response = JSON.parse(response);
            $('#RId').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#RId').append("<option value='"+value.route_id+"'>"+value.route_name+" ("+value.city_name+")</option>");
            });
        }
    });
}
</script>