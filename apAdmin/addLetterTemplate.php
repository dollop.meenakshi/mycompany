<?php
    if(isset($_GET['letter_id']))
    {
        if(!ctype_digit($_GET['letter_id']))
        {
            $_SESSION['msg1'] = "Invalid Request!";
        ?>
        <script>
            window.location = "addLetterTemplate";
        </script>
        <?php
            exit;
        }
        $letter_id = $_GET['letter_id'];
        $q = $d->select("letter_master","letter_id = '$letter_id'");
        if(mysqli_num_rows($q) > 0)
        {
            $data = $q->fetch_assoc();
            extract($data);
            $letter_text = str_replace(array("\r","\n"),"",$letter_text);
            $letter_text = addslashes($letter_text);
        }
        else
        {
            $_SESSION['msg1'] = "Invalid Id!";
        ?>
        <script>
            window.location = "addLetterTemplate";
        </script>
        <?php
            exit;
        }
    }
?>
<!-- <link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/> -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/smoothness/jquery-ui.css" />
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Add Letter Template</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col">
                <div class="card">
                    <form enctype="multipart/form-data" id="letterTemplateAddForm" action="controller/letterController.php" method="post">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="input-10" class="col-form-label">Select Letter <span class="text-danger">*</span></label>
                                    <select onchange="getLetterDesc(this.value);" name="letter_id" id="letter_id" class="form-control single-select" required>
                                        <option value="">--Select--</option>
                                        <?php
                                            $ql = $d->selectRow("lm.letter_id,lm.letter_name","letter_master AS lm","lm.active_status = 0");
                                            if(mysqli_num_rows($ql) > 0)
                                            {
                                                while ($lData = $ql->fetch_assoc())
                                                {
                                        ?>
                                        <option <?php if (isset($letter_id) && $letter_id = $lData['letter_id']) { echo "selected"; } ?> value="<?php echo $lData['letter_id']; ?>"><?php echo $lData['letter_name']; ?></option>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-9">
                                    <table class="table descTable <?php if(!isset($_GET['letter_id'])){ echo "d-none"; } ?>">
                                        <thead>
                                            <th>Variable</th>
                                            <th>Value</th>
                                        </thead>
                                        <tbody class="addDesc">
                                        <?php
                                            if(isset($_GET['letter_id']))
                                            {
                                                foreach (json_decode($letter_varibles) as $key => $value)
                                                {
                                                ?>
                                                <tr>
                                                    <td><?php echo $key; ?></td>
                                                    <td><?php echo $value; ?></td>
                                                </tr>
                                                <?php
                                                }
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="letter_text" class="col-form-label">Letter <span class="text-danger">*</span></label>
                                    <textarea style="height: 1100px;" class="form-control" required id="letter_text" name="letter_text"><?php
                                    // if(isset($_GET['letter_id'])){ echo $letter_text; }
                                    ?>
                                    </textarea>
                                </div>
                            </div>

                            <!-- <div class="row">
                                <div class="col-sm-12">
                                    <label for="letter_text" class="col-form-label">Letter <span class="text-danger">*</span></label>
                                    <textarea class="form-control" required id="summernoteImgage" name="letter_text"><?php
                                    // if(isset($_GET['letter_id'])){ echo $letter_text; }
                                    ?>
                                    </textarea>
                                </div>
                            </div> -->

                            <div class="form-footer text-center">
                                <input type="hidden" name="addLetterTemplate" value="addLetterTemplate">
                                <input type="hidden" name="letter_id" id="letter_id_hid" value="<?php if(isset($_GET['letter_id'])){ echo $letter_id; } ?>">
                                <button name="addLetterTemplate" id="submitBtn" value="addLetterTemplate" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i><?php echo $xml->string->update; ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<!-- <script src="assets/plugins/summernote/dist/summernote-bs4.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/@tinymce/tinymce-jquery@1/dist/tinymce-jquery.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.tiny.cloud/1/mc5byr33kilvqqblgxq9euoqmcbqwex77ka47o319oz6942t/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    function getLetterDesc(letter_id)
    {
        $.ajax({
            url: 'controller/letterController.php',
            data: {getLetterDesc:"getLetterDesc",letter_id:letter_id,csrf:csrf},
            type: "post",
            success: function(response)
            {
                $('.descTable').removeClass('d-none');
                response = JSON.parse(response);
                $('#letter_id_hid').val(response.letter_id);
                $('#submitBtn').text("Update");
                $('#summernoteImgage').summernote('editor.pasteHTML', response.letter_text);
                letter_varibles = JSON.parse(response.letter_varibles);
                $.each(letter_varibles, function (key, val)
                {
                    $('.addDesc').append('<tr><td>'+key+'</td><td>'+val+'</td></tr>');
                });
            }
        });
    }

    $('#letter_text').tinymce({
        toolbar: 'link',
        plugins: 'link',
        menubar: true,
        plugins: [
          'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
          'anchor', 'searchreplace', 'visualblocks', 'fullscreen',
          'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount'
        ],
        toolbar: 'undo redo | blocks | bold italic backcolor | ' +
          'alignleft aligncenter alignright alignjustify | ' +
          'bullist numlist outdent indent | removeformat | help',
        setup: function (editor) {
            editor.on('init', function () {
                editor.setContent('<?php echo $letter_text; ?>');
            });
        }
    });
</script>