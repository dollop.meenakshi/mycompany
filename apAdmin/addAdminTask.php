
<?php
extract(array_map("test_input", $_POST));
if (isset($edit_task)) {
    $q = $d->select("task_master,users_master","task_master.task_assign_to=users_master.user_id AND task_master.task_id='$task_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Task</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addTaskFrom" action="controller/TaskController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Task Name <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Task Name" name="task_name" value="<?php if($data['task_name'] !=""){ echo $data['task_name']; } ?>">
                                </div>
                            </div> 
                        </div>
						<div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Task Due Date <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control datepicker_task_due_date" placeholder="Task Due Data"readonly name="task_due_date" value="<?php if($data['task_due_date'] !=""){ echo $data['task_due_date']; } ?>">
                                </div>
                            </div> 
                        </div>
						<div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Task Attachment</label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="file" class="form-control idProof" accept=".jpg, .jpeg, .png, .doc, .docx, .pdf" name="task_attachment">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Task Note </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                <textarea name="task_note" class="form-control " placeholder="Task Note"><?php if($data['task_note'] !=""){ echo $data['task_note']; } ?></textarea>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4 <?php if (isset($edit_task)) { echo "d-none"; }?>">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
									<select name="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)">
										<option value="">Select Branch</option> 
										<?php 
										$qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
										while ($blockData=mysqli_fetch_array($qb)) {
										?>
										<option  <?php if($block_id==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
										<?php } ?>
									</select> 
                                </div>
                            </div> 
                        </div>
						<div class="col-md-4 <?php if (isset($edit_task)) { echo "d-none"; }?>">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
									<select name="floor_id" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value);">
										<option value="">Select Department</option> 
										<?php 
											$qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$block_id' $blockAppendQuery");  
											while ($depaData=mysqli_fetch_array($qd)) {
										?>
										<option  <?php if($floor_id==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
										<?php } ?>
									</select>
                                </div>
                            </div> 
                        </div>
						<div class="col-md-4 <?php if (isset($edit_task)) { echo "d-none"; }?>">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Assign To <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
									<select name="task_assign_to" id="user_id" class="form-control single-select" <?php if (isset($edit_task)) { echo "disabled"; }?>>
										<option value="">Select Employee</option> 
										<?php 
											$user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND floor_id = '$floor_id' $blockAppendQueryUser");  
											while ($userdata=mysqli_fetch_array($user)) {
										?>
										<option <?php if($task_assign_to==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
										<?php } ?>
									</select>
                                </div>
                            </div> 
                        </div>
						<div class="col-md-4 <?php if (isset($edit_task)) { echo "d-none"; }?>">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">No of Task Step </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
									<input maxlength="1" autocomplete="off" onkeyup="getTaskStepList();" type="text" id="no_of_task_step" placeholder="No of Task Step" class="form-control" name="no_of_task_step">
                                </div>
                            </div> 
                        </div>
                    </div>    
					<div id="taskStepForm" class="row"></div>
                                
                    <div class="form-footer text-center">
                    	<?php if (isset($edit_task)) {  ?>
							<input type="hidden" id="task_id" name="task_id" value="<?php if($data['task_id'] !=""){ echo $data['task_id']; } ?>" >
							<input type="hidden" name="task_assign_to_old" value="<?php if($data['task_assign_to'] !=""){ echo $data['task_assign_to']; } ?>" >
							<button id="updateTaskBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
							<input type="hidden" name="updateTask"  value="updateTask">
							<button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addTaskFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                    	<?php } else { ?>
							<button id="addTaskBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
							<input type="hidden" name="addTask"  value="addTask">
							<button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addTaskFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                    	<?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

</div><!--End content-wrapper-->
 