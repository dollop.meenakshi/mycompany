<?php 
error_reporting(0);
if (isset($_GET['from']) && $_GET['block_id']!='All') {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1 || filter_var($_GET['block_id'], FILTER_VALIDATE_INT) != true){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='visitorReports';
        </script>");
  }
} else if(isset($_GET['from']) && $_GET['block_id']=='All'){
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='visitorReports';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Daily Visitor  Report</h4>
        </div>
     
     </div>

    <form id="getVisitorReportForm">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control from" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control toDate" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
          <div class="col-lg-4 col-6">
            <label  class="form-control-label">Select <?php echo $xml->string->block; ?> </label>
             <select required="" class="form-control" name="block_id" id="block_id">
              <?php if (count($blockAryAccess)>0) { ?>
                <option value="">Select <?php echo $xml->string->block; ?></option>
              <?php } else { ?>

                <option value="All">All</option>
              
                  <?php } $q=$d->select("block_master","society_id='$society_id' $blockAppendQuery ","");
                        while ($mData=mysqli_fetch_array($q)) {
                          $block_id=$mData['block_id']; ?>
                          <option <?php if($mData['block_id']==$_GET['block_id']) { echo 'selected';} ?> value="<?php echo $mData['block_id'] ?>"><?php echo $mData['block_name']; ?></option>
                  <?php } ?>
             </select>
          </div>
           
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
            <input type="hidden" name="report_download_access" value="<?php echo $adminData['report_download_access'];?>">
            <input type="hidden" name="role_id" value="<?php echo $adminData['role_id'];?>">
            <label  class="form-control-label"> </label>
              <input type="button" style="margin-top:30px;" class="btn btn-success" name="getReport" id="getVisitorReport"  value="Get Report">
          </div>
          

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              
                <div id="visitorReport" style="display: none;">  
                  <table id="visitorReportWithBtn" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th> Mobile</th>
                            <th><?php echo $xml->string->block; ?></th>
                            <th>Employee</th>
                            <th>In Time</th>
                            <th>Out Time</th>
                            <th>Type</th>
                            <th>Vehicle Number </th>
                            <th>Visiting Reason</th>
                            <th>Status</th>
                            <th>Data Add Time</th>
                            <th>Added By</th>
                            <th>Exit By</th>
                        </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>  
                    
                  </table>
                </div>
                <div id="visitorReportNoData" style="display: block;">
                    
                    
                  <div class="" role="alert">
                   <span><strong>Note :</strong> Please Select date</span>
                  </div>
                  
                </div>

              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->