<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year']= $_REQUEST['month_year'];
  $bId= (int)$_REQUEST['bId'];
  $dId= (int)$_REQUEST['dId'];
  $month= $_REQUEST['month'];
  $laYear = $_REQUEST['laYear'];
  if (!isset($month)) {
    $month = date("m");
    $laYear = date("Y");
  }




  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-12">
          <h4 class="page-title">Absent Employees </h4>
        </div>
     </div>
     <form action="" class="branchDeptFilter">
        <div class="row pt-2 pb-2">
          <?php include('selectBranchDeptForFilterAll.php'); ?>
          <div class="col-md-2 col-6">
          <select name="laYear" class="form-control single-select">
            <option value="">Year</option>
            <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
              echo 'selected';
            } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $onePreviousYear) {
              echo 'selected';
            } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $currentYear) {
              echo 'selected';
            } ?> <?php if ($_GET['laYear'] == '') {
              echo 'selected';
            } ?> value="<?php echo $currentYear; ?>"><?php echo $currentYear; ?></option>
            <option <?php if ($_GET['laYear'] == $nextYear) {
              echo 'selected';
            } ?> value="<?php echo $nextYear; ?>"><?php echo $nextYear; ?></option>
          </select>
        </div>
          <div class="col-md-2 form-group">
              <select required class="form-control single-select" name="month" id="month">
            <option value="">-- Select Month--</option>
            <?php
            $selected = "";
            for ($m = 1; $m <= 12; $m++) {
              $monthName = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
              $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
              if (isset($month)  && $month != "") {
                if ($month == $m) {
                  $selected = "selected";
                } else {
                  $selected = "";
                }
              } else {
                $selected = "";
                if ($m == date('n')) {
                  $selected = "selected";
                } else {
                  $selected = "";
                }
              }
              ?>
              <option <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $monthName; ?></option>
            <?php } ?>
          </select>
          </div>
          <div class="col-md-2 form-group ">
              <input class="btn btn-success btn-sm " type="submit" name="getReport" class="form-control" value="Get">
          </div>    
      </div>
     </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                  <?php 
                    $startDate = date('Y-m-d',strtotime(date("$laYear-$month-01")));
                    $curentRunnigMonth =  date("Y-m");
                    $curentDate =  date("Y-m-d");
                    if ($curentRunnigMonth=="$laYear-$month") {
                      $month_end_date = date("Y-m-d", strtotime($curentDate));
                    } else {
                      $month_end_date = date("Y-m-t", strtotime($startDate));
                    }

                    $time3 = new DateTime($startDate);
                    $time4 = new DateTime($month_end_date);
                    $interval = $time3->diff($time4);
                    $indays =  $interval->format('%a');
                                         
                         ?>
                        <table id="<?php if ($adminData['report_download_access'] == 0) {
                       echo 'exampleReportWithoutBtn'; }  else {  echo 'exampleReport';  } ?>" class="table table-bordered">
                          <thead>
                              <tr>
                                <th>Sr.No</th>                        
                                <th> Date</th>
                                <th> Day</th>
                                <th> Name</th>
                                <th> Branch</th>
                                <th> Department</th>
                                <th> Leave Status</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php 
                            $i1 = 1;
                    for ($j = 0; $j <= $indays; $j++) { 
                       $jtemp = $j+1;
                       $dateTemp = "$laYear-$month-".$jtemp;
                       $dateTemp = date('Y-m-d', strtotime($dateTemp));
                       $from = $dateTemp;
                       $dateTemp = date('d-m-Y', strtotime($dateTemp));
                       $dayName = date('D', strtotime($dateTemp));


                        $i=1;
                        $todayDayNumber = date("w",strtotime($from));
                        $monthWeekNo = ceil(date( 'j', strtotime($from) ) / 7 );
                        $todayWeekNo = date('w',strtotime($from));

                        if(isset($bId) && $bId>0) {
                          $blockFilterQuery = " AND users_master.block_id='$bId'";
                        }

                        if(isset($dId) && $dId>0) {
                          $deptFilterQuery = " AND users_master.floor_id='$dId'";
                        }
                        
                        $q2 = $d->select("attendance_master LEFT JOIN leave_master ON leave_master.user_id = attendance_master.user_id AND leave_master.leave_start_date =attendance_master.attendance_date_start,users_master", "users_master.user_id=attendance_master.user_id AND attendance_master.society_id='$society_id' AND users_master.delete_status=0 AND attendance_master.attendance_date_start='$from' $blockFilterQuery $deptFilterQuery $blockAppendQueryUser", "");
                        $userIds = array();
                        while ($data2=mysqli_fetch_array($q2)) {
                          array_push($userIds,$data2['user_id']);
                        }
                        // print_r($userIds);
                        $userIds = join("','",$userIds); 
                        $q=$d->select("block_master,floors_master,shift_timing_master,users_master LEFT JOIN leave_master ON leave_master.user_id=users_master.user_id AND leave_start_date = '$from'","shift_timing_master.shift_time_id=users_master.shift_time_id AND users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND  users_master.society_id='$society_id' AND   users_master.is_attendance_mandatory=0 AND users_master.user_status=1 AND users_master.delete_status=0 AND users_master.user_id NOT IN ('$userIds') $blockFilterQuery $deptFilterQuery  $blockAppendQueryUser","");
                        $counter = 1;
                            while ($data=mysqli_fetch_array($q)) {
                              $has_altenate_week_off = $data['has_altenate_week_off'];
                              $alternate_week_off = explode(",",$data['alternate_week_off']);
                              $alternate_weekoff_days = explode(",",$data['alternate_weekoff_days']);

                              $weekOffAray = explode(",",$data['week_off_days']);

                                if($has_altenate_week_off == 0 && $data['attendance_id']=="" && !in_array($todayDayNumber, $weekOffAray) || $has_altenate_week_off == 1 && !in_array($monthWeekNo,$alternate_week_off) && in_array($todayWeekNo,$alternate_weekoff_days)){ ?>
                              <tr>
                                <td><?php echo $counter++; ?></td>
                                <td><?php echo $dateTemp; ?></td>
                                <td><?php echo $dayName; ?></td>
                                <td><?php echo $data['user_full_name']; ?></td>
                                <td><?php echo $data['block_name']; ?></td>
                                <td><?php echo $data['floor_name']; ?></td>
                                <td><?php if($data['leave_id']>0){ 
                                    if ($data['leave_status']==0) {
                                      echo "Requested";
                                    }else if ($data['leave_status']==1 && $data['leave_day_type']==0) {
                                      echo "Full Day";
                                      
                                    }else if ($data['leave_status']==1 && $data['leave_day_type']==1) {
                                      echo "Half Day";
                                    }

                                    if ($data['paid_unpaid']==0) {
                                        echo " (Paid)";
                                      } else {
                                        echo " (Un Paid)";
                                      }
                                 } ?></td>
                              </tr>
                              <?php }
                              } } ?>
                          </tbody>
                      </table>
                      
                </div>
            </div>
          </div>
        </div>
     </div><!-- End Row-->
    </div>
  </div>
 
<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
<?php
 if(!isset($_GET['month_year']))
 { ?>
    $('#month_year').val(<?php echo $currentMonth; ?>)
<?php } ?>
function popitup(url) {
  newwindow=window.open(url,'name','height=800,width=900, location=0');
  if (window.focus) {
    newwindow.focus()
    }
    return false;
  }
</script>
<style>
.attendance_status
{  
  min-width: 113px;
}
</style>
