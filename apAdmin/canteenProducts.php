<?php error_reporting(0);
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Canteen Product</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <a href="addVendorProductBtn" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteCanteenProduct');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
      </div>
     </div>
      <form action="" method="get">
        <div class="row pt-2 pb-2">
          <div class="col-md-3 form-group">
            <select name="vId" class="form-control single-select" onchange="getVendorProductCategoryByVendorId(this.value);" required="">
                <option value="">--Select Vendor--</option> 
                <?php 
                  $qd=$d->select("vendor_master","society_id='$society_id' AND vendor_category_id=1 AND vendor_master_delete=0 AND vendor_active_status=0");  
                  while ($vendorData=mysqli_fetch_array($qd)) {
                ?>
                <option  <?php if($_GET['vId']==$vendorData['vendor_id']) { echo 'selected';} ?> value="<?php echo  $vendorData['vendor_id'];?>" ><?php echo $vendorData['vendor_name'];?></option>
                <?php } ?>
              </select>
          </div>  
          <div class="col-md-3 form-group">
            <select name="cId" id="cId" class="form-control single-select">
                <option value="">All Category</option> 
                <?php 
                  $qd=$d->select("vendor_product_category_master","vendor_category_id=1 AND vendor_product_category_status = 0 AND is_delete=0 AND vendor_id='$_GET[vId]'");  
                  while ($vendorData=mysqli_fetch_array($qd)) {
                ?>
                <option  <?php if($_GET['cId']==$vendorData['vendor_product_category_id']) { echo 'selected';} ?> value="<?php echo  $vendorData['vendor_product_category_id'];?>" ><?php echo $vendorData['vendor_product_category_name'];?></option>
                <?php } ?>
              </select>
          </div> 
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
            </div>    
        </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                  <?php 
                    $i=1;
                                           
                     if(isset($_GET['vId']) && $_GET['vId']>0) {
                      $vendorFilterQuery = " AND vendor_master.vendor_id='$_GET[vId]'";
                     }

                     if(isset($_GET['cId']) && $_GET['cId']>0) {
                      $categoryFilterQuery = " AND vendor_product_category_master.vendor_product_category_id='$_GET[cId]'";
                     }
                     
                      $q=$d->select("vendor_product_master,vendor_master,vendor_product_category_master","vendor_product_master.vendor_id = vendor_master.vendor_id AND vendor_product_master.vendor_product_category_id = vendor_product_category_master.vendor_product_category_id AND vendor_product_master.vendor_category_id='1' AND vendor_product_master.vendor_product_delete= 0 $vendorFilterQuery $categoryFilterQuery");
                        $counter = 1;
                    if(isset($_GET['vId'])){
                     ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Vendor Name</th>
                        <th>Category Name</th>
                        <th>Product Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="showFilterData">
                   <?php while ($data=mysqli_fetch_array($q)) { ?>
                    <tr>
                      <td class="text-center">
                          <?php $totalVariant = $d->count_data_direct("vendor_product_variant_id","vendor_product_variant_master","vendor_product_id='$data[vendor_product_id]' AND vendor_product_variant_delete=0");
                            
                          
                          if( $totalVariant==0) {
                          ?>
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['vendor_product_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['vendor_product_id']; ?>">                      
                          <?php } ?>
                      </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['vendor_name']; ?></td>
                      <td><?php  echo $data['vendor_product_category_name']; ?></td>
                      <td><?php  echo $data['vendor_product_name']; ?></td>
                      <td>
                      <div class="d-flex align-items-center">
                          <button type="button" class="btn btn-sm btn-primary mr-1" onclick="vendorProductSetData(<?php echo $data['vendor_product_id']; ?>)" data-toggle="modal" data-target="#addModal" > <i class="fa fa-pencil"></i></button> 
                          <?php if($data['vendor_product_active_status']=="0"){
                          ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_product_id']; ?>','canteenProductDeactive');" data-size="small"/>
                          <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_product_id']; ?>','canteenProductActive');" data-size="small"/>
                          <?php } ?>
                        </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
              <?php } else {  ?>
                <div class="" role="alert">
                  <span><strong>Note :</strong> Please Select Vendor</span>
                  </div>
              <?php } ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Canteen Product</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           <form id="addCanteenProductForm" action="controller/VendorProductController.php" enctype="multipart/form-data" method="post">
           
                <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Product Category<span class="required">*</span></label>
                        <div class="col-lg-8 col-md-8" id="">
                            <select name="vendor_product_category_id" id="vendor_product_category_id" class="form-control restFrm single-select">
                                <option value="">--Select Category--</option> 
                                    <?php 
                                    $qd=$d->select("vendor_product_category_master","vendor_category_id='1'");  
                                    while ($categoryData=mysqli_fetch_array($qd)) {
                                    ?>
                                <option value="<?php echo  $categoryData['vendor_product_category_id'];?>" ><?php echo $categoryData['vendor_product_category_name'];?></option>
                                <?php } ?>
                        
                                </select>                 
                            </div>
                </div> 
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Product Name <span class="required">*</span></label>
                    <div class="col-lg-8 col-md-8" id="">
                    <input type="text"  required="" name="vendor_product_name" id="vendor_product_name" class="form-control">
                    </div>                   
                </div>  
                                    
                <div class="form-footer text-center">
                    
                    <input type="hidden" id="vendor_product_id" name="vendor_product_id" value="" >
                    <button id="addCanteenProductBtn" name="addCanteenProductBtn" type="submit"  class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addCanteenProduct" value="addCanteenProduct">
                    
                    <button id="addCanteenProductBtn" type="submit"  class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
                
                    <button type="button"  value="add" class="btn btn-danger " onclick="resetForm();"><i class="fa fa-check-square-o"></i> Reset</button>
                    
                </div>

         </form>
      
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">
  function resetForm()
 {
  $(".restFrm").val('').trigger('change');
  $('#vendor_product_name').val('');
 } 
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
.hideupdate{
  display:none;
}

</style>