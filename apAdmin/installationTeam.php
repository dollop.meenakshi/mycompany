    <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Installation Team</h4>
        <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="welcome">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Installation Team</li>
         </ol>
     </div>
     <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="#" data-toggle="modal" data-target="#addAgentModal" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
       
        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      <th class='deleteTh'>#</th>
                      <th>Person Name</th>
                      <th>Mobile</th>
                      <th>Report</th>
                      <th>Password</th>
                      <th>Created Date</th>
                      <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                    $q=$d->select("installation_team_master","","");
                    $i = 1;
                    while($row=mysqli_fetch_array($q)){?>
                    <tr>
                      
                      <td><?php echo $i++; ?></td>
                        <td><?php echo $row['full_name'] ?></td>
                        <td><?php echo $row['mobile_number'] ?></td>
                         <td>
                          <form action="installationReport?id=<?= $row['installation_team_id']?>" method="post">
                              <input type="hidden" name="forgot_mobile_installation_team" value="<?php echo $row['mobile_number'] ?>">
                              <button class="btn btn-sm btn-primary">Report</button>
                           </form>
                        </td>
                        <td>
                          <form action="controller/installationController.php" method="post">
                              <input type="hidden" name="forgot_mobile_installation_team" value="<?php echo $row['mobile_number'] ?>">
                              <button class="btn btn-sm btn-warning">Reset</button>
                           </form>
                        </td>
                        <td><?php echo date('d-M-Y', strtotime($row['created_date'])) ?></td>
                        <td class="text-center">
                    
                           <button type="button" data-toggle="modal" data-target="#editAgent" onClick="editinstallationteam('<?= $row['society_id']?>','<?= $row['installation_team_id']?>','<?= $row['full_name']?>','<?= $row['mobile_number']?>')" class="btn btn-sm btn-primary waves-effect waves-light m-1" title="Edit"> <i class="fa fa-pencil"></i> </button>
                        
                         <form action="controller/installationController.php" method="post" style="float: left;" >
                          <input type="hidden" name="installation_team_id_delete"  value="<?php echo $row['installation_team_id']; ?>">
                           <button type="submit" class="btn btn-sm btn-danger form-btn waves-effect waves-light m-1" title="Delete"> <i class="fa fa-trash-o"></i> </button>
                         </form>
                        <!-- <td>
                          <button type="button" class="btn btn-sm btn-primary waves-effect waves-light m-1" title="Edit Details"> <i class="fa fa-pencil"></i> </button>
                        </td> -->
                    </tr>
                  <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
<div class="modal fade" id="addAgentModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add New Team Member</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="insTeam" action="controller/installationController.php" method="post" enctype="multipart/form-data">
               
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Full Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input required="" id="full_name" maxlength="50" type="text" name="full_name"  class="form-control text-capitalize">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->members; ?> Mobile <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input required="" minlength="10" maxlength="10" id="mobile_number" type="text" name="mobile_number"  class="form-control onlyNumber" inputmode="numeric">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Password <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input required="" minlength="4" maxlength="30" id="password" type="password" name="password"  class="form-control">
                    </div>
                </div>
                
                <div class="form-footer text-center">
                  <button type="submit" name="addMember" value="addMember" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->

<div class="modal fade" id="editAgent">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit <?php echo $xml->string->members; ?> Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="editinsTeam" action="controller/installationController.php" method="post" enctype="multipart/form-data">
                
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Full Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="hidden" id="member_id" name="member_id"  class="form-control">
                      <input required="" id="member_full_name" maxlength="50" type="text" name="member_full_name"  class="form-control text-capitalize">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->members; ?> Mobile <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input required="" minlength="10" maxlength="10" id="member_mobile_number" type="text" name="member_mobile_number"  class="form-control onlyNumber" inputmode="numeric">
                    </div>
                </div>
                
                <div class="form-footer text-center">
                  <button type="submit" name="editMember" value="editMember" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Edit</button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->