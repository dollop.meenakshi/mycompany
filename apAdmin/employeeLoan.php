<?php

error_reporting(0);
$lsId = (int) $_REQUEST['lsId'];
$cpId = (int) $_REQUEST['cpId'];
$bId = (int) $_REQUEST['bId'];
$dId = (int) $_REQUEST['dId'];
$uId = (int)$_REQUEST['uId'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title">Employee Loan</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
                <div class="btn-group float-sm-right">
                    <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
                    <a href="addEmployeeLoan"  class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteEmployeeLoan');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <form action="" class="branchDeptFilterWithUser">
            <div class="row pt-2 pb-2">
                <?php  include('selectBranchDeptEmpForFilterAll.php'); ?> 
                <div class="col-md-2 col-6 form-group">
                    <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-12 month'));} ?>">   
                  </div>
                  <div class="col-md-2 col-6 form-group">
                    <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
                  </div> 
                <div class="col-md-2 form-group mt-auto">
                    <input class="btn btn-success btn-sm " type="submit" name="getReport"  value="Get Data">
                </div>

            </div>

        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-body">
                        <div class="table-responsive">
                            <?php 
                            $i = 1;
                           // $q = $d->selectRow("employee_loan","");
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>Action</th>
                                        <th>Employee Name</th>
                                        <th>Department</th>
                                        <th> Date</th>
                                        <th>Loan Amount</th>
                                        <th>Emi Amount</th>
                                        <th>Paid Amount</th>
                                        <th>Remaining</th>
                                        <th>EMI's</th>
                                        <th>Created By</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 

                                    if($dId>0){
                                        $dptFilter = " AND employee_loan_master.floor_id='$dId'";
                                    }
                                    if($bId>0){
                                        $blkFilter = " AND employee_loan_master.block_id='$bId'";
                                    }else {
                                        $limit = " LIMIT 1000";
                                    }

                                    if (isset($uId) && $uId > 0) {
                                      $userFilterQuery = "AND employee_loan_master.user_id='$uId'";
                                    }
                                    if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                                      $dateFilterQuery = " AND employee_loan_master.loan_date BETWEEN '$from' AND '$toDate'";
                                    }

                                      $q = $d->selectRow('employee_loan_master.*,users_master.*,floors_master.floor_id,floors_master.floor_name,block_master.block_id,block_master.block_name,loan_created_by_admin.admin_name,(SELECT SUM(emi_amount) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=1) AS paid_emi_amount,(SELECT count(*) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=1) AS paidEmiCount',"employee_loan_master LEFT JOIN floors_master ON floors_master.floor_id = employee_loan_master.floor_id LEFT JOIN block_master ON block_master.block_id = employee_loan_master.block_id LEFT JOIN users_master ON users_master.user_id = employee_loan_master.user_id  LEFT JOIN bms_admin_master AS loan_created_by_admin ON loan_created_by_admin.admin_id = employee_loan_master.loan_created_by","employee_loan_master.society_id = '$society_id' $blkFilter $dptFilter $userFilterQuery $dateFilterQuery","ORDER BY employee_loan_master.loan_id DESC $limit");
                                    while ($data=mysqli_fetch_assoc($q)) {
                                       ?>
                                       <tr>
                                           <td>
                                               <?php 
                                                $totalAttendance = $d->count_data_direct("loan_emi_id", "loan_emi_master", "loan_id='$data[loan_id]' AND is_emi_paid=1");
                                                    if ($totalAttendance == 0)  { ?>
                                                    <input type="hidden" name="id" id="id"  value="<?php echo $data['loan_id']; ?>">
                                                    <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['loan_id']; ?>">
                                                    <?php } ?>
                                                    </td>
                                                    <td><?php echo $i++; ?></td>
                                                    <td>
                                                    <div class="d-flex align-items-center">
                                                   
                                                     <a  href="loanEmiDetail?lId=<?php echo $data['loan_id']; ?>" class="btn btn-primary btn-sm ml-1"><i class="fa fa-eye" ></i> Manage EMI</a>
                                                     <?php if((isset($data['loan_created_by']) && $data['loan_created_by'] ==$_COOKIE['bms_admin_id']) && $data['paidEmiCount']==0 ){ ?>
                                                        <form action="addEmployeeLoan" method="POST">
                                                            <input type="hidden" name="loan_id" value="<?php echo $data['loan_id']; ?>">
                                                            <input type="hidden" name="edit_employee_loan" value="edit_employee_loan">
                                                        <button type="submit" class="btn btn-sm btn-warning ml-2" > <i class="fa fa-pencil"></i></button>
                                                        </form>
                                                    
                                                     <?php } ?>
                                                    
                                                     </div>
                                                    </td>
                                           <td><?php echo $data['user_full_name'] ?></td>
                                           <td><?php echo $data['floor_name']."(". $data['block_name'].")";?></td>
                                           <td><?php echo date("d M Y",strtotime($data['loan_date']));?></td>
                                           <td><?php echo $data['loan_amount'];?></td>
                                           <td><?php echo $data['loan_emi_amount'];?></td>
                                           <td><?php echo $data['paid_emi_amount'];?></td>
                                           <td><?php echo $data['loan_amount']-$data['paid_emi_amount'];?></td>
                                           <td><?php echo $data['loan_emi'];?></td>
                                           <td><?php echo $data['admin_name'];?></td>
                                          
                                       </tr>
                                       <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->


</div>
<div class="modal fade" id="addEmployeeLoan">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Add Loan</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-2 " style="align-content: center;">
                <div class="row">
                    <div class="card-body">
                        <form id="addEmployeeLoanFrm" action="controller/EmployeeLoanController.php" enctype="multipart/form-data" method="post">
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select id="block_id" name="block_id" class="form-control inputSl single-select" onchange="getFloorByBlockIdAddSalary(this.value)" required="">
                                        <option value="">Select Branch</option>
                                        <?php
                                        $qd = $d->select("block_master", "block_master.society_id='$society_id' AND block_master.block_status=0 $blockAppendQueryOnly ");
                                        while ($depaData = mysqli_fetch_array($qd)) {
                                        ?>
                                            <option <?php if ($data['block_id'] == $depaData['block_id']) { echo 'selected';} ?> <?php if ($block_id == $depaData['block_id']) {echo 'selected';} ?> value="<?php echo  $depaData['block_id']; ?>"><?php echo $depaData['block_name']; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-4 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select type="text" id="floor_id" required="" onchange="getUserByFloorId(this.value)" class="form-control single-select floor_id inputSl" name="floor_id">
                                        <option value="">-- Select --</option>
                                       <!--  <?php
                                        if (isset($sid) && $sid > 0) {
                                            $fblock_id = $data['block_id'];
                                        } else {
                                            $fblock_id = $block_id;
                                        }
                                        $floors = $d->select("floors_master,block_master", "block_master.block_id =floors_master.block_id AND  floors_master.society_id='$society_id' AND floors_master.block_id = '$fblock_id' $blockAppendQuery");
                                        while ($floorsData = mysqli_fetch_array($floors)) {
                                        ?>
                                            <option <?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] == $fid) {
                                                        echo "selected";
                                                    } ?> value="<?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] != "") {
                                                                    echo $floorsData['floor_id'];
                                                                } ?>"> <?php if (isset($floorsData['floor_name']) && $floorsData['floor_name'] != "") {
                                                                            echo $floorsData['floor_name'] . "(" . $floorsData['block_name'] . ")";
                                                                        } ?></option>
                                        <?php } ?> -->
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-4 col-form-label">Employee <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select name="user_id" id="user_id" class="inputSl form-control single-select">
                                        <option value="">-- Select Employee --</option>
                                        <?php
                                        $user = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND user_status !=0 AND floor_id = '$dId' $blockAppendQueryUser");
                                        while ($userdata = mysqli_fetch_array($user)) {
                                        ?>
                                            <option <?php if ($uId == $userdata['user_id']) {
                                                        echo 'selected';
                                                    } ?> value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Loan Amount<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <input type="text" value="" name="loan_amount" id="loan_amount" class="form-control rstFrm onlyNumber rstFrm">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Loan Date<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <input type="text" value="" name="loan_date" id="loan_date" class="form-control rstFrm  ">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Emi's<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <input type="text" value="" name="loan_emi" id="loan_emi" class="form-control rstFrm onlyNumber rstFrm">
                                </div>
                            </div>
                           
                            <div class="form-footer text-center">
                                <input type="hidden" id="loan_id" name="loan_id" value="" class="rstFrm">
                                <button type="submit" class="btn btn-success hideAdd course_lesson_button"><i class="fa fa-check-square-o"></i> </button>
                                <input type="hidden" name="addEmployeeLoan" value="addEmployeeLoan">
                                <button type="button" value="add" class="btn btn-danger " onclick="resetForm()"><i class="fa fa-check-square-o"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="showLoanModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Loan Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="showLoanData">
        </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Emi Amount</th>
                              <th>Status</th>
                              <th>Paid Date</th>
                          </tr>
                      </thead>
                      <tbody id="showLoanEmi">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css" />
<script src="assets/js/jquery.min.js"></script>


<script type="text/javascript">
   

    function resetForm() {
        $('.rstFrm').val('');
        $('.inputSl').val('');
        $('.inputSl').select2('');
    }

    function buttonSettingForLmsLesson() {
        $('.hideupdate').hide();
        $('.hideAdd').text('Add');
        $('.rstFrm').val('');
        $('.inputSl').val('');
        $('.inputSl').select2('');
    }

   


    function popitup(url) {
        newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    }
</script>