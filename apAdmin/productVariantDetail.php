<?php error_reporting(0);
if($_GET){
    $q=$d->select("vendor_product_variant_master,vendor_master,vendor_category_master,vendor_product_master","vendor_product_variant_master.vendor_id=vendor_master.vendor_id AND vendor_product_variant_master.vendor_category_id=vendor_category_master.vendor_category_id AND vendor_product_variant_master.vendor_product_id=vendor_product_master.vendor_product_id AND vendor_product_variant_master.vendor_product_variant_id='$_GET[id]' ");
    $data = mysqli_fetch_array($q);
	extract($data);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
          <h4 class="page-title">Product Variant Detail</h4>
        </div>
        <div class="col-sm-3 col-6 text-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <!-- <a href="addSiteBtn" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteSite');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-center">Product Variant Detail</h4>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span><b>Category:</b> <?php echo $data['vendor_category_name']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span><b>Vendor:</b> <?php echo $data['vendor_name']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span><b>Vendor Product:</b> <?php echo $data['vendor_product_name']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span><b>Variant Name:</b> <?php echo $data['vendor_product_variant_name']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span><b>Variant Price:</b> <?php echo $data['vendor_product_variant_price']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span><b>Popular Product:</b> <?php if($data['popular_product'] == 1){ echo "YES";}else{ echo "NO";} ?></span>
                    </div>
                    <div class="col-md-12 pt-4">
                        <span><b>Variant Description:</b> <?php echo $data['vendor_product_variant_description']; ?></span>
                    </div>
                </div>
                
                  <div class="table-responsive pt-4">
                    <div class="col-sm-12 col-12">
                        <div class="btn-group float-sm-right">
                            <?php
                              //echo $q=$d->count_data("vendor_product_image_master","vendor_product_variant_id='$data[vendor_product_variant_id]'");
                              $totalAssign = $d->count_data_direct("vendor_product_image_name","vendor_product_image_master","vendor_product_variant_id='$data[vendor_product_variant_id]'");
                              if($totalAssign < 5){
                            ?>
                            <a href="addAttendanceType" data-toggle="modal" data-target="#productVariantImageModal"  onclick="buttonSetting()" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Image</a>
                            <?php } ?>
                            <!-- <a href="javascript:void(0)" onclick="DeleteAll('deleteAttendanceType');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
                        </div>
                    </div>
                    <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Sr. No.</th>
                              <th>Image</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody id="showFilterData" class="purchase_requirements">
                          
                          <?php 
                            $q=$d->select("vendor_product_image_master","vendor_product_variant_id='$data[vendor_product_variant_id]'");
                            $counter = 1;
                            while ($productImages=mysqli_fetch_array($q)) {
                          ?>
                            <tr>
                                <td><?php echo $counter++; ?></td>
                                <td>
                                    <a href="../img/vendor_product/<?php echo $productImages['vendor_product_image_name']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $productImages['vendor_product_image_name']; ?>"><img width="50" height="50" onerror="this.onerror=null; this.src='../img/ajax-loader.gif'"  src="../img/ajax-loader.gif" data-src="../img/vendor_product/<?php echo $productImages['vendor_product_image_name']; ?>"  href="#divForm<?php echo $productImages['vendor_product_image_id']; ?>" class="btnForm lazyload" ></a>
                                </td>
                                <td>
                                <div class="d-flex align-items-center">
                                    <button type="button" class="btn btn-sm btn-primary mr-1" onclick="vendorProductImageSetData('<?php echo $productImages['vendor_product_image_id']; ?>', '<?php echo $productImages['vendor_product_variant_id']; ?>', '<?php echo $productImages['vendor_product_image_name']; ?>')"> <i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-sm btn-danger mr-1" onclick="DeleteProductVariantImage('deleteProductVariantImage', '<?php echo $productImages['vendor_product_image_id']; ?>', '<?php echo $productImages['vendor_product_image_name']; ?>')"> <i class="fa fa-trash-o fa-lg"></i></i></button>
                                    <?php if($productImages['vendor_product_image_active_status']=="0"){ ?>
                                        <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $productImages['vendor_product_image_id']; ?>','productVariantImageDeactive');" data-size="small"/>
                                    <?php } else { ?>
                                        <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $productImages['vendor_product_image_id']; ?>','productVariantImageActive');" data-size="small"/>
                                    <?php } ?>
                                </div>
                                </td>
                            </tr>
                          <?php } ?>     
                      </tbody>
                      
                    </table>
                  </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div><!--End content-wrapper-->
<div class="modal fade" id="productVariantImageModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Stationery Product</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           <form id="addProductVariantImageForm" action="controller/VendorProductVariantController.php" enctype="multipart/form-data" method="post">
           
                <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Image<span class="required">*</span></label>
                        <div class="col-lg-8 col-md-8" id="">   
                            <input type="file" class="form-control" name="vendor_product_image_name" value="" onchange="readURL(this);">            
                        </div>
                </div>
                <div class="form-group row" id="blah">
                    
                </div>                    
                <div class="form-footer text-center">
                    <input type="hidden" id="vendor_product_image_id" name="vendor_product_image_id" value="" >
                    <input type="hidden" id="vendor_product_variant_id" name="vendor_product_variant_id" value="<?php echo $_GET['id']; ?>" >
                    <input type="hidden" id="vendor_product_image_name_old" name="vendor_product_image_name_old" value="" >
                    <button id="addProductVariantImageBtn" name="addProductVariantImageBtn" type="submit"  class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addProductVariantImage" value="addProductVariantImage">
                    
                    <button id="addProductVariantImageBtn" type="submit"  class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
                
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetForm('addProductVariantImageForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                    
                </div>

         </form>
      
       </div>
      </div>
      
    </div>
  </div>
</div>
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //$('#blah').attr('src', e.target.result);
            $('#blah').html('<img src="'+e.target.result+'" width="75%" height="50%">');
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function resetForm()
{
  $('#vendor_product_image_name').val();
  $('#blah').html('<img  >');
}
function popitup(url) {
  newwindow=window.open(url,'name','height=800,width=900, location=0');
  if (window.focus) {newwindow.focus()}
    return false;
}
</script>
<style>
.hideupdate{
  display:none;
}

</style>