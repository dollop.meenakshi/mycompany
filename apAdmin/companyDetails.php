<?php
$q=$d->select("society_master","society_id='$society_id'");
$bData=mysqli_fetch_array($q);
extract($bData);
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="buildingDetails" action="controller/buildingController.php" method="post" enctype="multipart/form-data">
              <h4 class="form-header text-uppercase">
              <i class="fa fa-building"></i>
              <?php echo $xml->string->edit; ?> <?php echo $xml->string->BuildingDetails; ?>
              </h4>
              <div class="form-group row">
               
                <div class="col-lg-6 col-md-6  text-center">
                  <label for="soceityLogo">
                       <img  onerror="this.src='../img/defaultSocityLogo.jpg'"  id="societyLogoView" style="border: 1px solid gray; width: 250px; height: auto; border-radius: 15px;cursor: pointer;" src="<?php if(file_exists("../img/society/$socieaty_logo")) { echo '../img/society/'.$socieaty_logo; }  else { echo '../img/defaultSocityLogo.jpg'; } ?>"  src="#" alt="your image" class='' />
                     <input accept="image/*" class="photoInput photoOnly" id="soceityLogo" type="file" name="socieaty_logo">
                     <br>
                     <?php echo $xml->string->society; ?> <?php echo $xml->string->logo; ?> (300 PX * 200 PX)
                   </label>
                </div>
                <div class="col-lg-6 col-md-6  text-center">
                   <label for="soceityCover">
                       <img  onerror="this.src='../img/coverPhoto.jpg'"  id="societyCoverView" style="border: 1px solid gray; width: 100%; border-radius: 15px; height: auto;cursor: pointer;" src="<?php if(file_exists("../img/society/$socieaty_cover_photo")) { echo '../img/society/'.$socieaty_cover_photo; }  else { echo '../img/coverPhoto.jpg'; } ?>"  src="#" alt="your image" class='' />
                     <input accept="image/*" class="photoInput photoOnly" id="soceityCover" type="file" name="socieaty_cover_photo">
                     <br>
                     <?php echo $xml->string->society; ?> <?php echo $xml->string->photo; ?> (500 PX & 300 PX)
                   </label>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-1" class="col-sm-2 col-form-label"><?php echo $xml->string->society; ?> <?php echo $xml->string->name; ?> <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <input maxlength="50" required="" value="<?php echo $society_name; ?>" type="text" class="form-control" id="input-1" name="society_name_update">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-2" class="col-sm-2 col-form-label"><?php echo $xml->string->address; ?>  <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <textarea maxlength="300" required=""  name="society_address_edit" class="form-control" rows="4" id="input-2"><?php echo $society_address; ?></textarea>
                </div>
              </div>

              <div class="form-group row">
                <label for="input-3" class="col-sm-2 col-form-label"><?php echo $xml->string->company_website; ?></label>
                <div class="col-sm-4">
                  <input maxlength="100"  value="<?php echo $company_website; ?>" type="url" placeholder="http://www.example.com" class="form-control" id="input-3" name="company_website">
                </div>
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->default_timezone; ?> <span class="required">*</span></label>
                <div class="col-sm-4">
                  <select class="form-control single-select" id="input-10" name="default_time_zone" required="">
                    <option value="">-- Select --</option>
                    <?php $qt = $d->select("timezoneMaster","");
                    while ($timeData=mysqli_fetch_array($qt)) { ?>
                      <option <?php if($default_time_zone==$timeData['timezone_name']) { echo 'selected'; } ?> value="<?php echo $timeData['timezone_name'];?>"><?php echo $timeData['timezone_name'];?>-<?php echo $timeData['timezone_category'];?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-9" class="col-sm-2 col-form-label">Company Email <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                  <input value="<?php echo $secretary_email; ?>" required type="email" class="form-control"  name="secretary_email">
                 
                </div>
                <label for="input-9" class="col-sm-2 col-form-label">Company Contact <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                  <input value="<?php echo $secretary_mobile; ?>" required type="text" class="form-control"  name="secretary_mobile">
                  
                </div>
            </div>
             <div class="form-group row">
                  <label for="input-4" class="col-sm-2 col-form-label"><?php echo $xml->string->pincode; ?> <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" id="input-4"  maxlength="6" value="<?php if($society_pincode!='0') { echo $society_pincode; } ?>" required="" class="form-control onlyNumber" inputmode="numeric" name="society_pincode">
                  </div>
                <label for="input-5" class="col-sm-2 col-form-label"><?php echo $xml->string->tax_number; ?> </label>
                <div class="col-sm-4">
                  <input minlength="4" maxlength="30" value="<?php echo $gst_no; ?>" type="text" class="form-control text-uppercase alphanumeric" id="input-5" name="gst_no">
                </div>
              </div>
               <div class="form-group row">
                  <label for="input-6" class="col-sm-2 col-form-label"><?php echo $xml->string->pan_number; ?></label>
                  <div class="col-sm-4">
                    <input type="text" id="input-6"  maxlength="30" value="<?php  echo $pan_number; ?>" class="form-control text-uppercase" name="pan_number">
                  </div>
                <label for="input-7" class="col-sm-2 col-form-label"><?php echo $xml->string->currency_symbol; ?>  <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" minlength="1" maxlength="4" value="<?php echo $currency; ?>" type="text" class="form-control" id="input-7" name="currency">
                </div>
              </div>
                <div class="form-group row">
                
                <label for="input-8" class="col-sm-2 col-form-label"><?php echo $xml->string->visitors; ?> <?php echo $xml->string->auto_rejected; ?> <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input min="5" max="120" minlength="1" required="" maxlength="10" value="<?php echo $auto_reject_vistor_minutes; ?>" type="text" class="form-control onlyNumber" inputmode="numeric" id="input-8" name="auto_reject_vistor_minutes">
                  <i>In Minutes</i>
                </div>
              
               <?php
                if ($adminData['admin_type']==1 || $adminData['role_id']==2) { ?>
             
                <label for="input-9" class="col-sm-2 col-form-label"><?php echo $xml->string->complaint; ?> <?php echo $xml->string->reopen; ?> <?php echo $xml->string->max_time; ?><span class="required">*</span></label>
                <div class="col-sm-4">
                  <input min="1" max="46400" minlength="1" required="" maxlength="10" value="<?php echo $complaint_reopen_minutes; ?>" type="text" class="form-control onlyNumber" inputmode="numeric" id="input-9" name="complaint_reopen_minutes">
                  <i>In Minutes</i>
                </div>
            <?php  } else { ?>
                <input min="1" minlength="1" required="" maxlength="3" value="<?php echo $complaint_reopen_minutes; ?>" type="hidden" class="form-control onlyNumber" inputmode="numeric" id="input-10" name="complaint_reopen_minutes">
            <?php } ?>
            </div>
            
            
            <div class="form-group row">
              <input id="searchInput5" class="form-control" type="text" placeholder="Enter a Google location" >
              <div class="map" id="map" style="width: 100%; height: 400px;"></div>
            </div>
           
              <input  type="hidden" class="form-control" id="lat" name="society_latitude"  placeholder="Lattitude"  value="<?php echo $bData['society_latitude']; ?>">
              <input  type="hidden" class="form-control" id="lng"  name="society_longitude"  placeholder="sociaty_longitude" value="<?php echo $bData['society_longitude']; ?>">
             <div class="form-footer text-center">
                 <input type="hidden" value="<?php echo $socieaty_logo; ?>" class="form-control" id="input-4"  name="socieaty_logo_old" required>
                 <input type="hidden" value="<?php echo $socieaty_cover_photo; ?>" class="form-control" id="input-4"  name="socieaty_cover_photo_old" required>
                <input type="hidden" name="updateBuildingSingle" value="updateBuildingSingle">
                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
            </div>
            </form>
          </div>
        </div>
      </div>
      </div><!--End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->


    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $d->map_key();?>"></script>
<script>

    /* script */
    function initialize() {
       // var latlng = new google.maps.LatLng(23.05669,72.50606);
       

      var latitute =document.getElementById('lat').value;
      var longitute =document.getElementById('lng').value;
      var latlng = new google.maps.LatLng(latitute,longitute);

        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: true,
          anchorPoint: new google.maps.Point(0, -29)
          // icon:'img/direction/'+dirction+'.png'
       });
       var parkingRadition = 5;
        var citymap = {
            newyork: {
              center: {lat: latitute, lng: longitute},
              population: parkingRadition
            }
        };
       
        var input = document.getElementById('searchInput5');
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var geocoder = new google.maps.Geocoder();
        var autocomplete10 = new google.maps.places.Autocomplete(input);
        autocomplete10.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();   
        autocomplete10.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place5 = autocomplete10.getPlace();
            if (!place5.geometry) {
                window.alert("Autocomplete's returned place5 contains no geometry");
                return;
            }
      
            // If the place5 has a geometry, then present it on a map.
            if (place5.geometry.viewport) {
                map.fitBounds(place5.geometry.viewport);
            } else {
                map.setCenter(place5.geometry.location);
                map.setZoom(17);
            }
           
            marker.setPosition(place5.geometry.location);
            marker.setVisible(true);          
            
            var pincode="";
            for (var i = 0; i < place5.address_components.length; i++) {
              for (var j = 0; j < place5.address_components[i].types.length; j++) {
                if (place5.address_components[i].types[j] == "postal_code") {
                  pincode = place5.address_components[i].long_name;
                  // alert(pincode);
                }
              }
            }
            bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
            infowindow.setContent(place5.formatted_address);
            infowindow.open(map, marker);
           
        });
        // this function will work on marker move event into map 
        google.maps.event.addListener(marker, 'dragend', function() {
            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) { 
               var places = results[0]       ;
               console.log(places);
               var pincode="";
               var serviceable_area_locality= places.address_components[4].long_name;
               // alert(serviceable_area_locality);
            for (var i = 0; i < places.address_components.length; i++) {
              for (var j = 0; j < places.address_components[i].types.length; j++) {
                if (places.address_components[i].types[j] == "postal_code") {
                  pincode = places.address_components[i].long_name;
                  // alert(pincode);
                }
              }
            }
                  bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
                  // infowindow.setContent(results[0].formatted_address);
                  // infowindow.open(map, marker);
              }
            }
            });
        });
    }
    function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality){
       // document.getElementById('poi_point_address').value = address;
       document.getElementById('lat').value = lat;
       document.getElementById('lng').value = lng;
       // document.getElementById('collection_center_pincode').value = pin_code;
       // document.getElementById('serviceable_area_locality').value = serviceable_area_locality;

        // document.getElementById("POISubmitBtn").removeAttribute("hidden"); 
        // initialize();
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>