  <?php error_reporting(0);

  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  $cId = (int)$_REQUEST['cId'];
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year'] = $_REQUEST['month_year'];

  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Course View Report</h4>
        </div>
     
     </div>
     </div>
      <form action="" class="branchDeptFilter">
        <div class="row ">
          <?php include 'selectBranchDeptEmpForFilterAll.php' ?>
          <div class="col-md-2 col-6 form-group">
            <select required name="cId" class="form-control single-select" >
              <option value="">-- Select Course --</option>
              <?php
              $dq = $d->select("course_master", "society_id='$society_id' AND course_is_deleted=0 ");
              while ($dData = mysqli_fetch_array($dq)) {
              ?>
                <option <?php if ($cId == $dData['course_id']) { echo 'selected';
                        } ?> value="<?php echo $dData['course_id']; ?>"><?php echo $dData['course_title']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-1 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get">
            </div>
        </div>
      </form> 
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>                        
                        <th>completed Per.</th>                      
                        <th>Course</th>                      
                        <th>Name</th>                      
                        <th>Branch</th>                      
                        <th>Department</th>                      
                        <th>start date</th>                      
                        <th>completed date</th>                      
                        <!-- <th>Action</th> -->
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;

                      if (isset($dId) && $dId > 0) {
                        $deptFilterQuery = " AND users_master.floor_id='$dId'";
                      }

                      if(isset($bId) && $bId>0) {
                        $blockFilterQuery = " AND users_master.block_id='$bId'";
                      }

                     
                      if (isset($uId) && $uId > 0) {
                        $userFilterQuery = "AND lms_submit_master.user_id='$uId'";
                      }

                    if(isset($cId) && $cId>0) {
                      $courseFilterQuery = " AND lms_submit_master.course_id='$cId'";
                    }

                    $q=$d->select("course_master,lms_submit_master,users_master,block_master,floors_master","block_master.block_id=floors_master.block_id AND block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id  AND users_master.user_id=lms_submit_master.user_id AND course_master.course_id=lms_submit_master.course_id AND course_master.society_id='$society_id' AND course_master.course_is_deleted=0 AND lms_submit_master.course_id='$cId' $blockFilterQuery $deptFilterQuery $userFilterQuery ","");  
                    $counter = 1;
                    
                    while ($data=mysqli_fetch_array($q)) {
                     lms_submit_master
                     ?>
                    <tr>

                        <td><?php echo $counter++; ?></td>
                        <td><?php echo $data['completed_percentage'].' %'; ?></td>
                        <td><?php echo $data['course_title']; ?></td>
                        <td><?php echo $data['user_full_name'] .' ('.$data['user_designation'].')'; ?></td>
                        <td><?php echo $data['block_name']; ?></td>
                        <td><?php echo $data['floor_name']; ?></td>
                        <td><?php echo date("d M Y h:i A", strtotime($data['submitted_date'])); ?></td>
                        <td><?php if($data['completed_date']!='') { echo date("d M Y h:i A", strtotime($data['completed_date'])); } ?></td>
                        <!-- <td></td> -->
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

