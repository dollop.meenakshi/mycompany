<style>
  #map {
    height: 500px;
  }
  .gm-style img {
     max-width: none;
     border-radius: 25px !important;
    }
</style>

  <div id="map"></div>

<script>
  var zoomLevel = 13;
  var customLabel = {
    
  };
  function initMap() {
    // var zoomLevel = parseInt(document.getElementById('zoomLevel').value);
    // alert(zoomLevel);
    
    var bId = '<?php echo $bId; ?>';
    var dId = '<?php echo $dId; ?>';
    var mylat = <?php echo $sData['society_latitude']; ?>;
    var mylog = <?php echo $sData['society_longitude']; ?>;
    var map = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng(mylat, mylog),
      zoom: zoomLevel

    });

      google.maps.event.addListener(map, 'zoom_changed', function() {
        zoomLevel = map.getZoom();
      });

      // Change this depending on the name of your PHP or XML file
      downloadUrl('multiUserData.php?bId='+bId+'&dId='+dId, function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName('marker');
        Array.prototype.forEach.call(markers, function(markerElem) {
          var infoWindow = new google.maps.InfoWindow;
          
          var id = markerElem.getAttribute('id');
          var user_id = markerElem.getAttribute('user_id');
          var user_profile_pic = markerElem.getAttribute('user_profile_pic');
          var user_full_name = markerElem.getAttribute('user_full_name');
          var user_mobile = markerElem.getAttribute('user_mobile');
          var lastTime = markerElem.getAttribute('lastTime');
          var user_designation = markerElem.getAttribute('user_designation');
          var branch_name = markerElem.getAttribute('branch_name');
          var lastAddress = markerElem.getAttribute('lastAddress');
          var department_name = markerElem.getAttribute('department_name');
          var last_tracking_battery_status = markerElem.getAttribute('last_tracking_battery_status');
          var last_tracking_gps_accuracy = markerElem.getAttribute('last_tracking_gps_accuracy');
          var image = markerElem.getAttribute('image');
          var point = new google.maps.LatLng(
            parseFloat(markerElem.getAttribute('lat')),
            parseFloat(markerElem.getAttribute('lon')));
          infoWindow.setContent("<div class='mapDiv' style = 'width:280px;min-height:40px;text-align:left;'><img width='50' height='50' src=../img/users/recident_profile/"+user_profile_pic+"> <br><b>Employee :</b>"+user_full_name+" <br><b> Designation:</b>"+user_designation+"<br><b> Address:</b>"+lastAddress+" <br><b> Last Update :</b>"+lastTime+" <br> <b>Mobile:</b>"+user_mobile+" <br><b>Branch: </b>"+branch_name+" <br><b> Department:</b>"+department_name+" <br><b> Phone Battery Level:</b>"+last_tracking_battery_status+" <br><b> GPS Accuracy:</b>"+last_tracking_gps_accuracy+" <br>  </div>");
          infoWindow.open(map, marker);
          var icon = customLabel[image] || {};

          var icon = {
              url: image, // url
              scaledSize: new google.maps.Size(30, 30), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
          };

          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: icon,
            path: google.maps.SymbolPath.CIRCLE,
            scale: 10
          });





          marker.addListener('click', function() {
            // infoWindow.setContent(infowincontent);
            infoWindow.open(map, marker);
          });
        });
      });

  

    }



    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
      new ActiveXObject('Microsoft.XMLHTTP') :
      new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }
    
    function doNothing() {}

  </script>
  


  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>