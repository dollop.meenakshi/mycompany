<?php
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
extract(array_map("test_input" , $_POST));
if(isset($addBankDetails)){ ?>
  <div class="form-group row">
    <label for="payment_bank" class="col-sm-2 col-form-label">Bank Name <span class="required">*</span></label>
    <div class="col-sm-4" >
      <input type="text" name="payment_bank" id="payment_bank" class="form-control alphanumeric" maxlength="30" required="">
    </div>
    <label for="payment_number" class="col-sm-2 col-form-label">Cheque No. <span class="required">*</span></label>
    <div class="col-sm-4" >
      <input type="hidden" name="payment_name" value="Cheque">
      <input type="text" id="payment_number" name="payment_number" class="form-control number" required="" minlength="6" maxlength="6">
    </div>
  </div>
  <script type="text/javascript">
    $('.number').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
  </script>
<?php } if (isset($onlineBankDetails)) {  ?>
  <div class="form-group row">
    <label for="payment_bank" class="col-sm-2 col-form-label">Bank Name <span class="required">*</span></label>
    <div class="col-sm-4" >
      <input type="text" id="payment_bank" name="payment_bank" class="form-control alphanumeric" maxlength="30" required="">
    </div>
    <label for="payment_number" class="col-sm-2 col-form-label">Reference No. <span class="required">*</span></label>
    <div class="col-sm-4" >
      <input type="text" id="payment_number" name="payment_number" class="form-control" required="" maxlength="15">
    </div>
  </div>
<?php } if(isset($addBankDetailsModal)){ ?>
  <div class="form-group row">
    <label for="payment_bank" class="col-sm-4 col-form-label">Bank Name <span class="required">*</span></label>
    <div class="col-sm-8">
      <input type="text" name="payment_bank" id="payment_bank" class="form-control alphanumeric" maxlength="30" required="">
    </div>
  </div>
  <div class="form-group row">
    <label for="payment_number" class="col-sm-4 col-form-label">Cheque No. <span class="required">*</span></label>
    <div class="col-sm-8">
      <input type="hidden" name="payment_name" value="Cheque">
      <input type="text" id="payment_number" name="payment_number" class="form-control number" required="" minlength="6" maxlength="6">
    </div>
  </div>
  <script type="text/javascript">
    $('.number').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
  </script>
<?php }  if (isset($onlineBankDetailsModal)) {  ?>
  <div class="form-group row">
    <label for="payment_bank" class="col-sm-4 col-form-label">Bank Name <span class="required">*</span></label>
    <div class="col-sm-8">
      <input type="text" name="payment_bank" id="payment_bank" class="form-control alphanumeric" maxlength="30" required="">
    </div>
  </div>
  <div class="form-group row">
    <label for="payment_number" class="col-sm-4 col-form-label">Reference No. <span class="required">*</span></label>
    <div class="col-sm-8">
      <input type="text" id="payment_number" name="payment_number" class="form-control alphanumeric" required="" maxlength="15">
    </div>
  </div>
<?php } ?>
<script type="text/javascript">
  $(".alphanumeric").keypress(function (e) {
  var keyCode = e.which;
  if ( !( (keyCode >= 48 && keyCode <= 57) 
   ||(keyCode >= 65 && keyCode <= 90) 
   || (keyCode >= 97 && keyCode <= 122) ) 
   && keyCode != 8 && keyCode != 32) {
   e.preventDefault();
  }
});
</script>