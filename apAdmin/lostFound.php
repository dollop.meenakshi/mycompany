<?php
extract(array_map("test_input" , $_POST));
?>
<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Lost & Found</h4>
      </div>
    </div>
    <div class="row">
      <?php   
      // $blockAppendQueryUser
        $q=$d->select("lost_found_master","society_id='$society_id'  AND active_status='0' " );
        if(mysqli_num_rows($q)>0) {

          while($row=mysqli_fetch_array($q)){ 
           if ($row['user_id']!=0) {
              $qbb=$d->select("lost_found_master,users_master,block_master,unit_master","lost_found_master.society_id='$society_id'  AND lost_found_master.active_status='0' AND  lost_found_master.user_id=users_master.user_id  AND block_master.block_id=unit_master.block_id AND block_master.block_id=lost_found_master.block_id AND unit_master.unit_id=lost_found_master.unit_id AND lost_found_master.user_id!=0 AND lost_found_master.lost_found_master_id='$row[lost_found_master_id]'","" );
                $subData=mysqli_fetch_array($qbb);
                $userName=$subData['user_full_name'].'-'.$subData['user_designation'] ;
             } else {
                $qbb=$d->select("lost_found_master,employee_master","lost_found_master.society_id='$society_id'  AND lost_found_master.active_status='0' AND  lost_found_master.gatekeeper_id=employee_master.emp_id  AND lost_found_master.lost_found_master_id='$row[lost_found_master_id]'","" );
                  $subData=mysqli_fetch_array($qbb);
                  $userName= $subData['emp_name'].'-Security Guard';
              }
              
            $path = '../img/lostFound/'.$row['lost_found_image'];
          ?> 
          <div class="col-lg-3">
           <div class="card">
            <a>
              <img  onerror="this.src='../img/app_icon/Lost-foundxxxhdpi.png'" style="width: 100%;height: 200px;" <?php if(file_exists($path) && $row['lost_found_image'] != ''){ ?> class="btnForm lazyload" <?php } ?> href="#divForm<?php echo $row['lost_found_master_id'];?>" src="../img/app_icon/Lost-foundxxxhdpi.png" data-src="../img/lostFound/<?php echo $row['lost_found_image']; ?>" alt="<?php echo $row['lost_found_title']; ?>"></a>
              <div id="divForm<?php echo $row['lost_found_master_id'];?>" style="display:none">
                <picture>
                  <source srcset="../img/lostFound/<?php echo $row['lost_found_image']; ?>" media="(max-width: 800px)">
                  <img style="max-height: 500px; max-width: 500px;"   src="../img/lostFound/<?php echo $row['lost_found_image']; ?>" />
                </picture>
                <br>
                <span><center><b><?php custom_echo($row['lost_found_title'],28); ?></b></center></span>
              </div>
              <div class="p-2">
                 <h5 class="card-title text-primary"><?php custom_echo($row['lost_found_title'],18); ?>
                   <?php if($row['lost_found_type']==0)  { ?>
                   <span   data-toggle="modal" data-target="#userDetails" onclick="viewLostFound('<?php echo $row['lost_found_master_id']; ?>');" class="badge badge-success m-1 pointerCursor">Found by</span>
                  <?php } else { ?>
                   <span   data-toggle="modal" data-target="#userDetails" onclick="viewLostFound('<?php echo $row['lost_found_master_id']; ?>');" class="badge badge-danger m-1 pointerCursor">Lost by</span>

                  <?php } ?>
                 </h5><span   data-toggle="modal" data-target="#userDetails" onclick="viewLostFound('<?php echo $row['lost_found_master_id']; ?>');" class="pointerCursor"><?php custom_echo($userName,25); ?></span>
                 <hr>
                  
                  <span>Date: <?php echo $row['lost_found_date']; ?></span><br>
                  <?php
                   
                   if (count($blockAryAccess)!=0 && $row['user_id']!=0 && in_array($subData['block_id'], $blockAryAccess) ) { 
                   ?>
                    <form action="controller/buildingController.php" method="post">
                    <input type="hidden" name="lost_found_master_id"  value="<?php echo $row['lost_found_master_id']; ?>">
                     <button type="submit" class="form-btn btn btn-sm btn-danger waves-effect waves-light m-1" title="Delete"> <i class="fa fa-trash-o"></i> Delete</button>
                   </form>
                  <?php } else if($row['user_id']!=0 && count($blockAryAccess)==0 ) { ?>
                   <form action="controller/buildingController.php" method="post">
                    <input type="hidden" name="lost_found_master_id"  value="<?php echo $row['lost_found_master_id']; ?>">
                     <button type="submit" class="form-btn btn btn-sm btn-danger waves-effect waves-light m-1" title="Delete"> <i class="fa fa-trash-o"></i> Delete</button>
                   </form>
                    <?php } else if($row['user_id']==0) { ?>
                   <form action="controller/buildingController.php" method="post">
                    <input type="hidden" name="lost_found_master_id"  value="<?php echo $row['lost_found_master_id']; ?>">
                     <button type="submit" class="form-btn btn btn-sm btn-danger waves-effect waves-light m-1" title="Delete"> <i class="fa fa-trash-o"></i> Delete</button>
                   </form>
                   <?php }  ?>
              </div>
           </div>
          </div>
        <?php }  
        } else {
        echo "<img src='img/no_data_found.png'>";
      } ?>
    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  function viewLostFound(lost_id) {
    $.ajax({
      url: "getLostFoundUserDetails.php",
      cache: false,
      type: "POST",
      data: {lost_id : lost_id},
      success: function(response){
        $('#userDetailsModal').html(response);  
      }
    });
  } 
</script>
<script type="text/javascript">
  $(function(){
    $(".btnForm").fancybox();
  });
</script>

<div class="modal fade" id="userDetails">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">View Details </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="userDetailsModal">
          
      </div>
     
    </div>
  </div>
</div>