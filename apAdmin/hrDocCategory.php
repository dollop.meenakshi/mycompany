  <?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Document Category</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
       <div class="btn-group float-sm-right">
        <a href="javascript:void(0)" data-toggle="modal" data-target="#addModal"  onclick="buttonSettingForHRDoCat()" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteHrDoc');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
      </div>
     </div>
     </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                   <?php
                      $i = 1;
                      $q = $d->select("hr_document_category_master", "society_id='$society_id' AND hr_document_category_deleted=0 ");
                      $counter = 1;
                      while ($data = mysqli_fetch_array($q)) {
                          ?>
                      <tr>
                          <td class="text-center">
                            <?php $totalDocuemnt = $d->count_data_direct("hr_document_id", "hr_document_master", "hr_document_category_id='$data[hr_document_category_id]'");
                          if ($totalDocuemnt == 0) {
                              ?>
                              <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['hr_document_category_id']; ?>">
                            <?php }?>
                          </td>
                          <td><?php echo $counter++; ?></td>
                          <td><?php echo $data['hr_document_category_name']; ?></td>
                          <td><?php echo date("d M Y h:i A", strtotime($data['hr_document_category_created_date'])); ?></td>
                          <td>
                            <div class="d-flex align-items-center">
                                <form  method="post" accept-charset="utf-8">
                                    <input type="hidden" name="hr_document_category_id" value="<?php echo $data['hr_document_category_id']; ?>">
                                    <input type="hidden" name="edit_hr_doc" value="edit_hr_doc">
                                    <button type="button" onclick="HrDocCatDataSet(<?php echo $data['hr_document_category_id']; ?>)" data-toggle="modal" data-target="#addModal" class="btn btn-sm btn-primary mr-1"><i class="fa fa-pencil"></i></button>
                                </form>
                                <?php if ($data['hr_document_category_status'] == "0") {
                                      $status = "hrCategoryStatusDeactive";
                                      $active = "checked";
                                  } else {
                                      $status = "hrCategoryStatusActive";
                                      $active = "";
                                  }?>
                                <input  type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['hr_document_category_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                             </div>
                          </td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->


   <!-- FOr add edit hr doc -->
<div class="modal fade" id="addModal">
        <?php
          extract(array_map("test_input", $_POST));
          if (isset($edit_hr_doc)) {
              $q = $d->select("hr_document_category_master", "hr_document_category_id='$hr_document_category_id'");
              $data = mysqli_fetch_array($q);
              extract($data);
          }?>
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Document Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body">
           <form id="hrDocAdd" action="controller/hrDocCategoryController.php" enctype="multipart/form-data" method="post">
           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Document Category Name <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
                   <input type="text" class="form-control" id="hr_document_category_name" placeholder="HR Document Category Name" name="hr_document_category_name" value="<?php if ($data['hr_document_category_name'] != "") {echo $data['hr_document_category_name'];}?>">
               </div>
           </div>
           <div class="form-footer text-center">
             <input type="hidden" name="hr_document_category_id" id="hr_document_category_id" value="<?php if ($data['hr_document_category_id'] != "") {echo $data['hr_document_category_id'];}?>" >
             <button id="addHrDocBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addHrDoc"  value="addHrDoc">
              <button id="addHrDocBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('hrDocAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
             <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
           </div>
         </form>
       </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }

function buttonSettingForHRDoCat(){
  $('.hideupdate').hide();
  $('.hideAdd').show();
  $('#hr_document_category_name').val('');
  $('#hr_document_category_id').val('');
}
</script>.
<style>
.hideupdate{
  display:none;
}

</style>