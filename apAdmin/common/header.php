<?php 
error_reporting(0);
session_start();
$_SESSION["token"] = md5(uniqid(mt_rand(), true));

include 'object.php';
include 'appLoginCheck.php'; //admin app only
include 'checkLogin.php';
if (isset($_COOKIE['bms_admin_id'])) {
    $bms_admin_id = $_COOKIE['bms_admin_id'];
    $aq=$d->select("bms_admin_master,role_master","role_master.role_id=bms_admin_master.role_id AND bms_admin_master.admin_id='$bms_admin_id'");
    $adminData= mysqli_fetch_array($aq);
    $access_branchs=$adminData['access_branchs'];
    $access_departments=$adminData['access_departments'];
    $blockAryAccess = array();
    $departmentAry = array();
    $blockAryAccess = explode(",", $access_branchs);
    $departmentAry = explode(",", $access_departments);

    if ($adminData['admin_active_status']==1) {
       $_SESSION['msg1']="Your Account Deactivated.";
       header("location:logout.php");
       exit();
    }
     if(isset($_COOKIE['society_type']) &&  $_COOKIE['society_type']==1) { 
      $society_type = 1;
     } else  if(isset($_COOKIE['society_type']) &&  $_COOKIE['society_type']==0) { 
      $society_type = 0;
     } else {
      $society_type = 0 ;
     }
}
include 'accessControl.php';
include 'accessControlPageNew.php';
include 'checkLanguage.php';

if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
  if ($adminData['role_id']!=1) {
    $checS=$d->select("society_master","society_id='$society_id'");
    $sData=mysqli_fetch_array($checS);
    $plan_expire_date= $sData['plan_expire_date'];
    $financial_year_start= $sData['financial_year_start'];
    $financial_year_end= $sData['financial_year_end'];
    $socieaty_logo= $sData['socieaty_logo'];
    $currency= $sData['currency'];
    $virtual_wallet= $sData['virtual_wallet'];
    $socieaty_cover_photo= $sData['socieaty_cover_photo'];
    if ($financial_year_start!='' && $financial_year_end!="0000-00-00" && $financial_year_end!='' && $financial_year_end!="0000-00-00") {
       $date111=date_create($financial_year_start);
       $dateTo1111=date_create($financial_year_end);
       $financial_year_start= date_format($date111,"Y-m-d 00:00:00");
       $financial_year_end= date_format($dateTo1111,"Y-m-d 23:59:59");
       $financial_year = "true";
    } else {
       $financial_year = "false";
    }

    $today= date('Y-m-d');
    $date1 = new DateTime($today);
    $date2 = new DateTime($plan_expire_date);
    $interval = $date1->diff($date2);
    $difference_days= $interval->days . " days "; 

    $trial_days= $sData['trial_days'];
    $package_id= $sData['package_id'];
      if ($sData<1) {
        header("location:logout.php");
      } else {
         if ($sData['society_status']==1) {
           $_SESSION['msg1']="Your Account Deactivated.";
           header("location:logout1.php");
         }
      }
    if ($today>$plan_expire_date) {
      // echo "Plan Expire";
        if ($_GET['f']!='buyPlan') {
            $_SESSION['msg1']="Your Plan Has been Expire Please Renew Now";
            header("location:buyPlan");
        }

    }
  } else {
    $currency= '₹';
  }
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
  
  if ($access_branchs!='') {
    $blockids = join("','",$blockAryAccess); 
    if ($access_departments!='') {
      $departmentIds = join("','",$departmentAry); 
      $restictDepartment = " AND floors_master.floor_id IN ('$departmentIds')";
      $restictDepartmentUser = " AND users_master.floor_id IN ('$departmentIds')";
    } else {
      $restictDepartment = "";
      $restictDepartmentUser = "";
    }

    $blockAppendQuery = " AND block_master.block_id IN ('$blockids')";
    $blockAppendQueryUnit = " AND unit_master.block_id IN ('$blockids')";
    $blockAppendQueryFloor = " AND floors_master.block_id IN ('$blockids') $restictDepartment";
    $blockAppendQueryUser = " AND users_master.block_id IN ('$blockids') $restictDepartmentUser";
    $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

  } else {
    $blockAppendQuery = "";
    $blockAppendQueryUnit = "";
    $blockAppendQueryFloor = "";
    $blockAppendQueryUser = "";
    $blockAppendQueryOnly="";
  }
 if(file_exists("../img/society/$adminData[admin_profile]")) {
    $profilePath = "../img/society/$adminData[admin_profile]";
  } else {
    $profilePath= "img/user.png";
  }
  
  if(isset($_GET['CId']) && !empty($_GET['CId']) && ctype_digit($_GET['CId']))
  {
    setcookie('CId', $_GET['CId'], time() + (86400 * 5 ), "/$cookieUrl");
  }

  if(isset($_GET['SCId']) && !empty($_GET['SCId']))
  {
    $ci_st_id = explode("-",$_GET['SCId']);
    $city_id = $ci_st_id[0];
    $state_id = $ci_st_id[1];
    if(ctype_digit($city_id))
    {
      setcookie('CIId', $city_id, time() + (86400 * 5 ), "/$cookieUrl");
    }

    if(ctype_digit($state_id))
    {
      setcookie('SId', $state_id, time() + (86400 * 5 ), "/$cookieUrl");
    }
  }

  if(isset($_GET['AId']) && !empty($_GET['AId']) && ctype_digit($_GET['AId']))
  {
    setcookie('AId', $_GET['AId'], time() + (86400 * 5 ), "/$cookieUrl");
  }
  elseif(isset($_GET['AId']) && $_GET['AId'] == 0)
  {
    setcookie('AId', $_GET['AId'], time() + (86400 * 5 ), "/$cookieUrl");
  }

  if(isset($_GET['RId']) && !empty($_GET['RId']) && ctype_digit($_GET['RId']))
  {
    setcookie('RId', $_GET['RId'], time() + (86400 * 5 ), "/$cookieUrl");
  }

  if(isset($_GET['PId']) && !empty($_GET['PId']) && ctype_digit($_GET['PId']))
  {
    setcookie('PId', $_GET['PId'], time() + (86400 * 5 ), "/$cookieUrl");
  }

  if(isset($_GET['PCId']) && !empty($_GET['PCId']) && ctype_digit($_GET['PCId']))
  {
    setcookie('PCId', $_GET['PCId'], time() + (86400 * 5 ), "/$cookieUrl");
  }
  if(isset($_GET['PSCId']) && !empty($_GET['PSCId']) && ctype_digit($_GET['PSCId']))
  {
    setcookie('PSCId', $_GET['PSCId'], time() + (86400 * 5 ), "/$cookieUrl");
  }
  if(isset($_GET['UId']) && !empty($_GET['UId']) && ctype_digit($_GET['UId']))
  {
    setcookie('UId', $_GET['UId'], time() + (86400 * 5 ), "/$cookieUrl");
  }
  if(isset($_GET['Df']) && !empty($_GET['Df']))
  {
    $format = 'Y-m-d';
    $daf = DateTime::createFromFormat($format, $_GET['Df']);
    $resultf = $daf && $daf->format($format) === $_GET['Df'];
    if($resultf == 1)
    {
      setcookie('Df', $_GET['Df'], time() + (86400 * 5 ), "/$cookieUrl");
    }
  }
  if(isset($_GET['Dt']) && !empty($_GET['Dt']))
  {
    $format = 'Y-m-d';
    $dat = DateTime::createFromFormat($format, $_GET['Dt']);
    $resultt = $dat && $dat->format($format) === $_GET['Dt'];
    if($resultt == 1)
    {
      setcookie('Dt', $_GET['Dt'], time() + (86400 * 5 ), "/$cookieUrl");
    }
  }
  if(isset($_GET['DId']) && !empty($_GET['DId']) && ctype_digit($_GET['DId']))
  {
    setcookie('DId', $_GET['DId'], time() + (86400 * 5 ), "/$cookieUrl");
  }
  if(isset($_GET['Os']) && !empty($_GET['Os']) && ctype_digit($_GET['Os']))
  {
    setcookie('Os', $_GET['Os'], time() + (86400 * 5 ), "/$cookieUrl");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title><?php  echo ucwords($_GET['f']); ?> | MyCo</title>
  <!--favicon-->
  <link rel="icon" href="../img/fav.png" type="image/png">
  <!-- simplebar CSS-->
  <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="assets/css/sidebar-menu2.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <?php include 'colours.php'; ?>
  <link href="assets/css/app-style9.css" rel="stylesheet"/>
  <link href="assets/plugins/fullcalendar/css/fullcalendar.min.css" rel='stylesheet'/>

  <link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <!--Lightbox Css-->
  <link href="assets/plugins/fancybox/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>
<!-- pg-calendar-master -->

<link href="assets/plugins/pg-calendar-master/dist/css/pignose.calendar.css" rel="stylesheet" type="text/css"/>

  <!-- notifications css -->
  <link rel="stylesheet" href="assets/plugins/notifications/css/lobibox.min.css"/>

  <!--Select Plugins-->
  <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet"/>
  <!--inputtags-->
  <link href="assets/plugins/inputtags/css/bootstrap-tagsinput.css" rel="stylesheet" />
  <!--multi select-->
  <link href="assets/plugins/jquery-multi-select/multi-select.css" rel="stylesheet" type="text/css">
  <!--Bootstrap Datepicker-->
  <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">

  <!--material datepicker css-->
  <link rel="stylesheet" href="assets/plugins/material-datepicker/css/bootstrap-material-datetimepicker.min.css">
  <link rel="stylesheet" href="assets/css/googleapisMaterialIcons.css">

  <!--Select Plugins-->
  <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet"/>

   <!--Switchery-->
  <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

  <link href="assets/plugins/bootstrap-switch/bootstrap-switch.min.css" rel="stylesheet">
  <link href="assets/css/jquery-ui.css" rel="stylesheet">

  <!-- 20220728 -->
  <link rel="stylesheet" type="text/css" href="assets/css/daterangepicker.css" />
  <!-- 20220728 -->
  
</head>

<body>

<div class="ajax-loader">
  <img src="../img/ajax-loader.gif" class="img-responsive" />
</div>

<div id="spinner">  </div>

<!-- Start wrapper-->
 <div id="wrapper">
 <input id="reportName" type="hidden" value="<?php echo $_COOKIE['society_name']; ?>_<?php  echo ucwords($_GET['f']); ?>_<?php echo date('Y-m-d_H_i');?>">
  <!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper"  data-simplebar="" data-simplebar-auto-hide="true">
     <div class="brand-logo">
      <a href="welcome">
       <img src="../img/logoWhiteBg.png" class="logo-icon" alt="logo">
       <h5 class="logo-text text-white">Admin</h5>
     </a>
   </div>
    <?php include 'sidebar.php'; ?>
   
   </div>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top bg-primary">
  <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
    </li>
    <li class="nav-item">
      <form  autocomplete="off" class="search-bar" action="">
        <input type="text" id="searchMenu"  class="form-control" placeholder="Search..">
         <a href="javascript:void();"><i class="icon-magnifier"></i></a>
      </form>
    </li>
  </ul>
     
  <ul class="navbar-nav align-items-center right-nav-link">
   <?php if ($adminData['role_id']!=1){ ?>
    
    <li class="nav-item dropdown-lg">
      <a  data-toggle="modal" data-target="#notification"  class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect"  href="javascript:void();">
      <i class="fa fa-bullhorn"></i><span class="badge badge-warning badge-up">+</span></a>
    </li>
  <?php } ?>
    <li class="nav-item dropdown-lg">
      <!-- 20220728 -->
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
    <i class="fa fa-bell-o"></i><span class="badge badge-info badge-up" id="notificationCount"><?php 
       /*$totalNotification= $d->count_data_direct("notification_id","admin_notification","society_id='$society_id' AND admin_id =0 AND read_status=0 OR society_id='$society_id' AND read_status=0 AND admin_id ='$bms_admin_id'");
       if ($totalNotification<100) {
         echo $totalNotification;
       } else {
        echo "99+";
       }*/
     ?></span></a>
     <!-- 20220728 -->
      <div class="dropdown-menu dropdown-menu-right" style="right: 15px !important;">
        <ul class="list-group list-group-flush">
          <li class="list-group-item d-flex justify-content-between align-items-center">
           <?php echo $xml->string->new; ?> <?php echo $xml->string->notifications; ?>
          <span class="badge badge-info"><?php 
          echo $totalNotification;
          ?></span>
         <?php  $aq=$d->select("admin_notification","society_id='$society_id' AND read_status=0 AND admin_id =0 OR society_id='$society_id' AND read_status=0 AND admin_id ='$bms_admin_id'","ORDER BY notification_id DESC LIMIT 5");
         while ($adminNotification=mysqli_fetch_array($aq)) {
            ?>
          </li>
          <li class="list-group-item">
          <a href="readNotification.php?link=<?php echo $adminNotification['admin_click_action']; ?>&id=<?php echo $adminNotification['notification_id']; ?>&action=<?php echo $adminNotification['notification_action']; ?>">
           <div class="media">
              <?php
              if(file_exists("../img/noticeBoard/".$adminNotification['notification_logo'])) {  ?>
              <img width="32" class="mr-3" src="../img/noticeBoard/<?php echo $adminNotification['notification_logo']; ?>" alt="">
              <?php } else if(file_exists("../img/app_icon/".$adminNotification['notification_logo'])) {  ?>
              <img width="32" class="mr-3" src="../img/app_icon/<?php echo $adminNotification['notification_logo']; ?>" alt="">
              <?php } else { ?>
              <i class="fa fa-bell fa-2x mr-3 text-primary"></i>
              <?php } ?>
              <div class="media-body">
                  <h6 class="mt-0 msg-title"><?php echo $adminNotification['notification_tittle']; ?></h6>
                  <p class="msg-info"><?php echo custom_echo($adminNotification['notification_description'],30); ?></p>
              </div>
          </div>
          </a>
          </li>
          <?php } ?>
          <li class="list-group-item"><a href="adminNotification"><?php echo $xml->string->see; ?> <?php echo $xml->string->all; ?> <?php echo $xml->string->notifications; ?></a></li>
        </ul>
      </div>
    </li>
    
    <li class="nav-item">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
        <span class="user-profile ">
        

          <img onerror="this.src='img/user.png'"  src="<?php echo $profilePath; ?>" class="img-circle" alt="user avatar">  <i class="fa fa-angle-down "></i></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-right">
       <li class="dropdown-item user-details">
        <a href="javaScript:void();">
           <div class="media">
             <div class="avatar"><img onerror="this.src='img/user.png'"  class="align-self-start mr-3" src="<?php echo $profilePath; ?>" alt="user avatar"></div>
            <div class="media-body">
            <h6 class="mt-2 user-title"><?php echo $adminData['admin_name']; ?></h6>
            <p class="user-subtitle"><?php echo $adminData['role_name'] ?></p>
            </div>
           </div>
          </a>
        </li>
        <li class="dropdown-divider"></li>
       
          <a href="profile"><li class="dropdown-item"><i class="icon-wallet mr-2"></i> <?php echo $xml->string->my_profile; ?></li></a>
        <li class="dropdown-divider"></li>
          <a href="reminderList"><li class="dropdown-item"><i class="fa fa-calendar-check-o"></i> <?php echo $xml->string->my_reminders; ?></li></a>
        <li class="dropdown-divider d-md-none "></li>
          <a class="d-md-none " href="sound.php"><li class="dropdown-item"><i class="fa fa-bell"></i> <?php echo $xml->string->my_notification_setting; ?></li></a>
        <li class="dropdown-divider"></li>
        <form method="POST" action="logout.php">
          <button class="form-btn dropdown-item p-0"><li class="dropdown-item"><i class="icon-power mr-2"></i> <?php echo $xml->string->logout; ?></li></button>
        </form>
      </ul>
    </li>
    
  </ul>
</nav>
</header>
<!--End topbar header-->

<div class="clearfix"></div>
	