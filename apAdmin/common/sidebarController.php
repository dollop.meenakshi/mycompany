<?php 

include '../common/objectController.php';
extract(array_map("test_input", $_POST));

if (isset($_COOKIE['bms_admin_id'])) {
  $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
  $society_id = $_COOKIE['society_id'];
}

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);


if(!isset($_COOKIE["menuUpdatedDate_$bms_admin_id"]) || !file_exists("../img/menu_file_$bms_admin_id.json"))
{



  $q=$d->select("master_menu","page_status='0' AND status=0","ORDER BY order_no ASC");
  $menuArray = array();
  while ($data=mysqli_fetch_array($q)) {
      
    $menu_id=$data['menu_id'];   
    if($data['parent_menu_id']==0 && $data['sub_menu']==0) {
      if(in_array($menu_id, $accessMenuIdArr)){ 

        $menu = array(
          "menu_id" => $data['menu_id'],
          "menu_link" => $data['menu_link'],
          "menu_icon" => $data['menu_icon'],
          "menu_name" => $data['menu_name'],
          "language_key_name" => $data['language_key_name'],
          "subMenu"   => ""
        ); 
        array_push($menuArray, $menu);
      }
         
    }else  if($data['parent_menu_id']==0 && $data['sub_menu']==1){
      $subMenuArray = array();
      // $subMenuArray = array();
      $qs=$d->select("master_menu","parent_menu_id='$menu_id' ORDER BY order_no ASC");
      $subMenuContent="";
      while ($temData=mysqli_fetch_array($qs)) {
        if(in_array($temData['menu_id'], $accessMenuIdArr) || in_array($temData['menu_id'], $pagePrivilegeArr)){
          $submenu = array(
            "menu_id" => $temData["menu_id"],
            "menu_link" => $temData["menu_link"],
            "menu_name" => $temData["menu_name"],
            "menu_icon" => $temData["menu_icon"],
            "language_key_name" => $temData["language_key_name"],
          );
            array_push($subMenuArray,$submenu);
        } 
      }
      if(!empty($subMenuArray))
      {  

        $menu = array(
          "menu_id" => $data['menu_id'],
          "menu_link" => $data['menu_link'],
          "menu_icon" => $data['menu_icon'],
          "menu_name" => $data['menu_name'],
          "language_key_name" => $data['language_key_name'],
          "subMenu"   => $subMenuArray
        );

        array_push($menuArray, $menu);

      }
    }
  } 
  
  setcookie("menuUpdatedDate_$bms_admin_id","menu_file_$bms_admin_id.json",(time() + (43200)), "/$cookieUrl"); // 43200 = 12 hour
  
  file_put_contents("../img/menu_file_$bms_admin_id.json", json_encode($menuArray));

  $sidebareData = file_get_contents("../img/menu_file_$bms_admin_id.json");
  $sidebareData = json_decode($sidebareData,true);
}else{
  
  $sidebareData = file_get_contents("../img/menu_file_$bms_admin_id.json");
  $sidebareData = json_decode($sidebareData,true);
}







?>