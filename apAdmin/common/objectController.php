<?php 
session_start();
$url=$_SESSION['url'] ; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once '../lib/dao.php';
include_once '../lib/sms_api.php';
include '../lib/model.php';
include_once '../fcm_file/admin_fcm.php';
include_once '../fcm_file/gaurd_fcm.php';
include_once '../fcm_file/resident_fcm.php';
include_once '../fcm_file/resident_fcm_topic.php';
$d = new dao();
$m = new model();
$smsObj = new sms_api();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nResidentTopic = new firebase_resident_topic();
$nGaurd = new firebase_gaurd();
$con=$d->dbCon();
$created_by=$_COOKIE['admin_name'];
$updated_by=$_COOKIE['admin_name'];
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

$language_id = $_COOKIE['language_id'];
$xml=simplexml_load_file("../../img/$language_id.xml");
$societyLngName=  $xml->string->society;
// extract($_POST);
$base_url=$m->base_url();
$urlArya = explode("/", $base_url);
$cookieUrl = $urlArya[3];

if(isset($_REQUEST) && !empty($_REQUEST) && $_REQUEST["csrf"] != $_SESSION["token"] || $_REQUEST["csrf"]==''){
	$_SESSION['msg1']="Token Mismatch";
	header('location:'.$url);
	exit();
 }
$sq=$d->selectRow("society_name,currency,society_address,city_name,society_pincode,secretary_email,socieaty_logo,company_website","society_master","society_id='$society_id'");
$societyData=mysqli_fetch_array($sq);
$society_name=$societyData['society_name'];
$secretary_email= $societyData['secretary_email'];
$socieaty_logo= $societyData['socieaty_logo'];
$society_address_invoice= $societyData['society_address'].', '.$societyData['city_name'].' ('.$societyData['society_pincode'].')';
$currency = $societyData['currency'];
$company_website = $societyData['company_website'];
$received_by = "Auto";
?>