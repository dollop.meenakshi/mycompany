 <?php include 'sidebarController.php'; ?>
 <ul class="sidebar-menu do-nicescrol" >
     <?php
      if ($adminData['role_id']!=1){ ?>
      <li class="sidebar-header text-white"><b><?php echo $_COOKIE['society_name']; ?> 
      <br>
        Plan Expire :  <span class="badge badge-pill badge-danger m-1"> <?php echo  date("d/m/Y", strtotime($sData['plan_expire_date']));?> </span>
      <?php if(isset($adminData['device']) ) { ?>
        <a class="d-md-none btn-sm btn btn-warning  text-white" href="addMore.php">Add More/ Change Company</a>
      <?php } ?>
      </b>

      </li>
      <?php }
        

        foreach ($sidebareData as $data) {
          $menu_id=$data['menu_id']; 
          $language_key_name= $data['language_key_name'];
          $lngMenuName = $xml->string->$language_key_name;
            
          if($data['subMenu']=="") {
              if(in_array($menu_id, $accessMenuIdArr)){  ?>
                <li class="<?php echo ($_GET['f'] == $data['menu_link']) ? 'active':''; ?>">
                    <a href="<?php echo $data['menu_link']; ?>" class="waves-effect ">
                      <i class="<?php echo $data['menu_icon']; ?>"></i> <span><?php  if($lngMenuName!="") { echo  $lngMenuName; } else { echo $data['menu_name']; } ?></span> 
                    </a>
                </li>
        <?php }
               
          }else  if($data['subMenu']!=""){
               $parentActive = "";
               foreach ($data['subMenu'] as $temData) {
                 $temData['menu_link'].',';
                  if($temData['menu_link']==$_GET['f']){
                    $parentActive = "active";
                  } 
                } 

            ?>
            <li class="<?php echo $parentActive; ?>">
              <a href="javaScript:void();" class="waves-effect">
                <i class="<?php echo $data['menu_icon']; ?>"></i>
                <span><?php if($lngMenuName!="") { echo  $lngMenuName; } else { echo $data['menu_name'];} ?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="sidebar-submenu" >
                <?php 
                foreach ($data['subMenu'] as $temData) {
                  $menu_id1=$temData['menu_id'];
                  $language_key_name= $temData['language_key_name'];
                  $lngSubMenuName = $xml->string->$language_key_name;
                  if(in_array($temData['menu_id'], $accessMenuIdArr)){
                    $active = "";
                    if($_GET['f']==$temData['menu_link']) { $active="active"; }
                    if($lngSubMenuName!="") { $menu_name=$lngSubMenuName; } else { $menu_name= $temData['menu_name']; } 
                    ?>
                    <li class="<?php echo $active; ?>"><a href="<?php echo $temData['menu_link']; ?>"><i class="fa fa-arrow-right "></i><?php echo $menu_name;?></a>
                          </li>
                   <?php    
                  } 
                } ?>
              </ul>
            </li>
        <?php 
          }
        } 
        
        ?>
     
         <li class="<?= ($_GET['f'] == 'contactSupport') ? 'active':''; ?>">
          <a href="contactSupport" class="waves-effect ">
            <i class="fa fa-comments"></i> <span>Contact Support Team</span> 
          </a>
          
        </li>
     
      <li class="sidebar-header no-border"></li>
      <li class="sidebar-header no-border"></li>
      <li class="sidebar-header no-border"></li>
      
    </ul>