<?php 
$society_id = $_COOKIE['society_id'];
if (isset($_COOKIE['language_id'])) {
	$language_id = $_COOKIE['language_id'];
} else {
	$language_id = $_REQUEST['language_id'];
}
if (file_exists("../img/$language_id.xml") && $language_id!="") {
	$xml=simplexml_load_file("../img/$language_id.xml");
	// echo $xml->string->app_name;
} else {
	$q=$d->select("society_master","society_id='$society_id'");
	$bData=mysqli_fetch_array($q);
	$country_id  = $bData['country_id'];
    $society_id  = $bData['society_id'];
    setcookie('country_id', $country_id, time() + (86400 * 1 ), "/$cookieUrl"); // 86400 = 1 day
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/language_controller_web.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
	          "getLanguageValues=getLanguageValues&country_id=$country_id&society_id=$society_id");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'key: bmsapikey'
	));

	$server_output = curl_exec($ch);

	curl_close ($ch);
	$server_output=json_decode($server_output,true);

	$arrayCount= count($server_output['language_key']);
	$language_id = $server_output['language_id'];

	$myFile = "../img/$language_id.xml";

	$fh = fopen($myFile, 'w') or die("can't open file");
	$rss_txt = "";
	$rss_txt .= '<?xml version="1.0" encoding="utf-8"?>';
	$rss_txt .= "<rss version='2.0'>";
	    
	        $rss_txt .= '<string>';
	    for ($i1=0; $i1 < $arrayCount ; $i1++) { 
	    	$key_value = str_replace('&', '&amp;', $server_output['language_key'][$i1]['key_value']);

	    	$keyName  = $server_output['language_key'][$i1]['key_name'];
	    	

	        $rss_txt .= "<$keyName>$key_value</$keyName>";
	    }
	        $rss_txt .= '</string>';
	$rss_txt .= '</rss>';

	fwrite($fh, $rss_txt);
	fclose($fh);

	setcookie("lngUpdated", "lngUpdated", time() + (86400 * 5), "/$cookieUrl"); // 86400 = 5 day
}
