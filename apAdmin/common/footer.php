<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>

<input type="hidden" id="access_branchs_id" value="<?php echo $access_branchs;?>">
<?php if (isset($totalReminder) && $totalReminder>0) { ?>
<a title="Reminder" href="reminderList" class="reminderIcon"><i class="fa fa-calendar-check-o"></i> <span class="badge badge-info badge-up"><?php echo $totalReminder;?></span></a>
<?php } ?>
<!--End Back To Top Button-->
<div class="modal fade" id="notification">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->send; ?> <?php echo $xml->string->notifications; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="notificationValidation" action="controller/notificationController.php" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->add_comment_title; ?> <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input required="" maxlength="200" id="title" type="text" name="title"  class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->description; ?> <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <textarea maxlength="500" required=""  name="description"  class="form-control"></textarea>
            </div>
          </div>

          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->photo; ?>  </label>
            <div class="col-sm-8">
              <input type="file" accept="image/*" maxlength="500" name="notiUrl"  class="form-control-file border"></textarea>
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->send; ?>  <?php echo $xml->string->to; ?>  </label>
            <div class="col-sm-8">
              <select name="send_to" onchange="changeStatusValue(this.value)"  class="form-control single-select">
                <option value="All">All</option>
                <option value="0"><?php echo $xml->string->members; ?></option>
                <option value="1"><?php echo $xml->string->committee; ?></option>
                <option value="2"><?php echo $xml->string->gate_keeper; ?></option>
                
              </select>
            </div>
          </div>
          
          <div class="hideData">
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Branch  </label>
              <div class="col-sm-8">
                <select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required>
                <option value="">---Select Branch--</option> 
                  <option value="All">All</option> 
                  <?php 
                  $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                  while ($blockData=mysqli_fetch_array($qb)) {
                  ?>
                  <option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                  <?php } ?>
  
                </select> 
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Department  </label>
              <div class="col-sm-8">
                <select name="dId" class="form-control single-select floor_id" onchange="getUserByFloorId(this.value);" required>
                  <option value="">---Select Department--</option> 
                  <option value="All">All</option> 
                  <?php 
                    $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$bId' $blockAppendQuery");  
                    while ($depaData=mysqli_fetch_array($qd)) {
                  ?>
                  <option  <?php if($dId==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
                  <?php } ?>
                  
                </select>
              </div>
            </div>
          </div>
         
          <div class="form-footer text-center">
            <input type="hidden" name="blockAppendQueryOnly" value="<?php echo $blockAppendQueryOnly; ?>">
            <button type="submit" name="sendNoti" value="sendNoti" class="btn  btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->send; ?></button>
          </div>

        </form> 
      </div>
      
    </div>
  </div>
</div>

</div>
<script>
  var csrf ="<?php echo $_SESSION["token"]; ?>";
</script>
<!-- Bootstrap core JavaScript-->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<!-- simplebar js -->
<script src="assets/plugins/simplebar/js/simplebar.js"></script>
<!-- waves effect js -->
<script src="assets/js/waves.js"></script>
<!-- sidebar-menu js -->
<script src="assets/js/sidebar-menu.js"></script>
<!-- Custom scripts -->
<script src="assets/js/app-script.js"></script>

<!--Data Tables js-->
<script src="assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
<?php if ($hideDatatable!="true") {
if($adminData['report_download_access']==0) {
 ?>
 <script type="text/javascript">
  var exampleReportWithoutBtn = $('#exampleReportWithoutBtn').attr('id');
    if (typeof exampleReportWithoutBtn != "undefined") {
     $('body').bind('copy paste',function(e) {
        e.preventDefault(); return false; 
    });
   }
 </script>
<?php } ?>
<script src="assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>
<?php } ?>
<!--notification js -->
<script src="assets/plugins/notifications/js/lobibox.min.js"></script>
<script src="assets/plugins/notifications/js/notifications.min.js"></script>
<script src="assets/plugins/notifications/js/notification-custom-script.js"></script>


<!--Sweet Alerts -->
<script src="assets/plugins/alerts-boxes/js/sweetalert.min.js"></script>
<script src="assets/plugins/alerts-boxes/js/sweet-alert-script.js"></script>

<!-- Chart js -->
<script src="assets/plugins/Chart.js/Chart.min.js"></script>
<!--Peity Chart -->
<script src="assets/plugins/peity/jquery.peity.min.js"></script>
<!-- Index js -->
<!-- <script src="assets/js/index.js"></script> -->
<!--Lightbox-->
<script src="assets/plugins/fancybox/js/jquery.fancybox.min.js"></script>

<script src="assets/js/custom16.js"></script>

<!-- /////////////Dollop Infotech(October 21 2021)  //////////////  -->
<script src="assets/js/custom17.js"></script>

<script src="assets/plugins/summernote/dist/summernote-bs4.min.js"></script>
 
 <?php include 'common/alert.php'; ?>

<!--Form Validatin Script-->
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/validate7.js"></script>

<!--Select Plugins Js-->
<script src="assets/plugins/select2/js/select2.min.js"></script>
<!--Inputtags Js-->
<script src="assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>

<!--Bootstrap Datepicker Js-->
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- pg-calendar-master -->
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.full.js'></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.full.min.js'></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.js'></script>
<script src='assets/plugins/pg-calendar-master/dist/js/pignose.calendar.min.js'></script>

  <!-- full cALANDER -->
  <script src='assets/plugins/fullcalendar/js/moment.min.js'></script>
  <script src='assets/plugins/fullcalendar/js/fullcalendar.min.js'></script>
  <script src="assets/plugins/fullcalendar/js/fullcalendar-custom-script.js"></script>
  
    <!--Multi Select Js-->
    <script src="assets/plugins/jquery-multi-select/jquery.multi-select.js"></script>
    <script src="assets/plugins/jquery-multi-select/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="assets/js/select.js"></script>

    <!--material date picker js-->
    <script src="assets/plugins/material-datepicker/js/moment.min.js"></script>
    <script src="assets/plugins/material-datepicker/js/bootstrap-material-datetimepicker.min.js"></script>
    <script src="assets/plugins/material-datepicker/js/ja.js"></script>
    <link rel="stylesheet" href="assets/css/jquery.timepicker.min.css">
    <script src="assets/js/jquery.timepicker.min.js"></script>

    <script src="assets/js/datepicker.js"></script>
    <script src="assets/js/purchase.js"></script>


    <!-- 20220728 -->
    <script type="text/javascript" src="assets/js/moment-new.min.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.min.js"></script>
    <!-- 20220728 -->
    
  <script type="text/javascript" src="assets/js/lazyload.js"></script>
  <script type="text/javascript">
    function changeStatusValue(value){
      if(value==0){
        $('.hideData').show();
      }
      else
      {
        $('.hideData').hide();
      }
  }
    window.addEventListener("load", function(event) { lazyload(); });
  </script>

   <?php  if (!isset($_COOKIE['lngUpdated'])) { ?>
       <script type="text/javascript">
         updateLanguage();
       </script>
  <?php } ?>

   <!--Switchery Js-->
   <script src="assets/plugins/switchery/js/switchery.min.js"></script>
   <script>
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
      new Switchery($(this)[0], $(this).data());
    });
  </script>
<script type="text/javascript">
   $('form').append('<input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />');

   notificationCount();
  function notificationCount(){
    $.ajax({
        url: "controller/getNotificationCount.php",
        cache: false,
        type: "POST",
        data: {notificationCount:"yes"},
        success: function(response){
          
            $("#notificationCount").html(response);
        }
      });
  }
 </script>

<script type="">
  $(window).load(function() { 
        $("#spinner").fadeOut("slow"); 
    });
</script>
 <?php if ($hideDatatable=="true") { ?>
   <script src="assets/plugins/apexcharts-bundle/js/apexcharts.min.js"></script>
  <script type="text/javascript">

    $(function() {
      "use strict";


      var options = {
          series: [<?php echo (int)$totalPresentEmployee; ?>,<?php echo (int)$totalAbsentEmployee; ?>,<?php echo (int)$totalLateEmployee; ?>],
          colors: ['#2dce89', '#F41127', '#1A73E8'],
          chart: {
            height: 285,
            type: 'pie',
            ids:['present','absent','on_leave'],
            events: {
              dataPointSelection: function(event, chartContext, opts) {
                //window.location.href='chartAttendance'  
                var key = opts.w.config.chart.ids[opts.dataPointIndex];
                if(key == 'on_leave'){
                  window.location.replace("leaves?key="+key);
                }else{
                  window.location.replace("chartAttendance?key="+key);
                }               
              }
            }
          },
        labels: ['Present (<?php echo (int)$totalPresentEmployee; ?>)', 'Absent (<?php echo (int)$totalAbsentEmployee; ?>)', 'On Leave (<?php echo (int)$totalLateEmployee; ?>)'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 320
            },
            legend: {
              position: 'bottom'
            },
          }
        }]
        };

      var chart = new ApexCharts(document.querySelector("#unitStatus"), options);
      chart.render();

      var options = {
          series: [<?php echo (int)$totalPendingAttendance; ?>,<?php echo (int)$totalLetInEmployee; ?>,<?php echo (int)$totalEarlyOutEmployee; ?>],
          colors: ['#2dce89', '#1A73E8', '#F41127'],
          chart: {
            height: 285,
            type: 'pie',
            ids:['pending','late_in','early_out'],
            events: {
              dataPointSelection: function(event, chartContext, opts) {
                //window.location.href='chartAttendance'  
                var key = opts.w.config.chart.ids[opts.dataPointIndex];
                if(key == 'pending'){
                  window.location.replace("pendingAttendance?key="+key);
                }else{
                  window.location.replace("chartAttendance?key="+key);
                }               
              }
            }
          },
        labels: ['Pending (<?php echo (int)$totalPendingAttendance; ?>)', 'Late In (<?php echo (int)$totalLetInEmployee; ?>)', 'Early Out (<?php echo (int)$totalEarlyOutEmployee; ?>)'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 320
            },
            legend: {
              position: 'bottom'
            },
          }
        }]
        };

      var chart = new ApexCharts(document.querySelector("#userLateInAndPendingAttendance"), options);
      chart.render();

     
        var options = {
          series: [<?php echo (int)$totalAndroid; ?>, <?php echo (int)$totalIos; ?>, <?php echo (int)$NotLogin; ?>],
          labels: ['Android (<?php echo (int)$totalAndroid; ?>)', 'iOS (<?php echo (int)$totalIos; ?>)', 'Not Logged In (<?php echo (int)$NotLogin; ?>)'],
          chart: {
            foreColor: '#9ba7b2',
            height: 285,
            type: 'donut',
          },
          colors: ["#0d6efd", "#17a00e", "#f41127"],
          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                height: 320
              },
              legend: {
                position: 'bottom'
              }
            }
          }]
        };
        var chart = new ApexCharts(document.querySelector("#appUsageCOunter"), options);
        chart.render();

    });


  </script>


<?php } ?>
</body>

</html>
<style>
 .hideData{
   display: none;
 }
</style>