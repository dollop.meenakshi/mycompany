<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-5">
                <h4 class="page-title">Salary Raised Issues</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Salary Month</th>
                                        <th>Salary Issue</th>
                                        <th>Reject Reason</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    $q=$d->selectRow("srim.*,um.user_full_name,ssm.salary_month_name","salary_raised_issue_master AS srim JOIN salary_slip_master AS ssm ON ssm.salary_slip_id = srim.salary_slip_id JOIN users_master AS um ON um.user_id = srim.user_id");
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $user_full_name; ?></td>
                                        <td><?php echo $salary_month_name; ?></td>
                                        <td><?php echo $salary_issue; ?></td>
                                        <td><?php echo $reject_reason; ?></td>
                                        <td>
                                            <?php
                                            if($status == 0)
                                            {
                                            ?>
                                                <button type="button" title="Reject Issue" data-toggle="modal" data-target="#changeSalaryIssueStatusModal" class="btn btn-danger btn-sm" onclick="changeSalaryStatus(<?php echo $salary_issue_id ?>)"><i class="fa fa-times"></i></button>
                                                <button type="button" title="Approve Issue" class="btn btn-info btn-sm" onclick="approveSalaryIssue(<?php echo $salary_issue_id ?>)"><i class="fa fa-check"></i></button>
                                            <?php
                                            }
                                            elseif($status == 1)
                                            {
                                            ?>
                                                <span class="text-success">Solved</span>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <span class="text-danger">Rejected</span>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="changeSalaryIssueStatusModal">
        <div class="modal-dialog">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Change Salary Issue Status</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="salaryIssueStatusChangeForm" method="POST" action="controller/SalaryController.php" enctype="multipart/form-data">
                        <div class="row form-group">
                            <label class="col-sm-3 form-control-label">Reject Reason <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="5" maxlength="500" autocomplete="off" required name="reject_reason" id="reject_reason"></textarea>
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" name="salary_issue_id" id="salary_issue_id">
                            <input type="hidden" name="csrf" id="csrf">
                            <input type="hidden" name="changeSalaryIssueStatus" value="changeSalaryIssueStatus">
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>