<?php 
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
extract(array_map("test_input", $_POST));
if (isset($editDimensionalPerformance)) {
    $q = $d->selectRow('performance_dimensional_assign.*',"performance_dimensional_assign","performance_dimensional_assign.performance_dimensional_assign_id='$performance_dimensional_assign_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
    if ($block_ids!="") {
        $blockIds = explode(',', $block_ids);
    }
    if ($floor_ids!="") {
        $floorIds = explode(',', $floor_ids);
    }
    if ($level_ids!="") {
        $levelIds = explode(',', $level_ids);
    }
    if ($users_ids!="") {
        $usersIds = explode(',', $users_ids);
    }
}

$week_days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Assign Dimensional</h4>
     </div>
     </div>
    <!-- End Breadcrumb-->

    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="assignDimensionalFrom" action="controller/AssignDimensionalController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Dimensional <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select name="dimensional_id" class="form-control single-select" required  >
                                        <option value="">-- Select Dimensional --</option> 
                                        <?php 
                                        $dq=$d->select("dimensional_master","society_id='$society_id'");  
                                        while ($dimensionalData=mysqli_fetch_array($dq)) {
                                        ?>
                                        <option  <?php if(isset($dimensional_id) && $dimensional_id==$dimensionalData['dimensional_id']) { echo 'selected';} ?> value="<?php echo  $dimensionalData['dimensional_id'];?>" ><?php echo $dimensionalData['dimensional_name'];?></option>
                                        <?php } ?>

                                    </select>                
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select
                                    pmsAccessForBranchMultiSelectCls" multiple name="block_ids[]" id="block_ids" onchange="getDepartmentForPMS();">
                                        <option <?php if(isset($editDimensionalPerformance) && $block_ids == 0){echo 'selected';} ?> value="0">All Branch</option>
                                        <?php
                                        $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                                        while ($blockData=mysqli_fetch_array($bq)) {
                                        ?>
                                        <option <?php if(isset($blockIds) && in_array($blockData['block_id'],$blockIds)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" id="floor_ids" class="form-control multiple-select pmsAccessForDepartmentMultiSelectCls" multiple name="floor_ids[]">
                                        <option <?php if(isset($editDimensionalPerformance) && $floor_ids == 0){echo 'selected';} ?> value="0">All Department</option>
                                        <?php 
                                            $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') $blockAppendQuery");  
                                            while ($depaData=mysqli_fetch_array($qd)) {
                                        ?>
                                        <option <?php if(in_array($depaData['floor_id'],$floorIds)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
                                        <?php } ?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Level <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select
                                    pmsAccessForLevelMultiSelectCls" multiple name="level_ids[]" id="level_ids" onchange="getEmployeeByLeaveForPMS();">
                                        
                                        <?php
                                        $lq=$d->select("employee_level_master","society_id='$society_id' AND level_status = '0'");
                                        while ($leaveData=mysqli_fetch_array($lq)) {
                                        ?>
                                        <option <?php if(isset($levelIds) && in_array($leaveData['level_id'],$levelIds)){ echo "selected"; } ?> value="<?php echo $leaveData['level_id'];?>"> <?php echo $leaveData['level_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Employee <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" id="users_ids" class="form-control multiple-select pmsAccessForDepartmentMultiSelectCls" multiple name="users_ids[]">
                                        <option <?php if(isset($editDimensionalPerformance) && $users_ids == 0){echo 'selected';} ?> value="0">All Employee</option>

                                        <?php 
                                        if ($levelIds!="") {
                                            $level_ids = implode("','", $levelIds);
                                        }
                                        if($level_ids != '' && $level_ids != 0){
                                            $qd=$d->select("users_master","society_id='$society_id' AND users_master.level_id IN ('$level_ids') $blockAppendQueryUser");  
                                            while ($empData=mysqli_fetch_array($qd)) {
                                        ?>
                                        <option <?php if(isset($usersIds) && in_array($empData['user_id'],$usersIds)){ echo "selected"; } ?> value="<?php echo  $empData['user_id'];?>" ><?php echo $empData['user_full_name'];?> (<?php echo $empData['user_designation'];?>)</option>
                                        <?php } }?> 

                                            
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Type <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" id="pms_type" onchange="pmsType(this.value);" class="form-control single-select" name="pms_type">
                                        <option value="">-- Select Type --</option>
                                        <option <?php if(isset($editDimensionalPerformance) && $pms_type == 0){ echo "selected"; } ?> value="0">Weekly</option>
                                        <option <?php if(isset($editDimensionalPerformance) && $pms_type == 1){ echo "selected"; } ?> value="1">Monthly</option>
                                        <option <?php if(isset($editDimensionalPerformance) && $pms_type == 2){ echo "selected"; } ?> value="2">Quarterly</option>
                                        <option <?php if(isset($editDimensionalPerformance) && $pms_type == 3){ echo "selected"; } ?> value="3">Half Yearly</option>
                                        <option <?php if(isset($editDimensionalPerformance) && $pms_type == 4){ echo "selected"; } ?> value="4">Yearly</option>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4 common-hide <?php if(isset($editDimensionalPerformance) && $pms_type == '0'){}else{echo 'd-none';} ?> weekly-days">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Week Day <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select id="week_day" type="text" required="" class="form-control single-select" name="week_day">
                                        <option value="">-- Select Week Day --</option>
                                        <?php
                                        $week_day = explode(',', $data['days']);
                                        for ($i = 0; $i < count($week_days); $i++) {
                                        ?>
                                            <option <?php if (isset($data['day']) && ($i == $day)) {
                                                        echo "selected";
                                                    } ?> value="<?php echo $i; ?>"><?php echo $week_days[$i]; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 common-hide <?php if(isset($editDimensionalPerformance) && $pms_type != '0'){}else{echo 'd-none';} ?> month-days">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Month Day <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select id="month_day" type="text" required="" class="form-control single-select" name="month_day">
                                        <option value="">-- Select Month Day --</option>
                                        <?php
                                        $month_days = explode(',', $data['days']);
                                        for ($i = 1; $i <= 31; $i++) {
                                        ?>
                                            <option <?php if (isset($data['day']) && ($i == $day)) {
                                                        echo "selected";
                                                    }?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 common-hide start-month <?php if(isset($editDimensionalPerformance) && $pms_type != '0'&& $pms_type != '1'){}else{echo 'd-none';} ?>">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Start Month <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select id="start_month" type="text" required="" class="form-control single-select" name="start_month">
                                        <option value="">-- Select Start Month --</option>
                                        <option <?php if($data['start_month']=='01'){echo 'selected';} ?> value="01">January</option>
                                        <option <?php if($data['start_month']=='02'){echo 'selected';} ?> value="02">February</option>
                                        <option <?php if($data['start_month']=='03'){echo 'selected';} ?> value="03">March</option>
                                        <option <?php if($data['start_month']=='04'){echo 'selected';} ?> value="04">April</option>
                                        <option <?php if($data['start_month']=='05'){echo 'selected';} ?> value="05">May</option>
                                        <option <?php if($data['start_month']=='06'){echo 'selected';} ?> value="06">June</option>
                                        <option <?php if($data['start_month']=='07'){echo 'selected';} ?> value="07">July</option>
                                        <option <?php if($data['start_month']=='08'){echo 'selected';} ?> value="08">August</option>
                                        <option <?php if($data['start_month']=='09'){echo 'selected';} ?> value="09">September</option>
                                        <option <?php if($data['start_month']=='10'){echo 'selected';} ?> value="10">October</option>
                                        <option <?php if($data['start_month']=='11'){echo 'selected';} ?> value="11">November</option>
                                        <option <?php if($data['start_month']=='12'){echo 'selected';} ?> value="12">December</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>    
                    
                    <div class="form-footer text-center">              
                        <?php if (isset($editDimensionalPerformance)) { ?>      
                        <button id="addCanteenProductVariantBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                        <input type="hidden" name="assignDimensional"  value="assignDimensional">
                        <?php }else{ ?>
                        <button id="addCanteenProductVariantBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                        <input type="hidden" name="assignDimensional"  value="assignDimensional">
                        <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    function pmsType(value){
        $('.common-hide').addClass('d-none');
        if(value == 0){
            $('.common-hide').addClass('d-none');
            $('.weekly-days').removeClass('d-none');
        }else if(value == 1){
            $('.common-hide').addClass('d-none');
            $('.month-days').removeClass('d-none');
        }else if(value == 2){
            $('.common-hide').addClass('d-none');
            $('.month-days').removeClass('d-none');
            $('.start-month').removeClass('d-none');
        }else if(value == 3){
            $('.common-hide').addClass('d-none');
            $('.month-days').removeClass('d-none');
            $('.start-month').removeClass('d-none');
        }else if(value == 4){
            $('.common-hide').addClass('d-none');
            $('.start-month').removeClass('d-none');
            $('.month-days').removeClass('d-none');
        }
    }
</script>
  