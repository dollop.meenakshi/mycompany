<?php 
  error_reporting(0);
  extract($_REQUEST);
  $complaint_category= (int)$complaint_category;

  $complaint_category_id=$adminData['complaint_category_id'];
  $compCtgAry = explode(",",$complaint_category_id);
  $ids = join("','",$compCtgAry); 
  $compCtgAryFinal = array_filter($compCtgAry);
  $compCtgAryFinal = array_values($compCtgAryFinal);

$qry2 ="";
$qry3 ="";

  if($complaint_category !="" && $complaint_category !="0" && $complaint_category !="-1" ){
      $qry2 ="AND complains_master.complaint_category='$complaint_category' ";
  }

   if(!empty($ids)){
      $qry3 ="AND complains_master.complaint_category IN ('$ids') ";
  }

if (isset($_GET['blockunit']) && $_GET['blockunit']=='Yes') {  ?>

  <div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-6 col-12">
        <h4 class="page-title">Block <?php echo $xml->string->units; ?> for Add New Complaint From App</h4>
        
      </div>
      
      <div class="col-sm-3 col-4 text-right">
        <div class="btn-group">
          <a  data-toggle="modal" data-target="#addBlockComplaint" class="btn btn-primary text-white float-right btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Block New <?php echo $xml->string->unit; ?> </a>  
          <a href="javascript:void(0)" onclick="DeleteAll('deleteBlockCompalaine');" class="btn  btn-sm btn-danger float-right"><i class="fa fa-trash-o fa-lg"></i>  </a>
        </div>
      </div>
    </div>
    <div class="row pt-2 pb-2">
      
     
    </div>
     <div class="row">
      <div class="col-lg-6">
        <i>Receive New Complaint Copy on Email <a href="complaintEmailReceipt" class="fa fa-btn btn-sm btn-warning mb-1">Set Email</a></i>
      </div>
      <div class="col-lg-6 text-right">
         <a title="Complaints" href="complaints" class="btn btn-sm btn-danger mb-1">Complaints</a>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th><?php echo $xml->string->unit; ?></th>
                    <th>Block Date</th>
                    <th>Blocked By</th>
                    <th>Block Message</th>      
                  </tr>
                </thead>

                <tbody>
                  <?php 
                    $queryAry=array();
                    $unitALreadyBlock=array();
                    $sr=1;
                    $q=$d->select("complains_block_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND  complains_block_master.unit_id=unit_master.unit_id AND complains_block_master.society_id='$society_id'   $blockAppendQueryUnit","ORDER BY complains_block_master.complains_block_id DESC");
                    while ($data=mysqli_fetch_array($q)) {
                     array_push($unitALreadyBlock, $data['unit_id']);
                     $bq=$d->select("users_master,block_master","block_master.block_id=users_master.block_id AND  users_master.unit_id='$data[unit_id]' ");
                    $unitData= mysqli_fetch_array($bq);
                    $unitName = $unitData['user_full_name'].'-'.$unitData['user_designation'];

                     ?>
                    <tr>
                       <td class="text-center">
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['unit_id']; ?>">
                        </td>
                        
                       
                         <td><?php echo $unitName; ?> </button>
                        </td>
                        <td><?php echo $data['block_date']; ?></td>
                        <td><?php
                          $qu=$d->select("bms_admin_master","admin_id='$data[block_by]'");
                          $userData=mysqli_fetch_array($qu);
                          echo custom_echo($userData['admin_name'],14);
                        //else echo "-";
                         ?></td>
                        
                        <td class="tableWidth"><?php echo custom_echo($data['block_message'],50); ?></td>

                    </tr>
                  <?php }  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } else {
 
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-6 col-12">
        <h4 class="page-title">Complaints</h4>
      </div>
      <div class="col-sm-3 col-8">
        <form method="get" action="">
          <select class="form-control complain-select" name="complaint_category" onchange="this.form.submit()">
            <option value="-1">All Category</option>
            <?php
              $complain = $d->select("complaint_category","active_status=0");
              if(empty($compCtgAryFinal)){
              while($complainData = mysqli_fetch_array($complain)){ ?>
                <option <?php if(isset($complaint_category) && $complaint_category==$complainData['complaint_category_id']){ echo "selected";} ?> value="<?php echo $complainData['complaint_category_id'] ?>"><?php echo html_entity_decode($complainData['category_name']) ?></option>
              <?php  } } else{
              while($complainData = mysqli_fetch_array($complain)){ 
                if(in_array($complainData['complaint_category_id'], $compCtgAryFinal)){?>
                <option <?php if(isset($complaint_category) && $complaint_category==$complainData['complaint_category_id']){ echo "selected";} ?> value="<?php echo $complainData['complaint_category_id'] ?>"><?php echo html_entity_decode($complainData['category_name']) ?></option>
            <?php  } } } ?>
            
          </select>
        </form>
      </div>
      <div class="col-sm-3 col-4 text-right">
        <div class="btn-group">
          <a  data-toggle="modal" data-target="#addComplaint" class="btn btn-primary text-white float-right btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>  
          <a href="javascript:void(0)" onclick="DeleteAll('deleteCompalaine');" class="btn  btn-sm btn-danger float-right"><i class="fa fa-trash-o fa-lg"></i>  </a>
        </div>
      </div>
    </div>
    <div class="row pt-2 pb-2">
      <div class="col-sm-8 col-8">
      <ol class="breadcrumb">
          <li>
            <?php if (($adminData['admin_type'] ==1 || $adminData['complaint_category_id']=='') && !isset($complaint_category)) { ?>
              <a href ="complaints?complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-primary m-1">All (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.flag_delete!=2 $blockAppendQueryUnit"); ?>)</span></a>
              <a href ="complaints?type=1&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-success m-1">Close (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=1 and complains_master.flag_delete=0 $blockAppendQueryUnit"); ?>)</span></a>
              <a href ="complaints?type=2&complaint_category=<?php echo $complaint_category; ?>" ><span class="badge badge-pill badge-info m-1">Re Open  (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=2 and complains_master.flag_delete=0 $blockAppendQueryUnit"); ?>)</span></a>
           
              <a href ="complaints?type=0&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-warning m-1">Open (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=0 and complains_master.flag_delete=0 $blockAppendQueryUnit"); ?>)</span></a>
              <a href ="complaints?type=3&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-primary m-1">In Progress (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=3 and complains_master.flag_delete=0 $blockAppendQueryUnit"); ?>)</span></a>
              <a href ="complaints?flag_delete=1&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-danger m-1">Deleted (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.flag_delete=1 $blockAppendQueryUnit"); ?>)</span></a>
            <?php } else if (isset($complaint_category)) {
               if(!empty($ids) && $qry3!='') {
                      $append_temp ="AND complains_master.complaint_category IN ('$ids') ";
                    }
             ?>
              <a href ="complaints?complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-primary m-1">All (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND   complains_master.flag_delete!=2 $qry2 $blockAppendQueryUnit  $append_temp"); ?>)</span></a>
              <a href ="complaints?type=1&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-success m-1">Close (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=1 and complains_master.flag_delete=0  $qry2 $blockAppendQueryUnit  $append_temp"); ?>)</span></a>
              <a href ="complaints?type=2&complaint_category=<?php echo $complaint_category; ?>" ><span class="badge badge-pill badge-info m-1">Re Open  (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=2 and complains_master.flag_delete=0  $qry2 $blockAppendQueryUnit  $append_temp"); ?>)</span></a>
              <a href ="complaints?type=0&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-warning m-1">Open (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=0 and complains_master.flag_delete=0 $qry2 $blockAppendQueryUnit  $append_temp"); ?>)</span></a>
              <a href ="complaints?type=3&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-primary m-1">In Progress (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=3 and complains_master.flag_delete=0 $qry2 $blockAppendQueryUnit  $append_temp"); ?>)</span></a>
              <a href ="complaints?flag_delete=1&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-danger m-1">Deleted (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.flag_delete=1  $qry2 $blockAppendQueryUnit  $append_temp"); ?>)</span></a>
            <?php } else{ ?>
              <a href ="complaints?complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-primary m-1">All (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id'  AND complains_master.flag_delete!=2 $qry3 $blockAppendQueryUnit"); ?>)</span></a>
              <a href ="complaints?type=1&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-success m-1">Close (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=1 and complains_master.flag_delete=0 $qry3 $blockAppendQueryUnit"); ?>)</span></a>
              <a href ="complaints?type=2&complaint_category=<?php echo $complaint_category; ?>" ><span class="badge badge-pill badge-info m-1">Re Open  (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=2 and complains_master.flag_delete=0  $qry3 $blockAppendQueryUnit"); ?>)</span></a>
              <a href ="complaints?type=0&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-warning m-1">Open (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=0 and complains_master.flag_delete=0  $qry3 $blockAppendQueryUnit"); ?>)</span></a>
              <a href ="complaints?type=3&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-primary m-1">In Progress (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complain_status=3 and complains_master.flag_delete=0 $qry3 $blockAppendQueryUnit"); ?>)</span></a>
              <a href ="complaints?flag_delete=1&complaint_category=<?php echo $complaint_category; ?>"><span class="badge badge-pill badge-danger m-1">Deleted (<?php echo $d->count_data_direct("complain_id","complains_master,unit_master","unit_master.unit_id=complains_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.flag_delete=1  $qry3  $blockAppendQueryUnit"); ?>)</span></a>
            <?php } ?>
          </li>
            
        </ol>
      </div>
      <div class="col-sm-4 col-4">
        Last Checked Time: <i class="lastcheckedtime"> <?php echo date("h:i A");?> </i>
      </div>
    </div>
     <div class="row">
      <div class="col-lg-6">
        <i>Receive New Complaint Copy on Email <a href="complaintEmailReceipt" class="fa fa-btn btn-sm btn-warning mb-1">Set Email</a></i>
      </div>
      <div class="col-lg-6 text-right">
         <a title="Restrict Block for add new Complaint" href="complaints?blockunit=Yes" class="btn btn-sm btn-danger mb-1">Block <?php echo $xml->string->unit; ?></a>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th></th>
                    <th>Status</th>
                    <th>No.</th>
                    <th><?php echo $xml->string->unit; ?></th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Date</th>      
                    <th>Category</th> 
                    <?php //IS_1266 ?>
                    <th>Ratings</th>      
                  </tr>
                </thead>

                <tbody>
                  <?php 
                    $queryAry=array();
                    $type = $_GET['type'];
                    if ($type!='') {
                      $type = (int)$type;
                      $append_query="complain_status='$type' AND flag_delete=0";
                      array_push($queryAry, $append_query);
                    } 
                   
                    if ($_GET['flag_delete'] == 1 && $_GET['flag_delete'] != "" ) {
                      $append_query1 =" flag_delete=1";
                      array_push($queryAry, $append_query1);

                    }
                    if (isset($complaint_category) && $qry2!='') {
                      $append_query13="complaint_category='$complaint_category' ";
                      array_push($queryAry, $append_query13);

                    }
                    if(!empty($ids) && $qry3!='') {
                      $append_query2 ="complaint_category IN ('$ids') ";
                      array_push($queryAry, $append_query2);

                    }
       
                   $appendQuery=  implode(" AND ", $queryAry);
                    if ($appendQuery!="") {
                      $appendQueryNew = " AND $appendQuery";
                    }

                    $sr=1;
                          $q=$d->select("complains_master,unit_master,floors_master,users_master","complains_master.user_id=users_master.user_id AND floors_master.floor_id=users_master.floor_id AND  complains_master.unit_id=unit_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.flag_delete!=2 $appendQueryNew  $blockAppendQueryUnit","ORDER BY complains_master.complain_id DESC");
                    $currentTotalCompalaint = mysqli_num_rows($q);  
                    while ($data=mysqli_fetch_array($q)) {
                      // check attachment 
                        $qc=$d->select("complains_track_master","complain_id='$data[complain_id]'");
                        $atData= mysqli_fetch_array($qc);
                        $complains_track_img = $atData['complains_track_img'];
                        

                     ?>
                    <tr>
                       <td class="text-center">
                        <?php if ($data['flag_delete']==1): ?>
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['complain_id']; ?>">
                        <?php endif ?>
                        </td>
                        <td>
                          <a onclick="return popitup('complaintReceipt.php?id=<?php echo $data['complain_id'] ?>&societyid=<?php echo $data['society_id'] ?>')" href="#" class="badge badge-success"><i class="fa fa-print"></i> </a>
                        </td>
                        <td>
                          <?php if ($data['flag_delete']==0) {
                            if ($data['complain_status']==0) {  ?>
                              <span   data-toggle="modal" data-target="#vieComp" onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-warning m-1 pointerCursor">OPEN <i class="fa fa-pencil"></i></span>
                            <?php } elseif ($data['complain_status']==1) { ?>
                              <span   data-toggle="modal" data-target="#vieComp" onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-success  m-1 pointerCursor">CLOSE</span>
                            <?php  } elseif ($data['complain_status']==2) { ?>
                              <span  data-toggle="modal" data-target="#vieComp" onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-info  m-1 pointerCursor">REOPEN <i class="fa fa-pencil"></i></span>
                            <?php } elseif ($data['complain_status']==3) { ?>
                              <span  data-toggle="modal" data-target="#vieComp" onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-primary  m-1 pointerCursor">In Progress <i class="fa fa-pencil"></i></span>
                            <?php } 
                          } else { ?> 
                              <span  data-toggle="modal" data-target="#vieComp" onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-danger  m-1 pointerCursor">Deleted <i class="fa fa-pencil"></i></span><?php 
                          }?>
                        </td>
                         <td>
                          <form action="complaintHistory" method="get">
                            <input type="hidden" name="id" value="<?php echo $data['complain_id']; ?>">
                            <button class="btn btn-link p-0">CN<?php echo $data['complain_id']; ?> <i class="fa fa-history"></i></button>
                          </form>
                        </td>
                        <td><?php 
                            echo $data['user_full_name'].' ('. $data['floor_name'].')';
                        //else echo "-";
                         ?></td>
                        <td class="tableWidth">
                          <?php if ($data['complaint_new_status']==0) { ?>
                          <span class="badge badge-dark">New</span>
                          <?php } if ($complains_track_img!="") { ?>
                             <a  target="_blank" href="../img/complain/<?php echo $complains_track_img; ?>"><i class="fa fa-paperclip"></i> </a>
                        <?php } echo $data['compalain_title']; ?> </td>
                        <td class="tableWidth"><?php echo custom_echo($data['complain_description'],50); ?></td>
                        <td><?php echo $data['complain_date']; ?></td>
                        <td><?php
                        
                        if($data['complaint_category'] ==""){
                          echo "-";
                        } else { 
                            $q11=$d->selectRow("category_name","complaint_category","complaint_category_id='$data[complaint_category]'");
                            $catData=mysqli_fetch_array($q11);
                            if(!empty($catData)){
                              echo $catData['category_name'];
                            } else {
                              echo "-";
                            }
                            
                            } ?></td>

                        <td> <a href="#" <?php if($data['feedback_msg']!=''){ ?> data-toggle="modal" data-target="#feedbackDetails" onclick="displayMsg('<?php echo $data['feedback_msg']; ?>')" <?php } ?>>

                         <?php 
                         $outputString="";

                         $userRating =   $data['rating_star'] ;
                          $outputString .= '
                                <div class="row-item"> <div class="list-inline" > ';
                            if($userRating !=""){
                            for ($count = 1; $count <= 5; $count ++) {
                                $starRatingId = $data['complain_id'] . '_' . $count;
                               
                                if ($count <= $userRating) {
                                    
                                    $outputString .= '<span style="color:#ff6e00; value="' . $count . '" id="' . $starRatingId . '" class="star selected">&#9733;</span>';
                                } else {
                                    $outputString .= '<span value="' . $count . '"  id="' . $starRatingId . '" class="star"  >&#9733;</span>';
                                    
                                }
                            }  
                            $outputString .= '
                         </div>
                         
                          
                        </div>
                         ';
                         echo $outputString;
                         }
                         ?></a>
                        </td>
                        
                    </tr>
                  <?php }  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php } ?>
<script type="text/javascript">
  function vieComplain(complain_id) {
      $.ajax({
          url: "getComplaineDetails.php",
          cache: false,
          type: "POST",
          data: {complain_id : complain_id},
          success: function(response){
              $('#comResp').html(response);
            
              
          }
       });
  } 

  function displayMsg(argument) {
    $('#feedbackMsg').html(argument);
  }

   function vieComplainPopUp(complain_id) {
      $.ajax({
          url: "getComplaineDetails.php",
          cache: false,
          type: "POST",
          data: {complain_id : complain_id, onlyInfo:"yes"},
          success: function(response){
              $('#comRespInfo').html(response);
            
              
          }
       });
  } 

setInterval(function(){
  checkNewCompalint();
}, 60000 * 5);

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

   function checkNewCompalint() {
      $.ajax({
          url: "getNewCompalaintCount.php",
          cache: false,
          type: "POST",
          data: {appendQuery : "<?php echo $appendQueryNew;?>", currentTotalCompalaint:"<?php echo $currentTotalCompalaint;?>"},
          success: function(response){
           // console.log(formatAMPM(new Date));
            $('.lastcheckedtime').text(formatAMPM(new Date));
            if(response!='0') {
              console.log(response);
              swal({
                title: response,
                text: "",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancel", "View Complaint"],
              })
              .then((willDelete) => {
                if (willDelete) {
                   history.go(0);
                } else {
                  // swal("Your data is safe!");
                }
              });
            }
              
          }
       });
  } 

</script>

<div class="modal fade" id="vieCompInfo">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Complaint Info </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="comRespInfo">
          
      </div>
     
    </div>
  </div>
</div>


<div class="modal fade" id="vieComp">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-success">
        <h5 class="modal-title text-white">View Complaint </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="comResp">
          
      </div>
     
    </div>
  </div>
</div>
<?php if (isset($_GET['blockunit']) && $_GET['blockunit']=='Yes') {  ?>

<div class="modal fade" id="addBlockComplaint">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Complaint Block For <?php echo $xml->string->unit; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="complaintBlockValidation" method="POST" action="controller/compalainController.php" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="input-10" class="col-sm-3 col-form-label"><?php echo $xml->string->unit; ?> <span class="required">*</span></label>
              <div class="col-sm-9">
               <select type="text" required="" class="form-control single-select" name="unit_id">
                  <option value="">-- Select --</option>
                  <?php 
                  $ids = join("','",$unitALreadyBlock); 
                    $q13=$d->select("unit_master,block_master,users_master,floors_master","unit_master.unit_id NOT IN ('$ids') AND  floors_master.floor_id=users_master.floor_id AND users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.member_status=0 $blockAppendQuery","ORDER BY unit_master.unit_id ASC");
                     while ($blockRow11=mysqli_fetch_array($q13)) {
                   ?>
                    <option value="<?php echo $blockRow11['unit_id'];?>"><?php echo $blockRow11['user_full_name'];?>-<?php echo $blockRow11['floor_name'];?>  </option>
                    <?php }?>
                  </select>
              </div>
            </div>
            
            
            <div class="row form-group">
              <label class="col-sm-3 form-control-label">Block Message  <span class="required">*</span></label>
              <div class="col-sm-9">
                <textarea rows="7" type="text" required="" maxlength="500" class="form-control" name="block_message"></textarea>
              </div>
            </div>
            
            <div class="form-footer text-center">
              <input type="hidden" name="blockUnit">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Submit</button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>

<?php } ?>

<div class="modal fade" id="addComplaint">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Complaint</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="complaintValidation" method="POST" action="controller/compalainController.php" enctype="multipart/form-data">
          <div class="row form-group">
              <label for="input-10" class="col-sm-3 col-form-label">Branch <span class="required">*</span></label>
            <div class="col-sm-9">
              <select name="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required>
                <option value="">-- Select Branch --</option> 
                <?php 
                $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                while ($blockData=mysqli_fetch_array($qb)) {
                ?>
                <option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                <?php } ?>

              </select>         
            </div>
          </div>

          <div class="row form-group">
          <label for="input-10" class="col-sm-3 col-form-label">Department <span class="required">*</span></label>
            <div class="col-sm-9">
              <select name="floor_id" id="dId" class="form-control single-select" onchange="getUserByFloorId(this.value);" required>
                <option value="">-- Select Department --</option> 
                <?php 
                  $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$bId' $blockAppendQuery");  
                  while ($depaData=mysqli_fetch_array($qd)) {
                ?>
                <option  <?php if($dId==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
                <?php } ?>
                
                </select>
            </div>
          </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-3 col-form-label"><?php echo $xml->string->member; ?> <span class="required">*</span></label>
              <div class="col-sm-9">
               <select type="text" required="" class="form-control single-select" id="user_id" name="user_id">
                  <option value="">-- Select--</option>
                  <!-- <?php 
                   /*  $q3=$d->select("unit_master,block_master,users_master,floors_master","floors_master.floor_id=users_master.floor_id AND users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.member_status=0 $blockAppendQuery","ORDER BY unit_master.unit_id ASC");
                     while ($blockRow=mysqli_fetch_array($q3)) { */
                   ?>
                    <option value="<?php //echo $blockRow['user_id'];?>"><?php //echo $blockRow['user_full_name'];?> (<?php //echo $blockRow['floor_name'];?>) </option>
                    <?php // }?> -->
                  </select>
              </div>
            </div>
            <div class="row form-group">
              <label class="col-sm-3 form-control-label">Category <span class="text-danger">*</span></label>
              <div class="col-sm-9">
                <select class="form-control single-select" name="complaint_category">
                  <option value="">--SELECT--</option>
                  <?php
                   if(!empty($ids) && $qry3!='') {
                      $append_temp ="AND complaint_category_id IN ('$ids') ";
                    }
                    $complain = $d->select("complaint_category","active_status=0  $append_temp");
                    while($complainData = mysqli_fetch_array($complain)){ ?>
                      <option value="<?php echo $complainData['complaint_category_id'] ?>"><?php echo html_entity_decode($complainData['category_name']); ?></option>
                  <?php  }
                  ?>
                </select>
              </div>
            </div>
            <div class="row form-group">
              <label class="col-sm-3 form-control-label">Complaint <span class="text-danger">*</span></label>
              <div class="col-sm-9">
                <input maxlength="100" type="text" class="form-control txtNumeric" required="" name="compalain_title">
              </div>
            </div>
            
            <div class="row form-group">
              <label class="col-sm-3 form-control-label">Description</label>
              <div class="col-sm-9">
                <textarea type="text" maxlength="500" class="form-control" name="complain_description"></textarea>
              </div>
            </div>
            <div class="row form-group">
              <label class="col-sm-3 form-control-label">File </label>
              <div class="col-sm-9">
                <!-- <input type="file" class="form-control-file border photoOnly" accept="image/*" name="complain_photo"> -->
                <label for="soceityCover">
                      <!--  <img id="societyCoverView" style="border: 1px solid gray; width: 100%; border-radius: 15px; height: 200px;" src="../img/imagePlaceholder.jpg"  src="#" alt="your image" class='' /> -->
                      <img id="societyCoverView"  src="../img/imagePlaceholder.jpg" alt="Photo" style="border: 1px solid gray; border-radius: 15px; height: 100px;" >
                     <input accept="image/*,video/*" class="photoInput " id="soceityCover" type="file" name="complain_photo">
                     <br>
                     Complaint File 
                   </label>
              </div>
            </div>
            <div class="form-footer text-center">
              <input type="hidden" name="addComplaint">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Submit</button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>

<div class="modal fade" id="feedbackDetails">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Feedback Message</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="feedbackMsg">
          
      </div>
     
    </div>
  </div>
</div>


<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=800, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>