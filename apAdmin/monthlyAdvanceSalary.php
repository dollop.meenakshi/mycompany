<?php

error_reporting(0);
$lsId = (int) $_REQUEST['lsId'];
$cpId = (int) $_REQUEST['cpId'];
$bId = (int) $_REQUEST['bId'];
$dId = (int) $_REQUEST['dId'];
$laYear =  $_REQUEST['laYear'];
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$month_year = (int) $_REQUEST['month'];
?>
?>
<div class="content-wrapper">
    <div class="container-fluid">
       
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title">Monthly Advance Salary</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
                <div class="btn-group float-sm-right">
                </div>
            </div>
        </div>
        <form action="" class="branchDeptFilterWithUser">
            <div class="row pt-2 pb-2">
                <?php  include('selectBranchDeptForFilter.php'); ?> 
                <div class="col-md-3 form-group col-6" >
            <select class="form-control single-select"  name="month" id="month">
              <option value="">-- Select --</option> 
              <?php
              $selected = "";
              for ($m = 1; $m <= 12; $m++) {
                $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
                if (isset($_GET['month'])  && $_GET['month'] !="") {
                  if($_GET['month'] == $m)
                  {
                    $selected = "selected";
                  }else{
                    $selected = "";
                  }
                  
                } else {
                  $selected = "";
                  if($m==date('n'))
                  { 
                    $selected = "selected";
                  }
                  else
                  {
                    $selected = "";
                  }
                }
              ?>
              <option  <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $month; ?></option> 
            <?php }?>
            </select> 
            
          </div>
          <div class="col-md-2 form-group col-6" >
              <select name="laYear" class="form-control single-select ">
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $twoPreviousYear) {
                          echo 'selected';
                        } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $onePreviousYear) {
                          echo 'selected';
                        } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $currentYear) {
                          echo 'selected';
                        }else{ echo ''; } ?> <?php if (!isset($_GET['laYear'])) {
                                echo 'selected';
                              } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $nextYear) {
                          echo 'selected';
                        } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
              </select>
          </div>
        <div class="col-md-2 form-group mt-auto">
            <input class="btn btn-success btn-sm " type="submit" name="getReport"  value="Get Data">
        </div>
            </div>

        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">

                    <div class="card-body">
                        <div class="table-responsive">
                            <?php 
                            $i = 1;
                            if($dId>0){
                                $dptFilter = " AND users_master.floor_id='$dId'";
                            }
                            if($bId>0){
                                $blkFilter = " AND users_master.block_id='$bId'";
                            }
                            if($month_year>0){
                                $month_year = "01"."-".$month_year."-".$laYear;
                                $mYr = date('Y-m',strtotime($month_year));
                                $mnthFilter = " AND monthly_advance_salary.month='$mYr'";
                            }
                            $q = $d->selectRow('monthly_advance_salary.*,bms_admin_master.admin_name,users_master.*,floors_master.floor_id,floors_master.floor_name,block_master.block_id,block_master.block_name','monthly_advance_salary LEFT JOIN bms_admin_master ON bms_admin_master.admin_id = monthly_advance_salary.paid_by ,users_master LEFT JOIN floors_master ON floors_master.floor_id=users_master.floor_id LEFT JOIN block_master ON block_master.block_id=users_master.block_id ',"users_master.user_id=monthly_advance_salary.user_id $blockAppendQuery $dptFilter $blkFilter $mnthFilter");
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>Action</th>
                                        <th>Employee Name</th>
                                        <th>Department</th>
                                        <th>Month</th>
                                        <th> Date</th>
                                        <th>Amount</th>
                                        <th>Paid Amount</th>
                                        <th>Remaining Amount</th>
                                        <th>Paid By</th>
                                        <th>Status</th>
                                       
                                       <!--  <th>Reason</th> -->
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    while ($data=mysqli_fetch_assoc($q)) {
                                       ?>
                                       <tr>
                                           <td>
                                               <?php /* $totalAttendance = $d->count_data_direct("attendance_break_history_id", "attendance_break_history_master", "attendance_type_id='$data[attendance_type_id]'");
                                                    if ($totalAttendance == 0)  {*/ ?>
                                                    <!-- <input type="hidden" name="id" id="id"  value="<?php echo $data['advance_salary_id']; ?>">
                                                    <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['advance_salary_id']; ?>"> -->
                                                    <?php //} ?>
                                                    </td>
                                                    <td><?php echo $i++; ?></td>
                                                    <td>
                                                    <div class="d-flex align-items-center">
                                                        <?php if(isset($data['advance_salary_created_by']) && $data['advance_salary_created_by'] ==$_COOKIE['bms_admin_id']){ ?>
                                                     <button type="button" class="btn btn-sm btn-primary ml-2" onclick="editAdvaceSalary(<?php echo $data['advance_salary_id']; ?>)"> <i class="fa fa-pencil"></i></button>
                                                     <?php } ?>
                                                     <button onclick="getAdvaceSalaryDetail( <?php echo $data['advance_salary_id'];?>)" class="btn btn-primary btn-sm ml-1"><i class="fa fa-eye" ></i></button>
                                                    <!--  <input type="hidden" name="user_id_attendace" value="<?php if ($data['user_id'] != "") {echo $data['user_id'];}   ?>"> -->
                                                     <!-- <a href="attendanceCalendar?uId=<?php if ($data['user_id'] != "") { echo $data['user_id'];} ?>&bId=<?php if ($data['block_id'] != "") {echo $data['block_id'];} ?>&dId=<?php if ($data['floor_id'] != "") {echo $data['floor_id']; } ?>&month=<?php echo date('m'); ?>&laYear=<?php echo date('Y'); ?>">
                                                         <button type="button" class="btn btn-sm btn-primary ml-2">
                                                         <i class="fa fa-calendar"></i>
                                                         </button>
                                                     </a> -->
                                                     </div>
                                                    </td>
                                           <td><?php echo $data['user_full_name'] ?></td>
                                           <td><?php echo $data['floor_name']."(". $data['block_name'].")";?></td>
                                           <td><?php echo $data['month'];?></td>
                                           <td><?php echo date("d M Y",strtotime($data['monthly_advance_salary_date']));?></td>
                                           <td><?php echo $data['total_advance_amount'];?></td>
                                           <td><?php echo $data['total_paid_amount'];?></td>
                                           <td><?php echo $data['remaning_amount'];?></td>
                                           <td><?php echo $data['admin_name'];?></td>
                                           <td><?php if($data['is_complete_paid']==1){ echo "Paid"; }else{ echo "Unpaid"; } ?></td>
                                           
                                       </tr>
                                       <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

</div>
<div class="modal fade" id="addAdvanceSalaryModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Advance Salary</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-2 " style="align-content: center;">
                <div class="row">
                    <div class="card-body">
                        <form id="addAdvanceSalary" action="controller/AdvanceSalaryController.php" enctype="multipart/form-data" method="post">
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select id="block_id" name="block_id" class="form-control inputSl single-select" onchange="getFloorByBlockIdAddSalary(this.value)" required="">
                                        <option value="">Select Branch</option>
                                        <?php
                                        $qd = $d->select("block_master", "block_master.society_id='$society_id' AND block_master.block_status=0 $blockAppendQueryOnly ");
                                        while ($depaData = mysqli_fetch_array($qd)) {
                                        ?>
                                            <option <?php if ($data['block_id'] == $depaData['block_id']) { echo 'selected';} ?> <?php if ($block_id == $depaData['block_id']) {echo 'selected';} ?> value="<?php echo  $depaData['block_id']; ?>"><?php echo $depaData['block_name']; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-4 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select type="text" id="floor_id" required="" onchange="getUserByFloorId(this.value)" class="form-control single-select floor_id inputSl" name="floor_id">
                                        <option value="">-- Select --</option>
                                       <!--  <?php
                                        if (isset($sid) && $sid > 0) {
                                            $fblock_id = $data['block_id'];
                                        } else {
                                            $fblock_id = $block_id;
                                        }
                                        $floors = $d->select("floors_master,block_master", "block_master.block_id =floors_master.block_id AND  floors_master.society_id='$society_id' AND floors_master.block_id = '$fblock_id' $blockAppendQuery");
                                        while ($floorsData = mysqli_fetch_array($floors)) {
                                        ?>
                                            <option <?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] == $fid) {
                                                        echo "selected";
                                                    } ?> value="<?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] != "") {
                                                                    echo $floorsData['floor_id'];
                                                                } ?>"> <?php if (isset($floorsData['floor_name']) && $floorsData['floor_name'] != "") {
                                                                            echo $floorsData['floor_name'] . "(" . $floorsData['block_name'] . ")";
                                                                        } ?></option>
                                        <?php } ?> -->
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-4 col-form-label">Employee <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select name="user_id" id="user_id" class="inputSl form-control single-select">
                                        <option value="">-- Select Employee --</option>
                                        <?php
                                        $user = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND user_status !=0 AND floor_id = '$dId' $blockAppendQueryUser");
                                        while ($userdata = mysqli_fetch_array($user)) {
                                        ?>
                                            <option <?php if ($uId == $userdata['user_id']) {
                                                        echo 'selected';
                                                    } ?> value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Amount<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <input type="text" value="" name="advance_salary_amount" id="advance_salary_amount" class="form-control rstFrm onlyNumber rstFrm">
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Advance Salary Date<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <input type="text" value="" name="advance_salary_date" id="advance_salary_date" class="form-control rstFrm onlyNumber rstFrm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Mode<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select class="form-control inputSl" required name="advance_salary_mode" id="advance_salary_mode">
                                        <option value="">-- Select --</option>
                                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['advance_salary_mode'] == 0) {
                                                    echo "selected";
                                                } ?> value="0">Bank Transaction</option>
                                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['advance_salary_mode'] == 1) {
                                                    echo "selected";
                                                } ?> value="1">Cash</option>
                                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['advance_salary_mode'] == 2) {
                                                    echo "selected";
                                                } ?> value="2">Cheque</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="input-10" class="col-lg-4  col-md-4 col-form-label">Remark<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <textarea name="advance_salary_remark" id="advance_salary_remark" class="form-control rstFrm"></textarea>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <input type="hidden" id="advance_salary_id" name="advance_salary_id" value="" class="rstFrm">
                                <button type="submit" class="btn btn-success hideAdd course_lesson_button"><i class="fa fa-check-square-o"></i> </button>
                                <input type="hidden" name="addAdvanceSalary" value="addAdvanceSalary">
                                <button type="button" value="add" class="btn btn-danger " onclick="resetForm()"><i class="fa fa-check-square-o"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="advanceSalaryModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Advance Salary Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="advanceSalaryData">

        </div>
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css" />
<script src="assets/js/jquery.min.js"></script>


<script type="text/javascript">
   

    function resetForm() {
        $('.rstFrm').val('');
        $('.inputSl').val('');
        $('.inputSl').select2('');
    }

    function buttonSettingForLmsLesson() {
        $('.hideupdate').hide();
        $('.hideAdd').text('Add');
        $('.rstFrm').val('');
        $('.inputSl').val('');
        $('.inputSl').select2('');
    }

   


    function popitup(url) {
        newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    }
</script>