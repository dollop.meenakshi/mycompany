<?php 
error_reporting(0);
$facility_id = (int)$_GET['facility_id'] ;
if (isset($_GET['from']) && $_GET['facility_id']!='All') {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1 || filter_var($_GET['facility_id'], FILTER_VALIDATE_INT) != true){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='facilityBookingReport';
        </script>");
  }
} else if(isset($_GET['from']) && $_GET['facility_id']=='All'){
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='facilityBookingReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Facility Booking Report</h4>
       
        </div>
     
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
          <div class="col-lg-4 col-6">
            <label  class="form-control-label">Select Facility </label>
             <select required="" class="form-control" name="facility_id">
                <option value="All">All</option>
                  <?php  $q=$d->select("facilities_master","society_id='$society_id'","");
                        while ($mData=mysqli_fetch_array($q)) {?>
                          <option <?php if($mData['facility_id']==$facility_id) { echo 'selected';} ?> value="<?php echo $mData['facility_id'] ?>"><?php echo $mData['facility_name']; ?></option>
                  <?php } ?>
             </select>
          </div>
           
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
          <div class="col-lg-2">
          

          </div>

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                // extract(array_map("test_input" , $_GET));
               $from = date('Y-m-d', strtotime($_GET['from']));
                $toDate = date('Y-m-d', strtotime($_GET['toDate']));
                  if ($_GET['facility_id']=="All") {
                       $q=$d->select("facilitybooking_master,facilities_master,unit_master,block_master,users_master,floors_master","floors_master.floor_id=users_master.floor_id AND floors_master.block_id=block_master.block_id AND users_master.user_id=facilitybooking_master.user_id AND   block_master.block_id=unit_master.block_id AND unit_master.unit_id=facilitybooking_master.unit_id AND facilitybooking_master.facility_id =facilities_master.facility_id  AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.book_status=1 AND facilitybooking_master.booked_date BETWEEN '$from' AND '$toDate' ","ORDER BY unit_master.unit_id ASC");
                    } else {
                       $q=$d->select("facilitybooking_master,facilities_master,unit_master,block_master,users_master,floors_master","floors_master.floor_id=users_master.floor_id AND floors_master.block_id=block_master.block_id AND users_master.user_id=facilitybooking_master.user_id AND   block_master.block_id=unit_master.block_id AND unit_master.unit_id=facilitybooking_master.unit_id AND facilitybooking_master.facility_id =facilities_master.facility_id  AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.facility_id ='$facility_id' AND facilitybooking_master.book_status=1 AND facilitybooking_master.booked_date BETWEEN '$from' AND '$toDate' ","ORDER BY unit_master.unit_id ASC");
                  }

                  $i=1;
                if (isset($_GET['from'])) {
               ?>
              <div class="row">
                <?php if ($_GET['facility_id']=="All") { ?>
                <div class="col-md-3 pl-3">
                  Total Unpaid Amount:  <?php 
                      $count5=$d->sum_data("receive_amount","facilitybooking_master","payment_status='1' AND  facilitybooking_master.book_status=1  AND booked_date BETWEEN '$from' AND '$toDate'");
                      while($row=mysqli_fetch_array($count5))
                     {
                          $asif=$row['SUM(receive_amount)'];
                     echo   $totalMain=number_format($asif,2,'.','');
                             
                      }
                   ?>
                </div>
                <div class="col-md-3 pl-3">
                  Total Paid Amount: <?php 
                      $count56=$d->sum_data("receive_amount","facilitybooking_master","payment_status='0' AND facilitybooking_master.book_status=1  AND booked_date BETWEEN '$from' AND '$toDate'");
                      while($row=mysqli_fetch_array($count56))
                     {
                          $asif=$row['SUM(receive_amount)'];
                     echo   $totalMain=number_format($asif,2,'.','');
                             
                      }
                   ?>
                </div>
                <?php } else { ?>
                  <div class="col-md-3 pl-3">
                  Total Unpaid Amount:  <?php 
                      $count5=$d->sum_data("receive_amount","facilitybooking_master","payment_status='1' AND facility_id='$_GET[facility_id]'  AND booked_date BETWEEN '$from' AND '$toDate'");
                      while($row=mysqli_fetch_array($count5))
                     {
                          $asif=$row['SUM(receive_amount)'];
                     echo   $totalMain=number_format($asif,2,'.','');
                             
                      }
                   ?>
                </div>
                <div class="col-md-3 pl-3">
                  Total Paid Amount: <?php 
                      $count56=$d->sum_data("receive_amount","facilitybooking_master","payment_status='0' AND facilitybooking_master.book_status=1 AND  facility_id='$_GET[facility_id]'  AND booked_date BETWEEN '$from' AND '$toDate'");
                      while($row=mysqli_fetch_array($count56))
                     {
                          $asif=$row['SUM(receive_amount)'];
                     echo   $totalMain=number_format($asif,2,'.','');
                             
                      }
                   ?>
                </div>
                <?php } ?>
              </div>
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Facility</th>
                        <th><?php echo $xml->string->block; ?> <?php echo $xml->string->floor; ?></th>
                        <th>Employee</th>
                        <th>Amount</th>
                        <th>Transaction Charges</th>
                        <th>Person</th>
                        <th>Status</th>
                        <th>Booked Date</th>
                        <th>End Date</th>
                        <th>Invoice Number</th>
                        <th>Payment Date</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q)) {
                    // echo '<pre>';
                    // print_r($data);
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['facility_name']; ?></td>
                        <td><?php echo $data['block_name']; ?>-<?php echo $data['floor_name']; ?></td>
                        <td><?php echo $data['user_full_name']; ?> (<?php echo $data['user_designation']; ?>)</td>
                        <td><?php echo  number_format($data['receive_amount']-$data['transaction_charges'],2,'.',''); ?></td>
                        <td><?php echo $data['transaction_charges']; ?></td>
                        <td><?php echo $data['no_of_person']; ?></td>
                    <td>
                       <?php if ($data['payment_status']==1) {
                        echo "Unpaid";
                       } elseif ($data['payment_status']==0) {
                        echo "Paid";
                       } 
                       ?>
                         
                      </td>
                        <td><?php  echo $data['booked_date']; ?></td>
                        <td><?php  echo $data['booking_expire_date']; ?></td>
                        <td><?php  echo 'INVFAC'.$data['booking_id']; ?></td>
                        <td><?php  if($data['payment_received_date']!="") { echo date('Y-m-d h:i A', strtotime($data['payment_received_date'])); } ?></td>
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            <?php } else {  ?>
              <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select Date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->