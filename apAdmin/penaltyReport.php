<?php 
error_reporting(0);
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='penaltyReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Penalty  Report</h4>
        </div>
     
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
           <div class="col-lg-2 col-6">
            <label  class="form-control-label">Type </label>
            <select name="type"  class="form-control">
              <option <?php if($_GET['type']=='all') { echo 'selected';} ?> value="all"  >All</option>
              <option <?php if($_GET['type']=='unpaid') { echo 'selected';} ?>  value="unpaid" >Un Paid</option>
              <option <?php if($_GET['type']=='paid') { echo 'selected';} ?>  value="paid" >Paid</option>
            </select>
          </div>
           
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
          

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                extract(array_map("test_input" , $_GET));
                // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                 $date=date_create($from);
                $dateTo=date_create($toDate);
                $nFrom= date_format($date,"Y-m-d 00:00:00");
                $nTo= date_format($dateTo,"Y-m-d 23:59:59");
                       // $q=$d->select("event_master","society_id='$society_id' AND event_start_date BETWEEN '$nFrom' AND '$nTo'");
                  if ($_GET['type']=='unpaid') {
                    $q=$d->select("penalty_master,unit_master,users_master,block_master","block_master.block_id=unit_master.block_id AND penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id' AND  penalty_master.paid_status=0 AND penalty_master.penalty_datetime BETWEEN '$nFrom' AND '$nTo'","ORDER BY penalty_master.created_at DESC");
                  }elseif ($_GET['type']=='paid') {
                    $q=$d->select("penalty_master,unit_master,users_master,block_master","block_master.block_id=unit_master.block_id AND penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id'  AND penalty_master.paid_status=1 AND penalty_master.penalty_datetime BETWEEN '$nFrom' AND '$nTo'","ORDER BY penalty_master.created_at DESC");
                  } else {
                  $q=$d->select("penalty_master,unit_master,users_master,block_master","block_master.block_id=unit_master.block_id AND penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id' AND penalty_master.penalty_datetime BETWEEN '$nFrom' AND '$nTo'","ORDER BY penalty_master.created_at DESC");
                  }

                  $i=1;
                if (isset($_GET['from'])) {
               ?>
                
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Penalty</th>
                        <th>Penalty Date</th>
                        <th><?php echo $xml->string->unit; ?></th>
                        <th>Penalty Amount</th>
                        <th>Received Amount</th>
                        <th>Transaction Charge</th>
                        <th>Invoice Number</th>
                        <th>Status</th>
                       
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['penalty_name']; ?></td>
                        <td><?php  if($data['penalty_datetime']!="") { echo date('Y-m-d h:i A', strtotime($data['penalty_datetime'])); } ?></td>
                        <td>
                        <?php
                            if(!empty($data['user_full_name']))
                            {
                              echo $data['user_full_name'];
                            }
                            if(!empty($data['user_designation']))
                            {
                              echo ' ('.$data['user_designation'] . ")";    
                            }
                          ?>
                        </td>
                        <td><?php echo $data['penalty_amount']; ?></td>
                        <td><?php if($data['paid_status']==1) { echo $data['penalty_amount']; } ?></td>
                      
                        <td><?php if($data['paid_status']==1) { echo $data['transaction_charges']; } ?></td>
                        <td><?php echo 'INVPN'.$data['penalty_id']; ?></td>
                        <td><?php
                         if ($data['paid_status']==0) { 
                          echo "Unpaid";
                         } else {
                          echo "Paid";
                         }
                          ?></td>
                       
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            <?php } else {  ?>
              <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->