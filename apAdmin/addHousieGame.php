  <?php
error_reporting(0);
extract($_REQUEST);
if (isset($managePlayer) && $managePlayer=='yes') {
  if(filter_var($id, FILTER_VALIDATE_INT) != true){
   echo ("<script LANGUAGE='JavaScript'>
        window.location.href='manageHousie';
        </script>");
  }
$id =(int)$id;
$q = $d->select("housie_room_master","room_id = '$id' AND housie_type=1");
$data = mysqli_fetch_array($q);
extract($data);
 $gameTime =$data['game_date'].' '.$data['game_time'];
 $cTimeNow= date("Y-m-d H:i:s");
 $timeFirst  = strtotime($gameTime);
$timeSecond = strtotime($cTimeNow);
 $differenceInSeconds = $timeFirst - $timeSecond;
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Housie Game Participate Player (<?php echo $game_name; ?> - <?php echo date('d M Y h:i A',strtotime($game_date.' '.$game_time)); ?>)</h4>
        
      </div>
      <div class="col-sm-3">
        <?php if($differenceInSeconds>900){ 
          ?>
       <a  data-toggle="modal" data-target="#addHousiePlayer" class="btn btn-primary text-white float-right btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Player</a>  
     <?php } else {?>
        <a  onclick="swal('Add Player will be disabled before 15 minutes of game start !');"  class="btn btn-primary text-white float-right btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Player</a>  

     <?php } ?>
     </div>
    </div>
    <!-- End Breadcrumb-->

   <div class="row">
      <div class="col-lg-12">
       <div class="card profile-card-2">
       
        <div class="card-body">

          <div class="table-responsive">
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Unit</th>
                  <th>Action</th>

                </tr>
              </thead>
              <tbody>
                <?php
                 $unitALreadyBlock=array();
                $q=$d->select("users_master,housie_participate_player,block_master","users_master.block_id=block_master.block_id AND housie_participate_player.user_id=users_master.user_id AND housie_participate_player.room_id='$room_id'","");
                $i = 0;
                $cnt = 1;
                while($row=mysqli_fetch_array($q))
                { 
                    array_push($unitALreadyBlock, $row['user_id']);
                  $i++;

                  ?>
                  <tr>

                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['user_full_name']; ?> (<?php if ($row['user_type']==0) {
                                //IS_662
                                  if ($row['member_status']==0) {
                                  echo "Primary ";
                                 }
                                  echo "Owner";
                                 } elseif ($row['user_type']==1) {
                                  if ($row['member_status']==0) {
                                  echo "Primary ";
                                 }
                                  echo "Tenant";
                                 } ?>)</td>
                    <td><?php echo $row['block_name']; ?>-<?php echo $row['unit_name']; ?></td>
                    <td>
                      <?php if($differenceInSeconds>900){ ?>
                       <form action="controller/housieController.php" method="POST" >
                          <input type="hidden" name="room_id" value="<?php echo $row['room_id'];?>">
                          <input type="hidden" name="user_id" value="<?php echo $row['user_id'];?>">
                          <input type="hidden" value="deleteParticipatetPlayer" name="deleteParticipatetPlayer">
                         <button type="submit" class="form-btn btn btn-sm btn-danger" name="">Delete </button>
                        </form>
                      <?php } ?>
                    </td>

                  </tr>
                  <?php } ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
    </div>
</div>



</div>

</div>


<div class="modal fade" id="addHousiePlayer">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-myassociation">
      <div class="modal-header bg-myassociation">
        <h5 class="modal-title text-white">Add Player</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="complaintBlockValidation" method="POST" action="controller/housieController.php" enctype="multipart/form-data">
            <input type="hidden" name="room_id" value="<?php echo $room_id;?>">
            <div class="form-group row">
              <label for="input-10" class="col-sm-3 col-form-label">Unit <span class="required">*</span></label>
              <div class="col-sm-9">
               <select type="text" required="" class="form-control single-select" name="user_id">
                  <option value="">-- Select --</option>
                  <?php 
                   $ids = join("','",$unitALreadyBlock); 
                    $q3=$d->select("block_master,users_master","users_master.user_id NOT IN ('$ids') AND users_master.delete_status=0  AND users_master.society_id='$society_id' AND users_master.user_status==1 $blockAppendQuery","ORDER BY block_master.block_sort ASC");
                     while ($blockRow=mysqli_fetch_array($q3)) {
                   ?>
                    <option value="<?php echo $blockRow['user_id'];?>"><?php echo $blockRow['user_full_name'];?>  (<?php echo
                        $blockRow['company_name'];
                  ?> - <?php echo $blockRow['block_name'];?>)</option>
                    <?php }?>
                  </select>
              </div>
            </div>
            
            
           
            
            <div class="form-footer text-center">
              <input type="hidden" name="addParticipatePlayer" value="addParticipatePlayer">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Submit</button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>

<?php  } else { ?>

<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php if (isset($que) & $que>0) {
              
             ?>

             <button  data-toggle="modal" data-target="#editbillCategoryModal" onclick="importquestion('<?php echo $id;?>','<?php echo $que;?>');" class="btn btn-primary btn-sm pull-right" type=""><i class='fa fa-download'></i> Import From Question Bank</button>
             <form enctype="multipart/form-data" id="addHousieQuestion" action="controller/housieController.php" method="post">

              <input type="hidden" name="room_id" value="<?php echo $id;?>">
            
              <h4 class="form-header text-uppercase">
                <i class="fa fa-cube"></i>
               Add/Edit Housie Questions
              </h4>
               <?php
               if ($editQuestion=='true') {
                if (filter_var($id, FILTER_VALIDATE_INT) != true) {
                   echo ("<script LANGUAGE='JavaScript'>
                  window.location.href='manageHousie';
                </script>");
                  exit();
                }
                $id = (int)$id;
                 $qe=$d->select("housie_questions_master","room_id='$id' AND que_id='$que_id'","ORDER BY que_id DESC");
                   
                 $row=mysqli_fetch_array($qe);
                ?>
                  <input type="hidden" name="editQue" value="editQue">
                  <input type="hidden" name="que_id" value="<?php echo $que_id;?>">
                   <div class="form-group row">
                    <label for="input-<?php echo $a; ?>" class="col-sm-2 col-form-label">Question<span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <?php //IS_1260 alphanumeric ?>
                      <input required=""  type="text" class="form-control" id="input-" name="question_name" maxlength="100" value="<?php echo $row['question'];?>" >
                    </div>

                   
                    </div>
                 
                  <div class="form-group row">
                    <label for="input-14<?php echo $a; ?>" class="col-sm-2 col-form-label">Answer <span class="required">*</span></label>
                    <div class="col-sm-10">
                      <input required="" type="text"  value="<?php echo $row['answer'];?>"  maxlength="20"  class="form-control " id="input-14"  name="answer"  value="" >
                    </div>
                    
                  </div>
               <?php  } else if(isset($importFromServer)) {
                  $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL,"https://master.myassociation.app/commonApi/questionsController.php");
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS,
                              "getHouseQuestionList=getHouseQuestionList&housie_game_id=$game_id");

                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                  $server_output = curl_exec($ch);

                  curl_close ($ch);

                  $server_output=json_decode($server_output,true);
                   // $que= count($server_output['question']);

                   for ($i=0; $i<$que ; $i++) {
               $a= $i+1; ?>
                <input type="hidden" name="addQuest" value="addQuest">
                <div class="form-group row">
                  <label for="input-<?php echo $a; ?>" class="col-sm-2 col-form-label">Question <?php echo $a; ?><span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <?php //IS_1260 alphanumeric ?>
                    <input required=""  type="text" class="form-control" id="input-<?php echo $a; ?>" name="question_name[<?php echo $i;?>]" maxlength="100" value="<?php echo $server_output['question'][$i]['question'];  ?>" >
                  </div>

                 
                  </div>
               
                <div class="form-group row">
                  <label for="input-14<?php echo $a; ?>" class="col-sm-2 col-form-label">Answer <span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input required="" type="text"  maxlength="20"  class="form-control " id="input-14<?php echo $a; ?>"  name="answer[<?php echo $i;?>]"  value="<?php echo $server_output['question'][$i]['answer'];  ?>" >
                  </div>
                  
                </div>


                <?php } } else  {
                for ($i=0; $i<$que ; $i++) {
               $a= $i+1; ?>
                <input type="hidden" name="addQuest" value="addQuest">
              <div class="form-group row">
                <label for="input-<?php echo $a; ?>" class="col-sm-2 col-form-label">Question <?php echo $a; ?><span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <?php //IS_1260 alphanumeric ?>
                  <input required=""  type="text" class="form-control" id="input-<?php echo $a; ?>" name="question_name[<?php echo $i;?>]" maxlength="100" value="" >
                </div>

               
                </div>
             
              <div class="form-group row">
                <label for="input-14<?php echo $a; ?>" class="col-sm-2 col-form-label">Answer <span class="required">*</span></label>
                <div class="col-sm-10">
                  <input required="" type="text"  maxlength="20"  class="form-control " id="input-14<?php echo $a; ?>"  name="answer[<?php echo $i;?>]"  value="" >
                </div>
                
              </div>
            <?php }  } ?>

           
             
              <div class="form-footer text-center">
                 <?php   if($editQuestion=='true') { ?> 
                      <input type="hidden" name="editPoll" value="editPoll">
                     <button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> Update</button>
                  <?php } else {?>
                      
                <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-check-square-o"></i> Add</button>
              <?php } ?>
              </div>
            </form>
            <?php } else { 
              $qold=$d->select("housie_room_master","room_id='$room_id'");
              $row=mysqli_fetch_array($qold);
              extract($row);
              ?>
            <form enctype="multipart/form-data" id="addHousie" action="controller/housieController.php" method="post">

             
              <h4 class="form-header text-uppercase">
                <i class="fa fa-cube"></i>
               Add Housie Game
              </h4>
              <div class="form-group row">
                <label for="input-1033" class="col-sm-2 col-form-label">Game Name <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <?php //IS_1260 alphanumeric ?>
                  <input required=""  type="text" class="form-control" id="input-1033" name="game_name" maxlength="100" value="<?php if(isset($editGame) ){ echo $game_name;} ?>" >
                </div>

                <div class="col-sm-4">

                </div>
              </div>
             
              <div class="form-group row">
                <label for="input-14" class="col-sm-2 col-form-label">Game Start Date<span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text"  readonly="" class="form-control housie_start_date"  name="game_date"  value="<?php if(isset($editGame) && $rescheduleGame==''){ echo $game_date;} ?>" >
                </div>
                <label for="input-14" class="col-sm-2 col-form-label">Start Time<span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text" readonly="" class="form-control timepicker" id="poll-end-date"  name="game_time"  value="<?php if(isset($editGame) && $rescheduleGame==''){ echo date('h:i A',strtotime($game_time));} ?>" >
                </div>
              </div>

              <?php if (!isset($editGame)) { ?>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">No Of Question (Min 20) <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                  <select required="" class="form-control" id="que_id" name="que_id" >
                    <option value="">-- Select --</option>
                    <?php 
                    for ($i=20; $i <=60; $i++) { ?>
                      <option <?php if(isset($voting_id) && $no_of_option_selected==$i ){ echo "selected";} ?> value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?php }
                    ?>
                  </select>
                  <!--  <input required="" autocomplete="off" maxlength="2" onkeyup ="getOptionList();" type="text" id="no_of_option" class="form-control no_of_option" id="input-10" name="no_of_option"> -->
                </div>
                <label for="input-Interval" class="col-sm-2 col-form-label">Question/Answer Interval <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                  <input required="" min="5" max="20" type="text" class="form-control numberOnly" id="input-que_interval" name="que_interval" maxlength="3"  value="<?php if(isset($editGame) ){ echo $que_interval/2;} else { echo '15';} ?>" >
                </div>
              </div>
              
              <?php } else { ?>
                 <div class="form-group row">
                
                <label for="input-Interval" class="col-sm-2 col-form-label">Question/Answer Interval <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                  <input required="" min="5" max="20" type="text" class="form-control numberOnly" id="input-que_interval" name="que_interval" maxlength="3"  value="<?php if(isset($editGame) ){ echo $que_interval/2;} else { echo '15';} ?>" >
                </div>
              </div>
              <?php  } ?>
              <div class="form-group row">
                 <label for="input-housie_type" class="col-sm-2 col-form-label">Housie Type <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                  <select required="" class="form-control" id="housie_type" name="housie_type" >
                    <option value="">-- Select --</option>
                    <option <?php if(isset($editGame)  && $housie_type==0){ echo 'selected';}  ?> value="0">All <?php echo $xml->string->members;?></option>
                    <option <?php if(isset($editGame)  && $housie_type==1){ echo 'selected';} ?> value="1">Private Selected <?php echo $xml->string->members;?></option>
                  </select>
                </div>

              </div>

              <div class="form-group row">
                 <label for="input-12" class="col-sm-2 col-form-label">Stage Rules <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <div class="row">
                    <?php
                    $i=1;
                    $qqq = $d->select("housie_rules_master","");
                    while ($quizData=mysqli_fetch_array($qqq)) {
                      $rule_id=$quizData['master_rule_id'];
                      $abc=$d->select("housie_claim_rules_master","master_rule_id='$rule_id' AND room_id='$room_id'");
                      $oldRule=mysqli_fetch_array($abc);
                      $oldRuleId=$oldRule['master_rule_id'];
                     ?>
                      <label class="col-sm-4">
                        <input <?php if($oldRuleId==$rule_id) { echo 'checked'; } ?>  style="margin-top: 0px !important;"   value="<?php echo $quizData["master_rule_id"]; ?>" name="master_rule_id[]" type="checkbox">  <?php echo $quizData['rule_name'] ?>
                      </label>
                      
                      <label class=" col-sm-4">
                        <input maxlength="3" class="form-control onlyNumber" inputmode="numeric" type="number" value="<?php echo $oldRule['allow_users_claim'] ?>" min="1"  placeholder="No. of Winners" name="allow_users[]">
                      </label>
                      <label class=" col-sm-4">
                        <input maxlength="3" class="form-control onlyNumber" inputmode="numeric" value="<?php echo $oldRule['rule_points'] ?>" type="number" min="1" placeholder="Point per Winner" name="rule_points[]">
                      </label>
                      <?php } ?>
                    </div>
                </div>
              </div>   
              <div class="form-group row">
                <label for="input-14" class="col-sm-2 col-form-label">Sponser Logo</label>
                <div class="col-sm-4">
                  <input  type="file"  accept="image/*" readonly="" class="form-control-file border"  name="sponser_photo"   >
                  <input  type="hidden" name="sponser_photo_old"  value="<?php if(isset($editGame) && $rescheduleGame==''){ echo $sponser_photo;} ?>"   >
                </div>
                <label for="input-14" class="col-sm-2 col-form-label">Sponser Name</label>
                <div class="col-sm-4">
                  <input  type="text" maxlength="80"  class="form-control" name="sponser_name"  value="<?php if(isset($editGame) && $rescheduleGame==''){ echo $sponser_name;} ?>" >
                </div>
              </div>
              <div class="form-group row">
                <label for="input-14" class="col-sm-2 col-form-label">Sponser URL</label>
                <div class="col-sm-10">
                  <input type="url"  maxlength="150" class="form-control"  name="sponser_url"    value="<?php if(isset($editGame) && $rescheduleGame==''){ echo $sponser_url;} ?>" >
                </div>
                
              </div>

             
              <div class="form-footer text-center">
                 <?php   if(isset($editGame) && $rescheduleGame=='') { ?> 
                     <input type="hidden" name="editHousieGame" value="editHousieGame">
                     <input type="hidden" name="room_id" value="<?php echo $room_id;?>">
                     <input type="hidden" name="updateGame" value="updateGame">
                     <button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> Update</button>
                     <?php } else if(isset($editGame) && $rescheduleGame=='rescheduleGame') {
                       $q11=$d->select("housie_questions_master","room_id='$room_id'","ORDER BY rand()");
                      while($row=mysqli_fetch_array($q11))
                      {  ?>
                         <input required=''  type='hidden' name='question_name[]' maxlength='100' value="<?php echo $row['question'];?>">
                         <input required=''  type='hidden' name='answer[]' maxlength='100' value="<?php echo $row['answer'];?>">
                     <?php }
                      ?> 
                     <input type="hidden" name="rescheduleGame" value="rescheduleGame">
                     <input type="hidden" name="room_id" value="<?php echo $room_id;?>">
                     <button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> Reschedule
                  <?php } else {?>
                       <input type="hidden" name="addHousieGame" value="addHousieGame">
                <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-check-square-o"></i> Add</button>
              <?php } ?>
              </div>
            </form>

          <?php } ?>
          </div>
        </div>
      </div>
    </div><!--End Row-->

  </div>
  <!-- End container-fluid-->
</div>

<div class="modal fade" id="editbillCategoryModal">
  <div class="modal-dialog">
    <div class="modal-content border-myassociation">
      <div class="modal-header bg-myassociation">
        <h5 class="modal-title text-white">Import Housie Questions</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="queDiv">
        <h5>Please Wait..,Question fetching from server </h5>
      </div>
     
    </div>
  </div>
</div>
<?php } ?>