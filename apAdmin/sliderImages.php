    <div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-6">
        <h4 class="page-title">App Banner</h4>
        
      </div>
      
    </div>
    
    <?php
    $rq1 = $d->select("app_slider_master","society_id='$society_id'");
    $rows = mysqli_num_rows($rq1);
    if ($rows < 50) {
    ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="sliderImageAddValidation" method="post" action="controller/sliderController.php" enctype="multipart/form-data">
              <h4 class="form-header text-uppercase">
              <i class="fa fa-address-book-o"></i>
              Add Banner Images 
              </h4>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Banner Image <span class="required">*</span></label>
                <div class="col-sm-10">
                  <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
                  <input required="" class="form-control-file border onlyPhoto" required="" type="file"  name="slider_image" accept="image/*">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Youtube Video Id </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control"  name="youtube_url" minlength="5" maxlength="40">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">URL </label>
                <div class="col-sm-10">
                  <input class="form-control" maxlength="100" type="url"  name="page_url">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Phone Number </label>
                <div class="col-sm-10">
                  <input class="form-control" id="trlDays" type="text" maxlength="10" minlength="10" name="page_mobile">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">About Offer </label>
                <div class="col-sm-10">
                  <textarea class="form-control"  type="text" maxlength="250"  name="about_offer"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Order By</label>
                <div class="col-sm-10">
                  <label><input type="radio" class="order_by" name="order_by" checked value="0" > Random</label>
                  <label><input type="radio" class="order_by" name="order_by" value="1"> Ordered</label>
                </div>
              </div>
              <div class="form-group row d-none" id="banner_order_div">
                <label for="input-12" class="col-sm-2 col-form-label">Order Value <span class="required">*</span></label>
                <div class="col-sm-10">
                  <input type="text" class="form-control onlyNumber" required name="order_value">
                </div>
              </div>
              <div class="form-footer text-center">
                <button type="submit" class="btn btn-success" name = "addSliderImage"><i class="fa fa-check-square-o"></i> SAVE</button>
               
              </div>
            </form>
          </div>
        </div>
      </div>
      </div><!--End Row-->
      <?php
      }
      ?>
      <!--End Row-->
      <!-- Msanage Slider -->
     
<?php //16march2020 ?>
 <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      
                      
                      <th>#</th>
                      <th>Slider</th>
                       <th>Company</th>
                     
                    </tr>
                </thead>
                <tbody>
                    <?php 
                  $i=1;
                    $q=$d->select("app_slider_master","");
                  while ($data=mysqli_fetch_array($q)) {
                    extract($data);
                   ?>
                    <tr>
                  
                      <td><?php echo $i++; ?></td>
                      <td><a  data-fancybox="images" data-caption="Photo Name : <?php echo $slider_image_name; ?>"  href="../img/sliders/<?php echo $slider_image_name; ?>" target="_blank"><img width="100" src="../img/sliders/<?php echo $slider_image_name; ?>"></a></td>
                      <td>
                          <form action="controller/sliderController.php" method="post">
                             <input type="hidden" name="app_slider_id_delete" value="<?php echo $app_slider_id; ?>">
                            <input type="hidden" name="deleteSliderImage" value="deleteSliderImage">
                            <a name="editSlider" value="editSlider" href="editSliderImage?id=<?php echo $app_slider_id; ?>" class="btn btn-sm btn-warning">Edit </a>
                            <?php if($slider_type!=1){ ?>
                          	<button type="submit" class="btn btn-sm btn-danger form-btn shadow-primary">Delete</button>
                          <?php } ?>
                          </form>
                      </td>
                    </tr>
                   
                   <?php } ?> 
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
<?php //16march2020 ?>


      </div><!--End Row-->
    </div>
  </div>
</div>
<!-- End container-fluid-->
</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<script src="assets/js/jquery.min.js"></script>

<script>
  $(document).on('change', '.order_by', function(){
    value = $(this).val();
    if(value == 1){
      $('#banner_order_div').removeClass('d-none');
    }else{
      $('#banner_order_div').addClass('d-none');
    }
  });
</script>