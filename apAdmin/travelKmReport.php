  <?php error_reporting(0);

  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year'] = $_REQUEST['month_year'];

  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row ">
        <div class="col-sm-9 col-12">
          <h4 class="page-title">Travel KM Report</h4>
        </div>
      </div>
      <form action="" class="branchDeptFilter">
        <div class="row ">
          <?php include 'selectBranchDeptEmpForFilterAll.php' ?>

          <div class="col-md-2 col-6 form-group">
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-1 days'));} ?>">   
          </div>
          <div class="col-md-2 col-6 form-group">
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
          </div>          
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get">
            </div>
        </div>
      </form>
      <div class="row mt-2">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php
                $i = 1;
                if (isset($dId) && $dId > 0) {
                  $deptFilterQuery = " AND users_master.floor_id='$dId'";
                }

                if(isset($bId) && $bId>0) {
                  $blockFilterQuery = " AND users_master.block_id='$bId'";
                }

                if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                  $dateFilterQuery = " AND attendance_master.attendance_date_start BETWEEN '$from' AND '$toDate'";
                }
                if (isset($uId) && $uId > 0) {
                  $userFilterQuery = "AND attendance_master.user_id='$uId'";
                }

                $q = $d->selectRow("attendance_master.*,users_master.*,block_master.block_name,floors_master.floor_name,shift_timing_master.shift_type,leave_master.leave_type_id,leave_master.leave_day_type,leave_master.leave_id","attendance_master LEFT JOIN shift_timing_master ON shift_timing_master.shift_time_id = attendance_master.shift_time_id 
                LEFT JOIN leave_master ON leave_master.user_id = attendance_master.user_id AND leave_master.leave_start_date =attendance_master.attendance_date_start,users_master,block_master,floors_master", " users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.user_id=attendance_master.user_id AND attendance_master.society_id='$society_id' AND users_master.delete_status=0  $blockFilterQuery $deptFilterQuery $dateFilterQuery $userFilterQuery $blockAppendQueryUser", "ORDER BY attendance_master.attendance_date_start DESC");
               
                $counter = 1;
             
                ?>
                  <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Sr.No</th>
                        <th>Name</th>
                        <th>Branch (Department)</th>
                        <th>Travel KM</th>
                        <th>Punch In Date</th>
                        <th>Punch In</th>
                        <th>Punch Out Date</th>
                        <th>Punch Out</th>
                        <th>Hours</th>
                        <th>is modified</th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      while ($data = mysqli_fetch_array($q)) {
                        if ($data['attendance_in_from'] != null) {
                          if ($data['attendance_in_from'] == 0) {
                            $attendance_in_from = "Face App";
                          } else {
                            $attendance_in_from = "User App";
                          }
                        } else {
                          $attendance_in_from = "";
                        }
                        if ($data['attendance_out_from'] != null) {
                          if ($data['attendance_out_from'] == 0) {
                            $attendance_out_from = "Face App";
                          } else {
                            $attendance_out_from = "User App";
                          }
                        } else {
                          $attendance_out_from = "";
                        }

                      ?>
                        <tr <?php if ($data['is_modified'] == "1") { echo "class='text-danger'";  } ?>>

                          <td><?php echo $counter++; ?></td>
                         
                         
                          <td><?php echo $data['user_full_name']; ?></td>
                          <td><?php echo $data['block_name']; ?> (<?php echo $data['floor_name']; ?>)</td>
                          <td class="salary_earn_ded"><?php  $totalKm = $data['total_travel_meter']/1000;
                                    echo number_format((float)$totalKm, 2, '.', ''); 
                             ?></td>
                          <td><?php if ($data['attendance_date_start'] != '0000-00-00' && $data['attendance_date_start'] != 'null') {
                                echo date("d M Y", strtotime($data['attendance_date_start'])) . " (" . date("D", strtotime($data['attendance_date_start'])) . ")";
                              } ?></td>
                          <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_in_time'] != 'null') {
                            if ($attendance_in_from == "") {
                              echo date("h:i A", strtotime($data['punch_in_time']));
                            } else {
                               echo date("h:i A", strtotime($data['punch_in_time'])) . "(" . $attendance_in_from . ")";
                              }
                              } ?>
                           
                          </td>

                          <td><?php if ($data['attendance_date_end'] != '0000-00-00' && $data['attendance_date_end'] != 'null') { echo date("d M Y", strtotime($data['attendance_date_end']));
                                if ($attendance_out_from != "") {
                                  echo "(" . $attendance_out_from . ")";
                                }
                              } ?></td>
                          <td><?php if ($data['punch_out_time'] != '00:00:00' && $data['punch_out_time'] != 'null') {
                                echo date("h:i A", strtotime($data['punch_out_time']));
                              } ?>
                            <?php if ($data['punch_out_image'] != '') { ?>
                            <?php } ?>
                            <?php if ($data['punch_out_latitude'] != '' & $data['punch_out_longitude'] != '') { ?>
                            <?php } ?>
                          </td>
                          <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_out_time'] != '00:00:00' && $data['total_working_hours']=='') {
                            echo $d->getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],$data['punch_in_time'],$data['punch_out_time']);
                              } else {
                                echo $data['total_working_hours'];
                              } ?>
                          </td>
                          <td><?php if ($data['is_modified'] == '1' ) {
                             
                              echo  "Yes";
                              } else {
                              echo "No";
                              } ?>
                          </td>
                        </tr>
                      <?php } ?>
                        <tr>
                          <td>Total KM</td>
                          <td></td>
                          <td></td>
                          <td class="tot"><b></b></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                    </tbody>
                  </table>
                
              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
<script src="assets/js/jquery.min.js"></script>
<script>
var ttc = '<?php echo $ttc; ?>';
$(document).ready(function()
{
  
    var sum = 0
    $(".salary_earn_ded").each(function()
    {
      sum += parseFloat($(this).text());
    });
    $('.tot').text(sum.toFixed(2));
});
</script>