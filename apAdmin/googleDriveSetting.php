<?php 
$q=$d->select("society_master","society_id='$society_id'");  
$data=mysqli_fetch_array($q);
extract($data);
?>
<div class="content-wrapper">
    <div class="container-fluid"> 
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Google Drive Setting</h4>
       
     </div>
     <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb--> 
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="addGoogleDriveSettingForm" action="controller/buildingController.php" method="post">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group row w-100 mx-0">
                      <label for="input-12" class="col-sm-12 col-form-label">Google Client ID <span class="required">*</span></label>
                      <div class="col-sm-12">
                        <input type="text" name="google_client_id" id="google_client_id" class="form-control" required="" value="<?php echo $google_client_id; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group row w-100 mx-0">
                      <label for="input-12" class="col-sm-12 col-form-label">Google Client Secret Key <span class="required">*</span></label>
                      <div class="col-sm-12">
                        <input type="text" id="google_client_secret_key" class="form-control" id="input-10" name="google_client_secret_key" required="" value="<?php echo $google_client_secret_key; ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-footer text-center">
                	<input type="hidden" name="addGoogleDriveSetting" value="<?php echo $society_id; ?>">
                	<input type="hidden" name="addGoogleDriveSetting" value="addGoogleDriveSetting">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->save; ?></button>
                </div>
              </form>
            </div>
            <div class="">
              <ul>1. Go to the Google API Console.</ul>
              <ul>2. Select an existing project from the projects list, or click NEW PROJECT to create a new project:
                <li>Type the Project Name.</li>
                <li>The project ID will be created automatically under the Project Name field. (Optional) You can change this project ID by the Edit link, but it should be unique worldwide.</li>
                <li>Click the CREATE button.</li>
              </ul>
              <ul>3. Select the newly created project and enable the Google Drive API service.
                <li>In the sidebar, select Library under the APIs & Services section.</li>
                <li>Search for the Google Drive API service in the API list and select Google Drive API.</li>
                <li>Click the ENABLE button to make the Google Drive API Library available.</li>
              </ul>
              <ul>4. In the sidebar, select Credentials under the APIs & Services section.</ul>
              <ul>5. Select the OAuth consent screen tab, specify the consent screen settings.
                <li>Enter the Application name.</li>
                <li>Choose a Support email.</li>
                <li>Specify the Authorized domains which will be allowed to authenticate using OAuth.</li>
                <li>Click the Save button.</li>
              </ul>
              <ul>6. Select the Credentials tab, click the Create credentials drop-down and select OAuth client ID.
                <li>In the Application type section, select Web application.</li>
                <li>In the Authorized redirect URIs field, specify the redirect URL.</li>
                <li>Copy Authorized redirect URI</li>
                <li>Click the Create button.</li>
              </ul>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->