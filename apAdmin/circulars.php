<?php
extract(array_map("test_input" , $_POST));
?>
<style type="text/css" media="screen">
 .card-body img {
  width: 280px !important;
 }  
</style>
<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
        <h4 class="page-title"> <?php echo $xml->string->notice_board; ?></h4>
     </div>
     <div class="col-sm-3 col-6">
        <div class="btn-group float-sm-right">
          <a href="circularAdd" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i>  <?php echo $xml->string->add; ?> <?php echo $xml->string->notice_board; ?></a>
          <!-- </button> -->
        </div>
      </div>
     </div>
    <!-- End Breadcrumb-->
     <div class="row"  id="AllNoticeBoard">
        <?php /*$q=$d->select("notice_board_master","society_id='$society_id'  ","ORDER BY updated_at DESC");
          if(mysqli_num_rows($q)>0) {
            while($row=mysqli_fetch_array($q)){ ?> 
              <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card">
                   <?php //IS_569 title="<?php echo $row['notice_title']; ?>
                  <div class="card-header bg-primary text-white" title="<?php echo $row['notice_title'];?>"><i class="fa fa-bullhorn" aria-hidden="true"></i> <?php 
                  //IS_569
                  // echo $row['notice_title'];
                  $strlen = strlen($row['notice_title']);
                  if($strlen > 40 ){
                    $str = substr($row['notice_title'], 0, 30);
                    echo $str."...";
                  } else {
                     echo $row['notice_title'];
                  }
                   //IS_569
                 ?> 
                  
                </div>
                  <div class="card-body">
                     <?php //IS_569 <div class="cls-notice-info"> 
                     ?>
                    <p class="card-text"><div class="cls-notice-info"><?php echo stripslashes($row['notice_description']);?></div></p>
                  </div>
                  <?php //IS_983

                  if($row['block_id'] =="0" || $row['block_id'] ==""){
                    $block_name_data = "All";
                  } else{ 

                    $block_master_qry=$d->select("block_master","block_id in (".$row['block_id'].")  ","");
                    $block_name_data = array();
                     while ($block_master_data=mysqli_fetch_array($block_master_qry)) {
                        $block_name_data[] = $block_master_data['block_name'];
                     }

                      $block_name_data = implode(", ",  $block_name_data);
                    //$block_master_data =   mysqli_fetch_assoc($block_master_qry);
                    //$block_name_data = $block_master_data['block_name'];
                  }

                  


                  ?>
                  <div  class="card-footer">
                  <?php if ($row['notice_attachment']!='') {
                    echo "<a target='_blank' href=../img/noticeBoard/$row[notice_attachment]><i class='fa fa-paperclip'></i> ".$xml->string->attachment."</a>";
                    echo "<br>";
                  } else {
                    echo "<br>";
                  } ?>
                  Circular For: <?php echo $block_name_data;?> <?php echo $xml->string->block;?>  <?php 
                  if ($row['noticeboard_type']==1) {
                    echo  '('.$d->count_data_direct("notice_board_unit_id","notice_board_unit_master","society_id='$society_id' AND notice_board_id='$row[notice_board_id]'").' Department)';
                  }
                  ?> <br>


                    <?php   //IS_1008
                    // Updated on: <?php echo $row['notice_time']; 
                     if($row['updated_at'] ==NULL && $row['created_at'] ==NULL){ ?>
                      <?php echo $xml->string->updated_on; ?>: <?php   echo  date("d M Y h:i A", strtotime($row['notice_time']));?><br>
                    <?php  } else  if($row['updated_at'] ==NULL){ ?>
                     ?php echo $xml->string->created_on; ?>: <?php echo date("d M Y h:i A", strtotime($row['created_at']));?><br>
                 <?php  } else { ?>
                  
                     <?php echo $xml->string->updated_on; ?>: <?php   echo date("d M Y h:i A", strtotime($row['updated_at']));?><br>
                <?php  } //IS_1008 
                      $selectedBlocks = explode(",",$row['block_id']);
                      $difrentResult=array_diff($selectedBlocks,$blockAryAccess);
                      if (count($difrentResult)>0  && $blockAppendQueryOnly!='' && $row['block_id']!=0) {
                     
                         
                      } else {

                      ?>
                      Status: 
                        <?php if ($row['active_status']==0) { ?>
                        <span class="badge badge-pill badge-primary m-1"><?php echo $xml->string->published; ?> </span>
                      <?php } else { ?>
                        <span class="badge badge-pill badge-warning m-1"><?php echo $xml->string->save_as_draft; ?> </span>
                      <?php } ?>
                     <br>

                 
                    <form class="mt-2" id="signupForm" style="float: left;" action="controller/noticeController.php" method="post">
                      <input type="hidden" name="block_id" value="<?php echo $row['block_id']; ?>">
                      <input type="hidden" name="notice_board_id_delete" value="<?php echo $row['notice_board_id']; ?>">
                      <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash-o"></i> <?php echo $xml->string->delete; ?></button>
                    </form>
                    <form class="mt-2" style="float: left;margin-left: 10px;" action="circularAdd" method="post">
                      <input type="hidden" name="notice_board_id" value="<?php echo $row['notice_board_id']; ?>">
                      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i> <?php echo $xml->string->edit; ?></button>
                    </form>
                    <?php } 
                    if($row['active_status'] != 1)
                    {
                    ?>
                      <form class="mt-2" style="float: left;margin-left: 10px;" action="circularAdd" method="post">
                        <input type="hidden" name="block_id" value="<?php echo $row['block_id']; ?>">
                        <input type="hidden" name="notice_board_id_read" value="<?php echo $row['notice_board_id']; ?>">
                        <button title="Read Status" type="submit" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> <?php echo $xml->string->read_status; ?></button>
                      </form>
                    <?php 
                  } ?>
                  </div>
                </div>
              </div>
              <?php 
            } 
          } else {
      echo "<img src='img/no_data_found.png'>";
    }*/ ?>
      </div>

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
  <script src="assets/js/jquery.min.js"></script>
  <script type="text/javascript">
    var csrf ="<?php echo $_SESSION["token"]; ?>";
    var offset=0;
    loadMoreData(offset);
    
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height()+1 >= $(document).height()) {
            
            offset=offset+1;
            loadMoreData(offset);
            
        }
    });


    function loadMoreData(offset){
      var limit =12;
      var last_id = $(".post-id:last").attr("data-id");
      var key = offset;
      
      
      $.ajax(
        {
            url: "getCirculars.php",
            type: "POST",
            data: {limit:limit,offset:offset,csrf:csrf},
            beforeSend: function()
            {
                $('.ajax-load').show();
            }
        })
        .done(function(data)
        {
            $('.ajax-load').hide();
            $("#AllNoticeBoard").append(data);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('server not responding...');
        });
    }
  
  </script>
