<?php error_reporting(0);
$uId = (int)$_REQUEST['uId'];
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
     <div class="row pt-2 pb-2">
        <div class="col-md-9">
          <h4 class="page-title">Employee Location </h4>
     </div>
     <div class="col-md-3 ">
       <div class="btn-group float-sm-right">
        
      </div>
     </div>
     </div>
     <form action="" class="branchDeptFilter">
      <div class="row ">
        <?php include('selectBranchDeptForFilter.php'); ?>
        <div class="col-md-3 form-group ">
          <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
        </div>
      </div>
    </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="card-block table-responsive">
                         <?php include 'muliViewEmployees.php'; ?>
              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div>

<script type="text/javascript">
  initMap();
  setInterval(function()
    {
      initMap();
    }, 50000);
</script>