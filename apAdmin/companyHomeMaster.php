<?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Home</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
          <?php $total = $d->count_data_direct("company_home_id","company_home_master","society_id='$society_id'");
            if( $total==0) {
          ?>  
          <a href="javascript:void(0)" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          <?php } ?>
            <a href="javascript:void(0)" onclick="DeleteAll('deleteCompanyHome');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>                      
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("company_home_master","society_id='$society_id'");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                        
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['company_home_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['company_home_id']; ?>">                         
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['company_home_title']; ?></td>
                       <td><?php custom_echo($data['company_home_description'], 80); ?></td>
                       <td><button type="submit" class="btn btn-sm btn-primary mr-2" onclick="companyHomeDataSet(<?php echo $data['company_home_id']; ?>)"> <i class="fa fa-pencil"></i></button>
                       <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="getCompanyHomeData(<?php echo $data['company_home_id']; ?>)"> <i class="fa fa-eye"></i></button>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Company Home</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addCompanyHomeMasterForm" action="controller/CompanyHomeController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Title <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                  <input type="text" class="form-control" placeholder="Company Title" id="company_home_title" name="company_home_title" value="">
              </div>  
           </div>   
           <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Description <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                  <textarea class="form-control" placeholder="Company description" id="company_home_description" name="company_home_description"></textarea>
              </div>  
           </div>                 
           <div class="form-footer text-center">
             <input type="hidden" id="company_home_id" name="company_home_id" value="" >
             <button id="addCompanyHomeMasterBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addCompanyHomeMaster"  value="addCompanyHomeMaster">
             <button id="addCompanyHomeMasterBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyHomeMaster');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>


<div class="modal fade" id="companyHomeModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Company Home Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">

          <div class="row col-md-12"  id="showCompanyHome">
          </div>

        </div>
      </div>

    </div>
  </div>
</div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
