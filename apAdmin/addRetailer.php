<?php
if(isset($_POST['addRetailerBtn']) && $_POST['addRetailerBtn'] == "addRetailerBtn")
{
    extract($_POST);
}
$retailer_distributor_id_arr = [];
$retailer_route_id_arr = [];
if(isset($_POST['editRetailer']) && isset($_POST['retailer_id']))
{
    $retailer_id = $_POST['retailer_id'];
    $q = $d->select("retailer_master","retailer_id = '$retailer_id'");
    $data = $q->fetch_assoc();
    extract($data);

    $q1 = $d->selectRow("retailer_distributor_relation_id,distributor_id","retailer_distributor_relation_master","retailer_id = '$retailer_id'");
    while($data1 = $q1->fetch_assoc())
    {
        $retailer_distributor_id_arr[] = $data1['distributor_id'];
    }

    $q2 = $d->selectRow("route_id","route_retailer_master","retailer_id = '$retailer_id'");
    while($data2 = $q2->fetch_assoc())
    {
        $retailer_route_id_arr[] = $data2['route_id'];
    }
    $form_id = "retailerAddForm";
}
else
{
    $form_id = "retailerAddForm";
}

$soc_lat_lng = $d->selectRow("society_latitude,society_longitude","society_master","society_id = '$society_id'");
$fet = $soc_lat_lng->fetch_assoc();
$society_latitude = $fet['society_latitude'];
$society_longitude = $fet['society_longitude'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="<?php echo $form_id; ?>" action="controller/retailerController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <?php
                            if(isset($_POST['editRetailer']) && isset($_POST['retailer_id']))
                            {
                            ?>
                            <i class="fa fa-edit"></i>
                            Edit Retailer
                            <?php
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-plus"></i>
                            Add Retailer
                            <?php
                            }
                            ?>
                            </h4>
                            <div class="form-group row">
                                <label for="country_id" class="col-sm-2 col-form-label">Country <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="country_id" name="country_id" required onchange="getStateCity(this.value);">
                                        <option value="">-- Select --</option>
                                        <?php
                                        $cq = $d->select("countries");
                                        while ($cd = $cq->fetch_assoc())
                                        {
                                        ?>
                                        <option <?php if($country_id == $cd['country_id']) { echo 'selected'; } ?> value="<?php echo $cd['country_id'];?>"><?php echo $cd['country_name'];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="state_city_id" class="col-sm-2 col-form-label">State-City <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="state_city_id" name="state_city_id" required onchange="getAreaList(this.value);">
                                        <option value="">-- Select --</option>
                                        <?php
                                        if(isset($editRetailer) || isset($addRetailerBtn))
                                        {
                                            $qt = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id' AND c.flag = 1");
                                            while ($qd = $qt->fetch_assoc())
                                            {
                                            ?>
                                        <option <?php if($city_id == $qd['city_id']) { echo 'selected'; } ?> value="<?php echo $qd['city_id']."-".$qd['state_id'];?>"><?php echo $qd['city_name'];?>-<?php echo $qd['state_name'];?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="route_description" class="col-sm-2 col-form-label">Area <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="area_id" name="area_id" required onchange="getDistributorList(this.value);">
                                        <option value="">-- Select --</option>
                                        <?php
                                        if(isset($editRetailer) || isset($addRetailerBtn))
                                        {
                                            $qt = $d->selectRow("area_id,area_name","area_master_new","country_id = '$country_id' AND state_id = '$state_id' AND city_id = '$city_id'");
                                            while ($qd = $qt->fetch_assoc())
                                            {
                                            ?>
                                        <option <?php if($area_id == $qd['area_id']) { echo 'selected'; } ?> value="<?php echo $qd['area_id']; ?>"><?php echo $qd['area_name'];?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="retailer_name" class="col-sm-2 col-form-label">Retailer Name <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editRetailer)){ echo $retailer_name; } ?>" required type="text" class="form-control" name="retailer_name" id="retailer_name" maxlength="150">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="route_id" class="col-sm-2 col-form-label">Route</label>
                                <div class="col-sm-4">
                                    <select class="form-control multiple-select" multiple id="route_id" name="route_id[]">
                                        <option value="">-- Select --</option>
                                        <?php
                                            $append = "";
                                            if(isset($city_id) && $city_id != 0)
                                            {
                                                $append .= " AND rm.city_id = '$city_id'";
                                            }
                                            $qro = $d->selectRow("rm.route_id,rm.route_name,c.city_name","route_master AS rm JOIN cities AS c ON c.city_id = rm.city_id","rm.route_active_status = 0".$append);
                                            while ($qdro = $qro->fetch_assoc())
                                            {
                                        ?>
                                        <option <?php if(in_array($qdro['route_id'],$retailer_route_id_arr)) { echo 'selected'; } ?> value="<?php echo $qdro['route_id']; ?>"><?php echo $qdro['route_name'] . " (" . $qdro['city_name'] . ")"; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                                <label for="retailer_contact_person" class="col-sm-2 col-form-label">Retailer Contact Person Name <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editRetailer)){ echo $retailer_contact_person; } ?>" required type="text" class="form-control" name="retailer_contact_person" id="retailer_contact_person" maxlength="100">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="retailer_contact_person_number" class="col-sm-2 col-form-label">Retailer Contact Person Number <span class="text-danger">*</span></label>
                                <div class="col-sm-2">
                                    <select name="retailer_contact_person_country_code" class="form-control single-select" id="retailer_contact_person_country_code" required>
                                        <?php include 'country_code_option_list.php'; ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input value="<?php if(isset($editRetailer)){ echo $retailer_contact_person_number; } ?>" required type="text" class="form-control onlyNumber" name="retailer_contact_person_number" id="retailer_contact_person_number" maxlength="15">
                                </div>
                                <label for="retailer_alt_contact_number" class="col-sm-2 col-form-label">Retailer Alt Number</label>
                                <div class="col-sm-2">
                                    <select name="retailer_alt_country_code" class="form-control single-select" id="retailer_alt_country_code">
                                        <?php include 'country_code_option_list.php'; ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input value="<?php if(isset($editRetailer) && $retailer_alt_contact_number != 0){ echo $retailer_alt_contact_number; } ?>" required type="text" class="form-control onlyNumber" name="retailer_alt_contact_number" id="retailer_alt_contact_number" maxlength="15">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="route_description" class="col-sm-2 col-form-label">Distributor <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control multiple-select" multiple id="distributor_id" name="distributor_id[]" required>
                                        <option value="">-- Select --</option>
                                        <?php
                                        // if(isset($editRetailer) || isset($addRetailerBtn) && isset($state_id) && isset($city_id))
                                        // {
                                            /*$append = "";
                                            if(!empty($state_id))
                                            {
                                                $append .= " AND dm.state_id = '$state_id'";
                                            }
                                            if(!empty($city_id))
                                            {
                                                $append .= " AND dm.city_id = '$city_id'";
                                            }*/
                                            /*if(!empty($area_id))
                                            {
                                                $append .= " AND dm.distributor_area_id = '$area_id'";
                                            }*/
                                            $qd = $d->selectRow("dm.distributor_id,dm.distributor_name,dm.distributor_contact_person,dm.distributor_contact_person_number,dm.distributor_contact_person_country_code","distributor_master AS dm","dm.distributor_status = 1");
                                            while ($qdd = $qd->fetch_assoc())
                                            {
                                            ?>
                                        <option <?php if(in_array($qdd['distributor_id'],$retailer_distributor_id_arr)) { echo 'selected'; } ?> value="<?php echo $qdd['distributor_id']; ?>"><?php echo $qdd['distributor_name'] . " (" .$qdd['distributor_contact_person']. " - " . $qdd['distributor_contact_person_country_code'] . " " . $qdd['distributor_contact_person_number'] .")"; ?></option>
                                        <?php
                                            }
                                        // }
                                        ?>
                                    </select>
                                </div>
                                <label for="retailer_geofence_range" class="col-sm-2 col-form-label">Range(Meter) <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" type="text" value="<?php if (isset($editRetailer) && $retailer_geofence_range > 0) { echo $retailer_geofence_range; }else{ echo 50; } ?>" onkeyup="chnageUserRange()" min="5" id="retailer_geofence_range" name="retailer_geofence_range" class="form-control onlyNumber">
                                    <input type="hidden" id="textInput2" value="<?php if(isset($editRetailer) && $retailer_geofence_range > 0) {echo $retailer_geofence_range; } ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="retailer_pincode" class="col-sm-2 col-form-label">Retailer Pincode </label>
                                <div class="col-sm-4">
                                    <input type="text" id="retailer_pincode" maxlength="6" value="<?php if(isset($editRetailer) && $retailer_pincode != 0) { echo $retailer_pincode; } ?>" class="form-control onlyNumber" inputmode="numeric" name="retailer_pincode">
                                </div>
                                <label for="retailer_gst_no" class="col-sm-2 col-form-label">Retailer GST No </label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" type="text" value="<?php if (isset($editRetailer) && $retailer_gst_no > 0) { echo $retailer_gst_no; } ?>" id="retailer_gst_no" name="retailer_gst_no" class="form-control" maxlength="15">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="retailer_credit_limit" class="col-sm-2 col-form-label">Retailer Credit Limit </label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" type="text" value="<?php if (isset($editRetailer) && $retailer_credit_limit > 0) { echo $retailer_credit_limit; } ?>" id="retailer_credit_limit" name="retailer_credit_limit" inputmode="numeric" class="form-control onlyNumber" maxlength="15">
                                </div>
                                <label for="retailer_credit_days" class="col-sm-2 col-form-label">Retailer Credit Days </label>
                                <div class="col-sm-4">
                                    <input type="text" id="retailer_credit_days" value="<?php if(isset($editRetailer) && $retailer_credit_days != 0) { echo $retailer_credit_days; } ?>" class="form-control onlyNumber" inputmode="numeric" name="retailer_credit_days" maxlength="3">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="retailer_photo" class="col-sm-2 col-form-label">Retailer Photo </label>
                                <div class="col-sm-4">
                                    <input type="file" id="retailer_photo" name="retailer_photo" inputmode="numeric" class="form-control-file border imageOnly" accept="image/*">
                                </div>
                                <label for="photo_preview" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-4">
                                    <?php
                                    if(isset($editRetailer) && $retailer_photo != "" && file_exists("../img/users/recident_profile/".$retailer_photo))
                                    {
                                    ?>
                                    <img id="blah" src="../img/users/recident_profile/<?php if(isset($editRetailer) && $retailer_photo != ""){ echo $retailer_photo; } ?>" width="100" height="100" alt="Retailer photo" class='profile d-none'/>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <img id="blah" width="100" height="100" alt="Retailer photo" class='profile d-none'/>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="retailer_address" class="col-sm-2 col-form-label">Retailer Address <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <textarea rows="4" class="form-control" name="retailer_address" id="retailer_address" maxlength="200"><?php if(isset($editRetailer)){ echo $retailer_address; } ?></textarea>
                                </div>
                                <label for="retailer_type" class="col-sm-2 col-form-label">Retailer Type </label>
                                <div class="col-sm-4">
                                    <input type="text" id="retailer_type" value="<?php if(isset($editRetailer) && $retailer_type != "") { echo $retailer_type; } ?>" class="form-control" name="retailer_type">
                                </div>
                            </div>
                            <div class="row form-group">
                                <input id="searchInput6" name="location_name" class="form-control" type="text" placeholder="Enter location" value="<?php if(isset($editRetailer) && $retailer_google_address != ""){ echo $retailer_google_address; } ?>">
                                <div class="map" id="map2" style="width: 100%; height: 300px;"></div>
                            </div>
                            <input type="hidden" class="form-control" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                            <input type="hidden" name="zoomValue" id="zoomValue" value="17">
                            <input type="hidden" name="zoomValueBlk" id="zoomValueBlk" value="17">
                            <input type="hidden" value="<?php if(isset($editRetailer) && $retailer_latitude > 0){ echo $retailer_latitude; } ?>" id="retailer_latitude" name="retailer_latitude">
                            <input type="hidden" value="<?php if(isset($editRetailer) && $retailer_longitude > 0){ echo $retailer_longitude; } ?>" id="retailer_longitude" name="retailer_longitude" class="form-control">
                            <div class="form-footer text-center">
                                <input type="hidden" name="addRetailer" value="addRetailer">
                                <?php
                                if(isset($editRetailer))
                                {
                                    foreach($retailer_distributor_id_arr AS $key => $value)
                                    {
                                    ?>
                                <input type="hidden" class="form-control" name="old_distributor_id[<?php echo $key; ?>]" value="<?php echo $value; ?>">
                                    <?php
                                    }
                                    foreach($retailer_route_id_arr AS $key1 => $value1)
                                    {
                                    ?>
                                <input type="hidden" class="form-control" name="old_route_id[<?php echo $key1; ?>]" value="<?php echo $value1; ?>">
                                    <?php
                                    }
                                ?>
                                <input type="hidden" name="old_photo" value="<?php echo $retailer_photo; ?>">
                                <input type="hidden" class="form-control" name="retailer_id" id="retailer_id" value="<?php echo $retailer_id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                <input type="hidden" class="form-control" name="retailer_id" id="retailer_id" value="">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->

<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>
<script>
$().ready(function()
{
    var latitude = "<?php echo $society_latitude; ?>";
    var longitude = "<?php echo $society_longitude; ?>";
    <?php
    if(isset($editRetailer) && !empty($retailer_latitude) && !empty($retailer_longitude))
    {
    ?>
    initialize2(<?php echo $retailer_latitude; ?>, <?php echo $retailer_longitude; ?>);
    <?php
    }
    else
    {
    ?>
    initialize2(latitude, longitude);
    <?php
    }
    ?>

    $("#retailer_gst_no").keyup(function ()
    {
        $(this).val($(this).val().toUpperCase());
    });

    $("#retailer_gst_no").on("keydown", function (e)
    {
        return e.which !== 32;
    });

    <?php
    if(isset($editRetailer))
    {
        if($retailer_photo != "" && file_exists("../img/users/recident_profile/".$retailer_photo))
        {
    ?>
    $('#blah').removeClass('d-none');
    $('#blah').attr('src', '../img/users/recident_profile/<?php echo $retailer_photo; ?>');
        <?php
        }
        else
        {
        ?>
    $('#blah').addClass('d-none');
        <?php
        }
        ?>
    $('#retailer_photo').rules('remove', 'required');
    $("#retailer_contact_person_country_code").val('<?php echo $retailer_contact_person_country_code; ?>').trigger('change');
    $("#retailer_alt_country_code").val('<?php echo $retailer_alt_country_code; ?>').trigger('change');
    <?php
    }
    ?>
});

function initialize2(d_lat, d_long)
{
    var latlng = new google.maps.LatLng(d_lat, d_long);
    var latitute = "<?php echo $society_latitude; ?>";
    var longitute = "<?php echo $society_longitude; ?>";
    zoomVal = $('#zoomValue').val();
    var map = new google.maps.Map(document.getElementById('map2'), {
        center: latlng,
        zoom: parseInt(zoomVal)
    });
    mapzoom = map.getZoom();
    /////////////////on zoom in out
    google.maps.event.addListener(map, 'zoom_changed', function() {
        var zoomLevel = map.getZoom();
        $('#zoomValue').val(zoomLevel);
        mapzoom = map.getZoom(); //to stored the current zoom level of the map
    });
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
    });
    // Add circle overlay and bind to marker
    retailer_geofence_range = $('#retailer_geofence_range').val();
    if (retailer_geofence_range == "") {
        retailer_geofence_range = 10;
    }
    var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(retailer_geofence_range), // 10 miles in metres
        fillColor: '#AA0000'
    });
    circle.bindTo('center', marker, 'position');
    var parkingRadition = 5;
    var citymap = {
        newyork: {
            center: {
                lat: latitute,
                lng: longitute
            },
            population: parkingRadition
        }
    };

    var input = document.getElementById('searchInput6');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    autocomplete10.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place5 = autocomplete10.getPlace();
        if (!place5.geometry) {
            window.alert("Autocomplete's returned place5 contains no geometry");
            return;
        }

        // If the place5 has a geometry, then present it on a map.
        if (place5.geometry.viewport) {
            map.fitBounds(place5.geometry.viewport);
        } else {
            map.setCenter(place5.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place5.geometry.location);
        marker.setVisible(true);

        var pincode = "";
        for (var i = 0; i < place5.address_components.length; i++) {
            for (var j = 0; j < place5.address_components[i].types.length; j++) {
                if (place5.address_components[i].types[j] == "postal_code") {
                    pincode = place5.address_components[i].long_name;
                }
            }
        }
        bindDataToForm2(place5.formatted_address, place5.geometry.location.lat(), place5.geometry.location.lng(), pincode, place5.name);
        infowindow.setContent(place5.formatted_address);
        infowindow.open(map, marker);

    });
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({
            'latLng': marker.getPosition()
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var places = results[0];
                    var pincode = "";
                    var serviceable_area_locality = places.address_components[4].long_name;
                    for (var i = 0; i < places.address_components.length; i++) {
                        for (var j = 0; j < places.address_components[i].types.length; j++) {
                            if (places.address_components[i].types[j] == "postal_code") {
                                pincode = places.address_components[i].long_name;
                            }
                        }
                    }
                    bindDataToForm2(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng(), pincode, serviceable_area_locality);
                }
            }
        });
    });
}

function bindDataToForm2(address, lat, lng, pin_code, serviceable_area_locality)
{
    document.getElementById('retailer_latitude').value = lat;
    document.getElementById('searchInput6').value = address;
    document.getElementById('retailer_longitude').value = lng;
}

function chnageUserRange()
{
    var latitude = $('#retailer_latitude').val();
    var longitude = $('#retailer_longitude').val();
    if(latitude == "")
    {
        latitude = 23.037786;
    }
    if(longitude == "")
    {
        longitude = 72.512043;
    }
    initialize2(latitude, longitude);
}

function readURL(input)
{
    if (input.files && input.files[0] && input.files[0].size <= 3000000 )
    {
        var reader = new FileReader();
        reader.onload = function(e)
        {
            $('#blah').removeClass('d-none');
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#retailer_photo").change(function()
{
    readURL(this);
});

function getStateCity(country_id)
{
    $.ajax({
        url: 'controller/retailerController.php',
        data: {getStateCityTag:"getStateCityTag",country_id:country_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#state_city_id').empty();
            response = JSON.parse(response);
            $('#state_city_id').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#state_city_id').append("<option value='"+value.city_id+"-"+value.state_id+"'>"+value.city_name+" - "+value.state_name+"</option>");
            });
        }
    });
}

function getAreaList(city_state_id)
{
    const country_id = $('#country_id').val();
    const city_id = city_state_id.substring(0, city_state_id.indexOf('-'));
    const state_id = city_state_id.substring(city_state_id.indexOf('-') + 1);
    $.ajax({
        url: 'controller/retailerController.php',
        data: {getAreaTag:"getAreaTag",country_id:country_id,state_id:state_id,city_id:city_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#area_id').empty();
            response = JSON.parse(response);
            $('#area_id').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#area_id').append("<option value='"+value.area_id+"'>"+value.area_name+"</option>");
            });
        }
    });

    $.ajax({
        url: 'controller/retailerController.php',
        data: {getRouteList:"getRouteList",city_id:city_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#route_id').empty();
            response = JSON.parse(response);
            $('#route_id').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#route_id').append("<option value='"+value.route_id+"'>"+value.route_name+" ("+value.city_name+")</option>");
            });
        }
    });
}

function getDistributorList(area_id)
{
    $.ajax({
        url: 'controller/retailerController.php',
        data: {getAreaLatLon:"getAreaLatLon",area_id:area_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            response = JSON.parse(response);
            initialize2(response.area_latitude,response.area_longitude);
        }
    });
}
</script>