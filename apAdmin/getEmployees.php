<?php
session_start();
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$society_id = $_COOKIE['society_id'];
extract(array_map("test_input" , $_POST));
if (isset($argument)) {
if($argument=='') {
//IS_1498
if (isset($emp_type)) {
if ($emp_type=='All') {
$q=$d->select("employee_master","society_id='$society_id'","");
} else   {
$q=$d->select("employee_master","society_id='$society_id' AND emp_type='$emp_type' ","");
}
} else
//IS_1498
if ($emp_type_id==1) {
$emp_type_id=0;
$q=$d->select("employee_master","society_id='$society_id' AND emp_type_id='$emp_type_id'","");
}
else if ($emp_type_id=='All') {
$q=$d->select("employee_master","society_id='$society_id'","");
} else {
$q=$d->select("employee_master","society_id='$society_id' AND emp_type_id='$emp_type_id'","");
}
} else {
//IS_1498
if (isset($emp_type)) {
if ($emp_type=='All') {
$q=$d->select("employee_master"," emp_name LIKE '%$argument%' AND society_id='$society_id'","");
} else   {
$q=$d->select("employee_master"," emp_name LIKE '%$argument%' AND society_id='$society_id' AND emp_type='$emp_type'","");
}
} else
//IS_1498
if ($emp_type_id==1) {
$emp_type_id=0;
$q=$d->select("employee_master","society_id='$society_id' AND emp_name LIKE '%$argument%' AND emp_type_id='$emp_type_id'","");
}
else if ($emp_type_id=='All') {
$q=$d->select("employee_master","society_id='$society_id' AND emp_name LIKE '%$argument%'","");
} else {
$q=$d->select("employee_master","society_id='$society_id' AND emp_type_id='$emp_type_id' AND emp_name LIKE '%$argument%'","");
}
}

$i=0;
if(mysqli_num_rows($q)>0) {
while ($row=mysqli_fetch_array($q)) {
extract($row);
$EW=$d->select("emp_type_master","emp_type_id='$row[emp_type_id]'");
$empTypeData=mysqli_fetch_array($EW);
?>
<div class="col-lg-3 col-6">
  <div id="accordion1">
    <div class="card mb-2">
      <div id="collapse-1" class="collapse show" data-parent="#accordion1" style="">
        <div class="p-2">
          <div class="text-center">
            <img style="height: 150px !important;width: 80;" onerror="this.src='img/user.png'"  src="../img/emp/<?php echo $emp_profile; ?>"  class="img-fluid rounded "  alt="card img">
          </div>
          <div class="card-title text-uppercase text-primary"> <?php echo $emp_name; ?><br><?php if ($empTypeData['emp_type_name']!='') {
            echo custom_echo($empTypeData['emp_type_name'],10);
            } else {
            echo "Security Guard";
            }  ?>
          </div>
          <ul class="list-unstyled">
            <li><i class="fa fa-mobile"></i> <?php echo $emp_mobile; ?></li>
            <!-- <li>Email : <?php echo $emp_email; ?></li> -->
            <!-- <li>Salary : <?php if ($emp_sallary>0) { echo $emp_sallary; } ?></li> -->
            <li>Status :
              <?php if ($entry_status==1) {
              echo "<span class='badge badge-success m-1'>Available</span>";
              } elseif ($entry_status==2) {
              echo "<span class='badge badge-danger  m-1'>Not Available</span>";
              }  else {
              echo "<span class='badge badge-info  m-1'>No Data</span>";
              }?>
            </li>
            <?php if($device_mac!='' && $emp_type==0){ ?>
            <li><i class="fa fa-lock"></i> Phone Lock:
              <?php
              if($device_lock=="0"){
              ?>
              <input type="checkbox" checked  data-color="#15ca20" onchange ="changeStatus('<?php echo $emp_id; ?>','phoneUnlock');" data-size="small"/>
              <?php } else { ?>
              <input type="checkbox"   data-color="#15ca20" onchange ="changeStatus('<?php echo $emp_id; ?>','phoneLock');" data-size="small"/>
              <?php } ?>
            </li>
            <!-- <li>MAC: <?php echo $device_mac; ?> </li> -->
            <?php } ?>
          </ul>
          <ul  class="list-inline text-center">
            <li class="list-inline-item">
              <?php if($emp_id_proof!='') { ?>
              <a title="View ID Proof" target="_blank" href="../img/emp_id/<?php echo $emp_id_proof;?>" name="Id Proof" class="btn btn-warning btn-sm"><i class="fa fa-id-card-o"></i></a>
              <?php } ?>
            </li>
            <li class="list-inline-item">
              <form title="Edit" action="employee" method="post">
                <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
                <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                <button type="submit" name="updateemployee" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></button>
              </form>
            </li>
            <li class="list-inline-item">
              <form title="Delete" action="controller/employeeController.php" method="post"  >
                <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
                <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                <input type="hidden" value="deleteemployee" name="deleteemployee">
                <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i></button>
              </form>
            </li>
            <li class="list-inline-item">
              <a title="Print I-Card" href="printIcardSingle.php?emp_id=<?php echo $emp_id; ?>" class="btn btn-info btn-sm"><i class="fa fa-print"></i> </a>
            </li>
            <?php if($device_mac!='' && $emp_type==0){ ?>
            <li class="list-inline-item">
              <form title="Logout From App" action="controller/employeeController.php" method="post" class="mt-4">
                <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
                <input type="hidden" name="emp_token" value="<?php echo $emp_token; ?>">
                <input type="hidden" name="emp_logout" value="emp_logout">
                <input type="hidden" name="emp_id" value="<?php echo $row['emp_id']; ?>">
                <button type="submit" class="btn btn-sm form-btn btn-warning" ><i class="fa fa-power-off" aria-hidden="true"></i></button>
              </form>
            </li>
            <?php } ?>
            <?php //IS_1498
            if($emp_type_id != 0){?>
            <li class="list-inline-item">
              <form title="Manage" action="employee" method="post" class="mt-4">
                <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                <input type="hidden" name="manageEmployeeUnit" value="yes">
                <button type="submit" name="manageEmployeeUnitBtn" class="btn btn-secondary  btn-sm"><i class="fa fa-home"></i> Units: <?php echo $d->count_data_direct("employee_unit_id","employee_unit_master","emp_id='$emp_id'"); ?></button>
              </form>
              
            </li>
            <?php } else if($emp_type == 0){?>
                    <li class="list-inline-item">
                    <form title="Eye Attendance" action="getEyeStatus" method="post" class="mt-4">
                      <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                      <input type="hidden" name="manageEmployeeEye" value="yes">
                      <button type="submit" name="manageEmployeeUnitBtn" class="btn btn-danger  btn-sm"><i class="fa fa-eye"></i> Eye Attendance</button>
                    </form>
                  </li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } } else {
echo "<img src='img/no_data_found.png'>";
} }?>
<!-- <script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/switchery/js/switchery.min.js"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script>
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
$('.js-switch').each(function() {
new Switchery($(this)[0], $(this).data());
});
</script> -->