

<style>
  input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>
<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>

<?php
$q1 = $d->selectRow('*', "society_master", "society_id='$society_id'");
$data2 = mysqli_fetch_assoc($q1);

$user_latitude = $data2['society_latitude'];
$user_longitude = $data2['society_longitude'];
extract(array_map("test_input", $_POST));
if (isset($edit_company_current_opening)) {
    $q = $d->select("company_current_opening_master","company_current_opening_id='$company_current_opening_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
    $scheduleArr = json_decode($data['company_current_opening_schedule']);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Company Current Opening</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addCompanyCurrentOpeningFrom" action="controller/CompanyCurrentOpeningController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Opening Title <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Opening Title" name="company_current_opening_title" value="<?php if($data['company_current_opening_title'] !=""){ echo $data['company_current_opening_title']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Opening Position <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select class="form-control single-select" name="company_current_opening_position">
                                        <option value="">-- Select Opening Position --</option>
                                        <option <?php if($data['company_current_opening_position']  == "1"){ echo 'selected'; } ?> value="1">1</option>
                                        <option <?php if($data['company_current_opening_position']  == "2"){ echo 'selected'; } ?> value="2">2</option>
                                        <option <?php if($data['company_current_opening_position']  == "3"){ echo 'selected'; } ?> value="3">3</option>
                                        <option <?php if($data['company_current_opening_position']  == "4"){ echo 'selected'; } ?> value="4">4</option>
                                        <option <?php if($data['company_current_opening_position']  == "5"){ echo 'selected'; } ?> value="5">5</option>
                                        <option <?php if($data['company_current_opening_position']  == "6"){ echo 'selected'; } ?> value="6">6</option>
                                        <option <?php if($data['company_current_opening_position']  == "7"){ echo 'selected'; } ?> value="7">7</option>
                                        <option <?php if($data['company_current_opening_position']  == "8"){ echo 'selected'; } ?> value="8">8</option>
                                        <option <?php if($data['company_current_opening_position']  == "9"){ echo 'selected'; } ?> value="9">9</option>
                                        <option <?php if($data['company_current_opening_position']  == "10"){ echo 'selected'; } ?> value="10">10</option>
                                        <option <?php if($data['company_current_opening_position']  == "10+"){ echo 'selected'; } ?> value="10+">10+</option>
                                        <option <?php if($data['company_current_opening_position']  == "20+"){ echo 'selected'; } ?> value="20+">20+</option>
                                        <option <?php if($data['company_current_opening_position']  == "30+"){ echo 'selected'; } ?> value="30+">30+</option>
                                        <option <?php if($data['company_current_opening_position']  == "40+"){ echo 'selected'; } ?> value="40+">40+</option>
                                        <option <?php if($data['company_current_opening_position']  == "50+"){ echo 'selected'; } ?> value="50+">50+</option>
                                        <option <?php if($data['company_current_opening_position']  == "I have an ongoing need to fill this role"){ echo 'selected'; } ?> value="I have an ongoing need to fill this role">I have an ongoing need to fill this role</option>
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Job Type <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select class="form-control single-select" name="company_current_opening_timing">
                                        <option value="">-- Select Job Type --</option>
                                        <option value="Full Time" <?php if($data['company_current_opening_timing']  == "Full Time"){ echo 'selected'; } ?>>Full Time</option>
                                        <option value="Part Time" <?php if($data['company_current_opening_timing']  == "Part Time"){ echo 'selected'; } ?>>Part Time</option>
                                        <option value="Regular / Permanent" <?php if($data['company_current_opening_timing']  == "Regular / Permanent"){ echo 'selected'; } ?>>Regular / Permanent</option>
                                        <option value="Contractual / Temporary" <?php if($data['company_current_opening_timing']  == "Contractual / Temporary"){ echo 'selected'; } ?>>Contractual / Temporary</option>
                                        <option value="Freelance" <?php if($data['company_current_opening_timing']  == "Freelance"){ echo 'selected'; } ?>>Freelance</option>
                                        <option value="Volunteer" <?php if($data['company_current_opening_timing']  == "Volunteer"){ echo 'selected'; } ?>>Volunteer</option>
                                        <option value="Internship" <?php if($data['company_current_opening_timing']  == "Internship"){ echo 'selected'; } ?>>Internship</option>
                                        <option value="Fresher" <?php if($data['company_current_opening_timing']  == "Fresher"){ echo 'selected'; } ?>>Fresher</option>
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Schedule <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select class="form-control multiple-select" multiple name="company_current_opening_schedule[]">
                                        <option value="">-- Select Type --</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Morning shift', $scheduleArr)){echo 'selected'; } ?> value="Morning shift">Morning shift</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Day shift', $scheduleArr)){echo 'selected'; } ?> value="Day shift">Day shift</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Evening shift', $scheduleArr)){echo 'selected'; } ?> value="Evening shift">Evening shift</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Night shift', $scheduleArr)){echo 'selected'; } ?> value="Night shift">Night shift</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Flexible shift', $scheduleArr)){echo 'selected'; } ?> value="Flexible shift">Flexible shift</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Rotational shift', $scheduleArr)){echo 'selected'; } ?> value="Rotational shift">Rotational shift</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Fixed shift', $scheduleArr)){echo 'selected'; } ?> value="Fixed shift">Fixed shift</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Monday to Friday', $scheduleArr)){echo 'selected'; } ?> value="Monday to Friday">Monday to Friday</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Weekend availability', $scheduleArr)){echo 'selected'; } ?> value="Weekend availability">Weekend availability</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Weekend only', $scheduleArr)){echo 'selected'; } ?> value="Weekend only">Weekend only</option>
                                        <option <?php if($company_current_opening_schedule && in_array('UK shift', $scheduleArr)){echo 'selected'; } ?> value="UK shift">UK shift</option>
                                        <option <?php if($company_current_opening_schedule && in_array('US shift', $scheduleArr)){echo 'selected'; } ?> value="US shift">US shift</option>
                                        <option <?php if($company_current_opening_schedule && in_array('Other', $scheduleArr)){echo 'selected'; } ?> value="Other">Other</option>
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Location <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Location" id="company_current_opening_address" name="company_current_opening_address" value="<?php if($data['company_current_opening_address'] !=""){ echo $data['company_current_opening_address']; }else{ echo $data2['city_name']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Pay Scale <span class="required">*</span></label>
                                <div class="col-lg-6 col-md-6 col-6" id="">
                                    <input type="text" class="form-control" placeholder="Pay Scale" id="company_current_opening_pay_scale" name="company_current_opening_pay_scale" value="<?php if($data['company_current_opening_pay_scale'] !=""){ echo $data['company_current_opening_pay_scale']; } ?>">
                                </div>
                                <div class="col-lg-6 col-md-6 col-6" id="">
                                    <select class="form-control" name="company_current_opening_salary_period">
                                        <option <?php if($data['company_current_opening_salary_period']  == "Per Month"){ echo 'selected'; } ?> value="Per Month">Per Month</option>
                                        <option <?php if($data['company_current_opening_salary_period']  == "Per Year"){ echo 'selected'; } ?> value="Per Year">Per Year</option>
                                        <option <?php if($data['company_current_opening_salary_period']  == "Per Hour"){ echo 'selected'; } ?> value="Per Hour">Per Hour</option>
                                        <option <?php if($data['company_current_opening_salary_period']  == "Per Day"){ echo 'selected'; } ?> value="Per Day">Per Day</option>
                                        <option <?php if($data['company_current_opening_salary_period']  == "Per Week"){ echo 'selected'; } ?> value="Per Week">Per Week</option>
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Experience <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Experience" id="company_current_opening_experience" name="company_current_opening_experience" value="<?php if($data['company_current_opening_experience'] !=""){ echo $data['company_current_opening_experience']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Opening Description <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                <textarea id="summernoteImgage" name="company_current_opening_description" class="form-control "><?php if($data['company_current_opening_description'] !=""){ echo $data['company_current_opening_description']; } ?></textarea>
                                </div>
                            </div> 
                        </div>
                    </div>    
                                
                    <div class="form-footer text-center">
                    <?php //IS_1019 addPenaltiesBtn 
                    if (isset($edit_company_current_opening)) {                    
                    ?>
                    <input type="hidden" id="company_current_opening_id" name="company_current_opening_id" value="<?php if($data['company_current_opening_id'] !=""){ echo $data['company_current_opening_id']; } ?>" >
                    <button id="addCompanyCurrentOpeningBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addCompanyCurrentOpening"  value="addCompanyCurrentOpening">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                        <?php }
                        else {
                        ?>
                    
                    <button id="addCompanyCurrentOpeningBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addCompanyCurrentOpening"  value="addCompanyCurrentOpening">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyCurrentOpeningFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
