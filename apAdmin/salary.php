<?php
error_reporting(0);
/* $currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year')); */

$uId = (int)$_REQUEST['uId'];
$dId = (int)$_REQUEST['dId'];
$bId = (int)$_REQUEST['bId'];
$gId = (int)$_REQUEST['gId'];
$increment = (int) $_REQUEST['increment'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-3 col-md-6 col-6">
        <h4 class="page-title">Employee CTC</h4>
      </div>
      <div class="col-sm-3 col-md-6 col-6">
        <div class="btn-group float-sm-right">
          <a href="addSalary" onclick="buttonSetting()" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
          <a href="javascript:void(0)" data-toggle="modal" data-target="#bulkUpload" class="btn  mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Bulk Add </a>
          <a href="javascript:void(0)" onclick="DeleteAll('deletesalary');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
      </div>
    </div>
    <form action="" class="branchDeptFilter">
      <div class="row pt-2 pb-2">
        <div class="col-md-2 form-group ">
            <select  id="gId" required name="gId"  onchange="this.form.submit()" class="form-control single-salary-group" >
               <option value="">Select Group</option>
               <?php 
                
                  $qs=$d->select("salary_group_master","");
                
               while($sgData=mysqli_fetch_array($qs)) { ?>
               <option <?php if(isset($sgData) && $sgData['salary_group_id']==$gId ){echo "selected"; }?> value="<?php echo $sgData['salary_group_id'];?>"><?php echo $sgData['salary_group_name'];?> (S<?php echo $sgData['salary_group_id'];?>)</option>
               <?php } ?>
             </select>
        </div>
        <?php include('selectBranchDeptEmpForFilter.php'); ?>
        <div class="col-md-1 form-group ">
          <input class="btn btn-success btn-sm " type="submit" name="getReport" value="Get Data">
        </div>
      </div>
    </form>
    <?php if (isset($gId) && $gId > 0) { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Sr.No</th>
                    <th>View</th>
                    <th>Action</th>
                    <th>Incremet</th>
                    <th>Department</th>
                    <th>Employee</th>
                    <th>type</th>
                    <th>Net Salary</th>
                    <th>Gross Salary</th>
                    <th>CTC Monthly</th>
                    <th>CTC Annum</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Next Increment</th>
                    <th>Salary Group</th>
                  </tr>
                </thead>
                  <tbody>
                    <?php
                    $i = 1;

                    if (isset($dId) && $dId > 0) {
                      $deptFilterQuery = " AND users_master.floor_id='$dId'";
                    }
                    if (isset($uId) && $uId > 0) {
                      $UserFilterQuery = " AND salary.user_id='$uId'";
                    }
                    if (isset($gId) && $gId > 0) {
                      $salaryGroupFilter = " AND salary.salary_group_id='$gId'";
                    }

                    $q = $d->selectRow("salary.*,salary_group_master.salary_group_name,floors_master.floor_name,users_master.*","salary LEFT JOIN salary_group_master ON salary_group_master.salary_group_id=salary.salary_group_id,users_master,floors_master", "salary.user_id=users_master.user_id AND salary.floor_id=floors_master.floor_id AND salary.society_id='$society_id'  AND salary.is_delete='0' AND users_master.delete_status=0 $salaryGroupFilter $deptFilterQuery  $UserFilterQuery $blockAppendQueryUser", "ORDER BY salary.salary_id DESC");
                    $counter = 1;

                    while ($data = mysqli_fetch_array($q)) {
                     /// print_r($data);
                    ?>

                      <tr>
                        <td class="text-center">
                          <?php  $totalSalary = $d->count_data_direct("salary_id", "salary_slip_master", "salary_id='$data[salary_id]'");
                          if ($totalSalary > 0) { 
                            $disbleApply = "disabled";
                          } else {
                            $disbleApply ="";
                          }
                          ?>
                            <input type="hidden" name="id" id="id" value="<?php echo $data['salary_id']; ?>">
                            <input type="checkbox" <?php echo $disbleApply;?> name="" class="multiDelteCheckbox" value="<?php echo $data['salary_id']; ?>">
                         
                        </td>
                        
                        <td><?php echo $counter++; ?></td>
                        <td> 
                          <!-- <button type="button" class="btn btn-primary btn-sm salary_Modal" data-id="<?php echo $data['salary_id']; ?>">View</button> -->
                        <a href="<?php echo $base_url.'apAdmin/employeeSalaryDetail?sId='.$data['salary_id']; ?>"><button class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> view</button></a>
                      </td>
                        <td>
                          <div class="d-flex align-items-center">
                            <?php if ($data['is_preivous_salary'] != 1) { ?>
                            <a href="addSalary?sid=<?php echo $data['salary_id']; ?>">
                              <button  type="submit" class="btn btn-sm btn-warning mr-2"><i class="fa fa-pencil"></i></button>
                            </a>
                            <?php } ?>
                            
                          </div>
                        </td>
                        <td>
                          <div class="d-flex align-items-center">
                            <?php if($data['is_preivous_salary'] !=1 ){ ?>
                            <a href="addSalary?is_increment=1&sid=<?php echo $data['salary_id']; ?>">
                              <button type="submit" class="btn btn-sm btn-primary mr-2"><i class="fa fa-plus"></i></button>
                              </a>
                              <?php } else {?>

                                <sapn class="badge badge-warning">Previous</sapn>
                              <?php }?>
                          </div>
                        </td>
                        <td><?php echo $data['floor_name']; ?></td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php 
                          if($data['salary_type']=="0"){
                            $typ = "Fixed";
                          }else if($data['salary_type']=="1")
                          {
                            $typ = "Per Day";
                          }else
                          {
                            $typ = "Per Hour";
                          }
                        ?><span class="badge badge-info"><?php echo $typ; ?></span></td>
                        <td><?php echo $data['net_salary']; ?></td>
                        <td><?php echo $data['gross_salary']; ?></td>
                        <td><?php echo number_format((float)($data['total_ctc']), 2, '.', ''); ?></td>
                        <td><?php echo number_format((float)($data['total_ctc']*12), 2, '.', ''); ?></td>
                        <td><?php if ($data['salary_start_date'] != "0000-00-00" && $data['salary_start_date'] != null) {
                              echo date("d M Y ", strtotime($data['salary_start_date']));
                            } ?></td>
                        <td><?php if ($data['salary_end_date'] != "0000-00-00" && $data['salary_end_date'] != null) {
                              echo date("d M Y ", strtotime($data['salary_end_date']));
                            } ?></td>
                        <td><?php if (
                          $data['salary_increment_date'] != "0000-00-00" && $data['salary_increment_date'] != null) {
                              echo date("d M Y ", strtotime($data['salary_increment_date']));
                            } ?></td>
                        <td><?php echo $data['salary_group_name'];?></td>
                        
                       
                      </tr>
                    <?php } ?>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->
    <?php } else { 
      ?>
       <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <span> NOTE : Please select salary group</span>
            </div>
          </div>
        </div>
       </div>
      <?php } ?>
  </div>
</div>



<div class="modal fade" id="salaryModal">
  <div class="modal-dialog" style="max-width: 90%">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Salary CTC Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div id="salaryData"></div>
        <div class="col-md-12 row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Earning</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody id="earnData">
                    </tbody>
                  </table>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th> Deduction Name</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody id="deductData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 text-center">
                  <label for="exampleInputEmail1">Net Salary : </label>
                    <span id="netSalary"></span>
                  </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="bulkUpload">
    <div class="modal-dialog">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white"><?php echo $xml->string->import; ?> <?php echo $xml->string->bulk; ?> <?php echo $xml->string->parking; ?></h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="groupExportForm" action="controller/BulkSalaryController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 1 -> Download CSV After Selecting Salary Group</label>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label>Salary Group</label>
                        </div>
                        <div class="col-sm-9">
                            <select class="form-control single-select" name="salary_group_id" id="salary_group_id">
                                <option value="">--Select--</option>
                                <?php
                                $q = $d->selectRow("salary_group_id,salary_group_name","salary_group_master","active_status = 0");
                                while($gData = $q->fetch_assoc())
                                {
                                ?>
                                <option <?php if(isset($_GET['gId']) && $_GET['gId'] == $gData['salary_group_id']){ echo "selected"; } ?> value="<?php echo $gData['salary_group_id']; ?>"><?php echo $gData['salary_group_name']; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="hidden" name="exportGroupData" value="exportGroupData"/>
                            <button type="submit" class="btn btn-sm btn-primary">Download</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 2 -> <?php echo $xml->string->fill_your_data; ?> </label>
                        <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 3 -> <?php echo $xml->string->import_file; ?></label>
                        <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 4 -> <?php echo $xml->string->click_upload_btn; ?></label>
                        <label for="input-10" class="col-sm-12 col-form-label text-danger"> NOTE: DON'T CHANGE COLUMN FROM CSV </label>
                        <label for="input-10" class="col-sm-12 col-form-label text-danger"> NOTE: Please add date in yyyy-mm-dd Format Only in Salary Start Date & Next Increament Date. Ex(2022-12-18) </label>
                        <label for="input-10" class="col-sm-12 col-form-label text-danger"> Salary Type -> 0 = Fixed, 1 = Per Day, 2 = Per Hour </label>
                    </div>
                </form>
                <form id="importValidation" action="controller/BulkSalaryController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->import; ?> CSV <?php echo $xml->string->file; ?> <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <input required type="file" name="file" accept=".csv" class="form-control-file border">
                        </div>
                    </div>
                    <div class="form-footer text-center">
                        <input type="hidden" name="importCTCBulk" value="importCTCBulk">
                        <button type="button" onclick="checkValidation();" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->upload; ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  //////////////////////holiday data
  function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
    }
    return false;
  }

  function checkValidation()
    {
        if($('#importValidation').valid())
        {
            var salary_group_id = $('#salary_group_id').val();
            if(salary_group_id == "")
            {
                swal("Error! Please Select Salary Group First !", {icon: "error",});
            }
            else
            {
                $('#importValidation').append('<input type="hidden" name="salary_group_id" value="'+salary_group_id+'"/>');
                $('#importValidation').submit();
            }
        }
        else
        {
            $('#importValidation').validate();
        }
    }

</script>