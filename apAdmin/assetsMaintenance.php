<?php 
$id = (int)$_GET['id'];
$mt = $_GET['mt'];
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<h4 class="page-title">Assets Maintenance</h4>
			</div>
			
			<div class="col-lg-6 col-md-6">
				<div class="btn-group float-sm-right">
				<a href="addAssetsMaintenance" class="btn mr-1 btn-sm btn-primary waves-effect waves-light">
					+ADD
				</a>
				<a href="javascript:void(0)" onclick="DeleteAll('deleteAssetsMaintenance');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
				</div>
			</div>
		</div>

		<form class="branchDeptFilter" action="" method="get">
			<div class="row pt-2 pb-2">
				<div class="col-md-3 form-group" >
					<select type="text" name="id" id="id" class="form-control single-select" style="width: 100%">
						<option value="">All Category</option>
						<?php $q12 = $d->select("assets_category_master", "", "order by assets_category ASC");
						while ($row12 = mysqli_fetch_array($q12)) { ?>
						<option value="<?php echo $row12['assets_category_id']; ?>" <?php if ($id == $row12['assets_category_id']) {echo "selected";} ?>><?php echo $row12['assets_category']; ?> </option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3 form-group" >
					<select class="form-control single-select" name="mt">
						<option <?php if(isset($mt) && $mt == 'all'){echo 'selected';} ?> value="all">All Maintenance Type</option>
						<option <?php if(isset($mt) && $mt == '0'){echo 'selected';} ?> value="0">Custom Date</option>
						<option <?php if(isset($mt) && $mt == '1'){echo 'selected';} ?> value="1">Weekly</option>
						<option <?php if(isset($mt) && $mt == '2'){echo 'selected';} ?> value="2">Month Days</option>
						<option <?php if(isset($mt) && $mt == '3'){echo 'selected';} ?> value="3">Monthly</option>
						<option <?php if(isset($mt) && $mt == '4'){echo 'selected';} ?> value="4">Quarterly</option>
						<option <?php if(isset($mt) && $mt == '5'){echo 'selected';} ?> value="5">Half Yearly</option>
						<option <?php if(isset($mt) && $mt == '6'){echo 'selected';} ?> value="6">Yearly</option>
					</select>
				</div>
				<div class="col-md-3 form-group">
					<input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-bordered">
								<thead>
									<tr>
										<th>S. No</th>
										<th>#</th>
										<th>Category</th>
										<th>Item Name</th>
										<th>Maintenance Type</th>
										<th>Maintenance Day</th>
										<th>Vendor Name</th>
										<th>Vendor Mobile</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i1 = 1;
									if (isset($id) && $id > 0) {
										$appendQery = " AND assets_item_detail_master.assets_category_id='$id'";
									}
									if (isset($mt) && $mt != 'all') {
										$typeAppendQery = " AND assets_maintenance_master.maintenance_type='$mt'";
									}
									
									$q = $d->select("assets_category_master,assets_item_detail_master,assets_maintenance_master","assets_category_master.assets_category_id=assets_maintenance_master.assets_category_id AND assets_item_detail_master.assets_id=assets_maintenance_master.assets_id AND assets_maintenance_master.society_id='$society_id' $appendQery $typeAppendQery");
									while ($row = mysqli_fetch_array($q)) {
									?>
									<tr>
										<td><?php echo $i1++; ?> </td>
										<td class="text-center">
											<input type="hidden" name="id" id="id" value="<?php echo $row['assets_maintenance_id']; ?>">                  
											<input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $row['assets_maintenance_id']; ?>">                      
										</td>
										<td><?php echo $row['assets_category']; ?> </td>
										<td><?php echo $row['assets_name']; ?> </td>
										<td><?php if($row['maintenance_type'] == 0){
													echo 'Custom Date';
												}
												elseif($row['maintenance_type'] == 1){
													echo 'Weekly';
												}
												elseif($row['maintenance_type'] == 2){
													echo 'Month Days';
												}
												elseif($row['maintenance_type'] == 3){
													echo 'Monthly';
												}
												elseif($row['maintenance_type'] == 4){
													echo 'Quarterly';
												}
												elseif($row['maintenance_type'] == 5){
													echo 'Half Yearly';
												}
												elseif($row['maintenance_type'] == 6){
													echo 'Yearly';
												}
										 	?>
										</td>
										<td>
											<?php if($row['maintenance_type'] == 0){
												echo date("d F Y", strtotime($row['custom_date']));
											}elseif($row['maintenance_type'] == 1){
												$date = date('Y-m-d');
												$week_days = explode(',',$row['days']);
												$days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
												for ($i = 0; $i <= 6; $i++) {
													if (in_array($i, $week_days)) {
														echo $days[$i].' ';
													}
												}
											}elseif($row['maintenance_type'] == 2){
												$month = date('F');
												$month_days = explode(',',$row['days']);
												for ($i = 0; $i < count($month_days); $i++) {
													echo $month.' '.$month_days[$i].' ';
												}
											}elseif($row['maintenance_type'] == 3){
												$month = date('F');
												echo $month.' '.$row['days'];
											}elseif($row['maintenance_type'] == 4){
												$date = date('Y').'-'.$row['start_month'].'-'.$row['days'];
												$month = date('Y-m-d');
												if(date('Y-m-d') <= date('Y-m-d', strtotime($date))){
													$monthNum  = $row['start_month'];
													$dateObj   = DateTime::createFromFormat('!m', $monthNum);
													$monthName = $dateObj->format('F');
													echo $monthName.' '.$row['days'];
												}else{
												 	$newDate = date('m', strtotime($date. ' + 3 months'));
													$monthNum  = $newDate;
													$dateObj   = DateTime::createFromFormat('!m', $monthNum);
													$monthName = $dateObj->format('F');
													echo $monthName.' '.$row['days'];
												}
											}elseif($row['maintenance_type'] == 5){
												$date = date('Y').'-'.$row['start_month'].'-'.$row['days'];
												$month = date('Y-m-d');
												if(date('Y-m-d') <= date('Y-m-d', strtotime($date))){
													$monthNum  = $row['start_month'];
													$dateObj   = DateTime::createFromFormat('!m', $monthNum);
													$monthName = $dateObj->format('F');
													echo $monthName.' '.$row['days'];
												}else{
												 	$newDate = date('m', strtotime($date. ' + 6 months'));
													$monthNum  = $newDate;
													$dateObj   = DateTime::createFromFormat('!m', $monthNum);
													$monthName = $dateObj->format('F');
													echo $monthName.' '.$row['days'];
												}
											}elseif($row['maintenance_type'] == 6){
												$monthNum  = $row['start_month'];
												$dateObj   = DateTime::createFromFormat('!m', $monthNum);
												$monthName = $dateObj->format('F');
												echo $monthName.' '.$row['days'];
												
												
											} ?>
										</td>
										<td><?php echo $row['vendor_name']; ?> </td>
										<td><?php echo $row['vendor_mobile_no']; ?> </td>
										<td>
											<div class="d-flex align-items-center">
												<form action="addAssetsMaintenance" method="POST">
													<input type="hidden" name="assets_maintenance_id" value="<?php echo $row['assets_maintenance_id']; ?>">
													<input type="hidden" name="edit_assets_maintenance" value="edit_assets_maintenance">
													<button class="btn btn-primary btn-sm " title="Summary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
												</form>
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>