
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-4 col-6">
                <h4 class="page-title">Statistics & Reports</h4>
            </div>
            <div class="col-sm-8 col-6">
                <form action="" class="branchDeptFilter">
                    <div class="row ">
                      <div class="col-md-6 col-6 form-group">
                        <select class="form-control single-select-new" id="user_id" name="user_id" required onchange="checkEmpRoute();">
                            <option value="">-- Select Employee--</option>
                            <?php
                                $qu = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","um.delete_status = 0 AND um.active_status = 0");
                                while ($ud = $qu->fetch_assoc())
                                {
                                ?>
                            <option <?php if($user_id == $ud['user_id']) { echo 'selected'; } ?> value="<?php echo $ud['user_id']; ?>"><?php echo $ud['user_full_name'] . " (" . $ud['block_name'] . " - " . $ud['floor_name'] . ")"; ?></option>
                            <?php
                                }
                            ?>
                        </select>
                      </div>
                      <div class="col-md-6 col-6 form-group">
                             <input  type="text" required=""  class="form-control" autocomplete="off" id="default-datepicker"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
                      </div>          
                      
                    </div>
                  </form>
            </div>
        </div>
        
        <!-- End Breadcrumb-->
        <div class="row">
            
            <div class="col-lg-12">
                <div class="row ">
                    <div class="col-12 col-lg-6 col-xl-3">
                      <div class="card gradient-ibiza">
                        <div class="card-body">
                          <div class="media align-items-center">
                          <div class="media-body">
                             <p class="text-white">Total Orders</p>
                            <h4 class="text-white line-height-5">85,297</h4>
                          </div>
                          <div class=""><span id="dashboard2-chart-1"><canvas width="135" height="40" style="display: inline-block; width: 135px; height: 40px; vertical-align: top;"></canvas></span></div>
                        </div>
                        </div>
                        <div class="card-footer border-light-2">
                          <p class="mb-0 text-white">
                                <span class="mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                <span class="text-nowrap">Since last month</span>
                              </p>
                        </div>
                      </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl-3">
                      <div class="card gradient-branding">
                        <div class="card-body">
                          <div class="media align-items-center">
                          <div class="media-body">
                             <p class="text-white">Order Revenue</p>
                            <h4 class="text-white line-height-5">$4897</h4>
                          </div>
                          <div class=""><span id="dashboard2-chart-2"><canvas width="135" height="40" style="display: inline-block; width: 135px; height: 40px; vertical-align: top;"></canvas></span></div>
                        </div>
                        </div>
                        <div class="card-footer border-light-2">
                          <p class="mb-0 text-white">
                                <span class="mr-2"><i class="fa fa-arrow-up"></i> 5.28%</span>
                                <span class="text-nowrap">Since last month</span>
                              </p>
                        </div>
                      </div>
                    </div>

                    <div class="col-12 col-lg-12 col-xl-3">
                      <div class="card gradient-deepblue">
                        <div class="card-body">
                          <div class="media align-items-center">
                          <div class="media-body">
                             <p class="text-white">Order quantities</p>
                            <h4 class="text-white line-height-5">6,500</h4>
                          </div>
                          <div class=""><span id="dashboard2-chart-3"><canvas width="135" height="40" style="display: inline-block; width: 135px; height: 40px; vertical-align: top;"></canvas></span></div>
                        </div>
                        </div>
                        <div class="card-footer border-light-2">
                          <p class="mb-0 text-white">
                                <span class="mr-2"><i class="fa fa-arrow-up"></i> 1.48%</span>
                                <span class="text-nowrap">Since last week</span>
                              </p>
                        </div>
                      </div>
                    </div>

                    <div class="col-12 col-lg-6 col-xl-3">
                      <div class="card gradient-ibiza">
                        <div class="card-body">
                          <div class="media align-items-center">
                          <div class="media-body">
                             <p class="text-white">Productivity</p>
                            <h4 class="text-white line-height-5">85,297</h4>
                          </div>
                          <div class=""><span id="dashboard2-chart-1"><canvas width="135" height="40" style="display: inline-block; width: 135px; height: 40px; vertical-align: top;"></canvas></span></div>
                        </div>
                        </div>
                        <div class="card-footer border-light-2">
                          <p class="mb-0 text-white">
                                <span class="mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                <span class="text-nowrap">Since last month</span>
                              </p>
                        </div>
                      </div>
                    </div>
                    
                  </div>
            </div>

            <div class="col-lg-6">
               <div class="card">
                  <div class="card-body"> 
                    <ul class="nav nav-tabs nav-tabs-primary">
                      <li class="nav-item">
                        <a class="nav-link active show" data-toggle="tab" href="#tabe-1">Today</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#tabe-2">This Week</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#tabe-3">This Month</a>
                      </li>
                     
                      
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div id="tabe-1" class="container tab-pane  active show">
                        <div class="table-responsive">
         
                             <table class="table align-items-center table-flush">
                              <thead>
                               <tr>
                                 <th>Product</th>
                                 <th>Photo</th>
                                 <th>Product ID</th>
                                 <th>Status</th>
                                 <th>Amount</th>
                                 <th>Completion</th>
                               </tr>
                               </thead>
                               <tbody><tr>
                               
                                <td>Iphone 5</td>
                                <td><img src="assets/images/products/01.png" class="product-img" alt="product img"></td>
                                <td>#9405822</td>
                                <td><span class="btn btn-sm btn-outline-success btn-round btn-block">Paid</span></td>
                                <td>$ 1250.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-ohhappiness" role="progressbar" style="width: 100%"></div>
                                   </div></td>
                               </tr>

                               <tr>
                               
                                <td>Earphone GL</td>
                                <td><img src="assets/images/products/02.png" class="product-img" alt="product img"></td>
                                <td>#9405820</td>
                                <td><span class="btn btn-sm btn-outline-info btn-round btn-block">Pending</span></td>
                                <td>$ 1500.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-scooter" role="progressbar" style="width: 80%"></div>
                                   </div></td>
                               </tr>

                               <tr>
                               
                                <td>HD Hand Camera</td>
                                <td><img src="assets/images/products/03.png" class="product-img" alt="product img"></td>
                                <td>#9405830</td>
                                <td><span class="btn btn-sm btn-outline-danger btn-round btn-block">Failed</span></td>
                                <td>$ 1400.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-ibiza" role="progressbar" style="width: 60%"></div>
                                   </div></td>
                               </tr>

                               <tr>
                               
                                <td>Clasic Shoes</td>
                                <td><img src="assets/images/products/04.png" class="product-img" alt="product img"></td>
                                <td>#9405825</td>
                                <td><span class="btn btn-sm btn-outline-success btn-round btn-block">Paid</span></td>
                                <td>$ 1200.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-ohhappiness" role="progressbar" style="width: 100%"></div>
                                   </div></td>
                               </tr>

                               <tr>
                               
                                <td>Hand Watch</td>
                                <td><img src="assets/images/products/05.png" class="product-img" alt="product img"></td>
                                <td>#9405840</td>
                                <td><span class="btn btn-sm btn-outline-danger btn-round btn-block">Failed</span></td>
                                <td>$ 1800.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-ibiza" role="progressbar" style="width: 75%"></div>
                                   </div></td>
                               </tr>

                                <tr>
                                  
                                <td>HD Hand Camera</td>
                                <td><img src="assets/images/products/03.png" class="product-img" alt="product img"></td>
                                <td>#9405830</td>
                                <td><span class="btn btn-sm btn-outline-info btn-round btn-block">Pending</span></td>
                                <td>$ 1400.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-scooter" role="progressbar" style="width: 70%"></div>
                                   </div></td>
                               </tr>

                             </tbody></table>
                           </div>
                      </div>
                      <div id="tabe-2" class="container tab-pane fade">
                        
                      </div>
                      <div id="tabe-3" class="container tab-pane fade">
                       
                      </div>
                    </div>
                  </div>
               </div>

            </div>
            <div class="col-lg-6">
               <div class="card">
                  <div class="card-body"> 
                        <div id="ordeSumeryChart"></div>
                 </div>
                </div>
            </div>
        </div><!-- End Row-->
        <div class="row">
            <div class="col-lg-6">
               <div class="card">
                  <div class="card-body"> 
                    <ul class="nav nav-tabs nav-tabs-primary">
                      <li class="nav-item">
                        <a class="nav-link active show" data-toggle="tab" href="#tabe-1">Today</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#tabe-2">This Week</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#tabe-3">This Month</a>
                      </li>
                     
                      
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div id="tabe-1" class="container tab-pane  active show">
                        <div class="table-responsive">
         
                             <table class="table align-items-center table-flush">
                              <thead>
                               <tr>
                                 <th>Product</th>
                                 <th>Photo</th>
                                 <th>Product ID</th>
                                 <th>Status</th>
                                 <th>Amount</th>
                                 <th>Completion</th>
                               </tr>
                               </thead>
                               <tbody><tr>
                               
                                <td>Iphone 5</td>
                                <td><img src="assets/images/products/01.png" class="product-img" alt="product img"></td>
                                <td>#9405822</td>
                                <td><span class="btn btn-sm btn-outline-success btn-round btn-block">Paid</span></td>
                                <td>$ 1250.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-ohhappiness" role="progressbar" style="width: 100%"></div>
                                   </div></td>
                               </tr>

                               <tr>
                               
                                <td>Earphone GL</td>
                                <td><img src="assets/images/products/02.png" class="product-img" alt="product img"></td>
                                <td>#9405820</td>
                                <td><span class="btn btn-sm btn-outline-info btn-round btn-block">Pending</span></td>
                                <td>$ 1500.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-scooter" role="progressbar" style="width: 80%"></div>
                                   </div></td>
                               </tr>

                               <tr>
                               
                                <td>HD Hand Camera</td>
                                <td><img src="assets/images/products/03.png" class="product-img" alt="product img"></td>
                                <td>#9405830</td>
                                <td><span class="btn btn-sm btn-outline-danger btn-round btn-block">Failed</span></td>
                                <td>$ 1400.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-ibiza" role="progressbar" style="width: 60%"></div>
                                   </div></td>
                               </tr>

                               <tr>
                               
                                <td>Clasic Shoes</td>
                                <td><img src="assets/images/products/04.png" class="product-img" alt="product img"></td>
                                <td>#9405825</td>
                                <td><span class="btn btn-sm btn-outline-success btn-round btn-block">Paid</span></td>
                                <td>$ 1200.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-ohhappiness" role="progressbar" style="width: 100%"></div>
                                   </div></td>
                               </tr>

                               <tr>
                               
                                <td>Hand Watch</td>
                                <td><img src="assets/images/products/05.png" class="product-img" alt="product img"></td>
                                <td>#9405840</td>
                                <td><span class="btn btn-sm btn-outline-danger btn-round btn-block">Failed</span></td>
                                <td>$ 1800.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-ibiza" role="progressbar" style="width: 75%"></div>
                                   </div></td>
                               </tr>

                                <tr>
                                  
                                <td>HD Hand Camera</td>
                                <td><img src="assets/images/products/03.png" class="product-img" alt="product img"></td>
                                <td>#9405830</td>
                                <td><span class="btn btn-sm btn-outline-info btn-round btn-block">Pending</span></td>
                                <td>$ 1400.00</td>
                                <td><div class="progress shadow" style="height: 4px;">
                                      <div class="progress-bar gradient-scooter" role="progressbar" style="width: 70%"></div>
                                   </div></td>
                               </tr>

                             </tbody></table>
                           </div>
                      </div>
                      <div id="tabe-2" class="container tab-pane fade">
                        
                      </div>
                      <div id="tabe-3" class="container tab-pane fade">
                       
                      </div>
                    </div>   
                 </div>
                </div>
            </div>
            <div class="col-lg-6">
               <div class="card">
                  <div class="card-body"> 
                         <div id="prdouctPieChart"></div>
                 </div>
                </div>
            </div>
        </div><!-- End Row-->
        <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body"> 
                         <div id="monthlyChart"></div>
                 </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body"> 
                         <div id="YearlyChart"></div>
                 </div>
                </div>
            </div>
        </div>
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/plugins/apexcharts-bundle/js/apexcharts.min.js"></script>
<script>
    var options = {
          series: [{
          name: 'Order Amount',
          data: [44, 55, 57, 56, 61, 58, 63, 60, 66,20,40,5,44, 55, 57, 56, 61, 58, 63, 60, 66,0,0,5,44, 55, 57, 56, 61, 58, 63]
        }, {
          name: 'Quantity',
          data: [76, 85, 101, 98, 87, 105, 91, 114, 94,10,11,12,76, 85, 101, 98, 87, 105, 91, 114, 94,10,11,12,76, 85, 101, 98, 87, 105, 91]
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['1','2', '3', '4', '5', '6', '7', '8', '9', '10','11','12','13','14', '15', '16', '17', '18', '19', '20', '21', '22','23','24','25','26','27','28','29','30','31'],
        },
        yaxis: {
          title: {
            text: 'Daily Sales 2022'
          }
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return "$ " + val + " monthlyChart"
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#monthlyChart"), options);
        chart.render();
      

    var options = {
          series: [{
          name: 'Order Amount',
          data: [44, 55, 57, 56, 61, 58, 63, 60, 66,0,0]
        }, {
          name: 'Quantity',
          data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'],
        },
        yaxis: {
          title: {
            text: 'Monthly Sales 2022'
          }
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return "$ " + val + " thousands"
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#YearlyChart"), options);
        chart.render();
      
    var options = {
          series: [44, 55, 13, 43, 22],
          chart: {
          width: 480,
          type: 'pie',
        },
        labels: ['Team A', 'Team B', 'Team C', 'Team D', 'Team E'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#prdouctPieChart"), options);
        chart.render();

     var options = {
          series: [{
          name: 'Servings',
          data: [44, 55, 41, 67, 22, 43, 21, 33, 45, 31, 87, 65, 35]
        }],
          annotations: {
          points: [{
            x: 'Bananas',
            seriesIndex: 0,
            label: {
              borderColor: '#775DD0',
              offsetY: 0,
              style: {
                color: '#fff',
                background: '#775DD0',
              },
              text: 'Bananas are good',
            }
          }]
        },
        chart: {
          height: 350,
          type: 'bar',
        },
        plotOptions: {
          bar: {
            borderRadius: 10,
            columnWidth: '50%',
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: 2
        },
        
        grid: {
          row: {
            colors: ['#fff', '#f2f2f2']
          }
        },
        xaxis: {
          labels: {
            rotate: -45
          },
          categories: ['Apples', 'Oranges', 'Strawberries', 'Pineapples', 'Mangoes', 'Bananas',
            'Blackberries', 'Pears', 'Watermelons', 'Cherries', 'Pomegranates', 'Tangerines', 'Papayas'
          ],
          tickPlacement: 'on'
        },
        yaxis: {
          title: {
            text: 'orders summary',
          },
        },
        fill: {
          type: 'gradient',
          gradient: {
            shade: 'light',
            type: "horizontal",
            shadeIntensity: 0.25,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 0.85,
            opacityTo: 0.85,
            stops: [50, 0, 100]
          },
        }
        };

        var chart = new ApexCharts(document.querySelector("#ordeSumeryChart"), options);
        chart.render();

function getStateCity(country_id)
{
    $.ajax({
        url: 'controller/areaController.php',
        data: {getStateCityTag:"getStateCityTag",country_id:country_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#SCId').empty();
            response = JSON.parse(response);
            $('#SCId').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#SCId').append("<option value='"+value.city_id+"-"+value.state_id+"'>"+value.city_name+" - "+value.state_name+"</option>");
            });
        }
    });
}

function submitAddBtnForm()
{
    var country_id = $('#CId').val();
    var state_city_id = $('#SCId').val();
    var city_id = state_city_id.substr(0, state_city_id.indexOf('-'));
    var state_id = state_city_id.split("-")[1];
    if(country_id != "" && country_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'country_id',
            value: country_id
        }).appendTo('#addAreaBtnForm');
    }
    if(city_id != "" && city_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'city_id',
            value: city_id
        }).appendTo('#addAreaBtnForm');
    }if(state_id != "" && state_id != null)
    {
        $('<input>').attr({
            type: 'hidden',
            name: 'state_id',
            value: state_id
        }).appendTo('#addAreaBtnForm');
    }
    $("#addAreaBtnForm").submit()
}
</script>