<?php
error_reporting(0);
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$_GET['month_year'] = $_REQUEST['month_year'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row ">
            <div class="col-sm-9 col-12">
                <h4 class="page-title">Update Attendances</h4>
            </div>
        </div>
        <form action="" class="branchDeptFilter">
            <div class="row ">
                <div class="col-md-2 form-group">
                    <label class="form-control-label">Branch </label>
                    <select name="bId" class="form-control single-select" required="" onchange="getVisitorFloorByBlockId(this.value)">
                        <option value="">-- Select Branch --</option>
                        <?php
                        $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQuery");
                        while ($blockData = mysqli_fetch_array($qb)) {
                        ?>
                        <option <?php if ($_GET['bId'] == $blockData['block_id']) {
                            echo 'selected';
                        } ?> value="<?php echo $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label class="form-control-label">Department </label>
                    <select name="dId" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value)" required="">
                        <option value="">-- Select Department --</option>
                        <?php
                        $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id = '$_GET[bId]' $blockAppendQueryFloor");
                        while ($depaData = mysqli_fetch_array($qd)) {
                        ?>
                        <option <?php if ($_GET['dId'] == $depaData['floor_id']) {
                            echo 'selected';
                        } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2 form-group" id="">
                    <label class="form-control-label">Employee </label>
                    <select id="user_id" type="text" class="form-control single-select " name="user_id">
                        <option value="">Select Employee </option>
                        <?php
                        if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                        $userData = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND floor_id = $_GET[dId] $blockAppendQueryUser");
                        while ($user = mysqli_fetch_array($userData)) { ?>
                        <option <?php if (isset($_GET['user_id']) && $_GET['user_id'] == $user['user_id']) {echo "selected";} ?> value="<?php if (isset($user['user_id']) && $user['user_id'] != "") {echo $user['user_id'];} ?>"> <?php if (isset($user['user_full_name']) && $user['user_full_name'] != "") { echo $user['user_full_name'];} ?></option>
                        <?php }
                        } ?>
                    </select>
                </div>
                <div class="col-md-2 col-6 form-group">
                    <label class="form-control-label">From Date </label>
                    <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" name="from" value="<?php if (isset($_GET['from']) && $_GET['from'] != '') {echo $_GET['from']; } else { echo date('Y-m-d', strtotime('-30 days'));} ?>">
                </div>
                <div class="col-md-2 col-6 form-group">
                    <label class="form-control-label">To Date </label>
                    <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerTo" name="toDate" value="<?php if (isset($_GET['toDate']) && $_GET['toDate'] != '') { echo $_GET['toDate']; } else { echo date('Y-m-d');} ?>">
                </div>
                <div class="col-md-1 text-center my-auto">
                    <input class="btn btn-success btn-sm mt-2" type="submit" name="getReport" value="Get Data">
                </div>
            </div>
        </form>
        <div class="row mt-2">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $i = 1;
                            if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                            $deptFilterQuery = " AND users_master.floor_id='$_GET[dId]'";
                            }
                            if (isset($_GET['from']) && isset($_GET['from']) && $_GET['toDate'] != '' && $_GET['toDate']) {
                            $dateFilterQuery = " AND attendance_master.attendance_date_start BETWEEN '$_GET[from]' AND '$_GET[toDate]'";
                            }
                            if (isset($_GET['user_id']) && $_GET['user_id'] > 0) {
                            $userFilterQuery = "AND attendance_master.user_id='$_GET[user_id]'";
                            }
                            $q = $d->select("attendance_master LEFT JOIN shift_timing_master ON shift_timing_master.shift_time_id = attendance_master.shift_time_id
                            LEFT JOIN leave_master ON leave_master.user_id = attendance_master.user_id AND leave_master.leave_start_date =attendance_master.attendance_date_start,users_master", "users_master.user_id=attendance_master.user_id AND attendance_master.society_id='$society_id' AND users_master.delete_status=0 $deptFilterQuery $dateFilterQuery $userFilterQuery $blockAppendQueryUser", "ORDER BY attendance_master.attendance_date_start DESC");
                            $counter = 1;
                            if (isset($_GET['dId']))
                            {
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <!-- <th>Leave</th> -->
                                        <th>location</th>
                                        <!-- <th>Status</th> -->
                                        <th>Action</th>
                                        <th>Employee Name</th>
                                        <th>Punch In Date</th>
                                        <th>Punch In</th>
                                        <th>Punch Out Date</th>
                                        <th>Punch Out</th>
                                        <th>Hours</th>
                                        <th>In Branch</th>
                                        <th>Out Branch</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while($data = mysqli_fetch_array($q))
                                    {
                                        if($data['attendance_in_from'] != null)
                                        {
                                            if($data['attendance_in_from'] == 0)
                                            {
                                                $attendance_in_from = "Face App";
                                            }
                                            else
                                            {
                                                $attendance_in_from = "User App";
                                            }
                                        }
                                        else
                                        {
                                            $attendance_in_from = "";
                                        }
                                        if($data['attendance_out_from'] != null)
                                        {
                                            if($data['attendance_out_from'] == 0)
                                            {
                                                $attendance_out_from = "Face App";
                                            }
                                            else
                                            {
                                                $attendance_out_from = "User App";
                                            }
                                        }
                                        else
                                        {
                                            $attendance_out_from = "";
                                        }
                                    ?>
                                    <tr>
                                        <td><?php echo $counter++; ?></td>
                                        <!-- <td><?php
                                            if(isset($data['leave_day_type']) && $data['leave_day_type'] == 0)
                                            {
                                                $leave = "Full";
                                            }
                                            elseif(isset($data['leave_day_type']) && $data['leave_day_type'] == 1)
                                            {
                                                $leave = "Half";
                                            }
                                            else
                                            {
                                                $leave = "";
                                            }
                                            if ($data['punch_out_time'] != "00:00:00")
                                            {
                                            ?>
                                            <button class="btn <?php if ($leave!="") { echo 'btn-danger'; } else { echo 'btn-primary'; } ?> btn-sm" onclick="openLeaveModal(<?php if ($data['leave_id'] != '') { echo $data['leave_id']; } else { echo 0;} ?>,<?php echo $data['attendance_id']; ?>,<?php echo $data['user_id']; ?>,'<?php echo date('Y-m-d', strtotime($data['attendance_date_start'])); ?>','<?php echo $interval->format('%h'); ?>')">
                                            <?php if ($leave!="") { ?>
                                            <?php } else { ?>
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            <?php } echo $leave; ?></button>
                                            <?php
                                            }
                                            else
                                            {
                                                if($data['attendance_date_start'] < date('Y-m-d'))
                                                {
                                            ?>
                                                    <button type="button" class="btn btn-sm btn-warning " onclick="changeUserAttendace(<?php if ($data['attendance_id'] != '') {echo $data['attendance_id']; }  ?>,<?php if ($_GET['dId'] != '') {  echo $_GET['dId'];}  ?>,'<?php if ($data['attendance_id'] != '') { echo $data['shift_type']; }  ?>','<?php if ($data['attendance_id'] != '') {echo $data['attendance_date_start']; }  ?>','<?php if ($data['punch_in_time'] != '') {echo $data['punch_in_time'];}  ?>')"><i class="fa fa-clock-o"></i></button>
                                                <?php
                                                }
                                                else
                                                {
                                                    $punch_in_time = new DateTime($data['punch_in_time']);
                                                    $crtime = new DateTime(date('H:i:s'));
                                                    $intrval = $punch_in_time->diff($crtime);
                                                    $intm =  $intrval->format('%H:%I');
                                                }
                                            }
                                            ?>
                                        </td> -->
                                        <td>
                                            <button type="button" class="btn btn-sm btn-primary ml-2" onclick="viewUserCheckInOutImage('<?php echo $data['attendance_id']; ?>')"> <i class="fa fa-picture-o"></i> <i class="fa fa-map-marker"></i></button>
                                            <?php
                                            if($data['wfh_attendance'] == 1)
                                            {
                                            ?>
                                                <b><span class="badge badge-pill m-1 badge-info"><i class="fa fa-home"></i> WFH</span></b>
                                            <?php } ?>
                                        </td>
                                        <!-- <td>
                                            <?php
                                            if($data['attendance_status'] == 0)
                                            {
                                                if($data['attendance_range_in_km'] != "")
                                                {
                                                    $data['attendance_range_in_km'] = $data['attendance_range_in_km'];
                                                }
                                                else
                                                {
                                                    $data['attendance_range_in_km'] = 0;
                                                }
                                                if($data['attendance_range_in_meter'] != "0.00")
                                                {
                                                    $data['attendance_range_in_meter'] = $data['attendance_range_in_km'];
                                                }
                                                else
                                                {
                                                    $data['attendance_range_in_meter'] = 0;
                                                }
                                                if($data['punch_out_latitude'] != "")
                                                {
                                                    $out_lat = $data['punch_out_latitude'];
                                                }
                                                else
                                                {
                                                    $out_lat = 0;
                                                }
                                                if($data['punch_out_longitude'] != "")
                                                {
                                                    $out_lng = $data['punch_out_longitude'];
                                                }
                                                else
                                                {
                                                    $out_lng = 0;
                                                }
                                            ?>
                                                <button title="Approve Attendance" type="button" class="btn btn-sm btn-primary ml-1" onclick="pendingAttendanceStatusChange(1, '<?php echo $data['attendance_id']; ?>', 'attendaceStatus','<?php echo $data['punch_in_latitude']; ?>', '<?php echo $data['punch_in_longitude']; ?>','<?php echo $data['block_id']; ?>','<?php echo $data['attendance_range_in_meter'] ?>','<?php echo $data['attendance_range_in_km'] ?>','<?php echo $data['punch_in_image'] ?>','<?php echo $out_lat; ?>','<?php echo $out_lng ?>','<?php echo $data['punch_out_image']; ?>')"><i class="fa fa-check"></i></button>
                                                <button title="Decline Attendance" type="button" class="btn btn-sm btn-danger ml-1" onclick="pendingAttendanceStatusChange(2, '<?php echo $data['attendance_id']; ?>', 'attendaceStatus','<?php echo $data['punch_in_latitude']; ?>', '<?php echo $data['punch_in_longitude']; ?>','<?php echo $data['block_id']; ?>','<?php echo $data['attendance_range_in_meter'] ?>','<?php echo $data['attendance_range_in_km'] ?>','<?php echo $data['punch_in_image'] ?>','<?php echo $out_lat; ?>','<?php echo $out_lng ?>','<?php echo $data['punch_out_image']; ?>')"><i class="fa fa-times"></i></button>
                                            <?php
                                            }
                                            elseif($data['attendance_status'] == 1)
                                            {
                                            ?>
                                                <span class="badge badge-pill badge-success m-1">Approved </span>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <span class="badge badge-pill badge-danger m-1">Declined </span>
                                            <?php
                                            } ?>
                                        </td> -->
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <button type="button" class="btn btn-sm btn-primary ml-2" onclick="allAttendaceSet(<?php echo $data['attendance_id']; ?>)"> <i class="fa fa-eye"></i></button>
                                                <input type="hidden" name="user_id_attendace" value="<?php if ($data['user_id'] != "") { echo $data['user_id'];} ?>">
                                                <?php
                                                if($data['attendance_date_start'] != date("Y-m-d"))
                                                {
                                                ?>
                                                <button type="button" title="Edit Attendance" class="btn btn-sm btn-warning ml-2" onclick="updateAttendance('<?php echo $data['attendance_id']; ?>')"> <i class="fa fa-pencil"></i></button>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </td>
                                        <td><?php echo $data['user_full_name']; ?></td>
                                        <td><?php if ($data['attendance_date_start'] != '0000-00-00' && $data['attendance_date_start'] != 'null') {
                                            echo date("d M Y", strtotime($data['attendance_date_start'])) . " (" . date("D", strtotime($data['attendance_date_start'])) . ")";
                                        } ?></td>
                                        <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_in_time'] != 'null') {
                                            if ($attendance_in_from == "") {
                                            echo date("h:i A", strtotime($data['punch_in_time']));
                                            } else {
                                            echo date("h:i A", strtotime($data['punch_in_time'])) . "(" . $attendance_in_from . ")";
                                            }
                                            } ?>
                                        </td>
                                        <td><?php if ($data['attendance_date_end'] != '0000-00-00' && $data['attendance_date_end'] != 'null') { echo date("d M Y", strtotime($data['attendance_date_end']));
                                            if ($attendance_out_from != "") {
                                            echo "(" . $attendance_out_from . ")";
                                            }
                                        } ?></td>
                                        <td><?php if ($data['punch_out_time'] != '00:00:00' && $data['punch_out_time'] != 'null') {
                                            echo date("h:i A", strtotime($data['punch_out_time']));
                                            } ?>
                                            <?php if ($data['punch_out_image'] != '') { ?>
                                            <?php } ?>
                                            <?php if ($data['punch_out_latitude'] != '' & $data['punch_out_longitude'] != '') { ?>
                                            <?php } ?>
                                        </td>

                                        <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_out_time'] != '00:00:00' && $data['total_working_hours']=='') {
                                            echo $d->getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],$data['punch_in_time'],$data['punch_out_time']);
                                              } else {
                                                echo $data['total_working_hours'];
                                              } ?>
                                        </td>
                                        <td><?php  echo $data['punch_in_branch'];  ?></td>
                                       <td><?php  echo $data['punch_out_branch'];  ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php } else {  ?>
                            <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select Department</span>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            </div><!-- End Row-->
        </div>
        <!-- End container-fluid-->
    </div>
    <div class="modal fade" id="ChanngeAttendaceDataModal">
        <div class="modal-dialog ">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Update Attendance</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="align-content: center;">
                    <div class="card-body">
                        <form id="changeAttendance" name="changeAttendance" enctype="multipart/form-data" method="post">
                            <div class="form-group row hidePunchDateForShift" id="hidePunchDateForShift">
                                <label for="input-10" class="col-sm-6 col-form-label">Punch Out Date </label>
                                <div class="col-lg-6 col-md-6" id="">
                                    <select type="text" readonly maxlength="10" style="text-transform:uppercase;" name="punch_out_date" id="punch_out_date" class="form-control punch_out_date">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-6 col-form-label">Punch Out </label>
                                <div class="col-lg-6 col-md-6" id="">
                                    <input type="text" class="form-control time-picker-shft-Out_update" readonly id="punch_out_time" name="punch_out_time">
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <input type="hidden" name="updateUserAttendance" value="updateUserAttendance">
                                <input type="hidden" name="attendance_id" id="attendance_id">
                                <input type="hidden" class="indate" name="indate" id="indate">
                                <button id="addHrDocumentBtn" type="submit" onclick="updateUserAttendanceData()" class="btn btn-success addUserBank hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addAttendaceModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Attendance Data</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="align-content: center;">
                    <div class="card-body">
                        <div class="row col-md-12 ">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody id="attendanceData">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row col-md-12 wrkRpt">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="UserCheckInLocation">
        <div class="modal-dialog modal-lg">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Attendance Data</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="align-content:center;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-6 in_reason_hide">
                                <span id="in_reason"></span>
                            </div>
                            <div class="col-md-6 col-6 out_reason_hide">
                                <span id="out_reason"></span>
                            </div>
                            <div class="col-md-6 col-12 out_reason_hide">
                                <label>Punch In</label>
                                <span id="approvInTime"></span>
                                <span class="badge badge-pill badge-primary m-1 InawayShow"></span>
                            </div>
                            <div class="col-md-6 col-12 out_reason_hide">
                                <label>Punch out</label>
                                <span id="approvOutTime"></span>
                                <span class="badge badge-pill badge-primary m-1 away"></span>
                            </div>
                            <div class="col-md-3 col-12 text-center">
                                <div class="punchInImage" id=""></div>
                            </div>
                            <div class="col-md-3 col-12 text-center">
                                <div class="map" id="map" style="width: 100%; height: 300px;"></div>
                            </div>
                            <div class="col-md-3 col-12  text-center">
                                <div class="punchOutImage" id=""></div>
                            </div>
                            <div class="col-md-3  col-12 text-center mapDiv">
                                <div class="map" id="aprovePunchOut" style="width: 100%; height: 300px;"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <form id="attendanceDeclineReasonForm">
                                <div class="col-md-12 attendanceDeclineReason">
                                    <label class="form-control-label">Decline Reason <span class="text-danger">*</span></label>
                                    <textarea class="form-control" required name="attendance_declined_reason" id="attendance_declined_reason"></textarea>
                                </div>
                                <div class="form-footer text-center">
                                    <button type="submit" class="btn btn-primary attendanceDeclineReasonClass">Change Status</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="UserCheckInOutImage">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Attendance Details</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="align-content: center;">
                    <div class="card-body p-0">
                         <div class="row">
                          <div class="col-md-6">
                            <div><b>Punch In :</b> <span id="in_time"></span></div>
                            <div ><b>Distance:</b> <span class="Inkm"></span></div>
                            <div ><b>Address:</b> <span class="InAddress"></span></div>
                          </div>
                          <div class="col-md-6">
                            <div><b>Punch Out :</b> <span id="out_time"></span></div>
                            <div ><b>Distance:</b> <span class="Outkm"></span></div>
                            <div ><b>Address:</b> <span class="OutAddress"></span></div>
                           
                          </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6 col-6 text-center hideInData">
                              <div class="userCheckInOutImageData " id="userCheckInOutImageData">
                              </div>
                            </div>
                            <div class="col-md-6 col-6 text-center hideOutData">
                              <div class="userCheckOutImageData " id="userCheckOutImageData">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12 col-12 float-left mapDiv ">
                              <div class="map mr-1" id="map3" style="width: 100%; height: 280px;">
                              </div>
                            </div>
                            
                          </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="attendanceDeclineReason">
        <div class="modal-dialog ">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Declined Reason</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="holiday" style="align-content: center;">
                    <textarea class="form-control" name="attendance_declined_reason" id="attendance_declined_reason"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary attendanceDeclineReasonClass" data-dismiss="modal" aria-label="Close">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="leaveModal2">
        <div class="modal-dialog ">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Add Leave</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="addLeaveForm" action="controller/leaveController.php" method="post">
                    <div class="modal-body" id="leaveBoday" style="align-content: center;">
                    </div>
                    <input type="hidden" name="leave_date" id="leave_date">
                    <input type="hidden" name="leave_user_id" id="leave_user_id">
                    <input type="hidden" name="leave_id" id="leave_id">
                    <input type="hidden" name="toDateTo" value="<?php echo $_GET['toDate']; ?>">
                    <input type="hidden" name="fromDate" value="<?php echo $_GET['from']; ?>">
                    <input type="hidden" name="branchId" value="<?php echo $_GET['bId']; ?>">
                    <input type="hidden" name="dpId" value="<?php echo $_GET['dId']; ?>">
                    <input type="hidden" name="leave_attendance_id" id="leave_attendance_id">
                    <div class="modal-footer">
                        <div class="col-md-12 text-center">
                            <input type="hidden" name="MarkAsLeave" value="MarkAsLeave">
                            <button type="submit" class="btn btn-primary attendanceDeclineReasonClass">Save changes</button>
                            <button type="button" id="DeleteLeave" onclick="removeUserLeave()" class="btn btn-danger ">Remove Leave</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateAttendanceModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Update Attendance</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="align-content: center;">
                    <div class="card-body">
                        <form action="" id="updateAttendanceForm" name="updateAttendanceForm" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="" class="shift_type">
                            <div class="form-group row">
                                <label for="attendance_start_date" class="col-sm-6 col-form-label">Attendance Start Date <span class="text-danger">*</span></label>
                                <div class="col-lg-6 col-md-6">
                                    <input type="text" readonly name="attendance_start_date" id="attendance_start_date_View" class="form-control asd"/>
                                </div>
                            </div>
                            <div class="form-group row attendance-end-date-div">
                                <label for="attendance_end_date" class="col-sm-6 col-form-label">Attendance End Date <span class="text-danger">*</span></label>
                                <div class="col-lg-6 col-md-6">
                                    <select class="form-control single-select aed" id="attendance_end_date" name="attendance_end_date">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="punch_in_time" class="col-sm-6 col-form-label">Punch In <span class="text-danger">*</span></label>
                                <div class="col-lg-6 col-md-6">
                                    <input type="text" class="form-control" readonly id="punch_in_time" name="punch_in_time">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="edit_punch_out_time" class="col-sm-6 col-form-label">Punch Out <span class="text-danger">*</span></label>
                                <div class="col-lg-6 col-md-6">
                                    <input type="text" class="form-control" readonly id="edit_punch_out_time" name="edit_punch_out_time">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="modified_remark" class="col-sm-6 col-form-label">Remarks</label>
                                <div class="col-lg-6 col-md-6">
                                    <textarea class="form-control" maxlength="400" id="modified_remark" name="modified_remark"></textarea>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <input type="hidden" name="editAttendance" value="editAttendance">
                                <input type="hidden" name="shift_time_id" id="shift_time_id">
                                <input type="hidden" name="user_id" id="update_user_id">
                                <input type="hidden" name="unit_id" id="update_unit_id">
                                <input type="hidden" name="user_name" id="update_user_name">
                                <input type="hidden" name="update_attendance_id" id="update_attendance_id">
                                <button type="button" onclick="editAttendanceForm()" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>
    <script type="text/javascript">
    function initialize(d_lat, d_long, blockRange, blockLat, blockLOng, out_lat, out_lng) {
    var latlng = new google.maps.LatLng(d_lat, d_long);
    var Blatlng = {
    lat: parseFloat(blockLat),
    lng: parseFloat(blockLOng)
    };
    var latitute = d_lat;
    var longitute = d_long;
    var map = new google.maps.Map(document.getElementById('map'), {
    center: latlng,
    zoom: 13
    });
    ////User marker
    var marker = new google.maps.Marker({
    map: map,
    icon: "assets/images/placeholder.png",
    position: latlng,
    // draggable: true,
    anchorPoint: new google.maps.Point(0, -29)
    });
    
    ////Company marker
    var marker2 = new google.maps.Marker({
    map: map,
    position: Blatlng,
    //  draggable: true,
    anchorPoint: new google.maps.Point(0, -29)
    });
    ////Company circle
    var circle = new google.maps.Circle({
    map: map,
    radius: parseInt(blockRange), // 10 miles in metres
    fillColor: '#AA0000'
    });
    circle.bindTo('center', marker2, 'position');
    var parkingRadition = 5;
    var citymap = {
    newyork: {
    center: {
    lat: latitute,
    lng: longitute
    },
    population: parkingRadition
    }
    };
    /*  var input = document.getElementById('searchInput5');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map); */
    var infowindow = new google.maps.InfoWindow();
    
    }
    function initializeCommonMap(in_lat, in_long,out_lat,out_long) {

      var latlng = new google.maps.LatLng(in_lat, in_long);
      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize3(d_lat, d_long) {
       
      var latlng = new google.maps.LatLng(d_lat, d_long);
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
   

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize2(out_lat, out_long) {

      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlngOut,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));


      var infowindow = new google.maps.InfoWindow();
      
    }
    
    function approvePunchOutMap(d_lat, d_long, blockRange, blockLat, blockLOng) {
    var latlng = new google.maps.LatLng(d_lat, d_long);
    var Blatlng = {
    lat: parseFloat(blockLat),
    lng: parseFloat(blockLOng)
    };
    var latitute = d_lat;
    var longitute = d_long;
    var map = new google.maps.Map(document.getElementById('aprovePunchOut'), {
    center: latlng,
    zoom: 13
    });
    ////User marker
    var marker = new google.maps.Marker({
    map: map,
    icon: "assets/images/placeholder.png",
    position: latlng,
    // draggable: true,
    anchorPoint: new google.maps.Point(0, -29)
    });
    ////Company marker
    var marker2 = new google.maps.Marker({
    map: map,
    position: Blatlng,
    //  draggable: true,
    anchorPoint: new google.maps.Point(0, -29)
    });
    ////Company circle
    var circle = new google.maps.Circle({
    map: map,
    radius: parseInt(blockRange), // 10 miles in metres
    fillColor: '#AA0000'
    });
    circle.bindTo('center', marker2, 'position');
    var parkingRadition = 5;
    var citymap = {
    newyork: {
    center: {
    lat: latitute,
    lng: longitute
    },
    population: parkingRadition
    }
    };
    /*  var input = document.getElementById('searchInput5');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map); */
    var infowindow = new google.maps.InfoWindow();
    /* autocomplete10.addListener('place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place5 = autocomplete10.getPlace();
    if (!place5.geometry) {
    window.alert("Autocomplete's returned place5 contains no geometry");
    return;
    }
    // If the place5 has a geometry, then present it on a map.
    if (place5.geometry.viewport) {
    map.fitBounds(place5.geometry.viewport);
    } else {
    map.setCenter(place5.geometry.location);
    map.setZoom(17);
    }
    marker.setPosition(place5.geometry.location);
    marker.setVisible(true);
    var pincode = "";
    for (var i = 0; i < place5.address_components.length; i++) {
    for (var j = 0; j < place5.address_components[i].types.length; j++) {
    if (place5.address_components[i].types[j] == "postal_code") {
    pincode = place5.address_components[i].long_name;
    }
    }
    }
    bindDataToForm(place5.formatted_address, place5.geometry.location.lat(), place5.geometry.location.lng(), pincode, place5.name);
    infowindow.setContent(place5.formatted_address);
    infowindow.open(map, marker);
    }); */
    // this function will work on marker move event into map
    /* google.maps.event.addListener(marker, 'dragend', function() {
    geocoder.geocode({
    'latLng': marker.getPosition()
    }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
    if (results[0]) {
    var places = results[0];
    var pincode = "";
    var serviceable_area_locality = places.address_components[4].long_name;
    for (var i = 0; i < places.address_components.length; i++) {
    for (var j = 0; j < places.address_components[i].types.length; j++) {
    if (places.address_components[i].types[j] == "postal_code") {
    pincode = places.address_components[i].long_name;
    }
    }
    }
    bindDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng(), pincode, serviceable_area_locality);
    }
    }
    });
    }); */
    }
    <?php if (!isset($_GET['month_year'])) { ?>
    $('#month_year').val(<?php echo $currentMonth; ?>)
    <?php } ?>
    function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
    newwindow.focus()
    }
    return false;
    }
    </script>
    <style>
    .attendance_status {
    min-width: 113px;
    }
    </style>