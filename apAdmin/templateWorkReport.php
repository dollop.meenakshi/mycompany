<?php

$currentYear = date('Y');
$uId = (int)$_REQUEST['uId'];
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$previous15DaysDate = date('Y-m-d', strtotime('-31 days'));
if(isset($_GET['from']) && !empty($_GET['from']) && $_GET['from'] != 0)
{
    $date_from = $_GET['from'];
}
else
{
    $date_from = "";
}
if(isset($_GET['toDate']) && !empty($_GET['toDate']) && $_GET['toDate'] != 0)
{
    $date_to = $_GET['toDate'];
}
else
{
    $date_to = "";
}
if(isset($_GET['template_id']) && !empty($_GET['template_id']) && $_GET['template_id'] != 0)
{
    $template_id = $_GET['template_id'];
}
else
{
    $template_id = "";
}

 if(isset($uId) && $uId>0) {
    $UserFilterQuery = " AND wrm.user_id='$uId'";
}

$que_arr = [];
$que_seq = [];
$new = [];
if(isset($_GET['from']) && !empty($_GET['from']) && isset($_GET['toDate']) && !empty($_GET['toDate']) && isset($_GET['template_id']) && !empty($_GET['template_id']) && $_GET['from'] != 0 && $_GET['toDate'] != 0 && $_GET['template_id'] != 0)
{
    $all_que = $d->selectRow("template_question_id,template_question","template_question_master","template_id = '$template_id'");
    while($qd = $all_que->fetch_assoc())
    {
        $que_seq[] = htmlspecialchars_decode($qd['template_question']);
        $que_arr[] = $qd;
    }
    $all_user_ids = [];
    $all_ans_new = [];
    $all_ans = [];
    $q = $d->select("work_report_employee_ans","work_report_template_id = '$template_id' AND work_report_date BETWEEN '$date_from' AND '$date_to' ","ORDER BY work_report_employee_ans_id");
    while ($data = $q->fetch_assoc())
    {
        $all_ans_new[$data['work_report_id']][] = $data;
        $all_ans[] = $data;
        $all_user_ids[] = $data['user_id'];
    }
    $all_user_ids = array_unique($all_user_ids);
    $user_id_str = implode(",",$all_user_ids);

    if(!empty($all_user_ids) && $user_id_str != "")
    {
        if ($access_branchs!='') {
            if ($access_departments!='') {
              $restictDepartmentUser = " AND um.floor_id IN ('$departmentIds')";
            }
            $appendAccessQuery = "  AND um.block_id IN ('$blockids') $restictDepartmentUser";
        }

        $q1 = $d->selectRow("wrm.work_report_id,um.user_id,um.user_full_name,fm.floor_name,wrm.work_report_date,wrm.work_report_created_date","users_master AS um JOIN work_report_master AS wrm ON wrm.user_id = um.user_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","wrm.user_id IN ($user_id_str) AND wrm.work_report_date BETWEEN '$date_from' AND '$date_to' AND find_in_set($template_id, cast(wrm.template_ids as char)) > 0 $UserFilterQuery $appendAccessQuery");
        while($data1 = $q1->fetch_assoc())
        {
            $new[] = $data1;
        }
    }
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title">Template Work Report</h4>
            </div>
        </div>
        <form id="fltrFrm">
            <div class="row pb-2">
                <div class="col-md-2 col-6 form-group">
                    <input type="text" required="" readonly="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" name="from" value="<?php if (isset($_GET['from']) && $_GET['from'] != '') { echo $_GET['from']; } else { echo $previous15DaysDate; } ?>">
                </div>
                <div class="col-md-2 col-6 form-group">
                    <input type="text" required="" readonly="" class="form-control" autocomplete="off" id="autoclose-datepickerTo" name="toDate" value="<?php if (isset($_GET['toDate']) && $_GET['toDate'] != '') { echo $_GET['toDate']; } else { echo date('Y-m-d'); } ?>">
                </div>
                <div class="col-md-3 col-6 form-group">
                    <select name="template_id" class="form-control single-select">
                        <option value="">Select Template</option>
                        <?php
                            $t = $d->selectRow("template_id,template_name","template_master","society_id = '$society_id' AND template_status = 0");
                            while ($td = $t->fetch_assoc())
                            {
                        ?>
                        <option  <?php if($template_id == $td['template_id']) { echo 'selected';} ?> value="<?php echo $td['template_id'];?>" ><?php echo $td['template_name'];?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4 form-group col-6">
                    <select name="uId" id="uId" class="form-control single-select">
                        <option value="">All Employee </option> 
                        <?php 
                        if (isset($all_user_ids)) {
                         $userIds = join("','",$all_user_ids); 
                        }
                            $user=$d->selectRow("users_master.*,block_master.block_name,floors_master.floor_name","users_master,block_master,floors_master","block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND block_master.block_id=floors_master.block_id AND users_master.society_id='$society_id' AND users_master.delete_status=0 AND users_master.user_status !=0 AND users_master.user_id IN ('$userIds') $blockAppendQueryUser");  
                            while ($userdata=mysqli_fetch_array($user)) {
                        ?>
                        <option <?php if($uId==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'].' ('.$userdata['user_designation'].')'. $userdata['floor_name'].'-'.$userdata['block_name'];?></option> 
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-1 from-group col-2">
                    <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get ">
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $i = 1;
                            ?>
                            <table id="<?php if ($adminData['report_download_access'] == 0) {
                                            echo 'exampleReportWithoutBtn';
                                        } else {
                                            echo 'exampleReport';
                                        } ?>" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee Name</th>
                                        <th>Department</th>
                                        <th>Date</th>
                                        <th>Submited Date</th>
                                        <th>Day</th>
                                        <?php
                                        for($tq = 0; $tq < count($que_arr) ; $tq++)
                                        {
                                        ?>
                                        <th><?php echo $que_arr[$tq]['template_question']; ?></th>
                                        <?php
                                        }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;

                                    foreach ($new AS $k1 => $v1) {  ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $v1['user_full_name']; ?></td>
                                        <td><?php echo $v1['floor_name']; ?></td>
                                        <td><?php echo $v1['work_report_date']; ?></td>
                                        <td><?php if($v1['work_report_date']!=="0000-00-00" && $v1['work_report_date']!='null') { echo date("d M Y h:i A", strtotime($v1['work_report_created_date'])); }?></td>
                                        <td>
                                        <?php
                                            $timestamp = strtotime($v1['work_report_date']);
                                            $day = date('l', $timestamp);
                                            echo $day;
                                        ?> 
                                        </td>
                                        <?php
                                        for($tq1 = 0; $tq1 < count($que_arr) ; $tq1++)
                                        {
                                            $quePos = $tq1;
                                            $work_report_id = $v1['work_report_id'];
                                        ?>
                                            <td>
                                               <?php
                                                if ($all_ans_new[$work_report_id][$quePos]['work_report_question_type']==2) {
                                                  $optionValueArr = json_decode($all_ans_new[$work_report_id][$quePos]['work_report_employee_ans'], true);
                                                   echo implode(",",$optionValueArr);
                                                } else if ($all_ans_new[$work_report_id][$quePos]['work_report_question_type']==4 && $all_ans_new[$work_report_id][$quePos]['has_multi_attachment']==1) {
                                                  $optionValueArr = json_decode($all_ans_new[$work_report_id][$quePos]['work_report_employee_ans'], true);
                                                
                                                    for ($iAtte=0; $iAtte < count($optionValueArr); $iAtte++) {
                                                        $fileNameAtt = $optionValueArr[$iAtte]['file'];
                                                        echo "<a href='../img/work_report/$fileNameAtt'>".$optionValueArr[$iAtte]['file']."</a>";
                                                        echo "<Br>";
                                                    }

                                                }else if ($all_ans_new[$work_report_id][$quePos]['work_report_question_type']==4 && $all_ans_new[$work_report_id][$quePos]['has_multi_attachment']==0) {
                                                     $fileNameAtt =  $all_ans_new[$work_report_id][$quePos]['work_report_employee_ans'];
                                                    echo "<a href='../img/work_report/$fileNameAtt'>".$fileNameAtt."</a>";

                                                } else if ($all_ans_new[$work_report_id][$quePos]['work_report_question_type']==10) {
                                                 if ($all_ans_new[$work_report_id][$quePos]['work_report_question_type']!="") {
                                                  $jsonArray = json_decode($all_ans_new[$work_report_id][$quePos]['work_report_employee_ans'], true);
                                                   // print_r($optionValueArr);
                                                   if (isset($jsonArray) && count($jsonArray)>0) {
                                                    for ($iTest=1; $iTest < count($jsonArray) ; $iTest++) {
                                                        echo $iTest.'. '.$jsonArray[$iTest]['topic'].' ('.$topicTime = $jsonArray[$iTest]["time"].' Min) ';
                                                        
                                                     }

                                                   }
                                                   }
                                                }else if ($all_ans_new[$work_report_id][$quePos]['work_report_question_type']==11) {
                                                      $jsonArray = json_decode($all_ans_new[$work_report_id][$quePos]['work_report_employee_ans'], true);
                                                    if($jsonArray['description']!="") {
                                                      echo $jsonArray['description'].' ('.$jsonArray['time'].' Min)';

                                                    }
                                                   
                                                }else if ($all_ans_new[$work_report_id][$quePos]['work_report_question_type']==9) {
                                                  echo $all_ans_new[$work_report_id][$quePos]['work_report_employee_ans']. '%';
                                                } else {
                                                    echo $all_ans_new[$work_report_id][$quePos]['work_report_employee_ans'];
                                                } 
                                               
                                                 ?>
                                            </td>
                                        <?php  }  ?>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<!--Start Back To Top Button-->