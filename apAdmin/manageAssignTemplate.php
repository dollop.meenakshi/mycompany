<?php error_reporting(0);

$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-6 col-6">
                <h4 class="page-title"> Manage Assign Template</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
            <div class="btn-group float-sm-right">
                <a href="assignTemplate" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
                <a href="javascript:void(0)" onclick="DeleteAll('deleteAssignedTemplate');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
            </div>
            </div>
        </div>   
        <!-- <form class="branchDeptFilter" action="" method="get">
            <div class="row pt-2 pb-2">
              <?php include 'selectBranchDeptEmpForFilter.php'; ?>
              <div class="col-md-3 form-group">
                <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
              </div>
            </div>
        </form> -->
    

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php //if((isset($bId) && $bId >0) && (isset($dId) && $dId >0) ){
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>Template</th>
                                        <th>Allow Multiple</th>
                                        <th>Assign For</th>
                                        <th>Action</th>
                                    
                                    </tr>
                                </thead>
                                <tbody id="showFilterData">
                                    <?php
                                    $q = $d->select("template_assign_master,template_master","template_assign_master.template_id=template_master.template_id AND template_assign_master.society_id='$society_id'");
                                    $counter = 1;
                                    while ($data = mysqli_fetch_array($q)) {
                                    ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="id" id="id"  value="<?php echo $data['template_assign_id']; ?>">                  
                                                <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['template_assign_id']; ?>"> 
                                            </td>
                                            <td><?php echo $counter++; ?></td>
                                            <td><?php echo $data['template_name']; ?></td>
                                            <td><?php if($data['allow_muliple_time']==1) { echo "Yes"; } else { echo "No";} ?></td>
                                            <td><?php 
                                                if($data['template_assign_for'] == 0){
                                                    echo "All Company";
                                                }
                                                else if($data['template_assign_for'] == 1){
                                                    echo "Branch Wise";
                                                }
                                                else if($data['template_assign_for'] == 2){
                                                    echo "Department Wise";
                                                }
                                                else if($data['template_assign_for'] == 3){
                                                    echo "Employee Wise";
                                                }
                                             ?></td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                <form action="assignTemplate" method="post">
                                                    <input type="hidden" name="template_assign_id" value="<?php echo $data['template_assign_id'];?>">
                                                    <input type="hidden" name="edit_assign" value="edit_assign">
                                                    <button type="submit" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-pencil"></i></button>
                                                </form>
                                                </div>
                                            </td>
                                        

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            
                            </table>
                            <?php //}else{ ?>
                           <!--  <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select Department</span>
                            </div> -->
                        <?php //} ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->