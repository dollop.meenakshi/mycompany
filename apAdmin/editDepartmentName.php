<div class="content-wrapper"> 
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title"><?php echo $xml->string->floors; ?></h4>
        
      </div>
      <div class="col-sm-3">
        <div class="btn-group float-sm-right">
          <a href="#" data-toggle="modal" data-target="#addUnit"  class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"> </i><?php echo $xml->string->add; ?> <?php echo $xml->string->unit; ?> </a>
        </div>  
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <?php
      extract(array_map("test_input" , $_POST));
      // print_r($_POST);
      ?>
      
    </div>
    <div class="row">
      <?php
      $fq=$d->select("floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floors_master.block_id='$bId' AND floors_master.floor_id = $floor_id");
      while ($floorData=mysqli_fetch_array($fq)) {
      $floorId= $floorData['floor_id'];
      $no_of_unit= $floorData['no_of_unit'];
      $unit_type= $floorData['unit_type'];
      $block_id= $floorData['block_id'];
      $blockName= $floorData['block_name'];
      ?>
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="unitEdit" action="controller/unitControllerUpdate.php" method="post">
              <input type="hidden" name="updateUnitName" value="updateUnitName">
              <h4 class="form-header text-uppercase">
              <i class="fa fa-building"></i>
              <?php echo $floorData['floor_name'] ?>
              </h4> 
              <?php
              $uq=$d->select("unit_master","floor_id='$floorId' AND society_id='$society_id'","");
              if(mysqli_num_rows($uq)>0) {
              $i = 0;
              while ($unitData=mysqli_fetch_array($uq)) {
              $i++;
              ?>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label"><?php echo $xml->string->unit; ?>  <?php echo $i; ?></label>
                <div class="col-sm-4">
                  <input type="hidden" name="floor_id" id="floor_id" value="<?php echo $floor_id; ?>">
                  <input type="hidden" name="bId" id="floor_id" value="<?php echo $bId; ?>">
                  <input type="hidden" name="society_id" id="society_id" value="<?php echo $society_id; ?>">
                  <input required="" type="hidden" name="unit_id[]" class="form-control" value="<?php echo $unitData['unit_id'] ?>"><input maxlength="30" required="" type="text" name="unit_name[]" class="form-control space option_name-cls" value="<?php echo $unitData['unit_name'] ?>"  id="option_name<?php echo $i;?>" onblur="blurtxt();">
                </div>
              </div>
              <?php } } ?>
              <div class="form-footer text-center">
                
                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->update; ?> </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      </div><!--End Row-->
      <?php } ?>
      
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <!--Start Back To Top Button-->
<div class="modal fade" id="addUnit">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add; ?> <?php echo $xml->string->unit; ?> </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"  >
           <form action="controller/unitControllerUpdate.php" method="post" id="blockAdd">
              <input type="hidden" name="floor_id" id="floor_id" value="<?php echo $floor_id; ?>">
                  <input type="hidden" name="block_id" id="floor_id" value="<?php echo $bId; ?>">
                  <input type="hidden" name="society_id" id="society_id" value="<?php echo $society_id; ?>">
              <div class="form-group row">
                  <label for="input-10" class="col-sm-3 col-form-label"><?php echo $xml->string->unit; ?>  <?php echo $xml->string->name; ?>  <span class="text-danger">*</span></label>
                  <div class="col-sm-9">
                    <input  required="" type="text" maxlength="30" class="form-control text-capitalize" id="input-10" name="unit_name_single">
                  </div>
               
                 
                </div>
             
                <div class="form-footer text-center">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->add; ?> </button>
                </div>
              </form>
      </div>
      
    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">



  $( "#unitEdit" ).submit(function( event ) {
    var error = 0;
    $('.option_name-cls').each(function() {
      var clsVal =  $(this).val();
      var eleId = $(this).attr('id');

      if($.trim(clsVal) ==""){
        error++;
          /*$(eleId+'_div').removeClass('valid');
          $(eleId+'_div').addClass('error');*/


          $('#'+eleId+'_div').css('display','inline-block');
          $('#'+eleId+'_div').css('color','#ff0000');
          $('#'+eleId+'_div').text("Please enter "+eleId);
          $(".ajax-loader").hide();
        } else {
          /*$(eleId).addClass('valid');
          $(eleId).removeClass('error');*/
          $(".ajax-loader").show();
          $('#'+eleId+'_div').css('display','none');
        }
      });

var valOption = [];

          $('.option_name-cls').each(function(i){
            valOption[i] = $(this).val();
          });
        
$('.option_name-cls').each(function() {
      var clsVal =  $(this).val();
      var eleId = $(this).attr('id');
var numOccurences =1;

 if($.trim(clsVal) !=""){  
      numOccurences = $.grep(valOption, function (elem) {
        return elem.toLowerCase() === clsVal.toLowerCase();
    }).length;

       if(numOccurences>1){
        error++;
              $('#'+eleId+'_div').css('display','inline-block');
              $('#'+eleId+'_div').css('color','#ff0000');
              $('#'+eleId+'_div').text("Duplicate Option");
      } else {
           $('#'+eleId+'_div').css('display','none');
        }
}
  });
//IS_1309
//alert(error);

    


    if(error > 0 ){  

      event.preventDefault();
    }  else {  
      $( "#poll" ).submit();
    } 
  });

function blurtxt(){


    var valOption = [];
          
              $('.option_name-cls').each(function(i){
              //  alert($(this).val());
                valOption[i] = $(this).val();
              });
          
    var error = 0;



        
        $('.option_name-cls').each(function() {
        var clsVal =  $(this).val();
        var eleId = $(this).attr('id');
        
          if($.trim(clsVal) ==""){
            error++;
            $('#'+eleId+'_div').css('display','inline-block');
            $('#'+eleId+'_div').css('color','#ff0000');
            $('#'+eleId+'_div').text("Please enter "+eleId);

          } else {
            $('#'+eleId+'_div').css('display','none');
          }
        });

    //IS_1309
    $('.option_name-cls').each(function() {
          var clsVal =  $(this).val();
          var eleId = $(this).attr('id');
    var numOccurences =1;

     if($.trim(clsVal) !=""){  
          numOccurences = $.grep(valOption, function (elem) {
            return elem.toLowerCase() === clsVal.toLowerCase();
        }).length;
     
           if(numOccurences>1){
            error++;
                  $('#'+eleId+'_div').css('display','inline-block');
                  $('#'+eleId+'_div').css('color','#ff0000');
                  $('#'+eleId+'_div').text("Duplicate Option");
          } else {
               $('#'+eleId+'_div').css('display','none');
            }
    }
      });
    //IS_1309


     
    if(error>0){
      $(':input[type="submit"]').prop('disabled', true);
    } else {
      $(':input[type="submit"]').prop('disabled', false);
    }


        if(error > 0   ){
        event.preventDefault();
        } /*else {
          $( "#poll" ).submit();
        }*/
  }

</script>