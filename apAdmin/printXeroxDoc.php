

<!DOCTYPE html>
<html>
<head>
	<title>Xerox Document</title>
	<style type="text/css">
		body {
			background-color:white;
			font-family: 'Open Sans', sans-serif;
		}
		.id-card-holder {
			width: 200px;
		    padding: 4px;
		    /*margin: 0 auto;*/
		    margin-left: 2px;
		    margin-right: : 2px;
		    margin-bottom : 10px;
		    /*background-color: #1f1f1f;*/
		    border-radius: 5px;
		    position: relative;
		    display: inline-block;
		}

		.id-card {

			background-image: url('../img/icardbg.jpg');
			padding: 0px;
			border-radius: 5px;
			text-align: center;
			box-shadow: 0 0 1.5px 0px #b9b9b9;
			background-repeat: no-repeat;
			background-size: cover;
		}
		.id-card img {
			margin: 0 auto;
		}
		.header {
			margin: auto;
			width: 60px;
		}
		.header img {
			width: 100%;
    		margin-top: 5px;
		}
		.photo {
			margin: auto;
			width: 87px;
			height: 87px;
			border: 1px solid;
			border-radius: 50%;
			background: white;
			overflow: hidden;
		}
		.photo img {
			width: 100%;
			height: 100%;

		}
		hr {
			margin: 2px;
		}
		h2 {
			font-size: 12px;
			margin: 2px 0;
		}
		h3 {
			font-size: 10px;
			margin: 2.5px 0;
			font-weight: 300;
		}
		.qr-code img {
			width: 85px;
		}
		p {
			font-size: 10px;
			margin: 2px;
		}
		.id-card-hook {
			background-color: #000;
		    width: 70px;
		    margin: 0 auto;
		    height: 15px;
		    border-radius: 5px 5px 0 0;
		}
		.id-card-hook:after {
			content: '';
		    background-color: #d7d6d3;
		    width: 47px;
		    height: 6px;
		    display: block;
		    margin: 0px auto;
		    position: relative;
		    top: 6px;
		    border-radius: 4px;
		}
		.id-card-tag-strip {
			width: 45px;
		    height: 40px;
		    background-color: #0950ef;
		    margin: 0 auto;
		    border-radius: 5px;
		    position: relative;
		    top: 9px;
		    z-index: 1;
		    border: 1px solid #0041ad;
		}
		.id-card-tag-strip:after {
			content: '';
		    display: block;
		    width: 100%;
		    height: 1px;
		    background-color: #c1c1c1;
		    position: relative;
		    top: 10px;
		}
		.id-card-tag {
			width: 0;
			height: 0;
			border-left: 100px solid transparent;
			border-right: 100px solid transparent;
			border-top: 100px solid #0958db;
			margin: -10px auto -30px auto;
		}
		.id-card-tag:after {
			content: '';
		    display: block;
		    width: 0;
		    height: 0;
		    border-left: 50px solid transparent;
		    border-right: 50px solid transparent;
		    border-top: 100px solid #d7d6d3;
		    margin: -10px auto -30px auto;
		    position: relative;
		    top: -130px;
		    left: -50px;
		}
		.addressSoc {
			font-size: 7px;
			padding-top: 2px;
			font-weight: bold;
		}
		.addressDiv {
			height: 35px;
		}
		.website {
			padding-bottom: 2px;
			font-size: 7px;
		}
		.qr-code {
			margin: auto;
			width: 87px;
			border: 1px solid;
			border-radius: 5px;
		}
	</style>
</head>

  <body onload="window.print()">
<?php if(isset($_GET['doc']) && $_GET['doc'] !=""){ ?>
	<img src="../img/documents/<?php echo $_GET['doc'] ?>">
	<?php } ?>

</body>
<script type="text/javascript">
        Android.print('print');
  </script>
</html>
