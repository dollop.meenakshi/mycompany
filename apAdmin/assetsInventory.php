<?php
extract(array_map("test_input", $_REQUEST));
extract($_GET);

?>
<?php if (isset($_GET['custodian']) && $_GET['custodian'] == "handover") { ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <h4 class="page-title">Handover Custodian</h4>
        </div>
        <div class="col-lg-4 col-md-6">

        </div>
        <div class="col-lg-4 col-md-2">

        </div>
      </div>
      <div class="row mt-3">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
            <form id="addAssetsInventory" method="POST" class="form-horizontal" action="controller/assetsInventoryDetailsController.php" enctype="multipart/form-data">
            <?php $aq=$d ->selectRow('item_purchase_date',"assets_item_detail_master","assets_id='$_GET[id]'");
                $assetsData=mysqli_fetch_assoc($aq);
                $adq=$d ->selectRow('end_date,inventory_id',"assets_detail_inventory_master","assets_id='$_GET[id]'","ORDER BY inventory_id DESC");
                $assetsDetailData=mysqli_fetch_assoc($adq);
                 ?>
            <div class="modal-body">
              <?php if($assetsDetailData){ ?>
              <input type="hidden" name="iteam_created_date" id="iteam_created_date" value="<?php if($assetsDetailData['end_date'] != '0000-00-00'){ echo date("Y-m-d", strtotime($assetsDetailData['end_date'])); } ?>">
              <?php }else{ ?>
                <input type="hidden" name="iteam_created_date" id="iteam_created_date" value="<?php if($assetsData['item_purchase_date'] != '0000-00-00'){ echo date("Y-m-d", strtotime($assetsData['item_purchase_date'])); } ?>">
              <?php } ?>
              <input type="hidden" name="assets_id" id="assets_id" value="<?php if (isset($_GET['id']) && $_GET['id'] > 0) {
                                                                            echo $_GET['id'];
                                                                          } ?>">
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Branch<span class="required">*</span></label>
                <div class="col-lg-8">
                    <select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required>
                        <option value="">-- Select Branch --</option> 
                        <?php 
                        $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                        while ($blockData=mysqli_fetch_array($qb)) {
                        ?>
                        <option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                        <?php } ?>
                    </select> 
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Department<span class="required">*</span></label>
                <div class="col-lg-8">
                  <select class="form-control single-select" required="" name="floor_id" id="floor_id" onchange="getCustodian();">
                    <option value="">--Select Department--</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">custodian<span class="required">*</span></label>
                <div class="col-lg-8">
                  <select class="form-control single-select" required="" name="user_id" id="user_id">
                    <option value="">--Select Employee--</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Handover Date<span class="required">*</span></label>
                <div class="col-lg-8">
                  <input type="text" readonly="" class="form-control autoclose-datepicker-item-custodian" required="" id="handover_date" name="handover_date">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Handover Image</label>
                <div class="col-lg-8">
                  <input type="file" class="form-control photoOnly" accept="image/*" id="handover_image" name="handover_image">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Condition Type<span class="required">*</span></label>
                <div class="col-lg-8">
                  <select class="form-control" required="" name="condition_type" id="condition_type">
                    <option value="">--Select--</option>
                    <option value="old">Old</option>
                    <option value="new">New</option>

                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Remark</label>
                <div class="col-lg-8">
                  <input type="text"  class="form-control remark" id="remark" name="remark">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div class="col-md-12 text-center">
                <input type="hidden" name="handover_custodian" id="handover_custodian" value="handover_custodian" />
                <button type="submit" class="btn btn-success" id="handover_custodian" value="handover_custodian">Add</button>
              </div>
            </div>
          </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php }else if(isset($_GET['custodian']) && $_GET['custodian'] == "takeover"){ ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <h4 class="page-title">Takeover Custodian</h4>
        </div>
        <div class="col-lg-4 col-md-6">

        </div>
        <div class="col-lg-4 col-md-2">

        </div>
      </div>
      <div class="row mt-3">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
            <form id="addAssetsInventory" method="POST" class="form-horizontal" action="controller/assetsInventoryDetailsController.php" enctype="multipart/form-data">
            <?php $aq=$d ->selectRow('start_date',"assets_detail_inventory_master","inventory_id='$_GET[inventory_id]'");
                $assetsData=mysqli_fetch_assoc($aq); ?>
            <div class="modal-body pb-0">
              <input type="hidden" name="handover_date" id="handover_date" value="<?php echo date("Y-m-d", strtotime($assetsData['start_date'])); ?>">
              <input type="hidden" name="assets_id" id="assets_id" value="<?php if (isset($_GET['id']) && $_GET['id'] > 0) {echo $_GET['id'];} ?>">
              <input type="hidden" name="inventory_id" id="inventory_id" value="<?php if (isset($_GET['inventory_id']) && $_GET['inventory_id'] > 0) {echo $_GET['inventory_id'];} ?>">
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Takeover Date <span class="required">*</span></label>
                <div class="col-lg-8">
                  <input type="text" readonly="" class="form-control autoclose-datepicker-item-custodian-takeover" onchange="changesTakeDate(this.value)" required="" id="takeover_date" name="takeover_date">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Takeover Image</label>
                <div class="col-lg-8">
                  <input type="file" class="form-control photoOnly" accept="image/*" id="takeover_image" name="takeover_image">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Remark</label>
                <div class="col-lg-8">
                  <input type="text"  class="form-control remark" id="takeover_remark" name="takeover_remark">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Handover Assets</label>
                <div class="col-lg-8 col-md-8 col-8" id="">
                    <div class="form-check-inline">
                        <input class="form-check-input radio_week_off" type="radio" name="handover_assets" onchange="addhandoverAssets(this.value)" id="handover_assets_no" value="0" checked="">
                        <span class="checkmark"></span>
                        <label class="form-check-label" for="handover_assets_no">
                            No
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <input class="form-check-input radio_week_off" type="radio" onchange="addhandoverAssets(this.value)" name="handover_assets" id="handover_assets_yes" value="1">
                        <span class="checkmark"></span>
                        <label class="form-check-label" for="handover_assets_yes">
                            Yes
                        </label>
                    </div>
                </div>
              </div>
            </div>
            <div class="modal-body d-none handover_assets">
              <?php if($assetsDetailData){ ?>
              <input type="hidden" name="iteam_created_date" id="iteam_created_date" value="<?php if($assetsDetailData['end_date'] != '0000-00-00'){ echo date("Y-m-d", strtotime($assetsDetailData['end_date'])); } ?>">
              <?php }else{ ?>
                <input type="hidden" name="iteam_created_date" id="iteam_created_date" value="<?php if($assetsData['item_purchase_date'] != '0000-00-00'){ echo date("Y-m-d", strtotime($assetsData['item_purchase_date'])); } ?>">
              <?php } ?>
              <input type="hidden" name="assets_id" id="assets_id" value="<?php if (isset($_GET['id']) && $_GET['id'] > 0) {
                                                                            echo $_GET['id'];
                                                                          } ?>">
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Branch<span class="required">*</span></label>
                <div class="col-lg-8">
                    <select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required>
                        <option value="">-- Select Branch --</option> 
                        <?php 
                        $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                        while ($blockData=mysqli_fetch_array($qb)) {
                        ?>
                        <option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                        <?php } ?>
                    </select> 
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Department<span class="required">*</span></label>
                <div class="col-lg-8">
                  <select class="form-control single-select" required="" name="floor_id" id="floor_id" onchange="getCustodian();">
                    <option value="">--Select Department--</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">custodian<span class="required">*</span></label>
                <div class="col-lg-8">
                  <select class="form-control single-select" required="" name="user_id" id="user_id">
                    <option value="">--Select Employee--</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Handover Date<span class="required">*</span></label>
                <div class="col-lg-8">
                  <input type="text" readonly="" class="form-control take_handover_date" required="" id="handover_date" name="handover_date">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Handover Image</label>
                <div class="col-lg-8">
                  <input type="file" class="form-control photoOnly" accept="image/*" id="handover_image" name="handover_image">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Condition Type<span class="required">*</span></label>
                <div class="col-lg-8">
                  <select class="form-control" required="" name="condition_type" id="condition_type">
                    <option value="">--Select--</option>
                    <option value="old">Old</option>
                    <option value="new">New</option>

                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-form-label">Remark</label>
                <div class="col-lg-8">
                  <input type="text"  class="form-control remark" id="remark" name="remark">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <div class="col-md-12 text-center">
                <input type="hidden" name="takeover_custodian" id="takeover_custodian" value="takeover_custodian" />
                <button type="submit" class="btn btn-success" id="takeover_custodian" value="takeover_custodian">Add</button>
              </div>
            </div>
          </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php }else { ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-8">
        <h4 class="page-title">Inventory Item Management</h4>
      </div>
      <div class="col-sm-4">
        <form action="" method="get" id="searchbycatform">
          <select type="text" required="" onchange="this.form.submit();" name="id" id="id" class="single-select form-control" style="width: 100%">
            <option value="">Select Assets Iteam</option>
            <?php $q12 = $d->select("assets_item_detail_master,assets_category_master", "assets_category_master.assets_category_id=assets_item_detail_master.assets_category_id", "ORDER BY assets_item_detail_master.assets_name ASC");
            while ($row12 = mysqli_fetch_array($q12)) { ?>
              <option value="<?php echo $row12['assets_id']; ?>" <?php if (isset($_REQUEST) && isset($_REQUEST['id'])) {
                                                                    if ((int)$_REQUEST['id'] == $row12['assets_id']) {
                                                                      echo "selected";
                                                                    }
                                                                  } ?>><?php echo $row12['assets_name']; ?> (<?php echo $row12['assets_category']; ?>)</option>
            <?php } ?>
          </select>
        </form>
      </div>
      
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card mt-2">
          <div class="card-body">
            <h4>Summary</h4>
            <hr>
            <?php
            if (isset($id) && $id > 0) {
              if (isset($id) && $id > 0) {
                $assets_id = (int)$id;
                $appendquery = " AND assets_item_detail_master.assets_id='$assets_id'";
              }
              $q = $d->select1("assets_item_detail_master LEFT JOIN assets_detail_inventory_master ON assets_detail_inventory_master.assets_id = assets_item_detail_master.assets_id AND assets_detail_inventory_master.end_date='0000-00-00' LEFT JOIN users_master ON users_master.user_id =assets_detail_inventory_master.user_id ,assets_category_master","assets_item_detail_master.*,assets_category_master.*,users_master.user_full_name,users_master.user_designation,assets_detail_inventory_master.user_id" , "assets_item_detail_master.assets_category_id=assets_category_master.assets_category_id  $appendquery ", "");

              $data=mysqli_fetch_array($q); 
            ?>

                <div class="row ml-2">
                <div class="col-lg-8 row">
                  <div class="col-lg-6">
                    <p><label>Category : </label> <?php echo $data['assets_category']; ?></p>
                    <p><label>Item Name : </label> <?php echo $data['assets_name']; ?></p>
                    <p><label>Item Location : </label> <?php echo $data['assets_location']; ?></p>
                    <p><label>Brand : </label> <?php echo $data['assets_brand_name']; ?></p>
                    <p><label>Current Custodian : </label> <?php echo $data['user_full_name'].' ('.$data['user_designation'] .')'; ?></p>
                    <?php if ($data['assets_invoice'] != "") { ?>
                            <a target="_blank" href="../img/society/<?php echo $data['assets_invoice']; ?>" class="btn btn-primary btn-sm"><i class="fa fa-file-pdf-o"></i> View File </a>
                          <?php } ?>
                  </div>
                  <div class="col-lg-6">
                    <p><label>Item Price : </label> <?php echo $data['item_price']; ?></p>
                    <p><label>Purchase Date : </label> <?php if($data['item_purchase_date'] != '0000-00-00' && $data['item_purchase_date'] != 'null'){ echo date("d M Y", strtotime($data['item_purchase_date'])); }?></p>
                    <p><label>Item Code : </label> <?php echo $data['assets_item_code']; ?></p>
                    <p><label>Created By : </label> <?php if ($data['created_by'] > 0) {
                                                    $adminData = $d->selectArray("bms_admin_master", "admin_id='$data[created_by]' AND society_id='$society_id'");
                                                    echo $adminData['admin_name'];
                                                  } ?> </p>
                    <p><label>Created Date : </label> <?php echo date("d M Y h:i A", strtotime($data['iteam_created_date'])); ?> </p>
                  </div>
                  <?php if($data['assets_description'] != ''){ ?>
                  <div class="col-lg-12">
                    <p><label>Description : </label> <?php echo $data['assets_description']; ?></p>
                  </div>
                  <?php } ?>
                </div>
                <div class="col-lg-4">
                <?php if($data['assets_file'] != ''){ 
                  if(file_exists("../img/society/$data[assets_file]")) {
                  ?>
                  <a data-fancybox="images" data-caption="Photo Name : <?php echo $data['assets_file'] ?>" href="../img/society/<?php echo $data['assets_file'] ?>" target="_blank"><img class="lazyload img-fluid" onerror="this.src='../img/banner_placeholder.png'" src='../img/banner_placeholder.png' id="<?php echo $row['assets_id']; ?>" data-src="../img/society/<?php echo $data['assets_file']; ?>"></a>
                  <?php } } ?>
                </div>
                </div>

            <?php } ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-3">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php if (isset($id) && $id > 0) {  ?>
              <div class="table-responsive">
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Employee Name</th>
                      <th>Department</th>
                      <th>Handover Date</th>
                      <th>Handover Image</th>
                      <th>Takeover Date</th>
                      <th>Takeover Image</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i = 1;
                    if ($id > 0) {
                      $assets_id = (int)$id;
                      $appendquery = " AND assets_detail_inventory_master.assets_id='$assets_id'";
                    }

                    $q = $d->select("assets_detail_inventory_master,users_master,floors_master,block_master,user_employment_details,assets_category_master", "assets_detail_inventory_master.user_id=users_master.user_id AND assets_detail_inventory_master.floor_id=floors_master.floor_id AND assets_detail_inventory_master.user_id=user_employment_details.user_id AND assets_detail_inventory_master.assets_category_id=assets_category_master.assets_category_id AND assets_id='$id' AND block_master.block_id=floors_master.block_id"," ORDER BY inventory_id DESC");

                    while ($row = mysqli_fetch_array($q)) {
                    ?>
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $row['user_full_name']; ?> (<?php echo $row['designation']; ?>) <?php if($row['end_date'] == "0000-00-00"){ ?><i data-toggle="tooltip" title="Current Custodian" class="fa fa-check-circle text-success" aria-hidden="true"></i><?php } ?></td>
                        <td><?php echo $row['floor_name'].'-'.$row['block_name']; ?></td>
                        <td>
                          <?php if ($row['start_date'] == "0000-00-00") {
                            $row['start_date'] = '';
                          } else {
                            echo date("d M Y", strtotime($row['start_date']));
                          } ?>
                        </td>
                        <td class="text-center align-middle">
                        <?php if ($row['handover_image'] != "") { 
                          if(file_exists("../img/society/$row[handover_image]")) { ?>
                          <a data-fancybox="images" data-caption="Photo Name : <?php echo $row['handover_image'] ?>" href="../img/society/<?php echo $row['handover_image'] ?>" target="_blank">
                            <img style="max-height:100px; max-width:100px;" class="lazyload" onerror="this.src='../img/banner_placeholder.png'" src='../img/banner_placeholder.png' id="<?php echo $row['assets_id']; ?>" data-src="../img/society/<?php echo $row['handover_image']; ?>"></a>
                          <?php } } ?>
                        </td>
                        <td>
                          <?php if ($row['end_date'] == "0000-00-00") {
                            $row['end_date'] = '';
                          } else {
                            echo date("d M Y", strtotime($row['end_date']));
                          } ?>
                        </td>
                        <td class="text-center align-middle">
                        <?php if ($row['takeover_image'] != "") {
                          if(file_exists("../img/society/$row[takeover_image]")) {
                        ?>
                          <a data-fancybox="images" data-caption="Photo Name : <?php echo $row['takeover_image'] ?>" href="../img/society/<?php echo $row['takeover_image'] ?>" target="_blank">
                            <img style="max-height:100px; max-width:100px;" class="lazyload" onerror="this.src='../img/banner_placeholder.png'" src='../img/banner_placeholder.png' id="<?php echo $row['assets_id']; ?>" data-src="../img/society/<?php echo $row['takeover_image']; ?>"></a>
                          <?php } } ?>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            <?php } else {
              echo "Please Select Assets Item";
            } ?>
          </div>
        </div>
      </div>
    </div>
    <!-- ------------------------------------------ -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white" id="exampleModalLabel">Edit Custodian</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          
        </div>
      </div>
    </div>
    <!-- ----------------------------------------------------------------------------------------------- -->

  </div>
</div>
<?php } ?>
</div>
</div>
</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  function addhandoverAssets(value){
    if(value == 1){
        $('.handover_assets').removeClass('d-none');
    }
    else
    {
        $('.handover_assets').addClass('d-none');
    }
  }

  function changesTakeDate(date) {
    $('.take_handover_date').val('');
    $('.take_handover_date').datepicker('destroy');
 
  $( ".take_handover_date" ).datepicker({
     startDate: date,
    format: 'yyyy-mm-dd',
     
   })
  }
</script>
