<?php error_reporting(0);

$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-6 col-6">
                <h4 class="page-title"> Task Access</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
            <div class="btn-group float-sm-right">
               
            </div>
            </div>
        </div>   
        <form class="branchDeptFilter" action="" method="get">
            <div class="row pt-2 pb-2">
              <?php include 'selectBranchDeptEmpForFilter.php'; ?>
              <div class="col-md-3 form-group">
                <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
              </div>
            </div>
        </form>
    

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php if((isset($bId) && $bId >0) && (isset($dId) && $dId >0) )
                            {
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Branch</th>
                                        <th>Department</th>
                                        <th>Employee</th>
                                        <th>Designation</th>
                                        <th>Action</th>
                                    
                                    </tr>
                                </thead>
                                <tbody id="showFilterData">
                                    <?php
                                        if(isset($uId) && $uId > 0) {
                                            $userFilterQuery = " AND users_master.user_id=$uId";
                                        }
                                        if(isset($dId) && $dId > 0) {
                                            $dIdFilterQuery = " AND floors_master.floor_id=$dId";
                                        }
                                    $q = $d->select("users_master,block_master,floors_master", "users_master.floor_id = floors_master.floor_id AND  block_master.block_id=floors_master.block_id AND users_master.society_id='$society_id' AND users_master.delete_status= 0   $dIdFilterQuery $userFilterQuery $blockAppendQuery");
                                    $counter = 1;
                                    while ($data = mysqli_fetch_array($q)) {
                                    ?>
                                        <tr>
                                            <td><?php echo $counter++; ?></td>
                                            <td><?php echo $data['block_name']; ?></td>
                                            <td><?php echo $data['floor_name']; ?></td>
                                            <td><?php echo $data['user_full_name']; ?></td>
                                            <td><?php echo $data['user_designation']; ?></td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                <form action="addTaskAccess" method="post">
                                                    <input type="hidden" name="user_id" value="<?php echo $data['user_id'];?>">
                                                    <button type="submit" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-pencil"></i></button>
                                                </form>
                                                </div>
                                            </td>
                                        

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            
                            </table>
                            <?php }else
                            { ?>
                            <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select Department</span>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->