<?php
// error_reporting(0);
//$month_year= $_REQUEST['month_year'];
//$laYear = $_REQUEST['laYear'];
$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
$btId = (int)$_GET['btId'];
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_GET['toDate']) != 1) {
    $_SESSION['msg1'] = "Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employeeExpenseReport';
        </script>");
  }
}
if (!isset($_GET['laYear'])) {
  $filterDate = $currentYear.'-'.$currentMonth;
} else {
  $filterDate = $_GET['laYear'].'-'.$_GET['month_year'];
}

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Break Report</h4>
      </div>

    </div>

    <form action="" method="get" class="branchDeptFilter">
      <div class="row pb-2">
        <?php include('selectBranchDeptEmpForFilterAll.php');?>
        
        <div class="col-md-3 col-6">
          <select name="laYear" class="form-control" >
            <option value="">Year</option>
            <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $onePreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $currentYear) {
                      echo 'selected';
                    } ?> <?php if ($_GET['laYear'] == '') { echo 'selected';} ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
            <option <?php if ($_GET['laYear'] == $nextYear) {
                      echo 'selected';
                    } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear; ?></option>
          </select>
        </div>
        <div class="col-md-3 col-6">
          <select class="form-control month_year single-select" name="month_year" required>
                <option value="">--Select Month --</option>
                <option <?php if($_GET['month_year']=="01") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '01') { echo 'selected';}?> value="01">January</option>
                <option <?php if($_GET['month_year']=="02") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '02') { echo 'selected';}?> value="02">February</option>
                <option <?php if($_GET['month_year']=="03") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '03') { echo 'selected';}?> value="03">March</option>
                <option <?php if($_GET['month_year']=="04") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '04') { echo 'selected';}?> value="04">April</option>
                <option <?php if($_GET['month_year']=="05") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '05') { echo 'selected';}?> value="05">May</option>
                <option <?php if($_GET['month_year']=="06") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '06') { echo 'selected';}?> value="06">June</option>
                <option <?php if($_GET['month_year']=="07") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '07') { echo 'selected';}?> value="07">July</option>
                <option <?php if($_GET['month_year']=="08") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '08') { echo 'selected';}?> value="08">August</option>
                <option <?php if($_GET['month_year']=="09") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '09') { echo 'selected';}?> value="09">September</option>
                <option <?php if($_GET['month_year']=="10") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '10') { echo 'selected';}?> value="10">October</option>
                <option <?php if($_GET['month_year']=="11") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '11') { echo 'selected';}?> value="11">November</option>
                <option <?php if($_GET['month_year']=="12") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '12') { echo 'selected';}?> value="12">December</option> 
            </select> 
        </div>
        <div class="col-md-3 col-6">
            <select class="form-control break_type single-select" name="btId" required>
            <option value="0" selected="" data-select2-id="">All Break</option>
                    <?php 
                    $qb = $d->selectRow("attendance_type_name,attendance_type_id", "attendance_type_master", "attendance_type_delete= 0  AND attendance_type_delete= 0");
                    while ($breakData = mysqli_fetch_array($qb)) {
                    ?>
                        <option <?php if($btId == $breakData['attendance_type_id']){echo 'selected';} ?> value="<?php echo $breakData['attendance_type_id']; ?>"><?php echo $breakData['attendance_type_name']; ?></option>
                    <?php } ?>
            </select> 
        </div>
        

        
        <input type="hidden" name="">
        
        <div class="col-lg-2 from-group col-6">
          <input  class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Report">
        </div>


      </div>
    </form>
    <!-- End Breadcrumb-->
    <input type="hidden" name="floor_id_old" id="floor_id_old" value="<?php if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                                                                              echo $_GET['dId'];
                                                                            } ?>">

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                <div class="card-body">
                    <div class="table-responsive">
                    <?php
                    extract(array_map("test_input", $_GET));

                    if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                        $deptFilterQuery = " AND users_master.floor_id='$_GET[dId]'";
                    }
                    if (isset($uId) && $uId > 0) {
                        $userFilterQuery = " AND attendance_master.user_id='$uId'";
                    }
                    
                    if(isset($filterDate) && (int)$filterDate>0 ) {
                        $MonthFilterQuery = " AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y-%m')='$filterDate'";
                    }

                    if(isset($btId) && $btId>0 ) {
                        $breakTypeQuery = " AND attendance_break_history_master.attendance_type_id='$btId'";
                    }
                    
                    $q = $d->selectRow("attendance_master.attendance_id, attendance_master.user_id,attendance_master.society_id, floors_master.floor_name, block_master.block_name, users_master.user_full_name,users_master.user_designation, attendance_break_history_master.*, attendance_type_master.*","attendance_master,users_master,floors_master,block_master, attendance_type_master, attendance_break_history_master", "attendance_master.attendance_id=attendance_break_history_master.attendance_id AND attendance_type_master.attendance_type_id=attendance_break_history_master.attendance_type_id AND users_master.user_id=attendance_master.user_id AND attendance_master.society_id='$society_id' AND attendance_master.attendance_status !=0 AND floors_master.floor_id = users_master.floor_id AND block_master.block_id = users_master.block_id  $MonthFilterQuery $blockAppendQueryFloor $deptFilterQuery $userFilterQuery $breakTypeQuery","ORDER BY attendance_break_history_master.break_start_date DESC , users_master.user_full_name ASC");

                    $i = 1;
                    ?>

                        <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee</th>
                                    <th>Designation</th>
                                    <th>Branch/Departement</th>
                                    <th>Break Type</th>
                                    <th>Break In Date</th>
                                    <th>Break In Time</th>
                                    <th>Break Out Date</th>
                                    <th>Break Out Time</th>
                                    <th>Total Hours</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                while ($data = mysqli_fetch_array($q)) {
                                ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $data['user_full_name']; ?></td>
                                        <td><?php echo $data['user_designation']; ?></td>
                                        <td><?php echo $data['block_name'].'-'.$data['floor_name']; ?></td>
                                        <td><?php echo $data['attendance_type_name']; ?></td>
                                        <td><?php if ($data['break_start_date'] != '0000-00-00' && $data['break_start_date'] != 'null') {
                                            echo date("d M Y", strtotime($data['break_start_date']))." (".date("D", strtotime($data['break_start_date'])).")";
                                            } ?></td>
                                        <td><?php if ($data['break_in_time'] != '00:00:00' && $data['break_in_time'] != 'null') {
                                            echo date("h:i A", strtotime($data['break_in_time']));
                                            } ?></td>
                                        <td><?php if ($data['break_end_date'] != '0000-00-00' && $data['break_end_date'] != 'null') {
                                            echo date("d M Y", strtotime($data['break_end_date']))." (".date("D", strtotime($data['break_end_date'])).")";
                                            } ?></td>
                                        <td><?php if ($data['break_out_time'] != '00:00:00' && $data['break_out_time'] != 'null') {
                                            echo date("h:i A", strtotime($data['break_out_time']));
                                            } ?></td>
                                        <td><?php if ($data['break_in_time'] != '00:00:00' && $data['break_out_time'] != '00:00:00' && $data['total_working_hours']=='') {
                                            echo $d->getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],$data['break_in_time'],$data['break_out_time']);
                                            } else {
                                                echo $data['total_working_hours'];
                                            } ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-    fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>
