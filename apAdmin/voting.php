<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Building Voting</h4>
        <ol class="breadcrumb">
             <li class="breadcrumb-item"><a href="welcome">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Voting</li>
         </ol>
     </div>
    
     </div>
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="signupForm">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-bullhorn"></i>
                   Building Voting
                </h4>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label">Voting Question</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input-10" name="voting_question">
                  </div>
                  
                  <div class="col-sm-4">
                   
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label">Voting Description</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="input-12" name="voting_description">
                  </div>
                  
                </div>
               

                <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label">Voting Stat Date</label>
                  <div class="col-sm-4">
                    <input type="date" class="form-control" id="input-14" name="voting_start_date">
                  </div>
                  <label for="input-14" class="col-sm-2 col-form-label">Voting End Date</label>
                  <div class="col-sm-4">
                    <input type="date" class="form-control" id="input-14" name="voting_end_date">
                  </div>
                  
                </div>
                
                
                
                 <div class="form-group row">
                 
                </div> 
                <div class="form-footer text-center">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->