<?php error_reporting(0);
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-8 col-6">
                <h4 class="page-title">Face App admin</h4>
            </div>
            <div class="col-sm-4 col-6">
                <div class="form-group row w-100 mx-0">
                    <div class="col-lg-12 col-md-12 col-12" id="">
                        <select  type="text" required="" class="form-control " name="admin" id="admin" onchange="changeStatus(this.value,'adminAttendanceTakingOn');">
                            <option value="">-- select admin for face app login --</option> 
                            <?php 
                                $admin=$d->select("role_master,bms_admin_master,society_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.society_id='$society_id' AND role_master.role_id>2 AND bms_admin_master.is_attendance_taking_on=0 AND bms_admin_master.admin_active_status=0 ","");  
                                while ($adminData=mysqli_fetch_array($admin)) {
                            ?>
                            <option value="<?php echo $adminData['admin_id']; ?>"><?php echo $adminData['admin_name']; ?></option> 
                            <?php } ?>
                        </select>                
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Admin Name</th>
                                    <th>Mobile</th>
                                    <th>Admin Pin</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody id="showFilterData">
                                <?php
                                $i=1;

                                $q = $d->select("bms_admin_master","is_attendance_taking_on= 1");
                                $counter = 1;
                                while ($data = mysqli_fetch_array($q)) {
                                ?>
                                    <tr>

                                       
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['admin_name']; ?></td>
                                        <td><?php echo $data['country_code'].' '.$data['admin_mobile']; ?></td>
                                        <td><?php if($data['admin_pin'] >0) {echo $data['admin_pin'];} ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <?php if ($data['is_attendance_taking_on'] == "1") {
                                                ?>
                                                <button class="btn btn-danger btn-sm" onclick="deleteFaceAppDevice('<?php echo $data['admin_id']; ?>','adminAttendanceTakingOff');"><i class="fa fa-trash"></i></button>
                                                <?php } 
                                                 if ($data['admin_pin'] == "" || $data['admin_pin'] == "0") { ?>
                                                <button type="button" class="btn btn-sm btn-primary ml-1" onclick="adminChangePin(<?php echo $data['admin_id']; ?>)" >Add Pin </button>
                                                <?php }else{ ?>
                                                    <button type="button" class="btn btn-sm btn-info ml-1" onclick="adminChangePin(<?php echo $data['admin_id']; ?>)" >Change Pin </button>
                                                <?php } ?> 
                                            </div>
                                        </td>

                                    </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9 col-6">
        <h4 class="page-title">Face app Device</h4>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Device Mac</th>
                                    <th>App Version</th>
                                    <th>Device Model</th>
                                    <th>Device Location</th>
                                    <th>Last Sync Date</th>
                                    <th>Admin</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="showFilterData">
                                <?php
                                $i=1;
                                $q2 = $d->selectRow("face_app_device_master.*,bms_admin_master.admin_name","face_app_device_master LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=face_app_device_master.admin_id","face_app_device_master.society_id= '$society_id'");
                                $counter = 1;
                                while ($data2 = mysqli_fetch_array($q2)) {
                                ?>
                                <tr>
                                    <td><?php echo $counter++; ?></td>
                                    <td><?php echo $data2['device_mac']; ?></td>
                                    <td><?php echo $data2['app_version']; ?></td>
                                    <td><?php echo $data2['device_model']; ?></td>
                                    <td><?php echo $data2['device_location']; ?></td>
                                    <td><?php if($data2['last_syc_date'] !="0000-00-00 00:00:00"){ echo date('d-M-Y H:i A',strtotime($data2['last_syc_date'])); } ?></td>
                                    <td><?php echo $data2['admin_name']; ?></td>
                                    <td> 
                                        <div class="d-flex align-items-center">
                                            <button data-toggle="tooltip" title="Edit" type="button" class="btn btn-sm btn-info ml-1" onclick="editFaceAppDevice(<?php echo $data2['face_app_device_id']; ?>)" ><i class="fa fa-pencil"></i></button>
                                            <button type="button" class="btn btn-sm btn-primary ml-1" onclick="showFaceAppDevice(<?php echo $data2['face_app_device_id']; ?>)" ><i class="fa fa-eye"></i></button>
                                            <?php if($data2['user_token'] !=""){ ?>
                                            <form method="post" id="logoutFrm" action="controller/AdminPinController.php">
                                                <input type="hidden" name="logoutFaceApp" value="logoutFaceApp">
                                                <input type="hidden" name="face_app_device_id" value="<?php echo $data2['face_app_device_id']; ?>">
                                            <button type="button" data-toggle="tooltip" title="Logout" onclick="logoutConfirm()" class="btn btn-sm btn-danger ml-1" ><i class="fa fa-sign-out" aria-hidden="true"></i></button>
                                            </form>

                                             <form method="post" id="reloadDevice" action="controller/AdminPinController.php">
                                                <input type="hidden" name="reloadDevice" value="reloadDevice">
                                                <input type="hidden" name="face_app_device_id" value="<?php echo $data2['face_app_device_id']; ?>">
                                            <button type="button" data-toggle="tooltip" title="Reload Device"  class="btn btn-sm btn-warning ml-1 form-btn" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                            </form>
                                             <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row-->

</div>
<!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->

<div class="modal fade" id="addPinModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Admin Pin</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body">  
            <form id="addAdminPinForm" action="controller/AdminPinController.php" enctype="multipart/form-data" method="post"> 
                <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Admin Pin <span class="required">*</span></label>
                    <div class="col-lg-8 col-md-8" id="">
                        <input type="text" maxlength="4" class="form-control" placeholder="Admin Pin" id="admin_pin" name="admin_pin" value="">
                    </div> 
                </div> 
                <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Confirm Pin <span class="required">*</span></label>
                    <div class="col-lg-8 col-md-8" id="">
                        <input type="text" maxlength="4" class="form-control" placeholder="Confirm Pin" id="confirm_admin_pin" name="confirm_admin_pin" value="">
                    </div> 
                </div>    
                <div class="form-footer text-center">
                    <input type="hidden" id="admin_id" name="admin_id" value="">
                    <button id="addAdminPinBtn" type="submit"  class="btn btn-success addAdminPinbtn "><i class="fa fa-check-square-o"></i> Set Pin </button>
                    <input type="hidden" name="addAdminPin"  value="addAdminPin">
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('leaveAssignAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                </div>
            </form>
        </div>
        </div>
        
        </div>
    </div>
</div>
<div class="modal fade" id="editFaceAppDeviceModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Change Face App Device</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body">  
            
            <form id="editFaceApppDevice" action="controller/AdminPinController.php" enctype="multipart/form-data" method="post"> 
                 
                <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch </label>
                    <div class="col-lg-8 col-md-8" id="">
                        <select name="block_ids[]" multiple id="block_id" class="form-control single-select" onchange="getFLoorByMultiBLockIds()">
                            <option >-- Select Branch --</option> 
                            <?php 
                            $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                            while ($blockData=mysqli_fetch_array($qb)) {
                            ?>
                            <option   <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                            <?php } ?>
                        </select>
                    </div> 
                </div> 
                <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Department </label>
                    <div class="col-lg-8 col-md-8" id="">
                        <select name="floor_ids[]" multiple id="floor_id" class="form-control single-select" >
                        </select>
                    </div> 
                </div>    
                <div class="form-group row">
                    <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Device Location <span class="required">*</span></label>
                    <div class="col-lg-8 col-md-8" id="">
                       <input type="text" name="device_location" class="form-control" id="device_location" >
                    </div> 
                </div>    
                <div class="form-footer text-center">
                    <input type="hidden" id="face_app_device_id" name="face_app_device_id" value="">
                    <button id="addAdminPinBtn" type="submit"  class="btn btn-success  "><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="editFaceAppDevice"  value="editFaceAppDevice">
                    <button type="button"   class="btn btn-danger " onclick="resetForm();"><i class="fa fa-echck-square-o"></i> Reset</button>
                </div>
            </form>
        </div>
        </div>
        
        </div>
    </div>
</div>
<div class="modal fade" id="DetailFaceAppDeviceModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
            <h5 class="modal-title text-white"> Face App Device</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body"  style="align-content: center;">
        <div class="card-body">  
            <div class="row  showDetail">
            </div>
            <div class="col-md-12 col-12 text-center">
                <div class="map" id="map" style="width: 100%; height: 300px;"></div>
            </div>
        </div>
        </div>
        
        </div>
    </div>
</div>

<script src="assets/js/jquery.min.js"></script>

<script src="assets/plugins/select2/js/select2.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>


<script>
    $('#admin').select2();
    function resetForm() {
        $('#floor_id').val('');
        $('#block_id').val('');
        $('#floor_id').select2();
        $('#block_id').select2();
    }
function getFaceAppDevice(d_lat, d_long) {

var latlng = new google.maps.LatLng(d_lat, d_long);

var latitute = d_lat;
var longitute = d_long;

var map = new google.maps.Map(document.getElementById('map'), {
  center: latlng,
  zoom: 13
});
////User marker
var marker = new google.maps.Marker({
  map: map,
  icon: "assets/images/placeholder.png",
  position: latlng,
  // draggable: true,
  anchorPoint: new google.maps.Point(0, -29)
});
var infowindow = new google.maps.InfoWindow();
}

</script>