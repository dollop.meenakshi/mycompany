<?php 


if(!isset($_GET['id'])){
  $_SESSION['msg1'] = "Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='workReportTemplate';
        </script>");
}
$template_id = (int)$_REQUEST['id'];
$type = $_REQUEST['type'];
if(isset($template_id) && $template_id > 0){
  $q=$d->select("template_question_master,template_master","template_question_master.template_id=template_master.template_id AND template_question_master.society_id='$society_id' AND template_master.template_id='$template_id'");
}
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Work Report Template</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <?php if(isset($template_id) && $template_id > 0 && $type == 'edit'){ ?>
            <a href="addTemplateQuestion?id=<?php echo $template_id; ?>" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <?php } ?>
          </div>
        </div>
     </div>
      <?php if(isset($template_id) && $template_id > 0){ 
        if(mysqli_num_rows($q) > 0){?>
      <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php 
              $counter = 1;
                while ($data=mysqli_fetch_array($q)) { ?>
                    <div class="d-flex">
                      <h6><?php echo $counter++.') '.$data['template_question'] ?>
                      <?php if($data['is_required'] == 0){ echo '<span class="required">*</span>';} ?>
                      </h6>
                      <?php if($type == 'edit'){ ?>
                        <div class="d-flex ms-auto">
                          <form action="addTemplateQuestion" method="post">
                              <input type="hidden" name="edit_template_question" value="edit_template_question">
                              <input type="hidden" name="template_question_id" value="<?php echo $data['template_question_id']; ?>">
                              <button type="submit" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-pencil"></i></button>
                          </form>

                          <form action="controller/WorkReportTemplateController.php" method="post">
                              <input type="hidden" name="delete_template_question" value="delete_template_question">
                              <input type="hidden" name="template_question_id" value="<?php echo $data['template_question_id']; ?>">
                              <input type="hidden" name="template_id" value="<?php echo $data['template_id']; ?>">
                              <button type="submit" class="btn btn-sm btn-danger  mr-2 form-btn"><i class="fa fa-trash-o fa-lg"></i></button>
                          </form>
                        </div>
                      <?php } ?>
                    </div>
                    <div class="ml-4">
                        <?php if($data['question_type'] == 0){ ?>
                          <div class="form-group col-md-4">
                            <textarea type="text" class="form-control" placeholder="Your Answer"></textarea>
                          </div>
                        <?php }elseif($data['question_type'] == 1){ 
                            $optionValueArr = json_decode($data['question_type_value'], true);
                            if (isset($optionValueArr)) {
                              for ($i=0; $i < count($optionValueArr); $i++) {
                                 ?>
                                <input type="radio" id="<?php echo 'radio_'.$i.$data['template_question_id']; ?>" name="radio" value="<?php echo $optionValueArr['option_'.$i] ?>">
                                <label for="<?php echo 'radio_'.$i.$data['template_question_id']; ?>"><?php echo $optionValueArr['option_'.$i] ?></label><br>
                              <?php } 
                            } ?>
                        <?php }elseif($data['question_type'] == 2){
                            $optionValueArr = json_decode($data['question_type_value'], true);
                            for ($i=0; $i < count($optionValueArr); $i++) {
                               ?>
                              <input type="checkbox" id="<?php echo 'checkbox_'.$i.$data['template_question_id']; ?>" name="checkbox" value="<?php echo $optionValueArr['option_'.$i] ?>">
                              <label for="<?php echo 'checkbox_'.$i.$data['template_question_id']; ?>"><?php echo $optionValueArr['option_'.$i] ?></label><br>
                            <?php } ?>
                        <?php }elseif($data['question_type'] == 3){ ?>
                          <div class="form-group col-md-4">
                            <select type="text" class="form-control">
                            <option value="">Dropdown</option> 
                            <?php $optionValueArr = json_decode($data['question_type_value'], true);
                            for ($i=0; $i < count($optionValueArr); $i++) {
                               ?>
                              <option value="<?php echo $i; ?>"><?php echo $optionValueArr['option_'.$i] ?></option> 
                            <?php } ?>
                            </select>   
                            </div>
                        <?php }elseif($data['question_type'] == 4){ ?>
                          <div class="form-group col-md-4">
                            <input type="file" class="form-control">
                          </div>
                        <?php }elseif($data['question_type'] == 5){ ?>
                          <div class="form-group col-md-4">
                            <input type="text" class="form-control template-datepicker" readonly>
                          </div>
                        <?php }elseif($data['question_type'] == 6){ ?>
                          <div class="form-group col-md-4">
                            <input type="text" class="form-control template-timepicker" readonly>
                          </div>
                       <?php }elseif($data['question_type'] == 7){ ?>
                          <div class="form-group col-md-4">
                            <input type="text" class="form-control template-datetimepicker" readonly>
                          </div>
                        <?php }elseif($data['question_type'] == 8){ ?>
                          <div class="form-group col-md-4">
                            <input type="text" class="form-control onlyNumber" maxlength="10">
                          </div>
                         <?php }elseif($data['question_type'] == 9){ ?>
                          <div class="form-group col-md-4">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped" style="width:40%"></div>
                            </div> 
                          </div>
                        <?php }elseif($data['question_type'] == 10){ ?>
                          <div class="form-group "> 
                            <div class="col-md-4">
                              <input type="text" class="form-control mb-1" placeholder="Topic Name">

                             <input type="text" class="form-control onlyNumber" placeholder="Time">
                          </div>
                          </div>
                        <?php }elseif($data['question_type'] == 11){ ?>
                          <div class="form-group "> 
                            <div class="col-md-4">
                              <textarea class="form-control mb-1" placeholder=""></textarea>

                              <input type="text" class="form-control onlyNumber" placeholder="Time">
                          </div>
                        </div>
                        <?php } ?>
                      </div>
                    <?php if($type == 'edit'){ ?>
                      <?php } ?>
                <?php } ?>
              </div>
            </div>
          </div>
        </div><!-- End Row-->
      <?php }else{ ?>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <p>Please Configure your template question</p>
              </div>
            </div>
          </div>
        </div>
      <?php } }?>
    </div>
    <!-- End container-fluid-->
    
    </div>
<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Template</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addTemplateForm" action="controller/WorkReportTemplateController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Template Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                  <input type="text" class="form-control" placeholder="Template Name" id="template_name" name="template_name" value="">
              </div>  
           </div>                                      
           <div class="form-footer text-center">
             <input type="hidden" id="template_id" name="template_id" value="" >
             <button id="addTemplateBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addTemplate"  value="addTemplate">
             <button id="addTemplateBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>