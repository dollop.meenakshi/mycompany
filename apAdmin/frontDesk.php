<?php 
if(isset($_POST["user_id"])){
  $q=$d->select("users_entry_login","user_entry_id='". $_POST["user_id"] ."'");
  $data = $q->fetch_assoc();
  // echo "<pre>"; print_r($data);
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Add User</h4>
        <?php //echo $xml->string->balancesheet; 
        ?>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="userAdd" action="controller/frontDeskController.php" method="post" enctype="multipart/form-data">
              <input type="hidden" name="user_id" value="<?php echo $data['user_entry_id']?>">
              <h4 class="form-header text-uppercase">
                <i class="fa fa-address-book-o"></i><?php if(isset($data['block_id'])){?>Edit User<?php }else{ echo "Add User";}?>  
              </h4>
              <div class="form-group row">
                <label for="input-15" class="col-sm-3 col-form-label">User Name <span class="required">*</span></label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $data['login_name']?>" class="form-control" id="user_name" name="user_name">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-15" class="col-sm-3 col-form-label">User Mobile <span class="required">*</span></label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $data['login_mobile']?>" class="form-control number" maxlength="10" id="user_mobile" name="user_mobile">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-15" class="col-sm-3 col-form-label">User Password<span class="required">*</span></label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $data['login_password']?>" class="form-control " id="user_password" name="user_password">
                </div>
              </div>
              <div <?php if(isset($data['block_id'])){?>style="display: none;" <?php } ?>  class="form-group row">
                <label for="input-15" class="col-sm-3 col-form-label">Branch<span class="required">*</span></label>
                <div class="col-sm-9">
                  <select class="form-control select2" <?php if(isset($data['block_id'])){?>style="display: none;" <?php } ?>id="branch_id" name="branch_id">
                    <option value="">Please Select</option>
                    <?php $q = $d->select("block_master");
                    while ($res = $q->fetch_assoc()) {  ?>
                      <option <?php  if($data['block_id']==$res['block_id']){
                        echo "selected";
                      }?> value="<?php echo $res['block_id'];?>"><?php echo $res['block_name'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-15" class="col-sm-3 col-form-label">User Profile </label>
                <div class="col-sm-9">
                  <input type="File" value="" class="form-control-file border" accept="image/*" id="user_pic" name="user_pic">
                  <input type="hidden" value="<?php echo $data['login_profile']?>" id="old_profile" name="old_profile">
                 <?php if(isset($data['block_id']) && $data['login_profile']!=""){?>
                  <img src="../commercial/img/profile/<?php echo $data['login_profile']?>" alt="Profile" class="lightbox-thumb img-thumbnail" style="width:200px;height:150px;"><br>
                  <?php }?>
                  <span id="addClass" class=""><strong>Note :</strong> The maximum file size is 2MB</span>
                </div>
              </div>
              <div class="form-footer text-center">
                <input type="hidden" value="" id="addUser" name="addUser">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!--End Row-->
  </div>
  <!-- End container-fluid-->
</div>
<!--End content-wrapper-->
<script>
</script>