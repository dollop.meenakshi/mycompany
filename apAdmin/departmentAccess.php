<?php error_reporting(0);
$bId = (int)$_REQUEST['bId'];?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-3 col-md-6 col-6">
			<h4 class="page-title">Department Restrict</h4>
			</div>
			<div class="col-sm-3 col-md-6 col-6">
			<div class="btn-group float-sm-right">
				
			</div>
			</div>
		</div>

		<form class="branchDeptFilter" action="" method="get">
			<div class="row pt-2 pb-2">
				<div class="col-md-3 form-group">
					<select name="bId" class="form-control single-select" onchange="this.form.submit();">
						<option value="">-- Select Branch --</option> 
						<?php 
						$qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
						while ($blockData=mysqli_fetch_array($qb)) {
						?>
						<option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
						<?php } ?>

					</select>         
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-bordered">
								<thead>
									<tr>
										<th>Sr.No</th>                        
										<th>Branch</th>
										<th>Department</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$i=1;
									if(isset($bId) && $bId>0) {
										$blockFilterQuery = " AND floors_master.block_id='$bId'";
									}
									$q = $d->select("floors_master,block_master","block_master.block_id = floors_master.block_id AND floors_master.society_id='$society_id' $blockAppendQuery $blockAppendQueryFloor $blockFilterQuery ");
								
									$counter = 1;
									while ($data=mysqli_fetch_array($q)) {
									
									?>
									<tr>
										<td><?php echo $counter++; ?></td>
										<td><?php echo $data['block_name']; ?></td>
										<td><?php echo $data['floor_name']; ?></td>
										<td>
											<div class="d-flex align-items-center">
												<form action="addDepartmentAccess" method="POST">
													<input type="hidden" name="floor_id" value="<?php echo $data['floor_id'] ?>">
													<button type="submit" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-pencil"></i></button>
												</form>
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>

