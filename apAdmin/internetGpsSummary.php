  <?php error_reporting(0);

  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year'] = $_REQUEST['month_year'];
  if (!isset($_REQUEST['laYear'])) {
    $laYear = $currentYear;
  } else {
    $laYear = $_REQUEST['laYear'];
  }
  if (!isset($_REQUEST['month'])) {
    $cmonth = date('m');
  } else {
    $cmonth = $_REQUEST['month'];
  }

  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row ">
        <div class="col-sm-9 col-12">
          <h4 class="page-title">GPS/Internet On/Off Summary</h4>
        </div>
        <div class="col-sm-3 col-12 text-right">
          <!-- <a class="btn btn-warning btn-sm" href="travelKmReport">View Report</a> -->
        </div>
      </div>
      <form action="" class="branchDeptFilter">
        <div class="row ">
          <?php include 'selectBranchDeptEmpForFilterAll.php' ?>

          <div class="col-md-2 col-6 form-group">
            <select class="form-control single-select"  name="month" id="month">
              <option value="">-- Select --</option> 
              <?php
              $selected = "";
              for ($m = 1; $m <= 12; $m++) {
                $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
                if (isset($_GET['month'])  && $_GET['month'] !="") {
                  if($_GET['month'] == $m)
                  {
                    $selected = "selected";
                  }else{
                    $selected = "";
                  }
                  
                } else {
                  $selected = "";
                  if($m==date('n'))
                  { 
                    $selected = "selected";
                  }
                  else
                  {
                    $selected = "";
                  }
                }
              ?>
              <option  <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $month; ?></option> 
            <?php }?>
            </select>  
          </div>
          <div class="col-md-2 col-6 form-group">
            <select name="laYear" class="form-control single-select ">
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $twoPreviousYear) {
                          echo 'selected';
                        } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $onePreviousYear) {
                          echo 'selected';
                        } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $currentYear) {
                          echo 'selected';
                        }else{ echo ''; } ?> <?php if (!isset($_GET['laYear'])) {
                                echo 'selected';
                              } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $nextYear) {
                          echo 'selected';
                        } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
              </select>
          </div>          
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get">
            </div>
        </div>
      </form>
      <div class="row mt-2">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php
                $i = 1;
                if (isset($dId) && $dId > 0) {
                  $deptFilterQuery = " AND users_master.floor_id='$dId'";
                }

                if(isset($bId) && $bId>0) {
                  $blockFilterQuery = " AND users_master.block_id='$bId'";
                }
                if (isset($laYear) && isset($laYear) && $cmonth != '' && $cmonth) {
                   $monthYear = $laYear.'-'.$cmonth;
                  $dateFilterQuery = " AND DATE_FORMAT(user_gps_on_off.created_date,'%Y-%m') = '$monthYear'";
                }

                if (isset($uId) && $uId > 0) {
                  $userFilterQuery = "AND user_gps_on_off.user_id='$uId'";
                }

                $q = $d->selectRow("user_gps_on_off.*,users_master.*,block_master.block_name,floors_master.floor_name","user_gps_on_off,users_master,block_master,floors_master 
               ", " users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.user_id=user_gps_on_off.user_id AND users_master.society_id='$society_id' AND users_master.delete_status=0  $blockFilterQuery $deptFilterQuery $dateFilterQuery $userFilterQuery $blockAppendQueryUser", "ORDER BY user_gps_on_off.user_gps_on_off_id DESC");
               
                $counter = 1;
             
                ?>
                  <table id="example" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Sr.No</th>
                        <th>Name</th>
                        <th>Branch (Department)</th>
                        <th>Type</th>
                        <th>Action</th>
                        <th>Latitude</th>
                        <th>Logitude</th>
                        <th>Date </th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      while ($data = mysqli_fetch_array($q)) {
                        if ($data['type'] ==1 ) {
                            $type = "GPS";
                          } else {
                            $type = "Internet";
                          }

                      ?>
                        <tr>

                          <td><?php echo $counter++; ?></td>
                          <td><?php echo $data['user_full_name']; ?></td>
                          <td><?php echo $data['block_name']; ?> (<?php echo $data['floor_name']; ?>)</td>
                          <td><?php  echo $type; ?> </td>
                          <td><?php  echo $data['gps_status']; ?></td>
                          <td><?php if($data['latitude']!='0.0') {  echo $data['latitude']; } ?></td>
                          <td><?php if($data['longitude']!='0.0') {  echo $data['longitude'];  }?></td>
                          <td><?php 
                              echo date("d M Y h:i A", strtotime($data['created_date']));
                             ?>
                          </td>

                          
                        </tr>
                      <?php } ?>
                       
                    </tbody>
                  </table>
                
              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
