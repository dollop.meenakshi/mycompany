<script type="text/javascript" src="assets/js/select.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom16.js"></script>
<script>
  function showLeaveTypeDetailOption(value){
    if($('#groupLeaveType'+value).prop('checked')==true){
      $('.groupLeaveTypeShow'+value).removeClass('d-none');
    }else{
      $('#number_of_leaves_'+value).val('');
      $('.groupLeaveTypeShow'+value).addClass('d-none');
    }
  }
</script>
<?php
$currentYear = date('Y');
$btnValue = "Add";
$addquery= "addBulkLeaveDefaultCount";
$lgId = (int)$_REQUEST['lgId'];
extract(array_map("test_input", $_POST));
if (isset($lgId) && $lgId > 0) {
  $q = $d->select("leave_group_master", "leave_group_id='$lgId'");
  $data = mysqli_fetch_array($q);
  extract($data);
}
?>
<style>
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
</style>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Leave Group </h4>
      </div>
    </div>

    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="addLeaveDefaultCountForm" action="controller/LeaveCountController.php" enctype="multipart/form-data" method="post">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Leave Group Name <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12">
                      <input type="text" id="leave_group_name" required="" class="form-control" name="leave_group_name" value="<?php if (isset($leave_group_name)) {echo $leave_group_name;} ?>">
                      <input type="hidden" id="lgId" value="<?php echo $lgId; ?>">
                    </div>
                  </div>
                </div>
              </div>
              <?php
              $q=$d->select("leave_type_master","society_id='$society_id' AND leave_type_delete=0 AND leave_type_active_status=0");
              $i=0;
              while ($data = mysqli_fetch_array($q)) {
              ?>
                <div class="row">
                  <div class="col-md-12">
                      <div class="form-group row w-100 mx-0">
                          
                          <div class="col-lg-4 col-md-4" id="">
                              <?php
                              $ldcq = $d->select("leave_default_count_master","leave_type_id='$data[leave_type_id]' AND leave_group_id='$lgId'");
                              $ldcqData = mysqli_fetch_array($ldcq);
                              if($ldcqData['number_of_leaves']!=''){
                                $btnValue = "Update";
                                $addquery = "updateLeaveDefaultCount";
                              }
                              ?>
                              <input type="checkbox" class="leaveTypeCheck" id="groupLeaveType<?php echo $i;?>" onchange="showLeaveTypeDetailOption(<?php echo $i;?>)" <?php if(isset($ldcqData) && !empty($ldcqData)){ echo 'checked'; } ?>>
                              <label for="groupLeaveType<?php echo $i;?>"> <?php echo $data['leave_type_name']; ?></label>
                              <input type="hidden" class="form-control" name="leave_type_id[<?php echo $i;?>]" id="leave_type_id_<?php echo $i;?>" value="<?php echo $data['leave_type_id']; ?>">
                          </div>
                      </div>
                  </div>
                  <div class="row m-0 groupLeaveTypeShow<?php echo $i;?> <?php if(!isset($ldcqData) && empty($ldcqData)){ echo 'd-none'; } ?>">
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12  col-md-12 col-form-label">No Of Leaves</label>
                            <div class="col-lg-12 col-md-12" id="">
                                <input step="0.5" min="0" type="text" class="form-control onlyNumber number_of_leaves getLeaveTypeList" name="number_of_leaves[<?php echo $i;?>]" id="number_of_leaves_<?php echo $i;?>" max="365" value="<?php echo $ldcqData['number_of_leaves']; ?>" placeholder="No Of Leave" onkeyup="setUseInMonthLeaves(<?php echo $i;?>)">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Use In Month</label>
                            <div class="col-lg-12 col-md-12" id="">
                                <select class="form-control single-select" name="leaves_use_in_month[<?php echo $i;?>]" id="leaves_use_in_month_<?php echo $i;?>">
                                <?php if(!isset($ldcqData)){ ?>
                                    <option value="">--Select Use In Month--</option>
                                <?php } ?>
                                    <?php $x= 1;
                                        for($a=($x-0.5); $a<=$ldcqData['number_of_leaves']; $a+=0.5){ ?>
                                            <option <?php if($ldcqData['leaves_use_in_month']==$a){echo 'selected';} ?> value="<?php echo $a; ?>"><?php echo $a; ?></option>
                                    <?php } ?>
                                </select>
                                <!-- <input type="text" class="form-control onlyNumber" name="leaves_use_in_month[]" max="30" value="<?php if($ldcqData['leaves_use_in_month']>0){echo $ldcqData['leaves_use_in_month'];} ?>" placeholder="Use Leave In Month"> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Carry Forward</label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select  type="text" required="" class="form-control" id="carry_forward_to_next_year_<?php echo $i;?>" onchange="carryForward(<?php echo $i;?>)" name="carry_forward_to_next_year[<?php echo $i;?>]" >
                                    <option <?php if(isset($ldcqData) && $ldcqData['carry_forward_to_next_year'] == 1){echo "selected";} ?> value="1">No</option>
                                    <option <?php if(isset($ldcqData) && $ldcqData['carry_forward_to_next_year'] == 0){echo "selected";} ?> value="0">Yes</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-4 <?php if(isset($ldcqData) && $ldcqData['carry_forward_to_next_year'] == 0){echo '';}else{echo 'd-none';} ?> carryForward<?php echo $i;?>">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Maximum Carry Forward</label>
                            <div class="col-lg-12 col-md-12" id="">
                                <select class="form-control single-select" name="max_carry_forward[<?php echo $i;?>]" id="max_carry_forward_<?php echo $i;?>" onchange="setMinCarryForward(<?php echo $i;?>)">
                                <?php if(!isset($ldcqData)){ ?>
                                    <option value="">--Select Maximum Carry Forward--</option>
                                <?php } ?>
                                    
                                    <?php for($j=$ldcqData['number_of_leaves']; $j>=0.5; $j-=0.5){ ?>
                                        <option <?php if($ldcqData['max_carry_forward']==$j){echo 'selected';} ?> value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                    <?php } ?>
                                </select>
                                <!-- <input type="text" class="form-control onlyNumber max_carry_forward" name="max_carry_forward[<?php echo $i;?>]" id="max_carry_forward_<?php echo $i;?>" max="30" value="<?php if($ldcqData['max_carry_forward']>0){echo $ldcqData['max_carry_forward'];} ?>" placeholder="Maximum Carry Forward"> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-4 <?php if(isset($ldcqData) && $ldcqData['carry_forward_to_next_year'] == 0){echo '';}else{echo 'd-none';} ?> carryForward<?php echo $i;?>">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Minimum Carry Forward</label>
                            <div class="col-lg-12 col-md-12" id="">
                                <select class="form-control single-select" name="min_carry_forward[<?php echo $i;?>]" id="min_carry_forward_<?php echo $i;?>">
                                <?php if(!isset($ldcqData)){ ?>
                                    <option value="">--Select Minimum Carry Forward--</option>
                                <?php } ?>
                                    <?php $y= 1;
                                    for($k=($y-0.5); $k<=$ldcqData['max_carry_forward']; $k+=0.5){ ?>
                                        <option <?php if($ldcqData['min_carry_forward']==$k){echo 'selected';} ?> value="<?php echo $k; ?>"><?php echo $k; ?></option>
                                    <?php } ?>
                                </select>
                                <!-- <input type="text" class="form-control onlyNumber" name="min_carry_forward[<?php echo $i;?>]" id="min_carry_forward_<?php echo $i;?>" max="30" value="<?php if($ldcqData['min_carry_forward']>0){echo $ldcqData['min_carry_forward'];} ?>" placeholder="Minimum Carry Forward"> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Pay Out</label>
                            <div class="col-lg-12 col-md-12" id="">
                                <select  type="text" required="" class="form-control" name="pay_out[]" id="pay_out_<?php echo $i;?>">
                                    <option <?php if(isset($ldcqData) && $ldcqData['pay_out'] == 0){echo "selected";} ?> value="0">Yes</option>
                                    <option <?php if(isset($ldcqData) && $ldcqData['pay_out'] == 1){echo "selected";} ?> value="1">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Remark</label>
                            <div class="col-lg-12 col-md-12" id="">
                                <textarea type="text" class="form-control" name="pay_out_remark[]" id="pay_out_remark_<?php echo $i;?>" placeholder="Remark"><?php echo $ldcqData['pay_out_remark']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 <?php if($ldcqData['number_of_leaves'] >= 12){ }else{ echo 'd-none'; } ?> leaveCalculation<?php echo $i;?>">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12  col-md-12 col-form-label">Leave Calculation</label>
                            <div class="col-lg-12 col-md-12" id="">
                                <select type="text" required="" class="form-control" name="leave_calculation[]" id="pay_out_<?php echo $i;?>" onchange="leaveCalculation(this.value, <?php echo $i;?>);">
                                    <option <?php if(isset($ldcqData) && $ldcqData['leave_calculation'] == 0){echo "selected";} ?> value="0">Manual Calculation</option>
                                    <option <?php if(isset($ldcqData) && $ldcqData['leave_calculation'] == 1){echo "selected";} ?> value="1">Average Calculation</option>
                                </select>
                                <a href="javascript:void(0);" onclick="showAverageLeaveCalculation(<?php echo $i;?>);" class="text-danger averageCalculation<?php echo $i;?> <?php if($ldcqData['leave_calculation'] == 1){ }else{ echo 'd-none'; } ?>">* How it works</a>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <?php $i++; }?>
                <div class="form-footer text-center">
                  <button id="addLeaveAssignBulkBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $btnValue; ?></button>
                  <input type="hidden" name="leave_group_id" id="leave_group_id" value="<?php echo $lgId; ?>">
                  <input type="hidden" name="<?php echo $addquery; ?>" value="<?php echo $addquery; ?>">
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!--End Row-->
  </div>
  <!-- End container-fluid-->
</div>
<!--End content-wrapper-->

<script>
  var current_year = '<?php echo $currentYear; ?>';
  function setUseInMonthLeaves(id) {
    value = $('#number_of_leaves_' + id).val();
    use_in_month_content = ``;
    max_carry_forward_content = ``;
    min_carry_forward_content = ``;
    month_use_value = value >= 31 ? 31 : value;
    var x = 1;
    for (var i = (x - 0.5); i <= month_use_value; i += 0.5) {
      use_in_month_content += `<option value="` + i + `">` + i + `</option>`;
    }
    for (var j = value; j >= 0.5; j -= 0.5) {
      max_carry_forward_content += `<option value="` + j + `">` + j + `</option>`;
    }
    for (var k = (x - 0.5); k <= value; k += 0.5) {
      min_carry_forward_content += `<option value="` + k + `">` + k + `</option>`;
    }

    if (value >= 12) {
      $('.leaveCalculation' + id).removeClass('d-none');
    } else {
      $('.leaveCalculation' + id).addClass('d-none');
    }

    $('#leaves_use_in_month_' + id).html(use_in_month_content);
    $('#max_carry_forward_' + id).html(max_carry_forward_content);
    $('#min_carry_forward_' + id).html(min_carry_forward_content);
  }

  function leaveCalculation(value, id) {
    if (value == 1) {
      $('.averageCalculation' + id).removeClass('d-none');
    } else {
      $('.averageCalculation' + id).addClass('d-none');
    }
  }

  function showAverageLeaveCalculation(id) {
    var number_of_leaves = $('#number_of_leaves_' + id).val();
    var leaves_use_in_month = $('#leaves_use_in_month_' + id).val();
    var avg_leave = number_of_leaves / 12;
    var total_leave_till_month = avg_leave * 4;
    var applicable_leave_till = total_leave_till_month - 1;
    if (applicable_leave_till > leaves_use_in_month) {
      applicable_paid_leave = leaves_use_in_month;
    } else {
      applicable_paid_leave = applicable_leave_till;
    }
    const el = document.createElement('div')
    el.innerHTML = `<div>
                      <h5 class="text-center">Here's a Average Leave Calculation Example</h5>
                      <p class="text-left">
                          Number of leaves = ` + number_of_leaves + ` <br>
                          Use in Month = ` + leaves_use_in_month + ` <br>
                          Previous Taken Paid Leave = 1 <br>
                          Leave Date = 30 April ` + current_year + ` <br>
                          Leave Month Number = 04 (April=04) <br>
                          ` + number_of_leaves + `/12 = ` + avg_leave + ` (Average Month Leave) <br>
                          ` + avg_leave + ` * 04 = ` + total_leave_till_month + ` (Total Leave Till Leave Month) <br>
                          ` + total_leave_till_month + ` - 1 = ` + applicable_leave_till + ` (Applicable Leave Till Leave Month) <br><br>
                          CASE 1: Applicable Leave Till Leave Month(` + applicable_leave_till + `) is greater than and equal to Use in Month(` + leaves_use_in_month + `) <br>
                          THEN, Applicable Paid Leave = ` + leaves_use_in_month + ` <br><br>
                          CASE 2:  Applicable Leave Till Leave Month(` + applicable_leave_till + `) is Less than Use in Month(` + leaves_use_in_month + `) <br>
                          THEN, Applicable Paid Leave = ` + applicable_leave_till + ` <br>
                          <br></p>
                          <p class="text-success"> Applicable Paid Leave = ` + applicable_paid_leave + `</p>
                  </div>`
    swal({
      content: el,
    });
  }

  function carryForward(id) {
    value = $('#carry_forward_to_next_year_' + id).val();
    if (value == 0) {
      $('.carryForward' + id).removeClass('d-none');
    } else {
      $('.carryForward' + id).addClass('d-none');
    }
  }

  function setMinCarryForward(id) {
    value = $('#max_carry_forward_' + id).val();
    min_carry_forward_content = ``;
    var x = 1;
    for (var l = (x - 0.5); l < value; l += 0.5) {
      min_carry_forward_content += `<option value="` + l + `">` + l + `</option>`;
    }
    $('#min_carry_forward_' + id).html(min_carry_forward_content);
  }

  $(".onlyNumber").keydown(function(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
      // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
      // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  });

  $(".only5DecimalPoint").keyup(function(e) {
    value = $(this).val();
    var splitVal = value.split('.');
    var number = splitVal[0];
    var decimal = splitVal[1];
    if (decimal) {
      if (decimal != 5) {
        next_num = parseInt(number) + 1;
        swal("Please enter valid leave (Ex. " + number + ", " + number + ".5, or " + next_num + ")");
        $(this).val(splitVal[0] + '.0');
      }
    }
    e.preventDefault();
  });

  
</script>