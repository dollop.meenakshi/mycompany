  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-9">
          <h4 class="page-title">Manage Votings</h4>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="welcome">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Votings</li>
          </ol>
        </div>
        <div class="col-sm-3">
         <div class="btn-group float-sm-right">
          <a href="voting" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
          
          
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Question</th>
                    <th>Description</th>
                    <th>Stat Date</th>
                    <th>End Date</th>
                    <th>Status</th>
                    <th>Action</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Tiger Nixon</td>
                    <td>System Architect</td>
                    <td>Edinburgh</td>
                    <td>61</td>
                    <td>Active</td>
                    
                    <td>
                      <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Edit</button></td>
                      
                    </tr>
                    
                  </tbody>
                  
                </div>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-fluid-->
  
</div><!--End content-wrapper-->
<!--Start Back To Top Button-->



