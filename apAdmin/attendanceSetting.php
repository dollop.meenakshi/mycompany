<?php
error_reporting(0);
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-md-12">
                <h4 class="page-title"> Attendance Setting</h4>
            </div>
        </div>
        <form class="branchDeptFilter" action="">
            <div class="row pt-2 pb-2">
                <?php include 'selectBranchDeptForFilter.php' ?>
                <div class="col-md-1 form-group">
                    <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
                <?php
                if(isset($dId) && $dId > 0)
                {
                ?>
                <div class="col-md-5 text-right form-group">
                    <input class="btn btn-success btn-sm" type="button" name="getReport" onclick="openMultiModal();" value="Update Multiple">
                </div>
                <?php
                }
                ?>
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            if (isset($dId) && $dId > 0)
                            {
                                $deptFilterQuery = " AND users_master.floor_id='$dId'";
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>User Name</th>
                                        <th>Department</th>
                                        <th>Attendance From</th>
                                    </tr>
                                </thead>
                                <tbody id="showFilterData">
                                    <?php
                                    if (isset($bId) && $bId != "")
                                    {
                                        $branchFilterQuery = " AND users_master.block_id='$bId'";
                                    }
                                    $q = $d->select("users_master,block_master,floors_master", "users_master.floor_id = floors_master.floor_id AND  block_master.block_id=floors_master.block_id AND users_master.society_id='$society_id' AND users_master.delete_status= 0 $deptFilterQuery $branchFilterQuery $blockAppendQuery");
                                    $counter = 1;
                                    while ($data = mysqli_fetch_array($q))
                                    {
                                    ?>
                                    <tr>
                                        <td class="text-center">
                                          <input type="checkbox" name="" class="multiUpdateCheckbox" value="<?php echo $data['user_id']; ?>">
                                        </td>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['user_full_name']; ?></td>
                                        <td><?php echo $data['floor_name']; ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <form>
                                                    <select class="form-control single-select" name="take_attendance_from<?php echo $counter; ?>" id="take_attendance_from<?php echo $counter; ?>" onchange="updateAttendanceSetting(this,'<?php echo $data['user_id']; ?>');">
                                                        <option value="0" <?php if($data['take_attendance_from'] == 0){ echo "selected"; } ?>>Default Company Setting</option>
                                                        <option value="1" <?php if($data['take_attendance_from'] == 1){ echo "selected"; } ?>>Mobile App</option>
                                                        <option value="2" <?php if($data['take_attendance_from'] == 2){ echo "selected"; } ?>>Face App</option>
                                                    </select>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            else
                            {
                            ?>
                            <h4>Please Select Department !!!</h4>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<div class="modal fade" id="exEmployeeDetailModel">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Ex-Employee Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="exEmployeeDetailModelDiv" style="align-content: center;">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="update_attendance_setting_multi">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Update Multiple User Attendance Setting</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-2" style="align-content: center;">
                <form>
                    <div class="row mx-0" id="shift_timing">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-5 col-md-4 col-form-label col-12">Attendance Setting <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-8 col-12" id="">
                                <select class="form-control single-select" name="take_attendance_from" id="take_attendance_from">
                                    <option value="">--Select--</option>
                                    <option value="0">Default Company Setting</option>
                                    <option value="1">Mobile App</option>
                                    <option value="2">Face App</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group  row w-100 mx-0 ">
                            <div class="col-md-12 text-center">
                                <button type="button" class="btn btn-sm btn-primary" onclick="UpdateMulti()">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
function updateAttendanceSetting(id,user_id)
{
    take_attendance_from = id.value;
    $.ajax({
        url: "controller/AttendanceTypeController.php",
        cache: false,
        type: "POST",
        data: {
            update_attendance_setting:"update_attendance_setting",
            user_id:user_id,
            take_attendance_from:take_attendance_from,
            csrf:csrf
        },
        success: function(response)
        {
            document.location.reload(true);
        }
    });
}

function openMultiModal()
{
    var oTable = $("#example").dataTable();
    var val = [];
    $(".multiUpdateCheckbox:checked", oTable.fnGetNodes()).each(function(i)
    {
        val[i] = $(this).val();
    });

    if(val == "")
    {
        swal(
            'Warning !',
            'Please Select at least 1 user !',
            'warning'
        );
    }
    else
    {
        $('#update_attendance_setting_multi').modal();
    }
}

function UpdateMulti()
{
    take_attendance_from = $('#take_attendance_from').val();
    var oTable = $("#example").dataTable();
    var val = [];
    $(".multiUpdateCheckbox:checked", oTable.fnGetNodes()).each(function(i)
    {
        val[i] = $(this).val();
    });
    if(take_attendance_from == '' || take_attendance_from == null)
    {
        swal(
            'Warning !',
            'Please Select Attendance Setting !',
            'warning'
        );
    }
    else
    {
        swal({
            title: "Are you sure?",
            text: "You want to update Attendance Setting!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) =>
        {
            if (willDelete)
            {
                $.ajax({
                    url: "controller/AttendanceTypeController.php",
                    cache: false,
                    type: "POST",
                    data: {
                        updateAttendanceSettingMulti:"updateAttendanceSettingMulti",
                        user_id:val,
                        take_attendance_from:take_attendance_from,
                        csrf:csrf
                    },
                    success: function(response)
                    {
                        document.location.reload(true);
                    }
                });
            }
            else
            {
                swal("data not updated!");
            }
        });
    }
}
</script>