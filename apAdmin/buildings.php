  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Societies (Apartments)</h4>
        <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="welcome">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Societies</li>
         </ol>
     </div>
     <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <a href="building" class="btn btn-primary waves-effect btn-sm waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
       
        </button>
        
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo $xml->string->society; ?> Id</th>
                      <th>Name</th>
                      <th>Mobile</th>
                      <th>Password</th>
                      <th>Palan</th>
                      <th>Palan Expire</th>
                      <th>Password</th>
                      <th>Plan</th>
                      <th>Status</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                   
                      $i=1;
                      $q = $d->select("society_master" ,"","order by society_id  DESC");
                      while ($data=mysqli_fetch_array($q)) {
                        extract($data);
                        $qq=$d->select("bms_admin_master","society_id ='$society_id'","ORDER BY admin_id ASC");
                        $data11=mysqli_fetch_array($qq);
                        $admin_password=$data11['admin_password'];
                       ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                        <td><?php echo 'BUI_'.$society_id; ?></td>
                        <td><?php echo $society_name; ?></td>
                        <!-- <td><?php echo $society_address; ?></td> -->
                        <td><?php echo $secretary_mobile; ?></td>
                        <td><?php echo $admin_password; ?></td>
                        <td><?php 
                           $qw2=$d->select("package_master","package_id='$package_id'","");
                            $row=mysqli_fetch_array($qw2);
                            if ($row>0) {
                              echo $row['package_name'];
                            } else {
                              echo "Trial (".$data['trial_days']." Days)";
                            }
                         ?></td>
                         <td><?php echo $plan_expire_date; ?></td>
                         
                         <td>
                           <form action="controller/loginController.php" method="post">
                              <input type="hidden" name="forgot_email_admin" value="<?php echo $data11['admin_email'] ?>">
                              <button class="btn btn-sm btn-warning">Reset</button>
                           </form>
                         </td>
                         <td>
                            
                              <button onclick="changePlan('<?php echo $society_id; ?>');" data-toggle="modal" data-target="#planModal" class="btn btn-sm btn-danger">Change</button>
                         </td>
                        <td>
                           <?php
                            if($society_status=="0"){
                            ?>
                              <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $society_id; ?>','societyDeactive');" data-size="small"/>
                              <?php } else { ?>
                             <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $society_id; ?>','societyActive');" data-size="small"/>
                            <?php } ?>
                        </td>
                        <td>
                          <form action="building" method="post">
                            <input type="hidden" name="society_id_edit" value="<?php echo $society_id; ?>">
                            <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light m-1" title="Edit"> <i class="fa fa-pencil"></i> 
                          </button>
                            
                          </form>
                        </td>
                        <td>
                         <!--  <form action="controller/buildingController.php" method="post" >
                            <input type="hidden" name="society_id_delete" value="<?php echo $society_id; ?>">
                            <button type="submit" class="form-btn btn btn-sm btn-danger waves-effect waves-light m-1" title="Delete"> <i class="fa fa-trash-o"></i> </button>
                            
                          </form> -->
                        </td>
                    </tr>
                  <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->

   <div class="modal fade" id="planModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Change <?php echo $xml->string->society; ?> Plan</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
         <form id="signupForm" action="controller/buildingController.php" method="post" enctype="multipart/form-data">
        <div class="form-group row" id="planFormRes">

          
        </div>

        <div class="form-group row">
          <label for="input-12" class="col-sm-4 col-form-label">Payment Status <span class="required">*</span></label>
           <div class="col-sm-8">
             <select class="form-control paymentSelect" name="amountReceivedType">
               <option value="0">Not Received</option>
               <option value="1">Received</option>
             </select>
           </div>
        </div>
        <div class="form-group row" id="amoutLDiv">
          <label for="input-12" class="col-sm-4 col-form-label"> Amount <span class="required">*</span></label>
           <div class="col-sm-8" >
             <input type="text" required="" id="working_days" name="amountReceived" class="form-control">
           </div>
        </div>
         <div class="form-footer text-center">
            <button type="submit" id="" name=""  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
            <input type="hidden" name="updatePlan" value="updatePlan">
            <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> RESET</button>
        </div>

        </form>
      </div>
     
    </div>
  </div>
</div><!--End Modal -->

<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {

    $('#amoutLDiv').hide();

    $('.paymentSelect').change(function(){
      var data= $(this).val();
      // alert(data);
       if(data!=0) {
          $('#amoutLDiv').show();
       } else {
          $('#amoutLDiv').hide();
       }          
    });
  
});

</script>