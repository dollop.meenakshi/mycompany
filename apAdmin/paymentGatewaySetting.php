<?php 
  $cq=$d->select("society_payment_getway","society_id='$society_id'");
  $paydata=mysqli_fetch_array($cq);

$q=$d->select("society_master","society_id='$society_id'");
$bData=mysqli_fetch_array($q);
$country_id= $bData['country_id'];
extract(array_map("test_input" , $_REQUEST));


if(isset($EditFlg) && $EditFlg=="Yes"){
  $society_payment_getway_id = (int)$society_payment_getway_id;
  $society_payment_getway_qry=$d->select("society_payment_getway","society_id='$society_id' and society_payment_getway_id= '$society_payment_getway_id'  ");
$society_payment_getway_data = mysqli_fetch_array($society_payment_getway_qry);


  ?> 

<div class="content-wrapper">
  <div class="container-fluid">

    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Edit Payment Gateway</h4>
      </div>
      <div class="col-sm-3">
        <div class="btn-group float-sm-right">
        
           <a href="paymentGatewaySetting" class="btn  btn-sm btn-success " >List</a> 
        </div>
      </div>
    </div>


    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      
      <div class="col-sm-3 col-12 text-right">
        
     </div>

    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">


            <form id="editpaymentGatewayFrm" action="controller/buildingController.php" method="post" enctype="multipart/form-data">
              
              <input type="hidden" name="society_payment_getway_id" value="<?php echo $society_payment_getway_data['society_payment_getway_id']; ?>">
              <span id="updatePayGateModalDiv">
                
                <div class="form-group row">
                <label for="payment_getway_master_id" class="col-sm-4 col-form-label">Payment Gateway Company <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                  <select required="" name="payment_getway_master_id" class="form-control">
                    <?php
                    $i=1;
                    $q=$d->select("payment_getway_master","payment_getway_master_id='$society_payment_getway_data[payment_getway_master_id]' AND getway_country_id='$country_id'");
                    while ($data=mysqli_fetch_array($q)) {
                    ?>
                    <option <?php if($society_payment_getway_data['payment_getway_master_id']==$data['payment_getway_master_id']){ echo "selected"; } ?>   value="<?php echo $data['payment_getway_master_id']; ?>"><?php echo $data['payment_getway_name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
               <div class="form-group row">
                              
                <label for="merchant_id" class="col-sm-4 col-form-label">Name  <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="60" type="text" required="" class="form-control" id="name" value="<?php echo  $society_payment_getway_data['name'];?>"  name="name">
                </div>
              </div>
              <?php if($society_payment_getway_data['payment_getway_master_id']==1) {  ?>
              <div id="payUmoneyDivEdit">
                <div class="form-group row">
                  
                  <label for="merchant_id" class="col-sm-4 col-form-label">Merchant Id <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="merchant_id"   name="merchant_id" value="<?php echo  $society_payment_getway_data['merchant_id'];?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Merchant Key <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="merchant_key" name="merchant_key"  value="<?php echo  $society_payment_getway_data['merchant_key'];?>" >
                  </div>
                </div>
                <div class="form-group row">
                  
                  
                  <label for="salt_key" class="col-sm-4 col-form-label">Salt Key <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="salt_key" name="salt_key"  value="<?php echo  $society_payment_getway_data['salt_key'];?>"  >
                  </div>
                  
                </div>
                <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Transaction Charges % </label>
                  <div class="col-sm-8">
                    <input maxlength="5" type="text" max="10"  class="form-control onlyNumber" inputmode="numeric" id="transaction_charges" name="transaction_charges" value="<?php if($society_payment_getway_data['transaction_charges']>0) { echo  $society_payment_getway_data['transaction_charges']; } ?>" >
                  </div>
                </div>
              </div>
              <?php } else if($society_payment_getway_data['payment_getway_master_id']==2) {  ?>
              <div id="razorpayDivEdit">
                <div class="form-group row">
                  
                  <label for="merchant_id" class="col-sm-4 col-form-label">Razor Pay keyId <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="merchant_id"   name="RozerPaykeyId" value="<?php echo  $society_payment_getway_data['merchant_id'];?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Razor Pay Secret key<i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="merchant_key" name="SecretKey"  value="<?php echo  $society_payment_getway_data['merchant_key'];?>" >
                  </div>
                </div>
                <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Transaction Charges % </label>
                  <div class="col-sm-8">
                    <input maxlength="5" type="text" max="10" class="form-control onlyNumber" inputmode="numeric" id="transaction_charges" name="transaction_charges_razer"  value="<?php if($society_payment_getway_data['transaction_charges']>0) { echo  $society_payment_getway_data['transaction_charges']; } ?>" >
                  </div>
                </div>
              </div>
              <?php } else if($society_payment_getway_data['payment_getway_master_id']==3) {  ?>
              <div id="upiDiv">
                  <div class="form-group row">
                    
                    <label for="merchant_id" class="col-sm-4 col-form-label">UPI Id <i class="text-danger">*</i></label>
                    <div class="col-sm-8">
                     <input maxlength="60" type="text" required="" class="form-control" id="merchant_id"   name="UpiId" value="<?php echo  $society_payment_getway_data['merchant_id'];?>">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="merchant_key" class="col-sm-4 col-form-label">Merchant Account Name <i class="text-danger">*</i></label>
                    <div class="col-sm-8">
                      <input maxlength="60" type="text" required="" class="form-control" id="merchant_key" name="MerchantAccount"  value="<?php echo  $society_payment_getway_data['merchant_key'];?>" >
                    </div>
                  </div>

                </div>
              <?php } else if($society_payment_getway_data['payment_getway_master_id']==4) {  ?>
                <div class="form-group row">
                  
                  <label for="merchant_id" class="col-sm-4 col-form-label">Public Key <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                   <input maxlength="60" type="text" required="" class="form-control" id="merchant_id"   name="flutterPublicKey" value="<?php echo  $society_payment_getway_data['merchant_id'];?>">
                  </div>
                </div>
                 <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Secret Key  <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="merchant_key" name="flutterSecretKey"  value="<?php echo  $society_payment_getway_data['merchant_key'];?>" >
                  </div>
                </div>
                 <div class="form-group row">
                 
              
                  <label for="salt_key" class="col-sm-4 col-form-label">Encryption Key <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="salt_key" name="flutterEncryptionKey"  value="<?php echo  $society_payment_getway_data['salt_key'];?>" >
                  </div>
                   
                </div>
                <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Transaction Charges % </label>
                  <div class="col-sm-8">
                    <input maxlength="5" type="text" max="10"  class="form-control onlyNumber" inputmode="numeric" id="transaction_charges" name="transaction_charges_flutter" value="<?php echo  $society_payment_getway_data['transaction_charges'];?>" >
                  </div>
                </div>
              <?php } ?>

                            </span>
                            <div class="form-footer text-center">
                              <input type="hidden" name="editPaymentGetwat" value="editPaymentGetwat">
                <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button>
              </div>
             </form>


            
          </div>
        </div>
      </div>
    </div><!--End Row-->

  </div>
  <!-- End container-fluid-->

    </div><!--End content-wrapper-->
 

     
  <?php
} else if(isset($_GET['viewRequest']) && $_GET['viewRequest']=="yes"){ 
        $id = (int)$id;
        $ch = curl_init();
       
        curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/contact_fincasysteam_controller.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "getPaymentGetwatRequestDetails=getPaymentGetwatRequestDetails&society_id=$society_id&requiest_id=$id  ");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'key: bmsapikey'
        ));

        $server_output = curl_exec($ch);

        curl_close ($ch);
        $server_output=json_decode($server_output,true);
        $totalRq = count($server_output['payment_gateway_requiest']);
        if ( $totalRq==0) {
           echo ("<script LANGUAGE='JavaScript'>
            window.location.href='paymentGatewaySetting';
            </script>");
        }

?>


<link href="assets/plugins/vertical-timeline/css/vertical-timeline1.css" rel="stylesheet"/>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Request History (REQ<?php echo $server_output['requiest_id']; ?>)</h4>
        <div class="table-responsive">
        <table class="table table-bordered bg-white">
          <tbody>
            <tr>
              <th>Apply For</th>
              <td><?php echo $server_output['payment_getway_name']; ?></td>
            </tr>
            <tr>
              <th>Requested By</th>
              <td><?php echo $server_output['created_by']; ?></td>
            </tr>
            <tr>
              <th>Date</th>
              <td><?php echo  date('d M Y h:i A',strtotime($server_output['created_date'])); ?></td>
            </tr>
            <tr>
              <th>Email</th>
              <td><?php echo $server_output['email_id']; ?></td>
            </tr>
            <tr>
              <th>Mobile</th>
              <td><?php echo $server_output['contact_mobile']; ?></td>
            </tr>
            <tr>
              <th>Registration Proof</th>
              <td><?php
                if ($server_output['registrationProof']!='') {  ?>
                    <a href="<?php echo $server_output['registrationProof'];  ?>" target="_blank" class="btn btn-primary btn-sm">View</a>
                <?php } ?>
              </td>
            </tr>
            <tr>
              <th>Pan Card</th>
              <td><?php
                if ($server_output['panCard']!='') {  ?>
                    <a href="<?php echo $server_output['panCard'];  ?>" target="_blank" class="btn btn-primary btn-sm">View</a>
                <?php } ?>
              </td>
            </tr>
            <tr>
              <th><?php echo $xml->string->tax;?> Certificate</th>
              <td><?php
                if ($server_output['gstCerty']!='') {  ?>
                    <a href="<?php echo $server_output['gstCerty'];  ?>" target="_blank" class="btn btn-primary btn-sm">View</a>
                <?php } ?>
              </td>
            </tr>
            <tr>
              <th>Cancelled Cheque</th>
              <td><?php
                if ($server_output['cancelledCheque']!='') {  ?>
                    <a href="<?php echo $server_output['cancelledCheque'];  ?>" target="_blank" class="btn btn-primary btn-sm">View</a>
                <?php } ?>
              </td>
            </tr>
            <tr>
              <th>Authorized Address</th>
              <td><?php
                if ($server_output['authorizedAddress']!='') {  ?>
                    <a href="<?php echo $server_output['authorizedAddress'];  ?>" target="_blank" class="btn btn-primary btn-sm">View</a>
                <?php } ?>
              </td>
            </tr>
            <tr>
              <th>Authorized Pancard</th>
              <td><?php
                if ($server_output['authorizedPancard']!='') {  ?>
                    <a href="<?php echo $server_output['authorizedPancard'];  ?>" target="_blank" class="btn btn-primary btn-sm">View</a>
                <?php } ?>
              </td>
            </tr>
            <tr>
              <th>Status</th>
              <td><?php echo $server_output['current_status'];  ?></td>
            </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
     <div class="row pt-2 pb-2">
      <div class="col-sm-12 text-center">
         <button data-toggle="modal" data-target="#vieComp"   onclick="vieComplain('<?php echo $id; ?>');"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-pencil"></i> Reply</button>
      </div>
    </div>
    <section class="cd-timeline js-cd-timeline">
      <div class="cd-timeline__container">
        <?php 
          for ($ir=0; $ir < $totalRq; $ir++) { 
          
        ?>
          <div class="cd-timeline__block js-cd-block <?php if($server_output['payment_gateway_requiest'][$ir]['admin_name']!=''){ echo "floatRight"; } ?>">
            <div class="cd-timeline__img cd-timeline__img--picture js-cd-img text-center ">
              <img src="../img/fav.png">
            </div> 

            <div class="cd-timeline__content js-cd-content">
              <p><?php if($server_output['payment_gateway_requiest'][$ir]['admin_name']!=''){ 
                
                ?>
                <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="img/user.png"  width="30" height="30"   src="#" alt="your image" class='profile' />
                <?php echo $server_output['payment_gateway_requiest'][$ir]['admin_name'] .' (Admin)';} else {
                ?>
                <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="img/user.png"  width="30" height="30"   src="#" alt="your image" class='profile' />
                <?php echo $server_output['payment_gateway_requiest'][$ir]['user_name'];
              } ?></p>
              <h6 style="word-wrap: break-word;"><?php echo $server_output['payment_gateway_requiest'][$ir]['log_name']; ?></h6>
              
              <?php if ($server_output['payment_gateway_requiest'][$ir]['file_name']!='') { 
                $ext = pathinfo($server_output['payment_gateway_requiest'][$ir]['file_name'], PATHINFO_EXTENSION);
                if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                  $imgIcon = 'img/jpg.png';
                } else{
                  $imgIcon = '';
                }

                if ($imgIcon!="") {
                ?>
                <a data-fancybox="images" data-caption="Photo Name : <?php echo $server_output['payment_gateway_requiest'][$ir]['file_name']; ?>" target="_blank" href="<?php echo $server_output['payment_gateway_requiest'][$ir]['file_name']; ?>">
                  <img class="lazyload" src="../img/ajax-loader.gif" data-src="<?php echo $server_output['payment_gateway_requiest'][$ir]['file_name']; ?>" width="100" height="100">
                </a>
              <?php }  else {  ?>
                <a  target="_blank" href="<?php echo $server_output['payment_gateway_requiest'][$ir]['file_name']; ?>">
                <i class="fa fa-paperclip"></i>  Attachment 
                </a>
              <?php } } ?>
             
            
              <p><?php echo date('d M Y h:i A',strtotime($server_output['payment_gateway_requiest'][$ir]['log_time'])); ?> </p>

              <span class="cd-timeline__date"><?php echo date('M d',strtotime($server_output['payment_gateway_requiest'][$ir]['log_time'])); ?></span>
            </div>
          </div>
        <?php } ?>
      </div>
    </section>
  </div>
</div>


<div class="modal fade" id="vieComp">
  <div class="modal-dialog ">
    <div class="modal-content border-success">
      <div class="modal-header bg-success">
        <h5 class="modal-title text-white">Reply </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
          <form id="reqFoprmValidation" action="controller/feedbackController.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
            <input type="hidden"  value="<?php echo $id; ?>" name="requiest_id">
                <div class="row">
                    <label for="input-10" class="col-sm-4 col-form-label">Request Number </label>
                    <div class="col-sm-8" id="">
                        <span class="badge badge-pill badge-primary m-1">REQ<?php echo $server_output['requiest_id']; ?></span>
                    </div>
                </div>
                <div class="form-group row" >
                    <label for="input-10" class="col-sm-4 col-form-label">Status </label>
                    <div class="col-sm-8" id="">
                        <?php echo $server_output['current_status'];  ?>
                    </div>
                </div>
                <div class="form-group row" >
                    <label for="input-10" class="col-sm-4 col-form-label">Message <span class="text-danger">*</span></label>
                    <div class="col-sm-8" id="">
                        <textarea  required="" rows="4" maxlength="500" class="form-control" name="log_name"></textarea>
                    </div>
                </div>
                <div class="form-group  row" >
                    <label for="input-10" class="col-sm-4 col-form-label">Image / Attachment  </label>
                    <div class="col-sm-8" id="">
                        <input type="file" id="complains_track_img" name="file_name" class="form-control-file border docOnly" accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" >
                    </div>
                </div>
               
                <div class="form-footer text-center">
                  <input type="hidden" name="changeStausPaymentStatus" value="changeStausPaymentStatus">
                  <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                    
                </div>

            
          </form> 
      </div>
     
    </div>
  </div>
</div>

<?php } else {



 ?>
 

<div class="content-wrapper">
  <div class="container-fluid">

    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-12">
        <h4 class="page-title">Payment Gateways</h4>
      </div>
      <div class="col-sm-3 col-12 text-right">
        <div class="btn-group float-sm-right">
          <button class="btn  btn-sm btn-warning " data-toggle="modal" data-target="#applyModal"><i class="fa fa-plus mr-1"></i> Apply for payment Gateway </button> 
          <button class="btn  btn-sm btn-success " data-toggle="modal" data-target="#addModal"><i class="fa fa-plus mr-1"></i> Add </button> 

        </div>
      </div>
    </div>

    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      
      <div class="col-sm-3 col-12 text-right">
        <?php 
         $cq1=$d->select("society_payment_getway","society_id='$society_id'");
        // $paydata11=mysqli_fetch_array($cq1);
         // print_r($paydata11);
         /*if (mysqli_num_rows($cq1)>1) {
         ?>
        <form id="signupForm"method="post" action="controller/buildingController.php">
          <input type="hidden"  name="removePaymentGetway" value="removePaymentGetway">
           <button type="submit" class="btn btn-danger "><i class="fa fa-trash-o"></i> Remove Details</button>
        </form>
        <?php }*/ ?>
     </div>

    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">


            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <!-- <th>#</th> -->
                    <th>Name</th>
                    <th>Company</th>
                    <th>Merchant Id/Public Key</th>
                    <th>Merchant Key/Secret Key</th>
                    <th>Salt Key/Encryption Key  </th>
                    <th>Used</th>
                    <th>Transaction Charges % </th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php 
                        $counter = mysqli_num_rows($cq1);
                        $alreadyAddMerchant = array();
                    while ($data=mysqli_fetch_array($cq1)) {
                      array_push($alreadyAddMerchant, $data['merchant_id']);
                     ?>
                    <tr>
                       <!-- <td class="text-center">
                        
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['society_payment_getway_id']; ?>">
                        
                        </td> -->
                        <td><?php 
                        $q=$d->select("payment_getway_master"," payment_getway_master_id ='$data[payment_getway_master_id]' ");
                        $payment_getway_master=mysqli_fetch_array($q); 
                        echo $payment_getway_master['payment_getway_name'];
                        ?>
                        <td> 
                        <?php echo $data['name'] ;?>   </td>
                        <td> <?php echo $data['merchant_id'];?>   </td>
                        <td> <?php echo $data['merchant_key'];?>   </td>
                        <td> <?php echo $data['salt_key'];?>   </td>
                        <td>  
                          <?php
                          if ($data['is_upi']==1) {
                         echo $totalUserd=$d->count_data_direct("society_payment_getway_id","balancesheet_master","society_id='$society_id' AND society_payment_getway_id_upi='$data[society_payment_getway_id]'").' Balancesheets'; 
                          } else {
                         echo $totalUserd=$d->count_data_direct("society_payment_getway_id","balancesheet_master","society_id='$society_id' AND society_payment_getway_id='$data[society_payment_getway_id]'").' Balancesheets'; 
                            
                          }
                            ?> 
                           </td>
                          
                        </td>
                        <td> <?php if($data['transaction_charges']>0) { echo $data['transaction_charges'].' %'; } ?>   </td>
                        <td>
                          <?php
                                if($data['status']=="0"){
                                ?>
                                  <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['society_payment_getway_id']; ?>','PaymentGatDeactive');" data-size="small"/>
                                  <?php } else { ?>
                                 <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['society_payment_getway_id']; ?>','PaymentGatActive');" data-size="small"/>
                                <?php } ?>
                        </td>
                         <td>
                           <div style="display: inline-block;">
                            <a title="Edit"     href="paymentGatewaySetting?society_payment_getway_id=<?php echo $data['society_payment_getway_id'];?>&EditFlg=Yes" > <span class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i></span></a>
                                  </div>
                                  <?php if( $totalUserd==0 ) { ?> 
                                  <div  style="display: inline-block;">
                                     <form title="Delete" action="controller/buildingController.php" method="post"  >
                              <input type="hidden" name="society_payment_getway_id" value="<?php echo $data['society_payment_getway_id']; ?>">
                              <input type="hidden" value="deletepaymentgateway" name="deletepaymentgateway">
                              <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i></button>
                            </form>
                          </div>
                          <?php } ?> 
                        </td>
                         
                         
                    </tr>
                  <?php }  ?>
                </tbody>
              </table>
            </div>


            
          </div>
        </div>
      </div>
    </div><!--End Row-->


     <?php 
        $ch = curl_init();
       
        curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/contact_fincasysteam_controller.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "getPaymentGetwatRequest=getPaymentGetwatRequest&society_id=$society_id");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'key: bmsapikey'
        ));

        $server_output = curl_exec($ch);

        curl_close ($ch);
        $server_output=json_decode($server_output,true);
        $totalRq = count($server_output['payment_gateway_requiest']);

        ?>

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <h5>Payment Gateway Requests</h5>

            <div class="table-responsive">
              <table id="exampleReportBalancesheet" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Apply For</th>
                    <th>Request Date</th>
                    <th>Request By</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php 
                        for ($iC=0; $iC <$totalRq ; $iC++) { 

                     ?>
                    <tr>
                      
                        <td></td>
                        <td>  <?php echo $server_output['payment_gateway_requiest'][$iC]['payment_getway_name'] ;?>    </td>
                        <td>  <?php echo $server_output['payment_gateway_requiest'][$iC]['created_date'] ;?>    </td>
                        <td>  <?php echo $server_output['payment_gateway_requiest'][$iC]['created_by'] ;?>    </td>
                        <td>  <?php echo $server_output['payment_gateway_requiest'][$iC]['current_status'] ;?>  
                            <a class="btn btn-primary btn-sm" href="paymentGatewaySetting?viewRequest=yes&id=<?php echo $server_output['payment_gateway_requiest'][$iC]['requiest_id']; ?>">View </a>
                          </td>
                        <td>
                            <?php if( $server_output['payment_gateway_requiest'][$iC]['current_status_int']== 0 ) { ?> 
                              <form title="Delete" action="controller/feedbackController.php" method="post"  >
                                <input type="hidden" name="requiest_id_delete" value="<?php echo $server_output['payment_gateway_requiest'][$iC]['requiest_id']; ?>">
                                <input type="hidden" value="deletePgRequiest" name="deletePgRequiest">
                              <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i></button>
                            </form>
                          <?php } ?> 
                           <?php if( $server_output['payment_gateway_requiest'][$iC]['current_status_int']== 3 && !in_array($server_output['payment_gateway_requiest'][$iC]['merchant_id'], $alreadyAddMerchant)) { ?>
                           <button data-toggle="modal" data-target="#addModalConfigure"   onclick="configurePaymentGateway('<?php echo $server_output['payment_gateway_requiest'][$iC]['requiest_id']; ?>');"  class="btn btn-sm btn-danger" type="button"><i class="fa fa-cogs"></i> Configure</button>
                           <?php } ?> 
                        </td>
                      
                        
                         
                         
                    </tr>
                  <?php }  ?>
                </tbody>
              </table>
            </div>


            
          </div>
        </div>
      </div>
    </div><!--End Row-->


  </div>
  <!-- End container-fluid-->

    </div><!--End content-wrapper-->

<div class="modal fade" id="infoModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Information</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="infoModalDiv">
          
      </div>
     
    </div>
  </div>
</div>

    <div class="modal fade" id="addModal"><!-- Edit Modal -->
      <div class="modal-dialog">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Add Payment Gateway</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" >
            <form id="paymentGatewayFrm" action="controller/buildingController.php" method="post" enctype="multipart/form-data">
              

               <div class="form-group row">
                <label for="payment_getway_master_id" class="col-sm-4 col-form-label">Payment Gateway Company <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                  <select required="" id="paymentCompanyChange" name="payment_getway_master_id" class="form-control">
                    <option value="">-- Select -- </option>
                   <?php 
                   $i=1;
                   $q=$d->select("payment_getway_master","getway_country_id='$country_id'");
                   while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <option value="<?php echo $data['payment_getway_master_id']; ?>"><?php echo $data['payment_getway_name']; ?></option>
                  <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                
                <label for="name" class="col-sm-4 col-form-label">Name  <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="60" type="text" required="" class="form-control" id="name"   name="name">
                </div>
              </div>
              <div id="payUmoneyDiv" style="display: none;">
                <div class="form-group row">
                  
                  <label for="merchant_id" class="col-sm-4 col-form-label">Merchant Id <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                   <input maxlength="60" type="text" required="" class="form-control" id="merchant_id"  name="merchant_id">
                  </div>
                </div>
                 <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Merchant Key <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="merchant_key" name="merchant_key"  >
                  </div>
                </div>
                

                <div class="form-group row">
                 
              
                  <label for="salt_key" class="col-sm-4 col-form-label">Salt Key <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="salt_key" name="salt_key"   >
                  </div>
                   
                </div>
                <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Transaction Charges % </label>
                  <div class="col-sm-8">
                    <input maxlength="5" type="text" max="10"  class="form-control onlyNumber" inputmode="numeric" id="transaction_charges" name="transaction_charges"  >
                  </div>
                </div>
              </div>
               <div id="razorpayDiv" style="display: none;">
                <div class="form-group row">
                  
                  <label for="merchant_id" class="col-sm-4 col-form-label">Razor Pay keyId <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                   <input maxlength="60" type="text" required="" class="form-control" id="merchant_id"   name="RozerPaykeyId">
                  </div>
                </div>
                 <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Razor Pay Secret key <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="merchant_key" name="SecretKey"  >
                  </div>
                </div>
                <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Transaction Charges % </label>
                  <div class="col-sm-8">
                    <input maxlength="5" type="text" max="10"  class="form-control onlyNumber" inputmode="numeric" id="transaction_charges" name="transaction_charges_razer"  >
                  </div>
                </div>
                
              </div>
              <div id="flutterwaveDiv" style="display: none;">
                <div class="form-group row">
                  
                  <label for="merchant_id" class="col-sm-4 col-form-label">Public Key <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                   <input maxlength="60" type="text" required="" class="form-control" id="merchant_id"   name="flutterPublicKey">
                  </div>
                </div>
                 <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Secret Key  <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="merchant_key" name="flutterSecretKey"  >
                  </div>
                </div>
                 <div class="form-group row">
                 
              
                  <label for="salt_key" class="col-sm-4 col-form-label">Encryption Key <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="salt_key" name="flutterEncryptionKey"   >
                  </div>
                   
                </div>
                <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Transaction Charges % </label>
                  <div class="col-sm-8">
                    <input maxlength="5" type="text" max="10"  class="form-control onlyNumber" inputmode="numeric" id="transaction_charges" name="transaction_charges_flutter"  >
                  </div>
                </div>
                
              </div>
               <div id="upiDiv" style="display: none;">
                <div class="form-group row">
                  
                  <label for="merchant_id" class="col-sm-4 col-form-label">UPI Id <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                   <input maxlength="60" type="text" required="" class="form-control" id="merchant_id"   name="UpiId">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="merchant_key" class="col-sm-4 col-form-label">Merchant Account Name <i class="text-danger">*</i></label>
                  <div class="col-sm-8">
                    <input maxlength="60" type="text" required="" class="form-control" id="merchant_key" name="MerchantAccount"  >
                  </div>
                </div>

              </div>
              

              <div class="form-footer text-center">
                <input type="hidden" name="addPaymentGetwat" value="addPaymentGetwat">
                
                <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i>Add</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!--end modal -->


    <div class="modal fade" id="addModalConfigure"><!-- Edit Modal -->
      <div class="modal-dialog">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Add Payment Gateway</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="congigureDiv">
            
          </div>
        </div>
      </div>
    </div><!--end modal -->



    <div class="modal fade" id="applyModal"><!-- Edit Modal -->
      <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Apply For Payment Gateway</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="paymentGatewayApplyFrm" action="controller/feedbackController.php" method="post" enctype="multipart/form-data">
              

               <div class="form-group row">
                <label for="payment_getway_master_id" class="col-sm-4 col-form-label">Apply For Company <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                  <select required="" id="paymentCompanyChange" name="payment_getway_name" class="form-control">
                   <?php 
                   $i=1;
                   $q=$d->select("payment_getway_master","getway_country_id='$country_id'");
                   while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <option value="<?php echo $data['payment_getway_name']; ?>"><?php echo $data['payment_getway_name']; ?></option>
                  <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label  class="col-sm-4 col-form-label">Contact Person Name  <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="60" type="text" required="" class="form-control" id="name"   name="name">
                </div>
              </div>
              <div class="form-group row">
                
                <label  class="col-sm-4 col-form-label">Contact Person Mobile Number<i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="15"  type="text" required="" class="form-control onlyNumber" inputmode="numeric" id="mobile"   name="mobile">
                </div>
              </div>
              <div class="form-group row">
                
                <label  class="col-sm-4 col-form-label">Email Id <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="75" type="email" required="" class="form-control" id="email"   name="email">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Registration Proof Copy <i title='Municipal Corporation Department Certificate/License' class="fa fa-info-circle" aria-hidden="true"></i> <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="75"  accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" type="file" required="" class="form-control idProof" id="registrationProof"   name="registrationProof">
                </div>
              </div>
              <div class="form-group row">
                <label  class="col-sm-4 col-form-label">Copy of PAN card  <i title='<?php echo $xml->string->society; ?> Pan Card' class="fa fa-info-circle" aria-hidden="true"></i><i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="75"  accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" type="file" required="" class="form-control idProof" id="panCard"   name="panCard">
                </div>
              </div>
              <div class="form-group row">
                <label  class="col-sm-4 col-form-label"><?php echo $xml->string->tax;?> Certificate Copy <i title='<?php echo $xml->string->society; ?> <?php echo $xml->string->tax;?> Certificate' class="fa fa-info-circle" aria-hidden="true"></i> </label>
                <div class="col-sm-8">
                 <input maxlength="75"  accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" type="file" class="form-control idProof" id="gstCerty"   name="gstCerty">
                </div>
              </div>
              <div class="form-group row">
                <label  class="col-sm-4 col-form-label">Cancelled Cheque Copy <i title='<?php echo $xml->string->society; ?> Bank Account  Cancelled Cheque' class="fa fa-info-circle" aria-hidden="true"></i><i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="75"  accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" type="file" required="" class="form-control idProof" id="cancelledCheque"   name="cancelledCheque">
                </div>
              </div>
              <div class="form-group row">
                <label  class="col-sm-4 col-form-label">Authorized signatory address proof Copy  <i title='Passport/AADHAR/Voter ID/DL' class="fa fa-info-circle" aria-hidden="true"></i> <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="75"  accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" type="file" required="" class="form-control idProof" id="authorizedAddress"   name="authorizedAddress">
                </div>
              </div>
               <div class="form-group row">
                <label  class="col-sm-4 col-form-label">Authorized signatory Pancard  <i class="text-danger">*</i></label>
                <div class="col-sm-8">
                 <input maxlength="75"  accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" type="file" required="" class="form-control idProof" id="authorizedPancard"   name="authorizedPancard">
                </div>
              </div>
               <div class="form-group row">
                <label for="day" class="col-sm-12 col-form-label"> <input required="" type="checkbox" name="dayName"> I AGREE TO SHARE ABOVE DETAILS FOR PAYMENT GATEWAY APPLY</label>

              </div>

              <div class="form-footer text-center">
                <input type="hidden" name="addPaymentGetwatRequest" value="addPaymentGetwatRequest">
                
                <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i>Add</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!--end modal -->
    <?php } ?> 