<?php
extract($_REQUEST);
if (filter_var($_REQUEST['id'], FILTER_VALIDATE_INT) != true || (isset($_REQUEST['id']) && filter_var($_REQUEST['id'], FILTER_VALIDATE_INT) != true)) {
	$_SESSION['msg1']="Invalid  Request";
	echo ("<script LANGUAGE='JavaScript'>
		window.location.href='events';
		</script>");
	exit();
}
$event_id=$id;
$society_id = $_COOKIE['society_id'];
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9 col-9">
				<h4 class="page-title">View Events</h4>
			</div>
			<div class="col-sm-3 col-3">
				<div class="btn-group float-sm-right">
				
				</div>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<!-- Event Details -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<?php
						$eq = $d->select("event_master","event_id = '$event_id'");
						$event_master_data = $eData = mysqli_fetch_array($eq);
						 $today=date('Y-m-d');
						  $endDate= date('Y-m-d',strtotime($event_master_data['event_end_date']));
	 					?>
						<div class="form-group row">
							<div class="col-md-9">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Event Name : <?php echo $mainEvent = $eData['event_title']; ?> </h4>
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Description: <?php echo $eData['event_description']; ?> </h4>
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Event Date : <?php echo date('d M Y',strtotime($event_master_data['event_start_date'])); ?> to <?php echo date('d M Y',strtotime($event_master_data['event_end_date'])); ?>  </h4>
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Location:<?php echo $eData['eventMom']; ?>  </h4>
							</div>
							<div class="col-md-3">
								<?php if(file_exists("../img/event_image/$eData[event_image]")) { ?>
								<img height="140" src="../img/event_image/<?php echo $eData['event_image']; ?>" class="card-img-top" alt="Event Image">
								<?php } else {  ?>
								 <img height="250"  src="../img/eventDefault.png"  class="card-img-top" alt="Event Image">
			              		<?php } ?>

							</div>
							<div class="col-md-12">
								
							</div>
							
							
						</div>
						<div class="form-group row">
							<!-- <div class="col-md-6">
								
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Type : <?php if($eData['event_type']==0) { echo "Unpaid"; } else { echo "Paid (Employee:" .$eData['adult_charge'].' & Child:'.$eData['child_charge'].' & Guest:'.$eData['guest_charge'].')'; } ?> </h4>
							</div> -->
							<div class="col-md-6">
								<!-- <h4 for="input-16" class="col-form-label" style="font-size: 14px;">Going: <?php echo $aData['sum']+$cData['sumchild']+$gData['sumguest']; ?> </h4> -->
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div><!--End Row-->
			<?php if ($today<=$endDate) { ?>

			<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form id="eventBookFrm" action="controller/eventController.php" method="POST" enctype="multipart/form-data"  >
						<input type="hidden" name="event_id" id="event_id" value="<?php echo $id ;?>" >
						<input type="hidden" name="event_name" id="event_name" value="<?php echo $event_master_data['event_title'] ;?>" >
						<?php 
						$block_id = explode (",", $event_master_data['block_id']); 
						$block_id = implode ("','", $block_id);
						?>
						<h5 class="card-title text-primary text-center">Offline Booking</h5>
						<hr>
						
						<div class="form-group row">
								<label for="unit_id_main" class="col-sm-2 col-form-label">Branch <span class="text-danger">*</span> </label>
								<div class="col-sm-4">
									<select name="bId" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required>
										<option value="">-- Select Branch --</option> 
										<?php 
										print_r('$block_id');
										//print_r();
										if($block_id !=0){
											$bFltr = "AND block_id IN ('$block_id')";
										}
										$qb=$d->select("block_master","society_id='$society_id' $bFltr  $blockAppendQueryOnly");  
										while ($blockData=mysqli_fetch_array($qb)) {
										?>
										<option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
										<?php } ?>

									</select> 

								</div>
								<label for="input-14" class=" col-sm-2 col-form-label">Department <span class="required">*</span></label>
								
								<div class="col-sm-4">
									<select name="dId" id="dId" class="form-control single-select" onchange="getUserByFloorId(this.value);" required>
									<option value="">-- Select Department --</option> 
									<?php 
										$qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$bId' $blockAppendQuery");  
										while ($depaData=mysqli_fetch_array($qd)) {
									?>
									<option  <?php if($dId==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
									<?php } ?>
									
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="unit_id_main" class="col-sm-2 col-form-label">Booking for <span class="text-danger">*</span> </label>
								<div class="col-sm-4">
									<select onchange="resetBookingForm();" type="text" required="" class="form-control single-select" name="user_id" id="user_id">
										<option value="">-- Select Employee --</option>
										
									</select>

								</div>
								<label for="input-14" class=" col-sm-2 col-form-label">Event Date <span class="required">*</span></label>
								
								<div class="col-sm-4">
									<select class="form-control" name="events_day_id" id="events_day_id" onchange="getBookingANDAmountEventDetail()">

									 	<option value="">--Select--</option>


									 	<?php
										$today= date("Y-m-d");
										// $event_start_time = strtotime($event_start_date); // or your date as well
										// $event_end_time = strtotime($event_end_date. ' +1 day');
										// $datediff = $event_end_time - $event_start_time;
										// $days =  round($datediff / (60 * 60 * 24));
										$event_days_master_qry=$d->select("event_days_master","society_id='$society_id' and event_id ='$event_id' AND event_date >= '$today'","");
										while ($event_days_master_data = mysqli_fetch_array($event_days_master_qry)) {
											?>
											<option data-id="<?php echo $event_days_master_data['event_allow']; ?>" value="<?php echo $event_days_master_data['events_day_id']; ?>"><?php echo $event_days_master_data['event_day_name']."( ".date("d M Y", strtotime($event_days_master_data['event_date']))." )";?> <?php if($event_days_master_data['event_type']==0) { echo "Free";} else { echo 'Paid';} ?></option>
										<?php }  ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-2">
									<label for="input-14" class="col-form-label">Notes :</label>
								</div>
								<div class="col-sm-4"> <textarea class="form-control" name="notes" id="notes"></textarea></div>
							</div>
							
							<div id="eventDetailsDiv"></div>
							<div id="eventPersonDetails">  </div>
							<input type="hidden" name="eventPerson" id="eventPerson" value="0">
							<div id="payementDetailsDiv"></div>
							<div id="addBankDetails"> <input type="hidden" name="payment_type" id="payment_type" value=""> </div>
							<input type="hidden" name="isCash" id="isCash" value="">

							

							<div class="form-footer text-center">
								<input type="hidden" name="bookEvent" value="bookEvent">
								<button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> Confirm Booking</button>
							</div>
							
							
						</form>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
			<?php }
					$event_days_master_qry = $d->select("event_days_master","event_id = '$event_id'");
					//$event_days_master_data = mysqli_fetch_array($event_days_master_qry);
				 while ($event_days_master_data=mysqli_fetch_array($event_days_master_qry)) {
				 	?>
 		<div class="row">
			<div class="col-lg-12">
				<div class="card">
				 	<div class="card-body">
				 		 
				 		<div class=" row">
				 			 
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label text-uppercase" style="font-size: 14px;">Event Day Name : <?php echo $event_days_master_data['event_day_name']; ?> </h4>
							</div>
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Event Type: <?php if($event_days_master_data['event_type']=="0"){ echo "Free";} else {echo "Paid"; } ; ?> </h4>
							</div>
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;"> Date: <?php echo $eeData= date("d M Y h:i A", strtotime($event_days_master_data['event_date'].' '.$event_days_master_data['event_time'])) ; ?> </h4>
							</div>
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Booking Open : <?php
								$today=date('Y-m-d');
                            // $cTime= date("H:i:s");
		                            $expire = strtotime($event_days_master_data['event_date']);
		                            $today = strtotime("$today midnight");
								if( $today > $expire){
									$eventExpire= TRUE;
									echo "Auto Closed";
								}else  {
									$eventExpire= FALSE;
			                    if($event_days_master_data['active_status']=="0"){
			                      ?>
			                      <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $event_days_master_data['events_day_id']; ?>','eventClose');" data-size="small"/>
			                    <?php } else { ?>
			                      <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $event_days_master_data['events_day_id']; ?>','eventOpen');" data-size="small"/>
		                      <?php } 
								}
		                      ?></h4>
							</div>
							<?php
								$aq = $d->selectRow("SUM(going_person) as sum","event_attend_list","event_id = '$event_id' AND society_id='$society_id' AND events_day_id='$event_days_master_data[events_day_id]' AND book_status=1");
								$aData = mysqli_fetch_array($aq);
								$bookAdult= $aData['sum'];
								if ($bookAdult=='') {
									$bookAdult=0;
								}

								$qc = $d->selectRow("SUM(going_child) as sumchild","event_attend_list","event_id = '$event_id' AND society_id='$society_id' AND events_day_id='$event_days_master_data[events_day_id]' AND book_status=1");
								$cData = mysqli_fetch_array($qc);
								$bookChild=  $cData['sumchild'];
								if ($bookChild=='') {
									$bookChild=0;
								}
								$qg = $d->selectRow("SUM(going_guest) as sumguest","event_attend_list","event_id = '$event_id' AND society_id='$society_id' AND events_day_id='$event_days_master_data[events_day_id]' AND book_status=1");
								$gData = mysqli_fetch_array($qg);
								$bookGuest= $gData['sumguest'];
								if ($bookGuest=='') {
									$bookGuest=0;
								}
								?>

						</div>
						<?php if($event_days_master_data['event_type']=="1"){ 
							 $adult_charge = $event_days_master_data['adult_charge'];
                             $child_charge =   $event_days_master_data['child_charge'];
                             $guest_charge =   $event_days_master_data['guest_charge'];

                             $adult_charge_tenant  = $event_days_master_data['adult_charge_tenant'];
                             $child_charge_tenant =   $event_days_master_data['child_charge_tenant'];
                             $guest_charge_tenant =   $event_days_master_data['guest_charge_tenant'];
                             // if ($event_master_data['taxble_type']=='1') {
                             // 	 $gstValueAdult =  $adult_charge - ($adult_charge  * (100/(100+$event_master_data['tax_slab']))); 
                             // 	 $gstValueAdultTenant =  $adult_charge_tenant - ($adult_charge_tenant  * (100/(100+$event_master_data['tax_slab']))); 
                             //    $gstValueChild = $child_charge - ($child_charge  * (100/(100+$event_master_data['tax_slab']))); 
                             //    $gstValueChildTenant = $child_charge_tenant - ($child_charge_tenant  * (100/(100+$event_master_data['tax_slab']))); 
                             //    $gstValueGuest =$guest_charge - ($guest_charge  * (100/(100+$event_master_data['tax_slab'])));
                             //    $gstValueGuestTenant =$guest_charge_tenant - ($guest_charge_tenant  * (100/(100+$event_master_data['tax_slab'])));
                             //    // find igst
                             //    if ($event_master_data['gst']=="0") {
                             //       $adult_charge =   $adult_charge;
                             //       $child_charge =   $child_charge;
                             //       $guest_charge =   $guest_charge;

                             //       $adult_charge_tenant =   $adult_charge_tenant;
                             //       $child_charge_tenant =   $child_charge_tenant;
                             //       $guest_charge_tenant =   $guest_charge_tenant;
                             //    } else {
                             //       $adult_charge =   $adult_charge+$gstValueAdult;
                             //       $child_charge =   $child_charge+$gstValueChild;
                             //       $guest_charge =   $guest_charge+$gstValueGuest;

                             //       $adult_charge_tenant =   $adult_charge_tenant+$gstValueAdultTenant;
                             //       $child_charge_tenant =   $child_charge_tenant+$gstValueChildTenant;
                             //       $guest_charge_tenant =   $guest_charge_tenant+$gstValueGuestTenant;
                             //    }

                             // }
                             // echo $event_master_data['gst'];
                              $adult_charge= number_format($adult_charge,2);
                              $child_charge= number_format($child_charge,2);
                              $guest_charge= number_format($guest_charge,2);

                              $adult_charge_tenant= number_format($adult_charge_tenant,2);
                              $child_charge_tenant= number_format($child_charge_tenant,2);
                              $guest_charge_tenant= number_format($guest_charge_tenant,2);
						 ?>
						<div class="row">
							
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;"> Employee Charge: <?php echo $adult_charge; ?> </h4>

							</div>
							<?php if($event_days_master_data['event_allow']=="1"){  ?>
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;"> Child Charge: 
								<?php if ($event_days_master_data['maximum_pass_children']>0) {
								 echo $child_charge;  
								 } else {
								 echo "Not Allowed";
								}
								?> </h4>
							</div>
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;"> Guest Charge:
								 <?php if ($event_days_master_data['maximum_pass_guests']>0) {
								 echo $guest_charge;  
								 } else {
								 echo "Not Allowed";
								}
								?>
								</h4>

							</div>
							<?php } ?>
							<div class="col-md-3">

							</div>
							
							
							
						</div>
					<?php  }?>
						<div class=" row">
							
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Employee Booked: <?php echo $bookAdult.'/'.$event_days_master_data['maximum_pass_adult']; ?> </h4>

							</div>
							<?php if($event_days_master_data['event_allow']=="1"){  ?>
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Child Booked:
								 <?php if ($event_days_master_data['maximum_pass_children']>0) {
								 echo $bookChild.'/'.$event_days_master_data['maximum_pass_children'];  
								 } else {
								 echo "Not Allowed";
								} ?>
							</div>
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Guest Booked:
								 <?php if ($event_days_master_data['maximum_pass_guests']>0) {
								 echo $bookGuest.'/'.$event_days_master_data['maximum_pass_guests'];  
								 } else {
								 echo "Not Allowed";
								}
								?>
								</h4>
							</div>
							<?php } ?>
							<div class="col-md-3">
								<h4 for="input-16" class="col-form-label" style="font-size: 14px;">Total Booked: <?php echo $TotalBooked= $aData['sum']+$cData['sumchild']+$gData['sumguest']; ?> </h4>
							</div>
							
						</div>
						<div class=" row">
							<div class="col-md-12">
								
								<button type="button" onclick="rescheduleEvent('<?php echo $event_days_master_data['event_date'];?>','<?php echo $event_days_master_data['event_time']; ?>','<?php echo $event_days_master_data['events_day_id']; ?>','<?php echo $event_id; ?>',`<?php echo $event_days_master_data['event_day_name']; ?>`,` <?php echo $mainEvent; ?>`);" class="btn btn-info btn-sm waves-effect waves-light mr-1"><i class="fa fa-retweet"></i> Reschedule Event</button>
								<?php if ($TotalBooked>0) {
									$today=date('Y-m-d');
									$eData= date('Y-m-d',strtotime($event_master_data['event_end_date']));
								if ($today<=$eData) { 
								?>
								 <form style="float: left;" action="controller/eventController.php" method="post" >
					                <input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
					                <input type="hidden" name="events_day_id" value="<?php echo $event_days_master_data['events_day_id']; ?>">
					                <input type="hidden" name="event_name" value="<?php echo $mainEvent; ?>">
					                <input type="hidden" name="event_date" value="<?php echo $eeData; ?>">
					                
					                <input type="hidden" name="event_day_name" value="<?php echo $event_days_master_data['event_day_name']; ?>">
					                <input type="hidden" name="sndEventReminder" value="sndEventReminder">
					                <button type="submit" class="btn form-btn btn-success btn-sm waves-effect waves-light"><i class="fa fa-bell"></i> Send Event Reminder</button>
					              <?php } ?>
					                <a href="eventReport?eId=<?php echo $event_days_master_data['events_day_id'];?>&getReport=Get+Report" class=" btn btn-sm btn-warning waves-effect waves-light mr-1" ><i class="fa fa-file mr-1"></i>Booking Report</a>
					              </form>
								  
								  <?php if($event_days_master_data['event_type']==0) { 
									  
									 
									 ?>
								  
								  	<button style="float: left;"  type="button" onclick="cancelEventDayBooking('<?php echo $event_days_master_data['events_day_id']; ?>',`<?php echo $event_days_master_data['event_day_name']; ?>`,'<?php echo $event_days_master_data['event_date']; ?>','<?php echo $event_id; ?>')" class="btn btn-danger  mr-1  btn-sm waves-effect waves-light"><i class="fa fa-ban"></i> Cancel Event</button>
								  
								  <?php  } else  { 
									  
									 
									 ?>
									<button style="float: left;"  type="button" onclick="cancelPaidEventDayBooking('<?php echo $event_days_master_data['events_day_id']; ?>',`<?php echo $event_days_master_data['event_day_name']; ?>`,'<?php echo $event_days_master_data['event_date']; ?>','<?php echo $event_id; ?>')" class="btn btn-danger  mr-1 btn-sm waves-effect waves-light"><i class="fa fa-ban"></i> Cancel Event</button>
								  <?php } } ?>
					             <?php ?>	
							</div>
						</div>
						<form>
						<div class="row mt-3">
						<div class="col-md-6 col-6 form-group">
							<?php
							
							$events_daId = $event_days_master_data['events_day_id'];
							$blk_id = $_REQUEST['blk_id_'.$events_daId]; 
							//echo $blk_id;
							?>
							<?php $dpt_id = $_REQUEST['dpt_id_'.$events_daId]; ?>
							<input type="hidden" name="id" value="<?php echo $event_id; ?>">
							<select name="blk_id_<?php echo $events_daId; ?>"  class=" single-select" onchange="getEvntFltrFloorByBlockId(this.value);">
								<option value=""> Select Branch</option>
								<option <?php if($blk_id=="All"){ echo "selected"; } ?> value="All">All Branch</option>
								<?php
								$qbb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
								while ($blockDatab=mysqli_fetch_array($qbb)) {
								?>
								<option <?php if(isset($blk_id) && $blk_id==$blockDatab['block_id']){ echo "selected"; } ?>  value="<?php echo  $blockDatab['block_id'];?>" ><?php echo $blockDatab['block_name'];?></option>
								<?php } ?>
							</select>	
						</div>
						<div class="col-md-6 col-6 form-group">
							<select name="dpt_id_<?php echo $events_daId; ?>"  onchange="this.form.submit()" class=" single-select dpt_id" >
								<option value="">--Select--</option>
								<option <?php if($blk_id=="All"){ echo "selected"; }?> value="">All Department</option>
								<?php
								if(isset($blk_id) && $blk_id>0){

									$dptQ = $d->selectRow('floors_master.*','floors_master',"block_id=$blk_id");
									while ($dptDatab=mysqli_fetch_array($dptQ)) {
										?>
										<option <?php if(isset($dpt_id) && $dpt_id==$dptDatab['floor_id']){ echo "selected"; } ?>  value="<?php echo  $dptDatab['floor_id'];?>" ><?php echo $dptDatab['floor_name'];?></option>
										<?php } 
								}
								?>
								
							</select>	
						</div>
						</div>
						</form>
				 	</div>
					 
					
						
					
				 	<div class="table-responsive">
							<?php if($event_days_master_data['event_type']==0) { ?>
								<table id="example" class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th> Employee Name</th>
											<th> Employee</th>
											<?php if($event_days_master_data['event_allow']=="1"){  ?>
											<th> Child </th>
											<th> Guest </th>
											<?php } ?>
											<th>Notes</th>
											<th>Print</th>
										</tr>
									</thead>
									<tbody>
										<?php $events_day_id = $event_days_master_data['events_day_id'];
										 if(isset($dpt_id) && $dpt_id>0){
											$dptFltr = " AND users_master.floor_id=$dpt_id";
										 }
										 if(isset($blk_id) && $blk_id>0){
											$blkFltr = " AND users_master.block_id=$blk_id";
										 }

										$q=$d->select("event_attend_list,users_master","event_attend_list.user_id=users_master.user_id AND event_attend_list.event_id='$event_id' AND event_attend_list.society_id='$society_id' and event_attend_list.events_day_id='$events_day_id' $dptFltr $blkFltr","ORDER BY event_attend_list.event_attend_id DESC");
										$i = 0;
										while($row=mysqli_fetch_array($q))
										{
										extract($row);
										$i++;
										$user_id = $row['user_id'];
										?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php
													$uq = $d->select("unit_master","unit_id = '$unit_id' AND society_id='$society_id'");
													$uData = mysqli_fetch_array($uq);
													$bq = $d->selectRow("block_master.block_name","block_master,unit_master","unit_master.unit_id='$unit_id' AND unit_master.block_id = block_master.block_id");
													$bData = mysqli_fetch_array($bq);
											
					                        if(!empty($row['user_full_name']))
					                        {
					                          echo $row['user_full_name'] . " - ";
					                        }
											if(!empty($row['user_designation']))
					                        {
					                          echo $row['user_designation'] . " - ";    
					                        }
											echo "(".$bData['block_name'].")";
											
											?></td>
											<td><?php echo $going_person; ?></td>
											<?php if($event_days_master_data['event_allow']=="1"){  ?>
											<td><?php echo $going_child; ?></td>
											<td><?php echo $going_guest; ?></td>
											<?php } ?>
											<td><?php echo $notes; ?></td>
											<td>
												<?php if($row['book_status']==1){ 
												if ($eventExpire==TRUE) {echo "Event Expire";
												} else {?>
												<form action="controller/eventController.php" method="post" >
													<a  href="eventPassesPrint.php?id=<?php echo $event_attend_id; ?>&user_id=<?php echo $user_id; ?>&society_id=<?php echo $_COOKIE['society_id']; ?>" class="btn btn-success btn-sm"><i class="fa fa-print"></i> Passes</a>

														 <input type="hidden" name="cancelEventBooking" value="cancelEventBooking">
															<input type="hidden" name="event_day_name" value="<?php echo $event_days_master_data['event_day_name']; ?>">
															<input type="hidden" name="event_date" value="<?php echo $event_days_master_data['event_date']; ?>">
															<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
															<input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
															<input type="hidden" name="event_attend_id" value="<?php echo $row['event_attend_id']; ?>">
														<?php 	if (count($blockAryAccess)>0 && in_array($row['block_id'], $blockAryAccess)) { ?>
														<button type="submit" class="form-btn btn btn-danger btn-sm" name=""><i class="fa fa-cross"></i> Cancel</button>
														<?php } else if(count($blockAryAccess)==0) { ?>
															<button type="submit" class="form-btn btn btn-danger btn-sm" name=""><i class="fa fa-cross"></i> Cancel</button>
														<?php }?>
													</form>
												 <?php } } else if($row['book_status']==3){
												 	echo "Cancelled";
												 }?>
											</td>
										</tr>
										<?php }?>
									</tbody>
								</table>
							<?php } else  { ?>
								<table id="example" class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th> Employee Name</th>
											<th> Employee</th>
											<th> Child </th>
											<th> Guest </th>
											<th> Received Amount </th>
											<th> Transaction Charges</th>
											<th> Notes</th>
											<th>Print</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$events_day_id = $event_days_master_data['events_day_id'];

										if(isset($dpt_id) && $dpt_id>0){
											$dptFltr2 = " AND users_master.floor_id=$dpt_id";
										 }
										 if(isset($blk_id) && $blk_id>0){
											$blkFltr2 = " AND users_master.block_id=$blk_id";
										 }

										$q=$d->select("event_attend_list,users_master","event_attend_list.user_id=users_master.user_id AND event_attend_list.event_id='$event_id' AND event_attend_list.society_id='$society_id' and event_attend_list.events_day_id='$events_day_id'$dptFltr2  $blkFltr2","ORDER BY event_attend_list.event_attend_id DESC");
										$i = 0;
										while($row=mysqli_fetch_array($q))
										{
										extract($row);
										$i++;
										$user_id = $row['user_id'];
										?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php
													$uq = $d->select("unit_master","unit_id = '$unit_id' AND society_id='$society_id'");
													$uData = mysqli_fetch_array($uq);
													$bq = $d->selectRow("block_master.block_name","block_master,unit_master","unit_master.unit_id='$unit_id' AND unit_master.block_id = block_master.block_id");
													$bData = mysqli_fetch_array($bq);
											
					                          echo $row['user_full_name'] . " - ";
											if(!empty($row['user_designation']))
					                        {
					                          echo $row['user_designation'];    
					                         }
											?>

											</td>
											<td><?php echo $going_person; ?></td>
											<td><?php echo $going_child; ?></td>
											<td><?php echo $going_guest; ?></td>
											<td> <?php echo number_format($recived_amount-$transaction_charges,2); ?></td>
											<td><?php echo $transaction_charges; ?></td>
											<td><?php echo $notes; ?></td>
											<td>
												<?php if($row['book_status']==1){ ?>
												<a  href="eventPassesPrint.php?id=<?php echo $event_attend_id; ?>&user_id=<?php echo $user_id; ?>&society_id=<?php echo $_COOKIE['society_id']; ?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Passes</a>

												<a   href="invoice.php?user_id=<?php echo $row['user_id'] ?>&unit_id=<?php echo $row['unit_id'] ?>&type=E&societyid=<?php echo $row['society_id'] ?>&id=<?php echo $row['event_id'] ?>&event_attend_id=<?php echo $row['event_attend_id'] ?>" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Invoice</a>
												<?php 	if (count($blockAryAccess)>0 && in_array($row['block_id'], $blockAryAccess)) { ?>
												<a href="#" onclick="cancelEvent('<?php echo $event_attend_id; ?>')" class=" btn btn-sm btn-danger waves-effect waves-light " data-toggle="modal" data-target="#billPay" href="javascript:void();"> Cancel</a>
												<?php } else if(count($blockAryAccess)==0) { ?>
													<a href="#" onclick="cancelEvent('<?php echo $event_attend_id; ?>')" class=" btn btn-sm btn-danger waves-effect waves-light " data-toggle="modal" data-target="#billPay" href="javascript:void();"> Cancel</a>

												<?php } } else if($row['book_status']==3){
												 	echo "Cancelled";
												 	if ($row['refund_amount']>0) {
												 		echo "<br>";
												 		echo "Refund Amount : " .$row['refund_amount'];
												 	}
												 }?>

											</td>
										</tr>
										<?php }?>
									</tbody>
								</table>
							<?php } ?>
							</div>
							</div>
			</div>
		</div>
		<?php
			     }
					?>
				
					<?php //IS_140 ?>
			<!-- End Of Event Details -->
			
			</div>
		</div>
		<!-- End container-fluid-->
		<!--End content-wrapper-->
		<!--Start Back To Top Button-->
<script type="text/javascript">
  function popitup(url) {
  newwindow=window.open(url,'name','height=800,width=900, location=0');
  if (window.focus) {newwindow.focus()}
  return false;
  }
</script>

<script src="assets/js/jquery.min.js"></script>
<!-- <script src="assets/js/custom17.js"></script> -->

<?php //IS_910 ?>
<div class="modal fade" id="billPay">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Cancel Event </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="cancelPaidEvent">
        
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="cancelEventDayBookingModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Reasons to Cancel Event</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="" action="controller/eventController.php" enctype="multipart/form-data" method="post">
					<div class="form-group row">
                 
						<label for="input-13" class="col-sm-4 col-form-label"> Day Name</label>
						<div class="col-sm-8 free_event_day_name">
							
						</div> 
				  	</div>
					<div class="form-group row">
						<label for="input-14" class="col-sm-4 col-form-label">Date </label>
							<div class="col-sm-8 free_event_date">
							
							</div>
					</div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea required class="form-control" name="refund_remark"></textarea>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                    <input type="hidden" name="cancelEventDayBooking"  value="cancelEventDayBooking">
                      <input type="hidden" name="events_day_id" id="free_events_day_id">
                      <input type="hidden" name="event_day_name"  id="free_event_day_name">
                      <input type="hidden" name="event_date"  id="free_event_date">
                      <input type="hidden" name="event_id"  id="free_event_id">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Cancel Event </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="cancelPaidEventDayBookingModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Reasons to Cancel Event</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="" action="controller/eventController.php" enctype="multipart/form-data" method="post">
					<div class="form-group row">
                 
						<label for="input-13" class="col-sm-4 col-form-label"> Day Name</label>
						<div class="col-sm-8 paid_event_day_name">
							
						</div> 
				  	</div>
					<div class="form-group row">
						<label for="input-14" class="col-sm-4 col-form-label">Date </label>
							<div class="col-sm-8 paid_event_date">
							
							</div>
					</div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea required class="form-control" name="refund_remark"></textarea>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                    <input type="hidden" name="cancelPaidEventDayBooking"  value="cancelPaidEventDayBooking">
                      <input type="hidden" name="events_day_id" id="paid_events_day_id">
                      <input type="hidden" name="event_day_name"  id="paid_event_day_name">
                      <input type="hidden" name="event_date"  id="paid_event_date">
                      <input type="hidden" name="event_id"  id="paid_event_id">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Cancel Event </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="rescheduleEventModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Reschedule Event</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="rescheduleEventForm" action="controller/eventController.php" enctype="multipart/form-data" method="post">
					<div class="form-group row">
                 
						<label for="input-13" class="col-sm-4 col-form-label"> Day Name</label>
						<div class="col-sm-8 event_day_name">
							
						</div> 
				  	</div>
					
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Event Date <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
						<input required="" type="text" readonly="" class="form-control event_start_time" id="event-start-date" value="" name="event_start_time" data-dtp="dtp_E1fhm">
                        </div>                   
                    </div>  
					<div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Event Time <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
						<select maxlength="8" type="text" class="form-control" id="event_time" value="" name="event_time" required="" placeholder="Start Time">
						<?php
						$hourFormat = 'H:i';
							$minutesInterval = new DateInterval('PT15M');
							$tNow = new DateTime();
							$tStart = new DateTime();
							$tStart->setTime(00, 00);
							$tEnd = new DateTime();
							$tEnd->setTime(23, 45);
							$tCount = clone $tStart;
							while($tCount <= $tEnd){
							$attribute = "";
							$dateFormatted = $tCount->format($hourFormat);
							?>
							<option <?php if($event_time_new==$dateFormatted) { echo 'selected';} ?> value="<?php echo $dateFormatted; ?>" <?php echo $attribute; ?>>
								<?php echo $dateFormatted; ?>
							</option>
							<?php
								$tCount->add($minutesInterval);
							} ?>
						</select>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                    <input type="hidden" name="rescheduleEvent"  value="rescheduleEvent">
                      <input type="hidden" name="events_day_id" id="rs_events_day_id">
                      <input type="hidden" name="event_id"  id="rs_event_id">
					  <input type="hidden" name="event_day_name"  id="rs_event_day_name">
					  <input type="hidden" name="event_title"  id="rs_event_title">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Reschedule Event </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>

<!--End Modal -->

<script >
	function getEvntFltrFloorByBlockId(id) {
		alert("ss,d");
	}
</script>
