<?php 
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

if (isset($_COOKIE['bms_admin_id'])) {
    $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
extract(array_map("test_input" , $_POST));

//IS_605
if(isset($common_entry_id)  ){

  $q=$d->select("common_entry_master","common_entry_id='$common_entry_id' AND society_id='$society_id'");
$row=mysqli_fetch_array($q);
extract($row);
 ?>

 
<form id="EditCommEntryFrm" action="controller/commonEntryController.php" method="POST" enctype="multipart/form-data" >
              <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />  
                 
                <div class="form-group row">
                 
               <label for="input-13" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="common_entry_name" id="common_entry_name" value="<?php if(isset($common_entry_id)) {echo $common_entry_name;} ?> ">
                  </div> 
                </div>
                 <div class="form-group row">
                    <label for="input-14" class="col-sm-2 col-form-label">Image </label>
                  <div class="col-sm-10">
                    <?php if(isset($common_entry_id)){ ?>
                      <input type="hidden" name="common_entry_image_old" value="<?php echo $common_entry_image;?>">
                    <?php }?>
                    <input required="" accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG"   type="file" class="form-control-file photoOnly border"  name="common_entry_image">
                    (Photo  No more than 4 MB)
                  </div>
                </div>

                 

               
                  
                 
                    <input type="hidden" name="common_entry_image_old"  value="<?php echo $common_entry_image; ?>"> 
                    <div class="form-footer text-center">
                    <input type="Hidden" name="common_entry_id" value="<?php echo $common_entry_id;?>">
                    <input type="submit" id="updateCommEntryBtn" class="btn btn-primary" name="updateCommEntry" value="update">
                  </div>
                  
              </form>
              <script src="assets/js/jquery.min.js"></script>
              <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
 <script type="text/javascript">
   //12march2020

    jQuery(document).ready(function($){
$.validator.addMethod('filesize4MB', function (value, element, arg) {
        var size =4000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 4MB."));

 $.validator.addMethod("noSpace", function(value, element) { 
   return value == '' || value.trim().length != 0;  
}, "No space please and don't leave it empty");



 
$("#EditCommEntryFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            common_entry_name:
            {
                required: true,
                noSpace: true 
            },
            common_entry_image:
            {
                required: false,
                filesize4MB: true 
            },
        },
        messages: {
            common_entry_name: {
                required: "Please enter common entry name", 
       }
      }
});
});
//12march2020
 </script>
        
<?php }   ?>