<?php
error_reporting(0);
extract($_REQUEST);
if(filter_var($emp_id, FILTER_VALIDATE_INT) != true){
   $_SESSION['msg1']='Invalid Url';
     echo ("<script LANGUAGE='JavaScript'>
      window.location.href='employees';
      </script>");
}
if (isset($emp_id)) {
  $q=$d->select("employee_master","society_id='$society_id' AND emp_id='$emp_id'");
  $row=mysqli_fetch_array($q);
  extract($row);
}
if (isset($manageEmployeeEye) && $manageEmployeeEye=='eye') {
?>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="election" action="controller/employeeController.php" method="post">
                <?php if(isset($emp_id) ){  ?>
                  <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                  <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
                  <input type="hidden" name="emp_token" value="<?php echo $emp_token; ?>">
                <?php } ?>
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-eye"></i>
                  Manage <?php echo $xml->string->gate_keeper; ?> Eye Attendance of <?php echo $emp_name;?>
                </h4>
                <div class="form-group row">
                  <label for="password-field" class="col-sm-4 col-form-label">Eye Attendance Status <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                      <select  type="text" required="" class="form-control ey_attendace" name="ey_attendace">
                        
                          <option <?php if($ey_attendace==0) { echo "selected"; }?>  value="0">Off </option>
                          <option <?php if($ey_attendace==1) { echo "selected"; }?>  value="1">On </option>
                        </select>
                  </div>
                </div>
               
                <div style="<?php if($ey_attendace==0) { echo 'display: none'; } ?>" class="form-group row" id="eyeAttendance1">
                  <label for="password-field" class="col-sm-4 col-form-label">Attendance Time <span class="text-danger">*</span></label>
                    <div class="col-lg-4">
                      <input type="text" required="" readonly class="form-control timepicker"  value="<?php if(isset($emp_id) && $start_time!="00:00:00") { echo date('h:i A',strtotime($start_time));} ?>" name="start_time" placeholder="Start Time">
                    </div>
                    <div class="col-lg-4">
                      <input type="text" required="" readonly class="form-control timepicker"  value="<?php if(isset($emp_id) && $end_time!="00:00:00") { echo  date('h:i A',strtotime($end_time));} ?>" name="end_time" placeholder="End Time">
                    </div>
                </div>
                <div  style="<?php if($ey_attendace==0) { echo 'display: none'; } ?>"  class="form-group row" id="eyeAttendance">
                  <label for="password-field" class="col-sm-4 col-form-label">Duration <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                      <select  type="text" required="" class="form-control" name="duration">
                        <option value="">-- Select --</option>
                        <?php if ($adminData['role_id']==2)  { ?>
                          <option <?php if($duration==2) { echo "selected"; }?>   value="2">Every 2 Minutes (Only for Testing) </option>
                       <?php  } ?>
                          <option <?php if($duration==30) { echo "selected"; }?>   value="30">Every 30 Minutes </option>
                          <option <?php if($duration==60) { echo "selected"; }?> value="60">Every 1 Hour </option>
                          <option <?php if($duration==120) { echo "selected"; }?> value="120">Every 2 Hour </option>
                          <option <?php if($duration==180) { echo "selected"; }?> value="180">Every 3 Hour </option>
                          <option <?php if($duration==240) { echo "selected"; }?> value="240">Every 4 Hour </option>
                          <option <?php if($duration==300) { echo "selected"; }?> value="300">Every 5 Hour </option>
                        </select>
                  </div>
                </div>
                <div class="form-footer text-center">
                
                    <input type="hidden" name="updateAttendace" value="updateAttendace">
                    <button type="submit" class="btn btn-success" name="addElectionBtn"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->


        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
              <form action="" method="get">
                <input type="hidden" name="emp_id" value="<?php echo $emp_id;?>">
               <div class="row pt-2 pb-2">
                  <div class="col-lg-4 col-6">
                    <label  class="form-control-label">From Date </label>
                    <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>">  
                  </div>
                  <div class="col-lg-4 col-6">
                    <label  class="form-control-label">To Date </label>
                    <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>">  
                  </div>
                  <div class="col-lg-2 col-6">
                       <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
                  </div>
                  <div class="col-lg-2 col-6 pt-4">
                   <a href="javascript:void(0)" onclick="DeleteAll('deleteEyeReport');" class="btn mb-1  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                  </div>
                </div>
              </form>
               </div>
                <div class="table-responsive">
                  <table id="example" class="table table-bordered">
                    <thead>
                      <tr>
                         <th>#</th>
                        <th>#</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Photo</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        if (isset($_GET['from']) && $_GET['from']!='') {
                          $today = $_GET['from'].date(" 00:00:01");
                          $todayEnd = $_GET['toDate'].date(" 23:59:00");
                        } else {
                          $today = date("Y-m-d 00:00:00");
                          $todayEnd = date("Y-m-d 23:59:00");

                        }
                          $q=$d->select("gatekeeper_eye_report","report_type=0 AND scannedTime BETWEEN '$today' AND '$todayEnd' AND society_id='$society_id' AND emp_id='$emp_id'","ORDER BY eye_id DESC","LIMIT 500");
                        $i = 0;
                        while($row=mysqli_fetch_array($q)) {
                          $i++;
                        
                      ?>
                        <tr>
                          <td class="text-center">
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $row['eye_id']; ?>">

                        </td>
                          <td><?php echo $i; ?></td>
                          <td><?php echo  date('d M Y',strtotime($row['scannedTime'])); ?></td>
                          <td><?php echo  date('h:i A',strtotime($row['scannedTime'])); ?></td>
                          <td><a href="../img/gatekeeper_eye/<?php echo $row['eye_photo']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["eye_photo"]; ?>"><img width='50' height='50' src="../img/gatekeeper_eye/<?php echo $row['eye_photo']; ?>"  alt="" ></a></td>
                          
                        </tr>
                      <?php }?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

    </div>
    <!-- End container-fluid-->
<?php }  else {
   $q=$d->select("employee_master","society_id='$society_id' AND emp_id='$emp_id'");
  $row=mysqli_fetch_array($q);
  extract($row);

  ?>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row pt-2 pb-2">
                   <div class="col-lg-6 col-6 pt-6">
                    <h6 class="form-header text-uppercase">
                      <i class="fa fa-qrcode"></i>
                      Manage QR Code Stickers
                    </h6>
                  </div>
                  <div class="col-lg-6 col-6 pt-6 text-right">
                   
                   <a href="javascript:void(0)" data-toggle="modal" data-target="#billCategoryModal"  class="btn mb-1 mr-2 btn-sm btn-primary pull-right"><i class="fa fa-plus fa-lg"></i> Add </a>

                   <a href="printQrcode.php?emp_id=<?php echo $row[qr_code_master_id]; ?>"  class="btn btn-warning btn-sm mr-2 "><i class="fa fa-qrcode"></i> Print All QR</a>

                  </div>
                </div>

                <table id="default-datatable" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Area</th>
                        <th>Print</th>
                         <th>#</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                       
                        $q=$d->select("qr_code_master","society_id='$society_id' ","LIMIT 50");
                        $i = 0;
                        while($row=mysqli_fetch_array($q)) {
                          $i++;
                      ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo  $row['area_name']; ?></td>
                          <td>
                            <a title="Print I-Card" href="window.open('printQrcode.php?id=<?php echo $row[qr_code_master_id]; ?>&isSingle=true" class="btn btn-info btn-sm"><i class="fa fa-print"></i> Print</a>

                          </td>
                          <td class="text-center">
                          <form action="controller/qrController.php" method="post">
                            <input type="hidden" name="deleteMasterQr" value="deleteMasterQr">
                            <input type="hidden" name="qr_code_master_id" value="<?php echo $row['qr_code_master_id']; ?>">
                            <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                            <button class="btn form-btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                          </form>

                        </td>
                          
                        </tr>
                      <?php }?>
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="election" action="controller/employeeController.php" method="post">
                <?php if(isset($emp_id) ){  ?>
                  <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                  <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
                  <input type="hidden" name="emp_token" value="<?php echo $emp_token; ?>">
                <?php } ?>
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-qrcode"></i>
                  Manage <?php echo $xml->string->gate_keeper; ?> QR Attendance of <?php echo $emp_name;?>
                </h4>
                <div class="form-group row">
                  <label for="password-field" class="col-sm-4 col-form-label">QR Attendance Status <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                      <select  type="text" required="" class="form-control ey_attendace" name="qr_attendace">
                        
                          <option <?php if($qr_attendace==0) { echo "selected"; }?>  value="0">Off </option>
                          <option <?php if($qr_attendace==1) { echo "selected"; }?>  value="1">On </option>
                        </select>
                  </div>
                </div>
               <div  style="<?php if($qr_attendace==0) { echo 'display: none'; } ?>"  class="form-group row" id="eyeAttendance">
                  <label for="password-field" class="col-sm-4 col-form-label">Type <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                    <div class="icheck-inline">
                      <input <?php if($qr_attendace_self==0){echo "checked ";} ?>  required="" checked="" type="radio" id="anyTime qr_attendace_self radio_1" value="0" name="qr_attendace_self">
                      <label for="anyTime">Any Time Scan</label>
                    </div>
                    <div class="icheck-inline">
                      <input <?php if($qr_attendace_self==1){echo "checked ";} ?> required="" type="radio" id="DurationWise Feet qr_attendace_self" value="1" name="qr_attendace_self">
                      <label for="DurationWise">Duration Wise</label>
                    </div>

                  </div>
                </div>
                <div style="<?php if($qr_attendace==0 || $qr_attendace_self==0) { echo 'display: none'; } ?>" class="form-group row eyeAttendance1" id="eyeAttendance1">
                  <label for="password-field" class="col-sm-4 col-form-label timeLble">Attendance Time <span class="text-danger">*</span></label>
                    <div class="col-lg-4 timeLbleDiv">
                      <input type="text" required="" readonly class="form-control timepicker"  value="<?php if(isset($emp_id) && $start_time!="00:00:00") { echo date('h:i A',strtotime($start_time));} ?>" name="start_time" placeholder="Start Time">
                    </div>
                    <div class="col-lg-4 timeLbleDiv">
                      <input type="text" required="" readonly class="form-control timepicker"  value="<?php if(isset($emp_id) && $end_time!="00:00:00") { echo  date('h:i A',strtotime($end_time));} ?>" name="end_time" placeholder="End Time">
                    </div>
                </div>
                <div  style="<?php if($qr_attendace==0) { echo 'display: none'; } ?>"  class="form-group row eyeAttendance" id="eyeAttendance">
                  

                  <label style="<?php if($qr_attendace_self==0) { echo 'display: none'; } ?>" for="password-field" class="col-sm-4 col-form-label alertDurationLbl">Alert Duration <span class="text-danger">*</span></label>
                  <div style="<?php if($qr_attendace_self==0) { echo 'display: none'; } ?>" class="col-sm-8 alertDurationLblDiv">
                      <select  type="text" required="" class="form-control" name="duration">
                        <option value="">-- Select --</option>
                        <?php if ($adminData['role_id']==2)  { ?>
                          <option <?php if($duration==2) { echo "selected"; }?>   value="2">Every 2 Minutes (Only for Testing) </option>
                       <?php  } ?>
                          <option <?php if($duration==30) { echo "selected"; }?>   value="30">Every 30 Minutes </option>
                          <option <?php if($duration==60) { echo "selected"; }?> value="60">Every 1 Hour </option>
                          <option <?php if($duration==120) { echo "selected"; }?> value="120">Every 2 Hour </option>
                          <option <?php if($duration==180) { echo "selected"; }?> value="180">Every 3 Hour </option>
                          <option <?php if($duration==240) { echo "selected"; }?> value="240">Every 4 Hour </option>
                          <option <?php if($duration==300) { echo "selected"; }?> value="300">Every 5 Hour </option>
                        </select>
                  </div>
                </div>
                <div class="form-footer text-center">
                
                    <input type="hidden" name="updateAttendaceQr" value="updateAttendaceQr">
                    <button type="submit" class="btn btn-success" name="addElectionBtn"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->


        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
              <form action="" method="get">
                <input type="hidden" name="emp_id" value="<?php echo $emp_id;?>">
               <div class="row pt-2 pb-2">
                  <div class="col-lg-4 col-6">
                    <label  class="form-control-label">From Date </label>
                    <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>">  
                  </div>
                  <div class="col-lg-4 col-6">
                    <label  class="form-control-label">To Date </label>
                    <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>">  
                  </div>
                  <div class="col-lg-2 col-6">
                       <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
                  </div>
                  <div class="col-lg-2 col-6 pt-4">
                   <a href="javascript:void(0)" onclick="DeleteAll('deleteEyeReport');" class="btn mb-1  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                  </div>
                </div>
              </form>
               </div>
                <div class="table-responsive">
                  <table id="example" class="table table-bordered">
                    <thead>
                      <tr>
                         <th>#</th>
                        <th>#</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Place</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        if (isset($_GET['from']) && $_GET['from']!='') {
                          $today = $_GET['from'].date(" 00:00:01");
                          $todayEnd = $_GET['toDate'].date(" 23:59:00");
                        } else {
                          $today = date("Y-m-d 00:00:00");
                          $todayEnd = date("Y-m-d 23:59:00");

                        }
                          $q=$d->select("gatekeeper_eye_report,qr_code_master","qr_code_master.qr_code_master_id=gatekeeper_eye_report.qr_code_id AND gatekeeper_eye_report.report_type=1 AND gatekeeper_eye_report.scannedTime BETWEEN '$today' AND '$todayEnd' AND gatekeeper_eye_report.society_id='$society_id' AND gatekeeper_eye_report.emp_id='$emp_id'","ORDER BY gatekeeper_eye_report.eye_id DESC","LIMIT 500");
                        $i = 0;
                        while($row=mysqli_fetch_array($q)) {
                          $i++;
                        
                      ?>
                        <tr>
                          <td class="text-center">
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $row['eye_id']; ?>">

                        </td>
                          <td><?php echo $i; ?></td>
                          <td><?php echo  date('d M Y',strtotime($row['scannedTime'])); ?></td>
                          <td><?php echo  date('h:i A',strtotime($row['scannedTime'])); ?></td>
                          <td><?php echo  $row['area_name']; ?></td>
                          
                        </tr>
                      <?php }?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

    </div>


<div class="modal fade" id="billCategoryModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add QR Code</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="addQrCode" method="post" action="controller/qrController.php" enctype="multipart/form-data">
            <div class="row form-group">
              <label class="col-sm-6 form-control-label">Number Of QR Code<span class="text-danger">*</span></label>
              <div class="col-sm-6">
                <select onchange="getQrBox();" maxlength="2" id="no_of_qr" class="form-control onlyNumber" inputmode="numeric" required="" name="no_of_qr">
                  <option value="">  -- Select --</option>
                  <?php for ($i=1; $i <51 ; $i++) {  ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <div class="qrDiv">


            </div>
            <div class="form-footer text-center">
              <input type="hidden" name="addMasterQr" value="addMasterQr">
              <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
              <button class="btn btn-sm btn-success"><i class="fa fa-check"></i> Next</button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>


<?php } ?>  
  <script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  $('.ey_attendace').change(function(){
            var data= $(this).val();
             if(data==1) {
              $('#eyeAttendance1').show();
              $('#eyeAttendance').show();
              $('.eyeAttendance1').hide();
              $("input[name=qr_attendace_self][value='0']").prop("checked",true);
             } else {
              $('#eyeAttendance1').hide();
              $('#eyeAttendance').hide();
              $('.eyeAttendance').hide();
             }          
          });

</script>

<script type="text/javascript">


 

  $( "#addQrCode" ).submit(function( event ) {
    var error = 0;
    $('.option_name-cls').each(function() {
      var clsVal =  $(this).val();
      var eleId = $(this).attr('id');

      if($.trim(clsVal) ==""){
        error++;
          /*$(eleId+'_div').removeClass('valid');
          $(eleId+'_div').addClass('error');*/


          $('#'+eleId+'_div').css('display','inline-block');
          $('#'+eleId+'_div').css('color','#ff0000');
          $('#'+eleId+'_div').text("Please enter name");
          $(".ajax-loader").hide();
        } else {
          /*$(eleId).addClass('valid');
          $(eleId).removeClass('error');*/
          $(".ajax-loader").show();
          $('#'+eleId+'_div').css('display','none');
        }
      });

//IS_1309

var valOption = [];

 
          $('.option_name-cls').each(function(i){
            valOption[i] = $(this).val();
          });
        
          $('.option_name-cls').each(function() {
                var clsVal =  $(this).val();
                var eleId = $(this).attr('id');
          var numOccurences =1;

           if($.trim(clsVal) !=""){  
                numOccurences = $.grep(valOption, function (elem) {
                  return elem.toLowerCase() === clsVal.toLowerCase();
              }).length;

                 if(numOccurences>1){
                  error++;
                        $('#'+eleId+'_div').css('display','inline-block');
                        $('#'+eleId+'_div').css('color','#ff0000');
                        $('#'+eleId+'_div').text("Duplicate Option");
                } else {
                     $('#'+eleId+'_div').css('display','none');
                  }
          }
            });
          //IS_1309
          //alert(error);

              if($('#isOptionAdded').val()=="No"){
                error++;
                $('#no_of_option-error').text("Please select No QR Code");
                $('#no_of_option').addClass('error');
              }

              


              if(error > 0 ){  

                event.preventDefault();
              }  else {  
                $( "#addQrCode" ).submit();
              } 
  });



</script>