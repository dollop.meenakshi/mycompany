  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-7">
        <h4 class="page-title">Primary Employee Report</h4>

        </div>
        <div class="col-sm-3 col-5 text-right">
            
        </div>
      
     </div>

       
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 

                 $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND users_master.user_type = '0' and unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND  users_master.member_status=0  AND users_master.user_status!=0 $blockAppendQuery","ORDER BY unit_master.unit_id ASC");

                  $i=1;
               ?>
                
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $xml->string->block; ?></th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Device</th>
                       
                    </tr>
                </thead>
                <tbody>
                  <?php   while ($data=mysqli_fetch_array($q3)) {  ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['floor_name']; ?>-<?php echo $data['block_name']; ?></td>
                         
                        <td><?php echo $data['user_full_name'];  ?></td>
                        <td><?php  if ($adminData['role_id']==2) {
                          echo $data['country_code'] . " " . $data['user_mobile']; 
                        } else {
                            echo $data['country_code'] . " " . substr($data['user_mobile'], 0, 2) . '*****' . substr($data['user_mobile'],  -3);
                        } ?></td>
                        <td>
                          <?php  if ($adminData['role_id']==2) {
                          echo $data['user_email']; 
                        } else {
                            echo "".substr($data['user_email'], 0, 2) . '*****' . substr($data['user_email'],  -3);
                        } ?>
                      </td>
                        <td> <?php echo $data['device']; ?></td>
                        
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->