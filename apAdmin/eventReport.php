<?php 
error_reporting(0);
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='eventReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Events  Report</h4>
        </div>
     
     </div>

    <form action="" method="get" id="eventReportFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-6 col-8">
              <select required="" class="form-control single-select" name="eId">
                <option value="">-- Select Event --</option>
                  <?php  $q=$d->select("event_master,event_days_master","event_days_master.event_id=event_master.event_id AND event_master.society_id='$society_id'","ORDER BY event_master.event_id DESC");
                        while ($mData=mysqli_fetch_array($q)) {
                          $eId=$mData['event_id']; ?>
                          <option <?php if($mData['events_day_id']==$_GET['eId']) { echo 'selected';} ?> value="<?php echo $mData['events_day_id'] ?>"><?php echo $mData['event_day_name']; ?> - <?php echo $mData['event_title']; ?> <?php if($mData['event_type']=="0"){   ?><span class="badge badge-pill badge-success m-1">(Free)</span><?php } else { ?><span class="badge badge-pill badge-danger m-1">(Paid)</span><?php }?> <?php echo date('Y-m-d',strtotime($mData['event_date']));?></option>
                  <?php } ?>
             </select>
          </div>
          
           
          <div class="col-lg-2 col-4">
              <input  class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
          

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                
                if (isset($_GET['eId'])) {
                  $events_day_id= (int)$_GET['eId'];
                  $eqqq = $d->select("event_master,event_days_master","event_days_master.event_id=event_master.event_id AND event_days_master.events_day_id = '$events_day_id'");
                  $event_days_master_data=mysqli_fetch_array($eqqq);
                  $event_id= $event_days_master_data['event_id'];

               ?>
                
              <?php if($event_days_master_data['event_type']==0) { ?>
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th> Event</th>
                      <th> Event Date</th>
                      <th> <?php echo $xml->string->unit; ?></th>
                      <th> Adult</th>
                      <th> Child </th>
                      <th> Guest </th>
                      <th> Notes</th>
                      <th> Status</th>
                      
                      <th> Booking Date</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php 

                    $q=$d->select("event_attend_list,users_master","event_attend_list.user_id=users_master.user_id AND event_attend_list.event_id='$event_id' AND event_attend_list.society_id='$society_id' and event_attend_list.events_day_id='$events_day_id' ","ORDER BY event_attend_list.event_attend_id DESC");
                    $i = 0;
                    while($row=mysqli_fetch_array($q))
                    {
                    extract($row);
                    $i++;
                    $user_id = $row['user_id'];
                    ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $event_days_master_data['event_day_name'].'-'.$event_days_master_data['event_title']; ?></td>
                      <td><?php echo $event_days_master_data['event_date']; ?></td>
                      <td><?php
                         
                          $bq = $d->selectRow("block_master.block_name","block_master,unit_master","unit_master.unit_id='$unit_id' AND unit_master.block_id = block_master.block_id");
                          $bData = mysqli_fetch_array($bq);
                      echo $row['user_full_name'].'-'.$row['user_designation'] ." (".$bData['block_name'].')';
                      
                      ?></td>
                      <td><?php echo $going_person; ?></td>
                      <td><?php echo $going_child; ?></td>
                      <td><?php echo $going_guest; ?></td>
                      <td><?php echo $notes; ?></td>
                      <td>
                        <?php if($row['book_status']==1){ 
                        echo "Booked";
                       } else if($row['book_status']==3){
                          echo "Cancelled";
                         }?>
                      </td>

                       <td><?php if($row['payment_received_date']!="") { echo date('Y-m-d h:i A', strtotime($row['payment_received_date'])); } ?></td>
                      
                    </tr>
                    <?php }?>
                  </tbody>
                  
                </table>
              <?php } else  { ?>
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th> Event</th>
                      <th> Event Date</th>
                      <th> <?php echo $xml->string->unit; ?></th>
                      <th> Adult</th>
                      <th> Child </th>
                      <th> Guest </th>
                      <th> Received Amount </th>
                      <th>Transaction Charges</th>
                      <th> Notes</th>
                      <th>Status</th>
                      <th>Invoice Number</th>
                      <th>Booking Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $q=$d->select("event_attend_list,users_master","event_attend_list.user_id=users_master.user_id AND event_attend_list.event_id='$event_id' AND event_attend_list.society_id='$society_id' and event_attend_list.events_day_id='$events_day_id' ","ORDER BY event_attend_list.event_attend_id DESC");
                    $i = 0;
                    while($row=mysqli_fetch_array($q))
                    {
                    extract($row);
                    $i++;
                    $user_id = $row['user_id'];
                    ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $event_days_master_data['event_day_name'].'-'.$event_days_master_data['event_title']; ?></td>
                      <td><?php echo $event_days_master_data['event_date']; ?></td>
                      <td><?php
                         
                          $bq = $d->selectRow("block_master.block_name","block_master,unit_master","unit_master.unit_id='$unit_id' AND unit_master.block_id = block_master.block_id");
                          $bData = mysqli_fetch_array($bq);
                        echo $row['user_full_name'].'-'.$row['user_designation'] ." (".$bData['block_name'].')';

                      ?>

                      </td>
                      <td><?php echo $going_person; ?></td>
                      <td><?php echo $going_child; ?></td>
                      <td><?php echo $going_guest; ?></td>
                      <td><?php echo number_format($recived_amount-$transaction_charges,2,'.',''); ; ?></td>
                      <td><?php echo $transaction_charges; ?></td>
                      <td><?php echo $notes; ?></td>
                      <td>
                        <?php if($row['book_status']==1){ 

                          echo "Booked";
                         } else if($row['book_status']==3){
                          echo "Cancelled";
                         }?>
                      </td>
                      <td><?php echo 'INVEV'.$row['event_attend_id']; ?></td>
                       <td><?php if($row['payment_received_date']!="") { echo date('Y-m-d h:i A', strtotime($row['payment_received_date'])); } ?></td>
                    </tr>
                    <?php }?>
                  </tbody>
                  
                </table>
              <?php } ?>
            

            <?php } else {  ?>
              <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select Event</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->