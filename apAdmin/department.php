<div class="content-wrapper">
    <div class="container-fluid"> 
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"><?php echo $xml->string->add; ?>/<?php echo $xml->string->edit; ?> <?php echo $xml->string->floor; ?></h4>
       
     </div>
     <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb--> 
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="floorValidation" action="controller/floorController.php" method="post">
                
                <div class="form-group row">
                   <label for="input-12" class="col-sm-2 col-form-label"><?php echo $xml->string->block; ?> <span class="required">*</span></label>
                  <div class="col-sm-4">
                   <select  name="block_id" class="form-control">
                    <option value="">-- <?php echo $xml->string->block; ?> --</option>
                    <?php 
                    $i=1;
                    $q = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","");
                    while ($data=mysqli_fetch_array($q)) {
                       $floorCount= $d->count_data_direct("floor_id","floors_master","society_id='$society_id' AND  block_id='$data[block_id]'");
                       if($floorCount==0) {
                     ?>
                     <option value="<?php echo $data['block_id'];?>"><?php echo $data['block_name']; ?></option>
                     <?php } } ?>
                   </select>
                  </div>
                 
                   <label for="input-12" class="col-sm-2 col-form-label"><?php echo $xml->string->number_of; ?> <?php echo $xml->string->floor; ?> <span class="required">*</span></label>
                    <div class="col-sm-4">
                    <input  maxlength="3" autocomplete="off" onkeyup="getFloorList();" type="text" id="no_of_floor" class="form-control" id="input-10" name="no_of_floor">
                  </div>
               
                 
                </div> 
                <div class="form-group row">
                  
                 
                 
                 
                </div> 
               <div id="floorResp" class="form-group row">

               </div>
                <div class="form-footer text-center">
                <input type="hidden" value="0" name="no_of_unit">
                <input type="hidden" value="1" name="unit_type">
                <input type="hidden" name="addFloorMulti" value="addFloorMulti">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->save; ?></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->