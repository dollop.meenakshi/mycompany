<?php
$all = [];
$all_retailer = [];
if(isset($_GET['DId']) && !empty($_GET['DId']))
{
    $distributor_id = $_GET['DId'];
    $CId = $_GET['CId'];
    if ((int) $distributor_id != $_GET['DId'])
    {
        $_SESSION['msg1'] = "Invalid Id!";
    ?>
    <script>
        window.location = "manageDistributor?CId=<?php echo $CId; ?>";
    </script>
    <?php
    exit;
    }
    $q = $d->selectRow("dm.*,rm.retailer_name,amn.area_name","distributor_master AS dm LEFT JOIN retailer_distributor_relation_master AS rdrm ON rdrm.distributor_id = dm.distributor_id LEFT JOIN retailer_master AS rm ON rm.retailer_id = rdrm.retailer_id LEFT JOIN area_master_new AS amn ON amn.area_id = dm.distributor_area_id","dm.distributor_id = '$distributor_id' AND dm.society_id = '$society_id'");
    if(mysqli_num_rows($q) > 0)
    {
        while($data = $q->fetch_assoc())
        {
            $all_retailer[] = $data['retailer_name'];
            $all[] = $data;
        }
    }
    else
    {
        $_SESSION['msg1'] = "Invalid Id!";
    ?>
    <script>
        window.location = "manageDistributor?CId=<?php echo $CId; ?>";
    </script>
    <?php
    exit;
    }
    $all_retailer = array_unique($all_retailer);
}
else
{
    $CId = $_GET['CId'];
    $_SESSION['msg1'] = "Invalid Id!";
?>
<script>
    window.location = "manageDistributor?CId=<?php echo $CId; ?>";
</script>
<?php
exit;
}
?>
<style>
    .small-width{
        width: 50px;
    }
</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Distributor Details</h4>
            </div>
            <div class="col-sm-3 col-6">
                <div class="btn-group float-sm-right">
                    <form class="mr-2" action="addDistributor" method="post">
                        <input type="hidden" name="distributor_id" value="<?php echo $distributor_id; ?>">
                        <input type="hidden" name="editDistributor" value="editDistributor">
                        <button type="submit" class="btn btn-sm btn-warning"> Edit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body wrapper">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="table-responsive">
                                    <table class="table w-100">
                                        <tr>
                                            <th class="small-width">Distributor Name</th>
                                            <td><?php echo $all[0]['distributor_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Distributor Code</th>
                                            <td><?php echo $all[0]['distributor_code']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Contact Person</th>
                                            <td><?php echo $all[0]['distributor_contact_person']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Contact Number</th>
                                            <td><?php echo $all[0]['distributor_contact_person_country_code'] . " " . $all[0]['distributor_contact_person_number']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Alternate Number</th>
                                            <td><?php echo ($all[0]['distributor_alt_contact_number'] == 0) ? "" : $all[0]['distributor_alt_contact_number_country_code'] . " " . $all[0]['distributor_alt_contact_number']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Order Email</th>
                                            <td><?php echo $all[0]['distributor_email']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Distributor Email</th>
                                            <td><?php echo $all[0]['distributor_email_main']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Address</th>
                                            <td style="white-space: normal !important;"><?php echo $all[0]['distributor_address']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Pincode</th>
                                            <td><?php echo $all[0]['distributor_pincode']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Area</th>
                                            <td><?php echo $all[0]['area_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">GST</th>
                                            <td><?php echo $all[0]['distributor_gst_no']; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="small-width">Retailer</th>
                                            <td style="white-space: normal !important;">
                                            <?php
                                            foreach($all_retailer AS $key1 => $value1)
                                            {
                                            ?>
                                                <span class="badge badge-info"><?php echo $value1; ?></span>
                                            <?php
                                            }
                                            ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php
                                        if($all[0]['distributor_photo'] != "" && file_exists("../img/users/recident_profile/".$all[0]['distributor_photo']))
                                        {
                                        ?>
                                        <img class="img-fluid p-4" style="height:300px;" src="../img/users/recident_profile/<?php echo $all[0]['distributor_photo']; ?>"/>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="col-sm-12 p-3">
                                        <div class="map" id="map2" style="width: 100%; height: 300px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $d->map_key(); ?>"></script>
<script>
function initMap()
{
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };

    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("map2"), mapOptions);
    map.setTilt(50);

    // Multiple markers location, latitude, and longitude
    <?php
    if(isset($all[0]['distributor_latitude']) && !empty($all[0]['distributor_latitude']) && isset($all[0]['distributor_longitude']) && !empty($all[0]['distributor_longitude']))
    {
    ?>
    var markers = [
        ["<?php echo $all[0]['distributor_name']; ?>","<?php echo $all[0]['distributor_latitude']; ?>","<?php echo $all[0]['distributor_longitude']; ?>"]
    ];
    <?php
    }
    else
    {
    ?>
    var markers = [
        ["<?php echo $all[0]['distributor_name']; ?>","23.037786","72.512043"]
        ];
    <?php
    }
    ?>

    // Info window content
    var infoWindowContent = [
        ["<div class='info_content'><h6><?php echo $all[0]['distributor_name']; ?></h6><p><?php echo ($all[0]['distributor_address'] != "") ? $all[0]['distributor_address'] : ""; ?></p><p><?php echo $all[0]['distributor_contact_person']; ?></p><p><?php echo $all[0]['distributor_contact_person_country_code'] . " " . $all[0]['distributor_contact_person_number']; ?></p></div>"]
    ];

    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Place each marker on the map
    for( i = 0; i < markers.length; i++ )
    {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });

        // Add info window to marker
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event)
    {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });
}

// Load initialize function
google.maps.event.addDomListener(window, 'load', initMap);
</script>