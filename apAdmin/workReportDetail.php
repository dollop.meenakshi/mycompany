<?php
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$base_url=$m->base_url();
$work_report_id = $_POST['work_report_id'];
$society_id=$_COOKIE['society_id'];

?>
<?php

if ($work_report_id != '') {

    $q = $d->selectRow('*', "work_report_master,users_master,floors_master", "work_report_master.user_id=users_master.user_id AND work_report_master.floor_id=floors_master.floor_id AND work_report_master.work_report_id='$work_report_id'");
    $data=mysqli_fetch_array($q); ?>
    
    <div class="col-md-6 mb-3">
        <b>Name </b>
        <p class="d-block" ><?php echo $data['user_full_name'] ?></p>
    </div>
    <div class="col-md-6 mb-3">
        <b>Designation </b>
        <p class="d-block" ><?php echo $data['user_designation'] ?></p>
    </div>
    <div class="col-md-6 mb-3">
        <b>Department Name </b>
        <p class="d-block" ><?php echo $data['floor_name'] ?></p>
    </div>
    <div class="col-md-6 mb-3">
        <b>Date</b>
        <p class="d-block" ><?php echo $data['work_report_date'] ?></p>
    </div>
    <?php if($data['work_report_type'] == 0){ ?>
    <div class="col-md-12 mb-3">
        <b>Work Report </b>
            <p class="d-block" ><?php echo $data['work_report'] ?></p>
    </div>
        <?php if($data['work_report_file'] != ''){ ?>
    <div class="col-md-12 mb-3">
        <b>Attachment </b>
            <p class="d-block" ><?php 
                $jsonArray = json_decode($data['work_report_file'],true);

                if (is_array($jsonArray)) {
                    for ($i=0; $i < count($jsonArray); $i++) {
                        $i1 = $i+1;
                        $document = $jsonArray["document_".$i];
                        $ext = pathinfo($document, PATHINFO_EXTENSION);
                          $singleView= 0;
                          if ($ext == 'pdf' || $ext == 'PDF') {
                            $imgIcon = 'img/pdf.png';
                            $singleView= 0;
                          } else if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                            $imgIcon = 'img/jpg.png';
                             $singleView= 1;
                          } else if ($ext == 'png') {
                            $imgIcon = 'img/png.png';
                             $singleView= 1;
                          } else if ($ext == 'doc' || $ext == 'docx') {
                            $imgIcon = 'img/doc.png';
                             $singleView= 0;
                          } else {
                            $imgIcon = 'img/doc.png';
                             $singleView= 0;
                          }
                        if($singleView==1) {
                            echo "<a data-fancybox='images' data-caption='Photo Name : Work Report Attachment' href=".$base_url."img/work_report/".$document.">$i1. $document</a><br>";
                        } else {
                            echo "<a href=".$base_url."img/work_report/".$document.">$i1. $document</a><br>";
                        }
                    }
                }
             ?></p>
    </div>
    <?php } } else{ ?>
    <div class="col-md-12 mb-3">
        <b>Work Report </b>
        <?php $q1 = $d->selectRow('*', "work_report_employee_ans", "work_report_id='$work_report_id'");
            $counter = 1;
            while ($data1=mysqli_fetch_array($q1)) { ?>
                <h6><?php echo $counter++.') '.$data1['work_report_template_question'] ?></h6>
                <div class="ml-4">
                    <?php if($data1['work_report_question_type'] == 0){ ?>
                        <div class="form-group col-md-4">
                        <p class="d-block" ><?php echo $data1['work_report_employee_ans'] ?></p>
                        </div>
                    <?php }elseif($data1['work_report_question_type'] == 1){ 
                        $optionValueArr = json_decode($data1['work_report_question_type_value'], true);
                        for ($i=0; $i < count($optionValueArr); $i++) {
                            $check = '';
                            if($optionValueArr['option_'.$i] == $data1['work_report_employee_ans']){ 
                                $check = 'checked';
                            }
                            ?>
                            <input type="radio" disabled id="<?php echo 'radio_'.$i.$data1['work_report_employee_ans_id']; ?>" name="radio<?php echo $data1['work_report_employee_ans_id']?>" value="<?php echo $optionValueArr['option_'.$i] ?>" <?php echo $check; ?>>
                            <label for="<?php echo 'radio_'.$i.$data1['work_report_employee_ans_id']; ?>"><?php echo $optionValueArr['option_'.$i] ?></label><br>
                        <?php } ?>
                    <?php }elseif($data1['work_report_question_type'] == 2){
                        $optionValueArr = json_decode($data1['work_report_question_type_value'], true);
                        $workReportAnswer = json_decode($data1['work_report_employee_ans'], true);
                       
                        for ($i=0; $i < count($optionValueArr); $i++) {
                                $optionKey = "option_$i";
                            ?>
                            <input type="checkbox" disabled id="<?php echo 'checkbox_'.$i.$data1['work_report_employee_ans_id']; ?>" name="checkbox" value="<?php echo $optionValueArr['option_'.$i] ?>" <?php if(array_key_exists($optionKey, $workReportAnswer)){ echo 'checked';} ?>>
                            <label for="<?php echo 'checkbox_'.$i.$data1['work_report_employee_ans_id']; ?>"><?php echo $optionValueArr['option_'.$i] ?></label><br>
                        <?php } ?>
                    <?php }elseif($data1['work_report_question_type'] == 3){ ?>
                        <div class="form-group col-md-4">
                            <p class="d-block" ><?php echo $data1['work_report_employee_ans'] ?></p>  
                        </div>
                    <?php }elseif($data1['work_report_question_type'] == 4){
                        if ($data1['has_multi_attachment']==1) { ?>
                           <div class="form-group col-md-4">
                            <?php if($data1['work_report_employee_ans']!="") { 
                                    $workReportEmployeeAns = $data1['work_report_employee_ans'];
                                    $jsonArray = json_decode($workReportEmployeeAns,true);
                                    for ($iAtte=0; $iAtte < count($jsonArray); $iAtte++) {
                                        $fileNameAtt = $jsonArray[$iAtte]['file'];
                                        echo "<a href='../img/work_report/$fileNameAtt'>".$jsonArray[$iAtte]['file']."</a>";
                                        echo "<Br>";
                                    }
                             }?>
                            </div>

                       <?php } else { ?>
                        <div class="form-group col-md-4">
                            <?php if($data1['work_report_employee_ans']!="") { ?>
                            <p class="d-block" ><a href="../img/work_report/<?php echo $data1['work_report_employee_ans'] ?>"  target="_blank" >Attachment </a></p>
                            <?php }?>
                        </div>
                    <?php } }elseif($data1['work_report_question_type'] == 5){ ?>
                        <div class="form-group col-md-4">
                            <p class="d-block" ><?php echo $data1['work_report_employee_ans'] ?></p>
                        </div>
                    <?php }elseif($data1['work_report_question_type'] == 6){ ?>
                        <div class="form-group col-md-4">
                            <p class="d-block" ><?php echo $data1['work_report_employee_ans'] ?></p>
                        </div>
                    <?php }elseif($data1['work_report_question_type'] == 7){ ?>
                        <div class="form-group col-md-4">
                            <p class="d-block" ><?php echo $data1['work_report_employee_ans'] ?></p>
                        </div>
                    <?php }elseif($data1['work_report_question_type'] == 8){ ?>
                        <div class="form-group col-md-4">
                            <p class="d-block" ><?php echo $data1['work_report_employee_ans'] ?></p>
                        </div>
                    <?php }elseif($data1['work_report_question_type'] == 9){ ?>
                        <div class="form-group col-md-4">
                            <?php if($data1['work_report_employee_ans']!="") { ?>
                            <div class="progress">
                              <div class="progress-bar  progress-bar-striped" style="width:<?php echo $data1['work_report_employee_ans']; ?>%"><?php echo $data1['work_report_employee_ans']; ?>%</div>
                            </div>
                            <?php } ?>
                        </div>
                     <?php }elseif($data1['work_report_question_type'] == 10){ ?>
                        <div class="form-group col-md-4">
                            <?php 
                             $workReportEmployeeAns = $data1['work_report_employee_ans'];
                            $jsonArray = json_decode($workReportEmployeeAns,true);
                             if (isset($jsonArray) && count($jsonArray)>0) { ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Topic</th>
                                        <th>Time </th>
                                    </tr>
                                    <?php  for ($i=0; $i < count($jsonArray) ; $i++) {
                                        $topicTime = $jsonArray[$i]["time"];
                                        if ($topicTime != '' && 
                                            filter_var($topicTime, FILTER_VALIDATE_INT) == true) {
                                            $topicTimeCount = $topicTimeCount + $topicTime;
                                        }
                                      ?>
                                    <tr>
                                        <td><?php echo $jsonArray[$i]['topic']; ?></td>
                                        <td><?php echo $jsonArray[$i]['time']; ?> Minutes</td>
                                    </tr>
                                   <?php  }  ?>
                                   <tr>
                                        <th>Total Time</th>
                                        <th><?php echo $topicTimeCount;?> Minutes</th>
                                   </tr>
                                </table>
                             <?php }
                             ?>
                        </div>
                    <?php }elseif($data1['work_report_question_type'] == 11){ ?>
                        <div class="form-group col-md-4">
                            <?php 
                            $workReportEmployeeAns = $data1['work_report_employee_ans'];
                            $jsonArray = json_decode($workReportEmployeeAns,true);
                                echo $jsonArray['description'].' ('.$jsonArray['time'].' Min)';
                            ?>
                        </div>
                    <?php } ?>
                </div>
            <?php }  ?>
    </div>
<?php } }?>
