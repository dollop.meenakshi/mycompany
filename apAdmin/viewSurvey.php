<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-6">
        <h4 class="page-title">Survey</h4>
      </div>
      <div class="col-sm-3 col-6">
        <div class="btn-group float-sm-right">
          <a href="addSurvey" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          <a href="javascript:void(0)" onclick="DeleteAll('deleteSurvey');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>Survey Title</th>
                    <th>survey date</th>
                    <th> For</th>
                    <th>Questions</th>
                    <th>Status</th>
                    <th>Action</th>
                    
                    
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $q=$d->select("survey_master","society_id='$society_id'","ORDER BY survey_id DESC");
                  $iNo = 1;
                  while($row=mysqli_fetch_array($q))
                  {
                  extract($row);
                  $result_found = $d->count_data_direct("survey_id","survey_result_master","survey_id ='$row[survey_id]' AND society_id='$society_id'");
                  
                  ?>
                  <tr>
                    <td class='text-center'>
                      <?php
                      if($result_found <1) {
                      ?>
                      <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['survey_id']; ?>">
                      <?php } else { ?> 
                        <input disabled="" type="checkbox" class="multiDelteCheckbox"  value="<"> 
                      <?php } ?>
                      
                    </td>
                    <td><?php echo  $iNo++; ?></td>
                    <td><?php echo custom_echo($row['survey_title'],50); ?></td>
                    <td><?php echo  date("d M Y
                    ", strtotime($row['survey_date'])); ?></td>
                    <td><?php
                      $election_for_string= $xml->string->election_for; 
                       $electionArray = explode("~", $election_for_string);
                      if ($survey_for==0) {
                        echo  $all= $xml->string->all.' '. $xml->string->floors; 

                        } else {
                          $qf=$d->selectRow("floor_name,block_name","floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floor_id='$row[survey_for]'");
                          $floorData=mysqli_fetch_array($qf);
                          echo $floorData['floor_name'] . " (" . $floorData['block_name'] .")";
                        }
                        ?>
                    </td>
                    <td>
                      <form action="surveyQuestions" method="get">
                        <input type="hidden" name="survey_id" id="survey_id" value="<?php echo $row['survey_id'] ?>">
                        <button type="submit" class="btn btn-primary btn-sm" name="Questions">Questions (<?php $survey_question_master_qry=$d->select("survey_question_master","survey_id ='$row[survey_id]' AND society_id='$society_id'");
                     echo $totalQue= mysqli_num_rows($survey_question_master_qry);
                      ?>)</button>
                        
                      </form>
                      
                    </td>

                   
                      <td>
                      <?php
                       $cTime= date("H:i:s");
                      $today = strtotime("today midnight");
                      $expire = strtotime($row['survey_date']);
                     
                      
                       if($row['survey_status']=="0"  && $totalQue>0){
                      ?>
                        <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['survey_id']; ?>','SurveyClose');" data-size="small"/> Close Survey
                        <?php } else  if($row['survey_status']=="1" && $totalQue>0) {
                         ?>
                       <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['survey_id']; ?>','SurveyActive');" data-size="small"/> Open Survey
                      <?php } else  if($row['survey_status']=="2" && $totalQue>0) {
                         ?>
                       <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['survey_id']; ?>','SurveyActive');" data-size="small"/> Start Survey
                      <?php }  else {
                        echo "Add Questions";
                      }   ?>
                    </td>

                    <td>
                      <div style="display: inline-block;">
                        <form action="surveyQuestions" method="get">
                          <input type="hidden" name="survey_id" id="survey_id" value="<?php echo $row['survey_id'] ?>">
                          <input type="hidden" name="viewResult" id="viewResult" value="yes">
                          <button type="submit" class="btn btn-primary btn-sm" name="viewResultSurvey">View</button>
                          
                        </form>
                      </div>
                      
                      
                      
                      <?php  if($result_found ==0) { ?>
                      <div style="display: inline-block;">
                        <form   class="mr-2" action="addSurvey" method="get">
                          <input type="hidden" name="survey_id" value="<?php echo $row['survey_id']; ?>">
                          <input type="hidden" name="society_id" value="<?php echo $row['society_id']; ?>">
                          <input type="hidden" name="action" value="Edit">
                          <button type="submit" class="btn btn-sm btn-warning"> Edit</button>
                        </form>
                      </div>
                      <div style="display: inline-block;">
                        <form   class="mr-2" action="controller/surveyController.php" method="POST">
                          <input type="hidden" name="survey_id" value="<?php echo $row['survey_id']; ?>">
                          <input type="hidden" name="society_id" value="<?php echo $row['society_id']; ?>">
                          <input type="hidden" name="deleteSurvey" value="deleteSurvey">
                          <button type="submit" class="btn btn-sm btn-danger form-btn"> Delete</button>
                        </form>
                      </div>
                      <?php } ?>
                    </td>
                    
                    
                    
                  </tr>
                  <?php }?>
                </tbody>
                
              </div>
            </table>
          </div>
        </div>
      </div>
    </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->
  
  </div><!--End content-wrapper-->
  <!--Start Back To Top Button-->