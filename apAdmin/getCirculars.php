<?php

session_start();
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
include 'common/checkLanguage.php';

$d = new dao();
$m = new model();
$society_id=$_COOKIE['society_id'];
$blockAryAccess = array();
$bms_admin_id = $_COOKIE['bms_admin_id'];
  $blockCheck=$d->select("admin_block_master","admin_id='$bms_admin_id' ");
  while ($accessBlock=mysqli_fetch_array($blockCheck)) {
      array_push($blockAryAccess , $accessBlock['block_id']);
  }
  if (count($blockAryAccess)>0) {
    $blockids = join("','",$blockAryAccess); 
    $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

  } else {
    $blockAppendQuery = "";
    $blockAppendQueryOnly="";
  }
 
extract(array_map("test_input" , $_POST));

if(isset($limit) && isset(($offset))){
  $offSet = $offset * $limit;
  $limitQuery = " LIMIT $limit OFFSET $offSet ";
}

  
  $q=$d->select("notice_board_master","society_id='$society_id'  ","ORDER BY updated_at DESC");
          if(mysqli_num_rows($q)>0) {
            while($row=mysqli_fetch_array($q)){ ?> 
              <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card">
                   <?php //IS_569 title="<?php echo $row['notice_title']; ?>
                  <div class="card-header bg-primary text-white" title="<?php echo $row['notice_title'];?>"><i class="fa fa-bullhorn" aria-hidden="true"></i> <?php 
                  //IS_569
                  // echo $row['notice_title'];
                  $strlen = strlen($row['notice_title']);
                  if($strlen > 40 ){
                    $str = substr($row['notice_title'], 0, 30);
                    echo $str."...";
                  } else {
                     echo $row['notice_title'];
                  }
                   //IS_569
                 ?> 
                  
                </div>
                  <div class="card-body">
                     <?php //IS_569 <div class="cls-notice-info"> 
                     ?>
                    <p class="card-text"><div class="cls-notice-info"><?php echo stripslashes($row['notice_description']);?></div></p>
                  </div>
                  <?php //IS_983

                  if($row['block_id'] =="0" || $row['block_id'] ==""){
                    $block_name_data = "All";
                  } else{ 

                    $block_master_qry=$d->select("block_master","block_id in (".$row['block_id'].")  ","");
                    $block_name_data = array();
                     while ($block_master_data=mysqli_fetch_array($block_master_qry)) {
                        $block_name_data[] = $block_master_data['block_name'];
                     }

                      $block_name_data = implode(", ",  $block_name_data);
                    //$block_master_data =   mysqli_fetch_assoc($block_master_qry);
                    //$block_name_data = $block_master_data['block_name'];
                  }

                  


                  ?>
                  <div  class="card-footer">
                  <?php if ($row['notice_attachment']!='') {
                    echo "<a target='_blank' href=../img/noticeBoard/$row[notice_attachment]><i class='fa fa-paperclip'></i> ".$xml->string->attachment."</a>";
                    echo "<br>";
                  } else {
                    echo "<br>";
                  } ?>
                  Circular For: <?php echo $block_name_data;?> <?php echo $xml->string->block;?>  <?php 
                  if ($row['noticeboard_type']==1) {
                    echo  '('.$d->count_data_direct("notice_board_unit_id","notice_board_unit_master","society_id='$society_id' AND notice_board_id='$row[notice_board_id]'").' Department)';
                  }
                  ?> <br>


                    <?php   //IS_1008
                    // Updated on: <?php echo $row['notice_time']; 
                     if($row['updated_at'] ==NULL && $row['created_at'] ==NULL){ ?>
                      <?php echo $xml->string->updated_on; ?>: <?php   echo  date("d M Y h:i A", strtotime($row['notice_time']));?><br>
                    <?php  } else  if($row['updated_at'] ==NULL){ ?>
                     ?php echo $xml->string->created_on; ?>: <?php echo date("d M Y h:i A", strtotime($row['created_at']));?><br>
                 <?php  } else { ?>
                  
                     <?php echo $xml->string->updated_on; ?>: <?php   echo date("d M Y h:i A", strtotime($row['updated_at']));?><br>
                <?php  } //IS_1008 
                      $selectedBlocks = explode(",",$row['block_id']);
                      $difrentResult=array_diff($selectedBlocks,$blockAryAccess);
                      if (count($difrentResult)>0  && $blockAppendQueryOnly!='' && $row['block_id']!=0) {
                     
                         
                      } else {

                      ?>
                      Status: 
                        <?php if ($row['active_status']==0) { ?>
                        <span class="badge badge-pill badge-primary m-1"><?php echo $xml->string->published; ?> </span>
                      <?php } else { ?>
                        <span class="badge badge-pill badge-warning m-1"><?php echo $xml->string->save_as_draft; ?> </span>
                      <?php } ?>
                     <br>

                 
                    <form class="mt-2" id="signupForm" style="float: left;" action="controller/noticeController.php" method="post">
                      <input type="hidden" name="block_id" value="<?php echo $row['block_id']; ?>">
                      <input type="hidden" name="notice_board_id_delete" value="<?php echo $row['notice_board_id']; ?>">
                      <button type="submit" class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash-o"></i> <?php echo $xml->string->delete; ?></button>
                    </form>
                    <form class="mt-2" style="float: left;margin-left: 10px;" action="circularAdd" method="post">
                      <input type="hidden" name="notice_board_id" value="<?php echo $row['notice_board_id']; ?>">
                      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i> <?php echo $xml->string->edit; ?></button>
                    </form>
                    <?php } 
                    if($row['active_status'] != 1)
                    {
                    ?>
                      <form class="mt-2" style="float: left;margin-left: 10px;" action="circularAdd" method="post">
                        <input type="hidden" name="block_id" value="<?php echo $row['block_id']; ?>">
                        <input type="hidden" name="notice_board_id_read" value="<?php echo $row['notice_board_id']; ?>">
                        <button title="Read Status" type="submit" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> <?php echo $xml->string->read_status; ?></button>
                      </form>
                    <?php 
                  } ?>
                  </div>
                </div>
              </div>
              <?php 
            } 
          } else if($offset==0) {
      echo "<img src='img/no_data_found.png'>";
    }
     
    ?>
    <script type="text/javascript">
      
$('.form-btn').on('click',function(e){
    e.preventDefault();
    var form = $(this).parents('form');
     swal({
        title: "Are you sure?",
        text: "",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ['Cancel', 'Yes, I am sure !'],
      })
     .then((willDelete) => {
        if (willDelete) {
          form.submit();
        }
      });

});
    </script>