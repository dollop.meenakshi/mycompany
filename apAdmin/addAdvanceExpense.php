<style>
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
-webkit-appearance: none;
margin: 0;
}
</style>
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" /> -->
<?php
extract(array_map("test_input", $_POST));
if(isset($edit_give_expense))
{
    $q = $d->selectRow("ue.*,um.block_id","user_expenses AS ue JOIN users_master AS um ON um.user_id = ue.user_id","user_expense_id = '$user_expense_id'");
    $data = $q->fetch_assoc();
    extract($data);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row  ">
            <div class="col-sm-6">
                <h4 class="page-title">Add Advance Expenses</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="giveExpenseForm" action="controller/ExpenseSettingController.php" enctype="multipart/form-data" method="post">
                            <div class="form-group row">
                                <label for="amount" class="col-lg-2 col-md-2 col-form-label">Branch<span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <select id="block_id" name="block_id" class="form-control inputSl single-select" onchange="getFloorByBlockIdAddSalary(this.value)" required="">
                                        <option value="">Select Branch</option>
                                        <?php
                                        $qd = $d->select("block_master", "block_master.society_id='$society_id' AND block_master.block_status=0 $blockAppendQueryOnly ");
                                        while ($depaData = mysqli_fetch_array($qd)) {
                                        ?>
                                        <option <?php if ($data['block_id'] == $depaData['block_id']) { echo 'selected';} ?> <?php if ($block_id == $depaData['block_id']) {echo 'selected';} ?> value="<?php echo  $depaData['block_id']; ?>"><?php echo $depaData['block_name']; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label for="input-10" class="col-sm-2 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <select type="text" id="floor_id" required="" onchange="getUserByFloorId(this.value)" class="form-control single-select floor_id inputSl" name="floor_id">
                                        <option value="">-- Select --</option>
                                        <?php
                                        if (isset($data)) {
                                        $floors = $d->select("floors_master,block_master", "block_master.block_id =floors_master.block_id AND  floors_master.society_id='$society_id' AND floors_master.block_id = '$data[block_id]' $blockAppendQuery");
                                        while ($floorsData = mysqli_fetch_array($floors)) {
                                        ?>
                                        <option <?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] == $data['floor_id']) {
                                            echo "selected";
                                            } ?> value="<?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] != "") {
                                            echo $floorsData['floor_id'];
                                            } ?>"> <?php if (isset($floorsData['floor_name']) && $floorsData['floor_name'] != "") {
                                            echo $floorsData['floor_name'] . "(" . $floorsData['block_name'] . ")";
                                        } ?></option>
                                        <?php } }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-2 col-form-label">Employee <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <select name="user_id" id="user_id" class="inputSl form-control single-select">
                                        <option value="">-- Select Employee --</option>
                                        <?php
                                        if (isset($data)) {
                                        $user = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND user_status !=0 AND floor_id = '$data[floor_id]' $blockAppendQueryUser");
                                        while ($userdata = mysqli_fetch_array($user)) {
                                        ?>
                                        <option <?php if ($data['user_id'] == $userdata['user_id']) {
                                            echo 'selected';
                                            } ?> value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name']; ?>
                                        </option>
                                        <?php } } ?>
                                    </select>
                                </div>
                                <label for="expense_title" class="col-sm-2 col-form-label">Title <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <input type="text" autocomplete="off" maxlength="80" name="expense_title" id="expense_title" required class="form-control" value="<?php if(isset($edit_give_expense)){ echo $expense_title; } ?>"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-lg-2 col-md-2 col-form-label">Date<span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <input type="text" readonly autocomplete="off" name="date" id="date" class="form-control past-dates-datepicker" value="<?php if(isset($edit_give_expense)){ echo $date; } ?>"/>
                                </div>
                                <label for="description" class="col-sm-2 col-form-label">Description <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                <input type="text" autocomplete="off" name="description" id="description" maxlength="200" class="form-control" value="<?php if(isset($edit_give_expense)){ echo $description; } ?>"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="expense_document" class="col-lg-2 col-md-2 col-form-label">Attachment</label>
                                <div class="col-lg-4 col-md-4">
                                    <input type="file" name="expense_document" id="expense_document" accept="image/jpeg,image/png,application/pdf,image/jpg" class="form-control"/>
                                </div>
                                <label for="amount" class="col-sm-2 col-form-label">Amount <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <input type="text" required autocomplete="off" name="amount" id="amount" class="form-control decimal unit-amount" value="<?php if(isset($edit_give_expense)){ echo trim($amount, "-"); } ?>"/>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <input type="hidden" name="csrf" value='<?php echo $_SESSION["token"]; ?>'>
                                <?php
                                if(isset($edit_give_expense))
                                {
                                ?>
                                    <input type="hidden" name="old_document" value="<?php echo $expense_document; ?>"/>
                                    <input type="hidden" name="bId" value="<?php echo $_POST['bId']; ?>"/>
                                    <input type="hidden" name="dId" value="<?php echo $_POST['dId']; ?>"/>
                                    <input type="hidden" name="uId" value="<?php echo $_POST['uId']; ?>"/>
                                    <input type="hidden" name="getReport" value="<?php echo $_POST['getReport']; ?>"/>
                                    <input type="hidden" name="f" value="<?php echo $_POST['f']; ?>"/>
                                    <input type="hidden" id="user_expense_id" name="user_expense_id" value="<?php echo $data['user_expense_id']; ?>">
                                    <input type="hidden" name="updateGiveExpense" value="updateGiveExpense">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <input type="hidden" name="giveExpense" value="giveExpense">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save</button>
                                <?php
                                }
                                ?>
                                <button type="button" value="add" class="btn btn-danger resetBtn"><i class="fa fa-check-square-o"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--End Row-->
    </div>
</div>
<!--End content-wrapper