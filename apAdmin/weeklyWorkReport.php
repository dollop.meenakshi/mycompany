<?php 
error_reporting(0);
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$_GET['month_year'] = $_REQUEST['month_year'];
$_GET['laYear'] = $_REQUEST['laYear'];
$uId = $_GET['uId'];
$bId = $_GET['bId'];
$dId = $_GET['dId'];

if(isset($uid))
{
  $user_shift = $d->selectRow('*', "users_master", "user_id='$uid'"); 
  $user_shift_data = mysqli_fetch_assoc($user_shift);
  $shift_time_id=$user_shift_data['shift_time_id'];
}
else
{
  $shift_time_id="";
}
?>
  <input type = "hidden" id="shift_time_id" value="<?php echo $shift_time_id; ?>">

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row ">
      <div class="col-md-12 col-12">
        <h4 class="page-title">Weekly Attendance</h4>
      </div>
    </div>
    <form action="" class="branchDeptFilterWithUser">
      <div class="row ">
      <?php include('selectBranchDeptEmpForFilter.php'); ?>

        <!-- <div class="col-md-3 form-group">
          <label class="form-control-label">Department </label>
          <select name="dId" class="form-control single-select" onchange="getUserForSalary(this.value)" required="">
            <option value="">Select Department</option>
            <?php
            $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id");
            while ($depaData = mysqli_fetch_array($qd)) {
            ?>
              <option <?php if ($_GET['dId'] == $depaData['floor_id']) {
                        echo 'selected';
                      } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name']; ?> (<?php echo $depaData['block_name']; ?>)</option>
            <?php } ?>

          </select>
        </div> -->
        <div class="col-md-3 col-6 form-group">
          <select name="laYear" class="form-control single-select" id="laYear">
          <option value=""> Select Year</option> 

              <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $twoPreviousYear) {
                        echo 'selected';
                      } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
              <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $onePreviousYear) {
                        echo 'selected';
                      } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
              <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $currentYear) {
                        echo 'selected';
                      } ?> <?php if (!isset($_GET['laYear'])) {
                              echo 'selected';
                            } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?>
                </option>
              <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $nextYear) {
                        echo 'selected';
                      } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
          </select>
      </div>
      <div class="col-md-3 col-6 form-group">
          <select class="form-control month_year single-select" id="month_year" name="month_year">
              <option value=""> Select Month</option> 
              <?php
              $selected = "";
              for ($m = 1; $m <= 12; $m++) {
                $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
                if (isset($_GET['month_year'])  && $_GET['month_year'] !="") {
                  if($_GET['month_year'] == $m)
                  {
                    $selected = "selected";
                  }else{
                    $selected = "";
                  }
                  
                  
                } else {
                  $selected = "";
                  if($m==date('n'))
                  { 
                    $selected = "selected";
                  }
                  else
                  {
                    $selected = "";
                  }
                }

              ?>
              <option  <?php echo $selected; ?>  data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $month; ?></option> 
                  <?php }

                  ?>
          </select> 
        </div>
        <!-- <div class="col-md-3 form-group" id="">
          <label class="form-control-label">Employee </label>
          <select id="user_id" type="text" class="form-control single-select " required name="user_id">
            <option value=""> Select Employee </option>
            <?php
            if (isset($_GET['dId']) && $_GET['dId'] > 0) {
              $userData = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND floor_id = $_GET[dId]");
              while ($user = mysqli_fetch_array($userData)) {
            ?>
                <option <?php if (isset($_GET['uId']) && $_GET['uId'] == $user['user_id']) {
                          echo "selected";
                        } ?> value="<?php if (isset($user['user_id']) && $user['user_id'] != "") {
                                      echo $user['user_id'];
                                    } ?>"> <?php if (isset($user['user_full_name']) && $user['user_full_name'] != "") {
                                              echo $user['user_full_name'];
                                            } ?></option>
            <?php }
            } ?>
          </select>
        </div> -->
        <div class="col-md-3 text-left ">
          <input class="btn btn-success btn-sm " type="submit" name="getReport" value="Get Data">
        </div>
      </div>
    </form>
    <div class="row mt-2">
      <div class="col-lg-12">
        <div class="col-md-4 float-left">
          <label>Total Month Hours : </label>
          <span id="totalHours"> 00 Hours </span>
        </div>
        <div class="col-md-4 float-left">
          <label>Total Working Hours : </label>
          <span id="totalWorkHours"> 00 Hours </span>
        </div>
        <div class="col-md-4 float-left">
          <label>Total Extra Hours : </label>
          <span id="totalExtraHours"> 00 Hours </span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <?php
              $i = 1;
             $counter = 1;
              if (isset($_GET['dId'])) {
              ?>
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>Week</th>
                      <th>Total Hours</th>
                      <th>History</th>
                    </tr>
                  </thead>
                  <tbody class="weeklyreport" id="">

                  </tbody>
                </table>
              <?php } else {  ?>
                <div class="" role="alert">
                  <span><strong>Note :</strong> Please Select Department</span>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="getWeeklyReportModal">
  <div class="modal-dialog " style="max-width: 90%">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <div class="row col-md-12">
          <div class="col-md-6">
            <h5 class="modal-title text-white">Weekly History</h5>
          </div>
          <div class="col-md-6">
            <label class="modal-title text-white">Week:</label>
            <h5 class=" text-white " id="weekModal"></h5>
          </div>
        </div>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">

          <div class="row col-md-12  table-responsive" id="">
            <table  class="table table-bordered ">
              <thead>
                <tr>
                  <th>Sr.No</th>
                  <th>In Date </th>
                  <th>In Time</th>
                  <th>Out Date</th>
                  <th>Out Time</th>
                  <th>Total hours</th>
                  <th>Punch Out missing</th>
                </tr>
              </thead>
              <tbody id="getWeeklyReportData">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  <?php if (isset($_GET['uId']) && $_GET['uId'] > 0) {
  ?>
  <?php if (isset($_GET['month_year']) && $_GET['month_year'] > 0) { ?> 
    month_year = '<?php echo $_GET['month_year'];?>';
                 <?php }
                 else{ ?>
  month_year = '0';
  <?php } ?>
  <?php if (isset($_GET['uId']) && $_GET['uId'] > 0) { ?> 
  /*   user_id = '<?php //echo $_GET['uId'];?>'; */
    user_id = $('#uId').val();
                 <?php }
                 else{ ?>
  user_id = '0';
  <?php } ?>
  laYear = $('#laYear').val();
   
    getWeeklyData(user_id, '<?php echo $society_id; ?>',laYear,month_year );
  <?php } ?>
 var weeklyData = "";
function getWeeklyData(userId, society_id,year,month) {
  $.ajax({
    url: "../residentApiNew/attendanceControllerWeb.php",
    cache: false,
    type: "POST",
    data: {
      getWeeklyAttendanceHistory: "getWeeklyAttendanceHistory",
      society_id: society_id,
      user_id: userId,
      year: year,
      month: month,
    },
    success: function (response) {
      var ToatlWorkHour=0;
      var totalHour = 0;
      var totalDiffer =0;
      showData = "";
      if (response.status != "201") {
     
        $('#week_position').text(response.week_position + "Week");
        counter = 1;
        weeklyData = response.weekly_histrory;
        $.each(response.weekly_histrory, function (index, value) {
          showModal = "";
          ToatlWorkHour +=parseFloat(value.total_spend_time);
          totalHour +=parseFloat(value.total_hours);
          showData += `<tr>
                        <td>`+ counter++ + `</td>
                        <td>`+ value.week + `</td>
                        <td>`+value.total_spend_time+`/`+ value.total_hours + `</td>`;
                        if (value.history.length > 0) {
                          showData += `<td><input type="button" id="` + index + `" onclick="getWeeklyHistory(` + index +`)" value="History" class="btn btn-sm btn-primary getWeeklyHistory"></td>`;
                        }
                        else
                        {
                          showData += `<td>No History</td>`;
                        }
          showData += `</tr>`;
        });
        $('.weeklyreport').html(showData);
      }

    }
  });
}

getToatlMonthData(user_id, laYear,month_year)
function getToatlMonthData(user_id,year,month)
{
  shift_time_id = $('#shift_time_id').val();
  $.ajax({
  url: "../residentApiNew/MonthAttendanceData.php",
      cache: false,
      type: "POST",
      data: {user_id :user_id,
            getMonthlyAttendanceHistory:"getMonthlyAttendanceHistory",
            month: month,
            year: year,
            shift_time_id:shift_time_id,
            society_id:'<?php echo $society_id; ?>'
        },
      success: function(response){
        $('#totalHours').text(response.total_month_hours_view);
        $('#totalWorkHours').text(response.total_month_hour_spent_view);
        $('#totalExtraHours').text(response.total_extra_hours_view);
      }
    })
}

function getWeeklyHistory(id)
 {
  showData = "";

  $('#getWeeklyReportModal').modal();
  $.each(weeklyData, function (index, value) {
    if (id == index) {
      j=1;
      $.each(value.history, function (i, val) {
     
        if(val.is_punch_out_missing==false)
        {
          is_punch_out_missing = "No";
        }
        else
        {
          is_punch_out_missing = "Yes";
        }
        $('#weekModal').text(value.week);
        showData += `<tr>
                    <td>`+ j++ + `</td>
                    <td>`+ val.attendance_date_start + ` (` +val.attendance_day_start+` )`;
                    if (val.extra_day==true) {
                      showData += `<span class='badge badge-success'>(Extra Day)</span>`;
                    }
                  showData += 
                    `</td>
                    <td>`+ val.punch_in_time + `</td>
                    <td>`+ val.attendance_date_end + ` (` +val.attendance_day_end+` )</td>
                    <td>`+ val.punch_out_time + `</td>
                    <td>`+ val.total_hours + `</td>
                    <td>`+ is_punch_out_missing + `</td>
                  </tr>`;
      });
    }
  });
  $('#getWeeklyReportData').html(showData);
 
}
  function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
    }
    return false;
  }
  
</script>
<style>
  .attendance_status {
    min-width: 113px;
  }
</style>