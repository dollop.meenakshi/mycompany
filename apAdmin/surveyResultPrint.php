<?php
if (filter_var($_GET['id'], FILTER_VALIDATE_INT) == true && filter_var($_GET['society_id'], FILTER_VALIDATE_INT) == true) {
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
extract($_REQUEST);
if(isset($id)  ){
$_SESSION['survey_id'] = $id;
} else{
$survey_id =  $_SESSION['survey_id'];
}
$survey_id = $id;
include 'common/checkLanguage.php';

?>
<!-- simplebar CSS-->
<link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
<!-- Bootstrap core CSS-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
<!-- animate CSS-->
<link href="assets/css/animate.css" rel="stylesheet" type="text/css"/>
<!-- Icons CSS-->
<link href="assets/css/icons.css" rel="stylesheet" type="text/css"/>
<!-- Sidebar CSS-->
<link href="assets/css/sidebar-menu2.css" rel="stylesheet"/>
<!-- Custom Style-->
<?php include 'common/colours.php'; ?>
<link href="assets/css/app-style9.css" rel="stylesheet"/>
<link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
<link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
<!--Lightbox Css-->
<link href="assets/plugins/fancybox/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/Chart.js/Chart.min.js"></script>

<style type="text/css">
.lineHeight{
line-height: 2;
}
</style>
<body class="bg-white">

<?php
extract($_GET);
$survey_question_master_qry=$d->select("survey_question_master","society_id='$society_id' AND survey_id='$survey_id'");
$socData=$d->selectArray("society_master","society_id='$society_id'");
?>
<?php if(mysqli_num_rows($survey_question_master_qry) <= 0 ){?>
<div class="row">
  <div class="col-lg-12">
    <img src='img/no_data_found.png'>
  </div>
</div>
<?php }  else { ?>
<div class="row">
  <div class="col-lg-12">
    <div class="card">  
      <div class="row no-print" id="printPageButton">
          <div class="col-lg-12 text-center">
            <a href="#" onclick="window.history.go(-1); return false;"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Back</a>
            <a href="javaScript:void();" onclick="printDiv('printableArea')"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
          </div>
        </div>
      <div class="m-4 border border-dark" id="printableArea">
        <div class="row m-3">
          <div class="col-sm-6">
            <h4><i class="fa fa-building"></i> <?php echo $socData['society_name']; ?></h4>
            <address>
              <?php echo $socData['society_address']; ?>
            </address>
          </div>
          <div class="col-sm-6 text-right">
            <img src="../img/logo.png" height="100">
          </div>
        </div>
        <hr>
        <script type="text/javascript" src="assets/js/loader.js"></script>
        <?php $survey_master_qry=$d->select("survey_master","society_id='$society_id' AND survey_id='$survey_id'");
        $survey_master_result = mysqli_fetch_assoc($survey_master_qry);
        ?><div class="card-body ">
          <div class="row">
            <div class="col-md-6">
              <h4>Survey Title: <?php echo $survey_master_result['survey_title']; ?></h4>
              
            </div>
            <div class="col-md-6">
              <h5>Survey Date: <?php echo date("d M Y",strtotime($survey_master_result['survey_date'])); ?></h5>
            </div>
            <div class="col-md-12">
              <?php if(trim($survey_master_result['survey_desciption']) !="") {?>
              <p>Description: <?php echo $survey_master_result['survey_desciption']; ?></p>
              <?php } ?>
            </div>
          </div>
          
        </div>
        <?php
        
        //   $survey_question_master_result=mysqli_fetch_array($survey_question_master_qry);
                //  echo "<pre>";print_r( $row);
                 //   extract($survey_question_master_result);
                    while($survey_question_master_result=mysqli_fetch_array($survey_question_master_qry)) {  
        //  echo "<pre>";print_r( $survey_question_master_result);echo "</pre>";
        ?>
        
        <div class="card-body ">
          <h5 class="card-title">Question: <?php echo $survey_question_master_result['survey_question']; ?></h5>
          <p><b>Survey For:</b> <?php  
                $election_for_string= $xml->string->election_for; 
                       $electionArray = explode("~", $election_for_string);
                      if ($survey_master_result['survey_for']==0) 
                      {
                        echo  $all= $xml->string->all.' '. $xml->string->floors;
                      } 
                      else 
                      {
                        $qf=$d->selectRow("floor_name,block_name","floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floor_id='$survey_master_result[survey_for]'");
                        $floorData=mysqli_fetch_array($qf);
                        echo $floorData['floor_name'] . " (" . $floorData['block_name'] .")";
                      }
                  ?>
                </p>
          <p><b>Survey Option:   </b>
            <ul class="list-unstyled">
              <?php
              $i11=1;
              $sq = $d->select("survey_option_master","survey_id = '$survey_id' AND society_id = '$society_id' AND survey_question_id = '$survey_question_master_result[survey_question_id]' ");
              while($sData = mysqli_fetch_array($sq)){?>
              <li><?php echo $i11++;?>. <?php echo $sData['survey_option_name']; ?></li>
              <?php  } ?>
            </ul>
          </p>
          
          
          
        </div>
        <?php
        $sq = $d->select("survey_result_master","survey_id = '$survey_id' AND society_id = '$society_id' and survey_question_id = '$survey_question_master_result[survey_question_id]'");
        if(mysqli_num_rows($sq) > 0 ) {
          $optionArray = array();
          $optionArrayResult = array();

        ?>
        <div class="card-body border-top">
          <script>
               google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                  var data = google.visualization.arrayToDataTable([
                  ['Options', 'Count'],
                  <?php
                  $sq = $d->select("survey_option_master","survey_id = '$survey_id' AND society_id = '$society_id' and survey_question_id = '$survey_question_master_result[survey_question_id]' ");
                  while($sData = mysqli_fetch_array($sq)){
                    $voteCount = $d->count_data_direct("survey_result_id","survey_result_master","survey_id = '$survey_id' AND survey_option_id='$sData[survey_option_id]'");
                    $voteCOunt= (int)$voteCount;
                    $survey_option_name = $sData['survey_option_name'].'';
                  echo "['".$sData['survey_option_name']."', ".$voteCOunt."],";
                   } ?> 
                ]);
                  var options = {
                    titleTextStyle: {
                      fontSize: 16,
                  },
                    'title':'Poll Result', 
                    'width':400, 
                    'height':400,
                    'colors' : ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'],
                    'is3D': true,
                    //IS_875
                    'sliceVisibilityThreshold' : 0
                  };

                  // Display the chart inside the <div> element with id="piechart"
                  var chart = new google.visualization.PieChart(document.getElementById('piechart<?php echo $survey_question_master_result[survey_question_id] ; ?>'));
                  chart.draw(data, options);
                }
              </script>
              <div id="piechart<?php echo $survey_question_master_result[survey_question_id] ; ?>" align='center'></div>
          
            
        </div>
          
          
          <?php } else{
          echo '<div class="card-body border-top"><center><b>No Result</b></center></div>';
          } ?>
          <div class="table-responsive">
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Employee - Branch</th>
                  
                </tr>
              </thead>
              <tbody>
                <?php
                $i=1;
                $q=$d->select("survey_result_master,unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=survey_result_master.unit_id AND survey_result_master.user_id=users_master.user_id AND survey_result_master.survey_id = '$survey_id' AND survey_result_master.survey_question_id = '$survey_question_master_result[survey_question_id]'");
                while ($empData=mysqli_fetch_array($q)) {
                ?>
                <tr>
                  
                  <td><?php echo $i++; ?></td>
                  <td><?php echo $empData['user_full_name']; ?>-<?php echo $empData['user_designation']; ?> (<?php echo $empData['block_name']; ?>)</td>
                  
                  
                </tr>
                <?php } ?>
              </tbody>
              
            </table>
          </div>
          <?php
          } ?>
          
        </div>
        
        
        
       
      </div>
    </div>
  </div>
</div>
<?php } ?>
<script type="text/javascript">
function printDiv(divName) {
var printContents = document.getElementById(divName).innerHTML;
var originalContents = document.body.innerHTML;
document.body.innerHTML = printContents;
window.print();
document.body.innerHTML = originalContents;
}
</script>
<style type="text/css">
@media print {
#printPageButton {
display: none;
}
}
</style>
<!-- Bootstrap core JavaScript-->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- simplebar js -->
<script src="assets/plugins/simplebar/js/simplebar.js"></script>
<!-- waves effect js -->
<script src="assets/js/waves.js"></script>
<!-- sidebar-menu js -->
<script src="assets/js/sidebar-menu.js"></script>
<!-- Custom scripts -->
<script src="assets/js/app-script.js"></script>
</body>
</html>
<?php } else{
echo "Invalid Request!!!";
}?>