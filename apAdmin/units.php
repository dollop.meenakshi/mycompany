 <div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-12">
        <h4 class="page-title"><?php echo $xml->string->units; ?></h4>
      </div>
      <div class="col-sm-3 col-6">
       <div class="btn-group float-sm-right">
       
          <!-- <?php //IS_1106 ?>
         <a href="unitBHKName" class="btn btn-info btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> <?php echo $xml->string->one_bhk; ?> <?php echo $xml->string->name; ?></a> -->
      </div>
    </div> 
  </div>

  <!-- End Breadcrumb-->

  <div class="row blockList">

     <?php 
    $i=1;
    $q = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_id ASC");
    while ($data=mysqli_fetch_array($q)) {
      $j=$i++;
      extract($data); ?>  
    <div class="col-lg-2 col-6">
    <a href="units?bId=<?php echo $block_id ?>">
      <?php if (isset($_GET['bId']) && filter_var($_GET['bId'], FILTER_VALIDATE_INT) == true) {
       ?>
      <div class="card <?php if($block_id==$_GET[bId]) { echo 'bg-google-plus'; $bId = $block_id; } else { echo 'bg-primary'; }  ?> ">
       <div class="card-body text-center text-white">
         <?php echo $xml->string->block; ?>
       <h5 class="mt-2 text-white"><?php echo $block_name; ?></h5>
      
       </div>
      </div>
      <?php } else { ?>
      <div class="card <?php  if($j==1) { echo 'bg-google-plus'; } else { echo 'bg-primary'; }  ?> ">
       <div class="card-body text-center text-white">
         <?php echo $xml->string->block; ?>
       <h5 class="mt-2 text-white"><?php echo $block_name; ?></h5>
      
       </div>
      </div>
      <?php } ?>
    </a>
    </div>
    <?php } ?>
    

</div>
<div class="row">
  <?php 
    $q3 = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_id ASC LIMIT 1");
    $fdata=mysqli_fetch_array($q3);
    $firstBlockId= $fdata['block_id'];
    
    if(isset($_GET['bId']) && filter_var($_GET['bId'], FILTER_VALIDATE_INT) == true) {
      $fq=$d->select("floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floors_master.block_id='$_GET[bId]' $blockAppendQuery");
    } else {
      $fq=$d->select("floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floors_master.block_id='$firstBlockId' $blockAppendQuery");
    }
   if(mysqli_num_rows($fq)>0) { 
    while ($floorData=mysqli_fetch_array($fq)) {
      $floorId= $floorData['floor_id'];
      $no_of_unit= $floorData['no_of_unit'];
      $unit_type= $floorData['unit_type'];
      $block_id= $floorData['block_id'];
      $blockName= $floorData['block_name'];
      if (isset($_GET['bId']) && filter_var($_GET['bId'], FILTER_VALIDATE_INT) == true) {
        $bId = $_GET['bId'];
      }else{
        $bId = $firstBlockId;
      }
   ?>

  <div class="col-12 col-lg-12">
    <div class="card border border-info floorList">
      <div class="card-body">
        <div class="floor text-left p-l">
          <form action="editDepartmentName" method="post" >
            <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
            <input type="hidden" name="bId" value="<?php echo $bId; ?>">
            <input type="hidden" name="floor_id" value="<?php echo $floorId ?>">
            <input type="hidden" name="block_id" value="<?php echo $block_id ?>">
           
               <input title="Edit Units Name" type="submit" class="pull-right btn btn-sm btn-primary fa fa-edit mr-1" value="&#xf044" name="editFloor"> 
              
          </form>
           <h6 class="mb-0"><?php echo $floorData['floor_name']; ?></h6>
         </div>
        <div class="media align-items-center">
         
        <div class="media-body text-left p-1 ml-5">
          <div class="row m-0">
            <?php 
            $uq=$d->select("unit_master","floor_id='$floorId' AND society_id='$society_id'");
            if(mysqli_num_rows($uq)>0) { 
            while ($unitData=mysqli_fetch_array($uq)) { 
               $gu=$d->select("users_master","unit_id='$unitData[unit_id]' AND  block_id='$block_id' AND floor_id='$floorId' AND society_id='$society_id'");
                $userData=mysqli_fetch_array($gu);
               if($unitData['unit_status']==0) {
              ?>
            <div class="col-6 col-lg-2 col-xl-2">
              <div class="card gradient-quepal no-bottom-margin">
                <div class="card-body text-center">
                <h6 class="text-white mt-2"><?php echo $blockName; ?>-<?php echo $unitData['unit_name']; ?> <i title="Delete Unit" class="fa fa-trash-o pull-right" onclick="DeleteUnit('<?php echo $unitData['unit_id']; ?>');"></i> </h6>
              </div>
              </div>
            </div>
          <?php } else { ?>
            <div class="col-6 col-lg-2 col-xl-2">
              <div class="card gradient-scooter no-bottom-margin">
                <div class="card-body text-center">
                  <h6 class="text-white mt-2"><?php echo $blockName; ?>-<?php echo $unitData['unit_name']; ?>  <i title="Delete Unit" class="fa fa-trash-o pull-right" onclick="showError('Registered Units Can\'t Deleted ')"></i> </h6>
              </div>
              </div>
            </div>
            <?php } } } else {  ?>
              <form action="controller/unitController.php" method="post">
                <input type="hidden" name="no_of_unit" value="<?php echo $no_of_unit; ?>">
                <input type="hidden" name="unit_type" value="<?php echo $unit_type; ?>">
                <input type="hidden" name="floor_id" value="<?php echo $floorId; ?>">
                <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
                <button style="margin-left: 20px;margin-top: 30px;" class="btn btn-primary btn-sm"><?php echo $xml->string->add; ?></button>
              </form>
            <?php }  ?>
            
          </div>

        </div>
      </div>
     </div>
   </div>
  </div>
 <?php } } else {
      echo "<img src='img/no_data_found.png'>";
      } ?>
 
</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->

