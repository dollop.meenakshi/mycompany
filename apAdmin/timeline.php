<html>

<head>
  <meta charset="uft-8">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <title>Facebook Post</title>
</head>
<style type="text/css">
  ul.horizontal {
    margin: 0;
    padding: 0;
  }

  ul.horizontal li {
    display: block;
    float: left;
    padding: 0 10px
  }

  .facebook-card {
    max-width: 600px;
    margin: 0 auto;
    border: 15px solid #ffffff;
    background: #fff;
    border-radius: 4px;
    margin-top: 70px;
    margin-bottom: 80px;
  }

  .imgRedonda {
    width: 75px;
    height: 75px;
    border-radius: 150px;
  }

  .imgRedonda1 {
    width: 50px;
    height: 50px;
    border-radius: 150px;
  }

  .profileName {
    font-style: inherit;
    font-size: 20px;
    color: #365899;
    text-decoration: none;
  }

  .profileName1 {
    font-style: inherit;
    font-size: 18px;
    color: #365899;
    text-decoration: none;
  }

  .textbox {
    border-radius: 150px;
    size: 100px;
  }

  .time {
    font-size: 15px;
    color: gray;
  }

  .facebook-card-content {
    border-radius: 150px;
  }

  .facebook-card-header {
    padding: 15px;
    align-items: right;
  }

  .facebook-card-user-image {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    margin-right: 15px;
  }

  .facebook-card-user-name {
    font-size: 18px;
    font-weight: bold;
    text-decoration: none;
    color: #000;
  }

  .facebook-card-image img {
    width: 100%;
  }

  .facebook-card-content {
    padding: 15px;
  }

  .facebook-card-content a {
    color: #000;
    text-decoration: none;
    font-weight: bold;
  }

  .iconos {
    color: gray;
  }

  .like {
    color: blue;
  }

  .kokoro {
    color: red;
  }

  .smile {
    color: yellow;
  }

  .other {
    color: #365899;
  }

  .facebook-comments {
    float: left;
    margin: 0;
  }

  /*4march2020*/
  .facebook-card {
    margin-top: 30px !important;
    margin-bottom: 25px !important;
  }

  .standard-padding {
    padding-top: 50px !important;
  }

  @media screen and (max-width: 480px) {

    .standard-padding {
      padding-left: 5px !important;
      padding-right: 5px !important;
    }
  }

  .cls-pic {
    padding-right: 10px !important;
    float: left;
  }

  .cls-content {
    word-break: break-all !important;
  }

  .cls-delete {
    float: right;
    padding-left: 10px !important;
    padding-right: 10px !important;
    font-weight: bold;

  }


  /*4march2020*/
</style>

<body style="background-image: url(img/nf-bg.jpg);">

  <div class="standard-padding">
    <div class="col-md-12 text-right">
      <button class="btn btn-sm btn-primary mt-4 mx-auto" data-target="#feed" data-toggle="modal"> Add</button>
    </div>

    <div id="searchedEmployees" >
    </div>
    
  </div>
  </div>

<!--  <div class="modal fade" id="addTimeline" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Timeline</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="timeLine" action="controller/TimelineController.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="input-10" class="col-sm-4 col-form-label">Timeline File</label>
              <input type="file" class="form-control" name="feed_img">
            </div>
            <div class="form-group">
              <label for="input-10" class="col-sm-4 col-form-label">Timeline Message</label>
              <textarea type="text" class="form-control" name="feed_msg"></textarea>
            </div>
            <div class="form-footer text-center">
                  <input type="hidden" id="timeline_id" name="timeline_id" value="" class="rstFrm">
                  <button id="addCourseLessonBtn" type="submit" class="btn btn-success hideupdate course_lesson_button"><i class="fa fa-check-square-o"></i>Add</button>
                  <input type="hidden" name="addTimeline" value="addTimeline">
                  <button type="button" value="add" class="btn btn-danger " onclick="resetForm()"><i class="fa fa-check-square-o"></i> Reset</button>
              </div>
          </form>
        </div>
       
      </div>
    </div>
  </div> -->


  <div class="modal fade" id="feed">
    <div class="modal-dialog">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Add News Feed</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="controller/newsFeedController.php" method="post" enctype="multipart/form-data">
            <div class="form-group row">
              <div class="col-sm-12" id="PaybleAmount">
                <select name="society_id" required="" class="form-control">
                  <option>- Select Society- </option>
                  <!-- <option value="0"> All </option> -->
                  <?php $qs = $d->select("society_master", "");
                  while ($row = mysqli_fetch_array($qs)) { ?>
                    <option value="<?php echo $row['society_id']; ?>"><?php echo $row['society_name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12" id="PaybleAmount">
                <textarea required="" maxlength="500" type="text" name="feed_msg" placeholder="Whats Your Mind" class="form-control"></textarea>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-12" id="PaybleAmount">
                <input type="file" accept="image/*" name="image" class="form-control-file border photoOnly">
              </div>
            </div>

            <div class="form-footer text-center">
              <button type="submit" name="addFeed" value="addFeed" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Send</button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>
  <!--End Modal -->

  <div class="modal fade" id="viewLikesModal">
    <div class="modal-dialog">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Likes</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="likeResp">

        </div>

      </div>
    </div>
  </div>
  <!--End Modal -->
  <script src="assets/js/jquery.min.js"></script>
  <script type="text/javascript">
    function viewLikes(feed_id) {
      $.ajax({
        url: "getLikeDetails.php",
        cache: false,
        type: "POST",
        data: {
          feed_id: feed_id
        },
        success: function(response) {
          $('#likeResp').html(response);


        }
      });
    }

    window.addEventListener('load', function(event) {
      document.querySelectorAll(".inlineVideo").forEach((el) => {
        el.onplay = function(e) {
          // pause all the videos except the current.
          document.querySelectorAll(".inlineVideo").forEach((el1) => {
            if (el === el1)
              el1.play();
            else
              el1.pause();
          });
        }
      });
    });



    $(function() {
      // $("a[rel=group224]").fancybox();
      $(".fancybox").fancybox();
    });

    //////////// On Load ////////////

    var offset = 0;
    $(window).bind('scroll', function(){
      if($(window).scrollTop() + $(window).innerHeight()+1 >= $(document).height()){
        offset = offset+1;
        getTimeline(offset);
      }
    });

    getTimeline(offset);
    function getTimeline(offset=0,limit=10) {
      $.ajax({
          url: "getTimeline.php",
          cache: false,
          type: "POST",
          data: {limit:limit,offset:(offset*limit),blockAppendQueryUser:"<?php echo $blockAppendQueryUser;?>",timeline: "yes"},
          
          success: function(response){
              $("#searchedEmployees").append(response);
          }
      });
    } 

    
    /////////////////////////////////
  </script>