  <?php error_reporting(0); 

  if($_GET['date']!='') {  $sDate= $_GET['date']; } else {   $sDate= date("Y-m-d"); }
  if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$sDate)) {
      $_SESSION['msg1']='Invalid Date';
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='visitors';
      </script>");
  } 
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
        <form action="" method="get"  accept-charset="utf-8">
     <div class="row pt-2 pb-2">
        <div class="col-sm-6 col-12">
          <h4 class="page-title">Visitors</h4>
        </div>
        <div class="col-sm-4 col-9">
          <input type="hidden" name="type" value="<?php echo $_GET['type'];?>" >
          <input  class="form-control" readonly="" id="autoclose-datepicker-dob" name="date" type="text" value="<?php echo $sDate; ?>" >
        </div>
        <div class="col-sm-2 col-3">
          <button class="btn btn-warning " type="submit" name=""><i class="fa fa-search"></i></button>
        </div>
     </div>
    </form>
    <!-- End Breadcrumb-->
     <div class="row">
         <div class="col-lg-6 col-6">
          <a href="visitors?type=in&date=<?php echo $sDate; ?>">
          <div class="card bg-success <?php if($_GET['type']=='in') { echo 'activeVisit';} ?>">
           <div class="card-body text-center text-white">
           <h5 class="mt-2 text-white">
             <img src="img/in.png" height="120">
           </h5>
          
           </div>
          </div>
          </a>
        </div>
        <div class="col-lg-6 col-6">
         <a href="visitors?type=out&date=<?php echo $sDate; ?>">
          <div class="card bg-google-plus <?php if($_GET['type']=='out') { echo 'activeVisit';} ?>">
           <div class="card-body text-center text-white">
           <h5 class="mt-2 text-white">
             <img src="img/out.png" height="120">
             
           </h5>
          
           </div>
          </div>
          </a>
        </div>
     </div>

     <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="">

        <?php if ($_GET['type']=='in'){ ?>
          
       
          <ul class="nav nav-tabs nav-tabs-info nav-justified">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#tabe-13"> New</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-14"> Staff</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-16"> Cab/Delivery</span></a>
            </li>
             <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-15"> Exp-Visitor</span></a>
            </li>
            <?php //IS_1217 ?>
            <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-17">Common Visitor</span></a>
            </li>
             <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-18">Daily Visitor</span></a>
            </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div id="tabe-13" class="container-fluid tab-pane active show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable1" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Visit Date</td>
                          <td>Employee</td>
                          <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("unit_master,block_master,users_master,visitors_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND visitors_master.unit_id=unit_master.unit_id AND  visitors_master.society_id='$society_id' AND visitors_master.visitor_status=2  AND visitors_master.visitor_type=0 AND visitors_master.user_id=users_master.user_id AND visitors_master.visit_date='$sDate' $blockAppendQuery","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                             <?php if($data['visitor_profile']!="") { ?>
                            <a href="../img/visitor/<?php echo $data['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["visitor_name"]; ?>"><img class="lazyload" width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png" data-src="../img/visitor/<?php echo $data['visitor_profile']; ?>"></a>
                          <?php }?>
                          </td>
                          <td><?php echo $data['visitor_name'];?></td>
                          <td><?php echo $data['country_code'].' '.$data['visitor_mobile']; ?></td>
                          <td><?php echo $data['visit_date']; ?> <?php echo $data['visit_time']; ?></td>
                          <td><?php echo $data['user_full_name']; ?>-<?php echo $data['user_designation']; ?> (<?php echo $data['block_name']; ?>) </td>
                          <td><?php  if($data['in_with_mask']=='true') { echo "Yes" ; } else if($data['in_with_mask']=='false') { echo "No" ; } ?></td>
                          <td><?php if($data['in_temperature']>0) { echo $data['in_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="tabe-14" class="container-fluid tab-pane fade ">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable2" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>In Time</td>
                          <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q1=$d->select("employee_master,staff_visit_master","employee_master.emp_id=staff_visit_master.emp_id AND staff_visit_master.visit_status=0 AND employee_master.society_id='$society_id' AND staff_visit_master.visit_entry_date_time LIKE '%$sDate%' ","ORDER BY staff_visit_master.staff_visit_id DESC LIMIT 500");
                        while ($data1=mysqli_fetch_array($q1)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                            <?php if($data1['emp_profile']!="") { ?>
                            <a href="../img/emp/<?php echo $data1['emp_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data1["emp_name"]; ?>"><img class="lazyload" width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png" data-src="../img/emp/<?php echo $data1['emp_profile']; ?>"></a>
                            <?php }?>
                          </td>
                          <td><?php echo $data1['emp_name'];?></td>
                          <td><?php echo $data1['country_code'].' '.$data1['emp_mobile']; ?></td>
                          <td><?php echo $data1['visit_entry_date_time']; ?></td>
                          <td><?php  if($data1['in_with_mask']=='true' && $data1['emp_type_id']!=0) { echo "Yes" ; } else if($data1['in_with_mask']=='false' && $data1['emp_type_id']!=0) { echo "No" ; } ?></td>
                          <td><?php if($data1['in_temperature']>0) { echo $data1['in_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
             <div id="tabe-15" class="container-fluid tab-pane fade ">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                     <table id="default-datatable" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Visit Date</td>
                          <td>Employee</td>
                           <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q2=$d->select("unit_master,block_master,users_master,visitors_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND visitors_master.unit_id=unit_master.unit_id AND  visitors_master.society_id='$society_id' AND visitors_master.visitor_status=2 AND visitors_master.visitor_type=1 AND visitors_master.user_id=users_master.user_id AND visitors_master.visit_date='$sDate'  $blockAppendQuery","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data2=mysqli_fetch_array($q2)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data2['visitor_profile']!="") { ?>
                            <a href="../img/visitor/<?php echo $data2['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data2["gallery_title"]; ?>"><img class="lazyload" width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png" data-src="../img/visitor/<?php echo $data2['visitor_profile']; ?>"></a>
                          <?php } ?>
                          </td>
                          <td><?php echo $data2['visitor_name'];?></td>
                          <td><?php echo $data2['country_code'].' '.$data2['visitor_mobile']; ?></td>
                          <td><?php echo $data2['visit_date']; ?> <?php echo $data2['visit_time']; ?></td>
                          <td><?php echo $data2['user_full_name']; ?>-<?php echo $data2['user_designation']; ?> (<?php echo $data2['block_name']; ?>) </td>
                           <td><?php  if($data2['in_with_mask']=='true') { echo "Yes" ; } else if($data2['in_with_mask']=='false') { echo "No" ; } ?></td>
                          <td><?php if($data2['in_temperature']>0) { echo $data2['in_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="tabe-16" class="container-fluid tab-pane show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable3" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Visit Date</td>
                          <td>Employee</td>
                          <td>Type</td>
                          <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("unit_master,block_master,users_master,visitors_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND visitors_master.unit_id=unit_master.unit_id AND  visitors_master.society_id='$society_id' AND visitors_master.visitor_status=2  AND visitors_master.visitor_type=2 AND visitors_master.user_id=users_master.user_id AND visitors_master.visit_date='$sDate'  $blockAppendQuery OR users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND visitors_master.unit_id=unit_master.unit_id AND  visitors_master.society_id='$society_id' AND visitors_master.visitor_status=2  AND visitors_master.visitor_type=3 AND visitors_master.user_id=users_master.user_id AND visitors_master.visit_date='$sDate'  $blockAppendQuery","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data['visitor_profile']!="") { ?>
                            <a href="../img/visitor/<?php echo $data['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["visitor_name"]; ?>"><img class="lazyload" width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png" data-src="../img/visitor/<?php echo $data['visitor_profile']; ?>"></a>
                            <?php } ?>
                          </td>
                          <td><?php echo $data['visitor_name'];?></td>
                          <td><?php echo $data['country_code'].' '.$data['visitor_mobile']; ?></td>
                          <td><?php echo $data['visit_date']; ?> <?php echo $data['visit_time']; ?></td>
                          <td><?php echo $data['user_full_name']; ?>-<?php echo $data['user_designation']; ?> (<?php echo $data['block_name']; ?>) </td>
                          <td><?php echo $data['visit_from']; ?> </td>
                           <td><?php  if($data['in_with_mask']=='true') { echo "Yes" ; } else if($data['in_with_mask']=='false') { echo "No" ; }?></td>
                          <td><?php if($data['in_temperature']>0) { echo $data['in_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           
            <div id="tabe-17" class="container-fluid tab-pane show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable4" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Visit Date</td>
                           
                          <td>Type</td>
                          <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("visitors_master","  visitors_master.visitor_type=5 and visitors_master.visitor_status =2  and  visitors_master.society_id='$society_id' AND visit_date='$sDate' ","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                          if ($data['block_id']!=0 && $data['floor_id']!=0 && $data['unit_id']!=0) {
                              $gq= $d->select("block_master,unit_master","block_master.block_id=unit_master.block_id AND block_master.block_id='$data[block_id]' AND unit_master.unit_id='$data[unit_id]'");
                              $blockData=mysqli_fetch_array($gq);
                              $block_name = $blockData['block_name'].'-'.$blockData['unit_name'];
                          } 
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data['visitor_profile']!="") { ?>
                            <a href="../img/visitor/<?php echo $data['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["visitor_name"]; ?>"><img class="lazyload" width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png" data-src="../img/visitor/<?php echo $data['visitor_profile']; ?>"></a>
                          <?php } ?>
                          </td>
                          <td><?php echo $data['visitor_name'];?></td>
                          <td><?php echo $data['country_code'].' '.$data['visitor_mobile']; ?></td>
                          <td><?php echo  date ("d-m-Y h:i A", strtotime($data['visit_date'].' '.$data['visit_time'])); ?></td>
                           
                          <td><?php if ($data['visit_from']=='') {
                                echo $block_name;
                              } else {
                                echo $data['visit_from'];

                              } ?>
                              
                          </td>
                          <td><?php  if($data['in_with_mask']=='true') { echo "Yes" ; } else if($data['in_with_mask']=='false') { echo "No" ; } ?></td>
                          <td><?php if($data['in_temperature']>0) { echo $data['in_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="tabe-18" class="container-fluid tab-pane show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable5" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Visit Date</td>
                          <td>Type</td>
                            <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("daily_visitors_master,visitors_master","daily_visitors_master.visitor_id=daily_visitor_id AND   visitors_master.visitor_type=4 and visitors_master.visitor_status =2  and  visitors_master.society_id='$society_id' ","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data['visitor_profile']!="") { ?>
                            <a href="../img/visitor/<?php echo $data['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["visitor_name"]; ?>"><img class="lazyload" width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png"  data-src="../img/visitor/<?php echo $data['visitor_profile']; ?>"></a>
                          <?php } ?>
                          </td>
                          <td><?php echo $data['visitor_name'];?></td>
                          <td><?php echo $data['country_code'].' '.$data['visitor_mobile']; ?></td>
                          <td><?php echo  date ("d-m-Y h:i A", strtotime($data['visit_date'].' '.$data['visit_time'])); ?></td>
                           
                          <td><?php echo $data['visit_from']; ?></td>
                          <td><?php  if($data['in_with_mask']=='true') { echo "Yes" ; }else if($data['in_with_mask']=='false') { echo "No" ; } ?></td>
                          <td><?php if($data['in_temperature']>0) { echo $data['in_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        <?php } elseif ($_GET['type']=='out') { ?>
          <ul class="nav nav-tabs nav-tabs-info nav-justified">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#tabe-13"> New</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-14"> Staff</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-16"> Cab/Delivery</span></a>
            </li>
             <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-15"> Exp-Visitor</span></a>
            </li>
            <?php //IS_1217 ?>
             <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-17">Common Visitor</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-18">Daily Visitor</span></a>
            </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div id="tabe-13" class="container-fluid tab-pane active show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable1" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>In Date</td>
                          <td>Out Date</td>
                          <td>Employee</td>
                           <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("unit_master,block_master,users_master,visitors_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND visitors_master.unit_id=unit_master.unit_id AND  visitors_master.society_id='$society_id' AND visitors_master.visitor_status=3  AND visitors_master.visitor_type=0 AND visitors_master.user_id=users_master.user_id AND visitors_master.exit_date='$sDate' $blockAppendQuery","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data['visitor_profile']!="") { ?>
                                <a href="../img/visitor/<?php echo $data['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["visitor_name"]; ?>"><img class="lazyload"  onerror="this.src='img/user.png'" width="30" height="30" src="img/user.png" data-src="../img/visitor/<?php echo $data['visitor_profile']; ?>"></a>
                              <?php }?>
                              </td>
                          <td><?php echo $data['visitor_name'];?></td></td>
                          <td><?php echo $data['country_code'].' '.$data['visitor_mobile']; ?></td>
                          <td><?php echo $data['visit_date']; ?> <?php echo $data['visit_time']; ?></td>
                          <td><?php echo $data['exit_date']; ?> <?php echo $data['exit_time']; ?></td>
                          <td><?php echo $data['user_full_name']; ?>-<?php echo $data['user_designation']; ?> (<?php echo $data['block_name']; ?>) </td>
                           <td><?php  if($data['out_with_mask']=='true') { echo "Yes" ; }else if($data['out_with_mask']=='false') { echo "No" ; } ?></td>
                          <td><?php if($data['out_temperature']>0) { echo $data['out_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="tabe-14" class="container-fluid tab-pane fade ">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable2" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Out Time</td>
                           <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q1=$d->select("employee_master,staff_visit_master","employee_master.emp_id=staff_visit_master.emp_id AND staff_visit_master.visit_status=1 AND employee_master.society_id='$society_id' AND staff_visit_master.visit_exit_date_time LIKE '%$sDate%' ","ORDER BY staff_visit_master.staff_visit_id DESC LIMIT 500");
                        while ($data1=mysqli_fetch_array($q1)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data1['emp_profile']!="") { ?>
                            <a href="../img/emp/<?php echo $data1['emp_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data1["emp_name"]; ?>"><img class="lazyload"  width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png" data-src="../img/emp/<?php echo $data1['emp_profile']; ?>"></a>
                          <?php } ?>
                          </td>
                          <td><?php echo $data1['emp_name'];?></td>
                          <td><?php echo $data1['country_code'].' '.$data1['emp_mobile']; ?></td>
                          <td><?php echo $data1['visit_exit_date_time']; ?></td>
                           <td><?php  if($data1['out_with_mask']=='true' && $data1['emp_type_id']!=0) { echo "Yes" ; } else if($data1['out_with_mask']=='false' && $data1['emp_type_id']!=0) { echo "No" ; } ?></td>
                          <td><?php if($data1['out_temperature']>0) { echo $data1['out_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="tabe-15" class="container-fluid tab-pane fade ">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                     <table id="default-datatable" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Visit Date</td>
                          <td>Out Date</td>
                          <td>Employee</td>
                           <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q2=$d->select("unit_master,block_master,users_master,visitors_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND visitors_master.unit_id=unit_master.unit_id AND  visitors_master.society_id='$society_id' AND visitors_master.visitor_status=3 AND visitors_master.visitor_type=1 AND visitors_master.user_id=users_master.user_id AND visitors_master.exit_date='$sDate' $blockAppendQuery","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data2=mysqli_fetch_array($q2)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data2['visitor_profile']!="") { ?>
                            <a href="../img/visitor/<?php echo $data2['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data2["visitor_name"]; ?>"><img class="lazyload"  onerror="this.src='img/user.png'" width="30" height="30" src="img/user.png" data-src="../img/visitor/<?php echo $data2['visitor_profile']; ?>"></a>
                            <?php } ?>
                          </td>
                          <td><?php echo $data2['visitor_name'];?></td>
                          <td><?php echo $data2['country_code'].' '.$data2['visitor_mobile']; ?></td>
                          <td><?php echo $data2['visit_date']; ?> <?php echo $data2['visit_time']; ?></td>
                          <td><?php echo $data2['exit_date']; ?> <?php echo $data2['exit_time']; ?></td>
                          <td><?php echo $data2['user_full_name']; ?>-<?php echo $data2['user_designation']; ?> (<?php echo $data2['block_name']; ?>) </td>
                          <td><?php  if($data2['out_with_mask']=='true') { echo "Yes" ; } else if($data2['out_with_mask']=='false') { echo "No" ; } ?></td>
                          <td><?php if($data2['out_temperature']>0) { echo $data2['out_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div id="tabe-16" class="container-fluid tab-pane  show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable3" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Visit Date</td>
                          <td>Exit Date</td>
                          <td>Employee</td>
                          <td>Type</td>
                           <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("unit_master,block_master,users_master,visitors_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND visitors_master.unit_id=unit_master.unit_id AND  visitors_master.society_id='$society_id' AND visitors_master.visitor_status=3  AND visitors_master.visitor_type=2 AND visitors_master.user_id=users_master.user_id AND visitors_master.exit_date='$sDate' $blockAppendQuery OR users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND visitors_master.unit_id=unit_master.unit_id AND  visitors_master.society_id='$society_id' AND visitors_master.visitor_status=3  AND visitors_master.visitor_type=3 AND visitors_master.user_id=users_master.user_id AND visitors_master.exit_date='$sDate' $blockAppendQuery","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data['visitor_profile']!="") { ?>
                            <a href="../img/visitor/<?php echo $data['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["visitor_name"]; ?>"><img class="lazyload"  width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png" data-src="../img/visitor/<?php echo $data['visitor_profile']; ?>"></a>
                          <?php  } ?>
                          </td>
                          <td><?php echo $data['visitor_name'];?></td>
                          <td><?php echo $data['country_code'].' '.$data['visitor_mobile']; ?></td>
                           <td><?php echo  date ("d-m-Y h:i A", strtotime($data['visit_date'].' '.$data['visit_time'])); ?></td>
                           <td><?php echo  date ("d-m-Y h:i A", strtotime($data['exit_date'].' '.$data['exit_time'])); ?></td>
                           
                          <td><?php echo $data['user_full_name']; ?>-<?php echo $data['user_designation']; ?> (<?php echo $data['block_name']; ?>) </td>
                          <td><?php echo $data['visit_from']; ?> </td>
                          <td><?php  if($data['out_with_mask']=='true') { echo "Yes" ; } else if($data['out_with_mask']=='false') { echo "No" ; } ?></td>
                          <td><?php if($data['out_temperature']>0) { echo $data['out_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <?php //IS_1217 ?>
            <div id="tabe-17" class="container-fluid tab-pane show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable4" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Visit Date</td>
                          <td>Exit Date</td>
                          <td>Type</td>
                           <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("visitors_master","visitors_master.visitor_type=5 and visitors_master.visitor_status =3 and  visitors_master.society_id='$society_id' AND exit_date='$sDate' ","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                          if ($data['block_id']!=0 && $data['floor_id']!=0 && $data['unit_id']!=0) {
                              $gq= $d->select("block_master,unit_master","block_master.block_id=unit_master.block_id AND block_master.block_id='$data[block_id]' AND unit_master.unit_id='$data[unit_id]'");
                              $blockData=mysqli_fetch_array($gq);
                              $block_name = $blockData['block_name'].'-'.$blockData['unit_name'];
                          } 
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data['visitor_profile']!="") { ?>
                            <a href="../img/visitor/<?php echo $data['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["visitor_name"]; ?>"><img class="lazyload"  width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png" data-src="../img/visitor/<?php echo $data['visitor_profile']; ?>"></a>
                          <?php } ?>
                          </td>
                          <td><?php echo $data['visitor_name'];?></td>
                          <td><?php echo $data['country_code'].' '.$data['visitor_mobile']; ?></td>
                          <td><?php echo  date ("d-m-Y h:i A", strtotime($data['visit_date'].' '.$data['visit_time'])); ?></td>
                          <td><?php echo  date ("d-m-Y h:i A", strtotime($data['exit_date'].' '.$data['exit_time'])); ?></td>
                           
                          <td><?php if ($data['visit_from']=='') {
                            echo $block_name;
                          } else {
                            echo $data['visit_from'];

                          } ?></td>
                            <td><?php  if($data['out_with_mask']=='true') { echo "Yes" ; } else if($data['out_with_mask']=='false') { echo "No" ; } ?></td>
                          <td><?php if($data['out_temperature']>0) { echo $data['out_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php //IS_1217 ?>

            <div id="tabe-18" class="container-fluid tab-pane show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable6" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Photo</td>
                          <td>Name</td>
                          <td>Mobile</td>
                          <td>Visit Date</td>
                          <td>Exit Date</td>
                           
                          <td>Type</td>
                           <td>With Mask</td>
                          <td>Body Temp.</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("daily_visitors_master,visitors_master","daily_visitors_master.visitor_id=daily_visitor_id AND  visitors_master.visitor_type=4 and visitors_master.visitor_status =3 and  visitors_master.society_id='$society_id'  AND visitors_master.exit_date='$sDate'","ORDER BY visitors_master.visitor_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                              <?php if($data['visitor_profile']!="") { ?>
                            <a href="../img/visitor/<?php echo $data['visitor_profile'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["visitor_name"]; ?>"><img class="lazyload"  width="30" onerror="this.src='img/user.png'" height="30" src="img/user.png" data-src="../img/visitor/<?php echo $data['visitor_profile']; ?>"></a>
                          <?php } ?>
                          </td>
                          <td><?php echo $data['visitor_name'];?></td>
                          <td><?php echo $data['country_code'].' '.$data['visitor_mobile']; ?></td>
                          <td><?php echo  date ("d-m-Y h:i A", strtotime($data['visit_date'].' '.$data['visit_time'])); ?></td>
                          <td><?php echo  date ("d-m-Y h:i A", strtotime($data['exit_date'].' '.$data['exit_time'])); ?></td>
                           
                          <td><?php echo $data['visit_from']; ?></td>
                            <td><?php  if($data['out_with_mask']=='true') { echo "Yes" ; } else if($data['out_with_mask']=='false') { echo "No" ; }?></td>
                          <td><?php if($data['out_temperature']>0) { echo $data['out_temperature']; } ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        <?php } else { ?>
          <div class="alert-message m-3">
            <span><strong>Error!</strong> Please Select Type</span>
          </div>
        <?php } ?>
      </div>
    </div>
    
  </div>

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



