<?php 
session_start();
extract($_POST);
$dayNamePos = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'); 
$daykey = array_search( $_POST['dayname'], $dayNamePos);
if(isset($editTimeSlotSIngle)) { 
	$c_start_time = date('h:i A',strtotime($_POST['c_start_time']));
          $c_end_time = date('h:i A',strtotime($_POST['c_end_time']));
}
?>
 <form action="controller/facilitiesController.php" method="POST" >
<div class="row">
	<input type="hidden" name="csrf" value='<?php echo $_SESSION["token"]; ?>'>
	<input type="hidden" name="facility_day_id" value='<?php echo $facility_day_id; ?>'>
	<input type="hidden" name="facility_id" value='<?php echo $facility_id; ?>'>
	
	<div class="col-lg-6 mt-2">
		Day : <?php echo $_POST['dayname']; ?>
	</div>
</div>
<div class="row fullDiv<?php echo $_POST['dayname'];?>">
	<div class="col-lg-6 mt-2 countDiv<?php echo $_POST['dayname'];?>">
	  <input required="" type="text" readonly class="form-control appenddayCheck<?php echo $_POST['dayname'];?><?php echo $_POST['timeSlotNumber'];?>" value="<?php if(isset($editTimeSlotSIngle)) { echo $c_start_time; }?>"   name="opening_time" placeholder="Opening Time">
	</div>
	<div class="col-lg-6 mt-2">
	  <input required=""	 required="" type="text" readonly class="form-control  appenddayCheckEnd<?php echo $_POST['dayname'];?><?php echo $_POST['timeSlotNumber'];?>" value="<?php if(isset($editTimeSlotSIngle)) { echo $c_end_time; }?>"  name="closing_time" placeholder="Closing Time">
	</div>
	
</div>
<div class="form-footer text-center">
	<?php if(isset($editTimeSlotSIngle)) {  ?>
	<input value="UpdateMoreTimeslotSingle" name="UpdateMoreTimeslotSingle" type='hidden'>
	<input type="hidden" name="facility_schedule_id" value='<?php echo $facility_schedule_id; ?>'>
	<button type="submit" class="form-btn btn btn-danger btn-sm " name="">Update</button>
	<?php   } else { ?>
	<input value="addMoreTimeslotSingle" name="addMoreTimeslotSingle" type='hidden'>
	<button type="submit" class="form-btn btn btn-danger btn-sm " name="">Add</button>
	<?php } ?>
</div>
</form>
