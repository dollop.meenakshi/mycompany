<?php 
$floor_id = (int)$_GET['floor_id'];
error_reporting(0);
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employeeExpenseReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Employee Expense Report</h4>
        </div>
     
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <div class="input-group mb-2">
              <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom" placeholder="YYYY-MM-DD"  name="from" value="<?php echo $_GET['from']; ?>" readonly> 
              <!-- <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>  -->
          </div>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <div class="input-group mb-2">
               <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo" placeholder="YYYY-MM-DD" name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly> 
               <!-- <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>  -->
            </div>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Department </label>
            <select name="floor_id"  onchange="getUserByFloorId(this.value);" class="form-control single-select">
                <option value="">All Department</option> 
                  <?php 
                    $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id");  
                    while ($depaData=mysqli_fetch_array($qd)) {
                  ?>
                <option  <?php if($_GET['floor_id']==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
                <?php } ?>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Employer </label>
            <select name="user_id"  id="user_id" class="form-control single-select">
                <option value="">All Employer</option> 
                <?php 
                    $user=$d->select("users_master","society_id='$society_id' AND floor_id=$floor_id");  
                    while ($userdata=mysqli_fetch_array($user)) {
                ?>
                <option <?php if($_GET['user_id']==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
                <?php } ?>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Expense Status </label>
            <select name="expense_status"  class="form-control">
                <option value="">All</option> 
                <option <?php if($_GET['expense_status']=='0') { echo 'selected';} ?> value="0"  >Pending</option>
                <option <?php if($_GET['expense_status']=='1') { echo 'selected';} ?>  value="1" >Approved</option>
                <option <?php if($_GET['expense_status']=='2') { echo 'selected';} ?>  value="2" >Declined</option>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Status </label>
            <select name="expense_paid_status"  class="form-control">
                <option value="">All</option> 
                <option <?php if($_GET['expense_paid_status']=='1') { echo 'selected';} ?> value="1"  >Paid</option>
                <option <?php if($_GET['expense_paid_status']=='0') { echo 'selected';} ?>  value="0" >Unpaid</option>
            </select>
          </div>
          <div class="col-lg-12 col-6 text-center">
            <label  class="form-control-label"> </label>
              <button type="submit" class="btn btn-sm btn-primary">Get Report</button>
          </div>
     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                extract(array_map("test_input" , $_GET));
                // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                 $date=date_create($from);
                $dateTo=date_create($toDate);
                $nFrom= date_format($date,"Y-m-d 00:00:00");
                $nTo= date_format($dateTo,"Y-m-d 23:59:59");
                if(isset($_GET['floor_id']) && $_GET['floor_id'] > 0) {
                    $deptFilterQuery = " AND user_expenses.floor_id='$_GET[floor_id]'";
                }
                if(isset($_GET['user_id']) && $_GET['user_id'] > 0) {
                    $userFilterQuery = " AND user_expenses.user_id='$_GET[user_id]'";
                }
                if(isset($_GET['expense_status']) && $_GET['expense_status'] != '') {
                  $eStatusFilterQuery = " AND user_expenses.expense_status='$_GET[expense_status]'";
                }
                if(isset($_GET['expense_paid_status']) && $_GET['expense_paid_status'] != '') {
                  $statusFilterQuery = " AND user_expenses.expense_paid_status='$_GET[expense_paid_status]'";
                }
                $q = $d->select("user_expenses,floors_master,users_master", "users_master.user_id=user_expenses.user_id AND user_expenses.floor_id=floors_master.floor_id AND user_expenses.society_id='$society_id' AND user_expenses.date BETWEEN '$nFrom' AND '$nTo' $deptFilterQuery $userFilterQuery $eStatusFilterQuery $statusFilterQuery ", "ORDER BY user_expenses.created_at DESC");
                  $i=1;
                if (isset($_GET['from'])) {
               ?>
                <div class="row">
                
                <div class="col-md-3 pl-3">
                  Total Expense Price:  <?php 
                $q3 = $d->selectRow("SUM(amount) AS expAmount","user_expenses,users_master","users_master.user_id=user_expenses.user_id  AND user_expenses.society_id='$society_id' AND user_expenses.date BETWEEN '$nFrom' AND '$nTo' $deptFilterQuery $userFilterQuery $eStatusFilterQuery $statusFilterQuery ", "ORDER BY user_expenses.created_at DESC");
                $PriceData = mysqli_fetch_assoc($q3);
                     // print_r($PriceData);
                     echo number_format($PriceData['expAmount'],2,'.','');
                   ?>
                </div>
              </div>
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Department</th>
                        <th>Employee</th>
                        <th>Expense Title</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Status</th>  
                        <th>Paid Status</th>  
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['floor_name']; ?> (<?php echo $data['user_designation']; ?>)</td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php echo $data['expense_title']; ?></td>
                        <td><?php echo $data['amount']; ?></td>
                        <td><?php echo date("d M Y", strtotime($data['date'])); ?></td>
                        <td><?php
                            if ($data['expense_status']==0) { 
                                echo "Pending";
                            } else if($data['expense_status']==1){
                                echo "Approved";
                            } else{
                                echo "Declined";
                            }
                        ?></td>
                        <td><?php
                            if ($data['expense_paid_status']==0) { 
                                echo "Unpaid";
                            } else{
                                echo "Paid";
                            }
                        ?></td>
                    </tr>
                  <?php } ?>
                </tbody>  
            </table>
            <?php } else {  ?>
              <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->