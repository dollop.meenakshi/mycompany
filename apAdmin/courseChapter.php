<style>
  .showYoutubeinput {
    display: none;
  }

  .inoutForText {
    display: none;
  }
</style>
<?php
error_reporting(0);
$cId = (int)$_REQUEST['cId'];

      $courseSql = $d->selectRow('course_master.*','course_master',"course_id=$cId AND course_is_deleted=0");
      $courseData = mysqli_fetch_assoc($courseSql);
      if($courseData){
      $q = $d->selectRow("course_chapter_master.*,users_master.user_full_name,bms_admin_master.admin_name,(SELECT COUNT(*)  FROM course_lessons_master WHERE course_lessons_master.course_chapter_id = course_chapter_master.course_chapter_id AND course_lessons_master.lesson_is_deleted=0 AND course_lessons_master.lesson_is_active=0) AS lCout", "course_chapter_master LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=course_chapter_master.course_chapter_created_by LEFT JOIN users_master ON users_master.user_id=course_chapter_master.course_chapter_created_by", " course_chapter_master.society_id='$society_id' AND course_chapter_master.course_chapter_is_deleted=0  AND course_chapter_master.course_id='$cId'", "ORDER BY course_chapter_master.course_chapter_id ASC");
        $counter = 1;
      }
?>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-3 col-md-6 col-6">
        <h4 class="page-title"><?php echo $courseData['course_title'];?> Course Chapter</h4>
      </div>
      <div class="col-sm-3 col-md-6 col-6">
        <div class="btn-group text-right float-sm-right">
          <?php 
           if( isset($courseData) ){
          ?>
          <a href="addcourseChapter" data-toggle="modal" data-target="#addModal" onclick="buttonSettingForLmsChptr()" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
          <?php } ?>
        </div>
      </div>
    </div>
   <div id="accordion8">
      <?php
      $i = 1;
      if($courseData){
      if(mysqli_num_rows($q)>0){
      while ($data = mysqli_fetch_array($q)) {
        if ($data['course_chapter_created_by_type'] == 0) {
          $createdBy = $data['admin_name'];
        } else {
          $createdBy = $data['user_full_name'];
        }
      ?>

        <div class="col-lg-12">
          <div id="accordion<?php echo $data['course_chapter_id']; ?>">
            <div class="card">
              <div class="card-header bg-primary">
                <div class="row">
                  <div class="col-md-6">
                    <button class="btn btn-link text-white shadow-none collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $data['course_chapter_id']; ?>" aria-expanded="true" aria-controls="collapse-1"><?php echo $i++;?>. <?php echo $data['chapter_title']; ?></button>
                  </div>
                  <div class="col-md-3 text-right">
                    <p class="pl-3 text-white"> <i class="fa fa-calendar" aria-hidden="true"></i> <?php if ($data['course_chapter_created_date'] != "0000-00-00") {echo date("d M Y h:i A", strtotime($data['course_chapter_created_date']));} ?>
                    </p>
                  </div>
                  <div class="col-md-3 text-right">
                      <p>
                        <?php $totalVariant = $d->count_data_direct("lessons_id", "course_lessons_master", "course_chapter_id='$data[course_chapter_id]'");
                        if ($totalVariant == 0) { ?>
                          <i class=" btn btn-sm btn-danger fa fa-trash text-white " onclick="deletCourseChapter(<?php echo $data['course_chapter_id']; ?>)"></i>
                        <?php }  ?>
                      
                      <?php if ($data['course_chapter_is_active'] == "0") {
                        $status = "courseChapterStatusDeactive";
                        $active = "checked";
                      } else {
                        $status = "courseChapterStatusActive";
                        $active = "";
                      } ?>
                      <input type="checkbox" <?php echo $active; ?> class="js-switch ml-1" data-color="#15ca20" onchange="changeStatus('<?php echo $data['course_chapter_id']; ?>','<?php echo $status; ?>');" data-size="small" />
                      <a class="btn text-white btn-sm btn-warning" onclick="courseChapterDataSet(<?php echo $data['course_chapter_id']; ?>)"> Edit </a>
                      </p>
                  </div>
                </div>
              </div>
              <div id="collapse-<?php echo $data['course_chapter_id']; ?>" class="collapse show" data-parent="#accordion<?php echo $data['course_chapter_id']; ?>" style="">
                <div class="card-body">
                  <div class="row m-0 mb-2">
                    <div class="col-md-9 col-6">
                      <!-- <p class="card-title mb-0">Shivani Gupta (QA)</p> -->
                      <div class="text-muted small">Added By: <?php echo $createdBy; ?> </div>
                    </div>
                    <div class="col-md-3 col-6 text-right">
                      <a href="courseLessons?cpId=<?php echo $data['course_chapter_id']; ?>" class="btn btn-sm btn-primary">Add Lesson (<?php echo $data['lCout']; ?>)</a>
                     
                    </div>
                  </div>
                  <div class="text-right mb-1"></div>
                  <?php if (strlen($data['chapter_description']>0)): ?>
                  <label class="text-primary">Description : </label>
                  <p class="card-text ml-1 mb-0"><?php echo $data['chapter_description']; ?></p>
                  <?php endif ?>
                  <div class="row mt-1">
                    <?php
                    $counter = 1;
                    $lessonSql = $d->selectRow('(SELECT COUNT(*) FROM user_course_master WHERE user_course_master.lessons_id = course_lessons_master.lessons_id AND user_status!=0 AND delete_status=0) AS total_submited_employees, course_lessons_master.*,users_master.user_full_name,bms_admin_master.admin_name,lesson_type_master.lesson_type', "course_lessons_master LEFT JOIN lesson_type_master ON lesson_type_master.lesson_type_id = course_lessons_master.lesson_type_id LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=course_lessons_master.lesson_created_by LEFT JOIN users_master ON users_master.user_id=course_lessons_master.lesson_created_by", "course_lessons_master.course_chapter_id='$data[course_chapter_id]' AND course_lessons_master.lesson_is_deleted =0 ", "ORDER BY course_lessons_master.lessons_id");
                    if (mysqli_num_rows($lessonSql) > 0) {
                    ?>
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Sr.No</th>
                              <th>Lesson Title</th>
                              <th>Lesson Type</th>
                              <th>View</th>
                              <th>Date</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php

                            while ($lessData = mysqli_fetch_array($lessonSql)) {
                              if ($lessData['lesson_created_by_type'] == 0) {
                                $createdLessBy = $lessData['admin_name'];
                              } else {
                                $createdLessBy = $lessData['user_full_name'];
                              }

                            ?>
                              <tr>
                                <!--<td class="text-center">
                                   <input type="hidden" name="id" id="id" value="<?php echo $lessData['lessons_id']; ?>">
                                  <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $lessData['lessons_id']; ?>"> 
                                </td>-->
                                <td><?php echo $counter++; ?></td>
                                <td><?php custom_echo($lessData['lesson_title'],30); ?></td>
                                <td><?php echo $lessData['lesson_type']; ?></td>
                                <td><?php if ($lessData['lesson_video_link'] != "") { ?>
                                    <a target="_blank" class="badge badge-primary" href="<?php echo $lessData['lesson_video_link']; ?>">View </a>
                                  <?php } else if ($lessData['lesson_video'] != "") { ?>
                                    <a target="_blank" class="badge badge-primary" href="../img/course/<?php echo $lessData['lesson_video']; ?>">View </i></a>
                                  <?php } ?>
                                </td>
                                <td><?php echo date("d M Y h:i A", strtotime($lessData['lesson_created_date'])); ?></td>
                                <td>
                                  <div class="d-flex align-items-center">
                                    <a href="courseLessons?cpId=<?php echo $data['course_chapter_id']; ?>&lsId=<?php echo $lessData['lessons_id']; ?>"  class="btn btn-sm btn-primary mr-2" > <i class="fa fa-pencil"></i></a>
                                    <!-- onclick="courseLessonsDataSet(<?php echo $lessData['lessons_id']; ?>)" -->
                                    <?php if ($lessData['lesson_is_active'] == "0") {
                                      $status = "lessonStatusDeactive";
                                      $active = "checked";
                                    } else {
                                      $status = "lessonStatusActive";
                                      $active = "";
                                    } ?>
                                    <input type="checkbox" <?php echo $active; ?> class="js-switch " data-color="#15ca20" onchange="changeStatus('<?php echo $lessData['lessons_id']; ?>','<?php echo $status; ?>');" data-size="small" />
                                    <button type="submit" class="btn btn-sm btn-primary ml-2 mr-1" onclick="courseLessonsModal(<?php echo $lessData['lessons_id']; ?>)"> <i class="fa fa-eye"></i></button>
                                    <?php if ($lessData['total_submited_employees']==0) { ?>
                                    <button class="btn btn-sm badge-danger" onclick="lessonDelete(<?php echo $lessData['lessons_id']; ?>)"><i class="fa fa-trash"></i></button>
                                    <?php } ?>
                                  </div>
                                </td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php }
      }
      else{
        ?>
         <div class="" role="alert">
            <span><strong>Note :</strong> Please Add Chapters For this course .</span>
          </div>
        <?php
      }
    }else
    { ?>
      <div class="" role="alert">
         <span><strong>Note :</strong> No Course Available .</span>
       </div>
     <?php

    }
      ?>
    </div>
  </div>
  <!-- End container-fluid-->
</div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Course Chapter</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body">

          <form id="courseChapterAdd" action="controller/courseChapterController.php" enctype="multipart/form-data" method="post">
            <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-6 col-form-label">Chapter Title <span class="required">*</span></label>
              <div class="col-lg-12 col-md-12" id="">
                <input type="text" class="form-control frmRst" placeholder="Chapter Title " id="chapter_title" name="chapter_title" value="">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-lg-12 col-md-12 col-6 col-form-label">Description</label>
              <div class="col-lg-12 col-md-12" id="">
                <textarea class="form-control frmRst" rows="7" id="chapter_description" placeholder="Chapter Description" name="chapter_description"></textarea>
              </div>
            </div>
            <div class="form-footer text-center">
              <input type="hidden" id="course_chapter_id" name="course_chapter_id" class="frmRst" value="">
              <input type="hidden" id="course_id" name="course_id" class="" value="<?php echo $cId; ?>">
              <button id="addcourseChapterBtn" type="submit" class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
              <input type="hidden" name="addcourseChapter" value="addcourseChapter">
              <button type="submit" class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
              <button type="button" value="add" class="btn btn-danger cancel" onclick="resetForm();"><i class="fa fa-check-square-o"></i> Reset</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addLessonModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Course Lessons</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">
          <form id="addCourseLesson" action="controller/CourseLessonController.php" enctype="multipart/form-data" method="post">
            <div class="form-group row ">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Lesson Title<span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" value="<?php if (isset($data['lesson_title']) && $data['lesson_title'] != "") {echo $data['lesson_title']; } ?>" name="lesson_title" id="lesson_title" class="form-control rstFrm">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Lesson Type <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <select type="text" id="lesson_type_id" required="" class="form-control inputSl single-select " name="lesson_type_id">
                  <option value="">-- Select --</option>
                  <?php
                  $course = $d->select("lesson_type_master", "is_deleted=0 AND is_active=0");
                  while ($courseData = mysqli_fetch_array($course)) {
                  ?>
                    <option <?php if (isset($data['lesson_type_id']) && $data['lesson_type_id'] == $courseData['lesson_type_id']) {
                              echo "selected";
                            } ?> data-name="<?php echo $courseData['lesson_type']; ?>" value="<?php if (isset($courseData['lesson_type_id']) && $courseData['lesson_type_id'] != "") {echo $courseData['lesson_type_id'];} ?>"> <?php if (isset($courseData['lesson_type_id']) && $courseData['lesson_type_id'] != "") { echo $courseData['lesson_type'];} ?>
                    </option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row lessonInput">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Lesson File <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="file" name="lesson_video" accept="audio/*" id="lesson_video" class=" rstFrm form-control-file border ">
                <input type="hidden" class="rstFrm" name="lesson_video_old" id="lesson_video_old" value="<?php if (isset($data['lesson_video']) && $data['lesson_video'] != "") { echo $data['lesson_video'];} ?>">
              </div>
            </div>
            <div class="showYoutubeinput">
              <div class="form-group row ">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Link <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                  <input type="text" value="<?php if (isset($data['lesson_video_link']) && $data['lesson_video_link'] != "") { echo $data['lesson_video_link']; } ?>" name="lesson_video_link" id="lesson_video_link" class="form-control rstFrm ">
                </div>
              </div>
            </div>
            <div class="inoutForText">
              <div class="form-group row ">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Text <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                  <textarea required="" id="CoursesummernoteImgage" class="lesson_text" name="lesson_text"><?php echo $data['lesson_text']; ?></textarea>
                </div>
              </div>
            </div>
            <input type="hidden" id="course_chapter_id" required="" value="" name="course_chapter_id" class="cpId">
            <div class="form-footer text-center">
              <input type="hidden" id="lessons_id" name="lessons_id" value="<?php echo $lsId;  ?>" class="rstFrm">
              <button id="addCourseLessonBtn" type="submit" class="btn btn-success hideupdate course_lesson_button"><i class="fa fa-check-square-o"></i><?php if (isset($lsId) && $lsId > 0) { echo "Add"; } else { echo "Update";} ?> </button>
              <button type="submit" class="btn btn-success hideAdd course_lesson_button"><i class="fa fa-check-square-o"></i><?php if (isset($lsId) && $lsId > 0) {echo "Add"; } else { echo "Add";} ?> </button>
              <input type="hidden" name="addCourseLesson" value="addCourseLesson">
              <button type="button" value="add" class="btn btn-danger " onclick="resetLessonForm()"><i class="fa fa-check-square-o"></i> Reset</button>
            </div>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>
<div class="modal fade" id="detail_view">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Course Lesson</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 " style="align-content: center;">
        <div class="row mx-0" id="course_lesson">

        </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>
<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css" />
<script type="text/javascript">
  function openLessonModal(Cpid) {
    $('.cpId').val(Cpid);
    $('.hideupdate').hide();
    $(".lesson_text").summernote("code", "");
    $('.hideAdd').show();
    $('.rstFrm').val('');
    $('.inputSl').val('');
    $('.inputSl').select2('');
    $('#addLessonModal').modal();
  }

  $(document).ready(function() {
    $("#lesson_type_id").change(function() {
      $("#lesson_video").val("");
      type = $(this).find(':selected').attr('data-name');
      if (type == "PDF" || type == "Document" || type == "Audio" || type == "Video") {
        $('.showYoutubeinput').hide();
        $('.inoutForText').hide();
        $('.lessonInput').show();
        if (type == "PDF") {
          $('#lesson_video').addClass('pdfOnly');
          $('#lesson_video').attr("accept", "image/*,.pdf");
          $('#lesson_video').rules("add", {
            extension: "pdf",
            messages: {
                required: "Please Select PDF Only",
            }
        });
        } else if (type == "Audio") {
          $('#lesson_video').attr("accept", "audio/*");
          $('#lesson_video').rules("add", {
            extension: "mp3",
            messages: {
                required: "Please Select Audio Only",
            }
          });
        } else if (type == "Video") {
          $('#lesson_video').attr("accept", "video/*");
          $('#lesson_video').rules("add", {
            extension: "mp4|gif",
            messages: {
                required: "Please Select Video Only",
            }
        });
        } else {
          $('#lesson_video').attr("accept", ".txt,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document");
          $('#lesson_video').rules("add", {
            extension: "docx|txt|doc|pdf|xml",
            messages: {
                required: "Please Select Document Only",
            }
         });
        }
      } else if (type == "Text") {
        $('.showYoutubeinput').hide();
        $('.inoutForText').show();
        $('.lessonInput').hide();
      } else {
        $('.showYoutubeinput').show();
        $('.inoutForText').hide();
        $('.lessonInput').hide();
      }
    });
  });


  $("document").ready(function() {

    $("#lesson_video").change(function() {
      lesson_type_id = $('#lesson_type_id').val();
      if (lesson_type_id != "") {

        fileName = this.value;
        fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
        console.log(fileExtension)
        if(lesson_type_id == 2) {
          extensionResume = ["pdf", "PDF"];
        } else if (lesson_type_id == 3) {
          extensionResume = ["txt", "TXT", "doc", "DOC", "docx", "DOCX"];
        } else if (lesson_type_id == 4) {
          extensionResume = ["mp4", "MP4"];
        } else if (lesson_type_id == 6) {
          extensionResume = ["mp3", "MP3"];
        }
        console.log(jQuery.inArray( fileExtension, extensionResume));
        if((jQuery.inArray( fileExtension, extensionResume))== -1 ){
          $("#lesson_video").val("");
          swal('Invalid Type');
        }
      } else {
        $("#lesson_video").val("");
        swal('Please Select Lesson type');
      }
    });
  });

  function checkLessonType() {
    type = $("#lesson_type_id").val();
    type = parseInt(type);

    if (type == 2 || type == 3 || type == 4 || type == 6) {
      $('.showYoutubeinput').hide();
      $('.inoutForText').hide();
      $('.lessonInput').show();

    } else if (type == 7) {
      console.log(type);
      $('.showYoutubeinput').hide();
      $('.inoutForText').show();
      $('.lessonInput').hide();
    } else {
      $('.showYoutubeinput').show();
      $('.inoutForText').hide();
      $('.lessonInput').hide();
    }
  }

  function resetForm() {
    $('.frmRst').val('');
    $('.inputSl').val('').trigger('change');
  }
  function resetLessonForm() {
    $('.rstFrm').val('');
    $('.inputSl').val('').trigger('change');
  }

  function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
    }
    return false;
  }

  function buttonSettingForLmsChptr() {
    $('.hideupdate').hide();
    $('.hideAdd').show();
    $('.frmRst').val('');
    $('.inputSl').val('');
    $('.inputSl').select2();

  }

  /* $(document).ready(function() {
    $("#lesson_type_id").change(function() {
      type = $(this).find(':selected').attr('data-name');
      if (type == "PDF" || type == "Document" || type == "Audio" || type == "Video") {
        $('.showYoutubeinput').hide();
        $('.lessonInput').show();

      } else if (type == "Text") {

      } else {
        $('.showYoutubeinput').show();
        $('.lessonInput').hide();
      }
    });
  }); */
</script>
<style>
  .showYoutubeinput {
    display: none;
  }
</style>