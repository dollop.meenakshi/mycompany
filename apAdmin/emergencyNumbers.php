
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-6">
        <h4 class="page-title"><?php echo $xml->string->emergency_numbers; ?> </h4>
      </div>
      <div class="col-sm-3 col-6">
        <div class="btn-group float-sm-right">
          <a href="#" data-toggle="modal" data-target="#addEmrModal" class="btn btn-primary btn-sm  waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteEmergency');" class="btn btn-danger  btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> <?php echo $xml->string->delete; ?> </a>
          
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>#</th>
                    <th><?php echo $xml->string->name; ?></th>
                    <th><?php echo $xml->string->description; ?></th>
                    <th><?php echo $xml->string->emergency_number; ?></th>
                    <th><?php echo $xml->string->block; ?></th>
                    <th><?php echo $xml->string->action; ?></th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $q=$d->select("emergemcy_number_list","society_id='$society_id'","");
                  $i = 0;
                  while($row=mysqli_fetch_array($q))
                  {
                  // extract($row);
                  $i++;
                    $designation=mysqli_real_escape_string($con,  $row['designation']);
                    $name=mysqli_real_escape_string($con,  $row['name']);
                  ?>
                  <tr>
                     <td class='text-center'>
                      <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['emergency_id']; ?>">
                    </td>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['designation']; ?></td>
                    <td><?php echo $row['mobile']; ?></td>
                    <td><?php if($row['city_id']!="0") {
                      $qc=$d->selectRow("block_name","block_master","block_id='$row[city_id]'");
                      $cityData=mysqli_fetch_array($qc);
                      echo $cityData['block_name'];
                    } else {
                      echo "All";
                    }; ?></td>
                    
                    <td>
                        <button data-toggle="modal" data-target="#emrNumberEdit"  type="button" onclick="getEmerEditNumber('<?php echo $row['emergency_id']; ?>');" class="btn btn-primary btn-sm" name="pullingReport"><i class="fa fa-pencil"></i> </button>
                        
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
                
              </div>
            </table>
          </div>
        </div>
      </div>
    </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->
  
  </div><!--End content-wrapper-->
  <!--Start Back To Top Button-->





<div class="modal fade" id="addEmrModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add; ?> <?php echo $xml->string->emergency_number; ?> </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        
          <form id="emergencyNumberAdd" action="controller/emerController.php" method="post">
                <div class="form-group row">
                    <label for="inputTextBox" class="col-sm-4 col-form-label"><?php echo $xml->string->name; ?> <span class="text-danger">*</span></label>
                    <div class="col-sm-8" id="parkingName1">
                      <input type="text" id="onlyName" maxlength="20" name="name"  required="" class="form-control text-capitalize onlyName" >
                      
                    </div>
                </div>
              
               
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->description; ?> <span class="text-danger">*</span></label>
                    <div class="col-sm-8" >
                      <input type="text" maxlength="20" name="designation" required class="form-control  onlyName">
                    </div>
                </div>
                 <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->emergency_number; ?> <span class="text-danger">*</span></label>
                    <div class="col-sm-8" >
                      <input type="text" maxlength="15" minlength="3" id="editMobile1" inputmode="numeric" name="mobile" required class="form-control ">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->blocks; ?> <span class="text-danger">*</span></label>
                    <div class="col-sm-8" id="PaybleAmount">
                      <select    name="city_id" id="city_id" required class="form-control ">
                        <option value="0"> All <?php echo $xml->string->blocks; ?></option>
                        <?php $q = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_sort ASC"); 
                         while ($data=mysqli_fetch_array($q)) { ?>
                        <option value="<?php echo $data['block_id']; ?>"><?php echo $data['block_name']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                </div>

                <div class="form-footer text-center">
                  <input type="hidden" name="addEmergency" value="allocateParking">
                  
                  <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->add; ?></button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->

<div class="modal fade" id="emrNumberEdit">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->edit; ?> <?php echo $xml->string->emergency_number; ?> </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="editemerDiv">

          
     
    </div>
  </div>
</div><!--End Modal -->
</div>