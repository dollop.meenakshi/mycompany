<?php
error_reporting(0);
extract(array_map("test_input" , $_REQUEST));
 if(filter_var($request_id, FILTER_VALIDATE_INT) != true){
     $_SESSION['msg1']='Invalid REQUEST';
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='requests';
      </script>");
}
$request_id = (int)$request_id;

$q=$d->select("request_master,users_master,unit_master,block_master","block_master.block_id=unit_master.block_id AND unit_master.unit_id=request_master.request_unit_id AND users_master.user_id=request_master.request_user_id AND request_master.society_id='$society_id' AND request_master.request_id='$request_id'","ORDER BY request_master.request_id DESC");
$data=mysqli_fetch_array($q);
if ($data['user_type']==0) {
                            $uType = "Owner";
                          } else {
                            $uType= "Tenant";
                          }
?>
<link href="assets/plugins/vertical-timeline/css/vertical-timeline1.css" rel="stylesheet"/>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Request History (<?php echo 'REQ'.$data['request_id']; ?>)</h4>
        <p><b><?php echo $xml->string->unit; ?> :</b> <?php echo $data['user_full_name'].'-'.$data['user_designation']; ?>- (<?php echo $data['block_name']; ?> )</p>
        <p><b>Title :</b> <?php echo $data['request_title']; ?></p>
        <p class="text-justify"><b>Description :</b> <?php echo $data['request_description']; ?></p>
        <p><b>Date :</b> <?php echo  date('d M Y h:i A',strtotime($data['request_date'])); ?></p>
          Status : <?php
              // if ($data['flag_delete']==0) {
               
               if ($data['request_status']==0) {  ?>
                <span   data-toggle="modal" data-target="#vieComp"  onclick="vieComplain('<?php echo $data['request_id']; ?>');" class="badge badge-warning m-1 pointerCursor">OPEN <i class="fa fa-pencil"></i></span>
              <?php } elseif ($data['request_status']==1) { ?>
                <span   data-toggle="modal" data-target="#vieComp"  onclick="vieComplain('<?php echo $data['request_id']; ?>');" class="badge badge-success  m-1 pointerCursor">CLOSE </span>
              <?php  } elseif ($data['request_status']==2) { ?>
                <span  data-toggle="modal" data-target="#vieComp"   onclick="vieComplain('<?php echo $data['request_id']; ?>');" class="badge badge-info  m-1 pointerCursor">In Progress <i class="fa fa-pencil"></i></span>
              <?php  } elseif ($data['request_status']==3) { ?>
                 <span   data-toggle="modal" data-target="#vieComp"  onclick="vieComplain('<?php echo $data['request_id']; ?>');" class="badge badge-warning m-1 pointerCursor">OPEN <i class="fa fa-pencil"></i></span>
              <?php } ?>
      </div>
    </div>
     <div class="row pt-2 pb-2">
      <div class="col-sm-12 text-center">
         <button data-toggle="modal" data-target="#vieComp"   onclick="vieComplain('<?php echo $data['request_id']; ?>');"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-pencil"></i> Reply</button>
      </div>
    </div>
    <section class="cd-timeline js-cd-timeline">
      <div class="cd-timeline__container">
        <?php 
          $timelineQuery = $d->select("request_track_master,request_master","request_track_master.request_id=request_master.request_id AND request_track_master.request_id='$request_id'","ORDER BY request_track_master.request_track_id DESC");
          while($timelineData = mysqli_fetch_array($timelineQuery)){
        ?>
          <div class="cd-timeline__block js-cd-block <?php if($timelineData['request_track_by']==1){ echo "floatRight"; } ?>">
            <div class="cd-timeline__img cd-timeline__img--picture js-cd-img text-center ">
              <img src="../img/fav.png">
            </div> 

            <div class="cd-timeline__content js-cd-content">
              <p><?php if($timelineData['request_track_by']==1){ 
                $qad=$d->select("bms_admin_master","admin_id='$timelineData[admin_id]'");
                $adminData=mysqli_fetch_array($qad);
                ?>
                <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="../img/society/<?php echo $adminData['admin_profile']; ?>"  width="30" height="30"   src="#" alt="your image" class='profile' />
                <?php echo $adminData['admin_name'] .' (Admin)';} else {
                ?>
                <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $data['user_profile_pic']; ?>"  width="30" height="30"   src="#" alt="your image" class='profile' />
                <?php echo $data['user_full_name'];
              } ?></p>
              <h6 style="word-wrap: break-word;"><?php echo $timelineData['request_track_msg']; ?></h6>
              
              <?php if ($timelineData['request_track_img']!='') { ?>
                <a data-fancybox="images" data-caption="Photo Name : <?php echo $timelineData["request_track_img"]; ?>" target="_blank" href="../img/request/<?php echo $timelineData['request_track_img']; ?>">
                  <img src="../img/request/<?php echo $timelineData['request_track_img'] ?>" width="100" height="100">
                </a>
              <?php } ?>
              <?php if ($timelineData['request_track_voice']!='') { ?>
                <br>
                <br>
               <div class="table-responsive">
                <audio controls>
                  <source src="../img/request/<?php echo $timelineData['request_track_voice']; ?>" type="audio/mp3">
                </audio> 
              </div>
              <?php } ?>
              <p><?php echo date('d M Y h:i A',strtotime($timelineData['request_track_date_time'])); ?> (<?php echo $timelineData['request_status_view']; ?>)</p>

              <span class="cd-timeline__date"><?php echo date('M d',strtotime($timelineData['request_track_date_time'])); ?></span>
            </div>
          </div>
        <?php } ?>
      </div>
    </section>
  </div>
</div>
<!-- <script src="assets/js/jquery.min.js"></script> -->
<!-- <script src="assets/plugins/vertical-timeline/js/vertical-timeline.js"></script> -->

<script type="text/javascript">
  function vieComplain(request_id) {
      $.ajax({
          url: "getRequestDetails.php",
          cache: false,
          type: "POST",
          data: {request_id : request_id},
          success: function(response){
              $('#comResp').html(response);
            
              
          }
       });
  } 
</script>



<div class="modal fade" id="vieComp">
  <div class="modal-dialog">
    <div class="modal-content border-success">
      <div class="modal-header bg-success">
        <h5 class="modal-title text-white">View Complain </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="comResp">
          
      </div>
     
    </div>
  </div>
</div>