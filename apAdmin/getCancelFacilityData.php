<?php 
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

if (isset($_COOKIE['bms_admin_id'])) {
    $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
extract(array_map("test_input" , $_POST));
$checS=$d->select("society_master","society_id='$society_id'");
$sData=mysqli_fetch_array($checS);
$currency= $sData['currency'];
$virtual_wallet= $sData['virtual_wallet'];

//IS_605
if(isset($booking_id)  ){

  
$q=$d->select("facilitybooking_master,facilities_master,unit_master","unit_master.unit_id =facilitybooking_master.unit_id AND facilitybooking_master.facility_id=facilities_master.facility_id AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.booking_id = $booking_id","ORDER BY booking_id DESC");
$row=mysqli_fetch_array($q);
extract($row);
 $receive_amount =  $receive_amount-$transaction_charges;
 ?>

 
<form id="EditCommEntryFrm" action="controller/facilitiesController.php" method="POST" enctype="multipart/form-data" >
              <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />  
                 
                <div class="form-group row">
                 
               <label for="input-13" class="col-sm-4 col-form-label"> Facility Name</label>
                  <div class="col-sm-8">
                    <?php echo $facility_name;?>
                  </div> 
                </div>
                 <div class="form-group row">
                    <label for="input-14" class="col-sm-4 col-form-label">Booking Date </label>
                  <div class="col-sm-8">
                    <?php echo $booked_date;?>
                  </div>
                </div>
                <div class="form-group row">
                    <label for="input-14" class="col-sm-4 col-form-label">Received Amount </label>
                  <div class="col-sm-8">
                    <?php echo $receive_amount;?>
                  </div>
                </div>
                <div class="form-group row">
                    <label for="input-14" class="col-sm-4 col-form-label">Refund Amount <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                    <input type="text" min="0" max="<?php echo $receive_amount;?>" name="expenses_amount" value="<?php echo $receive_amount;?>" maxlength="10"   class="form-control onlyNumber" inputmode="numeric">
                  </div>
                </div>
                <div class="form-group row">
                    <label for="input-14" class="col-sm-4 col-form-label">Refund Mode <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                    <select required="" name="refund_type" class="form-control">
                      <option value="">-- Select --</option>
                      <option value="0">Cash</option>
                      <?php if($virtual_wallet==0) { ?>
                      <!-- <option value="1">User Wallet</option> -->
                    <?php } ?>
                    </select>
                  </div>
                </div>
               <div class="form-group row">
                <label for="input-14" class="col-sm-4 col-form-label">Remark <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                  <textarea maxlength="100" required="" type="text" class="form-control " name="expenses_title" id="expenses_title"></textarea>
                </div>
                </div>
                  
                 
                  <input type="hidden" name="cancelFacilityReq" value="cancelFacilityReq">
                  <input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id ?>">
                  <input type="hidden" name="facility_name" value="<?php echo $facility_name ?>">
                  <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
                  <input type="hidden" name="facility_id" value="<?php echo $facility_id ?>">
                  <input type="hidden" name="booking_id" value="<?php echo $booking_id ?>">
                    <div class="form-footer text-center">
                    <input type="submit" id="updateCommEntryBtn" class="btn form-btn btn-primary" name="updateCommEntry" value="Cancel Booking">
                  </div>
                  
              </form>
              <script src="assets/js/jquery.min.js"></script>
              <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
 <script type="text/javascript">
   //12march2020

    jQuery(document).ready(function($){
$.validator.addMethod('filesize4MB', function (value, element, arg) {
        var size =4000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 4MB."));

 $.validator.addMethod("noSpace", function(value, element) { 
   return value == '' || value.trim().length != 0;  
}, "No space please and don't leave it empty");



 
$("#EditCommEntryFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            expenses_title:
            {
                required: true,
                noSpace: true 
            },
             expenses_amount:
            {
                required: true,
                noSpace: true 
            },
            common_entry_image:
            {
                required: false,
                filesize4MB: true 
            },
        },
        messages: {
            expenses_title: {
                required: "Please enter remark", 
       }
      },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                // $(".ajax-loader").show();
                form.submit(); 
          }
});
});
//12march2020
 </script>
        
<?php }   ?>