<?php 
if (!isset($d)) {
  header('location:welcome');
}
$hideDatatable = "true";
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- <a href="test.php">Audio Check</a> -->
      <!--Start Dashboard Content-->
    <!-- Super Admin   -->
    <?php
   

    $blockAppendQuery;
    if ($adminData['role_id']==1){ 

        $sql01 = "SELECT 
              (SELECT COUNT(*) FROM society_master) AS society_count ,
              (SELECT COUNT(*) FROM bms_admin_master WHERE society_id!=0 AND admin_active_status=0) AS admin_count ,
              (SELECT (CASE WHEN SUM(used_credit)>0 THEN SUM(used_credit) ELSE '0' END) FROM sms_log_master) AS sms_credit_used 
            ";
        $dashboardCount = $d->executeSql($sql01,'row_array');

      ?>
        
    <div class="row mt-3">
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
          <div class="card gradient-bloody">
            <a href="buildings">
            <div class="p-2">
              <div class="media align-items-center">
              <div class="media-body">
                <p class="text-white">Companies</p>
                <h4 class="text-white line-height-5"><?php echo $dashboardCount['society_count'];//$d->count_data_direct("society_id","society_master",""); ?></h4>
              </div>
              <div class="w-circle-icon rounded-circle  border-white">
                <img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/my-companyxxxhdpi.png"></div>
            </div>
            </div>
            </a>
          </div>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
          <div class="card gradient-scooter">
            <a href="companyAdmins">
            <div class="p-2">
              <div class="media align-items-center">
              <div class="media-body">
                <p class="text-white"> Admins</p>
                <h4 class="text-white line-height-5"><?php echo $dashboardCount['admin_count'];//$d->count_data_direct("society_id","bms_admin_master","society_id!=0 AND admin_active_status=0"); ?></h4>
              </div>
              <div class="w-circle-icon rounded-circle border-white">
                <img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/My-profilexxxhdpi.png"></div>
            </div>
            </div>
            </a>
          </div>
        </div>
     
      <div class="col-6 col-lg-4 col-md-4 col-xl-3">
        <div class="card gradient-blooker">
          <div class="p-2">
            <div class="media align-items-center">
            <div class="media-body">
              <p class="text-white">SMS Credit Used</p>
              <h4 class="text-white line-height-5">
               <?php 
               echo $asif=$dashboardCount['sms_credit_used'];
                /*$count5=$d->sum_data("used_credit","sms_log_master","");
                  while($row=mysqli_fetch_array($count5))
                 {
                   echo   $asif=$row['SUM(used_credit)'];
                    
                  }*/ ?>
              </h4>
            </div>
            <div class="w-circle-icon rounded-circle border-white">
              <img class="myIcon lazyload" src="../img/newFav.png" data-src="img/icons/cash-icon.png"></div>
          </div>
          </div>
        </div>
      </div>
     
    </div>
   

    <?php } else {
      $difference_days = (int)$difference_days;
      if ($package_id==0 && $difference_days<11) { 
     ?>
      <div class="alert alert-danger alert-dismissible" role="alert">
       <button type="button" class="close" data-dismiss="alert">×</button>
       
        <div class="alert-message">
          <span> Your Free Trial Plan Expire on <?php echo $plan_expire_date; 
         echo " (".$difference_days."  left)"; 

           ?></span>
          <a href="buyPlan">Click Here to Renew </a>
        </div>
      </div>
   <?php } if ($package_id!=0 && $difference_days<31) { 
     ?>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <div class="alert-message">
          <span><strong>Alert !</strong> Your Plan Expire on <?php echo  date("d/m/Y", strtotime($plan_expire_date)); 
         // echo " (".$difference_days."  left)"; 
           ?></span>
          <a href="buyPlan">Click Here to Renew </a>
        </div>
      </div>
   <?php } 
   //////////////////////////////////////
  $currentYear = date('Y');        
  $sql02 = "SELECT 
              (SELECT COUNT(*) FROM block_master WHERE society_id='$society_id' $blockAppendQuery) AS block_count ,
              (SELECT COUNT(*) FROM floors_master 
                  INNER JOIN block_master ON (floors_master.block_id=block_master.block_id) 
                  WHERE floors_master.society_id='$society_id' $blockAppendQuery $blockAppendQueryFloor) AS floor_count,

              (SELECT COUNT(*) FROM users_master 
                  INNER JOIN floors_master ON (floors_master.floor_id=users_master.floor_id) 
                  INNER JOIN block_master ON (block_master.block_id=floors_master.block_id) 
                  WHERE users_master.society_id='$society_id' AND users_master.delete_status=0 $blockAppendQuery $blockAppendQueryUser) AS user_count,
              (SELECT COUNT(*) FROM event_master 
                  WHERE event_master.society_id='$society_id') AS event_count,
              (SELECT COUNT(*) FROM holiday_master 
                  WHERE holiday_master.society_id='$society_id' AND DATE_FORMAT(holiday_start_date, '%Y')='$currentYear') AS holiday_count,
              (SELECT COUNT(*) FROM shift_timing_master 
                  WHERE is_deleted='0') AS shift_count,
              (SELECT COUNT(*) FROM assets_item_detail_master 
                  WHERE society_id='$society_id' AND status=0) AS assets_count,
              (SELECT COUNT(*) FROM survey_master 
                  WHERE society_id='$society_id') AS survey_count,
              (SELECT COUNT(*) FROM local_service_provider_users) AS vendor_count,


              (SELECT COUNT(*) FROM unit_master 
                  INNER JOIN users_master ON (users_master.unit_id=unit_master.unit_id) 
                  INNER JOIN floors_master ON (floors_master.floor_id=unit_master.floor_id) 
                  INNER JOIN block_master ON (block_master.block_id=floors_master.block_id) 
                  WHERE users_master.delete_status=0 AND users_master.society_id='$society_id' AND users_master.device='android' AND users_master.user_status=1 $blockAppendQueryUser) AS android_user_count,
              (SELECT COUNT(*) FROM unit_master 
                  INNER JOIN users_master ON (users_master.unit_id=unit_master.unit_id) 
                  INNER JOIN floors_master ON (floors_master.floor_id=unit_master.floor_id) 
                  INNER JOIN block_master ON (block_master.block_id=floors_master.block_id) 
                  WHERE users_master.delete_status=0 AND users_master.society_id='$society_id' AND users_master.device='' AND users_master.user_status=1 $blockAppendQueryUser) AS notLogin_count,
              (SELECT COUNT(*) FROM unit_master 
                  INNER JOIN users_master ON (users_master.unit_id=unit_master.unit_id) 
                  INNER JOIN floors_master ON (floors_master.floor_id=unit_master.floor_id) 
                  INNER JOIN block_master ON (block_master.block_id=floors_master.block_id) 
                  WHERE users_master.delete_status=0 AND users_master.society_id='$society_id' AND users_master.device='' AND users_master.user_status=2 $blockAppendQueryUser) AS notLoginAccess_count ,
              (SELECT COUNT(*) FROM unit_master 
                  INNER JOIN users_master ON (users_master.unit_id=unit_master.unit_id) 
                  INNER JOIN floors_master ON (floors_master.floor_id=unit_master.floor_id) 
                  INNER JOIN block_master ON (block_master.block_id=floors_master.block_id) 
                  WHERE users_master.delete_status=0 AND users_master.society_id='$society_id' AND users_master.device='ios' AND users_master.user_status=1 $blockAppendQueryUser) AS ios_user_count ,
              (SELECT COUNT(*) FROM users_master
                  INNER JOIN unit_master ON (unit_master.unit_id=users_master.unit_id)
                  INNER JOIN block_master ON (block_master.block_id=unit_master.block_id)
                  WHERE users_master.member_status=0 AND users_master.user_status=0 AND users_master.society_id='$society_id' $blockAppendQueryUser
              ) AS nCount,
              (SELECT COUNT(*) FROM attendance_master
                  INNER JOIN users_master ON (users_master.user_id=attendance_master.user_id)
                  WHERE attendance_master.society_id='$society_id' AND attendance_master.attendance_date_start = CURRENT_DATE() $blockAppendQueryUser
              ) AS attendance_count ,
              (SELECT COUNT(*) FROM users_master
                  WHERE society_id='$society_id' AND delete_status=0 AND active_status=0 AND is_attendance_mandatory=0 $blockAppendQueryUser
              ) AS user_attendance_count,
                
              (SELECT COUNT(leave_master.user_id) FROM leave_master
                  INNER JOIN users_master ON (users_master.user_id=leave_master.user_id)
                  WHERE leave_master.society_id='$society_id' AND leave_master.leave_start_date = CURRENT_DATE() AND leave_master.leave_day_type=0 AND leave_master.leave_status=1 $blockAppendQueryUser
              ) AS leave_count,
              (SELECT COUNT(*) FROM attendance_master
                  INNER JOIN users_master ON (users_master.user_id=attendance_master.user_id)
                  WHERE attendance_master.society_id='$society_id' AND attendance_master.attendance_date_start = CURRENT_DATE() AND attendance_master.late_in=1 $blockAppendQueryUser
              ) AS totalLetInEmployee,
              (SELECT COUNT(*) FROM attendance_master
                  INNER JOIN users_master ON (users_master.user_id=attendance_master.user_id)
                  WHERE attendance_master.society_id='$society_id' AND attendance_master.attendance_date_start = CURRENT_DATE() AND attendance_master.attendance_status=0 $blockAppendQueryUser 
              ) AS totalPendingAttendance,
              (SELECT COUNT(*) FROM attendance_master
                  INNER JOIN users_master ON (users_master.user_id=attendance_master.user_id)
                  WHERE attendance_master.society_id='$society_id' AND attendance_master.attendance_date_end = CURRENT_DATE() AND attendance_master.early_out=1 $blockAppendQueryUser
              ) AS totalEarlyOutEmployee
                  ";

                  
    if ($adminData['role_id']==1) {
      $sql02 .= ", (SELECT COUNT(*) FROM bms_admin_master
                  INNER JOIN society_master ON (society_master.society_id=bms_admin_master.society_id)
                  WHERE bms_admin_master.role_id!=2) AS management_count";

    } else {
      $sql02 .= ", (SELECT COUNT(*) FROM role_master
                    INNER JOIN bms_admin_master ON (bms_admin_master.role_id=role_master.role_id)
                    INNER JOIN society_master ON (society_master.society_id=bms_admin_master.society_id)
                    WHERE bms_admin_master.society_id='$society_id' AND role_master.role_id!=1 AND role_master.role_id!=2 AND bms_admin_master.user_type=0 AND bms_admin_master.admin_active_status=0) AS management_count 
                    ";
    }
      
    $dashboardCount = $d->executeSql($sql02,'row_array');
    //print_r($sql02);die();
   ?>


    <div class="row mt-3">
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="branches">
            <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary"><?php echo $xml->string->blocks; ?></p>
                      <h4 class="my-1"><?php echo $dashboardCount['block_count'];//d->count_data_direct("block_id","block_master","society_id='$society_id' $blockAppendQuery"); ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/Facilitiesxxxhdpi.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="departments">
            <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary"> <?php echo $xml->string->floors; ?></p>
                      <h4 class="my-1"><?php echo $dashboardCount['floor_count'];//$d->count_data_direct("floor_id","floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.society_id='$society_id' $blockAppendQuery"); ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/Social-intxxxhdpi.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="companyEmployees">
            <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary"><?php echo $xml->string->units; ?></p>
                      <h4 class="my-1"><?php echo $totalUnits=$dashboardCount['user_count'];//$d->count_data_direct("user_id","users_master,floors_master,block_master","block_master.block_id=floors_master.block_id AND  users_master.floor_id=floors_master.floor_id AND users_master.society_id='$society_id' AND users_master.delete_status=0  $blockAppendQuery"); ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/Members_1xxxhdpi.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </a>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="companyAdmins">
              <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary"><?php echo $xml->string->committee; ?></p>
                      <h4 class="my-1"><?php 
                //IS_1189
                 /*if ($adminData['role_id']==1) {
            $q=$d->select("bms_admin_master,society_master","society_master.society_id=bms_admin_master.society_id AND bms_admin_master.role_id!=2","");
            } else {
              $q=$d->select("role_master,bms_admin_master,society_master","bms_admin_master.role_id=role_master.role_id AND society_master.society_id=bms_admin_master.society_id  AND bms_admin_master.society_id='$society_id' AND role_master.role_id!=1 AND role_master.role_id!=2 AND bms_admin_master.user_type=0 AND bms_admin_master.admin_active_status=0","");
            }
              echo mysqli_num_rows($q);*/
              echo $dashboardCount['management_count'];
               // echo $d->count_data_direct("admin_id","bms_admin_master","society_id='$society_id'"); ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/Visitors-mgmtxxxhdpi.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

           
            </a>
        </div>
      
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="events">
             <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary"><?php echo $xml->string->events; ?></p>
                      <h4 class="my-1"><?php echo $dashboardCount['event_count'];//$d->count_data_direct("event_id","event_master","society_id='$society_id' "); 
                      ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/Events_1xxxhdpi.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            </a>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="holidays">

              <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary">Holidays</p>
                      <h4 class="my-1"><?php
                      
                       echo $dashboardCount['holiday_count'];//$d->count_data_direct("holiday_id","holiday_master","society_id='$society_id' AND DATE_FORMAT(holiday_start_date, '%Y')='$currentYear'"); ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/holiday.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="shiftTiming">
            <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary">Shift</p>
                      <h4 class="my-1"><?php echo $dashboardCount['shift_count'];//$d->count_data_direct("facility_id","facilities_master","facility_active_status='0'"); ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/attendance_tracker.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="itemDetails">
              <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary">Assets</p>
                      <h4 class="my-1"><?php 
               
                echo $dashboardCount['assets_count'];//$d->count_data_direct("assets_id","assets_item_detail_master","society_id='$society_id' AND status=0");
                
                 ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/assets_management.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            
            </a>
        </div>
     
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="viewSurvey">
              <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary"><?php echo $xml->string->survey; ?></p>
                      <h4 class="my-1"><?php echo $dashboardCount['survey_count'];//$d->count_data_direct("survey_id","survey_master","society_id='$society_id' "); ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/Survey_1xxxhdpi.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            
            </a>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="vendors">
              <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary"><?php echo $xml->string->vendors; ?></p>
                      <h4 class="my-1"> <?php echo $dashboardCount['vendor_count'];//$d->count_data_direct("service_provider_users_id","local_service_provider_users",""); ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/Seervice-providerxxxhdpi.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            
            </a>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="companyEmployeeReport?bId=&dId=&device=android">
              <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary"><?php echo $xml->string->android; ?> <?php echo $xml->string->users; ?></p>
                      <h4 class="my-1"> <?php echo $totalAndroid= $dashboardCount['android_user_count'];//$totalAndroid=$d->count_data_direct("user_id","unit_master,users_master,block_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.device='android' AND users_master.user_status=1 $blockAppendQueryUser");

                /*$NotLogin=$d->count_data_direct("user_id","unit_master,users_master,block_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.device=''  AND users_master.user_status=1 $blockAppendQueryUser");

                $NotLoginAccess=$d->count_data_direct("user_id","unit_master,users_master,block_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.device='' AND users_master.user_status=2 $blockAppendQueryUser");*/ 

                  
                  $NotLogin = $dashboardCount['notLogin_count'];
                  $NotLoginAccess = $dashboardCount['notLoginAccess_count'];
                ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/android.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </a>
        </div>
        <div class="col-6 col-lg-4 col-md-4 col-xl-3">
            <a href="companyEmployeeReport?bId=&dId=&device=ios">
              <div class="col">
              <div class="card radius-10 border-1px">
                <div class="p-2">
                  <div class="d-flex align-items-center">
                    <div>
                      <p class="mb-0 text-primary"><?php echo $xml->string->ios; ?> <?php echo $xml->string->users; ?></p>
                      <h4 class="my-1"> <?php 
                echo $totalIos= $dashboardCount['ios_user_count'];
                //echo $totalIos=$d->count_data_direct("user_id","unit_master,users_master,block_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND  users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id' AND users_master.device='ios'  AND users_master.user_status=1 $blockAppendQueryUser");
                 ?></h4>
                    </div>
                    <div class="widgets-icons bg-light-success text-success ms-auto"><img class="myIcon lazyload" src="../img/newFav.png" data-src="../img/app_icon/apple.png"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            </a>
        </div>
      </div>
    
      
      <!-- Employee Birthday section start -->
      <?php
        $today_full_date = date("m-d");
        $fifteen_days = date("m-d",strtotime("15 Days"));
        $currentDay = date('m-d');
        $nextDay = date('m-d', strtotime('+1 day'));
        $q = $d->selectRow("user_first_name,user_id,user_profile_pic,member_date_of_birth,user_full_name,user_designation,wedding_anniversary_date","users_master","delete_status = 0 AND user_status = '1' AND active_status = '0' AND (DATE_FORMAT(member_date_of_birth, '%m-%d') BETWEEN '$today_full_date' AND '$fifteen_days')","ORDER BY Month(member_date_of_birth) ASC,day( member_date_of_birth) ASC");
        $bd = [];
        while ($data = $q->fetch_assoc())
        {
          $bd[] = $data;
        }
        $wa = $d->selectRow("user_first_name,user_id,user_profile_pic,user_full_name,user_designation,wedding_anniversary_date","users_master","delete_status = 0 AND user_status = '1' AND active_status = '0' AND (DATE_FORMAT(wedding_anniversary_date, '%m-%d') BETWEEN '$today_full_date' AND '$fifteen_days')","ORDER BY Month(wedding_anniversary_date) ASC,day( wedding_anniversary_date) ASC");
        $wd = [];
        while ($wad = $wa->fetch_assoc())
        {
          $wd[] = $wad;
        }

        $jd = $d->selectRow("users_master.user_id,users_master.user_first_name,user_profile_pic,user_full_name,user_designation,joining_date","users_master JOIN user_employment_details ON user_employment_details.user_id = users_master.user_id","delete_status = 0 AND user_status = '1' AND active_status = '0' AND (DATE_FORMAT(joining_date, '%m-%d') BETWEEN '$today_full_date' AND '$fifteen_days')","ORDER BY Month(joining_date) ASC,day( joining_date) ASC");
        $j = [];
        while ($jdd = $jd->fetch_assoc())
        {
          $j[] = $jdd;
        }
        if(mysqli_num_rows($q) > 0 || mysqli_num_rows($wa) > 0 || mysqli_num_rows($jd) > 0)
        {
          $ends = array('th','st','nd','rd','th','th','th','th','th','th');
      ?>
      <div class="row">
        <div class="col-md-10 ">
          <h6 class="mb-0"> Birthdays, Work Anniversary & Wedding Anniversary </h6>
        </div>
        <div class="col-md-2 text-right">
          <a class="p-1 nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" href="celebrationsList"><i class="fa fa-list"></i></a>
        </div>
      </div>
        
      <div class="row">
          <?php
          foreach($bd AS $key => $data)
          {
          ?>
          <div class="card col-lg-2 col-md-2 col-6 col-xl-2 radius-10 border-1px p-1 m-1">
              <div class=" ">
                <?php
                if(date("m-d", strtotime($data['member_date_of_birth'])) == $currentDay)
                { ?>
                <a href="javascript:void(0)" onclick="birthdayNotification(<?php echo $data['user_id']; ?>)">
                <?php
                } 
                ?>
                <div class="text-center">
                    <?php if(file_exists('../img/users/recident_profile/'.$data['user_profile_pic']) && $data['user_profile_pic']!="user_default.png" && $data['user_profile_pic']!="user.png") { ?>
                    
                    <img class="myIcon lazyload rounded-circle" onerror="this.onerror=null; this.src='../img/users/recident_profile/user_default.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $data['user_profile_pic']; ?>">
                    <?php
                    }
                    else
                    {
                    ?>
                    <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
                      <?php $shortChar=$d->get_sort_name($data['user_full_name']); echo trim($shortChar); ?>
                    </div>
                    <?php
                    }
                    ?>
                    <!-- <img class="myIcon lazyload mt-2" src="img/icons/cake.png"> -->
                  
                  <div class="w-100 pl-1">
                    <span class="mb-0 text-dark" style="font-size: 14px;"><b><?php echo $data['user_first_name']; ?>-<?php echo custom_echo($data['user_designation'],17); ?></b></span>
                    <p class="text-dark mb-0">
                      <?php if(date("m-d", strtotime($data['member_date_of_birth'])) == $currentDay){
                      echo "Today";
                      }else if(date("m-d", strtotime($data['member_date_of_birth'])) == $nextDay){
                      echo "Tomorrow";
                      } ?>
                    </p>
                    <?php
                    $dateOfbirth = $data['member_date_of_birth'];
                    $wed = date("d",strtotime($data['member_date_of_birth']));
                    $curd = date("d");
                    $totd = $wed - $curd;
                    $diff = date_diff(date_create($dateOfbirth), date_create(date("Y-m-d")));
                    if($totd > 0)
                    {
                      $number = $diff->format('%y') + 1;
                    }
                    else
                    {
                      $number = $diff->format('%y');
                    }
                    if (($number %100) >= 11 && ($number%100) <= 13)
                    {
                       $abbreviation = $number. 'th';
                    }
                    else
                    {
                       $abbreviation = $number. $ends[$number % 10];
                    }
                    ?>
                    <span class="text-dark"><?php echo "(".$abbreviation." birthday <i class='fa fa-birthday-cake' aria-hidden='true'></i>)"; ?> </span>
                  </div>
                </div>
                <?php
                if(date("m-d", strtotime($data['member_date_of_birth'])) == $currentDay) { ?>
                </a> <?php }   ?>
              </div>
          </div>
          <?php }
          
          foreach($wd AS $key => $data)
          {
          ?>
          <div class="card col-lg-2 col-md-2 col-6 col-xl-2 radius-10 border-1px p-1 m-1">
            <div class="text-center">
                <?php
                if(date("m-d", strtotime($data['wedding_anniversary_date'])) == $currentDay)
                {
                ?>
                <a href="javascript:void(0)" onclick="weddingNotification(<?php echo $data['user_id']; ?>)" >
                <?php }  ?>
                    <div class="text-success">
                      <?php if(file_exists('../img/users/recident_profile/'.$data['user_profile_pic']) && $data['user_profile_pic']!="user_default.png" && $data['user_profile_pic']!="user.png") { ?>
                    
                      <img class="myIcon lazyload rounded-circle" onerror="this.onerror=null; this.src='../img/users/recident_profile/user_default.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $data['user_profile_pic']; ?>">
                      <?php
                      }
                      else
                      {
                      ?>
                      <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
                        <?php $shortChar=$d->get_sort_name($data['user_full_name']); echo trim($shortChar); ?>
                      </div>
                      <?php
                      }
                      ?>
                    </div>
                    <div class="w-100 pl-1">
                     
                      <span class="mb-0 text-dark" style="font-size: 14px;"><b><?php echo $data['user_first_name']; ?>-<?php echo custom_echo($data['user_designation'],17); ?></b></span>
                      <p class="text-dark mb-0">
                        <?php if(date("m-d", strtotime($data['wedding_anniversary_date'])) == $currentDay){
                        echo " Today";
                        }else if(date("m-d", strtotime($data['wedding_anniversary_date'])) == $nextDay){
                        echo "Tomorrow";
                        } ?>
                      </p>
                      <?php
                      $dateOfWedding = $data['wedding_anniversary_date'];
                      $wed = date("d",strtotime($data['wedding_anniversary_date']));
                      $curd = date("d");
                      $totd = $wed - $curd;
                      $diff = date_diff(date_create($dateOfWedding), date_create(date("Y-m-d")));
                      if($totd > 0)
                      {
                        $number = $diff->format('%y') + 1;
                      }
                      else
                      {
                        $number = $diff->format('%y');
                      }
                      if (($number %100) >= 11 && ($number%100) <= 13)
                      {
                         $abbreviation = $number. 'th';
                      }
                      else
                      {
                         $abbreviation = $number. $ends[$number % 10];
                      }
                      ?>
                      <span class="text-dark"><?php echo "(".$abbreviation." Wedding Anni. <i class='fa fa-ravelry' aria-hidden='true'></i>)";  ?></span>
                    </div>
                  <?php
                  if(date("m-d", strtotime($data['wedding_anniversary_date'])) == $currentDay) { ?>
                  </a> <?php }  ?>
            </div>
          </div>
          <?php } 
            foreach($j AS $key => $data)
            {
            ?>
            <div class="card col-lg-2 col-md-2 col-6 col-xl-2 radius-10 border-1px p-1 m-1">
                <div class="text-center">
                  <?php
                  if(date("m-d", strtotime($data['joining_date'])) == $currentDay)
                  { ?>
                  <a href="javascript:void(0)" onclick="workNotification(<?php echo $data['user_id']; ?>)" class="">
                  <?php }  ?>
                    <div class="text-success">
                      <?php if(file_exists('../img/users/recident_profile/'.$data['user_profile_pic']) && $data['user_profile_pic']!="user_default.png" && $data['user_profile_pic']!="user.png") { ?>
                    
                      <img class="myIcon lazyload rounded-circle" onerror="this.onerror=null; this.src='../img/users/recident_profile/user_default.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $data['user_profile_pic']; ?>">
                      <?php
                      }
                      else
                      {
                      ?>
                      <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
                        <?php $shortChar=$d->get_sort_name($data['user_full_name']); echo trim($shortChar); ?>
                      </div>
                      <?php
                      }
                      ?>
                    </div>
                  <div class="w-100 pl-1">
                    
                    <span class="mb-0 text-dark" style="font-size: 14px;"><b><?php echo $data['user_first_name']; ?>-<?php echo custom_echo($data['user_designation'],17); ?></b></span>
                    <p class="text-dark mb-0">
                      <?php if(date("m-d", strtotime($data['joining_date'])) == $currentDay){
                      echo "Today";
                      }else if(date("m-d", strtotime($data['joining_date'])) == $nextDay){
                      echo "Tomorrow";
                      } ?>
                    </p>
                    <?php
                    $dateOfJoin = $data['joining_date'];
                    $wed = date("d",strtotime($data['joining_date']));
                    $curd = date("d");
                    $totd = $wed - $curd;
                    $diff = date_diff(date_create($dateOfJoin), date_create(date("Y-m-d")));
                    if($totd > 0)
                    {
                      $number = $diff->format('%y') + 1;
                    }
                    else
                    {
                      $number = $diff->format('%y');
                    }
                    if (($number %100) >= 11 && ($number%100) <= 13)
                    {
                       $abbreviation = $number. 'th';
                    }
                    else
                    { 
                       $abbreviation = $number. $ends[$number % 10];
                    }
                    ?>
                    <span class="text-dark"><?php  if ($number==0) { echo "(New Joining <i class='fa fa-briefcase' aria-hidden='true'></i>)"; } else { echo "(".$abbreviation." Work Anni. <i class='fa fa-briefcase' aria-hidden='true'></i>)";  } ?></span>
                  </div>
               
                <?php
                if(date("m-d", strtotime($data['joining_date'])) == $currentDay)
                {
                ?>
                </a>
                <?php } 
                ?>
                </div>
            </div>
            <?php }  ?>
      </div>
        
      <?php } ?>
      <!-- Employee Birthday section end -->
     

      <?php 
        $nq=$d->select("users_master,unit_master,block_master,floors_master","floors_master.floor_id=unit_master.floor_id AND users_master.delete_status=0 AND block_master.block_id=unit_master.block_id AND users_master.unit_id=unit_master.unit_id AND users_master.member_status=0 AND users_master.user_status=0 AND users_master.society_id='$society_id' $blockAppendQueryUser","ORDER BY users_master.user_id DESC LIMIT 6");
        
        $toatlPendingPrimary = $dashboardCount['nCount'];
        if (mysqli_num_rows($nq)>0) {
      ?>
      <div class="row mt-2 mb-2">
         <div class="col-lg-6 col-6">
          <h6 class="mb-0"><?php echo $xml->string->new; ?> <?php echo $xml->string->members; ?> <?php echo $xml->string->request; ?></h6>
         </div>
         <?php if ($toatlPendingPrimary>6) { ?>
         <div class="col-lg-6 col-6">
           <a class="btn btn-danger btn-sm pull-right" href="pendingUser?pendingUser=yes"> <?php echo $xml->string->view; ?> <?php echo $xml->string->pending; ?> <?php echo $xml->string->request; ?> (<?php echo $toatlPendingPrimary; ?>) </a>
          </div>
        <?php } ?>
       </div>
      <div class="row">
        <?php 
        while ($newUserData=mysqli_fetch_array($nq)) {
          $user_full_name= $newUserData['user_full_name'];
          $shortChar=$d->get_sort_name($user_full_name); 
         ?>
        <div class="col-lg-2 col-md-4 col-6">
         <a href="pendingUser?id=<?php echo $newUserData['user_id']; ?>">
          <div class="card pt-3 text-center">
          
          <?php if(file_exists('../img/users/recident_profile/'.$newUserData['user_profile_pic']) && $newUserData['user_profile_pic']!="user_default.png" && $newUserData['user_profile_pic']!="user.png") { ?> 
          <img height="40" width="40" src="img/user.png" onerror="this.src='img/user.png'" data-src="../img/users/recident_profile/<?php echo $newUserData['user_profile_pic']; ?>" class="rounded-circle shadow lazyload m-auto " alt="Card image cap"/>
          <?php } else {  ?>
            <div style="line-height: 40px; margin: auto !important;font-size: 18px;width: 40px;height: 40px;" class="profile-image-name">
              <?php echo trim($shortChar); ?>
            </div>
          <?php } ?>
          <div class="p-2 text-center " style="overflow: hidden;">
            <span class="mb-0 text-dark" style="font-size: 14px;"><b><?php custom_echo($newUserData['user_full_name'],15); ?></b></span>
            
            <?php if($newUserData['user_mobile']>0) { ?>
            <p class="text-dark mb-0"><?php custom_echo($newUserData['country_code'].' '.$newUserData['user_mobile'],16); ?></p>
            <span><?php custom_echo($newUserData['user_designation'],17);?></span><br>
            <?php } ?>
            <span class='badge bg-primary text-white'><?php  custom_echo($newUserData['floor_name'],15); ?>-<?php  custom_echo($newUserData['block_name'],15);?></span>
           
          </div>
          </div>


        
          </a>
        </div>
        <?php }?>
      </div>
      <?php } 
       
      ?>
     
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="card">
            <div class="card-header">
                <?php echo $xml->string->unit; ?> <?php echo $xml->string->status; ?>
                <div class="card-action">
                 <?php 
                  $totalPresentEmployee = $dashboardCount['attendance_count'];
                  $totalLeaveEmployee = $dashboardCount['leave_count'];
                  $totalAbsentEmployee= ($dashboardCount['user_attendance_count']-$dashboardCount['attendance_count'])-$totalLeaveEmployee;
                  ?>
                 </div>
                </div>
                <div class="card-body">
                  <div id="unitStatus" ></div>
                </div>
          </div>
        </div>

        <div class="col-12 col-lg-6">
          <div class="card">
            <div class="card-header">
                <?php echo $xml->string->unit; ?> Attendance <?php echo $xml->string->status; ?>
                <div class="card-action">
                 <?php 
                
                $totalLetInEmployee = $dashboardCount['totalLetInEmployee'];
                $totalPendingAttendance = $totalTenants = $dashboardCount['totalPendingAttendance'];
                $totalEarlyOutEmployee = $dashboardCount['totalEarlyOutEmployee'];

                  ?>
                 </div>
                </div>
                <div class="card-body">
                  <div id="userLateInAndPendingAttendance" ></div>
                </div>
          </div>
        </div>

        


        <div class="col-12 col-lg-6">
          <div class="card">
            <div class="card-header">
                <?php echo $xml->string->app_name; ?> <?php echo $xml->string->users; ?> 
                <div class="card-action">
                 </div>
                </div>
                <div class="card-body">
                  <div id="appUsageCOunter"></div>
                </div>
          </div>
        </div>
        
      </div>

      
      <?php   //IS_11234 ?>
      <!--End Dashboard Content-->
    </div>
    <!-- End container-fluid-->
</div><!--End content-wrapper-->

<div class="modal fade" id="birthNotiModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white modal-title-noti">Send Birthday Notification</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
          <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                <form id="birthdayNotificationForm" action="controller/statusController.php" enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Title <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12">
                          <input type="text" class="form-control" id="title" name="title" value="Happy Birthday">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Description <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12">
                          <textarea class="form-control" id="description" name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-footer text-center">
                      <input type="hidden" name="status" value="birthdayNotification">
                      <input type="hidden" name="celebrationType" id="celebrationType">
                      <input type="hidden" name="user_id" id="user_id">
                      <button type="submit" class="btn btn-success">Send </button>
                    </div>
                </form>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<div class="modal fade" id="birthWeddingJoinModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Celebrations List Of Upcoming 15 Days</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
          <div class="card-body">
            <h5>Birthdays</h5>
            <div class="row birthCelebration">
            </div>
            <h5 class="mt-3">Wedding Anniversary</h5>
            <div class="row weddingCelebration">
            </div>
            <h5 class="mt-3">Work Anniversary</h5>
            <div class="row workCelebration">
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<?php
   $q111=$d->select("reminder_master","society_id='$society_id' AND admin_id='$_COOKIE[bms_admin_id]' AND reminder_status=0","ORDER BY reminder_id DESC");
    $today11=date('Y-m-d');
    $cTime= date("H:i:s");
    $todayNew = strtotime("$today11 $cTime");
    $ru=1;
    while($dataReminder=mysqli_fetch_array($q111)){
      $expireReminder = strtotime($dataReminder['reminder_date']);
       if($todayNew > $expireReminder){
        $totalReminder= $ru++;
       }
    }


     $sq = $d->select("floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.society_id = '$society_id'");
        if(mysqli_num_rows($sq) > 0 ) {
          $optionArray = array();
          $optionArrayResult = array();
          while($sData2 = mysqli_fetch_array($sq)){
            array_push($optionArray,$sData2['floor_name'].'-'.$sData2['block_name']);
            $voteCount = $d->count_data_direct("user_id","users_master","floor_id = '$sData2[floor_id]' AND society_id = '$society_id' AND delete_status=0");
            array_push($optionArrayResult,$voteCount);
          }
          $empLabel = implode('","', $optionArray);
          $empLabel = '"'.$empLabel.'"';
          $labelsValue = implode(",", $optionArrayResult);
      }
?>
<!-- Chart JS -->

<?php } ?>