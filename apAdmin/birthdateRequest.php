<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-5">
                <h4 class="page-title">Birth Date Requests</h4>
            </div>
            <div class="col-sm-3 col-7">
                <div class="btn-group float-sm-right">
                    <a href="javascript:void(0)" onclick="approveRequest();" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-check"></i> Approve </a>
                    <a href="javascript:void(0)" onclick="rejectRequest();" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-times"></i> Reject </a>
                </div>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Old Birthdate</th>
                                        <th>New Birthdate</th>
                                        <th>Action</th>
                                        <th>Reject Reason</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $q = $d->selectRow("bcr.*,um.member_date_of_birth,um.user_full_name,bm.block_name,fm.floor_name","birthdate_change_request AS bcr JOIN users_master AS um ON um.user_id = bcr.user_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id");
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td class='text-center'>
                                            <?php
                                            if($birthdate_status == 0)
                                            {
                                            ?>
                                            <input type="checkbox" user-id="<?php echo $user_id; ?>" birth-date="<?php echo $new_birth_date; ?>" class="multiApproveRejectCheckbox" value="<?php echo $birthdate_change_request_id; ?>">
                                        <?php } ?>
                                        </td>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $user_full_name . " (" . $block_name . " - " . $floor_name . ")"; ?></td>
                                        <td><?php echo $member_date_of_birth; ?></td>
                                        <td><?php echo $new_birth_date; ?></td>
                                        <td>
                                            <?php
                                            if($birthdate_status == 0)
                                            {
                                            ?>
                                                <button type="button" class="btn btn-success btn-sm" onclick="approveBirthdateRequest(<?php echo $birthdate_change_request_id ?>,<?php echo $user_id ?>,'<?php echo $new_birth_date ?>')"><i class="fa fa-check"></i></button>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="rejectBirthdateRequest(<?php echo $birthdate_change_request_id ?>)"><i class="fa fa-times"></i></button>

                                            <?php
                                            }
                                            elseif($birthdate_status == 1)
                                            {
                                            ?>
                                            <span class="text-success">Approved</span>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <span class="error">Rejected</span>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $reject_reason; ?></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="rejectRequestModal">
        <div class="modal-dialog">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Reject Birth Date Request Reason</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="rejectBirthdateRequestForm" method="POST" action="controller/userController.php" enctype="multipart/form-data">
                        <div class="row form-group">
                            <label class="col-sm-3 form-control-label">Reject Reason <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" maxlength="200" required name="reject_reason" id="reject_reason"></textarea>
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" name="csrf" id="edit_csrf">
                            <input type="hidden" name="birthdate_change_request_id" id="birthdate_change_request_id">
                            <input type="hidden" name="rejectBirthdateRequest" value="rejectBirthdateRequest">
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>