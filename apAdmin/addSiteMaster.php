<?php error_reporting(0);
$sId = (int)$_REQUEST['sId'];
$sitemanager = array();
$Procurement = array();
$Finance = array();
if (isset($sId) && $sId > 0) {
    $q = $d->selectRow('*', "site_master", "site_id='$sId'");
    $sitedata = mysqli_fetch_assoc($q);
    $q = $d->selectRow('site_manager_master.*,users_master.user_id,users_master.block_id,users_master.floor_id', "site_manager_master,users_master", "users_master.user_id = site_manager_master.site_manager_id AND site_manager_master.site_id='$sId'");
    while($siteEmpdata = mysqli_fetch_assoc($q)){
       $bId = $siteEmpdata ['block_id'];
       $dId = $siteEmpdata ['floor_id'];
       ////-site manager,1-Procurement,2-Finance	
      
       if($siteEmpdata['manager_type']==0){
        
            array_push($sitemanager,$siteEmpdata['site_manager_id']);
       }
       else if($siteEmpdata['manager_type']==1){
        array_push($Procurement,$siteEmpdata['site_manager_id']);
       }
       else
       {
        array_push($Finance,$siteEmpdata['site_manager_id']);
       }
    }
   
} else {
    $q = $d->selectRow('*', "society_master", "society_id='$society_id'");
    $data = mysqli_fetch_assoc($q);
    $user_latitud = $data['society_latitude'];
    $user_longitude = $data['society_longitude'];
}

?>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row pt-2 pb-2">
            <div class="col-md-12">
                <h4 class="page-title">Add Site Master</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                    <form id="addSiteForm" action="controller/siteController.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Branch <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select name="br_id" id="br_id" class="form-control single-select updateFrom " onchange="getSiteManagerFloorByBLock(this.value)">
                                                <option value="">Select Branch</option>
                                                <?php
                                                $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQueryOnly");
                                                while ($blockData = mysqli_fetch_array($qb)) {
                                                    
                                                ?>
                                                    <option <?php if ($bId == $blockData['block_id']) {
                                                                echo 'selected';
                                                            } ?> value="<?php echo  $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?>
                                                    </option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Department <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <select name="floor_id" id="floor_id" class="form-control single-select updateFrom" onchange="getUserByFloorId(this.value)">
                                                    <option value=""> Select Department</option>
                                                    <?php if(isset($sId)){
                                                        $qb = $d->select("floors_master", "block_id='$bId'");
                                                        while ($dpData = mysqli_fetch_array($qb)) {
                                                       ?>
                                                            <option <?php if ($dId == $dpData['floor_id']) {
                                                                        echo 'selected';
                                                                    } ?> value="<?php echo  $dpData['floor_id']; ?>"><?php echo $dpData['floor_name']; ?>
                                                            </option>
                                                        <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Site Manager <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                        <select name="site_manager_id[]" id="user_id" multiple class="form-control  updateFrom multiple-select">
                                            <option >Select </option>
                                            <?php
                                            if(isset($sId) && $sId>0){
                                            $qd=$d->select("users_master","society_id='$society_id' AND floor_id = $dId");  
                                                    while ($userData=mysqli_fetch_array($qd)) {
                                            ?>
                                             <option <?php if(in_array($userData['user_id'], $sitemanager)){echo "selected"; } ?> value="<?php echo  $userData['user_id']; ?>" ><?php echo $userData['user_full_name']; ?></option>
                                            <?php } }
                                            ?>

                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Procurement Manager<span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                        <select name="procurement_id[]"  multiple class="form-control uId updateFrom multiple-select">
                                            <option >Select </option>
                                            <?php
                                            if(isset($sId) && $sId>0){
                                            $qd=$d->select("users_master","society_id='$society_id' AND floor_id = $dId");  
                                                    while ($userData=mysqli_fetch_array($qd)) {
                                            ?>
                                             <option <?php if(in_array($userData['user_id'], $Procurement)){echo "selected"; } ?> value="<?php echo  $userData['user_id']; ?>" ><?php echo $userData['user_full_name']; ?></option>
                                            <?php } }
                                            ?>

                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Finance Manager <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                        <select name="finance_manager_id[]" multiple class="form-control uId updateFrom multiple-select">
                                            <option >Select </option>
                                            <?php
                                            if(isset($sId) && $sId>0)
                                            $qd=$d->select("users_master","society_id='$society_id' AND floor_id = $dId");  
                                                    while ($userData=mysqli_fetch_array($qd)) {
                                            ?>
                                             <option <?php if(in_array($userData['user_id'], $Finance)){echo "selected"; } ?> value="<?php echo  $userData['user_id']; ?>" ><?php echo $userData['user_full_name']; ?></option>
                                            <?php } 
                                            ?>

                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Site Name <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                             <input type="text" required="" name="site_name" id="site_name" value="<?php if(isset($sitedata) && $sitedata['site_name'] !=""){ echo $sitedata['site_name']; } ?>" class="form-control restInput ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Site Address <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <textarea required="" name="site_address" id="site_address" class="form-control restInput "><?php if(isset($sitedata) && $sitedata['site_address'] !=""){ echo $sitedata['site_address']; } ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-sm-12 col-form-label">Site Decription <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 ">
                                            <textarea name="site_description" id="site_description" class="form-control restInput"><?php if(isset($sitedata) && $sitedata['site_description'] !=""){ echo $sitedata['site_description']; } ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div class="form-footer text-center">
                                        <input type="hidden" id="site_id" name="site_id" value="<?php if(isset($sId) && $sId>0){ echo $sId; } ?>">
                                        <input type="hidden" id="user_id_old" name="user_id_old" value="">
                                        <input type="hidden" id="floor_id_old" name="floor_id_old" value="">
                                        <input type="hidden" name="addSite" value="addSite">
                                        <button id="addSiteBtn" type="submit" class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i><?php if(isset($sId) && $sId>0){ echo "Update";} else { echo "Add"; } ?> </button>

                                        <button type="button" value="add" class="btn btn-danger cancel" onclick="resetSiteFrm();"><i class="fa fa-check-square-o"></i> Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function resetSiteFrm() {
        $('.restInput').val("");
        $('.updateFrom').val("");
        $('.updateFrom').select2();
    }
</script>

<style>
    .locationRangeDiv {
        display: none;
    }
    .UserlocationRangeDiv {
        display: none;

    }
</style>