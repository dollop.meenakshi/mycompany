<?php 

$dayNamePos = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'); 
$daykey = array_search( $_POST['dayname'], $dayNamePos);
?>
<div class="row fullDiv<?php echo $_POST['dayname'];?>">
	<div class="col-lg-6 mt-2 countDiv<?php echo $_POST['dayname'];?>">
	  <input type="text" readonly class="form-control appenddayCheck<?php echo $_POST['dayname'];?><?php echo $_POST['timeSlotNumber'];?>"   name="<?php echo $_POST['dayname'];?>_opening_time[]" placeholder="Opening Time">
	</div>
	<div class="col-lg-6 mt-2">
	  <input type="text" readonly class="form-control  appenddayCheckEnd<?php echo $_POST['dayname'];?><?php echo $_POST['timeSlotNumber'];?>"  name="<?php echo $_POST['dayname'];?>_closing_time[]" placeholder="Closing Time">
	</div>
	<?php if ($_POST['timeSlotNumber']==0) { ?>
	<div class="col-lg-3 mt-4" style="position: absolute;right: 0;margin-right: -130px;">
	  <a onclick="deleteTimeSlot('fullDiv<?php echo $_POST['dayname'];?>');" href="javascript:void(0)" > <i class="fa fa-trash-o"></i> Remove All</a>

	</div>
	<?php } ?>
</div>
