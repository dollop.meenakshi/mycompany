<?php error_reporting(0);
$pId = (int)$_REQUEST['pId'];
if ($pId > 0) {
    $q = $d->selectRow("product_master.*", 'product_master', "product_id = $pId");
    $data = mysqli_fetch_array($q);
    extract($data);
}

$q2 = $d->selectRow("product_variant_master.*", 'product_variant_master', "product_id=$pId AND variant_delete_status=0");
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">Product Details With Variant</h4>
            </div>
            <div class="col-sm-3 col-6 text-right">
                <a href="addSiteBtn" data-toggle="modal" data-target="#addVerientModal" onclick="buttonSettingForVerient()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
                <?php if(mysqli_num_rows($q2)>0){ ?>
                <a href="javascript:void(0)" onclick="DeleteAll('deleteProductVerient');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                       <div class="col-md-4 pt-4">
                            <span>Product Name: <?php echo $data['product_name']; ?></span>
                        </div>
                        <div class="col-md-4 pt-4">
                            <span>Alias Name: <?php echo $data['product_alias_name']; ?></span>
                        </div>
                        <div class="col-md-4 pt-4">
                            <span>Brand: <?php echo $data['product_brand']; ?> </span>
                        </div>
                    </div>
                    <div class="table-responsive pt-4">
                        <table id="example" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Sr. No.</th>
                                    <th>Varient Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="showFilterData" class="purchase_requirements">
                                <?php
                               
                                $counter = 1;
                                while ($Pdata = mysqli_fetch_array($q2)) {
                                ?>
                                    <tr>
                                        <td> 
                                            <input type="hidden" name="id" id="id" value="<?php echo $Pdata['product_variant_id']; ?>">
                                            <?php 
                                            
                                            $total = $d->count_data_direct("puchase_product_id", "purchase_product_master", "product_variant_id='$Pdata[product_variant_id]'");
                                            if($total==0){ ?>
                                            <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $Pdata['product_variant_id']; ?>">
                                            <?php }?>
                                            
                                    </td>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $Pdata['product_variant_name']; ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                 <input type="hidden" name="product_variant_id" value="<?php echo $Pdata['product_variant_id']; ?>">
                                                   <button type="button" onclick="editProductVariant(<?php echo $Pdata['product_variant_id']; ?>)" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button>
                                                <?php
                                                // print_r($Pdata['variant_active_status']);
                                                if ($Pdata['variant_active_status'] == "0") {
                                                    $status = "productVarientStatusDeactive";
                                                    $active = "checked";
                                                } else {
                                                    $status = "productVarientStatusActive";
                                                    $active = "";
                                                } ?>
                                                <input type="checkbox" <?php echo $active; ?> class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $Pdata['product_variant_id']; ?>','<?php echo $status; ?>');" data-size="small" />
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Row-->

</div>
<!-- End container-fluid-->

</div>
<!--End content-wrapper-->

<div class="modal fade" id="addVerientModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Add Variant</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="align-content: center;">
                <form id="addProductVerientFrm" action="controller/productVerientController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-4 col-form-label">Variant Name <span class="required">*</span></label>
                        <div class="col-sm-8" id="">
                            <input required="" type="product_variant_name" name="product_variant_name"  id="product_variant_name" class="form-control border restIn">
                        </div>
                    </div>
                    <div class="form-footer text-center">
                        <input type="hidden" name="addProductVerient" value="addProductVerient">
                        <input type="hidden" name="pr_id" id="pr_id" value="<?php echo $pId; ?>">
                        <input type="hidden" class="restIn" name="product_variant_id" id="product_variant_id" >
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-square-o btnUpAdd"></i>  </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- <script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script> -->
<script type="text/javascript">
    function popitup(url) {
        newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    }
    function buttonSettingForVerient(params) {
        $('.restIn').val('');
        $('.btnUpAdd').text('Add');
    }
</script>
<style>
    .hideupdate {
        display: none;
    }
</style>