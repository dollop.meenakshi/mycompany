<!-- <button id="rzp-button1">Pay with Razorpay</button> -->
<link rel="icon" href="../img/fav.png" type="image/png">
<title>MyCo Payment</title>
<style type="text/css">
    .ajax-loader {
      display: block;
      /*background-color: rgba(255,255,255,0.7);*/
      position:fixed; width:100%; height:100%;
      z-index: 10000000;
    }

    .ajax-loader img {
       margin-left: auto;
      margin-right: auto;
      margin-top: 250px;
      display: block;
    }


</style>
<body style="background-image: url(https://master.my-company.app/img/paymentbg.jpg);">
<div class="ajax-loader">
  <img src="../img/ajax-loader.gif" class="img-responsive" />
</div>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<form name='razorpayform' action="verify.php" method="POST">
    <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id">
    <input type="hidden" name="razorpay_signature"  id="razorpay_signature" >
</form>
<script>
// Checkout details as a json
var options = <?php echo $json?>;

/**
 * The entire list of Checkout fields is available at
 * https://docs.razorpay.com/docs/checkout-form#checkout-fields
 */
options.handler = function (response){
    document.getElementById('razorpay_payment_id').value = response.razorpay_payment_id;
    document.getElementById('razorpay_signature').value = response.razorpay_signature;
    console.log(response);
    document.razorpayform.submit();
};

// Boolean whether to show image inside a white frame. (default: true)
options.theme.image_padding = false;

options.modal = {
    ondismiss: function() {
        // console.log("This code runs when the popup is closed");
        window.location.href='buyPlan';
    },
    // Boolean indicating whether pressing escape key 
    // should close the checkout form. (default: true)
    escape: true,
    // Boolean indicating whether clicking translucent blank
    // space outside checkout form should close the form. (default: false)
    backdropclose: false
};

var rzp = new Razorpay(options);

// document.getElementById('rzp-button1').onclick = function(e){
    rzp.open();
    // e.preventDefault();
// }


</script>
    
</body>