<?php
$society_id=$_COOKIE['society_id'];
error_reporting(0);
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];

if (isset($dId) && $dId > 0) {
    $dptFilterQuery = " AND users_master.floor_id='$dId'";
  }
  if (isset($bId) && $bId > 0) {
    $blkFilterQuery = " AND users_master.block_id='$bId'";
  }
function parseToXML($htmlStr)
{
$xmlStr=str_replace('<','&lt;',$htmlStr);
$xmlStr=str_replace('>','&gt;',$xmlStr);
$xmlStr=str_replace('"','&quot;',$xmlStr);
$xmlStr=str_replace("'",'&#39;',$xmlStr);
$xmlStr=str_replace("&",'&amp;',$xmlStr);
return $xmlStr;
}
include 'common/object.php';


$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
// Select all the rows in the markers table
$bms_admin_id = $_COOKIE['bms_admin_id'];
$aq=$d->select("bms_admin_master,role_master","role_master.role_id=bms_admin_master.role_id AND bms_admin_master.admin_id='$bms_admin_id'");
$adminData= mysqli_fetch_array($aq);
$access_branchs=$adminData['access_branchs'];
$access_departments=$adminData['access_departments'];
$blockAryAccess = array();
$departmentAry = array();
$blockAryAccess = explode(",", $access_branchs);
$departmentAry = explode(",", $access_departments);

if ($access_branchs!='') {
  $blockids = join("','",$blockAryAccess); 
  if ($access_departments!='') {
    $departmentIds = join("','",$departmentAry); 
    $restictDepartment = " AND floors_master.floor_id IN ('$departmentIds')";
    $restictDepartmentUser = " AND users_master.floor_id IN ('$departmentIds')";
  } else {
    $restictDepartment = "";
    $restictDepartmentUser = "";
  }

  $blockAppendQuery = " AND block_master.block_id IN ('$blockids')";
  $blockAppendQueryUnit = " AND unit_master.block_id IN ('$blockids')";
  $blockAppendQueryFloor = " AND floors_master.block_id IN ('$blockids') $restictDepartment";
  $blockAppendQueryUser = " AND users_master.block_id IN ('$blockids') $restictDepartmentUser";
  $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

} else {
  $blockAppendQuery = "";
  $blockAppendQueryUnit = "";
  $blockAppendQueryFloor = "";
  $blockAppendQueryUser = "";
  $blockAppendQueryOnly="";
}

  $query= $d->select('block_master,floors_master,users_master',"block_master.block_id = users_master.block_id  AND floors_master.floor_id = users_master.floor_id  AND delete_status=0 AND last_tracking_longitude!='' $blockAppendQueryUser $blkFilterQuery $dptFilterQuery");
  header("Content-type: text/xml");
// echo mysqli_num_rows($query);
// Start XML file, echo parent node
echo '<markers>';
$ind=1;
// Iterate through the rows, printing XML nodes for each
while ($row = mysqli_fetch_array($query)){
  // echo "string";
   $user_id = $row['user_id'];

      
      if (file_exists('../img/users/recident_profile/'.$row['user_profile_pic'])) {
        $user_profile_pic = $row['user_profile_pic'];
        $image = '../img/users/recident_profile/'.$user_profile_pic;
      } else {
        $user_profile_pic = "user_default.png";
        $image = '../img/users/recident_profile/user_default.png';
      }
      $lat = $row['last_tracking_latitude'];
      $lon = $row['last_tracking_longitude'];
      $lstTimeView = "(".time_elapsed_string(date("d M Y h:i A", strtotime($row['last_tracking_location_time']))).')';
      $lastTime = $row['last_tracking_location_time'].' '. $lstTimeView;
      $last_tracking_address = $row['last_tracking_address'];
    	// $image = '../img/start.png';
     
     

  // Add to XML document node
  echo '<marker ';
  echo 'id="' . $ind++ . '" ';
  echo 'user_id="' . parseToXML($row['user_id']) . '" ';
  echo 'user_full_name="' . parseToXML($row['user_full_name']) . '" ';
  echo 'user_designation="' . parseToXML($row['user_designation']) . '" ';
  echo 'user_mobile="' . parseToXML($row['user_mobile']) . '" ';
  echo 'branch_name="' . parseToXML($row['block_name']) . '" ';
  echo 'department_name="' . parseToXML($row['floor_name']) . '" ';
  echo 'user_profile_pic="' . parseToXML($row['user_profile_pic']) . '" ';
  echo 'last_tracking_battery_status="' . parseToXML($row['last_tracking_battery_status'].' %') . '" ';
  echo 'last_tracking_gps_accuracy="' . parseToXML($row['last_tracking_gps_accuracy'].' M') . '" ';
  echo 'lastAddress="' . parseToXML($last_tracking_address) . '" ';
  echo 'lat="' . parseToXML($lat) . '" ';
  echo 'lon="' . parseToXML($lon) . '" ';
  echo 'lastTime="' . parseToXML($lastTime) . '" ';
  echo 'image="' . parseToXML($image) . '" ';
  echo '/>';
}

// End XML file
echo '</markers>';

?>