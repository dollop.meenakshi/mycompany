  <?php error_reporting(0);

  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year'] = $_REQUEST['month_year'];

  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Task</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="javascript:void(0)" onclick="DeleteAll();" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
     </div>
     <form action="" class="branchDeptFilter">
        <div class="row ">
          <?php include 'selectBranchDeptEmpForFilterAll.php' ?>

          <div class="col-md-2 col-6 form-group">
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-90 days'));} ?>">   
          </div>
          <div class="col-md-2 col-6 form-group">
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
          </div>          
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get">
            </div>
        </div>
      </form> 
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <!-- <th>#</th> -->
                        <th>Sr.No</th>                        
                        <th>Main Task</th>
                        <th>Task</th>
                        <th>Created By</th>
                        <th>Due Date</th>                      
                        <th>Assign To</th>                      
                        <th>Status</th>                      
                        <th>Complete Date</th>                      
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;

                       if (isset($dId) && $dId > 0) {
                        $deptFilterQuery = " AND users_master.floor_id='$dId'";
                      }

                      if(isset($bId) && $bId>0) {
                        $blockFilterQuery = " AND users_master.block_id='$bId'";
                      }

                      if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                        $dateFilterQuery = " AND DATE_FORMAT(task_master.task_created_date,'%Y-%m-%d') BETWEEN '$from' AND '$toDate'";
                      }
                      if (isset($uId) && $uId > 0) {
                        $userFilterQuery = "AND task_master.task_add_by='$uId'";
                      }

                        $q=$d->selectRow("task_master_main.task_master_main_name,task_master.*,users_master.user_designation,users_master.user_full_name AS added_by,UST.user_full_name AS assign_to ,UST.user_designation AS assign_to_designation","task_master LEFT JOIN users_master AS UST ON UST.user_id=task_master.task_assign_to,users_master,block_master,floors_master,task_master_main","task_master_main.task_master_main_id=task_master.task_master_main_id AND users_master.block_id=block_master.block_id AND floors_master.floor_id=users_master.floor_id  AND task_master.task_add_by=users_master.user_id AND task_master.society_id='$society_id' AND task_master.task_add_by_type=0 AND task_master.task_status=0 AND task_master.task_delete=0 $blockFilterQuery $deptFilterQuery $dateFilterQuery  $userFilterQuery ","ORDER BY task_master.task_id DESC");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <!-- <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['task_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['task_id']; ?>">                      
                        </td> -->
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['task_master_main_name']; ?></td>
                       <td><?php echo $data['task_name']; ?></td>
                       <td><?php echo $data['added_by'].' ('.$data['user_designation'].')'; ?></td>
                       <td><?php echo date("d M Y", strtotime($data['task_due_date'])); ?></td>
                       <td><?php echo $data['assign_to'].' ('.$data['assign_to_designation'].')'; ?></td>
                       <?php 
                        if($data['task_complete'] == 0){
                          $status = "Pending";
                          $statusClass = "badge-primary";
                        }
                        else if($data['task_complete'] == 1){
                            $status = "In Progress";
                            $statusClass = "badge-info";
                        }else{
                            $status = "Completed";
                            $statusClass = "badge-success";
                        }
                        ?>
                        <td><b><span class="badge badge-pill m-1 <?php echo $statusClass; ?>"><?php echo $status; ?></span></b></td>
                        <td><?php if($data['task_complete_date'] != '0000-00-00 00:00:00' && $data['task_complete_date'] != null){ echo date("d M Y h:i A", strtotime($data['task_complete_date']));} ?></td>
                        <td>
                            <div class="d-flex align-items-center">
                                <a href="taskDetail?id=<?php echo $data['task_id']?>" title="View Detail" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-eye"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
