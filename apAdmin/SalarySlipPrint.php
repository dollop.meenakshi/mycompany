<script src="assets/js/jquery.min.js"></script>

<?php include 'lib/dao.php'; ?>


<?php

error_reporting(0);
// session_cache_limiter(false);
session_start();
include_once 'lib/dao.php';
include_once 'lib/model.php';

$d = new dao();
$m = new model();
$con = $d->dbCon();
error_reporting(0);
$society_id = (int)$_REQUEST['sId'];
$salary_slip_id = (int)$_REQUEST['salId'];
$earnArray = array();
$contriArray = array();
$dedcutArray = array();
$q = $d->selectRow("society_master.*,salary_setting.*", "society_master LEFT JOIN salary_setting ON salary_setting.society_id=society_master.society_id", "society_master.society_id=$society_id");
$society_data = mysqli_fetch_assoc($q);

$q2 = $d->selectRow("salary_slip_master.*,
                    pre_by_admin.admin_name AS pre_by_admin_name,
                    floors_master.floor_name ,
                    block_master.block_name ,
                    users_master.user_full_name ,
                    user_bank_master.esic_no ,
                    user_bank_master.account_no ,
                    user_bank_master.bank_name ,
                    user_bank_master.pan_card_no ,
                    users_master.user_designation ,
                    salary.salary_group_id ,
                    checked_by_admin.admin_name AS checked_by_admin_name,
                    pub_by_admin.admin_name AS pub_by_admin_name
                    ","salary_slip_master
                    LEFT JOIN bms_admin_master  AS pre_by_admin ON pre_by_admin.admin_id = salary_slip_master.prepared_by
                    LEFT JOIN block_master ON block_master.block_id = salary_slip_master.block_id
                    LEFT JOIN users_master ON users_master.user_id = salary_slip_master.user_id
                    LEFT JOIN user_bank_master ON user_bank_master.user_id = salary_slip_master.user_id  AND user_bank_master.is_primary=1
                    LEFT JOIN floors_master ON floors_master.floor_id = salary_slip_master.floor_id
                    LEFT JOIN salary ON salary.salary_id = salary_slip_master.salary_id
                    LEFT JOIN bms_admin_master AS checked_by_admin ON checked_by_admin.admin_id = salary_slip_master.checked_by
                    LEFT JOIN bms_admin_master AS pub_by_admin ON pub_by_admin.admin_id = salary_slip_master.authorised_by
                    ","salary_slip_id=$salary_slip_id"
);
$salary_slip_data = mysqli_fetch_assoc($q2);
if ($salary_slip_data) {
  if (isset($salary_slip_data['salary_month_name']) && $salary_slip_data['salary_month_name'] != "") {
    $salary_month_name =  $salary_slip_data['salary_month_name'];
    $salary_group_id =  $salary_slip_data['salary_group_id'];
  }
  $q3 = $d->selectRow("salary_slip_sub_master.*,salary_common_value_master.show_employer_contribution", "salary_slip_sub_master, salary_common_value_master", "salary_slip_sub_master.salary_slip_id=$salary_slip_id AND salary_common_value_master.salary_group_id=$salary_group_id AND salary_common_value_master.salary_earning_deduction_id=salary_slip_sub_master.salary_earning_deduction_id");
  while ($salData = mysqli_fetch_array($q3)) {
    // echo "<pre>";
    // print_r($salData);
    if ($salData['earning_deduction_type_current'] == 0) {
      array_push($earnArray, $salData);
    } else {
      array_push($dedcutArray, $salData);
    }

    if ($salData['show_employer_contribution'] == 1) {
      array_push($contriArray, $salData);
    }
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Payslip <?php echo $salary_month_name; ?>-<?php echo $salary_slip_data['user_full_name']; ?></title>
  <link rel="icon" href="../img/fav.png" type="image/png">
  <meta name="author" content="harnishdesign.net">
  <meta name="viewport" content="width=1024">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/app-style9.css" rel="stylesheet" />
  <?php include 'common/colours.php'; ?>
  <style type="text/css">
    label {
      margin-bottom: 2px;
    }
    label,body  { 
      color:  black !important;
    }
    @media print {
      .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6,
      .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
           float: left;               
      }

      .col-lg-12 {
           width: 100%;
      }

      .col-lg-11 {
           width: 91.66666666666666%;
      }

      .col-lg-10 {
           width: 83.33333333333334%;
      }

      .col-lg-9 {
            width: 75%;
      }

      .col-lg-8 {
            width: 66.66666666666666%;
      }

       .col-lg-7 {
            width: 58.333333333333336%;
       }

       .col-lg-6 {
            width: 50%;
       }

       .col-lg-5 {
            width: 41.66666666666667%;
       }

       .col-lg-4 {
            width: 33.33333333333333%;
       }

       .col-lg-3 {
            width: 25%;
       }

       .col-lg-2 {
              width: 16.666666666666664%;
       }

       .col-lg-1 {
              width: 8.333333333333332%;
        }            
}
.table-padding {
    padding: 14px !important;
}
  </style>
</head>

<body id="printableArea" class="bg-white">

  <div class="container invoice-container">
    <div class="border border-dark">
      <div class="row " >
        <!-- <div class="col-lg-12 no-print text-center" id="printPageButton">
            <a href="#" onclick="window.history.go(-1); return false;"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Back</a>
            <a href="#" onclick="printDiv('printableArea')"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
          </div> -->
        <div class="col-lg-2 align-center text-center align-middle p-3">
          <img style="max-width:200px; max-height:100px; " id="logo" class="" src="../img/society/<?php echo $society_data['socieaty_logo']; ?>" title="invoice" alt="" />
        </div>
        <div class="col-lg-8 text-center p-3">
          <h2 class="text-primary mb-0">
            <?php if (isset($society_data['society_name']) && $society_data['society_name'] != "") {
              echo $society_data['society_name'];
            } ?>
          </h2>
          <br>
          <p>
            <?php if (isset($society_data['society_address']) && $society_data['society_address'] != "") {
              echo $society_data['society_address'];
            } ?>
          </p>
          <p>
            <?php if (isset($society_data['secretary_email']) && $society_data['secretary_email'] != "") {
              echo $society_data['secretary_email'] . " , ";
            } ?>
            <?php if (isset($society_data['secretary_mobile']) && $society_data['secretary_mobile'] != "") {
              echo $society_data['secretary_mobile'];
            } ?>
          </p>
          <p>
            <?php if (isset($society_data['company_website']) && $society_data['company_website'] != "") {
              $company_website =  str_replace("https://", "", $society_data['company_website']);
               $company_website =  str_replace("http://", "", $company_website); 
              echo $company_website;
            } ?>
           
          </p>
          <p>
            <?php if (isset($society_data['pan_number']) && $society_data['pan_number'] != "") { ?>
              <b>Pan No :</b>
            <?php echo  $society_data['pan_number'];
            } ?>
            <?php if (isset($society_data['gst_no']) && $society_data['gst_no'] != "") {

              echo  " , ";
            ?>
              <b>GST : </b>
            <?php echo $society_data['gst_no'];
            } ?>
          </p>
          <span style="font-size: 20px;"><b>Pay Slip for month of <?php echo date("F-Y", strtotime('01-'.$salary_month_name)); ?></b></span>
          
        </div>
        <div class="col-lg-2 text-center p-3">
        </div>
      </div>
      
        <div class="row ">
          
          <div class="col-md-6 pl-4 "  style="width: 50%;">
            <h4><?php echo $salary_slip_data['user_full_name']; ?></h4>
            <label>Department</label> : <?php  if($salary_slip_data['present_department_name']!="") { echo $salary_slip_data['present_department_name']; } else { echo $salary_slip_data['floor_name']; } ?> (<?php if($salary_slip_data['present_branch_name']!="") { echo $salary_slip_data['present_branch_name']; } else { echo $salary_slip_data['block_name']; } ?>) <br>
            <label>Designation </label> : <?php if($salary_slip_data['present_designation_name']!="") { echo $salary_slip_data['present_designation_name']; } else { echo $salary_slip_data['user_designation']; } ?><br>
            <label>Month Working Days </label> :<?php echo $salary_slip_data['total_month_days']; ?> <br>
            <label>Leave Days</label> :<?php echo $salary_slip_data['leave_days']; ?> <br>
            <label>Extra Days</label> :<?php echo $salary_slip_data['extra_days']; ?> <br>
            <label> Present Days </label>: <?php echo $salary_slip_data['total_working_days']; ?> <br>
            
          </div>
          <div class="col-md-6 pl-4 pt-4 text-left" style="width: 50%;">
            <label>Bank Name </label> <?php echo $salary_slip_data['bank_name']; ?><br>
            <label> Account No . </label>:<?php echo $salary_slip_data['account_no']; ?> <br>
            <?php if($salary_slip_data['pan_card_no']!="") { ?>
            <label> Pan No . </label>:<?php echo $salary_slip_data['pan_card_no']; ?> <br>
            <?php } if($salary_slip_data['esic_no']!="") { ?>
            <label> ESIC No .</label> :<?php echo $salary_slip_data['esic_no']; ?> <br>
            <?php }?>
            <label> Salary Mode</label> :
            <?php
                if (isset($salary_slip_data['salary_mode']) && $salary_slip_data['salary_mode'] != "") {
                  if ($salary_slip_data['salary_mode'] == 0) {
                    echo "Bank Transfer";
                  } else if ($salary_slip_data['salary_mode'] == 1) {
                    echo "Cash";
                  } else {
                    echo "Cheque";
                  }
                }
                ?><br>
            <?php if(isset($salary_slip_data['paid_week_off']) && $salary_slip_data['paid_week_off']>0){ ?>
              <label> Paid Week-Off </label>: <?php echo $salary_slip_data['paid_week_off']; ?> <br>
            <?php } ?>
            <?php if(isset($salary_slip_data['paid_holidays']) && $salary_slip_data['paid_holidays']>0){ ?>
              <label> Paid Holidays </label>: <?php echo $salary_slip_data['paid_holidays']; ?> <br>
            <?php } ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 table-padding">
            <table style="width: 100%;" border="1" class="">
              <thead>
                <tr>
                  <th colspan="2" class="text-center ">Earning</th>
                  <th colspan="2" class="text-center ">Deduction</th>
                </tr>
                
              </thead>
              <tbody>
                <?php

                if (!empty($earnArray)) {

                  $arrayEarning = array();
                  $arrayDeduction = array();
                  $otherEarningAry = array();
                  $otherDeductionAry = array();
                  $AdvaceSalaryDeductionAry = array();
                  $EmiDeductionAry = array();

                  for ($i1=0; $i1 <count($earnArray) ; $i1++) { 
                      $earning = array();  
                      $earning["name"] = $earnArray[$i1]['earning_deduction_name_current'];
                      $earning["value"] = $earnArray[$i1]['earning_deduction_amount'];
                      $earning["type"] ="1";

                      array_push($arrayEarning, $earning);
                  }

                  if (isset($salary_slip_data['other_earning']) && $salary_slip_data['other_earning'] > 0) {
                    $otherEarning = array();
                    $otherEarning["name"] = "Other Earning";
                    $otherEarning["value"] = $salary_slip_data['other_earning'];
                    $otherEarning["type"] ="1";
                    array_push($otherEarningAry, $otherEarning);
                  }

                  for ($i2=0; $i2 <count($dedcutArray) ; $i2++) { 
                      $dedction = array();  
                      $dedction["name"] = $dedcutArray[$i2]['earning_deduction_name_current'];
                      $dedction["value"] = $dedcutArray[$i2]['earning_deduction_amount'];
                      $dedction["type"] ="2";
                      array_push($arrayDeduction, $dedction);
                  }

                  if (isset($salary_slip_data['other_deduction']) && $salary_slip_data['other_deduction'] > 0) {
                    $otherDedction = array();
                    $otherDedction["name"] = "Other Dedction";
                    $otherDedction["value"] = $salary_slip_data['other_deduction'];
                    $otherDedction["type"] ="2";
                    array_push($otherDeductionAry, $otherDedction);
                  }

                  if(isset($salary_slip_data['advance_salary_paid_amount']) && $salary_slip_data['advance_salary_paid_amount'] > 0) {

                    $advaceSalary = array();
                    $advaceSalary["name"] = "Advance Salary Deduction";
                    $advaceSalary["value"] = $salary_slip_data['advance_salary_paid_amount'];
                    $advaceSalary["type"] ="2";
                    array_push($AdvaceSalaryDeductionAry, $advaceSalary);
                  }

                  if(isset($salary_slip_data['emi_deduction']) && $salary_slip_data['emi_deduction'] > 0) {

                    $emi = array();
                    $emi["name"] = "Loan E.M.I Deduction";
                    $emi["value"] = $salary_slip_data['emi_deduction'];
                    $emi["type"] ="2";
                    array_push($EmiDeductionAry, $emi);
                  }

                  $earningNewArray = array_merge($arrayEarning,$otherEarningAry);
                  $deductionNewArray = array_merge($arrayDeduction,$otherDeductionAry,$AdvaceSalaryDeductionAry,$EmiDeductionAry);

                 

                  $count = count($earningNewArray);
                  $Dcount = count($deductionNewArray);
                  if ($count<$Dcount) {
                   $countTemp = $Dcount;
                  } else {
                     $countTemp = $count;
                  }

                  for ($i = 0; $i < $countTemp; $i++) {
                ?>
                    <tr>
                      
                      <td class="">
                        <?php echo $earningNewArray[$i]['name']; ?>
                      </td>
                      <td class="text-right">
                        <?php echo $earningNewArray[$i]['value']; ?>
                      </td>
                      <td class="">
                        <?php echo $deductionNewArray[$i]['name']; ?>
                      </td>
                      <td  class="text-right">
                         <?php echo $deductionNewArray[$i]['value']; ?>
                      </td>

                    </tr>
                    
                <?php } ?>
                  <tr>
                      <td class="">
                        &nbsp;
                      </td>
                      <td class="text-right">
                        
                      </td>
                      <td class="">
                      </td>
                      <td  class="text-right">
                      </td>
                  </tr>

                <?php }  ?>
                <tr>
                  <td class="">
                    Gross Salary (A) 
                  </td>
                  <td class="text-right ">
                    <?php if (isset($salary_slip_data['total_earning_salary']) && $salary_slip_data['total_earning_salary'] > 0) {
                      echo $salary_slip_data['total_earning_salary'];
                    } ?>
                  </td>
                  <td class="">
                    Total Deduction (B)
                  </td>
                  <td class="text-right ">
                    <?php if (isset($salary_slip_data['total_deduction_salary']) && $salary_slip_data['total_deduction_salary'] > 0) {
                      echo $salary_slip_data['total_deduction_salary'];
                    } ?>
                  </td>
                </tr>
                
                
                <tr >
                 
                  <td colspan="3" class="text-center">
                   <b> Net Salary (A-B) </b>
                  </td>
                  <td class="text-right "><b>
                    <?php if (isset($salary_slip_data['total_net_salary']) && $salary_slip_data['total_net_salary'] > 0) {
                      $netsalary = round($salary_slip_data['total_earning_salary']-$salary_slip_data['total_deduction_salary']);
                      echo  $society_data['currency'] ." ". number_format($netsalary,2);
                   
                    } ?> </b>
                  </td>
                  
                </tr>
                <?php 
                if(isset($salary_slip_data['expense_amount']) && $salary_slip_data['expense_amount']>0){ ?>
                  <tr>
                    <td colspan="3">
                        Reimbursement
                    </td>
                    <td class="text-right">
                      <?php if (isset($salary_slip_data['expense_amount']) && $salary_slip_data['expense_amount'] > 0) {
                        echo $salary_slip_data['expense_amount'];
                      } else {
                        echo "-";
                      } ?>
                  </td>
                </tr>
               
               <tr >
                 
                  <td colspan="3" class="text-center">
                   <b> Net Payable </b>
                  </td>
                  <td class="text-right "><b>
                    <?php if (isset($salary_slip_data['total_net_salary']) && $salary_slip_data['total_net_salary'] > 0) {
                      $netsalary = round($salary_slip_data['total_net_salary']);
                      echo  $society_data['currency'] ." ". number_format($netsalary,2);
                   
                    } ?> </b>
                  </td>
                  
                </tr>
                <?php } ?>
                <tr >
                 
                  <td class="" colspan="4">
                   Amount in Words:  <?php   $netsalaryinWord = changeAmountInWords($netsalary);
                          echo ucfirst($netsalaryinWord);
                    ?>
                  </td>
                 
                  
                </tr>
                <?php
               /// print_r($contriArray);
                  for ($contri = 0; $contri < COUNT($contriArray); $contri++) {
                ?>
                    <tr>
                      
                      <td class="">
                        <?php echo $contriArray[$contri]['earning_deduction_name_current']." Employer Contribution"; ?>
                      </td>
                      <td class="text-right">
                        <?php echo $contriArray[$contri]['employer_contribution_amount']; ?>
                      </td>
                      <td class="">
                        
                      </td>
                      <td  class="text-right">
                         
                      </td>

                    </tr>
                    
                <?php } ?>

              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 pl-4">
            <?php 
            
            if($salary_slip_data['prepared_by']==$salary_slip_data['checked_by']){ ?>
              <label>Generated & Checked By : </label> <?php echo $salary_slip_data['pre_by_admin_name']; ?> <br>
        <?php  } else{ ?>
          <label>Generated By :</label> <?php echo $salary_slip_data['pre_by_admin_name']; ?> <br>
        <?php if($salary_slip_data['checked_by_admin_name']!='') {  ?>
          <label>Checked By :</label> <?php echo $salary_slip_data['checked_by_admin_name']; ?> <br>
        <?php }  } if($salary_slip_data['pub_by_admin_name']!='') { ?>
            
            
            <label>Published By:</label> <?php echo $salary_slip_data['pub_by_admin_name']; ?> <br>
          <?php } ?>
          </div>
          <div class="col-md-6 col-sm-6 pl-4 text-center">
          <?php if(file_exists('../img/stamp/'.$society_data['salary_setting_sign_stamp']) && $society_data['salary_setting_sign_stamp'] !=""){?>
                  <img src="../img/stamp/<?php echo $society_data['salary_setting_sign_stamp']; ?>" style="width: 150px;max-height:200px;">
            <?php }?>
           
          </div>
          
          <!--  <h4><span class='card-title'><u>Terms & Conditions</u></span></h4> -->
          <div class="col-md-12 text-center mt-2">
            <footer class="text-center">
              <p class="text-gray-dark" align="center" style="color: #b5b5b5;">This is a computer generated salary slip <?php if(!isset($society_data['salary_setting_sign_stamp']) && $society_data['salary_setting_sign_stamp'] ==""){?>, thus no signature is required<?php }?></p>
            </footer>
          </div>
        </div>
      </div>
    </div>
</body>
<script type="text/javascript">
  function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }

     window.onload = function() { printDiv('printableArea'); }

   Android.print(printContents);
</script>

</html>
<?php

function changeAmountInWords($number){

   $no = floor($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'one', '2' => 'two',
    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
    '7' => 'seven', '8' => 'eight', '9' => 'nine',
    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
    '13' => 'thirteen', '14' => 'fourteen',
    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
    '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
    '60' => 'sixty', '70' => 'seventy',
    '80' => 'eighty', '90' => 'ninety');
   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';
  return $result ."only.";
}
?>