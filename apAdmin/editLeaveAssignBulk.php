<?php
extract(array_map("test_input", $_POST));
$user_id = (int)$_POST['user_id'];
$floor_id = (int)$_POST['floor_id'];
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$previousYear = $leave_year-1;
extract($data);
$ulgq=$d->select("leave_assign_master","society_id='$society_id' AND user_id='$user_id' AND assign_leave_year = $leave_year", "GROUP BY user_id");
$ulgData=mysqli_fetch_array($ulgq);
$leave_group_id = $ulgData['leave_group_id'];
$lgq=$d->select("leave_group_master","society_id='$society_id' AND leave_group_id='$leave_group_id'");
$lgData=mysqli_fetch_array($lgq);
$get_leave_group_id = $lgData['leave_group_id'];
$q=$d->selectRow("
                    (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=0 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')=leave_assign_master.assign_leave_year) AS total_full_day_leave, 
                    
                    (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                    AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=1 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')=leave_assign_master.assign_leave_year) AS total_half_day_leave, 

                    (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year=leave_assign_master.assign_leave_year AND is_leave_carry_forward=0) AS pay_out_leave,

                    (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year=leave_assign_master.assign_leave_year AND is_leave_carry_forward=1) AS carry_forward_leave,

                    leave_type_master.*,leave_assign_master.user_total_leave,leave_assign_master.user_leave_id,leave_assign_master.applicable_leaves_in_month",
                    
                    "leave_type_master LEFT JOIN leave_assign_master ON leave_assign_master.leave_type_id=leave_type_master.leave_type_id AND leave_assign_master.user_id='$user_id' AND leave_assign_master.assign_leave_year='$leave_year'",
                    
                    "leave_type_master.society_id='$society_id' AND leave_type_master.leave_type_delete=0  AND leave_assign_master.leave_group_id=$leave_group_id AND leave_assign_master.assign_leave_year=$leave_year");
$data = mysqli_fetch_array($q);
$used_leave = $data['total_full_day_leave']+($data['total_half_day_leave']/2)+$data['pay_out_leave']+$data['carry_forward_leave'];
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Bulk Leave Assign</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addLeaveAssignBulk" action="controller/LeaveAssignController.php" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Employee <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8 col-12" id="">
                                <select  type="text" required="" class="form-control" id="user_id" name="user_id">
                                    <?php
                                    $userData=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
                                    while ($user=mysqli_fetch_array($userData)) {
                                    ?>
                                    <option <?php if(isset($user_id) && $user_id == $user['user_id']){ echo "selected"; } ?> value="<?php if(isset($user['user_id']) && $user['user_id'] !=""){ echo $user['user_id']; } ?>"> <?php if(isset($user['user_full_name']) && $user['user_full_name'] !=""){ echo $user['user_full_name']; } ?></option>
                                    <?php }?>
                                </select>
                            </div>                   
                        </div> 
                    </div>  
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Year <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8 col-12" id="">
                                <select  type="text" id="assign_leave_year" required="" class="form-control" name="assign_leave_year" >
                                    <option value="<?php echo $leave_year ?>"><?php echo $leave_year ?></option>
                                </select>
                            </div>   
                        </div>  
                    </div>
                    <div class="col-md-4">
                    <?php if($used_leave==0){ ?>
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Leave Group <span class="required">* </span> 
                            <?php 
                             if($get_leave_group_id=='' && $get_leave_group_id==null){
                                 ?>
                                <label class="required" id="hideMessage">(Assigned Leave Group Not Found!)</label>
                            <?php } ?>
                            </label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                            <select  type="text" required="" class="form-control single-select" id="leave_group_id" name="leave_group_id" onchange="getAllGroupLeaveTypeOption(this.value, <?php echo $user_id; ?>, <?php echo $floor_id; ?>, <?php echo $currentYear; ?>, <?php echo $nextYear; ?>, <?php echo $previousYear; ?>, <?php echo $leave_year; ?> )">
                                <option value="">-- Select --</option> 
                                <?php 
                                    $leaveGroup=$d->select("leave_group_master","society_id='$society_id' AND leave_group_active_status = 0");  
                                    while ($leaveGroupData=mysqli_fetch_array($leaveGroup)) {
                                ?>
                                <option <?php if($leave_group_id == $leaveGroupData['leave_group_id']){echo 'selected';} ?> value="<?php if(isset($leaveGroupData['leave_group_id']) && $leaveGroupData['leave_group_id'] !=""){ echo $leaveGroupData['leave_group_id']; } ?>"><?php if(isset($leaveGroupData['leave_group_name']) && $leaveGroupData['leave_group_name'] !=""){ echo $leaveGroupData['leave_group_name']; } ?></option> 
                                <?php } ?>
                            </select>  
                            </div>
                        </div> 
                        <?php } ?>
                    </div>
                </div>
                <div id="show-all-group-leave">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="input-10" class="col-form-label">Leave Type</label>
                        </div>

                        <div class="col-md-2">
                            <label for="input-10" class="col-form-label">Used Leave</label>
                        </div>

                        <div class="col-md-3">
                            <label for="input-10" class="col-form-label">Paid Leave</label>
                        </div>
                        
                        <div class="col-md-3">
                            <label for="input-10" class="col-form-label">Paid Leave Use In Month</label>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        $i=0;
                        //$q=$d->select("leave_assign_master,leave_type_master","leave_assign_master.user_id='$user_id' AND leave_assign_master.assign_leave_year='$leave_year' AND leave_assign_master.society_id='$society_id' AND leave_assign_master.leave_type_id=leave_type_master.leave_type_id");
                        $q=$d->selectRow("
                        (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=0 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')=leave_assign_master.assign_leave_year) AS total_full_day_leave, 
                        
                        (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$user_id' 
                        AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=1 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')=leave_assign_master.assign_leave_year) AS total_half_day_leave, 

                        (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year=leave_assign_master.assign_leave_year AND is_leave_carry_forward=0) AS pay_out_leave,

                        (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$user_id' AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year=leave_assign_master.assign_leave_year AND is_leave_carry_forward=1) AS carry_forward_leave,

                        leave_type_master.*,leave_assign_master.user_total_leave,leave_assign_master.user_leave_id,leave_assign_master.applicable_leaves_in_month",
                        
                        "leave_type_master LEFT JOIN leave_assign_master ON leave_assign_master.leave_type_id=leave_type_master.leave_type_id AND leave_assign_master.user_id='$user_id' AND leave_assign_master.assign_leave_year='$leave_year'",
                        
                        "leave_type_master.society_id='$society_id' AND leave_type_master.leave_type_delete=0  AND leave_assign_master.leave_group_id=$leave_group_id AND leave_assign_master.assign_leave_year=$leave_year");
                        while ($data = mysqli_fetch_array($q)) {
                            //$q=$d->select("leave_assign_master","user_id='$user_id' AND assign_leave_year='$leave_year' AND society_id='$society_id'");
                        ?>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <input type="text" class="form-control" disabled value="<?php echo $data['leave_type_name']; ?>">
                                <input type="hidden" class="form-control" name="user_leave_id[<?php echo $i; ?>]" value="<?php echo $data['user_leave_id']; ?>">
                                <input type="hidden" class="form-control" name="leave_type_id[<?php echo $i; ?>]" value="<?php echo $data['leave_type_id']; ?>">
                                <input type="hidden" name="del_leave_group_id" id="del_leave_group_id" value="<?php echo $leave_group_id; ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group row w-100 mx-0">
                                <?php  
                                $used_leave = $data['total_full_day_leave']+($data['total_half_day_leave']/2)+$data['pay_out_leave']+$data['carry_forward_leave'];
                                ?>
                                <input type="text" class="form-control" disabled value="<?php echo $used_leave; ?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group row w-100 mx-0">
                                <?php //$used_leave = $used_leave==0?1:$used_leave ?>
                                <input step="0.5" min="0" type="text" class="form-control user_total_leave onlyNumber" name="user_total_leave[<?php echo $i; ?>]" id="user_total_leave_<?php echo $i; ?>" min="<?php echo $used_leave; ?>" max="365" value="<?php echo $data['user_total_leave']; ?>" placeholder="User Total Leave" onkeyup="setUseInMonthLeaves(<?php echo $i;?>)">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group row w-100 mx-0">
                                <select name="applicable_leaves_in_month[<?php echo $i; ?>]" id="applicable_leaves_in_month_<?php echo $i; ?>" class="form-control single-select">
                                    <?php $x= 1;
                                        for($a=($x-0.5); $a<=$data['user_total_leave']; $a+=0.5){ ?>
                                            <option <?php if($data['applicable_leaves_in_month']==$a){echo 'selected';} ?> value="<?php echo $a; ?>"><?php echo $a; ?></option>
                                    <?php } ?>
                                </select>
                            </div> 
                        </div>
                        <?php $i++; }?>
                    </div>
                </div>
                               
                <div class="form-footer text-center">
                    <button id="addLeaveAssignBulkBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                    <input type="hidden" name="updateLeaveAssignBulk"  value="updateLeaveAssignBulk">
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addLeaveAssignBulk');"><i class="fa fa-check-square-o"></i> Reset</button>
                </div>

              </form>
           
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">
       function setUseInMonthLeaves(id){
            value = $('#user_total_leave_'+id).val();
            use_in_month_content = ``;
            month_use_value = value>=31?31:value;
            var x = 1;
            for (var i = (x-0.5); i <=month_use_value; i += 0.5) {
                use_in_month_content += `<option value="`+i+`">`+i+`</option>`;
            }
            $('#applicable_leaves_in_month_'+id).html(use_in_month_content);
        }
    </script>

<style>
    input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	  -webkit-appearance: none;
	  margin: 0;
	}
</style>