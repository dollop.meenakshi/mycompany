<div class="content-wrapper">
    <div class="container-fluid">
    <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title"><?php echo $xml->string->society; ?>  Facilities Booking</h4>
	   </div>
	   <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->



     <div class="row">
    
     <?php 
            $q=$d->select("facilities_master","society_id='$society_id'","");
           if(mysqli_num_rows($q)>0) { 
            while ($row=mysqli_fetch_array($q)) {
                extract($row);
                if ($amount_type==0) {
                  $personText = " / Per Person";
                } else {
                   $personText = "";
                }
             ?>
      <div class="col-lg-4">
         <div class="card">
            <img class="card-img-top lazyload" height="220" onerror="this.src='../img/facility/default.png'"   src="../img/ajax-loader.gif" data-src="../img/facility/<?php echo $facility_photo; ?>" alt="Card image cap">
            <div class="card-body">
               <h5 class="card-title text-primary text-center"><?php 
         
               echo custom_echo($facility_name,30);
             //IS_613
               if ($facility_type==2) {
               ?>  <span class="badge badge-success m-1">Free</span>
               <?php } else { ?>  <span class="badge badge-secondary m-1">Paid </span> <?php }?></h5>
               <hr>
                <div class="row text-center">
                   <?php if ($facility_type==2) {
                    $type= "Free";
                  } else  if ($facility_type==0)  {
                    $type= "Per Day".$personText;
                  } else  if ($facility_type==1)  {
                    $type= "Per Month".$personText;
                  } else  if ($facility_type==3)  {
                    $type= "Per Hour".$personText;
                  }  else  if ($facility_type==4)  {
                    $type= "Per Time Slot".$personText;
                  } ?> 
                <div class="col-sm-12"> <?php 
                if ($facility_type!=2) {
                    echo $currency." ". number_format($facility_amount,2) .' '. $type;
                } else {
                  echo $type;
                }
                ?>
                 
                </div>
              </div>
               <hr>
               <div class="">
                <?php if ($facility_type!=2) { ?>
              <form action="facilityBook" method="GET" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $facility_id;?>">
                <input type="hidden" name="updatefacilities" value="updatefacilities">
                <button style="margin-right: 5px;" type="submit" class="btn btn-block btn-success btn-sm" name="">Book Now / Manage Booking</button>
              </form>
                <?php  } else {?>
                <br>
            <?php } ?>
              </div>
            </div>
         </div>
      </div><?php }  } else {
      echo "<img src='img/no_data_found.png'>";
        
      }?>
    
   </div><!--End Row-->


   
   </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
   </div><!--End content-wrapper-->