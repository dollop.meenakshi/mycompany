<?php
if(isset($_POST['addDistributorBtn']) && $_POST['addDistributorBtn'] == "addDistributorBtn")
{
    extract($_POST);
}
if(isset($_POST['editDistributor']) && isset($_POST['distributor_id']))
{
    $distributor_id = $_POST['distributor_id'];
    $q = $d->select("distributor_master","distributor_id = '$distributor_id'");
    $data = $q->fetch_assoc();
    extract($data);
    $form_id = "distributorAddForm";
}
else
{
    $form_id = "distributorAddForm";
}

if(isset($distributor_area_id) && !empty($distributor_area_id) && $distributor_area_id != 0)
{
    $q1 = $d->selectRow("latitude,longitude","area_master_new","area_id = '$distributor_area_id'");
    $fetch = $q1->fetch_assoc();
    $area_latitude = $fetch['latitude'];
    $area_longitude = $fetch['longitude'];
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="<?php echo $form_id; ?>" action="controller/distributorController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <?php
                            if(isset($_POST['editDistributor']) && isset($_POST['distributor_id']))
                            {
                            ?>
                            <i class="fa fa-edit"></i>
                            Edit Distributor
                            <?php
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-plus"></i>
                            Add Distributor
                            <?php
                            }
                            ?>
                            </h4>
                            <div class="form-group row">
                                <label for="country_id" class="col-sm-2 col-form-label">Country <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="country_id" name="country_id" required onchange="getStateCity(this.value);">
                                        <option value="">-- Select --</option>
                                        <?php
                                        $cq = $d->select("countries");
                                        while ($cd = $cq->fetch_assoc())
                                        {
                                        ?>
                                        <option <?php if($country_id == $cd['country_id']) { echo 'selected'; } ?> value="<?php echo $cd['country_id'];?>"><?php echo $cd['country_name'];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="distributor_name" class="col-sm-2 col-form-label">Distributor Name <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editDistributor)){ echo $distributor_name; } ?>" required type="text" class="form-control" name="distributor_name" id="distributor_name" maxlength="150">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="distributor_code" class="col-sm-2 col-form-label">Distributor Code</label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editDistributor)){ echo $distributor_code; } ?>" required type="text" class="form-control" name="distributor_code" id="distributor_code" maxlength="100">
                                </div>
                                <label for="distributor_contact_person" class="col-sm-2 col-form-label">Distributor Contact Person Name <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editDistributor)){ echo $distributor_contact_person; } ?>" required type="text" class="form-control" name="distributor_contact_person" id="distributor_contact_person" maxlength="100">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="distributor_contact_person_number" class="col-sm-2 col-form-label">Distributor Contact Person Number <span class="text-danger">*</span></label>
                                <div class="col-sm-2">
                                    <select name="distributor_contact_person_country_code" class="form-control single-select" id="distributor_contact_person_country_code" required>
                                        <?php include 'country_code_option_list.php'; ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input value="<?php if(isset($editDistributor)){ echo $distributor_contact_person_number; } ?>" required type="text" class="form-control onlyNumber" name="distributor_contact_person_number" id="distributor_contact_person_number" maxlength="15">
                                </div>
                                <label for="distributor_alt_contact_number" class="col-sm-2 col-form-label">Distributor Alternate Number</label>
                                <div class="col-sm-2">
                                    <select name="distributor_alt_contact_number_country_code" class="form-control single-select" id="distributor_alt_contact_number_country_code">
                                        <?php include 'country_code_option_list.php'; ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input value="<?php if(isset($editDistributor) && $distributor_alt_contact_number != 0){ echo $distributor_alt_contact_number; } ?>" type="text" class="form-control onlyNumber" name="distributor_alt_contact_number" id="distributor_alt_contact_number" maxlength="15">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="distributor_email" class="col-sm-2 col-form-label">Order Email</label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" type="email" value="<?php if (isset($editDistributor) && $distributor_email != "") { echo $distributor_email; } ?>" id="distributor_email" name="distributor_email" class="form-control" maxlength="200">
                                </div>
                                <label for="distributor_email" class="col-sm-2 col-form-label">Distributor Email</label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" type="email" value="<?php if (isset($editDistributor) && $distributor_email_main != "") { echo $distributor_email_main; } ?>" id="distributor_email_main" name="distributor_email_main" class="form-control" maxlength="200">
                                </div>
                                
                            </div>
                            <div class="form-group row">
                                <label for="distributor_pincode" class="col-sm-2 col-form-label">Distributor Pincode</label>
                                <div class="col-sm-4">
                                    <input type="text" id="distributor_pincode" maxlength="6" value="<?php if(isset($editDistributor) && $distributor_pincode != 0) { echo $distributor_pincode; } ?>" class="form-control onlyNumber" inputmode="numeric" name="distributor_pincode">
                                </div>
                                <label for="distributor_photo" class="col-sm-2 col-form-label">Distributor Photo</label>
                                <div class="col-sm-4">
                                    <input type="file" id="distributor_photo" name="distributor_photo" inputmode="numeric" class="form-control-file border imageOnly" accept="image/*">
                                </div>
                                <label for="photo_preview" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-4">
                                    <img id="blah" src="<?php if(isset($editDistributor) && $distributor_photo != ""){ echo $distributor_photo; } ?>" width="100" height="100" alt="Distributor photo" class='profile d-none'/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="distributor_address" class="col-sm-2 col-form-label">Distributor Address</label>
                                <div class="col-sm-4">
                                    <textarea class="form-control" rows="4" name="distributor_address" id="distributor_address" maxlength="200"><?php if(isset($editDistributor)){ echo $distributor_address; } ?></textarea>
                                </div>
                                <label for="distributor_gst_no" class="col-sm-2 col-form-label">Distributor GST No</label>
                                <div class="col-sm-4">
                                    <input autocomplete="off" type="text" value="<?php if (isset($editDistributor) && $distributor_gst_no > 0) { echo $distributor_gst_no; } ?>" id="distributor_gst_no" name="distributor_gst_no" class="form-control" maxlength="15">
                                </div>
                            </div>
                            <div class="row form-group">
                                <input id="searchInput6" name="location_name" class="form-control" type="text" placeholder="Enter location" value="<?php if(isset($editDistributor) && $distributor_google_address != ""){ echo $distributor_google_address; } ?>">
                                <div class="map" id="map2" style="width: 100%; height: 300px;"></div>
                            </div>
                            <input type="hidden" class="form-control" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                            <input type="hidden" name="zoomValue" id="zoomValue" value="17">
                            <input type="hidden" name="zoomValueBlk" id="zoomValueBlk" value="17">
                            <input type="hidden" value="<?php if(isset($editDistributor) && $distributor_latitude > 0){ echo $distributor_latitude; } ?>" id="distributor_latitude" name="distributor_latitude">
                            <input type="hidden" value="<?php if(isset($editDistributor) && $distributor_longitude > 0){ echo $distributor_longitude; } ?>" id="distributor_longitude" name="distributor_longitude" class="form-control">
                            <div class="form-footer text-center">
                                <input type="hidden" name="addDistributor" value="addDistributor">
                                <?php
                                if(isset($editDistributor))
                                {
                                ?>
                                <input type="hidden" name="old_photo" value="<?php echo $distributor_photo; ?>">
                                <input type="hidden" class="form-control" name="distributor_id" id="distributor_id" value="<?php echo $distributor_id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                <input type="hidden" class="form-control" name="distributor_id" id="distributor_id" value="">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>
<script>
$().ready(function()
{
    var latitude = 23.037786;
    var longitude = 72.512043;
    <?php
    if(isset($distributor_area_id) && !empty($distributor_area_id) && isset($area_latitude) && !empty($area_latitude) && isset($area_longitude) && !empty($area_longitude))
    {
    ?>
    var area_latitude = '<?php echo $area_latitude; ?>';
    var area_longitude = '<?php echo $area_longitude; ?>';
    initialize2(area_latitude, area_longitude);
    <?php
    }
    elseif(isset($editDistributor) && !empty($distributor_latitude) && !empty($distributor_longitude))
    {
    ?>
    initialize2(<?php echo $distributor_latitude; ?>, <?php echo $distributor_longitude; ?>);
    <?php
    }
    else
    {
    ?>
    initialize2(latitude, longitude);
    <?php
    }
    ?>

    $("#distributor_gst_no").keyup(function ()
    {
        $(this).val($(this).val().toUpperCase());
    });

    $("#distributor_gst_no").on("keydown", function (e)
    {
        return e.which !== 32;
    });

    <?php
    if(isset($editDistributor))
    {
        if($distributor_photo != "" && file_exists("../img/users/recident_profile/".$distributor_photo))
        {
    ?>
    $('#blah').removeClass('d-none');
    $('#blah').attr('src', '../img/users/recident_profile/<?php echo $distributor_photo; ?>');
        <?php
        }
        else
        {
        ?>
    $('#blah').addClass('d-none');
        <?php
        }
        ?>
    $('#distributor_photo').rules('remove', 'required');
    $("#distributor_contact_person_country_code").val('<?php echo $distributor_contact_person_country_code; ?>').trigger('change');
    $("#distributor_alt_contact_number_country_code").val('<?php echo $distributor_alt_contact_number_country_code; ?>').trigger('change');
    <?php
    }
    ?>
});

function initialize2(d_lat, d_long)
{
    var latlng = new google.maps.LatLng(d_lat, d_long);
    var latitute = 23.037786;
    var longitute = 72.512043;
    zoomVal = $('#zoomValue').val();
    var map = new google.maps.Map(document.getElementById('map2'), {
        center: latlng,
        zoom: parseInt(13)
    });
    mapzoom = map.getZoom();
    /////////////////on zoom in out
    google.maps.event.addListener(map, 'zoom_changed', function() {
        var zoomLevel = map.getZoom();
        $('#zoomValue').val(zoomLevel);
        mapzoom = map.getZoom(); //to stored the current zoom level of the map
    });
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
    });
    var parkingRadition = 5;
    var citymap = {
        newyork: {
            center: {
                lat: latitute,
                lng: longitute
            },
            population: parkingRadition
        }
    };

    var input = document.getElementById('searchInput6');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    autocomplete10.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place5 = autocomplete10.getPlace();
        if (!place5.geometry) {
            window.alert("Autocomplete's returned place5 contains no geometry");
            return;
        }

        // If the place5 has a geometry, then present it on a map.
        if (place5.geometry.viewport) {
            map.fitBounds(place5.geometry.viewport);
        } else {
            map.setCenter(place5.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place5.geometry.location);
        marker.setVisible(true);

        var pincode = "";
        for (var i = 0; i < place5.address_components.length; i++) {
            for (var j = 0; j < place5.address_components[i].types.length; j++) {
                if (place5.address_components[i].types[j] == "postal_code") {
                    pincode = place5.address_components[i].long_name;
                }
            }
        }
        bindDataToForm2(place5.formatted_address, place5.geometry.location.lat(), place5.geometry.location.lng(), pincode, place5.name);
        infowindow.setContent(place5.formatted_address);
        infowindow.open(map, marker);

    });
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({
            'latLng': marker.getPosition()
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var places = results[0];
                    var pincode = "";
                    var serviceable_area_locality = places.address_components[4].long_name;
                    for (var i = 0; i < places.address_components.length; i++) {
                        for (var j = 0; j < places.address_components[i].types.length; j++) {
                            if (places.address_components[i].types[j] == "postal_code") {
                                pincode = places.address_components[i].long_name;
                            }
                        }
                    }
                    bindDataToForm2(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng(), pincode, serviceable_area_locality);
                }
            }
        });
    });
}

function bindDataToForm2(address, lat, lng, pin_code, serviceable_area_locality)
{
    document.getElementById('distributor_latitude').value = lat;
    document.getElementById('searchInput6').value = address;
    document.getElementById('distributor_longitude').value = lng;
}

function chnageUserRange()
{
    var latitude = $('#distributor_latitude').val();
    var longitude = $('#distributor_longitude').val();
    initialize2(latitude, longitude);
}

function readURL(input)
{
    if (input.files && input.files[0] && input.files[0].size <= 3000000 )
    {
        var reader = new FileReader();
        reader.onload = function(e)
        {
            $('#blah').removeClass('d-none');
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#distributor_photo").change(function()
{
    readURL(this);
});

function getStateCity(country_id)
{
    $.ajax({
        url: 'controller/distributorController.php',
        data: {getStateCityTag:"getStateCityTag",country_id:country_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#state_city_id').empty();
            response = JSON.parse(response);
            $('#state_city_id').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#state_city_id').append("<option value='"+value.city_id+"-"+value.state_id+"'>"+value.city_name+" - "+value.state_name+"</option>");
            });
        }
    });
}

function getAreaList(city_state_id)
{
    const country_id = $('#country_id').val();
    const city_id = city_state_id.substring(0, city_state_id.indexOf('-'));
    const state_id = city_state_id.substring(city_state_id.indexOf('-') + 1);
    $.ajax({
        url: 'controller/distributorController.php',
        data: {getAreaTag:"getAreaTag",country_id:country_id,state_id:state_id,city_id:city_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#area_id').empty();
            response = JSON.parse(response);
            $('#area_id').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#area_id').append("<option value='"+value.area_id+"'>"+value.area_name+"</option>");
            });
        }
    });
}

function setAreaMap(area_id)
{
    $.ajax({
        url: 'controller/distributorController.php',
        data: {getAreaLatLon:"getAreaLatLon",area_id:area_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            response = JSON.parse(response);
            initialize2(response.area_latitude,response.area_longitude);
        }
    });
}
</script>