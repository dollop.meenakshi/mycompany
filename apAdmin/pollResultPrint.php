<?php 
if (filter_var($_GET['id'], FILTER_VALIDATE_INT) == true && filter_var($_GET['society_id'], FILTER_VALIDATE_INT) == true) {
  $url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
  $activePage = basename($_SERVER['PHP_SELF'], ".php");
  include_once 'lib/dao.php';
  include 'lib/model.php';
  $d = new dao();
  $m = new model();
  include 'common/checkLanguage.php';

?>
<!-- simplebar CSS-->
<link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
<!-- Bootstrap core CSS-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
<!-- animate CSS-->
<link href="assets/css/animate.css" rel="stylesheet" type="text/css"/>
<!-- Icons CSS-->
<link href="assets/css/icons.css" rel="stylesheet" type="text/css"/>
<!-- Sidebar CSS-->
<link href="assets/css/sidebar-menu2.css" rel="stylesheet"/>
<!-- Custom Style-->
<?php include 'common/colours.php'; ?>
<link href="assets/css/app-style9.css" rel="stylesheet"/>
  <link rel="icon" href="../img/fav.png" type="image/png">

<link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
<link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
<!--Lightbox Css-->
<link href="assets/plugins/fancybox/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/apexcharts-bundle/js/apexcharts.min.js"></script>
<style type="text/css">
  .lineHeight{
    line-height: 2;
  }
</style>
<body style="background-color: white;">

<?php 
  extract($_GET);
  $vId = $id;
  $q = $d->select("voting_master,voting_option_master","voting_master.voting_id=voting_option_master.voting_id AND voting_master.society_id = '$society_id' AND voting_master.voting_id = '$id'");
  $data = mysqli_fetch_array($q);
  extract($data);
  $socData=$d->selectArray("society_master","society_id='$society_id'");
?>
<?php if(mysqli_num_rows($q) <= 0 ){?> 
  <div class="row">
    <div class="col-lg-12">
      <img src='img/no_data_found.png'>
    </div>
  </div>
<?php }  else { ?>
  <div class="row " style="padding: 5%;">
    <div class="col-lg-12">
      <div class="card">
        <div class="row text-center no-print" id="printPageButton">
          <div class="col-lg-12">
             <a href="#" onclick="window.history.go(-1); return false;"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Back</a>
              <a href="#" onclick="printDiv('printableArea')"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
          </div>
          
        </div>
        <div class="m-4 border border-dark" id="printableArea">
          <div class="row m-3">
            <div class="col-sm-6">
              <h4><i class="fa fa-building"></i> <?php echo $socData['society_name']; ?></h4>
              <address>
                <?php echo $socData['society_address']; ?>
              </address>
            </div>
            <div class="col-sm-6 text-right">
              <img src="../img/logo.png" height="100">
            </div>
          </div>
          <hr>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6">
                <h5 class="card-title">Question: <?php echo $voting_question; ?></h5>
                <p><b>Description:</b> <?php echo $voting_description; ?></p>
                  <p><b>Poll For:</b> <?php 
                   $election_for_string= $xml->string->election_for; 
                       $electionArray = explode("~", $election_for_string);
                       if ($poll_for==0) {
                        echo  $all= $xml->string->all.' '. $xml->string->floors; 
                        } else {
                          $qf=$d->selectRow("floor_name,block_name","floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.floor_id='$poll_for'");
                          $floorData=mysqli_fetch_array($qf);
                          echo $floorData['floor_name'].' ('.$floorData['block_name'].')' ;
                        }
                  ?>
                </p>
                <p><b>Poll Option:   </b>
                  <ul class="list-unstyled">
                    <?php
                    $i11=1;
                    $optionArray = array();
                    $optionArrayResult = array();
                    $sq = $d->select("voting_option_master","voting_id = '$voting_id' AND society_id = '$society_id'");
                    while($sData = mysqli_fetch_array($sq)){
                      array_push($optionArray,$sData['option_name']);
                      $voteCount = $d->count_data_direct("voting_result_id","voting_result_master","voting_id = '$vId' AND voting_option_id='$sData[voting_option_id]'");
                      array_push($optionArrayResult,$voteCount);
                      ?>
                    <li><?php echo $i11++;?>. <?php echo $sData['option_name']; ?></li>
                    <?php  } ?>
                  </ul>
                </p>
              </div>
              <div class="col-sm-6">
                
                <?php if(trim($voting_end_date) !="" && $voting_end_date !="0000-00-00 00:00:00"){ ?>
                  <p><b>Poll Start Date:</b> <?php echo date("d-m-Y h:i A",strtotime($voting_start_date)); ?></p>
                <p><b>Poll End Date:</b> <?php echo date("d-m-Y h:i A",strtotime($voting_end_date)); ?></p><?php } else {?> 
                   <p><b>Poll Date:</b> <?php echo date("d-m-Y h:i A",strtotime($voting_start_date)); ?></p>
                <?php } ?>
                <p><b>Participated Members:</b> <?php 
                  $q1=$d->select("voting_result_master,unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=voting_result_master.unit_id AND voting_result_master.user_id=users_master.user_id AND  voting_result_master.voting_id = '$vId'");
                  echo $totalParticiapte= mysqli_num_rows($q1);
                  ?>
                 </p>
                <p><b>Not Participated Members:</b>
                  <?php 

                  $qry ="";
            if ($poll_for==0) {
              $for="All Family Members";
            } else if ($poll_for==1) {
              $for="Owner Primary Member";
              $qry =" and users_master.user_type =0 and users_master.member_status=0 AND unit_master.unit_status=1";
            }else if ($poll_for==2) {
              $for="Tenant Primary Member";
              $qry =" and users_master.user_type =1 and users_master.member_status=0 AND unit_master.unit_status=3";
            }else if ($poll_for==3) {
              $for="Owner & Tenant Primary Member";
                        // /IS_660
              $qry =" AND ((users_master.user_type=0  and   users_master.member_status=0 AND unit_master.unit_status=1)  OR (  users_master.user_type=1  and   users_master.member_status=0 AND unit_master.unit_status=3) )  ";
            }

                $totalUnit=$d->count_data_direct("user_id"," unit_master,users_master","users_master.delete_status=0 AND users_master.user_status=1 AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id'  $qry"); 
                 
                    echo $totalUnit-$totalParticiapte;
                  ?>
                </p>
              </div>
            </div>
          </div>
           <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Vote For</th>
                        <th>Branch - Employee</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  $i=1;
                  $q=$d->select("voting_result_master,unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=voting_result_master.unit_id AND voting_result_master.user_id=users_master.user_id AND voting_result_master.voting_id = '$vId'");
                  while ($empData=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                      
                        <td><?php echo $i++; ?></td>
                        <td><?php 
                            $vq=$d->select("voting_option_master","voting_option_id='$empData[voting_option_id]'");
                            $opData=mysqli_fetch_array($vq);
                            echo  $opData['option_name'];
                        ?></td>
                        <td><?php echo $empData['user_full_name']; ?>-<?php echo $empData['user_designation']; ?> (<?php echo $empData['block_name']; ?>)</td>
                     
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
          </div>
          <div class="card-body border-top">
            <?php if ($totalParticiapte>0) {  
              $labelsValue = implode(",", $optionArrayResult);
             $pollLabels = implode('","', $optionArray);
             $pollLabels = '"'.$pollLabels.'"';
              ?>
              
              <script>
                $(function() {
            "use strict";
              
                 var options = {
                  series: [{
                  data: [<?php echo $labelsValue;?>]
                }],
                  chart: {
                  height: 350,
                  type: 'bar',
                  toolbar: {
                        show: false
                    },
                  events: {
                    click: function(chart, w, e) {
                      // console.log(chart, w, e)
                    }
                  }
                },
                plotOptions: {
                  bar: {
                    columnWidth: '45%',
                    distributed: true,
                  }
                },
                dataLabels: {
                  enabled: false
                },
                legend: {
                  show: false
                },
                xaxis: {
                  categories: [<?php echo $pollLabels;?>],
                  labels: {
                    style: {
                      fontSize: '12px'
                    }
                  }
                }
                };

                var chart = new ApexCharts(document.querySelector("#chart"), options);
                chart.render();

              });
              </script>
              <div id="chart" ></div>
            <?php }?>
          </div>
         
         
        </div>
        
      </div>
    </div>
  </div>
<?php } ?>

</body>


<style type="text/css">
  @media print {
    #printPageButton {
      display: none;
    }
  }
</style>

<!-- Bootstrap core JavaScript-->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<!-- simplebar js -->
<script src="assets/plugins/simplebar/js/simplebar.js"></script>
<!-- waves effect js -->
<script src="assets/js/waves.js"></script>
<!-- sidebar-menu js -->
<script src="assets/js/sidebar-menu.js"></script>
<!-- Custom scripts -->
<script src="assets/js/app-script.js"></script>
<script type="text/javascript">
  function printDiv(divName) {
   var printContents = document.getElementById(divName).innerHTML;
   var originalContents = document.body.innerHTML;
   document.body.innerHTML = printContents;
   window.print();
   document.body.innerHTML = originalContents;
   Android.print('print');
  }

   // window.onload = function() { printDiv('printableArea'); }

</script>
</body>

</html>
<?php } else{
  echo "Invalid Request!!!";
}?>