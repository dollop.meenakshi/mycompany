<?php
// $q = $d->select("society_master","society_id = '$society_id'");
// $bData = $q->fetch_assoc();
// extract($bData);
if(isset($_SESSION['reload_page']))
{
    unset($_SESSION['reload_page']);
    echo ("<script LANGUAGE='JavaScript'>window.location.href='adminMenuOrdering';</script>");
}
?>
<style>
    .block {
        border: 1px solid #f1e8e2;
        background: #fff;
    }
    .block-title {
        font-family: Arial;
        color: #4c4743;
        padding: 0 10px;
        height: 33px;
        line-height: 33px;
        position: relative;
    }
    .sortable {
        list-style: none;
        padding-left: 0;
        cursor: pointer;
    }
    .sortable ul {
        margin-left: 25px;
    }
    .ui-sortable-helper {
        box-shadow: rgba(0,0,0,0.15) 0 3px 5px 0;
        width: 300px!important;
        height: 33px!important;
    }
    .sortable-placeholder {
        height: 35px;
        background: #e3dcd7;
    }
</style>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row mb-2">
        <div class="col-sm-9">
            <h4 class="page-title"><i class="fa fa-list"></i>Admin Menu Reordering</h4>
        </div>
        <div class="col-sm-3 text-right">
            <form method="post" action="controller/menuController.php">
                <input type="hidden" name="regenerateMenuJson" value="regenerateMenuJson"/>
                <button type="submit" class="btn btn-sm btn-primary">Reload Menu</button>
            </form>
        </div>
      </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-3 col-md-3 col-sm-1">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-10" id="accordion">
                                <ul class="sortableMainMenu list-unstyled" id="sortable">
                            <?php
                                $q = $d->selectRow("mm.menu_id,mm.menu_name,mm.sub_menu,mm.menu_link","master_menu AS mm","mm.parent_menu_id = 0 AND mm.page_status = 0","ORDER BY mm.order_no ASC");
                                while($data = $q->fetch_assoc())
                                {
                                    extract($data);
                            ?>
                                    <li class="pt-1 pb-1">
                                        <div class="block block-title bg-primary text-white mainMenuId" data-post-id="<?php echo $menu_id; ?>" data-toggle="collapse" data-target="#collapse<?php echo $menu_id; ?>" aria-expanded="true" aria-controls="collapse<?php echo $menu_id; ?>">
                                            <?php echo $menu_name;
                                            if($menu_link == "#")
                                            {
                                            ?>
                                            <span class="float-right">
                                                <i class="fa fa-chevron-down text-white" aria-hidden="true"></i>
                                            </span>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    <?php
                                        if($sub_menu == 1)
                                        {
                                            $q1 = $d->selectRow("mm.menu_id,mm.menu_name","master_menu AS mm","mm.parent_menu_id = '$menu_id' AND mm.page_status = 0","ORDER BY mm.order_no ASC");
                                            if(mysqli_num_rows($q1) > 0)
                                            {
                                    ?>
                                        <div id="collapse<?php echo $menu_id; ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <ul class="sortableSub list-unstyled" id="parent_id<?php echo $menu_id; ?>">
                                            <?php
                                                while($data1 = $q1->fetch_assoc())
                                                {
                                        ?>
                                                <li>
                                                    <div class="block block-title bg-dark text-white subMenuId" data-post-id="<?php echo $data1['menu_id']; ?>"><?php echo $data1['menu_name']; ?></div>
                                                </li>
                                        <?php
                                                }
                                        ?>
                                            </ul>
                                        </div>
                                        <?php
                                            }
                                        }
                                    ?>
                                    </li>
                            <?php
                                }
                            ?>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->