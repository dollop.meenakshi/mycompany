  <?php error_reporting(0);
  $dId  = (int)$_GET['dId'];
  $bId  = (int)$_GET['bId'];
  $week_days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Shift Timing</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
              <a href="addShiftTiming"  class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
              <a href="javascript:void(0)" onclick="DeleteAll('deleteShiftTiming');" class="btn  btn-sm btn-danger pull-right mr-1" ><i class="fa fa-trash-o fa-lg"></i> Delete </a>
            </div>
        </div>
     </div>
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr> 
                       <th>#</th>
                        <th>Sr.No</th> 
                        <th>Action</th>
                        <th>Shift Code</th>
                        <th>Shift Name</th>
                        <th>Employees</th>
                        <th>Shift Start</th>                      
                        <th>Shift End</th>                      
                        <th>Week Off </th>                      
                        <th>Per Day Hour</th>                                           
                    </tr>
                </thead>
                <tbody>
                   <?php 
                       
                        $i=1;
                        $q=$d->selectRow('shift_timing_master.*,(SELECT COUNT(*) FROM users_master WHERE users_master.shift_time_id = shift_timing_master.shift_time_id AND users_master.delete_status=0 ) AS shiftCOunt',"shift_timing_master","shift_timing_master.society_id='$society_id' AND shift_timing_master.is_deleted = 0  ",'ORDER BY shift_timing_master.shift_name ASC ');
                      $counter = 1;
                      while ($data=mysqli_fetch_array($q)) {
                          $arData = array();
                          for($i=0; $i<count($week_days); $i++){
                            if($data['week_off_days'] !="")
                            {
                              $week_off_days = explode(',',$data['week_off_days']);
                              if(in_array($i, $week_off_days)){
                                array_push($arData,$week_days[$i]);
                              } 
                            }
                          }
                          if(!empty($arData)){
                            $showOff = implode(',',$arData);
                          }
                          else
                          {
                            $showOff = "";
                          }
                      if($data['shift_type']=="Day"){
                        $shift_type = '<i data-toggle="tooltip" title="Day Shift" class="fa fa-sun-o" style="color:#e31b12;" aria-hidden="true"></i>';
                      }
                      else{
                        $shift_type = '<i data-toggle="tooltip" title="Night Shift" class="fa fa-moon-o" style="color:black;" aria-hidden="true"></i>';
                      }
                     ?>
                    <tr>
                   
                       <td class="text-center">
                         
                          <input  <?php if($data['shiftCOunt']>0) { echo 'disabled'; } ?>  type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['shift_time_id']; ?>">                      
                           
                        </td>
                        <td><?php echo $counter++; ?></td>
                        <td>
                         <div class="d-flex align-items-center">
                          <form action="addShiftTiming" method="post" accept-charset="utf-8" class="mr-2">
                              <input type="hidden" name="shift_time_id" value="<?php echo $data['shift_time_id']; ?>">
                              <input type="hidden" name="edit_shift_timing" value="edit_shift_timing">
                              <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button> 
                            </form>
                            <button type="submit" class="btn mr-1 btn-sm btn-primary" onclick="shiftTimingModal(<?php echo $data['shift_time_id']; ?>)"> <i class="fa fa-eye"></i></button>
                            <a href="addShiftTiming?shftId=<?php echo $data['shift_time_id']; ?>" class="btn  btn-sm btn-warning">
                              <i class="fa fa-clone" aria-hidden="true"></i>
                            </a>
                         </div>
                        </td>
                       <td><?php echo 'S'.$data['shift_time_id']; ?>
                       <td><?php echo $data['shift_name']." ". $shift_type; ?>
                        </td>
                       <td> <a href="userShift?sId=<?php echo $data['shift_time_id']; ?>">
                        <?php if($data['shiftCOunt']>0){ echo $data['shiftCOunt']; } ?>
                       </a></td>
                       <td><?php  echo date("h:i A", strtotime($data['shift_start_time'])); ?></td>
                       <td><?php echo date("h:i A", strtotime($data['shift_end_time'])); ?></td>
                       <td><?php echo $showOff; ?> </td>
                      
                       
                       <td><?php if($data['per_day_hour'] !="00:00:00"){ echo $data['per_day_hour']; } else { echo ""; } ?></td>
                       
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>
<div class="modal fade" id="detail_view">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Shift Timing Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="shift_timing">

        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
