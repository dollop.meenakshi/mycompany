<?php 
$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$salt="LkSmIDpkYr";

if(!isset($txnid)) {
  // header("location:buyPlan");
  echo ("<script LANGUAGE='JavaScript'>window.location.href='buyPlan';</script>");
}
$q=$d->select("transection_master","payment_txnid='$txnid'");
$data = mysqli_fetch_array($q);
extract($data);
// print_r($_REQUEST);
extract($_REQUEST);
// Salt should be same Post Request 

If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
  } else {
        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
         }
		 $hash = hash("sha512", $retHashSeq);
      $invoice_no= "0".date('ymdi').$udf1;

  $a1= array (
    'user_mobile'=> $udf3,
    'payment_mode'=> $mode,
    'transection_amount'=> $amount,
    'transection_date'=> $addedon,
    'payment_status'=> $status,
    'payuMoneyId'=> $payuMoneyId,
    'payment_txnid'=> $txnid,
    'payment_firstname'=> $firstname,
    'payment_phone'=> $udf3,
    'payment_email'=> $udf4,
    'bank_ref_num'=> $bank_ref_num,
    'bankcode'=> $bankcode,
    'name_on_card'=> $name_on_card,
    'cardnum'=> $cardnum,
    'error_Message'=> $error_Message,
    'invoice_no'=> $invoice_no,
     );

  	 $traDate= DateTime::createFromFormat("Y-m-d H:i:s", $addedon)->format("Y-m-d");

   	$qq=$d->selectRow("payment_txnid","transection_master","payment_txnid='$txnid'");
	  $oT=mysqli_fetch_array($qq);
	  if($oT>0) {
	     $d->update("transection_master",$a1,"payment_txnid='$txnid'");
	  } else {
	    $d->insert("transection_master",$a1);
	  }
		  $q=$d->select("package_master","package_id='$udf2'","");
		  $row=mysqli_fetch_array($q);
	if (!isset($_SESSION['package_update'])) {
		  $prev_expire_data= $sData['plan_expire_date'];
		  $no_month=$row['no_of_month'];

		  $effectiveDate = strtotime("+$no_month months", strtotime($prev_expire_data)); // returns timestamp
		  $new_plan_expire_date=date('Y-m-d',$effectiveDate);

		  $m->set_data('package_id',$udf2);
	      $m->set_data('trial_days',0);
	      $m->set_data('plan_expire_date',$new_plan_expire_date);

	      $a5 =array(
	          'package_id'=>$m->get_data('package_id'),
	          'trial_days'=>$m->get_data('trial_days'),
            'plan_expire_date'=>$m->get_data('plan_expire_date'),
	          'last_renew_date'=>date('Y-m-d'),
	      );
	      $d->update("society_master",$a5,"society_id='$udf1'");
	      $_SESSION['package_update']="Package Updated";

     $societyLngName=  $xml->string->society;
        $msg= "Dear $firstname,\nWe thank you for the payment made of Rs. $amount as against the MyCo $societyLngName Plan\nYour current plan expires on $new_plan_expire_date\n\nThank you, Team MyCo";
	}
 ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Payment Success</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="welcome">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Payment Success</li>
        </ol>
        <div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<div class="alert-icon">
			 <i class="fa fa-check-circle"></i>
			</div>
			<div class="alert-message">
			  <span><strong>Success!</strong> Your Payment Received Successfullly.</span>
			</div>
		</div>
      </div>
    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card" id="printableArea">
          <div class="card-body">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h3>
                Invoice
                <small>#<?php echo $invoice_no; ?></small>
              </h3>
            </section>

            <!-- Main content -->
            <section class="invoice">
              <!-- title row -->
              <div class="row mt-3">
                <div class="col-lg-6">
                  <h4><i class="fa fa-building"></i> MyCo</h4>
                   <address>
                    <strong>MyCo</strong><br>
                  </address>
                  <span><?php echo $xml->string->tax;?> No: </span>
                </div>
                <div class="col-lg-6 text-right">
                  <h5 class="">Date: <?php echo $traDate; ?></h5>
                  <b>Invoice #<?php echo $invoice_no; ?></b><br>
                  <b>Order ID:</b> <?php echo $payuMoneyId ?><br>
                  <b>Payment Date:</b> <?php echo $addedon; ?><br>
                </div>
              </div>

              <hr>
              <div class="row invoice-info">
                
                <div class="col-sm-12 invoice-col">
                  Customer Details
                  <address>
                    <strong><?php echo $_COOKIE['society_name']; ?></strong><br>
                    <?php echo $sData['society_address']; ?>
                  </address>
                  <p><?php echo $xml->string->tax;?>  No: </p>

                </div><!-- /.col -->

              </div><!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Qty</th>
                        <th>Package</th>
                        <th>No Of Month #</th>
                        <th>Description</th>
                        <th>Subtotal</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td><?php echo $productinfo; ?></td>
                        <td><?php echo $row['no_of_month'];?></td>
                        <td><?php echo $row['packaage_description'];?></td>
                        <td><?php echo $currency; ?> <?php echo $amount; ?></td>
                      </tr> 
                      
                    </tbody>
                  </table>
                </div><!-- /.col -->
              </div><!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-lg-6 payment-icons">
                  <p class="lead">Payment Methods:</p>
                  <img src="img/PayUmoney_Logo.jpg" alt="PayUmoney">
                  
                </div><!-- /.col -->
                <div class="col-lg-6">
                  <div class="table-responsive">
                    <table class="table">
                      <tbody>
                        <tr>
                          <th style="width:50%">Subtotal:</th>
                          <td><?php echo $currency; ?> <?php echo $amount; ?></td>
                        </tr>
                       <!--  <tr>
                          <th>Tax (9.3%)</th>
                          <td><?php echo $currency; ?> 10.34</td>
                        </tr> -->
                        
                        <tr>
                          <th>Total:</th>
                          <td><?php echo $currency; ?> <?php echo $amount; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->

              <!-- this row will not appear when printing -->
              <hr>
              <div class="row no-print" id="printPageButton">
                <div class="col-lg-3">
                  <a href="#" onclick="printDiv('printableArea')"  class="btn btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
                </div>
                <div class="col-lg-9">
                  <div class="float-sm-right">
                    <button class="btn btn-primary m-1"><i class="fa fa-envelope"></i> Receive on Mail</button>
                  </div>
                </div>
              </div>
            </section><!-- /.content -->
          </div>
        </div>
      </div>
    </div><!--End Row-->

  </div>
  <!-- End container-fluid-->

    </div><!--End content-wrapper-->

 <script type="text/javascript">
 	function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
 </script>

 <style type="text/css">
 	@media print {
	  #printPageButton {
	    display: none;
	  }
	}
 </style>