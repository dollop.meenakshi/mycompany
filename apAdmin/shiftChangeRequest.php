<?php
error_reporting(0);
$dId = (int)$_REQUEST['dId'];
$bId = (int)$_REQUEST['bId'];
if(isset($_REQUEST['sId']))
{
    $sId = (int)$_REQUEST['sId'];
}
else
{
    $sId = 0;
}
$week_days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
?>
<div class="content-wrapper">
    <input type="hidden" name="dId_hidden" id="dId_hidden" value="<?php if(isset($_GET['dId']) && $_GET['dId'] !="") { echo $_GET['dId'];}?>">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title">Employee Shift Change Request</h4>
            </div>
        </div>
        <form action="" class="branchDeptFilter">
            <div class="row pt-2 pb-2">
                <?php include('selectBranchDeptForFilter.php'); ?>
                <div class="col-md-1 form-group">
                    <input class="btn btn-success btn-sm" type="submit" name="getReport"  value="Get Data">
                </div>
                
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
//                             error_reporting(E_ALL);
// ini_set('display_errors', '1');
                            $i = 1;
                            if(isset($dId) && $dId > 0)
                            {
                                $deptFilterQuery = " AND users_master.floor_id='$_GET[dId]'";
                            }
                            if(isset($sId) && $sId > 0)
                            {
                                $shftFilterQuery = " AND users_master.shift_time_id='$sId'";
                            }
                            $q = $d->selectRow("shift_change_request.shift_change_request_id,oldShiftTable.shift_name as oldShiftName,oldShiftTable.shift_time_id as oldShiftCode,oldShiftTable.shift_start_time as oldShiftStart ,oldShiftTable.shift_end_time as oldShiftEnd, newShiftTable.shift_name as newShiftName,newShiftTable.shift_time_id as newShiftCode,newShiftTable.shift_start_time as newShiftStart ,newShiftTable.shift_end_time as newShiftEnd ,users_master.user_token,users_master.device,users_master.user_id,users_master.user_full_name,users_master.user_designation,floors_master.floor_name,block_master.block_name","users_master,floors_master,block_master,shift_change_request LEFT JOIN shift_timing_master as oldShiftTable ON oldShiftTable.shift_time_id=shift_change_request.current_shift_id 
                                LEFT JOIN shift_timing_master as newShiftTable ON newShiftTable.shift_time_id=shift_change_request.new_shift_id
                                ","shift_change_request.changed_by_id=users_master.user_id AND   block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.society_id='$society_id' AND users_master.delete_status = 0 $deptFilterQuery $shftFilterQuery $blockAppendQueryUser");
                            $counter = 1;
                          
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Action</th>
                                        <th>Employee</th>
                                        <th>Department</th>
                                        <th>New Shift Name </th>
                                        <th>Current Shift Name </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($data = mysqli_fetch_array($q))
                                    {
                                        
                                    ?>
                                    <tr>
                                       
                                        <td><?php echo $counter++; ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                              <form action="controller/ShiftTimingController.php" method="post" accept-charset="utf-8" class="mr-2">
                                              <input type="hidden" name="shift_change_request_id" value="<?php echo $data['shift_change_request_id']; ?>">
                                              <input type="hidden" name="shiftChangeApprove" value="shiftChangeApprove">
                                              <input type="hidden" name="user_full_name" value="<?php echo $data['user_full_name']; ?>">
                                              <input type="hidden" name="user_id" value="<?php echo $data['user_id']; ?>">
                                              <input type="hidden" name="user_token" value="<?php echo $data['user_token']; ?>">
                                              <input type="hidden" name="device" value="<?php echo $data['device']; ?>">
                                              <input type="hidden" name="new_shift_id" value="<?php echo $data['newShiftCode']; ?>">
                                              <input type="hidden" name="newShift" value="<?php echo 'S'.$data['newShiftCode']; ?>">
                                              <input type="hidden" name="oldShift" value="<?php echo 'S'.$data['oldShiftCode']; ?>">
                                              <button title="Approve Request" type="submit" class="btn form-btn btn-sm btn-primary ml-1" ><i class="fa fa-check"></i></button>
                                              </form>
                                              <form action="controller/ShiftTimingController.php" method="post" accept-charset="utf-8" class="mr-2">
                                                <input type="hidden" name="shift_change_request_id" value="<?php echo $data['shift_change_request_id']; ?>">
                                                  <input type="hidden" name="rejectShiftRequest" value="rejectShiftRequest">
                                                  <input type="hidden" name="user_full_name" value="<?php echo $data['user_full_name']; ?>">
                                                  <input type="hidden" name="user_id" value="<?php echo $data['user_id']; ?>">
                                                  <input type="hidden" name="user_token" value="<?php echo $data['user_token']; ?>">
                                                  <input type="hidden" name="device" value="<?php echo $data['device']; ?>">
                                                  <input type="hidden" name="newShift" value="<?php echo 'S'.$data['newShiftCode']; ?>">
                                                  <input type="hidden" name="oldShift" value="<?php echo 'S'.$data['oldShiftCode']; ?>">
                                                  <button title="Decline Shift Change" type="button" class="btn btn-sm form-btn btn-danger ml-1" ><i class="fa fa-times"></i></button>
                                              </form>
                                            </div>
                                        </td>
                                        <td><?php echo $data['user_full_name']; ?></td>
                                        <td><?php echo $data['floor_name']."(".$data['block_name'].")"; ?></td>
                                        <td><?php echo $data['newShiftName'].' ('.'S'.$data['newShiftCode'].')'; ?> </td>
                                        <td><?php echo $data['oldShiftName'].' ('.'S'.$data['oldShiftCode'].')'; ?> </td>
                                        
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
    <div class="modal fade" id="detail_view">
        <div class="modal-dialog ">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Shift Timing Details</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body px-2 "  style="align-content: center;">
                    <div class="row mx-0" id="shift_timing">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update_user_shift">
        <div class="modal-dialog ">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Update User Shift</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body px-2 "  style="align-content: center;">
                    <form id="updateUserShift">
                        <div class="row mx-0" id="shift_timing">
                            <div class="HideNoteClass">
                                <label  class=" error">Note:  <span class="error">If you change shift then all calculation done as per new shift</span></label>
                            </div>
                            <div class="form-group row w-100 mx-0">
                                <input type="hidden" name="userIdForShift" id= "userIdForShift">
                                <label for="input-10" class="col-lg-5 col-md-4 col-form-label col-12">Shift  <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-8 col-12" id="">
                                    <select class="form-control single-select" required name="shift_id" id="shift_id">
                                    </select>
                                </div>
                            </div>
                            <?php
                            $a_date = date('Y-m-d');
                            $m_date= date("Y-m-t", strtotime($a_date));
                            if($a_date == $m_date){ ?>
                            <div class="form-group HideNoteClass row w-100 mx-0">
                                <label for="input-10" class="col-lg-5 col-md-4 col-form-label col-12">Shift Change   <span class="required">*</span></label>
                                <select class="single-select"  id="apply_from" name="apply_from">
                                    <option value="">select</option>
                                    <option value="1">For Current Month</option>
                                    <option value="2">For Next Month</option>
                                </select>
                            </div>
                            <?php }
                            ?>
                            <div class="form-group  row w-100 mx-0 ">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-sm btn-primary " onclick="UpdateUserShift()">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update_user_shift_multi">
        <div class="modal-dialog ">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Update Multiple User Shift</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body px-2" style="align-content: center;">
                    <form id="updateUserShift">
                        <div class="row mx-0" id="shift_timing">
                            <div class="HideNoteClass">
                                <label  class=" error">Note:  <span class="error">If you change shift then all calculation done as per new shift</span></label>
                            </div>
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-5 col-md-4 col-form-label col-12">Shift <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-8 col-12" id="">
                                    <select class="form-control single-select" required name="shift_id_multi" id="shift_id_multi">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group  row w-100 mx-0 ">
                                <div class="col-md-12 text-center">
                                    <button type="button" class="btn btn-sm btn-primary" onclick="UpdateMulti()">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/plugins/select2/js/select2.min.js"></script>
    <script type="text/javascript">
    <?php
    if(isset($_GET['bId']) && $_GET['bId'] > 0)
    {
    ?>
        getFloorByBlockIdForEmpShift(<?php echo $_GET['bId']; ?>);
        getUserShiftByFloorIdMulti(<?php echo $_GET['dId']; ?>);
    <?php
    }
    ?>
    function popitup(url)
    {
        newwindow=window.open(url,'name','height=800,width=900, location=0');
        if (window.focus) {newwindow.focus()}
        return false;
    }
    function getFloorByBlockIdForEmpShift(id)
    {
        dId_hidden = $('#dId_hidden').val();
        var optionContent ="";
        $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getFloorByBlockId",
                block_id:id,
            },
            success: function(response)
            {
                optionContent = `<option value="0">All Departments</option>`;
                $.each(response.floor, function( index, value )
                {
                    if(dId_hidden==value.floor_id)
                    {
                        selected="selected";
                    }
                    else
                    {
                        selected="";
                    }
                    optionContent += `<option `+selected+`  value="`+value.floor_id+`" >`+value.floor_name+` (`+value.block_name+`)</option>`;
                });
                $('#floor_id').html(optionContent);
            }
        });
    }

    function getUserShiftByFloorIdMulti(f_id)
    {
        var options ="";
        $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getShiftByFloorId",
                floor_id:f_id,
            },
            success: function(response)
            {
                // $('#userIdForShift').val(user_id);
                options = `<option value="">Select Shift</option>`;
                $.each(response.shift, function(index,value)
                {
                    selected = "";
                    options += `<option `+selected+` value="`+value.shift_time_id+`" >`+value.shift_name+` (`+value.shift_time_view+`) `+value.show_off_name+`</option>`;
                });
                $('#shift_id_multi').html(options);
            }
        });
    }

    function openMultiModal()
    {
        var oTable = $("#example").dataTable();
        var val = [];
        $(".multiUpdateCheckbox:checked", oTable.fnGetNodes()).each(function(i)
        {
            val[i] = $(this).val();
        });

        if(val == "")
        {
            swal(
                'Warning !',
                'Please Select at least 1 user !',
                'warning'
            );
        }
        else
        {
            $('#update_user_shift_multi').modal();
        }
    }

    function UpdateMulti()
    {
        shift_id = $('#shift_id_multi').val();
        var oTable = $("#example").dataTable();
        var val = [];
        $(".multiUpdateCheckbox:checked", oTable.fnGetNodes()).each(function(i)
        {
            val[i] = $(this).val();
        });
        if(shift_id == '' || shift_id == 0 || shift_id == null)
        {
            swal(
                'Warning !',
                'Please Select Shift !',
                'warning'
            );
        }
        else
        {
            swal({
                title: "Are you sure?",
                text: "You want to update shift timinings!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) =>
            {
                if (willDelete)
                {
                    $.ajax({
                        url: "../residentApiNew/commonController.php",
                        cache: false,
                        type: "POST",
                        data: {
                            action:"updateUsersShiftMulti",
                            admin_id: <?php echo $_COOKIE['bms_admin_id']; ?>,
                            admin_name: "<?php echo $_COOKIE['admin_name']; ?>",
                            user_id:val,
                            shift_time_id:shift_id
                        },
                        success: function(response)
                        {
                            if(response.status == 200)
                            {
                                swal("Shift successfully updated",{
                                    icon: "success",
                                });
                                document.location.reload(true);
                            }
                            else
                            {
                                swal("Something Wrong!",{
                                  icon: "error",
                                });
                                document.location.reload(true);
                            }
                        }
                    });
                }
                else
                {
                    swal("data not updated!");
                }
            });
        }
    }
    </script>.