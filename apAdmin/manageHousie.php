<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-4 col-12">
        <h4 class="page-title"> Housie Game </h4>

      </div>
      <div class="col-sm-5 col-12">
        Note: Minimum 20 Questions need to Play Game
      </div>
      <div class="col-sm-3 col-12">
        <div class="btn-group float-sm-right">
          <a href="addHousieGame" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
          <a href="javascript:void(0)" onclick="DeleteAll('deleteHousieGame');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>


        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>Game Name</th>
                    <th>Date & Time</th>
                    <th>Reminder</th>
                    <th>Questions</th>
                    <th>Action</th>
                    <th>Result</th>
                    <th>Type</th>
                    <th>Sponsor</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $q=$d->select("housie_room_master","society_id='$society_id'","ORDER BY room_id DESC LIMIT 200");
                    $i = 0;
                    while($row=mysqli_fetch_array($q)) {
                      $qqq=$d->select("housie_questions_master","room_id ='$row[room_id]' AND society_id='$society_id'");
                      $today  = date("Y-m-d");
                      $timer_interval= $row['que_interval'];
                      $gameTime =$row['game_date'].' '.$row['game_time'];
                      $totalQue= mysqli_num_rows($qqq);
                      $totalTimeInSecond= $totalQue * $timer_interval ;
                      $time = new DateTime("$row[game_date] $row[game_time]");
                      $time->add(new DateInterval('PT' . $totalTimeInSecond . 'S'));
                      $gameEndTime = $time->format('Y-m-d H:i');
                      $gameEndTimeExpire = strtotime($gameEndTime);
                      $cTime= date("H:i:s");
                      $cTimeNow= date("Y-m-d H:i:s");
                      $i++;
                      $survey_question_master_qry=$d->select("housie_questions_master","room_id ='$row[room_id]' AND society_id='$society_id'");
                        $totalQue= mysqli_num_rows($survey_question_master_qry);

                      $expire = strtotime($row['game_date'].' '.$row['game_time']);
                      $today = strtotime("$today $cTime");
                  ?>
                    <tr class=" <?php if($totalQue<20) { echo 'bg-warning text-white';} ?>">
                      <td class='text-center'>
                        <?php
                         if($today >= $expire ){
                         $playedUser=  $d->count_data_direct("join_room_id","housie_join_room_master","room_id='$row[room_id]'");
                         if ($playedUser==0) { ?>
                           <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['room_id']; ?>">
                         <?php }
                          else  if($today <=  $gameEndTimeExpire && $row['room_status']==0) {
                          
                          }else if($row['room_status']==1 || $today >=  $gameEndTimeExpire) {
                         ?>
                           <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['room_id']; ?>">
                        <?php } } else { ?>
                          <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['room_id']; ?>">
                        <?php }?>
                        
                      </td>
                      <td><?php echo $i; ?></td>
                      <td class="tableWidth"><?php echo $row['game_name']; ?></td>
                      <td><?php echo date('d M Y',strtotime($row['game_date'])); ?> <?php echo date('h:i A',strtotime($row['game_time'])); ?></td>
                      <td>
                        <?php if ($totalQue>=20 && $today <=  $gameEndTimeExpire && $row['room_status']==0) { ?>
                         <form  action="controller/housieController.php" method="post" >
                          <input type="hidden" name="fulldateTime" value="<?php echo date('d M Y',strtotime($row['game_date'])); ?> <?php echo date('h:i A',strtotime($row['game_time'])); ?>">
                          <input type="hidden" name="room_id_notification" value="<?php echo $row['room_id']; ?>">
                          <button type="submit" class="form-btn btn btn-link btn-sm "><i class="fa fa-bell"></i> Reminder</button>
                        </form>
                      <?php } ?>
                      </td>
                      <td>
                        <form action="housieQuestions" method="get">
                        <input type="hidden" name="room_id" id="room_id" value="<?php echo $row['room_id'] ?>">
                          <button type="submit" class="btn btn-primary btn-sm " name="Questions">Questions (<?php  echo $totalQue; ?>)</button>
                          
                        </form>
                      </td>
                      <td>
                      <?php
                     
                      if($today >  $expire ) {
                        if ($totalQue>=20) {
                     ?>
                       <form   class="mr-2" action="addHousieGame" method="post">
                            <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>">
                             <input type="hidden" name="society_id" value="<?php echo $row['society_id']; ?>">
                             <input type="hidden" name="editGame" value="<?php echo 'editGame'; ?>">
                             <input type="hidden" name="rescheduleGame" value="<?php echo 'rescheduleGame'; ?>">
                            <input type="hidden" name="action" value="Edit">
                            <button type="submit" class="btn btn-sm btn-danger"> Reschedule</button>
                          </form>
                       <?php } } else   {
                        $timeFirst  = strtotime($gameTime);
                        $timeSecond = strtotime($cTimeNow);
                        $differenceInSeconds = $timeFirst - $timeSecond;
                        if($differenceInSeconds<900){ ?>

                            <button  onclick="swal('Edit option is disabled before 15 minutes of game start !');"   type="submit" class="btn btn-sm btn-warning"> Edit</button>
                       <?php  } else {
                        ?>
                         <form   class="mr-2" action="addHousieGame" method="post">
                            <input type="hidden" name="room_id" value="<?php echo $row['room_id']; ?>">
                             <input type="hidden" name="society_id" value="<?php echo $row['society_id']; ?>">
                             <input type="hidden" name="editGame" value="<?php echo 'editGame'; ?>">
                            <input type="hidden" name="action" value="Edit">
                            <button type="submit" class="btn btn-sm btn-warning"> Edit</button>
                           
                          </form>

                       <?php } }
                        if ($row['housie_type']=='1') { 
                       ?> 
                        <a  href="addHousieGame?id=<?php echo $row['room_id']; ?>&managePlayer=yes" class="btn btn-sm btn-danger mt-1"> Manage Players (<?php echo $d->count_data_direct("room_id","housie_participate_player","room_id='$row[room_id]'"); ?>)</a>
                        <?php } ?>
                      </td>
                      <td>
                        <?php
                         if($today >= $expire ){
                         $playedUser=  $d->count_data_direct("join_room_id","housie_join_room_master","room_id='$row[room_id]'");
                         if ($playedUser==0) {
                           echo "0 User Join This Game";
                         }
                          else  if($today <=  $gameEndTimeExpire && $row['room_status']==0) {
                            echo "Game Running";

                          }else if($row['room_status']==1 || $today >=  $gameEndTimeExpire) {
                         ?>
                          <form action="housieResult" method="post">
                            <input type="hidden" name="room_id" id="electionData" value="<?php echo $row['room_id'] ?>">
                            <button type="submit" class="btn btn-success btn-sm" name="housieResult">View Result</button>
                          </form>
                        <?php } } else { ?>
                          Game Not Started yet
                        <?php }?>
                      </td>
                      <td><?php if ($row['housie_type']=='0') { echo "<i class='text-success'>Public</i>";} else {
                        echo "<i class='text-danger'>Private</i>";
                      } ?>
                      </td>
                      <td><?php if ($row['sponser_photo']!='') { ?>
                        <img title="<?php echo $row['sponser_name']; ?>" src="../img/game/<?php echo $row['sponser_photo']; ?>" height="50" width="100"  alt="">
                      <?php } ?>
                      </td>
                     
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>