<?php 
extract($_REQUEST);
$vId =(int)$vId;
$q = $d->select("voting_master,voting_option_master","voting_master.voting_id=voting_option_master.voting_id AND voting_master.society_id = '$society_id' AND voting_master.voting_id = '$vId'");
$data = mysqli_fetch_array($q);
//$data = array_map("html_entity_decode", $data);
extract($data);


 ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->

     <?php ////IS_1041
 if(mysqli_num_rows($q) <= 0 ){?> 
  <div class="row">
      <div class="col-lg-12">
        <img src='img/no_data_found.png'>
      </div>
    </div>
 <?php }  else { ?>

  
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Poll Result</h4>
        
      </div>
      <div class="col-sm-3">
        <a href="pollResultPrint.php?id=<?php echo $voting_id ?>&society_id=<?php echo $data['society_id'] ?>" class="btn btn-success float-right"><i class="fa fa-print"></i> Print</a>
      </div>

    </div>
    <!-- End Breadcrumb-->



    <div class="row">
      <div class="col-lg-12">
       <div class="card profile-card-2">
       
        <div class="card-body ">
          <h5 class="card-title">Question: <?php echo $voting_question; ?></h5>
          <p><b>Description:</b> <?php echo $voting_description; ?></p>
          <p><b>Poll For:</b> <?php
          $election_for_string= $xml->string->election_for; 
          $electionArray = explode("~", $election_for_string);
           if ($poll_for==0) {
                        echo  $all= $xml->string->all.' '. $xml->string->floors; 
                        } else {
                         $qf=$d->selectRow("floor_name,block_name","floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.floor_id='$poll_for'");
                          $floorData=mysqli_fetch_array($qf);
                          echo $floorData['floor_name'].' ('.$floorData['block_name'].')' ;
                        }
                        ?></p>
          <p><b>Poll Option:   </b>
            <ul class="list-unstyled">
              <?php
              $i11=1;
              $optionArray = array();
              $optionArrayResult = array();
              $sq = $d->select("voting_option_master","voting_id = '$voting_id' AND society_id = '$society_id'");
              while($sData = mysqli_fetch_array($sq)){
                array_push($optionArray,$sData['option_name']);
                  $voteCount = $d->count_data_direct("voting_result_id","voting_result_master","voting_id = '$vId' AND voting_option_id='$sData[voting_option_id]'");
                array_push($optionArrayResult,$voteCount);
                ?>
              <li><?php echo $i11++;?>. <?php echo $sData['option_name']; ?></li>
              <?php  } ?>
            </ul>

          </p>
          <p><b>Poll Start Date:</b> <?php echo $voting_start_date;  ?>
          <p><b>Poll End Date:</b> <?php echo $voting_end_date;  ?>
          <p><b>Poll Attachment:</b> <a target="_blank" href="../img/voting_doc/<?php echo $voting_attachment;?>"><?php echo $voting_attachment;?></a>
          <p><b>Participated Members:</b> <?php 
                  $q1=$d->select("voting_result_master,unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=voting_result_master.unit_id AND voting_result_master.user_id=users_master.user_id AND  voting_result_master.voting_id = '$vId'");
                   echo $totalParticiapte= mysqli_num_rows($q1);
                   ?></p>
          <p><b>Not Participated Members:</b>

            <?php 
            $qry ="";
            if ($poll_for==0) {
              $for="All Family Members";
            } else if ($poll_for==1) {
              $for="Owner Primary Member";
              $qry =" and users_master.user_type =0 and users_master.member_status=0 AND unit_master.unit_status=1";
            }else if ($poll_for==2) {
              $for="Tenant Primary Member";
              $qry =" and users_master.user_type =1 and users_master.member_status=0 AND unit_master.unit_status=3";
            }else if ($poll_for==3) {
              $for="Owner & Tenant Primary Member";
                        // /IS_660
              $qry =" AND ((users_master.user_type=0  and   users_master.member_status=0 AND unit_master.unit_status=1)  OR (  users_master.user_type=1  and   users_master.member_status=0 AND unit_master.unit_status=3) )  ";
            }

                 $totalUnit=$d->count_data_direct("user_id","unit_master,users_master","users_master.delete_status=0 AND users_master.user_status=1 AND users_master.unit_id=unit_master.unit_id AND users_master.society_id='$society_id'  $qry"); 
                  echo $totalUnit-$totalParticiapte;
                   ?>
          </p>
        </div>

        <div class="card-body border-top">
          <?php if ($totalParticiapte>0) { 
            // print_r($optionArrayResult);
             $labelsValue = implode(",", $optionArrayResult);
             $pollLabels = implode('","', $optionArray);
             $pollLabels = '"'.$pollLabels.'"';
            ?>
            <script src="assets/js/jquery.min.js"></script>
             <script src="assets/plugins/apexcharts-bundle/js/apexcharts.min.js"></script>
            <script>
              
               $(function() {
            "use strict";
              
                var options = {
                    series: [{
                    data: [<?php echo $labelsValue;?>]
                  }],
                    chart: {
                    height: 350,
                    type: 'bar',
                    toolbar: {
                        show: false
                    },
                    events: {
                      click: function(chart, w, e) {
                        // console.log(chart, w, e)
                      }
                    }
                  },
                  plotOptions: {
                    bar: {
                      columnWidth: '45%',
                      distributed: true,
                    }
                  },
                  dataLabels: {
                    enabled: false
                  },
                  legend: {
                    show: false
                  },
                  xaxis: {
                   categories: [<?php echo $pollLabels;?>],
                    labels: {
                      style: {
                        fontSize: '12px'
                      }
                    }
                  }
                  };

                
                  var chart = new ApexCharts(document.querySelector("#barChart"), options);
                  chart.render();

              });
            </script>
             <div id="barChart" ></div>
          <?php }?>
        </div>
       
        <div class="table-responsive">

              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $xml->string->block; ?>-<?php echo $xml->string->unit; ?></th>
                        <th>Vote For</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  $i=1;
                  $q=$d->select("voting_result_master,unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=voting_result_master.unit_id AND voting_result_master.user_id=users_master.user_id AND voting_result_master.voting_id = '$vId'");
                  while ($empData=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php
                        
                        if(!empty($empData['user_full_name']))
                        {
                          echo $empData['user_full_name'] . " - ";
                        }
                        if(!empty($empData['user_designation']))
                        {
                          echo $empData['user_designation'] . " - ";    
                        }?> 
                       (<?php echo $empData['block_name']; ?>)</td>
                        <td><?php 
                            $vq=$d->select("voting_option_master","voting_option_id='$empData[voting_option_id]'");
                            $opData=mysqli_fetch_array($vq);
                            echo  $opData['option_name'];
                        ?></td>
                     
                    </tr>
                  <?php } ?>
                </tbody>
                
            </table>
            </div>



      </div>


</div>




</div>
<?php } ?>
</div>
<!-- End container-fluid-->
</div><!--End content-wrapper-->

<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=800, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>