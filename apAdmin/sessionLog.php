  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-6">
          <h4 class="page-title">Session Logs (Login/Logout)</h4>
        
        </div>
        <div class="col-sm-3">
          <form action="" method="get" >
          <select name="type" class="form-control single-select" onchange="this.form.submit()">
            <option <?php if($_GET['type']==0) { echo 'selected'; } ?> value="0"> Employee</option>
            <option <?php if($_GET['type']==1) { echo 'selected'; } ?> value="1"> Admin</option>
          </select>
          </form>
        </div>
       <div class="col-sm-3 text-right">
          <a href="#" class="btn btn-sm btn-danger  "><i class="fa fa-trash-o mr-1"></i> Delete</a>
       </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      <th class="deleteTh">
                          <input type="checkbox" class="selectAll" label="check all"  />
                      </th>
                      <th>#</th>
                      <th>Name</th>
                      <th>Designation</th>
                      <th>Mobile</th>
                      <th>Ip Address</th>
                      <th>Date Time</th>
                      <th>Type</th>
                      <th>Browser/Device</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $i=1;
                    if ($_GET['type']==1) {
                      $q = $d->select("session_log,bms_admin_master" ,"session_log.admin_id=bms_admin_master.admin_id AND bms_admin_master.admin_mobile!='9687271071' AND bms_admin_master.admin_mobile!='9737564998' AND session_log.admin_id!=0","order by session_log.sessionId  DESC LIMIT 500");
                    } else {
                      $q = $d->select("session_log,users_master" ,"session_log.user_id=users_master.user_id AND session_log.user_id!=0","order by session_log.sessionId  DESC LIMIT 500");
                    }
                    while ($data=mysqli_fetch_array($q)) {
                      // print_r($data);
                      if ($_GET['type']==1) {
                        $name = $data['admin_name'];
                        $mobile = $data['admin_mobile'];
                      } else {
                        $name = $data['user_full_name'];
                        $mobile = $data['user_mobile'];
                      }
                     ?>
                    <tr>
                    <td class='text-center'>
                      <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $data['sessionId']; ?>">
                    </td>
                      <td><?php echo $i++; ?></td>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $data["role_name"]; ?></td>
                        <td><?php echo $data["country_code"].' '.$mobile; ?></td>
                        <td><a target="_blank" href="https://www.opentracker.net/feature/ip-tracker?ip=<?php echo $data["ip_address"]; ?>&k="><?php echo $data["ip_address"]; ?></a></td>
                        <td><?php echo $data["loginTime"]; ?></td>
                        <td><?php if($data['login_type']==1) { echo "Logout"; }else { echo "Login";} ?></td>
                        <td><?php echo $data["browser"]; ?></td>
                        <!-- <td></td> -->
                    </tr>
                    <?php } ?>
                    
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



