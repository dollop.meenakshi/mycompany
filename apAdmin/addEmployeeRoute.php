<?php
if(isset($_POST['addEmployeeRouteBtn']) && $_POST['addEmployeeRouteBtn'] == "addEmployeeRouteBtn")
{
    extract($_POST);
}
if(isset($_POST['editEmployeeRoute']) && isset($_POST['route_employee_assign_id']))
{
    $route_employee_assign_id = $_POST['route_employee_assign_id'];
    $q = $d->select("route_employee_assign_master","route_employee_assign_id = '$route_employee_assign_id'");
    $data = $q->fetch_assoc();
    extract($data);
    $route_assign_week_days_arr = explode(',',$route_assign_week_days);
    $form_id = "employeeRouteAddForm";
    $add_lab = "Edit";
}
else
{
    $form_id = "employeeRouteAddForm";
    $add_lab = "Add";
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="<?php echo $form_id; ?>" action="controller/employeeRouteController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <i class="fa fa-plus"></i>
                            <?php echo $add_lab; ?> Employee Route
                            </h4>
                            <div class="form-group row">
                                <label for="route_id" class="col-sm-1 col-form-label">Route <span class="required">*</span></label>
                                <div class="col-sm-5">
                                    <select class="form-control single-select" id="route_id" name="route_id" required onchange="checkEmpRoute();">
                                        <option value="">-- Select --</option>
                                        <?php
                                        $cq = $d->selectRow("rm.route_id,rm.route_name,c.city_name","route_master AS rm JOIN cities AS c ON c.city_id = rm.city_id");
                                        while ($cd = $cq->fetch_assoc())
                                        {
                                        ?>
                                        <option <?php if($route_id == $cd['route_id']) { echo 'selected'; } ?> value="<?php echo $cd['route_id'];?>"><?php echo $cd['route_name'] . " (" . $cd['city_name'] . ")"; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="user_id" class="col-sm-1 col-form-label">User <span class="required">*</span></label>
                                <div class="col-sm-5">
                                    <select class="form-control single-select-new" id="user_id" name="user_id" required onchange="checkEmpRoute();">
                                        <option value="">-- Select --</option>
                                        <?php
                                            $qu = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","um.delete_status = 0 AND um.active_status = 0");
                                            while ($ud = $qu->fetch_assoc())
                                            {
                                            ?>
                                        <option <?php if($user_id == $ud['user_id']) { echo 'selected'; } ?> value="<?php echo $ud['user_id']; ?>"><?php echo $ud['user_full_name'] . " (" . $ud['block_name'] . " - " . $ud['floor_name'] . ")"; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="route_assign_week_days" class="col-sm-1 col-form-label">Week Days <span class="text-danger">*</span></label>
                                <div class="col-sm-11">
                                    <select class="form-control multiple-select" multiple id="route_assign_week_days" name="route_assign_week_days[]" required>
                                        <option value="">-- Select --</option>
                                        <option value="0" <?php if(isset($_POST['editEmployeeRoute']) && in_array(0, $route_assign_week_days_arr)){ echo 'selected'; } ?>>Sunday</option>
                                        <option value="1" <?php if(isset($_POST['editEmployeeRoute']) && in_array(1, $route_assign_week_days_arr)){ echo 'selected'; } ?>>Monday</option>
                                        <option value="2" <?php if(isset($_POST['editEmployeeRoute']) && in_array(2, $route_assign_week_days_arr)){ echo 'selected'; } ?>>Tuesday</option>
                                        <option value="3" <?php if(isset($_POST['editEmployeeRoute']) && in_array(3, $route_assign_week_days_arr)){ echo 'selected'; } ?>>Wednesday</option>
                                        <option value="4" <?php if(isset($_POST['editEmployeeRoute']) && in_array(4, $route_assign_week_days_arr)){ echo 'selected'; } ?>>Thursday</option>
                                        <option value="5" <?php if(isset($_POST['editEmployeeRoute']) && in_array(5, $route_assign_week_days_arr)){ echo 'selected'; } ?>>Friday</option>
                                        <option value="6" <?php if(isset($_POST['editEmployeeRoute']) && in_array(6, $route_assign_week_days_arr)){ echo 'selected'; } ?>>Saturday</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                            <div class="form-footer text-center">
                                <input type="hidden" name="addEmployeeRoute" value="addEmployeeRoute">
                                <?php
                                if(isset($editEmployeeRoute))
                                {
                                ?>
                                <input type="hidden" class="form-control" name="route_employee_assign_id" id="route_employee_assign_id" value="<?php echo $route_employee_assign_id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script>

function checkEmpRoute()
{
    var route_id = $('#route_id').val();
    var user_id = $('#user_id').val();
    if(route_id != "" && user_id != "" && user_id != null && route_id != null)
    {
        $.ajax({
            url: 'controller/employeeRouteController.php',
            data: {checkEmployeeRouteAdded:"checkEmployeeRouteAdded",user_id:user_id,route_id:route_id,csrf:csrf},
            type: "post",
            success: function(response)
            {
                response = JSON.parse(response);
                if(response == 1)
                {
                    $("#user_id option").prop("selected", false);
                    $("#user_id").trigger('change');
                    $("#route_id option").prop("selected", false);
                    $("#route_id").trigger('change');
                    swal("User already added in this route !", {icon: "error",});
                }
            }
        });
    }
}

</script>