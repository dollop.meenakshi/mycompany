<?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Employee Detail Change Request</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
              <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <!-- <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteLeaveType');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>                        
                        <th>Name</th>
                        <th>Personal Info Req Date</th>                      
                        <th>Contact Detail Req Date</th>                      
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("users_master, floors_master, block_master, user_employment_details","users_master.society_id='$society_id' AND users_master.floor_id=floors_master.floor_id AND users_master.block_id=block_master.block_id AND user_employment_details.user_id=users_master.user_id AND ((users_master.contact_details_change_req != '' AND users_master.contact_details_change_req IS NOT NULL) OR (users_master.personal_info_change_req != '' AND users_master.personal_info_change_req IS NOT NULL)) $blockAppendQueryUser");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                        <td><?php echo $counter++; ?></td>
                        <td><?php echo $data['user_full_name']; ?> (<?php echo $data['user_designation']; ?>)</td>
                        <td>
                            <?php if($data['personal_info_change_req'] != null){
                                echo date("d M Y h:i A", strtotime($data['personal_info_req_date']));
                                $personalInfoNewData = $data['personal_info_change_req'];
                                $personalInfoOldData = json_encode(array(
                                        "blood_group" => $data['blood_group'],
                                        "gender" => $data['gender'],
                                        "intrest_hobbies" => $data['intrest_hobbies'],
                                        "language_known" => $data['language_known'],
                                        "marital_status" => $data['marital_status'],
                                        "member_date_of_birth" => $data['member_date_of_birth']!='0000-00-00'?$data['member_date_of_birth']:'',
                                        "nationality" => $data['nationality'],
                                        "professional_skills" => $data['professional_skills'],
                                        "special_skills" => $data['special_skills'],
                                        "total_family_members" => $data['total_family_members'],
                                        "wedding_anniversary_date" => $data['wedding_anniversary_date']!='0000-00-00'?$data['wedding_anniversary_date']:'',
                                        ));
                                ?>
                                <button type="button" class="btn btn-sm btn-info ml-2 mr-2" onclick='viewPersonalInfo(<?php echo $personalInfoNewData; ?>, <?php echo $personalInfoOldData; ?>, <?php echo $data["user_id"]?>)'> <i class="fa fa-eye"></i></button>
                                
                                <?php }
                                ?>
                        </td>
                        <td>
                            <?php if($data['contact_details_change_req'] != null){
                                echo date("d M Y h:i A", strtotime($data['contact_details_change_req_date'])); 
                                $contactDetailsNewData = $data['contact_details_change_req'];
                                $contactDetailsOldData = json_encode(array(
                                        "alt_mobile" => $data['alt_mobile'],
                                        "country_code_alt" => $data['country_code_alt'],
                                        "country_code_emergency" => $data['country_code_emergency'],
                                        "country_code_whatsapp" => $data['country_code_whatsapp'],
                                        "emergency_number" => $data['emergency_number'],
                                        "last_address" => $data['last_address'],
                                        "permanent_address" => $data['permanent_address'],
                                        "personal_email" => $data['personal_email'],
                                        "user_email" => $data['user_email'],
                                        "whatsapp_number" => $data['whatsapp_number'],
                                        ));
                                ?>
                                <button type="button" class="btn btn-sm btn-info ml-2 mr-2" onclick='viewContactDetail(<?php echo $contactDetailsNewData; ?>, <?php echo $contactDetailsOldData; ?>, <?php echo $data["user_id"]?>)'> <i class="fa fa-eye"></i></button>
                                
                            <?php }
                            ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="personalInfoModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white" id="modalHeadName"></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body"> 
          <div class="table-responsive"> 
            <table class="table table-bordered dataTable no-footer">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Old</th>
                        <th>New</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody id="personalInfoData">
                    
                </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="d-flex align-items-center">
          <form method="POST" action="controller/userController.php">
            <input type="hidden" name="changeEmployeeDetail" value="changeEmployeeDetail">
            <input type="hidden" name="user_id" class="user_id" value="">
            <input type="hidden" name="new_data" id="new_data" value="">
            <input type="hidden" name="old_data" id="old_data" value="">
            <input type="hidden" name="user_details_menu_type" class="user_details_menu_type" value="">
            <button type="submit" class="btn btn-sm btn-primary mr-2 form-btn">Accept</button>
          </form>
          <form method="POST" action="controller/userController.php">
            <input type="hidden" name="rejectEmployeeDetailChangeRequest" value="rejectEmployeeDetailChangeRequest">
            <input type="hidden" name="user_id" class="user_id" value="">
            <input type="hidden" name="user_details_menu_type" class="user_details_menu_type" value="">
            <button type="submit" class="btn btn-sm btn-danger form-btn">Reject</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
