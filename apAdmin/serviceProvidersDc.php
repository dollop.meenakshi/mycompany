<?php
extract(array_map("test_input" , $_REQUEST));
error_reporting(0);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "getCountriesMaster=getCountriesMaster&language_id=1&countryids=$countryids");

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'key: bmsapikey'
));

$server_output = curl_exec($ch);

curl_close ($ch);
$server_output=json_decode($server_output,true);
?>

<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-4">
        <h4 class="page-title">Service Providers </h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="welcome">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Service Providers </li>
        </ol>
      </div>
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
        <div class="btn-group float-sm-right">
          <a href="addVendor"  class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New </a>
          <a href="#" data-toggle="modal" data-target="#bulkUpload" class="btn btn-info btn-sm waves-effect waves-light"><i class="fa fa-file-excel-o"></i> Bulk Import </a>
          <a href="#" onclick="DeleteAll('deleteServiceProvider');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
      </div>
    </div>
    <div class="row pt-2 pb-2">
      <div class="col-lg-12">
        <form action="" method="get" accept-charset="utf-8">

          <div class="form-group row">
            <label for="country_id" class="col-sm-1 col-form-label"> Country <span class="required">*</span></label>
            <div class="col-sm-3">
              <select type="text" required="" id="country_id" onchange="getStates();" class="form-control single-select" name="countryId">
                <option value="">-- Select --</option>
                <?php 
                for ($ic=0; $ic <count($server_output['countries']) ; $ic++) { 
                  ?>
                  <option <?php if( isset($_GET['countryId']) && $server_output['countries'][$ic]['country_id']==$_GET['countryId']) {echo "selected";} ?> value="<?php echo $server_output['countries'][$ic]['country_id'];?>"><?php echo $server_output['countries'][$ic]['name'];?></option>
                <?php }?>
              </select>
            </div>
            <label for="state_id" class="col-sm-1 col-form-label"> State <span class="required">*</span></label>
            <div class="col-sm-3">
              <?php  if(isset($_GET['sId'])) {
                 $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS,
                              "getState=getState&country_id=$_GET[countryId]&language_id=1");

                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                  
                  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'key: bmsapikey'
                  ));

                  $server_output = curl_exec($ch);

                  curl_close ($ch);
                  $server_output=json_decode($server_output,true);
               ?>
                <select type="text" onchange="getCity();"  required="" class="form-control single-select" id="state_id" name="sId">
                  <?php
                   for ($is=0; $is <count($server_output['states']) ; $is++) { 
                    ?>
                    <option <?php if( isset($_GET['sId']) && $server_output['states'][$is]['state_id']==$_GET['sId']) {echo "selected";} ?> value="<?php echo $server_output['states'][$is]['state_id'];?>"><?php echo $server_output['states'][$is]['name'];?></option>
                  <?php }  ?>
                </select>
              <?php } else { ?>
                <select type="text" onchange="getCity();"  required="" class="form-control single-select" id="state_id" name="sId">
                  <option value="">-- Select --</option>
                </select>
              <?php } ?>
            </div>

            <label for="input-101" class="col-sm-1 col-form-label"> City <span class="required">*</span></label>
            <div class="col-sm-2">
              <?php  if(isset($_GET['cId'])) {
                $ch = curl_init();
                      curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS,
                                  "getCity=getCity&state_id=$_GET[sId]&language_id=1");

                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                      
                      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'key: bmsapikey'
                      ));

                      $server_output = curl_exec($ch);

                      curl_close ($ch);
                      $server_output=json_decode($server_output,true);
               ?>
                <select type="text"  required="" class="form-control single-select" id="city_id" name="cId">
                  <?php
                   for ($icity=0; $icity <count($server_output['cities']) ; $icity++) { 
                    ?>
                    <option <?php if( isset($_GET['cId']) && $server_output['cities'][$icity]['city_id']==$_GET['cId']) {echo "selected";} ?> value="<?php echo $server_output['cities'][$icity]['city_id'];?>"><?php echo $server_output['cities'][$icity]['name'];?></option>
                  <?php }  ?>
                </select>
              <?php } else { ?>
                <select  type="text" required="" class="form-control single-select" name="cId" id="city_id">
                  <option value="">-- Select --</option>

                </select>
              <?php } ?>
            </div>
            <div for="input-101" class="col-sm-1">
              <button class="btn btn-success" type="submit">Filter</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- End Breadcrumb-->
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <!-- <table id="checkAllwithAllDelete" class="table table-bordered"> -->
            <table id="spTable" class="table table-bordered">
              <thead>
                <tr>
                  <th class="deleteTh">
                    <input type="checkbox" id="checkUncheckAllDelete" value="CheckAll"  />
                  </th>
                  <th>#</th>
                  <th>#</th>
                  <th>Company</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $i=1;
                  $queryAry=array();
                  if ($countryId!=0) {

                    $query= "country_id='$countryId'";
                    array_push($queryAry, $query);
                  }
                  if ($sId!=0) {
                    $query= "state_id='$sId'";
                    array_push($queryAry, $query);
                  }
                  if ($cId!=0) {
                    $atchQuery= "city_id='$cId'";
                    array_push($queryAry, $atchQuery);
                  } 

                  $appendQuery= implode(" AND ", $queryAry);

                  $where = "";
                  if($appendQuery){
                    $where = "AND ".$appendQuery;
                  }
                  $q=$d->select("local_service_provider_users","service_provider_status=1 $where $countryAppendQuerySP","ORDER BY service_provider_users_id DESC");
                  while ($data=mysqli_fetch_array($q)) {
                    $spData[] = $data;
                  }
                  for ($j=0; $j < count($spData); $j++) { 
                    $id = $spData[$j]['service_provider_users_id'];
                ?>
                <tr>
                  <td class='text-center'>
                    <?php 
                      $count = $d->count_data_direct("common_service_providers_id","common_service_providers","service_provider_users_id='$id'");
                      if($count==0){
                    ?>
                     <input type="checkbox" class="sp_delete" value="<?php echo $spData[$j]['service_provider_users_id']; ?>" id="common_<?php echo $spData[$j]['service_provider_users_id']; ?>">
                    <?php } ?>
                  </td>
                  <td><?php echo $i++; ?></td>
                  <td><img width="75" src="../img/local_service_provider/local_service_users/<?php echo $spData[$j]['service_provider_user_image']; ?>"></td>
                  <td>
                    <?php echo $count ?>
                    <a name="manageSp" value="manageSp" href="CommonServiceProviderSetting?id=<?php echo $spData[$j]['service_provider_users_id']; ?>" class="btn btn-sm btn-primary"><i class="fa fa-tasks"></i> </a>
                  </td>
                  <td class="tableWidth"><?php echo $spData[$j]['service_provider_name'];
                  if ($kyc_status==1) {
                    echo " <img src='../img/check.png' width='15'>";
                  }
                  ?></td>
                  <td class="tableWidth"><?php echo $spData[$j]['category_name']; ?></td>
                  <td><?php echo $spData[$j]['service_provider_phone']; ?></td>
                  <td class="tableWidth"><?php echo $spData[$j]['service_provider_email']; ?></td>
                  <td>
                    <form class="d-inline-block" action="addVendor" method="post">
                      <input type="hidden" name="service_provider_users_id" value="<?php echo $spData[$j]['service_provider_users_id']; ?>">
                      <button name="editSp" value="editSp" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></button>
                    </form>

                    <a class="d-inline-block btn btn-sm btn-warning" href="serviceProviderDetails?sp_id=<?php echo $spData[$j]['service_provider_users_id']; ?>"><i class="fa fa-eye"></i></a>
                    <br>
                    <br>
                    <?php if($spData[$j]['service_provider_status']=="0"){ ?>
                      <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $spData[$j]['service_provider_users_id']; ?>','SpDeactive');" data-size="small"/>
                    <?php } else { ?>
                      <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $spData[$j]['service_provider_users_id']; ?>','SpActive');" data-size="small"/>
                    <?php } ?>
                  </td>

                </tr>

                <?php } ?> 
              </tbody>

            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php
for ($md=0; $md < count($spData) ; $md++) { 
  $id = $spData[$md]['service_provider_users_id'];
  $count = $d->count_data_direct("common_service_providers_id","common_service_providers","service_provider_users_id='$id'");
  if($count==0){
?>
    <input style="display: none" class="multiDelteCheckbox" value="<?php echo $spData[$md]['service_provider_users_id']; ?>" type="checkbox" id="common_<?php echo $spData[$md]['service_provider_users_id']; ?>_hidden" name="media_select[]">
<?php } } ?>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
 
  $(".sp_delete").click(function(){ 
    var elmId = $(this). attr("id");
    if(this.checked) {
      $('#'+elmId+'_hidden').prop('checked', true);
    } else {
      $('#'+elmId+'_hidden').prop('checked', false);
    }
  });
</script>


<div class="modal fade" id="addCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Service Provider Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="controller/serviceProviderController.php" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Category Name *</label>
            <div class="col-sm-8">
              <input required="" type="text" name="service_provider_category_name"  class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Image *</label>
            <div class="col-sm-8">
              <input required="" type="file" name="service_provider_category_image"  class="form-control-file border">
            </div>
          </div>

          <div class="form-footer text-center">
            <button type="submit" name="addCategory" value="importParking" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Add</button>
          </div>

        </form> 
      </div>

    </div>
  </div>
</div><!--End Modal -->


<div class="modal fade" id="editCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Service Provider Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="controller/serviceProviderController.php" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Category Name *</label>
            <div class="col-sm-8">
              <input type="hidden" name="local_service_provider_id" id="local_service_provider_id">
              <input required="" id="service_provider_category_name" type="text" name="service_provider_category_name"  class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Image </label>
            <div class="col-sm-8">
              <input type="hidden" name="service_provider_category_image_old" id="service_provider_category_image">
              <input  type="file" name="service_provider_category_image"  class="form-control-file border">
            </div>
          </div>

          <div class="form-footer text-center">
            <button type="submit" name="editCategory" value="editCategory" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Update</button>
          </div>

        </form> 
      </div>

    </div>
  </div>
</div><!--End Modal -->

<div class="modal fade" id="bulkUpload">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Import Bulk Service Providers</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="input-10" class="col-sm-12 col-form-label">Step 1 -> Download Formated CSV  <a href="downloadServiceProviderCSV.php" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-check-square-o"></i> Download CSV</a></label>
            <label for="input-10" class="col-sm-12 col-form-label">Step 2 -> Fill Your Data</label>
            <label for="input-10" class="col-sm-12 col-form-label">Step 3 -> Import This File Here</label>
            <label for="input-10" class="col-sm-12 col-form-label">Step 4 -> Click on Upload Button</label>
          </div>
        </form> 
        <form action="importServiceProviderCSVData.php" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Import CSV File <span class="text-danger">*</span></label>
            <div class="col-sm-8" id="PaybleAmount">
              <input required="" type="file" name="file"  class="form-control-file border csvupload" accept=".csv" />
            </div>
          </div>

          <div class="form-footer text-center">
            <button type="submit" name="importCsvData" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Upload</button>
          </div>

        </form> 
      </div>

    </div>
  </div>
</div>