<?php
session_start();
// error_reporting(0);
extract(array_map("test_input" , $_POST));
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$society_id = $_COOKIE['society_id'];
$vId = $voting_id;
$q = $d->select("voting_master","society_id = '$society_id' AND voting_id = '$voting_id'");
$data = mysqli_fetch_array($q);
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
// google.load("visualization", "1", {packages:["corechart"], callback: drawChart});
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Options', 'Count'],
  <?php
  $sq = $d->select("voting_option_master","voting_id = '$voting_id' AND society_id = '$society_id'");
  while($sData = mysqli_fetch_array($sq)){
  // echo "['".$sData['option_name']."', ".$sData['option_count']."],";
  echo "['".$sData['option_name'].":".$sData['option_count']."', ".$sData['option_count']."],";
   } ?> 
]);

  // Optional; add a title and set the width and height of the chart
  var options = {
    titleTextStyle: {
      color:'#242124',
      fontSize: 16,
  },
    'title':'Statistics Information', 
    'width':480, 
    'height':400
  };

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}
</script>

<label for="input-10"  style="padding-bottom: 15px;" class="col-sm-12 col-form-label">Polling Question : <?php echo  $data['voting_question'];?> </label>
<label for="input-10"  style="padding-bottom: 15px;" class="col-sm-12 col-form-label">Polling Description : <?php echo  $data['voting_description'];?> </label>
<!-- Pie Chart -->
<div id="piechart" align='center'></div>




