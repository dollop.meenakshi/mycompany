  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-6">
          <h4 class="page-title">Admin Notification</h4>
        </div>
        <div class="col-lg-3 col-md-4 col-6">
          <form method="get">
            <input id="reportrange" class="reportrange form-control" name="e1" >
          </form>
        </div>
        <div class="col-sm-6 col-6">
           <div class="btn-group float-sm-right">
            <!-- <a href="sos" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a> -->
             <a href="javascript:void(0)" onclick="DeleteAll('deleteAdminNotification');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

          </div>
        </div>
      </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body" id="notificationData">
              <!-- <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>#</th>
                        <th> Title</th>
                        <th> Description</th>
                        <th>Date</th>
                        
                    </tr>
                </thead>
                <tbody>
                     <?php 
                  $m->set_data('read_status','1');
                  $arrayName = array('read_status'=>$m->get_data('read_status'));
                  $q2=$d->update("admin_notification",$arrayName,"society_id='$society_id'");
                  $i=1;
                  $q=$d->select("admin_notification","society_id='$society_id' AND admin_id=0 OR society_id='$society_id' AND admin_id='$bms_admin_id' ","ORDER BY notification_id DESC LIMIT 1000");
                  while($row=mysqli_fetch_array($q))
                  {
                    extract($row);
                  ?>
                    <tr>
                      <td class='text-center'>
                      <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['notification_id']; ?>">
                        </td>
                        <td><?php echo $i++; ?></td>
                        <td>
                          <?php  if ($row['notification_action']!='custom') {  ?>
                          <a href="readNotification.php?link=<?php echo $row['admin_click_action']; ?>&id=<?php echo $row['notification_id']; ?>"><?php echo $notification_tittle; ?></a>
                          <?php } else { ?>
                              <a href="viewNotification?id=<?php echo $row['notification_id']; ?>"><?php echo $notification_tittle; ?></a>
                          <?php } ?>  
                        </td>
                        <td><?php echo $notification_description;  ?></td>
                        <td><?php echo $notifiaction_date; ?></td>
                        
                    </tr>
                    <?php }?>
                    
                </tbody>
                                
                </div>
              </table>
              </div> -->
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->

<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">

  $(function() {

      var start = moment().subtract(6, 'days');
      var end = moment();

      function cb(start, end) {
          
          $('#reportrange').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            var startDate = start.format('YYYY-MM-D');
            var endDate = end.format('YYYY-MM-D');
            $(".ajax-loader").show();
            $.ajax({
              url: "getNotification.php",
              cache: false,
              type: "POST",
              data: {startDate:startDate,endDate:endDate,csrf:csrf},
              success: function(response){
                  $("#notificationData").html(response);
                  $(".ajax-loader").hide();
              }
           });
      }

      $('#reportrange').daterangepicker({
          startDate: start,
          endDate: end,
          locale: {
            format: 'MMMM D, YYYY'
          },
          maxDate: new Date(),
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          
      }, cb);

      cb(start, end);

  });

</script>




