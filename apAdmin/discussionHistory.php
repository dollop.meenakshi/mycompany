<?php
error_reporting(0);
extract(array_map("test_input" , $_REQUEST));
 if(filter_var($id, FILTER_VALIDATE_INT) != true){
     $_SESSION['msg1']='Invalid REQUEST';
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='manageDiscussions';
      </script>");
}
$request_id = (int)$request_id;
$id = (int)$id;

 $q=$d->select("discussion_forum_master","society_id='$society_id' AND discussion_forum_id='$id'","ORDER BY discussion_forum_id DESC");
 $data=mysqli_fetch_array($q);
?>
<link href="assets/plugins/vertical-timeline/css/vertical-timeline1.css" rel="stylesheet"/>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Discussion Forum</h4>
        
       
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
             <p><b>Title :</b> <?php echo $data['discussion_forum_title']; ?></p>
            <p class="text-justify"><b>Description :</b> <?php echo $data['discussion_forum_description']; ?></p>
            <p><b>Created Date :</b> <?php echo  date('d M Y h:i A',strtotime($data['created_date'])); ?></p>
            <p><b>Created By :</b>
           <?php
            if ($data['admin_id']!=0) {
               $qad=$d->select("bms_admin_master","admin_id='$data[admin_id]'");
               $adminData=mysqli_fetch_array($qad);
              echo "Admin (".$adminData['admin_name'].")";
            }else if ($data['user_id']!=0) { 
               $q111=$d->select("block_master,unit_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND users_master.user_id='$data[user_id]'","");
              $userdata=mysqli_fetch_array($q111);
              echo $userdata['user_full_name'].' '.$userdata['user_designation']." (".$userdata['block_name'].")";
            } 
            ?>
          </p>
          <p><b>Status:</b>
             <?php if($data['active_status']=="0"){
                        ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['discussion_forum_id']; ?>','discussionDeactive');" data-size="small"/>
                          <?php } else { ?>
                         <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['discussion_forum_id']; ?>','discussionActive');" data-size="small"/>
                        <?php } ?>
          </p>
          <p><b>Document</b> </p>
                <?php 
          if ($data['discussion_file']!='') { 
          $dFile= $data['discussion_file'];
              $ext = pathinfo($dFile, PATHINFO_EXTENSION);
              if ($ext == 'pdf' || $ext == 'PDF') {
                $imgIcon = 'img/pdf.png';
              } elseif ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                $imgIcon = 'img/jpg.png';
              } elseif($ext == 'png'){
                $imgIcon = 'img/png.png';
              }elseif ($ext == 'doc' || $ext == 'docx') {
                $imgIcon = 'img/doc.png';
              } elseif ($ext == 'ppt' || $ext == 'PPT') {
                $imgIcon = 'img/ppt.png';
              } elseif ($ext == 'xls' || $ext == 'XLS') {
                $imgIcon = 'img/excel.png';
              } else{
                $imgIcon = 'img/doc.png';
              }
            ?>
            <a href="../img/request/<?php echo $data['discussion_file'];?>" target="_blank" class="btn btn-link mb-0"><div style="height: 60px;width: 60px;background-image: url(<?php echo $imgIcon ?>);background-repeat: no-repeat;background-size: contain;" class="d-block text-break"></div></a>

            <!-- <a target="_blank" href="../img/request/<?php echo $data['discussion_file'];?>" ><img width="50" height="50" src="<?php echo $imgIcon;?>" alt=""></a> -->
          <?php } ?>
          <?php if ($data['discussion_photo']!='') { ?>
            <a href="../img/request/<?php echo $data['discussion_photo'];?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["discussion_photo"]; ?>"><img width="300" height="250" src="../img/request/<?php echo $data['discussion_photo'];?>" alt=""></a>
          <?php } ?>

        

          <div class="row pt-2 pb-2">
      <div class="col-sm-12 text-center">
         <button data-toggle="modal" data-target="#vieComp"   class="btn btn-sm btn-primary" type="button"><i class="fa fa-comments"></i> Comment</button>
      </div>
    </div>
    <?php 
          $timelineQuery = $d->select("discussion_forum_comment","discussion_forum_id='$id' AND prent_comment_id=0","ORDER BY comment_id DESC");
          while($timelineData = mysqli_fetch_array($timelineQuery)){
            $prent_comment_id= $timelineData['comment_id'];
            if($prent_comment_id > 0){
        ?>
      <section class="cd-timeline js-cd-timeline" >
      <div class="cd-timeline__container">
        
        
          <div class="cd-timeline__block js-cd-block <?php if($timelineData['admin_id']!=0){ echo "floatRight"; } ?>">
            <div class="cd-timeline__img cd-timeline__img--picture js-cd-img text-center ">
              <img src="../img/fav.png">
            </div> 

            <div class="cd-timeline__content js-cd-content" style="border: 1px solid gray;">
              <p><?php if($timelineData['admin_id']!=0){ 
                $qad=$d->select("bms_admin_master","admin_id='$timelineData[admin_id]'");
                $adminData=mysqli_fetch_array($qad);
                ?>
                <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="../img/society/<?php echo $adminData['admin_profile']; ?>"  width="30" height="30"   src="#" alt="your image" class='profile' />
                <?php echo $adminData['admin_name'];} else {

                   $q111=$d->select("block_master,unit_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND users_master.user_id='$timelineData[user_id]'","");
                    $userdataComment=mysqli_fetch_array($q111);
                    
                    if(!empty($userdataComment['user_full_name']))
                    {
                      $user_full_name = $userdataComment['user_full_name'] . " - ";
                    }
                    if(!empty($userdataComment['user_designation']))
                    {
                      $user_designation = $userdataComment['user_designation'];
                    }
                    if(!empty($userdataComment['block_name']))
                    {
                      $block_name = $userdataComment['block_name'];
                    }
                   $user_full_name=$company_name. $user_full_name ."-".$user_designation." (".$block_name.")";
                ?>
                <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $userdataComment['user_profile_pic']; ?>"  width="30" height="30"   src="#" alt="your image" class='profile' />
                <?php echo $user_full_name;
              } ?></p>
              <h6 style="word-wrap: break-word;"><?php echo $timelineData['comment_messaage']; ?></h6>
              
             <form action="controller/discussionForumController.php" method="post" accept-charset="utf-8">
                <input type="hidden" name="discussion_forum_id" value="<?php echo $timelineData['discussion_forum_id'];?>">
                <input type="hidden" name="comment_id_delete" value="<?php echo $timelineData['comment_id'];?>">
              <p><?php echo date('d M Y, h:i A',strtotime($timelineData['created_date'])); ?> <button type="submit" class="btn form-btn btn btn-danger btn-sm pull-right ">  <i class="fa fa-trash "></i></button></p>
              </form>

              <span class="cd-timeline__date"><?php echo date('M d',strtotime($timelineData['created_date'])); ?></span>
              <span>
                <?php $subComment = $d->select("discussion_forum_comment","discussion_forum_id='$id' AND prent_comment_id='$prent_comment_id'","ORDER BY comment_id DESC");
                  $totalRepy= mysqli_num_rows($subComment);
                  if ($totalRepy>0) {
                    echo "<b>".$totalRepy.' Reply <i class="fa fa-reply" aria-hidden="true"></i></b>';
                 ?>
              </span>
                <?php while ($subData=mysqli_fetch_array($subComment)) {  ?>
                <div class="subCommentDiv pl-3">
                      <p><?php if($subData['admin_id']!=0){ 
                      $qad=$d->select("bms_admin_master","admin_id='$subData[admin_id]'");
                      $adminData=mysqli_fetch_array($qad);
                      ?>
                      <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="../img/society/<?php echo $adminData['admin_profile']; ?>"  width="30" height="30"   src="#" alt="your image" class='profile' />
                      <?php echo $adminData['admin_name'];} else {

                         $q111=$d->select("block_master,unit_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND users_master.user_id='$subData[user_id]'","");
                          $userdataComment=mysqli_fetch_array($q111);
                         $user_full_name=$userdataComment['user_full_name'].'-'.$userdataComment['user_designation']." (".$userdataComment['block_name'].')';
                      ?>
                      <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $userdataComment['user_profile_pic']; ?>"  width="30" height="30"   src="#" alt="your image" class='profile' />
                      <?php echo $user_full_name;
                    } ?></p>
                    <h6 style="word-wrap: break-word;"><?php echo $subData['comment_messaage']; ?></h6>
                    
                   <form action="controller/discussionForumController.php" method="post" accept-charset="utf-8">
                      <input type="hidden" name="discussion_forum_id" value="<?php echo $subData['discussion_forum_id'];?>">
                      <input type="hidden" name="comment_id_delete" value="<?php echo $subData['comment_id'];?>">
                    <p><?php echo date('d M Y, h:i A',strtotime($subData['created_date'])); ?> <button type="submit" class="btn form-btn btn btn-danger btn-sm pull-right ">  <i class="fa fa-trash "></i></button></p>
                    </form>

                    <span class="cd-timeline__date"><?php echo date('M d',strtotime($subData['created_date'])); ?></span>
                </div>
                <?php } }?>
            </div>
          </div>
        </div>
      </section>
      <?php } }?>
          </div>
        </div>
      </div>
    </div>


     
  </div>
</div>
<!-- <script src="assets/js/jquery.min.js"></script> -->
<!-- <script src="assets/plugins/vertical-timeline/js/vertical-timeline.js"></script> -->





<div class="modal fade" id="vieComp">
  <div class="modal-dialog">
    <div class="modal-content border-success">
      <div class="modal-header bg-success">
        <h5 class="modal-title text-white">Discussion Comment </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="">
            <form id="commentAddDiscusstion" method="POST" action="controller/discussionForumController.php" enctype="multipart/form-data">
            <div class="row form-group">  
              <input type="hidden" id="" name="addComment" value="addComment">
              <input type="hidden" id="discussion_forum_id" name="discussion_forum_id" value="<?php echo $id;?>">
              <input type="hidden" id="active_status" name="active_status" value="<?php echo $data['active_status'];?>">
              <input type="hidden" id="discussion_forum_id" name="discussion_forum_for" value="<?php echo $data['discussion_forum_for'];?>">
              <label class="col-sm-3 form-control-label">Comment <span class="text-danger">*</span></label>
              <div class="col-sm-9">
                <textarea  maxlength="250" class="form-control " id="comment_messaage" required="" name="comment_messaage"></textarea>
              </div>
            </div>
           
            <div class="form-footer text-center">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Send</button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>