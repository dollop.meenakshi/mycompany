<?php 
error_reporting(0);
if (filter_var($_GET['emp_id'], FILTER_VALIDATE_INT) == true || filter_var($_GET['visitor_id'], FILTER_VALIDATE_INT) == true) {
// session_cache_limiter(false);
session_start();
include_once 'lib/dao.php';
include_once 'lib/model.php';

$d = new dao();
$m = new model();
extract(array_map("test_input" , $_GET));
if (isset($_COOKIE['bms_admin_id'])) {
    $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
$qs=$d->select("society_master","society_id='$society_id'");
	$bData=mysqli_fetch_array($qs);
}
   $society_id = (int)$society_id;
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Print Icard</title>
	<?php include 'common/colours.php'; ?>
  <link href="assets/css/app-style9.css" rel="stylesheet"/>

	<style type="text/css">
		body {
			background-color:white;
			font-family: 'Open Sans', sans-serif;
		}
		.id-card-holder {
			width: 200px;
		    padding: 4px;
		    /*margin: 0 auto;*/
		    margin-left: 2px;
		    margin-right: : 2px;
		    margin-bottom : 10px;
		    /*background-color: #1f1f1f;*/
		    border-radius: 5px;
		    position: relative;
		    display: inline-block;
		}
		
		.id-card {
			
			background-image: url('../img/icardbg.jpg');
			padding: 0px;
			border-radius: 5px;
			text-align: center;
			box-shadow: 0 0 1.5px 0px #b9b9b9;
			background-repeat: no-repeat;
			background-size: cover;
		}
		.id-card img {
			margin: 0 auto;
		}
		.header {
			margin: auto;
			width: 60px;
		}
		.header img {
			width: 100%;
    		margin-top: 5px;
		}
		.photo {
			margin: auto;
			width: 87px;
			height: 87px;
			border: 1px solid var(--primary);
			border-radius: 50%;
			background: white;
			overflow: hidden;
		}
		.photo img {
			width: 100%;
			height: 100%;
    		
		}
		hr {
			margin: 2px;
		}
		h2 {
			font-size: 12px;
			margin: 2px 0;
		}
		h3 {
			font-size: 10px;
			margin: 2.5px 0;
			font-weight: 300;
		}
		.qr-code img {
			width: 85px;
		}
		p {
			font-size: 10px;
			margin: 2px;
		}
		.id-card-hook {
			background-color: #000;
		    width: 70px;
		    margin: 0 auto;
		    height: 15px;
		    border-radius: 5px 5px 0 0;
		}
		.id-card-hook:after {
			content: '';
		    background-color: #d7d6d3;
		    width: 47px;
		    height: 6px;
		    display: block;
		    margin: 0px auto;
		    position: relative;
		    top: 6px;
		    border-radius: 4px;
		}
		.id-card-tag-strip {
			width: 45px;
		    height: 40px;
		    background-color: #0950ef;
		    margin: 0 auto;
		    border-radius: 5px;
		    position: relative;
		    top: 9px;
		    z-index: 1;
		    border: 1px solid #0041ad;
		}
		.id-card-tag-strip:after {
			content: '';
		    display: block;
		    width: 100%;
		    height: 1px;
		    background-color: #c1c1c1;
		    position: relative;
		    top: 10px;
		}
		.id-card-tag {
			width: 0;
			height: 0;
			border-left: 100px solid transparent;
			border-right: 100px solid transparent;
			border-top: 100px solid #0958db;
			margin: -10px auto -30px auto;
		}
		.id-card-tag:after {
			content: '';
		    display: block;
		    width: 0;
		    height: 0;
		    border-left: 50px solid transparent;
		    border-right: 50px solid transparent;
		    border-top: 100px solid #d7d6d3;
		    margin: -10px auto -30px auto;
		    position: relative;
		    top: -130px;
		    left: -50px;
		}
		.addressSoc {
			font-size: 7px;
			padding-top: 2px;
			font-weight: bold;
			color: var(--primary);
		}
		.addressDiv {
			height: 35px;
		}
		.website {
			padding-bottom: 2px;
			font-size: 7px;
		}
		.qr-code {
			margin: auto;
			width: 87px;
			border: 1px solid var(--primary);
			border-radius: 5px;
		}
	</style>
</head>

  <body onload="window.print()">

	<?php if (isset($visitor_id)) {  
        $q3=$d->select("daily_visitors_master,society_master","daily_visitors_master.society_id='$society_id' and daily_visitors_master.active_status = 0 AND daily_visitors_master.visitor_id='$visitor_id' order by daily_visitors_master.visitor_id DESC ","");

		$i=0;
		while($data=mysqli_fetch_array($q3)){ 
			$fd=$d->selectRow("visitor_sub_image,visitor_sub_type_name","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                        $vistLogo=mysqli_fetch_array($fd);
                        if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                            $visit_company = $vistLogo['visitor_sub_type_name'].'';
                        } else {
                            $visit_logo="";
                        }
			?>
		<div class="id-card-holder">
			<div class="id-card" >
				<div class="header">
					<?php if (isset($bData['socieaty_logo'])){ ?>
					<img  onerror="this.src='img/logo.png'" src="../img/society/<?php echo $bData['socieaty_logo'];?>">
					<?php }else {?>
					<img src="img/logo.png">
					<?php } ?>
				</div>
				<div class="photo">
					<div style="width: 85px;height: 85px;border: 1px solid;margin: auto;border-color: #bbb;
    		border-radius: 5px 5px 0px 0px !important;">
					<img  height="80" width="" onerror="this.src='img/user.png'"  src="../img/visitor/<?php echo $data['visitor_profile']; ?>"  class="img-fluid rounded shadow mb-3" alt="card img">
					</div>
				</div>
				<h2 class="nameStyle"><?php echo $data['visitor_name']; ?> </h2>
				<p style="font-size: 10px;"><?php
				 $user_mobile=$data['visitor_mobile'];
				 echo $visit_company.'-Daily Visitor';
				?></p>
				<div class="qr-code">

					<?php $qr_size          = "300x300";
						$qr_content       = "$user_mobile";
						$qr_correction    = strtoupper('H');
						$qr_encoding      = 'UTF-8';

	    				//form google chart api link
						$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
						?>
						<img src="<?php echo $qrImageUrl; ?>" alt="">
				</div>
				
				<h3><?php echo $data['society_name']; ?></h3>
				<hr>
				<div class="addressDiv">
				<p class="addressSoc"><?php echo $data['society_address']; ?></p>
				</div>

			</div>
		</div>
	    <?php }	} else  if (isset($allUsers)) {  
        $q3=$d->select("unit_master,block_master,users_master,floors_master,society_master","users_master.society_id=society_master.society_id AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 ","ORDER BY unit_master.unit_id ASC");

		$i=0;
		while($data=mysqli_fetch_array($q3)){ ?>
		<div class="id-card-holder">
			<div class="id-card" >
				<div class="header">
					<?php if (isset($bData['socieaty_logo'])){ ?>
					<!-- <img height="40" onerror="this.src='img/logo.png'" src="../img/society/<?php echo $bData['socieaty_logo'];?>"> -->
					<?php }else {?>
					<!-- <img src="img/logo.png"> -->
					<?php } ?>
				</div>
				<div class="photo">
					<div style="width: 85px;height: 85px;border: 1px solid;margin: auto;border-color: #bbb;
    		border-radius: 5px 5px 0px 0px !important;">
					<img  height="80" width="" onerror="this.src='img/user.png'"  src="../img/users/recident_profile/<?php echo $data['user_profile_pic']; ?>"  class="img-fluid rounded shadow mb-3" alt="card img">
					</div>
				</div>
				<h2 class="nameStyle"><?php echo $data['user_full_name']; ?> </h2>
				<p style="font-size: 10px;"><?php
				 $user_id=$data['user_id'];
				 echo $data['block_name'].'-'.$data['unit_name'];
				?></p>
				<div class="qr-code">

					<?php $qr_size          = "300x300";
						$qr_content       = "$user_id";
						$qr_correction    = strtoupper('H');
						$qr_encoding      = 'UTF-8';

	    				//form google chart api link
						$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
						?>
						<img src="<?php echo $qrImageUrl; ?>" alt="">
				</div>
				
				<h3><?php echo $data['society_name']; ?></h3>
				<hr>
				<div class="addressDiv">
				<p class="addressSoc"><?php echo $data['society_address']; ?></p>
				</div>

			</div>
		</div>
	    <?php }	} else  {
		 if (isset($emp_id)) {
   			$emp_id = (int)$emp_id;
             
              $q1=$d->select("employee_master,society_master","society_master.society_id=employee_master.society_id AND employee_master.society_id='$society_id' AND employee_master.emp_id='$emp_id'","");
            }
		 // $q1=$d->select("employee_master,society_master","society_master.society_id=employee_master.society_id AND employee_master.society_id='$society_id'","");
	$i=0;
	while($data=mysqli_fetch_array($q1)){
 	$EW=$d->select("emp_type_master","emp_type_id='$data[emp_type_id]'");
 	$empTypeData=mysqli_fetch_array($EW); ?>
	<div class="id-card-holder">
		<div class="id-card" >
			<div class="header">

				<?php if (isset($bData['socieaty_logo'])){ ?>
				<img onerror="this.src='img/logo.png'" src="../img/society/<?php echo $bData['socieaty_logo'];?>">
				<?php }else {?>
				<img src="img/logo.png">
				<?php } ?>
			</div>
			<div class="photo">
				<div style="width: 85px;height: 85px;border: 1px solid;margin: auto;border-color: #bbb;
    		border-radius: 5px 5px 0px 0px !important;">
				<img  height="80" onerror="this.src='img/user.png'"  src="../img/emp/<?php echo $data['emp_profile']; ?>"  class="img-fluid rounded shadow mb-3" alt="card img">
				</div>
			</div>
			<h2 class="nameStyle"><?php echo $data['emp_name']; ?> </h2>
			<p style="font-size: 10px;"><?php
			 $emp_id=$data['emp_id'];
			  $emp_mobile=$data['emp_mobile'];
			  if ($empTypeData['emp_type_name']!='') {
                      echo $empTypeData['emp_type_name'];
                    } else {
                      echo "Security Guard";
                    }?></p>
			<div class="qr-code">

				<?php $qr_size          = "300x300";
					$qr_content       = "$emp_mobile";
					$qr_correction    = strtoupper('H');
					$qr_encoding      = 'UTF-8';

    				//form google chart api link
					$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
					?>
					<img src="<?php echo $qrImageUrl; ?>" alt="">
			</div>
			
			<h3><?php echo $data['society_name']; ?></h3>
			<hr>
			<div class="addressDiv">
			<p class="addressSoc"><?php echo $data['society_address']; ?></p>
			</div>

		</div>
	</div>
		<?php } } ?>
</body>
<script type="text/javascript">
        Android.print('print');
  </script>
</html>
<?php } else{
	echo "Invalid Request!!!";
}