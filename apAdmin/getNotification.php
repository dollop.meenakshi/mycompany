<?php 
session_start();
include 'common/object.php';
include 'common/checkLogin.php';
include 'common/checkLanguage.php';

// include 'accessControlPage.php';
extract(array_map("test_input" , $_POST));
$society_id = $_COOKIE['society_id'];
$bms_admin_id = $_COOKIE['bms_admin_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
//print_r($_POST);die();


?>
  <div class="table-responsive">
    <table id="notificationTable" class="table table-bordered">
      <thead>
          <tr>
            <th><input type="checkbox" name="select_all" value="1" id="example-select-all">&nbsp;&nbsp;&nbsp;&nbsp;</th>
              <!-- <th class="text-center"><input type="checkbox" class="multiDelteCheckbox"  value=""></th> -->
              <th>#</th>
              <th> Title</th>
              <th> Description</th>
              <th>Date</th>
              
          </tr>
      </thead>
      <tbody>
        <?php 
        $arrayName = array('read_status'=>1);
        $q2=$d->update("admin_notification",$arrayName,"society_id='$society_id'");
        $i=1;
        $query = "";
        if($startDate!="" && $endDate!="")
        {
          $startDate = date("Y-m-d", strtotime($startDate));
          $endDate = date("Y-m-d", strtotime($endDate));
          $query = " AND DATE_FORMAT(notifiaction_date,'%Y-%m-%d') >= '$startDate' AND DATE_FORMAT(notifiaction_date,'%Y-%m-%d') <= '$endDate'";
        }
        $q=$d->select("admin_notification","((society_id='$society_id' AND admin_id=0) OR (society_id='$society_id' AND admin_id='$bms_admin_id')) $query ","ORDER BY notification_id DESC LIMIT 1000");
        while($row=mysqli_fetch_array($q))
        {
          extract($row);
          $link = $row['admin_click_action']; 
          $link = str_replace("&","~",$link);
        ?>
          <tr>
            <td class='text-center'>
            <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['notification_id']; ?>">
              </td>
              <td><?php echo $i++; ?></td>
              <td>
                <?php  if ($row['notification_action']!='custom') {  ?>
                <a href="readNotification.php?link=<?php echo $link; ?>&id=<?php echo $row['notification_id']; ?>"><?php echo $notification_tittle; ?></a>
                <?php } else { ?>
                    <a href="viewNotification?id=<?php echo $row['notification_id']; ?>"><?php echo $notification_tittle; ?></a>
                <?php } ?>  
              </td>
              <td><?php echo $notification_description;  ?></td>
              <td><?php echo $notifiaction_date; ?></td>
              
          </tr>
          <?php } ?>
        </tbody>
                                  
      </div>
    </table>
  </div>

  <script type="text/javascript">
    
    var notificationTable = $('#notificationTable').DataTable( {
    drawCallback: function(){
            $("img.lazyload").lazyload();
         },
    lengthChange: true,
    pageLength: 25,
    lengthMenu: [[5,10, 25, 50, -1], [5, 10, 25, 50, "All"]],
    columnDefs: [{
       targets: 0,
       bSortable: false,
       searchable: false,
       orderable: false,
       className: 'dt-body-center',
       render: function (data, type, full, meta){
           return data;
           // return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
       }
    }],
    // buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
  } );
// Handle click on "Select all" control
  $('#example-select-all').on('click', function(){

    // Get all rows with search applied
    var rows = notificationTable.rows({ 'search': 'applied' }).nodes();
    // Check/uncheck checkboxes for all rows in the table
    $('input[type="checkbox"]', rows).prop('checked', this.checked);
  });
  // Handle click on checkbox to set state of "Select all" control
   $('#notificationTable tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });
  </script>
