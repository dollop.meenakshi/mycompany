<?php 
// session_cache_limiter(false);
session_start();
include_once 'lib/dao.php';
include_once 'lib/model.php';

$d = new dao();
$m = new model();
extract(array_map("test_input" , $_GET));
if (isset($_COOKIE['bms_admin_id'])) {
	$bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
	$society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

if ($_GET['type']=="qrcode") {
?>
<!DOCTYPE html>
<html>
<head>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
	<?php include 'common/colours.php'; ?>
	<link href="assets/css/app-style9.css" rel="stylesheet"/>

	<title>Assets QR Code Print</title>
	<style type="text/css">
		body {
			/*background-color: #d7d6d3;*/
			font-family: 'Open Sans', sans-serif;
		}
		h2 {
			font-size: 12px;
			margin: 5px 0;
		}
		h3 {
			font-size: 10px;
			margin: 2.5px 0;
			font-weight: 300;
		}
		.addressSoc {
			font-size: 7px;
			font-weight: bold;
		}
		.col-md-4 {
			-ms-flex: 0 0 33.333333%;
			flex: 0 0 33.333333%;
			width:33.33% !important;
			float: left;
			display: inline-block;
			padding: 0px !important; 
		}

 .main-div{
 	border-radius: 50% !important; background-color: var(--primary) !important; color:#fff !important; padding-top:10px  !important; padding-left: 5px  !important; padding-right:5px  !important;  float: left;
 }
 .img-div{
 	width:110px !important; 
 	height: 110px !important; 
 	border-radius: 50% !important;
 	background-color: #fff !important;
 	opacity: 1 !important;
 	box-shadow: 3px 3px 20px
 	rgba(0, 0, 0, 0.5);
 	border: 2px solid
 	rgba(255, 255, 255, 1);
 }
 .sosa-name-div{
 	text-transform: uppercase; font-weight: 600 !important; padding: 10px !important;
 }
 .block-info{
 	text-transform: uppercase;
 	overflow-wrap: break-word;
 	min-height: 24px !important;
 	min-width: 60px !important;
 	max-height: 20px !important;
 	max-width: 90px !important;
 	background-color:#fff !important;
 	font-weight: 600 !important;
 	line-height: 25px !important;
 	color:var(--primary) !important;
 	box-shadow: 3px 3px 20px
 	rgba(0, 0, 0, 0.5);
 	border-top: 2px solid
 	rgba(255, 255, 255, 1);
 }
 .parking-permit{
 	text-transform: uppercase; padding: 10px !important;font-weight: 600;
 }
 .new{

 	padding: 0px !important;
 }	 
 .new2{
 	padding-top: 10px !important;
 }
 .img-new-div {
 	z-index: 10;
 	position: relative;

 }
 .detail-div
 {
 	position: relative;
 	/* height:auto;
 	width: auto; */
 	margin-top: 15px;
 	z-index: 20;
 	color: var(--primary) !important;
 	font-weight: bold;
 	/*margin-left: 22% !important;*/
 	margin: auto;
 	text-align: center;
 	clear: both;
 	padding-bottom: 10px;
 	background-repeat: no-repeat;
 	background-size: cover;
 	padding-top: 5px;
 	margin-bottom : 25px;
 	/*border: 1px solid var(--primary);*/
 	border-radius: 15px;
	overflow: hidden;
	display: inline-block;
 }

 .sosa-text {
 	text-align: center;
 	color: var(--primary);
 	font-size: 14px;
 	margin-top: 52px;
 	letter-spacing: 0px;
 	font-weight: 600;
 	text-align: center;
 	/*line-height: 0px;*/
 	word-wrap: break-word;
 	width: 240px;
 	margin-bottom: 0px;
 	padding-left: 12px;
 }


 .sosa-name {
 	color: var(--primary);
 	font-size: 12px;
 	/* display: inline-block; */
 	position: relative;
 	top: auto;
 	letter-spacing: 0px;
 	font-weight: 600;
 	text-align: center;
 	/*line-height: 0px;*/
 }
 .type-img{
 	height: 30px !important;
 	width: 30px !important;
 }
 .main-img{
 	width: 200px !important;
 	height: 200px !important;
 }
 .parking-name{
 	font-weight: 600;
 	font-size: 35px;
 	/*margin-top: 45px;*/
 	margin-bottom: 0px;
 }
 .water_mark_image{
	width: 100%;
    height: 100%;
    object-fit: contain;
    position: absolute;
    top: -50%;
    left: -50%;
    transform: translate(50%, 50%);
    opacity: 0.1;
 }
@page {
  size: A4;
  margin-bottom: 20mm !important;
}

@media print {
	.img-new-div {
 	z-index: 10;
 	position: relative;
 }
 .type-img{
 	height: 40px !important;
 	width: 40px !important;
 }
 .main-img{
 	width: 200px !important;
 	height: 200px !important;
 }
 .parking-name{
 	font-weight: 600;
 	font-size: 35px;
 	/*margin-top: 45px;*/
 	margin-bottom: 0px;	
 }
}
.print:last-child {
     page-break-after: auto;
}
</style>


</head>
<!-- <body> -->
<body style="background-color: white;" onload="window.print()">

<form method="GET">
	<?php if(isset($assets_id) && $assets_id!=""){ ?>
	<input type="hidden" name="assets_id" value="<?php echo $assets_id; ?>">
	<?php } ?>
	<input type="hidden" name="type" value="qrcode">
	<select name="qr_size" class="" onchange="this.form.submit();">
		<option value="">--QR Size--</option>
		<option <?php if(isset($_GET['qr_size']) && $_GET['qr_size'] == "100x100"){echo "selected";} ?> value="100x100">100x100</option>
		<option <?php if(isset($_GET['qr_size']) && $_GET['qr_size'] == "255x255"){echo "selected";} ?> value="255x255">255x255</option>
		<option <?php if(isset($_GET['qr_size']) && $_GET['qr_size'] == "300x300"){echo "selected";} ?> value="300x300">300x300</option>
	</select>
</form>
	 
	
	<div class="row printable m-3">
			<?php
			$qry ="";
			if(isset($assets_id) && $assets_id!=""){
				$assets_id = (int)$_GET['assets_id'];
				$qry = " AND assets_id='$assets_id'";
			}
			$q=$d->select("assets_item_detail_master","society_id='$society_id' AND move_to_scrap=0 $qry ");

			$i=0;
			while($data=mysqli_fetch_array($q)){
					
				//$item_name = (strlen($data['assets_name']) > 6) ? substr($data['assets_name'],0,6).'-'.$data['assets_id'] : $data['assets_name'].'-'.$data['assets_id'];
				$item_name = base64_encode(date('Ymdhis').'-'.$data['assets_id']);
 
			 ?>


				<div class="col-md-4 text-center print"  style="margin-bottom: 15px !important;">
						
					<div  class="detail-div bg-white" >
						<?php 
						$qr_size          = isset($_GET['qr_size']) && $_GET['qr_size'] != '' ? $_GET['qr_size'] :"255x255";
						$qr_content       = "$item_name";
						$qr_correction    = strtoupper('H');
						$qr_encoding      = 'UTF-8';

	    				//form google chart api link
						$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
						?>
						<img class="water_mark_image" src="../img/logo.png" alt="">
						<img src="<?php echo $qrImageUrl; ?>" alt="">
						<h5 style="margin-bottom: 0px !important" class="sosa-name"><?php echo $data['assets_name']; ?> (<?php echo $data['assets_brand_name']; ?>)</h5>
						<i style="font-size: 8px;"><?php echo $data['sr_no']; ?></i>

							
					</div>
				</div>
		 

 	
				<?php }  ?> 
</div>
</body>
<script type="text/javascript">
        Android.print('print');
  </script>
</html>
<?php } ?>