<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
$templateId = (int)$_REQUEST['id'];
extract($_POST);
if (isset($edit_template_question))
{
    $q = $d->select("template_question_master","template_question_id = '$template_question_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Template Question</h4>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="addTemplateQuestionFrom" action="controller/WorkReportTemplateController.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Template <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select type="text" required="" class="form-control single-select" name="template_id" id="template_id">
                                                <option value="">-- Select Template --</option>
                                                <?php
                                                $tq=$d->select("template_master","society_id='$society_id' AND template_status=0");
                                                while ($tData=mysqli_fetch_array($tq)) {
                                                ?>
                                                <option <?php if(isset($data['template_id']) && $data['template_id'] ==$tData['template_id']){ echo "selected"; }else{if(isset($templateId) && $templateId ==$tData['template_id']){ echo "selected"; }} ?> value="<?php if(isset($tData['template_id']) && $tData['template_id'] !=""){ echo $tData['template_id']; } ?>"><?php if(isset($tData['template_name']) && $tData['template_name'] !=""){ echo $tData['template_name']; } ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Template Question <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" class="form-control" placeholder="Template Question" name="template_question" value="<?php if(isset($edit_template_question) && $template_question != ""){ echo $data['template_question']; } ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Type <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select type="text" required="" class="form-control single-select" name="question_type" id="question_type"  onchange="questionType(this.value)">
                                                <option value="">-- Select Type --</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "0"){ echo 'selected'; } ?> value="0">Description</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "1"){ echo 'selected'; } ?> value="1">Radio Button</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "2"){ echo 'selected'; } ?> value="2">Checkbox</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "3"){ echo 'selected'; } ?> value="3">Dropdown</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "4"){ echo 'selected'; } ?> value="4">File Upload</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "5"){ echo 'selected'; } ?> value="5">Date</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "6"){ echo 'selected'; } ?> value="6">Time</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "7"){ echo 'selected'; } ?> value="7">Date and Time</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "8"){ echo 'selected'; } ?> value="8">Number (0-9)</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "9"){ echo 'selected'; } ?> value="9">Progress Bar (0 to 100%)</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "10"){ echo 'selected'; } ?> value="10">Topic With Time</option>
                                                <option <?php if(isset($edit_template_question) && $data['question_type'] == "11"){ echo 'selected'; } ?> value="11">Description With Time</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row moreOption <?php if (isset($edit_template_question)) { if($data['question_type'] == 1 || $data['question_type'] == 2 || $data['question_type'] == 3){}else{echo 'd-none';} }else{ echo 'd-none';} ?>">
                                <div class="col-md-8">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Question Type Value <span class="required">*</span></label>
                                    <div class="form-group row w-100 mx-0 showMoreOption">
                                        <?php if (!isset($edit_template_question)) { ?>
                                        <div class="col-sm-12 d-flex removeDiv_0">
                                            <input type="radio" disabled class="d-none optionRadio">
                                            <input type="checkbox" disabled class="d-none optionCheckbox">
                                            <input type="text" value="" required="" class="border border-top-0 border-left-0 border-right-0 w-100 ml-2 mb-2 template-option-error-placement" id="question_type_value0" name="question_type_value[]" placeholder="Enter Option">
                                            <button type="button" onclick="removeDiv(0);" class="border-0 bg-light mb-2 rounded-circle remove_button d-none"><i class="fa fa-times"></i></button>
                                        </div>
                                        <?php } ?>
                                        <?php if (isset($edit_template_question)) {
                                        $question_type_value = json_decode($data['question_type_value'], true);
                                        if (isset($question_type_value)) {
                                        // code...
                                        for ($i=0; $i < count($question_type_value); $i++) {
                                        $clicks = $i;
                                        $divCount = $i+1;
                                        ?>
                                        <div class="col-sm-12 d-flex removeDiv_<?php echo $i; ?>">
                                            <input type="radio" disabled class="<?php if($data['question_type'] == 1){}else{echo 'd-none';} ?> optionRadio">
                                            <input type="checkbox" disabled class="<?php if($data['question_type'] == 2){}else{echo 'd-none';} ?> optionCheckbox">
                                            <input type="text" required="" class="border border-top-0 border-left-0 border-right-0 w-100 ml-2 mb-2 template-option-error-placement" id="question_type_value<?php echo $i; ?>" name="question_type_value[]" placeholder="Enter Option" value="<?php echo $question_type_value['option_'.$i]; ?>">
                                            <button type="button" onclick="removeDiv(<?php echo $i; ?>);" class="border-0 bg-light mb-2 rounded-circle remove_button <?php if (isset($edit_template_question)) { }else{ echo 'd-none';} ?>"><i class="fa fa-times"></i></button>
                                        </div>
                                        <?php } } else {
                                        $clicks = "0";
                                        $divCount = "0";
                                        } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-12 d-flex">
                                        <a href="javascript:void(0);" onclick="addOptionDiv()" class="border-0 bg-light mb-2"><input type="radio" disabled class="<?php if (isset($edit_template_question)) { if($data['question_type'] == 1 ){}else{echo 'd-none';} }else{ echo 'd-none';} ?> optionRadio">
                                        <input type="checkbox" disabled class="<?php if (isset($edit_template_question)) { if($data['question_type'] == 2 ){}else{echo 'd-none';} }else{ echo 'd-none';} ?> optionCheckbox"> Add More Option</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Is Required <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="radio" id="is_required_no" name="is_required" value="0" <?php if (isset($edit_template_question)) { if($data['is_required'] == 0){echo'checked';} }else{ echo'checked';} ?>>
                                            <label for="is_required_no">Yes</label>
                                            <input type="radio" id="is_required_no" name="is_required" value="1" <?php if (isset($edit_template_question)) { if($data['is_required'] == 1){echo'checked';} } ?>>
                                            <label for="is_required_no">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row multiAttView <?php if (isset($edit_template_question)) { if($data['question_type'] == 4){}else{echo 'd-none';} }else{ echo 'd-none';} ?>">
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Allow Multiple File Upload (Max 3) <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="radio" id="template_multiple_file_no" name="template_multiple_file" value="0" <?php if (isset($edit_template_question)) { if($data['template_multiple_file'] == 0){echo'checked';} }else{ echo'checked';} ?>>
                                            <label for="template_multiple_file_no">No</label>
                                            <input type="radio" id="template_multiple_file_yes" name="template_multiple_file" value="1" <?php if (isset($edit_template_question)) { if($data['template_multiple_file'] == 1 ){echo'checked';} } ?>>
                                            <label for="template_multiple_file_yes">Yes</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                            <?php
                                if (isset($edit_template_question))
                                {
                            ?>
                                <input type="hidden" name="template_id" id="template_id" value="<?php echo $template_id; ?>">
                                <input type="hidden" name="addTemplateQuestion" value="addTemplateQuestion">
                                <input type="hidden" id="template_question_id" name="template_question_id" value="<?php echo $template_question_id; ?>" >
                                <button id="addTemplateQuestionBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                            <?php
                            }
                            else
                            {
                            ?>
                                <input type="hidden" name="addTemplateQuestion" value="addTemplateQuestion">
                                <button id="addTemplateQuestionBtn" name="addMoreTemplateQuestion" value="addMoreTemplateQuestion" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save & Add More</button>
                                <button id="addTemplateQuestionBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save</button>
                            <?php
                            }
                            ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script>
<?php
    if (isset($edit_template_question))
    {
?>
    var clicks = <?php echo $clicks; ?>;
    var divCount = <?php echo $divCount; ?>;
<?php
    }
    else
    {
?>
    var divCount = 1;
    var clicks = 0;
<?php
}
?>

    function addOptionDiv(i)
    {
        clicks += 1;
        divCount += 1;
        var question_type = $('#question_type').val();
        htmlData = `<div class="col-sm-12 d-flex removeDiv_`+clicks+`">`;
        if(question_type == 1)
        {
            htmlData +=
            `<input type="radio" disabled class="optionRadio">
            <input type="checkbox" disabled class="optionCheckbox d-none">`;
        }
        else if(question_type == 2)
        {
            htmlData +=
            `<input type="checkbox" disabled class="optionCheckbox">
            <input type="radio" disabled class="optionRadio d-none">`;
        }
        htmlData += `<input type="text" value="" required="" class="border border-top-0 border-left-0 border-right-0 w-100 ml-2 mb-2 template-option-error-placement" id="question_type_value`+clicks+`" name="question_type_value[]" placeholder="Enter Option">
                    <button type="button" onclick="removeDiv(`+clicks+`);" class="border-0 bg-light mb-2 rounded-circle remove_button"><i class="fa fa-times"></i></button>
                </div>`;
        $('.showMoreOption').append(htmlData);
        checkOptionDiv();
    }

    function checkOptionDiv()
    {
        if(divCount == 1)
        {
            $('.remove_button').addClass('d-none');
        }
        else
        {
            $('.remove_button').removeClass('d-none');
        }
    }

    function removeDiv(id)
    {
        $('.removeDiv_'+id).remove();
        $('#question_type_value'+id+'-error').remove();
        divCount -= 1;
        checkOptionDiv();
    }

    function questionType(value)
    {
        $('.moreOption').addClass('d-none');
        $('.optionRadio').addClass('d-none');
        $('.optionCheckbox').addClass('d-none');
        if(value == 1)
        {
            $('.moreOption').removeClass('d-none');
            $('.optionRadio').removeClass('d-none');
        }
        else if(value == 2)
        {
            $('.moreOption').removeClass('d-none');
            $('.optionCheckbox').removeClass('d-none');
        }
        else if(value == 3)
        {
            $('.moreOption').removeClass('d-none');
        } else if(value == 4)
        {
            $('.multiAttView').removeClass('d-none');
        }
    }
</script>