<?php
error_reporting(0);
extract(array_map("test_input" , $_REQUEST));
if (filter_var($id, FILTER_VALIDATE_INT) != true ) {
  $_SESSION['msg1']="Invalid Request";
  echo ("<script LANGUAGE='JavaScript'>
    window.location.href='complaints';
    </script>");
}
$complain_id = (int)$id;
$q=$d->select("complains_master,users_master,unit_master,floors_master","floors_master.floor_id=users_master.floor_id AND complains_master.unit_id=unit_master.unit_id AND users_master.user_id=complains_master.user_id AND complains_master.society_id='$society_id' AND complains_master.complain_id='$complain_id'","ORDER BY complains_master.complain_id DESC");
$data=mysqli_fetch_array($q);

if ($data['complaint_new_status']==0) {
      $aComplaint =array(
          'complaint_new_status'=> 1
       );
    
    $d->update("complains_master",$aComplaint,"complain_id='$complain_id'");
  }
?>
<link href="assets/plugins/vertical-timeline/css/vertical-timeline1.css" rel="stylesheet"/>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Complaint History (CN<?php echo $data['complain_id']; ?>)</h4>
        <div class="table-responsive">
        <table class="table table-bordered bg-white">
          <tbody>
            <tr>
              <th>Employee</th>
              <td><?php echo $data['user_full_name']; ?> (<?php echo $data['floor_name']; ?>)</td>
            </tr>
            <tr>
              <th>Title</th>
              <td><?php echo $data['compalain_title']; ?></td>
            </tr>
            <tr>
              <th>Description</th>
              <td><?php echo $data['complain_description']; ?></td>
            </tr>
            <tr>
              <th>Date</th>
              <td><?php echo  date('d M Y h:i A',strtotime($data['complain_date'])); ?></td>
            </tr>
            
            <tr>
              <th>Status</th>
              <td><?php
              if ($data['flag_delete']==0) {
               
               if ($data['complain_status']==0) {  ?>
                <span   data-toggle="modal" data-target="#vieComp"  onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-warning m-1 pointerCursor">OPEN <i class="fa fa-pencil"></i></span>
              <?php } elseif ($data['complain_status']==1) { ?>
                <span   data-toggle="modal" data-target="#vieComp"  onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-success  m-1 pointerCursor">CLOSE </span>
              <?php  } elseif ($data['complain_status']==2) { ?>
                <span  data-toggle="modal" data-target="#vieComp"   onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-info  m-1 pointerCursor">REOPEN <i class="fa fa-pencil"></i></span>
              <?php } elseif ($data['complain_status']==3) { ?>
                <span  data-toggle="modal" data-target="#vieComp"   onclick="vieComplain('<?php echo $data['complain_id']; ?>');" class="badge badge-primary  m-1 pointerCursor">In Progress <i class="fa fa-pencil"></i></span>
              <?php } } else {
                echo "Deleted by User";
              }?></td>
            </tr>
             <?php if ($data['service_type']==1)  {  ?>
              <tr>
              <th>Payment Status</th>
              <td><?php if ($data['amount_status']==0)  {
                echo "Received";
              } else  {
                echo "Not Received";
              } ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        </div>
      </div>
    </div>
     <div class="row pt-2 pb-2">
      <div class="col-sm-12 text-center">
         <button data-toggle="modal" data-target="#vieComp"   onclick="vieComplain('<?php echo $data['complain_id']; ?>');"  class="btn btn-sm btn-primary" type="button"><i class="fa fa-pencil"></i> Reply</button>
      </div>
    </div>
  	<section class="cd-timeline js-cd-timeline">
      <div class="cd-timeline__container">
        <?php 
          $timelineQuery = $d->select("complains_track_master,complains_master","complains_track_master.complain_id=complains_master.complain_id AND complains_track_master.complain_id='$complain_id'","ORDER BY complains_track_master.complains_track_id DESC");
          while($timelineData = mysqli_fetch_array($timelineQuery)){
        ?>
          <div class="cd-timeline__block js-cd-block <?php if($timelineData['complains_track_by']==1){ echo "floatRight"; } ?>">
            <div class="cd-timeline__img cd-timeline__img--picture js-cd-img text-center ">
              <img src="../img/fav.png">
            </div> 

            <div class="cd-timeline__content js-cd-content">
              <p><?php if($timelineData['complains_track_by']==1){ 
                $qad=$d->select("bms_admin_master","admin_id='$timelineData[admin_id]'");
                $adminData=mysqli_fetch_array($qad);
                ?>
                <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="../img/society/<?php echo $adminData['admin_profile']; ?>"  width="30" height="30"   src="#" alt="your image" class='profile' />
                <?php echo $adminData['admin_name'] .' (Admin)';} else {
                ?>
                <img class="rounded-circle" id="blah"  onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $data['user_profile_pic']; ?>"  width="30" height="30"   src="#" alt="your image" class='profile' />
                <?php echo $data['user_full_name'];
              } ?></p>
              <h6 style="word-wrap: break-word;"><?php echo $timelineData['complains_track_msg']; ?></h6>
              
              <p><?php if($timelineData['technician']!='') { echo 'Technician : '.$timelineData['technician']; } ?></p>
              <?php if ($timelineData['complains_track_img']!='') { 
                $ext = pathinfo($timelineData['complains_track_img'], PATHINFO_EXTENSION);
                if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                  $imgIcon = 'img/jpg.png';
                } else{
                  $imgIcon = '';
                }

                if ($imgIcon!="") {
                ?>
                <a data-fancybox="images" data-caption="Photo Name : <?php echo $timelineData["complains_track_img"]; ?>" target="_blank" href="../img/complain/<?php echo $timelineData['complains_track_img']; ?>">
                  <img class="lazyload" src="../img/ajax-loader.gif" data-src="../img/complain/<?php echo $timelineData['complains_track_img'] ?>" width="100" height="100">
                </a>
              <?php }  else {  ?>
                <a  target="_blank" href="../img/complain/<?php echo $timelineData['complains_track_img']; ?>">
                <i class="fa fa-paperclip"></i>  Attachment 
                </a>
              <?php } } ?>
              <?php if ($timelineData['complains_track_voice']!='') { ?>
                <div class="table-responsive">
                <audio controls>
                  <source src="../img/complain/<?php echo $timelineData['complains_track_voice']; ?>" type="audio/mp3">
                </audio> 
                </div>
              <?php } ?>
            
              <p><?php echo date('d M Y h:i A',strtotime($timelineData['complains_track_date_time'])); ?> (<?php echo $timelineData['complaint_status_view']; ?>)</p>

              <span class="cd-timeline__date"><?php echo date('M d',strtotime($timelineData['complains_track_date_time'])); ?></span>
            </div>
          </div>
        <?php } ?>
      </div>
    </section>
  </div>
</div>
<!-- <script src="assets/js/jquery.min.js"></script> -->
<!-- <script src="assets/plugins/vertical-timeline/js/vertical-timeline.js"></script> -->

<script type="text/javascript">
  function vieComplain(complain_id) {
      $.ajax({
          url: "getComplaineDetails.php",
          cache: false,
          type: "POST",
          data: {complain_id : complain_id},
          success: function(response){
              $('#comResp').html(response);
            
              
          }
       });
  } 
</script>



<div class="modal fade" id="vieComp">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-success">
      <div class="modal-header bg-success">
        <h5 class="modal-title text-white">View Complain </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="comResp">
          
      </div>
     
    </div>
  </div>
</div>