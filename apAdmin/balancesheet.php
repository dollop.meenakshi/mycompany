 <?php 
    if(isset($_POST['balancesheet_id_edit'])) {
    $btnName="Update";
    extract(array_map("test_input" , $_POST));
    $q=$d->select("balancesheet_master","balancesheet_id='$balancesheet_id_edit' AND society_id='$society_id'");
    $data=mysqli_fetch_array($q);

    $society_paymentAray = explode(",", $data['society_payment_getway_id']);
    } else {
    $btnName="Add";
    }

    $q=$d->select("society_master","society_id='$society_id'");
    $bData=mysqli_fetch_array($q);
    $country_id= $bData['country_id'];
     ?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> <?php echo $xml->string->balancesheet; ?></h4>
        
     </div>
    
     </div>
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="balancesheet" action="controller/balancesheetController.php" method="post" enctype="multipart/form-data">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                    <?php echo $xml->string->balancesheet; ?>
                </h4>
               
                <div class="form-group row">
                  <label for="input-15" class="col-sm-3 col-form-label"><?php echo $xml->string->balancesheet; ?> <?php echo $xml->string->name; ?> <span class="required">*</span></label>
                  <div class="col-sm-9">
                     <?php if(isset($_POST['balancesheet_id_edit'])) { ?>
                      <input type="text" value="<?php echo $data['balancesheet_name']; ?>" class="form-control " id="input-15" name="balancesheet_name">
                      <input type="hidden" name="balancesheet_id_edit" value="<?php echo $data['balancesheet_id']; ?>">
                     <?php } else { ?>
                      <input required="" type="text" class="form-control " id="input-15" name="balancesheet_name_add">

                     <?php } ?>
                  </div>
                </div>
                 <div class="form-group row">
                  <label for="input-15" class="col-sm-3 col-form-label"><?php echo $xml->string->balancesheet; ?> <?php echo $xml->string->block_type; ?> </label>
                  <div class="col-sm-9">
                    <select  type="text"  class="form-control single-select" name="block_id" >
                        <option value="0">General</option>
                        <?php 
                          $q3=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly","");
                           while ($blockRow=mysqli_fetch_array($q3)) {
                          ?>
                          <option  <?php if($data['block_id']==$blockRow['block_id']){ echo "selected"; } ?> value="<?php echo $blockRow['block_id'];?>"><?php echo $blockRow['block_name'];?> - <?php echo $xml->string->block; ?></option>
                          <?php } ?>
                        </select>
                        <i><?php echo $xml->string->keep_general_or; ?> <?php echo $xml->string->block; ?> <?php echo $xml->string->wise; ?> </i>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-15" class="col-sm-3 col-form-label"><?php echo $xml->string->payment_gateway; ?> </label>
                  <div class="col-sm-9">
                    <select multiple="multiple" name="society_payment_getway_id[]" class="form-control multiple-select">
                      <option value=""> -- <?php echo $xml->string->select; ?> --</option>
                      <?php
                      $q=$d->select("society_payment_getway","status=0 AND is_upi=0");
                      while ($data11=mysqli_fetch_array($q)) {
                      ?>
                      <option  <?php  if(isset($_POST['balancesheet_id_edit']) && in_array($data11['society_payment_getway_id'], $society_paymentAray )){ echo "selected"; } ?>     value="<?php echo $data11['society_payment_getway_id']; ?>"><?php echo $data11['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <?php if($country_id==101) { ?>
                <div class="form-group row">
                  <label for="input-15" class="col-sm-3 col-form-label"><?php echo $xml->string->upi; ?> </label>
                  <div class="col-sm-9">
                    <select  name="society_payment_getway_id_upi" class="form-control">
                      <option value=""> -- <?php echo $xml->string->select; ?>  --</option>
                      <?php
                      $q111=$d->select("society_payment_getway","status=0 AND is_upi=1");
                      while ($data22=mysqli_fetch_array($q111)) {
                      ?>
                      <option  <?php  if(isset($_POST['balancesheet_id_edit']) && $data22['society_payment_getway_id']==$data['society_payment_getway_id_upi']){ echo "selected"; } ?>     value="<?php echo $data22['society_payment_getway_id']; ?>"><?php echo $data22['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <?php } ?>
                <div class="form-footer text-center">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->save; ?> </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->