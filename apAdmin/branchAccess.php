<?php error_reporting(0);?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-3 col-md-6 col-6">
			<h4 class="page-title">Branch Restrict</h4>
			</div>
			<div class="col-sm-3 col-md-6 col-6">
			<div class="btn-group float-sm-right">
				
			</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-bordered">
								<thead>
									<tr>
										<th>Sr.No</th>                        
										<th>Branch</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$i=1;
									$q = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_sort ASC");
								
									$counter = 1;
									while ($data=mysqli_fetch_array($q)) {
									
									?>
									<tr>
										<td><?php echo $counter++; ?></td>
										<td><?php echo $data['block_name']; ?></td>
										<td>
											<div class="d-flex align-items-center">
												<form action="addBranchAccess" method="POST">
													<input type="hidden" name="block_id" value="<?php echo $data['block_id'] ?>">
													<button type="submit" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-pencil"></i></button>
												</form>
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
