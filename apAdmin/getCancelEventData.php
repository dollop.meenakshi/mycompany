<?php 
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

if (isset($_COOKIE['bms_admin_id'])) {
    $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
extract(array_map("test_input" , $_POST));
$checS=$d->select("society_master","society_id='$society_id'");
$sData=mysqli_fetch_array($checS);
$currency= $sData['currency'];
$virtual_wallet= $sData['virtual_wallet'];

//IS_605
if(isset($event_attend_id)  ){

  $q=$d->select("event_master,event_attend_list,event_days_master","event_days_master.events_day_id=event_attend_list.events_day_id AND event_attend_list.event_id=event_master.event_id AND  event_master.society_id = '$society_id' AND  event_attend_list.event_attend_id='$event_attend_id' AND event_attend_list.book_status=1","order by event_attend_list.event_attend_id DESC LIMIT 100");
$row=mysqli_fetch_array($q);
extract($row);
 ?>

 
<form id="EditCommEntryFrm" action="controller/eventController.php" method="POST" enctype="multipart/form-data" >
              <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />  
                 
                <div class="form-group row">
                 
               <label for="input-13" class="col-sm-4 col-form-label"> Day Name</label>
                  <div class="col-sm-8">
                    <?php echo $event_day_name;?>
                  </div> 
                </div>
                 <div class="form-group row">
                    <label for="input-14" class="col-sm-4 col-form-label">Date </label>
                  <div class="col-sm-8">
                    <?php echo $event_date;?>
                  </div>
                </div>
                <div class="form-group row">
                    <label for="input-14" class="col-sm-4 col-form-label">Received Amount </label>
                  <div class="col-sm-8">
                    <?php echo $recived_amount=$recived_amount-$transaction_charges;?>
                  </div>
                </div>
                <div class="form-group row">
                    <label for="input-14" class="col-sm-4 col-form-label">Refund Amount <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                    <input type="text"  min="0" max="<?php echo $recived_amount;?>" name="expenses_amount" value="<?php echo $recived_amount;?>" maxlength="10"   class="form-control onlyNumber" inputmode="numeric">
                  </div>
                </div>
               <div class="form-group row">
                    <label for="input-14" class="col-sm-4 col-form-label">Refund Mode <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                    <select required="" name="refund_type" class="form-control">
                      <option value="">-- Select --</option>
                      <option value="0">Cash</option>
                      <?php if($virtual_wallet==0) { ?>
                      <!-- <option value="1">User Wallet</option> -->
                    <?php } ?>
                    </select>
                  </div>
                </div>
               <div class="form-group row">
                <label for="input-14" class="col-sm-4 col-form-label">Remark <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                  <textarea maxlength="100" required="" type="text" class="form-control " name="expenses_title" id="expenses_title"></textarea>
                </div>
                </div>
                  
                 
                     <input type="hidden" name="cancelEventPaid" value="cancelEventPaid">
                  <input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id ?>">
                  <input type="hidden" name="event_day_name" value="<?php echo $event_day_name ?>">
                  <input type="hidden" name="event_date" value="<?php echo $event_date ?>">
                  <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
                  <input type="hidden" name="event_id" value="<?php echo $event_id ?>">
                  <input type="hidden" name="event_attend_id" value="<?php echo $event_attend_id ?>">
                  <input type="hidden" name="recived_amount" value="<?php echo $recived_amount ?>">
                  <input type="hidden" name="gst" value="<?php echo $gst ?>">
                  <input type="hidden" name="is_taxble" value="<?php echo $is_taxble ?>">
                  <input type="hidden" name="taxble_type" value="<?php echo $taxble_type ?>">
                  <input type="hidden" name="tax_slab" value="<?php echo $tax_slab ?>">
                    <div class="form-footer text-center">
                    <input type="Hidden" name=" " value="<?php echo $common_entry_id;?>">
                    <input type="submit" id="updateCommEntryBtn" class="btn form-btn btn-primary" name="updateCommEntry" value="Cancel Booking">
                  </div>
                  
              </form>
              <!-- <script src="assets/js/jquery.min.js"></script>
              <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script> -->
 <script type="text/javascript">
   //12march2020

    jQuery(document).ready(function($){
$.validator.addMethod('filesize4MB', function (value, element, arg) {
        var size =4000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 4MB."));

 $.validator.addMethod("noSpace", function(value, element) { 
   return value == '' || value.trim().length != 0;  
}, "No space please and don't leave it empty");



 
$("#EditCommEntryFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            expenses_title:
            {
                required: true,
                noSpace: true 
            },
             expenses_amount:
            {
                required: true,
                noSpace: true 
            },
            common_entry_image:
            {
                required: false,
                filesize4MB: true 
            },
        },
        messages: {
            expenses_title: {
                required: "Please enter remark", 
       }
      },
      submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
});
});
//12march2020
 </script>
        
<?php }   ?>