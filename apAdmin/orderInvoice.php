
<?php 
error_reporting(0);
if($_GET){
  $url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
  $activePage = basename($_SERVER['PHP_SELF'], ".php");
  include_once 'lib/dao.php';
  include 'lib/model.php';
  $d = new dao();
  $m = new model();
  $con=$d->dbCon();
  extract(array_map("test_input" , $_GET));
  // if (!isset($societyid)) {
  //   echo "Invalid Url !";
  //   exit();
  // }
  include 'common/checkLanguage.php';
    $q=$d->select("order_master,users_master","order_master.user_id=users_master.user_id AND order_master.order_id='$_GET[id]' ");
    $data = mysqli_fetch_array($q);
	extract($data);
  $invoiceNumber= "#ORDER$data[order_id]";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>#INVPN116 Invoice</title>
<link rel="icon" href="../img/fav.png" type="image/png">
<meta name="author" content="harnishdesign.net">
<meta name="viewport" content="width=1024">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' type='text/css'>
<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="assets/css/invoice.css"/>

</head>
<body id="printableArea"  >
  <div class="container-fluid invoice-container">
  <div class="row no-print" id="printPageButton">
    <div class="col-lg-12 text-center">
      <a href="#" onclick="window.history.go(-1); return false;"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Back</a>
      <a href="#" onclick="printDiv('printableArea')"  class="btn btn-sm btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
    </div>
  </div>
  <div >
    <header>
      <n>Original for Recipient</n>
    <div class="row align-items-center">
      <div class="col-sm-7 text-center text-sm-left mb-3 mb-sm-0"><br />
                <img width="200" id="logo" src="" title="invoice" alt="" />
              </div>
      <div class="col-sm-5 text-center text-sm-right">
        <h2 class="text-primary mb-0"> Invoice</h2>
        <h6 class="text-secondary mb-0">Invoice No.: <i class="text-danger"><?php echo $invoiceNumber; ?></i></h6>
        <h6 class="text-secondary mb-0">Category : Order</h6>
      </div>
    </div>
    </header>
    <main>
  
    <div class="row">
      <div class="col-sm-6 "> <strong>Developer's Company</strong>
        <address>
        Parshwa Tower, Above Kotak Mahindra Bank, SG Highway, Bodakdev, near Pakwan II, Ahmedabad, Gujarat, Ahmedabad<br>
         -  (380054)
  	   <br />
       <br />
               </address>
      </div>
    
    </div> 

     <div class="row">
      <div class="col-sm-4 "> Order By
        <address>
        <?php echo $data['user_full_name']; ?>
        <strong> </strong> <br/>
      <br>
     
       <br/>
              </address>
      </div>
      <div class="col-sm-4 "> Order Number
        <address>
        <?php echo $data['order_no']; ?>
        <strong> </strong> <br/>
      <br>
     
       <br/>
              </address>
      </div> 
      <div class="col-sm-4 "> Order Date
        <address>
        <?php echo date("d M Y h:i A", strtotime($data['order_date'])); ?>
        <strong> </strong> <br/>
      <br>
     
       <br/>
              </address>
      </div>


  <div class="table-responsive">
  <table class="table">
    <thead style='background-color:#6e767b;color: white;'>
      <tr>
        <th scope="col">#</th>
        <th style="max-width: 280px;" scope="col">Variant Name</th>
        <th style="max-width: 280px;" scope="col">Quantity</th>
        <th scope="col" class="text-right">Price</th>
      </tr>
    </thead>
    <tbody>
      <!-- <tr>
        <th scope="row">1</th>
        <td style="max-width: 280px;">
          Basic Salary
        </td>       
        <td>tj</td>
      </tr> -->
      <?php 
          $q=$d->select("order_product_master,vendor_product_category_master,vendor_product_master,vendor_master","order_product_master.vendor_id=vendor_master.vendor_id AND order_product_master.vendor_product_id=vendor_product_master.vendor_product_id AND order_product_master.vendor_product_category_id=vendor_product_category_master.vendor_product_category_id AND order_product_master.order_id='$data[order_id]'");
          $counter = 1;
          while ($purchaseItem=mysqli_fetch_array($q)) {
        ?>
          <tr>
              <td scope="row"><?php echo $counter++; ?></td>
              <td style="max-width: 280px;"><?php echo $purchaseItem['vendor_product_variant_name']; ?></td>
              <td style="max-width: 280px;"><?php echo $purchaseItem['vendor_product_quantity']; ?></td>
              <td align="right"style="max-width: 280px;"><?php echo $purchaseItem['vendor_product_variant_price']; ?></td>
              
          </tr>
        <?php } ?>
        <tr>
        <th scope="row"></th>
      
        <td><strong>Grand Total</strong></td>
        <th scope="row"></th>
        <td align="right"><strong><?php echo $data['order_total_amount']; ?></strong></td>
      </tr>
       
    </tbody>
  </table>
</div>
   </main>
     
    <br> <h4><span  class='card-title' ><u>Terms & Conditions</u></span></h4>    <footer class="text-center">
    <hr>
     <p class="text-gray-dark" align="center" style="color: #b5b5b5;">This is a computer generated invoice, thus no signature is required.</p>
   
    </footer>

   
  </div>
  </div>
</body>
<script type="text/javascript">
  function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }

  window.onload = function() { printDiv('printableArea'); }

    Android.print(printContents);
</script>
</html>
