<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title">Retailer Report</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $q = $d->selectRow("rom.route_name,rm.*,um.user_full_name,amn.area_name,dm.distributor_name","retailer_master AS rm JOIN route_retailer_master AS rrm ON rrm.retailer_id = rm.retailer_id JOIN route_employee_assign_master AS ream ON ream.route_id = rrm.route_id JOIN users_master AS um ON um.user_id = ream.user_id JOIN route_master AS rom ON rom.route_id = rrm.route_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id JOIN retailer_distributor_relation_master AS rdrm ON rdrm.retailer_id = rm.retailer_id JOIN distributor_master AS dm ON dm.distributor_id = rdrm.distributor_id");
                            $i = 1;
                            ?>
                            <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Retailer</th>
                                        <th>Contact Person</th>
                                        <th>Contact Person Number</th>
                                        <th>Address</th>
                                        <th>Area</th>
                                        <th>Route Name</th>
                                        <th>Pincode</th>
                                        <th>Distributor</th>
                                        <th>GST</th>
                                        <th>Remarks</th>
                                        <th>Credit Limit</th>
                                        <th>Credit Days</th>
                                        <th>Retailer Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($data = $q->fetch_assoc())
                                    {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $data['user_full_name']; ?></td>
                                        <td><?php echo $data['retailer_name']; ?></td>
                                        <td><?php echo $data['retailer_contact_person']; ?></td>
                                        <td><?php echo $data['retailer_contact_person_country_code']. " " .$data['retailer_contact_person_number']; ?></td>
                                        <td><?php echo chunk_split($data['retailer_address'],50,"<br>"); ?></td>
                                        <td><?php echo $data['area_name']; ?></td>
                                        <td><?php echo $data['route_name']; ?></td>
                                        <td><?php echo $data['retailer_pincode']; ?></td>
                                        <td><?php echo $data['distributor_name']; ?></td>
                                        <td><?php echo $data['retailer_gst_no']; ?></td>
                                        <td><?php echo $data['retailer_remark']; ?></td>
                                        <td><?php echo $data['retailer_credit_limit']; ?></td>
                                        <td><?php echo $data['retailer_credit_days']; ?></td>
                                        <td><?php echo $data['retailer_code']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div>