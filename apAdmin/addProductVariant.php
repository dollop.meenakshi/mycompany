<?php
if(isset($_POST['addProductVariantBtn']) && $_POST['addProductVariantBtn'] == "addProductVariantBtn")
{
    extract($_POST);
}
if(isset($_POST['editProductVariant']) && isset($_POST['product_variant_id']))
{
    $product_variant_id = $_POST['product_variant_id'];
    $q = $d->select("retailer_product_variant_master","product_variant_id = '$product_variant_id'");
    $data = $q->fetch_assoc();
    extract($data);
    $form_id = "productVariantAddForm";
}
else
{
    $form_id = "productVariantAddForm";
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="<?php echo $form_id; ?>" action="controller/orderProductController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <?php
                            if(isset($_POST['editProductVariant']) && isset($_POST['product_variant_id']))
                            {
                            ?>
                            <i class="fa fa-edit"></i>
                            Edit Product Variant
                            <?php
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-plus"></i>
                            Add Product Variant
                            <?php
                            }
                            ?>
                            </h4>
                            <div class="form-group row">
                                <label for="product_id" class="col-sm-2 col-form-label">Product <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="product_id" name="product_id" required>
                                        <option value="">-- Select --</option>
                                        <?php
                                        $qp = $d->selectRow("rpm.product_id,rpm.product_name","retailer_product_master AS rpm JOIN product_category_master AS pcm ON pcm.product_category_id = rpm.product_category_id","rpm.product_status = 0 AND rpm.product_delete = 0 AND pcm.product_category_status = 0 AND product_category_delete = 0");
                                        while ($dp = $qp->fetch_assoc())
                                        {
                                        ?>
                                        <option <?php if($product_id == $dp['product_id']) { echo 'selected'; } ?> value="<?php echo $dp['product_id'];?>"><?php echo $dp['product_name'];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="product_variant_name" class="col-sm-2 col-form-label">Variant Name <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editProductVariant)){ echo $product_variant_name; } ?>" required type="text" class="form-control" name="product_variant_name" id="product_variant_name" maxlength="150">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="retailer_selling_price" class="col-sm-2 col-form-label">Retailer Selling Price <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editProductVariant)){ echo $retailer_selling_price; } ?>" required type="text" class="form-control onlyNumber preventDD" inputmode="numeric" name="retailer_selling_price" id="retailer_selling_price" maxlength="10">
                                </div>
                                <label for="maximum_retail_price" class="col-sm-2 col-form-label">MRP</label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editProductVariant)){ echo $maximum_retail_price; } ?>" type="text" class="form-control onlyNumber preventDD" inputmode="numeric" name="maximum_retail_price" id="maximum_retail_price" maxlength="10">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="menufacturing_cost" class="col-sm-2 col-form-label">Manufacturing Cost</label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editProductVariant)){ echo $menufacturing_cost; } ?>" type="text" class="form-control onlyNumber preventDD" inputmode="numeric" name="menufacturing_cost" id="menufacturing_cost" maxlength="10">
                                </div>
                                <label for="unit_measurement_id" class="col-sm-2 col-form-label">Unit <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="unit_measurement_id" name="unit_measurement_id" required>
                                        <option value="">-- Select --</option>
                                        <?php
                                            $qu = $d->selectRow("unit_measurement_id,unit_measurement_name","unit_measurement_master","unit_measurement_status = 0 AND unit_measurement_delete = 0");
                                            while ($qdu = $qu->fetch_assoc())
                                            {
                                            ?>
                                        <option <?php if($unit_measurement_id == $qdu['unit_measurement_id']) { echo 'selected'; } ?> value="<?php echo $qdu['unit_measurement_id']; ?>"><?php echo $qdu['unit_measurement_name'];?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="variant_bulk_type" class="col-sm-2 col-form-label">Bulk Type <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="variant_bulk_type" name="variant_bulk_type" required>
                                        <option value="">-- Select --</option>
                                        <option value="0" <?php if(isset($editProductVariant) && $variant_bulk_type == 0){ echo "selected"; } ?>>Piece wise</option>
                                        <option value="1" <?php if(isset($editProductVariant) && $variant_bulk_type == 1){ echo "selected"; } ?>>Box wise</option>
                                    </select>
                                </div>
                                <label for="variant_per_box_piece" class="col-sm-2 col-form-label shd d-none">No Of Piece In Box <span class="text-danger">*</span></label>
                                <div class="col-sm-4 shd d-none">
                                    <input value="<?php if(isset($editProductVariant) && $variant_per_box_piece != 0){ echo $variant_per_box_piece; } ?>" required type="text" class="form-control onlyNumber preventD" inputmode="numeric" name="variant_per_box_piece" id="variant_per_box_piece" maxlength="10">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sku" class="col-sm-2 col-form-label">SKU</label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editProductVariant)){ echo $sku; } ?>" type="text" class="form-control" name="sku" id="sku" maxlength="64">
                                </div>
                                <label for="weight_in_kg" class="col-sm-2 col-form-label">Weight IN K.G</label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editProductVariant)){ echo $weight_in_kg; } ?>" type="text" class="form-control onlyNumber preventDD" name="weight_in_kg" id="weight_in_kg" maxlength="9">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="variant_photo" class="col-sm-2 col-form-label">Variant Image</label>
                                <div class="col-sm-4">
                                    <input type="file" id="variant_photo" name="variant_photo" inputmode="numeric" class="form-control-file border imageOnly" accept="image/*">
                                </div>
                                <label for="photo_preview" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-4">
                                    <img id="blah" src="../img/vendor_product/<?php if(isset($editProductVariant) && $variant_photo != ""){ echo $variant_photo; } ?>" width="100" height="100" alt="Variant Image" class='profile d-none'/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="variant_description" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea rows="3" id="variant_description" name="variant_description" maxlength="1000" class="form-control"><?php if(isset($editProductVariant) && $variant_description != "") { echo $variant_description; } ?></textarea>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                            <div class="form-footer text-center">
                                <input type="hidden" name="addProductVariant" value="addProductVariant">
                                <?php
                                if(isset($editProductVariant))
                                {
                                ?>
                                <input type="hidden" name="old_photo" value="<?php echo $variant_photo; ?>">
                                <input type="hidden" class="form-control" name="product_variant_id" id="product_variant_id" value="<?php echo $product_variant_id; ?>">
                                <input type="hidden" class="form-control" name="product_category_id" id="product_category_id" value="<?php echo $product_category_id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script>
$().ready(function()
{
    $('.preventDD').keypress(function (e)
    {
        var character = String.fromCharCode(e.keyCode)
        var newValue = this.value + character;
        if ((isNaN(newValue) || parseFloat(newValue) * 100 % 1 > 0) || (e.keyCode === 46 && this.value.split('.').length === 2))
        {
            e.preventDefault();
            return false;
        }
    });
    $('.preventD').keypress(function (e)
    {
        if (e.keyCode === 190 || e.keyCode === 110 || e.keyCode == 46)
        {
            e.preventDefault();
            return false;
        }
    });
    <?php
    if(isset($editProductVariant))
    {
        if($variant_photo != "" && file_exists("../img/vendor_product/".$variant_photo))
        {
    ?>
    $('#blah').removeClass('d-none');
    $('#blah').attr('src', '../img/vendor_product/<?php echo $variant_photo; ?>');
    <?php
        }
    ?>
    $('#variant_photo').rules('remove', 'required');
    <?php
        if($variant_bulk_type == 1)
        {
        ?>
        $('.shd').removeClass('d-none');
        <?php
        }
    }
    ?>
});

function readURL(input)
{
    if (input.files && input.files[0] && input.files[0].size <= 3000000 )
    {
        var reader = new FileReader();
        reader.onload = function(e)
        {
            $('#blah').removeClass('d-none');
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
    else
    {
        $('#blah').addClass('d-none');
    }
}

$("#variant_photo").change(function()
{
    readURL(this);
});

$(function()
{
    $("#variant_bulk_type").change(function()
    {
        var val = $('option:selected', this).val();
        if(val == 0)
        {
            $('.shd').addClass("d-none");
        }
        else
        {
            $('.shd').removeClass("d-none");
        }
    });
});
</script>