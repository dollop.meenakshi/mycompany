<?php
extract(array_map("test_input" , $_POST));
if (isset($notice_board_id_read)) {
  $q=$d->select("notice_board_master","society_id='$society_id' AND notice_board_id='$notice_board_id_read'");
  $row=mysqli_fetch_array($q);

   $q3=$d->select("unit_master,block_master,users_master,floors_master,notice_board_read_status","notice_board_read_status.user_id=users_master.user_id AND notice_board_read_status.notice_board_id='$notice_board_id_read' AND users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  AND users_master.user_status!=0 $blockAppendQuery","ORDER BY block_master.block_sort ASC");
   $userIdArray = array();
   $userIdOnly = array();
   while ($araData=mysqli_fetch_array($q3)) {
     array_push($userIdArray, $araData);
     array_push($userIdOnly, $araData['user_id']);
   } 

    


  ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title"><?php echo $xml->string->notice_board; ?> <?php echo $xml->string->read_status; ?> </h4>
        <div class="table-responsive">
        <table class="table table-bordered bg-white">
          <tbody>
            <tr>
              <th><?php echo $xml->string->title; ?></th>
              <td><?php echo $row['notice_title']; ?></td>
            </tr>
           
            <tr>
              <th><?php echo $xml->string->description_contact_finca_fragment; ?></th>
              <td> <div class="cls-notice-info" style="overflow-x:auto !important;"><?php echo $row['notice_description']; ?></div></td>
            </tr>
            <tr>
              <th><?php echo $xml->string->updated_on; ?></th>
              <td><?php echo  date('d M Y h:i A',strtotime($row['updated_at'])); ?></td>
            </tr>
            <tr>
              <th><?php echo $xml->string->blocks; ?></th>
              <td><?php
                if($row['block_id'] =="0" || $row['block_id'] ==""){
                    $block_name_data = $xml->string->all;;
                  } else{ 

                    $block_master_qry=$d->select("block_master","block_id in (".$row['block_id'].")  ","");
                    $block_name_data = array();
                     while ($block_master_data=mysqli_fetch_array($block_master_qry)) {
                        $block_name_data[] = $block_master_data['block_name'];
                     }

                      $block_name_data = implode(", ",  $block_name_data);
                    //$block_master_data =   mysqli_fetch_assoc($block_master_qry);
                    //$block_name_data = $block_master_data['block_name'];
                  }
                  echo $block_name_data ;
              if ($row['noticeboard_type']==1) {
                echo  '('.$d->count_data_direct("notice_board_unit_id","notice_board_unit_master","society_id='$society_id' AND notice_board_id='$row[notice_board_id]'").' Employees)';
              }
            ?></td>
            </tr>
            <tr>
              <th><?php echo $xml->string->attachment; ?> </th>
              <td>
                <?php if ($row['notice_attachment']!='') {
                  echo "<a target='_blank' href=../img/noticeBoard/$row[notice_attachment]><i class='fa fa-paperclip'></i> ".$xml->string->attachment."</a>";
                  echo "<br>";
                } else {
                  echo $xml->string->not_available;
                } ?>
              </td>
            </tr>
             <tr>
              <th><?php echo $xml->string->total; ?> <?php echo $xml->string->read; ?> <?php echo $xml->string->users; ?>  </th>
              <td>
                <?php echo $aleradyReadCount= count($userIdArray);
                     $qry ="";
                     $newqry="";
                     $aleradyRead= "";
                 if($row['block_id']!=0){
                    $block_ids = explode(",", $row['block_id']);
                    $ids = join("','",$block_ids); 
                    $block_id_arr = $block_ids;
                    $qry =" and users_master.block_id IN ('$ids') ";
                } 
                /* if ($row['noticeboard_type']==1) {
                    $oldUnitAray = array();
                    $q11=$d->select("notice_board_unit_master","society_id='$society_id' AND notice_board_id='$notice_board_id_read'");
                    while($rowUnit=mysqli_fetch_array($q11)) {
                      array_push($oldUnitAray, $rowUnit['unit_id']);
                    }
                    $ids1 = join("','",$oldUnitAray); 
                    $newqry =" and users_master.unit_id IN ('$ids1') ";
                } */
                if ($row['noticeboard_type']==1) {
                  $oldFloorAray = array();
                  $q11=$d->select("notice_board_unit_master","society_id='$society_id' AND notice_board_id='$notice_board_id_read'");
                  while($rowFloor=mysqli_fetch_array($q11)) {
                    array_push($oldFloorAray, $rowFloor['floor_id']);
                  }
                  $ids2 = join("','",$oldFloorAray); 
                  $newFloorQry =" and users_master.floor_id IN ('$ids2') ";
              }
              
                if ($aleradyReadCount>0) {
                   $ids2 = join("','",$userIdOnly); 
                    $ALreadyReadQuery =" and users_master.user_id NOT IN ('$ids2') ";
                }
                 ?>
              </td>
            </tr>
            <tr>
              <th><?php echo $xml->string->total; ?> <?php echo $xml->string->unread; ?> <?php echo $xml->string->users; ?>  </th>
              <td>
                <form action="controller/noticeController.php" method="post" >
                <?php 
                   $qunRad=$d->select("unit_master,block_master,users_master,floors_master","users_master.user_token!='' AND users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  AND users_master.user_status!=0 $qry $newFloorQry $ALreadyReadQuery  $blockAppendQuery","ORDER BY block_master.block_sort ASC");
                   echo $totalUnread=  mysqli_num_rows($qunRad); 
                   echo " (Only App Usage Users)";
                   if ($totalUnread>0 && $row['active_status']==0) {
                 ?>
                <input type="hidden" name="notice_board_id_unread" value="<?php echo $notice_board_id_read; ?>">
                <button type="submit" class="btn form-btn btn-success btn-sm waves-effect waves-light m-1 "><i class="fa fa-bell"></i> <?php echo $xml->string->send; ?> <?php echo $xml->string->reminder; ?> <?php echo $xml->string->to; ?> <?php echo $xml->string->unread; ?> <?php echo $xml->string->users; ?></button>
              <?php } ?>
              </form>
              </td>
            </tr>
             
          </tbody>
        </table>
        </div>
      </div>
    </div>
     <div class="row pt-2 pb-2">
      <div class="col-sm-12 text-center">
          <div class="card">
            <div class="card-header"><i class="fa fa-users"></i> <?php echo $xml->string->read; ?> <?php echo $xml->string->users; ?></div>
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                 // $q3=$d->select("users_master","society_id='$society_id'   ","ORDER BY user_id ASC");
                  $i=1;
               ?>
                
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $xml->string->block; ?></th>
                        <th><?php echo $xml->string->floor; ?></th>
                        <th><?php echo $xml->string->name; ?></th>
                        <th><?php echo $xml->string->mobile_no; ?></th>
                        <th><?php echo $xml->string->read; ?> <?php echo $xml->string->reminder_time; ?></th>
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  for ($i1=0; $i1 <count($userIdArray) ; $i1++) { 
                   ?>
                    <tr>
                       
                        <td><?php echo $i1+1; ?></td>
                        <td><?php echo $userIdArray[$i1]['block_name']; ?></td>
                        <td><?php 
                         echo $userIdArray[$i1]['floor_name'];
                         ?></td>
                        <td><?php
                     echo $userIdArray[$i1]['user_full_name'].' ('.$userIdArray[$i1]['user_designation'].')'; 
                        ?></td>
                        <td><?php  if ($adminData['role_id']==2) {
                          echo $userIdArray[$i1]['country_code'].' '. $userIdArray[$i1]['user_mobile']; 
                        } else {
                           if($userIdArray[$i1]['member_status']=="0" ){
                           echo $userIdArray[$i1]['country_code'].' '.$userIdArray[$i1]['user_mobile']; 
                           } else {
                            echo $userIdArray[$i1]['country_code'].' '."".substr($userIdArray[$i1]['user_mobile'], 0, 2) . '*****' . substr($userIdArray[$i1]['user_mobile'],  -3);
                           }
                        } ?></td>
                       <td><?php echo date("d M Y h:i A", strtotime($userIdArray[$i1]['read_time'])); ?></td>
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            

            </div>
            </div>
          </div>

      </div>
    </div>

    <div class="row pt-2 pb-2">
      <div class="col-sm-12 text-center">
          <div class="card">
            <div class="card-header"><i class="fa fa-users"></i> <?php echo $xml->string->unread; ?>  <?php echo $xml->string->users; ?>  </div>
            <div class="card-body">
              
              <div class="table-responsive">
                
              <table id="default-datatable1" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $xml->string->block; ?> </th>
                        <th><?php echo $xml->string->floor; ?> </th>
                        <th><?php echo $xml->string->name; ?> </th>
                        <th><?php echo $xml->string->mobile_no; ?> </th>
                    </tr>
                </thead>
                <tbody>
                  <?php 

                  $i2=1;
                 while($unData=mysqli_fetch_array($qunRad)) { 
                   ?>
                    <tr>
                       
                        <td><?php echo $i2++; ?></td>
                        <td><?php echo $unData['block_name']; ?></td>
                        <td><?php 
                       
                           echo $unData['floor_name'];
                         ?></td>
                        <td><?php
                          echo $unData['user_full_name'] .' ('.$unData['user_designation'].')'; 
                        
                        ?></td>
                        <td><?php  if ($adminData['role_id']==2) {
                          echo $unData['country_code'].' '.$unData['user_mobile']; 
                        } else {
                           if($unData['member_status']=="0" ){
                           echo $unData['country_code'].' '.$unData['user_mobile']; 
                           } else {
                            echo $unData['country_code'].' '."".substr($unData['user_mobile'], 0, 2) . '*****' . substr($unData['user_mobile'],  -3);
                           }
                        }

                         ?></td>
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            

            </div>
            </div>
          </div>

      </div>
    </div>
   
  </div>
</div>


<?php }  else { 

if (isset($notice_board_id)) {
  $q=$d->select("notice_board_master","society_id='$society_id' AND notice_board_id='$notice_board_id'");
  $row=mysqli_fetch_array($q);
  $selectedBlocks = explode(",",$row['block_id']);

  $ids = join("','",$selectedBlocks);   
  if ($ids!=0 || $ids!= '') {
    $appendOldBlaock= " AND unit_master.block_id IN ('$ids')";
  } 


  $difrentResult=array_diff($selectedBlocks,$blockAryAccess);
  $oldFloorAray = array();
  if ($row['noticeboard_type']==1) {
      $q11=$d->select("notice_board_unit_master","society_id='$society_id' AND notice_board_id='$notice_board_id'");
      while($rowFloor=mysqli_fetch_array($q11)) {
        array_push($oldFloorAray, $rowFloor['floor_id']);
      }
  }

  $oldZoneAray = array();
  $q11z=$d->select("notice_board_zone_master","society_id='$society_id' AND notice_board_id='$notice_board_id'");
  while($rowZone=mysqli_fetch_array($q11z)) {
    array_push($oldZoneAray, $rowZone['zone_id']);
  }

  $oldLevelAray = array();
  $q11l=$d->select("notice_board_level_master","society_id='$society_id' AND notice_board_id='$notice_board_id'");
  while($rowLevel=mysqli_fetch_array($q11l)) {
    array_push($oldLevelAray, $rowLevel['level_id']);
  }
}
// print_r($oldUnitAray);
// echo "string";
?>

<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title"><?php echo $xml->string->society; ?> <?php echo $xml->string->notice_board; ?></h4>
        
      </div>
      
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-5">
        <div class="card">
          <form enctype="multipart/form-data" id="noticeBoard" action="controller/noticeController.php" method="post">
            <div class="card-header text-uppercase bg-white"><?php echo $xml->string->notice_board; ?></div>
            
            <div class="card-body">
              <?php $totalZone = $d->count_data_direct("zone_id","zone_master","society_id='$society_id' AND zone_status = 0"); 
                    $totalLevel = $d->count_data_direct("level_id","employee_level_master","society_id='$society_id' AND level_status = 0");
              ?>
            <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified">
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#BranchWise" data-toggle="pill" class="nav-link active show"><i class="fa fa-building"></i> <span class="hidden-xs">BRANCH WISE</span></a>
                </li>
                <?php if(($totalZone > 0) || ($totalLevel > 0)){ ?>
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#ZoneAndLevelWise" data-toggle="pill" class="nav-link"><i class="fa fa-th-list"></i> <span class="hidden-xs">ZONE & LEVEL WISE </span></a>
                </li>
                <?php } ?>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="BranchWise">
                  <label for="input-10" class="col-form-label">Select <?php echo $xml->string->block; ?>  <span class="text-danger">*</span></label>
                  <select multiple="multiple" onchange="getnewFloorlist();" name="block_id[]" id="block_id" class="form-control multiple-select-notice noticeBoardMultiSelectCls" required=""   >
                    <option value="">--<?php echo $xml->string->block; ?>--</option>
                    <?php 
                    extract($_REQUEST);
                    $qss=$d->select("block_master" ,"society_id='$society_id' and block_status = 0  $blockAppendQuery ","ORDER BY block_sort ASC");

                            // IS_1209
                    if(mysqli_num_rows($qss) >0){
                      if ($blockAppendQueryOnly=='') {
                      ?>
                      <option class="deselectOther" <?php if (isset($notice_board_id) && $row['block_id'] ==0 ) {   echo "selected";}?>  value="0"><?php echo $xml->string->all; ?> <?php echo $xml->string->blocks; ?></option><?php
                      }
                    }
                            // IS_1209
                  
                    while ($sData=mysqli_fetch_array($qss)) {
                    ?>
                    <option class="unselectAll" <?php if (isset($notice_board_id) &&  in_array($sData['block_id'] , $selectedBlocks)) {   echo "selected";}?>   value="<?php echo $sData['block_id']; ?>"><?php echo $sData['block_name']; ?></option>
                  <?php } ?>
                  </select>
                  <?php //IS_983 ?>
                  <br><Br>

                  <label for="input-10" class="col-form-label">Select Department <span class="text-danger">*</span></label>
                    <select multiple="multiple" name="floor_id[]" id="floorList" class="form-control multiple-select-unit_notice"    >
                      <option value=""> All Department</option>
                      <?php 
                      if (isset($notice_board_id)) {
                      $ids = join("','",$selectedBlocks);  
                      } 
                        $q3=$d->select("floors_master, block_master","floors_master.floor_status=0 AND floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floors_master.block_id IN ('$ids') $blockAppendQuery","");
                        while ($floorRow=mysqli_fetch_array($q3)) {
                          ?>
                          <option  <?php if (isset($notice_board_id) &&  in_array($floorRow['floor_id'] , $oldFloorAray)) {   echo "selected";}?>   value="<?php echo $floorRow['floor_id'];?>"><?php echo $floorRow['floor_name'];?> (<?php echo $floorRow['block_name'];?>)</option>
                        <?php }?>
                  </select>
                  <?php //IS_983 ?>
                  <br><Br>
              </div>
              <div class="tab-pane " id="ZoneAndLevelWise">
                  

                  <?php 
                    if( $totalZone > 0) {
                  ?>
                  <label for="input-10" class="col-form-label">Select Zone </label>
                  <select multiple="multiple" name="zone_id[]" id="zone_id" class="form-control multiple-select-notice noticeBoardZoneMultiSelectCls" >
                    <option value="">-- Zone --</option>
                    <?php 
                    extract($_REQUEST);
                    $zq=$d->select("zone_master" ,"society_id='$society_id' AND zone_status = 0 ","");

                            // IS_1209
                    if(mysqli_num_rows($zq) >0){
                      if ($blockAppendQueryOnly=='') {
                      ?>
                      <option class="deselectOther" <?php if (isset($notice_board_id) && $row['noticeboard_zone_type'] == 0) {   echo "selected";}?>  value="0"><?php echo $xml->string->all; ?> Zone</option><?php
                      }
                    }
                            // IS_1209
                  
                    while ($zData=mysqli_fetch_array($zq)) {
                    ?>
                    <option class="unselectAll" <?php if (isset($notice_board_id) &&  in_array($zData['zone_id'] , $oldZoneAray)) {   echo "selected";}?>   value="<?php echo $zData['zone_id']; ?>"><?php echo $zData['zone_name']; ?></option>
                  <?php } ?>
                    </select>
                    <?php //IS_983 ?>
                    <br><Br>
                    <?php } ?>
                      <?php 
                        if( $totalLevel > 0) {
                      ?>
                    <label for="input-10" class="col-form-label">Select Employee Level </label>
                  <select multiple="multiple" name="level_id[]" id="level_id" class="form-control multiple-select-notice noticeBoardLevelMultiSelectCls" >
                    <option value="">-- Employee Level --</option>
                    <?php 
                    extract($_REQUEST);
                    $elq=$d->select("employee_level_master" ,"society_id='$society_id' and level_status = 0 ","");

                            // IS_1209
                    if(mysqli_num_rows($elq) >0){
                      if ($blockAppendQueryOnly=='') {
                      ?>
                      <option class="deselectOther" <?php if (isset($notice_board_id) && $row['noticeboard_level_type'] == 0) {   echo "selected";}?>  value="0"><?php echo $xml->string->all; ?> Employee Level</option><?php
                      }
                    }
                            // IS_1209
                  
                    while ($elData=mysqli_fetch_array($elq)) {
                    ?>
                    <option class="unselectAll" <?php if (isset($notice_board_id) &&  in_array($elData['level_id'] , $oldLevelAray)) {   echo "selected";}?>   value="<?php echo $elData['level_id']; ?>"><?php echo $elData['level_name']; ?></option>
                  <?php } ?>
                  </select>
                  
                  <?php //IS_983 ?>
                  <br><Br>
                  <?php } ?>
              </div>
            </div>
          
           <label for="input-10" class="col-form-label">Circular Title <span class="text-danger">*</span></label>
           <input required="" maxlength="250" class="form-control" type="text" name="notice_title" placeholder="Circular Title" value="<?php echo $row['notice_title'] ?>" >
           <br>
           <label for="input-10" class="col-form-label">Description </label>
           <textarea required="" id="summernoteImgage" name="notice_description"><?php echo $row['notice_description'];?></textarea>
          <br>
          <label for="input-14" class=""><?php echo $xml->string->attachment; ?></label><br>
           <input  class="fform-control-file border docOnly" type="file" name="notice_attachment" placeholder="Notice Board Title"  >
           <?php  if ($row['notice_attachment']!="") 
           { 
              $ext = pathinfo($row['notice_attachment'], PATHINFO_EXTENSION);
              if ($ext == 'pdf' || $ext == 'PDF') {
                $imgIcon = 'img/pdf.png';
              } elseif ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                $imgIcon = 'img/jpg.png';
              } elseif($ext == 'png'){
                $imgIcon = 'img/png.png';
              }elseif ($ext == 'doc' || $ext == 'docx') {
                $imgIcon = 'img/doc.png';
              } else{
                $imgIcon = 'img/doc.png';
              }
              ?>
              <a title="View" class="btn btn-link btn-sm" target="_blank" href="../img/noticeBoard/<?php echo $row['notice_attachment']; ?>"><div style="height: 70px;width: 70px;background-image: url(<?php echo $imgIcon ?>);background-repeat: no-repeat;background-size: contain;" class="d-block text-break"></div></a>
          <?php } ?>
          <br>
              <?php if (isset($notice_board_id) && $row['active_status']==1 || !isset($notice_board_id)) { ?>
                <label for="input-14" class=""><?php echo $xml->string->publish; ?></label><br>
              <?php } else { ?>
                <input type="hidden" name="active_status" value="<?php echo $row['active_status']; ?>">
              <?php } ?>
                <div class="col-sm-10">
                   <?php if (isset($notice_board_id) && $row['active_status']==1) {  ?>
                    <div class=" icheck-inline">
                      <input  <?php if($row['active_status']=='0'){echo "checked";} ?>    type="radio" id="publishNow" value="0" name="active_status">
                      <label for="publishNow"><?php echo $xml->string->publish_now; ?></label>
                    </div>
                    <div class="icheck-inline">
                      <input   <?php if($row['active_status']=='1'){echo "checked";} ?>   type="radio"  id="saveDraft" value="1" name="active_status">
                      <label for="saveDraft"><?php echo $xml->string->save_as_draft; ?> </label>
                    </div>
                   <?php } else if (!isset($notice_board_id)) { ?>
                    <div class=" icheck-inline">
                      <input  checked  type="radio" id="publishNow" value="0" name="active_status">
                      <label for="publishNow"><?php echo $xml->string->publish_now; ?></label>
                    </div>
                    <div class="icheck-inline">
                      <input    type="radio"  id="saveDraft" value="1" name="active_status">
                      <label for="saveDraft"><?php echo $xml->string->save_as_draft; ?> </label>
                    </div>
                   <?php } ?>
                </div>
                
          <div class="form-footer text-center">
            <?php if (isset($notice_board_id)) { ?>
              <input type="hidden" name="notice_attachment_old" value="<?php echo $row['notice_attachment']; ?>">
              <input type="hidden" name="notice_board_id" value="<?php echo $notice_board_id; ?>">
              <button name="" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i><?php echo $xml->string->update; ?></button>
              <input type="hidden" name="updateNotice" value="updateNotice">
            <?php } else { ?>
              <input type="hidden" name="addNotice" value="addNotice">
              <button name="" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i><?php echo $xml->string->add; ?></button>
            <?php } ?>
            
          </div>
          
        </form>
      </div>
    </div>
  </div>
</div>

</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<?php } ?>