<?php
if(isset($_POST['addAreaBtn']) && $_POST['addAreaBtn'] == "addAreaBtn")
{
    extract($_POST);
}
if(isset($_POST['editArea']) && isset($_POST['area_id']))
{
    $area_id = $_POST['area_id'];
    $q = $d->select("area_master_new","area_id = '$area_id'");
    $data = $q->fetch_assoc();
    extract($data);
    $form_id = "areaUpdateForm";
}
else
{
    $form_id = "areaAddForm";
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="<?php echo $form_id; ?>" action="controller/areaController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <?php
                            if(isset($_POST['editArea']) && isset($_POST['area_id']))
                            {
                            ?>
                            <i class="fa fa-edit"></i>
                            Edit Area
                            <?php
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-plus"></i>
                            Add Area
                            <?php
                            }
                            ?>
                            </h4>
                            <div class="form-group row">
                                <label for="country_id" class="col-sm-2 col-form-label">Country <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="country_id" name="country_id" required onchange="getStateCity(this.value);">
                                        <option value="">-- Select --</option>
                                        <?php
                                        $cq = $d->select("countries");
                                        while ($cd = $cq->fetch_assoc())
                                        {
                                        ?>
                                        <option <?php if($country_id == $cd['country_id']) { echo 'selected'; } ?> value="<?php echo $cd['country_id'];?>"><?php echo $cd['country_name'];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="state_city_id" class="col-sm-2 col-form-label">State-City <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="state_city_id" name="state_city_id" required>
                                        <option value="">-- Select --</option>
                                        <?php
                                        if(isset($editArea) || isset($addAreaBtn))
                                        {
                                            $qt = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id' AND c.flag = 1");
                                            while ($qd = $qt->fetch_assoc())
                                            {
                                            ?>
                                        <option <?php if(isset($city_id) && $city_id == $qd['city_id']) { echo 'selected'; } ?> value="<?php echo $qd['city_id']."-".$qd['state_id'];?>"><?php echo $qd['city_name'];?>-<?php echo $qd['state_name'];?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="area_name" class="col-sm-2 col-form-label">Area Name <span class="text-danger">*</span></label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editArea)){ echo $area_name; } ?>" required type="text" class="form-control" name="area_name" id="area_name" maxlength="100">
                                </div>
                                <label for="pincode" class="col-sm-2 col-form-label"><?php echo $xml->string->pincode; ?></label>
                                <div class="col-sm-4">
                                    <input type="text" id="pincode" maxlength="6" value="<?php if($pincode != 0) { echo $pincode; } ?>" class="form-control onlyNumber" inputmode="numeric" name="pincode">
                                </div>
                            </div>
                            <div class="form-group row">
                                <input id="searchInput5" class="form-control" type="text" placeholder="Enter a Google location" >
                                <div class="map" id="map" style="width: 100%; height: 400px;"></div>
                            </div>
                            <input type="hidden" class="form-control" id="lat" name="latitude" value="<?php echo $latitude; ?>">
                            <input type="hidden" class="form-control" id="lng"  name="longitude" value="<?php echo $longitude; ?>">
                            <input type="hidden" class="form-control" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                            <div class="form-footer text-center">
                                <input type="hidden" name="addArea" value="addArea">
                                <?php
                                if(isset($editArea))
                                {
                                ?>
                                <input type="hidden" class="form-control" name="area_id" id="area_id" value="<?php echo $area_id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $d->map_key();?>"></script>
<script>
function initialize()
{
    var latitute = document.getElementById('lat').value;
    var longitute = document.getElementById('lng').value;
    if(latitute == "")
    {
        latitute = 23.037786;
    }
    if(longitute == "")
    {
        longitute = 72.512043;
    }
    var latlng = new google.maps.LatLng(latitute,longitute);
    var map = new google.maps.Map(document.getElementById('map'), {
    center: latlng,
    zoom: 13
    });
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
    });
    var parkingRadition = 5;
    var citymap =
    {
        newyork:
        {
            center: {lat: latitute, lng: longitute},
            population: parkingRadition
        }
    };
    var input = document.getElementById('searchInput5');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    autocomplete10.addListener('place_changed', function()
    {
        infowindow.close();
        marker.setVisible(false);
        var place5 = autocomplete10.getPlace();
        if (!place5.geometry) {
        window.alert("Autocomplete's returned place5 contains no geometry");
        return;
        }
        // If the place5 has a geometry, then present it on a map.
        if (place5.geometry.viewport) {
        map.fitBounds(place5.geometry.viewport);
        } else {
        map.setCenter(place5.geometry.location);
        map.setZoom(17);
        }
        marker.setPosition(place5.geometry.location);
        marker.setVisible(true);
        var pincode="";
        for (var i = 0; i < place5.address_components.length; i++) {
        for (var j = 0; j < place5.address_components[i].types.length; j++) {
        if (place5.address_components[i].types[j] == "postal_code") {
        pincode = place5.address_components[i].long_name;
        }
        }
        }
        bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
        infowindow.setContent(place5.formatted_address);
        infowindow.open(map, marker);
    });
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function() {
    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
    {
        if (status == google.maps.GeocoderStatus.OK)
        {
            if (results[0])
            {
                var places = results[0];
                var pincode = "";
                var serviceable_area_locality= places.address_components[4].long_name;
                for (var i = 0; i < places.address_components.length; i++)
                {
                    for (var j = 0; j < places.address_components[i].types.length; j++)
                    {
                        if (places.address_components[i].types[j] == "postal_code")
                        {
                            pincode = places.address_components[i].long_name;
                        }
                    }
                }
                bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
            }
        }
    });
    });
}
function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality)
{
    document.getElementById('lat').value = lat;
    document.getElementById('lng').value = lng;
}
google.maps.event.addDomListener(window, 'load', initialize);

function getStateCity(country_id)
{
    $.ajax({
        url: 'controller/areaController.php',
        data: {getStateCityTag:"getStateCityTag",country_id:country_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#state_city_id').empty();
            response = JSON.parse(response);
            $('#state_city_id').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#state_city_id').append("<option value='"+value.city_id+"-"+value.state_id+"'>"+value.city_name+" - "+value.state_name+"</option>");
            });
        }
    });
}
</script>