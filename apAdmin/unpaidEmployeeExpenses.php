<?php
error_reporting(0);
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$bId = (int)$_REQUEST['bId'];
$uId = (int)$_REQUEST['uId'];
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$ueMonth = $_REQUEST['ueMonth'];
$ueYear = $_REQUEST['ueYear'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-md-9">
                <h4 class="page-title">Unpaid Expenses</h4>
            </div>
            <div class="col-md-3">
                <div class="btn-group float-sm-right">
                    <?php if($_GET['dId'] != '' && $_GET['ueYear'] > 0  && $_GET['ueMonth'] > 0 && ($_GET['uId'] != '' && $_GET['uId'] > 0)) { ?>
                    <a href="javascript:void(0)" onclick="PaidAll('payEmployeeExpense');" class="btn  btn-sm btn-primary pull-right"><i class="fa fa-money"></i> Paid </a>
                    <?php } else {
                    if($_GET['uId'] != '' && $_GET['uId'] > 0){
                    ?>
                    <a href="javascript:void(0)" onclick="PaidAll('payEmployeeExpense');" class="btn  btn-sm btn-primary pull-right"><i class="fa fa-money"></i> Paid </a>
                    <?php
                    }
                    }?>
                </div>
            </div>
        </div>
        <form class="branchDeptFilter" action="" >
            <div class="row pt-2 pb-2">
                <?php include 'selectBranchDeptEmpForFilterAll.php' ?>
                <div class="col-md-2 form-group">
                    <select name="ueMonth" class="form-control single-select">
                        <option <?php if($ueMonth=="0") { echo 'selected';} ?> value="0">All Month</option>
                        <option <?php if($ueMonth=="01") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '01') { echo 'selected';}?> value="01">January</option>
                        <option <?php if($ueMonth=="02") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '02') { echo 'selected';}?> value="02">February</option>
                        <option <?php if($ueMonth=="03") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '03') { echo 'selected';}?> value="03">March</option>
                        <option <?php if($ueMonth=="04") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '04') { echo 'selected';}?> value="04">April</option>
                        <option <?php if($ueMonth=="05") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '05') { echo 'selected';}?> value="05">May</option>
                        <option <?php if($ueMonth=="06") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '06') { echo 'selected';}?> value="06">June</option>
                        <option <?php if($ueMonth=="07") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '07') { echo 'selected';}?> value="07">July</option>
                        <option <?php if($ueMonth=="08") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '08') { echo 'selected';}?> value="08">August</option>
                        <option <?php if($ueMonth=="09") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '09') { echo 'selected';}?> value="09">September</option>
                        <option <?php if($ueMonth=="10") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '10') { echo 'selected';}?> value="10">October</option>
                        <option <?php if($ueMonth=="11") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '11') { echo 'selected';}?> value="11">November</option>
                        <option <?php if($ueMonth=="12") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '12') { echo 'selected';}?> value="12">December</option>
                    </select>
                </div>
                <div class="col-md-2 form-group">
                    <select name="ueYear" class="form-control single-select">
                        <option <?php if($ueYear=="0") { echo 'selected';} ?> value="0">All Year</option>
                        <option <?php if($ueYear==$twoPreviousYear) { echo 'selected';} ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                        <option <?php if($ueYear==$onePreviousYear) { echo 'selected';} ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                        <option <?php if($ueYear==$currentYear) { echo 'selected';}?> <?php if($ueYear == '') { echo 'selected';}?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                        <option <?php if($ueYear==$nextYear) { echo 'selected';} ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
                    </select>
                </div>
                <div class="col-md-2 form-group">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $i = 1;
                            if(isset($bId) && $bId>0) {
                                $BranchFilterQuery = " AND users_master.block_id='$bId'";
                              }
                            if(isset($dId) && $dId > 0)
                            {
                                $deptFilterQuery = " AND user_expenses.floor_id = '$dId'";
                            }
                            if(isset($uId ) && $uId > 0)
                            {
                                $userFilterQuery = " AND user_expenses.user_id = '$uId'";
                            }
                            if(isset($ueYear) && $ueYear > 0)
                            {
                                $yearFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%Y') = '$ueYear'";
                            }
                            if(isset($ueMonth) && $ueMonth > 0)
                            {
                                $monthFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%m') = '$ueMonth'";
                            }
                            if((isset($dId) && $dId > 0) || (isset($uId ) && $uId > 0) || (isset($ueYear) && $ueYear > 0) || (isset($ueMonth) && $ueMonth > 0))
                            {
                                $q = $d->selectRow("user_expenses.*,floors_master.*,users_master.*,approved_user.user_full_name AS approved_user_name,approved_admin.admin_name AS approved_admin_name,ecm.expense_category_name","user_expenses LEFT JOIN users_master AS approved_user ON approved_user.user_id = user_expenses.expense_approved_by_id LEFT JOIN bms_admin_master AS approved_admin ON approved_admin.admin_id = user_expenses.expense_approved_by_id LEFT JOIN expense_category_master AS ecm ON ecm.expense_category_id = user_expenses.expense_category_id,floors_master,users_master", "users_master.user_id = user_expenses.user_id AND user_expenses.floor_id = floors_master.floor_id AND user_expenses.society_id = '$society_id' AND user_expenses.expense_status = 1 AND user_expenses.expense_paid_status = 0 AND user_expenses.expense_type = 0 AND users_master.delete_status = 0 $deptFilterQuery $yearFilterQuery $BranchFilterQuery $monthFilterQuery $userFilterQuery $blockAppendQueryUser ", "ORDER BY user_expenses.date DESC");
                            }
                            else
                            {
                                $q = $d->selectRow("user_expenses.*,floors_master.*,users_master.*,approved_user.user_full_name AS approved_user_name,approved_admin.admin_name AS approved_admin_name,ecm.expense_category_name","user_expenses LEFT JOIN users_master AS approved_user ON approved_user.user_id = user_expenses.expense_approved_by_id LEFT JOIN bms_admin_master AS approved_admin ON approved_admin.admin_id = user_expenses.expense_approved_by_id LEFT JOIN expense_category_master AS ecm ON ecm.expense_category_id = user_expenses.expense_category_id,floors_master,users_master", "users_master.user_id = user_expenses.user_id AND user_expenses.floor_id = floors_master.floor_id AND user_expenses.society_id = '$society_id' AND user_expenses.expense_status = 1 AND user_expenses.expense_paid_status = 0 AND user_expenses.expense_type = 1 AND users_master.delete_status = 0 $BranchFilterQuery $blockAppendQueryUser","ORDER BY user_expenses.date DESC LIMIT 1000");
                            }
                            $counter = 1;
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <?php if($_GET['dId'] != '' && $_GET['ueYear'] > 0  && $_GET['ueMonth'] > 0 && ($_GET['uId'] != '' && $_GET['uId'] > 0)) {
                                        ?>
                                        <td>#</td>
                                        <?php }else{
                                        if($_GET['uId'] != '' && $_GET['uId'] > 0){
                                        ?>
                                        <td>#</td>
                                        <?php }
                                        } ?>
                                        <th>Sr.No</th>
                                        <th>Action</th>
                                        <th>Employee</th>
                                        <th>Department</th>
                                        <th>Expense Title</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                        <th>Category </th>
                                        <th>Type</th>
                                        <th>Approved By</th>
                                        <th>Unit</th>
                                        <th>Unit Name</th>
                                        <th>Unit Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($data = mysqli_fetch_array($q)) {
                                    ?>
                                    <tr>
                                        <?php if($_GET['dId'] != '' && $_GET['ueYear'] > 0  && $_GET['ueMonth'] > 0 && ($_GET['uId'] != '' && $_GET['uId'] > 0)) {
                                        ?>
                                        <td class="text-center">
                                            <input type="hidden" name="id" id="id"  value="<?php echo $data['leave_type_id']; ?>">
                                            <input type="checkbox" name="" class="multiPaidCheckbox" value="<?php echo $data['user_expense_id']; ?>" amount="<?php echo $data['amount']; ?>" user_id="<?php echo $data['user_id']; ?>">
                                        </td>
                                        <?php } else{  if($_GET['uId'] != '' && $_GET['uId'] > 0){
                                        ?>
                                        <td class="text-center">
                                            <input type="hidden" name="id" id="id"  value="<?php echo $data['leave_type_id']; ?>">
                                            <input type="checkbox" name="" class="multiPaidCheckbox" value="<?php echo $data['user_expense_id']; ?>" amount="<?php echo $data['amount']; ?>" user_id="<?php echo $data['user_id']; ?>">
                                        </td>
                                        <?php } }?>
                                        <td><?php echo $counter++; ?></td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-primary mr-1 pd-1" onclick="employeeExpensesDetail(<?php echo $data['user_expense_id']; ?>)" >
                                            <i class="fa fa-eye"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-secondary mr-1 pd-1" onclick="singleExpensePaid(`<?php echo $data['user_id']; ?>`,`<?php echo $data['user_expense_id']; ?>`,`<?php echo $data['amount']; ?>`)">Paid</button>
                                        </td>
                                        <td><?php echo $data['user_full_name']; ?><br>
                                            <span>(<?php echo $data['user_designation']; ?>)</span>
                                        </td>
                                        <td><?php echo $data['floor_name']; ?> </td>
                                        <td><?php echo $data['expense_title']; ?></td>
                                       
                                        <td><?php echo $data['amount']; ?></td>
                                         <td><?php echo date("d M Y", strtotime($data['date'])); ?></td>
                                        <td><?php echo $data['expense_category_name']; ?></td>
                                        <td><?php echo ($data['expense_category_type'] == 1) ? 'Unit Wise' : 'Amount Wise'; ?></td>
                                         <td><?php if($data['expense_approved_by_type']==0){ echo $data['approved_user_name']; } else { echo $data['approved_admin_name']; } ?></td>
                                        <td><?php echo ($data['unit'] != "0.00") ? $data['unit'] : ""; ?></td>
                                        <td><?php echo $data['expense_category_unit_name']; ?></td>
                                        <td><?php echo ($data['expense_category_unit_price'] != "0.00") ? $data['expense_category_unit_price'] : ""; ?></td>
                                       
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<div class="modal fade" id="employeeExpensesModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Employee Expenses</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="employeeExpensesData" style="align-content: center;">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expensePayModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Expense Pay Detail</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="billPayDiv" style="align-content: center;">
                <div class="card-body">
                    <form id="payExpenseAmountForm" action="controller/ExpenseSettingController.php" enctype="multipart/form-data" method="post">
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Amount </label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" class="form-control" placeholder="amount" id="amount" name="amount" readonly value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Expense Payment Mode <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8" id="">
                                <select class="form-control single-select" id="expense_payment_mode" name="expense_payment_mode">
                                    <option value="">--SELECT--</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Bank Transfer">Bank Transfer</option>
                                    <option value="Cheque">Cheque</option>
                                    <option value="Paytm">UPI</option>
                                </select>
                                <!-- <input type="text" class="form-control" placeholder="Expense Payment Mode" id="expense_payment_mode" name="expense_payment_mode" value=""> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Reference Number </label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" class="form-control" placeholder="Reference Number" id="reference_no" name="reference_no" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Remark </label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" class="form-control" placeholder="Remark" id="remark" name="remark" value="">
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" id="user_expense_id" name="user_expense_id" value="" >
                            <input type="hidden" id="redirect_b_id" name="redirect_b_id" value="<?php echo $bId; ?>" >
                            <input type="hidden" id="redirect_f_id" name="redirect_f_id" value="<?php echo $dId; ?>" >
                            <input type="hidden" id="redirect_month" name="redirect_month" value="<?php echo $ueMonth; ?>" >
                            <input type="hidden" id="redirect_year" name="redirect_year" value="<?php echo $ueYear; ?>" >
                            <input type="hidden" id="user_id" name="user_id" value="" >
                            <input type="hidden" name="payExpenseAmount"  value="payExpenseAmount">
                            <button id="payExpenseAmountBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                            <button type="button"  value="add" class="btn btn-danger cancel" onclick="resetForm();"><i class="fa fa-check-square-o"></i> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function resetForm()
{
$('#expense_payment_mode').val('');
$('#reference_no').val('');
$('#remark').val('');
}
function expensePay(id){
$('#user_expense_id').val(id);
$('#expensePayModal').modal();
}
function popitup(url) {
newwindow=window.open(url,'name','height=800,width=900, location=0');
if (window.focus) {newwindow.focus()}
return false;
}
</script>