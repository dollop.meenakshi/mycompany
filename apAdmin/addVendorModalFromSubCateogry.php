<?php
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$scId = (int)$_POST['scId'];
if (isset($scId)) {
    $getQ = $d->selectRow('product_sub_category_master.*', 'product_sub_category_master', "product_sub_category_id=$scId");
    $get_sub_cat_data = mysqli_fetch_assoc($getQ);
}
$society_id = $_COOKIE['society_id'];
?>

<div class="form-group row">
    <label for="input-10" class="col-sm-4 col-form-label">Vendor <span class="required">*</span></label>
    <div class="col-lg-8 col-md-8" id="">
        <select type="text" class="form-control single-select frmInputSl" multiple id="vendor_id" name="vendor_id[]">
            <option value="">-- Select --</option>
            <?php
            $getVendorCat = $d->selectRow('*', 'product_sub_category_vendor_master', "product_sub_category_id=$scId");
            $filter = "";
            $vIds = array();
            if (mysqli_num_rows($getVendorCat) > 0) {
                while ($data = mysqli_fetch_assoc($getVendorCat)) {
                    if ($data) {
                        $vId  = $data['vendor_id'];
                        array_push($vIds, $vId);
                    }
                }
                if (!empty($vIds)) {
                    $vendorIds = join("','", $vIds);
                    $filter = " AND service_provider_users_id NOT IN ('$vendorIds');";
                }
            }
            $getvByCat = $d->selectRow('*', 'product_category_vendor_master', "product_category_id='$get_sub_cat_data[product_category_id]'");
            $filter = "";
            $CvIds = array();
            if (mysqli_num_rows($getvByCat) > 0) {
                while ($getvByCatdata = mysqli_fetch_assoc($getvByCat)) {
                   // print_r($getvByCatdata);
                    if ($getvByCatdata) {
                        $vId2  = $getvByCatdata['vendor_id'];
                        if (!in_array($vId2, $vIds)) {
                            array_push($CvIds, $vId2);
                        }
                    }
                }
                if (!empty($CvIds)) {
                    $CvendorIds = join("','", $CvIds);
                   // print_r($CvendorIds);
                    $fltr = " AND service_provider_users_id  IN ('$CvendorIds');";
                }
            }
                $floor = $d->select("local_service_provider_users", " service_provider_status=0 AND service_provider_delete_status=0 $fltr $filter ");
                while ($floorData = mysqli_fetch_array($floor)) {
                ?>
                <option <?php if (isset($data['vendor_id']) && $data['vendor_id'] == $floorData['service_provider_users_id']) {
                            echo "selected";} ?> value="<?php if (isset($floorData['service_provider_users_id']) && $floorData['service_provider_users_id'] != "") {echo $floorData['service_provider_users_id'];} ?>"><?php if (isset($floorData['service_provider_name']) && $floorData['service_provider_name'] != "") { echo $floorData['service_provider_name'];} ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-footer text-center">
    <input type="hidden" id="product_sub_category_id" name="product_sub_category_id" value="<?php echo $scId; ?>" class="">
    <button name="addproductCategoryBtn" type="submit" class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
    <input type="hidden" name="addVendorSubCategory" value="addVendorSubCategory">
    <button id="addproductCategoryBtn" type="submit" class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
    <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addproductCategoryForm');"><i class="fa fa-check-square-o"></i> Reset</button>
</div>

<script type="text/javascript" src="assets/js/select.js"></script>