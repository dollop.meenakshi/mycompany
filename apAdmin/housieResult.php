<?php 
extract($_REQUEST);

$q = $d->select("housie_room_master","room_id = '$room_id'");
$data = mysqli_fetch_array($q);
extract($data);
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Housie Game Result</h4>
        
      </div>

    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
       <div class="card profile-card-2">
       
        <div class="card-body">
          <h5 class="card-title">Game Name: <?php echo $game_name; ?></h5>
          <h5 class="card-title">Participate Users: <?php echo $d->count_data_direct("join_room_id","housie_join_room_master","room_id='$room_id'"); ?></h5>
          <h5 class="card-title">Winner Users: <?php  $qwin=$d->select("housie_winner_master,users_master","housie_winner_master.user_id=users_master.user_id AND housie_winner_master.room_id='$room_id'","GROUP BY housie_winner_master.user_id"); 
            echo mysqli_num_rows($qwin);
          ?></h5>
          <p class="card-text"><b>Game Date:</b> <?php echo date('d M Y h:i A',strtotime($game_date.' '.$game_time)); ?></p>
       

          <div class="table-responsive">
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Rules</th>
                  <th>Winning Point</th>

                </tr>
              </thead>
              <tbody>
                <?php
                $q=$d->select("housie_winner_master,users_master,housie_rules_master,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND housie_rules_master.master_rule_id=housie_winner_master.master_rule_id AND  housie_winner_master.user_id=users_master.user_id AND housie_winner_master.room_id='$room_id'","ORDER BY housie_winner_master.winner_id ASC");
                $i = 0;
                $cnt = 1;
                while($row=mysqli_fetch_array($q))
                { 
                  $cnt++;
                          // extract($row);
                  $i++;

                  ?>
                  <tr>

                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['user_full_name']; ?>-<?php echo $row['user_designation']; ?> (<?php echo $row['block_name']; ?>)</td>
                    <td><?php echo $row['rule_name']; ?></td>
                    <td><?php echo $row['winning_point']; ?></td>

                  </tr>
                  <?php } ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
    </div>
</div>

  
   <div class="row">
      <div class="col-lg-12">
       <div class="card profile-card-2">
       
        <div class="card-body">
          <h5 class="card-title">Participate Users</h5>

          <div class="table-responsive">
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th><?php echo $xml->string->unit; ?></th>
                  <th>Join Time</th>

                </tr>
              </thead>
              <tbody>
                <?php
                $q=$d->select("users_master,housie_join_room_master,unit_master,block_master","block_master.block_id=unit_master.block_id   AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id AND housie_join_room_master.user_id=users_master.user_id AND housie_join_room_master.room_id='$room_id'","");
                $i = 0;
                $cnt = 1;
                while($row=mysqli_fetch_array($q))
                { 
                  $cnt++;
                          // extract($row);
                  $i++;

                  ?>
                  <tr>

                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['user_full_name']; ?> </td>
                    <td><?php echo $row['user_designation']; ?>-<?php echo $row['block_name']; ?></td>
                    <td><?php echo $row['join_time']; ?></td>

                  </tr>
                  <?php } ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
    </div>
</div>



</div>

</div>
<!-- End container-fluid-->
</div><!--End content-wrapper-->

