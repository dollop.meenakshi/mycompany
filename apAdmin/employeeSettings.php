<?php error_reporting(0);

$user_id = (int)$_GET['user_id'];
$dId = (int)$_GET['dId'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title"> Employee Setting</h4>
            </div>
        </div>
        <form action="" method="get">
            <div class="row pt-2 pb-2">
                <div class="col-md-3 form-group">
                <label  class="form-control-label">Branch </label>
                    <select name="bId" class="form-control single-select" required="" onchange="getVisitorFloorByBlockId(this.value)">
                        <option value="">-- Select Branch --</option> 
                        <?php 
                            $qb=$d->select("block_master","society_id='$society_id'");  
                            while ($blockData=mysqli_fetch_array($qb)) {
                        ?>
                        <option  <?php if($_GET['bId']==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                <label  class="form-control-label">Department </label>
                    <select name="dId" id="floor_id" class="form-control single-select" required="" onchange="getVisitorUserByFloor(this.value)">
                        <option value="">-- Select Department --</option> 
                        <?php 
                            $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id = '$_GET[bId]'");  
                            while ($depaData=mysqli_fetch_array($qd)) {
                        ?>
                        <option  <?php if($_GET['dId']==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                <label  class="form-control-label">Employee </label>
                    <select  type="text" class="form-control single-select" name="user_id" id="user_id">
                        <option value="">-- Select Employee --</option>
                        <?php
                        $userData=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND floor_id = '$_GET[dId]'");
                        while ($user=mysqli_fetch_array($userData)) {
                        ?>
                        <option <?php if(isset($_GET['user_id']) && $_GET['user_id'] == $user['user_id']){ echo "selected"; } ?> value="<?php if(isset($user['user_id']) && $user['user_id'] !=""){ echo $user['user_id']; } ?>"> <?php if(isset($user['user_full_name']) && $user['user_full_name'] !=""){ echo $user['user_full_name']; } ?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="col-md-3 form-group mt-auto">
                <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
    

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                <div class="card-body">
                    <div class="table-responsive">
                        <?php if((isset($dId) && $dId >0) || (isset($user_id) && $user_id >0) )
                        {
                        ?>
                        <table id="example" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>User Name</th>
                                    <th>Department</th>
                                    <th>View Absent Present</th>
                                  
                                </tr>
                            </thead>
                            <tbody id="showFilterData">
                                <?php
                                    if(isset($user_id) && $user_id > 0) {
                                        $userFilterQuery = " AND users_master.user_id=$user_id";
                                    }
                                    if(isset($dId) && $dId > 0) {
                                        $dIdFilterQuery = " AND floors_master.floor_id=$dId";
                                    }
                                $q = $d->select("users_master,block_master,floors_master", "users_master.floor_id = floors_master.floor_id AND  block_master.block_id=floors_master.block_id AND users_master.society_id='$society_id' AND users_master.delete_status= 0   $dIdFilterQuery $userFilterQuery");
                                $counter = 1;
                                while ($data = mysqli_fetch_array($q)) {
                                ?>
                                    <tr>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['user_full_name']; ?></td>
                                        <td><?php echo $data['floor_name']; ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <?php if ($data['is_view_absent_present'] == "0") {
                                                    $status = "";
                                                    $activeDeactive4="userViewAbsentPresentActive";
                                                ?>
                                                <?php } else { 
                                                    $status = "checked";
                                                    $activeDeactive4="userViewAbsentPresentDeactive";

                                                } ?>
                                                <input type="checkbox" <?php echo $status; ?> class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['user_id']; ?>','<?php echo $activeDeactive4;  ?>');" data-size="small" />
                                            </div>
                                        </td>
                                       

                                    </tr>
                                <?php } ?>
                            </tbody>
                           
                        </table>
                        <?php }else
                        { ?>
                        <div class="" role="alert">
                            <span><strong>Note :</strong> Please Select Department</span>
                        </div>
                       <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Row-->

</div>
<!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->

<div class="modal fade" id="exEmployeeDetailModel">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Ex-Employee Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="exEmployeeDetailModelDiv" style="align-content: center;">

            </div>

        </div>
    </div>
</div>