<?php
extract(array_map("test_input", $_POST));
$user_id = $_POST['user_id'];
$floor_id = $_POST['floor_id'];
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$previousYear = $leave_year-1;
extract($data);

?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Bulk Leave Assign</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addLeaveAssignBulk" action="controller/LeaveAssignController.php" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Employee <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select  type="text" required="" class="form-control single-select" id="user_id" name="user_id">
                                    <?php
                                    $userData=$d->select("users_master","society_id='$society_id' AND user_id='$user_id'");
                                    while ($user=mysqli_fetch_array($userData)) {
                                    ?>
                                    <option <?php if(isset($user_id) && $user_id == $user['user_id']){ echo "selected"; } ?> value="<?php if(isset($user['user_id']) && $user['user_id'] !=""){ echo $user['user_id']; } ?>"> <?php if(isset($user['user_full_name']) && $user['user_full_name'] !=""){ echo $user['user_full_name']; } ?></option>
                                    <?php }?>
                                </select>
                            </div>                   
                        </div> 
                    </div>  
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Year <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select  type="text" id="assign_leave_year" required="" class="form-control" name="assign_leave_year" >
                                    <option value="<?php echo $leave_year ?>"><?php echo $leave_year ?></option>
                                   <!--  <option value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option> -->
                                </select>
                                <input type="hidden" class="form-control" name="carry_forward_from_year" value="<?php echo $previousYear; ?>">
                            </div>   
                        </div>  
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Leave Group <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                            <select  type="text" required="" class="form-control single-select" id="leave_group_id" name="leave_group_id" onchange="getAllGroupLeaveTypeOption(this.value, <?php echo $user_id; ?>, <?php echo $floor_id; ?>, <?php echo $currentYear; ?>, <?php echo $nextYear; ?>, <?php echo $previousYear; ?>, <?php echo $leave_year; ?> )">
                                <option value="">-- Select --</option> 
                                <?php 
                                    $leaveGroup=$d->select("leave_group_master","society_id='$society_id' AND leave_group_active_status = 0");  
                                    while ($leaveGroupData=mysqli_fetch_array($leaveGroup)) {
                                ?>
                                <option value="<?php if(isset($leaveGroupData['leave_group_id']) && $leaveGroupData['leave_group_id'] !=""){ echo $leaveGroupData['leave_group_id']; } ?>"><?php if(isset($leaveGroupData['leave_group_name']) && $leaveGroupData['leave_group_name'] !=""){ echo $leaveGroupData['leave_group_name']; } ?></option> 
                                <?php } ?>
                            </select>  
                            </div>                   
                        </div> 
                    </div> 
                </div>
                
                <div id="show-all-group-leave">

                </div>

                               
                <div class="form-footer text-center">
                    <button id="addLeaveAssignBulkBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addLeaveAssignBulk"  value="addLeaveAssignBulk">
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addLeaveAssignBulk');"><i class="fa fa-check-square-o"></i> Reset</button>
                </div>

              </form>
           
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">
       function setUseInMonthLeaves(id){
            value = $('#user_total_leave_'+id).val();
            use_in_month_content = ``;
            month_use_value = value>=31?31:value;
            var x = 1;
            for (var i = (x-0.5); i <=month_use_value; i += 0.5) {
                use_in_month_content += `<option value="`+i+`">`+i+`</option>`;
            }
            $('#applicable_leaves_in_month_'+id).html(use_in_month_content);
        }

    </script>

<style>
    input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	  -webkit-appearance: none;
	  margin: 0;
	}
</style>