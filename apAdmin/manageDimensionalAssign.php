<?php error_reporting(0);
$currentDate = date('Y-m-d');
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Manage Dimensional Performance</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
              <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <a href="assignDimensional" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteAssignDimensionals');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Dimensional Name</th>
                        <th>PMS Type</th>                      
                        <th>PMS Date</th>                      
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("performance_dimensional_assign,dimensional_master","performance_dimensional_assign.dimensional_id=dimensional_master.dimensional_id AND performance_dimensional_assign.society_id='$society_id'");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['performance_dimensional_assign_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['performance_dimensional_assign_id']; ?>">
                        </td>
                        <td><?php echo $counter++; ?></td>
                                               <td><?php echo $data['dimensional_name']; ?></td>

                        <td><?php if($data['pms_type'] == 0){
                            echo 'Weekly';
                          }
                          elseif($data['pms_type'] == 1){
                            echo 'Monthly';
                          }
                          elseif($data['pms_type'] == 2){
                            echo 'Quarterly';
                          }
                          elseif($data['pms_type'] == 3){
                            echo 'Half Yearly';
                          }
                          elseif($data['pms_type'] == 4){
                            echo 'Yearly';
                          }
                        ?>
                      </td>
                       <td>
                           <?php if($data['pms_type'] == 0){
                            $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
                            $day = $data['day'];
                            $to_date = date('Y-m-d');
                            echo $weekly_date = date('d F Y', strtotime($days[$day], strtotime($to_date)));
                          }
                          elseif($data['pms_type'] == 1){
                            $month = date('F Y');
                            $month_days = explode(',',$data['day']);
                            $month_day = $data['day'].' '.$month; 
                            if(date('Y-m-d', strtotime($month_day)) < $currentDate){
                                $month_day = date('d F Y', strtotime($month_day. ' + 1 months'));
                            }
                            echo $month_day;
                          }
                          elseif($data['pms_type'] == 2){
                            $date = date('Y').'-'.$data['start_month'].'-'.$data['day'];
                            if($currentDate <= date('Y-m-d', strtotime($date))){
                                $monthNum  = $data['start_month'];
                                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                $monthName = $dateObj->format('F').' '.date('Y');
                                $quarterly_date = $data['day'].' '.$monthName;
                            }else{
                                  $newDate = date('m', strtotime($date. ' + 3 months'));
                                  $monthNum  = $newDate;
                                  $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                  $monthName = $dateObj->format('F').' '.date('Y');
                                  $quarterly_date = $data['day'].' '.$monthName;
                            }

                            if(date('Y-m-d', strtotime($quarterly_date)) < $currentDate){
                                $quarterly_date = date('d F Y', strtotime($quarterly_date. ' + 3 months'));
                                while(date('Y-m-d', strtotime($quarterly_date)) < $currentDate){
                                  $quarterly_date = date('d F Y', strtotime($quarterly_date. ' + 3 months'));
                                }
                            }
                            echo $quarterly_date;
                          }
                          elseif($data['pms_type'] == 3){
                            $date = date('Y').'-'.$data['start_month'].'-'.$data['day'];
                            if($currentDate <= date('Y-m-d', strtotime($date))){
                                $monthNum  = $data['start_month'];
                                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                $monthName = $dateObj->format('F').' '.date('Y');
                                $half_year_date = $data['day'].' '.$monthName;
                            }else{
                                  $newDate = date('m', strtotime($date. ' + 6 months'));
                                  $monthNum  = $newDate;
                                  $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                  $monthName = $dateObj->format('F').' '.date('Y');
                                  $half_year_date = $data['day'].' '.$monthName;
                            }

                            if(date('Y-m-d', strtotime($half_year_date)) < $currentDate){
                                $half_year_date = date('d F Y', strtotime($half_year_date. ' + 6 months'));
                                while(date('Y-m-d', strtotime($half_year_date)) < $currentDate){
                                  $half_year_date = date('d F Y', strtotime($half_year_date. ' + 6 months'));
                                }
                            }
                            echo $half_year_date;
                          }
                          elseif($data['pms_type'] == 4){
                            $date = date('Y').'-'.$data['start_month'].'-'.$data['day'];
                            $monthNum  = $data['start_month'];
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('F').' '.date('Y');
                            $year_date = $data['day'].' '.$monthName;

                            if(date('Y-m-d', strtotime($year_date)) < $currentDate){
                                $year_date = date('d F Y', strtotime($year_date. ' + 1 year'));
                            }
                            echo $year_date;
                          }
                        ?> 
                       </td>
                       <td>
                       <div class="d-flex align-items-center">
                          <form action="assignDimensional" method="post">
                            <input type="hidden" name="editDimensionalPerformance" value="editDimensionalPerformance">
                            <input type="hidden" name="performance_dimensional_assign_id" value="<?php echo $data['performance_dimensional_assign_id']; ?>">
                             <button type="submit" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-pencil"></i></button>
                          </form>
                       </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
