<?php
    error_reporting(0);
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title">Sister Companies</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
                <div class="btn-group float-sm-right">
                    <a href="addSisterCompanies" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteSisterCompanies');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>Company Code</th>
                                        <th>Company Name</th>
                                        <th>Sister Company Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Employees</th>
                                        <th>Logo</th>
                                        <th>Stamp</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                $q = $d->selectRow("scm.*,sm.society_name,(SELECT COUNT(*) FROM users_master WHERE users_master.sister_company_id = scm.sister_company_id AND user_status!=0 AND delete_status=0) AS total_employees ","sister_company_master AS scm JOIN society_master AS sm ON sm.society_id = scm.society_id","scm.society_id = '$society_id'","ORDER BY scm.sister_company_id DESC");
                                    $counter = 1;
                                    while ($data = $q->fetch_assoc())
                                    {
                                        extract($data);
                                ?>
                                    <tr>
                                        <td class="text-center">
                                    <?php
                                        if($total_employees == 0)
                                        {
                                    ?>
                                            <input type="hidden" name="id" id="id" value="<?php echo $sister_company_id; ?>">
                                            <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $sister_company_id; ?>">
                                    <?php
                                        }
                                    ?>
                                        </td>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo "C".$sister_company_id; ?></td>
                                        <td><?php echo $society_name; ?></td>
                                        <td><?php echo $sister_company_name; ?></td>
                                        <td><?php echo $sister_company_phone; ?></td>
                                        <td><?php echo $sister_company_email; ?></td>
                                        <td><?php echo $total_employees; ?></td>
                                        <td>
                                        <?php
                                            if($sister_company_logo != "" && file_exists("../img/society/".$sister_company_logo))
                                            {
                                        ?>
                                            <a href="../img/society/<?php echo $sister_company_logo;?>" data-fancybox="images" data-caption="Photo Name : <?php echo $sister_company_name; ?> Logo"><img class="img-fluid" src="../img/society/<?php echo $sister_company_logo; ?>"/></a>
                                        <?php
                                            }
                                        ?>
                                        </td>
                                        <td>
                                        <?php
                                            if($sister_company_stamp != "" && file_exists("../img/society/".$sister_company_stamp))
                                            {
                                        ?>
                                            <a href="../img/society/<?php echo $sister_company_stamp;?>" data-fancybox="images" data-caption="Photo Name : <?php echo $sister_company_name; ?> Stamp Photo"><img class="img-fluid" src="../img/society/<?php echo $sister_company_stamp; ?>"/></a>
                                        <?php
                                            }
                                        ?>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <button onclick="getSisterComInfo(<?php echo $sister_company_id; ?>);" class="btn btn-sm btn-info mr-2" title="Other Details"> <i class="fa fa-list"></i></button>
                                                <form action="addSisterCompanies" method="post">
                                                    <input type="hidden" name="sister_company_id" value="<?php echo $sister_company_id; ?>"/>
                                                    <input type="hidden" name="editSisterCompany" value="<?php echo $editSisterCompany; ?>"/>
                                                    <button type="submit" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-pencil"></i></button>
                                                </form>
                                            <?php
                                                if($status == 1)
                                                {
                                            ?>
                                                <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $sister_company_id; ?>','sisterCompanyDeactive');" data-size="small"/>
                                            <?php
                                                }
                                                else
                                                {
                                            ?>
                                                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $sister_company_id; ?>','sisterCompanyActive');" data-size="small"/>
                                            <?php
                                                }
                                            ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div>

<div class="modal fade" id="sisterCompanyDetailsModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white"><label class="text-white" id="sister_company_name"></label> - Sister Company Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="form-group row sisComDetails">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $d->map_key();?>"></script>
<script>
    function getSisterComInfo(sister_company_id)
    {
        $.ajax({
            url: 'controller/sisterCompanyController.php',
            cache: false,
            data: {getSisterComInfo:"getSisterComInfo",sister_company_id:sister_company_id,csrf:csrf},
            type: "post",
            success: function(response)
            {
                response = JSON.parse(response);
                $('#sister_company_name').text(response.sister_company_name);
                if(response.sister_company_pincode != "" && response.sister_company_pincode != null)
                {
                    $('.sisComDetails').append('<div class="col-2 p-2"><b>Pin Code : </b></div><div class="col-4 p-2">'+response.sister_company_pincode+'</div>');
                }
                if(response.sister_company_website != "" && response.sister_company_website != null)
                {
                    $('.sisComDetails').append('<div class="col-2 p-2"><b>Website : </b></div><div class="col-4 p-2"><a target="_blank" href="'+response.sister_company_website+'">'+response.sister_company_website+'</div>');
                }
                if(response.sister_company_gst_no != "" && response.sister_company_gst_no != null)
                {
                    $('.sisComDetails').append('<div class="col-2 p-2"><b>GST : </b></div><div class="col-4 p-2">'+response.sister_company_gst_no+'</div>');
                }
                if(response.sister_company_pan != "" && response.sister_company_pan != null)
                {
                    $('.sisComDetails').append('<div class="col-2 p-2"><b>PAN : </b></div><div class="col-4 p-2">'+response.sister_company_pan+'</div>');
                }
                var add = ""
                if(response.sister_company_address != "" && response.sister_company_address != null)
                {
                    add = response.sister_company_address;
                    $('.sisComDetails').append('<div class="col-2 p-2"><b>Address : </b></div><div class="col-10 p-2">'+response.sister_company_address+'</div>');
                }
                if(response.sister_company_latitude != "" && response.sister_company_latitude != null && response.sister_company_longitude != "" && response.sister_company_longitude != null)
                {
                    $('.sisComDetails').append('<div class="col-12 p-2"><div id="map" style="width: 100%; height: 400px;"></div></div>');
                    var latinfo = new google.maps.LatLng(response.sister_company_latitude,response.sister_company_longitude);
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: latinfo,
                        zoom: 15
                    });
                    var marker = new google.maps.Marker({
                        map: map,
                        position: latinfo,
                        draggable: false,
                        // animation: google.maps.Animation.BOUNCE,
                        anchorPoint: new google.maps.Point(0, -29)
                    });
                    var infowindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(marker, 'click', function()
                    {
                        var iwContent = '<div id="pop_window">' + '<div><b>Location</b> : '+add+'</div></div>';
                        infowindow.setContent(iwContent);
                        infowindow.open(map, marker);
                    });
                }
                $('#sisterCompanyDetailsModal').modal('show');
            }
        });
    }
</script>