 
  <style>

  .showYoutubeinput {
    display: none;
  }

  .inoutForText {
    display: none;
  }

    </style>
  
  <?php
  error_reporting(0);
  $lsId = (int) $_REQUEST['lsId'];
  $cpId = (int) $_REQUEST['cpId'];
  if ($lsId > 0) {
    $Cls = $d->selectRow('course_lessons_master.*', "course_lessons_master", "lessons_id=$lsId");
    $data = mysqli_fetch_assoc($Cls);
  }
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Course Lessons</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
          </div>
        </div>
      </div>
      <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <form id="addCourseLesson" action="controller/CourseLessonController.php" enctype="multipart/form-data" method="post">
              <div class="form-group row ">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Lesson Title<span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" value="<?php if (isset($data['lesson_title']) && $data['lesson_title'] != "") {echo $data['lesson_title']; } ?>" name="lesson_title" id="lesson_title" class="form-control rstFrm">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Lesson Type <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <select type="text" id="lesson_type_id" required="" class="form-control inputSl single-select " name="lesson_type_id">
                  <option value="">-- Select --</option>
                  <?php
                  $course = $d->select("lesson_type_master", "is_deleted=0 AND is_active=0");
                  while ($courseData = mysqli_fetch_array($course)) {
                  ?>
                    <option <?php if (isset($data['lesson_type_id']) && $data['lesson_type_id'] == $courseData['lesson_type_id']) {
                              echo "selected";
                            } ?> data-name="<?php echo $courseData['lesson_type']; ?>" value="<?php if (isset($courseData['lesson_type_id']) && $courseData['lesson_type_id'] != "") {echo $courseData['lesson_type_id'];} ?>"> <?php if (isset($courseData['lesson_type_id']) && $courseData['lesson_type_id'] != "") { echo $courseData['lesson_type'];} ?>
                    </option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group row lessonInput">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Lesson File <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="file" name="lesson_video" id="lesson_video" class=" rstFrm form-control-file border ">
                <input type="hidden" class="rstFrm" name="lesson_video_old" id="lesson_video_old" value="<?php if (isset($data['lesson_video']) && $data['lesson_video'] != "") { echo $data['lesson_video'];} ?>">
              </div>
            </div>
            <div class="showYoutubeinput">
              <div class="form-group row ">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Link <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                  <input type="text" value="<?php if (isset($data['lesson_video_link']) && $data['lesson_video_link'] != "") { echo $data['lesson_video_link']; } ?>" name="lesson_video_link" id="lesson_video_link" class="form-control rstFrm ">
                </div>
              </div>
            </div>
            <div class="inoutForText">
              <div class="form-group row ">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Text <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                  <textarea required="" id="CoursesummernoteImgage" class="lesson_text" name="lesson_text"><?php echo $data['lesson_text']; ?></textarea>
                </div>
              </div>
            </div>
            <div class="form-group row total_video_durationDiv <?php if (isset($data['lesson_title']) && $data['lesson_type_id'] == "2") { echo "d-none"; } ?>">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Minimum Watch/Read Minutes <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input required autocomplete="off" value="<?php if($data['total_video_duration']>0) { echo $data['total_video_duration']/60; } ?>" max="1000" min="1" type="text" name="total_video_duration" id="total_video_duration" class=" form-control onlyNumber">
              </div>
            </div>

            <input type="hidden" id="course_chapter_id" required="" value="<?php echo $cpId ?>" name="course_chapter_id" class="cpId">
            <div class="form-footer text-center">
              <input type="hidden" id="lessons_id" name="lessons_id" value="<?php echo $lsId;  ?>" class="rstFrm">
              <!-- <button id="addCourseLessonBtn" type="submit" class="btn btn-success hideupdate course_lesson_button"><i class="fa fa-check-square-o"></i><?php if (isset($lsId) && $lsId > 0) { echo "Add"; } else { echo "Update";} ?> </button> -->
              <button type="submit" class="btn btn-success hideAdd course_lesson_button"><i class="fa fa-check-square-o"></i><?php if (isset($lsId) && $lsId > 0) {echo "Update"; } else { echo "Add";} ?> </button>
              <input type="hidden" name="addCourseLesson" value="addCourseLesson">
              <button type="button" value="add" class="btn btn-danger " onclick="resetLessonForm()"><i class="fa fa-check-square-o"></i> Reset</button>
            </div>
              </form>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
  </div>
  <link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css" />
  <script src="assets/js/jquery.min.js"></script>


  <script type="text/javascript">

  function resetLessonForm() {
    $('.rstFrm').val('');
    $('#lesson_video').val('');
    $('.inputSl').val('').trigger('change');
  }

    <?php if (isset($data['lesson_type_id'])) { ?>
      checkLessonType();
    <?php } ?>

    function resetForm() {
      $('.rstFrm').val('');
      $('.inputSl').val('');
      $('.inputSl').select2('');
    }

    function buttonSettingForLmsLesson() {
      $('.hideupdate').hide();
      $('.hideAdd').show();
      $('.rstFrm').val('');
      $('.inputSl').val('');
      $('.inputSl').select2('');
    }

    $(document).ready(function() {
    $("#lesson_type_id").change(function() {
      $("#lesson_video").val("");
      type = $(this).find(':selected').attr('data-name');
      console.log('type'+type)
      if(type !=undefined){
         if (type == "PDF") {
           $('.total_video_durationDiv').hide();
          } else {
           $('.total_video_durationDiv').show();
          }
      if (type == "PDF" || type == "Document" || type == "Audio" || type == "Video") {
        $('.showYoutubeinput').hide();
        $('.inoutForText').hide();
        $('.lessonInput').show();
        if (type == "PDF") {
          $('#lesson_video').addClass('pdfOnly');
          $('#lesson_video').attr("accept", "image/*,.pdf");
          $('#lesson_video').rules("add", {
            extension: "pdf",
            messages: {
                required: "Please Select PDF Only",
            }
        });
        } else if (type == "Audio") {
          $('#lesson_video').attr("accept", "audio/*");
          $('#lesson_video').rules("add", {
            extension: "mp3",
            messages: {
                required: "Please Select Audio Only",
            }
        });
        } else if (type == "Video") {
          $('#lesson_video').attr("accept", "video/*");
          $('#lesson_video').rules("add", {
            extension: "mp4|gif",
            messages: {
                required: "Please Select Video Only",
            }
        });
        } else {
          $('#lesson_video').attr("accept", ".txt,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document");
          $('#lesson_video').rules("add", {
            extension: "docx|txt|doc|pdf|xml",
            messages: {
                required: "Please Select Document Only",
            }
        });
        }

      } else if (type == "Text") {
        $('.showYoutubeinput').hide();
        $('.inoutForText').show();
        $('.lessonInput').hide();
      } else {
        $('.showYoutubeinput').show();
        $('.inoutForText').hide();
        $('.lessonInput').hide();
      }
    }
    });
  });


  $("document").ready(function() {

    $("#lesson_video").change(function() {
      lesson_type_id = $('#lesson_type_id').val();
      if (lesson_type_id != "") {

        fileName = this.value;
        fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
        console.log(fileExtension)
        if(lesson_type_id == 2) {
          extensionResume = ["pdf", "PDF"];
        } else if (lesson_type_id == 3) {
          extensionResume = ["txt", "TXT", "doc", "DOC", "docx", "DOCX"];
        } else if (lesson_type_id == 4) {
          extensionResume = ["mp4", "MP4"];
        } else if (lesson_type_id == 6) {
          extensionResume = ["mp3", "MP3"];
        }
        console.log(jQuery.inArray( fileExtension, extensionResume));
        if((jQuery.inArray( fileExtension, extensionResume))== -1 ){
          $("#lesson_video").val("");
         // swal('Invalid Type');
        }
      } else {
        $("#lesson_video").val("");
        swal('Please Select Lesson type');
      }
    });
  });

  function checkLessonType() {
    type = $("#lesson_type_id").val();
    type = parseInt(type);

    if (type == 2 || type == 3 || type == 4 || type == 6) {
      $('.showYoutubeinput').hide();
      $('.inoutForText').hide();
      $('.lessonInput').show();

    } else if (type == 7) {
      console.log(type);
      $('.showYoutubeinput').hide();
      $('.inoutForText').show();
      $('.lessonInput').hide();
    } else {
      $('.showYoutubeinput').show();
      $('.inoutForText').hide();
      $('.lessonInput').hide();
    }
  }

    function popitup(url) {
      newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
      if (window.focus) {
        newwindow.focus()
      }
      return false;
    }
  </script>