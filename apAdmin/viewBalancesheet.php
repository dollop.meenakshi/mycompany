<?php 
error_reporting(0);
extract($_REQUEST);
  if(filter_var($balancesheet_id, FILTER_VALIDATE_INT) != true){
  $_SESSION['msg1']='Invalid User';
  echo ("<script LANGUAGE='JavaScript'>
      window.location.href='balancesheets';
      </script>");
}
  $qb=$d->select("balancesheet_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
  $bbData=mysqli_fetch_array($qb);

      
    $count2=$d->sum_data("receive_amount - transaction_charges","facilitybooking_master","payment_status=0 AND society_id='$society_id' AND balancesheet_id='$bbData[balancesheet_id]'");
      $row2=mysqli_fetch_array($count2);
        $asif2=$row2['SUM(receive_amount - transaction_charges)'];
      $totalFac=number_format($asif2,2,'.','');

      $countev=$d->sum_data("recived_amount - transaction_charges","event_attend_list","society_id='$society_id' AND balancesheet_id='$bbData[balancesheet_id]'");
      $rowev=mysqli_fetch_array($countev);
      $asif=$rowev['SUM(recived_amount - transaction_charges)'];
      $totalEventBooking=number_format($asif,2,'.','');

               
    $count7=$d->sum_data("penalty_amount","penalty_master","paid_status='1' AND society_id='$society_id' AND balancesheet_id='$balancesheet_id'");
      $row=mysqli_fetch_array($count7);
        $asif=$row['SUM(penalty_amount)'];
        $totalPenalaty=number_format($asif,2,'.','');
             

     $icnome=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$bbData[balancesheet_id]' AND cash_on_hand=0");
    $row2=mysqli_fetch_array($icnome);
   
      $asif5=$row2['SUM(income_amount)'];
      $totalIncome=number_format($asif5,2,'.','');

      $icnomeCash=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$bbData[balancesheet_id]' AND cash_on_hand=1");
    $row3=mysqli_fetch_array($icnomeCash);
   
      $asif6=$row3['SUM(income_amount)'];
      $totalCashonHand=number_format($asif6,2,'.','');
     
   
     $totalIncome=  $totalFac+$totalIncome+$totalEventBooking+$totalPenalaty;

    $count4=$d->sum_data("expenses_amount","expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$bbData[balancesheet_id]'");
      $row5=mysqli_fetch_array($count4);
          $asif5=$row5['SUM(expenses_amount)'];
        $totalExp=number_format($asif5,2,'.','');
 ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-5">
        <h4 class="page-title"><?php
        
         echo $bbData['balancesheet_name']; ?> (<?php echo $currency; ?> <?php echo $totalBlance= number_format(($totalIncome+$totalCashonHand)-$totalExp,2);?>)</h4>
       
      </div>
       <div class="col-sm-7 text-right">
        <a class=" btn btn-sm btn-warning waves-effect waves-light "  href="balancesheetReport?id=<?php echo $balancesheet_id;?>"> View Report </a>
          <?php if ($totalBlance>0): ?>
           <a href="#" class=" btn btn-sm btn-danger waves-effect waves-light " data-toggle="modal" data-target="#billPay" href="javascript:void();"><i class="fa fa-plus mr-1"></i> Add Expense </a>
          <?php endif ?>
          <a href="#" class=" btn btn-sm btn-success waves-effect waves-light " data-toggle="modal" data-target="#income" href="javascript:void();"><i class="fa fa-plus mr-1"></i> Add Income </a>
          <?php //if ($totalBlance==0): ?>
          <a href="#" class=" btn btn-sm btn-danger waves-effect waves-light " data-toggle="modal" data-target="#cashonhand" href="javascript:void();"><i class="fa fa-plus mr-1"></i> Add Cash On Hand </a>
          <?php//endif ?>
      </div>
    </div>
    
  </div>
  <br>
  <!-- End Breadcrumb-->
  
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="">
          <ul class="nav nav-tabs nav-tabs-info nav-justified">
              <?php //IS_649
                extract($_REQUEST);
                
                $table_13="";
                $table_14="";
               
                if($tab=="tabe-13"){
                   $table_13="active";
                } else if($tab=="tabe-14"){
                  $table_14="active";
                } else {
                  $table_13="active";
                }
                      //IS_649
              ?>


            <li class="nav-item">
              <a class="nav-link <?php echo $table_13;?>" data-toggle="tab" href="#tabe-13"><i class="fa fa-money"></i> <span class="hidden-xs">Income (
                <?php  echo number_format($totalIncome,2); ?>
              )</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link  <?php echo $table_14;?>" data-toggle="tab" href="#tabe-14"><i class="fa fa-money"></i> <span class="hidden-xs">Expense (
                <?php echo number_format($totalExp,2); ?>
              )</span></a>
            </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div id="tabe-13" class="container-fluid tab-pane <?php echo $table_13;?> show">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="table-responsive">
                      <table id="balancesheetIncome" class="table table-bordered">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Unit</th>
                            <th>Income From</th>
                            <th>Type</th>
                            <th> Date</th>
                            <th> Amount</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php

                          $i=1;
                          extract($_REQUEST);
                         
                          $q3=$d->select("facilities_master,facilitybooking_master,users_master,block_master","block_master.block_id=users_master.block_id AND users_master.user_id=facilitybooking_master.user_id AND facilitybooking_master.facility_id=facilities_master.facility_id AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.balancesheet_id='$balancesheet_id' AND facilitybooking_master.payment_status=0","ORDER BY facilitybooking_master.booking_id DESC");
                          
                          
                           $q4=$d->select("expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id' AND income_amount!=0 ","ORDER BY expenses_balance_sheet_id DESC");

                           $q5=$d->select("event_attend_list,unit_master,block_master,users_master","users_master.user_id=event_attend_list.user_id AND unit_master.unit_id=event_attend_list.unit_id AND unit_master.block_id=block_master.block_id AND  event_attend_list.recived_amount!='0.00' AND event_attend_list.society_id='$society_id' AND  event_attend_list.balancesheet_id='$balancesheet_id' ","");
                           $q6=$d->select("penalty_master,unit_master,users_master,block_master","block_master.block_id=unit_master.block_id and unit_master.unit_id=penalty_master.unit_id AND users_master.user_id=penalty_master.user_id AND  penalty_master.penalty_amount!='0.00' AND penalty_master.society_id='$society_id' AND  penalty_master.balancesheet_id='$balancesheet_id' AND penalty_master.paid_status=1","");

                          if (mysqli_num_rows($q5) > 0) {
                          while ($eventData=mysqli_fetch_array($q5)) {
                            $eq=$d->select("event_master","event_id='$eventData[event_id]'");
                            $evenName=mysqli_fetch_array($eq);

                          ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $eventData['user_full_name']; ?>-<?php echo $eventData['user_designation']; ?> (<?php echo $eventData['block_name']; ?> )</td>
                            <td><?php echo $evenName['event_title']; ?></td>
                            <td>Event Booking</td>
                            <td><?php echo $eventData['payment_received_date']; ?></td>
                            <td><?php echo number_format($eventData['recived_amount']-$eventData['transaction_charges'],2); ?></td>
                            <td></td>
                          </tr>
                          <?php  }}
                          if (mysqli_num_rows($q3) > 0) {
                          while ($fData=mysqli_fetch_array($q3)) {
                          ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $fData['user_full_name'].'-'.$fData['user_designation']; ?> (<?php echo $fData['block_name'];  ?> )</td>
                            <td><?php echo $fData['facility_name']; ?></td>
                            <td>Facility</td>
                            <td><?php echo $fData['payment_received_date']; ?></td>
                            <td><?php echo number_format($fData['receive_amount']-$fData['transaction_charges'],2); ?></td>
                            <td></td>

                          </tr>
                          <?php  }}
                          
                           if (mysqli_num_rows($q6) > 0) {
                          while ($penaltyData=mysqli_fetch_array($q6)) {
                          ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $penaltyData['user_full_name'].'-'.$penaltyData['user_designation']; ?> (<?php echo $penaltyData['block_name'];  ?> )</td>
                            <td><?php echo $penaltyData['penalty_name']; ?></td>
                            <td>Penalty</td>
                            <td><?php echo $penaltyData['penalty_receive_date']; ?></td>
                            <td><?php echo number_format($penaltyData['penalty_amount'],2); ?></td>
                            <td></td>

                          </tr>
                          <?php  }}  
                           if (mysqli_num_rows($q4) > 0) {
                          while ($incomeData=mysqli_fetch_array($q4)) {
                          ?>
                          <tr class="<?php if($incomeData['cash_on_hand']==1) { echo 'text-success';} ?>" >
                            <td><?php echo $i++; ?></td>
                            <td>Admin (
                                <?php  
                                $qad=$d->select("bms_admin_master","admin_id='$incomeData[received_by]'");
                                $adminData=mysqli_fetch_array($qad);
                                echo  $adminData['admin_name'];
                                ?>
                            )</td>
                            <td><?php echo $incomeData['expenses_title']; ?></td>
                            <td><?php if($incomeData['cash_on_hand']==1) { echo 'Cash On Hand';} else { echo 'Income'; } ?></td>
                            <td><?php echo date("Y-m-d", strtotime($incomeData['expenses_add_date'])); ?></td>
                            <td><?php echo number_format($incomeData['income_amount'],2); ?></td>
                            <td>
                              <?php 

                               if($incomeData['created_date']!="" && $incomeData['received_by']==$_COOKIE['bms_admin_id'] || $incomeData['created_date']!="" && $adminData['admin_type'] ==1) {

                               $cTime= date("Y-m-d H:i:s");    
                              $d1= new DateTime($incomeData['created_date']); 
                              $d2= new DateTime($cTime);
                              $interval= $d1->diff($d2);
                              $hoursDifrent=  ($interval->days * 24) + $interval->h;
                               $totalBlanceTemp = ($totalIncome+$totalCashonHand)-$totalExp;
                              if($hoursDifrent<=48 &&  $totalBlanceTemp>=$incomeData['income_amount']) {
                              ?>

                              <form  action="controller/balancesheetController.php" method="post" >
                                <input type="hidden" name="income_balance_sheet_id_delete" value="<?php echo $incomeData['expenses_balance_sheet_id']; ?>">
                                <input type="hidden" name="expenses_title_delete" value="<?php echo $incomeData['expenses_title']; ?>">
                                <input type="hidden" name="expenses_amount_delete" value="<?php echo $incomeData['income_amount']; ?>">
                                <input type="hidden" name="balancesheet_id" value="<?php echo $incomeData['balancesheet_id']; ?>">
                                <button type="submit" class="btn form-btn btn-danger btn-sm waves-effect waves-light m-1 "><i class="fa fa-trash-o"></i></button>
                                 <a data-toggle="modal" data-target="#editIncome"  onclick="editIncome('<?php echo $incomeData['expenses_balance_sheet_id'] ?>');" href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>
                              </form>
                            <?php  } else if($hoursDifrent<=48) { ?>
                               <a data-toggle="modal" data-target="#editIncome"  onclick="editIncome('<?php echo $incomeData['expenses_balance_sheet_id'] ?>');" href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>
                            <?php } }  ?>
                               <?php  
                            if ($incomeData['expenses_photo']!="") { ?>
                             <a class="btn btn-primary btn-sm" target="_blank" href="../img/expenses/<?php echo $incomeData['expenses_photo']; ?>"><i class="fa fa-paperclip"></i> </a>
                              <?php } ?>
                            </td>

                          </tr>
                          <?php  }}  ?>
                        </tbody>
                        
                      </table>
                    </div>
            </div>
            <div id="tabe-14" class="container-fluid tab-pane <?php echo $table_14;?> fade show">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="table-responsive">
                      <table id="balancesheetExpence" class="table table-bordered" style="width: 100% !important;">
                        <thead>
                          <tr>
                            <td>#</td>
                            <td>Title</td>
                            <td>Amount</td>
                            <td>Date</td>
                            <td>Category</td>
                            <td>Action</td>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $i=1;
                          extract(array_map("test_input" , $_POST));
                          $qe=$d->select("expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$balancesheet_id' AND expenses_amount!=0","ORDER BY   expenses_balance_sheet_id DESC");
                          while ($sData=mysqli_fetch_array($qe)) {
                          ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $sData['expenses_title']; ?></td>
                            <td><?php echo number_format($sData['expenses_amount'],2); ?></td>
                            <td><?php echo date("Y-m-d", strtotime($sData['expenses_add_date'])); ?></td>
                            <td><?php  if($sData['expense_category_id']!=0) {
                               $q11=$d->select("expense_category_master","active_status=0 AND expense_category_id='$sData[expense_category_id]'","");
                               $row=mysqli_fetch_array($q11);
                               echo $row['expense_category_name'];
                            } ?></td>
                            <td> 
                              <?php 
                              

                              if($sData['created_date']!="" && $sData['received_by']==$_COOKIE['bms_admin_id'] || $sData['created_date']!="" && $adminData['admin_type'] ==1) {

                               $cTime= date("Y-m-d H:i:s");    
                              $d1= new DateTime($sData['created_date']); 
                              $d2= new DateTime($cTime);
                              $interval= $d1->diff($d2);
                              $hoursDifrent=  ($interval->days * 24) + $interval->h;
                              if($hoursDifrent<=48) {
                              ?>

                              <form  action="controller/balancesheetController.php" method="post" >
                                <input type="hidden" name="expenses_balance_sheet_id_delete" value="<?php echo $sData['expenses_balance_sheet_id']; ?>">
                                <input type="hidden" name="expenses_title_delete" value="<?php echo $sData['expenses_title']; ?>">
                                <input type="hidden" name="expenses_amount_delete" value="<?php echo $sData['expenses_amount']; ?>">
                                <input type="hidden" name="balancesheet_id" value="<?php echo $sData['balancesheet_id']; ?>">
                                <button type="submit" class="btn form-btn btn-danger btn-sm waves-effect waves-light m-1 "><i class="fa fa-trash-o"></i></button>
                                 <a data-toggle="modal" data-target="#editExpense"  onclick="editExpense('<?php echo $sData['expenses_balance_sheet_id'] ?>');" href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>

                                <a  onclick="return popitup('paymentReceiptAndroid.php?type=exp&societyid=<?php echo $sData['society_id'] ?>&id=<?php echo $sData['expenses_balance_sheet_id'] ?>')" href="#" class="btn btn-warning btn-sm"><i class="fa fa-print"></i> </a>
                              </form>
                            <?php  } } else { ?>
                                <a  onclick="return popitup('paymentReceiptAndroid.php?type=exp&societyid=<?php echo $sData['society_id'] ?>&id=<?php echo $sData['expenses_balance_sheet_id'] ?>')" href="#" class="btn btn-warning btn-sm"><i class="fa fa-print"></i> </a>
                            <?php  } 
                            if ($sData['expenses_photo']!="") { ?>
                             <a class="btn btn-primary btn-sm" target="_blank" href="../img/expenses/<?php echo $sData['expenses_photo']; ?>"><i class="fa fa-paperclip"></i> </a>
                              <?php } ?>
                            </td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
    
  </div>
  
</div>
<!-- End container-fluid-->
</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<!--End Back To Top Button-->
<div class="modal fade" id="billPay">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Expense </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="expense" action="controller/balancesheetController.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="padiMainExpense" value="padiMainExpense">
          <input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id ?>">
          <div class="form-group row">
            <label for="expenses_title" class="col-sm-5 col-form-label">Expense Title <span class="required">*</span></label>
            <div class="col-sm-7">
              <input maxlength="100" required="" type="text" class="form-control " name="expenses_title" id="expenses_title">
            </div>
            
          </div>
          <div class="form-group row">
            <label for="expAmoint" class="col-sm-5 col-form-label">Expense Amount <span class="required">*</span></label>
            <div class="col-sm-7">
              <input required="" type="text" maxlength="7" id="expAmoint" min="1" max="<?php echo $totalBlance; ?>" class="form-control onlyNumber" inputmode="numeric" name="expenses_amount">
            </div>
            
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Expense Date <span class="required">*</span></label>
            <div class="col-sm-7">
              <?php //IS_579  id="autoclose-datepicker1" to  expenses_add_date class expense-income-cls?>
              <input required="" type="text" autocomplete="off" class="form-control expense-income-cls"  id="expenses_add_date" name="expenses_add_date">
            </div>
          </div>

          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Expense Category </label>
            <div class="col-sm-7">
              <select type="text" class="form-control single-select"  id="expense_category_id" name="expense_category_id">
                <option value=""> -- Select --</option>
                <?php 
                 $qcc=$d->select("expense_category_master","active_status=0 AND category_type = 0","");
                 while($row=mysqli_fetch_array($qcc))
                 {?>
                <option value="<?php echo $row['expense_category_id']; ?>"><?php echo $row['expense_category_name']; ?></option>
                <?php  }?>
              </select>
            </div>
          </div>
          
          <div class="form-group row">
            <label for="input-13" class="col-sm-5 col-form-label">Select Image</label>
            
            <div class="col-sm-7">
              <input type="file" class="form-control-file border docOnly" id="input-14" name="expenses_photo">
            </div>
          </div>
          <?php //IS_1686 ?>
         
         
          
          <div class="form-footer text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o" name="addExpenses"></i> Submit </button>
            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  </div><!--End Modal -->


<div class="modal fade" id="editExpense">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Expense </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="editcomplaintCatDetails">
       
      </div>
      
    </div>
  </div>
  </div><!--End Modal -->



<div class="modal fade" id="editIncome">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Income </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="editcomplaintCatDetailsIncome">
       
      </div>
      
    </div>
  </div>
  </div><!--End Modal -->

   <div class="modal fade" id="income">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Income </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="incomeAdd" action="controller/balancesheetController.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="addMainIncome" value="addMainIncome">
          <input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id ?>">
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Income Title <span class="required">*</span></label>
            <div class="col-sm-7">
              <input maxlength="100" required="" type="text" class="form-control " name="expenses_title">
            </div>
            
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Income Amount <span class="required">*</span></label>
            <div class="col-sm-7">
              <input required="" type="text" maxlength="15" id="expAmoint" min="1"  class="form-control" name="income_amount">
            </div>
            
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Income Date <span class="required">*</span></label>
            <div class="col-sm-7">
              <?php //IS_579  id="autoclose-datepicker1" to expenses_add_date class  expense-income-cls?>
              <input required="" autocomplete="off" type="text" class="form-control expense-income-cls" autocomplete="off" id="expenses_add_date" name="expenses_add_date">
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Income Category </label>
            <div class="col-sm-7">
              <select type="text" class="form-control single-select"  id="expense_category_id" name="expense_category_id">
                <option value=""> -- Select --</option>
                <?php 
                 $qcc=$d->select("expense_category_master","active_status=0 AND category_type=1","");
                 while($row=mysqli_fetch_array($qcc))
                 {?>
                <option value="<?php echo $row['expense_category_id']; ?>"><?php echo $row['expense_category_name']; ?></option>
                <?php  }?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="expenses_photo" class="col-sm-5 col-form-label">Select Image</label>
            
            <div class="col-sm-7">
              <input type="file" class="form-control-file border docOnly" id="input-14" name="expenses_photo" id="expenses_photo">
            </div>
          </div>

           <?php //IS_1686 ?>
          <div class="form-group row">
            <label for="is_taxble" class="col-sm-5 col-form-label">Bill Type <span class="text-danger">*</span></label>
                  <div class="col-sm-7">
                    <select required="" class="form-control  " name="is_taxble" id="is_taxble_bill_type" >
                      <?php include 'billTypeOptionList.php'; ?>
                    </select>
                  </div>
          </div>

          <div style="display: none" id="income_gst_detail_div" >

                  <div class="form-group row">
                  <label for="gst" class="col-sm-5 col-form-label">Tax Amount </label>
                   <div class="col-sm-7">
                    <div class=" icheck-inline">
                      <input  checked   type="radio"  id="inline-radio-info-income" value="0" name="gst">
                      <label for="inline-radio-info-income">Included</label>
                    </div>
                    <div class="icheck-inline">
                      <input      type="radio" id="inline-radio-success-income" value="1" name="gst">
                      <label for="inline-radio-success-income">Excluded</label>
                    </div>
                  </div>
                    
                  <label for="taxble_type" class="col-sm-5 col-form-label"><?php echo $xml->string->tax_type; ?> </label>
                  <div class="col-sm-7">
                     <select    class="form-control  " name="taxble_type" id="taxble_type">
                      <option value="">-- Select --</option>
                      <?php include 'taxOptionList.php'; ?> 
                    </select>
                  </div>
                </div>

                 <div class="form-group row">
                  
                  <label for="tax_slab" class="col-sm-5 col-form-label">Tax Value <span class="text-danger">*</span></label>
                  <div class="col-sm-7">
                    <?php 
                          $q=$d->select("gst_master","status='0'","");
                          ?>
                       <select <?php if(mysqli_num_rows($q)){ echo 'required=""'; } ?>  class="form-control  " name="tax_slab">
                      <option value="">-- Select --</option>
                        <?php
                           while ($row=mysqli_fetch_array($q)) {
                         ?>
                          <option title="<?php echo $row['description'];?>" value="<?php echo $row['slab_percentage'];?>"><?php echo $row['slab_percentage'];?>%</option>
                          <?php }?>
                    </select>
                  </div>
                 </div>
                 
                 </div>
          <?php //IS_1686 ?>
          
          <div class="form-footer text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o" name="addIncome"></i> Submit </button>
            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  </div><!--End Modal -->


  <div class="modal fade" id="cashonhand">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Cash On Hand </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="chand" action="controller/balancesheetController.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="cash_on_hand" value="1">
          <input type="hidden" name="addMainIncome" value="addMainIncome">  
          <input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id ?>">
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Remark <span class="required">*</span></label>
            <div class="col-sm-7">
              <input maxlength="100" required="" type="text" class="form-control " name="expenses_title">
            </div>
            
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Cash On Hand Amount <span class="required">*</span></label>
            <div class="col-sm-7">
              <input required="" type="text" maxlength="15" id="expAmoint" min="1"  class="form-control" name="income_amount">
            </div>
            
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Till Date <span class="required">*</span></label>
            <div class="col-sm-7">
              <?php //IS_579  id="autoclose-datepicker1" to expenses_add_date class  expense-income-cls?>
              <input required="" autocomplete="off" type="text" class="form-control expense-income-cls"  id="expenses_add_date" name="expenses_add_date">
            </div>
          </div>
          
          <div class="form-group row">
            <label for="expenses_photo" class="col-sm-5 col-form-label">Select Image</label>
            
            <div class="col-sm-7">
              <input type="file" class="form-control-file border docOnly" id="input-14" name="expenses_photo" id="expenses_photo">
            </div>
          </div>

           <?php //IS_1686 ?>
           <input type="hidden" name="is_taxble" value="0">
         

        
          
          <div class="form-footer text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o" name="addIncome"></i> Submit </button>
            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  </div><!--End Modal -->
  <script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=800, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>