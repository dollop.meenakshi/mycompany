<?php 
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
extract(array_map("test_input",$_POST));
for ($i=1; $i <=$no_of_blocks ; $i++) { 
if($block_type=='1') {
 ?>
  <label for="input-10"  style="padding-bottom: 15px;" class="col-sm-2 col-form-label">Block Name <?php echo $i; ?> </label>
  <div class="col-sm-4" style="padding-bottom: 15px;">
    <input value="<?php echo $i; ?>" type="text" id="block_name<?php echo $i ?>"  maxlength="10" class="form-control" name="block_name[<?php echo $i ?>]">
  </div>
  <script type="text/javascript">
    $("[name^=block_name]").each(function () {
        $('#'+this.id).rules("add", {
            required: true,
            noSpace: true,
        });
    });
  </script>
<?php }  else if ($block_type=='A') { 
	$j= $i-1;
	$alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

	?>
  <label for="input-10"  style="padding-bottom: 15px;" class="col-sm-2 col-form-label">Block Name <?php echo $i; ?> </label>
  <div class="col-sm-4" style="padding-bottom: 15px;">
    <input value="<?php echo $alphabet[$j]; ?>" type="text" maxlength="10" id="block_name<?php echo $i ?>" class="form-control" name="block_name[<?php echo $i ?>]">
  </div>
  <script type="text/javascript">
    $("[name^=block_name]").each(function () {
        $('#'+this.id).rules("add", {
            required: true,
            noSpace: true,
        });
    });
  </script>
<?php } else { ?>
<label for="input-10"  style="padding-bottom: 15px;" class="col-sm-2 col-form-label">Branch Name <?php echo $i; ?> </label>
  <div class="col-sm-4" style="padding-bottom: 15px;">
    <input value="" type="text" id="block_name<?php echo $i ?>" class="form-control"  maxlength="10" name="block_name[<?php echo $i ?>]">
  </div>
  <script type="text/javascript">
    $("[name^=block_name]").each(function () {
        $('#'+this.id).rules("add", {
            required: true,
            noSpace: true,
        });
    });
  </script>

<?php } }  ?>
<input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />