<?php 
$master_url=$d->master_url();
$keydb = $m->api_key();
$pk=$d->select("society_master","society_id='$society_id'");
$pkData=mysqli_fetch_array($pk);  
$app_name = $d->app_name();
extract($pkData);
if ($package_id==0) {
  $planName= "Trial";
  $no_of_month=$pkData['trial_days'].' Days';
  $amount= "";
} else {

  $language_id = $_COOKIE['language_id'];
  $chLang = curl_init();
  curl_setopt($chLang, CURLOPT_URL,$master_url."commonApi/packageController.php");
  curl_setopt($chLang, CURLOPT_POST, 1);
  curl_setopt($chLang, CURLOPT_POSTFIELDS,
            "getPackage=getPackage&language_id=$language_id&society_id=$society_id&package_id=$package_id");

  curl_setopt($chLang, CURLOPT_RETURNTRANSFER, true);

  curl_setopt($chLang, CURLOPT_HTTPHEADER, array(
   'key: '.$keydb
  ));

  $server_output_lng = curl_exec($chLang);

  curl_close ($chLang);
  $server_output_lng=json_decode($server_output_lng,true);

  $planName= $server_output_lng['cPlanName'];
  $no_of_month = $server_output_lng['cPlanMonth'].' Month';
  $amount =$server_output_lng['cPlanAmount'].' INR';
}
 ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Renew Your Plan</h4>
      </div>

    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="signupForm" action="packagePayment.php" method="POST">
              <h4 class="form-header text-uppercase">
                
                Plan Information
              </h4>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->society; ?> Name </label>
                <div class="col-sm-10">
                  <?php echo $_COOKIE['society_name']; ?>
                </div>

              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Plan Name </label>
                <div class="col-sm-10">
                  <?php echo $planName; ?> - <?php echo $no_of_month; ?>  (<?php echo $amount; ?>)
                </div>

              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Expire Date </label>
                <div class="col-sm-10">
                  <?php echo $plan_expire_date; ?>
                </div>

              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Last Renew Date </label>
                <div class="col-sm-10">
                  <?php echo $last_renew_date; ?>
                </div>

              </div>
                  <input type="hidden" name="package_id" value="<?php echo $package_id; ?>">

                <div class="form-footer text-center">
                <?php if ($package_id==0) { ?>
                  <button type="button" onclick="alert('Please contact to Sales team of $app_name for buy any plan, currently you are using Trial plan');" name="" class="btn btn-success"><i class="fa fa-check-square-o"></i> Renew Online</button>

                  <input type="hidden" name="addpackage" value="addpackage">
                <?php } else { ?>
                  <input type="hidden" name="addpackage" value="addpackage">
                  <button type="submit" name="" class="btn btn-success"><i class="fa fa-check-square-o"></i> Renew Online</button>
                <?php } ?>
                </div>
            </form>
            <div class="alert alert-warning alert-dismissible" role="alert">
              <div class="alert-message">
                <span><strong>Warning!</strong> For Change your Plan please Contact to Sales team of <?php echo $app_name;?> </span>
              </div>
            </div>
              

              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Status</th>
                    <th>Logo</th>
                    <th>Payment for</th>
                    <th>Mode</th>
                    <th>Amount</th>
                    <th>Payment Date</th>
                    <th>Txt Id</th>
                    <th>Order Id</th>
                    <th>Invoice No</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php

                  $q=$d->select("transection_master","society_id='$society_id'","ORDER BY transection_master.transection_id DESC");
                  $i = 0;
                  while($row=mysqli_fetch_array($q))
                  {
                  // extract($row);
                    if($row['payment_mode']=="razorpay" && file_exists('../img/Razorpay_logo.png')){
                      $logoUrl = "../img/Razorpay_logo.png";
                    }
                  $i++;
                  
                  ?>
                  <tr>
                    
                    <td><?php echo $i; ?></td>
                    <td><?php  
                          //$token = "TFwQwwmeHuQzxl6dF1E0lpINK69Rr/HRNbqtGlTF2JE=";
                          if ($row['order_status'] == 0) {  ?>
                              <span class="badge badge-success m-1 pointerCursor">SUCCESS </span>
                              
                            <?php } elseif ($row['order_status'] == 1) { ?>
                              <span class="badge badge-warning  m-1 pointerCursor">PENDING</span>
                              <button class="btn btn-link p-0" data-toggle="modal" data-target="#updateTransactionStatus" onclick="checkTransactionStatus('<?php echo $row['merchant_id'];?>','<?php echo $row['merchant_key'];?>','<?php echo $row['order_id'];?>','<?php echo $row['payment_mode'];?>','<?php echo $row['transection_id'];?>','<?php echo $row['no_of_month'];?>','<?php echo $row['package_id'];?>','<?php echo $logoUrl; ?>','<?php echo $row['authorization_token'];?>','<?php echo $row['payment_txnid'];?>')"><i class="fa fa-history"></i></button>
                            <?php  } elseif ($row['order_status'] == 2) { ?>
                              <span class="badge badge-danger  m-1 pointerCursor">FAILED </span>
                              
                            <?php } 
                          ?>
                    </td>
                    <td><?php 
                    if($row['payment_mode']=="razorpay" && file_exists('../img/Razorpay_logo.png')){ ?>
                      <img src="<?php echo $logoUrl; ?>" style="width: 80px;" alt="logo" />
                    <?php } ?>
                    </td>
                    
                    
                    <td><?php echo $row['package_name']; ?></td>
                    <td><?php echo $row['payment_mode']; ?></td>
                    <td><?php echo number_format($row['transection_amount']-$row['transaction_charges'],2); ?></td>
                    <td><?php echo $row['transection_date']; ?></td>
                    <td><?php echo $row['payment_txnid']; ?></td>
                    <td><?php echo $row['order_id']; ?></td>
                    <td><?php echo $invoice_no; ?></td>
                    
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!--End Row-->

  </div>
  <!-- End container-fluid-->

</div><!--End content-wrapper-->

<div class="modal fade" id="updateTransactionStatus">
    <div class="modal-dialog modal-lg">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Check Transaction Status</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="transactionStatusForm">
            <div class="row form-group ">
              <div class="col-sm-12 col-md-12 text-center">
                <img src="" id="paymentGatewayLogo" style="width: 80px;" alt="logo" />
              </div>
            </div>
            <div class="row form-group">
              <div class="col-sm-12 col-md-12 text-center">
                <label class="form-control-label">Latest payment status is : <span class="required" id="paymentStatus"></span></label>
              </div>
              
            </div>

            <div class="form-footer text-center">
              <div class="col-sm-12 col-md-12 text-center">
                <input type="hidden" name="order_id" id="order_id" >
                <input type="hidden" name="transection_id" id="transection_id" >
                <input type="hidden" name="no_of_month" id="no_of_month" >
                <input type="hidden" name="package_id" id="package_id" >
                <input type="hidden" name="status" id="status" >
                <input type="hidden" name="updateStatus" value="yes">
                <button type="button" onclick="updateTransactionStatus();" id="paymentStatusBtn" style="display: none;" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Update</button>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>