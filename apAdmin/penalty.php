<?php 
extract(array_map("test_input" , $_POST));
if (isset($edit_penaly) ) {
  $q=$d->select("penalty_master,unit_master,users_master,block_master","block_master.block_id=users_master.block_id and penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id' AND penalty_master.penalty_id='$penalty_id' $blockAppendQuery ","ORDER BY penalty_master.penalty_date DESC");
    $data=mysqli_fetch_array($q);
    extract($data);
    
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Penalty</h4>
       
     </div>
     <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <?php if (isset($edit_penaly) ) {  ?>
                 <form id="penaltyEdit" action="controller/penaltyController.php" enctype="multipart/form-data" method="post">
                  <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
                  <input type="hidden" name="penalty_id" id="penalty_id" value="<?php echo $penalty_id;?>">
                  <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
                  <input type="hidden" name="user_name" id="user_name" value="<?php echo $user_full_name;?>">
                  
                  <input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id;?>" id="balancesheet_id">
                  <input type="hidden" name="society_id" value="<?php echo $society_id;?>" id="society_id">
                  <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Branch <span class="required">*</span></label>
                <div class="col-sm-4 form-group">
                      <select name="block_id" disabled class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required>
                        <option value="">-- Select Branch --</option> 
                        <?php 
                        $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                        while ($blockData=mysqli_fetch_array($qb)) {
                        ?>
                        <option  <?php if($data['block_id']==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                        <?php } ?>

                      </select>         
                    </div>

                    <label for="input-10" class="col-sm-2 col-form-label">Department<span class="required">*</span></label>
                    <div class="col-sm-4" >
                      <select name="floor_id" id="floor_id" disabled class="form-control single-select" onchange="getUserByFloorId(this.value);" required>
                        <option value="">-- Select Department --</option> 
                        <?php 
                          $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$data[block_id]' $blockAppendQuery");  
                          while ($depaData=mysqli_fetch_array($qd)) {
                        ?>
                        <option  <?php if($data['floor_id']==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
                        <?php } ?>
                        
                        </select>
                    </div>
                    </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Employee <span class="required">*</span></label>
                    <div class="col-sm-4" id="">
                      <select disabled="" type="text" required="" class="form-control single-select " name="user_id" id="uId" >
                         <option value="">-- Select --</option>
                        <?php
                      
                         $q3=$d->select("users_master","user_id='$data[user_id]'");
                        while ($blockRow=mysqli_fetch_array($q3)) {
                          ?>
                          <option <?php if($data['user_id'] ==$blockRow['user_id']){ echo "selected";} ?> value="<?php echo $blockRow['user_id'];?>"><?php echo $blockRow['user_full_name'];?>  
                          </option>
                        <?php }?> 
                      </select>
                    </div>
                    <label for="input-10" class="col-sm-2 col-form-label">Photo 1</label>
                    <div class="col-sm-4" >
                      <input type="hidden" name="penalty_photo_old" value="<?php echo $penalty_photo;?>">
                      <input type="hidden" id="penalty_photo_old_2" name="penalty_photo_old_2" value="<?php echo $penalty_photo_2;?>">
                      <input type="hidden" id="penalty_photo_old_3" name="penalty_photo_old_3" value="<?php echo $penalty_photo_3;?>">
                      <input type="file"  accept="image/*"  maxlength="250" required="" name="penalty_photo" class="form-control-file border ">
                      
                    </div>
                  </div>
                  <div class="form-group row">
                      <label for="input-12sd" required class="col-sm-2 col-form-label">Photo 2</label>
                    <div class="col-sm-4">
                      <input class="photoOnly form-control-file border" type="file" accept="image/*"  name="penalty_photo_2">
                    </div>
                      <label for="input-12sd" required class="col-sm-2 col-form-label">Photo 3</label>
                    <div class="col-sm-4">
                      <input class="photoOnly form-control-file border" type="file" accept="image/*"   name="penalty_photo_3">
                    </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-sm-2">
                      </div>
                       <?php if ($penalty_photo!="") { ?>
                     <div class="col-sm-3">
                      <img class="card-img-top" height="150" onerror="this.src='../img/imagePlaceholder.jpg'"  src="../img/billReceipt/<?php echo $penalty_photo; ?>" alt="Card image cap">
                      Photo 1
                     </div>

                     <?php } if ($penalty_photo_2!="") { ?>
                     <div class="col-sm-3" id="facility_photo_2">
                      <img  class="card-img-top" height="150" onerror="this.src='../img/imagePlaceholder.jpg'"  src="../img/billReceipt/<?php echo $penalty_photo_2; ?>" alt="Card image cap">
                      Photo 2
                      <a style="position: absolute;top: 2px;right: 11px;background: white;width: 17px;height: 20px;padding-left: 2px;;" href="#" onclick="removePhoto2();" class="text-danger"><i class="fa fa-trash-o"></i></a>
                     </div>
                     <?php } if ($penalty_photo_3!="") { ?>
                     <div class="col-sm-3" id="facility_photo_3">
                      <img id="" class="card-img-top" height="150" onerror="this.src='../img/imagePlaceholder.jpg'"  src="../img/billReceipt/<?php echo $penalty_photo_3; ?>" alt="Card image cap">
                      Photo 3
                      <a style="position: absolute;top: 2px;right: 11px;background: white;width: 17px;height: 20px;padding-left: 2px;" href="#" onclick="removePhoto3();" class="text-danger"><i class="fa fa-trash-o"></i></a>
                     </div>
                     <?php } ?>
                  </div>
                  <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Penalty Description <span class="required">*</span></label>
                    <div class="col-sm-10" >
                      <textarea maxlength="300" required="" name="penalty_name" class="form-control "><?php echo $data['penalty_name']; ?></textarea>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    
                    <label for="input-10" class="col-sm-2 col-form-label">Date <span class="required">*</span></label>
                    <div class="col-sm-4" >
                      <input type="text" id="date-time-picker" readonly="" maxlength="250" required="" name="penalty_date" class="form-control" value="<?php echo $data['penalty_date']; ?>">
                      <input type="hidden"  name="penalty_date_old" class="form-control" value="<?php echo $data['penalty_date']; ?>">
                    </div>
                 
                    <label for="input-10" class="col-sm-2 col-form-label">Penalty Amount <span class="required">*</span></label>
                    <div class="col-sm-4" >
                      <input type="text" maxlength="7" required="" name="penalty_amount" class="form-control onlyNumber" inputmode="numeric" value="<?php if($data['gst'] == "1") { echo $data['amount_without_gst'];} else { echo $data['penalty_amount']; } ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="input-14" class="col-sm-2 col-form-label">Balance Sheet <i class="text-danger">*</i></label>
                    <div class="col-sm-4">
                      <select  type="text" required="" class="form-control  single-select" name="balancesheet_id">
                        <option value="">-- Select --</option>
                        <?php
                        error_reporting(0);
                        $q=$d->select("balancesheet_master","society_id='$society_id'","");
                        while ($row=mysqli_fetch_array($q)) {
                          if ($adminData['admin_type']==1 || $row['block_id']==0 ||  count($blockAryAccess)==0 ||  in_array($row['block_id'], $blockAryAccess) || $data['balancesheet_id']==$row['balancesheet_id'] ) {
                          ?>
                          <option <?php if($data['balancesheet_id']==$row['balancesheet_id']){ echo "selected";} ?> value="<?php echo $row['balancesheet_id'];?>"><?php echo $row['balancesheet_name'];?></option>
                        <?php } }?>
                      </select>
                    </div>
                 
                    
                    <label for="is_taxble" class="col-sm-2 col-form-label">Bill Type <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      
                      <select required=""   class="form-control  " name="is_taxble" id="is_taxble" class="bill_type-cls" onchange="getGSTDetails();">
                        <option value="">-- Select --</option>
                        <?php include 'billTypeOptionList.php'; ?>
                      </select>
                    </div>
                  </div>
                  <div <?php if($data['is_taxble']=="1"  ){ } else { ?> style="display: none" <?php } ?>  id="gst_detail_div_edit" >
                    <div class="form-group row">
                      <label for="gst" class="col-sm-2 col-form-label">Tax Amount </label>
                      <div class="col-sm-4">
                        <div class=" icheck-inline">
                          <input   <?php if($data['gst']=="0"  ){ echo 'checked=""';} ?>   type="radio"  id="inline-radio-info_edit" value="0" name="gst">
                          <label for="inline-radio-info_edit">Included</label>
                        </div>
                        <div class=" icheck-inline">
                          <input  <?php if($data['gst']=="1"  ){ echo 'checked=""';} ?>        type="radio" id="inline-radio-success_edit" value="1" name="gst">
                          <label for="inline-radio-success_edit">Excluded</label>
                        </div>
                      </div>
                      
                    
                      <label for="taxble_type" class="col-sm-2 col-form-label"><?php echo $xml->string->tax_type; ?></label>
                      <div class="col-sm-4">
                        <select    class="form-control  " name="taxble_type" id="taxble_type">
                          <option value="">-- Select --</option>
                          <?php include 'taxOptionList.php'; ?>
                          
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      
                      <label for="tax_slab" class="col-sm-2 col-form-label">Tax Value <span class="text-danger">*</span></label>
                      <div class="col-sm-4">
                        <?php  $q=$d->select("gst_master","status='0'","ORDER BY slab_percentage ASC");
                        ?>
                        <select <?php if(mysqli_num_rows($q)){ echo 'required=""'; } ?> required="" class="form-control  " name="tax_slab">
                          <option value="">-- Select --</option>
                          <?php
                          
                          while ($row=mysqli_fetch_array($q)) {
                            ?>
                            <option title="<?php echo $row['description'];?>" <?php if($data['tax_slab']==$row['slab_percentage']  ){ echo "selected";} ?>   value="<?php echo $row['slab_percentage'];?>"><?php echo $row['slab_percentage'];?>%</option>
                          <?php }?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <?php //IS_1470 ?>
                  <div class="form-footer text-center">
                    <button id="EditPaneltyBtn" type="submit" name="EditPanelty" value="EditPanelty" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                  </div>
                </form>

              <?php } else { ?>
                <form id="penaltyAdd" action="controller/penaltyController.php" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Branch <span class="required">*</span></label>
                <div class="col-sm-4 form-group">
                      <select name="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" >
                        <option value="">-- Select Branch --</option> 
                        <?php 
                        $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                        while ($blockData=mysqli_fetch_array($qb)) {
                        ?>
                        <option  <?php if($bId==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                        <?php } ?>

                      </select>         
                    </div>

                    <label for="input-10" class="col-sm-2 col-form-label">Department<span class="required">*</span></label>
                    <div class="col-sm-4" >
                      <select name="floor_id" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value);" >
                        <option value="">-- Select Department --</option> 
                        <?php 
                          $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$bId' $blockAppendQuery");  
                          while ($depaData=mysqli_fetch_array($qd)) {
                        ?>
                        <option  <?php if($dId==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
                        <?php } ?>
                        
                        </select>
                    </div>
                    </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Employee <span class="required">*</span></label>
                    <div class="col-sm-4" id="">
                     <select type="text"  multiple class="form-control multiple-select uId" name="user_id[]" >
                        <option value="">-- Select --</option>
                        <?php 
                          /* $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.floor_id=floors_master.floor_id AND users_master.delete_status=0 AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status!=0 $blockAppendQuery ","ORDER BY unit_master.unit_id ASC");
                           while ($blockRow=mysqli_fetch_array($q3)) { */
                         ?>
                         <!--  <option value="<?php echo $blockRow['unit_id'];?>~<?php echo $blockRow['user_id'];?>"><?php echo $blockRow['user_full_name'];?> (<?php echo $blockRow['user_designation'];?>-<?php echo $blockRow['floor_name'];?> <?php echo $blockRow['block_name'];?>)
                             
                          </option>
                          <?php //}?> -->
                        </select>
                    </div>
                    <label for="input-10" class="col-sm-2 col-form-label">Photo 1<span class="required">*</span></label>
                    <div class="col-sm-4" >
                      <input type="file"  accept="image/*"  maxlength="250" required="" name="penalty_photo" class="form-control-file border photoOnly" id="penalty_photo">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-12sd" required class="col-sm-2 col-form-label">Photo 2</label>
                  <div class="col-sm-4">
                    <input class="photoOnly form-control-file border" type="file" accept="image/*"  name="penalty_photo_2">
                  </div>
                    <label for="input-12sd" required class="col-sm-2 col-form-label">Photo 3</label>
                  <div class="col-sm-4">
                    <input class="photoOnly form-control-file border" type="file" accept="image/*"   name="penalty_photo_3">
                  </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label"> Description <span class="required">*</span></label>
                    <div class="col-sm-10" >
                      <textarea maxlength="300" required="" name="penalty_name" class="form-control "></textarea>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Date <span class="required">*</span></label>
                    <div class="col-sm-4" >
                      <input type="text" id="date-time-picker" readonly="" maxlength="250" required="" name="penalty_date" class="form-control">
                    </div>
                     <label for="input-10" class="col-sm-2 col-form-label"> Amount <span class="required">*</span></label>
                    <div class="col-sm-4" >
                      <input type="text" maxlength="7" required="" name="penalty_amount" class="form-control onlyNumber" inputmode="numeric">
                    </div>
                </div>
                 

                <div class="form-group row">

                  <label for="input-14" class="col-sm-2 col-form-label">Balance Sheet <i class="text-danger">*</i></label>
                  <div class="col-sm-4">
                    <select type="text" required="" class="form-control single-select" name="balancesheet_id">
                        <option value="">-- Select --</option>
                         <?php 
                           error_reporting(0);
                          $q=$d->select("balancesheet_master","society_id='$society_id'","");
                           while ($row=mysqli_fetch_array($q)) {
                            if ($adminData['admin_type']==1 || $row['block_id']==0 ||  count($blockAryAccess)==0 ||  in_array($row['block_id'], $blockAryAccess)  ) {
                         ?>
                          <option value="<?php echo $row['balancesheet_id'];?>"><?php echo $row['balancesheet_name'];?></option>
                          <?php } }?>
                     </select>
                  </div>
                  <label for="is_taxble" class="col-sm-2 col-form-label">Bill Type <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                   
                       <select required=""   class="form-control  " name="is_taxble" id="is_taxble" >
                      <?php include 'billTypeOptionList.php'; ?>
                    </select>
                  </div>
                </div>

                <div style="display: none" id="gst_detail_div" >

                  <div class="form-group row">
                  <label for="gst" class="col-sm-2 col-form-label">Tax Amount </label>
                   <div class="col-sm-4">
                    <div class=" icheck-inline">
                      <input  checked   type="radio"  id="inline-radio-info" value="0" name="gst">
                      <label for="inline-radio-info">Included</label>
                    </div>
                    <div class=" icheck-inline">
                      <input      type="radio" id="inline-radio-success" value="1" name="gst">
                      <label for="inline-radio-success">Excluded</label>
                    </div>
                  </div>
                    
                  <label for="taxble_type" class="col-sm-2 col-form-label"><?php echo $xml->string->tax_type; ?> </label>
                  <div class="col-sm-4">
                     <select    class="form-control  " name="taxble_type" id="taxble_type">
                      <option value="">-- Select --</option>
                      <?php include 'taxOptionList.php'; ?>
                    </select>
                  </div>
                </div>

                 <div class="form-group row">
                  
                  <label for="tax_slab" class="col-sm-2 col-form-label">Tax Value <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <?php 
                          $q=$d->select("gst_master","status='0'","ORDER BY slab_percentage ASC");
                          ?>
                       <select <?php if(mysqli_num_rows($q)){ echo 'required=""'; } ?>  class="form-control  " name="tax_slab">
                      <option value="">-- Select --</option>
                        <?php
                           while ($row=mysqli_fetch_array($q)) {
                         ?>
                          <option title="<?php echo $row['description'];?>" value="<?php echo $row['slab_percentage'];?>"><?php echo $row['slab_percentage'];?>%</option>
                          <?php }?>
                    </select>
                  </div>
                 </div>
                 
                 </div>
                <?php //IS_1470 ?>
         
                <div class="form-footer text-center">
                  <?php //IS_1019 addPenaltiesBtn ?>

                  <button id="addPenaltiesBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                 <input type="hidden" name="add"  value="add">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('penaltyAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                </div>

              </form>
            <?php } ?>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->

    <script type="text/javascript">
       function removePhoto2() {
        $('#facility_photo_2').remove();
        $('#penalty_photo_old_2').val('');
      }

       function removePhoto3() {
        $('#facility_photo_3').remove();
        $('#penalty_photo_old_3').val('');
      }

    </script>