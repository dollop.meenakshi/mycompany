<?php error_reporting(0);
  
  error_reporting(0);
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year']= $_REQUEST['month_year'];
  $bId= $_REQUEST['bId'];
  $dId= $_REQUEST['dId'];
  $fromDate= $_REQUEST['fromDate'];
  $toDate= $_REQUEST['toDate'];
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-3">
          <h4 class="page-title">Absent To Leave </h4>
        </div>
     </div>
     <form action="" class="branchDeptFilter">
        <div class="row pt-2 pb-2">
          <?php include('selectBranchDeptForFilter.php'); ?>
		  <div class="col-md-2 col-6 form-group">
            <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" name="fromDate" value="<?php if (isset($fromDate) && $fromDate != '') {echo $fromDate;} else {echo date('Y-m-01');} ?>">
          </div>
          <div class="col-md-2 col-6 form-group">
            <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerTo" name="toDate" value="<?php if (isset($toDate) && $toDate != '') {echo $toDate;} else {echo date('Y-m-t');} ?>">
          </div>
          <div class="col-md-3 form-group ">
              <input class="btn btn-success btn-sm " type="submit" name="getReport" class="form-control" value="Get Data">
          </div>    
      </div>
     </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                  <?php 
                  if(isset($dId) && $dId>0) {
                    $deptFilterQuery = " AND users_master.floor_id='$dId'";
                    
                    $userArray = array();
                    $uq = $d->select("users_master","society_id='$society_id' $deptFilterQuery $blockAppendQueryUser");
                    while ($userData=mysqli_fetch_array($uq)) {
                      $user_data = array('user_id' => $userData['user_id'],
                                          'user_name' => $userData['user_full_name']);
                      array_push($userArray,$user_data);
                    }
                    //print_r($userArray);
                    $crDate = date('Y-m-d');
                    $aq = $d->select("attendance_master,users_master","attendance_master.user_id=users_master.user_id AND attendance_master.society_id='$society_id' AND attendance_master.attendance_date_start BETWEEN '$fromDate' AND '$toDate' $deptFilterQuery $blockAppendQueryUser");
                    $userAttendanceArray = array();
                    while ($attendanceData=mysqli_fetch_array($aq)) {
                      $attendance_data = array('user_id' => $attendanceData['user_id'],
                                          'attendance_date' => $attendanceData['attendance_date_start']);
                      array_push($userAttendanceArray,$attendance_data);
                    }
                    //print_r($userAttendanceArray);
                    
                    $lq=$d->select("leave_master,users_master","leave_master.user_id=users_master.user_id AND leave_master.society_id='$society_id' AND leave_master.leave_start_date BETWEEN '$fromDate' AND '$toDate' $deptFilterQuery $blockAppendQueryUser");
                    $userLeaveArray = array();
                    while ($leaveData=mysqli_fetch_array($lq)) {
                      $leave_data = array('user_id' => $leaveData['user_id'],
                                          'leave_date' => $leaveData['leave_start_date']);
                      array_push($userLeaveArray,$leave_data);
                    }
                    //print_r($userLeaveArray);
                    $counter = 1;
                    ?>
                    <table id="example" class="table table-bordered">
                      <thead>
                          <tr>
                            <th>Sr.No</th>                        
                            <th>Employee Name</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php 
                       
                        for($u=0; $u<count($userArray); $u++){
                          $begin = new DateTime($fromDate);
                          $end = new DateTime($toDate);
                          $interval = DateInterval::createFromDateString('1 day');
                          $period = new DatePeriod($begin, $interval, $end);
                          foreach ($period as $dt) {
                            $date = $dt->format("Y-m-d");
                            if(in_array($date,$userAttendanceArray['attendance_date']) && in_array($userArray[$u]['user_id'],$userAttendanceArray['user_id'])){
                            ?>
                            <tr>
                              <td><?php echo $counter++; ?></td>
                              <td><?php echo $userArray[$u]['user_name']; ?></td>
                            </tr>
                          <?php } } } ?>
                      </tbody>
                    </table>
                    <?php } else {  ?>
                      <div class="" role="alert">
                        <span><strong>Note :</strong> Please Select Department</span>
                      </div>
                    <?php } ?>
                </div>
            </div>
          </div>
        </div>
     </div><!-- End Row-->
    </div>
  </div>
 
<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>
<script type="text/javascript">
<?php
 if(!isset($_GET['month_year']))
 { ?>
    $('#month_year').val(<?php echo $currentMonth; ?>)
<?php } ?>
function popitup(url) {
  newwindow=window.open(url,'name','height=800,width=900, location=0');
  if (window.focus) {
    newwindow.focus()
    }
    return false;
  }
</script>
<style>
.attendance_status
{  
  min-width: 113px;
}
</style>
