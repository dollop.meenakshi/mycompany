<?php 
// session_cache_limiter(false);
session_start();
include_once 'lib/dao.php';
include_once 'lib/model.php';

$d = new dao();
$m = new model();
extract(array_map("test_input" , $_GET));
if (isset($_COOKIE['bms_admin_id'])) {
    $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);



?>
<!DOCTYPE html>
<html>
<head>
	<title>QE Code print</title>
	<style type="text/css">
		.qrBox {
			width: 49%;
			border: 1px solid;
			display: inline-block;
			margin-bottom: 3px;
			margin-top: 3px;
			padding-top: 3px;
			padding-bottom: 3px;
			text-align: center;
		}
		p {
			margin: 0px;
		}
	</style>
</head>

  <body onload="window.print()">

            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
                  <div class="row">
                   <?php 
                    $i=1;
                    if (isset($isSingle) && $isSingle=='true') {
                    $id = (int)$id;
                    $q11 = $d->select("qr_code_master,society_master" ,"society_master.society_id=qr_code_master.society_id AND  qr_code_master.society_id='$society_id' AND qr_code_master_id='$id'","LIMIT 1");
                    } else {
                    $q11 = $d->select("qr_code_master,society_master" ,"society_master.society_id=qr_code_master.society_id AND  qr_code_master.society_id='$society_id'","");

                    }
                   if(mysqli_num_rows($q11)>0) {

                    while ($data=mysqli_fetch_array($q11)) {
                      extract($data); ?>  
                    
                    <div class="col-lg-2 col-md-6 qrBox text-center">
                      
                        <?php $qr_size          = "320x320";
                          $qr_content       = "$society_id-$qr_code_master_id-".$data['area_name'];
                          $qr_correction    = strtoupper('H');
                          $qr_encoding      = 'UTF-8';

                            //form google chart api link
                          $qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
                          ?>
                          <img src="<?php echo $qrImageUrl; ?>" alt="">
                          <p><?php echo $data['area_name']; ?></p>
                          <p><b><?php echo $data['society_name']; ?></b></p>
                         
                    </div> 
                    <?php } } else {
                      echo "<img src='img/no_data_found.png'>";
                    } ?>
                   
                </div>
            </div>
          
</body>
<script type="text/javascript">
        Android.print('print');
  </script>
</html>