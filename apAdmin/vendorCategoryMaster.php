<?php error_reporting(0);
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
          <h4 class="page-title">Vendor Category Master</h4>
        </div>
        <div class="col-sm-3 col-6 text-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <!-- <a href="xeroxPaperBtn" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deletePaperSize');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
        </div>
      </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
               
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <!-- <th>#</th> -->
                        <th>Sr.No</th>
                        <th>Vendor Category Name</th>
                        <th>Vendor Category Image</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody id="showFilterData">
                   <?php 
                    $i=1;
                                           
                      if(isset($dId) && $dId>0) {
                        $departmentFilterQuery = " AND users_master.floor_id='$dId'";
                       }
                     
                      $q=$d->select("vendor_category_master");
                        $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                        
                      <!-- <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['xerox_paper_size_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['xerox_paper_size_id']; ?>">                      
                        </td> -->
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['vendor_category_name']; ?></td>
                      <td><?php if ($data['vendor_category_image']!='') { ?>
                        <a href="../img/billReceipt/<?php echo $data['vendor_category_image']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["vendor_category_image"]; ?>"><img width="50" height="50"  src="../img/ajax-loader.gif" data-src="../img/billReceipt/<?php echo $data['vendor_category_image']; ?>"  href="#divForm<?php echo $data['penalty_id'];?>" class="btnForm lazyload" ></a>
                        <?php } else {
                        echo "Not Uploaded";
                        } ?></td>
                      <td>
                      <?php if($data['vendor_category_satus']=="0"){
                              $status = "vendorcategoryMasterStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "vendorcategoryMasterStatusActive";
                              $active="";  
                          } ?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_category_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->

<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
.hideupdate{
  display:none;
}

</style>