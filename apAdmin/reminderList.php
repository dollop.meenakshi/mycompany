<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
        <h4 class="page-title"> Reminder List</h4>
     </div>
     <div class="col-sm-3 col-6 text-right">
          <a href="#"  data-toggle="modal" data-target="#addReminder"  class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Reminder</a>
      </div>
     </div>
     <div class="row pt-2 pb-2">
      <div class="col-sm-8 col-8">
      <ol class="breadcrumb">
        <span class="badge badge-pill badge-danger m-1">Pending </span>
        <span class="badge badge-pill badge-success m-1">Completed </span>
        <span class="badge badge-pill bg-primary text-white m-1">Upcoming </span>
      </ol>
    </div>
  </div>
    <!-- End Breadcrumb-->
     <div class="row">
        <?php
        $today=date('Y-m-d');
          $cTime= date("H:i:s");
          $today = strtotime("$today $cTime");
        $q=$d->select("reminder_master","admin_id='$_COOKIE[bms_admin_id]'","ORDER BY reminder_date DESC LIMIT 21");
         if(mysqli_num_rows($q)>0) {
          while($data=mysqli_fetch_array($q)){
          extract($data); 
            $expire = strtotime($reminder_date);
          ?> 
        <div class="col-lg-4">
          <div class="card">
            
            <div class="card-header <?php if($data['reminder_status']=="1"){  echo "bg-success";} else if($today > $expire) { echo "bg-danger"; } else { echo "bg-primary"; }?> text-white" title=""><i class="fa fa-bell" aria-hidden="true"></i>  <?php echo  date("j M Y H:i A", strtotime($reminder_date)); ?> (<?php echo  date("l", strtotime($reminder_date)); ?>)
            
          </div>
            <div class="card-body">
               <?php //IS_569 <div class="cls-notice-info"> 
               ?>
              <p class="card-text"><div class="cls-notice-info" style="height:auto !important;"><?php echo  $reminder_text; ?></div></p>
            </div>
             
            <div  class="card-footer"> 
              <?php if($data['reminder_status']=="0"){
                        ?>
                           

                            <div style="display: inline-block;">
                        <form  action="controller/reminderController.php" method="post">
                          <input type="hidden" name="reminder_id" value="<?php echo $reminder_id; ?>">
                          <input type="hidden" name="DoneRem" value="deleteRems">
                          <button type="submit" name="DoneRem" class="btn btn-secondary btn-sm  form-btn"> Complete</button>
                        </form>
                      </div>


                          <?php }  ?>
                      
                     
                       
                        <div style="display: inline-block;">
                        <form  action="controller/reminderController.php" method="post">
                          <input type="hidden" name="reminder_id" value="<?php echo $reminder_id; ?>">
                          <input type="hidden" name="deleteRem" value="deleteRems">
                          <button type="submit" name="deleteRem" class="btn btn-danger btn-sm  form-btn"> Delete</button>
                        </form>
                      </div>

            </div>
          </div>
        </div>
         <?php } } else {
      echo "<img src='img/no_data_found.png'>";
    } ?>
      </div>

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
  
<div class="modal fade" id="addReminder">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Reminder</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="addUserDiv">
         <form id="reminder" action="controller/reminderController.php" method="post" >
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Quick Reminder </label>
              <div class="col-sm-10">
                <?php  $quick_reminder= $xml->string->quick_reminder; 
                $reminderArray = explode("~", $quick_reminder);
                ?>
                <select onchange="copyReminder()" class="form-control single-select"  name="quick_reminder" id="quick_reminder">
                  <option value=""> Select for Quick Reminder </option>
                  <?php for ($i=0; $i < count($reminderArray) ; $i++) {  ?>
                  <option><?php echo $reminderArray[$i];?></option>
                  <?php  }?>
                </select>
              </div>
               
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Reminder Title <span class="required">*</span></label>
              <div class="col-sm-10">
                
                <textarea type="text" minlength="1" maxlength="150" placeholder="Reminder Text" class="form-control"   maxlength="50" required="" id="reminder_text" name="reminder_text"></textarea>
                
              </div>
               
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Reminder Date <span class="required">*</span></label>
              <div class="col-sm-10">
                <input type="text" placeholder="Reminder Date" readonly=""  id="date-time-picker-rem" class="form-control" name="reminder_date"  required="">
              </div>
               
            </div>
           
            
            <div class="form-footer text-center">
              <input type="hidden" name="addUser">

                <input type="hidden" name="remAddBtn" value="remAddBtn">
              <button type="submit" name="" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>

              <button  type="reset" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
