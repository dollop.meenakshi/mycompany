<div class="content-wrapper">
    <div class="container-fluid">
    <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
		    <h4 class="page-title"> <?php echo $xml->string->facilities; ?></h4>
	   </div>
	   <div class="col-sm-3 col-6">
       <div class="btn-group float-sm-right">
         <a href="facility" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add_new; ?></a>
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->

     <div class="row">
    
     <?php 
     
            $q=$d->select("facilities_master","society_id='$society_id'","");
           if(mysqli_num_rows($q)>0) { 
            while ($row=mysqli_fetch_array($q)) {
                extract($row);
             ?>
      <div class="col-lg-4 col-12">
         <div class="card">
            <img class="card-img-top lazyload" height="220" onerror="this.src='../img/facility/default.png'"   src="../img/ajax-loader.gif" data-src="../img/facility/<?php echo $facility_photo; ?>" alt="Card image cap">
            <div class="card-body">
              <?php //IS_613 title="<?php echo $facility_name; ?>
               <h5 class="card-title text-primary text-center" title="<?php echo $facility_name; ?>" >
                <?php 
         
               echo custom_echo($facility_name,30);
             //IS_613
               if ($facility_type==2) {
               ?>  <span class="badge badge-success m-1">Free</span>
               <?php } else { ?>  <span class="badge badge-secondary m-1"><?php echo $xml->string->paid; ?> </span> <?php }?>

              </h5>
             
               <hr>
              <div class="row text-center">
                <div class="col-sm-3 col-3">
                  <form action="facility" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="facility_id" value="<?php echo $facility_id;?>">
                    <input type="hidden" name="updatefacilities" value="updatefacilities">
                    <button style="margin-right: 5px;" type="submit" class="btn btn-success btn-sm " name=""><i class="fa fa-pencil"></i> </button>
                  </form>
                </div>
                <div class="col-sm-3 col-4">
                  <form action="facility" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="facility_id" value="<?php echo $facility_id;?>">
                    <input type="hidden" name="manageWorkingHours" value="manageWorkingHours">
                    <button style="margin-right: 5px;" type="submit" class="btn btn-warning btn-sm " name=""><i class="fa fa-calendar"></i> <?php echo $xml->string->schedule; ?></button>
                  </form>
                </div>
                <div class="col-sm-3 col-2">
                 <?php
                    if($facility_active_status=="0"){
                    ?>
                      <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $facility_id; ?>','facilityDeactive');" data-size="small"/>
                      <?php } else { ?>
                     <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $facility_id; ?>','facilityActive');" data-size="small"/>
                    <?php } ?>
                </div>
               <div class="col-sm-3 col-3">
                <?php $toatlBooking =$d->count_data_direct("booking_id","facilitybooking_master","society_id='$society_id' AND facility_id='$facility_id'");
                if ($toatlBooking<1) {?>
                <form action="controller/facilitiesController.php" method="POST" >
                  <input type="hidden" name="facility_id" value="<?php echo $facility_id;?>">
                  <input type="hidden" name="deletedFacilityName" value="<?php echo $facility_name;?>">
                  <input type="hidden" value="deletefacilities" name="deletefacilities">
                 <button type="submit" class="form-btn btn btn-danger btn-sm " name=""><i class="fa fa-trash-o"></i> Delete</button>
                </form>
                <?php } else { ?>
                 <button onclick="showError('Facility Already booked by member');" class="btn btn-danger btn-sm " name="deletefacilities"><i class="fa fa-trash-o"></i> </button>
                <?php } ?>
                </div>
              </div>
            </div>
         </div>
      </div><?php }  } else {
      echo "<img src='img/no_data_found.png'>";
        
      }?>
    
   </div><!--End Row-->


   
   </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
   </div><!--End content-wrapper-->