<?php
//$month_year= $_REQUEST['month_year'];
$laYear = $_REQUEST['laYear'];
$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
$month = $_GET['month'];
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));


if (isset($dId) && $dId > 0) {
    $deptFilterQuery = " AND salary_slip_master.floor_id='$dId'";
}

if (isset($bId) && $bId > 0) {
    $branchFilterQuery = " AND salary_slip_master.block_id='$bId'";
}
if (isset($uId) && $uId > 0) {
    $userFilterQuery = " AND users_master.user_id='$uId'";
}
if (isset($_GET['laYear']) && $_GET['laYear'] > 0) {
    $YearFilterQuery = " AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y')='$_GET[laYear]'";
}



if (isset($month) && ($month > 0) && ($laYear > 0)) {
  $yearData  = $month . "-" . $laYear;
  $MonthFilterQuery = " AND salary_slip_master.salary_month_name='$yearData'";
} else {
  $yearData  = date('n',strtotime('last month')) . "-" . $currentYear;
  $MonthFilterQuery = " AND salary_slip_master.salary_month_name='$yearData'";
}

$month=$_GET['month'];
$Year = $_GET['laYear'];
$is_failed = $_GET['is_failed'];
if($is_failed >0 && $is_failed>0  && $is_failed!=''){
    $q= $d->selectRow('floors_master.floor_name,block_master.block_name,salary_slip_master.salary_slip_status,salary_slip_master.salary_month_name,salary_slip_master.salary_slip_id,salary_slip_master.prepared_by,salary_slip_master.checked_by,salary_slip_master.authorised_by,chekced.admin_name AS checked_user_by,gen.admin_name AS generated_user_by,published.admin_name AS published_user_by,salary_bulk_generate_report.*,users_master.user_id,users_master.user_mobile,users_master.user_full_name,users_master.user_designation,users_master.block_id,users_master.floor_id',"block_master,floors_master,users_master,salary_bulk_generate_report  LEFT JOIN salary_slip_master ON salary_slip_master.salary_slip_id = salary_bulk_generate_report.salary_slip_id LEFT JOIN bms_admin_master AS chekced ON chekced.admin_id = salary_slip_master.checked_by 
    LEFT JOIN bms_admin_master AS gen ON gen.admin_id = salary_slip_master.prepared_by
    LEFT JOIN bms_admin_master AS published ON published.admin_id = salary_slip_master.authorised_by ","floors_master.floor_id = users_master.floor_id AND block_master.block_id = users_master.block_id  AND users_master.user_id = salary_bulk_generate_report.user_id AND salary_bulk_generate_report.month='$month' AND salary_bulk_generate_report.year='$Year' $branchFilterQuery $deptFilterQuery $userFilterQuery");
}else
{
  
    $q= $d->selectRow('salary_slip_master.*,floors_master.floor_name,block_master.block_name,chekced.admin_name AS checked_user_by,gen.admin_name AS generated_user_by,published.admin_name AS published_user_by,users_master.user_id,users_master.user_mobile,users_master.user_full_name,users_master.user_designation,users_master.block_id,users_master.floor_id,user_bank_master.bank_name,user_bank_master.ifsc_code,user_bank_master.bank_branch_name,user_bank_master.account_no,user_bank_master.account_type',"block_master,floors_master,users_master,salary_slip_master LEFT JOIN user_bank_master ON  salary_slip_master.user_id = user_bank_master.user_id AND user_bank_master.is_primary=1 LEFT JOIN bms_admin_master AS chekced ON chekced.admin_id = salary_slip_master.checked_by 
    LEFT JOIN bms_admin_master AS gen ON gen.admin_id = salary_slip_master.prepared_by
    LEFT JOIN bms_admin_master AS published ON published.admin_id = salary_slip_master.authorised_by","block_master.block_id = users_master.block_id  AND floors_master.floor_id = users_master.floor_id  AND users_master.user_id = salary_slip_master.user_id AND  salary_slip_master.salary_slip_status=2 $branchFilterQuery $deptFilterQuery $userFilterQuery $MonthFilterQuery","ORDER BY salary_slip_master.salary_month_name ASC, users_master.user_full_name ASC");
}

$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));

$salaryArray = array();
$salary_slip_ids_array = array();
while ($data = mysqli_fetch_array($q)) {
    array_push($salaryArray, $data);
    array_push($salary_slip_ids_array, $data['salary_slip_id']);
}

$salaryDeductionType = array();
 $qsalType=$d->selectRow("salary_earning_deduction_id,earning_deduction_name","salary_earning_deduction_type_master","society_id='$society_id' AND  earn_deduct_is_delete=0");
while ($dataSalaryType=mysqli_fetch_array($qsalType)) {
    array_push($salaryDeductionType, $dataSalaryType);
}



?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title">Published Salary Report</h4>
            </div>
        </div>
        <form action="" method="get" id="fltrFrm" class="branchDeptFilter">
            <div class="row pb-2">
                <?php include('selectBranchDeptEmpForFilterAll.php'); ?>
                <div class="col-md-3 col-6">
                    <select name="laYear" class="form-control">
                        <option value="">Year</option>
                        <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
                                    echo 'selected';
                                } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
                        <option <?php if ($_GET['laYear'] == $onePreviousYear) {
                                    echo 'selected';
                                } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
                        <option <?php if ($_GET['laYear'] == $currentYear) {
                                    echo 'selected';
                                } ?> <?php if ($_GET['laYear'] == '') {
                                echo 'selected';
                            } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                        <option <?php if ($_GET['laYear'] == $nextYear) {
                                    echo 'selected';
                                } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear; ?></option>
                    </select>
                </div>
                <div class="col-md-3 col-6">
                    <select class="form-control single-select"  name="month" id="month">
                        <option value="0" <?php if($_GET['month']==0) { echo 'selected';} ?>>All Month</option> 
                        <?php
                        $selected = "";
                        for ($m = 1; $m <= 12; $m++) {
                            $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                            $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
                            if (isset($_GET['month'])  && $_GET['month'] !="") {
                            if($_GET['month'] == $m)
                            {
                                $selected = "selected";
                            }else{
                                $selected = "";
                            }
                            
                            } else {
                            $selected = "";
                            if ($m == date('n',strtotime('last month'))) 
                            { 
                                $selected = "selected";
                            }
                            else
                            {
                                $selected = "";
                            }
                            }
                        ?>
                        <option  <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $month; ?></option> 
                        <?php }?>
                    </select> 
                </div>
                <input type="hidden" name="is_failed" id="is_failed" value="<?php echo $is_failed; ?>">
                <div class="col-lg-2 from-group col-6">
                    <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Report">
                </div>
            </div>
        </form>
        <div class="row pb-2 ">
            <div class="col-lg-12 from-group col-12 text-right">
                <?php 
                    if($_GET['is_failed']==1){
                        $clsName = 'btn-success';
                        $kName = "View Published ";
                    }else{
                        $clsName = 'btn-danger';
                        $kName = "View Invalid ";
                    }
                    ?>
                <button type="button" class="btn btn-sm <?php echo $clsName; ?>" onclick="getFailedData()"> <?php echo $kName; ?>salary</button>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <input type="hidden" name="floor_id_old" id="floor_id_old" value="<?php if (isset($_GET['dId']) && $_GET['dId'] > 0) {echo $_GET['dId']; } ?>">

        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="card-body">

                        <div class="table-responsive">
                            <?php
                            $i = 1;
                           
                          
                            ?>

                                <table id="<?php if ($adminData['report_download_access'] == 0) {
                                                echo 'exampleReportWithoutBtn';
                                            } else {
                                                echo 'exampleReport';
                                            } ?>" class="table table-bordered">
                                    <thead>
                                        <tr>
                                        <?php if ($is_failed != 1) {?>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th>Designation</th>
                                            <th>Mobile</th>
                                            <th>Departement</th>
                                            <th>Month</th>
                                            <th>Bank Name</th>
                                            <th>Bank Branch</th>
                                            <th>IFSC</th>
                                            <th>Account Number</th>
                                            <th>Account type</th>
                                            <th>Net Salary</th>
                                            <th>Gorss Salary</th>
                                            <?php for ($sTypes=0; $sTypes <count($salaryDeductionType) ; $sTypes++) { 
                                             ?>
                                             <th><?php echo $salaryDeductionType[$sTypes]['earning_deduction_name'];?></th>
                                            <?php } ?>
                                            <th>Reimbursement</th>
                                            <th>Month Days</th>
                                            <th>Working Days</th>
                                            <th>Extra Days</th>
                                            <th>Paid Leave</th>
                                            <th>Generated By</th>
                                            <th>Prepared By</th>
                                            <th>Published By</th>
                                            <th>Date</th>
                                       <?php }else{ ?>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th>Mobile</th>
                                            <th>Departement</th>
                                            <th>Month</th>
                                            <th>Date</th>
                                            <th>salary Response</th>
                                           
                                        <?php } ?>

                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                       
                                        for ($is=0; $is <count($salaryArray) ; $is++) { 
                                         if ($is_failed != 1) { 
                                            
                                        ?>
                                            <tr>
                                                <td><?php echo  $i++; ?></td>
                                                <td><?php echo $salaryArray[$is]['user_full_name']; ?> <?php if(isset($salaryArray[$is]['user_designation']) && $salaryArray[$is]['user_designation'] !="") ; ?></td>
                                                <td><?php echo  $salaryArray[$is]['user_designation']; ?></td>
                                                <td><?php echo  $salaryArray[$is]['user_mobile']; ?></td>
                                                <td><?php echo  $salaryArray[$is]['floor_name']." (".$salaryArray[$is]['block_name'].")"; ?></td>
                                                <td><?php if($salaryArray[$is]['salary_month_name'] !=""){ echo $salaryArray[$is]['salary_month_name']; } ?></td>
                                                <td><?php echo $salaryArray[$is]['bank_name']; ?></td>
                                                <td><?php echo $salaryArray[$is]['bank_branch_name']; ?></td>
                                                <td><?php echo $salaryArray[$is]['ifsc_code']; ?></td>
                                                <td><?php echo $salaryArray[$is]['account_no']; ?></td>
                                                <td><?php echo $salaryArray[$is]['account_type']; ?></td>
                                                <td class="net_salary_td"><?php echo $salaryArray[$is]['total_net_salary']; ?></td>
                                                <td class="net_gross_td"><?php echo $salaryArray[$is]['total_earning_salary']; ?></td>
                                                 <?php for ($sTypes=0; $sTypes <count($salaryDeductionType) ; $sTypes++) { 
                                                 ?>
                                                 <td class="<?php echo 'salary_earn_ded'.$sTypes; ?>"><?php 
                                                   $salary_earning_deduction_idView =  $salaryDeductionType[$sTypes]['salary_earning_deduction_id'];
                                                    $dataValue = json_decode($salaryArray[$is]['salary_json'],true);
                                                   if ($dataValue[$salary_earning_deduction_idView]>0) {
                                                        // code...
                                                   echo  $dataValue[$salary_earning_deduction_idView];
                                                    } else {
                                                        echo "0";
                                                    }
                                                    // print_r($dataValue);
                                                  ?></td>
                                                <?php } ?>
                                                <td class="reimbursement_td"><?php echo $salaryArray[$is]['expense_amount']; ?></td>
                                                <td><?php echo $salaryArray[$is]['total_month_days']; ?></td>
                                                <td><?php echo $salaryArray[$is]['total_working_days']; ?></td>
                                                <td><?php echo $salaryArray[$is]['extra_days']; ?></td>
                                                <td><?php echo $salaryArray[$is]['paid_leave_days']; ?></td>
                                                <td><?php echo $salaryArray[$is]['generated_user_by']; ?></td>
                                                <td><?php echo $salaryArray[$is]['checked_user_by']; ?></td>
                                                <td><?php echo $salaryArray[$is]['published_user_by']; ?></td>
                                                <td><?php if($salaryArray[$is]['created_date'] !="") echo date("d-M-Y h:i A",strtotime($salaryArray[$is]['created_date'])); ?></td>
                                        <?php } else{ ?>
                                                <td><?php echo  $i++; ?></td>
                                                <td><?php echo $salaryArray[$is]['user_full_name']; ?> <?php if(isset($salaryArray[$is]['user_designation']) && $salaryArray[$is]['user_designation'] !="") echo "( ". $salaryArray[$is]['user_designation']." ) "; ?></td>
                                                <td><?php echo  $salaryArray[$is]['user_mobile']; ?></td>
                                                <td><?php echo  $salaryArray[$is]['floor_name']."( ".$salaryArray[$is]['block_name']." )"; ?></td>
                                                <td><?php echo date("F-Y", strtotime('01-'.$_GET['month'].'-'.$_GET['laYear'])); ?></td>
                                                <td><?php if($salaryArray[$is]['created_date'] !="") echo date("d-M-Y h:i A",strtotime($salaryArray[$is]['created_date'])); ?></td>
                                                <td><?php echo $salaryArray[$is]['salary_response']; ?></td>
                                                
                                            </tr>
                                          <?php }  } 
                                          if(count($salaryArray)>0 && $is_failed != 1) {
                                           ?>
                                          <tr>
                                                <td>Total</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td class="total_net_salary"></td>
                                                <td class="total_gross_salary"></td>
                                                 <?php for ($sTypes=0; $sTypes <count($salaryDeductionType) ; $sTypes++) { 
                                                 ?>
                                                 <td class="tot<?php echo $sTypes; ?>"></td>
                                                <?php } ?>
                                                <td class="total_reimbursement"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                           

                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-    fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->

<div class="modal fade" id="attnSmryDateDataMdl">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Date Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="align-content: center;">
                <div class="card-body">
                    <div class="row col-md-12 ">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody id="attnSmryDateData">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>
<script>

    function getFailedData() {
        is_failed = $('#is_failed').val();
        if(is_failed=="0"){
            $('#is_failed').val(1);
        }else
        {
            $('#is_failed').val(0);
        }
        $( "#fltrFrm" ).submit();
    }

var ttc = '<?php echo count($salaryDeductionType); ?>';
$(document).ready(function()
{
  for (let i = 0; i < ttc; i++)
  {
    var sum = 0
    $(".salary_earn_ded"+i).each(function()
    {
      sum += parseFloat($(this).text());
    });
    $('.tot'+i).text(sum.toFixed(2));
  }

    var sum = 0
    $(".net_salary_td").each(function()
    {
      sum += parseFloat($(this).text());
    });
    $('.total_net_salary').text(sum.toFixed(2));

    var sum1 = 0
    $(".net_gross_td").each(function()
    {
      sum1 += parseFloat($(this).text());
    });
    $('.total_gross_salary').text(sum1.toFixed(2));

    var sum2 = 0
    $(".reimbursement_td").each(function()
    {
      sum2 += parseFloat($(this).text());
    });
    $('.total_reimbursement').text(sum2.toFixed(2));


});
</script>