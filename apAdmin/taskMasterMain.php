  <?php error_reporting(0);

  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year'] = $_REQUEST['month_year'];

  ?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Main Task</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
           
          </div>
        </div>
     </div>
     <form action="" class="branchDeptFilter">
        <div class="row ">
          <?php include 'selectBranchDeptEmpForFilterAll.php' ?>

          <div class="col-md-2 col-6 form-group">
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-90 days'));} ?>">   
          </div>
          <div class="col-md-2 col-6 form-group">
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
          </div>          
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get">
            </div>
        </div>
      </form> 
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>                        
                        <th>Task Name</th>
                        <th>Created By</th>
                        <th>Branch</th>
                        <th>Department</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                    if (isset($dId) && $dId > 0) {
                      $deptFilterQuery = " AND users_master.floor_id='$dId'";
                    }

                    if(isset($bId) && $bId>0) {
                      $blockFilterQuery = " AND users_master.block_id='$bId'";
                    }

                    if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                      $dateFilterQuery = " AND DATE_FORMAT(task_master_main.task_master_main_created_date,'%Y-%m-%d') BETWEEN '$from' AND '$toDate'";
                    }
                    if (isset($uId) && $uId > 0) {
                      $userFilterQuery = "AND task_master_main.task_master_main_created_by='$uId'";
                    }

                        $q=$d->select("block_master,floors_master,task_master_main,users_master","users_master.block_id=block_master.block_id AND floors_master.floor_id=users_master.floor_id AND task_master_main.task_master_main_created_by=users_master.user_id AND task_master_main.society_id='$society_id' $blockFilterQuery $deptFilterQuery $dateFilterQuery  $userFilterQuery ");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['task_master_main_name']; ?></td>
                       <td><?php echo $data['user_full_name'].' ('.$data['user_designation'].')'; ?></td>
                       <td><?php echo $data['block_name']; ?></td>
                       <td><?php echo $data['floor_name']; ?></td>
                       <td><?php echo date("d M Y h:i A", strtotime($data['task_master_main_created_date']));?></td>
                       <td>
                          <div class="d-flex align-items-center">
                            <a href="taskDetail?id=<?php echo $data['task_master_main_id']?>" title="View Detail" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-eye"></i> View Task</a>
                          </div>
                       </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
