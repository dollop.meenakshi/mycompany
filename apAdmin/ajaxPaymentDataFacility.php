<?php //IS_846
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
extract(array_map("test_input" , $_POST));
if(isset($payment_type_value) && $payment_type_value == "1"){ ?>
  <div class="form-group row">
    <label for="bank_name" class="col-sm-4 col-form-label">Bank Name <span class="required">*</span></label>
    <div class="col-sm-8" >
      <input type="text" name="bank_name" id="bank_name" class="form-control alphanumeric" maxlength="30" required="">
    </div>
  </div>
  <div class="form-group row">
    <label for="payment_ref_no" class="col-sm-4 col-form-label">Cheque No. <span class="required">*</span></label>
    <div class="col-sm-8" >
      <input type="hidden" name="payment_name" value="Cheque">
      <input type="text" id="payment_ref_no" name="payment_ref_no" class="form-control number" required="" minlength="6" maxlength="6">
    </div>
  </div>
  <script type="text/javascript">
    $('.number').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
  </script>
<?php } if (isset($payment_type_value) && $payment_type_value == "2") {  ?>
  <div class="form-group row">
    <label for="bank_name" class="col-sm-4 col-form-label">Bank Name <span class="required">*</span></label>
    <div class="col-sm-8" >
      <input type="text" id="bank_name" name="bank_name" class="form-control alphanumeric" maxlength="30" required="">
    </div>
  </div>
  <div class="form-group row">
    <label for="payment_ref_no" class="col-sm-4 col-form-label">Transaction Id <span class="required">*</span></label>
    <div class="col-sm-8" >
      <input type="text" id="payment_ref_no" name="payment_ref_no" class="form-control" required="" maxlength="15">
    </div>
  </div>
<?php } if(isset($payment_type_value) && $payment_type_value == "0"){ ?>
  <div class="form-group row">
    <label for="payment_ref_no" class="col-sm-4 col-form-label">Reference Number </label>
    <div class="col-sm-8">
      <input type="text" name="payment_ref_no" id="payment_ref_no" class="form-control" maxlength="30" >
    </div>
  </div>
   
<?php }   ?>
<script type="text/javascript">
  $(".alphanumeric").keypress(function (e) {
  var keyCode = e.which;
  if ( !( (keyCode >= 48 && keyCode <= 57) 
   ||(keyCode >= 65 && keyCode <= 90) 
   || (keyCode >= 97 && keyCode <= 122) ) 
   && keyCode != 8 && keyCode != 32) {
   e.preventDefault();
  }
});
</script>