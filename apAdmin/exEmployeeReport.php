<?php
$floor_id = (int)$_GET['floor_id'];
$dId = (int)$_REQUEST['dId'];
$bId = (int)$_REQUEST['bId'];
error_reporting(0);
if (isset($_GET['from'])) {
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_GET['toDate']) != 1) {
        $_SESSION['msg1'] = "Invalid Report Request";
        echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employeeExpenseReport';
        </script>");
    }
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title">Ex Employee Report</h4>
            </div>

        </div>

        <form action="" method="get" class="">
            <div class="row pt-2 pb-2">
                <?php include('selectBranchDeptForFilter.php'); ?>

                <div class="col-lg-4 col-6 text-center ">
                    <label class="form-control-label"> </label>
                    <button type="submit" class="btn btn-sm btn-primary">Get Report</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            extract(array_map("test_input", $_GET));

                            if (isset($dId) && $dId > 0) {
                                $deptFilterQuery = " AND users_master.floor_id='$dId'";
                            }
                            if (isset($bId) && $bId > 0) {
                                $bFilterQuery = " AND users_master.block_id='$bId'";
                            }else
                            {
                                $limitQuery = " LIMIT 100";
                            }

                            $q2 = $d->selectRow("users_master.*,user_employment_details.*,floors_master.floor_id,floors_master.floor_name,block_master.block_id,block_master.block_name", "users_master LEFT JOIN floors_master ON floors_master.floor_id = users_master.floor_id LEFT JOIN block_master ON block_master.block_id = users_master.block_id LEFT JOIN user_employment_details ON user_employment_details.user_id = users_master.user_id", "users_master.delete_status=1 $deptFilterQuery $bFilterQuery  $blockAppendQueryUser $limitQuery");
                            $i = 1;
                            ?>
                            <table id="<?php if ($adminData['report_download_access'] == 0) {
                                            echo 'exampleReportWithoutBtn';
                                        } else {
                                            echo 'exampleReport';
                                        } ?>" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee</th>
                                        <th>Department</th>
                                        <th>Joining Date</th> 
                                        <th>Leaving Date</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($data = mysqli_fetch_array($q2)) {
                                       // print_r($data);
                                    ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo $data['user_full_name']; ?><?php if ($data['user_designation'] != "") { echo "(" . $data['user_designation'] . ")"; } ?> </td>
                                            <td><?php echo $data['floor_name']; ?> (<?php echo $data['block_name']; ?>) </td>
                                            <td><?php if ($data['joining_date'] != "" && $data['joining_date'] != "0000-00-00") echo date('d-M-Y', strtotime($data['joining_date'])); ?> </td>
                                            <td><?php if ($data['deleted_date'] != "" && $data['deleted_date'] != "0000-00-00") echo date('d-M-Y', strtotime($data['deleted_date'])); ?> </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->