<?php error_reporting(0);

$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$_GET['month_year'] = $_REQUEST['month_year'];

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row ">
      <div class="col-sm-6 col-12">
        <h4 class="page-title">Employee Promotion</h4>
      </div>
      <div class="col-sm-3 col-md-6 col-6">
        <div class="btn-group float-sm-right">
          <a href="javascript:void(0);" onclick="buttonSettingForAddPromotion()" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
          <a href="javascript:void(0)" onclick="DeleteAll('deletePromotions');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
      </div>
    </div>
    <form action="" class="branchDeptFilter">
      <div class="row ">
        <div class="col-md-3 form-group">
          <label class="form-control-label">Branch </label>
          <select name="bId" class="form-control single-select" required="" onchange="getVisitorFloorByBlockId(this.value)">
            <option value="">-- Select Branch --</option>
            <?php
            $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQuery");
            while ($blockData = mysqli_fetch_array($qb)) {
            ?>
              <option <?php if ($_GET['bId'] == $blockData['block_id']) {echo 'selected';} ?> value="<?php echo $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-3 form-group">
          <label class="form-control-label">Department </label>
          <select name="dId" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value)" required="">
            <option value="">-- Select Department --</option>
            <?php
            $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id = '$_GET[bId]' $blockAppendQueryFloor");
            while ($depaData = mysqli_fetch_array($qd)) {
            ?>
              <option <?php if ($_GET['dId'] == $depaData['floor_id']) {
                        echo 'selected';
                      } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name']; ?></option>
            <?php } ?>

          </select>
        </div>
        <div class="col-md-3 form-group" id="">
          <label class="form-control-label">Employee </label>
          <select id="user_id" type="text" class="form-control single-select " name="user_id">
            <option value="">Select Employee </option>
            <?php
            if (isset($_GET['dId']) && $_GET['dId'] > 0) {
              $userData = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND floor_id = $_GET[dId] $blockAppendQueryUser");
              while ($user = mysqli_fetch_array($userData)) {?>
                <option <?php if (isset($_GET['user_id']) && $_GET['user_id'] == $user['user_id']) { echo "selected";} ?> value="<?php if (isset($user['user_id']) && $user['user_id'] != "") {echo $user['user_id'];} ?>"> <?php if (isset($user['user_full_name']) && $user['user_full_name'] != "") {
                                              echo $user['user_full_name'];} ?></option>
            <?php }
            } ?>
          </select>
        </div>
        <!-- <div class="col-md-2 col-6 form-group">
          <label class="form-control-label">From Date </label>
          <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" name="from" value="<?php if (isset($_GET['from']) && $_GET['from'] != '') {echo $_GET['from'];} else { echo date('Y-m-01');} ?>">
        </div>
        <div class="col-md-2 col-6 form-group">
          <label class="form-control-label">To Date </label>
          <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerTo" name="toDate" value="<?php if (isset($_GET['toDate']) && $_GET['toDate'] != '') { echo $_GET['toDate'];} else { echo date('Y-m-t');} ?>">
        </div> -->
        <div class="col-md-1 text-center my-auto">
          <input class="btn btn-success btn-sm mt-2" type="submit" name="getReport" value="Get Data">
        </div>
      </div>
    </form>
    <div class="row mt-2">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <?php
              $i = 1;
              if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                $deptFilterQuery = " AND users_master.floor_id='$_GET[dId]'";
              }
              /* if (isset($_GET['from']) && isset($_GET['from']) && $_GET['toDate'] != '' && $_GET['toDate']) {
                $dateFilterQuery = " AND attendance_master.attendance_date_start BETWEEN '$_GET[from]' AND '$_GET[toDate]'";
              } */
              if (isset($_GET['user_id']) && $_GET['user_id'] > 0) {
                $userFilterQuery = "AND employee_history.user_id='$_GET[user_id]'";
              }
              $q = $d->selectRow("employee_history.*,users_master.*,floors_master.floor_id,floors_master.floor_name,block_master.block_name,block_master.block_id","employee_history LEFT JOIN users_master ON users_master.user_id=employee_history.user_id LEFT JOIN floors_master ON  floors_master.floor_id=users_master.floor_id LEFT JOIN  block_master ON block_master.block_id=users_master.block_id","employee_history.society_id=$society_id $deptFilterQuery $userFilterQuery");
              $counter = 1;
              
              ?>
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Sr.No</th>
                      <th>Action</th>
                      <th>Employee</th>
                      <th>Department</th>
                      <th>designation</th>
                      <th>joining date</th>
                      <th>End date</th>
                      <th>remark</th>
                      <th>Salary Increment</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    while ($data = mysqli_fetch_array($q)) {
                    ?>
                      <tr>
                        <td> 
                            <input type="hidden" name="id" id="id"  value="<?php echo $data['history_id']; ?>">
                            <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['history_id']; ?>">
                        </td>
                        <td><?php echo $counter++; ?></td>
                        <td>
                            <div class="d-flex align-items-center">
                            
                            <button onclick="getPromotionDetail( <?php echo $data['history_id'];?>)" class="btn btn-primary btn-sm ml-1"><i class="fa fa-eye" ></i></button>
                        
                            </div>
                        </td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php echo $data['floor_name']."(".$data['block_name'].")"; ?></td>
                        <td><?php echo $data['designation']; ?></td>
                        <td><?php if($data['joining_date'] !="0000-00-00"){ echo date('d-M-Y',strtotime($data['joining_date'])); }?></td>
                        <td><?php if($data['end_date'] !="0000-00-00"){ echo date('d-M-Y',strtotime($data['end_date'])); }?></td>
                        <td><?php custom_echo($data['remark'],30) ?></td>
                        <td><?php if($data['salary_incrment'] ==0){ echo "Yes"; } else {echo "No"; }?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              <?php // } else {  ?>
                
              <?php //} ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Row-->

  </div>
  <!-- End container-fluid-->
</div>


</div>


<div class="modal fade" id="addCurrentExperience">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add . " " . $xml->string->experience; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="">
         <form id="addNewExperienceForm" class="validateForm" action="controller/userController.php" method="post" enctype="multipart/form-data">
            <div class="col-md-12">
                <input type="hidden" required="" autocomplete="off" name="add_promotions" value="add_promotions">
                <input type="hidden" required="" autocomplete="off" name="joining_date_validate" id="joining_date_validate">
                <input type="hidden" required="" autocomplete="off" name="society_id" value="<?php echo $_COOKIE['society_id'] ?>" >
                
                <div class="row">
                    <label class="col-sm-3 text-uppercase">Branch <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <select name="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" >
                            <option value="">-- Select Branch --</option> 
                            <?php 
                            $qbr=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                            while ($blockDataS=mysqli_fetch_array($qbr)) {
                            ?>
                            <option  value="<?php echo  $blockDataS['block_id'];?>" ><?php echo $blockDataS['block_name'];?></option>
                            <?php } ?>

                        </select>         
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 text-uppercase">Department <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <select name="floor_id" id="dId" class="form-control single-select" onchange="getUserByFloorId(this.value);" >
                            <option value="">--Select Department --</option> 
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 text-uppercase">Employee <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <select name="user_id" id="uId" class="form-control single-select">
                            <option value="">-- Select Employee --</option> 
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo $xml->string->designation; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input type="text" required="" autocomplete="off" name="designation"  class="form-control">
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-3 text-uppercase"><?php echo "Work From"; ?> <span class="required">*</span></label>
                    <div class="form-group col-sm-9">
                        <input readonly="" class="form-control" id="new_datepicker-wf" autocomplete="off" name="joining_date" id="joining_date" type="text">
                        <input readonly="" class="form-control" id="new_datepicker-wf" autocomplete="off" name="joining_date_v" value="<?php if($proData['joining_date'] !="0000-00-00"){echo $proData['joining_date'];} else{ echo date('Y-m-d'); } ?>" id="joining_date_v" type="hidden">
                    </div>
                  </div>
                </div> 
                <!-- <div class="col-md-12">
                    <div class="row">
                        <label class="col-sm-3 text-uppercase"><?php echo "Work TO"; ?> <span class="required">*</span></label>
                        <div class="form-group col-sm-9">
                           <input readonly="" class="form-control" id="new_datepicker-wt" autocomplete="off" name="end_date" type="text">
                        </div>
                    </div>
                </div> -->
                <div class="col-md-12">
                    <div class="row">
                    <label class="col-sm-6 text-uppercase">Salary Increment<span class="required">*</span></label>
                      <input type="radio" name="salary_incrment" value="0">
                        <label class="col-sm-2 text-uppercase">Yes</label>
                      <input type="radio" checked name="salary_incrment" value="1">
                        <label class="col-sm-2 text-uppercase">No</label>
                       
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <label class="col-sm-3 text-uppercase">Remark </label>
                        <div class="form-group col-sm-9">
                            <textarea  class="form-control" autocomplete="off" name="remark"></textarea>
                        </div>
                    </div>
                </div>
            <div class="form-footer text-center">
                <button type="submit" class="btn btn-success"><?php echo $xml->string->save; ?> </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="PromotionModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->add . " " . $xml->string->experience; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="">
        <div class="PromotionModalData">
        </div>
      </div>
    </div>
  </div>
</div>


<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">


function buttonSettingForAddPromotion(params) {
    $('#addCurrentExperience').modal();
}
  
$(function() {
  $('#uId').change(function() {
        var id = $('select#uId option:selected').attr('data-date');
        $('#new_datepicker-wf').datepicker('destroy');
        $( "#new_datepicker-wf" ).datepicker({
          startDate: id,
          format: 'yyyy-mm-dd',
          
        })
        
     });
});

</script>
