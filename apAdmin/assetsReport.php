<?php 
// error_reporting(0);
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$id = (int)$_GET['id'];

 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Assets Report</h4>
       
        </div>
     
     </div>

    <form action="" method="get" class="branchDeptFilter">
      <div class="row pt-2 pb-2">
          <?php include 'selectBranchDeptForFilterAll.php' ?>
          <div class="col-md-3 form-group">
			      <select type="text" name="id" class="form-control single-select" >
              <option value="">All Category</option>
              <?php $q12 = $d->select("assets_category_master", "", "order by assets_category ASC");
              while ($row12 = mysqli_fetch_array($q12)) { ?>
                <option value="<?php echo $row12['assets_category_id']; ?>" <?php if ($id == $row12['assets_category_id']) { echo "selected"; } ?>><?php echo $row12['assets_category']; ?> </option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-3 form-group">
            <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
          </div> 
      </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                // extract(array_map("test_input" , $_GET));
               
				    if (isset($id) && $id > 0 && filter_var($id, FILTER_VALIDATE_INT) == true) {
					    $appendQery = " AND assets_item_detail_master.assets_category_id='$id'";
				    }
            if (isset($dId) && $dId > 0 && filter_var($dId, FILTER_VALIDATE_INT) == true) {
					    $deptAppendQery = " AND users_master.floor_id='$dId'";
				    }
            
             if(isset($bId) && $bId>0) {
                        $blockFilterQuery = " AND users_master.block_id='$bId'";
                      }
                  
                $q=$d->select("assets_category_master,assets_item_detail_master LEFT JOIN assets_detail_inventory_master ON assets_detail_inventory_master.assets_id = assets_item_detail_master.assets_id AND assets_detail_inventory_master.end_date='0000-00-00' LEFT JOIN users_master ON users_master.user_id =assets_detail_inventory_master.user_id","assets_item_detail_master.assets_category_id=assets_category_master.assets_category_id AND assets_item_detail_master.society_id='$society_id' AND assets_item_detail_master.move_to_scrap=0 $appendQery $blockFilterQuery $deptAppendQery","");
                    
                $i=1;
                
               ?>
              <div class="row">
                <div class="col-md-3 pl-3">
                  Total Assets Item:  <?php 
                     echo mysqli_num_rows($q);
                   ?>
                </div>
                <div class="col-md-3 pl-3">
                  Total Assets Price:  <?php 
                     $pq=$d->selectRow("SUM(assets_item_detail_master.item_price) AS item_price","assets_item_detail_master LEFT JOIN assets_detail_inventory_master ON assets_detail_inventory_master.assets_id = assets_item_detail_master.assets_id AND assets_detail_inventory_master.end_date='0000-00-00' LEFT JOIN users_master ON users_master.user_id =assets_detail_inventory_master.user_id","assets_item_detail_master.society_id='$society_id' AND assets_item_detail_master.move_to_scrap=0 $appendQery $blockFilterQuery $deptAppendQery");
                     $PriceData=mysqli_fetch_array($pq);
                     echo number_format($PriceData['item_price'],2,'.','');
                   ?>
                </div>
              </div>
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Category</th>
                        <th>Item Name</th>
                        <th>Brand </th>
                        <th>Item Image</th>
                        <th>Purchase Date</th>
                        <th>Current Custodian</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q)) {
                    // echo '<pre>';
                    // print_r($data);
                   ?>
                     <tr>
                        <td><?php echo $i++; ?> </td>
                        <td><?php echo $data['assets_category']; ?> </td>
                        <td><?php echo $data['assets_name']; ?> </td>
                        <td><?php echo $data['assets_brand_name']; ?> </td>
                        <td class="text-center align-middle">
                        <?php if($data['assets_file'] != ''){ 
                          if(file_exists("../img/society/$data[assets_file]")) {
                          ?>
                          <a data-fancybox="images" data-caption="Photo Name : <?php echo $data['assets_file'] ?>" href="../img/society/<?php echo $data['assets_file'] ?>" target="_blank">
                            <img style="max-height:100px; max-width:100px;" class="lazyload" onerror="this.src='../img/banner_placeholder.png'" src='../img/banner_placeholder.png' id="<?php echo $data['assets_id']; ?>" data-src="../img/society/<?php echo $data['assets_file']; ?>"></a>
                            <?php } } ?>
                        </td>
                        <td>
                          <?php if ($data['item_purchase_date'] != "0000-00-00") {
                            echo $data['item_purchase_date'];
                          } ?>
                        </td>
                        <td>
                          <?php 
                            echo $data['user_full_name']; 
                            ?>
                        </td>
                        <td><?php if($data['item_price']>0) { echo number_format($data['item_price'],2,'.',''); } ?> </td>
                      </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->