<?php 
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

if (isset($_COOKIE['bms_admin_id'])) {
    $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
extract(array_map("test_input" , $_POST));
if($block_id==0) {
	$i=1;
	$fq=$d->select("unit_master,block_master","unit_master.block_id=block_master.block_id AND block_master.society_id='$society_id' AND unit_master.unit_status!=0","ORDER BY unit_master.unit_id ASC");
	if(mysqli_num_rows($fq)>0) { 
        while ($floorData=mysqli_fetch_array($fq)) {  ?>
        	<input value="<?php echo $floorData['unit_id']; ?>" id="unit_id<?php echo $i++; ?>"   type="radio" name="unit_id[]"> <?php echo $floorData['block_name']; ?>-<?php echo $floorData['unit_name']; ?>
        	<br>
    <?php    }
    } else {
    	echo "No Unit Found";
    }


} else {
	$fq=$d->select("unit_master,block_master","unit_master.block_id=block_master.block_id AND  unit_master.society_id='$society_id' AND unit_master.block_id='$block_id' AND unit_master.unit_status!=0"."ORDER BY unit_master.unit_id ASC");
	$i=1;
	if(mysqli_num_rows($fq)>0) { 
        while ($floorData=mysqli_fetch_array($fq)) { ?>
        	<input value="<?php echo $floorData['unit_id']; ?>" id="unit_id<?php echo $i++; ?>"  type="radio" name="unit_id[]"> <?php echo $floorData['block_name']; ?>-<?php echo $floorData['unit_name']; ?>
        	<br>
    <?php   }
    } else {
    	echo "No Unit Found";
    }
}


?>
<input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
  <div class="form-footer text-center">
    <button type="button" onclick="getFloorIds();" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
</div>
