<?php 
session_start();
$society_id=$_COOKIE['society_id'];
include 'common/object.php';
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
$today=date("Y-m-d");
$cTime=date("H:i:s");
$checS=$d->select("society_master","society_id='$society_id'");
$sData=mysqli_fetch_array($checS);
$currency= $sData['currency'];
extract(array_map("test_input" , $_POST));
$q=$d->select("facilities_master"," facility_id='$facility_id'","");
$row=mysqli_fetch_array($q);
extract($row);
$uq = $d->selectRow("user_type","users_master","user_id='$user_id'");
$userData=mysqli_fetch_array($uq);
$user_type = $userData['user_type'];
if($user_type==1 && $facility_amount_tenant>0) {
   $facility_amount = $facility_amount_tenant;
 }
 if($amount_type==0) {
       $facility_amount = ($facility_amount * $no_of_person);
 } 

// echo $person_limit;
 $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
 $weekday = date('l', strtotime($book_date)); // note: first arg to date() is lower-case L 
 $key = array_search($weekday, $dayName); // $key = 2;
 $qt=$d->select("facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$key'","");
 $timData=mysqli_fetch_array($qt);
 if ($timData['facility_status']=="1" && $facility_type!=1) {
 	echo "<label class='col-sm-4 col-form-label'>Booking Time<span class='text-danger'>*</span></label>";
 	echo "<div class='col-sm-8 text-danger'> <b>This Facility Close on $weekday</b></div>";
 	echo ("<script LANGUAGE='JavaScript'>
		$('#bookingButton').hide();
		</script>");
 } else {
 $bookedAray = array();

// print_r($bookedAray);
?>


<input type="hidden" value="<?php echo $facility_type; ?>" id='facilityType'>
<input type="hidden" name="user_type" value="<?php echo $user_type; ?>" class='user_type' id='user_type'>
<input type="hidden" value="<?php echo $amount_type; ?>" id='amount_type'>
<?php if ($facility_type==1) { ?>
<label for="no_of_month" class=" col-sm-4 col-form-label"> No. Of Month  <span class="text-danger">*</span></label>
<div class="col-sm-8">
	<select onchange="getMonthList();" class="form-control booked_end_date" id="booked_end_date" name="no_of_month" required="">
		<option value="">-- Select --</option>
		<?php for ($i=1; $i <= 12 ; $i++) { ?> 
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
		<?php } ?>
	</select>
</div>
<?php } else { ?>
<label for="no_of_month" class=" col-sm-4 col-form-label">Booking Time Slot<span class="text-danger">*</span></label>
<div class="col-sm-8">
	<?php  $sq = $d->select("facility_schedule_master","facility_start_time!='00:00:00' AND facility_id='$facility_id' AND facility_status=0  AND facility_day_id='$key'","");
      if(mysqli_num_rows($sq)>0) {
        while ($sData= mysqli_fetch_array($sq)) {
        $time1 = strtotime($sData["facility_start_time"]);
        $time2 = strtotime($sData["facility_end_time"]);
        $difference_hours = abs($time2 - $time1) / 3600;
	       if($difference_hours <1) {
	         $difference_hours =1;
	       }
        if($facility_type==3){
         $fAmount = $facility_amount * $difference_hours;
        } else {
        	$fAmount = $facility_amount;
        }
        // check Available
        $count5=$d->sum_data("no_of_person","facility_booking_months","cancel_status=0 AND facility_id='$facility_id'  AND facility_schedule_id='$sData[facility_schedule_id]' AND booking_start='$book_date'");
    	$row11=mysqli_fetch_array($count5);
       	$booke_count=$row11['SUM(no_of_person)'];
       	if ($booke_count=='') {
       		$booke_count =0;
       	}
        $avSeat = $person_limit - $booke_count;
     

         ?>
	 <input   <?php if ($avSeat<$no_of_person && $amount_type==0 ||  $amount_type==1 && $booke_count>0) { echo 'disabled';} ?> class="facility_schedule_id" title="<?php echo number_format($fAmount,2,'.','');  ?>" type="checkbox" name="facility_schedule_id[]" value="<?php echo $sData['facility_schedule_id']; ?>"> <?php echo date('h:i A',strtotime($sData["facility_start_time"])).' to '.date('h:i A',strtotime($sData["facility_end_time"])); ?> (<?php echo  $currency.' '.number_format($fAmount,2,'.','');  ?>) <?php if ($amount_type==0) { echo '('.$booke_count.'/'.$person_limit;?>) <?php if ($avSeat<$no_of_person && $avSeat>0) { echo "<i class='text-danger'>".$avSeat. " Available</i>";} else  if ($avSeat<$no_of_person && $avSeat<1) { echo "<i class='text-danger'>Sold Out</i>";} } else if($amount_type==1 && $booke_count>0) { echo "<i class='text-danger'> Booked</i>";} ?><br>
	<?php }  } else {
		echo "No Time Slot Available For This Day";
	} ?>
	<!-- <input type="text" readonly class="form-control timepicker" value=""  name="booking_start_time_days" placeholder="From"> -->
</div>
<?php } ?>	
<!-- <script src="assets/js/jquery.min.js"></script> -->

<script type="text/javascript">

	$(document).ready(function() {
	  	// var basePrice = parseInt($(".price_field").val(),10);
	  	var facilityType = $("#facilityType").val();
	  	var basePrice = parseFloat(0);
	  	// alert(facilityType);
		$(".facility_schedule_id").change(function() {
		    var newPrice = basePrice;
		    $('.facility_schedule_id:checked').each(function() {
		    	// alert($(this).attr('title'));
			    if(facilityType==3 || facilityType==4) {
			        newPrice += parseFloat($(this).attr('title'),10);
			    } else {
			    	 newPrice = parseFloat($(this).attr('title'),10);
			    }
		    });
		    // console.log(newPrice);
		    $('.facility_amount').val(newPrice.toFixed(2));
		    $('.facility_amount_org_wallet').val(newPrice.toFixed(2));
		    $(".facility_amount").attr({"min" : newPrice.toFixed(2) });
		    if(newPrice>0) {
		    	$("#bookingButton").show();
		    } else {
		    	$("#bookingButton").hide();
		    }
		});


		
	});

	
	
	
	function checkAvailiity() {

		var no_of_person= parseInt($('#no_of_person').val());
	    var amount_type= parseInt($('#amount_type').val());
	    var user_type= parseInt($('#user_type').val());
		var booking_start_time_days= $('#booking_start_time_days').val();
	    var booking_end_time_days= $('#booking_end_time_days').val();
	    var book_date= $('.facility_datepicker').val();
	    var facility_id= $('#facility_id').val();
	    var facility_type= $('#facility_type').val();
	    if(booking_end_time_days!='' && booking_start_time_days!='') {
	    	$.ajax({
	    	
    			url: 'getFacilitAvailiblity.php',
    			type: 'POST',
    			data: {booking_start_time_days: booking_start_time_days,booking_end_time_days:booking_end_time_days,facility_id : facility_id, facility_type : facility_type,book_date:book_date,no_of_person:no_of_person},
    		})
    		.done(function(response) {
    			$('.noOfpersonDiv').html(response);
		        
		    });
	    } else {
    			$('.noOfpersonDiv').html(' ');

	    }

	}
	


	function getMonthList() {
		// var booking_start_time_day= $('#booking_start_time_days').val();
	    var user_type= $('.user_type').val();
		var booked_end_date= $('.booked_end_date').val();
	    var facility_amount= parseFloat($('#facility_amount').val());
	    var book_date= $('.facility_datepicker').val();
	    var facility_id= $('#facility_id').val();
	    var facility_type= $('#facility_type').val();
	    var no_of_person= $('#no_of_person').val();
	    if(book_date!='' && booked_end_date!='') {
	    $.ajax({
    			url: 'getFacilitMonthList.php',
    			type: 'POST',
    			data: {user_type:user_type,booked_end_date: booked_end_date,facility_id : facility_id, facility_type : facility_type,book_date:book_date,no_of_person:no_of_person},
    		})
    		.done(function(response) {
    			$('.mothListDiv').html(response);
		        
		    });
	    } else {
    			$('.mothListDiv').html(' ');

	    }
	}


</script>

<?php } ?>

  
</body>

</html>
   