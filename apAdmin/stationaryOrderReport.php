<?php 
error_reporting(0);
$status = $_REQUEST['status'];
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employeeExpenseReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Stationery Order Report</h4>
        </div>
     
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Department </label>
            <select name="floor_id"  class="form-control single-select" onchange="getVisitorUserByFloor(this.value);">
                <option value="">All Department</option> 
                  <?php 
                    $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id");  
                    while ($depaData=mysqli_fetch_array($qd)) {
                  ?>
                <option  <?php if($_GET['floor_id']==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name']; ?>)</option>
                <?php } ?>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Employer </label>
            <select name="user_id" id="user_id" class="form-control single-select">
                <option value="">All Employer</option> 
                <?php 
                    $user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND floor_id='$_GET[floor_id]'");  
                    while ($userdata=mysqli_fetch_array($user)) {
                ?>
                <option <?php if($_GET['user_id']==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
                <?php } ?>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Status </label>
            <select name="status"  class="form-control">
                <option value="">All</option>
                <option <?php if($status=="0") { echo 'selected';} ?> value="0">Placed</option>
                <option <?php if($status=="1") { echo 'selected';} ?> value="1">Preparing</option>
                <option <?php if($status=="2") { echo 'selected';} ?> value="2">Despatched</option>
                <option <?php if($status=="3") { echo 'selected';} ?> value="3">Delivered</option>
                <option <?php if($status=="4") { echo 'selected';} ?> value="4">Cancelled</option></select>
          </div>
         
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
          

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                extract(array_map("test_input" , $_GET));
                // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                 $date=date_create($from);
                $dateTo=date_create($toDate);
                $nFrom= date_format($date,"Y-m-d 00:00:00");
                $nTo= date_format($dateTo,"Y-m-d 23:59:59");
                if(isset($_GET['floor_id']) && $_GET['floor_id'] > 0) {
                    $deptFilterQuery = " AND users_master.floor_id='$_GET[floor_id]'";
                }
                if(isset($_GET['user_id']) && $_GET['user_id'] > 0) {
                    $userFilterQuery = " AND order_master.user_id='$_GET[user_id]'";
                }
                if(isset($status) && $status !='') {
                    $statusFilterQuery = " AND order_master.order_status='$status'";
                   }
               
                $q=$d->select("order_master,users_master,floors_master,block_master","users_master.floor_id=floors_master.floor_id AND block_master.block_id=users_master.block_id AND order_master.user_id=users_master.user_id AND order_master.society_id='$society_id' AND order_master.vendor_category_id = 2 AND order_master.order_date BETWEEN '$nFrom' AND '$nTo' $statusFilterQuery $deptFilterQuery $userFilterQuery");
                $i=1;
                if (isset($_GET['from'])) {
               ?>
                
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Employee</th>
                        <th>Department</th>
                        <th>Order NO</th>
                        <th>Total Amount</th>
                        <th>Date</th>
                        <th>Status</th>  

                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                       
                    <td><?php echo $i++; ?></td>
                      <td><?php echo $data['user_full_name']; ?></td>
                      <td><?php echo $data['floor_name']; ?></td>
                      <td><?php echo $data['order_no']; ?></td>
                      <td><?php echo $data['order_total_amount']; ?></td>
                      <td><?php echo date("d M Y h:i A", strtotime($data['order_date'])); ?></td>
                      <?php 
                        if($data['order_status'] == 0){
                            $status = "Placed";
                            $statusClass = "text-primary";
                        }
                        else if($data['order_status'] == 1){
                            $status = "Preparing";
                            $statusClass = "text-info";
                        }
                        else if($data['order_status'] == 2){
                            $status = "Despatched";
                            $statusClass = "text-secondary";
                        }
                        else if($data['order_status'] == 3){
                            $status = "Delivered";
                            $statusClass = "text-success";
                        }
                        else{
                            $status = "Cancelled";
                            $statusClass = "text-warning";
                        }
                      ?>
                      <td><b><span class="<?php echo $statusClass; ?>"><?php echo $status; ?></span></b></td>
                      
                        
                       
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            <?php } else {  ?>
              <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->