<?php
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$cId = (int)$_POST['cId'];
$society_id = $_COOKIE['society_id'];
?>
<div class="form-group row ">
    <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Lesson Title<span class="required">*</span></label>
    <div class="col-lg-8 col-md-8" id="">
        <input type="text" value="<?php if (isset($data['lesson_title']) && $data['lesson_title'] != "") {echo $data['lesson_title'];} ?>" name="lesson_title" id="lesson_title" class="form-control rstFrm">
    </div>
</div>
<div class="form-group row">
    <label for="input-10" class="col-sm-4 col-form-label">Lesson Type <span class="required">*</span></label>
    <div class="col-lg-8 col-md-8" id="">
        <select type="text" id="lesson_type_id" required="" class="form-control inputSl single-select " name="lesson_type_id">
            <option value="">-- Select --</option>
            <?php
            $course = $d->select("lesson_type_master", "is_deleted=0 AND is_active=0");
            while ($courseData = mysqli_fetch_array($course)) {
            ?>
            <option <?php if (isset($data['lesson_type_id']) && $data['lesson_type_id'] == $courseData['lesson_type_id']) { echo "selected";} ?> data-name="<?php echo $courseData['lesson_type']; ?>" value="<?php if (isset($courseData['lesson_type_id']) && $courseData['lesson_type_id'] != "") {echo $courseData['lesson_type_id'];} ?>"> <?php if (isset($courseData['lesson_type_id']) && $courseData['lesson_type_id'] != "") {echo $courseData['lesson_type'];} ?>
            </option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row lessonInput">
    <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Lesson File <span class="required">*</span></label>
    <div class="col-lg-8 col-md-8" id="">
        <input type="file" name="lesson_video" accept="audio/*" id="lesson_video" class=" rstFrm form-control-file border ">
        <input type="hidden" class="rstFrm" name="lesson_video_old" id="lesson_video_old" value="<?php if (isset($data['lesson_video']) && $data['lesson_video'] != "") {echo $data['lesson_video'];} ?>">
    </div>
</div>
<div class="showYoutubeinput">
    <div class="form-group row ">
        <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Link <span class="required">*</span></label>
        <div class="col-lg-8 col-md-8" id="">
            <input type="text" value="<?php if (isset($data['lesson_video_link']) && $data['lesson_video_link'] != "") {echo $data['lesson_video_link'];} ?>" name="lesson_video_link" id="lesson_video_link" class="form-control rstFrm ">
        </div>
    </div>
</div>
<div class="inoutForText">
    <div class="form-group row ">
        <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Text <span class="required">*</span></label>
        <div class="col-lg-8 col-md-8" id="">
            <textarea required="" id="CoursesummernoteImgage" class="lesson_text" name="lesson_text"><?php echo $data['lesson_text']; ?></textarea>
        </div>
    </div>
</div>
<input type="hidden" id="course_chapter_id" required="" value="" name="course_chapter_id" class="cpId">
<div class="form-footer text-center">
    <input type="hidden" id="lessons_id" name="lessons_id" value="<?php echo $lsId;  ?>" class="rstFrm">
    <button id="addCourseLessonBtn" type="submit" class="btn btn-success hideupdate course_lesson_button"><i class="fa fa-check-square-o"></i><?php if (isset($lsId) && $lsId > 0) {echo "Add";} else {echo "Update";} ?> </button>
    <button type="submit" class="btn btn-success hideAdd course_lesson_button"><i class="fa fa-check-square-o"></i><?php if (isset($lsId) && $lsId > 0) { echo "Add";} else { echo "Add"; } ?> </button>
    <input type="hidden" name="addCourseLesson" value="addCourseLesson">
    <button type="button" value="add" class="btn btn-danger " onclick="resetLessonForm()"><i class="fa fa-check-square-o"></i> Reset</button>
</div>


<script type="text/javascript" src="assets/js/select.js"></script>