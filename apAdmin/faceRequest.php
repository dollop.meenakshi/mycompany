<?php error_reporting(0);
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$uId = (int)$_REQUEST['uId'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2">
            <div class="col-md-12">
                <h4 class="page-title">User Face Data Request</h4>
            </div>
        </div>
        <form action="" method="get" class="branchDeptFilter">
            <div class="row">
                <?php include 'selectBranchDeptForFilterAll.php' ?>
                <div class="col-md-4 form-group">
                    <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $i = 1;
                            if (isset($_GET['bId']) && $_GET['bId'] > 0) {
                                $blockFilterQuery = " AND users_master.block_id='$_GET[bId]'";
                            }
                            if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                                $deptFilterQuery = " AND users_master.floor_id='$_GET[dId]'";
                            }
                            

                            $q = $d->select("users_master,block_master,floors_master,user_face_data_request", "block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND user_face_data_request.user_id=users_master.user_id AND user_face_data_request.society_id='$society_id' AND users_master.delete_status=0 $blockFilterQuery $deptFilterQuery $blockAppendQueryUser");
                            $counter = 1;
                            ?>
                                <table id="example" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>User Name</th>
                                            <th>Designation</th>
                                            <th>Date</th>
                                            <th>Photo</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    while ($data = mysqli_fetch_array($q)) {
                                    ?>
                                        <tr>
                                            <td><?php echo $counter++; ?></td>
                                            <td><?php echo $data['user_full_name']; ?></td>
                                            <td><?php echo $data['user_designation']; ?></td>
                                            <td><?php echo date("d M Y ", strtotime($data['face_request_date'])) ?></td>
                                            <td><?php if ($data['face_data_image']!='') { ?>
                                                <a href="../img/attendance_face_image/<?php echo $data['face_data_image']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["face_data_image"]; ?>"><img width="50" height="50" onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png"  src="../img/ajax-loader.gif" data-src="../img/attendance_face_image/<?php echo $data['face_data_image']; ?>"  href="#divForm<?php echo $data['user_id'];?>" class="btnForm lazyload" ></a>

                                                <a href="../img/attendance_face_image/<?php echo $data['face_data_image_two']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["face_data_image_two"]; ?>"><img width="50" height="50" onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png"  src="../img/ajax-loader.gif" data-src="../img/attendance_face_image/<?php echo $data['face_data_image_two']; ?>"  href="#divForm<?php echo $data['user_id'];?>" class="btnForm lazyload" ></a>
                                                <?php } ?>
                                            </td>
                                            <td><?php if ($data['user_face_data_request_id'] != '' && $data['user_face_data_request_id'] != '') { ?>
                                                    <a href="javascript:void(0)" onclick="ShowUserFaceRequest(<?php echo $data['user_face_data_request_id']; ?>);" class="btn btn-sm btn-danger"><i class="fa fa-eye fa-lg"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

</div>

<div class="modal fade" id="UserFaceRequest">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Face Request Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="align-content: center;">
                <div class="card-body">

                    <div class="row col-md-12" id="UserFaceRequestData">
                    </div>
                    <div class=" col-md-12" >
                    <form id="faceRequest" name="faceRequest" action="controller/FaceRequestController.php"  enctype="multipart/form-data" method="post">
                        <input type="hidden" name="user_face_data_id" id="user_face_data_id" >
                        <div class="form-group row" >
                            <label for="input-10" class="col-sm-6 col-form-label">Face Request Status <span class="text-danger">*</span></label>
                            <div class="col-lg-6 col-md-6" id="">
                                <select name="face_reuqest_status" onchange="changeFaceRequestStatusSelect(this.value)" readonly id="face_reuqest_status" class="form-control single-select">
                                <option value="">Select Status</option>
                                <option value="approved">Approved</option>
                                <option value="Reject">Reject</option>
                                </select>                 
                            </div>     
                        </div> 
                        <div class="form-group row decline_reason" >
                            <label for="input-10" class="col-sm-6 col-form-label">Decline Reason <span class="text-danger">*</span></label>
                            <div class="col-lg-12 col-md-12" id="">
                                <textarea class="form-control" name="decline_reason"></textarea>               
                            </div>     
                        </div> 
                        <div class="form-group row " >
                            <div class="col-md-12 col-lg-12 text-center">
                                <input type="hidden" name="chnageFaceRequestStatus" value="chnageFaceRequestStatus">
                                <input type="hidden" name="bId_vlaue" value="<?php echo $_GET['bId'] ;?>">
                                <input type="hidden" name="dId_vlaue" value="<?php echo $_GET['dId'] ;?>">
                                <button class="btn btn-primary" onclick="chnageFaceRequestStatus()">Submit</button>
                            </div>
                        </div>
                    </form> 
                    </div>       
                </div>
            </div>

        </div>
    </div>
</div>
<input type="hidden" name="dptIdOld" id="dptIdOld" value="<?php if (isset($dId) && $dId != "") {
                                                                echo $dId;
                                                            } ?>">
<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">

    function changeFaceRequestStatusSelect(value)
    {
       
        if(value=="approved")
        {
            $('.decline_reason').hide();
        }
        else
        {
            $('.decline_reason').show();
        }
    }
    <?php if (isset($bId) && $bId > 0) { ?>
        getFloorByBranchId(<?php echo $bId; ?>);
    <?php }
    ?>

    function getFloorByBranchId(id) {
        dptIdOld = $('#dptIdOld').val();
        var optionContent = "";
        $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action: "getFloorByBlockId",
                block_id: id,
            },
            success: function(response) {
                
                optionContent = `<option value="0">All Departments</option>`;
                $.each(response.floor, function(index, value) {
                    if (dptIdOld == value.floor_id) {
                        selected = "selected";
                    } else {
                        selected = "";

                    }
                    optionContent += `<option ` + selected + ` value="` + value.floor_id + `" >` + value.floor_name + ` (` + value.block_name + `)</option>`;
                });
                $('#dptId').html(optionContent);
            }
        });
    }

    function popitup(url) {
        newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    }
</script>
<style>
    .decline_reason{
        display: none;
    }
</style>