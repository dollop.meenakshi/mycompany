<?php
extract(array_map("test_input", $_POST));
$block_id = $_POST['block_id'];
$q = $d->selectRow("*","block_master" ,"society_id='$society_id' AND block_id = '$block_id' $blockAppendQuery"); 
$data=mysqli_fetch_array($q);
$block_name = $data['block_name'];
$accessBlockIds=$data['access_block_ids'];
$access_block_ids=explode(",", $accessBlockIds);
?>
<?php if(isset($block_id)){ ?>
<div class="content-wrapper">
    <div class="container-fluid">
      	<!-- Breadcrumb-->
     	<div class="row pt-2 pb-2">
        	<div class="col-sm-9">
        		<h4 class="page-title"> Add Branch Restrict</h4>
     		</div>
			<div class="col-sm-3">

			</div>
     	</div>
    	<!-- End Breadcrumb-->

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">  
						<form id="addLeaveAssignBulk" action="controller/BranchAndDepartmentAccessController.php" enctype="multipart/form-data" method="post">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row w-100 mx-0">
										<label for="input-10" class="col-lg-2 col-md-2 col-form-label">Branch Access For<span class="required">*</span></label>
										<div class="col-lg-4 col-md-4 col-12" id="">
											<input type="text" value="<?php echo $block_name; ?>" class="form-control" disabled>
											<input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
										</div>                   
									</div> 
								</div>  
								<div class="col-md-12 text-center">
									<div class="form-group row w-100 mx-0">
										<div class="col-sm-12">
											<input type="checkbox" id="user-checkbox" class="selectAll" />
											<label for="user-checkbox"><?php echo $xml->string->check_uncheck; ?> </label>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group row w-100 mx-0">
										<?php 
										$q = $d->select("block_master" ,"society_id='$society_id' AND block_id != '$block_id' $blockAppendQuery","ORDER BY block_sort ASC"); 
										//$data=mysqli_fetch_array($q);
										while($data=mysqli_fetch_array($q)){
											$blockId=$data['block_id'];?>
										<div class="col-lg-3 col-md-3 col-12" id="">
											<label>
												<input <?php if(in_array($blockId, $access_block_ids)){ echo "checked"; } ?> type="checkbox" class="multiDelteCheckbox" value="<?php echo $data['block_id'] ?>" name="access_block_ids[]">
												<span><b><?php echo $data['block_name'] ?></b></span>
											</label>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="form-footer text-center">
								<?php if(isset($accessBlockIds) && $accessBlockIds == '') { ?>
									<button id="addBranchAccessBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
								<?php } else { ?>
									<button id="addBranchAccessBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> UPDATE</button>
								<?php } ?>
								<input type="hidden" name="addBranchAccess"  value="addBranchAccess">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><!--End Row-->

    </div>
    <!-- End container-fluid-->

</div><!--End content-wrapper-->
<?php } ?>
