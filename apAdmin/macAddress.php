<?php error_reporting(0);
$uId = (int)$_REQUEST['uId'];
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$status = (int)$_REQUEST['status'];

?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12 col-md-12 col-12">
          <h4 class="page-title">Mobile Device Bind Request</h4>
        </div>
     </div>
     <form action="" class="branchDeptFilter">
        <div class="row ">
          <?php include('selectBranchDeptEmpForFilterAll.php');?>
          <div class="col-md-2 form-group ">
                <select name="status" class="form-group single-select">
                  <option  <?php if($status==0) { echo 'selected'; } ?> value="0"> Pending</option> 
                  <option  <?php if($status==1) { echo 'selected'; } ?> value="1"> Approved</option> 
                </select> 
           </div>
          <div class="col-md-1 form-group ">
                <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
           </div>
        </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>    
                         <th>Action</th>                    
                        <th>Name</th>
                        <th>Branch (Department)</th>
                        <th>MAC </th>
                        <th>Device</th>
                        <th> Modal</th>
                        <th>Date</th>
                        <th>Reason</th>
                        <th>Change By</th>                      
                       
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 

                        if(isset($uId) && $uId>0){
                          $userFilterQuery = " AND user_mac_address_master.user_id='$uId'";
                        }
                         if(isset($status) && $status>0){
                           $statusFilter = " AND user_mac_address_master.mac_address_change_status='$status'";
                        } else {
                           $statusFilter = " AND user_mac_address_master.mac_address_change_status='0'";
                        }

                        if(isset($dId) && $dId>0){
                          $dptFilterQuery = " AND users_master.floor_id='$dId'";
                        }
                        if(isset($bId) && $bId>0){
                          $blkFilterQuery = " AND users_master.block_id='$bId'";
                        }
                        $q=$d->selectRow("user_mac_address_master.*, users_master.user_full_name,users_master.user_mac_address,u.user_full_name AS status_change_by,bms_admin_master.admin_name","users_master,user_mac_address_master LEFT JOIN users_master AS u ON u.user_id=user_mac_address_master.mac_address_status_change_by LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=user_mac_address_master.mac_address_status_change_by","user_mac_address_master.user_id=users_master.user_id AND user_mac_address_master.society_id='$society_id' $statusFilter  $userFilterQuery $blkFilterQuery $dptFilterQuery","ORDER BY user_mac_address_master.user_mac_address_id DESC");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td><?php echo $counter++; ?></td>
                       <td>
                        <div class="d-flex align-items-center">
                        <button type="button" class="btn btn-sm btn-primary mr-2" onclick="userMACAddressDetail(<?php echo $data['user_mac_address_id']; ?>)"> <i class="fa fa-eye"></i></button>
                       
                        <?php if($data['mac_address_change_status'] == 0){ ?>
                        <div class="d-flex align-items-center">
                          <form action="controller/MACAdressController.php" method="post" accept-charset="utf-8" class="mr-2">
                          <input type="hidden" name="user_mac_address_id" value="<?php echo $data['user_mac_address_id']; ?>">
                          <input type="hidden" name="macAddressApprove" value="macAddressApprove">
                          <input type="hidden" name="mac_address_change_status" value="1">
                          <input type="hidden" name="user_id" value="<?php echo $data['user_id']; ?>">
                          <input type="hidden" name="mac_address" value="<?php echo $data['mac_address']; ?>">
                          <button title="Approve Request" type="submit" class="btn form-btn btn-sm btn-primary ml-1" ><i class="fa fa-check"></i></button>
                          </form>
                          <button title="Decline Leave" type="button" class="btn btn-sm btn-danger ml-1" onclick="macAddressRequestStatusChange('2', '<?php echo $data['user_mac_address_id']; ?>')"><i class="fa fa-times"></i></button>
                        </div>
                        </div>
                        <?php }elseif($data['mac_address_change_status'] == 1){ ?>
                          <b><span class="badge badge-pill m-1 badge-success">Approved</span></b>
                        <?php }else{ ?>
                          <b><span class="badge badge-pill m-1 badge-danger">Rejected</span></b>
                        <?php } ?>
                        </td>
                       <td><?php echo $data['user_full_name']; ?></td>
                       <td><?php echo $data['user_full_name']; ?></td>
                       <td><?php echo $data['mac_address']; ?>   <?php if($data['mac_address']  !="" && $data['mac_address'] == $data['user_mac_address']){ ?><i data-toggle="tooltip" title="Current MAC Address" class="fa fa-check-circle text-success" aria-hidden="true"></i><?php } ?></td>
                       <td><?php echo $data['mac_address_device']; ?></td>
                       <td><?php echo $data['mac_address_phone_modal']; ?></td>
                       <td><?php if($data['mac_address_status_change_date'] !="0000-00-00"){echo  date('d-m-Y',strtotime($data['mac_address_status_change_date']));} ?></td>
                       <td><?php if($data['mac_address']  !="" && $data['mac_address']) { custom_echo($data['mac_address_change_reason'],50); }else { echo "No Data";} ?></td>
					             
                       <td><?php if($data['mac_address_status_change_by_type']==0){ echo $data['status_change_by'];}else{ echo $data['admin_name'];} ?></td>
                       
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="macAddreeStatusRejectModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Reject Reason</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="macAddressRejectForm" action="controller/MACAdressController.php" name="changeAttendance"  enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea required class="form-control" name="mac_address_reject_reason"></textarea>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                    <input type="hidden" name="macAddressReject"  value="macAddressReject">
                      <input type="hidden" id="mac_address_change_status" name="mac_address_change_status"  value="">
                      <input type="hidden" name="user_mac_address_id"  id="user_mac_address_id">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="userMACAddressDetailModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">User MAC Address Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="userMACAddressDetailData" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
