<?php
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-md-12">
                <h4 class="page-title">Expense Setting</h4>
            </div>
        </div>
        <form action="" method="get" class="branchDeptFilter">
            <div class="row pt-2 pb-2">
                <?php include 'selectBranchDeptForFilter.php' ?>
                <div class="col-md-2 form-group">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $i = 1;
                            if(isset($bId) && $bId > 0)
                            {
                                $blockFilterQuery = " AND floors_master.block_id = '$bId'";
                            }
                            if(isset($dId) && $dId > 0)
                            {
                                $blockFilterQuery .= " AND floors_master.floor_id = '$dId'";
                            }
                            $q = $d->select("floors_master,block_master", "floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floors_master.floor_status=0 $blockFilterQuery $blockAppendQuery");
                            $counter = 1;
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Branch</th>
                                        <th>Department</th>
                                        <th>Expense On/Off</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php while ($data = mysqli_fetch_array($q)) { ?>
                                    <tr>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['block_name']; ?></td>
                                        <td><?php echo $data['floor_name']; ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <?php if ($data['expense_on_off'] == "0") {
                                                $status = "expenseOff";
                                                $active = "checked";
                                                } else {
                                                $status = "expenseOn";
                                                $active = "";
                                                }?>
                                                <input  type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['floor_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addExpenseAmountModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Expense Amount</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="billPayDiv" style="align-content: center;">
                <div class="card-body">
                    <form id="addExpenseAmountForm" action="controller/ExpenseSettingController.php" enctype="multipart/form-data" method="post">
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Expense Amount <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" min="1" class="form-control" id="max_expense_amount" placeholder="Max Expense Amount" name="max_expense_amount" value="">
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" name="expense_floor_id" id="expense_floor_id" value="" >
                            <button id="addExpenseAmountBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                            <input type="hidden" name="addExpenseAmount"  value="addExpenseAmount">
                            <button type="reset"  value="add" class="btn btn-danger  cancel" onclick="resetFrm('addExpenseAmountForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>