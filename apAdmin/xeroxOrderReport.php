<?php 
error_reporting(0);
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employeeExpenseReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Xerox Order Report</h4>
        </div>
     
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Department </label>
            <select name="floor_id"  class="form-control single-select">
                <option value="">All Department</option> 
                  <?php 
                    $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id");  
                    while ($depaData=mysqli_fetch_array($qd)) {
                  ?>
                <option  <?php if($_GET['floor_id']==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
                <?php } ?>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Employer </label>
            <select name="user_id"  class="form-control single-select">
                <option value="">All Employer</option> 
                <?php 
                    $user=$d->select("users_master","society_id='$society_id'");  
                    while ($userdata=mysqli_fetch_array($user)) {
                ?>
                <option <?php if($_GET['user_id']==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
                <?php } ?>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Xerox Status </label>
            <select name="xerox_doc_status"  class="form-control">
                <option value="">All</option> 
                <option <?php if($_GET['xerox_doc_status']=='0') { echo 'selected';} ?> value="0"  >Active</option>
                <option <?php if($_GET['xerox_doc_status']=='1') { echo 'selected';} ?>  value="1" >Deactive</option>
            </select>
          </div>
         
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
          

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                extract(array_map("test_input" , $_GET));
                // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                 $date=date_create($from);
                $dateTo=date_create($toDate);
                $nFrom= date_format($date,"Y-m-d 00:00:00");
                $nTo= date_format($dateTo,"Y-m-d 23:59:59");
                if(isset($_GET['floor_id']) && $_GET['floor_id'] > 0) {
                    $deptFilterQuery = " AND xerox_doc_master.floor_id='$_GET[floor_id]'";
                }
                if(isset($_GET['user_id']) && $_GET['user_id'] > 0) {
                    $userFilterQuery = " AND xerox_doc_master.user_id='$_GET[user_id]'";
                }
                if(isset($_GET['xerox_doc_status']) ) {
                    $stQuery = " AND xerox_doc_master.xerox_doc_status='$_GET[xerox_doc_status]'";
                }
               
                $q = $d->select("xerox_doc_master,xerox_paper_size,xerox_type_category,floors_master,users_master", "xerox_paper_size.xerox_paper_size_id = xerox_doc_master.xerox_paper_size_id AND xerox_type_category.xerox_type_id= xerox_doc_master.xerox_type_id AND users_master.user_id=xerox_doc_master.user_id AND users_master.floor_id=floors_master.floor_id  AND xerox_doc_master.uploade_date BETWEEN '$nFrom' AND '$nTo' $deptFilterQuery $userFilterQuery  $stQuery ", "ORDER BY xerox_doc_master.uploade_date DESC");
                  $i=1;
                if (isset($_GET['from'])) {
               ?>
                
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Department</th>
                        <th>Employee</th>
                        <th>Xerox Type</th>
                        <th>Xerox Size</th>
                        <th>Date</th>
                        <th>Status</th>  

                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['floor_name']; ?> (<?php echo $data['user_designation']; ?>)</td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php echo $data['xerox_type_name']; ?></td>
                        <td><?php echo $data['xerox_paper_size_name']; ?></td>

                        <td><?php echo date("d M Y", strtotime($data['uploade_date'])); ?></td>
                        <td><?php
                            if ($data['xerox_doc_status']==0) { 
                                echo "Active";
                            }  else{
                                echo "Deactive";
                            }
                        ?></td>
                        
                       
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            <?php } else {  ?>
              <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->