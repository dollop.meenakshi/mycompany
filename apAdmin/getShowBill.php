<?php
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
if (isset($_COOKIE['bms_admin_id'])) {
$bms_admin_id = $_COOKIE['bms_admin_id'];
$aq=$d->select("bms_admin_master,role_master","role_master.role_id=bms_admin_master.role_id AND bms_admin_master.admin_id='$bms_admin_id'");
    $adminData= mysqli_fetch_array($aq);
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

$sq=$d->selectRow("currency,bill_cancel_minutes","society_master","society_id='$society_id'");
$societyData = mysqli_fetch_array($sq);
$currency = $societyData['currency'];
$bill_cancel_minutes = $societyData['bill_cancel_minutes'];

$bms_admin_id = $_COOKIE['bms_admin_id'];
    $aq=$d->select("bms_admin_master,role_master","role_master.role_id=bms_admin_master.role_id AND bms_admin_master.admin_id='$bms_admin_id'");
    $adminData= mysqli_fetch_array($aq);

extract(array_map("test_input" , $_POST));
$q=$d->select("receive_bill_master,unit_master,block_master,users_master,bill_master","users_master.delete_status=0 AND bill_master.bill_master_id=receive_bill_master.bill_master_id AND users_master.unit_id=unit_master.unit_id AND  block_master.block_id=unit_master.block_id AND  receive_bill_master.unit_id=unit_master.unit_id AND receive_bill_master.receive_bill_id='$receive_bill_id' AND users_master.user_id=receive_bill_master.paid_by","ORDER BY user_id DESC");
$data=mysqli_fetch_array($q);
extract($data);
?>
<div class="table-responsive">

<table class="table table-bordered">
	<tr>
		<th>Bill For</th>
		<td><?php echo $data['block_name']."-".$data['unit_name']; ?> (<?php echo custom_echo($data['user_full_name'],20); ?>)</td>
	</tr>
	<tr>
		<th>Bill Generated Date</th>
		<td><?php echo $data['bill_payment_date']; ?></td>
	</tr>
	<tr>
		<th>Payment Mode</th>
		<td><?php if ($data['bill_payment_type'] == 0) {
				echo "Cash ";
				if($data['payment_detail']!=NULL){ echo  '('.$data['payment_detail'].')'; }
			}
			elseif ($data['bill_payment_type'] == 1) {
				echo "Cheque ";
			} else {
				echo "Online ";
			}
			?></td>
			</tr>
			<?php  if($data['bill_payment_type'] == 1){
			?>
			<tr> <th>Bank Name</th>
					<td><?php if($data['bank_name']==NULL){ echo "Not Available";}   echo  $data['bank_name'] ;?></td>
			</tr>
			<tr> <th>Cheque Number</th>
					<td><?php if($data['payment_detail']==NULL){ echo "Not Available";}  echo  $data['payment_detail'] ;?></td>
			</tr>
			<?php
			} else if($data['bill_payment_type'] == 2){
			?>
			<tr> <th>Bank Name</th>
					<td><?php if($data['bank_name']==NULL){ echo "Not Available";} echo  $data['bank_name'] ;?></td>
			</tr>
			<tr> <th>Transaction Id</th>
					<td><?php if($data['payment_detail']==NULL){ echo "Not Available";} echo  $data['payment_detail'] ;?></td>
			</tr>
			<?php
			}
			 //IS_846?>
			<tr> <th>Current Units</th>
					<td><?php  echo  $data['current_unit_read'] ;?></td>
			</tr>
			<tr> <th>Previous Units</th>
					<td><?php  echo  $data['previous_unit_read'] ;?></td>
			</tr>
			<tr> <th>Consumed Units</th>
					<td><?php  echo  $data['no_of_unit'];  if($data['agjustment_unit']!=0) { echo " (". $data['agjustment_unit'].' Adjustment Unit)';} ?>  </td>
			</tr>
			<tr> <th>Per Unit Price</th>
					<td><?php echo $currency; ?> <?php echo number_format($data['unit_price'],2).' Per Unit';?></td>
			</tr>
			

	<tr>
		<th>Bill Amount</th>
		<td><?php echo $currency; ?> <?php echo number_format($data['bill_amount'],2);  
			 $temBillAmount=  $data['no_of_unit']*$data['unit_price'];
             if ($data['minimum_charge']>$temBillAmount) {
             	echo "<br>Minimum charge applied on this bill";
             }

		?></td>
	</tr>
	<?php if ($data['transaction_charges']>0) { ?>
	<tr>
		<th>Transaction Charges<br> (Excluded)</th>
		<td><?php echo $currency; ?> <?php echo number_format($data['transaction_charges'],2); ?></td>
	</tr>
	<?php } if ($data['discount_amount']>0) { ?>
	<tr>
		<th>Discount</th>
		<td><?php echo $currency; ?> -<?php echo number_format($data['discount_amount'],2); ?></td>
	</tr>
	<?php }
	 if ($data['late_fees_amount']>0) { 
	 ?>
	<tr>
		<th>Late Fees</th>
		<td><?php echo $currency; ?> <?php echo number_format($data['late_fees_amount'],2); ?></td>
	</tr>
	<?php if ($data['wallet_amount_type']==0 && $data['wallet_amount']>0) { ?>
	<tr>
		<th>Credit Amount in Wallet</th>
		<td><?php echo $currency; ?> <?php echo number_format($data['wallet_amount'],2); ?></td>
	</tr>
	<?php } } ?>
	<tr>
		<th>Received Amount</th>
		<td><?php echo $currency; ?> <?php 
		if ($data['wallet_amount_type']==1) {
			# code...
		echo number_format($data['received_amount']-$data['transaction_charges'],2,'.',''); 
		} else {
		$tempAmt=   number_format($data['received_amount']-$data['transaction_charges'],2,'.','');
		echo number_format($tempAmt + $data['wallet_amount'],2);
		} ?>
		 </td>
	</tr>
	<tr>
		<th>Attachment</th>
		<td><?php if ($data['receive_bill_receipt_photo'] !="") { ?>
				<a target="_blank" href="../img/billReceipt/<?php echo $data['receive_bill_receipt_photo'];?>"><?php echo $data['receive_bill_receipt_photo']; ?></a>
		 <?php } ?></td>
	</tr>

	<tr>
		<th>Received By </th>
		<td><?php
			$receivedBy = (int)$data['received_by']; 
			if ($receivedBy!="" && $receivedBy!=0) 
			{
				$qad=$d->select("bms_admin_master","admin_id='$receivedBy'");
                $adminData=mysqli_fetch_array($qad);
                echo $adminData['admin_name'];
			} else if( $receivedBy!=0) {

			 echo $data['received_by'];
			}
		 ?></td>
	</tr>
	<tr>
		<th>Invoice

		</th>
		<th>
		<?php if ($data['received_amount']>0) { ?>
			<a  onclick="return popitup('invoice.php?user_id=<?php echo $data['user_id'] ?>&unit_id=<?php echo $data['unit_id'] ?>&type=B&societyid=<?php echo $data['society_id'] ?>&id=<?php echo $data['receive_bill_id'] ?>')" href="#" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print</a> (<?php echo number_format((float) ($data['received_amount'])  ,2);;?>)
			<?php if ($data['wallet_amount_type']==0 && $data['wallet_amount']>0) {  
				echo "<br>".$currency.' '.$data['wallet_amount'].' Credited in wallet for overpaid'; 
			}  else  if ($data['wallet_amount_type']==1 && $data['wallet_amount']>0) {  
				echo "<br>".$currency.' '.$data['wallet_amount'].' Debited from wallet'; 
			}  ?> 
		<?php } else {
			echo "Not Available";
		} ?>
		</th>
	</tr>
	<?php
	   $cTime= date("Y-m-d H:i:s");   
	  
	   	$start_date = new DateTime($data['bill_payment_date']);
	   	$since_start = $start_date->diff(new DateTime($cTime));

		$minutes = $since_start->days * 24 * 60;
		$minutes += $since_start->h * 60;
		$minutes += $since_start->i;
		 $minutes;
	   if ($minutes<=$bill_cancel_minutes && $receivedBy==$_COOKIE['bms_admin_id'] ||  $minutes<=$bill_cancel_minutes &&  $adminData['admin_type'] ==1) {
	   
	?>
		<tr>
			<th colspan="2" class="text-center">
				<form action="controller/billController.php" method="post" accept-charset="utf-8">
					<input type="hidden" name="unitName" value="<?php echo $block_name; ?>-<?php echo $unit_name; ?>">
					<input type="hidden" name="bill_id" value="<?php echo $bill_master_id; ?>">
					<input type="hidden" name="bId" value="<?php echo $block_id; ?>">
					<input type="hidden" name="bill_name_cancel" value="<?php echo $bill_name; ?>">
					<input type="hidden" name="receive_bill_id" value="<?php echo $receive_bill_id; ?>">
					<input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
					<input type="hidden" name="paidBillCancel" value="<?php echo 'paidBillCancel'; ?>">
					<input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>">
					<input type="hidden" name="paid_by" value="<?php echo $paid_by; ?>">
					<input type="hidden" name="user_mobile" value="<?php echo $user_mobile; ?>">
					<input type="hidden" name="wallet_amount" value="<?php echo $wallet_amount; ?>">
					<input type="hidden" name="wallet_amount_type" value="<?php echo $wallet_amount_type; ?>">
					<button type="submit" class="btn btn-sm btn-danger form-btn">Cancel This Bill</button>
				</form>
					<i>When cancel, All transaction history will delete for this unit.</i>
			</th>
		</tr>
	<?php  } ?>
</table>
</div>
<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }

    $('.form-btn').on('click',function(e){
    e.preventDefault();
    var form = $(this).parents('form');
     swal({
        title: "Are you sure?",
        text: "",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ['Cancel', 'Yes, I am sure !'],
      })
     .then((willDelete) => {
        if (willDelete) {
          form.submit();
        }
      });

});
</script>