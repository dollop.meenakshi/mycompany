  <?php 
  error_reporting(0);
  $currentYear = date('Y');
  $nextYear = date('Y', strtotime('+1 year'));
  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  $ltId = (int)$_REQUEST['ltId'];
  $year = (int)$_REQUEST['year'];
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Leave Balance Employee Wise</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
       <div class="btn-group float-sm-right">
        
         <a href="javascript:void(0)" onclick="DeleteAll('deleteLeaveAssign');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
     </div>
     </div>
     <form class="branchDeptFilterWithUser" action="" >
        <div class="row pt-2 pb-2">
          <?php include 'selectBranchDeptEmpForFilter.php' ?>
          <div class="col-md-3 form-group">
            <select name="ltId" class="form-control single-select">
              <option value="">All Leave Type</option> 
                <?php 
                  $qLeaveType=$d->select("leave_type_master","society_id='$society_id' AND leave_type_delete=0");  
                  while ($qLeaveTypeData=mysqli_fetch_array($qLeaveType)) {
                ?>
              <option  <?php if($ltId==$qLeaveTypeData['leave_type_id']) { echo 'selected';} ?> value="<?php echo  $qLeaveTypeData['leave_type_id'];?>" ><?php echo $qLeaveTypeData['leave_type_name'];?></option>
              <?php } ?>
            </select>
          </div>
          <?php include 'selectYearForFilter.php' ?>
          <div class="col-md-3 form-group mt-auto">
            <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
          </div>       
        </div>
      </form>
    
      <div class="row">
        <?php if(isset($dId) && isset($uId) && $dId>0 && $uId>0){ ?>
        <div class="col-md-12 pb-2">
          <a href="leaveCarryForward?bId=<?php echo $bId; ?>&dId=<?php echo $dId; ?>&uId=<?php echo $uId; ?>&year=<?php echo $year; ?>" class="btn btn-sm btn-primary waves-effect waves-light float-right ml-1">Carry Forward</a>
          <a href="leavePayOut?bId=<?php echo $bId; ?>&dId=<?php echo $dId; ?>&uId=<?php echo $uId; ?>&year=<?php echo $year; ?>" class="btn btn-sm btn-primary waves-effect waves-light float-right">Pay Out</a>
        </div>
        <?php } ?>  
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php 
                  $i=1;
                  if(isset($dId) && $dId>0) {
                    $deptFilterQuery = " AND users_master.floor_id='$dId'";
                  }
                  if(isset($uId) && $uId>0) {
                    $userFilterQuery = " AND leave_assign_master.user_id='$uId'";
                  }
                  if(isset($ltId) && $ltId>0) {
                    $leaveTypeFilterQuery = " AND leave_assign_master.leave_type_id='$ltId'";
                  }
                  if(isset($year) && $year>0) {
                    $assignYearFilterQuery = " AND leave_assign_master.assign_leave_year='$year'";
                  }
                    $q=$d->selectRow("*,
                      (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
                      AND leave_status=1 AND paid_unpaid=0 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=0 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year') AS total_full_day_leave, 
                      (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
                      AND leave_status=1 AND paid_unpaid=0 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=1 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year') AS total_half_day_leave, 
                      (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
                      AND leave_status=1 AND paid_unpaid=1 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=0 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year') AS unpaid_full_day_leave, 
                      (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
                      AND leave_status=1 AND paid_unpaid=1 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=1 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year') AS unpaid_half_day_leave,
                      (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = users_master.user_id AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year='$year' AND is_leave_carry_forward=0) AS pay_out_leave,
                      (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = users_master.user_id AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year='$year' AND is_leave_carry_forward=1) AS carry_forward_leave
                    ","leave_assign_master,leave_type_master,users_master","users_master.user_id=leave_assign_master.user_id AND leave_assign_master.leave_type_id=leave_type_master.leave_type_id AND leave_assign_master.society_id='$society_id' AND users_master.delete_status=0 $userFilterQuery $leaveTypeFilterQuery $assignYearFilterQuery $deptFilterQuery $blockAppendQueryUser","ORDER BY leave_assign_master.user_leave_id ASC");
                    $counter = 1;
                    if(isset($dId) && isset($uId) && $dId>0 && $uId>0){
                ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                    
                        <th>Leave Type</th>
                        <th>Assign Leaves</th>                      
                        <th>Use In Month</th>                      
                        <th>Remaining Paid Leave</th>                      
                        <th>Used Paid Leave</th>                      
                        <th>Unpaid Leave</th>                      
                        <th>Pay Out</th>                      
                        <th>Carry Forward</th>                      
                        <th>Year</th>                      
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    while ($data=mysqli_fetch_array($q)) {
                      /* $approvedFullDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
                      $approvedFullDayLeaveData = mysqli_fetch_assoc($approvedFullDayLeave);
                      $approvedHalfDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
                      $approvedHalfDayLeaveData = mysqli_fetch_assoc($approvedHalfDayLeave);
                      $noOfHalfDayLeave = $approvedHalfDayLeaveData['approved_leave']/2;
                      $payOutLeave = $d->selectRow('SUM(no_of_payout_leaves) AS no_of_payout_leaves', "leave_payout_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_payout_year = '$year' AND is_leave_carry_forward=0");
                      $payOutLeaveData = mysqli_fetch_assoc($payOutLeave);

                      $CarryForwardLeave = $d->selectRow('SUM(no_of_payout_leaves) AS no_of_payout_leaves', "leave_payout_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_payout_year = '$year' AND is_leave_carry_forward=1");
                      $CarryForwardLeaveData = mysqli_fetch_assoc($CarryForwardLeave);

                      $remaining_leave = $data['user_total_leave'] - ($approvedFullDayLeaveData['approved_leave'] + $noOfHalfDayLeave) - $payOutLeaveData['no_of_payout_leaves'] - $CarryForwardLeaveData['no_of_payout_leaves'];
                      $fullUnpaidLeave = $d->selectRow('COUNT(*) AS full_unpaid_leave', "leave_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=1 AND leave_day_type=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
                      $fullUnpaidLeaveData = mysqli_fetch_assoc($fullUnpaidLeave);
                      $halfUnpaidLeave = $d->selectRow('COUNT(*) AS half_unpaid_leave', "leave_master", "user_id='$data[user_id]' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=1 AND leave_day_type=1 AND DATE_FORMAT(leave_created_date,'%Y') = '$year'");
                      $halfUnpaidLeaveData = mysqli_fetch_assoc($halfUnpaidLeave);
                      $totalUnpaidLeave = $fullUnpaidLeaveData['full_unpaid_leave'] + ($halfUnpaidLeaveData['half_unpaid_leave']/2); */
                     ?>
                    <tr>
                       <td class="text-center">
                         <?php if($data['user_total_leave'] == $remaining_leave){ ?>
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['user_leave_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['user_leave_id']; ?>">      
                          <?php } ?>                
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['leave_type_name']; ?></td>
                       <td><?php echo $data['user_total_leave']; ?></td>
                       <td><?php echo $data['applicable_leaves_in_month']; ?></td>
                       <!-- <td><?php echo $remaining_leave; ?></td> -->
                       <td><?php echo $data['user_total_leave']-($data['total_full_day_leave']+($data['total_half_day_leave']/2))-$data['pay_out_leave']-$data['carry_forward_leave']; ?></td>
                       <td> <a href="javascript:void(0)" onclick="leaveHistory(<?php echo $data['user_id']; ?>, <?php echo $data['leave_type_id']; ?>, <?php echo $data['assign_leave_year']; ?>);"><?php echo ($data['total_full_day_leave']+($data['total_half_day_leave']/2)); ?></a></td>
                       <td><?php echo ($data['unpaid_full_day_leave']+($data['unpaid_half_day_leave']/2)); ?></td>
                       <td><?php echo $data['pay_out_leave']; ?></td>
                       <td><?php echo $data['carry_forward_leave']; ?></td>
                       <td><?php echo $data['assign_leave_year']; ?></td>
                       <td>
                       <div class="d-flex align-items-center">
                          <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="leaveAssignDataSet(<?php echo $data['user_leave_id']; ?>)"> <i class="fa fa-pencil"></i></button>

                          <?php if($data['leave_assign_active_status']=="0"){
                              $status = "leaveAssignStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "leaveAssignStatusActive";
                              $active="";  
                          } ?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['user_leave_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                        </div>
                        </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            <?php } else {  ?>
                  <div class="" role="alert">
                    <span><strong>Note :</strong> Please Select Department</span>
                  </div>
                <?php } ?>
            </div>
            </div>
          </div>
          <?php if(isset($dId) && isset($uId) && $dId>0 && $uId>0){ 
            if(isset($ltId) && $ltId>0) {
              $leaveTypeAppendQuery = " AND leave_assign_master.leave_type_id='$ltId'";
              $leaveTypeAppendQueryOnly = " AND leave_type_id='$ltId'";
            }
            $assignLeaveq = $d->selectRow("SUM(leave_assign_master.user_total_leave) AS assign_leave,
            (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$uId' 
            AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year' $leaveTypeAppendQueryOnly) AS total_full_day_leave, 
            (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$uId' 
            AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year' $leaveTypeAppendQueryOnly) AS total_half_day_leave, 
            (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$uId' 
            AND leave_status=1 AND paid_unpaid=1 AND leave_day_type=0 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year' $leaveTypeAppendQueryOnly) AS unpaid_full_day_leave, 
            (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = '$uId' 
            AND leave_status=1 AND paid_unpaid=1 AND leave_day_type=1 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year' $leaveTypeAppendQueryOnly) AS unpaid_half_day_leave,
            (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$uId' AND leave_payout_year='$year' AND is_leave_carry_forward=0 $leaveTypeAppendQueryOnly) AS pay_out_leave,
            (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = '$uId' AND leave_payout_year='$year' AND is_leave_carry_forward=1 $leaveTypeAppendQueryOnly) AS carry_forward_leave
            ", "leave_assign_master,users_master", "leave_assign_master.user_id='$uId' AND leave_assign_master.assign_leave_year='$year' AND leave_assign_master.user_id=users_master.user_id $leaveTypeAppendQuery $blockAppendQueryUser");
            $assignLeaveData = mysqli_fetch_assoc($assignLeaveq);
            if($assignLeaveData['assign_leave'] > 0){
            /* $approvedFullDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$uId' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$year' $leaveTypeAppendQueryOnly");
            $approvedFullDayLeaveData = mysqli_fetch_assoc($approvedFullDayLeave);
            $approvedHalfDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$uId' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_created_date,'%Y') = '$year' $leaveTypeAppendQueryOnly");
            $approvedHalfDayLeaveData = mysqli_fetch_assoc($approvedHalfDayLeave);
            $noOfHalfDayLeave = $approvedHalfDayLeaveData['approved_leave']/2;
            $used_leave = $approvedFullDayLeaveData['approved_leave'] + $noOfHalfDayLeave;
            $payOutLeave = $d->selectRow('SUM(no_of_payout_leaves) AS no_of_payout_leaves', "leave_payout_master", "user_id='$uId' AND leave_payout_year = '$year' AND is_leave_carry_forward=0 $leaveTypeAppendQueryOnly");
            $payOutLeaveData = mysqli_fetch_assoc($payOutLeave);

            $carryForwardLeave = $d->selectRow('SUM(no_of_payout_leaves) AS no_of_payout_leaves', "leave_payout_master", "user_id='$uId' AND leave_payout_year = '$year' AND is_leave_carry_forward=1 $leaveTypeAppendQueryOnly");
            $carryForwardLeaveData = mysqli_fetch_assoc($carryForwardLeave);

            $remaining_leave = $assignLeaveData['assign_leave'] - $used_leave - $payOutLeaveData['no_of_payout_leaves'] - $carryForwardLeaveData['no_of_payout_leaves'];
            
            $fullUnpaidLeave = $d->selectRow('COUNT(*) AS full_unpaid_leave', "leave_master", "user_id='$uId' AND leave_status=1 AND paid_unpaid=1 AND leave_day_type=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$year' $leaveTypeAppendQueryOnly");
            $fullUnpaidLeaveData = mysqli_fetch_assoc($fullUnpaidLeave);
            $halfUnpaidLeave = $d->selectRow('COUNT(*) AS half_unpaid_leave', "leave_master", "user_id='$uId' AND leave_status=1 AND paid_unpaid=1 AND leave_day_type=1 AND DATE_FORMAT(leave_created_date,'%Y') = '$year' $leaveTypeAppendQueryOnly");
            $halfUnpaidLeaveData = mysqli_fetch_assoc($halfUnpaidLeave);
            $totalUnpaidLeave = $fullUnpaidLeaveData['full_unpaid_leave'] + ($halfUnpaidLeaveData['half_unpaid_leave']/2); */
          ?>
          <div class="card">
            <div class="card-content">
              <div class="row row-group m-0">
                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                  <div class="card-body">
                    <div class="media align-items-center">
                      <?php //print_r($assignLeaveData); ?>
                        <div class="media-body">
                          <p class="text-success">Assign Leave</p>
                          <h4 class="line-height-5">
                            <a href="javascript:void(0)"><?php echo $assignLeaveData['assign_leave']; ?></a>
                          </h4>
                        </div>
                        <div class="w-circle-icon rounded border border-success shadow-success">
                        <i class="fa fa-bars text-success"></i></div>
                      </div>
                  </div>
                </div>
                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                  <div class="card-body">
                    <div class="media align-items-center">
                        <div class="media-body">
                        <p class="text-success">Remaining Paid Leave</p>
                        <h4 class="line-height-5">
                          <a href="javascript:void(0)"><?php echo $assignLeaveData['assign_leave']-($assignLeaveData['total_full_day_leave']+($assignLeaveData['total_half_day_leave']/2))-$assignLeaveData['pay_out_leave']-$assignLeaveData['carry_forward_leave']; ?></a>
                        </h4>
                        </div>
                        <div class="w-circle-icon rounded border border-success shadow-success">
                        <i class="fa fa-angellist text-success"></i></div>
                      </div>
                  </div>
                </div>
                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                  <div class="card-body">
                    <div class="media align-items-center">
                      <div class="media-body">
                      <p class="text-success">Used Paid Leave</p>
                      <h4 class="line-height-5">
                        <a href="javascript:void(0)"><?php echo ($assignLeaveData['total_full_day_leave']+($assignLeaveData['total_half_day_leave']/2)); ?></a>
                      </h4>
                      </div>
                      <div class="w-circle-icon rounded border border-success shadow-success">
                      <i class="fa fa-check-circle text-success"></i></div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                  <div class="card-body">
                    <div class="media align-items-center">
                      <div class="media-body">
                      <p class="text-success">Unpaid Leave</p>
                      <h4 class="line-height-5">
                        <a href="javascript:void(0)"><?php echo ($assignLeaveData['unpaid_full_day_leave']+($assignLeaveData['unpaid_half_day_leave']/2)); ?></a>
                      </h4>
                      </div>
                      <div class="w-circle-icon rounded border border-success shadow-success">
                      <i class="fa fa-arrow-circle-down text-success"></i></div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                  <div class="card-body">
                    <div class="media align-items-center">
                      <div class="media-body">
                      <p class="text-success">Pay Out Paid Leave</p>
                      <h4 class="line-height-5">
                        <a href="javascript:void(0)"><?php if($assignLeaveData['pay_out_leave'] != ''){ echo $assignLeaveData['pay_out_leave'];}else{echo '0.0';} ?></a>
                      </h4>
                      </div>
                      <div class="w-circle-icon rounded border border-success shadow-success">
                      <i class="fa fa-money text-success"></i></div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                  <div class="card-body">
                    <div class="media align-items-center">
                      <div class="media-body">
                      <p class="text-success">Carry Forward Paid Leave</p>
                      <h4 class="line-height-5">
                        <a href="javascript:void(0)"><?php if($assignLeaveData['carry_forward_leave'] != ''){echo $assignLeaveData['carry_forward_leave'];}else{echo '0.0';} ?></a>
                      </h4>
                      </div>
                      <div class="w-circle-icon rounded border border-success shadow-success">
                      <i class="fa fa-arrow-circle-right text-success"></i></div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          <?php } } ?>
          <?php if(isset($dId) && isset($uId) && $dId>0 && $uId>0){ 
            if(isset($ltId) && $ltId>0) {
              $leaveTypeAppendQuery01 = " AND leave_payout_master.leave_type_id='$ltId'";
            }
            $payOutHistoryq = $d->select("leave_payout_master,users_master,leave_type_master", "leave_payout_master.user_id=users_master.user_id AND leave_payout_master.leave_type_id=leave_type_master.leave_type_id AND leave_payout_master.user_id='$uId' AND leave_payout_master.leave_payout_year = '$year' $leaveTypeAppendQuery01 $blockAppendQueryUser");
            if(mysqli_num_rows($payOutHistoryq)>0){
          ?>
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Sr.No</th>                   
                      <th>Leave Type</th>
                      <th>No Of Pay Out</th>
                      <th>Amount</th>
                      <th>Date</th>
                      <th>Status</th>
                      <th>Remark</th>
                      <th>Pay</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $counter = 1;
                    while($payOutHistoryData=mysqli_fetch_array($payOutHistoryq)){
                      
                      ?>
                    <tr>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $payOutHistoryData['leave_type_name']; ?></td>
                      <td><?php echo $payOutHistoryData['no_of_payout_leaves']; ?></td>
                      <td><?php echo $payOutHistoryData['leave_payout_amount']; ?></td>
                      <td><?php echo date("d M Y h:i A", strtotime($payOutHistoryData['leave_payout_date'])); ?></td>
                      <?php if($payOutHistoryData['is_leave_carry_forward'] == 0){
                                    $status = "Pay Out";
                                    $statusClass = "badge-primary";
                                }else{
                                    $status = "Carry Forward";
                                    $statusClass = "badge-success";
                                }
                      ?>
                      <td><p class="card-title badge <?php echo $statusClass; ?>"><?php echo $status; ?></p></td>
                      <td><?php echo $payOutHistoryData['leave_payout_remark']; ?></td>
                      <td><?php if($payOutHistoryData['is_leave_carry_forward'] == 0 && $payOutHistoryData['leave_payout_status']==0){ ?><button class="btn btn-sm btn-primary" onclick="payOutLeave(<?php echo $payOutHistoryData['leave_payout_id']; ?>)">Pay</button><?php } ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <?php } } ?>
        </div>
      </div><!-- End Row-->

      
    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addEmployeeLeaveModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Leave Assign</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="leaveAssignAdd" action="controller/LeaveAssignController.php" enctype="multipart/form-data" method="post"> 
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Leave Type <span class="required">*</span></label>
                      <div class="col-lg-12 col-md-12 col-12" id="">
                        <select  type="text" required="" class="form-control single-select restFrm" id="leave_type_id" name="leave_type_id">
                        <option value="">-- Select --</option> 
                          <?php 
                              $leaveType=$d->select("leave_type_master","society_id='$society_id' AND leave_type_delete=0 AND leave_type_active_status=0");  
                              while ($leaveTypeData=mysqli_fetch_array($leaveType)) {
                          ?>
                          <option value="<?php if(isset($leaveTypeData['leave_type_id']) && $leaveTypeData['leave_type_id'] !=""){ echo $leaveTypeData['leave_type_id']; } ?>"><?php if(isset($leaveTypeData['leave_type_name']) && $leaveTypeData['leave_type_name'] !=""){ echo $leaveTypeData['leave_type_name']; } ?></option> 
                          <?php } ?>
                        </select>                   
                    </div> 
                </div>
              </div> 
              <div class="col-md-6 ">
                <div class="form-group row w-100 mx-0">
                  <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Year<span class="required">*</span></label>
                  <div class="col-lg-12 col-md-12 col-12" id="">
                  <select  type="text" id="assign_leave_year" required="" class="form-control" name="assign_leave_year" >
                      <option value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                      <option value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
                    </select>
                  </div>   
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                      <select name="block_id" class="form-control single-select restFrm" required="" onchange="getVisitorFloorByBlockId(this.value)">
                          <option value="">-- Select Branch --</option> 
                          <?php 
                              $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                              while ($blockData=mysqli_fetch_array($qb)) {
                          ?>
                          <option value="<?php echo $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                          <?php } ?>
                      </select>
                    </div>                   
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                      <select name="floor_id" id="floor_id" class="form-control single-select restFrm" onchange="getUserByLeaveType(this.value)" required="">
                        <option value="">-- Select Department --</option>
                      
                      </select>
                    </div>                   
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Employee <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                      <select  type="text" required="" class="form-control single-select restFrm" id="user_id" name="user_id">
                        <option value="">-- Select Employee --</option>
                        
                      </select>
                    </div>                   
                </div> 
              </div>
              <div class="col-md-6 ">
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">User Total Leave <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                        <input type="number" class="form-control inputReset" placeholder="User Total Leave" name="user_total_leave" value="">
                    </div> 
                </div> 
              </div>
              <div class="col-md-6 ">
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Applicable Leaves In Month </label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                        <input type="number" class="form-control inputReset" placeholder="Applicable Leaves In Month" name="applicable_leaves_in_month" value="">
                    </div> 
                </div> 
              </div>
            </div>                    
           <div class="form-footer text-center">
             <input type="hidden" name="addLeaveAssign"  value="addLeaveAssign">
             <button id="addLeaveAssignBtn" type="submit"  class="btn btn-success addLeaveAssignbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="button"  value="add" class="btn btn-danger" onclick="resetForm();"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Leave Assign</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
          <form id="leaveAssignUpdate" action="controller/LeaveAssignController.php" enctype="multipart/form-data" method="post" autocomplete="off"> 
            <div class="row">
              <div class="col-md-12 ">
                <div class="form-group row w-100 mx-0">
                    <p class="col-sm-12 text-danger">User Used Leaves: <span id="total_used_leave"></span></p>
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">User Total Leave <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                        <input step="0.5" min="0" type="text" class="form-control onlyNumber" placeholder="User Total Leave" id="user_total_leave" name="user_total_leave" value="" onkeyup="setUseInMonthLeaves()">
                    </div> 
                </div> 
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Applicable Leaves In Month</label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                    <select name="applicable_leaves_in_month" id="applicable_leaves_in_month" class="form-control single-select">

                    </select>
                        <!-- <input type="text" class="form-control" placeholder="Applicable Leaves In Month" id="applicable_leaves_in_month" name="applicable_leaves_in_month" value=""> -->
                    </div> 
                </div>
              </div>
            </div>                    
           <div class="form-footer text-center">
             <input type="hidden" id="user_leave_id" name="user_leave_id" value="" >
             <input type="hidden"  value="" >
             <button id="updateLeaveAssignBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
             <input type="hidden" name="updateLeaveAssign"  value="updateLeaveAssign">
             <!-- <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('leaveAssignUpdate');"><i class="fa fa-check-square-o"></i> Reset</button> -->
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="userLeaveHistoryModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Leave History</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                  <th>Sr.No</th>
                  <th>Leave Date</th>
                  <th>Leave Day Type</th>
              </tr>
            </thead>
            <tbody id="showLeaveHistoryData">

            </tbody>
          </table>
        </div>
      </div>
      
    </div>
  </div>
</div>


<div class="modal fade" id="leavePayoutModel">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Leave Payout</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
          <form id="leavePayout" action="controller/LeaveAssignController.php" enctype="multipart/form-data" method="post" autocomplete="off"> 
            <div class="row">
              <div class="col-md-12 ">
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Payout Mode <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                       <select class="form-control single select" name="payment_mode">
                         <option value="">select mode</option>
                         <option value="0">Cash</option>
                         <option value="1">Bank Transfer</option>
                         <option value="2">Cheque</option>
                       </select>
                    </div> 
                </div> 
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Reference Number <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                        <input  type="text" class="form-control onlyNumber" placeholder="User Total Leave" id="payment_reference_number" name="payment_reference_number" value="" >
                    </div> 
                </div> 
                <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Payment Remark <span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12" id="">
                        <textarea type="text" class="form-control " placeholder="User Total Leave" id="payment_remark" name="payment_remark" value="" ></textarea>
                    </div> 
                </div> 
              </div>
            </div>                    
           <div class="form-footer text-center">
          
             <input type="hidden" id="leave_payout_id" name="leave_payout_id" value="" >
             <input type="hidden"  value="<?php echo $bId; ?>" name="dId" >
             <input type="hidden"  value="<?php echo $bId; ?>" name="dId" >
             <input type="hidden"  name="uId"  value="<?php echo $uId; ?>" >
             <input type="hidden"  name="bId"  value="<?php echo $bId; ?>" >
           
             <button id="payoutLeave" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
             <input type="hidden" name="payoutLeave"  value="payoutLeave">
             <!-- <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('leaveAssignUpdate');"><i class="fa fa-check-square-o"></i> Reset</button> -->
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>
<script type="text/javascript">

    function setUseInMonthLeaves(){
        value = parseFloat($('#user_total_leave').val());
        used_leave = parseFloat($('#total_used_leave').text());

        //console.log(value);
        //console.log(used_leave);
        if(value >= used_leave){
          console.log(value);
          use_in_month_content = ``;
          month_use_value = value>=31?31:value;
          var x = 1;
          for (var i = (x-0.5); i <=month_use_value; i += 0.5) {
              use_in_month_content += `<option value="`+i+`">`+i+`</option>`;
          }
          $('#applicable_leaves_in_month').html(use_in_month_content);
          $('#updateLeaveAssignBtn').removeAttr('disabled');
        }else{
          $('#updateLeaveAssignBtn').attr('disabled','disabled');
        }
    }

    /* function checkUserUsedLeave(){
      value = $('#user_total_leave').val();
      used_leave = $('#total_used_leave').text();
    } */

  function resetForm()
  {
    $(".restFrm").val('').trigger('change');
    $('.inputReset').val('');
  }

</script>
