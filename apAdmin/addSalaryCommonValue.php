<style>
  .checkBoxH {
    display: none;
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  input[type=number] {
    -moz-appearance: textfield;
  }
</style>
<?php

$gId = (int)$_REQUEST['gId'];
extract(array_map("test_input", $_POST));
if (isset($gId) && $gId>0) {
  $q = $d->select("salary_group_master", "salary_group_id='$gId'");
  $data = mysqli_fetch_array($q);
  extract($data);
}
?>

<?php
  $earnArray = array();
  $qdt00 = $d->selectRow('salary_earning_deduction_type_master.*,salary_common_value_master.salary_common_value_remark,
    salary_common_value_master.amount_type,
    salary_common_value_master.amount_value,
    salary_common_value_master.salary_common_value_earn_deduction,
    salary_common_value_master.salary_common_value_id,salary_common_value_master.salary_earning_deduction_id AS salary_earning_deduction_id_common', "salary_earning_deduction_type_master 
    LEFT JOIN salary_common_value_master ON salary_common_value_master.salary_earning_deduction_id =salary_earning_deduction_type_master.salary_earning_deduction_id AND salary_common_value_master.salary_group_id ='$salary_group_id'
    ", "salary_earning_deduction_type_master.society_id='$society_id' AND salary_earning_deduction_type_master.salary_earning_deduction_status=0 AND salary_earning_deduction_type_master.earning_deduction_type=0 AND salary_earning_deduction_type_master.earn_deduct_is_delete = 0 ");
    $qdt=$qdt00;
  while ($earnDeductionTypeData = mysqli_fetch_array($qdt00)) {
    array_push($earnArray, $earnDeductionTypeData);
  }
                        
                        ?>
                      
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Salary Common Value</h4>
      </div>
      <div class="col-sm-3">
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="addSalaryCommonValueFrom" action="controller/SalaryCommonValueController.php" class="addSalaryCommonValueFrom" enctype="multipart/form-data" method="post">
              <input type="hidden" name="salary_group_id" value="<?php echo $salary_group_id;?>">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Salary Group Name<span class="required">*</span></label>
                    <div class="col-lg-12 col-md-12 col-12">
                      <input autocomplete="off" class="salary_group_name form-control" type="text" value="<?php if (isset($salary_group_name)) { echo $salary_group_name;  } ?>" name="salary_group_name"  >
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group row w-100 mx-0">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Working Days Type
                    <span class="required">*</span></label>
                      <div class="col-lg-10 col-md-10" id="">
                        <select type="text" id="working_day_calculation"  class="form-control single-select" name="working_day_calculation">
                          <option <?php if(isset($working_day_calculation) && $working_day_calculation==0){echo "selected";} ?> value="0"> Present Day (Excluded Week Off & Holiday)</option>
                          <option <?php if(isset($working_day_calculation) && $working_day_calculation==1){echo "selected";} ?> value="1"> Present Day & Paid Holiday (Excluded Week Off) </option>
                          <option <?php if(isset($working_day_calculation) && $working_day_calculation==2){echo "selected";} ?> value="2">Calendar days (Included Week Off & Holiday)</option>
                        
                        </select>
                      </div>
                    </div>
                  </div>
                <div class="col-md-4">
                    <div class="form-group row w-100 mx-0 formulaDiv <?php  if($allow_extra_day_payout_per_day=="1" &&  $allow_extra_day_payout_per_hour=="1" &&  $allow_extra_day_payout_fixed=="1"){ echo ""; } else if(isset($gId) && $gId>0){ echo "d-none"; }else { echo "";} ?>">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Extra Day/Hours Payout Formula
                      </label>
                      <div class="col-lg-10 col-md-10" id="">
                          <input type="number"  id="hourly_salary_extra_hours_payout" step="any" min=" 0.25" max="10.00" class="form-control onlyNumber inputSl " value="<?php if(isset($hourly_salary_extra_hours_payout) && $hourly_salary_extra_hours_payout !=""){ echo $hourly_salary_extra_hours_payout; } else { echo "1";} ?>" name="hourly_salary_extra_hours_payout" >
                        <i class="text-primary">  For Half amount  set 0.5 ,For Same Amount set 1 ,For Double amount set 2</i>
                      </div>
                    </div>
                  </div>

              </div>
              <div class="row">
                <div class="col-md-4">
                  <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Allow Extra Day Payout (In Per Day Salary)</label>
                  <select class="form-control"  onchange="allowExtra(this.value,'day')" id="allow_extra_day_payout_per_day" name="allow_extra_day_payout_per_day">
                      <option <?php if(isset($allow_extra_day_payout_per_day)){ if($allow_extra_day_payout_per_day=='1'){ echo "selected"; }else { echo ''; } }else{ echo "selected"; } ?> value="1"> Yes</option>
                      <option <?php if(isset($allow_extra_day_payout_per_day)){ if($allow_extra_day_payout_per_day=='0'){ echo "selected"; }else { echo ''; } }else{ echo ""; } ?> value="0">No</option>
                  </select>
                  
                  <div class="showSelect_day <?php if(isset($allow_extra_day_payout_per_day)){ if($allow_extra_day_payout_per_day=='0'){ echo "d-none"; }else { echo ''; } }else{ echo ""; } ?>">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Extra Day Payout Type (In Per Day Salary)</label>
                    <select class="single-select form-control" id="extra_day" onchange="extraPayOut(this.value,'day')" name="extra_day_payout_type_per_day" onchange=""> 
                      <option <?php if(isset($extra_day_payout_type_per_day) && $extra_day_payout_type_per_day=='0'){echo "selected"; }else{echo "selected"; } ?> value="0">Average</option>
                      <option <?php if(isset($extra_day_payout_type_per_day) && $extra_day_payout_type_per_day=='1'){echo "selected"; }else{echo ""; } ?> value="1">Fixed</option>
                    </select>
                  </div>
                  <div class="showSubSelect_day <?php if(isset($extra_day_payout_type_per_day) && $extra_day_payout_type_per_day=='1'){echo ""; }else{echo "d-none"; } ?> ">
                    <label for="input-10"  class="col-lg-12 col-md-12 col-form-label">Fixed Amount (In Per Day Salary)<span class="required">*</span></label>
                    <input class=" form-control onlyNumber fixed_amount_for_per_day" type="text" value="<?php if(isset($fixed_amount_for_per_day)){echo $fixed_amount_for_per_day; } ?>" name="fixed_amount_for_per_day"> 
                      
                  </div>
                </div>
                <div class="col-md-4">
                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Allow Extra Hour Payout (In Per Hour Salary)</label>
                <select class="form-control"  onchange="allowExtra(this.value,'hour')" id="allow_extra_day_payout_per_hour" name="allow_extra_day_payout_per_hour">
                      <option <?php if(isset($allow_extra_day_payout_per_hour)){ if($allow_extra_day_payout_per_hour=='1'){ echo "selected"; }else { echo ''; } }else{ echo "selected"; } ?> value="1"> Yes</option>
                      <option <?php if(isset($allow_extra_day_payout_per_hour)){ if($allow_extra_day_payout_per_hour=='0'){ echo "selected"; }else { echo ''; } }else{ echo ""; } ?> value="0">No</option>
                  </select>
                  
                  <div class="showSelect_hour <?php if(isset($allow_extra_day_payout_per_hour)){ if($allow_extra_day_payout_per_hour=='0'){ echo "d-none"; }else { echo ''; } }else{ echo ""; } ?>">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Extra Hour Payout Type (In Per Hour Salary)</label>
                    <select class="single-select form-control" id="extra_hour" onchange="extraPayOut(this.value,'hour')" name="extra_day_payout_type_per_hour"> 
                    <option <?php if(isset($extra_day_payout_type_per_hour) && $extra_day_payout_type_per_hour=='0'){echo "selected"; }else{echo "selected"; } ?> value="0">Average</option>
                      <option <?php if(isset($extra_day_payout_type_per_hour) && $extra_day_payout_type_per_hour=='1'){echo "selected"; }else{echo ""; } ?> value="1">Fixed</option>
                    </select>
                  </div>
                  <div class="showSubSelect_hour <?php if(isset($extra_day_payout_type_per_hour) && $extra_day_payout_type_per_hour=='1'){echo ""; }else{echo "d-none"; } ?>">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Fixed Amount (In Per Hour Salary)<span class="required">*</span></label>
                    <input class=" form-control onlyNumber fixed_amount_for_per_hour" type="text" value="<?php if(isset($fixed_amount_for_per_hour)){
                      echo $fixed_amount_for_per_hour;
                    } ?>" name="fixed_amount_for_per_hour"> 
                    
                  </div>
                </div>
                <div class="col-md-4">
                  <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Allow Extra Payout (In Fixed Salary)</label>
                  <select class="form-control"  onchange="allowExtra(this.value,'fixed')" id="allow_extra_day_payout_fixed" name="allow_extra_day_payout_fixed">
                      <option <?php if(isset($allow_extra_day_payout_fixed)){ if($allow_extra_day_payout_fixed=='1'){ echo "selected"; }else { echo ''; } }else{ echo "selected"; } ?> value="1"> Yes</option>
                      <option <?php if(isset($allow_extra_day_payout_fixed)){ if($allow_extra_day_payout_fixed=='0'){ echo "selected"; }else { echo ''; } }else{ echo ""; } ?> value="0">No</option>
                  </select>
                    
                  <div class="showSelect_fixed <?php if(isset($allow_extra_day_payout_fixed)){ if($allow_extra_day_payout_fixed=='0'){ echo "d-none"; }else { echo ''; } }else{ echo ""; } ?>">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Extra  Payout Type (In Fixed Salary)</label>
                    <select class="single-select form-control" id="extra_fixed"  onchange="extraPayOut(this.value,'fixed')" name="extra_day_payout_type_fixed"> 
                      <option <?php if(isset($extra_day_payout_type_fixed) && ($extra_day_payout_type_fixed=='0')){ echo "selected"; }else{ echo "selected"; }?> value="0">Average</option>
                      <option <?php if(isset($extra_day_payout_type_fixed) && ($extra_day_payout_type_fixed=='1')){ echo "selected"; }else{ echo ""; }?> value="1">Fixed</option>
                    </select>
                  </div>
                  <div class="showSubSelect_fixed <?php if(isset($extra_day_payout_type_fixed) && $extra_day_payout_type_fixed=='1'){echo ""; }else{echo "d-none"; } ?>">
                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Fixed Amount (In Fixed Salary)<span class="required">*</span></label>
                    <input class=" form-control onlyNumber fixed_amount_for_fixed" value="<?php if(isset($fixed_amount_for_fixed)){ echo $fixed_amount_for_fixed; }?>" type="text" name="fixed_amount_for_fixed"> 
                      
                  </div>
                </div>
                <div class="col-md-6 showSelect_hour <?php if(isset($allow_extra_day_payout_per_hour)){ if($allow_extra_day_payout_per_hour=='0'){ echo "d-none"; }else { echo ''; } }else{ echo ""; } ?>" >
                  <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Extra Hour Payout Mode</label>
                  <select class="form-control single-select" onchange="extraHrsCalculation(this.value)" name="extra_hour_calculation">
                    <option <?php if(isset($extra_hour_calculation) && $extra_hour_calculation=="1"){ echo "selected";} ?> value="1">Day By Day</option>
                    <option <?php if(isset($extra_hour_calculation) && $extra_hour_calculation=="0"){ echo "selected";} ?> value="0">After Completed Monthly Hours</option>
                  </select>
                  
                </div>
                <div class="col-md-6 showSelect_hour hrsCal <?php if(isset($allow_extra_day_payout_per_hour)){ if($allow_extra_day_payout_per_hour=='0'){ echo "d-none"; }else { echo ''; } }else{ echo ""; } ?> <?php if(isset($extra_hour_calculation)){ if($extra_hour_calculation=="1"){ echo ""; }else{ echo "d-none"; } } ?>" >
                  
                  <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Minimum Minutes Required for Extra Payout</label>
                    <input type="text" class="onlyNumber form-control" name="extra_hour_calculation_minutes" id="" value="<?php if(isset($extra_hour_calculation_minutes)){ echo $extra_hour_calculation_minutes;} ?>">
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-4">
                  <fieldset class="border p-2">
                    <legend align='center' class="w-auto">Earning</legend>
                    <div class="row container-fluid">
                      <?php
                     /// print_r($qdt);
                    ///  $earnArray = array();
                      $qdtNew = $d->selectRow('salary_earning_deduction_type_master.*,salary_common_value_master.salary_common_value_remark,
                        salary_common_value_master.amount_type,
                        salary_common_value_master.amount_value,
                        salary_common_value_master.amount_value_employeer,
                        salary_common_value_master.percent_min_max,
                        salary_common_value_master.salary_common_value_earn_deduction,
                        salary_common_value_master.salary_common_value_id,salary_common_value_master.salary_earning_deduction_id AS salary_earning_deduction_id_common', "salary_earning_deduction_type_master 
                        LEFT JOIN salary_common_value_master ON salary_common_value_master.salary_earning_deduction_id =salary_earning_deduction_type_master.salary_earning_deduction_id AND salary_common_value_master.salary_group_id ='$salary_group_id'
                        ", "salary_earning_deduction_type_master.society_id='$society_id' AND salary_earning_deduction_type_master.salary_earning_deduction_status=0 AND salary_earning_deduction_type_master.earning_deduction_type=0 AND salary_earning_deduction_type_master.earn_deduct_is_delete = 0 ");
                      while ($earnDeductionTypeData = mysqli_fetch_array($qdtNew)) {
                       /// array_push($earnArray, $earnDeductionTypeData);
                       if($earnDeductionTypeData['percent_min_max'] !=""){
                        $percent_min_max = json_decode($earnDeductionTypeData['percent_min_max'],true);
                       }else{
                        $percent_min_max = "";
                       }
                      /// print_r($percent_min_max);
                       ?>
                        <div class="form-group col-md-12 w-100 mx-0">
                          <div class="w-100">
                            <input type="hidden" name="salary_earning_deduction_id[]" class="form-control" value="<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>">
                            <input type="checkbox" <?php if (isset($gId)) {
                              if (isset($earnDeductionTypeData['salary_common_value_id']) && $earnDeductionTypeData['salary_earning_deduction_id'] == $earnDeductionTypeData['salary_earning_deduction_id_common']) {echo "checked"; } else {echo "";}
                            } else { echo ""; } ?> onclick="checkCkdEarn('earn',<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>)" class="earning_deduction_checkbox earnCheckBox"  data-name="<?php echo $earnDeductionTypeData['earning_deduction_name']; ?>" data-type="earn" value="<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>" data-id="<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>" id="<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>" name="salary_common_value_checkbox[]">
                            <label for="<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>"> 
                              <?php echo $earnDeductionTypeData['earning_deduction_name']; ?> </label>
                            </div>
                          </div>
                          <?php
                          if (isset($gId)) 
                          {
                            if (isset($earnDeductionTypeData['salary_common_value_id']) && $earnDeductionTypeData['salary_earning_deduction_id'] == $earnDeductionTypeData['salary_earning_deduction_id_common']) 
                            {
                              $clsKey = "";
                            } 
                            else {
                              $clsKey = "checkBoxH";
                            }
                          }
                          ?>

                          <div class="col-md-4 <?php echo $clsKey; ?> checkBoxHide_<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>">
                            <div class="w-100">
                              <select name="amount_type[]" id="amount_type_<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>" data-id="<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>" onchange="checkEarnType(this.value,<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>)" class="form-control earnAmountType single-select ">
                                <option <?php if ($earnDeductionTypeData['amount_type'] != "" && $earnDeductionTypeData['amount_type'] == 1) { echo "selected";} ?> value="1">Flat</option>
                                <option <?php if ($earnDeductionTypeData['amount_type'] != "" && $earnDeductionTypeData['amount_type'] == 0) { echo "selected";} ?> value="0">Percentage</option>
                              </select>
                            </div>
                          </div>
                         
                          <?php if (isset($earnDeductionTypeData['amount_type']) && $earnDeductionTypeData['amount_type'] == 0) {$checkEarnTyp = "";} else {$checkEarnTyp = "checkEarnTyp";} ?>
                          <div class="col-lg-8  col-md-6 col-12  <?php echo $checkEarnTyp; ?>  <?php echo $clsKey; ?> checkBoxHide_<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?> 
                          checkEarnTypeHide_<?php ////  echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>">
                            <div class="form-group col-md-12 mx-0 ">

                              <div class="w-100">
                                <input type="text" min="1"  class="form-control amountMax amount_value" id="amountValueId_<?php   echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>" placeholder="<?php if(isset($earnDeductionTypeData['amount_type'])){ 
                                    if($earnDeductionTypeData['amount_type'] == 1) { echo " Amount *"; }else{ echo "Percentage *"; } }else{ echo "Amount *"; }?> " name="amount_value[<?php   echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>]" value="<?php if ($earnDeductionTypeData['amount_value'] != "") {echo $earnDeductionTypeData['amount_value'];} ?>">
                              </div>
                            </div>
                          </div>
                          <?php

                                $earnDataArry = array();
                                if (isset($gId))
                                {
                                  if (($earnDeductionTypeData['amount_type'] != "") && ((int)$earnDeductionTypeData['amount_type'] == 0) || ((int)$earnDeductionTypeData['amount_type'] == 2)) {
                                    $earnDataArry = explode(',', $earnDeductionTypeData['salary_common_value_earn_deduction']);
                                    $tempHide = "";
                                  } else{$tempHide = "tempHide"; }
                                } 
                                else {
                                  $tempHide = "tempHide";
                                } ?>
                          <div class="col-lg-12  col-md-6 col-12 checkEarnTypeHide_<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>">
                              <input type="hidden" class="salary_common_value_earn_deduction_ids_<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>" value="<?php echo $earnDeductionTypeData['salary_common_value_earn_deduction']; ?>">
                            <select  id="checkedDataId_<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>" class="form-control multiEarning multiple-select-sal_common checkedData <?php echo $tempHide; ?>" name="multiEarning[<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>][]" multiple require data-id="<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>">
                              <option value="">Select Earnings</option>
                              <?php
                               
                              for ($l = 0; $l < COUNT($earnArray); $l++) { 
                                if($earnDeductionTypeData['salary_earning_deduction_id'] !=  $earnArray[$l]['salary_earning_deduction_id']){
                                
                                ?>
                                <!-- <option  data-name="<?php echo $earnArray[$l]['earning_deduction_name']; ?>" <?php if (in_array($earnArray[$l]['salary_earning_deduction_id'], $earnDataArry)) { echo "selected"; } ?> value="<?php echo $earnArray[$l]['salary_earning_deduction_id']; ?>"><?php echo $earnArray[$l]['earning_deduction_name']; ?></option> -->
                                <?php }
                              } ?>
                              </select>
                            </div>
                            <input type="hidden" class="onlyNumber   form-control" value="0" placeholder="Employeer Amount" min="0" name="amount_value_employeer[<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>]"> 
                            <input type="hidden" name="show_employer_contribution[<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>]" value="0">
                            <!-- <div class="col-lg-12   col-md-6 col-12 mt-2 ">
                              <div class="w-100">
                                  <input type="text" class="onlyNumber  form-control" value="<?php ///if(isset($earnDeductionTypeData['amount_value_employeer']) && $earnDeductionTypeData['amount_value_employeer'] !=""){ echo $earnDeductionTypeData['amount_value_employeer']; } ?>" placeholder="Employeer Amount" min="0" name="amount_value_employeer[<?php //echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>]"> 
                              </div>
                            </div> -->
                            
                                  <!-- <input type="hidden"   value="0"  name="percent_min[<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>]"> 
                                  <input  type="hidden" value="0"   value="0" name="percent_max[<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>]"> -->
                              
                            <!-- <div class="col-lg-6   col-md-6 col-12 mt-2 checkEarnTypeHide_<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>">
                              <div class="w-100">
                                  <input data-id="<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>" type="text" value="<?php if(isset($percent_min_max) && $percent_min_max !=""){ echo $percent_min_max['percent_max']; } ?>"  class="onlyNumber maxPercent_<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?> minMaxPercent form-control"  placeholder="max" min="0" name="percent_max[<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>]">
                              </div>
                            </div> -->
                          <div class="form-group col-md-12 mt-2 <?php echo $clsKey; ?> row w-100 mx-0 checkBoxHide_<?php echo $earnDeductionTypeData['salary_earning_deduction_id']; ?>">
                            <div class="w-100">
                              <input type="text" class="form-control" placeholder="Remark" name="salary_common_value_remark[]" value="<?php if ($earnDeductionTypeData['salary_common_value_remark'] != "") { echo $earnDeductionTypeData['salary_common_value_remark']; } ?>">
                            </div>
                          </div>
                        <?php } ?>

                      </div>
<!--                                 <input type="hidden" name="percent_deduction_max_amount[]" value="0">
-->                            </fieldset>
</div>
<div class="col-md-8">
  <fieldset class="border p-2">
    <legend align='center' class="w-auto">Deduction</legend>
    <div class="row container-fluid">
      <?php
      $qdt2 = $d->selectRow('salary_earning_deduction_type_master.*,salary_common_value_master.show_employer_contribution,salary_common_value_master.amount_value_employeer,salary_common_value_master.salary_common_value_earn_deduction,salary_common_value_master.percent_min_max,salary_common_value_master.amount_type,salary_common_value_master.amount_value,salary_common_value_master.salary_common_value_remark,salary_common_value_master.slab_json,salary_common_value_master.percent_deduction_max_amount,salary_common_value_master.salary_common_value_id,salary_common_value_master.salary_earning_deduction_id AS salary_earning_deduction_id_common', "salary_earning_deduction_type_master LEFT JOIN salary_common_value_master ON salary_common_value_master.salary_earning_deduction_id =salary_earning_deduction_type_master.salary_earning_deduction_id AND salary_common_value_master.salary_group_id ='$salary_group_id' ", "salary_earning_deduction_type_master.society_id='$society_id' AND salary_earning_deduction_type_master.salary_earning_deduction_status=0 AND salary_earning_deduction_type_master.earning_deduction_type=1 AND salary_earning_deduction_type_master.earn_deduct_is_delete = 0 $blockAppendQuery");
      while ($DeductionTypeData = mysqli_fetch_array($qdt2)) {
       
        $percent_min_max = "";
        if($DeductionTypeData['percent_min_max'] !=""){
          $percent_min_max = json_decode($DeductionTypeData['percent_min_max'],true);
         }else{
          $percent_min_max = "";
         }
        
        ?>
        <div class="form-group col-md-4 w-100 mx-0">
          <label for="input-10" class="w-100 col-form-label">  </label>
          <div class="w-100">
            <input type="hidden" name="salary_earning_deduction_id[]" class="form-control" value="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
            <input  type="checkbox" <?php if (isset($gId)) {
              if (isset($DeductionTypeData['salary_common_value_id']) && $DeductionTypeData['salary_earning_deduction_id'] == $DeductionTypeData['salary_earning_deduction_id_common']) {echo "checked";} else { echo "";}} else {echo "";} ?> data-type="deduct" class="earning_deduction_checkbox" value="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" data-id="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" id="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" name="salary_common_value_checkbox[]" onclick="checkCkdEarn('deduct',<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>)">
              <label for="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>"> <?php echo $DeductionTypeData['earning_deduction_name']; ?> </label>
            </div>
          </div>
          <?php
          if (isset($gId)) 
          {
            if (isset($DeductionTypeData['salary_common_value_id']) && $DeductionTypeData['salary_earning_deduction_id'] == $DeductionTypeData['salary_earning_deduction_id_common']) 
            {
              $clsKey = "";
            } 
            else 
            {
              $clsKey = "checkBoxH";
            }
          } ?>
          <div class="form-group col-md-2 <?php echo $clsKey; ?> w-100 mx-0 checkBoxHide_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
            <label for="input-10" class="w-100  col-form-label"> Type <span class="required">*</span></label>
            <div class="w-100">
              <select name="amount_type[]" id="amount_type_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" data-id="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" onchange="checkType(this.value,<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>)" class="form-control single-select ">
                <option value="-1">-- Type --</option>
                <option <?php if ($DeductionTypeData['amount_type'] != "" && $DeductionTypeData['amount_type'] == 0) {
                  echo "selected";
                } ?> value="0">Percentage</option>
                <option <?php if ($DeductionTypeData['amount_type'] != "" && $DeductionTypeData['amount_type'] == 1) {
                  echo "selected";
                } ?> value="1">Flat</option>
                <option <?php if ($DeductionTypeData['amount_type'] != "" && $DeductionTypeData['amount_type'] == 2) {
                  echo "selected";
                } ?> value="2">Slab</option>
              </select>
            </div>
          </div>
          <div class="form-group col-md-6 <?php echo $clsKey; ?> w-100 mx-0 checkBoxHide_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
            <label for="input-10" class="w-100 col-form-label"> Remark </label>
            <div class="w-100">
              <input type="text" class="form-control" placeholder="Salary Common Value Remark" name="salary_common_value_remark[]" value="<?php if ($DeductionTypeData['salary_common_value_remark'] != "") { echo $DeductionTypeData['salary_common_value_remark']; } ?>">
            </div>
          </div>
          
          
          <?php
          $earnDataArry = array();
          if (isset($gId))
          {
            if (($DeductionTypeData['amount_type'] != "") && ((int)$DeductionTypeData['amount_type'] == 0) || ((int)$DeductionTypeData['amount_type'] == 2)) {
              $earnDataArry = explode(',', $DeductionTypeData['salary_common_value_earn_deduction']);
              $tempHide = "";
            } else{$tempHide = "tempHide"; }
          } 
          else {
            $tempHide = "tempHide";
          } ?>
          <div class="col-lg-8 <?php echo $clsKey; ?>    offset-md-4 col-md-6 col-12  <?php echo  $tempHide; ?>  checkBoxHidee_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?> " id="selectEarnType_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
          <input type="hidden" class="salary_common_value_earn_deduction_ids_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" value="<?php echo $DeductionTypeData['salary_common_value_earn_deduction']; ?>">
            <select id="checkedDataId_<?php echo $DeductionTypeData['salary_earning_deduction_id'];  ?>" class="form-control multiple-select-sal_common-deduct  checkedData" onchange="changesEarnForDeduct(<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>)"  name="multiEarning[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>][]" multiple require>
              <option value="">Select Earnings</option>
              <?php
              for ($l = 0; $l < COUNT($earnArray); $l++) { ?>
                <!-- <option data-name="<?php echo $earnArray[$l]['earning_deduction_name']; ?>" <?php if (in_array($earnArray[$l]['salary_earning_deduction_id'], $earnDataArry)) { echo "selected"; } ?> value="<?php echo $earnArray[$l]['salary_earning_deduction_id']; ?>"><?php echo $earnArray[$l]['earning_deduction_name']; ?></option> -->
                <?php } ?>
              </select>
            </div>
            
            <div class="form-group <?php if ($DeductionTypeData['amount_type'] != "" && $DeductionTypeData['amount_type'] == 2) { echo "d-none";}else{echo "";} ?> col-lg-4  offset-md-4  col-md-2 <?php echo $clsKey; ?> <?php echo $tempHide; ?> checkBoxHide_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label hideAmountLable_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">Employee <span class="changeAmountLable_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>"><?php if ($DeductionTypeData['amount_type'] != "") { if ($DeductionTypeData['amount_type'] == 1) { echo " Amount";} else { echo " Percent";}} else {echo "  Amount";} ?></span><span class="required">*</span> </label>
            <div class="col-lg-12 col-md-12 col-12" id="">
              <input <?php if ($DeductionTypeData['amount_value'] != "" && $DeductionTypeData['amount_type'] == 0) { echo "max='99'";} ?> type="number" data-id="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" id="amountMax_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>"  class="amountMax  amountMax_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?> form-control onlyNumber amount_value" placeholder="" name="amount_value[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>]" value="<?php if ($DeductionTypeData['amount_value'] != "") {echo $DeductionTypeData['amount_value'];} ?>">
            </div>
          </div>
          
            <div class="col-lg-4 <?php echo $clsKey; ?>   form-group  checkBoxHide_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>  newMaxMInpercent_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>   col-md-6 col-12 <?php if (($DeductionTypeData['amount_type'] != "") && ((int)$DeductionTypeData['amount_type'] == 2  )){ echo "tempHide"; }else{ echo ''; }?> checkBoxHidee_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
              <div class="col-lg-12 col-md-12 col-12 <?php if (($DeductionTypeData['amount_type'] != "") && ((int)$DeductionTypeData['amount_type'] == 2  )){ echo "d-none"; }else{ echo ''; }?>">
              <label for="input-10" class="col-lg-12 col-md-12 col-form-label hideAmountLable_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>"> Employer Contribution <span class="changeAmountLable_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>"><?php if ($DeductionTypeData['amount_type'] != "") { if ($DeductionTypeData['amount_type'] == 1) { echo " Amount";} else { echo " Percent";}} else {echo "  Amount";} ?></span> </label>
                  <input type="text" class="onlyNumber changeEmployerAmount form-control hideAmountLable_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" value="<?php if(isset($DeductionTypeData['amount_value_employeer']) && $DeductionTypeData['amount_value_employeer'] !=""){ echo $DeductionTypeData['amount_value_employeer']; } ?>" placeholder="" min="0" data-id="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" name="amount_value_employeer[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>]"> 
              </div>
            </div>
            <div class="form-group showOnChangeEmplrAmnt_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?> hideAmountLable_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?> <?php if(isset($DeductionTypeData['amount_type'])) { if ($DeductionTypeData['amount_type'] != "" && $DeductionTypeData['amount_type'] == 2 ) { echo "d-none";}else{ if($DeductionTypeData['amount_value_employeer']>0){ echo "displayBlock"; }else{ echo "d-none";  } } }else{ echo "d-none"; } ?> col-lg-12 offset-md-4  col-md-2 <?php echo $clsKey; ?> checkBoxHide_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label ">show Employer Contribution in salary slip</label>
              <div class="" id="">
                  <input class="form-check-input ml-1" <?php if (isset($DeductionTypeData['show_employer_contribution']) && $DeductionTypeData['show_employer_contribution'] == "1") {echo 'checked'; }else{ echo ""; } ?>  name="show_employer_contribution[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>]" type="radio" value="1">
                  <label class="ml-4">Yes</label>
                  <input class="form-check-input ml-1" <?php if (isset($DeductionTypeData['show_employer_contribution']) ){ if($DeductionTypeData['show_employer_contribution'] == "0") {echo 'checked'; }else{ echo "";} }else{ echo "checked"; } ?> name="show_employer_contribution[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>]" type="radio" value="0">
                  <label class="ml-4">No</label>
              </div>
          </div>
         
            <div class="col-lg-4 <?php echo $clsKey; ?> offset-md-4   checkBoxHidee_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?> newMaxMInpercent_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>   col-md-6 col-12 mt-2 <?php if (($DeductionTypeData['amount_type'] != "") && ((int)$DeductionTypeData['amount_type'] == 1  ) || ((int)$DeductionTypeData['amount_type'] == 2  )){ echo "tempHide"; }else{ echo $tempHide; }?> checkBoxHidee_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
              <div class="w-100">
                  <input type="text" data-id="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" class="onlyNumber  form-control minMaxPercent minPercent_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" value="<?php if(isset($percent_min_max) && $percent_min_max !=""){ echo $percent_min_max['percent_min']; } ?>" placeholder="min" min="0" name="percent_min[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>]"> 
              </div>
            </div>
            <div class="col-lg-4 <?php echo $clsKey; ?> checkBoxHidee_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>  newMaxMInpercent_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>  col-md-6 col-12 mt-2 <?php if (($DeductionTypeData['amount_type'] != "") && ((int)$DeductionTypeData['amount_type'] == 1  ) || ((int)$DeductionTypeData['amount_type'] == 2  )){ echo "tempHide"; }else{ echo $tempHide; } ?> checkBoxHidee_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
              <div class="w-100">
                  <input type="text"  data-id="<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>"  value="<?php if(isset($percent_min_max) && $percent_min_max !=""){ echo $percent_min_max['percent_max']; } ?>"class="onlyNumber maxPercent_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?> minMaxPercent form-control "  placeholder="max" min="0" name="percent_max[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>]">
              </div>
            </div>
            <?php
            if ($DeductionTypeData['amount_type'] != "" && (int)$DeductionTypeData['amount_type'] == 0) {

              $tempHidse = "";
            }

            else {
              $tempHidse = "tempHide";
            }
            ?>
            <div class="col-lg-8  <?php echo $tempHidse; ?> <?php echo $clsKey; ?> offset-md-4 mt-1 col-md-6 col-12   checkBoxHidee_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?> " id="selectDeductPrcentAmount_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
              <input type="number" min="0" name="percent_deduction_max_amount[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>]" class="form-control" placeholder="Max deduction Amount" id="percent_deduction_max_amount_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>" value="<?php if ($DeductionTypeData['percent_deduction_max_amount'] != "") { if($DeductionTypeData['percent_deduction_max_amount']>0){ echo $DeductionTypeData['percent_deduction_max_amount'];} } ?>">
            </div>
            <?php
            if (isset($gId))
            {
              if ($DeductionTypeData['amount_type'] != "" && $DeductionTypeData['amount_type'] == 2) {
                $tem = explode('~', ($DeductionTypeData['slab_json']));
                $slabCount = count($tem);
                for ($f = 0; $f < $slabCount; $f++) {
                  $slab = json_decode($tem[$f], true);   ?>
                  <div class=" w-100 px-3" id="hideSlab_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
                    <div class=" row h_<?php echo $f1Temp= $f+1; ?> extraDiv">
                      <div class="col-md-8 offset-md-4">
                        <div class="row px-2">
                          <div class="form-group col-md-3 mx-0 ">
                            <label for="input-10" class="w-100 col-form-label"> Min<span class="required">*</span> </label>
                            <div class="w-100">
                              <input type="text" class=" min_0 minClass minUniqcls_<?php echo $f+1; ?> minMaxClass onlyNumber form-control" placeholder=" Min " name="min[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>][<?php echo  $f+1; ?>]" minData-id = "<?php echo $f+1; ?>"  value="<?php if ($slab['min'] != "") { echo $slab['min'];} ?>">
                            </div>
                          </div>
                          <div class="form-group col-md-3  mx-0 ">
                            <label for="input-10" class="w-100 col-form-label"> Max <span class="required">*</span></label>
                            <div class="w-100">
                              <input type="text" class="form-control maxuniqCls_<?php echo $f+1; ?> minMaxClass max_0" maxData-id="<?php echo $f+1; ?>" min="<?php if ($slab['min'] != "") { echo $slab['min'];} ?>" placeholder=" Max " name="max[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>][<?php echo  $f+1; ?>]" value="<?php if ($slab['max'] != "") {echo $slab['max'];} ?>">
                            </div>
                          </div>
                          <div class="form-group col-md-3 mx-0 ">
                            <label for="input-10" class="w-100 col-form-label"> Amount<span class="required">*</span> </label>
                            <div class="w-100">
                              <input type="text" class="form-control" placeholder="Amount " name="value[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>][<?php echo  $f+1; ?>]" value="<?php if ($slab['value'] != "") {echo $slab['value'];} ?>">
                            </div>
                          </div>
                          <?php if ($f == 0) { ?>
                            <div class="form-group col-md-2   mt-auto text-center">
                              <button type="button" onclick="addSlabDiv(<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>,<?php echo ($slabCount-1); ?>)" class="btn btn-sm btn-info mb-2"><i class="fa fa-plus"></i></button>
                            </div>
                          <?php } else { ?>
                            <div class="form-group col-md-2 w-100  mt-auto">
                              <button r-id="<?php echo $f1Temp; ?>" type="button" class="btn removeClick btn-sm btn-danger mb-2"><i class="fa fa-minus" ></i></button>
                            </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>

                <?php   } ?>
              <?php }
            } //else {  ?>
              <div class="hideSlab w-100 px-3" id="hideSlab_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>">
                <div class=" row">
                  <div class="col-md-8 offset-md-4">
                    <div class="row px-2">
                      <div class="form-group col-md-3 mx-0 ">
                        <label for="input-10" class="w-100 col-form-label"> Min<span class="required">*</span> </label>
                        <div class="w-100">
                          <input type="text" class="form-control min_0 minClass minMaxClass minUniqcls_0 onlyNumber" placeholder=" Min " minData-id="0" name="min[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>][0]" value="">
                        </div>
                      </div>
                      <div class="form-group col-md-3  mx-0 ">
                        <label for="input-10" class="w-100 col-form-label"> Max<span class="required">*</span> </label>
                        <div class="w-100">
                          <input type="text" class="form-control minMaxClass maxuniqCls_0 max_0" placeholder=" Max " maxData-id="0" name="max[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>][0]" value="">
                        </div>
                      </div>
                      <div class="form-group col-md-3 mx-0 ">
                        <label for="input-10" class="w-100 col-form-label"> Amount <span class="required">*</span></label>
                        <div class="w-100">
                          <input type="text" class="form-control" placeholder="Amount " name="value[<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>][0]" value="">
                        </div>
                      </div>
                      <div class="form-group col-md-2   mt-auto text-center">
                        <button type="button" onclick="addSlabDiv(<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>,0)" class="btn btn-sm btn-info mb-2"><i class="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php // } ?>
              <div class=" w-100 px-3 appendCLass_<?php echo $DeductionTypeData['salary_earning_deduction_id']; ?>"></div>
            <?php  } ?>
          </div>
        </fieldset>
      </div>
    </div>
    <input type="hidden" id="salary_common_value_id" name="salary_common_value_id" value="<?php if ($data['salary_common_value_id'] != "") {echo $data['salary_common_value_id'];} ?>">
    <div class="form-footer text-center">
<?php //IS_1019 addPenaltiesBtn 
if (isset($salary_group_id) && $salary_group_id>0) {
  ?>
  <button id="addSalaryCommonValueBtn"  type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
  <input type="hidden" name="addSalaryCommonValue" value="addSalaryCommonValue">
<?php //IS_837 onclick="resetFrm('penaltyAdd');" 
?>
<button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addSalaryCommonValueFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
<?php } else {
  ?>
  <button type="submit" disabled class="btn btn-success saveBtn"><i class="fa fa-check-square-o"></i> <?php if (isset($bId) && $bId > 0) {echo "Update";} else {echo "Add";} ?></button>
  <input type="hidden" name="addSalaryCommonValue" value="addSalaryCommonValue">
  <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addSalaryCommonValueFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
<?php } ?>
</div>
</form>
</div>
</div>
</div>
</div>
<!--End Row-->

</div>
<!-- End container-fluid-->

</div>
<!--End content-wrapper-->

<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">

$(document).on("change", ".changeEmployerAmount", function() {
   
   dataId = $( this ).attr('data-id');
   employerAmnt = $( this ).val();
  
   if(employerAmnt>0){
    $('.showOnChangeEmplrAmnt_'+dataId).removeClass('d-none');
   }else{
    $('.showOnChangeEmplrAmnt_'+dataId).addClass('d-none');
   }

});


  ernIdarrayTemp = [];
  $('input[name="salary_common_value_checkbox[]"]:checked').each(function() {
    type = $(this).attr('data-type');
    name = $(this).attr('data-name');
    value = $(this).val();
    if (type == "earn") {
      ernIdarrayTemp.push(value);
    }
  });

  if (ernIdarrayTemp.length>0) {
    $(".saveBtn").removeAttr("disabled");
  } else {
    $(".saveBtn").attr("disabled", true);
  }

  function checkEarnType(value, id) {
   // 
    if (parseInt(value) == 1) {
      $('.checkEarnTypeHide_' + id).hide();
      $('#amountValueId_'+id).attr('placeholder','Amount *');
     /// checkAndSetMultiEarn('earn',id,'0')
      $('#amountValueId_'+id).removeAttr('max');
    } else {
      checkValuMultichackoxEarn(id)
      $('.checkEarnTypeHide_' + id).show();
      $('#amountValueId_'+id).attr('placeholder','Percentage *');
      $('#amountValueId_'+id).attr('max',100);
    ///  checkAndSetMultiEarn('earn',id,'1')   
     }
   
  }


  function checkType(type, id) {
    checkValuMultichackoxEarn(id)
    $('#' + id).rules("add", {
      required: true,
      messages: {
        required: "Please Enter Value",
      }
    });
    if (type == 0) {
      $('.newMaxMInpercent_'+id).removeClass('tempHide');
      $('.newMaxMInpercent_'+id).removeClass('checkBoxH');
      $('#selectEarnType_' + id).show();
      $('.checkBoxHide_' + id).show();
      $('.checkBoxHide_' + id).removeClass('d-none');
      $('#selectDeductPrcentAmount_' + id).show();
      $('.changeAmountLable_' + id).text('Percent');
      $('.changeAmountLable_' + id).show();
      $('.hideAmountLable_'+id).removeClass('d-none');
      $('.amountMax_' + id).attr('max', 99);
      $('#selectEarnType_' + id).removeClass('tempHide');
      $('.showOnChangeEmplrAmnt_' + id).addClass('d-none');
      $('#hideSlab_' + id).css('display', 'none');
      $('.amountMax_' + id).show();
      $('.appendCLass_'+id).empty();
      multiEarnChnages();
// deductMaxAamountPercent();

    } else if (type == 2) {
      ///$('#amountMax_' + value).rules("remove");
      $('.changeAmountLable_' + id).text('Amount');
      $('.changeAmountLable_' + id).show();
      $('#hideSlab_' + id).show();
      $('.hideAmountLable_'+id).addClass('d-none');
      $('.changeAmountLable_' + id).hide();
// $('.extraDiv').show();
      $('.newMaxMInpercent_'+id).addClass('tempHide');
      $('.appendCLass_'+id).empty();
      $('.amountMax_' + id).hide();
//  $('#selectEarnType_' + id).css('display', 'none');
      $('#selectEarnType_' + id).show();
      $('#selectEarnType_' + id).removeClass('tempHide');
      $('.showOnChangeEmplrAmnt_' + id).addClass('d-none');

      $('#selectDeductPrcentAmount_' + id).css('display', 'none');
      salbValidation()
      addRuels()
    } else if (type == 1) {
      $('.hideAmountLable_'+id).removeClass('d-none');

     // $('.newMaxMInpercent_'+id).addClass('tempHide');
      $('.changeAmountLable_' + id).text('Amount');
      $('.changeAmountLable_' + id).show();
      $('.amountMax_' + id).show();
      $('.amountMax_' + id).removeAttr("max");
      $('#selectEarnType_' + id).css('display', 'none');
      $('#selectDeductPrcentAmount_' + id).css('display', 'none');
      $('#hideSlab_' + id).css('display', 'none');
// $('.extraDiv').hide();
      $('.appendCLass_'+id).empty();
      $('.showOnChangeEmplrAmnt_' + id).addClass('d-none');
      $('.newMaxMInpercent_' + id).removeClass('tempHide');

      addRuels()

    } else {
      $('.newMaxMInpercent_'+id).addClass('tempHide');
      $('.hideAmountLable_'+id).removeClass('d-none');
      $('.changeAmountLable_' + id).text('Amount');
      $('.changeAmountLable_' + id).show();
      $('.amountMax_' + id).show();
      $('#selectEarnType_' + id).css('display', 'none');
      $('#hideSlab_' + id).css('display', 'none');
// $('.extraDiv').hide();
      $('.appendCLass_'+id).empty();
      $('#selectDeductPrcentAmount_' + id).css('display', 'none');
      $('.showOnChangeEmplrAmnt_' + id).addClass('d-none');

      addRuels()
    }
  }
  var clicks = 0;
  function addSlabDiv(i,j) {
    if(j==0){
      clicks += 1;
    }else{
      if(clicks==0){
        clicks = j;
        clicks += 1;
      }else{
        clicks += 1;
      }
      
    }
   
    addHtmlData = `<div class=" row h_` + clicks + `  extraDiv" > 
    <div class="col-md-8 offset-md-4">  
    <div class="row px-2">
    <div class="form-group col-md-3  mx-0 ">
    <label for="input-10" class="w-100 col-form-label"> Min <span class="required">*</span></label>
    <div class="w-100" >
    <input type="text" class="form-control minClass minMaxClass  onlyNumber minUniqcls_` + (clicks+1) + ` min_` + clicks + `" id=
    "min_` + clicks + `" placeholder=" Min " minData-id="` + (clicks+1) + `" name="min[` + i + `][` + (clicks+1) + `]" value="">
    </div>                                            
    </div>
    <div class="form-group col-md-3  mx-0 ">
    <label for="input-10" class="w-100 col-form-label"> Max <span class="required">*</span></label>
    <div class="w-100" >
    <input type="text" id=
    "max_` + clicks + `" maxData-id="` + (clicks+1) + `" class="form-control maxuniqCls_` + (clicks+1) + ` minMaxClass max_` + clicks + `" placeholder=" Max " name="max[` + i + `][` + (clicks+1) + `]" value="">
    </div>                                            
    </div>
    <div class="form-group col-md-3  mx-0 ">
    <label for="input-10" class="w-100 col-form-label"> Amount <span class="required">*</span></label>
    <div class="w-100" >
    <input id=
    "value_` + clicks + `" type="text" class="form-control" placeholder="Amount " name="value[` + i + `][` + (clicks+1) + `]" value="">
    </div>                                            
    </div>
    <div class="form-group col-md-2   mt-auto text-center">
    <button type="button" r-id="` + clicks + `"  data-id="` + i + `" class="btn removeClick btn-sm btn-danger mb-2"><i class="fa fa-minus"></i></button>                                           
    </div>
    </div>
    </div>
    </div> `;
    $('.appendCLass_' + i).append(addHtmlData);
    var numItems = $('.extraDiv').length;
//    console.log(numItems);
    salbValidation()
  }

  $(document).on("change", ".minMaxPercent", function() {
   
      dataId = $( this ).attr('data-id');
    maxAmount = $('.maxPercent_'+dataId).val();
    minAmount = $('.minPercent_'+dataId).val();
    if (minAmount !="" && minAmount !="") {
      $('.maxPercent_'+dataId).attr('min',minAmount);
      
    }
   
    
  });
  $(document).on("change", ".minMaxClass", function() {
    if($( this ).hasClass( "minClass" )){
      dataId = $( this ).attr('minData-id');
    }else{
      dataId = $( this ).attr('maxData-id');
    }
    maxAmount = $('.maxuniqCls_'+dataId).val();
    minAmount = $('.minUniqcls_'+dataId).val();
    if (minAmount !="" && minAmount !="") {
      $('.maxuniqCls_'+dataId).attr('min',minAmount);
      
    }
   
    
  });
  // $(document).on("change", ".multiEarning", function() {
 
  $(document).on("click", ".removeClick", function() {
    id = $(this).attr('r-id');
    $('.h_' + id).empty();
  });
  $(window).bind("load", function() {
    bid = '<?php echo $bId; ?>';
    if (bid == 0) {
      
      checkCkdEarn();
    }
    addRuels();
    addAmntRuels();
  });
  ernIdarrayTempNew =[];
  function checkCkdEarn(checkBoxType,newId) {
    var checkedValue = $('.earning_deduction_checkbox:checked').val();

    ernIdarrayTemp = [];
    $('input[name="salary_common_value_checkbox[]"]:checked').each(function() {
      type = $(this).attr('data-type');
      name = $(this).attr('data-name');
      value = $(this).val();
      if (type == "earn") {
        ernIdarrayTemp.push(value);
        ernIdarrayTempNew.push(value);
      }
    });

    if (ernIdarrayTemp.length>0) {
      $(".saveBtn").removeAttr("disabled");
    } else {
      $(".saveBtn").attr("disabled", true);
    }


    $('input[name="salary_common_value_checkbox[]"]:unchecked').each(function() {
      idVal = $(this).val();
      $('.checkBoxHide_' + idVal).hide();
      $('#amount_type_' + idVal).val("1").trigger("change");
      $('#selectEarnType_' + idVal).hide();
    });
    data = "";
    dataArray =[];
    dataArrayVal =[];
    
    $('input[name="salary_common_value_checkbox[]"]:checked').each(function() {
      type = $(this).attr('data-type');
      name = $(this).attr('data-name');
      value = $(this).val();
      
     
      amount_type = $('#amount_type_' + value).val();
      $('.checkBoxHide_' + value).show();
      if (amount_type == "0") {
        $('.checkEarnTypeHide_' + value).show();
      } else {
        $('.checkEarnTypeHide_' + value).hide();
      }
      if (type == "earn") {
        if(name !=undefined && value !=null){

          // dataArray[value]=name;

          dataArrayVal.push(value);
          dataArray.push(name);
        }
        data += `<option   value="` + value + `">` + name + `</option>`;
      } else {
       // $('#amount_type_' + value).rules("remove");
       // $('#amountMax_' + value).rules("remove");
      }
    });
    ///checkAndSetMultiEarn(checkBoxType)

    checkValuMultichackoxEarn(newId)


  }


function checkValuMultichackoxEarn(newId){

  $('input[name="salary_common_value_checkbox[]"]:checked').each(function() {
      type = $(this).attr('data-type');
      name = $(this).attr('data-name');
      value = $(this).val();
      idsData = $('.salary_common_value_earn_deduction_ids_'+this.id).val();
      if(idsData==""){
        idsData = $('#checkedDataId_'+this.id).val();
      }
      var amount_type = $('#amount_type_'+value).val();

    
      if (type == "earn") {
        newData = "";
        $.each(dataArrayVal, function( index,e_d_id ) {
         
           if(jQuery.inArray(type, dataArrayVal) !== -1){
           }else{
            if(value !=e_d_id){
              if(jQuery.inArray(e_d_id, idsData) !== -1){
                  sl = "selected";
              }else{
                sl = "";
              }
              newData += `<option `+sl+`  value="` + e_d_id + `">` + dataArray[index] + `</option>`;
            }
           }
           
         })
         if(amount_type ==0 || amount_type ==2){
           $('#checkedDataId_'+value).html(newData);
          }
      }else{
        newData ='';
        $.each(dataArrayVal, function( index,e_d_id ) {
           
          if(jQuery.inArray(e_d_id, idsData) !== -1){
                  sl2 = "selected";
              }else{
                sl2 = "";
              }
          newData += `<option  `+sl2+` value="` + e_d_id + `">` + dataArray[index] + `</option>`;
         })
         if(amount_type ==0 || amount_type ==2){
          $('#checkedDataId_'+value).html(newData);
          }
      }
      
    });
}


function checkAndSetMultiEarn(checkBoxType,valueId='',status=""){

  if (checkBoxType == "earn") {
      $("[name^=multiEarning]").each(function() {
        idsData = $('#'+this.id).val();
        setId = this.id;
        // alert(setId)
        if(idsData !="" && idsData !=null){
          data2 = "";

          $.each(dataArrayVal, function( index,e_d_id ) {
           
            if(jQuery.inArray(e_d_id, idsData) !== -1){
              sl = "selected";
            }else{
              sl = "";
            }
            data2 += `<option `+sl+`  value="` + e_d_id + `">` + dataArray[index] + `</option>`;
          })
        
          $('#'+setId).html(data2);
        }
      });
// $('.checkedData').html(data);

    }

}

  function addRuels() {
    $('.amountMax').each(function () {
        $(this).rules("add", 
            {
                required: true,
                messages: {
                    required: "This Field Required",
                }
            })
    });

  }

  function addAmntRuels() {
    
    // $("[name^=amount_type]").each(function() {
    //   id = $(this).attr('data-id');
    //   $('#amount_type_' + id).rules("add", {
    //     required: true,
    //     noSpace: true,
    //     messages: {
    //       required: "Please Select Type",
    //     }
    //   });
    // });

  }

  function multiEarnChnages() {
    $("[name^=multiEarning]").each(function() {
      $('#' + this.id).rules("add", {
        required: true,
        messages: {
          required: "Please select Earning",
        }
      });
    });
  }
/* function deductMaxAamountPercent() {
$("[name^=percent_deduction_max_amount]").each(function() {
$('#' + this.id).rules("add", {
required: true,
messages: {
required: "Please select Earning",
}
});
});
} */

  function salbValidation() {

    var validator = $("#addSalaryCommonValueFrom").validate();
    $('#addSalaryCommonValueFrom').valid();
    $('.minClass').each(function () {
        $(this).rules("add", 
            {
                required: true,
                messages: {
                    required: "Please Enter Minimum Amount",
                }
            })
    });
   
    $("[name^=max]").each(function() {
      $(this).rules("add",{
        required: true,
        messages: {
          required: "Please Enter max value ",
        }
      });
    });
    $("[name^=value]").each(function() {
      $(this).rules("add",{
        required: true,
        messages: {
          required: "Please Enter Value",
        }
      });
    });

  }

  function changesEarnForDeduct(earnId) {
    ernIdarray = [];
    valueesAr = [];
    valuees = [];

    valuees = $("#checkedDataId_" + earnId).val();

    $('input[name="salary_common_value_checkbox[]"]:checked').each(function() {
      type = $(this).attr('data-type');
      name = $(this).attr('data-name');
      value = $(this).val();
      if (type == "earn") {
        ernIdarray.push(value);
      }
    });
// console.log(valuees);

    if(valuees !== null){
      if (valuees.length > 0) {
        $.each(valuees, function(index, ids) {
          if (jQuery.inArray(ids, ernIdarray) == -1) {
            swal('Please select Earning type first !')
            valuees.splice($.inArray(earnId, valuees), 1);
            valueesAr.push(valuees);
            selectedSet(valueesAr, earnId);
          }
        });

      }
    }
  }

  function selectedSet(valuees, earnId) {
 
    $("#checkedDataId_" + earnId).select2('val', valuees);
  }


  $('form.addSalaryCommonValueFrom').on('submit', function (event) {
    ///event.preventDefault();

    var validator = $("#addSalaryCommonValueFrom").validate({
        errorPlacement: function (error, element) {
            // if (element.hasClass('editor-error-placement')) {
            //     error.insertAfter(element.next('div'));  // editor
            // }
            // else 
            if (element.hasClass('multiple-select')) {
                 error.insertAfter(element.parent('div'));  // editor
             } else {
                error.insertAfter(element);               // default
            }
        },
        ignore: [],
        errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        salary_earning_deduction_id: {
            required: true,
        },
        
        salary_group_name:{
            required: true,
            noSpace:true,
        },
        // "multiEarning[][]": {
        //     required: true,
        // },
        /* "amount_value[]": {
            required: true,
        }, */
        amount_type:{
            required: true,
        },
       /*  amount_value:{
            required: true,
            noSpace:true
        }, */
    },
    messages: {
        salary_earning_deduction_id:{
            required: "Please select salary earning deduction type",
        },
        salary_group_name:{
            required: "Please enter salary group name",
        },
        // "multiEarning[]":{
        //     required: "Please Select Earnings ",
        // },
        amount_type:{
            required: "Please Select amount type",
        },
        /* "amount_value[]":{
            required: "Please Select amount",
        }, */

    },
    
    });
    if($('#extra_fixed').val()=='1'){

      $('.fixed_amount_for_fixed').each(function () {
          $(this).rules("add", 
              {
                  required: true,
                  min:1,
                  messages: {
                      required: "Please Enter fixed Amount",
                  }
              })
      });
    }
    if($('#extra_hour').val()=='1'){
    $('.fixed_amount_for_per_hour').each(function () {
        $(this).rules("add", 
            {
                required: true,
                min:1,
                messages: {
                    required: "Please Enter fixed Amount",
                }
            })
    });
  }
  if($('#extra_day').val()=='1'){
    $('.fixed_amount_for_per_day').each(function () {
        $(this).rules("add", 
            {
                required: true,
                min:1,
                messages: {
                    required: "Please Enter fixed Amount",
                }
            })
    });
  }
    $('.salary_group_name').each(function () {
        $(this).rules("add", 
            {
                required: true,
               
                messages: {
                    required: "Please enter salary group name",
                }
            })
    });
   
    var baseAmount = 1000.00;
    var amountType = 0;
    var earnCount = 0;
    var flatEarnCount = 0;
    var percentAr = [];
    var percentAplyOn = [];
    var ernIdsWithVAlue = [];
     
    $('input[name="salary_common_value_checkbox[]"]:checked').each(function() {
      if($(this).hasClass('earnCheckBox')){
        earnCount++;
          checkBoxId = $(this).attr('data-id');
          amount_type =  $('#amount_type_'+checkBoxId).val();
          if(amount_type==0){
            amountType++;
            amountValue =  $('#amountValueId_'+checkBoxId).val();
            checkedData =  $('#checkedDataId_'+checkBoxId).val();
            percentAr.push(amountValue);
            percentAplyOn.push(checkedData);
          }
          if(amount_type==1){
            flatEarnCount++;
          }
      }
      
    });
    console.log('flatEarnCount'+flatEarnCount)
    console.log('earnCount'+earnCount)
    console.log('amountType'+amountType)
    var v = $('#addSalaryCommonValueFrom').valid();
    if(flatEarnCount ==earnCount){
      swal("Please set atleast One Earn as a Percent");
          event.preventDefault();
    }else{
      finaResultAmount = 0;
   if(amountType>0){
      if(percentAr.length>0){
        for (let g = 0; g < percentAr.length; g++) {
          if(percentAplyOn[g] !="" && percentAplyOn[g] !=null){
          }else{
            tempAMount =(baseAmount*percentAr[g])/100;
            finaResultAmount = finaResultAmount+tempAMount;
          }
        }
      }
      var filteredPercentAplyOn = percentAplyOn.filter(function (el) {
        return el != null;
      });
    ///  console.log(filteredPercentAplyOn);
       if(filteredPercentAplyOn.length>0){
        newTempAmount = 0;
        newTempAmountOld = 0;
        actualAmount = 0;
         for (let h = 0; h < percentAr.length; h++) {
         
          if(percentAplyOn[h] !="" && percentAplyOn[h] !=null){
            newfinaResultAmount = 0;
            for (let j = 0; j < percentAplyOn[h].length; j++) {
              applyAmountValue =  $('#amountValueId_'+percentAplyOn[h][j]).val();
              test =(baseAmount*applyAmountValue)/100;
              newfinaResultAmount = newfinaResultAmount+test;
            }
            testTempAmount = (newfinaResultAmount*percentAr[h])/100;
            testTemppercentOnBase = (testTempAmount*100)/baseAmount;
           newTempAmountOld = newTempAmountOld+testTempAmount;
            newTempAmount = newTempAmount+testTemppercentOnBase;
             actualAmount = (newTempAmount*baseAmount ) / 100;
            
            
          }
          }
          finaResultAmount = actualAmount+finaResultAmount;
         
       }
       showOverAllPercent =(finaResultAmount/baseAmount ) * 100;
      if(finaResultAmount==baseAmount){
        if ($('form.addSalaryCommonValueFrom').validate().form()) {
           // event.preventDefault();
          } else {
              event.preventDefault();
          }
        }else{
          swal("Please set total value 100 % Percent , your current total value is "+showOverAllPercent.toFixed(2) +"%");
          event.preventDefault();
        }
    }else{
      if ($('form.addSalaryCommonValueFrom').validate().form()) {
        
      } else {
          event.preventDefault();
      }
    }
    }
    
    
 
     
    
   

});

function extraPayOut(payoutValue,keyForValue) {
 
  if(payoutValue=='0'){
    $('.showSubSelect_'+keyForValue).addClass('d-none');
  }else{
    $('.showSubSelect_'+keyForValue).removeClass('d-none');
  }
}

function allowExtra(allowValue,allowKey) {
  $('#extra_'+allowKey).val("0").trigger('change');
  if(allowValue=='0'){
    $('.showSelect_'+allowKey).addClass('d-none');
    // if($('#extra_'+allowKey).val()=="0"){
    //   $('.showSubSelect_'+allowKey).addClass('d-none');
    // }
  }else{
    $('.showSelect_'+allowKey).removeClass('d-none');
    // $('.showSubSelect_'+allowKey).removeClass('d-none');
  }
  checkAllAllowType();
}

function checkAllAllowType(){
  allow_extra_day_payout_per_day = $('#allow_extra_day_payout_per_day').val();
  allow_extra_day_payout_per_hour = $('#allow_extra_day_payout_per_hour').val();
  allow_extra_day_payout_fixed = $('#allow_extra_day_payout_fixed').val();
  
  if(allow_extra_day_payout_per_day=='1' || allow_extra_day_payout_per_day=='1' || allow_extra_day_payout_fixed =="1"){
    $('.formulaDiv').removeClass('d-none');
  }else{
    $('.formulaDiv').addClass('d-none');

  }
}

function extraHrsCalculation(hrsCalValue) {
  if(hrsCalValue=="1"){
    $('.hrsCal').removeClass('d-none');
  }else{
    $('.hrsCal').addClass('d-none');
  }
}
</script>
<style>
  .tempHide {
    display: none;
  }

  .hideSlab {
    display: none;
  }

  .checkEarnTyp {
    display: none;
  }
  .displayBlock {
    display: none;
  }
</style>