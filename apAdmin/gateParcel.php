<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Gate Parcel</h4>
     </div>
     <div class="col-sm-3">
     </div>
     </div>
    <!-- End Breadcrumb-->
     <div class="row">
     </div>

     <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="">
          <ul class="nav nav-tabs nav-tabs-info nav-justified">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#tabe-13"> Expected </span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-14"> Collected </span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link  show" data-toggle="tab" href="#tabe-16">Delivered</span></a>
            </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div id="tabe-13" class="container-fluid tab-pane active show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable1" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>ID</td>
                          <td>Parcel Photo</td>
                          <td>Company </td>
                          <td>Date</td>
                          <td><?php echo $xml->string->unit; ?> Name</td>
                          <td>No Of <br>Parcels</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("parcel_collect_master,unit_master,block_master,users_master,visitors_master","
                          visitors_master.parcel_collect_master_id =parcel_collect_master.parcel_collect_master_id and  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.unit_id=unit_master.unit_id AND  parcel_collect_master.society_id='$society_id' AND parcel_collect_master.parcel_status=0  AND   parcel_collect_master.user_id=users_master.user_id $blockAppendQuery ","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                           $visit_logo="";
                          $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]' ");
                            $vistLogo=mysqli_fetch_array($fd);
                            if(mysqli_num_rows($fd) > 0 ) {
                               if ($vistLogo['visitor_sub_image']!='') {
                                    $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                                } else {
                                    $visit_logo="";
                                }
                            }
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo 'PAR0'.$data['parcel_collect_master_id']; ?></td>
                          <td><a href="../img/parcel/<?php echo $data['parcel_photo'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["remark"]; ?>"><img width="30" onerror="this.src='../img/MyParcelNew.png'" height="30" src="../img/parcel/<?php echo $data['parcel_photo']; ?>"></a></td>
                          <td><a href="<?php echo $visit_logo; ?>" data-fancybox="images" ><img width="30" onerror="this.src='../img/MyParcelNew.png'" height="30" src="<?php echo $visit_logo; ?>"></a></td>
                          <td><?php echo date("d-m-Y h:i A", strtotime($data['visit_date'].' '.$data['visit_time'])); ?></td>
                          <td><?php echo $data['user_full_name']; ?>-<?php echo $data['user_designation']; ?> (<?php echo $data['block_name']; ?>) </td>
                          <td><?php echo $data['no_of_parcel'];?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="tabe-14" class="container-fluid tab-pane fade ">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable2" class="table table-bordered" style="width: 100% !important;">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>ID</td>
                          <td>Parcel Photo</td>
                          <td>Company </td>
                          <td>Delivery Boy </td>
                          <td>Delivery Boy Mobile</td>
                          <td>Date Collected</td>
                          <!-- <td>Delivery Date</td> -->
                          <td><?php echo $xml->string->unit; ?> Name</td>
                          <td>No Of <br>Parcels</td>
                          <td>Security</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("parcel_collect_master,unit_master,block_master,users_master ,employee_master","
                          employee_master.emp_id=parcel_collect_master.collected_by AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.unit_id=unit_master.unit_id AND  parcel_collect_master.society_id='$society_id' AND parcel_collect_master.parcel_status=1  AND   parcel_collect_master.user_id=users_master.user_id $blockAppendQuery ","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                            $visit_logo="";
                          $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]' ");
                          $vistLogo=mysqli_fetch_array($fd);
                          if(mysqli_num_rows($fd) > 0 ) {
                             if ($vistLogo['visitor_sub_image']!='') {
                                  $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                              } else {
                                  $visit_logo="";
                              }
                          }
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo 'PAR0'.$data['parcel_collect_master_id']; ?></td>
                          <td><a href="../img/parcel/<?php echo $data['parcel_photo'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["remark"]; ?>"><img class="lazyload" width="30" onerror="this.src='../img/MyParcelNew.png'" src="../img/MyParcelNew.png" height="30" data-src="../img/parcel/<?php echo $data['parcel_photo']; ?>"></a></td>
                             <td><a href="<?php echo $visit_logo; ?>" data-fancybox="images" ><img width="30" onerror="this.src='../img/MyParcelNew.png'" height="30" src="<?php echo $visit_logo; ?>"></a></td>
                          <td><?php echo $data['delivery_boy_name'];?></td>
                          <td><?php echo $data['db_country_code'].' '.$data['delivery_boy_number']; ?></td>
                          <td><?php echo date("d-m-Y h:i A", strtotime($data['parcel_collected_date'])); ?></td>
                          <!--  <td><?php echo date("d-m-Y h:i A", strtotime($data['parcel_deliverd_date'])); ?></td> -->
                          <td><?php echo $data['user_full_name']; ?>-<?php echo $data['user_designation']; ?> (<?php echo $data['block_name']; ?>) </td>
                           <td><?php echo $data['no_of_parcel'];?></td>
                           <td><?php echo $data['emp_name']; ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="tabe-16" class="container-fluid tab-pane show">
              <div class="">
                <div class="">
                  <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                  <div class="">
                    <div class="table-responsive">
                      <table id="default-datatable3" class="table table-bordered" style="width: 100% !important;">
                     <thead>
                        <tr>
                          <td>#</td>
                          <td>ID</td>
                          <td>Parcel Photo</td>
                          <td>Company </td>
                          <td>Delivery Boy </td>
                          <td>Delivery Boy Mobile</td>
                          <td>Date Collected</td>
                          <td>Delivery Date</td>
                          <td><?php echo $xml->string->unit; ?> Name</td>
                          <td>No Of <br>Parcels</td>
                          <td>Collected by(Security)</td>
                          <td>Delivered by(Security)</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        extract(array_map("test_input" , $_POST));
                        $q=$d->select("parcel_collect_master,unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND parcel_collect_master.unit_id=unit_master.unit_id AND  parcel_collect_master.society_id='$society_id' AND parcel_collect_master.parcel_status=2  AND   parcel_collect_master.user_id=users_master.user_id $blockAppendQuery ","ORDER BY parcel_collect_master.parcel_collect_master_id DESC LIMIT 500");
                        while ($data=mysqli_fetch_array($q)) {
                           $visit_logo="";
                          $fd=$d->selectRow("visitor_sub_image","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]' ");
                          $vistLogo=mysqli_fetch_array($fd);
                          if(mysqli_num_rows($fd) > 0 ) {
                             if ($vistLogo['visitor_sub_image']!='') {
                                  $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                              } else {
                                  $visit_logo="";
                              }
                          }
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo 'PAR0'.$data['parcel_collect_master_id']; ?></td>
                          <td><a href="../img/parcel/<?php echo $data['parcel_photo'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["remark"]; ?>"><img class="lazyload" width="30" onerror="this.src='../img/MyParcelNew.png'" src="../img/MyParcelNew.png" height="30" data-src="../img/parcel/<?php echo $data['parcel_photo']; ?>"></a></td>
                            <td><a href="<?php echo $visit_logo; ?>" data-fancybox="images" ><img width="30" onerror="this.src='../img/MyParcelNew.png'" height="30" src="<?php echo $visit_logo; ?>"></a></td>
                          <td><?php echo $data['delivery_boy_name'];?></td>
                          <td><?php echo $data['db_country_code'].' '.$data['delivery_boy_number']; ?></td>
                          <td><?php echo date("d-m-Y h:i A", strtotime($data['parcel_collected_date'])); ?></td>
                           <td><?php echo date("d-m-Y h:i A", strtotime($data['parcel_deliverd_date'])); ?></td>
                          <td><?php echo $data['user_full_name']; ?>-<?php echo $data['user_designation']; ?> (<?php echo $data['block_name']; ?>) </td>
                          <td><?php echo $data['no_of_parcel'];?></td>
                          <td><?php
                              $fdu1=$d->selectRow("emp_name,emp_mobile","employee_master","emp_id='$data[collected_by]'");
                              $gatData=mysqli_fetch_array($fdu1);
                               if ($gatData['emp_name']!='') {
                               echo   $collected_by = $gatData['emp_name'].'';
                              }
                           ?></td>
                          <td>
                            <?php
                            $fdu2=$d->selectRow("emp_name,emp_mobile","employee_master","emp_id='$data[deliverd_by]'");
                              $gatData1=mysqli_fetch_array($fdu2);
                               if ($gatData1['emp_name']!='') {
                                echo  $deliverd_by_gatekeeper = $gatData1['emp_name'].'';
                              } ?>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



