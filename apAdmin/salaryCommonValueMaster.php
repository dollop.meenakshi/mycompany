<?php 
error_reporting(0);

$gId = (int)$_REQUEST['gId'];
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$type = $_REQUEST['type'];
$i=1;

if (isset($gId) && $gId > 0) {
  $salaryGroupCategory = "AND salary_common_value_master.salary_group_id='$gId' ";
}
if (isset($type) && $type !="" && $type!="-1") {
  $TypeQuery = "AND salary_earning_deduction_type_master.earning_deduction_type='$type'";
}



    $q=$d->select("salary_group_master,salary_common_value_master,salary_earning_deduction_type_master","salary_group_master.salary_group_id=salary_common_value_master.salary_group_id AND salary_common_value_master.society_id='$society_id' AND  salary_common_value_master.salary_earning_deduction_id=salary_earning_deduction_type_master.salary_earning_deduction_id $salaryGroupCategory $TypeQuery $blockAppendQuery" ,"ORDER BY salary_common_value_master.salary_common_value_id DESC");
    $earnArray= array();
    $deductArray= array();
    while ($data=mysqli_fetch_array($q)) {
      $allow_extra_day_payout_per_day=$data['allow_extra_day_payout_per_day'];
      $extra_day_payout_type_per_day=$data['extra_day_payout_type_per_day'];
      $fixed_amount_for_per_day=$data['fixed_amount_for_per_day'];
      $allow_extra_day_payout_per_hour=$data['allow_extra_day_payout_per_hour'];
      $extra_hour_calculation=$data['extra_hour_calculation'];
      $extra_hour_calculation_minutes=$data['extra_hour_calculation_minutes'];
      $extra_day_payout_type_per_hour=$data['extra_day_payout_type_per_hour'];
      $fixed_amount_for_per_hour=$data['fixed_amount_for_per_hour'];
      $allow_extra_day_payout_fixed=$data['allow_extra_day_payout_fixed'];
      $extra_day_payout_type_fixed=$data['extra_day_payout_type_fixed'];
      $fixed_amount_for_fixed=$data['fixed_amount_for_fixed'];
      if($data['earning_deduction_type']==0){
        array_push($earnArray,$data);
      }else{
        array_push($deductArray,$data);
      }
    }
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Salary Group</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6 text-right">
            <a href="addSalaryCommonValue" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Group</a>
            <!-- <a href="javascript:void(0)" onclick="DeleteAll('deleteSalaryCommonValue');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
        </div>
     </div>
     <form action="" class="branchDeptFilter">
        <div class="row pt-2 pb-2">
            <div class="col-md-6 form-group">
                 <select name="gId" class="form-control single-select">
                    <option value="">Select Group</option>
                    <?php $qs=$d->select("salary_group_master","");
                    while($sgData=mysqli_fetch_array($qs)) { ?>
                    <option <?php if(isset($sgData) && $sgData['salary_group_id']=="$gId" ){echo "selected"; }?> value="<?php echo $sgData['salary_group_id'];?>"><?php echo $sgData['salary_group_name'];?></option>
                    <?php } ?>
              </select>
            </div>
            <div class="col-md-3 form-group">
              <select name="type" class="form-control single-select">
                <option value="-1">All</option>
                <option <?php if(isset($type) && $type=="0" ){echo "selected"; }?> value="0">Earning</option>
                <option  <?php if(isset($type) && $type==1){ echo "selected"; }?> value="1">Deduction</option>
              </select>
            </div>
          <div class="col-md-1 form-group ">
            <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
          </div>
        </div>
        </form>
          <?php if (isset($gId) && $gId >0 && $gId !="" &&(mysqli_num_rows($q)>0)) { ?>
            <div class="row  ">
              <div class="col-md-12 text-right">
              <a href="salaryCommonValueMaster"><button class="btn btn-sm btn-warning   mb-2"><i class="fa fa-eye"></i> View All Group</button></a>

              <a href="addSalaryCommonValue?gId=<?php echo $gId; ?>"><button class="btn btn-sm btn-success   mb-2"><i class="fa fa-pencil"></i> Edit This Group</button></a>
              </div>
            </div>
          
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body ">
                    <div class="row">
                      <?php if(isset($allow_extra_day_payout_per_day)) { ?>
                        <div class="col-md-4">
                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Allow Extra Day Payout (In Per Day Salary):<span> <?php if($allow_extra_day_payout_per_day=="1"){echo "Yes";}else{ echo "No";} ?></span></label>
                        <?php if($allow_extra_day_payout_per_day=="1"){ ?>
                          <label for="input-10" class="col-lg-12 col-md-12 col-form-label"> Extra Day Payout :<span> <?php if($extra_day_payout_type_per_day=="0"){echo "Average";}else{ echo "Fixed";} ?><?php if($extra_day_payout_type_per_day=="1"){ echo "(".$fixed_amount_for_per_day.")"; }?></span></label>

                          <?php } ?>
                        </div>
                      <?php } ?>
                      
                      <?php if(isset($allow_extra_day_payout_per_hour)) { ?>
                        <div class="col-md-4">
                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Allow Extra Hour Payout (In Per Hour Salary):<span> <?php if($allow_extra_day_payout_per_hour=="1"){echo "Yes";}else{ echo "No";} ?></span></label>
                        <?php if($allow_extra_day_payout_per_hour=="1"){ ?>
                          <label for="input-10" class="col-lg-12 col-md-12 col-form-label"> Extra Hour Payout :<span> <?php if($extra_day_payout_type_per_hour=="0"){echo "Average";}else{ echo "Fixed";} ?><?php if($extra_day_payout_type_per_hour=="1"){ echo "(".$fixed_amount_for_per_hour.")"; }?></span></label>

                          <?php } ?>
                         
                          <label for="input-10" class="col-lg-12 col-md-12 col-form-label"> Extra Hour Payout Mode :<span> <?php if($extra_hour_calculation=="0"){echo "After Completed Monthly Hours";}else{ echo "Day By Day";} ?>
                          <?php  if($extra_hour_calculation=="1"){  if($extra_hour_calculation_minutes>0){echo "( Minimum ".$extra_hour_calculation_minutes." Minutes Required)";}  }?></span></label>

                          
                        </div>
                      <?php } ?>
                      <?php if(isset($allow_extra_day_payout_fixed)) { ?>
                        <div class="col-md-4">
                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Allow Extra Fixed Payout (In Per Day Salary):<span> <?php if($allow_extra_day_payout_fixed=="1"){echo "Yes";}else{ echo "No";} ?></span></label>
                        <?php if($allow_extra_day_payout_fixed=="1"){ ?>
                          <label for="input-10" class="col-lg-12 col-md-12 col-form-label"> Extra  Payout :<span> <?php if($extra_day_payout_type_fixed=="0"){echo "Average";}else{ echo "Fixed";} ?><?php if($extra_day_payout_type_fixed=="1"){ echo "(".$fixed_amount_for_fixed.")"; }?></span></label>

                          <?php } ?>
                        </div>
                      <?php } ?>
                      
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="table-responsive">
                        <?php
                        $counter = 1;
                        if (isset($gId) && $gId>0) {
                        ?>
                          <table  class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>                        
                                    <th>Earning</th>
                                    <th>Amount Type</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($earnArray as $val) {?>
                                <tr>
                                  
                                    <td><?php echo $counter++; ?></td>
                                  <td><?php echo $val['earning_deduction_name']; ?></td>
                                  <td><?php if($val['amount_type'] == 0){ echo "Percentage"; }else if($val['amount_type'] == 1){ echo "Flat";} else {echo "Slab"; } ?></td>
                                  <td><?php if($val['amount_type'] == 0){ echo number_format((float)$val['amount_value'], 2, '.', '') .' %'; }else if($val['amount_type'] == 1){ echo $val['amount_value'];} else if($val['amount_type'] == 2) {echo "Slab";  } ?></td>
                                  <td>
                                  <div class="d-flex align-items-center">  
                                        
                                        <button type="button" onclick="ShowCommonValue(<?php echo $val['salary_common_value_id']; ?>)" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-eye"></i></button> 

                                      <?php if($val['salary_common_active_status']=="0"){
                                          $status = "salaryCommonValueStatusDeactive";  
                                          $active="checked";                     
                                      } else { 
                                          $status = "salaryCommonValueStatusActive";
                                          $active="";  
                                      } ?>
                                      <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $val['salary_common_value_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                                  </div>
                                  </td>
                                    
                                </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                      <?php } ?>
                      </div>
                      </div>
                      <div class="col-md-6">
                      <div class="table-responsive">
                      <?php
                      $counter = 1;
                      if (isset($gId) && $gId>0) {
                      ?>
                    <table id="" class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Sr.No</th>                        
                              <th>Deduction</th>
                              <th>Amount Type</th>
                              <th>Value</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php foreach($deductArray as $val) {?>
                          <tr>
                             
                              <td><?php echo $counter++; ?></td>
                             <td><?php echo $val['earning_deduction_name']; ?></td>
                             <td><?php if($val['amount_type'] == 0){ echo "Percentage"; }else if($val['amount_type'] == 1){ echo "Flat";} else {echo "Slab"; } ?></td>
                             <td><?php if($val['amount_type'] == 0){ echo number_format((float)$val['amount_value'], 2, '.', '').' %'; }else if($val['amount_type'] == 1){ echo $val['amount_value'];} else if($val['amount_type'] == 2) {echo "Slab";  } ?></td>
                             <td>
                             <div class="d-flex align-items-center">  
                                  
                                  <button type="button" onclick="ShowCommonValue(<?php echo $val['salary_common_value_id']; ?>)" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-eye"></i></button> 

                                <?php if($val['salary_common_active_status']=="0"){
                                    $status = "salaryCommonValueStatusDeactive";  
                                    $active="checked";                     
                                } else { 
                                    $status = "salaryCommonValueStatusActive";
                                    $active="";  
                                } ?>
                                <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $val['salary_common_value_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                             </div>
                             </td>
                              
                          </tr>
                          <?php } ?>
                      </tbody>
                      
                  </table>
                  <?php } ?>
                  </div>
                      </div>
                    </div>
                   
                  </div>
                </div>
              </div>
            </div><!-- End Row-->

    <?php } else { ?>

          <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive">
                      <?php
                      $counter = 1;
                      ?>
                    <table id="example" class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Sr.No</th>                        
                              <th>Group Id</th>
                              <th>Salary Group Name</th>
                              <th>Employees on This Group</th>
                              <th>Created Date</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php
                        $q= $d->selectRow("salary_group_master.*,(SELECT COUNT(*) FROM salary WHERE salary.salary_group_id = salary_group_master.salary_group_id AND salary.is_delete=0) AS total_group_used","salary_group_master","");
                       while ($data=mysqli_fetch_array($q)) { ?>
                          <tr>
                             
                             <td><?php echo $counter++; ?></td>
                             <td><?php echo "S".$data['salary_group_id']; ?></td>
                             <td><?php echo $data['salary_group_name']; ?></td>
                             <td><?php echo $data['total_group_used']; ?></td>
                             <td><?php echo date("d-m-Y h:i A",strtotime($data['salary_group_created_date'])); ?></td>
                             <td>
                              <div class="d-flex">

                              
                               <form method="post" action="controller/SalaryCommonValueController.php">
                                <a href="salaryCommonValueMaster?gId=<?php echo $data['salary_group_id'];?>" onclick="ShowCommonValue(<?php echo $data['salary_common_value_id']; ?>)" class="btn btn-sm btn-warning mr-2"> <i class="fa fa-eye"></i> Manage</a> 
                                
                                 <?php
                                    if($data['active_status']=="0")
                                    {
                                        $status = "salaryGroupDeactive";
                                        $active="checked";
                                    }
                                    else
                                    {
                                        $status = "salaryGroupActive";
                                        $active="";
                                    }
                                ?>
                                <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['salary_group_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                                
                                  <input type="hidden" name="delete_group_name" value="<?php echo $data['salary_group_name']; ?>">
                                  <input type="hidden" name="delete_salary_group_id" value="<?php echo $data['salary_group_id']; ?>">
                                  <input type="hidden" name="deleteSalaryGroup" value="deleteSalaryGroup">
                                  <?php if ($data['total_group_used']<1) { ?>
                                  <button class="btn btn-danger btn-sm form-btn" type="submit"><i class="fa fa-trash-o"></i></button>
                                  <?php } ?>
                                </form>
                                <a href="addSalaryCommonValue?gId=<?php echo $data['salary_group_id'];?>" class="btn btn-primary btn-sm ml-2"><i class="fa fa-pencil"></i></a>
                                </div>
                             </td>
                              
                          </tr>
                          <?php } ?>
                      </tbody>
                      
                  </table>
                  </div>
                  </div>
                </div>
              </div>
            </div><!-- End Row-->

    <?php } ?>

    </div>
    <!-- End container-fluid-->
    
    </div>


<div class="modal fade" id="editSalaryCommonValue">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Salary Common Value</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           <form id="editCommonValue"  action="controller/SalaryCommonValueController.php" enctype="multipart/form-data" method="post">
              <div class="form-group row">
                  <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Amount Type<span class="required">*</span></label>
                      <div class="col-lg-8 col-md-8" id="">
                          <select name="amount_type"  class="form-control" id="amount_type" >
                              <option value="">-- Select Amount Type --</option>
                              <option value="0">Percentage (%)</option>
                              <option value="1">Flat</option>
                            </select>                    
                      </div>
              </div> 
              <div class="form-group row"> 
                  <label for="input-10" class="col-sm-4 col-form-label">Amount <span class="required">*</span></label>
                  <div class="col-lg-8 col-md-8" id="">
                    <input type="text" class="form-control onlyNumber" placeholder="Amount" name="amount_value" id="amount_value">
                  </div>                   
              </div>  
              <div class="form-footer text-center">
                  <input type="hidden" id="salary_common_value_id" name="salary_common_value_id" value="" >
                  <input type="hidden" id="bId_value" name="bId_value" value="<?php echo $bId; ?>" >
                  <input type="hidden" id="editCommonValue" name="editCommonValue" value="editCommonValue" >
                  <button id="edit_salary_common_value"  name="edit_salary_common_value" type="submit"  class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
              </div>
         </form>
       </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="salaryCommonValueModal">
  <div class="modal-dialog" >
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Salary Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"  style="align-content: center;">
        <div id="salaryCommonValue"></div>
        <div class="col-md-12 row ernDedData">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Deduct from</th>
                          </tr>
                      </thead>
                      <tbody id="SalaryCommonValueEarnDedcutData">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="col-md-12 row SlabData">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Min</th>
                              <th>Max</th>
                              <th>Value</th>
                          </tr>
                      </thead>
                      <tbody id="SalarySlabMinMaxValue">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
  .ernDedData{
    display: none;
  }
</style>
