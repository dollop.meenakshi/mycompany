<?php 
error_reporting(0);
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$con=$d->dbCon();
$base_url=$m->base_url();
$master_url=$d->master_url();
$urlArya = explode("/", $base_url);
$cookieUrl = $urlArya[3];
$language_id = $_COOKIE['language_id'];
$society_id = $_COOKIE['society_id'];
$xml=simplexml_load_file("../img/$language_id.xml");
$bms_admin_id = $_COOKIE['bms_admin_id'];
$aq=$d->select("bms_admin_master","admin_id='$bms_admin_id' ");
$adminData= mysqli_fetch_array($aq);
$access_branchs=$adminData['access_branchs'];
$access_departments=$adminData['access_departments'];
$blockAryAccess = array();
$departmentAry = array();
$blockAryAccess = explode(",", $access_branchs);
$departmentAry = explode(",", $access_departments);

if ($access_branchs!='') {
    $blockids = join("','",$blockAryAccess); 
    if ($access_departments!='') {
      $departmentIds = join("','",$departmentAry); 
      $restictDepartment = " AND floors_master.floor_id IN ('$departmentIds')";
      $restictDepartmentUser = " AND users_master.floor_id IN ('$departmentIds')";
    } else {
      $restictDepartment = "";
      $restictDepartmentUser = "";
    }

    $blockAppendQuery = " AND block_master.block_id IN ('$blockids')";
    $blockAppendQueryUnit = " AND unit_master.block_id IN ('$blockids')";
    $blockAppendQueryFloor = " AND floors_master.block_id IN ('$blockids') $restictDepartment";
    $blockAppendQueryUser = " AND users_master.block_id IN ('$blockids') $restictDepartmentUser";
    $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

}  else {
    $blockAppendQuery = "";
    $blockAppendQueryUnit = "";
    $blockAppendQueryUser = "";
    $blockAppendQueryOnly="";
}


$aq=$d->select("bms_admin_master,role_master","role_master.role_id=bms_admin_master.role_id AND bms_admin_master.admin_id='$bms_admin_id'");
$adminData= mysqli_fetch_array($aq);
$access=$d->select("role_master","role_id='$adminData[role_id]'");
$accessData=mysqli_fetch_array($access);
$accessMenuId=$accessData['menu_id'];
$accessMenuIdArr=explode(",", $accessMenuId);
$pagePrivilegeId=$accessData['pagePrivilege'];
$searchTerm = $_GET['term'];
$searchTerm=mysqli_real_escape_string($con, $searchTerm);
$pagePrivilegeArr=explode(",", $pagePrivilegeId);
$sql = $d->select("master_menu","page_status=0 AND sub_menu=0 ");
$menuArray=array();
if (mysqli_num_rows($sql) > 0) {
  while($row =mysqli_fetch_assoc($sql)) {
    $row = array_map("html_entity_decode", $row);
    if(in_array($row['menu_id'], $accessMenuIdArr)){
      $language_key_name= $row['language_key_name'];
      if ($row['language_key_name']!="") {
        $lngMenuName = ''.$xml->string->$language_key_name;
      } else {
        $lngMenuName = $row['menu_name'];
      }
      $lngMenuNameIgnore = strtolower($lngMenuName);
      $searchTermIgnore = strtolower($searchTerm);
      if (strpos($lngMenuNameIgnore, $searchTermIgnore) !== false) {
        array_push($menuArray,$row);
      }
    }
  }
} 

$sql1 = $d->select("master_menu","page_status=1 AND menu_link='profile' ");
$menuArrayProfile=array();
if (mysqli_num_rows($sql1) > 0) {
  while($row1 =mysqli_fetch_assoc($sql1)) {
      $row1 = array_map("html_entity_decode", $row1);
      $language_key_name= $row1['language_key_name'];
      if ($row1['language_key_name']!="") {
        $lngMenuName = ''.$xml->string->$language_key_name;
      } else {
        $lngMenuName = html_entity_decode($row1['menu_name']);
      }
      $lngMenuNameIgnore = strtolower($lngMenuName);
      $searchTermIgnore = strtolower($searchTerm);
      if (strpos($lngMenuNameIgnore, $searchTermIgnore) !== false) {
        array_push($menuArrayProfile,$row1);
      }
  }
}
$userArray=array();
$userMaintenace=array();
$userBill=array();
if (strlen($searchTerm)>2) {
    
    $sqlUser = $d->selectRow("user_full_name,unit_name,block_name,user_id,user_designation","unit_master,block_master,users_master,floors_master","(users_master.delete_status=0 AND  block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status=1 $blockAppendQuery $blockAppendQueryUser) AND  (users_master.user_full_name LIKE '%$searchTerm%' OR unit_master.unit_name LIKE '%$searchTerm%' OR block_master.block_name LIKE '%$searchTerm%' OR users_master.user_mobile LIKE '%$searchTerm%' OR users_master.user_email LIKE '%$searchTerm%' )","ORDER BY users_master.user_full_name ASC LIMIT 10");


    if (mysqli_num_rows($sqlUser) > 0) {
      while($rowUser =mysqli_fetch_assoc($sqlUser)) {

        $member = array(); 
        $userName = $rowUser['user_full_name'].' ('.$rowUser['user_designation'].'-'.$rowUser['block_name'].')';
        $userName = html_entity_decode($userName);
        $member["menu_id"]=$rowUser['user_id'];
        $member["menu_name"]=$userName;
        $member["menu_link"]='employeeDetails?id='.$rowUser['user_id'];
        $member["language_key_name"]="";
        array_push($userArray,$member);
       }
      }
    }


  $menuArray = array_merge($menuArray,$menuArrayProfile,$userArray);

  $tutorialData = array(); 
  for ($i=0; $i < count($menuArray); $i++) { 
    
    $lngMenuName = $menuArray[$i]['menu_name'];
    $data['id']    = $menuArray[$i]['menu_link']; 
    $data['value'] = $lngMenuName;
      array_push($tutorialData, $data);
  }  
 echo json_encode($tutorialData);
?>