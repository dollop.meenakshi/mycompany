<?php
extract($_REQUEST);
if (filter_var($_REQUEST['event_id'], FILTER_VALIDATE_INT) != true || (isset($_REQUEST['event_id']) && filter_var($_REQUEST['event_id'], FILTER_VALIDATE_INT) != true)) {
	$_SESSION['msg1']="Invalid  Request";
	echo ("<script LANGUAGE='JavaScript'>
		window.location.href='events';
		</script>");
	exit();
}
//IS_1219
$q=$d->select("event_master","society_id='$society_id' AND event_id='$event_id'","");
$row=mysqli_fetch_array($q);
extract($row);
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-9">
				<h4 class="page-title">Book Event</h4>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="welcome">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Book Event</li>
				</ol>
			</div>
		</div>
		<!-- End Breadcrumb-->
		<?php //IS_1026
		if(mysqli_num_rows($q) <= 0  ){ ?>
			<div class="row">
				<div class="col-lg-12">
					<img src='img/no_data_found.png'>
				</div>
			</div>
		<?php } else if($booking_open=="1"){
			echo '<h5>Booking Closed...!</h5>';
		} else {  ?>
			<div class="row">
				<div class="col-lg-12">
				<?php /*if ($facility_type == 1) { ?>
				<form action="" method="get">
					<input type="hidden" name="facility_id" value="<?php echo $facility_id; ?>">
					<div class="form-group row">
						<label for="input-14" class="col-md-3 col-form-label">Check Availability </label>
						<div class="col-md-4 col-8">
							<input  value="<?php if(isset($chk)) { echo $chk;} else { echo $chk=date("Y-m-d"); }?>" type="text" required="" name="chk" id='default-datepicker1122' class="form-control"  >
						</div>
						<div class="col-md-4 col-4">
							<input type="submit"  class="btn btn-success" value="Check" >
						</div>
					</div>
				</form>
			<?php }*/ ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<form id="eventBookFrm" action="controller/eventController.php" method="POST" enctype="multipart/form-data"  >
						<input type="hidden" name="event_id" id="event_id" value="<?php echo $event_id ;?>" >
						
						<h5 class="card-title text-primary text-center"><?php echo $event_title; ?> ( <?php echo date("d-m-Y", strtotime($event_start_date))." - ".date("d-m-Y", strtotime($event_end_date)); ?> )</h5>
						<hr>
						<?php if(trim($event_description) !="") { ?> 
						<div class="form-group row">
							<div class="col-sm-2">
								<label for="input-14" class="col-form-label">Description :</label>
							</div>
							<div class="col-sm-10"><?php echo $event_description; ?></div>
						</div>
					<?php } ?> 
						<div class="form-group row">
							<label for="unit_id_main" class="col-sm-2 col-form-label">Booking for <span class="text-danger">*</span> </label>
							<div class="col-sm-4">
								<select type="text" required="" class="form-control single-select" name="user_id" id="user_id">
									<option value="">-- Select --</option>
									<?php
									$q3=$d->select("unit_master,block_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4","ORDER BY users_master.user_id DESC");
									while ($blockRow=mysqli_fetch_array($q3)) {
										?>
										<option value="<?php echo $blockRow['user_id'];?>"><?php echo $blockRow['block_name'];?>-<?php echo $blockRow['unit_name'];?>  <?php echo $blockRow['user_full_name'];?> <?php if ($blockRow['user_type']==1){ echo "(Tenant)";}else { echo "(Owner)"; }?></option>
									<?php }?>
								</select>

							</div>

							<div class="col-sm-2">
								<label for="input-14" class="col-form-label">Event Date : <span class="required">*</span></label>
							</div>
							<div class="col-sm-4">
								<select class="form-control" name="events_day_id" id="events_day_id" >

									<option value="">--Select--</option>


									<?php
										$event_start_time = strtotime($event_start_date); // or your date as well
										$event_end_time = strtotime($event_end_date. ' +1 day');
										$datediff = $event_end_time - $event_start_time;
										$days =  round($datediff / (60 * 60 * 24));
										$event_days_master_qry=$d->select("event_days_master","society_id='$society_id' and event_id ='$event_id' ","");
										while ($event_days_master_data = mysqli_fetch_array($event_days_master_qry)) {
											?>
											<option value="<?php echo $event_days_master_data['events_day_id']; ?>"><?php echo $event_days_master_data['event_day_name']."( ".date("d-m-Y", strtotime($event_days_master_data['event_date']))." )";?> </option>
										<?php }  ?>
									</select>
								</div>
							</div>
							
							<div id="eventDetailsDiv"></div>
							<div id="eventPersonDetails">  </div>
							<input type="hidden" name="eventPerson" id="eventPerson" value="0">
							<div id="payementDetailsDiv"></div>
							<div id="addBankDetails"> <input type="hidden" name="payment_type" id="payment_type" value=""> </div>
							<input type="hidden" name="isCash" id="isCash" value="">

							<div class="form-group row">
								<div class="col-sm-2">
									<label for="input-14" class="col-form-label">Notes :</label>
								</div>
								<div class="col-sm-4"> <textarea class="form-control" name="notes" id="notes"></textarea></div>
							</div>

							<div class="form-footer text-center">
								<button type="submit" class="btn btn-success" name="bookEvent"><i class="fa fa-check-square-o"></i> Confirm Booking</button>

							</div>



							
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<?php
	}
	?>

	<!-- User Table-->
	<div class="row">
		<div class="col-lg-12">
			<div class="card">

				<div class="card-body">
					<div class="table-responsive">
						<table id="example" class="table table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Unit</th>
									<th>Booked Date</th>
									<th>Person</th>
									<th>Child</th>
									<th>Guests</th>
									<th>Amount</th>
									<th>Payment</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i=1;
								$q1=$d->select("event_attend_list,event_days_master,users_master,unit_master","  unit_master.unit_id =  users_master.unit_id  and users_master.user_id=event_attend_list.user_id and  event_attend_list.events_day_id=event_days_master.events_day_id AND event_days_master.society_id='$society_id' AND event_days_master.event_id = $event_id");
								while ($fmData=mysqli_fetch_array($q1)) {


									?>
									<tr>
										<td><?php echo $i++; ?></td>
										<td><?php echo $fmData['unit_name']; ?></td>
										<td><?php echo $fmData['event_date']; ?></td>


										<td><?php 
										echo $fmData['going_person'] ; ?></td>
										<td><?php 
										echo $fmData['going_child']; ?></td>
										<td><?php 
										echo $fmData['going_guest']; ?></td>
										<td><?php echo $fmData['recived_amount']; ?></td>
										<td><?php if($fmData['event_type']==0) {
											echo "Free Event";
										} else  if($fmData['payment_type']==0) {
											echo "Cash";
										} elseif ($fmData['payment_type']==1) {
											echo "Cheque";
											echo "<br>";
											echo "Bank: ".$fmData['bank_name'];
											echo "<br>";
											echo "Cheque No.: ".$fmData['payment_ref_no'];
										} elseif ($fmData['payment_type']==2) {
											echo "Online";
											echo "<br>";
											echo "Bank: ".$fmData['bank_name'];
											echo "<br>";
											echo "Ref. No.: ".$fmData['payment_ref_no'];
										} elseif ($fmData['payment_type']==4) {
											echo "Un Paid";
										} ?></td>
										<td>
											<?php if ($fmData['book_status'] == 0 ) { ?> 
												<label>Pending</label>

												<?php
											} else if($fmData['book_status']==3){
												echo "<label>Cancelled</label>";
											}  else {
												echo "<label>Confirmed</label>";
											}?>
										</td>
										<td>
											<?php
												//IS_955 &&  $fmData['book_status'] !=3 added
											if ($fmData['payment_type']==4 &&  $fmData['book_status'] !=3   ) { ?>
												<p>
													<div style="display: inline-block;">
														<button onclick="getPayAmount('<?php echo $fmData['recived_amount'] ?>','<?php echo $fmData['event_attend_id'] ?>','<?php echo $fmData['balancesheet_id'] ?>','<?php echo $fmData['events_day_id'] ?>','<?php echo $fmData['event_id'] ?>');" data-toggle="modal" data-target="#payEventModal" class="btn btn-sm btn-secondary ">Pay Now?</button>
													</div>
												</p>
											<?php } 
											if ($fmData['book_status'] == 0) {
												?>
												<p>
													<?php 
													$today= strtotime("today midnight");
													 
		if( strtotime($fmData['event_date']) >= $today   ){ ?>
													<div style="display: inline-block;">
														<form action="controller/eventController.php" method="post" id="statusForm" >

															<input type="hidden" name="approveEvent" value="approveEvent">
															<input type="hidden" name="event_attend_id" value="<?php echo $fmData['event_attend_id']; ?>">
															<input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
															<input type="submit" name="" class="form-btn btn btn-primary btn-sm" value="Approved ?">
														</form>
													</div>
												
													<div style="display: inline-block;">
														<form action="controller/eventController.php" method="post" >
															<input type="hidden" name="cancelEventBooking" value="cancelEventBooking">
															<input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
															<input type="hidden" name="event_attend_id" value="<?php echo $fmData['event_attend_id']; ?>">
															<button type="submit" class="form-btn btn btn-danger btn-sm" name=""><i class="fa fa-times"></i> Cancel</button>
														</form>
													</div>
													<?php }?> 
												</p> 
											<?php } if($fmData['book_status'] == 1){ ?>
												<div style="display: inline-block;">
													<form action="controller/eventController.php" method="post" >
														 <input type="hidden" name="cancelEventBooking" value="cancelEventBooking">
															<input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
															<input type="hidden" name="event_attend_id" value="<?php echo $fmData['event_attend_id']; ?>">
														<button type="submit" class="form-btn btn btn-danger btn-sm" name=""><i class="fa fa-cross"></i> Cancel</button>
													</form>
												</div>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>




</div>
</div>
</div>
</div><!--End Row-->

</div>
</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">


	function getPayAmount(receive_amount,event_attend_id,balancesheet_id,events_day_id,event_id) {
		$('#payEventAmountFrm :input[name="receive_amount"]').val(receive_amount);
		$('#payEventAmountFrm :input[name="event_attend_id"]').val(event_attend_id);
		$('#payEventAmountFrm :input[name="balancesheet_id"]').val(balancesheet_id);
		$('#payEventAmountFrm :input[name="events_day_id"]').val(events_day_id);
		$('#payEventAmountFrm :input[name="event_id"]').val(event_id);
		document.getElementById('PaybleAmount').innerHTML=receive_amount; 
	}



	function getBankDetails(value){

		if(value != '0'){
			$.ajax({
				url: 'ajaxPaymentData.php',
				type: 'POST',
				data: {payment_type_value:value},
			})
			.done(function(response) {
				$('#addBankDetails').html(response);
				$('#isCash').val('1');

			});
		}else{
			$('#addBankDetails').html('');
			$('#isCash').val('');

		}
	}
	$( "#eventBookFrm" ).submit(function( event ) {
		var error = 0;


		if($('#isCash').val()=="" && ($('#payment_type').val() =="1" || $('#payment_type').val() =="2" )  ){
			error++;
			$('#addBankDetails').html('<center><label   style="color: #ff0000;" for="addBankDetails">Please Payment type details</label></center>');
		}

		if($('#isCash').val()=="" && ($('#payment_type').val() =="1" || $('#payment_type').val() =="2" )  ){
			error++;
			$('#addBankDetails').html('<center><label   style="color: #ff0000;" for="addBankDetails">Please Payment type details</label></center>');
		}
		/*if( $('#eventPerson').val()=="0" ){
			error++;
			$('#eventPersonDetails').html('<center><label   style="color: #ff0000;" for="eventPersonDetails">Please Select Number of Person (Adult/Child/guest) for event</label></center>');
		}*/

		if(error > 0 ){
			event.preventDefault();
		} else {
			$( "#eventBookFrm" ).submit();
		}

	});
</script>

<div class="modal fade" id="payEventModal">
	<div class="modal-dialog">
		<div class="modal-content border-primary">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white">Pay Event Amount</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> 
			<div class="modal-body" >
				<form id="payEventAmountFrm" action="controller/eventController.php" method="post">
					<input type="hidden" id="event_attend_id" name="event_attend_id">
					<input type="hidden" id="balancesheet_id" name="balancesheet_id">
					<input type="hidden" id="events_day_id" name="events_day_id">
					<input type="hidden" id="receive_amount" name="receive_amount">
					<input type="hidden" id="event_id" name="event_id">
					
					<div class="form-group row">
						<label for="input-10" class="col-sm-5 col-form-label">Payble Amount</label>
						<div class="col-sm-7" id="PaybleAmount">
						</div>
					</div>
					<div class="form-group row">
						<label for="payment_type" class="col-sm-5 col-form-label">Payment Type</label>
						<div class="col-sm-7">
							<select class="form-control" name="payment_type" onchange="getBankDetailsModal(this.value)">
								<option value="">--Select--</option>
								<option value="0">Cash</option>
								<option value="1">Cheque</option>
								<option value="2">Online</option>
							</select>
						</div>
					</div>
					<div id="getDetailsModal"></div>

					<input type="hidden" name="isCashModal" id="isCashModal" value="">
					<div class="form-footer text-center">
						<button type="submit" name="payEventAmount" class="btn btn-success"><i class="fa fa-check-square-o"></i> Pay Now</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php //IS_910 ?>
<script type="text/javascript">


	$( "#payEventAmountFrm" ).submit(function( event ) {
		var error = 0;
		
		
		if($('#isCashModal').val()=="" && ($('#payEventAmountFrm :input[name="payment_type"]').val() =="1" || $('#payEventAmountFrm :input[name="payment_type"]').val() =="2" )  ){
			error++;
			$('#getDetailsModal').html('<center><label   style="color: #ff0000;" for="addBankDetails">Please Payment type details</label></center>');
		}
		if(error > 0 ){
			event.preventDefault();
		} else {
			$( "#payEventAmountFrm" ).submit();
		}
		
	});

	function getBankDetailsModal(value){

		if(value != ''){
			$.ajax({
				url: 'ajaxPaymentData.php',
				type: 'POST',
				data: {payment_type_value: value},
			})
			.done(function(response) {
				$('#getDetailsModal').html(response);
				$('#isCashModal').val('1');
			});
		}else{
			$('#getDetailsModal').html('');
			$('#isCashModal').val('');
		}
	}
</script>