<?php error_reporting(0);
$currentDate = date('Y-m-d');
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$uId = (int)$_REQUEST['uId'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Performance Report</h4>
        </div>
        
     </div>
      <form class="branchDeptFilter" action="" method="get">
          <div class="row pt-2 pb-2">
            <?php include 'selectBranchDeptEmpForFilter.php' ?>
            <div class="col-md-3 form-group">
                <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
              </div>
          </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <?php if (isset($dId) && $dId > 0) { ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>                        
                        <th>Employee Name (Level)</th>                        
                        <th>Dimensional</th>
                        <th>PMS Type</th>                      
                        <th>Scheduled Date</th>                      
                        <th>Action</th>                      
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;

                      if(isset($bId) && $bId>0) {
                        $blockFilterQuery = " AND users_master.block_id='$bId'";
                      }
                      if(isset($dId) && $dId>0) {
                          $deptFilterQuery = " AND users_master.floor_id='$dId'";
                      }
                      if(isset($uId) && $uId>0) {
                          $userFilterQuery = " AND users_master.user_id='$uId'";
                      }


                      $q=$d->selectRow("pms_schedule_master.*, users_master.user_id, users_master.user_full_name, employee_level_master.level_name, dimensional_master.dimensional_name","pms_schedule_master, users_master, employee_level_master, dimensional_master","pms_schedule_master.dimensional_id=dimensional_master.dimensional_id AND pms_schedule_master.user_id=users_master.user_id AND pms_schedule_master.user_level_id = employee_level_master.level_id AND pms_schedule_master.society_id='$society_id' AND pms_schedule_master.schedule_status='1' $blockFilterQuery $deptFilterQuery $userFilterQuery $blockAppendQueryUser");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                      
                        <td><?php echo $counter++; ?></td>
                        <td><?php echo $data['user_full_name']; ?> (<?php echo $data['level_name'] ?>)</td>
                        <td><?php echo $data['dimensional_name']; ?></td>
                        <td><?php if($data['pms_type_on_schedule'] == 0){
                            echo 'Weekly';
                          }
                          elseif($data['pms_type_on_schedule'] == 1){
                            echo 'Monthly';
                          }
                          elseif($data['pms_type_on_schedule'] == 2){
                            echo 'Quarterly';
                          }
                          elseif($data['pms_type_on_schedule'] == 3){
                            echo 'Half Yearly';
                          }
                          elseif($data['pms_type_on_schedule'] == 4){
                            echo 'Yearly';
                          }
                        ?>
                      </td>
                       <td>
                           <?php echo date('d M Y', strtotime($data['schedule_date'])); ?>
                       </td>
                       <td><a href="performanceReportDetail?id=<?php echo $data['pms_schedule_id']; ?>" class="btn btn-primary btn-sm">View Report</a></td>
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
              <?php } else {  ?>
                <div class="" role="alert">
                  <span><strong>Note :</strong> Please Select Branch & Department</span>
                </div>
              <?php } ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="performanceAnswerModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Performance Answer</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
          <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12" id="showPerformanceAnswer">
                
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
