<?php 
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$society_id=$_COOKIE['society_id'];
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract(array_map("test_input" , $_POST));
include 'common/checkLanguage.php';


$bms_admin_id = $_COOKIE['bms_admin_id'];
$aq=$d->select("bms_admin_master","admin_id='$bms_admin_id' ");
$adminData= mysqli_fetch_array($aq);
$access_branchs=$adminData['access_branchs'];
$access_departments=$adminData['access_departments'];
$blockAryAccess = array();
$departmentAry = array();
$blockAryAccess = explode(",", $access_branchs);
$departmentAry = explode(",", $access_departments);

if ($access_branchs!='') {
    $blockids = join("','",$blockAryAccess); 
    if ($access_departments!='') {
      $departmentIds = join("','",$departmentAry); 
      $restictDepartment = " AND floors_master.floor_id IN ('$departmentIds')";
      $restictDepartmentUser = " AND users_master.floor_id IN ('$departmentIds')";
    } else {
      $restictDepartment = "";
      $restictDepartmentUser = "";
    }

    $blockAppendQuery = " AND block_master.block_id IN ('$blockids')";
    $blockAppendQueryUnit = " AND unit_master.block_id IN ('$blockids')";
    $blockAppendQueryFloor = " AND floors_master.block_id IN ('$blockids') $restictDepartment";
    $blockAppendQueryUser = " AND users_master.block_id IN ('$blockids') $restictDepartmentUser";
    $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

}  else {
    $blockAppendQuery = "";
    $blockAppendQueryUnit = "";
    $blockAppendQueryUser = "";
    $blockAppendQueryOnly="";
}

 $q344=$d->select("document_master","document_id='$document_id'","");
  $userData=mysqli_fetch_array($q344);
   if($userData['share_with']!='public') {  
   ?>
			<form id="personal-info" action="controller/documentController.php" method="post">
            <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
            <input type="hidden" id="dId" value="<?php echo $document_id;?>" name="document_id_share">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Share with <span class="text-danger">*</span></label>
                    <div class="col-sm-8" >
                      <select type="text" required=""  multiple="multiple" class="multiple-select1 form-control single-select" name="user_id[]">
                        <option value="">-- Select --</option>
                        <?php 
                          $q3=$d->select("unit_master,block_master,users_master","users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 $blockAppendQueryUser ","ORDER BY block_master.block_sort ASC");
                           while ($blockRow=mysqli_fetch_array($q3)) {
                         ?>
                          <option value="<?php echo $blockRow['user_id'];?>"><?php echo $blockRow['user_full_name'];?> <?php echo "- " . $blockRow['user_designation']; ?> (<?php echo $blockRow['block_name'];?>)</option>
                          <?php }?>
                        </select>
                    </div>
                </div>
                
                
         
                <div class="form-footer text-center">
                  <input type="hidden" name="sahreUser" value="sahreUser">
                  <button type="submit" name=""  class="btn btn-success"><i class="fa fa-check-square-o"></i> Share to Selected Users</button>
                </div>
              </form>
               <form action="controller/documentController.php" method="post">
                <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
            	<input type="hidden" id="dId" value="<?php echo $document_id;?>" name="document_id_share">
                <div class="form-footer text-center">
                 
                  <?php if ($blockAppendQuery=="") { ?>
                  <input type="hidden" name="shareAll" value="shareAll">
                  <button type="submit" name="shareAll" value="shareAll" class="btn form-btn btn-warning"><i class="fa fa-check-square-o"></i> Share To All</button>
                <?php } ?>
                </div>
               </form>
              <?php } ?>
                <div class="form-group table-responsive">
                  <table class="table table-bordered">
                    <tr>
                      <th>No</th>
                      <th><?php echo $xml->string->name; ?></th>
                      <th><?php echo $xml->string->block; ?></th>
                      <th><?php echo $xml->string->action; ?></th>
                    </tr>
                    <tbody id="">  
  <?php if($userData['share_with']=='public') {  ?>
  	<tr>
		<td>1</td>
		<td>This Document Shared to All</td>
		<td></td>
		<td>
			<form action="controller/documentController.php" method="post">
				<input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
				<input type="hidden" name="removeShareId" value="<?php echo $document_id; ?>">
				<button type="subit" class="btn form-btn btn-danger btn btn-sm">Unshare</button>
			</form>
		</td>
	</tr>

 <?php  } else {

$i=1;
 $q311=$d->select("unit_master,block_master,users_master,document_shared_master","document_shared_master.user_id=users_master.user_id AND document_shared_master.document_id='$document_id' AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id'","ORDER BY unit_master.unit_id ASC");
   while ($userData=mysqli_fetch_array($q311)) {
?>

	<tr>
		<td><?php echo $i++; ?></td>
		<td><?php echo  $userData['user_full_name'] ?></td>
		<td><?php echo $userData['block_name'];?></td>
		<td>
			<form action="controller/documentController.php" method="post">
				<input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
				<input type="hidden" name="share_id_remove" value="<?php echo $userData['share_id']; ?>">
				<button type="subit" class="btn btn-danger btn btn-sm">Un Share </button>
			</form>
		</td>
	</tr>

<?php } } ?>
</tbody>
                    
  </table>
</div>

</form> 
<!-- Bootstrap core JavaScript-->
<script src="assets/js/jquery.min.js"></script>


 

<!--Form Validatin Script-->
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/validate7.js"></script>

<!--Select Plugins Js-->
<script src="assets/plugins/select2/js/select2.min.js"></script>
<!--Inputtags Js-->
<script src="assets/plugins/inputtags/js/bootstrap-tagsinput.js"></script>

<!--Bootstrap Datepicker Js-->
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>


    <!--Multi Select Js-->
    <script src="assets/plugins/jquery-multi-select/jquery.multi-select.js"></script>
    <script src="assets/plugins/jquery-multi-select/jquery.quicksearch.js"></script>
    <script type="text/javascript" src="assets/js/select.js"></script>

  <script type="text/javascript">
  	$('.form-btn').on('click',function(e){
    e.preventDefault();
    var form = $(this).parents('form');
     swal({
        title: "Are you sure?",
        text: "",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ['Cancel', 'Yes, I am sure !'],
      })
     .then((willDelete) => {
        if (willDelete) {
          form.submit();
        }
      });

});
  </script>