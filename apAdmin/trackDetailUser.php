<?php error_reporting(0);
$society_address = preg_replace( "/(\r|\n)/", "", $sData['society_address']);
$dId = (int)$_REQUEST['dId'];
$bId = (int)$_REQUEST['bId'];

if (isset($_GET['from']) && isset($_GET['from']) && $_GET['toDate'] != '' && $_GET['toDate']) {
  $from  = $_GET['from'];
  $toDate  = $_GET['toDate'];

} else {
  $from  = date('Y-m-d');
  $toDate  = date("Y-m-d");
}

if(isset($_REQUEST['uId']))
{
  $uId = (int)$_REQUEST['uId'];
}
else
{
  $uId = 0;
}
 if (isset($_REQUEST['viewPlayBack']) && $_REQUEST['viewPlayBack']=='yes') { ?>

<script>
var step = 1000; // 5; // metres
function abc(aa) {
  aa = parseInt(aa)
  step = aa;
// alert(aa);
}
</script>
<style type="text/css">
#map-canvas1{
  height: 500px;
  width:100%;
}
@media screen and (max-width: 480px) {
  #map-canvas1{
    height: 400px;
    width:100%;
  }
}


</style>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row">
        <div class="col-sm-3 col-md-6 col-6 col-12">
          <h4 class="page-title">Location Play Back</h4>
        </div>

        <div class="col-sm-6 col-md-6 col-6  col-12 text-right">
          <a href="trackDetailUser?bId=<?php echo $bId; ?>&dId=<?php echo $dId; ?>&uId=<?php echo $uId; ?>&from=<?php echo $from; ?>&toDate=<?php echo $toDate; ?>" class="btn btn-sm btn-primary ml-1">Location Log</a>

          
        </div>
      
    </div>

    <form action="" class="branchDeptFilterWithUser">
      <input type="hidden" name="viewPlayBack" value="yes">
        <div class="row">
            <?php /// include('selectBranchDeptForFilter.php'); ?> 
            <div class="col-md-2 form-group">
            <label class="form-control-label">Branch </label>
            <select name="bId" class="form-control single-select" required="" onchange="getVisitorFloorByBlockId(this.value)">
              <option value="">-- Select --</option>
              <?php
              $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQuery");
              while ($blockData = mysqli_fetch_array($qb)) {
              ?>
                <option <?php if ($_GET['bId'] == $blockData['block_id']) {echo 'selected';} ?> value="<?php echo $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2 form-group">
            <label class="form-control-label">Department </label>
            <select name="dId" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value)" required="">
              <option value="">--select--</option>
              <?php
              $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id = '$_GET[bId]' $blockAppendQueryFloor");
              while ($depaData = mysqli_fetch_array($qd)) {
              ?>
                <option <?php if ($_GET['dId'] == $depaData['floor_id']) {
                          echo 'selected';
                        } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name']; ?></option>
              <?php } ?>

            </select>
          </div>
          <div class="col-md-2 form-group" id="">
            <label class="form-control-label">Employee </label>
            <select id="user_id" type="text" class="form-control single-select " name="uId">
              <option value="">--Select--</option>
              <?php
              if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                $userData = $d->select("users_master", "track_user_location=1 AND society_id='$society_id' AND delete_status=0 AND floor_id = $_GET[dId] $blockAppendQueryUser");
                while ($user = mysqli_fetch_array($userData)) {?>
                  <option <?php if (isset($uId) && $uId == $user['user_id']) { echo "selected";} ?> value="<?php if (isset($user['user_id']) && $user['user_id'] != "") {echo $user['user_id'];} ?>"> <?php if (isset($user['user_full_name']) && $user['user_full_name'] != "") {echo $user['user_full_name'];} ?></option>
              <?php }
              } ?>
            </select>
          </div>
          <div class="col-md-1 form-group">
              <label class="form-control-label">Speed </label>
                <select class="form-control single-select"   onchange="abc(this.value)">
                  <option value="1000">1X</option>
                  <option value="500">2X</option>
                  <option value="250">3X</option>
                  <option value="125">4X</option>
                  <option value="63">5X</option>
                </select>
            </div>
            <div class="col-md-2 col-6 form-group">
              <label class="form-control-label">From Date </label>
              <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" name="from" value="<?php if (isset($from) && $from != '') {echo $from;}  ?>">
            </div>
            <div class="col-md-2 col-6 form-group">
              <label class="form-control-label">To Date </label>
              <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerTo" name="toDate" value="<?php if (isset($toDate) && $toDate != '') { echo $toDate;} ?>">
            </div>
            
            <div class="col-md-1 form-group mt-auto">
              <input class="btn btn-success btn-sm " type="submit" name="getReport"  value="Get">
            </div>
        </div> 
    </form>
      <?php
     if ($uId>0) {
      $uq = $d->selectRow("attendance_master.total_travel_meter,last_tracking_gps_accuracy,last_tracking_battery_status,last_tracking_latitude,last_tracking_longitude,last_tracking_address,last_tracking_location_time,last_tracking_area,last_tracking_locality,last_tracking_mock_location,last_tracking_mock_location_app_name","users_master LEFT JOIN attendance_master ON attendance_master.user_id=users_master.user_id AND DATE_FORMAT(attendance_date_end,'%Y-%m-%d') = '$toDate' ","users_master.user_id='$uId' ");
      $lastUserData= mysqli_fetch_array($uq);
      $total_travel_meter = $lastUserData['total_travel_meter']/1000 .' KM';
      $last_tracking_latitude = $lastUserData['last_tracking_latitude'];
      $last_tracking_longitude = $lastUserData['last_tracking_longitude'];
      $last_tracking_address = $lastUserData['last_tracking_address'];
      $last_tracking_location_time = $lastUserData['last_tracking_location_time'];
      $last_tracking_area = $lastUserData['last_tracking_area'];
      $last_tracking_locality = $lastUserData['last_tracking_locality'];
      $last_tracking_battery_status = $lastUserData['last_tracking_battery_status'];
      $last_tracking_gps_accuracy = $lastUserData['last_tracking_gps_accuracy'];

    }
      if($lastUserData>0) { ?>
    <div class="row">
      <div class="col-lg-6">
        <h6>Last Location: <?php echo $lastUserData['last_tracking_address'];?> <a type="submit" class="btn btn-sm btn-link" onclick="userBackTrackDataLastData(<?php echo $uId; ?>)"> (View) </a></h6>
      </div>
      <div class="col-lg-6">
            <h6>Time: <?php if($lastUserData['last_tracking_location_time'] !="0000-00-00 00:00:00"){ echo date('d M Y h:i A',strtotime($lastUserData['last_tracking_location_time'])); } echo " (".time_elapsed_string(date("d M Y h:i A", strtotime($lastUserData['last_tracking_location_time']))); ?>) </h6>
      </div>
      <div class="col-lg-6">
            <h6>Phone Battery Level : <i class="fa fa-battery-three-quarters"></i> <?php echo $last_tracking_battery_status;?> % </h6>
      </div>
      <div class="col-lg-6">
            <h6> GPS Accuracy : <i class="fa fa-compass" aria-hidden="true"></i> <?php echo $last_tracking_gps_accuracy;?> M </h6>
      </div>
    </div>
      <?php } ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div id="map-canvas1"></div>
              <div><img src="../img/yellow_marker.png"> Spent More Than 10 Minutes <img src="../img/red_marker.png"> Spent More Than 30 Minutes <img src="../img/blue_marker.png"> Spent Less Than 10 Minutes</div>
              <?php if($lastUserData['total_travel_meter']>0) { ?>
              <div>Approx Travel KM : <?php echo number_format((float)$total_travel_meter, 2, '.', '');  ?></div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="assets/js/jquery.min.js"></script>
  <script  src="https://maps.googleapis.com/maps/api/js?key=<?php echo $d->map_key();?>"></script>
      <?php

      

      if (isset($from) && isset($from) && $toDate != '' && $toDate) {
        $dateFilterQuery = " AND DATE_FORMAT(user_back_track_master.user_back_track_date,'%Y-%m-%d') BETWEEN '$from' AND '$toDate'";
        
      }
      else
      {
          $limitSql  = " LIMIT 100";
      }
      // find branch department
      $query1=$d->select('block_master',"block_id='$bId' AND society_id='$society_id'","");
      $row1 = mysqli_fetch_assoc($query1);
      $lat = $sData['society_latitude'];
      $lon = $sData['society_longitude'];
      if ($row1['block_geofence_latitude']!='') {
        // code...
        $lat2 = $row1['block_geofence_latitude'];
        $lon2 = $row1['block_geofence_longitude'];
      } else {
        $lat2 = $lat;
        $lon2 = $lon;
      }
      $dump_lat= $lat2;
      $dump_lng= $lon2;

      $trackingArray = array();
       $query=$d->select('user_back_track_master',"gps_accuracy BETWEEN 2 AND 100 AND user_id='$uId' $dateFilterQuery","ORDER BY user_back_track_date ASC");
      while($row = mysqli_fetch_assoc($query)){
        array_push($trackingArray,$row);
      }
      if (count($trackingArray)>0) {
        $startLatitude = $trackingArray[0]['back_user_lat'];
        $startLongitude = $trackingArray[0]['back_user_long'];
      } else {
        $startLatitude= $lat2;
        $startLongitude= $lon2;
      }
      ?>
      <script>
        function initialize() {
          var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(<?php echo $startLatitude; ?>,<?php echo $startLongitude; ?>),
            mapTypeId: google.maps.MapTypeId.TERRAIN
          };
          var map = new google.maps.Map(document.getElementById('map-canvas1'),
            mapOptions);
          //For company Marker ANd Infowindow
          var infoWindow = new google.maps.InfoWindow();
          PoiPointP = new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $lon; ?>);
          // Function for adding a marker to the page.

          addPoiMarkerRedParking(PoiPointP);
          function addPoiMarkerRedParking(location) {
            marker = new google.maps.Marker({
              position: location,
              map: map,
              icon: '../img/building.png'

            });
          }
  //Attach click event to the marker.
  (function (marker) {
    google.maps.event.addListener(marker, "click", function (e) {
      infoWindow.setContent("<div  style = 'width:200px;min-height:40px;text-align:center;'><b><?php echo $sData['society_name']; ?></b><Br><i><?php echo $society_address; ?></i></div>");
      infoWindow.open(map, marker);
    });
  })(marker);

  // For branch Marker
  var infoWindow = new google.maps.InfoWindow();

  PoiPointD = new google.maps.LatLng(<?php echo $lat2; ?>, <?php echo $lon2; ?>);
  // Function for adding a marker to the page.

  addPoiMarkerRedDump(PoiPointD);
  function addPoiMarkerRedDump(location) {
    marker = new google.maps.Marker({
      position: location,
      map: map,
      icon: '../img/location.png'
    });
  }
  //Attach click event to the marker.
  (function (marker) {
    google.maps.event.addListener(marker, "click", function (e) {
      infoWindow.setContent("<div  style = 'width:200px;min-height:40px;text-align:center;'>Branch:  <?php echo $row1['block_name']; ?></div>");
      infoWindow.open(map, marker);
    });
  })(marker);

  var flightPlanCoordinates = [
  <?php
  for ($iNew=0; $iNew <count($trackingArray) ; $iNew++) { 
    $lat = $trackingArray[$iNew]['back_user_lat'];
    $lon = $trackingArray[$iNew]['back_user_long'];
    echo 'new google.maps.LatLng('.$lat.', '.$lon.'),';
  }
  ?>

  ];
  var userCoor = [
  <?php
  for ($iNew1=1; $iNew1 <count($trackingArray) ; $iNew1++) { 
      if ($trackingArray[$iNew1]['spend_sec']>0) {
        $spend_minutes = $trackingArray[$iNew1]['spend_sec']/60;
        $hours = floor($spend_minutes / 3600);
        $minutes = floor(($spend_minutes / 60) % 60);
        $seconds = $spend_minutes % 60;
        $spend_minutes =  "$hours:$minutes:$seconds";
        // $spend_minutes = number_format((float)$spend_minutes, 2, '.', '').' Min.';
        
        if ($trackingArray[$iNew1]['spend_sec']>1800) {
          // more than 10 minutes
          $marker_icon = "../img/red_marker.png";
        } else if ($trackingArray[$iNew1]['spend_sec']>600) {
          // more than 30 minutes
          $marker_icon = "../img/yellow_marker.png";
        } else {
          $marker_icon = "../img/blue_marker.png";
        }
      } else {
        $marker_icon = "../img/blue_marker.png";
        $spend_minutes = "0";
      }

      $lat = $trackingArray[$iNew1]['back_user_lat'];
      $lon = $trackingArray[$iNew1]['back_user_long'];
      $trakAddress = str_replace("'", "", $trackingArray[$iNew1]['back_address']);
      $trakAddress = $trakAddress;
      $gps_accuracy = $trackingArray[$iNew1]['gps_accuracy'].' M';
      $user_back_track_date = date('d M Y h:i A',strtotime($trackingArray[$iNew1]['user_back_track_date']));
      echo "['".$trakAddress."', $lat, $lon,'".$user_back_track_date."','".$gps_accuracy."','".$spend_minutes."','".$marker_icon."'],";
    }
  ?>
  ];

  var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";

  lineSymbol = {
    path: car,
    scale: .7,
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'red',
    offset: '5%',
  // rotation: parseInt(heading[i]),
  anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
  };

  var flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: '#2f648e',
    strokeOpacity: 1.0,
    strokeWeight: 2,
    icons: [{
      icon: lineSymbol,
      offset: '100%'
    }],
    map: map
  });

  var markerPoly, i;
  var infowindow = new google.maps.InfoWindow();

  for (i = 0; i < userCoor.length; i++) {  
    markerPoly = new google.maps.Marker({
      position: new google.maps.LatLng(userCoor[i][1], userCoor[i][2]),
      map: map,
      icon: userCoor[i][6]
    });


    google.maps.event.addListener(markerPoly, 'click', (function(markerPoly, i) {
      return function() {
        infowindow.setContent("<div  style = 'width:200px;min-height:40px;'><b>Address: </b> "+userCoor[i][0]+"<br><b>Time: </b> "+userCoor[i][3]+"<br><b>GPS Accuracy : </b> "+userCoor[i][4]+"<br><b>Spent Time : </b> "+userCoor[i][5]+"</div>");
        infowindow.open(map, markerPoly);
      }
    })(markerPoly, i));



  }

  if (timerHandle) {
    clearTimeout(timerHandle);
  }

  flightPath.setMap(map);
  abc(1000);
  // animateCircle(flightPath);
  setTimeout(animateCircle, step, flightPath); 
  // animateCircle(flightPath);
  }
  var tstep = 500;
  // var step = 200;
  var count = 0;
  var timerHandle = null;
  function animateCircle(flightPath) {
    var latlong;
  // window.setInterval(function() {
    count = (count + 1) % 200;
    console.log("step "+step);
    var icons = flightPath.get('icons');
    icons[0].offset = (count / 2) + '%';
    flightPath.set('icons', icons);

    latlong = flightPath.getPath();
    var p = flightPath.GetPointAtDistance(count);
    timerHandle = setTimeout(animateCircle, step,flightPath);
  // }, step);
  }

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">

google.maps.LatLng.prototype.distanceFrom = function (newLatLng) {
var EarthRadiusMeters = 6378137.0; // meters
var lat1 = this.lat();
var lon1 = this.lng();
var lat2 = newLatLng.lat();
var lon2 = newLatLng.lng();
var dLat = (lat2 - lat1) * Math.PI / 180;
var dLon = (lon2 - lon1) * Math.PI / 180;
var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
var d = EarthRadiusMeters * c;
return d;
}

google.maps.LatLng.prototype.latRadians = function () {
  return this.lat() * Math.PI / 180;
}

google.maps.LatLng.prototype.lngRadians = function () {
  return this.lng() * Math.PI / 180;
}

// === A method which returns the length of a path in metres ===
google.maps.Polygon.prototype.Distance = function () {
  var dist = 0;
  for (var i = 1; i < this.getPath().getLength(); i++) {
    dist += this.getPath().getAt(i).distanceFrom(this.getPath().getAt(i - 1));
  }
  return dist;
}

// === A method which returns a GLatLng of a point a given distance along the path ===
// === Returns null if the path is shorter than the specified distance ===
google.maps.Polygon.prototype.GetPointAtDistance = function (metres) {
// some awkward special cases
if (metres == 0) return this.getPath().getAt(0);
if (metres < 0) return null;
if (this.getPath().getLength() < 2) return null;
var dist = 0;
var olddist = 0;
for (var i = 1;
  (i < this.getPath().getLength() && dist < metres); i++) {
  olddist = dist;
dist += this.getPath().getAt(i).distanceFrom(this.getPath().getAt(i - 1));
}
if (dist < metres) {
  return null;
}
var p1 = this.getPath().getAt(i - 2);
var p2 = this.getPath().getAt(i - 1);
var m = (metres - olddist) / (dist - olddist);
return new google.maps.LatLng(p1.lat() + (p2.lat() - p1.lat()) * m, p1.lng() + (p2.lng() - p1.lng()) * m);
}

// === A method which returns an array of GLatLngs of points a given interval along the path ===
google.maps.Polygon.prototype.GetPointsAtDistance = function (metres) {
  var next = metres;
  var points = [];
// some awkward special cases
if (metres <= 0) return points;
var dist = 0;
var olddist = 0;
for (var i = 1;
  (i < this.getPath().getLength()); i++) {
  olddist = dist;
dist += this.getPath().getAt(i).distanceFrom(this.getPath().getAt(i - 1));
while (dist > next) {
  var p1 = this.getPath().getAt(i - 1);
  var p2 = this.getPath().getAt(i);
  var m = (next - olddist) / (dist - olddist);
  points.push(new google.maps.LatLng(p1.lat() + (p2.lat() - p1.lat()) * m, p1.lng() + (p2.lng() - p1.lng()) * m));
  next += metres;
}
}
return points;
}

// === A method which returns the Vertex number at a given distance along the path ===
// === Returns null if the path is shorter than the specified distance ===
google.maps.Polygon.prototype.GetIndexAtDistance = function (metres) {
// some awkward special cases
if (metres == 0) return this.getPath().getAt(0);
if (metres < 0) return null;
var dist = 0;
var olddist = 0;
for (var i = 1;
  (i < this.getPath().getLength() && dist < metres); i++) {
  olddist = dist;
dist += this.getPath().getAt(i).distanceFrom(this.getPath().getAt(i - 1));
}
if (dist < metres) {
  return null;
}
return i;
}
// === Copy all the above functions to GPolyline ===
google.maps.Polyline.prototype.Distance = google.maps.Polygon.prototype.Distance;
google.maps.Polyline.prototype.GetPointAtDistance = google.maps.Polygon.prototype.GetPointAtDistance;
google.maps.Polyline.prototype.GetPointsAtDistance = google.maps.Polygon.prototype.GetPointsAtDistance;
google.maps.Polyline.prototype.GetIndexAtDistance = google.maps.Polygon.prototype.GetIndexAtDistance;
</script>


<?php } else {
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row">
        <div class="col-sm-6 col-md-6 col-6 col-12">
          <h4 class="page-title">User Tracking Log</h4>
        </div>
        <div class="col-sm-6 col-md-6 col-6 text-right col-12">
          <a href="trackDetailUser?viewPlayBack=yes&bId=<?php echo $bId; ?>&dId=<?php echo $dId; ?>&uId=<?php echo $uId; ?>&from=<?php echo $from; ?>&toDate=<?php echo $toDate; ?>"  class="btn btn-sm btn-primary ml-1">Play Back</a>

          
        </div>
    </div>

    <form action="" class="branchDeptFilterWithUser">
        <div class="row">
            <?php /// include('selectBranchDeptForFilter.php'); ?> 
            <div class="col-md-2 form-group">
            <label class="form-control-label">Branch </label>
            <select name="bId" class="form-control single-select" required="" onchange="getVisitorFloorByBlockId(this.value)">
              <option value="">-- Select --</option>
              <?php
              $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQuery");
              while ($blockData = mysqli_fetch_array($qb)) {
              ?>
                <option <?php if ($_GET['bId'] == $blockData['block_id']) {echo 'selected';} ?> value="<?php echo $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2 form-group">
            <label class="form-control-label">Department </label>
            <select name="dId" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value)" required="">
              <option value="">--select--</option>
              <?php
              $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id = '$_GET[bId]' $blockAppendQueryFloor");
              while ($depaData = mysqli_fetch_array($qd)) {
              ?>
                <option <?php if ($_GET['dId'] == $depaData['floor_id']) {
                          echo 'selected';
                        } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name']; ?></option>
              <?php } ?>

            </select>
          </div>
          <div class="col-md-2 form-group" id="">
            <label class="form-control-label">Employee </label>
            <select id="user_id" type="text" class="form-control single-select " name="uId">
              <option value="">--Select--</option>
              <?php
              if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                $userData = $d->select("users_master", "track_user_location=1 AND  society_id='$society_id' AND delete_status=0 AND floor_id = $_GET[dId] $blockAppendQueryUser");
                while ($user = mysqli_fetch_array($userData)) {?>
                  <option <?php if (isset($uId) && $uId == $user['user_id']) { echo "selected";} ?> value="<?php if (isset($user['user_id']) && $user['user_id'] != "") {echo $user['user_id'];} ?>"> <?php if (isset($user['user_full_name']) && $user['user_full_name'] != "") {echo $user['user_full_name'];} ?></option>
              <?php }
              } ?>
            </select>
          </div>
            <div class="col-md-2 col-6 form-group">
              <label class="form-control-label">From Date </label>
              <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" name="from" value="<?php if (isset($from) && $from != '') {echo $from;}  ?>">
            </div>
            <div class="col-md-2 col-6 form-group">
              <label class="form-control-label">To Date </label>
              <input type="text" required="" class="form-control" autocomplete="off" id="autoclose-datepickerTo" name="toDate" value="<?php if (isset($toDate) && $toDate != '') { echo $toDate;} ?>">
            </div>
            <?php 
              if ($uId>0) {
             
                $uq = $d->selectRow("user_token,device,phone_model,phone_brand,mobile_os_version,app_version_code,last_tracking_gps_accuracy,last_tracking_latitude,last_tracking_longitude,last_tracking_address,last_tracking_location_time,last_tracking_area,last_tracking_locality,last_tracking_mock_location,last_tracking_mock_location_app_name,last_tracking_battery_status","users_master","user_id='$uId'");
                $lastUserData= mysqli_fetch_array($uq);
                $user_token = $lastUserData['user_token'];
                $device = $lastUserData['device'];
                $phone_model = $lastUserData['phone_model'].'-'.$lastUserData['phone_brand'];
                $app_version_code = $lastUserData['app_version_code'];
                $mobile_os_version = $lastUserData['mobile_os_version'];
                $last_tracking_latitude = $lastUserData['last_tracking_latitude'];
                $last_tracking_longitude = $lastUserData['last_tracking_longitude'];
                $last_tracking_address = $lastUserData['last_tracking_address'];
                $last_tracking_location_time = $lastUserData['last_tracking_location_time'];
                $last_tracking_location_date = date("Y-m-d",strtotime($lastUserData['last_tracking_location_time']));
                $last_tracking_area = $lastUserData['last_tracking_area'];
                $last_tracking_locality = $lastUserData['last_tracking_locality'];
                $last_tracking_battery_status = $lastUserData['last_tracking_battery_status'];
                $last_tracking_gps_accuracy = $lastUserData['last_tracking_gps_accuracy'];

                $cTime = date("Y-m-d H:i:s");
                $timeFirst  = strtotime($last_tracking_location_time);
                $timeSecond = strtotime($cTime);
                $differenceInSeconds = $timeSecond - $timeFirst;
               ?>

            <div class="col-md-2 form-group mt-auto">
             <!--  <input type="hidden" name="uId" value="<?php echo $uId;  ?>"> -->
              <input class="btn btn-success btn-sm " type="submit" name="getReport"  value="Get">
            </div>
            
        </div> 
    </form>
      <?php  
            
        if (mysqli_num_rows($uq)>0 && $last_tracking_location_date==date("Y-m-d")) {
        ?>
      <div class="row">
        <div class="col-lg-6">
          <h6>Last Location: <?php echo $lastUserData['last_tracking_address'];?> <a type="submit" class="btn btn-sm btn-link" onclick="userBackTrackDataLastData(<?php echo $uId; ?>)"> (View) </a></h6>
        </div>
        <div class="col-lg-6">
          <form  action="controller/userController.php" method="post">
              <h6>Time: <?php if($lastUserData['last_tracking_location_time'] !="0000-00-00 00:00:00"){ echo date('d M Y h:i A',strtotime($lastUserData['last_tracking_location_time'])); } echo " (".time_elapsed_string(date("d M Y h:i A", strtotime($lastUserData['last_tracking_location_time']))); ?>)  <?php if ($differenceInSeconds>3) {  ?>
              
                <input type="hidden" name="bId" value="<?php echo $bId; ?>">
                <input type="hidden" name="dId" value="<?php echo $dId; ?>">
                <input type="hidden" name="uId" value="<?php echo $uId; ?>">
                <input type="hidden" name="viewLocationLog" value="viewLocationLog">
                <button type="submit" class="btn btn-sm btn-warning ml-1">Relaod </button>
              
              <?php } ?></h6></form>
        </div>
        <div class="col-lg-6">
              <h6>Phone Battery Level : <i class="fa fa-battery-three-quarters"></i> <?php echo $last_tracking_battery_status;?> % </h6>
        </div>
        <div class="col-lg-6">
              <h6> GPS Accuracy : <i class="fa fa-compass" aria-hidden="true"></i> <?php echo $last_tracking_gps_accuracy;?> M </h6>
        </div>
      </div>
      <?php } } ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">

            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                  <?php
                   

                    $i = 1;
                    if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                      $dateFilterQuery = " AND DATE_FORMAT(user_back_track_master.user_back_track_date,'%Y-%m-%d') BETWEEN '$from' AND '$toDate'";
                    }
                    else
                    {
                        $limitSql  = " LIMIT 100";
                    }
                   
                    $q = $d->selectRow("user_back_track_master.*,date_format(user_back_track_date,'%Y-%m-%d %H:%i:00') as act_time,users_master.*,block_master.block_id,block_master.block_name,floors_master.floor_id,floors_master.floor_name","user_back_track_master LEFT JOIN users_master ON users_master.user_id=user_back_track_master.user_id LEFT JOIN floors_master ON users_master.floor_id=floors_master.floor_id LEFT JOIN block_master ON users_master.block_id=block_master.block_id","user_back_track_master.user_id=$uId $dateFilterQuery","group by act_time ORDER BY user_back_track_master.user_back_track_date DESC $limitSql");
                    $counter = 1;
                  ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Action</th>
                        <th>Latitude</th>
                        <th>Logitude</th>
                        <th>Address</th>
                        <th>Track date</th>
                        <th>area</th>
                        <th>locality</th>
                        <th>GPS Accuracy</th>
                        <th>Modified </th>
                        <th>Network</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                   <?php  while ($data = mysqli_fetch_array($q)) { 
                      switch ($data['location_type']) {
                        case '0':
                          $location_type = "Tracking Log";
                          break;
                        case '1':
                          $location_type = "Punch In From App";
                          break;
                        case '2':
                          $location_type = "Punch Out From App";
                          break;
                        case '3':
                          $location_type = "Lunch Break Start";
                          break;
                        case '4':
                          $location_type = "Lunch Break End";
                          break;
                        case '5':
                          $location_type = "Tea Break Start";
                          break;
                        case '6':
                          $location_type = "Tea Break End";
                          break;
                        case '7':
                          $location_type = "Personal Break Start";
                          break;
                        case '8':
                          $location_type = "Personal Break End";
                          break;
                        case '9':
                          $location_type = "Work Report Added";
                          break;
                        case '10':
                          $location_type = "Punch In From Face App";
                          break;
                        case '11':
                          $location_type = "Punch Out From Face App";
                          break;
                        case '12':
                          $location_type = "Punch In From Face App Offline Sync.";
                          break;
                        case '13':
                          $location_type = "Punch Out From Face App ffline Sync.";
                          break;
                        default:
                          $location_type = "Tracking Log";
                          break;
                      }

                     ?>
                    <tr <?php if($data['is_mock_location']==1) { echo "class='text-danger'";} else if($data['gps_accuracy']>200) { echo "class='text-danger'";} ?> >
                       <td><?php echo $counter++; ?></td>
                        <td>
                            <button type="submit" class="btn btn-sm btn-primary" onclick="userBackTrackData(<?php echo $data['user_back_track_id']; ?>)">
                             <i class="fa fa-eye"></i></button>
                        </td>
                       <td><?php echo $data['back_user_lat']; ?></td>
                       <td><?php echo $data['back_user_long']; ?></td>
                       <td><?php custom_echo($data['back_address'],23); ?></td>
                       <td><?php if($data['user_back_track_date'] !="0000-00-00 00:00:00"){ echo date('d M Y h:i A',strtotime($data['user_back_track_date'])); } ?></td>
                       <td><?php echo $data['back_area']; ?></td>
                       <td><?php echo $data['back_locality']; ?></td>
                       <td><?php if($data['gps_accuracy']>0) { echo $data['gps_accuracy'].' M'; } ?></td>
                         <td><?php  if($data['is_mock_location']==1) { echo "Yes"; 
                         echo "<br>App : ".$data['mock_app_name'];
                          } else { echo "No"; }?></td>
                        <td><?php  if($data['network_type']=='Wi-Fi') { echo "<i class='fa fa-wifi'"; } else if($data['network_type']=='Mobile Data')  { echo "<i class='fa fa-signal'></i>"; } else { echo "<i class='fa fa-ban'></i>"; } ?></td>
                       <td><?php  echo $location_type; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-compass"></i> GPS On/Off Log</div>
            <div class="card-body">
              <div class="table-responsive">
                  <?php

                    if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                      $dateFilterQuery11 = " AND DATE_FORMAT(user_gps_on_off.created_date,'%Y-%m-%d') BETWEEN '$from' AND '$toDate'";
                    }
                    else
                    {
                        $limitSql  = " LIMIT 100";
                    }
                    $q = $d->selectRow("user_gps_on_off.*","user_gps_on_off LEFT JOIN users_master ON users_master.user_id=user_gps_on_off.user_id","user_gps_on_off.type=1 AND user_gps_on_off.user_id=$uId $dateFilterQuery11","ORDER BY user_gps_on_off.user_gps_on_off_id DESC $limitSql");
                    $counter1 = 1;
                  ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Action</th>
                        <th>Status</th>
                        <th>Date </th>
                        <th>Latitude</th>
                        <th>Logitude</th>
                    </tr>
                </thead>
                <tbody>
                   
                   <?php  while ($data = mysqli_fetch_array($q)) { 
                     ?>
                    <tr >
                       <td><?php echo $counter1++; ?></td>
                        <td>
                          <?php  if($data['latitude']>0) { ?>
                          <button type="submit" class="btn btn-sm btn-primary" onclick="userGPSData(<?php echo $data['user_gps_on_off_id']; ?>)">
                             <i class="fa fa-eye"></i></button>
                          <?php } ?>
                        </td>
                       <td><?php echo $data['gps_status']; ?></td>
                       <td><?php if($data['created_date'] !="0000-00-00 00:00:00"){ echo date('d M Y h:i A',strtotime($data['created_date'])); } ?></td>
                       <td><?php  if($data['latitude']>0) { echo $data['latitude']; } ?></td>
                       <td><?php if($data['longitude']>0) { echo $data['longitude']; } ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

       <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-globe"></i> Internet On/Off Log</div>
            <div class="card-body">
              <div class="table-responsive">
                  <?php

                    if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                      $dateFilterQuery11 = " AND DATE_FORMAT(user_gps_on_off.created_date,'%Y-%m-%d') BETWEEN '$from' AND '$toDate'";
                    }
                    else
                    {
                        $limitSql  = " LIMIT 100";
                    }
                    $q = $d->selectRow("user_gps_on_off.*","user_gps_on_off LEFT JOIN users_master ON users_master.user_id=user_gps_on_off.user_id","user_gps_on_off.type=2 AND user_gps_on_off.user_id=$uId $dateFilterQuery11","ORDER BY user_gps_on_off.user_gps_on_off_id DESC $limitSql");
                    $counter1 = 1;
                  ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Action</th>
                        <th>Status</th>
                        <th>Date </th>
                        <th>Latitude</th>
                        <th>Logitude</th>
                    </tr>
                </thead>
                <tbody>
                   
                   <?php  while ($data = mysqli_fetch_array($q)) { 
                     ?>
                    <tr >
                       <td><?php echo $counter1++; ?></td>
                        <td><?php  if($data['latitude']>0) { ?>
                          <button type="submit" class="btn btn-sm btn-primary" onclick="userGPSData(<?php echo $data['user_gps_on_off_id']; ?>)">
                             <i class="fa fa-eye"></i></button>
                            <?php } ?>
                        </td>
                       <td><?php echo $data['gps_status']; ?></td>
                       <td><?php if($data['created_date'] !="0000-00-00 00:00:00"){ echo date('d M Y h:i A',strtotime($data['created_date'])); } ?></td>
                       <td><?php  if($data['latitude']>0) { echo $data['latitude']; } ?></td>
                       <td><?php if($data['longitude']>0) { echo $data['longitude']; } ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>
  <?php } ?>
    <div class="modal fade" id="userTrackDataModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Tracked Data</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body">
            <div class="row col-md-12 " id="userTrackDetail"></div>
            <div class="row col-md-12 " >
            <div class="map" id="trackDetailUserMap" style="width: 100%; height: 280px;">

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script src="assets/js/jquery.min.js"></script>


<script type="text/javascript">


function trackUserDetails(d_lat, d_long) {
      var latlng = new google.maps.LatLng(d_lat, d_long);
      var latitute = d_lat;
      var longitute = d_long;

      var map = new google.maps.Map(document.getElementById('trackDetailUserMap'), {
        center: latlng,
        zoom: 15
      });
      var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: false,
        anchorPoint: new google.maps.Point(0, -29)
      });
      
    }
    </script>
