
<?php
extract(array_map("test_input", $_POST));
if (isset($edit_company_service_details)) {
    $q = $d->select("company_service_details_master","company_service_details_id='$company_service_details_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Company Service Detail</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addCompanyServiceDetailForm" action="controller/CompanyServiceDetailController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                <select name="company_service_id" id="company_service_id" class="form-control single-select restFrm">
                                        <option value="">-- Select Service --</option> 
                                        <?php 
                                            $qcs=$d->select("company_service_master","society_id='$society_id'");  
                                            while ($serviceData=mysqli_fetch_array($qcs)) {
                                        ?>
                                        <option <?php if(isset($data) && $data['company_service_id'] == $serviceData['company_service_id']){ echo "selected";} ?> value="<?php echo  $serviceData['company_service_id'];?>" ><?php echo $serviceData['company_service_name'];?></option>
                                        <?php } ?>
                                
                                </select> 
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Detail Name <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Service Detail Title" id="company_service_details_title" name="company_service_details_title" value="<?php if($data['company_service_details_title'] !=""){ echo $data['company_service_details_title']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Detail Description <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <textarea class="form-control" placeholder="Service Detail description" id="company_service_details_description" name="company_service_details_description"><?php if($data['company_service_details_description'] !=""){ echo $data['company_service_details_description']; } ?></textarea>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">No of Service Detail List <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select class="form-control" id="no_of_list" name="no_of_list" value="" onchange="getServiceDetailListInput(this.value);">
                                        <option value="">--Select No of Service Detail List--</option>
                                        <?php for ($i=1; $i <= 50; $i++) { ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>  
                                        <?php } ?>
                                    </select>
                                    <!-- <input type="number" class="form-control" placeholder="No of Service Detail List" min="1" id="no_of_list" name="no_of_list" value="" onchange="getServiceDetailListInput(this.value);"> -->
                                </div>
                            </div> 
                        </div>
                    </div>   
                    <?php if (isset($edit_company_service_details)) {   
                        $q1 = $d->select("company_service_detail_list_master","company_service_details_id='$data[company_service_details_id]'");
                        //$data1 = mysqli_fetch_array($q1);
                        //$list = 1;
                        ?>
                        <div class=" row">
                            <?php
                        while($data1=mysqli_fetch_array($q1)){ 
                            $list = rand();
                            ?>
                            <div class="col-md-6">
                                <div class="form-group row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Detail List <span class="required">*</span></label>
                                    <div class="col-lg-12 col-md-12 col-12" id="">
                                        <input type="text" class="form-control option_name-cls" placeholder="Service Detail List"  id="company_service_detail_list_name<?php echo $list;?>" name="company_service_detail_list_name[]" value="<?php echo $data1['company_service_detail_list_name'] ?>">
                                        <label id="company_service_detail_list_name<?php echo $list;?>_div"  style="display: none;"  >Please enter Service Detail List </label>
                                    </div>
                                </div> 
                            </div>
                        <?php } ?>
                        </div> 
 
                    <?php } ?>
                    <div class="list_input row">
                        
                        
                        </div>
                        <input type="hidden" name="isOptionAdded" id="isOptionAdded" value="No">
                    <input type="hidden" id="company_service_details_id" name="company_service_details_id" value="<?php if($data['company_service_details_id'] !=""){ echo $data['company_service_details_id']; } ?>" >            
                    <div class="form-footer text-center">
                    <?php //IS_1019 addPenaltiesBtn 
                    if (isset($edit_company_service_details)) {                    
                    ?>
                    <button id="addCompanyServiceDetailBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addCompanyServiceDetail"  value="addCompanyServiceDetail">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyServiceDetailForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                        <?php }
                        else {
                        ?>
                    
                    <button id="addCompanyServiceDetailBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addCompanyServiceDetail"  value="addCompanyServiceDetail">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyServiceDetailForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">

function getServiceDetailListInput(val){
    var list = '';
    if(val>0){
        for (var i=1; i <= val ; i++) { 
            list += `<div class="col-md-6">
                                <div class="form-group row w-100 mx-0">
                                    <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Service Detail List <span class="required">*</span></label>
                                    <div class="col-lg-12 col-md-12 col-12" id="">
                                        <input type="text" class="form-control option_name-cls" placeholder="Service Detail List"  id="company_service_detail_list_name`+i+`" name="company_service_detail_list_name[]" value="">
                                        <label id="company_service_detail_list_name`+i+`_div"  style="display: none;"  >Please enter Service Detail List </label>
                                    </div>
                                </div> 
                            </div>`;
        }
        $('#isOptionAdded').val('Yes');
        $('.list_input').html(list);
    }else {
     $('#isOptionAdded').val('No');
     swal("Minimum 1 Service Detail List Required !", {
      icon: "error",
    });
     $('.list_input').html(' ');
   }
    
}


$( "#addCompanyServiceDetailForm" ).submit(function( event ) {

    var error = 0;
    $('.option_name-cls').each(function() {
    var clsVal =  $(this).val();
    var eleId = $(this).attr('id');

        if($.trim(clsVal) ==""){
            error++;
            /*$(eleId+'_div').removeClass('valid');
            $(eleId+'_div').addClass('error');*/

            $('#'+eleId+'_div').css('display','inline-block');
            $('#'+eleId+'_div').css('color','#ff0000');
            $('#'+eleId+'_div').text("Please enter Service Detail List Name "); //Dollop Infotech 27-Nov-2021
            $(".ajax-loader").hide();
        } else {
            /*$(eleId).addClass('valid');
            $(eleId).removeClass('error');*/
            $(".ajax-loader").show();
            $('#'+eleId+'_div').css('display','none');
        }
    });

    if($('#isOptionAdded').val()=="No"){
        error++;
        $('#no_of_option-error').text("Please select No Of Options");
        $('#no_of_option').addClass('error');
    }
    if(error > 0 ){  
        event.preventDefault();
    }  else {  
        $(".ajax-loader").hide();
        $("#addCompanyServiceDetailForm")[0].submit();
        
    } 
});

</script>
