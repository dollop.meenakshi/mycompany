<?php 
  extract(array_map("test_input" , $_POST));
  error_reporting(0);
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2">
      <div class="col-sm-3 col-6  pb-2">
        <h4 class="page-title">Common Entries</h4>
      </div>
      <div class="col-sm-6 col-6  pb-2">
        
      </div>
      
      <div class="col-sm-3 col-7  pb-2">
        <div class="btn-group float-sm-right">
           <a href="#" data-toggle="modal" data-target="#addComm" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
            
          
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row" id="searchedEmployees">
      <?php
       

          $q=$d->select("common_entry_master","society_id='$society_id' order by common_entry_id desc","");
        if(mysqli_num_rows($q)>0) {

          while ($row=mysqli_fetch_array($q)) {
            extract($row);
           
      ?>  
      <div class="col-lg-3 col-6">
        <div id="accordion1">
          <div class="card mb-2">
            <div id="collapse-1" class="collapse show" data-parent="#accordion1" style="">
              <div class="p-2">
                <div class="text-center">
                  <img style="height: 150px !important;width: 80;" onerror="this.src='img/user.png'"  src="../img/common_entry/<?php echo $common_entry_image; ?>"  class="img-fluid rounded "  alt="card img">
                </div>
                <div class="card-title text-uppercase text-primary"> <?php echo $common_entry_name; ?> 
                </div>
               
                <ul  class="list-inline text-center">
                  <li class="list-inline-item">
                    <?php if($common_entry_image!='') { ?>
                      <a title="View Image" target="_blank" href="../img/common_entry/<?php echo $common_entry_image;?>" name="Id Proof" class="btn btn-warning btn-sm"><i class="fa fa-id-card-o"></i></a>
                    <?php } ?>
                  </li>
 
                  <li class="list-inline-item">
                     

                     <a  class="btn btn-primary btn-sm" onclick="getCommEntry('<?php echo $common_entry_id; ?>');" data-toggle="modal" data-target="#editCM" href="javascript:void();">
                          <span class=" "><i class="fa fa-pencil-square-o"></i></span>

                           </a>


                  </li>
 
                  <li class="list-inline-item">
                    <form title="Delete" action="controller/commonEntryController.php" method="post"  >
                      <input type="hidden" name="common_entry_id" value="<?php echo $common_entry_id; ?>">
                      <input type="hidden" value="deleteCommonEntry" name="deleteCommonEntry">
                      <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i></button> 
                    </form>
                  </li>
                
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <?php } } else {
        echo "<img src='img/no_data_found.png'>";
      } ?>
    </div>
  </div>
</div>





<div class="modal fade" id="addComm">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Common Entry </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="comResp">
           <form id="CommEntryFrm" action="controller/commonEntryController.php" method="POST" enctype="multipart/form-data" >
                
                
                <div class="form-group row">
                 
               <label for="input-13" class="col-sm-2 col-form-label">Menu Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="common_entry_name" id="common_entry_name" value="">
                  </div> 
                </div>
                 <div class="form-group row">
                    <label for="input-14" class="col-sm-2 col-form-label">Image </label>
                  <div class="col-sm-10">
                     
                    <input required="" accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG"   type="file" class="form-control-file photoOnly border"  name="common_entry_image">
                    (Photo  No more than 4 MB)
                  </div>
                </div>
 
                <div class="form-footer text-center">
                    <button type="submit" id="addCommEntryBtn" class="btn btn-success" name="addCommEntry"><i class="fa fa-check-square-o"></i> SAVE</button>
                    <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> RESET</button>
                </div>
              
              </form>
      </div>
     
    </div>
  </div>
</div><!--End Modal -->



<div class="modal fade" id="editCM">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Common Entry</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="editCMDiv" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>

 