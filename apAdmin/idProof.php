  <?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">ID Proof</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteIdProof');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Name</th>
                        <th>Pages</th>                      
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("id_proof_master","society_id='$society_id'","ORDER BY id_proof_id DESC");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                        <?php  $totalIdProof = $d->count_data_direct("id_proof_id","user_id_proofs","id_proof_id='$data[id_proof_id]'");
                            if( $totalIdProof ==0) {
                          ?>
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['id_proof_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['id_proof_id']; ?>">
                           <?php } ?>   
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['id_proof_name']; ?></td>
                       <td><?php echo $data['id_proof_pages']; ?></td>
                       <td>
                       <div class="d-flex align-items-center">
                        <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="idProofDataSet(<?php echo $data['id_proof_id']; ?>)"> <i class="fa fa-pencil"></i></button>

                          <?php if($data['id_proof_status']=="0"){
                              $status = "idProofStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "idProofStatusActive";
                              $active="";  
                          } ?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['id_proof_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                       </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">ID Proof</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addIdProofForm" action="controller/IdProofController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">ID Proof Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                  <input type="text" class="form-control" placeholder="ID Proof Name" id="id_proof_name" name="id_proof_name" value="">
              </div>  
           </div>  
           <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">ID Proof Pages <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                 <!--  <input type="text" class="form-control onlyNumber" placeholder="ID Proof Pages" min="1" id="id_proof_pages" name="id_proof_pages" value=""> -->
                  <select class="form-control" id="id_proof_pages" name="id_proof_pages">
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select>
              </div>  
           </div>                                      
           <div class="form-footer text-center">
              <input type="hidden" id="id_proof_id" name="id_proof_id" value="" >
              <button id="addIdProofBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
              <input type="hidden" name="addIdProof"  value="addIdProof">
              <button id="addIdProofBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
              <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addIdProofForm');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
