<?php
error_reporting(0);
extract(array_map("test_input" , $_REQUEST));
if(isset($manageWorkingHours)) {
  $facility_id = (int)$facility_id;
 ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title"><?php echo $xml->string->manage; ?> <?php echo $xml->string->working_hours; ?></h4>
      </div>
    </div>
   <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
              <div class="table-responsive">
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo $xml->string->day; ?></th>
                      <th><?php echo $xml->string->status; ?></th>
                      <th><?php echo $xml->string->start_time_my_facility; ?></th>
                      <th><?php echo $xml->string->close_time; ?></th>
                      <th><?php echo $xml->string->action; ?></th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php
                     $is=1;
                     $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
                      $sq = $d->select("facility_schedule_master","facility_id='$facility_id' ","ORDER BY facility_day_id ASC, facility_start_time ASC");
                      while ($sData= mysqli_fetch_array($sq)) {
                        extract($sData);
                        $sq1 = $d->selectRow("facility_start_time","facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$facility_day_id' AND facility_schedule_id >'$facility_schedule_id'","LIMIT 1");
                        $nextData = mysqli_fetch_array($sq1);
                        if ($nextData['facility_start_time']!='') {
                          $next_start_time = $nextData['facility_start_time'];
                        } else {
                          $next_start_time  ='00:00:00';
                        }
                        $sq11 = $d->selectRow("facility_end_time","facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$facility_day_id' AND facility_schedule_id <'$facility_schedule_id'","ORDER BY facility_schedule_id DESC LIMIT 1");
                        $prevData = mysqli_fetch_array($sq11);
                        if ($prevData['facility_end_time']!='') {
                          $current_start_time = $prevData['facility_end_time'];
                        } else {
                          $current_start_time  ='00:00:00';
                        }
                      ?>
                    <tr>
                      <td><?php echo $i11= $i++;?> </td>
                      <td><?php echo  $dayName[$facility_day_id];?></td>
                      <td><?php if ($facility_start_time=="00:00:00" || $facility_status==1) {
                        echo "<label class='text-danger'>Close</label>";
                      } else {
                        echo "<label class='text-success'>Open</label>";
                      } ?></td>
                      <td><?php if($facility_start_time!="00:00:00" && $facility_status==0) { echo  date('h:i A',strtotime($facility_start_time));} ?></td>
                      <td><?php if($facility_end_time!="00:00:00" && $facility_status==0) { echo  date('h:i A',strtotime($facility_end_time));} ?></td>
                      <td>
                        <button style="float: left;" data-toggle="modal" data-target="#addTimeSlot"  onclick="addTimeSlotEdit('<?php echo $facility_id;?>','<?php echo $facility_day_id;?>','<?php echo  $dayName[$facility_day_id];?>','<?php echo $i11;?>','<?php echo $next_start_time;?>','<?php echo $facility_end_time;?>');" type="button" class="btn btn-primary btn-sm mr-2" name=""><i class="fa fa-plus"></i> </button>
                        <?php if ($facility_status==0  && $facility_start_time!="00:00:00") { ?>
                          <button style="float: left;" data-toggle="modal" data-target="#addTimeSlot"  onclick="singleTimeSlotEdit('<?php echo $facility_schedule_id;?>','<?php echo $facility_start_time;?>','<?php echo $facility_end_time;?>','<?php echo $facility_id;?>','<?php echo $facility_day_id;?>','<?php echo  $dayName[$facility_day_id];?>','<?php echo $i11;?>','<?php echo $next_start_time;?>','<?php echo $current_start_time;?>');" type="button" class="btn btn-warning btn-sm mr-2" name=""><i class="fa fa-pencil"></i> </button>
                          <form action="controller/facilitiesController.php" method="POST" >
                            <input type="hidden" name="facility_schedule_id" value="<?php echo $facility_schedule_id;?>">
                            <input type="hidden" name="facility_day_id" value="<?php echo $facility_day_id;?>">
                            <input type="hidden" name="facility_id" value="<?php echo $facility_id;?>">
                            <input type="hidden" name="deletedFacilityTimeSLot" value="deletedFacilityTimeSLot">
                           <button type="submit" class="form-btn btn btn-danger btn-sm " name=""><i class="fa fa-trash-o"></i> </button>
                          </form>
                        <?php } ?>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php } else {

if(isset($updatefacilities)) {
  $q=$d->select("facilities_master","facility_id='$facility_id'","");
  $row=mysqli_fetch_array($q);
  extract($row);
 if ($is_taxble=='1' && $gst==1) {
    $gst_amount =  $facility_amount - ($facility_amount * (100/(100+$tax_slab)));  //only for Include
    $facility_amount = $facility_amount-$gst_amount;
    $facility_amount =number_format($facility_amount,2,'.','');
    $gst_amount_tenant =  $facility_amount_tenant - ($facility_amount_tenant * (100/(100+$tax_slab)));  //only for Include
    $facility_amount_tenant = $facility_amount_tenant-$gst_amount_tenant;
    $facility_amount_tenant =number_format($facility_amount_tenant,2,'.','');
 }
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title"><?php echo $xml->string->society; ?> <?php echo $xml->string->facilities; ?></h4>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="facility" action="controller/facilitiesController.php" method="POST" enctype="multipart/form-data">
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->facility; ?> <?php echo $xml->string->name; ?> <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <input maxlength="30" required="" type="text" class="form-control " id="input-10" name="facility_name" value="<?php echo $facility_name;?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->facility; ?> <?php echo $xml->string->description_contact_finca_fragment; ?></label>
                <div class="col-sm-10">
                  <textarea maxlength="2000" type="text" class="form-control" id="input-11" name="facility_description" value=""><?php echo $facility_description; ?></textarea>
                </div>
              </div>
               <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->facility; ?> <?php echo $xml->string->address; ?></label>
                <div class="col-sm-10">
                  <textarea maxlength="250" type="text" class="form-control" id="input-11" name="facility_address" value=""><?php echo $facility_address; ?></textarea>
                </div>
              </div>
              <div>
                <div class="form-group row">
                   <label for="input-10" class="col-sm-2 col-md-2 col-form-label"><?php echo $xml->string->facility; ?> <?php echo $xml->string->block_type; ?> <span class="text-danger">*</span></label>
                  <div class="col-md-4 col-sm-10">
                    <?php $toatlBooking =$d->count_data_direct("booking_id","facilitybooking_master","society_id='$society_id' AND facility_id='$facility_id' AND book_status!=3");
                      ?>
                    <select <?php if ($toatlBooking>0) { echo 'disabled'; } ?> name="facility_type"  class="form-control facility_type">
                      <option value="">-- Select --</option>
                      <option <?php if(isset($updatefacilities) && $facility_type==2){ echo "selected";}  ?> value="2">Free</option>
                      <option <?php if(isset($updatefacilities) && $facility_type==1){ echo "selected";}  ?>  value="1">Month Wise Bookings</option>
                      <option <?php if(isset($updatefacilities) && $facility_type==0){ echo "selected";}  ?> value="0">Day Wise Booking</option>
                      <option <?php if(isset($updatefacilities) && $facility_type==3){ echo "selected";}  ?> value="3">Hourly Wise Booking</option>
                      <option <?php if(isset($updatefacilities) && $facility_type==4){ echo "selected";}  ?> value="4">Time Slot Wise</option>
                    </select>
                    <?php  if ($toatlBooking>0) { ?>
                      <input type="hidden" value="<?php echo $facility_type;?>" name="facility_type">
                    <?php } ?>
                  </div>
                  <label for="input-12sd" required class="col-sm-2 col-form-label"><?php echo $xml->string->facility; ?> <?php echo $xml->string->photo; ?>  <?php if(!isset($updatefacilities)){ echo '<span class=text-danger>*</span>'; }?></label>
                  <div class="col-sm-4">
                    <input class="photoOnly form-control-file border" type="file" accept="image/*"  <?php if(!isset($updatefacilities)){ echo 'required'; }?> name="facility_photo">
                  </div>
                </div>
                <div class="form-group row">
                    <label for="input-12sd" required class="col-sm-2 col-form-label"><?php echo $xml->string->facility; ?> <?php echo $xml->string->photo; ?> </label>
                  <div class="col-sm-4">
                    <input class="photoOnly form-control-file border" type="file" accept="image/*"  name="facility_photo_2">
                  </div>
                    <label for="input-12sd" required class="col-sm-2 col-form-label"><?php echo $xml->string->facility; ?> <?php echo $xml->string->photo; ?> </label>
                  <div class="col-sm-4">
                    <input class="photoOnly form-control-file border" type="file" accept="image/*"   name="facility_photo_3">
                  </div>
                </div>
                <?php if(isset($updatefacilities)) { ?>
                <div class="form-group row">
                   <div class="col-sm-2">
                    </div>
                     <?php if ($facility_photo!="") { ?>
                   <div class="col-sm-3">
                    <img class="card-img-top" height="150" onerror="this.src='../img/facility/default.png'"  src="../img/facility/<?php echo $facility_photo; ?>" alt="Card image cap">
                   </div>
                   <?php } if ($facility_photo_2!="") { ?>
                   <div class="col-sm-3" id="facility_photo_2">
                    <img  class="card-img-top" height="150" onerror="this.src='../img/facility/default.png'"  src="../img/facility/<?php echo $facility_photo_2; ?>" alt="Card image cap">
                    <a style="position: absolute;top: 2px;right: 11px;background: white;width: 17px;height: 20px;padding-left: 2px;;" href="#" onclick="removePhoto2();" class="text-danger"><i class="fa fa-trash-o"></i></a>
                   </div>
                   <?php } if ($facility_photo_3!="") { ?>
                   <div class="col-sm-3" id="facility_photo_3">
                    <img id="" class="card-img-top" height="150" onerror="this.src='../img/facility/default.png'"  src="../img/facility/<?php echo $facility_photo_3; ?>" alt="Card image cap">
                    <a style="position: absolute;top: 2px;right: 11px;background: white;width: 17px;height: 20px;padding-left: 2px;" href="#" onclick="removePhoto3();" class="text-danger"><i class="fa fa-trash-o"></i></a>
                   </div>
                   <?php } ?>
                </div>
              <?php } ?>
                <div id="paidDiv">
                  <div class="form-group row" id="hourly">
                    <label for="facAmountPerMonth" id="amountLbl" class="col-sm-2 col-form-label"><?php echo $xml->string->amount_khatabook; ?>
                      <?php if ($facility_type==0) {
                        echo " [Per Day]";
                      } else if ($facility_type==1) {
                        echo "[Per Month/Per]";
                      } else if ($facility_type==1) {
                        echo "[Per Hour]";
                      }?>
                      <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <input maxlength="12" required="" type="text" class="form-control onlyNumber" inputmode="numeric" id="facility_amount" name="facility_amount" placeholder="Amount" value="<?php echo $facility_amount;?>">
                    </div>
                    <label for="amount_type" class="col-sm-2 col-form-label"><?php echo $xml->string->amount_khatabook; ?>  <?php echo $xml->string->block_type; ?> <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <select required <?php if ($toatlBooking>0) { echo 'disabled'; } ?> class="form-control" name="amount_type" id="amount_type" class="" >
                        <option value="">-- Select --</option>
                        <option <?php if($row['amount_type']=="0"  ){ echo "selected";} ?>  title="Per Person" value="0">Per Person</option>
                        <option <?php if($row['amount_type']=="1"  ){ echo "selected";} ?> title="Fixed" value="1">Fixed</option>
                      </select>
                    </div>
                    <label for="is_taxble" class="col-sm-2 col-form-label"><?php echo $xml->string->bill; ?> <?php echo $xml->string->block_type; ?> <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <select required="" <?php if ($toatlBooking>0) { echo 'disabled'; } ?>  class="form-control  " name="is_taxble" id="is_taxble" class="bill_type-cls" >
                        <option value="">-- Select --</option>
                         <?php include 'billTypeOptionList.php'; ?>
                      </select>
                      <?php  if ($toatlBooking>0) { ?>
                        <input type="hidden" value="<?php echo $is_taxble;?>" name="is_taxble">
                      <?php } ?>
                    </div>
                  </div>
                  <div <?php if($row['is_taxble']=="1"  ){ } else { ?> style="display: none" <?php } ?>  id="gst_detail_div" >
                    <div class="form-group row">
                      <label for="gst" class="col-sm-2 col-form-label"><?php echo $xml->string->tax; ?> <?php echo $xml->string->amount; ?> <span class="text-danger">*</span></label>
                      <div class="col-sm-4">
                        <div class=" icheck-inline">
                          <input   <?php if($row['gst']=="0" && isset($updatefacilities) ){ echo 'checked=""';} else { echo 'checked=""'; }?> <?php if ($toatlBooking>0) { echo 'disabled'; } ?>   type="radio"  id="inline-radio-info_edit" value="0" name="gst">
                          <label for="inline-radio-info_edit"><?php echo $xml->string->included; ?></label>
                        </div>
                        <div class=" icheck-inline">
                          <input  <?php if($row['gst']=="1"  ){ echo 'checked=""';} ?> <?php if ($toatlBooking>0) { echo 'disabled'; } ?>  type="radio" id="inline-radio-success_edit" value="1" name="gst">
                          <label for="inline-radio-success_edit"><?php echo $xml->string->excluded; ?></label>
                        </div>
                         <?php  if ($toatlBooking>0) { ?>
                          <input type="hidden" value="<?php echo $gst;?>" name="gst">
                        <?php } ?>
                      </div>
                      <label for="taxble_type" class="col-sm-2 col-form-label"><?php echo $xml->string->tax_type; ?></label>
                      <div class="col-sm-4">
                        <select    class="form-control  " name="taxble_type" id="taxble_type">
                          <option value="">-- <?php echo $xml->string->select; ?> --</option>
                          <?php include 'taxOptionList.php'; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="tax_slab" class="col-sm-2 col-form-label">Tax Value <span class="text-danger">*</span></label>
                      <div class="col-sm-4">
                        <?php  $q=$d->select("gst_master","status='0'","ORDER BY slab_percentage ASC");
                        ?>
                        <select <?php if ($toatlBooking>0) { echo 'disabled'; } ?> <?php if(mysqli_num_rows($q)){ echo 'required=""'; } ?> required="" class="form-control  " name="tax_slab">
                          <option value="">-- <?php echo $xml->string->select; ?> --</option>
                          <?php
                          while ($row11=mysqli_fetch_array($q)) {
                            ?>
                            <option title="<?php echo $row['description'];?>" <?php if($row['tax_slab']==$row11['slab_percentage']  ){ echo "selected";} ?>   value="<?php echo $row11['slab_percentage'];?>"><?php echo $row11['slab_percentage'];?>%</option>
                          <?php }?>
                        </select>
                         <?php  if ($toatlBooking>0) { ?>
                          <input type="hidden" value="<?php echo $tax_slab;?>" name="tax_slab">
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row" id="perCap">
                 <label id="capDiv" for="input-10" class="col-sm-2 col-md-2 col-form-label pull-right"><?php echo $xml->string->person_capacity; ?>  <?php if ($facility_type==0) {
                        echo " [Per Day]";
                      } else if ($facility_type==1) {
                        echo "[Per Month/Per Person]";
                      } else if ($facility_type==1) {
                        echo "[Per Hour]";
                      }?> <span class="text-danger">*</span></label>
                 <div class="col-md-4 col-sm-10">
                  <input maxlength="7" required="" type="text" id="person_limit" class="form-control onlyNumber" inputmode="numeric" id="input-10" name="person_limit" value="<?php echo $person_limit;?>">
                </div>
                 <label for="input-14" class="col-sm-2 col-form-label"><?php echo $xml->string->balancesheet; ?> <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <select type="text" required="" class="form-control single-select" name="balancesheet_id">
                        <option value="">-- <?php echo $xml->string->select; ?> --</option>
                        <?php
                        error_reporting(0);
                        $q=$d->select("balancesheet_master","society_id='$society_id'","");
                        while ($row11=mysqli_fetch_array($q)) {
                          if ($adminData['admin_type']==1 || $row11['block_id']==0 || in_array($row11['block_id'], $blockAryAccess) || $balancesheet_id==$row11['balancesheet_id']) {
                         ?>
                         <option <?php if($balancesheet_id==$row11['balancesheet_id']) { echo 'selected';} ?> value="<?php echo $row11['balancesheet_id'];?>"><?php echo $row11['balancesheet_name'];?></option>
                       <?php } }?>
                     </select>
                    </div>
              </div>
              <?php 
              if(isset($updatefacilities)){
              } else { ?>
              <h5>Time Slot</h5>
              <hr>
            <input type="hidden" name="scheduleString" value="<?php echo $scheduleString ?>">
             <!-- add new only -->

              <?php  $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
               for ($di=0; $di <count($dayName) ; $di++) {
                ?>
              <div class="form-group row">
                <label class="form-control-label col-lg-2"><?php echo $dayName[$di];?></label>
                <div class="col-lg-1">
                  <input type="hidden" name="facility_day_id[]" value="<?php echo $di;?>">
                  <select class="form-control" name="<?php echo $dayName[$di];?>_status" onchange="hideDiv(<?php echo $di;?>)">
                    <option  value="0"><?php echo $xml->string->open; ?></option>
                    <option  value="1"><?php echo $xml->string->close; ?></option>
                  </select>
                </div>
                <div class="col-lg-3 hideOnClose<?php echo $di;?>" >
                      <input type="text" readonly class="form-control <?php echo $dayName[$di];?>"  name="<?php echo $dayName[$di];?>_opening_time[]" placeholder="Opening Time">
                </div>
                <div class="col-lg-3 hideOnClose<?php echo $di;?>" >
                    <input type="text" onkeydown="resetSubDiv('<?php echo $dayName[$di];?>','<?php echo $di;?>');" readonly class="form-control <?php echo $dayName[$di];?>1"  name="<?php echo $dayName[$di];?>_closing_time[]" placeholder="Closing Time">
                </div>
                <div class="col-lg-3 hideOnClose<?php echo $di;?>" >
                   <a onclick="addMoreTimeslot('<?php echo $dayName[$di];?>','<?php echo $di;?>');" href="javascript:void(0)" ><?php echo $xml->string->add_more; ?></a>
                </div>
                <div class="col-lg-3">
                </div>
                <div class="addMoreTimeslot_<?php echo $dayName[$di];?> col-lg-6  hideOnClose<?php echo $di;?> " style="<?php if($scheduleAry[$di]['facility_status']==1){ echo 'display:none';}?>">
                </div>
              </div>
            <?php } ?>
            <?php } ?>
              <div class="form-footer text-center">
                <?php if(isset($updatefacilities))
                {
                  ?>
                  <input type="hidden" name="facility_photo_old" value="<?php echo $facility_photo; ?>">
                  <input type="hidden" id="facility_photo_2_old" name="facility_photo_2_old" value="<?php echo $facility_photo_2; ?>">
                  <input type="hidden" id="facility_photo_3_old" name="facility_photo_3_old" value="<?php echo $facility_photo_3; ?>">
                  <input type="hidden" name="facility_id" value="<?php echo $facility_id; ?>">
                  <input type="hidden" name="updatefacilities" value="updatefacilities">
                  <input type="submit" name="" value="update Facility" class="btn btn-success">
                <?php } else  { ?>
                  <input type="hidden" name="addfacilities" value="addfacilities">
                  <button type="submit"  class="btn btn-success"> <?php echo $xml->string->save; ?></button>
                </div>
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<div class="modal fade" id="addTimeSlot">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white d-none" id="add_time"> <?php echo $xml->string->add_new; ?> <?php echo $xml->string->working_hours; ?></h5>
        <h5 class="modal-title text-white d-none" id="edit_time"> <?php echo $xml->string->edit; ?> <?php echo $xml->string->working_hours; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="AddTimeSLotDiv">
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<?php if ($facility_id!='') { ?>
  <script type="text/javascript">
    $(document).ready(function() {
      for (var i = 0; i < 7; i++) {
        var hideDiv = $('#hide'+i).val();
        if (hideDiv == 1) {
          $('.hideOnClose'+i).hide();
        }
      }

      $('.facility_type').change(function() {
        if (this.value == '2') {
          $('#paidDiv').hide();
          $("#perCap").hide();
        }
        else {
          $('#paidDiv').show();
          $("#perCap").show();
        }
      });
    });
  </script>
  <?php if($facility_type==2) { ?>
    <script type="text/javascript">
      $('#paidDiv').hide();
      $("#perCap").hide();
    </script>
  <?php }  else { ?>
    <script type="text/javascript">
      $('#paidDiv').show();
      $("#perCap").show();
    </script>
  <?php
  }
} else { ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#paidDiv').hide();
      $('#hourly').hide();
      $('#daily').hide();
      $('#monthly').hide();
      $("#perCap").hide();
      $('.facility_type').change(function() {
        if (this.value == '2') {
          $('#paidDiv').hide();
          $("#perCap").hide();
        }
        else  {
          $('#paidDiv').show();
          $("#perCap").show();
        }
      });
    });
  </script>
<?php } ?>
<script type="text/javascript">
  function hideDiv(id){
    $('.hideOnClose'+id).toggle();
  }
  $('.facility_type').on('change', function() {
    $('#hourly').show();
    if(this.value==3){
      $("#amountLbl").html("Amount [Per Hour] <span class='text-danger'>*</span>");
      $("#capDiv").html("Person Capacity [Per Hour] <span class='text-danger'>*</span>");
    }else  if(this.value==0){
      $("#amountLbl").html("Amount [Per Days] <span class='text-danger'>*</span>");
      $("#capDiv").html("Person Capacity [Per Days] <span class='text-danger'>*</span>");
    } else  if(this.value==1){
      $("#amountLbl").html("Amount [Per Month] <span class='text-danger'>*</span>");
      $("#capDiv").html("Person Capacity [Per Month/Per Person] <span class='text-danger'>*</span>");
    }
  });
  // $('#timings_type_daily').on('change', function() {
  //   $('#daily').toggle();
  // });
  // $('#timings_type_monthly').on('change', function() {
  //   $('#monthly').toggle();
  // });

   function removePhoto2() {
    $('#facility_photo_2').remove();
    $('#facility_photo_2_old').val('');
  }

   function removePhoto3() {
    $('#facility_photo_3').remove();
    $('#facility_photo_3_old').val('');
  }
</script>
