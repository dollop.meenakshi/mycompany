<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Holiday Group</h4>
            </div>
            <div class="col-sm-3">
                <div class="btn-group float-sm-right">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#modalHolidayGroup" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add</a>
                </div>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Holiday Group Name</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $q = $d->select("holiday_group_master");
                                    while ($data = $q->fetch_assoc())
                                    {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $data['holiday_group_name']; ?></td>
                                        <td>
                                            <?php
                                            if($data['holiday_group_active_status'] == "0")
                                            {
                                                $status = "holidayGroupStatusDeactive";
                                                $active = "checked";
                                            }
                                            else
                                            {
                                                $status = "holidayGroupStatusActive";
                                                $active = "";
                                            }
                                            ?>
                                            <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['holiday_group_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-warning btn-sm" onclick="editHolidayGroup('<?php echo $data['holiday_group_id']; ?>');" data-toggle="tooltip" title="Edit Holiday Group"><i class="fa fa-pencil"></i></button>
                                                <form class="deleteForm<?php echo $data['holiday_group_id']; ?> ml-2" action="controller/HolidayController.php" method="post">
                                                    <input type="hidden" name="holiday_group_id" value="<?php echo $data['holiday_group_id']; ?>">
                                                    <input type="hidden" name="deleteHolidayGroup" value="deleteHolidayGroup"/>
                                                    <button type="button" class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $data['holiday_group_id']; ?>');" data-toggle="tooltip" title="Delete Holiday Group"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->

<div class="modal fade" id="modalHolidayGroup">
    <div class="modal-dialog">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Add Holiday Group</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="holidayGroupAddForm" method="POST" action="controller/HolidayController.php" enctype="multipart/form-data">
                    <div class="row form-group">
                        <label class="col-sm-3 form-control-label">Holiday Group Name <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" maxlength="200" autocomplete="off" required name="holiday_group_name" id="holiday_group_name"/>
                        </div>
                    </div>
                    <div class="form-footer text-center">
                        <input type="hidden" name="csrf" value='<?php echo $_SESSION["token"]; ?>'>
                        <input type="hidden" name="addHolidayGroup" value="addHolidayGroup">
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalHolidayGroupEdit">
    <div class="modal-dialog">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Edit Holiday Group</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="holidayGroupEditForm" method="POST" action="controller/HolidayController.php" enctype="multipart/form-data">
                    <div class="row form-group">
                        <label class="col-sm-3 form-control-label">Holiday Group Name <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" maxlength="200" autocomplete="off" required name="holiday_group_name_edit" id="holiday_group_name_edit"/>
                        </div>
                    </div>
                    <div class="form-footer text-center">
                        <input type="hidden" name="csrf" value='<?php echo $_SESSION["token"]; ?>'>
                        <input type="hidden" name="updateHolidayGroup" value="updateHolidayGroup">
                        <input type="hidden" name="holiday_group_id" id="holiday_group_id">
                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>