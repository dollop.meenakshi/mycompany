<?php error_reporting(0);
$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
$month = (int)$_GET['month'];
$laYear = (int)$_GET['laYear'];
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$status = $_GET['status'];
$today = date("Y-m-d");
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Work From Home Requests</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="javascript:void(0)" onclick="DeleteAll('deleteChatGroupMember');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
     </div>

     <form class="branchDeptFilter" action="">
        <div class="row pt-2 pb-2">
        <?php include 'selectBranchDeptEmpForFilterAll.php'; ?>
          <div class="col-md-3 form-group" id="">
            <select type="text" class="form-control single-select " name="status">
              <option <?php if($status == 'all'){ echo "selected"; } ?> value="all">All</option>
              <option <?php if($status == '0'){ echo "selected"; } ?> value="0">Pending</option>
              <option <?php if($status == '1'){ echo "selected"; } ?>  value="1">Accept</option>
              <option <?php if($status == '2'){ echo "selected"; } ?>  value="2">Declined</option>
            </select>
          </div>
          <div class="col-md-3 col-6">
          <select name="laYear" class="form-control" >
            <option value="">Year</option>
            <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $onePreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $currentYear) {
                      echo 'selected';
                    } ?> <?php if ($_GET['laYear'] == '') { echo 'selected';} ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
            <option <?php if ($_GET['laYear'] == $nextYear) {
                      echo 'selected';
                    } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear; ?></option>
          </select>
        </div>
        <div class="col-md-3 col-6">
          <select class="form-control month single-select" name="month" required>
                <option value="">--Select Month --</option>
                <option <?php if($_GET['month']=="01") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '01') { echo 'selected';}?> value="01">January</option>
                <option <?php if($_GET['month']=="02") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '02') { echo 'selected';}?> value="02">February</option>
                <option <?php if($_GET['month']=="03") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '03') { echo 'selected';}?> value="03">March</option>
                <option <?php if($_GET['month']=="04") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '04') { echo 'selected';}?> value="04">April</option>
                <option <?php if($_GET['month']=="05") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '05') { echo 'selected';}?> value="05">May</option>
                <option <?php if($_GET['month']=="06") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '06') { echo 'selected';}?> value="06">June</option>
                <option <?php if($_GET['month']=="07") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '07') { echo 'selected';}?> value="07">July</option>
                <option <?php if($_GET['month']=="08") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '08') { echo 'selected';}?> value="08">August</option>
                <option <?php if($_GET['month']=="09") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '09') { echo 'selected';}?> value="09">September</option>
                <option <?php if($_GET['month']=="10") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '10') { echo 'selected';}?> value="10">October</option>
                <option <?php if($_GET['month']=="11") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '11') { echo 'selected';}?> value="11">November</option>
                <option <?php if($_GET['month']=="12") { echo 'selected';} ?> <?php if($_GET['month'] == '' && $currentMonth == '12') { echo 'selected';}?> value="12">December</option> 
            </select> 
        </div>
          <div class="col-md-3 form-group mt-auto">
            <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
          </div>
        </div>


      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php 
                    $limitQuery = " LIMIT 100";

                    if (isset($bId) && $bId> 0) {
                      $BranchFilterQuery = " AND users_master.block_id='$bId]'";
                    }

                    if (isset($dId) && $dId > 0) {
                        $deptFilterQuery = "AND wfh_master.floor_id='$dId'";
                    }
                    if (isset($uId) && $uId > 0) {
                        $userFilterQuery = "AND wfh_master.user_id='$uId'";
                        $limitQuery = " ";
                    }
                    if (isset($status) && $status != "all") {
                        $statusFilterQuery = "AND wfh_master.wfh_status='$status'";
                    }
                    if (isset($month) && ($month > 0) && ($laYear > 0)) {
                      $yearData  = date('Y-m', strtotime($laYear . "-" . $month.'-01')); 
                      $yearDataQuery = " AND DATE_FORMAT(wfh_master.wfh_start_date,'%Y-%m')='$yearData'";
                    } else {
                      $yearData  = date('Y-m', strtotime($currentYear . "-" . $currentMonth.'-01')); 
                      $yearDataQuery = " AND DATE_FORMAT(wfh_master.wfh_start_date,'%Y-%m') = '$yearData' ";
                    }
                     $yearDataQuery;
                    // if(isset($month) && (int)$_GET['month_year']>0 ) {
                    //   $MonthFilterQuery = " AND DATE_FORMAT(attendance_master.attendance_date_start,'%m')='$_GET[month_year]'";
                    // }

                    $q=$d->selectRow("wfh_master.*,users_master.user_id,users_master.user_designation,users_master.floor_id,users_master.block_id,users_master.user_full_name, PBU.user_full_name AS performed_by_user,PBA.admin_name AS performed_by_admin,floors_master.floor_name,block_master.block_name","block_master,floors_master,users_master,wfh_master LEFT JOIN users_master PBU ON PBU.user_id = wfh_master.wfh_status_changed_by LEFT JOIN bms_admin_master PBA ON PBA.admin_id = wfh_master.wfh_status_changed_by","block_master.block_id=floors_master.block_id AND block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND wfh_master.society_id='$society_id' AND wfh_master.user_id=users_master.user_id $BranchFilterQuery $deptFilterQuery $userFilterQuery $statusFilterQuery $blockAppendQueryUser $yearDataQuery","ORDER BY wfh_master.wfh_id DESC $limitQuery");

                    //if(isset($dId) && $dId > 0){
                ?>
                <table id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>                        
                            <th>Action</th>
                            <th>Employee</th>                      
                            <th>designation</th>
                            <th>WFH Date</th>                      
                            <th>Type</th>                      
                            <th>Department</th>
                            <th>Status</th>                      
                            <th>Performed By</th>                     
                            <th>Address</th>                     
                        
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $i=1;                   
                        $counter = 1;
                        while ($data=mysqli_fetch_array($q)) {
                        
                        ?>
                        <tr>
                        
                            <td><?php echo $counter++; ?></td>
                            <td>
                                <div class="d-flex align-items-center">
                                  <button type="button" class="btn btn-sm btn-primary ml-1" onclick="showWFHDetail(<?php echo $data['wfh_id']; ?>);"><i class="fa fa-eye"></i></button>
                                  <?php if($data['wfh_status'] == 1){ 
                                    if($today < $data['wfh_start_date']){ ?>
                                      
                                <button title="Approve" type="button" class="btn btn-sm btn-warning ml-1" onclick="wfhStatusChange('1', '<?php echo $data['wfh_id']; ?>', '<?php echo $data['wfh_latitude']; ?>', '<?php echo $data['wfh_longitude']; ?>', '<?php echo $data['wfh_attendance_range']; ?>', '<?php echo $data['wfh_take_selfie']; ?>', 'wfhEdit')"><i class="fa fa-pencil"></i></button>
                                  <?php } } ?>
                                </div>
                            </td>
                            <td><?php echo $data['user_full_name']; ?></td>
                            <td><?php echo $data['user_designation']; ?></td>
                            <td><?php echo date("dS M Y", strtotime($data['wfh_start_date']));?></td>
                            <td><?php if($data['wfh_day_type']==0) { echo "Full Day";} else { echo "Half Day"; }?></td>
                            <td><?php echo $data['floor_name'].'-'.$data['block_name']; ?></td>
                            <td>
                                <?php if($data['wfh_status'] == 0){ 
                                
                                ?>
                                <button title="Approve" type="button" class="btn btn-sm btn-primary ml-1" onclick="wfhStatusChange('1', '<?php echo $data['wfh_id']; ?>', '<?php echo $data['wfh_latitude']; ?>', '<?php echo $data['wfh_longitude']; ?>', '<?php echo $data['wfh_attendance_range']; ?>', '<?php echo $data['wfh_take_selfie']; ?>', 'wfhStatusApproved')"><i class="fa fa-check"></i></button>
                                <button title="Decline" type="button" class="btn btn-sm btn-danger ml-1" onclick="wfhStatusChange('2', '<?php echo $data['wfh_id']; ?>')"><i class="fa fa-times"></i></button>
                                <?php }elseif($data['wfh_status'] == 1){ ?>
                                <b><span class="badge badge-pill m-1 badge-success">Approved</span></b>
                                <?php }else{ ?>
                                <b><span class="badge badge-pill m-1 badge-danger">Declined</span></b>
                                <?php } ?>
                            </td>
                            <td><?php if($data['wfh_status_changed_type'] == 0){
                              echo $data['performed_by_user'];
                            }else{
                              echo $data['performed_by_admin'];
                            } ?></td>
                            <td><?php echo $data['wfh_address']; ?></td>
                            
                            
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php  /*} else {  ?>
                  <div class="" role="alert">
                    <span><strong>Note :</strong> Please Select Group</span>
                  </div>
                <?php } */ ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>

<div class="modal fade" id="wfhStatusApprovedModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">WFH Set Range</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
          <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="wfhStatusApprovedFrom" action="controller/wfhController.php" name="leaveStatusApproved"  enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-6 col-form-label">Range (Meter) <span class="text-danger">*</span></label>
                        <div class="col-lg-6 col-md-6 " id="">
                        <input type="text" value="" name="wfh_attendance_range" id="wfh_attendance_range" class="form-control" min="10" onkeyup="chnageWFHRange()">
                        </div>                   
                    </div>  
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-6 col-form-label">WFH Take Selfie <span class="text-danger">*</span></label>
                        <div class="col-lg-6 col-md-6" id="">
                          <select name="wfh_take_selfie" id="wfh_take_selfie" class="form-control">
                            <option value="">--Select--</option>
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                        </div>                   
                    </div>
                    <div class="form-group row">
                        <div class="map" id="map" style="width: 100%; height: 300px;"></div>   
                    </div>            
                    <div class="form-footer text-center">
                      <input type="hidden" id ="action_key" name="action"  value="">
                      <input type="hidden" class="wfh_status" name="wfh_status"  value="">
                      <input type="hidden" class="latitude" value="">
                      <input type="hidden" class="longitude" value="">
                      <input type="hidden" name="wfh_id"  class="wfh_id">
                      <input type="hidden" name="bId"  value="<?php echo $bId; ?>">
                      <input type="hidden" name="dId"  value="<?php echo $dId; ?>">
                      <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> APPROVE </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="wfhStatusRejectModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">WFH Reject Reason</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="wfhStatusRejectForm" action="controller/wfhController.php" enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea required class="form-control" name="wfh_declined_reason"></textarea>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                    <input type="hidden" name="wfhStatusReject"  value="wfhStatusReject">
                      <input type="hidden" class="wfh_status" name="wfh_status"  value="">
                      <input type="hidden" name="wfh_id"  class="wfh_id">
                      <input type="hidden" name="bId"  value="<?php echo $bId; ?>">
                      <input type="hidden" name="dId"  value="<?php echo $dId; ?>">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> REJECT </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="wfhDetailModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">WFH Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
      <div class="row mx-0" id="showWFHData">
      </div>
      <div class="form-group row">
          <div class="map" id="map01" style="width: 100%; height: 300px;"></div>   
      </div> 
      </div>
      
    </div>
  </div>
</div>


<script src="assets/js/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>
<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }

    function chnageWFHRange()
    {
      var latitude = $('.latitude').val();
      var longitude = $('.longitude').val();
      initialize(latitude,longitude);
    }

    function initialize(d_lat,d_long)
    {
        var latlng = new google.maps.LatLng(d_lat,d_long);
        var latitute = d_lat;
        var longitute = d_long;

        var map = new google.maps.Map(document.getElementById('map'), {
        center: latlng,
        zoom: 17
        });
        var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: false,
        anchorPoint: new google.maps.Point(0, -29)
        });
        wfh_attendance_range =$('#wfh_attendance_range').val();
        if(wfh_attendance_range == 0){
          wfh_attendance_range = 10;
        }
        var circle = new google.maps.Circle({
          map: map,
          radius: parseInt(wfh_attendance_range),    // 10 miles in metres
          fillColor: '#AA0000'
        });
        circle.bindTo('center', marker, 'position');
        var parkingRadition = 5;
        var citymap = {
            newyork: {
            center: {lat: latitute, lng: longitute},
            population: parkingRadition
            }
        };
    }

    function initialize01(d_lat,d_long,range)
    {
        var latlng = new google.maps.LatLng(d_lat,d_long);
        var latitute = d_lat;
        var longitute = d_long;

        var map = new google.maps.Map(document.getElementById('map01'), {
        center: latlng,
        zoom: 17
        });
        var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: false,
        anchorPoint: new google.maps.Point(0, -29)
        });
        var circle = new google.maps.Circle({
          map: map,
          radius: parseInt(range),    // 10 miles in metres
          fillColor: '#AA0000'
        });
        circle.bindTo('center', marker, 'position');
        var parkingRadition = 5;
        var citymap = {
            newyork: {
            center: {lat: latitute, lng: longitute},
            population: parkingRadition
            }
        };
    }
</script>
