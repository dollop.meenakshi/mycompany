<?php
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

$society_id=$_COOKIE['society_id'];
$group_id = $_POST['group_id'];
$group_type = $_POST['group_type'];
$csrf = $_POST['csrf'];

if ($group_type!=0) {
	$q = $d->selectRow('*', "chat_group_master", "group_id='$group_id' ");
} else {
	$q = $d->selectRow('*', "chat_group_master,users_master", "chat_group_master.group_id='$group_id' AND users_master.user_id=chat_group_master.created_by");
}
$data = mysqli_fetch_assoc($q);
extract($data);
?>

<?php if($data != ''){ 

if ($group_type==0) {
?>
<div class="form-group row">
	<label for="input-10" class="col-lg-12 col-md-12 col-form-label">Group Name <span class="required">*</span></label>
	<div class="col-lg-12 col-md-12" id="">
		<input type="hidden" name="group_type" value="<?php echo $data['group_type'];?>">
		<input type="text" class="form-control" placeholder="Group Name" id="group_name" name="group_name" value="<?php echo $group_name; ?>">
	</div>  
</div>  
<?php } else { ?>

	<input type="hidden" class="form-control" placeholder="Group Name" id="group_name" name="group_name" value="<?php echo $group_name; ?>">
<?php } ?> 
<div class="form-group row">
	<label for="input-10" class="col-lg-12 col-md-12 col-form-label">Group Icon </label>
	<div class="col-lg-12 col-md-12" id="">
		<input type="file" accept="image/*" class="form-control photoOnly" id="group_icon" name="group_icon" value="">
	</div>  
</div>
<?php if ($group_type==0) { ?>
<div class="form-group row">
	<label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch</label>
	<div class="col-lg-12 col-md-12" id="">
	<select name="block_id" id="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)">
		<option value="">-- Select Branch --</option> 
		<?php 
		$qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
		while ($blockData=mysqli_fetch_array($qb)) {
		?>
		<option <?php if($block_id == $blockData['block_id']){ echo "selected"; } ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
		<?php } ?>

	</select>
	</div>  
</div>
<div class="form-group row">
	<label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department</label>
	<div class="col-lg-12 col-md-12" id="">
	<select name="floor_id" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value);">
		<option value="">-- Select Department --</option> 
		<?php 
			$qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND block_master.block_id=$block_id $blockAppendQuery");  
			while ($depaData=mysqli_fetch_array($qd)) {
		?>
		<option <?php if($floor_id == $depaData['floor_id']){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
		<?php } ?>
	</select>
	</div>  
</div>
<div class="form-group row">
	<label for="input-10" class="col-lg-12 col-md-12 col-form-label">Admin</label>
	<div class="col-lg-12 col-md-12" id="">
	<select name="user_id" id="user_id" class="form-control multiple-select">
		<option value="">-- Select Employee --</option> 
		<?php 
		$user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND floor_id=$floor_id $blockAppendQueryUser");  
		while ($userdata=mysqli_fetch_array($user)) {
		?>
		<option <?php if($user_id == $userdata['user_id']){ echo "selected"; } ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
		<?php } ?>
	</select>
	</div>  
</div>   
<?php } ?>              
<div class="form-footer text-center">
	<input type="hidden" id="group_id" name="group_id" value="<?php echo $group_id ?>" >
	<input type="hidden" name="csrf" value="<?php echo $csrf ?>" >
	<button id="addChatGroupBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
<input type="hidden" name="updateChatGroup"  value="updateChatGroup">
</div>
<?php } ?>

<script type="text/javascript" src="assets/js/select.js"></script>