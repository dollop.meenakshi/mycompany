<?php
$package_name="";
$packaage_description="";
$package_amount="";
$no_of_month="";
extract(array_map("test_input" , $_POST));
if(isset($package_id))
{
  $q=$d->select("package_master","package_id='$package_id'");
  $row=mysqli_fetch_array($q);
  extract($row);
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Add/Edit Package</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="welcome">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Package</li>
        </ol>
      </div>
    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php if(isset($package_id))
              { ?>
            <form id="signupForm" action="controller/packageController.php" method="POST">
              <h4 class="form-header text-uppercase">
                Package Infomation
              </h4>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Package Name <span class="required">*</span></label>
                <div class="col-sm-10">
                  <input type="text" required="" class="form-control" id="input-12" name="package_name" value="<?php echo $package_name; ?>">
                </div>

              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Package Description </label>
                <div class="col-sm-10">
                  <textarea required="" class="form-control" id="input-123" name="packaage_description"><?php echo $packaage_description; ?></textarea>
                </div>

              </div>

              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label"> Amount <span class="required">*</span></label>
                <div class="col-sm-10">
                  <input type="text" min="1" required="" class="form-control" id="trlDays" name="package_amount" value="<?php echo $package_amount; ?>">
                </div>

              </div>
              <div class="form-group row">

                <label for="input-13" class="col-sm-2 col-form-label"> Duration Month<span class="required">*</span></label>
                <div class="col-sm-10">
                  <input type="text" required="" class="form-control" id="emp_sallary" name="no_of_month" value="<?php echo $no_of_month; ?>">
                </div>
              </div>

              <div class="form-group row">
                  
                  <label for="input-11" class="col-sm-2 col-form-label">Menu Permissions <span class="required">*</span></label>
                  <div class="col-sm-4" >
                    <div class="controls">
                      <h6>
                                    
                      </h6>
                     <?php
                      $q=$d->select("role_master","role_id='2'");
                      $roledata=mysqli_fetch_array($q);
                      $menuId=$roledata['menu_id'];
                      $menuId=explode(",", $menuId);

                      $menuId11=$row['menu_id_package'];
                      $menuId11=explode(",", $menuId11);

                    $i=1;
                    $q1=$d->select("master_menu","parent_menu_id=0");
                    while ($dataMenu=mysqli_fetch_array($q1)) {
                      $menu_id=$dataMenu['menu_id'];
                    if($dataMenu['parent_menu_id']==0 && $dataMenu['sub_menu']==0) {
                    if(in_array($menu_id, $menuId)){
                    ?>

                    <label  style="margin-left: -30px;" class="custom-control custom-checkbox error_color">

                      

                      <input <?php if(in_array($menu_id, $menuId11)){ echo "checked"; } ?> type="checkbox" class="menu_id" value="<?php echo $dataMenu['menu_id']; ?>" name="menu_id[]">

                      <span class="custom-control-indicator"></span>

                      <span class="custom-control-description"><b><?php echo $dataMenu['menu_name']; ?></b></span>

                    </label>

                    <?php } }  else if($dataMenu['sub_menu']==1) {

                    $menu_id=$dataMenu['menu_id'];
                    $ids = join("','",$menuId);   
                     $totalPage=  $d->count_data_direct("menu_id","master_menu","parent_menu_id='$menu_id' AND page_status=0 AND menu_id IN ('$ids')");
                      if ($totalPage>0) {
                    ?>
                    <label   class=" custom-checkbox error_color">
                      <span class="custom-control-description"><b><?php echo $dataMenu['menu_name']; ?></b></span>

                    </label>
                    <Br>
                    <!-- sub menu display -->
                    <?php

                    $s=$d->select("master_menu","parent_menu_id='$menu_id' AND page_status=0");

                  

                    while ($sdata=mysqli_fetch_array($s)) {
                    $sub_menu_id=$sdata['menu_id'];
                    if(in_array($sub_menu_id, $menuId)){ 
                    ?>

                    <label class="custom-control custom-checkbox error_color">

                      <input <?php if(in_array($sub_menu_id, $menuId11)){ echo "checked"; } ?>  type="checkbox" class="menu_id" value="<?php echo $sdata['menu_id']; ?>" name="menu_id[]">

                      <span class="custom-control-indicator"></span>

                      <span class="custom-control-description"><?php echo $sdata['menu_name']; ?></span>

                    </label>

                    <?php } } } ?>

                    <?php } } ?>
                  </div>
                  </div>
               
                  
                   <label for="input-11" class="col-sm-2 col-form-label">Page Access <span class="required">*</span></label>
                  <div class="col-sm-4" >
                    <div class="controls">
                      <?php
                      $q=$d->select("role_master","role_id='2'");
                      $pageData=mysqli_fetch_array($q);
                      $pagePrivilege=$pageData['pagePrivilege'];
                      $pagePrivilege=explode(",", $pagePrivilege);

                       $menuId22=$row['page_id_package'];
                      $menuId22=explode(",", $menuId22);

                      $i=1;
                      $q=$d->select("master_menu","parent_menu_id=0","ORDER BY order_no ASC");
                      while ($data=mysqli_fetch_array($q)) {
                      $menu_id=$data['menu_id'];

                      $totalPage=  $d->count_data_direct("menu_id","master_menu","parent_menu_id='$menu_id' AND page_status=1");
                      if ($totalPage>0) {
                       
                      ?>

                      <label  style="margin-left: -30px;" class="custom-control custom-checkbox error_color">

                        <span class="custom-control-indicator"></span>

                        <span class="custom-control-description"><b><?php echo $data['menu_name']; ?></b></span>

                      </label>
                     
                      <!-- sub menu display -->
                      <?php
                      }
                      $s=$d->select("master_menu","parent_menu_id='$menu_id' AND page_status=1");
                      while ($sdata=mysqli_fetch_array($s)) {
                      $sub_menu_id=$sdata['menu_id'];
                      if(in_array($sub_menu_id, $pagePrivilege)){
                      ?>

                      <label class="custom-control custom-checkbox error_color">

                        <input  <?php if(in_array($sub_menu_id, $menuId22)){ echo "checked"; } ?> type="checkbox" class="pagePrivilege" value="<?php echo $sdata['menu_id']; ?>" name="pagePrivilege[]">

                        <span class="custom-control-indicator"></span>

                        <span class="custom-control-description"><?php echo $sdata['menu_name']; ?></span>

                      </label>


                      <?php } } }  ?>
                     </div>
                  </div>

                </div>
                <div class="form-footer text-center">
              <?php if(isset($updatepackage))
              {
                ?>
                <input type="hidden" name="package_id" value="<?php echo ($package_id) ?>">
                <input type="submit" name="updatepackage" value="update package" class="btn btn-success">  
              <?php } else  { ?>   



                  <button type="submit" name="addpackage" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              <?php } ?>
            </form>
          <?php } else { ?>
             <form id="signupForm" action="controller/packageController.php" method="POST">
                <h4 class="form-header text-uppercase">
                  
                  Package Infomation
                </h4>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label">Package Name <span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" required="" class="form-control"  name="package_name" value="<?php echo $package_name; ?>">
                  </div>

                </div>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label">Package Description </label>
                  <div class="col-sm-10">
                    <textarea required="" class="form-control"  name="packaage_description"><?php echo $packaage_description; ?></textarea>
                  </div>

                </div>

                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label"> Amount <span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" min="1" required="" class="form-control" id="trlDays" name="package_amount" value="<?php echo $package_amount; ?>">
                  </div>

                </div>
                <div class="form-group row">

                  <label for="input-13" class="col-sm-2 col-form-label"> Duration Month<span class="required">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" required="" class="form-control" id="emp_sallary" name="no_of_month" value="<?php echo $no_of_month; ?>">
                  </div>
                </div>

                <div class="form-group row">
                    
                    <label for="input-11" class="col-sm-2 col-form-label">Menu Permissions <span class="required">*</span></label>
                    <div class="col-sm-4" >
                      <div class="controls">
                        <h6>
                                      
                        </h6>
                       <?php
                        $q=$d->select("role_master","role_id='2'");
                        $roledata=mysqli_fetch_array($q);
                        $menuId=$roledata['menu_id'];
                        $menuId=explode(",", $menuId);

                      $i=1;
                      $q1=$d->select("master_menu","parent_menu_id=0");
                      while ($dataMenu=mysqli_fetch_array($q1)) {
                        $menu_id=$dataMenu['menu_id'];
                      if($dataMenu['parent_menu_id']==0 && $dataMenu['sub_menu']==0) {
                      if(in_array($menu_id, $menuId)){
                      ?>

                      <label  style="margin-left: -30px;" class="custom-control custom-checkbox error_color">

                        

                        <input <?php if(in_array($menu_id, $menuId)){ echo "checked"; } ?> type="checkbox" class="menu_id" value="<?php echo $dataMenu['menu_id']; ?>" name="menu_id[]">

                        <span class="custom-control-indicator"></span>

                        <span class="custom-control-description"><b><?php echo $dataMenu['menu_name']; ?></b></span>

                      </label>

                      <?php } }  else if($dataMenu['sub_menu']==1) {

                      $menu_id=$dataMenu['menu_id'];
                      $ids = join("','",$menuId);   
                       $totalPage=  $d->count_data_direct("menu_id","master_menu","parent_menu_id='$menu_id' AND page_status=0 AND menu_id IN ('$ids')");
                        if ($totalPage>0) {
                      ?>
                      <label   class=" custom-checkbox error_color">
                        <span class="custom-control-description"><b><?php echo $dataMenu['menu_name']; ?></b></span>

                      </label>
                      <Br>
                      <!-- sub menu display -->
                      <?php

                      $s=$d->select("master_menu","parent_menu_id='$menu_id' AND page_status=0");
                      while ($sdata=mysqli_fetch_array($s)) {
                      $sub_menu_id=$sdata['menu_id'];
                      if(in_array($sub_menu_id, $menuId)){ 
                      ?>

                      <label class="custom-control custom-checkbox error_color">

                        <input <?php if(in_array($sub_menu_id, $menuId)){ echo "checked"; } ?>  type="checkbox" class="menu_id" value="<?php echo $sdata['menu_id']; ?>" name="menu_id[]">

                        <span class="custom-control-indicator"></span>

                        <span class="custom-control-description"><?php echo $sdata['menu_name']; ?></span>

                      </label>

                      <?php } } } ?>

                      <?php } } ?>
                    </div>
                    </div>
                 
                    
                     <label for="input-11" class="col-sm-2 col-form-label">Page Access <span class="required">*</span></label>
                    <div class="col-sm-4" >
                      <div class="controls">
                        <?php
                        $q=$d->select("role_master","role_id='2'");
                        $pageData=mysqli_fetch_array($q);
                        $pagePrivilege=$pageData['pagePrivilege'];
                        $pagePrivilege=explode(",", $pagePrivilege);
                        $i=1;
                        $q=$d->select("master_menu","parent_menu_id=0","ORDER BY order_no ASC");
                        while ($data=mysqli_fetch_array($q)) {
                        $menu_id=$data['menu_id'];

                        $totalPage=  $d->count_data_direct("menu_id","master_menu","parent_menu_id='$menu_id' AND page_status=1");
                        if ($totalPage>0) {
                         
                        ?>

                        <label  style="margin-left: -30px;" class="custom-control custom-checkbox error_color">

                          <span class="custom-control-indicator"></span>

                          <span class="custom-control-description"><b><?php echo $data['menu_name']; ?></b></span>

                        </label>
                       
                        <!-- sub menu display -->
                        <?php
                        }
                        $s=$d->select("master_menu","parent_menu_id='$menu_id' AND page_status=1");
                        while ($sdata=mysqli_fetch_array($s)) {
                        $sub_menu_id=$sdata['menu_id'];
                        if(in_array($sub_menu_id, $pagePrivilege)){
                        ?>

                        <label class="custom-control custom-checkbox error_color">

                          <input  <?php if(in_array($sub_menu_id, $pagePrivilege)){ echo "checked"; } ?> type="checkbox" class="pagePrivilege" value="<?php echo $sdata['menu_id']; ?>" name="pagePrivilege[]">

                          <span class="custom-control-indicator"></span>

                          <span class="custom-control-description"><?php echo $sdata['menu_name']; ?></span>

                        </label>


                        <?php } } }  ?>
                       </div>
                    </div>

                  </div>
                  <div class="form-footer text-center">
                <?php if(isset($updatepackage))
                {
                  ?>
                  <input type="hidden" name="package_id" value="<?php echo ($package_id) ?>">
                  <input type="submit" name="updatepackage" value="update package" class="btn btn-success">  
                <?php } else  { ?>   
                    <button type="submit" name="addpackage" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                  </div>
                <?php } ?>
            </form>
          <?php } ?>

          </div>
        </div>
      </div>
    </div><!--End Row-->

  </div>
  <!-- End container-fluid-->

    </div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">

  $(function() {

    $('.chk_boxes').click(function() {

        $('.menu_id').prop('checked', this.checked);

    });

});

</script>