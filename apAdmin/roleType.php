  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Admin Role</h4>
     </div>
     <div class="col-sm-3">
       <div class="btn-group float-sm-right">
       <?php if($adminData['role_id']<3 || $adminData['admin_type']==1){ ?>
        <a href="role" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add</a>
        <?php } ?>
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Role</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                  $i=1;
                  //print_r($adminData['role_id']);
                  if($adminData['role_id']<3){
                  $q=$d->select("role_master","");
                  }else{
                    $q=$d->select("role_master","role_id>2");
                  }
                  while ($data=mysqli_fetch_array($q)) {
                    $role=$data['role_id'];
                    
                   ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $data['role_name']; ?> <?php if($data['role_id']==2) { echo " (CHPL Admin)"; } else if ($data['admin_type']==1) {
                      echo " ($_COOKIE[society_name] Master Admin)"; }?></td>
                     <td><?php echo $data['role_description']; ?> </td>
                    <td>
                    <?php if($adminData['role_id']<3 || $adminData['admin_type']==1){ ?>
                      <!-- <form action="pagePrivilege" method="post" style ='float: left;margin-right: 5px;'>
                        <input type="hidden" name="role_id" value="<?php echo $data['role_id']; ?>">
                        <button name="editRole" class="btn btn-sm btn-success" data-toggle="tooltip" title="Page Privilege"> <i class="fa fa-edit"></i> Page Privileges</button>
                      </form> -->
                      <form action="role" method="post" style ='float: left;margin-right: 5px;'>
                        <input type="hidden" name="role_id" value="<?php echo $data['role_id']; ?>">
                        <button name="editRole" class="btn btn-sm btn-primary mr-2" data-toggle="tooltip" title="Edit Role"> <i class="fa fa-pencil"></i></button>
                      </form>
                      <?php

                         if($data['role_status']=="0"){
                              $status = "roleStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "roleStatusActive";
                              $active="";  
                          } 
                         
                          ?>
                          <input <?php if($data['admin_type']==1 || $data['role_id']==1 || $data['role_id']==2){ echo 'disabled'; } ?>  type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['role_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                          <?php  $totalRole = $d->count_data_direct("admin_id","bms_admin_master","role_id='$data[role_id]'");
                          if($totalRole == 0){
                          ?>
                          <form class="deleteForm<?php echo $data['role_id']; ?> mr-2" style ='float: left;' action="controller/menuController.php" method="post">
                            <input type="hidden" name="role_id_delete" value="<?php echo $data['role_id']; ?>">
                            <button name="deleteRole" type="button" class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $data['role_id']; ?>');" data-toggle="tooltip" title="Delete Role"><i class="fa fa-trash-o"></i></button>
                          </form>
                     <?php } } ?>
                    </td>
                  </tr>
                    <?php } ?>
                    
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



