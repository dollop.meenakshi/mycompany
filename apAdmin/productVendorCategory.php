<?php error_reporting(0);

$cId = (int)$_REQUEST['cId'];
$vId = (int)$_REQUEST['vId'];

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-6">
        <h4 class="page-title">Product Vendor Category Master</h4>
      </div>
      <div class="col-sm-3 col-6 text-right">
        <a href="addproductCategoryBtn" data-toggle="modal" data-target="#addModal" onclick="buttonSettingForCat()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
        <a href="javascript:void(0)" onclick="DeleteAll('deleteproductVendorCategory');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
      </div>
    </div>
    <form action="">
      
    </form>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Sr.No</th>
                  <th>Category Name</th>
                  <th>Vendor</th>
                  <th>Date</th>
                  <th>Action</th>

                </tr>
              </thead>
              <tbody id="showFilterData">
                <?php
                if (isset($vId) && $vId > 0) {
                  $vendorFilter = " AND product_category_vendor_master.vendor_id=$vId";
                }
                $q = $d->selectRow('product_category_vendor_master.*,product_category_master.product_category_id,product_category_master.category_name,local_service_provider_users.service_provider_name', "product_category_vendor_master 
                  LEFT JOIN product_category_master ON product_category_master.product_category_id = product_category_vendor_master.product_category_id
                  LEFT JOIN local_service_provider_users ON local_service_provider_users.service_provider_users_id =product_category_vendor_master.vendor_id ", "product_category_master.society_id='$society_id' AND product_category_vendor_master.product_category_id=$cId $vendorFilter  ");
                $counter = 1;
                while ($data = mysqli_fetch_array($q)) {
                 // print_r($data);
                ?>
                  <tr>
                    <td class="text-center">
                      <input type="hidden" name="id" id="id" value="<?php echo $data['product_category_vendor_id']; ?>">
                      <input type="hidden" name="csrf" id="csrf" value="<?php echo $_SESSION["token"]; ?>">
                      <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['product_category_vendor_id']; ?>">
                    </td>
                    <td><?php echo $counter++; ?></td>
                    <td><?php echo $data['category_name']; ?></td>
                    <td><?php echo $data['service_provider_name']; ?></td>
                    <td><?php echo date("d M Y h:i A", strtotime($data['created_date'])); ?></td>
                    <td>
                      <div class="d-flex align-items-center">
                        
                        <?php if ($data['active_status'] == "0") {
                        ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['product_category_vendor_id']; ?>','productVendoCategoryStatusDeactive');" data-size="small" />
                        <?php } else { ?>
                          <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['product_category_vendor_id']; ?>','productVendoCategoryStatusActive');" data-size="small" />
                        <?php } ?>
                      </div>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div><!-- End Row-->

</div>
<!-- End container-fluid-->
</div>
<!--End content-wrapper-->
<div class="modal fade" id="productCategoryDetailModel">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Product Category Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="productCategoryModelDiv" style="align-content: center;">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body">
          <form id="addproductVendorCategoryForm" action="controller/categoryMasterController.php" enctype="multipart/form-data" method="post">

            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Vendor <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <select type="text" multiple class="form-control single-select frmInputSl" id="vendor_id" name="vendor_id[]">
                  <option >-- Select --</option>
                  <?php
                    $getVendorCat = $d->selectRow('*','product_category_vendor_master',"product_category_id=$cId");
                    $filter = "";
                    $vIds = array();
                    if(mysqli_num_rows($getVendorCat)>0){
                        while($data=mysqli_fetch_assoc($getVendorCat)){
                            if($data){
                                $vId  =$data['vendor_id'];
                                array_push($vIds,$vId);
                            }
                        }
                        if(!empty($vIds)){
                            $vendorIds = join("','",$vIds); 
                            $filter = " AND service_provider_users_id NOT IN ('$vendorIds');";
                        }
                    }

                  $floor = $d->select("local_service_provider_users", " service_provider_status=0 AND service_provider_delete_status=0 $filter ");
                  while ($local_service_provider_users = mysqli_fetch_array($floor)) {
                  ?>
                    <option <?php if (isset($data['vendor_id']) && $data['vendor_id'] == $local_service_provider_users['service_provider_users_id']) {echo "selected";
                            } ?> value="<?php if (isset($local_service_provider_users['service_provider_users_id']) && $local_service_provider_users['service_provider_users_id'] != "") {echo $local_service_provider_users['service_provider_users_id'];} ?>"><?php if (isset($local_service_provider_users['service_provider_name']) && $local_service_provider_users['service_provider_name'] != "") {echo $local_service_provider_users['service_provider_name'];} ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
           <div class="form-footer text-center">
              <input type="hidden" id="product_category_id" name="product_category_id" value="<?php echo $cId; ?>" class="">
              <button id="addproductCategoryBtn" name="addproductCategoryBtn" type="submit" class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
              <input type="hidden" name="addproductCategoryVendor" value="addproductCategoryVendor">

              <button id="addproductCategoryBtn" type="submit" class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>

              <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addproductCategoryForm');"><i class="fa fa-check-square-o"></i> Reset</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
    }
    return false;
  }

  function buttonSettingForCat() {
    $('.hideAdd').show();
    $('.frmRst').val('');
    $('.frmInputSl').val('');
    $('.frmInputSl').select2();
    $('.hideupdate').hide();
  }
</script>
<style>
  .hideupdate {
    display: none;
  }
</style>