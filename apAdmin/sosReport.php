
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-7">
        <h4 class="page-title">SOS  Report</h4>

        </div>
        <div class="col-sm-3 col-5 text-right">
            <!-- <a href="exportCsv.php?report=users"  title="Export to CSV" class="btn btn-warning" type="submit" name="getReport" class="form-control" value="G">Export <i class="fa fa-file-excel-o"></i></a> -->
        </div>
      
     </div>

       
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 

             


                 $q3=$d->select("sos_request_master","society_id='$society_id' ","ORDER BY sos_request_id DESC");
                  $i=1;
               ?>
                
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>SOS By</th>
                        <th>SOS For</th>
                        <th>SOS Msg</th>
                        <th>Time</th>
                        <th>Image</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q3)) {
                   
                       if ($data['user_id']!=0 || $data['gatekeeper_id']!=0 ) {
                  
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php 
                          if ($data['user_id']!=0) {
                                $qbb=$d->select("sos_request_master,users_master,block_master,unit_master","sos_request_master.society_id='$society_id'  AND  sos_request_master.user_id=users_master.user_id  AND block_master.block_id=unit_master.block_id  AND unit_master.unit_id=sos_request_master.unit_id AND sos_request_master.user_id!=0 AND sos_request_master.sos_request_id='$data[sos_request_id]'","ORDER BY sos_request_id DESC" );
                              $subData=mysqli_fetch_array($qbb);
                              $block_name= $subData['user_designation'];
                             echo $userName = $subData['user_full_name'].'('. $block_name.')';
                            } else {
                              $qbb=$d->select("sos_request_master,employee_master","sos_request_master.society_id='$society_id'   AND  sos_request_master.gatekeeper_id=employee_master.emp_id  AND sos_request_master.sos_request_id='$data[sos_request_id]'","" );
                              $subData=mysqli_fetch_array($qbb);
                            echo  $userName =$subData['emp_name'].' (Security Guard)';
                            }
                        ?></td>
                        <td><?php 
                        if($data['send_to']=="1"){
                            echo "Employee";
                        } else  if($data['send_to']=="2") {
                            echo "Security Guard";
                        } else  if($data['send_to']=="3"){
                            echo "Admin";
                        }else  if($data['send_to']=="4"){
                            echo "Security Guard & Admin";
                        }else  if($data['send_to']=="5"){
                            echo "Security Guard & Branch";
                        }else  if($data['send_to']=="6"){
                            echo "Department";
                        } else {
                            echo "All";
                        }

                         ?></td>
                        <td><?php  echo $data['sos_msg'];  ?></td>
                        <td><?php if($data['sos_time']!="") { echo date('Y-m-d h:i A', strtotime($data['sos_time'])); } ?></td>
                        <td><a href="<?php echo $data['sos_image'] ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["sos_msg"]; ?>"><img width="50" src="<?php echo $data['sos_image'];?>" alt=""></a> </td>
                        <td> 
                          <a target="_blank" href="http://www.google.com/maps/place/<?php echo $data['sos_latitude']; ?>,<?php echo $data['sos_longitude']; ?>"> <i class="fa fa-map-marker"></i> View </a>
                        </td>
                    </tr>
                  <?php } } ?>
                </tbody>  
                
            </table>
            

            

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->