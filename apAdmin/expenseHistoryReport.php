<?php
error_reporting(0);
//$month_year= $_REQUEST['month_year'];
//$laYear = $_REQUEST['laYear'];
$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
$uId = (int)$_GET['uId'];
$status = $_GET['status'];

 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-6">
          <?php  if (isset($_REQUEST['singleView']) && $_REQUEST['singleView']=="yes") { ?>
          <h4 class="page-title">Expense Report</h4>
        <?php } else { ?>
          <h4 class="page-title">Paid Expense History Report</h4>
        <?php } ?>
        </div>
        <div class="col-sm-6 text-right">
          <?php  if (isset($_REQUEST['singleView']) && $_REQUEST['singleView']=="yes") { ?>
            <a href="expenseHistoryReport?bId=<?php echo $bId;?>&dId=<?php echo $dId;?>&uId=<?php echo $uId;?>" class="btn btn-primary btn-sm"> View Summary Report</a>
          <?php } else { ?>
            <a href="expenseHistoryReport?singleView=yes&bId=<?php echo $bId;?>&dId=<?php echo $dId;?>&uId=<?php echo $uId;?>" class="btn btn-primary btn-sm"> View Detail Report</a>
          <?php } ?>
        </div>
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
        <?php if (isset($_REQUEST['singleView']) && $_REQUEST['singleView']=="yes") { ?>
          <input type="hidden" name="singleView" value="yes">
        <?php }?>
          <?php include('selectBranchDeptEmpForFilterAll.php');?>
          <div class="col-md-3 col-6 form-group">
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-60 days'));} ?>">   
          </div>
          <div class="col-md-3 col-6 form-group">
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
          </div>  
          <?php  if (isset($_REQUEST['singleView']) && $_REQUEST['singleView']=="yes") { ?>
            <div class="col-md-3 col-6 form-group">
            <select  class="form-control single-select" autocomplete="off"  name="status">  
              <option  <?php if(isset($status) && $status == '0'){ echo  'selected';} ?> value="0"> All</option>
              <option <?php if(isset($status) && $status == '1'){ echo  'selected';} ?> value="1"> Paid</option>
              <option <?php if(isset($status) && $status == '2'){ echo  'selected';} ?> value="2">Un Paid</option>
              <option <?php if(isset($status) && $status == '3'){ echo  'selected';} ?> value="3">Pending</option>
              <option <?php if(isset($status) && $status == '4'){ echo  'selected';} ?> value="4">Approved & Unpaid</option>
              <option <?php if(isset($status) && $status == '5'){ echo  'selected';} ?> value="5">Rejected</option>
            </select>
          </div> 
          <?php } ?> 
          <div class="col-lg-2 col-6 ">
            <label  class="form-control-label"> </label>
              <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
     </div>
    </form>
    <!-- End Breadcrumb-->
          
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
              $i = 1;
                
                if(isset($bId) && $bId>0) {
                  $blockFilterQuery = " AND users_master.block_id='$bId'";
                }
                if (isset($dId) && $dId > 0) {
                  $deptFilterQuery = " AND users_master.floor_id='$dId'";
                }
                if (isset($uId) && $uId > 0) {
                  $userFilterQuery = " AND users_master.user_id='$uId'";
                }

                
                if (isset($_REQUEST['singleView']) && $_REQUEST['singleView']=="yes") {
                  if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                   $dateFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%Y-%m-%d') BETWEEN '$from' AND '$toDate'";
                  }

                   if ($status==1) {
                      $statusFilter = " AND  user_expenses.expense_paid_status=1";
                    }else if ($status==2) {
                       $statusFilter = " AND user_expenses.expense_paid_status=0 AND user_expenses.expense_status=1";
                    }else if ($status==3) {
                       $statusFilter = " AND user_expenses.expense_status=0 AND user_expenses.expense_paid_status=0";
                    } else if ($status==4) {
                       $statusFilter = " AND user_expenses.expense_status=1 AND user_expenses.expense_paid_status=0";
                    } else if ($status==5) {
                       $statusFilter = " AND user_expenses.expense_status=2";
                    } 


                  $q = $d->selectRow('bms_admin_master.admin_name as paid_by_admin, ecm.expense_category_name,approved_admin.admin_name AS approved_admin_name, paidUser.user_full_name AS paid_by_user_name ,approved_user.user_full_name AS approved_user_name,user_expenses.*,users_master.user_full_name,users_master.user_designation,block_master.block_name,users_master.floor_id,floors_master.floor_name,floors_master.floor_id',"user_expenses LEFT JOIN users_master AS approved_user ON approved_user.user_id = user_expenses.expense_approved_by_id LEFT JOIN users_master AS paidUser ON paidUser.user_id = user_expenses.expense_paid_by_id AND user_expenses.expense_paid_by_type=0 LEFT JOIN bms_admin_master ON bms_admin_master.admin_id= user_expenses.expense_paid_by_id AND  user_expenses.expense_paid_by_type=1  LEFT JOIN bms_admin_master AS approved_admin ON approved_admin.admin_id = user_expenses.expense_approved_by_id  LEFT JOIN expense_category_master AS ecm ON ecm.expense_category_id = user_expenses.expense_category_id ,users_master,floors_master,block_master ","user_expenses.society_id=$society_id AND users_master.floor_id=floors_master.floor_id AND users_master.user_id = user_expenses.user_id AND block_master.block_id =users_master.block_id $blockFilterQuery $deptFilterQuery $userFilterQuery $dateFilterQuery $statusFilter","ORDER BY user_expenses.created_at DESC");
                } else {

                  if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                   $dateFilterQuery = " AND DATE_FORMAT(user_expense_history.created_at,'%Y-%m-%d') BETWEEN '$from' AND '$toDate'";
                  }


                  $q = $d->selectRow('paidUser.user_full_name AS paid_by_user_name,bms_admin_master.admin_name,user_expense_history.*,users_master.user_full_name,users_master.user_designation,block_master.block_name,users_master.floor_id,floors_master.floor_name,floors_master.floor_id',"user_expense_history  LEFT JOIN users_master AS paidUser ON paidUser.user_id = user_expense_history.paid_by AND user_expense_history.paid_by_type=1 LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=user_expense_history.paid_by AND  user_expense_history.paid_by_type=0 ,users_master,floors_master,block_master ","user_expense_history.society_id=$society_id AND users_master.floor_id=floors_master.floor_id AND users_master.user_id = user_expense_history.user_id AND block_master.block_id =users_master.block_id $blockFilterQuery $deptFilterQuery $userFilterQuery $dateFilterQuery","ORDER BY user_expense_history.created_at DESC");
                }
                
                  $i=1;
               ?>
                
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
              <?php  if (isset($_REQUEST['singleView']) && $_REQUEST['singleView']=="yes") { ?>
                 <thead>
                    <tr>
                        <th>#</th>
                        <th>Employee</th>
                        <th>Branch</th>
                        <th>Department</th>
                        <th>Amount</th>
                        <th>Expense Month</th>
                        <th>Expense Date</th>
                        <th>Status</th>
                        <th>Expense Title</th>
                        <th>Category</th>
                        <th>Type</th>
                        <th>Paid By</th>
                        <th>Approved By</th>
                        <th>Description</th>
                        <th>Unit</th>
                        <th>Unit Name</th>
                        <th>Unit Price</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                        <td><?php  echo $data['user_expense_id']; //echo $i++; ?></td>
                        <td><?php echo $data['user_full_name']; ?> <?php if($data['user_designation'] !=""){ echo '('.$data['user_designation'].')'; }?></td>
                        <td><?php if($data['block_name'] !=""){ echo $data['block_name']; }?></td>
                        <td><?php echo $data['block_name']; ?></td>
                        <td class="expenseTotal"><?php echo $data['amount']; ?></td>
                        <td><?php echo date("m-Y", strtotime($data['date'])); ?></td>
                        <td><?php if($data['created_at'] !="0000-00-00 00:00:00") {echo date("d M Y", strtotime($data['created_at'])) ;} ?></td>
                        <td>
                          <?php if ($data['expense_paid_status']==1) {
                            echo "Paid";
                          }else if ($data['expense_paid_status']==0 && $data['expense_status']==1) {
                            echo "Un Paid";
                          }else if ($data['expense_paid_status']==0 && $data['expense_status']==0) {
                            echo "Pending";
                          } else if ($data['expense_paid_status']==0 && $data['expense_status']==1) {
                            echo "Approved (Unpaid)";
                          } else if ($data['expense_paid_status']==0 && $data['expense_status']==2) {
                            echo "Rejected";
                          }  ?>

                        </td>
                        <td><?php echo $data['expense_title']; ?></td>
                        <td><?php echo $data['expense_category_name']; ?></td>
                        <td><?php echo ($data['expense_category_type'] == 1) ? 'Unit Wise' : 'Amount Wise'; ?></td>
                        <td><?php if($data['expense_paid_by_type']==1){ echo $data['paid_by_admin']; } else { echo $data['paid_by_user_name']; } ?></td>
                        <td><?php if($data['expense_approved_by_type']==0){ echo $data['approved_user_name']; } else { echo  $data['approved_admin_name']; } ?></td>
                        <td><?php echo $data['description']; ?></td>
                        <td><?php echo ($data['unit'] != "0.00") ? $data['unit'] : ""; ?></td>
                        <td><?php echo $data['expense_category_unit_name']; ?></td>
                        <td><?php echo ($data['expense_category_unit_price'] != "0.00") ? $data['expense_category_unit_price'] : ""; ?></td>
                    </tr>
                  <?php } ?>
                     <tr>
                        <td><b>Total</b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b><div class="tot"></div></b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>  

              <?php } else { ?>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Employee</th>
                        <th>Branch</th>
                        <th>Department</th>
                        <th>Amount</th>
                        <th>Paid Month</th>
                        <th>Paid Date</th>
                        <th>Paid By</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['user_full_name']; ?> <?php if($data['user_designation'] !=""){ echo '('.$data['user_designation'].')'; }?></td>
                        <td><?php if($data['block_name'] !=""){ echo $data['block_name']; }?></td>
                        <td><?php echo $data['block_name']; ?></td>
                        <td class="<?php echo 'expenseTotal'; ?>"><?php echo $data['amount']; ?></td>
                        <td><?php if($data['created_at'] !="0000-00-00 00:00:00") {echo date("m-Y", strtotime($data['created_at'])) ;} ?></td>
                        <td><?php if($data['created_at'] !="0000-00-00 00:00:00") {echo date("d M Y", strtotime($data['created_at'])) ;} ?></td>
                        <td><?php if($data['paid_by_type']==0 ) { echo $data['admin_name']; } else {
                          echo $data['paid_by_user_name'];
                        } ?></td>
                        <td><?php echo $data['remark']; ?></td>
                    </tr>
                  <?php } ?>
                     <tr>
                        <td><b>Total</b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b><div class="tot"></div></b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>  
                <?php } ?>
            </table>
           
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->

   <script src="assets/js/jquery.min.js"></script>
<script>
$(document).ready(function()
{
  
    var sum = 0
    $(".expenseTotal").each(function()
    {
      sum += parseFloat($(this).text());
    });
    $('.tot').text(sum.toFixed(2));
});
</script>