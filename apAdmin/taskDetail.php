<?php error_reporting(0);
  if(isset($_GET['id'])){
    $id = (int)$_GET['id'];
    $pId = (int)$_GET['pId'];
    $status = $_GET['status'];
    //$q=$d->selectRow("*","task_master,users_master","task_master.task_add_by=users_master.user_id AND task_master.society_id='$society_id' AND task_master.task_id='$id'");
    //$q=$d->selectRow("task_master.*,users_master.user_full_name,bms_admin_master.admin_name","task_master LEFT JOIN users_master ON users_master.user_id=task_master.task_add_by LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=task_master.task_add_by","task_master.society_id='$society_id' AND task_master.task_id='$id'");
    //$data=mysqli_fetch_array($q);
    if(isset($pId) && $pId>0) {
      $priorityFilterQuery = " AND task_priority_master.task_priority_id='$pId'";
    }
    if(isset($status) && $status != '') {
        $statusFilterQuery = " AND task_master.task_complete='$status'";
    }
    $q=$d->selectRow("task_master_main.*,task_priority_master.task_priority_name,task_master.*, ABU.user_full_name AS add_by, ATU.user_full_name AS assign_to, ABU.user_designation AS add_by_designation, ATU.user_designation AS assign_to_designation,(SELECT COUNT(*) FROM task_master WHERE society_id='$society_id' AND task_master_main_id='$id' AND parent_task_id=0 ) AS total_task,(SELECT COUNT(*) FROM task_master WHERE society_id='$society_id' AND task_master_main_id='$id' AND parent_task_id=0 AND task_complete=0) AS total_pending_task,(SELECT COUNT(*) FROM task_master WHERE society_id='$society_id' AND task_master_main_id='$id' AND parent_task_id=0 AND task_complete=1) AS total_im_progress_task,(SELECT COUNT(*) FROM task_master WHERE society_id='$society_id' AND task_master_main_id='$id' AND parent_task_id=0 AND task_complete=2) AS total_completed_task","task_master_main,task_priority_master,task_master LEFT JOIN users_master ABU ON ABU.user_id=task_master.task_add_by LEFT JOIN users_master ATU ON ATU.user_id=task_master.task_assign_to","task_master_main.task_master_main_id=task_master.task_master_main_id AND task_master.task_priority_id=task_priority_master.task_priority_id AND task_master.task_master_main_id='$id' AND task_master.parent_task_id=0 $priorityFilterQuery $statusFilterQuery");
    //$taskData=mysqli_fetch_array($q);
  
  ?>
  <div class="content-wrapper pb-0">
      <div class="container-fluid">
        <!-- Breadcrumb-->
          <div class="row pt-2 pb-2">
              <div class="col-sm-3 col-md-6 col-6">
              <h4 class="page-title">Task List</h4>
              </div>
              <div class="col-sm-3 col-md-6 col-6">
          
              </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-12">
              <ol class="breadcrumb">
                <li>
                    <a href="taskDetail?id=<?php echo $id; ?>"><span class="badge badge-pill badge-warning m-1">All (<?php echo $d->count_data_direct("task_id","task_master","society_id='$society_id' AND task_master_main_id='$id' AND parent_task_id=0"); ?>)</span></a>
                    <a href="taskDetail?id=<?php echo $id; ?>&status=0"><span class="badge badge-pill badge-primary m-1">Pending (<?php echo $d->count_data_direct("task_id","task_master","society_id='$society_id' AND task_master_main_id='$id' AND parent_task_id=0 AND task_complete=0"); ?>)</span></a>
                    <a href="taskDetail?id=<?php echo $id; ?>&status=1"><span class="badge badge-pill badge-info m-1">In Progress (<?php echo $d->count_data_direct("task_id","task_master","society_id='$society_id' AND task_master_main_id='$id' AND parent_task_id=0 AND task_complete=1"); ?>)</span></a>
                    <a href="taskDetail?id=<?php echo $id; ?>&status=2"><span class="badge badge-pill badge-success m-1">Completed (<?php echo $d->count_data_direct("task_id","task_master","society_id='$society_id' AND task_master_main_id='$id' AND parent_task_id=0 AND task_complete=2"); ?>)</span></a>
                </li>
              </ol>
            </div>
            <div class="col-md-6 col-12">
              <form action="" class="">
                  <div class="col-md-6 form-group ml-auto">
                      <input type="hidden" name="id" value="<?php echo $id; ?>">
                      <select name="pId" class="form-control single-select" onchange="this.form.submit();">
                          <option value="">-- Select Priority --</option> 
                          <?php 
                              $qp=$d->select("task_priority_master","society_id='$society_id'");  
                              while ($priorityData=mysqli_fetch_array($qp)) {
                          ?>
                          <option  <?php if($pId==$priorityData['task_priority_id']) { echo 'selected';} ?> value="<?php echo $priorityData['task_priority_id'];?>" ><?php echo $priorityData['task_priority_name'];?></option>
                          <?php } ?>
                      </select>
                  </div>
              </form>
            </div>
          </div>
          
          <div class="row">
              <?php while($data=mysqli_fetch_array($q)){ ?>
              <div class="col-lg-12">
                  <div id="accordion<?php echo $data['task_id']; ?>">
                      <div class="card">
                            <?php if($data['task_complete'] == 0){
                                $status = "Pending";
                                $statusClass = "badge-primary";
                                $bgClass = "bg-primary";
                            }
                            else if($data['task_complete'] == 1){
                                $status = "In Progress";
                                $statusClass = "badge-info";
                                $bgClass = "bg-info";
                            }else{
                                $status = "Completed";
                                $statusClass = "badge-success";
                                $bgClass = "bg-success";
                            }
                            ?>
                          <div class="card-header <?php echo $bgClass; ?>">
                            <div class="row">
                              <div class="col-md-6">
                                <button class="btn btn-link text-white shadow-none collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $data['task_id']; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $data['task_id']; ?>"><?php echo $data['task_name']; ?></button>
                              </div>
                            
                              <div class="col-md-6">
                                <div class="form-group row w-100 pt-2 justify-content-end">
                                  <p class="text-white"> <i class="fa fa-hourglass-start" aria-hidden="true"></i> <?php echo $data['task_priority_name']; ?></p>
                                  <p class="pl-3 text-white"> <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("d M Y", strtotime($data['task_due_date'])); ?></p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div id="collapse-<?php echo $data['task_id']; ?>" class="collapse show" data-parent="#accordion<?php echo $data['task_id']; ?>" style="">
                              <div class="card-body">
                                  <?php $totalSubTask = $d->count_data_direct("task_id","task_master","parent_task_id='$data[task_id]'");
                                  $completedSubTask = $d->count_data_direct("task_id","task_master","parent_task_id='$data[task_id]' AND task_complete!=2"); 
                                  $task_complete_precent = 0;
                                  if($totalSubTask > 0){
                                    $task_complete_precent = (($totalSubTask-$completedSubTask)/$totalSubTask)*100;
                                  }else{
                                    if($data['task_complete']==2){
                                      $task_complete_precent = 100;
                                    }
                                  }
                                    ?>
                                  <div class="row m-0 mb-2">
                                    <div class="col-md-9">
                                      <p class="card-title mb-0"><?php echo $data['assign_to']; ?> (<?php echo $data['assign_to_designation']; ?>)</p>
                                      <div class="text-muted small">Added By: <?php echo $data['add_by']?>  (<?php echo $data['add_by_designation']; ?>)</div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="progress-wrapper">
                                        <div class="progress-content mb-1">
                                          <span class="progress-label">Task completed</span>
                                          <span class="progress-percentage"><?php echo round($task_complete_precent,2); ?>%</span>
                                        </div>
                                        <div class="progress">
                                          <div class="progress-bar bg-success" style="width:<?php echo $task_complete_precent ?>%"></div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <?php if( $totalSubTask!=0) { ?>
                                    <div class="text-right mb-1"><a href="taskDetail?action=subtask&task_id=<?php echo $data['task_id'] ?>&main_task_id=<?php echo $data['task_master_main_id'] ?>" class="card-text small"><u>Sub Task (<?php echo $totalSubTask; ?>)</u></a></div>
                                    <?php } ?>
                                  <p class="card-text ml-1 mb-0"><?php echo $data['task_note']?></p>
                                  <?php
                                  $subq=$d->select("task_user_history,users_master,block_master,floors_master","task_user_history.user_id=users_master.user_id AND block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND task_user_history.society_id='$society_id' AND task_user_history.task_id='$data[task_id]'","ORDER BY task_user_history.task_user_history_id DESC");
                                   if(mysqli_num_rows($subq)>0){?>
                                  <div class="row m-0">
                                    <?php while ($subData = mysqli_fetch_assoc($subq)) { ?>
                                    <div class="col-6 col-12 col-lg-3 col-xl-3 pt-3">
                                      <div class="card  no-bottom-margin">
                                        <?php $totalPause = $d->count_data_direct("task_pause_history_id","task_pause_history","task_user_history_id='$subData[task_user_history_id]' AND user_id='$subData[user_id]'"); ?>
                                        <a href="<?php if($totalPause > 0){ echo 'userTaskPauseHistory?id='.$subData['task_user_history_id']; }else{ echo "javascript:void(0)";} ?>">
                                        <div class="card-body text-center">
                                          <div class="row">
                                            <div class="col-3">
                                              <img onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $subData['user_profile_pic']; ?>" width="60" height="60" alt="your image" class="rounded-circle p-1 lazyload">
                                            </div>
                                            <div class="col-9">
                                              <h6 class="mt-2 text-capitalize"><b><?php echo $subData['user_full_name']; ?> <?php if($subData['task_pull_back_date'] == null || $subData['task_pull_back_date'] == '0000-00-00 00:00:00'){ ?><i data-toggle="tooltip" title="Assign User" class="fa fa-check-circle text-success" aria-hidden="true"></i><?php } ?></b> </h6>
                                              <p class="small" ><i class="fa fa-calendar"></i> <?php echo date("d M Y h:i A", strtotime($subData['task_assign_date'])); ?> </p>
                                            </div>
                                          </div>
                                        </div>
                                        </a>
                                      </div>
                                    </div>
                                    <?php } ?>
                                  </div>
                                  <?php } ?>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <?php } ?>
          </div><!-- End Row-->
      </div>
  </div>
<?php }else if(isset($_GET['action'])){ 
  $task_id = (int)$_GET['task_id'];
  $main_task_id = (int)$_GET['main_task_id'];
  $pId = (int)$_GET['pId'];
  $status = $_GET['status'];
  if(isset($pId) && $pId>0) {
    $priorityFilterQuery = " AND task_priority_master.task_priority_id='$pId'";
  }
  if(isset($status) && $status != '') {
      $statusFilterQuery = " AND task_master.task_complete='$status'";
  }
  $q=$d->selectRow("task_master_main.*,task_priority_master.task_priority_name,task_master.*, ABU.user_full_name AS add_by, ATU.user_full_name AS assign_to, ABU.user_designation AS add_by_designation, ATU.user_designation AS assign_to_designation","task_master_main,task_priority_master,task_master LEFT JOIN users_master ABU ON ABU.user_id=task_master.task_add_by LEFT JOIN users_master ATU ON ATU.user_id=task_master.task_assign_to","task_master_main.task_master_main_id=task_master.task_master_main_id AND task_master.task_priority_id=task_priority_master.task_priority_id AND task_master.parent_task_id='$task_id' $priorityFilterQuery $statusFilterQuery");
?>
  <div class="content-wrapper pb-0">
    <div class="container-fluid">
      <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
            <h4 class="page-title">Sub Task Detail</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
              <div class="btn-group float-right">
                <a href="taskDetail?id=<?php echo $main_task_id; ?>" class="btn mr-1 btn-sm btn-primary waves-effect waves-light">Back </a>
               </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-12">
            <ol class="breadcrumb">
              <li>
                  <a href="taskDetail?action=subtask&task_id=<?php echo $task_id; ?>&main_task_id=<?php echo $main_task_id; ?>"><span class="badge badge-pill badge-warning m-1">All (<?php echo $d->count_data_direct("task_id","task_master","society_id='$society_id' AND parent_task_id='$task_id'"); ?>)</span></a>
                  <a href="taskDetail?action=subtask&task_id=<?php echo $task_id; ?>&main_task_id=<?php echo $main_task_id; ?>&status=0"><span class="badge badge-pill badge-primary m-1">Pending (<?php echo $d->count_data_direct("task_id","task_master","society_id='$society_id' AND parent_task_id='$task_id' AND task_complete=0"); ?>)</span></a>
                  <a href="taskDetail?action=subtask&task_id=<?php echo $task_id; ?>&main_task_id=<?php echo $main_task_id; ?>&status=1"><span class="badge badge-pill badge-info m-1">In Progress (<?php echo $d->count_data_direct("task_id","task_master","society_id='$society_id' AND parent_task_id='$task_id' AND task_complete=1"); ?>)</span></a>
                  <a href="taskDetail?action=subtask&task_id=<?php echo $task_id; ?>&main_task_id=<?php echo $main_task_id; ?>&status=2"><span class="badge badge-pill badge-success m-1">Completed (<?php echo $d->count_data_direct("task_id","task_master","society_id='$society_id' AND parent_task_id='$task_id' AND task_complete=2"); ?>)</span></a>
              </li>
            </ol>
          </div>
          <div class="col-md-6 col-12">
            <form action="" class="">
                <div class="col-md-6 form-group ml-auto">
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <select name="pId" class="form-control single-select" onchange="this.form.submit();">
                        <option value="">-- Select Priority --</option> 
                        <?php 
                            $qp=$d->select("task_priority_master","society_id='$society_id'");  
                            while ($priorityData=mysqli_fetch_array($qp)) {
                        ?>
                        <option  <?php if($pId==$priorityData['task_priority_id']) { echo 'selected';} ?> value="<?php echo $priorityData['task_priority_id'];?>" ><?php echo $priorityData['task_priority_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
            </form>
          </div>
        </div>
        
        <div class="row">
            <?php while($data=mysqli_fetch_array($q)){ ?>
            <div class="col-lg-12">
                <div id="accordion<?php echo $data['task_id']; ?>">
                    <div class="card">
                          <?php if($data['task_complete'] == 0){
                              $status = "Pending";
                              $statusClass = "badge-primary";
                              $bgClass = "bg-primary";
                          }
                          else if($data['task_complete'] == 1){
                              $status = "In Progress";
                              $statusClass = "badge-info";
                              $bgClass = "bg-info";
                          }else{
                              $status = "Completed";
                              $statusClass = "badge-success";
                              $bgClass = "bg-success";
                          }
                          ?>
                        <div class="card-header <?php echo $bgClass; ?>">
                          <div class="row">
                            <div class="col-md-6">
                              <button class="btn btn-link text-white shadow-none collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $data['task_id']; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $data['task_id']; ?>"><?php echo $data['task_name']; ?></button>
                            </div>
                          
                            <div class="col-md-6">
                              <div class="form-group row w-100 pt-2 justify-content-end">
                                <p class="text-white"> <i class="fa fa-hourglass-start" aria-hidden="true"></i> <?php echo $data['task_priority_name']; ?></p>
                                <p class="pl-3 text-white"> <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("d M Y", strtotime($data['task_due_date'])); ?></p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div id="collapse-<?php echo $data['task_id']; ?>" class="collapse show" data-parent="#accordion<?php echo $data['task_id']; ?>" style="">
                            <div class="card-body">
                            <?php $totalSubTask = $d->count_data_direct("task_id","task_master","parent_task_id='$data[task_id]'");
                                  $completedSubTask = $d->count_data_direct("task_id","task_master","parent_task_id='$data[task_id]' AND task_complete!=2"); 
                                  $task_complete_precent = 0;
                                  if($totalSubTask > 0){
                                    $task_complete_precent = (($totalSubTask-$completedSubTask)/$totalSubTask)*100;
                                  }else{
                                    if($data['task_complete']==2){
                                      $task_complete_precent = 100;
                                    }
                                  }
                                    ?>
                                  <div class="row m-0 mb-2">
                                    <div class="col-md-9">
                                      <p class="card-title mb-0"><?php echo $data['assign_to']; ?> (<?php echo $data['assign_to_designation']; ?>)</p>
                                      <div class="text-muted small">Added By: <?php echo $data['add_by']?>  (<?php echo $data['add_by_designation']; ?>)</div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="progress-wrapper">
                                        <div class="progress-content mb-1">
                                          <span class="progress-label">Task completed</span>
                                          <span class="progress-percentage"><?php echo round($task_complete_precent,2); ?>%</span>
                                        </div>
                                        <div class="progress">
                                          <div class="progress-bar bg-success" style="width:<?php echo $task_complete_precent ?>%"></div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <?php if( $totalSubTask!=0) { ?>
                                    <div class="text-right mb-1"><a href="taskDetail?action=subtask&task_id=<?php echo $data['task_id'] ?>&main_task_id=<?php echo $data['task_master_main_id'] ?>" class="card-text small"><u>Sub Task (<?php echo $totalSubTask; ?>)</u></a></div>
                                    <?php } ?>
                                  <p class="card-text ml-1 mb-0"><?php echo $data['task_note']?></p>
                                
                                
                                <!-- <button type="button" onclick="getTaskUserHistory(<?php echo $data['task_id'] ?>)" class="btn btn-outline-warning btn-sm" > <i class="fa fa-history" aria-hidden="true"></i> User History</button> -->
                                <?php
                                  $subq=$d->select("task_user_history,users_master,block_master,floors_master","task_user_history.user_id=users_master.user_id AND block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND task_user_history.society_id='$society_id' AND task_user_history.task_id='$data[task_id]'","ORDER BY task_user_history.task_user_history_id DESC");
                                   if(mysqli_num_rows($subq)>0){?>
                                  <div class="row m-0">
                                    <?php while ($subData = mysqli_fetch_assoc($subq)) { ?>
                                    <div class="col-6 col-12 col-lg-3 col-xl-3 pt-3">
                                      <div class="card  no-bottom-margin">
                                        <?php $totalPause = $d->count_data_direct("task_pause_history_id","task_pause_history","task_user_history_id='$subData[task_user_history_id]' AND user_id='$subData[user_id]'"); ?>
                                        <a href="<?php if($totalPause > 0){ echo 'userTaskPauseHistory?id='.$subData['task_user_history_id']; }else{ echo "javascript:void(0)";} ?>">
                                        <div class="card-body text-center">
                                          <div class="row">
                                            <div class="col-3">
                                              <img onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/<?php echo $subData['user_profile_pic']; ?>" width="60" height="60" alt="your image" class="rounded-circle p-1 lazyload">
                                            </div>
                                            <div class="col-9">
                                              <h6 class="mt-2 text-capitalize"><b><?php echo $subData['user_full_name']; ?> <?php if($subData['task_pull_back_date'] == null || $subData['task_pull_back_date'] == '0000-00-00 00:00:00'){ ?><i data-toggle="tooltip" title="Assign User" class="fa fa-check-circle text-success" aria-hidden="true"></i><?php } ?></b> </h6>
                                              <p class="small" ><i class="fa fa-calendar"></i> <?php echo date("d M Y h:i A", strtotime($subData['task_assign_date'])); ?> </p>
                                              
                                            </div>
                                          </div>
                                        </div>
                                        </a>
                                      </div>
                                    </div>
                                    <?php } ?>
                                  </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div><!-- End Row-->
    </div>
</div>
<?php } ?>



<?php if($data['task_assign_to']>0){ ?>
<div class="content-wrapper pt-0">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Task User History</h4>
        </div>
        <?php if($data['task_add_by_type']==1){ ?>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#taskAssignModal" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Assign Task </a>
          </div>
        </div>
        <?php } ?>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <!-- <th>#</th> -->
                        <th>Sr.No</th>                        
                        <th>Branch</th>
                        <th>Department</th>
                        <th>User Name</th>
                        <th>Assign Date</th>
                        <th>Status Change Date</th>                      
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("task_user_history,users_master,block_master,floors_master","task_user_history.user_id=users_master.user_id AND block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND task_user_history.society_id='$society_id' AND task_user_history.task_id='$id'","ORDER BY task_user_history.task_user_history_id DESC");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <!-- <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['task_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['task_id']; ?>">                      
                        </td> -->
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['block_name']; ?></td>
                       <td><?php echo $data['floor_name']; ?></td>
                       <td><?php echo $data['user_full_name']; ?> <?php if($data['task_pull_back_date'] == null){ ?><i data-toggle="tooltip" title="Assign User" class="fa fa-check-circle text-success" aria-hidden="true"></i><?php } ?></td>
                       <td><?php echo date("d M Y h:i A", strtotime($data['task_assign_date'])); ?></td>
                        <td><?php if($data['status_change_date'] != '0000-00-00 00:00:00' && $data['status_change_date'] != null){ echo date("d M Y h:i A", strtotime($data['status_change_date']));} ?></td>
                        <td>
                            <div class="d-flex align-items-center">
                                <?php $totalPause = $d->count_data_direct("task_pause_history_id","task_pause_history","task_user_history_id='$data[task_user_history_id]' AND user_id='$data[user_id]'");
                                if($totalPause > 0){ ?>
                                <a href="userTaskPauseHistory?id=<?php echo $data['task_user_history_id']?>" title="Pause History" class="btn btn-sm btn-warning mr-2"> <i class="fa fa-history"></i></a>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
</div>
<?php } ?>

<div class="modal fade" id="taskStepModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Task Step</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="addTaskStepForm" action="controller/TaskController.php" enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Step Name <span class="required">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                            <input type="text" class="form-control task_step" placeholder="Step Name" id="task_step" name="task_step">
                        </div>                   
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Step Due Date <span class="required">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                            <input type="text" class="form-control task_step_due_date datepicker_task_step_due_date" readonly placeholder="Step Due Date" id="task_step_due_date" name="task_step_due_date">
                        </div>                   
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Step Note </label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea placeholder="Step Note" class="form-control" id="task_step_note" name="task_step_note"></textarea>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                        <input type="hidden" id="task_step_id" name="task_step_id" value="" >
                        <input type="hidden" name="task_id" value="<?php echo $id; ?>" >
                        <button id="addTaskStepBtn" type="submit"  class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                        <input type="hidden" name="addTaskStep"  value="addTaskStep">
                        
                        <button id="addTaskStepBtn" type="submit"  class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>            
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="taskAssignModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Task Step</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="taskAssignForm" action="controller/TaskController.php" enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Branch <span class="required">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                            <select name="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)">
                                <option value="">Select Branch</option> 
                                <?php 
                                $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                                while ($blockData=mysqli_fetch_array($qb)) {
                                ?>
                                <option value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                                <?php } ?>
                            </select>
                        </div>                   
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Department <span class="required">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                            <select name="floor_id" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value);">
                                <option value="">Select Department</option> 
                            </select>
                        </div>                   
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Employee <span class="required">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                            <select name="task_assign_to" id="user_id" class="form-control single-select">
                                <option value="">Select Employee</option> 
                            </select>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                        <input type="hidden" name="task_id" value="<?php echo $id; ?>" >
                        <input type="hidden" name="addTaskAssignUser"  value="addTaskAssignUser">
                        <button id="addTaskStepBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Assign</button>            
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="taskStepDetailModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Task Step Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="taskStepDetailData" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>
  
<div class="modal fade" id="taskUserHistoryModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Task User History</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="table-responsive">
          <table id="example" class="table table-bordered">
            <thead>
              <tr>
                <th>Sr.No</th>                        
                <!-- <th>Branch</th>
                <th>Department</th> -->
                <th>User Name</th>
                <th>Assign Date</th>
                <th>Status Change Date</th>                      
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="showTaskUserHistoryData">

            </tbody>
          </table>
        </div>
      </div>
      
    </div>
  </div>
</div>
