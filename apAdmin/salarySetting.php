
<?php
error_reporting(0);
$lsId = (int) $_REQUEST['lsId'];
$cpId = (int) $_REQUEST['cpId'];

  $Cls = $d->selectRow('salary_setting.*', "salary_setting", "society_id=$society_id");
  $data = mysqli_fetch_assoc($Cls);

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-3 col-md-6 col-6">
        <h4 class="page-title">Salary Setting</h4>
      </div>
      <div class="col-sm-3 col-md-6 col-6">
        <div class="btn-group float-sm-right">
          <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
          <!-- <a href="addLeaveType" data-toggle="modal" data-target="#addModal" onclick="buttonSettingForLmsLesson()" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
          <a href="javascript:void(0)" onclick="DeleteAll('deleteCourseLesson');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        --> </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <form id="salarySetting" action="controller/SalaryController.php" enctype="multipart/form-data" method="post">
          <div class="form-group row">
            <!-- <label for="input-10" class="col-sm-2 col-form-label">Hourly Salary Extra Hours Payout
            </label>
            <div class="col-lg-4 col-md-4" id="">
              <input type="number"  id="hourly_salary_extra_hours_payout" step="any" min=" 0.25" max="10.00" class="form-control onlyNumber inputSl " value="<?php if(isset($data['hourly_salary_extra_hours_payout']) && $data['hourly_salary_extra_hours_payout'] !=""){echo $data['hourly_salary_extra_hours_payout']; } ?>" name="hourly_salary_extra_hours_payout" >
              <i class="text-primary">  For Half amount  set 0.5 ,For Same Amount set 1 ,For Double amount set 2</i>
             
            </div> -->
            <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Salary Stamp/Signature</label>
            <div class="col-lg-4 col-md-4" id="">
              <input type="file" accept="image/*" value="<?php if (isset($data['salary_setting_sign_stamp']) && $data['salary_setting_sign_stamp'] != "") {echo $data['salary_setting_sign_stamp']; } ?>" name="salary_setting_sign_stamp" id="salary_setting_sign_stamp" class="form-control rstFrm">
            </div>
          </div>

          <!-- <div class="form-group row">
            <label for="input-10" class="col-sm-2 col-form-label">Working Days Type
            </label>
            <div class="col-lg-10 col-md-10" id="">
              <select type="text" id="working_day_calculation"  class="form-control single-select" name="working_day_calculation">
                <option <?php if(isset($data['working_day_calculation']) && $data['working_day_calculation']==0){echo "selected";} ?> value="0"> Present Day (Excluded Week Off & Holiday)</option>
                <option <?php if(isset($data['working_day_calculation']) && $data['working_day_calculation']==1){echo "selected";} ?> value="1"> Present Day & Paid Holiday (Excluded Week Off) </option>
                <option <?php if(isset($data['working_day_calculation']) && $data['working_day_calculation']==2){echo "selected";} ?> value="2">Calendar days (Included Week Off & Holiday)</option>
               
              </select>
            </div>
          </div> -->
          
          <div class="form-group row ">
            
            <div class="col-md-12 text-center">
            <?php if(isset($data['salary_setting_sign_stamp']) && $data['salary_setting_sign_stamp'] !=""){?>
                  <img src="../img/stamp/<?php echo $data['salary_setting_sign_stamp']; ?>" width="200px" style="max-height:300px;">
                  <br>
                  <button type="button" onclick="removeStampAndSign(<?php echo $data['salary_setting_id']; ?>)" class="btn btn-sm btn-danger">Remove Stamp / Signature <i class="fa fa-trash"></i></button>
            <?php }?>
            </div>
          </div>
          
          <div class="form-footer text-center">
            <input type="hidden" id="salary_setting_id" name="salary_setting_id" value="<?php if
            (isset($data['salary_setting_id']) && $data['salary_setting_id'] !=""){ echo $data['salary_setting_id']; }  ?>" class="rstFrm">
            <input type="hidden" id="salary_setting_sign_stamp_old" name="salary_setting_sign_stamp_old" value="<?php if
            (isset($data['salary_setting_sign_stamp']) && $data['salary_setting_sign_stamp'] !=""){ echo $data['salary_setting_sign_stamp']; }  ?>" class="rstFrm">
            
            <button type="submit" class="btn btn-success hideAdd course_lesson_button"><i class="fa fa-check-square-o"></i><?php if
            (isset($data['salary_setting_id']) && $data['salary_setting_id'] !=""){echo "Update"; } else { echo "Add";} ?> </button>
            <input type="hidden" name="updateSalarySetting" value="updateSalarySetting">
            <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
            <button type="button" value="add" class="btn btn-danger " onclick="resetLessonForm()"><i class="fa fa-check-square-o"></i> Reset</button>
          </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-fluid-->

</div>


<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css" />
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
</script>
<style>  
input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	  -webkit-appearance: none;
	  margin: 0;
	}
	input[type=number] {
	  -moz-appearance: textfield;
	}
</style>