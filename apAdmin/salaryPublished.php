<?php

$uId = (int) $_REQUEST['uId'];
$dId = (int) $_REQUEST['dId'];
$bId = (int) $_REQUEST['bId'];
$mnth = (int) $_REQUEST['month'];
$status = $_REQUEST['status'];
$s = ((int)$_REQUEST['status'])+1;
if($s>=3){
  $s = 2;
}
$laYear = (int) $_REQUEST['laYear'];

$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$increment = (int) $_REQUEST['increment'];

if (isset($mnth) && ($mnth > 0) && ($laYear > 0)) {
  $MonthFilterTemp = $mnth;
  $yearData  = $mnth . "-" . $laYear;
  $yearDataQuery = " AND salary_slip_master.salary_month_name='$yearData'";
} else {
  $laYear = $currentYear;
  $MonthFilterTemp = date('n') - 1;
  $yearData  = date('n',strtotime('last month')) . "-" . $currentYear;
  $yearDataQuery = " AND salary_slip_master.salary_month_name='$yearData'";
}

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-3 col-md-6 col-6">
        <h4 class="page-title"> Published Salary Slip</h4>
      </div>
      <div class="col-sm-3 col-md-6 col-6">
        <div class="btn-group float-sm-right">
          <a href="bulkSalaryReport?bId=<?php echo $bId;?>&dId=<?php echo $dId;?>&laYear=<?php echo $laYear;?>&month=<?php echo $MonthFilterTemp;?>&is_failed=0" class="btn btn-sm btn-warning waves-effect waves-light mr-1"><i class="fa fa-file-o"></i> View Report </a>
          <?php if (isset($bId) && $bId > 0 && $uId<1 || isset($dId) && $dId > 0 && $uId<1 ) { ?>
          <a href="SalarySlipPrintAll.php?sId=<?php echo $society_id; ?>&month=<?php echo $mnth; ?>&bId=<?php echo $bId; ?>&dId=<?php echo $dId; ?>&uId=<?php echo $uId; ?>&laYear=<?php echo $laYear;?>"  class="btn  btn-sm btn-secondary pull-right mr-1"><i class="fa fa-print fa-lg"></i> Print All </a>
        <?php } ?>
        </div>
      </div>
    </div>
    <form action="" class="branchDeptFilter">

      <div class="row pt-2 pb-2">
        <?php include('selectBranchDeptEmpForFilterAll.php'); ?>
        <div class="col-md-3 form-group">
          <select  <?php if (!isset($uId) || $uId == 0) {  echo "required"; }?> class="form-control single-select" name="month" id="month"   >
              
            <option value=""> -- Select Month --</option>
            <?php 
            $selected = "";
            for ($m = 1; $m <= 12; $m++) {
              $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
              $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
              if (isset($_GET['month'])  && $_GET['month'] != "") {
                if ($_GET['month'] == $m) {
                  $selected = "selected";
                } else {
                  $selected = "";
                }
              } else {
                $selected = "";
                if ($m == date('n',strtotime('last month'))) {
                  $selected = "selected";
                } else {
                  $selected = "";
                }
              }

            ?>
              <option <?php echo $selected; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $month; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-3 form-group">
          <select name="laYear" class="form-control single-select ">
            <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $twoPreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
            <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $onePreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
            <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $currentYear) {
                      echo 'selected';
                    } else {
                      echo '';
                    } ?> <?php if (!isset($_GET['laYear'])) {
                            echo 'selected';
                          } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
            <option <?php if (isset($_GET['laYear']) && $_GET['laYear'] == $nextYear) {
                      echo 'selected';
                    } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
          </select>
        </div>
       
        <div class="col-md-3 form-group ">
          <input class="btn btn-sm btn-success " type="submit" name="getReport" value="Get Data">
        </div>
      </div>
    </form>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php if (isset($status) && $status != "") {
              if ($status == 0) {
                $key = "Checked All";
              } else if ($status == 1) {
                $key = "Published All";
              } else {
                $key = "Share With User";
              }
            ?>
              <div class="col-md-6 float-left">
                <label> Select All</label>
                <input type="checkbox" name="" class="selectAll ml-2" value="">
              </div>
              <div class="col-md-6 float-left">
                <button class="btn btn-sm btn-primary selectAllBtn float-right mb-2" onclick="bulkActionSalarySlip(<?php echo $status; ?>)"> <?php echo $key; ?></button>
              </div>
            <?php } ?>
              <div class="table-responsive">
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>

                      <th>#</th>
                      <th>Sr.No</th>
                      <th>Status</th>
                      <th>Action</th>
                      <th>Department</th>
                      <th>Employee</th>
                      <th>Gross Salary</th>
                      <th>Net Salary</th>
                      <th>Total CTC</th>
                      <th>Month</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Share With User</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i = 1;
                    if (isset($bId) && $bId > 0) {
                      $brnchFilterQuery = " AND users_master.block_id='$bId'";
                    }
                    if (isset($dId) && $dId > 0) {
                      $deptFilterQuery = " AND users_master.floor_id='$dId'";
                    }
                    if (isset($uId) && $uId > 0) {
                      $UserFilterQuery = " AND salary_slip_master.user_id='$uId'";
                    }

                    

                   
                    $q = $d->select("salary_slip_master,users_master,floors_master", "salary_slip_master.user_id=users_master.user_id AND salary_slip_master.floor_id=floors_master.floor_id AND salary_slip_master.society_id='$society_id' AND salary_slip_status=2 AND users_master.user_status !=0  $deptFilterQuery  $UserFilterQuery   $yearDataQuery $statusFilterQuery $brnchFilterQuery", "ORDER BY salary_slip_master.salary_month_name,users_master.user_full_name ASC");
                    $counter = 1;

                    while ($data = mysqli_fetch_array($q)) {

                    ?>
                      <tr>
                        <td class="text-center">
                          <input type="hidden" name="id" id="id" value="<?php echo $data['salary_slip_id']; ?>">
                          <?php if (isset($status) && $status != "") {
                            if ($status == 0 && $data['salary_slip_status'] == 0) { ?>
                              <input type="checkbox" data-id="<?php echo $data['salary_slip_id']; ?>" name="bulkSelectCkbx[]" value="<?php echo $data['salary_slip_id']; ?>" class="blkChkcls">
                            <?php  } else if ($status == 1 && $data['salary_slip_status'] == 1) { ?>
                              <input type="checkbox" data-id="<?php echo $data['salary_slip_id']; ?>" name="bulkSelectCkbx[]" value="<?php echo $data['salary_slip_id']; ?>" class="blkChkcls">
                            <?php } else if ($status == 2 && $data['share_with_user'] == 0) {

                            ?>
                              <input type="checkbox" data-id="<?php echo $data['salary_slip_id']; ?>" name="bulkSelectCkbx[]" value="<?php echo $data['salary_slip_id']; ?>" class="blkChkcls">
                          <?php  }
                          } ?>

                        </td>
                        <td><?php echo $counter++; ?></td>
                        <td>
                          <?php if ($data['salary_slip_status'] == 2) { ?>
                            <span class="badge badge-pill m-1 badge-info">Published</span>
                          <?php } else if ($data['salary_slip_status'] == 0) { ?>
                            <span class="badge badge-pill m-1 badge-primary">Generated</span>
                          <?php } else { ?>
                            <span class="badge badge-pill m-1 badge-warning">Checked</span>
                          <?php } ?>
                        </td>
                        <td>
                          <div class="d-flex align-items-center">
                            
                            <?php  if ($data['salary_slip_status'] == 2) { ?>
                              <a href="SalarySlipPrint.php?sId=<?php echo $society_id; ?>&salId=<?php echo $data['salary_slip_id']; ?>"  class="btn btn-sm  btn-secondary mr-1"><i class="text-white fa fa-file-pdf-o"></i> Print</a>
                            <?php } ?>

                            <button type="button" onclick="salarySlipGeneration(<?php echo $data['salary_slip_id']; ?>)" class="btn btn-info btn-sm salary_slip_modal" data-id="<?php echo $data['salary_id']; ?>"><i class="fa fa-eye"></i></button>
                            <?php if ($data['salary_slip_status'] == 0) {
                              $darray = explode('-', $data['salary_month_name']);
                            ?>
                              <a href="salarySlip?sId=<?php echo $data['salary_slip_id']; ?>&block_id=<?php echo $data['block_id']; ?>&floor_id=<?php echo $data['floor_id']; ?>&user_id=<?php echo $data['user_id']; ?>&year=<?php echo $darray['1']; ?>&month=<?php echo $darray['0']; ?>">
                                <button type="submit" class="btn ml-1 btn-sm btn-warning mr-2"><i class="fa fa-pencil"></i></button>
                                </a>
                          </div>
                          <?php } ?>
                        </td>
                        <td><?php echo $data['floor_name']; ?></td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php echo $data['total_earning_salary']; ?></td>
                        <td><?php echo $data['total_net_salary']; ?></td>
                        <td><?php echo $data['total_ctc_cost']; ?></td>
                        <td><?php echo date(" M  ", strtotime($data['salary_start_date'])) . "-" . date("Y ", strtotime($data['salary_start_date'])); ?></td>
                        <td><?php echo date("d M Y ", strtotime($data['salary_start_date'])); ?></td>
                        <td><?php echo date("d M Y ", strtotime($data['salary_end_date'])); ?></td>
                        <td>
                          <div class="d-flex align-items-center">

                            <?php
                            if ($data['salary_slip_status'] == "2") {
                              if ($data['share_with_user'] == "1") {
                                $sts = "ChangeSalarySlipDeactiveStatus";
                                $active = "checked";
                                $disable = "disabled";
                              } else {
                                $sts = "ChangeSalarySlipStatus";
                                $active = "";
                                $disable  = "";
                              } ?>
                              <input id="salary_slip_id_<?php echo $data['salary_slip_id']; ?>" type="checkbox" <?php echo $active; ?> class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['salary_slip_id']; ?>','<?php echo $sts; ?>');" data-size="small" />
                            <?php } else {
                              echo "Not Published";
                            } ?>
                          </div>
                        </td>
                      </tr>

                    <?php } ?>
                  </tbody>
                </table>
              </div>
           
          </div>
        </div>
      </div>
    </div><!-- End Row-->
  </div>
</div>



<div class="modal fade" id="salarySLipModal">
  <div class="modal-dialog" style="max-width: 60%">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Salary Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div id="salarySlipData"></div>
        <div class="col-md-12 row">
          <div class="col-md-4">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Earning</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody id="earnData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>

                        <th>Deduction</th>
                        <th>Value</th>

                      </tr>
                    </thead>
                    <tbody id="deductData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
          <div class="col-md-4 contributionClass">
            <div class="card">
              <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>

                        <th>Name</th>
                        <th>Employer Contribution</th>

                      </tr>
                    </thead>
                    <tbody id="contributionData">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="row">

          <div class="col-md-6 text-center">
            <label for="exampleInputEmail1">Net Salary  (Earning - Deduction)+Reimbursement </label>
              <h6 id="netSalary"></h6>
          </div>
          <div class="col-md-6 text-center contributionClass">
            <label for="exampleInputEmail1">Total CTC (Earning+Total Contribution) </label>
              <h6 id="totalCtc"></h6>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- ------------ -->
<div class="modal fade" id="askStatusModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Status</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">

          <div class="form-group row">
            <input type="hidden" name="salary_slip_id" id="salary_slip_id">
            <input type="hidden" name="bms_admin" id="bms_admin" value="<?php echo $_COOKIE['bms_admin_id']; ?>">
            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Share With User</label>
            <div class="form-check  mr-1">
              <input class="form-check-input mr-1" type="radio" name="is_notify" id="is_notify" value="1">
              <span class="checkmark"></span>
              <label class="form-check-label" for="exampleRadios1">
                Yes
              </label>
            </div>
            <div class="form-check  mr-1 ">
              <input class="form-check-input  mr-1 " type="radio" name="is_notify" id="is_notify2" value="0">
              <span class="checkmark"></span>
              <label class="form-check-label" for="exampleRadios2">
                No
              </label>
            </div>
          </div>
        </div>
        <div class="form-footer row text-center">
          <div class="col-lg-12 col-md-12" id="">
            <button onclick="changeSalaryPublishStatus()" id="changeSalarySlipStatus" type="submit" class="btn btn-success "><i class="fa fa-check-square-o"></i>Change Status</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="askBulkStatusModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Status</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">

          <div class="form-group row">
            <input type="hidden" name="salary_slip_id" id="salary_slip_id">
            <input type="hidden" name="bms_admin" id="bms_admin" value="<?php echo $_COOKIE['bms_admin_id']; ?>">
            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Share With User</label>
            <div class="form-check  mr-1">
              <input class="form-check-input mr-1" checked type="radio" name="shareWUser"  value="1">
              <span class="checkmark"></span>
              <label class="form-check-label" for="exampleRadios1">
                Yes
              </label>
            </div>
            <div class="form-check  mr-1 ">
              <input class="form-check-input  mr-1 " type="radio" name="shareWUser"  value="0">
              <span class="checkmark"></span>
              <label class="form-check-label" for="exampleRadios2">
                No
              </label>
            </div>
          </div>
        </div>
        <div class="form-footer row text-center">
          <div class="col-lg-12 col-md-12" id="">
            <button onclick="changeSalaryBulkPublishStatus()" id="changeSalarySlipStatus" type="submit" class="btn btn-success "><i class="fa fa-check-square-o"></i>Change Status</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ------------ -->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  //////////////////////holiday data
  function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
    }
    return false;
  }
  $('.selectAll').click(function() {

    if ($(this).prop("checked") == true) {
      $('.blkChkcls').prop('checked', true);
    } else if ($(this).prop("checked") == false) {
      $('.blkChkcls').prop('checked', false);
    }
  });

  function bulkActionSalarySlip(status) {

    var val = [];
      var arr = $('.blkChkcls:checked').map(function() {
        return this.value;
      }).get();
      if (arr.length > 0) {

        if (status == 1) {
          $('#askBulkStatusModal').modal();

        }
        else
        {
          statusArray();
        }
      } else {
        swal("Please checked atlest one check box !", {
          icon: "error",
        });
    }
   
  }

function changeSalaryBulkPublishStatus()
{
  shareWUser = $('input[name="shareWUser"]:checked').val();

  statusArray(shareWUser)
}

function statusArray(shareWtUser=0){
    var val = [];
      var arr = $('.blkChkcls:checked').map(function() {
        return this.value;
      }).get();
      if (arr.length > 0) {

    $.each(arr, function(index, value) {
      var salary_id = value;
      
      var bms_id = $('#bms_admin').val();
      $(".ajax-loader").show();
       var res =  bulkFunction(salary_id,bms_id,shareWtUser);
    });
  
    window.location.replace("manageSalarySlip?bId=<?php echo $bId; ?>&dId=<?php echo $dId; ?>&month=<?php echo $mnth; ?>&laYear=<?php echo $laYear; ?>&status=<?php echo $s; ?>");

    } else {
    swal("Please checked atlest one check box !", {
      icon: "error",
    });
    }
}
  function bulkFunction(salary_id,  bms_id,shareWtUser) {
    status = '<?php echo $status; ?>';
    var m = 1;
    if (m == 1) {
      m = 0;
      var csrf = $('input[name="csrf"]').val();
      $.ajax({
        url: "controller/statusController.php",
        cache: false,
        type: "POST",
        // async: false,
        data: {
          action: "bulkSalaryStatusChanegs",
          salary_id: salary_id,
          status: status,
          csrf: csrf,
          bms_id: bms_id,
          shareWtUser: shareWtUser,

        },
        success: function(response) {
          $(".ajax-loader").hide();
          m = 1;
          if (response.status == '200') {
            $('#processKey_' + user_id).text('Generated');
          }
        }
      });
    }
  }


  var numItems = $('.blkChkcls').length;

  if (numItems <= 0) {
    $('.selectAllBtn').hide();
    $('.selectAll').hide();
  } else {
    $('.selectAllBtn').show();
    $('.selectAll').show();
  }


  
</script>