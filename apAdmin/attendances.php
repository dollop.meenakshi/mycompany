  <?php error_reporting(0);

  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year'] = $_REQUEST['month_year'];

  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row ">
        <div class="col-sm-9 col-12">
          <h4 class="page-title">View Attendances</h4>
        </div>
      </div>
      <form action="" class="branchDeptFilter">
        <div class="row ">
          <?php include 'selectBranchDeptEmpForFilterAll.php' ?>

          <div class="col-md-2 col-6 form-group">
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-1 days'));} ?>">   
          </div>
          <div class="col-md-2 col-6 form-group">
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
          </div>          
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get">
            </div>
        </div>
      </form>
      <div class="row mt-2">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php
                $i = 1;
                if (isset($dId) && $dId > 0) {
                  $deptFilterQuery = " AND users_master.floor_id='$dId'";
                }

                if(isset($bId) && $bId>0) {
                  $blockFilterQuery = " AND users_master.block_id='$bId'";
                }

                if (isset($from) && isset($from) && $toDate != '' && $toDate) {
                  $dateFilterQuery = " AND attendance_master.attendance_date_start BETWEEN '$from' AND '$toDate'";
                }
                if (isset($uId) && $uId > 0) {
                  $userFilterQuery = "AND attendance_master.user_id='$uId'";
                }
               
                $q = $d->selectRow("attendance_master.*,users_master.*,block_master.block_name,floors_master.floor_name,shift_timing_master.shift_type,leave_master.leave_type_id,leave_master.leave_day_type,leave_master.leave_id","attendance_master LEFT JOIN shift_timing_master ON shift_timing_master.shift_time_id = attendance_master.shift_time_id 
                LEFT JOIN leave_master ON leave_master.user_id = attendance_master.user_id AND leave_master.leave_start_date =attendance_master.attendance_date_start,users_master,block_master,floors_master", " users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.user_id=attendance_master.user_id AND attendance_master.society_id='$society_id' AND users_master.delete_status=0  $blockFilterQuery $deptFilterQuery $dateFilterQuery $userFilterQuery $blockAppendQueryUser", "ORDER BY attendance_master.attendance_date_start DESC");
               
                $counter = 1;
             
                ?>
                  <table id="example" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Sr.No</th>
                        <th>Leave</th>
                        <th>location</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>Name</th>
                        <th>Branch (Department)</th>
                        <th>Punch In Date</th>
                        <th>Punch In</th>
                        <th>Punch Out Date</th>
                        <th>Punch Out</th>
                        <th>Hours</th>
                        <th>In Branch</th>
                        <th>Out Branch</th>
                        <th>is modified</th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      while ($data = mysqli_fetch_array($q)) {
                        if ($data['attendance_in_from'] != null) {
                          if ($data['attendance_in_from'] == 0) {
                            $attendance_in_from = " <i class='fa fa-eye' aria-hidden='true'></i>";
                          } else {
                            $attendance_in_from = " <i class='fa fa-mobile' aria-hidden='true'></i>";
                          }
                        } else {
                          $attendance_in_from = "";
                        }
                        if ($data['attendance_out_from'] != null) {
                          if ($data['attendance_out_from'] == 0) {
                            $attendance_out_from = " <i class='fa fa-eye' aria-hidden='true'></i>";
                          } else {
                            $attendance_out_from = " <i class='fa fa-mobile' aria-hidden='true'></i>";
                          }
                        } else {
                          $attendance_out_from = "";
                        }

                      ?>
                        <tr <?php if ($data['is_modified'] == "1") { echo "class='text-danger'";  } ?>>

                          <td><?php echo $counter++; ?></td>
                          <td><?php
                              if (isset($data['leave_day_type']) && $data['leave_type_id']==0) {
                                $autoLeaveMsg = " Auto";
                              } else {
                                $autoLeaveMsg = "";
                              }
                              if (isset($data['leave_day_type']) && $data['leave_day_type'] == 0) {
                                $leave = "Full Day".$autoLeaveMsg;
                              } else if (isset($data['leave_day_type']) && $data['leave_day_type'] == 1) {
                                $leave = "Half Day".$autoLeaveMsg;
                              } else {
                                $leave = "";
                              }
                              if ($data['punch_out_time'] != "00:00:00" && $data['attendance_status'] != "2") {
                              ?>
                              <button class="btn <?php if ($leave!="") { echo 'btn-danger'; } else { echo 'btn-primary'; } ?> btn-sm" onclick="openLeaveModal(<?php if ($data['leave_id'] != '') { echo $data['leave_id']; } else { echo 0;} ?>,<?php echo $data['attendance_id']; ?>,<?php echo $data['user_id']; ?>,'<?php echo date('Y-m-d', strtotime($data['attendance_date_start'])); ?>','<?php echo $interval->format('%h'); ?>')">
                                <?php if ($leave!="") { ?>
                                  <!--  <i class="fa fa-plus" aria-hidden="true"></i> -->
                                <?php } else { ?>
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <?php } echo $leave; ?></button>
                              <?php } else {
                                 if ($data['attendance_date_start'] < date('Y-m-d') && $data['attendance_status'] != 2) {
                              ?>
                                <button type="button" class="btn btn-sm btn-warning " onclick="changeUserAttendace(<?php if ($data['attendance_id'] != '') { echo $data['attendance_id']; }  ?>,'<?php if ($_GET['dId'] != '') {  echo $_GET['dId'];}  ?>','<?php if ($data['attendance_id'] != '') { echo $data['shift_type']; }  ?>','<?php if ($data['attendance_id'] != '') {echo $data['attendance_date_start']; }  ?>','<?php if ($data['punch_in_time'] != '') {echo $data['punch_in_time'];}  ?>')">
                                  <i class="fa fa-clock-o"></i>
                                </button>
                                <?php } 
                              } ?>
                          </td>
                          <td>
                            <button type="button" class="btn btn-sm btn-primary ml-2" onclick="viewUserCheckInOutImage('<?php echo $data['attendance_id']; ?>')"> <i class="fa fa-picture-o"></i> <i class="fa fa-map-marker"></i></button>
                            <?php if ($data['wfh_attendance'] == 1) { ?>
                              <b><span class="badge badge-pill m-1 badge-info"><i class="fa fa-home"></i> WFH</span></b>
                            <?php } ?>
                          </td>
                          <td>

                            <?php
                            if ($data['attendance_status'] == 0) {
                              if ($data['attendance_range_in_km'] != "") {
                                $data['attendance_range_in_km'] = $data['attendance_range_in_km'];
                              } else {
                                $data['attendance_range_in_km'] = 0;
                              }
                              if ($data['attendance_range_in_meter'] != "0.00") {
                                $data['attendance_range_in_meter'] = $data['attendance_range_in_km'];
                              } else {
                                $data['attendance_range_in_meter'] = 0;
                              }
                              if ($data['punch_out_latitude'] != "") {
                                $out_lat = $data['punch_out_latitude'];
                              } else {
                                $out_lat = 0;
                              }
                              if ($data['punch_out_longitude'] != "") {
                                $out_lng = $data['punch_out_longitude'];
                              } else {
                                $out_lng = 0;
                              }

                            ?>
                              <button title="Approve Attendance" type="button" class="btn btn-sm btn-primary ml-1" onclick="pendingAttendanceStatusChange(1, '<?php echo $data['attendance_id']; ?>', 'attendaceStatus','<?php echo $data['punch_in_latitude']; ?>', '<?php echo $data['punch_in_longitude']; ?>','<?php echo $data['block_id']; ?>','<?php echo $data['attendance_range_in_meter'] ?>','<?php echo $data['attendance_range_in_km'] ?>','<?php echo $data['punch_in_image'] ?>','<?php echo $out_lat; ?>','<?php echo $out_lng ?>','<?php echo $data['punch_out_image']; ?>')"><i class="fa fa-check"></i></button>
                              <button title="Decline Attendance" type="button" class="btn btn-sm btn-danger ml-1" onclick="pendingAttendanceStatusChange(2, '<?php echo $data['attendance_id']; ?>', 'attendaceStatus','<?php echo $data['punch_in_latitude']; ?>', '<?php echo $data['punch_in_longitude']; ?>','<?php echo $data['block_id']; ?>','<?php echo $data['attendance_range_in_meter'] ?>','<?php echo $data['attendance_range_in_km'] ?>','<?php echo $data['punch_in_image'] ?>','<?php echo $out_lat; ?>','<?php echo $out_lng ?>','<?php echo $data['punch_out_image']; ?>')"><i class="fa fa-times"></i></button>
                            <?php } else if ($data['attendance_status'] == 1) { ?>
                              <span class="badge badge-pill badge-success m-1">Approved </span>
                            <?php } else { ?>
                              <span class="badge badge-pill badge-danger m-1">Declined </span>
                            <?php } ?>
                          </td>
                          <td>
                            <div class="d-flex align-items-center">
                              <button type="button" class="btn btn-sm btn-primary ml-2" onclick="allAttendaceSet(<?php echo $data['attendance_id']; ?>)"> <i class="fa fa-eye"></i></button>
                              <input type="hidden" name="user_id_attendace" value="<?php if ($data['user_id'] != "") { echo $data['user_id'];}   ?>">
                              
                            </div>
                          </td>
                          <td><?php echo $data['user_full_name']; ?></td>
                          <td><?php echo $data['block_name']; ?> (<?php echo $data['floor_name']; ?>)</td>
                          <td><?php if ($data['attendance_date_start'] != '0000-00-00' && $data['attendance_date_start'] != 'null') {
                                echo date("d M Y", strtotime($data['attendance_date_start'])) . " (" . date("D", strtotime($data['attendance_date_start'])) . ")";
                              } ?></td>
                          <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_in_time'] != 'null') {
                            
                               echo date("h:i A", strtotime($data['punch_in_time'])) . $attendance_in_from;
                              
                              } ?>
                           
                          </td>

                          <td><?php if ($data['attendance_date_end'] != '0000-00-00' && $data['attendance_date_end'] != 'null') { echo date("d M Y", strtotime($data['attendance_date_end']));
                                
                              } ?></td>
                          <td><?php if ($data['punch_out_time'] != '00:00:00' && $data['punch_out_time'] != 'null') {
                                echo date("h:i A", strtotime($data['punch_out_time'])).$attendance_out_from;
                              } ?>
                            <?php if ($data['punch_out_image'] != '') { ?>
                            <?php } ?>
                            <?php if ($data['punch_out_latitude'] != '' & $data['punch_out_longitude'] != '') { ?>
                            <?php } ?>
                          </td>
                          <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_out_time'] != '00:00:00' && $data['total_working_hours']=='') {
                            echo $d->getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],$data['punch_in_time'],$data['punch_out_time']);
                              } else {
                                echo $data['total_working_hours'];
                              } ?>
                          </td>
                           <td><?php  echo $data['punch_in_branch'];  ?></td>
                           <td><?php  echo $data['punch_out_branch'];  ?></td>
                          <td><?php if ($data['is_modified'] == '1' ) {
                             
                              echo  "Yes";
                              } else {
                              echo "No";
                              } ?>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                
              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

  </div>
  <div class="modal fade" id="ChanngeAttendaceDataModal">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Update Attendance</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body">
            <form id="changeAttendance" name="changeAttendance" enctype="multipart/form-data" method="post">
              <div class="form-group row hidePunchDateForShift" id="hidePunchDateForShift">
                <label for="input-10" class="col-sm-6 col-form-label">Punch Out Date <span class="text-danger">*</span></label>
                <div class="col-lg-6 col-md-6" id="">
                  <select type="text" readonly maxlength="10" style="text-transform:uppercase;" name="punch_out_date" id="punch_out_date" class="form-control punch_out_date">
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-6 col-form-label">Punch Out Time <span class="text-danger">*</span></label>
                <div class="col-lg-6 col-md-6" id="">
                  <input type="text" class="form-control time-picker-shft-Out_update" readonly id="punch_out_time" name="punch_out_time">
                </div>
              </div>
              <div class="form-footer text-center">
                <input type="hidden" name="updateUserAttendance" value="updateUserAttendance">
                <input type="hidden" name="attendance_id" id="attendance_id">
                <input type="hidden" class="indate" name="indate" id="indate">
                <button id="addHrDocumentBtn" type="submit" onclick="updateUserAttendanceData()" class="btn btn-success addUserBank hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="addAttendaceModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Attendance Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body">
            <div class="row col-md-12 ">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody id="attendanceData">
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row col-md-12 multiPunchInTable" style="display: none;">
              <table class="table table-bordered">
                <thead>
                  <th>Punch In</th>
                  <th>Punch Out</th>
                  <th>Hours</th>
                </thead>
                <tbody id="MulitPunchIn">
                </tbody>
              </table>
            </div>
            <div class="row col-md-12 wrkRpt">

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="UserCheckInLocation">
    <div class="modal-dialog modal-lg">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Attendance Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" >
          <div class="">
            
            <div class="row">
              <div class="col-md-6 col-12 out_reason_hide">
                <div><b>Punch In :</b> <span id="approvInTime"></span></div>
                <div><b>Distance :</b> <span class="InawayShow"></span></div>
                <div class="in_reason_div"><b>In Reason :</b> <span id="in_reason"></span></div>
              </div>
              <div class="col-md-6 col-12 out_reason_hide">
                <div><b>Punch out :</b> <span id="approvOutTime"></span></div>
                <div><b>Distance :</b> <span class="away"></span></div>
                <div class="out_reason_div"><b>Out Reason :</b> <span id="out_reason"></span></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-12 text-center">
                <div class="punchInImage" id=""></div>
              </div>
              <div class="col-md-6 col-12  text-center">
                <div class="punchOutImage" id=""></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-12 text-center mapDiv">
                <div class="map" id="aprovePunchOut" style="width: 100%; height: 300px;"></div>
              </div>
              
            </div>
            <div class="col-md-12">
              <form id="attendanceDeclineReasonForm">
                <div class="col-md-12 attendanceDeclineReason">
                  <label class="form-control-label">Decline Reason <span class="text-danger">*</span></label>
                  <textarea class="form-control" required name="attendance_declined_reason" id="attendance_declined_reason"></textarea>
                </div>
                <div class="form-footer text-center">
                    <input type="hidden" name="value" id="value">
                      <input type="hidden" name="id" id="id">
                      <input type="hidden" name="status" id="status">
                  <button type="submit" class="btn btn-primary attendanceDeclineReasonClass declineCLass ">Change Status</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>




  <div class="modal fade" id="UserCheckInOutImage">
    <div class="modal-dialog modal-lg ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Attendance Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body p-0">

            <div class="row">
              <div class="col-md-6">
                <div><b>Punch In :</b> <span id="in_time"></span></div>
                <div ><b>Distance:</b> <span class="Inkm"></span></div>
                <div ><b>Address:</b> <span class="InAddress"></span></div>
              </div>
              <div class="col-md-6">
                <div><b>Punch Out :</b> <span id="out_time"></span></div>
                <div ><b>Distance:</b> <span class="Outkm"></span></div>
                <div ><b>Address:</b> <span class="OutAddress"></span></div>
               
              </div>
            </div>
             <div class="row">
                <div class="col-md-6 col-6 text-center hideInData">
                  <div class="userCheckInOutImageData " id="userCheckInOutImageData">
                  </div>
                </div>
                <div class="col-md-6 col-6 text-center hideOutData">
                  <div class="userCheckOutImageData " id="userCheckOutImageData">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-12 float-left mapDiv">
                  <div class="map mr-1" id="map3" style="width: 100%; height: 280px;">
                  </div>
                </div>
                
              </div>
            </div>

            
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="attendanceDeclineReason">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Declined Reason</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="holiday" style="align-content: center;">
          <textarea class="form-control" name="attendance_declined_reason" id="attendance_declined_reason"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary attendanceDeclineReasonClass" data-dismiss="modal" aria-label="Close">Save changes</button>
        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="leaveModal2">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Add Leave</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="addLeaveForm" action="controller/leaveController.php" method="post">
          <div class="modal-body" id="leaveBoday" style="align-content: center;">
          </div>
          <input type="hidden" name="leave_date" id="leave_date">
          <input type="hidden" name="leave_user_id" id="leave_user_id">
          <input type="hidden" name="leave_id" id="leave_id">
          <input type="hidden" name="toDateTo" value="<?php echo $toDate; ?>">
          <input type="hidden" name="fromDate" value="<?php echo $from; ?>">
          <input type="hidden" name="branchId" value="<?php echo $_GET['bId']; ?>">
          <input type="hidden" name="dpId" value="<?php echo $_GET['dId']; ?>">
          <input type="hidden" name="leave_attendance_id" id="leave_attendance_id">
          <div class="modal-footer">
            <div class="col-md-12 text-center">
              <input type="hidden" name="MarkAsLeave" value="MarkAsLeave">
              <button type="submit" class="btn btn-primary attendanceDeclineReasonClass">Save changes</button>
              <button type="button" id="DeleteLeave" onclick="removeUserLeave()" class="btn btn-danger ">Remove Leave</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script src="assets/js/jquery.min.js"></script>


  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key(); ?>"></script>

  <script type="text/javascript">
    function initialize(d_lat, d_long, blockRange, blockLat, blockLOng, out_lat, out_lng) {

      var latlng = new google.maps.LatLng(d_lat, d_long);
      var Blatlng = {
        lat: parseFloat(blockLat),
        lng: parseFloat(blockLOng)
      };
      var latitute = d_lat;
      var longitute = d_long;

      var map = new google.maps.Map(document.getElementById('map'), {
        center: latlng,
        zoom: 13
      });
      ////User marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/green-dot.png",
        position: latlng,
        anchorPoint: new google.maps.Point(0, -29)
      });
     
      ////Company marker
      var marker2 = new google.maps.Marker({
        map: map,

        position: Blatlng,
        //  draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
      });
      ////Company circle
      var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange), // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker2, 'position');
      var parkingRadition = 5;
      var citymap = {
        newyork: {
          center: {
            lat: latitute,
            lng: longitute
          },
          population: parkingRadition
        }
      };

      /*  var input = document.getElementById('searchInput5');
       var geocoder = new google.maps.Geocoder();
       var autocomplete10 = new google.maps.places.Autocomplete(input);
       autocomplete10.bindTo('bounds', map); */
      var infowindow = new google.maps.InfoWindow();
     
    }

    function initializeCommonMap(in_lat, in_long,out_lat,out_long) {

      var latlng = new google.maps.LatLng(in_lat, in_long);
      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize3(d_lat, d_long) {
       
      var latlng = new google.maps.LatLng(d_lat, d_long);
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
   

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize2(out_lat, out_long) {

      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlngOut,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));


      var infowindow = new google.maps.InfoWindow();
      
    }


    function mapViewCommon(d_lat, d_long, blockRange, blockLat, blockLOng,in_latitude,out_longitude) {

      var labels = 'C';
      var labelIndex = 0;

      var latlngIn = new google.maps.LatLng(in_latitude, out_longitude);
      var latlng = new google.maps.LatLng(d_lat, d_long);
      var Blatlng = {
        lat: parseFloat(blockLat),
        lng: parseFloat(blockLOng)
      };
      var latitute = d_lat;
      var longitute = d_long;

      var map = new google.maps.Map(document.getElementById('aprovePunchOut'), {
        center: latlng,
        zoom: 13
      });
      ////User marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlng,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));

      var marker1 = new google.maps.Marker({
        map: map,
        position: latlngIn,
        label: labels['0'],
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));

      ////Company marker
      var marker2 = new google.maps.Marker({
        map: map,
        position: Blatlng,
        label: labels['0'],
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker2, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Work Location");
            infowindow.open(map, marker2);
          }
      })(marker2));

      ////Company circle
      var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange), // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker2, 'position');
      var parkingRadition = 5;
      var citymap = {
        newyork: {
          center: {
            lat: blockLat,
            lng: blockLOng
          },
          population: parkingRadition
        }
      };
      
      var infowindow = new google.maps.InfoWindow();

    }

    function mapViewIn(in_latitude,in_longitude,blockRange,blockLat, blockLOng) {

      var labels = 'C';
      var labelIndex = 0;

      var latlngIn = new google.maps.LatLng(in_latitude, in_longitude);
      
      var Blatlng = {
        lat: parseFloat(blockLat),
        lng: parseFloat(blockLOng)
      };
      
      var map = new google.maps.Map(document.getElementById('aprovePunchOut'), {
        center: latlngIn,
        zoom: 13
      });
      ////User marker

      var marker1 = new google.maps.Marker({
        map: map,
        position: latlngIn,
        label: labels['0'],
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));

      ////Company marker
      var marker2 = new google.maps.Marker({
        map: map,
        position: Blatlng,
        label: labels['0'],
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker2, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Work Location");
            infowindow.open(map, marker2);
          }
      })(marker2));

      ////Company circle
      var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange), // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker2, 'position');
      var parkingRadition = 5;
      var citymap = {
        newyork: {
          center: {
            lat: blockLat,
            lng: blockLOng
          },
          population: parkingRadition
        }
      };
      
      var infowindow = new google.maps.InfoWindow();

    }

    function mapViewOut(d_lat, d_long, blockRange, blockLat, blockLOng,in_latitude,out_longitude) {

      var labels = 'C';
      var labelIndex = 0;

      var latlng = new google.maps.LatLng(d_lat, d_long);
      var Blatlng = {
        lat: parseFloat(blockLat),
        lng: parseFloat(blockLOng)
      };
     
      var map = new google.maps.Map(document.getElementById('aprovePunchOut'), {
        center: latlng,
        zoom: 13
      });
      ////User marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlng,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));

      ////Company marker
      var marker2 = new google.maps.Marker({
        map: map,
        position: Blatlng,
        label: labels['0'],
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker2, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Work Location");
            infowindow.open(map, marker2);
          }
      })(marker2));

      ////Company circle
      var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange), // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker2, 'position');
      var parkingRadition = 5;
      var citymap = {
        newyork: {
          center: {
            lat: blockLat,
            lng: blockLOng
          },
          population: parkingRadition
        }
      };
      
      var infowindow = new google.maps.InfoWindow();

    }

    <?php if (!isset($_GET['month_year'])) { ?>
      $('#month_year').val(<?php echo $currentMonth; ?>)
    <?php } ?>

    function popitup(url) {
      newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
      if (window.focus) {
        newwindow.focus()
      }
      return false;
    }
  </script>
  <style>
    .attendance_status {
      min-width: 113px;
    }
  </style>