<?php
extract(array_map("test_input", $_POST));
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
extract($data);

?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Add Leave</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addLeaveForm" action="controller/leaveController.php" enctype="multipart/form-data" method="post">
                <div class="row">
                    <div class="col-md-4 ">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select name="block_id" id="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value); removeLeaveForm();" required>
                                    <option value="">-- Select Branch --</option> 
                                        <?php 
                                        $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                                        while ($blockData=mysqli_fetch_array($qb)) {
                                        ?>
                                    <option value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                                    <?php } ?>
                                
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select name="floor_id" id="floor_id" class="form-control single-select" onchange="getUserByFloorId(this.value);removeLeaveForm();" required>
                                    <option value="">-- Select Department --</option> 
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Employee <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select name="user_id" id="user_id" class="form-control single-select" required onchange="addMoreApplyLeaveForm();">
                                    <option value="">-- Select Employer --</option> 
                                </select>
                            </div>
                        </div> 
                    </div>
                    <!-- <div class="col-md-4">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">No Of Leave <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-12 col-12" id="">
                                <select name="no_of_leave" id="no_of_leave" class="form-control single-select" required onchange="addMoreApplyLeaveForm(this.value);">
                                    <option value="">-- Select No Of Leaves --</option> 
                                    <?php for ($i=1; $i <= 180; $i++) { ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option> 
                                    <?php } ?>
                                </select>
                            </div>
                        </div> 
                    </div> -->
                </div>
                <div class="add-more-leave">
                
                </div>
                
                <div class="form-footer text-center">
                    <button id="addLeaveAssignBulkBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addLeave"  value="addLeave">
                </div>

              </form>
           
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">
      
    </script>