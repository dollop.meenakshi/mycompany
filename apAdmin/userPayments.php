<?php error_reporting(0); ?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12 col-12">
        <h4 class="page-title">User Transactions</h4>
        
      </div>
    </div>
    <form method="get" action="">
    <div class="row pt-2 pb-2">
      <div class="col-lg-2 col-6 mt-2">
        <input type="text" class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" placeholder="From Date">  
      </div>
      <div class="col-lg-2 col-6 mt-2">
        <input  type="text"   class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" placeholder="To Date">  
      </div>
      <div class="col-sm-3 col-6 mt-2">
          <select class="form-control single-select" name="type" >
            <option value="">All Transactions</option>
           
            <option <?php if($_GET['type']==2 && $_GET['type']!='') { echo 'selected';} ?> value="2">Facility</option>
            <option <?php if($_GET['type']==3 && $_GET['type']!='') { echo 'selected';} ?> value="3">Event</option>
            <option <?php if($_GET['type']==4 && $_GET['type']!='') { echo 'selected';} ?> value="4">Penalty</option>
          </select>
      </div>
      <div class="col-sm-3 col-6 mt-2">
        <select type="text" class="form-control single-select" name="unitId" id="unitId">
          <option value="">All <?php echo $xml->string->units; ?></option>
          <?php 
          $q3=$d->select("unit_master,block_master,users_master","users_master.delete_status=0 AND  users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.member_status=0 $blockAppendQuery","");
          while ($blockRow=mysqli_fetch_array($q3)) {
            ?>
            <option <?php if($_GET['unitId']==$blockRow['unit_id']) { echo 'selected'; } ?> value="<?php echo $blockRow['unit_id'];?>"><?php echo $blockRow['user_full_name'];?>-<?php echo $blockRow['user_designation'];?>  (<?php echo $blockRow['block_name'];?>)</option>
          <?php }?>
        </select>
      </div> 
      <div class="col-lg-2 col-6 mt-2">
          <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
      </div>
     
    </div>
    </form>
    <!-- End Breadcrumb-->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th><?php echo $xml->string->unit; ?></th>
                    <th>Payment for</th>
                    <th>Mode</th>
                    <th>Amount</th>
                    <th>Transaction Charges</th>
                    <th>Payment Date</th>
                    <th>Txt Id</th>
                    <th>Invoice No</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php
                  extract(array_map("test_input" , $_GET));
                  if (isset($_GET['type']) && $_GET['type']!='') {
                    $type = (int)$_GET['type'];
                    $appendQuery= " AND society_transection_master.payment_for_type='$type'";
                  }

                  if (isset($_GET['unitId']) && $_GET['unitId']!='') {
                    $unit_id = (int)$_GET['unitId'];
                    $appendQueryUnitId= " AND society_transection_master.unit_id='$unit_id'";
                  }

                  if ($from!="" && $toDate) {
                      $date=date_create($from);
                      $dateTo=date_create($toDate);
                      $nFrom= date_format($date,"Y-m-d 00:00:00");
                      $nTo= date_format($dateTo,"Y-m-d 23:59:59");

                      $appendQueryDate= " AND society_transection_master.transection_date BETWEEN '$nFrom' AND '$nTo'";
                  }

                  $q=$d->select("society_transection_master,unit_master,block_master,users_master","users_master.user_id=society_transection_master.user_id AND users_master.unit_id=society_transection_master.unit_id AND block_master.block_id=unit_master.block_id AND society_transection_master.unit_id=unit_master.unit_id AND society_transection_master.society_id='$society_id' AND society_transection_master.payment_status='success' AND society_transection_master.transection_amount>0  $appendQuery $blockAppendQueryUser  $appendQueryDate $appendQueryUnitId","ORDER BY society_transection_master.transection_id DESC");
                  $i = 0;
                  while($row=mysqli_fetch_array($q))
                  {
                  // extract($row);
                    switch ($row['payment_for_type']) {
                      case '0':
                        $invoice_no= "INVMN".$row['common_id'];
                        break;
                      case '1':
                        $invoice_no= "INVBL".$row['common_id'];
                        break;
                      case '2':
                       $invoice_no= "INVFAC".$row['common_id'];
                        break;
                      case '3':
                        $invoice_no= "INVEV".$row['common_id'];
                        break;
                      case '4':
                        $invoice_no= "INVPN".$row['common_id'];
                        break;
                      default:
                        $invoice_no= "INV".$row['common_id'];
                        break;
                    }
                  $i++;
                  
                  ?>
                  <tr>
                    
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['user_full_name']; ?>-<?php echo $row['user_designation']; ?>(<?php echo $row['block_name']; ?>)</td>
                    <td><?php echo $row['payment_for_name']; ?></td>
                    <td><?php echo $row['payment_mode']; ?></td>
                    <td><?php echo number_format($row['transection_amount']-$row['transaction_charges'],2); ?></td>
                    <td><?php echo number_format($row['transaction_charges'],2); ?></td>
                    <td><?php echo $row['transection_date']; ?></td>
                    <td><?php echo $row['payment_txnid']; ?></td>
                    <td><?php echo $invoice_no; ?></td>
                    
                  </tr>
                  <?php }?>
                </tbody>
                
              </div>
            </table>
          </div>
        </div>
      </div>
    </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->
  
  </div><!--End content-wrapper-->
  <!--Start Back To Top Button-->
