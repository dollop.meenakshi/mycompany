<?php
extract(array_map("test_input", $_POST));
if (isset($edit_holiday)) {
  $q = $d->select("holiday_master", "holiday_id='$holiday_id'");
  $data = mysqli_fetch_array($q);
  extract($data);
}
$currentYear = date('Y');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$from = $_GET['from'];
$to = $_GET['to'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-3 col-md-6 col-6">
        <h4 class="page-title"> Holiday</h4>
      </div>
      <div class="col-sm-3 col-md-6 col-6 ">
        
        <?php  if (isset($_GET['previous']) && $_GET['previous'] == "year") {?>
        <a href="holiday"><button class="btn btn-primary btn-sm float-right mr-1 ">Add Holiday</button></a>
        <?php }else{ ?>
          <a href="holiday?previous=year"><button class="btn  btn-sm btn-primary float-right">Import From Previous Year</button></a>
        <?php } ?>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php if (isset($_GET['previous']) && $_GET['previous'] == "year") { ?>
              <form id="holidayAdd" action="" enctype="multipart/form-data" method="get">
                <input type="hidden" name="previous" value="year">
                <div class="form-group row">
                  <label for="input-10" class="col-lg-2 col-md-2 col-form-label">From Year <span class="required">*</span></label>
                  <div class="col-lg-4 col-md-4" id="">
                    <select name="from" class="form-control single-select" required="" onchange="changeYear(this.value)">
                      <option value="">Select From Year</option>
                      <option <?php if ($_GET['from'] == $twoPreviousYear) {
                                echo 'selected';
                              } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                      <option <?php if ($_GET['from'] == $onePreviousYear) {
                                echo 'selected';
                              } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                      <option <?php if ($_GET['from'] == $currentYear) {
                                echo 'selected';
                              } ?>  value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                      <option <?php if ($_GET['from'] == $nextYear) {
                                echo 'selected';
                              } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
                    </select>
                  </div>
                  <label for="input-10" class="col-lg-2 col-md-2 col-form-label">To Year</label>
                  <div class="col-lg-4 col-md-4" id="">
                    <select name="to" class="form-control single-select" id="toYear" required="">
                    <option value="">Select Year</option>

                      <option <?php if ($to == $twoPreviousYear) {
                                echo 'selected';
                              } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                      <option <?php if ($to == $onePreviousYear) {
                                echo 'selected';
                              } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                      <option <?php if ($to == $currentYear) {
                                echo 'selected';
                              } ?> <?php if (isset($to) && $to == '') {
                                      echo 'selected';
                                    } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                      <option <?php if ($to == $nextYear) {
                                echo 'selected';
                              } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
                    </select>
                  </div>
                </div>
                <div class="form-footer text-center">
                  <button type="submit" class="btn btn-success"> GET </button>
                </div>
              </form>
            <?php } else {
            ?>
              <form id="holidayAdd" action="controller/HolidayController.php" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                  <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Holiday Name <span class="required">*</span></label>
                  <div class="col-lg-4 col-md-4" id="">
                    <input type="text" class="form-control" placeholder="Holiday Name" name="holiday_name" value="<?php if ($data['holiday_name'] != "") {echo $data['holiday_name']; } ?>" required>
                  </div>
                  <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Holiday Description</label>
                  <div class="col-lg-4 col-md-4" id="">
                    <textarea class="form-control" name="holiday_description" placeholder="Holiday Description" value="<?php if ($data['holiday_description'] != "") { echo $data['holiday_description'];} ?>"><?php if ($data['holiday_description'] != "") { echo $data['holiday_description'];               } ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-10" class="col-lg-2  col-md-2 col-form-label">Holiday Image</label>
                  <div class="col-lg-4  col-md-4">
                    <input type="file" class="form-control photoOnly"  accept="image/*" name="holiday_image" id="holiday_image" />
                    <input type="hidden" class="form-control" name="holiday_image_old" id="holiday_image_old" value="<?php if ($data['holiday_image'] != "") { echo $data['holiday_image'];} ?>" />

                  </div>
                  <label for="input-10" class="col-lg-2  col-md-2 col-form-label">Department</label>
                  <div class="col-lg-4  col-md-4">
                    <select <?php if (isset($data['floor_id'])&& $data['floor_id'] !=""){
                                  echo 'disabled';
                                } ?> name="floor_id[]" multiple id="flr_id" class="form-control single-select muliDptHoliday">
                      <option value="0">All Department</option>
                      <?php
                      $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id $blockAppendQuery");
                      while ($depaData = mysqli_fetch_array($qd)) {
                      ?>
                        <option <?php if ($data['floor_id'] == $depaData['floor_id']) {
                                  echo 'selected';
                                } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name']; ?> (<?php echo $depaData['block_name']; ?>)</option>
                      <?php } ?>

                    </select>

                  </div>

                </div>
                <div class="form-group row">
                  <label for="input-10" class="col-lg-2  col-md-2 col-form-label">Holiday Start Date <span class="required">*</span></label>
                  <div class="col-lg-4  col-md-4">
                    <input type="text" id="default-datepicker-holiday_start_date" onchange="changeHolidayStartEnd(this.value)" placeholder="Holiday Date" readonly="" maxlength="250" required="" value="<?php if ($data['holiday_start_date'] != "") {echo $data['holiday_start_date'];} ?>" name="holiday_start_date" class="form-control startDateHoliday">
                    
                  </div>
                  <?php if ($data['holiday_end_date'] == "") { ?>
                    <label for="input-10" class="col-lg-2  col-md-2 col-form-label holidayEnd">Holiday End Date <span class="required">*</span></label>
                    <div class="col-lg-4  col-md-4">
                      <input type="text" <?php if ($data['holiday_end_date'] != "") { echo "disabled"; } ?> id="default-datepicker-holiday_end_date" placeholder="Holiday Date" readonly="" required="" value="<?php if ($data['holiday_end_date'] != "") { echo $data['holiday_end_date'];} ?>" name="holiday_end_date" class="form-control ">
                    </div>
                  <?php } ?>
                </div>

                <?php $q3 = $d->select("holiday_master", "society_id='$society_id'  ");
                $idArray = array();
                $counter = 1;
                $finalIds = "";
                while ($data3 = mysqli_fetch_array($q3)) {
                  array_push($idArray, $data3['holiday_id']);
                }
                $finalIds = implode(',', $idArray)

                ?>
                <input type="hidden" name="old_ids" value="<?php echo $finalIds; ?>">
                <div class="form-footer text-center">
                  <?php //IS_1019 addPenaltiesBtn
                  if (isset($edit_holiday)) {
                  ?>
                    <input type="hidden" name="holiday_id" value="<?php if ($data['holiday_id'] != "") {
                                                                    echo $data['holiday_id'];
                                                                  } ?>">
                    <input type="hidden" name="holiday_old_name" value="<?php if ($data['holiday_name'] != "") {
                                                                          echo $data['holiday_name'];
                                                                        } ?>">
                    <input type="hidden" name="holiday_old_days" value="<?php if ($data['holiday_days'] != "") {
                                                                          echo $data['holiday_days'];
                                                                        } ?>">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addHoliday" value="addHoliday">
                    <?php 
                    ?>
                  <?php } else {
                  ?>
                    <button id="addPenaltiesBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addHoliday" value="addHoliday">
                    <?php
                    ?>
                    <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('holidayAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                  <?php } ?>
                </div>
              </form>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>

<?php  if (isset($_GET['from']) && $_GET['from'] != "") { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="holidayBulkAdd" action="controller/BulkHolidayController.php" enctype="multipart/form-data" method="post">
              <?php
              if (isset($_GET['from']) && $_GET['from'] != "") {
                $q = $d->select("holiday_master", " holiday_master.society_id='$society_id' AND DATE_FORMAT(holiday_master.holiday_start_date, '%Y')='$_GET[from]' ", "ORDER BY holiday_master.holiday_id DESC");
                $counter = 1;
                $count = mysqli_num_rows($q);
                if($count>0){

                  while ($data = mysqli_fetch_array($q)) { ?>
                    <div class="form-group row">
                      <label for="input-10" class="col-lg-2  col-md-2 col-form-label">Holiday Name <span class="required">*</span></label>
                      <div class="col-lg-4  col-md-4">
                        <input type="text" class="form-control" placeholder="Holiday Name" name="holiday_name[]" value="<?php if ($data['holiday_name'] != "") { echo $data['holiday_name'];} ?>" required>
                    </div>
                    <label for="input-10" class="col-lg-2  col-md-2 col-form-label">Holiday Start Date <span class="required">*</span></label>
                      <div class="col-lg-4  col-md-4">
                        <input type="text" id="" placeholder="Holiday Date " readonly="" maxlength="250" required="" value="<?php if ($data['holiday_start_date'] != "") {echo date('Y-m-d', strtotime('+ 1 year', strtotime($data['holiday_start_date'])));} ?>" name="holiday_start_date[]" class="form-control default-datepicker-holiday_start_date">
                      </div>

                    </div>
                <?php }
                }
                else
                {
                  echo "Note : No Privious Year Holidays !!!!!! ";
                }
              }
              if($count>0){
              ?>
              <div class="form-footer text-center">
                <input type="hidden" name="holidayBulkAdd" value="holidayBulkAdd">
                <button type="submit" class="btn btn-success"> Add </button>
              </div>
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <!--End Row-->

  </div>
  <!-- End container-fluid-->

</div>
<!--End content-wrapper--------->

<script type="text/javascript">
  function changeYear(year)
  {
    const currentYear = new Date().getFullYear(); // 2020
    const previousYear =  parseInt(year)+1;
    const nextYear =  previousYear+1;
    showHtml =` <option value="">Select Year</option>
                <option value="`+previousYear+`">`+previousYear+`</option>
                <option value="`+nextYear+`">`+nextYear+`</option>`;
     
      $('#toYear').html(showHtml);
  }
  function removePhoto2() {
    $('#facility_photo_2').remove();
    $('#penalty_photo_old_2').val('');
  }

  function removePhoto3() {
    $('#facility_photo_3').remove();
    $('#penalty_photo_old_3').val('');
  }
</script>