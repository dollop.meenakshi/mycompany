<div class="content-wrapper">

  <div class="container-fluid">
      <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title"><?php echo $xml->string->block; ?></h4>
       
      </div>
    </div>
    <!-- End Breadcrumb-->
     
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form action="controller/blockController.php" method="post" id="blockAdd">
              <input type="hidden" name="society_id" id="society_id" value="<?php echo $society_id;?>">
              <h4 class="form-header text-uppercase">
                <i class="fa fa-address-book-o"></i>
                <?php echo $xml->string->block; ?>
              </h4>
                <?php $totalBlock= $d->count_data_direct("block_id","block_master","society_id='$society_id'");
                if ($totalBlock==0) {
               ?>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->number_of; ?> <?php echo $xml->string->blocks; ?> </label>
                <div class="col-sm-4">
                  <input type="text" required=""  autocomplete="off"  id="no_of_blocks" maxlength="2" class="form-control" id="input-10" name="no_of_blocks" onkeyup="getBlockList();" >
                </div>
                <input type="hidden" name="" value="0" id="block_type" name="block_type" >
               <!--  <label for="input-12" class="col-sm-2 col-form-label"><?php echo $xml->string->block; ?> <?php echo $xml->string->block_type; ?></label>
                <div class="col-sm-4">
                 <select onchange="getBlockList();" id="block_type" name="block_type" class="form-control">
                   <option>--<?php echo $xml->string->select_type; ?>--</option>
                   <option value="A">A,B,C,D</option>
                   <option value="1">1,2,3,4</option>
                   <option value="O">Other</option>
                 </select>
                </div> -->
               
              </div>
            <?php } else { ?>
            <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->block; ?> <?php echo $xml->string->name; ?> <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                  <input maxlength="30" required="" type="text" maxlength="30" class="form-control text-capitalize txtNumeric" id="input-10" name="blockNameSingle" autocomplete="off">
                </div>
             
               
              </div> 
            <?php } ?>
              <div class="form-group row" id="BlockResp">

              </div> 
              <div class="form-footer text-center">
                  <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->save; ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!--End Row-->
  </div>
</div>