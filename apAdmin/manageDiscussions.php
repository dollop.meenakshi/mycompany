<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-6">
        <h4 class="page-title"> Discussion Forum</h4>

      </div>
      <div class="col-sm-3 col-6">
        <div class="btn-group float-sm-right">
          <a href="discussionForum" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          <a href="javascript:void(0)" onclick="DeleteAll('deleteDiscussion');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>


        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>Title</th>
                    <th>Created Date</th>
                    <th>View</th>
                    <th>For</th>
                    <th>Created By</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $q=$d->select("discussion_forum_master","society_id='$society_id'","ORDER BY discussion_forum_id DESC");
                    $i = 0;
                    while($row=mysqli_fetch_array($q)) {
                      $i++;
                      
                  ?>
                    <tr>
                      <td class='text-center'>
                        <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['discussion_forum_id']; ?>">
                      </td>
                      <td><?php echo $i; ?></td>
                      <td ><?php echo custom_echo($row['discussion_forum_title'],60); ?></td>
                      <td><?php echo date('d M Y, h:i A',strtotime($row['created_date'])); ?></td>
                      <td>
                              <form   class="mr-2" action="discussionForum" method="post">
                            <a href="discussionHistory?id=<?php echo $row['discussion_forum_id'] ?>" class="btn btn-primary btn-sm" name="pullingReport">View</a>
                            <input type="hidden" name="discussion_forum_id" value="<?php echo $row['discussion_forum_id']; ?>">
                            <input type="hidden" name="editDiscussion" value="editDiscussion">
                            <button type="submit" class="btn btn-sm btn-warning"> Edit</button>
                          </form>
                      </td>
                       
                      <td>
                        <?php
                        if ($row['discussion_forum_for']==0) {
                        echo  $all= $xml->string->all.' '. $xml->string->floors; 

                        } else {
                          $qf=$d->selectRow("floors_master.floor_name,block_master.block_name","floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.floor_id='$row[discussion_forum_for]'");
                          $floorData=mysqli_fetch_array($qf);
                          echo $floorData['floor_name'] .' ('.$floorData['block_name'].')';
                        }

                        
                        ?>
                      </td>
                       <td>
                        <?php
                        if ($row['admin_id']!=0) {
                           $qad=$d->select("bms_admin_master","admin_id='$row[admin_id]'");
                           $adminData=mysqli_fetch_array($qad);
                          echo "Admin (".$adminData['admin_name'].")";
                        }else if ($row['user_id']!=0) { 
                           $q111=$d->select("block_master,unit_master,users_master","users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND users_master.user_id='$row[user_id]'","");
                          $userdata=mysqli_fetch_array($q111);
                          echo $userdata['user_full_name'] ." (".$userdata['user_designation'].')';
                        } 
                        ?>
                      </td>
                      <td>
                         <?php if($row['active_status']=="0"){
                        ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['discussion_forum_id']; ?>','discussionDeactive');" data-size="small"/>
                          <?php } else { ?>
                         <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $row['discussion_forum_id']; ?>','discussionActive');" data-size="small"/>
                        <?php } ?>
                      </td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>