<?php
error_reporting(0);
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$uId = (int)$_REQUEST['uId'];
$ueMonth = $_REQUEST['ueMonth'];
$ueYear = $_REQUEST['ueYear'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-md-9">
                <h4 class="page-title">Pending Expenses</h4>
            </div>
            <?php
            if((isset($dId) && $dId > 0) || (isset($uId) && $uId > 0) || (isset($ueYear) && $ueYear > 0) || (isset($ueMonth) && $ueMonth > 0))
            {
            ?>
            <div class="col-md-3">
                <div class="btn-group float-sm-right">
                    <a href="javascript:void(0)" onclick="approveRejectExpense('approve');" class="btn  btn-sm btn-primary pull-right"><i class="fa fa-check"></i> Approve </a>
                    <a href="javascript:void(0)" onclick="approveRejectExpense('reject');" class="btn  btn-sm btn-danger pull-right ml-2"><i class="fa fa-times"></i> Reject </a>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
        <form class="branchDeptFilter" action="" >
            <div class="row pt-2 pb-2">
                <?php include 'selectBranchDeptEmpForFilterAll.php' ?>
                <div class="col-md-2 form-group">
                    <select name="ueMonth" class="form-control single-select">
                        <option <?php if($ueMonth=="0") { echo 'selected';} ?> value="0">All Month</option>
                        <option <?php if($ueMonth=="01") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '01') { echo 'selected';}?> value="01">January</option>
                        <option <?php if($ueMonth=="02") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '02') { echo 'selected';}?> value="02">February</option>
                        <option <?php if($ueMonth=="03") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '03') { echo 'selected';}?> value="03">March</option>
                        <option <?php if($ueMonth=="04") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '04') { echo 'selected';}?> value="04">April</option>
                        <option <?php if($ueMonth=="05") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '05') { echo 'selected';}?> value="05">May</option>
                        <option <?php if($ueMonth=="06") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '06') { echo 'selected';}?> value="06">June</option>
                        <option <?php if($ueMonth=="07") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '07') { echo 'selected';}?> value="07">July</option>
                        <option <?php if($ueMonth=="08") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '08') { echo 'selected';}?> value="08">August</option>
                        <option <?php if($ueMonth=="09") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '09') { echo 'selected';}?> value="09">September</option>
                        <option <?php if($ueMonth=="10") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '10') { echo 'selected';}?> value="10">October</option>
                        <option <?php if($ueMonth=="11") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '11') { echo 'selected';}?> value="11">November</option>
                        <option <?php if($ueMonth=="12") { echo 'selected';} ?> <?php if($ueMonth == '' && $currentMonth == '12') { echo 'selected';}?> value="12">December</option>
                    </select>
                </div>
                <div class="col-md-2 form-group">
                    <select name="ueYear" class="form-control single-select">
                        <option <?php if($ueYear=="0") { echo 'selected';} ?> value="0">All Year</option>
                        <option <?php if($ueYear==$twoPreviousYear) { echo 'selected';} ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear ?></option>
                        <option <?php if($ueYear==$onePreviousYear) { echo 'selected';} ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear ?></option>
                        <option <?php if($ueYear==$currentYear) { echo 'selected';}?> <?php if($ueYear == '') { echo 'selected';}?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
                        <option <?php if($ueYear==$nextYear) { echo 'selected';} ?> value="<?php echo $nextYear ?>"><?php echo $nextYear ?></option>
                    </select>
                </div>
                <div class="col-md-2 form-group">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $i = 1;
                             if(isset($bId) && $bId>0) {
                                $BranchFilterQuery = " AND users_master.block_id='$bId'";
                              }

                            if(isset($dId) && $dId > 0)
                            {
                                $deptFilterQuery = " AND user_expenses.floor_id='$dId'";
                            }
                            if(isset($uId) && $uId > 0)
                            {
                                $userFilterQuery = " AND user_expenses.user_id='$uId'";
                            }
                            if(isset($ueYear) && $ueYear > 0)
                            {
                                $yearFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%Y') = '$ueYear'";
                            }
                            if(isset($ueMonth) && $ueMonth > 0)
                            {
                                $monthFilterQuery = " AND DATE_FORMAT(user_expenses.date,'%m') = '$ueMonth'";
                            }
                            $q = $d->select("user_expenses JOIN floors_master ON user_expenses.floor_id=floors_master.floor_id JOIN users_master ON users_master.user_id = user_expenses.user_id LEFT JOIN expense_category_master ON expense_category_master.expense_category_id = user_expenses.expense_category_id","user_expenses.society_id='$society_id' AND users_master.delete_status = 0 AND user_expenses.expense_paid_status = 0 AND expense_status = 0 AND user_expenses.expense_type = 0 $deptFilterQuery $yearFilterQuery $monthFilterQuery $BranchFilterQuery $blockAppendQueryUser $userFilterQuery", "ORDER BY user_expenses.date DESC");
                            $counter = 1;
                            
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <th>Sr.No</th>
                                        <th>Action</th>
                                        <th>Employee</th>
                                        <th>Department</th>
                                        <th>Title</th>
                                        <th>Amount</th>
                                        <th>Category</th>
                                        <th>Type</th>
                                        <th>Unit</th>
                                        <th>Unit Name</th>
                                        <th>Unit Price</th>
                                        <th>Date</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php while ($data = mysqli_fetch_array($q)) { ?>
                                    <tr>
                                        <td class="text-center">
                                            <input type="hidden" name="id" id="id" value="<?php echo $data['leave_type_id']; ?>">
                                            <input type="checkbox" name="" class="multipleCheckbox" value="<?php echo $data['user_expense_id']; ?>" user_expense_id="<?php echo $data['user_expense_id']; ?>">
                                        </td>
                                        <td><?php echo $counter++; ?></td>
                                        <td>
                                             <form action="addExpenses" method="post">
                                            <?php if($data['expense_status'] == 0)
                                            {
                                            ?>
                                            <button title="Approve Expense" type="button" class="btn btn-sm btn-primary ml-1" onclick ="changeStatus('<?php echo $data['user_expense_id']; ?>','approvedEmployeeExpenses');"><i class="fa fa-check"></i></button>
                                            <button title="Decline Expense" type="button" class="btn btn-sm btn-danger ml-1" onclick="employeeExpensesStatusChange(<?php echo $data['user_expense_id']; ?>)"><i class="fa fa-times"></i></button>
                                            <?php }elseif($data['expense_status'] == 1){ ?>
                                            <b><span class="badge badge-pill m-1 badge-success">Approved</span></b>
                                            <?php }else{ ?>
                                            <b><span class="badge badge-pill m-1 badge-danger">Declined</span></b>
                                            <?php
                                            }
                                            ?>
                                            <input type="hidden" name="user_expense_id" value="<?php echo $data['user_expense_id']; ?>">
                                            <button type="button" class="btn btn-sm btn-primary mr-1 pd-1" onclick="employeeExpensesDetail(<?php echo $data['user_expense_id']; ?>)" >
                                            <i class="fa fa-eye"></i>
                                            </button>
                                        
                                           
                                                <input type="hidden" name="user_expense_id" value="<?php echo $data['user_expense_id']; ?>"/>
                                                <input type="hidden" name="bId" value="<?php echo $_GET['bId']; ?>"/>
                                                <input type="hidden" name="dId" value="<?php echo $_GET['dId']; ?>"/>
                                                <input type="hidden" name="uId" value="<?php echo $_GET['uId']; ?>"/>
                                                <input type="hidden" name="ueMonth" value="<?php echo $_GET['ueMonth']; ?>"/>
                                                <input type="hidden" name="ueYear" value="<?php echo $_GET['ueYear']; ?>"/>
                                                <input type="hidden" name="getReport" value="<?php echo $_GET['getReport']; ?>"/>
                                                <input type="hidden" name="f" value="<?php echo $_GET['f']; ?>"/>
                                                <input type="hidden" name="csrf" value="<?php echo $_GET['csrf']; ?>"/>
                                                <input type="hidden" name="edit_expense" value="edit_expense"/>
                                                <button type="submit" title="Edit Expense" class="btn btn-sm btn-warning mr-1 pd-1"><i class="fa fa-pencil"></i></button>
                                            </form>
                                        </td>
                                        <td><?php echo $data['user_full_name']; ?><br>
                                            <span>(<?php echo $data['user_designation']; ?>)</span>
                                        </td>
                                        <td><?php echo $data['floor_name']; ?> </td>
                                        <td><?php echo $data['expense_title']; ?></td>
                                        <td><?php echo $data['amount']; ?></td>
                                        <td><?php echo $data['expense_category_name']; ?></td>
                                        <td><?php echo ($data['expense_category_type'] == 1) ? 'Unit Wise' : 'Amount Wise'; ?></td>
                                        <td><?php echo ($data['unit'] != "0.00") ? $data['unit'] : ""; ?></td>
                                        <td><?php echo $data['expense_category_unit_name']; ?></td>
                                        <td><?php echo ($data['expense_category_unit_price'] != "0.00") ? $data['expense_category_unit_price'] : ""; ?></td>
                                        <td><?php echo date("d M Y", strtotime($data['date'])); ?></td>
                                        
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<div class="modal fade" id="employeeExpensesModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Employee Expenses</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="employeeExpensesData" style="align-content: center;">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expenesDeclineReason">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Declined Reason</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <div class="card-body" >
                    <div class="row " >
                        <div class="col-md-12">
                            <form id="expenesStatusReject" action="controller/statusController.php" name="changeAttendance"  enctype="multipart/form-data" method="post">
                                <div class="form-group row">
                                    <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                                    <div class="col-lg-12 col-md-12" id="">
                                        <textarea required maxlength="250" class="form-control" name="expense_reject_reason"></textarea>
                                    </div>
                                </div>
                                <div class="form-footer text-center">
                                    <input type="hidden" name="status"  value="declinedEmployeeExpenses">
                                    <input type="hidden" name="user_expense_id"  class="user_expense_id">
                                    <input type="hidden" name="bId" value="<?php echo $bId; ?>">
                                    <input type="hidden" name="dId" value="<?php echo $dId; ?>">
                                    <input type="hidden" name="ueMonth" value="<?php echo $ueMonth; ?>">
                                    <input type="hidden" name="ueYear" value="<?php echo $ueYear; ?>">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expenesDeclineReasonNew">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Declined Reason</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <div class="card-body" >
                    <div class="row " >
                        <div class="col-md-12">
                            <form id="expenesStatusRejectNew" action="controller/ExpenseSettingController.php" name="changeAttendance" enctype="multipart/form-data" method="post">
                                <div class="form-group row">
                                    <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                                    <div class="col-lg-12 col-md-12" id="">
                                        <textarea required maxlength="250" class="form-control" name="expense_reject_reason_new"></textarea>
                                    </div>
                                </div>
                                <div class="form-footer text-center">
                                    <input type="hidden" name="rejectExpenses" value="rejectExpenses">
                                    <input type="hidden" name="bId" value="<?php echo $bId; ?>">
                                    <input type="hidden" name="dId" value="<?php echo $dId; ?>">
                                    <input type="hidden" name="ueMonth" value="<?php echo $ueMonth; ?>">
                                    <input type="hidden" name="ueYear" value="<?php echo $ueYear; ?>">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expensePayModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Expense Pay Detail</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="billPayDiv" style="align-content: center;">
                <div class="card-body">
                    <form id="payExpenseAmountForm" action="controller/ExpenseSettingController.php" enctype="multipart/form-data" method="post">
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Expense Payment Mode <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" class="form-control" placeholder="Expense Payment Mode" id="expense_payment_mode" name="expense_payment_mode" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Reference Number <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" class="form-control" placeholder="Reference Number" id="reference_no" name="reference_no" value="">
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" id="user_expense_id" name="user_expense_id" value="" >
                            <input type="hidden" name="payExpenseAmount"  value="payExpenseAmount">
                            <button id="payExpenseAmountBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                            <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('payExpenseAmountForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>