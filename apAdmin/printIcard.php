<?php 
error_reporting(0);
// session_cache_limiter(false);
session_start();
include_once 'lib/dao.php';
include_once 'lib/model.php';

$d = new dao();
$m = new model();
$con=$d->dbCon();
$bId = (int)$_GET['bId'];



extract(array_map("test_input" , $_GET));
if (isset($_COOKIE['bms_admin_id'])) {
    $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
	$qs=$d->select("society_master","society_id='$society_id'");
	$bData=mysqli_fetch_array($qs);
}

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

$base_url=$m->base_url();
   $society_id = (int)$society_id;


 $aq=$d->select("bms_admin_master","admin_id='$bms_admin_id' ");
$adminData= mysqli_fetch_array($aq);
$access_branchs=$adminData['access_branchs'];
$access_departments=$adminData['access_departments'];
$blockAryAccess = array();
$departmentAry = array();
$blockAryAccess = explode(",", $access_branchs);
$departmentAry = explode(",", $access_departments);

if ($access_branchs!='') {
    $blockids = join("','",$blockAryAccess); 
    if ($access_departments!='') {
      $departmentIds = join("','",$departmentAry); 
      $restictDepartment = " AND floors_master.floor_id IN ('$departmentIds')";
      $restictDepartmentUser = " AND users_master.floor_id IN ('$departmentIds')";
    } else {
      $restictDepartment = "";
      $restictDepartmentUser = "";
    }

    $blockAppendQuery = " AND block_master.block_id IN ('$blockids')";
    $blockAppendQueryUnit = " AND unit_master.block_id IN ('$blockids')";
    $blockAppendQueryFloor = " AND floors_master.block_id IN ('$blockids') $restictDepartment";
    $blockAppendQueryUser = " AND users_master.block_id IN ('$blockids') $restictDepartmentUser";
    $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

}  else {
    $blockAppendQuery = "";
    $blockAppendQueryUnit = "";
    $blockAppendQueryUser = "";
    $blockAppendQueryOnly="";
}

if($bId == 0){
	$firstBlockIdArray = array();
	$q =  $d->selectRow("block_master.*","block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_sort ASC");
	while ($data=mysqli_fetch_array($q)){	
		array_push($firstBlockIdArray,$data['block_id']);
	}

	$bId = $firstBlockIdArray[0];
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Icard Print</title>
	<?php include 'common/colours.php'; ?>
  <link href="assets/css/app-style9.css" rel="stylesheet"/>
	
	<style type="text/css">
		body {
			background-color:white;
			font-family: 'Open Sans', sans-serif;
		}
		.id-card-holder {
			width: 200px;
		    padding: 4px;
		    /*margin: 0 auto;*/
		    margin-left: 2px;
		    margin-right: : 2px;
		    margin-bottom : 10px;
		    /*background-color: #1f1f1f;*/
		    border-radius: 5px;
		    position: relative;
		    display: inline-block;
		}
		
		.id-card {
			
			background-image: url('../img/icardbg.jpg');
			padding: 0px;
			border-radius: 5px;
			text-align: center;
			box-shadow: 0 0 1.5px 0px #b9b9b9;
			background-repeat: no-repeat;
			background-size: cover;
		}
		.id-card img {
			margin: 0 auto;
		}
		.header {
			margin: auto;
			width: 60px;
		}
		.header img {
			width: 100%;
    		margin-top: 5px;
		}
		.photo {
			margin: auto;
			width: 87px;
			height: 87px;
			border: 1px solid var(--primary);
			border-radius: 50%;
			background: white;
			overflow: hidden;
		}
		.photo img {
			width: 100%;
			height: 100%;
    		
		}
		hr {
			margin: 2px;
		}
		h2 {
			font-size: 12px;
			margin: 2px 0;
			line-height: 1;
		}
		h3 {
			font-size: 10px;
			margin: 2.5px 0;
			font-weight: 300;
			line-height: 1;
		}
		.qr-code img {
			width: 85px;
		}
		p {
			font-size: 10px;
			margin: 2px;
		}
		.id-card-hook {
			background-color: #000;
		    width: 70px;
		    margin: 0 auto;
		    height: 15px;
		    border-radius: 5px 5px 0 0;
		}
		.id-card-hook:after {
			content: '';
		    background-color: #d7d6d3;
		    width: 47px;
		    height: 6px;
		    display: block;
		    margin: 0px auto;
		    position: relative;
		    top: 6px;
		    border-radius: 4px;
		}
		.id-card-tag-strip {
			width: 45px;
		    height: 40px;
		    background-color: #0950ef;
		    margin: 0 auto;
		    border-radius: 5px;
		    position: relative;
		    top: 9px;
		    z-index: 1;
		    border: 1px solid #0041ad;
		}
		.id-card-tag-strip:after {
			content: '';
		    display: block;
		    width: 100%;
		    height: 1px;
		    background-color: #c1c1c1;
		    position: relative;
		    top: 10px;
		}
		.id-card-tag {
			width: 0;
			height: 0;
			border-left: 100px solid transparent;
			border-right: 100px solid transparent;
			border-top: 100px solid #0958db;
			margin: -10px auto -30px auto;
		}
		.id-card-tag:after {
			content: '';
		    display: block;
		    width: 0;
		    height: 0;
		    border-left: 50px solid transparent;
		    border-right: 50px solid transparent;
		    border-top: 100px solid #d7d6d3;
		    margin: -10px auto -30px auto;
		    position: relative;
		    top: -130px;
		    left: -50px;
		}
		.addressSoc {
			font-size: 7px;
			padding-top: 2px;
			font-weight: bold;
			color: black;
		}
		.addressDiv {
			height: 35px;
		}
		.website {
			padding-bottom: 2px;
			font-size: 7px;
		}
		.qr-code {
			margin: auto;
			width: 87px;
			border: 1px solid var(--primary);
			border-radius: 5px;
		}
	</style>
</head>

  <body onload="window.print()">
		<?php
		if (isset($allDaily)) {  
       $query=$d->select("daily_visitors_master,society_master","daily_visitors_master.society_id='$society_id' and daily_visitors_master.active_status = 0  order by daily_visitors_master.visitor_id DESC ","");

		$i=0;
		while($data=mysqli_fetch_array($query)){ 
			extract($data);
			$fd=$d->selectRow("visitor_sub_image,visitor_sub_type_name","visitorSubType","visitor_sub_type_id='$data[visitor_sub_type_id]'");
                        $vistLogo=mysqli_fetch_array($fd);
                        if ($vistLogo['visitor_sub_image']!='') {
                            $visit_logo = $base_url.'img/visitor_company/'.$vistLogo['visitor_sub_image'].'';
                            $visit_company = $vistLogo['visitor_sub_type_name'].'';
                        } else {
                            $visit_logo="";
                        }
			
			?>
		<div class="id-card-holder">
			<div class="id-card" >
				<div class="header">
					<?php if (isset($bData['socieaty_logo'])){ ?>
					<img  onerror="this.src='img/logo.png'" src="../img/society/<?php echo $bData['socieaty_logo'];?>">
					<?php }else {?>
					<img src="img/logo.png">
					<?php } ?>
				</div>
				<div class="photo">
					
					<img onerror="this.src='img/user.png'"  src="../img/visitor/<?php echo $visitor_profile; ?>"  class="img-fluid rounded shadow mb-3" alt="card img">
				</div>
				<h2 style="text-transform: capitalize;"><?php echo ucfirst($data['visitor_name']); ?> </h2>
				<p style="font-size: 10px;"><?php
				 $user_mobile=$data['visitor_mobile'];
				 echo $visit_company.'-Daily Visitor';
				?></p>
				<div class="qr-code">

					<?php $qr_size          = "300x300";
						$qr_content       = "$user_mobile";
						$qr_correction    = strtoupper('H');
						$qr_encoding      = 'UTF-8';

	    				//form google chart api link
						$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
						?>
						<img src="<?php echo $qrImageUrl; ?>" alt="">
				</div>
				
				<h3><?php echo $data['society_name']; ?></h3>
				<hr>
			<div class="addressDiv">
				<p class="addressSoc"><?php echo $data['society_address']; ?></p>
				
			</div>
			</div>
		</div>
	    <?php }	} else if (isset($allUsers)) {   
	    if (isset($_GET['type'])  && $_GET['type']=='Owners') {
          $apendType=" AND users_master.user_type=0";
        } else if (isset($_GET['type'])  &&  $_GET['type']=='Tenants') {
          $apendType=" AND users_master.user_type=1";
        }	
        $q3=$d->select("unit_master,block_master,users_master,floors_master,society_master","users_master.delete_status=0 AND users_master.society_id=society_master.society_id AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.block_id='$bId' $apendType $blockAppendQueryUser ","ORDER BY block_master.block_sort");

		$i=0;
		while($data=mysqli_fetch_array($q3)){ ?>
		<div class="id-card-holder">
			<div class="id-card" >
				<div class="header">
					<?php if (isset($bData['socieaty_logo'])){ ?>
					<img onerror="this.src='../img/logo.png'" src="../img/society/<?php echo $bData['socieaty_logo'];?>">
					<?php }else {?>
					<img src="img/logo.png">
					<?php } ?>
				</div>
				<div class="photo">

					<img  height="80" width="" onerror="this.src='img/user.png'"  src="../img/users/recident_profile/<?php echo $data['user_profile_pic']; ?>"  class="img-fluid rounded shadow mb-3" alt="card img">
				</div>
				<h2 style="text-transform: capitalize;"><?php echo ucfirst($data['user_full_name']); ?> </h2>
				<p style="font-size: 10px;"><?php
				 $user_id=$data['user_id'];
				 $user_mobile=$data['user_mobile'];
				 echo $data['floor_name'].'-'.$data['block_name'];
				?></p>
				<div class="qr-code">

					<?php $qr_size          = "300x300";
						$qr_content       = "$user_mobile";
						$qr_correction    = strtoupper('H');
						$qr_encoding      = 'UTF-8';

	    				//form google chart api link
						$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
						?>
						<img src="<?php echo $qrImageUrl; ?>" alt="">
				</div>
				
				<h3><?php echo $data['society_name']; ?></h3>
				<hr>
				<div class="addressDiv">
				<p class="addressSoc"><?php echo $data['society_address']; ?></p>
				</div>

			</div>
		</div>
	    <?php }	} else  {
		 if (isset($emp_type_id)) {

			 	$empAry = array_map('intval', explode(',', $emp_id));
			 	 $ids = join("','",$empAry); 

			  if ($ids!='') {
			  	 $appQuery= " AND employee_master.emp_id IN ('$ids')";
			  }else {
			  	echo "Select Employee";
			  	exit();
			  } 

              if ($emp_type_id==1) {
                $emp_type_id=0;
                 
                $q1=$d->select("employee_master,society_master","society_master.society_id=employee_master.society_id AND employee_master.society_id='$society_id' AND employee_master.emp_type_id='$emp_type_id'  $appQuery","");
              }
              else if ($emp_type_id=='All') {
                $q1=$d->select("employee_master,society_master","society_master.society_id=employee_master.society_id AND employee_master.society_id='$society_id'  $appQuery","");
                
              } else if(is_int($emp_type_id)){

                $q1=$d->select("employee_master,society_master","society_master.society_id=employee_master.society_id AND employee_master.society_id='$society_id' AND employee_master.emp_type_id='$emp_type_id'  $appQuery","");
              } else {
              	$emp_type_id  = (int)$emp_type_id;
              	 $q1=$d->select("employee_master,society_master","society_master.society_id=employee_master.society_id AND employee_master.society_id='$society_id' AND employee_master.emp_type_id='$emp_type_id'  $appQuery","");
              	
              }
            } else {
              $q1=$d->select("employee_master,society_master","society_master.society_id=employee_master.society_id AND employee_master.society_id='$society_id'  $appQuery","");
            }
		 // $q1=$d->select("employee_master,society_master","society_master.society_id=employee_master.society_id AND employee_master.society_id='$society_id'","");
	$i=0;
	while($data=mysqli_fetch_array($q1)){
 	$EW=$d->select("emp_type_master","emp_type_id='$data[emp_type_id]'");
 	$empTypeData=mysqli_fetch_array($EW); ?>
	<div class="id-card-holder">
		<div class="id-card" >
			<div class="header">

				<?php if (isset($bData['socieaty_logo'])){ ?>
				<img onerror="this.src='img/logo.png'" src="../img/society/<?php echo $bData['socieaty_logo'];?>">
				<?php }else {?>
				<img src="img/logo.png">
				<?php } ?>
			</div>
			<div class="photo">
				
				<img  height="80" onerror="this.src='img/user.png'"  src="../img/emp/<?php echo $data['emp_profile']; ?>"  class="img-fluid rounded shadow mb-3" alt="card img">
			</div>
			<h2><?php echo ucfirst($data['emp_name']); ?> </h2>
			<p><?php
			 $emp_id=$data['emp_id'];
			 $emp_mobile=$data['emp_mobile'];
			  if ($empTypeData['emp_type_name']!='') {
                      echo $empTypeData['emp_type_name'];
                    } else {
                      echo "Security Guard";
                    }?></p>
			<div class="qr-code">

				<?php $qr_size          = "300x300";
					$qr_content       = "$emp_mobile";
					$qr_correction    = strtoupper('H');
					$qr_encoding      = 'UTF-8';

    				//form google chart api link
					$qrImageUrl = "https://chart.googleapis.com/chart?cht=qr&chs=$qr_size&chl=$qr_content&choe=$qr_encoding&chld=$qr_correction";
					?>
					<img src="<?php echo $qrImageUrl; ?>" alt="">
			</div>
			
			<h3><?php echo $data['society_name']; ?></h3>
			<hr>
			<div class="addressDiv">
				<p class="addressSoc"><?php echo $data['society_address']; ?></p>
			</div>

		</div>
	</div>
		<?php } } ?>
</body>
<script type="text/javascript">
        Android.print('print');
  </script>
</html>