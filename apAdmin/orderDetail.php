<?php error_reporting(0);
if($_GET){
    $q=$d->select("order_master,users_master","order_master.user_id=users_master.user_id AND order_master.order_id='$_GET[id]' ");
    $data = mysqli_fetch_array($q);
	extract($data);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
          <h4 class="page-title">Order Detail</h4>
        </div>
        <div class="col-sm-3 col-6 text-right">
          <a href="orderInvoice.php?id=<?php echo $data['order_id']; ?>" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Print</a>
        <!-- <a href="addSiteBtn" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteSite');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
        </div>
      </div>
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-center">Order Detail</h4>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span>User Name: <?php echo $data['user_full_name']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span>Order No: <?php echo $data['order_no']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span>Order Total Amount: <?php echo $data['order_total_amount']; ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                        <span>Order Date: <?php echo date("d M Y h:i A", strtotime($data['order_date'])); ?></span>
                    </div>
                    <div class="col-md-4 pt-4">
                    <span>Status</span>
                        <select style="min-width: 108px;" <?php if(($data['order_status']==3) || ($data['order_status']==4)){ echo "disabled";} ?> class="form-control valid" onchange="orderStatusChange(this.value, '<?php echo $data['order_id']; ?>')">
                            <?php if($data['order_status'] == 0){ ?>
                              <option value="0" <?php if ($data['order_status'] == 0) {echo "selected";}?>>Placed</option>
                              <option value="1" <?php if ($data['order_status'] == 1) {echo "selected";}?>>Preparing</option>
                              <option value="4" <?php if ($data['order_status'] == 4) {echo "selected";}?>>Cancelled</option>
                            <?php } else if($data['order_status'] == 1){ ?>
                              <option value="1" <?php if ($data['order_status'] == 1) {echo "selected";}?>>Preparing</option>
                              <option value="2" <?php if ($data['order_status'] == 2) {echo "selected";}?>>Despatched</option>
                              <option value="3" <?php if ($data['order_status'] == 3) {echo "selected";}?>>Delivered</option>
                            <?php }  else if($data['order_status'] == 2){ ?>
                              <option value="2" <?php if ($data['order_status'] == 2) {echo "selected";}?>>Despatched</option>
                              <option value="3" <?php if ($data['order_status'] == 3) {echo "selected";}?>>Delivered</option>
                            <?php }  else if($data['order_status'] == 3){ ?>
                              <option value="3" <?php if ($data['order_status'] == 3) {echo "selected";}?>>Delivered</option>
                            <?php }  else{ ?>
                              <option value="4" <?php if ($data['order_status'] == 4) {echo "selected";}?>>Cancelled</option>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                  <div class="table-responsive pt-4">
                  
                    <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Sr. No.</th>
                              <th>Vendor</th>
                              <th>Vendor Product</th>
                              <th>Price</th>
                              <th>Variant Name</th>
                              <th>Quantity</th>
                              <th>Product Category</th>
                          </tr>
                      </thead>
                      <tbody id="showFilterData" class="purchase_requirements">
                          
                          <?php 
                            $q=$d->select("order_product_master,vendor_product_category_master,vendor_product_master,vendor_master","order_product_master.vendor_id=vendor_master.vendor_id AND order_product_master.vendor_product_id=vendor_product_master.vendor_product_id AND order_product_master.vendor_product_category_id=vendor_product_category_master.vendor_product_category_id AND order_product_master.order_id='$data[order_id]'");
                            $counter = 1;
                            while ($purchaseItem=mysqli_fetch_array($q)) {
                          ?>
                            <tr>
                                <td><?php echo $counter++; ?></td>
                                <td><?php echo $purchaseItem['vendor_name']; ?></td>
                                <td><?php echo $purchaseItem['vendor_product_name']; ?></td>
                                <td><?php echo $purchaseItem['vendor_product_variant_price']; ?></td>
                                <td><?php echo $purchaseItem['vendor_product_variant_name']; ?></td>
                                <td><?php echo $purchaseItem['vendor_product_quantity']; ?></td>
                                <td><?php echo $purchaseItem['vendor_product_category_name']; ?></td>
                                
                            </tr>
                          <?php } ?>     
                      </tbody>
                      
                    </table>
                  </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    
</div><!--End content-wrapper-->

<script type="text/javascript">
function popitup(url) {
  newwindow=window.open(url,'name','height=800,width=900, location=0');
  if (window.focus) {newwindow.focus()}
    return false;
}
</script>
<style>
.hideupdate{
  display:none;
}

</style>