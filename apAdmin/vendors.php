
<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-4">
        <h4 class="page-title">Vendors </h4>
        
      </div>
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
        <div class="btn-group float-sm-right">
          <a href="addVendor"  class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New </a>
          <a href="#" data-toggle="modal" data-target="#bulkUpload" class="btn btn-info btn-sm waves-effect waves-light"><i class="fa fa-file-excel-o"></i> Bulk Import </a>
          <a href="#" onclick="DeleteAll('deleteServiceProvider');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
      </div>
    </div>
    
    <!-- End Breadcrumb-->
  </div>
 
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <!-- <table id="checkAllwithAllDelete" class="table table-bordered"> -->
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th class="deleteTh">
                    <input type="checkbox" name="" class="selectAll" value="">
                  </th>
                  <th>#</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Phone</th>
                  <!-- <th>Email</th> -->
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $i=1;
                 
                  $q=$d->select("local_service_provider_users","service_provider_delete_status = 0"," ORDER BY service_provider_users_id DESC");
                  while ($data=mysqli_fetch_array($q)) {
                    $spData[] = $data;
                  }
                  for ($j=0; $j < count($spData); $j++) { 
                    $id = $spData[$j]['service_provider_users_id'];
                ?>
                <tr>
                  <td class='text-center'>
                    
                      <input type="checkbox" class="multiDelteCheckbox" value="<?php echo $spData[$j]['service_provider_users_id']; ?>" >
                  </td>
                  <td><?php echo $i++; ?></td>
                  
                  <td class="tableWidth"><?php echo $spData[$j]['service_provider_name'];
                  if ($spData[$j]['kyc_status']==1) {
                    echo " <img src='../img/check.png' width='15'>";
                  }
                  ?></td>
                  <td class="tableWidth">
                     <?php 
                     $sp_id = $spData[$j]['service_provider_users_id'];?>
                     <?php $i1=1;
                     $qcd=$d->select("local_service_provider_users_category,local_service_provider_master","local_service_provider_master.local_service_provider_id=local_service_provider_users_category.category_id AND  local_service_provider_users_category.service_provider_users_id='$sp_id'");
                        while ($catData=mysqli_fetch_array($qcd)) {
                          echo $i1++;
                        echo  '. '.$catData['service_provider_category_name'];
                        echo "<br>";
                        }
                    
                    ?>
                   </td>
                  <td><?php echo $spData[$j]['service_provider_phone']; ?></td>
                  <!-- <td class="tableWidth"><?php echo $spData[$j]['service_provider_email']; ?></td> -->
                  <td>
                    <form class="d-inline-block" action="addVendor" method="post">
                      <input type="hidden" name="service_provider_users_id" value="<?php echo $spData[$j]['service_provider_users_id']; ?>">
                      <button name="editSp" value="editSp" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></button>
                    </form>

                    <a class="d-inline-block btn btn-sm btn-warning" href="serviceProviderDetails?sp_id=<?php echo $spData[$j]['service_provider_users_id']; ?>"><i class="fa fa-eye"></i></a>
                    <br>
                    <br>
                    <?php if($spData[$j]['service_provider_status']=="0"){ ?>
                      <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $spData[$j]['service_provider_users_id']; ?>','SpDeactive');" data-size="small"/>
                    <?php } else { ?>
                      <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $spData[$j]['service_provider_users_id']; ?>','SpActive');" data-size="small"/>
                    <?php } ?>
                  </td>

                </tr>

                <?php } ?> 
              </tbody>

            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>



<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
 
  $(".sp_delete").click(function(){ 
    var elmId = $(this). attr("id");
    if(this.checked) {
      $('#'+elmId+'_hidden').prop('checked', true);
    } else {
      $('#'+elmId+'_hidden').prop('checked', false);
    }
  });
</script>


<div class="modal fade" id="addCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Service Provider Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="controller/serviceProviderController.php" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Category Name *</label>
            <div class="col-sm-8">
              <input required="" type="text" name="service_provider_category_name"  class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Image *</label>
            <div class="col-sm-8">
              <input required="" type="file" name="service_provider_category_image"  class="form-control-file border">
            </div>
          </div>

          <div class="form-footer text-center">
            <button type="submit" name="addCategory" value="importParking" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Add</button>
          </div>

        </form> 
      </div>

    </div>
  </div>
</div><!--End Modal -->


<div class="modal fade" id="editCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Service Provider Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="controller/serviceProviderController.php" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Category Name *</label>
            <div class="col-sm-8">
              <input type="hidden" name="local_service_provider_id" id="local_service_provider_id">
              <input required="" id="service_provider_category_name" type="text" name="service_provider_category_name"  class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Image </label>
            <div class="col-sm-8">
              <input type="hidden" name="service_provider_category_image_old" id="service_provider_category_image">
              <input  type="file" name="service_provider_category_image"  class="form-control-file border">
            </div>
          </div>

          <div class="form-footer text-center">
            <button type="submit" name="editCategory" value="editCategory" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Update</button>
          </div>

        </form> 
      </div>

    </div>
  </div>
</div><!--End Modal -->

<div class="modal fade" id="bulkUpload">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Import Bulk Service Providers</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="input-10" class="col-sm-12 col-form-label">Step 1 -> Download Formated CSV  <a href="controller/bulkUploadController.php?ExporLocalserviceProvider=ExporLocalserviceProvider&&csrf=<?php echo $_SESSION["token"]; ?>" name="ExporLocalserviceProvider" value="ExporLocalserviceProvider" class="btn btn-sm btn-primary"><i class="fa fa-check-square-o"></i> Download</a></label>
            <label for="input-10" class="col-sm-12 col-form-label">Step 2 -> Fill Your Data</label>
            <label for="input-10" class="col-sm-12 col-form-label">Step 3 -> Import This File Here</label>
            <label for="input-10" class="col-sm-12 col-form-label">Step 4 -> Click on Upload Button</label>
          </div>
        </form> 
        <form action="controller/bulkUploadController.php" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label">Import CSV File <span class="text-danger">*</span></label>
            <div class="col-sm-8" id="PaybleAmount">
              <input required="" type="file" name="file"  class="form-control-file border csvupload" accept=".csv" />
            </div>
          </div>
          <input type="hidden" name="importServiceProvider" value="importServiceProvider">
          <div class="form-footer text-center">
            <button type="submit" name="importCsvData" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Upload</button>
          </div>

        </form> 
      </div>

    </div>
  </div>
</div>