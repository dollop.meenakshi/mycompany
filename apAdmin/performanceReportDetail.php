<?php error_reporting(0);
if(!isset($_GET['id'])){
  $_SESSION['msg1'] = "Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='performanceReport';
        </script>");
}
$pms_schedule_id = (int)$_REQUEST['id'];

if(isset($pms_schedule_id) && $pms_schedule_id > 0){
  $q=$d->select("pms_answer_master","pms_answer_master.society_id='$society_id' AND pms_answer_master.pms_schedule_id='$pms_schedule_id'");
}
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Performance Report Detail</h4>
        </div>
     </div>
      <?php if(isset($pms_schedule_id) && $pms_schedule_id > 0){ 
        if(mysqli_num_rows($q) > 0){?>
      <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row m-0">
              <div class="col-lg-12">
                <div class="row border mb-3 m-0">
                  <?php 
                    $q1=$d->selectRow("pms_schedule_master.*, users_master.user_id, users_master.user_full_name, employee_level_master.level_name, dimensional_master.dimensional_name, ru.user_full_name AS reviewer_name, rl.level_name AS reviewer_level","pms_schedule_master LEFT JOIN users_master ru ON ru.user_id = pms_schedule_master.reviewer_id LEFT JOIN employee_level_master rl ON rl.level_id = pms_schedule_master.reviewer_level_id, users_master, employee_level_master, dimensional_master","pms_schedule_master.dimensional_id=dimensional_master.dimensional_id AND pms_schedule_master.user_id=users_master.user_id AND pms_schedule_master.user_level_id = employee_level_master.level_id AND pms_schedule_master.society_id='$society_id' AND pms_schedule_master.schedule_status='1' AND pms_schedule_master.pms_schedule_id='$pms_schedule_id' $blockAppendQueryUser");
                    $row=mysqli_fetch_array($q1);

                  ?>
                  <div class="col-md-6">
                    <p><label>Dimensional Name : </label> <?php echo $row['dimensional_name']; ?></p>
                  </div>
                  <div class="col-md-6">
                    <p><label>Schedule Date : </label> <?php echo date('d M Y', strtotime($row['schedule_date'])); ?></p>
                  </div>
                  <div class="col-md-6">
                    <p><label>Employee Name : </label> <?php echo $row['user_full_name']; ?> (<?php echo $row['level_name'] ?>)</p>
                  </div>
                  <div class="col-md-6">
                    <p><label>Submitted Date : </label> <?php echo date('d M Y h:i A', strtotime($row['schedule_submit_date'])); ?></p>
                  </div>
                  <?php if($row['reviewer_id']>0){ ?>
                  <div class="col-md-6">
                    <p><label>Reviewer Name : </label> <?php echo $row['reviewer_name']; ?> (<?php echo $row['reviewer_level'] ?>)</p>
                  </div>
                  <div class="col-md-6">
                    <p><label>Review Date : </label> <?php echo date('d M Y h:i A', strtotime($row['review_date'])); ?></p>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <?php 
              $counter = 1;
                while ($data=mysqli_fetch_array($q)) { ?>
                    <div class="d-flex">
                      <h6><?php echo $counter++.') '.$data['attribute_name'] ?></h6>
                    </div>
                    <div class="ml-4">
                        <?php if($data['attribute_type'] == 0){ ?>
                          <div class="form-group col-md-12">
                            <p>Employee Answer:</p>
                            <p><b><?php echo $data['attribute_employee_answer']; ?></b></p>
                            <?php if($data['attribute_senior_answer'] != ''){ ?>
                              <p>Reviewer Answer:</p>
                              <div class="progress h-auto">
                              <div class="progress-bar" role="progressbar" style="width: <?php echo $data['attribute_senior_answer']; ?>%;"><?php echo $data['attribute_senior_answer']; ?>%</div>
                            </div>
                            <?php } ?>
                          </div>
                        <?php }elseif($data['attribute_type'] == 1){ ?>
                          <div class="form-group col-md-12">
                            <p>Employee Answer:</p>
                            <div class="progress h-auto">
                              <div class="progress-bar" role="progressbar" style="width: <?php echo $data['attribute_employee_answer']; ?>%;"><?php echo $data['attribute_employee_answer']; ?>%</div>
                            </div>  
                            <?php if($data['attribute_senior_answer'] != ''){ ?>
                              <p>Reviewer Answer:</p>
                              <div class="progress h-auto">
                              <div class="progress-bar" role="progressbar" style="width: <?php echo $data['attribute_senior_answer']; ?>%;"><?php echo $data['attribute_senior_answer']; ?>%</div>
                            </div>
                            <?php } ?>                        
                          </div>
                        <?php }elseif($data['attribute_type'] == 2){ ?>
                          <div class="form-group col-md-12">
                            <p>Employee Answer:</p>
                              <?php
                                $rating = $data['attribute_employee_answer'];
                                $star = '';
                                if($rating>0){
                                  (2.8*2)%2;
                                  $fullStars = floor( $rating );
                                  $halfStar = round(( $rating * 2 ) % 2);
                                  $t=0;
                                  for( $i=0; $i<5; $i++ ){
                                    if($i<$fullStars){
                                      $star .= "<img src='../img/star-full.jpg' alt='Full Star' style='width:30px; height:30px;margin-right: 8Px;'>";
                                      $t = $t+1;
                                    }
                                  }
                                  if( $halfStar ){
                                    $star .= "<img src='../img/star-half.jpg' alt='Half Star' style='width:30px; height:30px;margin-right: 8Px;'>";
                                    $t = $t+1;
                                  }
                                  for($i=5;$i>$t;$i--){
                                    $star .= "<img src='../img/star-not.jpg' alt='Half Star' style='width:30px; height:30px;margin-right: 8Px;'>";
                                  }
                                }else{
                                  for( $i=0; $i<5; $i++ ){
                                    $star .= "<img src='../img/star-not.jpg' alt='Half Star' style='width:30px; height:30px;margin-right: 8Px;'>";
                                  }
                                }
                                echo $star;
                              ?>
                              <?php if($data['attribute_senior_answer'] != ''){ ?>
                                <p>Reviewer Answer:</p>
                                <div class="progress h-auto">
                              <div class="progress-bar" role="progressbar" style="width: <?php echo $data['attribute_senior_answer']; ?>%;"><?php echo $data['attribute_senior_answer']; ?>%</div>
                            </div>
                                <?php } ?>  
                          </div>
                        <?php }elseif($data['attribute_type'] == 3){ ?>
                          <div class="form-group col-md-12">
                            <p>Employee Answer:</p>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '1'){ echo 'btn-primary'; } ?>">1</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '2'){ echo 'btn-primary'; } ?>">2</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '3'){ echo 'btn-primary'; } ?>">3</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '4'){ echo 'btn-primary'; } ?>">4</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '5'){ echo 'btn-primary'; } ?>">5</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '6'){ echo 'btn-primary'; } ?>">6</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '7'){ echo 'btn-primary'; } ?>">7</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '8'){ echo 'btn-primary'; } ?>">8</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '9'){ echo 'btn-primary'; } ?>">9</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == '10'){ echo 'btn-primary'; } ?>">10</button>
                            <?php if($data['attribute_senior_answer'] != ''){ ?>
                              <p>Reviewer Answer:</p>
                              <div class="progress h-auto">
                              <div class="progress-bar" role="progressbar" style="width: <?php echo $data['attribute_senior_answer']; ?>%;"><?php echo $data['attribute_senior_answer']; ?>%</div>
                            </div>
                            <?php } ?>
                          </div>
                        <?php }elseif($data['attribute_type'] == 4){ ?>
                          <div class="form-group col-md-12">
                            <p>Employee Answer:</p>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == 'A'){ echo 'btn-primary'; } ?>">A</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == 'B'){ echo 'btn-primary'; } ?>">B</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == 'C'){ echo 'btn-primary'; } ?>">C</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == 'D'){ echo 'btn-primary'; } ?>">D</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == 'E'){ echo 'btn-primary'; } ?>">E</button>
                            <button type="button" class="btn btn-sm <?php if($data['attribute_employee_answer'] == 'F'){ echo 'btn-primary'; } ?>">F</button>
                            <?php if($data['attribute_senior_answer'] != ''){ ?>
                              <p>Reviewer Answer:</p>
                              <div class="progress h-auto">
                              <div class="progress-bar" role="progressbar" style="width: <?php echo $data['attribute_senior_answer']; ?>%;"><?php echo $data['attribute_senior_answer']; ?>%</div>
                            </div>
                            <?php } ?>
                          </div>
                        <?php } ?>
                      </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div><!-- End Row-->
      <?php }else{ ?>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <p>Please Configure your template question</p>
              </div>
            </div>
          </div>
        </div>
      <?php } }?>
    </div>
    <!-- End container-fluid-->
    
    </div>
<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>