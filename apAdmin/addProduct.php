<?php
if(isset($_POST['addProductBtn']) && $_POST['addProductBtn'] == "addProductBtn")
{
    extract($_POST);
}
if(isset($_POST['editProduct']) && isset($_POST['product_id']))
{
    $product_id = $_POST['product_id'];
    $q = $d->select("retailer_product_master","product_id = '$product_id'");
    $data = $q->fetch_assoc();
    extract($data);
    $form_id = "productAddForm";
}
else
{
    $form_id = "productAddForm";
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <div class="pb-2">
                            <a href="#" class="btn btn-sm btn-warning float-right mmw-70" data-toggle="modal" data-target="#bulkUpload">Import</a>
                        </div> -->
                        <form id="<?php echo $form_id; ?>" action="controller/orderProductController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <?php
                            if(isset($_POST['editProduct']) && isset($_POST['product_id']))
                            {
                            ?>
                            <i class="fa fa-edit"></i>
                            Edit Product
                            <?php
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-plus"></i>
                            Add Product
                            <?php
                            }
                            ?>
                            </h4>
                            <div class="form-group row">
                                <label for="product_category_id" class="col-sm-2 col-form-label">Category <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="product_category_id" name="product_category_id" required onchange="getSubCatList(this.value);">
                                        <option value="">-- Select --</option>
                                        <?php
                                        $cq = $d->select("product_category_master","product_category_delete = 0 AND product_category_status = 0");
                                        while ($cd = $cq->fetch_assoc())
                                        {
                                        ?>
                                        <option <?php if($product_category_id == $cd['product_category_id']) { echo 'selected'; } ?> value="<?php echo $cd['product_category_id'];?>"><?php echo $cd['category_name'];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="product_sub_category_id" class="col-sm-2 col-form-label">Sub Category</label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="product_sub_category_id" name="product_sub_category_id">
                                        <option value="">-- Select --</option>
                                        <?php
                                        if(isset($editProduct) || isset($addProductBtn))
                                        {
                                            $qsc = $d->selectRow("product_sub_category_id,sub_category_name","product_sub_category_master","product_sub_category_status = 0 AND product_sub_category_delete = 0");
                                            while ($qdsc = $qsc->fetch_assoc())
                                            {
                                            ?>
                                        <option <?php if($product_sub_category_id == $qdsc['product_sub_category_id']) { echo 'selected'; } ?> value="<?php echo $qdsc['product_sub_category_id']; ?>"><?php echo $qdsc['sub_category_name'];?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="product_name" class="col-sm-2 col-form-label">Product Name <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <input value="<?php if(isset($editProduct)){ echo $product_name; } ?>" required type="text" class="form-control" name="product_name" id="product_name" maxlength="150">
                                </div>
                                <label for="product_description" class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-4">
                                    <textarea rows="5" id="product_description" name="product_description" maxlength="1000" class="form-control"><?php if(isset($editProduct) && $product_description != "") { echo $product_description; } ?></textarea>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                            <div class="form-footer text-center">
                                <input type="hidden" name="addProduct" value="addProduct">
                                <?php
                                if(isset($editProduct))
                                {
                                ?>
                                <input type="hidden" class="form-control" name="product_id" id="product_id" value="<?php echo $product_id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->


<div class="modal fade" id="bulkUpload">
    <div class="modal-dialog">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Import Bulk</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="controller/orderProductController.php" method="post" enctype="multipart/form-data">
                    <label class="col-sm-12 col-form-label">DOWNLOAD DETAILS FILE -> <button type="submit" name="getDetailsInfo" value="getDetailsInfo" class="btn btn-sm btn-primary"><i class="fa fa-check-square-o"></i>Download</button></label>
                </form>
                <form action="controller/orderProductController.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                    <div class="form-group row">
                        <label class="col-sm-12 col-form-label">STEP 1 -> DOWNLOAD FORMATTED CSV <button type="submit" name="ExportProducts" value="ExportProducts" class="btn btn-sm btn-primary"><i class="fa fa-check-square-o"></i> Download</button></label>
                        <label class="col-sm-12 col-form-label">STEP 2 -> FILL YOUR DATA</label>
                        <label class="col-sm-12 col-form-label">STEP 3 -> IMPORT THIS FILE HERE</label>
                        <label class="col-sm-12 col-form-label">STEP 4 -> CLICK ON UPLOAD BUTTON</label>
                        <label class="col-sm-12 col-form-label text-danger">Note : Please enter all valid details</label>
                    </div>
                </form>
                <form id="importValidation" action="controller/orderProductController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Import CSV File<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input required type="file" name="file" accept=".csv" class="form-control-file border">
                        </div>
                    </div>
                    <input type="hidden" name="importProducts" value="importProducts">
            </div>
            <div class="modal-footer">
                <input type="hidden" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                <button type="button" class="btn btn-danger btn-sm mmw-70" data-bs-dismiss="modal">CLOSE</button>
                <button type="submit" class="btn btn-sm btn-success">UPLOAD</button>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="assets/js/jquery.min.js"></script>
<script>
function getSubCatList(product_category_id)
{
    $.ajax({
        url: 'controller/orderProductController.php',
        data: {getSubCatList:"getSubCatList",product_category_id:product_category_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#product_sub_category_id').empty();
            response = JSON.parse(response);
            $('#product_sub_category_id').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#product_sub_category_id').append("<option value='"+value.product_sub_category_id+"'>"+value.sub_category_name+"</option>");
            });
        }
    });
}
</script>