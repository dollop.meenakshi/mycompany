 <?php 
    if(isset($_POST['sos_event_id'])) {
    $btnName="Update";
    extract(array_map("test_input" , $_POST));
    $q=$d->select("sos_events_master","sos_event_id='$sos_event_id'");
    $data=mysqli_fetch_array($q);

    } else {
    $btnName="Add";
    }
     ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
    
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="sosAdd" action="controller/sosController.php" enctype="multipart/form-data" method="post">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                   <?php echo $xml->string->society; ?> SOS
                </h4>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label">SOS Name <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <?php if(isset($_POST['sos_event_id'])) { ?>
                    <input maxlength="20" type="text" class="form-control text-capitalize" value="<?php echo $data['event_name']; ?>" id="input-10" name="event_name_edit" required="" maxlength="20">
                    <input type="hidden" name="sos_event_id" value="<?php echo $data['sos_event_id']; ?>">
                    <?php } else {?>
                    <input  maxlength="20" type="text" class="form-control text-capitalize" id="input-10" name="event_name" required="" maxlength="20">
                   <?php } ?>
                  </div>
                  
                </div>
               
                <div class="form-group row">
                  <label for="input-16" class="col-sm-2 col-form-label">SOS For <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control" required="" id="input-16" name="sos_for">
                      <option value=""> -- Select --</option>
                      <?php // IS_1023 isset($_POST['sos_event_id']) && added ?> 
                      <option <?php if($data['sos_for']==0) { echo 'selected'; } ?> value="0">All</option>
                      <option <?php if($data['sos_for']==1) { echo 'selected'; } ?> value="1">Resident</option>
                      <option <?php if($data['sos_for']==2) { echo 'selected'; } ?> value="2">Security Guard</option>
                      <option <?php if($data['sos_for']==3) { echo 'selected'; } ?> value="3">Secretary</option>
                      <option <?php if($data['sos_for']==4) { echo 'selected'; } ?> value="4">Security Guard & Secretary</option>
                      <option <?php if($data['sos_for']==5) { echo 'selected'; } ?> value="5">Security Guard & Residents </option>
                    </select>
                  </div>
                </div>
                <?php // IS_1023 for="sos_image" ?> 
                <div class="form-group row">
                  <label for="sos_image" class="col-sm-2 col-form-label">SOS Image <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <?php if(isset($_POST['sos_event_id'])) { ?>
                    <input accept="image/*" type="file" class="form-control-file border photoOnly" value="<?php echo $data['sos_image']; ?>" id="sos_image" name="sos_image"  >
                    <input type="hidden" name="sos_image_old" id="sos_image_old" value="<?php echo $data['sos_image']; ?>">
                    <?php } else {?>
<?php // IS_1023  img_req ?> 
                      <?php if(isset($_POST['sos_event_id'])) { ?>
                      <input type="hidden" id="img_req" value="1">
                    <?php }  else {?>
                      <input type="hidden" id="img_req" value="0">
                    <?php } ?>
                    <input accept="image/*" type="file" class="form-control-file border photoOnly" id="sos_image" name="sos_image" required="" >
                   <?php } ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="sos_duration" class="col-sm-2 col-form-label">SOS Minutes <span class="text-danger">*</span></label>
                  <div class="col-sm-10">

<?php // IS_1023   max="120" min="0"  maxlength="5" ?> 
                    <?php if(isset($_POST['sos_event_id'])) { ?>
                    <div class="input-group">
                      <input  type="text" class="form-control " required="" id="sos_duration" name="sos_duration" value="<?php echo $data['sos_duration'] ?>"  max="120" min="0"  maxlength="5"/>
                      <div class="input-group-prepend">
                        <span class="input-group-text">mins</span>
                      </div>
                    </div>
                    <?php } else {?>
                    <div class="input-group">
                      <input  type="text" class="form-control " required="" id="sos_duration" name="sos_duration" max="120" min="0" maxlength="5" />
                      <div class="input-group-prepend">
                        <span class="input-group-text">mins</span>
                      </div>
                    </div>
                   <?php } ?>
                  </div>
                </div>
 
                <div class="form-footer text-center">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>

            <?php if(!isset($_POST['sos_event_id'])) { ?>
             <div class="card-body">
              <form  id="empTypeAdd" action="controller/sosController.php" method="post" enctype="multipart/form-data">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                     Add From Common SOS 
                </h4>
                <?php 
                $soEmpType = array();
                $q11=$d->select("sos_events_master","society_id='$society_id' ");
               while ($row=mysqli_fetch_array($q11)) {
                    array_push($soEmpType, $row['event_name']);
                 }
                 // print_r($soEmpType);
                ?>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-3 col-form-label">SOS Types </label>
                  <div class="col-sm-9">
                     <ul style="list-style: none;">
                    <?php
                      $q=$d->select("sos_events_common","");
                      $i1=1;
                        while ($row=mysqli_fetch_array($q)) {
                          extract($row);
                    ?>    
                      <li><label><?php if(!in_array($event_name, $soEmpType)){ ?><input  class="sos_event_id" value="<?php echo $sos_event_id; ?>" id="unit_id<?php echo $i1++; ?>"  checked="" type="checkbox" name="sos_event_id[]"> <?php } ?> <img width="80" src="../img/sos/<?php echo $sos_image; ?>" alt="">  <?php echo $event_name; ?>   <?php if(in_array($event_name, $soEmpType)){ echo "(Already Added)"; } ?>
                      </label></li> 
                  <?php  } ?>
                  </ul>
                  </div>
                  
                </div>
                
              
                <div class="form-footer text-center">
                    <input type="hidden" name="addBulk" value="addBulk">
                    <button  type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                </div>
              </form>
            </div>
          <?php  }?>

          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->