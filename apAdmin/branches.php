  <?php
  
  
 
$q = $d->selectRow('*', "society_master", "society_id='$society_id'");
$data = mysqli_fetch_assoc($q);

$user_latitud = $data['society_latitude'];
$user_longitude = $data['society_longitude'];
 ?>
  
  <div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-md-4 col-6">
        <h4 class="page-title"> <?php echo $xml->string->blocks; ?></h4>
      </div>
      <div class="col-md-4  d-none d-lg-block">
          <?php if ($_GET['changeOrder']=='yes') {
            $dragId = "branch-image-list";
           ?>
          <span class="text-warning"><b>Drag to change order </b></span>
          <?php } else { ?>
          <a  href="branches?changeOrder=yes" class="btn  btn-info btn-sm waves-effect waves-light"><i class="fa fa-swap mr-1"></i> Change Order</a>
          <?php } ?>
          
          
      </div>
      <div class="col-md-4 col-6 text-right">
       <div class="btn-group float-sm-right">
        <?php $totalBlock= $d->count_data_direct("block_id","block_master","society_id='$society_id'");
        if ($totalBlock==0) {
         
         ?>
         <a href="branch" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> <?php echo $xml->string->multiple; ?> <?php echo $xml->string->block; ?></a>
       <?php } else { ?>
        <a href="branch" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> <?php echo $xml->string->add; ?> <?php echo $xml->string->single; ?> <?php echo $xml->string->block; ?></a>

      <?php } ?>
    </div>
  </div>
</div>
<!-- End Breadcrumb-->
<div class="row" id="<?php echo $dragId; ?>">
 <?php 
 $i=1;
 $q = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_sort ASC");
 if(mysqli_num_rows($q)>0) {

  while ($data=mysqli_fetch_array($q)) {
    extract($data);
    $countUsers=$d->count_data_direct("block_id","users_master","society_id!=0 AND society_id='$society_id' AND block_id='$block_id'");
    ?>  
    
    <div class="col-lg-3  col-6 branchBox" data-post-id="<?php echo $data["block_id"]; ?>">
      <div class="card bg-primary">
       <div class="card-body text-center text-white">
        <a href="departments" class="text-white">
         <?php echo $xml->string->blocks; ?>
         <h5 class="mt-2 text-white"><?php echo $block_name; ?> </h5>
       </a>
        <i title="Edit Block Name" class="fa fa-pencil pull-right"  data-toggle="modal" data-target="#editFloor" onclick="editBlock('<?php echo $data['block_id']; ?>','<?php echo $data['block_name']; ?>','<?php echo $data['block_sort'] ?>');"></i>
        <i title="marker" class="fa fa-map-marker pull-right" aria-hidden="true"  data-toggle="modal" data-target="#blockModal2" onclick="editBlockLatLong('<?php echo $data['block_id']; ?>')"></i>
          <?php if ($countUsers==0){ ?><i title="Delete Block" onclick="DeleteBlock('<?php echo $block_id ?>');" class="fa fa-trash-o pull-right mr-2"></i>
        <?php } else { ?>
          <i title="Delete Block" class="fa fa-trash-o pull-right" onclick="showError('Delete This <?php echo $xml->string->block; ?> All users then delete this <?php echo $xml->string->block; ?>..!')"></i>
        <?php } ?>



       
       <div class="row">
        <div class="col-12 border-right border-light-2">  



          

    </div>
  </div>
  <?php  ?>

</div>

</div>
</div>




<?php // ?>
<?php } } else {
  echo "<img src='img/no_data_found.png'>";
} 


?>
   




</div>
<!-- End container-fluid-->

</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->


<div class="modal fade" id="editFloor">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->edit; ?> <?php echo $xml->string->blocks; ?> <?php echo $xml->string->name; ?></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="blockAdd" action="controller/blockController.php" method="post">
          <input type="hidden" id="society_id" name="society_id" value="<?php echo $society_id;?>">
          <input type="hidden" id="floorId" name="block_id">
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->blocks; ?> <?php echo $xml->string->name; ?> <span class="required">*</span></label>
            <div class="col-sm-8" id="PaybleAmount">
              <input type="text" id="oldFloorname" class="form-control text-capitalize txtNumeric" name="block_name" required="" maxlength="30" autocomplete="off">
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->order_no; ?> <span class="required">*</span></label>
            <div class="col-sm-8" id="PaybleAmount">
              <input type="text" class="form-control number" id="block_sort" name="block_sort" required="">
            </div>
          </div>
          
          <div class="form-footer text-center">
            <input type="hidden" name="updateBlocks" value="updateBlocks">
            <button type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->update; ?></button>
          </div>

        </form>
      </div>
      
    </div>
  </div>
</div><!--End Modal -->
<div class="modal fade" id="blockModal2">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Block Map</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="blockAdd" action="controller/blockController.php" method="post">
        <input id="searchInput5" name="location_name" class="form-control" type="text" placeholder="Enter location" >
            <div class="map" id="map" style="width: 100%; height: 300px;"></div>
            <input type ="hidden"  readonly id="block_latitude" name="block_latitude" class="form-control" value="<?php echo $user_latitud; ?>">
            <input type ="hidden"  readonly id="update_block_id" name="update_block_id" class="form-control" >
            <input type ="hidden"    readonly id="block_longitude" name="block_longitude" class="form-control" value="<?php echo $user_longitude; ?>">
            <div class="form-footer text-center">
            <input type="hidden" name="updateBLockLatLong" value="updateBLockLatLong">
            <button type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->update; ?></button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
<!--  -->

<!--  -->
<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>
<script type="text/javascript">

$().ready(function()
  {
    var latitude = $('#block_latitude').val();
    var longitude = $('#block_longitude').val();
    initialize(latitude,longitude);
  });

  function initialize(d_lat,d_long)
  {
    var latlng = new google.maps.LatLng(d_lat,d_long);
    var latitute = 23.037786;
    var longitute = 72.512043;

    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });
    var parkingRadition = 5;
    var citymap = {
        newyork: {
          center: {lat: latitute, lng: longitute},
          population: parkingRadition
        }
    };

    var input = document.getElementById('searchInput5');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    autocomplete10.addListener('place_changed', function()
    {
      infowindow.close();
      marker.setVisible(false);
      var place5 = autocomplete10.getPlace();
      if (!place5.geometry) {
          window.alert("Autocomplete's returned place5 contains no geometry");
          return;
      }

      // If the place5 has a geometry, then present it on a map.
      if (place5.geometry.viewport) {
          map.fitBounds(place5.geometry.viewport);
      } else {
          map.setCenter(place5.geometry.location);
          map.setZoom(17);
      }

      marker.setPosition(place5.geometry.location);
      marker.setVisible(true);

      var pincode="";
      for (var i = 0; i < place5.address_components.length; i++) {
        for (var j = 0; j < place5.address_components[i].types.length; j++) {
          if (place5.address_components[i].types[j] == "postal_code") {
            pincode = place5.address_components[i].long_name;
          }
        }
      }
      bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
      infowindow.setContent(place5.formatted_address);
      infowindow.open(map, marker);

    });
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function()
    {
      geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
      {
        if (status == google.maps.GeocoderStatus.OK)
        {
          if (results[0])
          {
           
            var places = results[0];
            var pincode="";
            var serviceable_area_locality= places.address_components[4].long_name;
            for (var i = 0; i < places.address_components.length; i++)
            {
              for (var j = 0; j < places.address_components[i].types.length; j++)
              {
                if (places.address_components[i].types[j] == "postal_code")
                {
                  pincode = places.address_components[i].long_name;
                }
              }
            }
            bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
          }
        }
      });
    });
  }

  function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality)
  {
    document.getElementById('block_latitude').value = lat;
    document.getElementById('block_longitude').value = lng;
  }
  </script>