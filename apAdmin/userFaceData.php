<?php error_reporting(0);
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$uId = (int)$_REQUEST['uId'];
$faceType = $_REQUEST['faceType'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-md-12">
          <h4 class="page-title">User Face Data</h4>
        </div>
     </div>
     <form action="" method="get" class="branchDeptFilter">
        <div class="row pt-2 pb-2">
        <?php include('selectBranchDeptForFilterAll.php'); ?>
          <div class="col-md-2 form-group">
              <select name="faceType" class="form-control single-select">
                  <option <?php if($faceType=="0") { echo 'selected';} ?> value="0">All Employee</option>
                  <option <?php if($faceType==1) { echo 'selected';} ?> value="1">Registered Face</option>
                  <option <?php if($faceType==2) { echo 'selected';} ?> value="2">Not Registered</option>
                 
              </select>
          </div>
          <div class="col-md-4 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
          </div>
        </div>
    </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                  <?php 
                        $i=1;
                        if(isset($bId) && $bId>0) {
                            $blockFilterQuery = " AND users_master.block_id='$bId'";
                        }
                        if(isset($dId) && $dId>0) {
                            $deptFilterQuery = " AND users_master.floor_id='$dId'";
                        }

                        if (isset($faceType) && $faceType==1) {
                           $appendRegisterFace = " AND users_master.face_data_image!=''";
                        } else if (isset($faceType) && $faceType==2) {
                           $appendRegisterFace = " AND users_master.face_data_image=''";

                        }
                        
                        $q=$d->select("users_master,block_master,floors_master","block_master.block_id=users_master.block_id AND floors_master.floor_id=users_master.floor_id AND users_master.society_id='$society_id' AND users_master.delete_status=0 $blockFilterQuery $deptFilterQuery $blockAppendQuery $appendRegisterFace");
                        $counter = 1;
                        if (isset($bId) && isset($dId) ) {
                     
                  ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>                        
                        <th>Branch</th>                        
                        <th>Department</th>                        
                        <th>User Name</th>
                        <th>Designation</th>
                        <th>Face Image</th>
                        <th>Date</th>
                        <th>Face Data</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      while ($data = mysqli_fetch_array($q)) {
                       
                    ?>
                    <tr>
                        <td><?php echo $counter++; ?></td>
                        <td><?php echo $data['block_name']; ?></td>
                        <td><?php echo $data['floor_name']; ?></td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php custom_echo($data['user_designation'],20); ?></td>
                        <td><?php if ($data['face_data_image']!='') { ?>
                            <a href="../img/attendance_face_image/<?php echo $data['face_data_image']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["face_data_image"]; ?>"><img width="50" height="50" onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png"  src="../img/ajax-loader.gif" data-src="../img/attendance_face_image/<?php echo $data['face_data_image']; ?>"  href="#divForm<?php echo $data['user_id'];?>" class="btnForm lazyload" ></a>

                            <a href="../img/attendance_face_image/<?php echo $data['face_data_image_two']; ?>" data-fancybox="images" data-caption="Photo Name : <?php echo $data["face_data_image_two"]; ?>"><img width="50" height="50" onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png"  src="../img/ajax-loader.gif" data-src="../img/attendance_face_image/<?php echo $data['face_data_image_two']; ?>"  href="#divForm<?php echo $data['user_id'];?>" class="btnForm lazyload" ></a>
                            <?php } ?></td>
                        <td><?php if($data['user_face_id'] != '' && $data['user_face_data'] != '' && $data['face_added_date']!='0000-00-00 00:00:00' && $data['face_added_date']!='null') { echo date("d M Y h:i A", strtotime($data['face_added_date'])); }?></td>
                        <td><?php if($data['user_face_id'] != '' && $data['user_face_data'] != '') {?>
                            <span style="color: #24e624;"><i class="fa fa-check-circle" aria-hidden="true"></i></span>
                            <?php }else
                            { ?>
                            <span style="color: red;"><i class="fa fa-times-circle-o" aria-hidden="true"></i></span>

                            <?php } ?>
                        </td>
                        <td><?php if($data['user_face_id'] != '' && $data['user_face_data'] != '') {?>
                          <a href="javascript:void(0)" onclick="deleteUserFaceData(<?php echo $data['user_id']; ?>);" class="btn btn-sm btn-danger"><i class="fa fa-trash-o fa-lg"></i></a>
                          <?php } ?>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
                
            </table>
            <?php }  ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>
    <input type="hidden" name="dptIdOld" id="dptIdOld" value="<?php if(isset($dId) && $dId !=""){ echo $dId;} ?>">
    <script src="assets/js/jquery.min.js"></script>
 
<script type="text/javascript">

<?php  if(isset($bId) && $bId>0){ ?>
  getFloorByBranchId(<?php echo $bId;?>);
<?php }
?>
function getFloorByBranchId(id)
{
  dptIdOld = $('#dptIdOld').val();
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getFloorByBlockId",
          block_id:id,
        },
      success: function(response){
        console.log(response);
        optionContent = `<option value="0">All Departments</option>`;
        $.each(response.floor, function( index, value ) {
          if(dptIdOld==value.floor_id)
          {
            selected="selected";
          }
          else
          {
            selected="";

          }
          optionContent += `<option `+selected+` value="`+value.floor_id+`" >`+value.floor_name+` (`+value.block_name+`)</option>`;
        });
        $('#dptId').html(optionContent);
      }
  }); 
}
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
