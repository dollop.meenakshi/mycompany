<?php

session_start();
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$base_url=$m->base_url();
$society_id=$_COOKIE['society_id'];
extract(array_map("test_input" , $_POST));

if(isset($timeline) && $timeline=='yes')
{

  

  $qn = $d->selectRow("news_feed.*,
    (SELECT COUNT(*) FROM news_like WHERE feed_id=news_feed.feed_id AND active_status=0) AS likesCount,
    (SELECT COUNT(*) FROM news_comments WHERE feed_id=news_feed.feed_id AND parent_comments_id=0) AS commentCount
    ",
    "news_feed ",
    "news_feed.society_id='$society_id' AND news_feed.status = 0   ", 
    "ORDER BY feed_id DESC LIMIT $limit OFFSET $offset");
       // $qn = $d->selectRow("users_master.*,news_feed.*,bms_admin_master.*","news_feed LEFT JOIN users_master ON users_master.user_id = news_feed.user_id LEFT JOIN bms_admin_master ON bms_admin_master.admin_id= news_feed.user_id","news_feed.society_id='$society_id' AND news_feed.status = 0 AND news_feed.user_id=users_master.user_id  $blockAppendQueryUser", "ORDER BY feed_id DESC LIMIT 100"); 
    if (mysqli_num_rows($qn) > 0) {

      $feedCount = 1;
      while ($data_notification = mysqli_fetch_array($qn)) {
       // print_r($data_notification);
        $feed_id = $data_notification['feed_id'];
        $fedCount1 = $feedCount++;

        $likesCount = $data_notification['likesCount'];
        $totalCmt = $data_notification['commentCount'];
      ?>
        <div class="facebook-card">
          <div class="facebook-card-header">
            <img onerror="this.src='img/user.png'" class="imgRedonda" src="../img/users/recident_profile/<?php echo $data_notification['user_profile_pic']; ?>" width="10%">
            <a class="profileName" href="employeeDetails?id=<?php echo $data_notification['user_id']; ?>"><?php echo $data_notification['user_name']; ?> </a>

            <a style="margin-left: 12px;" href="javascript:void(0);" class="btn btn-sm btn-danger  pull-right" onclick="deletePost('<?php echo $feed_id; ?>')"><i title="Delete Post" class="fa fa-trash-o"></i></a>



            <p style="word-break: break-all !important;"> <?php echo $data_notification['feed_msg']; ?> </p>

          </div>
          <?php
          $checImage = $data_notification['feed_img'];
          $feed_video = $data_notification['feed_video'];
          $feed_img = explode('~', $data_notification['feed_img']);
          $imageCount = count($feed_img);
          if ($checImage != '' &&  $feed_video == '') {
          ?>
            <!--  <div class="facebook-card-image">
              <a href="../img/users/recident_feed/<?php echo $feed_img[0]; ?>" data-fancybox="images" data-caption="">
              <img src="../img/users/recident_feed/<?php echo $feed_img[0]; ?>">
              </a>
            </div> -->
            <div id="carousel-4<?php echo $fedCount1; ?>" class="carousel slide" data-ride="carousel">
              <ul class="carousel-indicators">
                <?php for ($i1 = 0; $i1 < $imageCount; $i1++) { ?>
                  <li data-target="#demo" data-slide-to="0" class="<?php if ($i1 == 1) {
                                                                      echo "active";
                                                                    } ?>"></li>
                <?php } ?>
              </ul>
              <div class="carousel-inner">
                <?php
                for ($x1 = 0; $x1 < $imageCount; $x1++) {
                ?>
                  <div style="background-color: white;" class="carousel-item <?php if ($x1 == 1) {
                                                                                echo 'active';
                                                                              } else if ($imageCount == 1) {
                                                                                echo 'active';
                                                                              } ?>">
                    <?php //IS_380  echo $feed_id; added in  data-fancybox 
                    ?>
                    <a href="../img/users/recident_feed/<?php echo $feed_img[$x1]; ?>" data-fancybox="images<?php echo $feed_id; ?>" data-caption="Photo Name : <?php echo $feed_img[$x1]; ?>">
                      <img style="max-height:450px;" class="d-block w-100 lazyload" src="../img/logo.png" data-src="../img/users/recident_feed/<?php echo $feed_img[$x1]; ?>" alt="">
                    </a>
                  </div>
                <?php } ?>
              </div>
              <a class="carousel-control-prev" href="#carousel-4<?php echo $fedCount1; ?>" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#carousel-4<?php echo $fedCount1; ?>" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
            <br>
          <?php } else if ($feed_video != '') {
            $ext = pathinfo($data_notification['feed_video'], PATHINFO_EXTENSION);
          ?>
            <video class="inlineVideo" autobuffer autoloop loop controls style="width: 100%;max-height: 550px;">
              <source src="../img/users/recident_feed/<?php echo $data_notification['feed_video']; ?>">
              <source src="../img/users/recident_feed/<?php echo $data_notification['feed_video']; ?>">
              <object type="video/<?php echo $ext; ?>" data="../img/users/recident_feed/<?php echo $data_notification['feed_video']; ?>" width="100%">
                <param name="src" value="../img/users/recident_feed/<?php echo $data_notification['feed_video']; ?>">
                <param name="autoplay" value="false">
                <param name="autoStart" value="0">
              </object>
            </video>
          <?php } ?>
          <ul class="horizontal">
            <li>
              <?php
              // $likesCount = $d->count_data_direct("like_id", "news_like", "feed_id='$feed_id' AND active_status=0 ");
              ?>
              <span data-toggle="modal" <?php if ($likesCount > 0) { ?> data-target="#viewLikesModal" onclick="viewLikes('<?php echo $feed_id; ?>');" <?php } ?> class=" pointerCursor">Like (<?php echo  $likesCount; ?>)</span>
            </li>
            <li>
              <?php //$totalCmt = $d->count_data_direct("comments_id", "news_comments", "feed_id='$feed_id' AND parent_comments_id=0"); ?>
              <?php if ($totalCmt > 0) {  ?>
                <button class="collapsed " onclick="hideNewsFeedInfo('<?php echo $feed_id; ?>');" data-toggle="collapse" data-target="#collapse<?php echo $feed_id; ?>" aria-expanded="false" aria-controls="collapse-6" style="border:none !important; background-color: #fff !important; color:gray !important; " title="View All">Comment (<?php echo $totalCmt; ?>)</button>
              <?php } ?>
            </li>
          </ul>
          <a class="time pull-right"> <?php echo time_elapsed_string($data_notification['modify_date']);  ?></a> <br>
          
          <div class="facebook-card-footer text-right">
          <!-- <button class="btn btn-sm btn-primary "  data-toggle="collapse" href="#collap_<?php //echo $feed_id; ?>">Add Comment</button> -->
          </div>
          <br>
          <div class="collapse" id="collap_<?php echo $feed_id; ?>">
            <div class="card card-body">
              <form>
                <div class="row">
                    <div class="col-md-12">
                        <textarea name="comment" id="comment_<?php echo $feed_id; ?>" placeholder="Add Comment" class="form-control "></textarea>
                    </div>
                    <div class="col-md-12 text-right">
                      <button type="button" onclick="addCommentOnPost(<?php echo $feed_id; ?>,<?php echo $society_id; ?>,<?php echo $_COOKIE['bms_admin_id']; ?>)" class="btn btn-sm btn-primary mt-1"> Add</button>
                    </div>
                </div>
              </form>
            </div>
          </div>
          <!-- <input type="textbox" class="textbox form-control" placeholder="Write a comment" size="45">    <br> <br> -->
          <?php //IS_387 
          ?>
          <div id="collapse<?php echo $feed_id; ?>" class="collapse" style="">
            <div class="collapse_<?php echo $feed_id;?>">
              <?php
              /*if ($totalCmt > 0) {
                $qcomment = $d->selectRow("news_comments.*,users_master.user_profile_pic", "news_comments LEFT JOIN users_master ON users_master.user_id = news_comments.user_id LEFT JOIN bms_admin_master ON bms_admin_master.admin_id = news_comments.user_id ", "news_comments.feed_id='$feed_id' AND news_comments.parent_comments_id=0 order by news_comments.modify_date asc  limit 0,100 ");
                while ($data_comment = $qcomment->fetch_assoc()) {
                  $comments_id = $data_comment['comments_id'];
                  ?>
                  <div class="facebook-card-comments">
                    <span style="float: left; ">
                      <img onerror="this.src='img/user.png'" class="imgRedonda1" src="../img/users/recident_profile/<?php echo $data_comment['user_profile_pic']; ?>" width="10%">
                    </span>
                    <span style="float: right;">
                      <i title="Delete Comment" class="text-danger fa fa-trash-o " data-toggle="modal" data-target="#editFloor" onclick="deleteComment('<?php echo $comments_id; ?>')"></i>
                    </span>
                    <p style="word-wrap: break-word;">
                      <a style="padding-left: 5px !important;" href="employeeDetails?id=<?php echo $data_comment['user_id']; ?>" target="_blank" class="profileName1 text-primary"> <?php echo $data_comment['user_name']; ?></a><span class="pull-right"><?php echo time_elapsed_string($data_comment['modify_date']); ?></span> <br><?php echo $data_comment['msg']; ?>
                    </p>
                  
                  </div>
                  <!-- sub comment -->
                  <?php $subComment = $d->selectRow("comments_id,user_profile_pic,news_comments.user_id,news_comments.user_name,news_comments.modify_date,news_comments.msg", "news_comments LEFT JOIN users_master ON users_master.user_id = news_comments.user_id LEFT JOIN bms_admin_master ON bms_admin_master.admin_id = news_comments.user_id", "news_comments.feed_id='$feed_id'  AND news_comments.parent_comments_id='$comments_id' order by news_comments.modify_date asc  limit 0,100 ");
                  while ($data_Sub_comment = $subComment->fetch_assoc()) {
                    $comments_id = $data_Sub_comment['comments_id']; ?>
                    <div class="facebook-card-comments" style="margin-left: 46px;">
                      <span style="float: left; ">
                        <img onerror="this.src='img/user.png'" class="imgRedonda1" src="../img/users/recident_profile/<?php echo $data_Sub_comment['user_profile_pic']; ?>" width="10%">
                      </span>

                      <span style="float: right;">
                        <i title="Delete Comment" class="text-danger fa fa-trash-o " data-toggle="modal" data-target="#editFloor" onclick="deleteComment('<?php echo $comments_id; ?>')"></i>
                      </span>
                      <p style="word-wrap: break-word; ">
                        <a style="padding-left: 5px !important;" href="employeeDetails?id=<?php echo $data_Sub_comment['user_id']; ?>" target="_blank" class="profileName1 text-primary"> <?php echo $data_Sub_comment['user_name']; ?></a><span class="pull-right"><?php echo time_elapsed_string($data_Sub_comment['modify_date']); ?></span> <br>
                      </p>
                      <p style="word-wrap: break-word; ">
                        <?php echo $data_Sub_comment['msg']; ?>
                      </p>
                      <br><br>
                    </div>

                    <!-- <div></div> -->
              <?php }
                }
              }*/ ?>
            </div>
          </div>
          <?php // IS_387
          ?>
        </div>
      <?php }
    } else if($offset==0){ ?>
      <div class="text-center m-5">
        <img width="270" src='img/no_data_found.png'>
      </div>
    <?php }
}
if(isset($getComment) && $getComment=="yes"){
  $qcomment = $d->selectRow("news_comments.*,users_master.user_profile_pic,users_master.user_designation", "news_comments LEFT JOIN users_master ON users_master.user_id = news_comments.user_id LEFT JOIN bms_admin_master ON bms_admin_master.admin_id = news_comments.user_id ", "news_comments.feed_id='$feed_id' AND news_comments.parent_comments_id=0 order by news_comments.modify_date asc  limit 0,100 ");
  while ($data_comment = $qcomment->fetch_assoc()) {
    $comments_id = $data_comment['comments_id'];
    ?>
    <div class="facebook-card-comments">
      <span style="float: left; ">
        <img onerror="this.src='img/user.png'" class="imgRedonda1" src="../img/users/recident_profile/<?php echo $data_comment['user_profile_pic']; ?>" width="10%">
      </span>
      <span style="float: right;">
        <i title="Delete Comment" class="text-danger fa fa-trash-o " data-toggle="modal" data-target="#editFloor" onclick="deleteComment('<?php echo $comments_id; ?>')"></i>
      </span>
      <p style="word-wrap: break-word;">
        <a style="padding-left: 5px !important;" href="employeeDetails?id=<?php echo $data_comment['user_id']; ?>" target="_blank" class="profileName1 text-primary"> <?php echo $data_comment['user_name']."(".$data_comment['user_designation'].")"; ?></a><span class="pull-right"><?php echo time_elapsed_string($data_comment['modify_date']); ?></span> <br><?php echo $data_comment['msg']; ?>
      </p>
    
    </div>
    <!-- sub comment -->
    <?php $subComment = $d->selectRow("comments_id,user_profile_pic,news_comments.user_id,news_comments.user_name,news_comments.modify_date,news_comments.msg,users_master.user_designation", "news_comments LEFT JOIN users_master ON users_master.user_id = news_comments.user_id LEFT JOIN bms_admin_master ON bms_admin_master.admin_id = news_comments.user_id", "news_comments.feed_id='$feed_id'  AND news_comments.parent_comments_id='$comments_id' order by news_comments.modify_date asc  limit 0,100 ");
    while ($data_Sub_comment = $subComment->fetch_assoc()) {
      $comments_id = $data_Sub_comment['comments_id']; ?>
      <div class="facebook-card-comments" style="margin-left: 46px;">
        <span style="float: left; ">
          <img onerror="this.src='img/user.png'" class="imgRedonda1" src="../img/users/recident_profile/<?php echo $data_Sub_comment['user_profile_pic']; ?>" width="10%">
        </span>

        <span style="float: right;">
          <i title="Delete Comment" class="text-danger fa fa-trash-o " data-toggle="modal" data-target="#editFloor" onclick="deleteComment('<?php echo $comments_id; ?>')"></i>
        </span>
        <p style="word-wrap: break-word; ">
          <a style="padding-left: 5px !important;" href="employeeDetails?id=<?php echo $data_Sub_comment['user_id']; ?>" target="_blank" class="profileName1 text-primary"> <?php echo $data_Sub_comment['user_name']."(".$data_Sub_comment['user_designation'].")"; ?></a><span class="pull-right"><?php echo time_elapsed_string($data_Sub_comment['modify_date']); ?></span> <br>
        </p>
        <p style="word-wrap: break-word; ">
          <?php echo $data_Sub_comment['msg']; ?>
        </p>
        <br><br>
      </div>

      <!-- <div></div> -->
<?php }
  }

}
?>
