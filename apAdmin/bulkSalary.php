<?php
extract(array_map("test_input", $_GET));
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));

$uId = (int) $_REQUEST['uId'];
$dId = (int) $_REQUEST['dId'];
$bId = (int) $_REQUEST['bId'];
$mnth = (int) $_REQUEST['month_year'];

  if (isset($_GET['laYear'])) {
    $laYear = $_GET['laYear'];
    $month_year = $_GET['month_year'];
    $startDate = date("$_GET[laYear]-$_GET[month_year]-01");
    $month_end_date = date("Y-m-t", strtotime($startDate));
    $salary_month_name = $_GET['month_year'] . "-" . $_GET['laYear'];
  } else {
    $laYear = $currentYear;
    $month_year = date('n',strtotime('last month'));
    $yearData  = date('n',strtotime('last month')) . "-" . $currentYear;
    $startDate  = $currentYear ."-". date('n',strtotime('last month')).'-01';
    $month_end_date = date("Y-m-t", strtotime($startDate));
    $salary_month_name = $yearData;
  }
  
  $crDate = date('Y-m-d');

  if (isset($bId) && $bId > 0) {
      $brnchFilterQuerySalary = " AND block_id='$bId'";
    }
    if (isset($dId) && $dId > 0) {
      $deptFilterQuerySalary = " AND floor_id='$dId'";
    }

  $q2 = $d->select('salary_slip_master', "salary_month_name= '$salary_month_name' $brnchFilterQuerySalary $deptFilterQuerySalary");
  $userIds = array();
  while ($data2 = mysqli_fetch_array($q2)) {
    array_push($userIds, $data2['user_id']);
    $dateAr[$data2['user_id']] = $data2['created_date'];
    //array_push($,$data2['user_id']);
  }
  // $userIds = join("','",$userIds); 
  $i = 1;


?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-8">
        <h4 class="page-title">Create Generate Bulk Salary </h4>
      </div>
      <div class="col-sm-4">
        <a href="#" class="btn btn-sm btn-warning float-right" data-toggle="modal" data-target="#bulkUpload">Add Past Date Salary</a>
      </div>
    </div>

    <form action="" method="get" class="branchDeptFilter">
      <div class="row pb-2">
        <?php include('selectBranchDeptForFilterAll.php'); ?>

        <div class="col-md-2 col-6">
          <select name="laYear" id="year"  onchange="yearChange(this.value)" class="form-control single-select">
            <option value="">Year</option>
            <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $onePreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $currentYear) {
                      echo 'selected';
                    } ?> <?php if ($_GET['laYear'] == '') {
                            echo 'selected';
                          } ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
            
          </select>
        </div>
        <div class="col-md-2 col-6">
          <select class="form-control month_year single-select" id="month" name="month_year">
            <option value="">-- Select --</option>
            <?php
            $selected = "";
            $crMnt = date('m');
            if(isset($laYear) && $laYear !=""){
              if($laYear==date('Y')){
                 $crMnt = date('m');
                // $cntMonth = $cntMonth;
              }else if($laYear<date('Y')){
                 $crMnt = 13;
              }
           }else{
              $crMnt = date('m');
           }
            for ($m = 1; $m < $crMnt; $m++) {
              $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
              $days = cal_days_in_month(CAL_GREGORIAN, $m, date('Y'));
              // if (isset($_GET['month_year'])  && $_GET['month_year'] != "") {
              //   echo '$m';
              //   echo $m;
              //   echo $_GET['month_year'];
              //   if ($_GET['month_year'] == $m) {
              //     $selected = "selected";
              //   } else {
              //     $selected = "";
              //   }
              // } else {
              //   $selected = "";
              //   $k = date('n') - 1;
              //   if ($m == $k) {
              //     $selected = "selected";
              //   } else {
              //     $selected = "";
              //   }
              // }
             
               
               
            ?>
              <option <?php if(isset($_GET['month_year']) && $_GET['month_year']== $m || $m==(int)$_GET['month_year']){ echo "selected"; }; ?> data-id="<?php echo $days; ?>" value="<?php echo $m; ?>"><?php echo $month; ?></option>
            <?php } ?>
          </select>
        </div>
        <input type="hidden" name="">
        <div class="col-lg-1 from-group col-6">
          <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get">
        </div>
      </div>

    </form>
    <!-- End Breadcrumb-->
    <input type="hidden" name="floor_id_old" id="floor_id_old" value="<?php if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                                                                        echo $_GET['dId'];
                                                                      } ?>">

    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">
            <?php
            $salaryMonthSatartDate = date('Y-m-d', strtotime('01-'.$salary_month_name));
            if (isset($bId) && $bId > 0) {
              $brnchFilterQuery = " AND users_master.block_id='$bId'";
            }
            if (isset($dId) && $dId > 0) {
              $deptFilterQuery = " AND users_master.floor_id='$dId'";
            }

            if (isset($mnth) && ($mnth > 0) && ($laYear > 0)) {
              $yearData  = $mnth . "-" . $laYear;
              $yearDataQuery = " AND salary_slip_master.salary_month_name='$yearData'";
            } else {
              $yearData  = date('n',strtotime('last month')) . "-" . $currentYear;
              $yearDataQuery = " AND salary_slip_master.salary_month_name='$yearData'";
            }

            $q = $d->selectRow("block_master.block_name,floors_master.floor_name,user_employment_details.joining_date,users_master.*,salary.salary_id,(SELECT COUNT(*) FROM attendance_master WHERE users_master.user_id = attendance_master.user_id AND user_status!=0 AND attendance_date_start BETWEEN '$startDate' AND '$month_end_date' AND attendance_status=1) AS total_attendace","block_master,floors_master,user_employment_details,users_master,salary ", " salary.user_id = users_master.user_id AND salary.is_preivous_salary=0 AND salary.gross_salary>0 AND salary.is_delete=0 AND user_employment_details.user_id = users_master.user_id  AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.society_id='$society_id' AND users_master.user_status=1   AND  (users_master.delete_status=0  AND users_master.register_date<='$month_end_date' OR  users_master.register_date<='$month_end_date' AND users_master.delete_status=1 AND users_master.deleted_date>='$startDate')
            $brnchFilterQuery  $deptFilterQuery  $blockAppendQueryUser", "ORDER BY users_master.user_full_name ASC LIMIT 5000");
            
            $count = mysqli_num_rows($q);
              if ($count > 0) { ?>
                <div class="col-md-4 gneClass float-left">
                  <label> Select All</label>
                  <input type="checkbox" name="" class="selectAll ml-2" value="">
                </div>
                <div class="col-md-6 gneClass float-left">
                  Note: Only Default Salary configured employees appear here
                </div>
                <div class="col-md-2 gneClass float-left">
                  <button onclick="OpenStatusModal()" class="btn btn-sm btn-primary float-right mb-2" > Generate Bulk Salary</button>
                </div>
            <?php }
             ?>

            <div class="table-responsive">
              
                <table id="bulkSalary" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Status</th>
                      <th>Employee</th>
                      <th>designation</th>
                      <th>Department</th>
                      <th>Month</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    
                    $finalData = array();
                    while ($data = mysqli_fetch_array($q)) {
                      if ($data['salary_id'] != "") {
                        if ($data['joining_date']!='' && $data['joining_date']!='0000-00-00') {
                          $employeeJoinDate = date('Y-m-01', strtotime($data['joining_date']));
                        } else {
                          $employeeJoinDate ="";
                        }
                        $employeeJoinDate = strtotime($employeeJoinDate);
                        $salaryMonthSatartDateStr = strtotime($salaryMonthSatartDate);
                      }
                    ?>
                      <tr>
                        <td>
                          <?php
                          if ($data['salary_id'] != "" && !in_array($data['user_id'], $userIds) && $data['total_attendace']>0) {
                          ?>
                              <input id="Chekbox_<?php echo $data['user_id']; ?>" type="checkbox" data-id="<?php echo $data['user_id']; ?>" name="bulkSLryCkbx[]" value="<?php echo $data['user_id']; ?>" class="blkChkcls">
                          <?php  } ?>
                        </td>
                         <td>
                          <?php
                          if ($data['salary_id'] != "") {
                            if (isset($userIds) &&  in_array($data['user_id'], $userIds)) {
                          ?>
                              <span class="badge badge-success">Generated ( <?php echo date('Y-m-d', strtotime($dateAr[$data['user_id']])); ?>)</span>
                            <?php } else if($data['total_attendace']==0) { ?>
                                <span class="badge badge-danger">Attendance Not Found</span>
                            <?php  }else  { ?>
                              <span class="badge badge-success" id="processKey_<?php echo $data['user_id']; ?>"></span>
                            <?php }
                          } else if($data['salary_id'] == "") { ?>
                            <span class="badge badge-danger">Salary Not Set</span>
                          <?php }  ?>
                        </td>
                        <td><?php echo $data['user_full_name'] ; if ($data['delete_status']==1) {
                          echo " <span class='badge badge-warning'>Ex Employee ($data[deleted_date])</span>";
                        }
                         ?></td>
                          <td><?php echo $data['user_designation'];
                         ?></td>
                         <td><?php echo $data['floor_name'] .'-'.$data['block_name']; ?></td>
                         <td><?php echo  date("M", strtotime($startDate)) . "-" . $laYear; ?></td>
                       
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              

            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->
  </div>
</div>

<div class="modal fade" id="bulksalaryModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Salary Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="bulkModalForm" method="post">

        <div class="modal-body" style="align-content: center;">
          <div class="form-group row">
            <label for="input-10" class="col-sm-6 col-form-label">Salary Mode </label>
            <div class="col-lg-6 col-md-6" id="">
              <select class="form-control " required name="salary_mode" id="salary_mode">
                <option value="">-- Select --</option>
                <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_mode'] == 0) {
                          echo "selected";
                        } ?> value="0">Bank Transfer</option>
                <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_mode'] == 1) {
                          echo "selected";
                        } ?> value="1">Cash</option>
                <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_mode'] == 2) {
                          echo "selected";
                        } ?> value="2">Cheque</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-6 col-form-label">Status Action </label>
            <div class="col-lg-6 col-md-6" id="">
              <?php
              $checkMenu = $d->selectRow('master_menu.*','master_menu',"menu_link='salaryChecked'");
              $getMenu = mysqli_fetch_assoc($checkMenu);
              $checkMenu2 = $d->selectRow('master_menu.*','master_menu',"menu_link='salaryPublished'");
              $getMenu2 = mysqli_fetch_assoc($checkMenu2);
              ?>
              <select class="form-control " required name="status_action" id="status_action" onclick="changeStatusAction(this.value)">
                <option value="">-- Select --</option>
                <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_mode'] == 0) {
                          echo "selected";
                        } ?> value="0">Generate</option>
                <?php
                     if($getMenu){
                     if(in_array($getMenu['menu_id'],$accessMenuIdArr)){ ?>
                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_slip_status'] == 1) {
                                 echo "selected";
                                 } ?> value="1"> Generate & Check</option>
                     <?php } } ?>
                     <?php 
                     if($getMenu2){
                     if(in_array($getMenu2['menu_id'],$accessMenuIdArr)){ ?>
                        <option <?php if (isset($salary_slip_data) && $salary_slip_data['salary_slip_status'] == 2) {
                                 echo "selected";
                                 } ?> value="2">Generate & Publish</option>
                     <?php } } ?>
              </select>
            </div>
          </div>
          <div class="sharWUser">
            <div class="form-group row ">
              <label for="input-10" class="col-sm-6 col-form-label">Share With User </label>
              <div class="col-lg-6 col-md-6" id="">
                <input type="radio" checked name="share_with_user" class="mr-2" value="1"><label> Yes</label>
                <input type="radio" name="share_with_user" class="mr-2" value="0"><label> No</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
             <label for="input-10" class="col-sm-6 col-form-label">Other Earning/ Deduction </label>
              <div class="col-lg-6 col-md-6" id="">
                <input type="checkbox" id="Reimbursement-checkbox" name="reimbursement_clear" value="1"> <label for="Reimbursement-checkbox">Settlement Reimbursement </label><br>
                <input type="checkbox" id="loan_emi_clear-checkbox" name="loan_emi_clear" value="1"> <label for="loan_emi_clear-checkbox">Deduct Loan E.M.I </label><br>
                <input type="checkbox" id="advance-salary-checkbox" name="advance_salary_clear" value="1"> <label for="advance-salary-checkbox">Deduct Advance Salary </label><br>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="col-md-12 text-center">
            <button type="button" class="btn btn-primary bulkModal">Save changes</button>
          </div>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="modal fade" id="bulkUpload">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Import Bulk Salary Slip</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="controller/BulkSalaryController.php" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 1 -> <?php echo $xml->string->formatted_csv; ?> <a href="controller/BulkSalaryController.php?ExportSalaryFormat=ExportSalaryFormat&&csrf=<?php echo $_SESSION["token"];?>" name="ExportUsers" value="ExportUsers" class="btn btn-sm btn-primary"><i class="fa fa-check-square-o"></i> Download</a></label>
            <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 2 -> <?php echo $xml->string->fill_your_data; ?> </label>
            <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 3 -> <?php echo $xml->string->import_file; ?></label>
            <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 4 -> <?php echo $xml->string->click_upload_btn; ?></label>
            <label class="col-sm-12 col-form-label text-danger">Note : Enter Month in digit format between 01 to 12</label>
            <label class="col-sm-12 col-form-label text-danger">Note : Enter year in digit format (YYYY)</label>
            <label class="col-sm-12 col-form-label text-danger">Note : Don't change column headers from CSV file</label>
            <label class="col-sm-12 col-form-label text-danger">Note : Salary Mode-> 0 = Bank Transfer, 1 = Cash, 2 = Cheque</label>
          </div>
        </form>
        <form id="importValidation" action="controller/BulkSalaryController.php" method="post" enctype="multipart/form-data">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label"><?php echo $xml->string->import; ?> CSV <?php echo $xml->string->file; ?> <span class="required">*</span></label>
            <div class="col-sm-8" id="PaybleAmount">
              <input required="" type="file" name="file" accept=".csv" class="form-control-file border">
            </div>
          </div>
          <div class="form-footer text-center">
            <input type="hidden" name="importBulkSalary" value="importBulkSalary">
            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->upload; ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>
<script>
  function OpenStatusModal() {

    var oTable = $("#bulkSalary").dataTable();
      var val = [];
            $(".blkChkcls:checked", oTable.fnGetNodes()).each(function(i) {
              val[i] = $(this).val();
            });
      if(val=="") {
        swal(
          'Warning !',
          'Please select at least 1 employee !',
          'warning'
        );
      } else {
        $('#bulksalaryModal').modal("show");
      }
  }

  $('.selectAll').click(function() {

    if ($(this).prop("checked") == true) {
      $('.blkChkcls').prop('checked', true);
    } else if ($(this).prop("checked") == false) {
      $('.blkChkcls').prop('checked', false);
    }
  });

  cnt = $('.blkChkcls').length;
  if (cnt <= 0) {
    $('.gneClass').hide();
  }

  function changeStatusAction(value) {
    ///sharWUser
    if (value == 2) {
      $('.sharWUser').show();
    } else {
      $('.sharWUser').hide();
    }

  }

  /* function generateSalary() {
    var val = [];
    $('#bulksalaryModal').modal();
  } */
  $('.bulkModal').click(function(e) {
      var reimbursement;
      var loan_emi;
      var advance_salary;
      salary_mode = $('#salary_mode').val();
      status_action = $('#status_action').val();
      
      if ($('#loan_emi_clear-checkbox').is(":checked"))
      {
        loan_emi = "1";
      } else {
        loan_emi = "0";
      }
      if ($('#Reimbursement-checkbox').is(":checked"))
      {
        reimbursement ="1";
      } else {
        reimbursement ="0";
      }
      if ($('#advance-salary-checkbox').is(":checked"))
      {
        advance_salary = "1";
      } else {
        advance_salary = "0";
      }

      
      if (status_action == 2) {
        share_with_user = $('input[name="share_with_user"]:checked').val();
      } else {
        share_with_user = 0
      }
      if (salary_mode != "" && status_action != "") {
        $('#bulksalaryModal').modal("hide");
        var arr = $('.blkChkcls:checked').map(function() {
          var user_id = this.value;
          salary_month = '<?php echo $month_year; ?>';
          salary_year = '<?php echo $laYear; ?>';
          $(".ajax-loader").show();
          var res = syncFUnction(user_id, salary_month, salary_year, salary_mode, status_action, share_with_user,loan_emi,reimbursement,advance_salary);
         
          if(res!=""){
            $('#processKey_' + user_id).text(res);
            $("#Chekbox_" + user_id).attr("disabled", true);
          }
        }).get();
        
      } else {
        swal("Please select salary mode & status!");
      }
    })
  function syncFUnction(user_id, salary_month, salary_year, salary_mode, status_action, share_with_user,loan_emi,reimbursement,advance_salary) {
    msg= "";
    var m = 1;
    if (m == 1) {
      m = 0;
      $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
         async: false,
        data: {
          action: "bulkSalary",
          user_id: user_id,
          salary_month: salary_month,
          salary_year: salary_year,
          status_action: status_action,
          share_with_user: share_with_user,
          loan_emi: loan_emi,
          reimbursement: reimbursement,
          advance_salary: advance_salary,
          salary_mode: salary_mode,
          bms_admin_id: '<?php echo $_COOKIE['bms_admin_id']; ?>',
          society_id: '<?php echo $society_id; ?>',
        },
        success: function(response) {
          $(".ajax-loader").hide();
          m = 1;
          
          msg = response.message;
          if (response.status == '200') {
           // $('#processKey_' + user_id).text('Generated');
          }
          if (response.status == '201') {
          //  $('#processKey_' + user_id).text(response.message);
          //  $('#processKey_' + user_id).removeClass('badge-success');
          //  $('#processKey_' + user_id).addClass('badge-warning');
          }

        }
        
      });
      return msg; 
    }
  }


  function yearChange(yearValue){
        // alert("ldfj");
         // yearValue = $(this).val();
          crntYear = '<?php echo date('Y');?>';
          crntM = '<?php echo date('m');?>';
          monthHtml = "";
         var  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
         var d = new Date();
         var monthName=months[d.getMonth()];
         console.log(monthName)
         if(crntYear==yearValue)
         {

            for (let index = 0; index < ((months.length)); index++) {
               const element = months[index];
               console.log(index);
               console.log(crntM);
            //   crntM = 2;
               if(index<crntM)
               {
                  monthHtml += `<option value="`+(index+1)+`" >`+element+`</option>`;
               }
            }
            $('#month').html(monthHtml);
         }
         else
         {
            for (let index = 0; index < ((months.length)); index++) {
               const element = months[index];
               monthHtml += `<option value="`+(index+1)+`" >`+element+`</option>`;
            }
            $('#month').html(monthHtml);
         }
      };
</script>
<style>
  .sharWUser {
    display: none;
  }
</style>