<?php 
extract($_POST); 
session_start();
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
include 'common/checkLanguage.php';
  $data = $d->selectArray("expense_category_master","expense_category_id='$expense_category_id' ");

  $totalUsed=  $d->count_data_direct("expense_category_id","expenses_balance_sheet_master","expense_category_id='$expense_category_id' "); 
?>

<form id="editComplainCategoryValidation" method="POST" action="controller/balancesheetController.php" enctype="multipart/form-data">
  <input type="hidden" name="expense_category_id" value="<?php echo $_POST['expense_category_id'] ?>">
  <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
  <div class="row form-group">
    <label class="col-sm-3 form-control-label">Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
      <input type="text" maxlength="200" class="form-control" autocomplete="off"  required="" name="expense_category_name" value="<?php echo $data['expense_category_name'] ?>">
    </div>
  </div>
  <div class="row form-group">
    <label class="col-sm-3 form-control-label"><?php echo $xml->string->category;?> <span class="text-danger">*</span></label>
     <div class="col-sm-9">
      <select <?php if($totalUsed>0) { echo 'disabled'; } ?> class="form-control" maxlength="200" autocomplete="off" required="" name="category_type">
        <option value=""><?php echo $xml->string->select;?> </option>
        <option value="0" <?php if($data['category_type']==0) { echo 'selected'; } ?>><?php echo $xml->string->expense;?> </option>
        <option value="1" <?php if($data['category_type']==1) { echo 'selected'; } ?>><?php echo $xml->string->income;?> </option>
      </select>
    </div>
  </div>
  <div class="form-footer text-center">
    <?php if($totalUsed>0) { ?>
      <input type="hidden" name="category_type" value="<?php echo $category_type; ?>">
    <?php } ?>
    <input type="hidden" name="addExpenseCategory">
    <button class="btn btn-sm btn-success"><i class="fa fa-check"></i> Submit</button>
  </div>
</form>

<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$().ready(function() {
  $.validator.addMethod("noSpace", function(value, element) { 
        return value == '' || value.trim().length != 0;  
    }, "No space please and don't leave it empty");

   $.validator.addMethod("alphaRestSpeChartor", function(value, element) {
        return this.optional(element) || value == value.match(/^[A-Za-z 0-9\d=!,\n\-@&()/?%._*]*$/);
    }, jQuery.validator.format("special characters not allowed"));


  $("#editComplainCategoryValidation").validate({
    rules: {
      expense_category_name: {
          required: true,
          maxlength: 200,
          noSpace: true,
      },
    },
  });
});
</script>