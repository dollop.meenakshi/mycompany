<?php error_reporting(0);
  $cId = (int)$_REQUEST['cId'];
  $scId = (int)$_REQUEST['scId'];
  $vId = (int)$_REQUEST['vId'];
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-6 col-12">
          <h4 class="page-title">Manage Product Prices</h4>
        </div>
        <div class="col-sm-6 col-12">
          <div class="btn-group float-sm-right">
            <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <a href="addProductPrice" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>

            <a href="javascript:void(0)" onclick="DeleteAll('deleteProductPrice');" class="btn  btn-sm btn-danger pull-right  mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#bulkUpload2" class="btn  mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Bulk Add </a>
          </div>
        </div>
      </div>
      <form action="" class="productFltr">
        <div class="row pt-2 pb-2">
          <!--  <div class="col-lg-3 col-6">

            <select type="text" class="form-control single-select "  id="vId" name="vId">
              <option value="">Select Vendor</option>
              <?php
              $floor = $d->select("local_service_provider_users", " service_provider_status=0 AND service_provider_delete_status=0 AND society_id = $society_id");
              while ($floorData = mysqli_fetch_array($floor)) {
              ?>
                <option <?php if (isset($vId) && $vId == $floorData['service_provider_users_id']) {
                          echo "selected";
                        } ?> value="<?php if (isset($floorData['service_provider_users_id']) && $floorData['service_provider_users_id'] != "") {
                                      echo $floorData['service_provider_users_id'];
                                    } ?>"><?php if (isset($floorData['service_provider_name']) && $floorData['service_provider_name'] != "") {
                                                                                                                                        echo $floorData['service_provider_name'];
                                                                                                                                      } ?></option>
              <?php } ?>
            </select>
          </div> -->
          <div class="col-lg-3 col-6 ">
            <select name="cId" class="form-control product_category_id single-select" onchange="getSubCategoryByCatId(this.value)">
              <option value="">--Select Category--</option>
              <?php
              $qd = $d->select("product_category_master", "society_id='$society_id' AND  product_category_status = 0 AND product_category_delete = 0 ");
              while ($depaData = mysqli_fetch_array($qd)) {
              ?>
                <option <?php if ($cId == $depaData['product_category_id']) {
                          echo 'selected';
                        } ?> value="<?php echo  $depaData['product_category_id']; ?>"><?php echo $depaData['category_name']; ?></option>
              <?php } ?>

            </select>
          </div>
          <div class="col-lg-3 col-6 ">
            <select name="scId" id="" class="form-control single-select product_sub_category_id">
              <option value="">--Select Sub Category---</option>
              <?php if (isset($cId) && $cId > 0) {
                $qd = $d->select("product_sub_category_master", "society_id='$society_id' AND product_sub_category_delete = 0 AND product_sub_category_status = 0 AND product_category_id= $cId");
                while ($depaData = mysqli_fetch_array($qd)) {
              ?>
              <option <?php if ($scId == $depaData['product_sub_category_id']) {echo 'selected';
                          } ?> value="<?php echo  $depaData['product_sub_category_id']; ?>"><?php echo $depaData['sub_category_name']; ?></option>
              <?php  }
              } ?>
            </select>
          </div>
          <div class="col-md-1">
            <button class="btn btn-primary btn-sm">Get Data</button>
          </div>
        </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl
                if (isset($cId) && $cId != "") {
                ?>
                  <table id="example" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Vendor</th>
                        <th>Product </th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>variant</th>
                        <th>Product price</th>
                        <th>minimum order quantity</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 1;
                      if(isset($scId) && $scId>0){
                          $Subfilter="AND product_sub_category_master.product_sub_category_id=$scId";
                      }
                      
                      $q = $d->selectRow("product_price_master.*,product_master.*,product_variant_master.product_variant_name,product_sub_category_master.sub_category_name,product_category_master.*,local_service_provider_users.service_provider_name,local_service_provider_users.service_provider_users_id","product_price_master LEFT JOIN local_service_provider_users ON local_service_provider_users.service_provider_users_id= product_price_master.vendor_id LEFT JOIN product_variant_master ON product_variant_master.product_variant_id=product_price_master.product_variant_id,product_master LEFT JOIN product_sub_category_master ON product_sub_category_master.product_sub_category_id=product_master.product_sub_category_id ,product_category_master","product_master.product_id=product_price_master.product_id AND product_category_master.product_category_id=product_master.product_category_id AND product_category_master.product_category_id='$cId'  AND product_price_master.product_price_delete_status=0  $Subfilter");
                      $counter = 1;
                      while ($data = mysqli_fetch_array($q)) {
                      ?>
                        <tr>
                          <td class="text-center">

                            <input type="hidden" name="id" id="id" value="<?php echo $data['product_price_id']; ?>">
                            <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['product_price_id']; ?>">
                          </td>
                          <td><?php echo $counter++; ?></td>
                          <td><?php echo $data['service_provider_name']; ?></td>
                          <td><?php echo $data['product_name']; ?></td>
                          <td><?php echo $data['category_name']; ?></td>
                          <td><?php echo $data['sub_category_name']; ?></td>
                          <td><?php echo $data['product_variant_name']; ?></td>
                          <td><?php echo $data['product_price']; ?></td>
                          <td><?php echo $data['minimum_order_quantity']; ?></td>
                          <td>
                            <div class="d-flex align-items-center">
                              <form action="addProductPrice" method="post" accept-charset="utf-8" class="mr-2">
                                <input type="hidden" name="product_price_id" value="<?php echo $data['product_price_id']; ?>">
                                <input type="hidden" name="edit_product" value="edit_product">
                                <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button>
                              </form>
                             <!--  <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="productDetailModal(<?php echo $data['product_id']; ?>)"> <i class="fa fa-eye"></i></button> -->
                              <a href="javascript:void(0)" onclick="getProductPrice(<?php echo $data['product_price_id'] ?>)"  class="btn btn-sm btn-primary mr-2" > <i class="fa fa-eye"></i></a>
                              <?php if ($data['product_active_status'] == "0") {
                                $status = "productPriceStatusDeactive";
                                $active = "checked";
                              } else {
                                $status = "productPriceStatusActive";
                                $active = "";
                              } ?>
                              <input type="checkbox" <?php echo $active; ?> class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['product_price_id']; ?>','<?php echo $status; ?>');" data-size="small" />
                            </div>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>

                  </table>
                <?php } else { ?>
                  <h4>Please Select Category !!!</h4>
                <?php   } ?>

              </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

  </div>
  <div class="modal fade" id="productPriceDetailModal">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Product Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body px-2 " style="align-content: center;">
          <div class="row mx-0" id="product_price_detail">

          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="bulkUpload2">
    <div class="modal-dialog">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white"><?php echo $xml->string->import; ?> <?php echo $xml->string->bulk; ?> <?php echo $xml->string->parking; ?></h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="" action="controller/bulkUploadController.php" method="post" enctype="multipart/form-data">
            <div class="form-group row">
              <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 1 -> <?php echo $xml->string->formatted_csv; ?> <a href="controller/bulkUploadController.php?ExportProductPrice=ExportProductPrice&&csrf=<?php echo $_SESSION["token"]; ?>" name="ExportProductPrice" value="ExportProductPrice" class="btn btn-sm btn-primary"><i class="fa fa-check-square-o"></i> Download</a></label>
              <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 2 -> <?php echo $xml->string->fill_your_data; ?> </label>
              <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 3 -> <?php echo $xml->string->import_file; ?></label>
              <label for="input-10" class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 4 -> <?php echo $xml->string->click_upload_btn; ?></label>
              <label for="input-10" class="col-sm-12 col-form-label text-danger"> NOTE: DON'T CHANGE COLUMN FROM CSV </label>
              <!-- <label for="input-10" class="col-sm-12 col-form-label text-danger"> <?php echo $xml->string->dont_change_parking_id; ?></label> -->
              <!--             <label for="input-10" class="col-sm-12 col-form-label text-danger"><?php echo $xml->string->dont_change_parking_type; ?></label>
 -->
            </div>

          </form>
          <form id="importValidation" action="controller/bulkUploadController.php" method="post" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label"><?php echo $xml->string->import; ?> CSV <?php echo $xml->string->file; ?> <span class="required">*</span></label>
              <div class="col-sm-8" id="PaybleAmount">
                <input required="" type="file" name="file" accept=".csv" class="form-control-file border">
              </div>
            </div>

            <div class="form-footer text-center">
              <input type="hidden" name="importProductPrices" value="importProductPrices">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->upload; ?></button>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>



  <div class="modal fade" id="addVerientModal">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Add Verient</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <form id="addProductVerientFrm" action="controller/productVerientController.php" method="post" enctype="multipart/form-data">
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Verient Name <span class="required">*</span></label>
              <div class="col-sm-8" id="">
                <input required="" type="text" name="product_variant_name"  class="form-control border">
              </div>
            </div>

            <div class="form-footer text-center">
              <input type="hidden" name="addProductVerient" value="addProductVerient">
              <input type="hidden" name="pr_id" id="pr_id">
              <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Add </button>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript">
    function popitup(url) {
      newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
      if (window.focus) {
        newwindow.focus()
      }
      return false;
    }

    function opneVerientModal(productId){
      $('#pr_id').val(productId);
      $('#addVerientModal').modal();
    }
  </script>