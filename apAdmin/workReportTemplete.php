<?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Work Report Templete</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
              <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
          
            <a href="javascript:void(0)" onclick="DeleteAll('deleteTemplete');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>                        
                        <th>Action</th>
                        <th>Name</th>
                        <th>Description</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("template_master","society_id='$society_id'");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <td class="text-center">
                            <?php  $totalQue = $d->count_data_direct("template_question_id","template_question_master","templete_id='$data[templete_id]'");
                              if( $totalQue==0) {
                          ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['templete_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['templete_id']; ?>">                      
                          <?php } ?>    
                        </td>
                        <td><?php echo $counter++; ?></td>
                        <td>
                       <div class="d-flex align-items-center">
                        <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="templeteDataSet(<?php echo $data['templete_id']; ?>)"> <i class="fa fa-pencil"></i></button>

                          <?php if($data['templete_status']=="0"){
                              $status = "templeteStatusDeactive";  
                              $active="checked";                     
                          } else { 
                              $status = "templeteStatusActive";
                              $active="";  
                          } ?>
                          <input type="checkbox" <?php echo $active; ?>  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['templete_id']; ?>','<?php echo $status; ?>');" data-size="small"/>

                          <a href="templeteQuestions?id=<?php echo $data['templete_id']; ?>&type=edit" class="btn btn-sm btn-warning ml-2">Manage</a>
                          <a href="templeteQuestions?id=<?php echo $data['templete_id']; ?>&type=view" class="btn btn-sm btn-info ml-2"><i class="fa fa-eye"></i></a>
                       </div>
                      </td>
                       <td><?php echo $data['templete_name']; ?></td>
                       <td><?php echo $data['templete_description']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Templete</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="addTempleteForm" action="controller/WorkReportTempleteController.php" enctype="multipart/form-data" method="post"> 
           <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                  <input type="text" class="form-control" placeholder="Templete Name" id="templete_name" name="templete_name" value="">
              </div>  
           </div>                                      
           <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Description</label>
              <div class="col-lg-8 col-md-8" id="">
                  <textarea type="text" class="form-control" placeholder="Templete Description" id="templete_description" name="templete_description" value=""></textarea>
              </div>  
           </div>                                      
           <div class="form-footer text-center">
             <input type="hidden" id="templete_id" name="templete_id" value="" >
             <button id="addTempleteBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addTemplete"  value="addTemplete">
             <button id="addTempleteBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>.
