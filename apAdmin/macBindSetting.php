<?php
error_reporting(0);
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$bind_mac_address = $sData['bind_mac_address'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-md-6">
                <h4 class="page-title">Mobile Device Bind Setting</h4>
            </div>
            <div class="col-md-6">
                 <li class="list-group-item d-flex justify-content-between align-items-center">
                      Default Setting : Bind Mac Address with mobile app
                      <span>
                         <?php  
                        if($bind_mac_address=="1"){
                        ?>
                          <input type="checkbox" checked=""  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $bind_mac_address; ?>','bindMacAddressOff');" data-size="small"/>
                          <?php } else { ?>
                         <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $bind_mac_address; ?>','bindMacAddressOn');" data-size="small"/>
                        <?php } ?>
                      </span>
                      </li>
            </div>
        </div>
        <form class="branchDeptFilter" action="">
            <div class="row pt-2 pb-2">
                <?php include 'selectBranchDeptForFilter.php' ?>
                <div class="col-md-1 form-group">
                    <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
                <?php
                if(isset($dId) && $dId > 0)
                {
                ?>
                <div class="col-md-5 text-right form-group">
                    <input class="btn btn-success btn-sm" type="button" name="getReport" onclick="openMultiModal();" value="Update Multiple">
                </div>
                <?php
                }
                ?>
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            if (isset($dId) && $dId > 0)
                            {
                                $deptFilterQuery = " AND users_master.floor_id='$dId'";
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>User Name</th>
                                        <th>Department</th>
                                        <th>Work Report </th>
                                    </tr>
                                </thead>
                                <tbody id="showFilterData">
                                    <?php
                                    if (isset($bId) && $bId != "")
                                    {
                                        $branchFilterQuery = " AND users_master.block_id='$bId'";
                                    }
                                    $q = $d->select("users_master,block_master,floors_master", "users_master.floor_id = floors_master.floor_id AND  block_master.block_id=floors_master.block_id AND users_master.society_id='$society_id' AND users_master.delete_status= 0 $deptFilterQuery $branchFilterQuery $blockAppendQuery");
                                    $counter = 1;
                                    while ($data = mysqli_fetch_array($q))
                                    {
                                    ?>
                                    <tr>
                                        <td class="text-center">
                                          <input type="checkbox" name="" class="multiUpdateCheckbox" value="<?php echo $data['user_id']; ?>">
                                        </td>
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['user_full_name']; ?></td>
                                        <td><?php echo $data['floor_name']; ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <form>
                                                    <select class="form-control single-select" name="mac_bind_on<?php echo $counter; ?>" id="mac_bind_on<?php echo $counter; ?>" onchange="updateWorkReport(this,'<?php echo $data['user_id']; ?>');">
                                                        <option value="0" <?php if($data['mac_bind_on'] == 0){ echo "selected"; } ?>>Default Setting</option>
                                                        <option value="1" <?php if($data['mac_bind_on'] == 1){ echo "selected"; } ?>>Not Bind</option>
                                                        
                                                    </select>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            else
                            {
                            ?>
                            <h4>Please Select Department !!!</h4>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<!--Start Back To Top Button-->
<div class="modal fade" id="exEmployeeDetailModel">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Ex-Employee Details</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="exEmployeeDetailModelDiv" style="align-content: center;">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="update_user_work_multi">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Update Multiple User Work Report</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-2" style="align-content: center;">
                <form>
                    <div class="row mx-0" id="shift_timing">
                        <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-5 col-md-4 col-form-label col-12">Work Report <span class="required">*</span></label>
                            <div class="col-lg-12 col-md-8 col-12" id="">
                                <select class="form-control single-select" name="mac_bind_on" id="mac_bind_on">
                                    <option value="">--Select--</option>
                                    <option value="0">Default Setting</option>
                                    <option value="1">Not Bind</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group  row w-100 mx-0 ">
                            <div class="col-md-12 text-center">
                                <button type="button" class="btn btn-sm btn-primary" onclick="UpdateMulti()">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
function updateWorkReport(id,user_id)
{
    mac_bind_on = id.value;
    $.ajax({
        url: "controller/MACAdressController.php",
        cache: false,
        type: "POST",
        data: {
            update_user_mac_seting:"update_user_mac_seting",
            user_id:user_id,
            mac_bind_on:mac_bind_on,
            csrf:csrf
        },
        success: function(response)
        {
            document.location.reload(true);
        }
    });
}

function openMultiModal()
{
    var oTable = $("#example").dataTable();
    var val = [];
    $(".multiUpdateCheckbox:checked", oTable.fnGetNodes()).each(function(i)
    {
        val[i] = $(this).val();
    });

    if(val == "")
    {
        swal(
            'Warning !',
            'Please Select at least 1 user !',
            'warning'
        );
    }
    else
    {
        $('#update_user_work_multi').modal();
    }
}

function UpdateMulti()
{
    mac_bind_on = $('#mac_bind_on').val();
    var oTable = $("#example").dataTable();
    var val = [];
    $(".multiUpdateCheckbox:checked", oTable.fnGetNodes()).each(function(i)
    {
        val[i] = $(this).val();
    });
    if(mac_bind_on == '' || mac_bind_on == null)
    {
        swal(
            'Warning !',
            'Please Select Work Report !',
            'warning'
        );
    }
    else
    {
        swal({
            title: "Are you sure?",
            text: "You want to update mac bind setting!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) =>
        {
            if (willDelete)
            {
                $.ajax({
                    url: "controller/MACAdressController.php",
                    cache: false,
                    type: "POST",
                    data: {
                        updateUsersDeviceBindMulti:"updateUsersDeviceBindMulti",
                        user_id:val,
                        mac_bind_on:mac_bind_on,
                        csrf:csrf
                    },
                    success: function(response)
                    {
                        document.location.reload(true);
                    }
                });
            }
            else
            {
                swal("data not updated!");
            }
        });
    }
}
</script>