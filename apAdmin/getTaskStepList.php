<?php 
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
extract(array_map("test_input" , $_POST));
$language_id = $_COOKIE['language_id'];
$xml=simplexml_load_file("../img/$language_id.xml");
$floorLngName = $xml->string->floor;

for ($i=1; $i <=$no_of_task_step ; $i++) { 
 ?>
<div class="col-md-3">
    <div class="form-group row w-100 mx-0">
        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Step Name <?php echo $i; ?> <span class="required">*</span></label>
        <div class="col-lg-12 col-md-12 col-12" id="">
            <input type="text" class="form-control task_step" placeholder="Step Name" id="task_step<?php echo $i; ?>" name="task_step[<?php echo $i; ?>]">
        </div>
    </div> 
</div>
<div class="col-md-3">
    <div class="form-group row w-100 mx-0">
        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Step Due Date <?php echo $i; ?> <span class="required">*</span></label>
        <div class="col-lg-12 col-md-12 col-12" id="">
            <input type="text" class="form-control task_step_due_date datepicker_task_step_due_date" readonly placeholder="Step Due Date" id="task_step_due_date<?php echo $i; ?>" name="task_step_due_date[<?php echo $i; ?>]">
        </div>
    </div> 
</div>
<div class="col-md-6">
    <div class="form-group row w-100 mx-0">
        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Step Note <?php echo $i; ?> </label>
        <div class="col-lg-12 col-md-12 col-12" id="">
            <textarea id="task_step_note<?php echo $i; ?>" placeholder="Step Note" name="task_step_note[<?php echo $i; ?>]" class="form-control task_step_note"></textarea>
        </div>
    </div> 
</div>

<?php }  ?>
<input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
<script src="assets/js/datepicker.js"></script>
<script type="text/javascript">
	$(".task_step").each(function () {
        $('#'+this.id).rules("add", {
            required: true,
            noSpace: true,
            messages: {
              required: "Please enter step name",
              noSpace: "No space please and don't leave it empty",
            }
        });
    });
    $(".task_step_due_date").each(function () {
        $('#'+this.id).rules("add", {
            required: true,
            messages: {
              required: "Please enter step due date",
            }
        });
    });
    $(".task_step_note").each(function () {
        $('#'+this.id).rules("add", {
            noSpace: true,
            maxlength: 1000,
            messages: {
              noSpace: "No space please and don't leave it empty",
            }
        });
    });
    
</script>