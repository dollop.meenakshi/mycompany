<?php error_reporting(0);
  $vId = (int)$_REQUEST['vId'];
  $pId = (int)$_REQUEST['pId'];
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Stationery Product Variant</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <a href="addStationaryProductVariant" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteStationaryProductVariant');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
      </div>
     </div>
      <form action="" method="get">
        <div class="row pt-2 pb-2">
          <div class="col-md-3 form-group">
            <select name="vId" class="form-control single-select" onchange="getVendorProductByVendorId(this.value);" required="">
                <option value="">--Select Vendor--</option> 
                <?php 
                  $qd=$d->select("vendor_master","society_id='$society_id' AND vendor_category_id=2 AND vendor_master_delete=0 AND vendor_active_status=0");  
                  while ($vendorData=mysqli_fetch_array($qd)) {
                ?>
                <option  <?php if($_GET['vId']==$vendorData['vendor_id']) { echo 'selected';} ?> value="<?php echo  $vendorData['vendor_id'];?>" ><?php echo $vendorData['vendor_name'];?></option>
                <?php } ?>
              </select>
          </div>  
          <div class="col-md-3 form-group">
            <select name="pId" id="pId" class="form-control single-select" >
                <option value="">All Product</option> 
                <?php 
                  $qd=$d->select("vendor_product_master","vendor_category_id=2 AND vendor_product_active_status=0 AND vendor_product_delete =0 AND vendor_id = '$_GET[vId]'");  
                  while ($productData=mysqli_fetch_array($qd)) {
                ?>
                <option  <?php if($_GET['pId']==$productData['vendor_product_id']) { echo 'selected';} ?> value="<?php echo  $productData['vendor_product_id'];?>" ><?php echo $productData['vendor_product_name'];?></option>
                <?php } ?>
              </select>
          </div>   
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
          </div>      
        </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                    <?php 
                    $i=1;
                                           
                     if(isset($_GET['vId']) && $_GET['vId']>0) {
                      $vendorFilterQuery = " AND vendor_master.vendor_id='$_GET[vId]'";
                     }

                     if(isset($_GET['pId']) && $_GET['pId']>0) {
                      $productFilterQuery = " AND vendor_product_master.vendor_product_id='$_GET[pId]'";
                     }
                     
                      $q=$d->select("vendor_product_variant_master,vendor_product_master,vendor_master","vendor_product_variant_master.vendor_id = vendor_master.vendor_id AND vendor_product_master.vendor_product_id = vendor_product_variant_master.vendor_product_id AND vendor_product_variant_master.vendor_category_id='2' AND vendor_product_variant_master.vendor_product_variant_delete= 0 $vendorFilterQuery $productFilterQuery");
                        $counter = 1;
                        if(isset($_GET['vId'])){
                     ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Vendor Name</th>
                        <th>Product Name</th>
                        <th>Product Variant Name</th>
                        <th>Price</th>
                        <th>Popular Product</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="showFilterData">
                   <?php while ($data=mysqli_fetch_array($q)) { ?>
                    <tr>
                      <td class="text-center">
                          <?php $totalVariant = $d->count_data_direct("vendor_cart_product_id","order_product_master","vendor_product_variant_id='$data[vendor_product_variant_id]'");
                            if( $totalVariant==0) {
                          ?>
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['vendor_product_variant_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['vendor_product_variant_id']; ?>">                      
                          <?php } ?>
                      </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['vendor_name']; ?></td>
                      <td><?php  echo $data['vendor_product_name']; ?></td>
                      <td><?php  echo $data['vendor_product_variant_name']; ?></td>
                      <td><?php  echo $data['vendor_product_variant_price']; ?></td>
                      <td>
                      <?php if($data['popular_product']=="1"){
                          ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_product_variant_id']; ?>','popularProductNo');" data-size="small"/>
                          <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_product_variant_id']; ?>','popularProductYes');" data-size="small"/>
                          <?php } ?>
                    </td>
                      <td>
                      <div class="d-flex align-items-center">
                          <form action="addStationaryProductVariant" method="post" accept-charset="utf-8" class="mr-2">
                              <input type="hidden" name="vendor_product_variant_id" value="<?php echo $data['vendor_product_variant_id']; ?>">
                              <input type="hidden" name="edit_stationary_product_variant" value="edit_stationary_product_variant">
                              <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button> 
                          </form>
                          <?php if($data['vendor_product_variant_active_status']=="0"){
                          ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_product_variant_id']; ?>','stationaryProductVariantDeactive');" data-size="small"/>
                          <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['vendor_product_variant_id']; ?>','stationaryProductVariantActive');" data-size="small"/>
                          <?php } ?>
                          <a href="productVariantDetail?id=<?php echo $data['vendor_product_variant_id']; ?>" class="btn btn-sm btn-primary ml-2" ><i class="fa fa-eye"></i></a>
                        </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
              <?php } else {  ?>
                <div class="" role="alert">
                  <span><strong>Note :</strong> Please Select Vendor</span>
                  </div>
              <?php } ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->

<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
.hideupdate{
  display:none;
}

</style>