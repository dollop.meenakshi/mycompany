<?php

 $message="
<html><link type='text/css' rel='stylesheet' id='dark-mode-general-link'><link type='text/css' rel='stylesheet' id='dark-mode-custom-link'><style type='text/css' id='dark-mode-custom-style'></style><head>
  <meta http-equiv='content-type' content='text/html; charset=windows-1252'>
        <style>
            div.container { width: 80%; padding-left: 10%; padding-top: 70px; padding-bottom: 70px;
            }
           
           
            @media only screen and (min-device-width: 320px) and (max-device-width: 480px) {
                div.container {
                    width: 90%;
                    padding-left: 4%;
                    padding-top: 30px;
                    padding-bottom: 30px;
                }
                
            }
           
          
        </style>
    </head>
    <body style='background-image: url(https://my-company.app/images/mailBg.jpg); background-repeat: no-repeat; background-size: cover;' cz-shortcut-listen='true'>
        <div class='container'>
            <div style='padding: 0px;color: #000;background-color: #fff; font-size: 20px;  border-radius: 10px;'>
                <center style='background: #fff;''>
                <a href='https://my-company.app/'><img src='https://master.my-company.app/img/logo.png'  style='height: 120px; padding-top: 12px;padding-bottom: 10px;'></a></center>


                <center><a href='https://my-company.app/'><img style='height:auto; width:100%; object-fit:fill;' src='https://my-company.app/images/companyBanner.png'></a></center>
                <h2 style='text-align: center;'>Hello <span style='color: red;'>$admin_name</span> !</h2>


               <div style='padding:20px;padding-top:0px'>
                <p style=' font-size: 18px;text-align: center;font-family: Poppins'>
                Welcome to MyCo! We're excited to provide you our $societyLngName management service, and hopefully 
                <br />
                you're excited too. Let come & enjoy the world of Digital
                 <br /><br />
                 Your $society_name $societyLngName Account ($role_name) has been created successfully. Use the below account 
                     <br />
                     credential to Login your Account.

                      <br />
                      <br />
                 <strong>Please change your account password after login.</strong><br />
                 <br />
                 <br />


              <div>
              <table style='margin-left: auto; 
              margin-right: auto;font-size: 18px;text-align: center;font-family: Poppins; background:#d2d2d2;color: black;width: 85%;border-radius: 10px;'>
              <tr>
              <td>
              Username: $country_code $admin_mobile
              <br />
               Password : $admin_password
              </td>
              </tr>
              </table>
                 </div>

                 <p style=' font-size: 18px;text-align: center;font-family: Poppins'>
                 <a style='width: 200px;background: red;color: white;text-decoration: none;padding: 8px;border-radius: 5px;' href='$forgotLink'>Login</a>
                 </p>
                 <p style=' font-size: 18px;text-align: center;font-family: Poppins'>
                  If you did not request for account create, please ignore this email or <a href='mailto:contact@my-company.app'> contact support</a> if you 
                 <br />
                  have questions.
                 <br />
                <br />
                If you’re having trouble with the button above, copy and paste the URL below into your web
                <br /> browser.
                <br />
                <br />
                <a style='color: white;' href='$forgotLink'>$forgotLink</a>
                </p>
                </p>
                 <br />
                 <p style='text-align:center;'>
                 Thank You,<br />
                The MyCo Team

                </p>
            
            </div>
            <p style=' font-size: 18px; text-align: center; background:#d2d2d2;padding-top:8px;padding-bottom:8px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px;color:#000 '>
                       &#169; 2021 MyCo. All rights reserved.
        </div>
    


</body>
</html>";

?>