<?php
  $message ="<html><link type='text/css' rel='stylesheet' id='dark-mode-general-link'><link type='text/css' rel='stylesheet' id='dark-mode-custom-link'><style type='text/css' id='dark-mode-custom-style'></style><head>
<meta http-equiv='content-type' content='text/html; charset=windows-1252'>
        <style>
        @media only screen and (min-device-width: 320px) and (max-device-width: 480px) {
                div.container {
                    width: 90%;
                    padding-left: 4%;
                    padding-top: 30px;
                    padding-bottom: 30px;
                }
        </style>
    </head>
    <body style='background-image: url(https://my-company.app/images/mailBg.jpg); background-repeat: no-repeat; background-size: cover;' cz-shortcut-listen='true'>
        <div class='container' style='width: 80%;padding-left: 10%;padding-top: 70px;padding-bottom: 70px;'>
            <div class='mainbody' style='padding: 0px;color: #000;background-color: #fff;font-size: 20px;border-radius: 10px;'>
                <center style='background-color:#fff;border-top-right-radius: 10px;
                border-top-left-radius: 10px'><img src='https://master.my-company.app/img/logo.png'  style='height: 120px; padding-top: 12px;padding-bottom: 12px;'></center>

                <center><img style='height:auto; width:100%; object-fit:fill;' src='https://my-company.app/images/companyBanner.png'></center>
                <h1 style='text-align: center;'>Hii <span style='color: red;'>$admin_name</span> ,</h1>

                <p style=' text-align: center;font-family: Poppins'>Please use the following OTP to login to the $society_name $app.
                </p>

                <p style=' font-size: 24px;text-align: center;font-family: Poppins'>Your OTP is :
                    <span style='color:#2f648e'>$msg</span>
                </p>

                <div style='padding:10px;'>
                <p style=' font-size: 24px;text-align: center;font-family: Poppins '>If you did not request for OTP, please ignore this email or<a href='mailto:contact@mycompany.app'> contact support</a> if you have questions.</p>

                <p style=' font-size: 24px;text-align: center;font-family: Poppins '>Thank You,<br />
                The MyCo Team</p>
                </div>
                    <p style=' font-size: 18px; text-align: center; background:#d2d2d2;padding-top:8px;padding-bottom:8px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px '>
                       &#169; 2021 MyCo. All rights reserved.
                 </p>
            </div>
        </div>
</body>
</html>";
?>