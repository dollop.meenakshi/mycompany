<?php 

 $message = "
       <!doctype html>
<html> 
<head>
<title>INVOICE</title>

</head>
<body style='color: #535b61;font-size: 14px;line-height: 22px;padding-top: 30px;padding-bottom: 30px;'>
<!-- Container -->

<div class='container-fluid invoice-container border' style='margin: 15px auto; padding: 12px; max-width: 530px;background-color: #fff;border: 3px solid #2f648e;  border-radius: 25px;'> 
  <!-- Header -->
  <header>
    <div class='row align-items-center'>
        <div style=' display: inline-block;width: 70%;padding: 5px;'>
          <img src='https://master.my-company.app/img/logo.png'  style='height: 20%; width: 20%;padding-top: 12px;padding-bottom: 12px;'>
          <h4 style='margin: 0;'>$society_name</h4>
          <address>$society_address</address>
        </div>
    </div>
  </header>
  <!-- Main Content -->
  <main>
    
    
    <div class='card' style='max-width: 100%;'>
      <div style='padding: 5px 5px;margin-bottom: 0;background-color: rgba(0, 0, 0, 0.05);border-bottom: 1px solid rgba(0, 0, 0, 0); border-radius:10px; margin-top: 10px; word-wrap:break-word;'> Complaint For : $compalain_title <span style='font-weight: 600 !important;'>
     <br>
      $description </span>
      <span style='float: right;'><b>Complaint No. : $complain_no </b></span> 
    
    <div class='table-responsive'>
          <table style= width:100%; class='table'>

                <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Employee</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $user_name</td>

               </tr>
                <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Branch</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $dep_bra </td>

               </tr>
                <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Category</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $category_name</td>

               </tr>

          </table>
        </div>
        </div>
      
      </div>
      <p class='ml-5 mt-5' style='font-size:13px;'>
         
       $complain_description
        <br>";
        if($notiUrl!='') { 
        $message .= "<p style='box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; line-height: 1.5em; margin-top: 0;' align='center'> <img src='$notiUrl' height=400 width=350/></p>";
        }
      $message .= "
      Thank You !<br />
      Team MyCo <br />
        </p>
        <hr>
     
      <p style='font-style: italic;font-size:11px;color:gray;'>
        important note: MyCo or MyCo partners never ask for your password, bank details or MPIN over email. please do not share your password or MPIN with anyone. for any questions write to <a href='mailto:contact@mycompany.app'>Support MyCo </a><br />
      </p>
  </main>
 
</div>

</body>
        ";
       ?>
