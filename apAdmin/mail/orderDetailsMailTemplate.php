<?php
$message = '
<!DOCTYPE html>
<html">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Order Details</title>
        <style type="text/css">
            #outlook a {padding:0;}
            body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; font-family: Helvetica, arial, sans-serif;}
            .ExternalClass {width:100%;}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
            .backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
            .main-temp table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; font-family: Helvetica, arial, sans-serif;}
            .main-temp table td {border-collapse: collapse;}
            .hscroll {
              overflow-x: auto;
            }
        </style>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="backgroundTable main-temp" style="background-color: #d5d5d5;">
            <tbody>
                <tr>
                    <td>
                        <table align="center" border="0" class="devicewidth" style="background-color: #ffffff;width:100%;">
                            <tbody>
                                <!-- Start header Section -->
                                <tr>
                                    <td style="padding-top: 30px;">
                                        <table align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner" style="border-bottom: 1px solid #eeeeee; text-align: right;width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td style="font-size: 14px; line-height: 18px; color: #666666; padding-bottom: 5px;">
                                                        <strong>Order Number:</strong> #'.$retailer_order_id.'
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 14px; line-height: 18px; color: #666666; padding-bottom: 25px;">
                                                        <strong>Order Date:</strong> '.$order_date.'
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!-- End header Section -->
                                <!-- Start address Section -->
                                <tr>
                                    <td style="padding-top: 0;">
                                        <table align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner" style="border-bottom: 1px solid #bbbbbb;width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 50%; font-size: 16px;color: #666666; padding-bottom: 5px;"><span style="font-weight: bold;">
                                                        Retailer Name :</span> '.$retailer_name.'
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <table align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner" style="border-bottom: 1px solid #bbbbbb;width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td style="font-size: 16px;color: #666666; padding-bottom: 5px;">
                                                        <span style="font-weight: bold;">Delivery Adderss : </span>'.$del_add.'
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <table align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner" style="border-bottom: 1px solid #bbbbbb;width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 50%; font-size: 16px;color: #666666; padding-bottom: 5px;"><span style="font-weight: bold;">
                                                        Contact Person</span> : '.$retailer_contact_person.'  '.$retailer_contact_person_country_code.' '.$retailer_contact_person_number.'
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!-- End address Section -->
                                <!-- Start product Section -->
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <div class="hscroll">
                                            <table border="1" align="center" cellspacing="0" cellpadding="6" style="width:100%;">
                                                <tbody>
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>SKU</th>
                                                        <th>Price</th>
                                                        <th>Case</th>
                                                        <th>Unit</th>
                                                        <th>Total Unit</th>
                                                        <th>Total Price</th>
                                                    </tr>';
                                                    $product_total_price = 0;
                                                    $product_total_qty = 0;
                                                    foreach($all_data AS $key => $value)
                                                    {
                                                        $product_total_price += $value['product_total_price'];
                                                        $product_total_qty += $value['product_quantity'];
                                                        if($value['order_packing_type'] == 0)
                                                        {
                                                             $pie = $value['product_quantity'];
                                                        }
                                                        else
                                                        {
                                                            $mul = $value['no_of_box'] * $value['order_per_box_piece'];
                                                            $pie = $value['product_quantity'] - $mul;
                                                        }
                                                        $message .= '
                                                    <tr>
                                                        <td>'.$value['product_name'].' '.$value['product_variant_name'].'</td>
                                                        <td>'.$value['sku'].'</td>
                                                        <td>'.$value['product_price'].'</td>
                                                        <td>'.$value['no_of_box'].' ('.$value['order_per_box_piece'].')</td>
                                                        <td>'.$pie.'</td>
                                                        <td>'.$value['product_quantity'].'</td>
                                                        <td>'.$value['product_total_price'].'</td>
                                                    </tr>';
                                                    }

                                                   $message .= '
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <!-- End product Section -->
                                <!-- Start calculation Section -->
                                <tr>
                                    <td style="padding-top: 0;">
                                        <table align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner" style="border-bottom: 1px solid #bbbbbb; margin-top: -5px;width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td rowspan="5" style="width: 55%;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 14px; font-weight: bold; line-height: 18px; color: #666666; padding-top: 10px;">
                                                        Total Unit
                                                    </td>
                                                    <td style="font-size: 14px; font-weight: bold; line-height: 18px; color: #666666; padding-top: 10px; text-align: right;">
                                                        '.$product_total_qty.'
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size: 14px; font-weight: bold; line-height: 18px; color: #666666; padding-top: 10px;">
                                                        Total Price
                                                    </td>
                                                    <td style="font-size: 14px; font-weight: bold; line-height: 18px; color: #666666; padding-top: 10px; text-align: right;">
                                                        '.$product_total_price.'
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!-- End calculation Section -->
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
';
?>