<?php 
if($socieaty_logo!="") { 
  $socieaty_logo = $base_url."img/society/$socieaty_logo";
}  else {
  $socieaty_logo= "https://master.my-company.app/img/logo.png";
}
$message="<!doctype html>
<html> 
<head>
<title>INVOICE</title>

</head>
<body style='color: #535b61;font-size: 14px;line-height: 22px;padding-top: 30px;padding-bottom: 30px;'>
<!-- Container -->

<div class='container-fluid invoice-container border' style='margin: 15px auto; padding: 12px; max-width: 600px;background-color: #fff;border: 3px solid #2f648e;  border-radius: 25px;'> 
  <!-- Header -->
  <header>
    <div class='row align-items-center'>
        <div style='display: inline;  padding-right: 5%;'>
          <img class='logo' src='$socieaty_logo' alt='MyCo' width='130px'/>
        </div>
        <div style=' display: inline-block;width: 70%;padding: 5px;'>
          <h4 style='margin: 0;'>$society_name</h4>
          <address>$society_address_invoice</address>
        </div>
    </div>
  </header>
  <!-- Main Content -->
  <main>
    
    
    <div class='card' style='max-width: 100%;'>
      <div style='padding: 5px 5px;margin-bottom: 0;background-color: rgba(0, 0, 0, 0.05);border-bottom: 1px solid rgba(0, 0, 0, 0); border-radius:10px; margin-top: 10px; word-wrap:break-word;'> Payment For  <span style='font-weight: 600 !important;'>
     <br>
      $description </span>
      <span style='float: right;'><b>Amount : $currency $received_amount</b></span> 
    
    <div class='table-responsive'>
          <table style= width:100%; class='table'>

                <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Branch</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $unit_name</td>

               </tr>
                <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Txn. ID</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $txt_id</td>

               </tr>

               <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Txn. Status</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;color:green;'>: $txt_status</td>
               </tr>
               <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Payment Mode </td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $payment_mode  </td>
               </tr>
               <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Ref. No.</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $invoice_number</td>
               </tr>
               <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Category</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $category</td>
               </tr>
               <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Paid By</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $paid_by</td>
               </tr>
               <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Received By</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $received_by</td>
               </tr>
                <tr>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>Txn. Date</td>
                <td style='padding: 5px;vertical-align: top;border-top:1px solid #dee2e6; word-wrap:anywhere;'>: $receive_date</td>
               </tr>
          </table>
        </div>
        </div>
      
      </div>
      <p class='ml-5 mt-5' style='font-size:13px;'>
      <div style='text-align:center;'>
       <a style='background: #2f648e;color: white;text-decoration: none;padding: 7px;border-radius: 10px;font-weight: bold;' target='_blank' href='$invoiceUrl' class='f-fallback button button--blue' target='_blank'>Download Invoice</a>
      </div>
          Hi <b>$paid_by</b><br/><br>

        If You Have not Made This Transaction or notice any error please contact us at<br />
          <a href='mailto:$secretary_email'>Email Us</a><br />
        <br>
      Thank You !<br />
      Management of $society_name<br />

        </p>
        <hr>

      
     
      <p style='font-style: italic;font-size:11px;color:gray;'>
        important note: MyCo or MyCo partners never ask for your password, bank details or MPIN over email. please do not share your password or MPIN with anyone. for any questions write to <a href='mailto:contact@mycompany.app'>Support MyCo </a><br />
      </p>
  </main>
 
</div>

</body>";

?>