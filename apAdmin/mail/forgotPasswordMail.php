<?php 

$message = "
		<html><link type='text/css' rel='stylesheet' id='dark-mode-general-link'><link type='text/css' rel='stylesheet' id='dark-mode-custom-link'><style type='text/css' id='dark-mode-custom-style'></style><head>
  <meta http-equiv='content-type' content='text/html; charset=windows-1252'>
        <style>
            div.container { width: 80%; padding-left: 10%; padding-top: 70px; padding-bottom: 70px;
            }
           
           
            @media only screen and (min-device-width: 320px) and (max-device-width: 480px) {
                div.container {
                    width: 90%;
                    padding-left: 4%;
                    padding-top: 30px;
                    padding-bottom: 30px;
                }
                
            }
           
          
        </style>
    </head>
    <body style='background-image: url(https://my-company.app/images/mailBg.jpg); background-repeat: no-repeat; background-size: cover;' cz-shortcut-listen='true'>
        <div class='container'>
            <div style='padding: 0px;color: #000;background-color: #fff; font-size: 20px;  border-radius: 10px;'>
                <center style='background: #fff;''>
                <a href='https://my-company.app/'><img src='https://master.my-company.app/img/logo.png'  style='height: 20%; width: 20%;padding-top: 12px;padding-bottom: 10px;'></a></center>


                <center><a href='https://my-company.app/'><img style='height:auto; width:100%; object-fit:fill;' src='https://my-company.app/images/companyBanner.png'></a></center>
                <h2 style='text-align: center;'>Hello <span style='color: red;'>$admin_name</span> !</h2>


               <div style='padding:20px;padding-top:0px'>
                <p style=' font-size: 18px;text-align: center;font-family: Poppins'>
                You recently requested to reset your password for your MyCo $societyLngName account. Use the button below to reset it.
                <br />
               
                      <br />
                 <strong>This password reset is only valid for the next 24 hours. </strong><br />
                 <br />
                 <div style='text-align:center;'>
                  <a style='width: 200px;background: #22BC66;color: white;text-decoration: none;padding: 8px;border-radius: 5px;' href='$forgotLink'>Reset Password</a>
                       
                </div>

              <div>
              
                 </div>

                
                 <p style=' font-size: 18px;text-align: center;font-family: Poppins'>
                  If you did not request a for account create, please ignore this email or <a href='mailto:contact@my-company.app'> contact support</a> if you 
                 <br />
                  have questions.
                 <br />
                <br />
                If you’re having trouble with the button above, copy and paste the URL below into your web browser.
                <br>
                <p class='sub' style='text-align:center; box-sizing: border-box; color: white; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; line-height: 1.5em; margin-top: 0;' align='left'>$forgotLink</p>
                </p>
                 <br />
                 <p style='text-align:center;'>
                 Thank You,<br />
                The MyCo Team

                </p>
                 

            </div>
            <p style=' font-size: 18px; text-align: center; background:#d2d2d2;padding-top:8px;padding-bottom:8px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px;color:#000 '>
                       &#169; 2021 MyCo. All rights reserved.
        </div>
    


</body>
</html>";

		?>