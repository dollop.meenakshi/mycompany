<?php 
 $message= "<html><link type='text/css' rel='stylesheet' id='dark-mode-general-link'><link type='text/css' rel='stylesheet' id='dark-mode-custom-link'><style type='text/css' id='dark-mode-custom-style'></style><head>
  <meta http-equiv='content-type' content='text/html; charset=windows-1252'>
        <style>
            div.container { width: 80%; padding-left: 10%; padding-top: 70px; padding-bottom: 70px;
            }
           
           
            @media only screen and (min-device-width: 320px) and (max-device-width: 480px) {
                div.container {
                    width: 90%;
                    padding-left: 4%;
                    padding-top: 30px;
                    padding-bottom: 30px;
                }
                
            }
           
          
        </style>
    </head>
    <body style='background-image: url(https://my-company.app/images/mailBg.jpg); background-repeat: no-repeat; background-size: cover;' cz-shortcut-listen='true'>
        <div class='container'>
            <div style='padding: 0px;color: #000;background-color: #fff; font-size: 20px;  border-radius: 10px;'>
                <center style='background: #fff;''>
                <a href='https://my-company.app/'><img src='https://master.my-company.app/img/logo.png'  style='height: 120px;padding-top: 12px;padding-bottom: 10px;'></a></center>


                <center><a href='https://my-company.app/'><img style='height:auto; width:100%; object-fit:fill;' src='https://my-company.app/images/companyBanner.png'></a></center>
                <h2 style='text-align: center;'>Hello <span style='color: red;'>$user_name</span> !</h2>


               <div style='color: #000; padding:20px;padding-top:0px'>
                <p style='font-size: 18px;text-align: center;font-family: Poppins'>
                Welcome to MyCo! We're excited to provide you our $societyLngName management service, and hopefully 
                <br />
                you're excited too. Let come & enjoy the world of Digital
                 <br /><br />
                 Your account has been created successfully for $society_name & your register mobile number is : <b>$country_code $user_mobile </b>
                    <br>
                      <br />
                 <strong>Please download the MyCo App from Play store or App Store click on below link </strong><br />
                 <br />
                 <div style='text-align:center;'>
                        <a target='_blank' href='https://play.google.com/store/apps/details?id=com.mycompany.fhpl'><img src='https://my-company.app/images/playstore.png' style='height: 50px'></a>

                    <a target='_blank' href='https://apps.apple.com/us/app/my-company-app/id1570587985'><img src='https://my-company.app/images/appStore.png' style='height: 50px'></a>
                </div>

              <div>
              
                 </div>

                
                 <p style=' font-size: 18px;text-align: center;font-family: Poppins'>
                  If you did not request for account create, please ignore this email or <a href='mailto:contact@my-company.app'> contact support</a> if you 
                 <br />
                  have questions.
                 <br />
                <br />
                If you’re having trouble with the button above, Please got to Play store or App Store and Search MyCo
                
                </p>
                 <br />
                 <p style='color: #000;text-align:center;'>
                 Thank You,<br />
                The MyCo Team

                </p>
        

            </div>
            <p style=' font-size: 18px; text-align: center; background:#d2d2d2;padding-top:8px;padding-bottom:8px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px;color:#000 '>
                       &#169; 2021 MyCo. All rights reserved.
        </div>
    


</body>
</html>";
    

    ?>