<?php 
 $message= "<html><link type='text/css' rel='stylesheet' id='dark-mode-general-link'><link type='text/css' rel='stylesheet' id='dark-mode-custom-link'><style type='text/css' id='dark-mode-custom-style'></style><head>
<meta http-equiv='content-type' content='text/html; charset=windows-1252'>
        <style>
            div.container {
                width: 90%;
                padding-left: 5%;
                padding-top: 30px;
                padding-bottom: 30px;
            }
            .payment-div {
                width: 50%;
                display: table-cell;
            }
            .text-center {
                text-align: center;
            }
            .row {
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                display: block;
            }
            @media only screen and (min-device-width: 320px) and (max-device-width: 480px) {
                div.container {
                    width: 90%;
                    padding-left: 4%;
                    padding-top: 30px;
                    padding-bottom: 30px;
                }
                .payment-div {
                    width: 100%;
                    display: block;
                }
                .mainbody{
                    font-size: 14px !important; 
                }
            }
            .mainbody {
                padding: 0px;
                color: #000;
                background-color: #fff;
                font-size: 20px;
                border-radius: 10px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
            }
            table,
            th,
            td {
                border: 1px solid black;
            }
        </style>
    </head>
    <body style='background-image: url(https://my-company.app/images/mailBg.jpg); background-repeat: no-repeat; background-size: cover;' cz-shortcut-listen='true'>

        <div class='container'>
            <div class='mainbody'>
                <center><img src='$socieaty_logo' style='width: 150px;padding-top: 12px;padding-bottom: 12px;'></center>
                <center><img style='height:auto; width:100%; object-fit:fill;' src='https://my-company.app/images/companyBanner.png'></center>
                <h2 style='text-align: center;'><span style='color: #73007E;'>$society_name </span> </h2>

                <h2 style='text-align: center;'><span style='color: black;font-size:16px;'>Circular : $paymentName</span> </h2>
               <div style='padding:10px;padding-top:0px'>
                <p style=' font-size: 18px;text-align: center;font-family: Poppins'>Dear $user_full_name, please find the details as discussed and pay as soon possible <br/></p>
                </div>
                <div style='text-align:center;'>
                   <a style='background: #2f648e;color: white;text-decoration: none;padding: 7px;border-radius: 10px;font-weight: bold;' target='_blank' href='$invoice' class='f-fallback button button--blue' target='_blank'>View Proforma Invoice</a>
                  </div>
                <div class='text-center row'>
                        <p style='font-size:12px;margin-bottom:2px;'>Download App </p>
                        <a target='_blank' href='https://play.google.com/store/apps/details?id=com.mycompany.fhpl'><img src='https://my-company.app/images/playstore.png'style='height: 50px'></a>

                    <a target='_blank' href='https://apps.apple.com/us/app/my-company-app/id1570587985'><img src='https://www.my-company.app/images/appStore.png' style='height: 50px'></a>
                </div>
                <br>
                <div style='padding:10px;'>
                <p style=' font-size: 18px;text-align: center;font-family: Poppins '>If you received this email by mistake, please ignore this email or<a href='mailto:contact@mycompany.app'> contact support</a> if you have questions.</p>
                <p style=' font-size: 18px;text-align: center;font-family: Poppins '>Thank You,
                <br/>Management of $society_name</p>
                <p style='text-align:center;color:red;font-size:14px;'>Disclaimer: This is an official notice sent by $societyLngName $society_name admin</p>
                </div>

                    <p style='color:#000; font-size: 18px; text-align: center; background:#d2d2d2;padding-top:8px;padding-bottom:8px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px '>
                       &#169; 2021 MyCo. All rights reserved.
                 </p>

            </div>
        </div>
    


</body>
</html>";

?>
            