<?php
date_default_timezone_set("Asia/Calcutta");
$message ="
<html>
    <link type='text/css' rel='stylesheet' id='dark-mode-general-link'>
    <link type='text/css' rel='stylesheet' id='dark-mode-custom-link'>
    <style type='text/css' id='dark-mode-custom-style'></style>
    <meta name='viewport' content='width=1024'>
    <head>
        <meta http-equiv='content-type' content='text/html; charset=windows-1252'>
        <style>
            @media only screen and (min-device-width: 320px) and (max-device-width: 480px) {
                div.container {
                    width: 100%;
                    padding-top: 10px;
                    padding-bottom: 10px;
                }
            }
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 95%;
            }
            th {
                font-size: 12px;
                border: 1px solid #000000;
                text-align: center;
            }
            td {
                border: 1px solid #000000;
                font-size: 12px;
            }
            tr:nth-child(even) {
                background-color: #dddddd;
            }
            .darkBg{
                background-color: #2f648e;
            }
            .fontWhite{
                color: white;
            }
            .pad-t-six{
                padding-top: 60px;
                text-align: right;
                float:right;
                padding-right: 12px;
            }
            @media only screen and (max-device-width: 640px)
            {
                .mar-top{
                    margin-top: 60px;
                }
                .pad-t-six{
                    padding-top: 30px;
                    align: left;
                    text-align: left;
                    font-size: 12px;
                }
            }
            @media only screen and (max-device-width: 768px)
            {
                .mar-top{
                    margin-top: 60px;
                }
                .pad-t-six{
                    padding-top: 30px;
                    align: left;
                    text-align: left;
                    font-size: 12px;
                }
            }
        </style>
    </head>
    <body cz-shortcut-listen='true'>
        <div class='container' style='width: 100%;padding-top: 30px;padding-bottom: 30px;'>
            <div class='mainbody' style='padding: 0px;color: #000;background-color: #fff;font-size: 20px;border-radius: 10px;'>

                <div style='background-color:#fff;border-top-right-radius: 10px;
                border-top-left-radius: 10px'>";

                if($soc_logo != "" && file_exists("../img/society/".$soc_logo))
                {
                    $message .= "
                    <img src='".$base_url."img/society/".$soc_logo."' style='height: 100px; padding-top: 12px;padding-bottom: 12px;padding-left: 120px;'>";
                }
                else
                {
                    $message .= "
                    <img src='https://www.my-company.app/assets/img/logo/mc-logo-new.png' style='height: 100px; padding-top: 12px;padding-bottom: 12px;padding-left: 120px;'>";
                }
                    $message .="<label class='pad-t-six'>Date : ".date("Y-m-d")."</label>
                </div>

                <div style='padding:10px;text-align: center;align:center;' class='mar-top'>
                    <h6 style='margin:3px;'>Total Order Values</h6>
                    <table align='center'>
                        <tr class='darkBg fontWhite'>
                            <th></th>
                            <th colspan='4'>$filter_data_view (Today)</th>
                            <th colspan='4'>$weekView (This Week)</th>
                            <th colspan='4'>$this_month (This Month)</th>
                        </tr>
                        <tr>
                            <th>Employee</th>
                            <th>Values</th>
                            <th>No. Of Orders</th>
                            <th>Quantity</th>
                            <th>Productivity</th>
                            <th>Values</th>
                            <th>No. Of Orders</th>
                             <th>Quantity</th>
                            <th>Productivity</th>
                            <th>Values</th>
                            <th>No. Of Orders</th>
                            <th>Quantity</th>
                            <th> Productivity</th>
                        </tr>";
                        foreach ($osda as $key => $value)
                        {
                            $message .= "
                        <tr>
                            <td align='left'>".$value['user_full_name']."</td>
                            <td align='right'>".$value['today_total_amount']."</td>
                            <td>".$value['today_total_orders']."</td>
                            <td>".$value['today_total_qtyt1']."</td>
                            <td>".$value['today_cp']."%</td>
                            <td align='right'>".$value['week_total_amount']."</td>
                            <td>".$value['week_total_orders']."</td>
                            <td>".$value['week_total_qtyt1']."</td>
                            <td>".$value['week_cp']."%</td>
                            <td align='right'>".$value['month_total_amount']."</td>
                            <td>".$value['month_total_orders']."</td>
                            <td>".$value['month_total_qtyt1']."</td>
                            <td>".$value['month_cp']."%</td>
                        </tr>";
                        }
                        $message .="
                    </table>
                </div>

                <div style='padding:10px;text-align: center;align:center;'>
                    <h6  style='margin:3px;'>Product Category Summary</h6>
                    <table align='center'>
                        <tr class='darkBg fontWhite'>
                            <th></th>
                            <th colspan='2'>$filter_data_view (Today)</th>
                            <th colspan='2'>$weekView (This Week)</th>
                            <th colspan='2'>$this_month (This Month)</th>
                        </tr>
                        <tr>
                            <th>Product</th>
                            <th>Value (INR)</th>
                            <th>Quantity (No. of Products)</th>
                            <th>Value (INR)</th>
                            <th>Quantity (No. of Products)</th>
                            <th>Value (INR)</th>
                            <th>Quantity (No. of Products)</th>
                        </tr>";
                        foreach ($pcda as $k => $v)
                        {
                            $message .= "
                        <tr>
                            <td align='left'>".$v['category_name']."</td>
                            <td align='right'>".$v['today_total_amount']."</td>
                            <td>".$v['today_total_qty']."</td>
                            <td align='right'>".$v['week_total_amount']."</td>
                            <td>".$v['week_total_qty']."</td>
                            <td align='right'>".$v['month_total_amount']."</td>
                            <td>".$v['month_total_qty']."</td>
                        </tr>";
                        }
                        $message .="
                    </table>";
                    $message .= "<img style='margin-top: 20px' src='$img1_url'/>";
                $message .= "
                </div>

                <div style='padding:10px;text-align: center;align:center;'>
                    <table align='center'>
                        <tr class='darkBg fontWhite'>
                            <th colspan='8'>Employee Summary (".$filter_data_view.")</th>
                        </tr>
                        <tr>
                            <th>Employee</th>
                            <th>Order Values</th>
                            <th>Total Retailers</th>
                            <th>Visits</th>
                            <th>Orders</th>
                            <th>Quantity</th>
                            <th>No Orders</th>
                            <th>Call Productivity</th>
                        </tr>";
                        foreach ($td3_arr as $k3 => $v3)
                        {
                            $message .= "
                        <tr>
                            <td align='left'>".$v3['user_full_name']."</td>
                            <td align='right'>".$v3['today_total_amount']."</td>
                            <td>".$v3['total_retailers']."</td>
                            <td>".$v3['today_tv']."</td>
                            <td>".$v3['today_total_orders']."</td>
                            <td>".$v3['today_total_qtyt3']."</td>
                            <td>".$v3['today_no_orders']."</td>
                            <td>".$v3['user_call_produ']."%</td>
                        </tr>";
                        }
                        $message .= "
                    </table>";
                    $message .= "<img style='margin-top: 20px' src='$img2_url'/></div>";
                    $message .= "<div style='padding:10px;text-align: center;align:center;'><img style='margin-top: 20px' src='$img3_url'/>";
                    $message .="
                </div>

                <div style='padding:10px;'>
                    <p style=' font-size: 24px;text-align: center;font-family: Poppins '>Thank You,<br />The MyCo Team</p>
                </div>
                <p style=' font-size: 18px; text-align: center; background:#d2d2d2;padding-top:8px;padding-bottom:8px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px '>&#169; ".date("Y")." MyCo. All rights reserved.</p>
            </div>
        </div>
    </body>
</html>";
?>