<?php //IS_67
 $message= "<html><link type='text/css' rel='stylesheet' id='dark-mode-general-link'><link type='text/css' rel='stylesheet' id='dark-mode-custom-link'><style type='text/css' id='dark-mode-custom-style'></style><head>
<meta http-equiv='content-type' content='text/html; charset=windows-1252'>
        <style>
            div.container {
                width: 80%;
                padding-left: 10%;
                padding-top: 70px;
                padding-bottom: 70px;
            }
            .payment-div {
                width: 50%;
                display: table-cell;
            }
            .text-center {
                text-align: center;
            }
            .row {
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                display: block;
            }
            @media only screen and (min-device-width: 320px) and (max-device-width: 480px) {
                div.container {
                    width: 90%;
                    padding-left: 4%;
                    padding-top: 30px;
                    padding-bottom: 30px;
                }
                .payment-div {
                    width: 100%;
                    display: block;
                }
                .mainbody{
                    font-size: 14px !important; 
                }
            }
            .mainbody {
                padding: 0px;
                color: #000;
                background-color: #fff;
                font-size: 20px;
                border-radius: 10px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
            }
            table,
            th,
            td {
                border: 1px solid black;
            }
        </style>
    </head>
    <body style='background-image: url(https://my-company.app/images/mailBg.jpg); background-repeat: no-repeat; background-size: cover;' cz-shortcut-listen='true'>

        <div class='container'>
            <div class='mainbody'>
                <center><img src='https://master.my-company.app/img/logo.png'  style='height: 120px; padding-top: 12px;padding-bottom: 12px;'></center>
                <center><img style='height:auto; width:100%; object-fit:fill;' src='https://my-company.app/images/companyBanner.png'></center>
                <h2 style='text-align: center;'>Hello <span style='color: red;'>$user_full_name</span> !</h2>


               <div style='padding:10px;padding-top:0px'>
                <p style=' font-size: 18px;text-align: center;font-family: Poppins'>New<span style='color: red;'> Penalty</span> has been generated for<br/>$penalty_name<br><br><b>Penalty Amount : ₹ $final_penalty_amount</b><br/><br/>Please download the MyCo App from Play store or App Store click on below link </p>


                </div>
                <div class='text-center row'>
                       <a target='_blank' href='https://play.google.com/store/apps/details?id=com.mycompany.fhpl'><img src='https://my-company.app/images/playstore.png'style='height: 50px'></a>

                    <a target='_blank' href='https://apps.apple.com/us/app/my-company-app/id1570587985'><img src='https://my-company.app/images/appStore.png' style='height: 50px'></a>
                </div>
                <br>
                <div style='padding:10px;'>
                <p style='color:#000; font-size: 18px;text-align: center;font-family: Poppins '>If you received this email by mistake, please ignore this email or<a href='mailto:contact@mycompany.app'> contact support</a> if you have questions.</p>
                <p style=' font-size: 18px;text-align: center;font-family: Poppins '>Thank You,
               <br/>Management of $society_name</p>
                </div>


                    <p style=' font-size: 18px; text-align: center; background:#d2d2d2;padding-top:8px;padding-bottom:8px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px '>
                       &#169; 2021 MyCo. All rights reserved.
                 </p>

            </div>
        </div>
    


</body>
</html>";

?>
            