<?php 
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
extract(array_map("test_input" , $_POST));
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
$idName= generateRandomString();
 ?>
<div class="form-group row">
 <!--  <div for="vehicle_no" class="col-sm-4">
  	<span class="deleteparty text-danger"><i class="fa fa-trash-o"></i></span>
  </div> -->
  <label for="<?php echo $idName;?>" class="col-sm-4 col-form-label">Vehicle No. <i onclick="lessParking();" class="deleteparty fa text-danger fa-trash-o float-right text-success"></i></label>
  <div class="col-sm-8" id="">
    <input maxlength="15" size="15" minlength="5" id="<?php echo $idName;?>" type="text" name="vehicle_no[]" class="form-control text-uppercase vehicle_number11"  >
     
  </div>
</div>

<input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
<script type="text/javascript">
  $('.deleteparty').click(function(){
       $(this).parent().parent().remove();
  });
  function lessParking() {
      var current_count = $('#totalParkings').val();
       var newCount = parseInt(current_count)-1;
        $("#totalParkings").val(newCount);

  }
</script>