<?php
if(isset($_POST['addSubCategoryBtn']) && $_POST['addSubCategoryBtn'] == "addSubCategoryBtn")
{
    extract($_POST);
}
if(isset($_POST['editSubCategory']) && isset($_POST['product_sub_category_id']))
{
    $product_sub_category_id = $_POST['product_sub_category_id'];
    $q = $d->select("product_sub_category_master","product_sub_category_id = '$product_sub_category_id'");
    $data = $q->fetch_assoc();
    extract($data);
    $form_id = "subCategoryUpdateForm";
}
else
{
    $form_id = "subCategoryAddForm";
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="<?php echo $form_id; ?>" action="controller/categorySubCategoryController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <?php
                            if(isset($_POST['editSubCategory']) && isset($_POST['product_sub_category_id']))
                            {
                            ?>
                            <i class="fa fa-edit"></i>
                            Edit Sub Category
                            <?php
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-plus"></i>
                            Add Sub Category
                            <?php
                            }
                            ?>
                            </h4>
                            <div class="form-group row">
                                <label for="category_name" class="col-sm-2 col-form-label">Category <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <select class="form-control single-select" name="product_category_id" id="product_category_id" required>
                                        <option value="">--Select--</option>
                                        <?php
                                        $gc = $d->selectRow("product_category_id,category_name","product_category_master","product_category_delete = 0");
                                        while($cd = $gc->fetch_assoc())
                                        {
                                        ?>
                                        <option <?php if((isset($editSubCategory) || isset($addSubCategoryBtn)) && $product_category_id == $cd['product_category_id']){ echo "selected"; } ?> value="<?php echo $cd['product_category_id']; ?>"><?php echo $cd['category_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sub_category_name" class="col-sm-2 col-form-label">Sub Category Name <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input value="<?php if(isset($editSubCategory)){ echo $sub_category_name; } ?>" required type="text" class="form-control" name="sub_category_name" id="sub_category_name" maxlength="100">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sub_category_description" class="col-sm-2 col-form-label">Sub Category Description</label>
                                <div class="col-sm-10">
                                    <textarea id="sub_category_description" name="sub_category_description" class="form-control"><?php if(isset($editSubCategory) && $sub_category_description != "") { echo $sub_category_description; } ?></textarea>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                            <div class="form-footer text-center">
                                <input type="hidden" name="addSubCategory" value="addSubCategory">
                                <?php
                                if(isset($editSubCategory))
                                {
                                ?>
                                <input type="hidden" name="product_sub_category_id" id="product_sub_category_id" value="<?php echo $product_sub_category_id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->