<?php 
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

if (isset($_COOKIE['bms_admin_id'])) {
  $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
  $society_id = $_COOKIE['society_id'];
}

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract(array_map("test_input" , $_POST));


if(isset($validatemobileFirst) && $validatemobileFirst=="yes"){

$q=$d->select("daily_visitors_master","visitor_mobile='$visitor_mobile'   and active_status=0 ");
     
 
   if(mysqli_num_rows($q) >0 ){
      echo "false";
   } else {
    echo "true";
   }  

}else if(isset($validatemobile) && $validatemobile=="yes"){

$q=$d->select("daily_visitors_master","visitor_mobile='$visitor_mobile'  and  visitor_id !='$visitor_id' and active_status=0 ");
     
 
   if(mysqli_num_rows($q) >0 ){
      echo "false";
   } else {
    echo "true";
   }  

}else 






if(isset($EditFlg) && $EditFlg=="Yes" && isset($visitor_id)){ 

$q=$d->select("daily_visitors_master","visitor_id='$visitor_id'","");
$data=mysqli_fetch_array($q);
extract($data);

?>

<style type="text/css" media="screen">
  .weekDays-selector input {
  display: none!important;
}

.weekDays-selector input[type=checkbox] + label {
  display: inline-block;
  border-radius: 6px;
  background: #dddddd;
  height: 35px;
  width: 25px;
  margin-right: 3px;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
}

.weekDays-selector input[type=checkbox]:checked + label {
  background: #2AD705;
  color: #ffffff;
}
</style>

  <input type="hidden" name="visitor_id" id="visitor_id" value="<?php echo $visitor_id; ?>">

  <div class="form-group row">
       <div class="col-sm-12 text-center">
        <label for="imgInp">
           <img id="blah" style="border: 1px solid gray;" src="<?php if(isset($visitor_profile)) { echo '../img/visitor/'.$visitor_profile; }  else { echo 'img/user.png'; } ?>"  width="100" height="100"   src="#" alt="your image" class='rounded-circle' />
           <i style="position: absolute;font-size: 20px;margin-left: -16px;margin-top: 66px;" class="fa fa-camera"></i>
         <input accept="image/*" class="photoInput photoOnly" id="imgInp" type="file" name="visitor_profile">
       </label>
       </div>
  </div>
  <div class="form-group row">
    <label for="visitor_name" class="col-sm-2 col-form-label">Visitor Name <span class="required">*</span></label>
    <div class="col-sm-4" id="">
      <input type="text" class="form-control text-capitalize" name="visitor_name" id="visitor_name" value="<?php echo $visitor_name; ?>"  >
    </div>
    <label for="visitor_mobile" class="col-sm-2 col-form-label">Visitor Mobile <span class="required">*</span></label>
    <div class="col-sm-2" id="">
      <input type="hidden" value="<?php echo $country_code; ?>" id="country_code_get" name="">
      <select name="country_code" class="form-control single-select" id="country_code" required="">
      <?php include 'country_code_option_list.php'; ?>
      </select>
    </div>
    <div class="col-sm-2" id="">

      <input type="text" maxlength="15" minlength="8" class="form-control onlyNumber" inputmode="numeric" name="visitor_mobile" value="<?php echo $visitor_mobile; ?>" id="visitor_mobile"  >
    </div>
  </div>
 

  <div class="form-group row">
    <label for="visit_from" class="col-sm-2 col-form-label">Valid Till <span class="required">*</span></label>
    <div class="col-sm-4" id=" ">
      <input type="text" class="form-control valid-till_edit" value="<?php echo $valid_till; ?>" name="valid_till"  readonly="" required="">
    </div>
    <label for="visit_from" class="col-sm-2 col-form-label">In/Out Time <span class="required">*</span></label>
    <div class="col-sm-2" id="">
      <input type="text" class="form-control time-picker" value="<?php echo  date("h:i A", strtotime($in_time)); ?>" name="in_time" id="in_time"  placeholder="In Time" required="">
    </div>
    <div class="col-sm-2  " id="">
      <input type="text" class="form-control time-picker1" value="<?php echo date("h:i A", strtotime($out_time)); ?>" name="out_time" id="out_time" placeholder="Out Time" required="">
    </div>
  </div>
  <div class="form-group row">
    <label for="visit_from" class="col-sm-2 col-form-label">Week Days <span class="required">*</span></label>
    <div class="col-sm-4" id=" ">
      <div class="weekDays-selector">
        <?php    
        $week_days = str_replace(", ",",",$week_days);
        $week_days_data = explode(",", $week_days) ; 
          ?> 
        <input <?php if(in_array("MON", $week_days_data)) { echo "checked";} ?> name="week_days[]" type="checkbox" id="weekday-mon1" value="MON" class="weekday1" />
        <label for="weekday-mon1">M</label>
        <input <?php if(in_array("TUE", $week_days_data)) { echo "checked";} ?>  name="week_days[]" type="checkbox" id="weekday-tue1" value="TUE" class="weekday1" />
        <label for="weekday-tue1">T</label>
        <input <?php if(in_array("WED", $week_days_data)) { echo "checked";} ?>  name="week_days[]" type="checkbox" id="weekday-wed1" value="WED" class="weekday1" />
        <label for="weekday-wed1">W</label>
        <input <?php if(in_array("THU", $week_days_data)) { echo "checked";} ?> name="week_days[]" type="checkbox" id="weekday-thu1" value="THU" class="weekday1" />
        <label for="weekday-thu1">T</label>
        <input  <?php if(in_array("FRI", $week_days_data)) { echo "checked";} ?> name="week_days[]" type="checkbox" id="weekday-fri1" value="FRI" class="weekday1" />
        <label for="weekday-fri1">F</label>
        <input <?php if(in_array("SAT", $week_days_data)) { echo "checked";} ?> name="week_days[]" type="checkbox" id="weekday-sat1" value="SAT" class="weekday1" />
        <label for="weekday-sat1">S</label>
        <input <?php if(in_array("SUN", $week_days_data)) { echo "checked";} ?> name="week_days[]" type="checkbox" id="weekday-sun1" value="SUN" class="weekday1" />
        <label for="weekday-sun1">S</label>
        <span id="chk_error1"></span>
      </div>
    </div>
    <label for="visit_from" class="col-sm-2 col-form-label">Visit Company <span class="required">*</span></label>
    <div class="col-sm-4" id=" ">
     <select onchange="otherDailyCheck11();"  required="" class="form-control complain-select" id="visitor_sub_type_id_edit" name="visitor_sub_type_id" >
      <option value="">Select Visitor Type</option>
      <?php
      $visitorMainType_qry = $d->select("visitorSubType","active_status=0 AND visitor_main_type_id=4");
                            //isset($complaint_category) &&
      while($visitorMainType_data = mysqli_fetch_array($visitorMainType_qry)){ ?>
        <option <?php if($visitor_sub_type_id==$visitorMainType_data['visitor_sub_type_id']){ echo "selected";} ?>  value="<?php echo $visitorMainType_data['visitor_sub_type_id']; ?>"><?php echo $visitorMainType_data['visitor_sub_type_name']; ?></option>
      <?php } ?>
    </select>
  </div>
</div>

 <div class="form-group row" id="otherCompanyEdit">
    <label for="visit_from" class="col-sm-2 col-form-label">Other Company Name<span class="required">*</span></label>
    <div class="col-sm-4" id=" ">
      <input type="text" id="visit_from_otherName" class="form-control text-capitalize"  value="<?php echo $visit_from; ?>" name="visit_from" id="visit_from">
    </div>
</div>

<div class="form-group row">
  <label for="vehicle_number" class="col-sm-2 col-form-label">Vehicle Number</label>
  <div class="col-sm-4" id=" ">
    <input type="text" class="form-control text-uppercase txtNumeric" name="vehicle_number" id="vehicle_number" value="<?php echo $vehicle_number; ?>">
  </div>
</div>

<div class="form-group row">
  <label for="visitor_profile" class="col-sm-2 col-form-label">ID Proof </label>
  <div class="col-sm-8">
    <div class="col-sm-10 table-responsive" style="display: inherit;">
        <label for="imgInpId">
           <img id="idproof" style="border: 1px solid gray;" src="../img/upload.png"  width="164" height="240"   src="#" alt="id proof" class=' ' />
         <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="idInput idProof" id="imgInpId" type="file" name="id_proof"><br>
         Front Side <?php if($visitor_id_proof!='') { ?> <a target="_blank" href="../img/visitor/<?php echo $visitor_id_proof;?>">View</a> <?php } ?>
       </label>
         <label for="imgInpId1">
           <img id="idproof1" style="border: 1px solid gray;" src="../img/upload.png"  width="164" height="240"   src="#" alt="id proof" class=' ' />
         <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="photoInput idProof" id="imgInpId1" type="file" name="visitor_id_proof_back">
         <br>
         Back Side <?php if($visitor_id_proof_back!='') { ?> <a target="_blank" href="../img/visitor/<?php echo $visitor_id_proof_back;?>">View</a> <?php } ?>
       </label> 
    </div>
  </div>
</div>
 
    <input  value="<?php echo $visitor_id_proof; ?>" type="hidden" class="form-control-file " name="id_proof_old"  >
    <input  value="<?php echo $visitor_id_proof_back; ?>" type="hidden" class="form-control-file " name="visitor_id_proof_back_old"  >
 <input type="hidden" class="form-control-file photoOnly" name="visitor_profile_old" accept="image/*"  value="<?php echo $visitor_profile; ?>" id="visitor_profile_old"  >
 
<script src="assets/js/datepicker.js"></script>
<script src="assets/js/custom16.js"></script> 
<script type="text/javascript">
  jQuery(document).ready(function($){

  var country_code = $('#country_code_get').val();
  $("#country_code").val(country_code);

  
});

   var fileExtension = ['jpeg', 'jpg', 'png', 'jpeg'];

  function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

  function readURLId(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#idproof').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function readURLId1(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#idproof1').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#imgInp").change(function() {
  readURL(this);
});

$("#imgInpId").change(function() {
  var fileExt= $(this).val().split('.').pop().toLowerCase();
  if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    if(fileExt=='pdf') {
      $('#idproof').attr('src', 'img/pdf.png');
    } else  if(fileExt=='doc' || fileExt=='docx')  {
      $('#idproof').attr('src', 'img/doc.png');
    }
  } else {
    readURLId(this);
  }
  // readURLId(this);
  // alert('afd');
});

$("#imgInpId1").change(function() {
  var fileExt= $(this).val().split('.').pop().toLowerCase();
  if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    if(fileExt=='pdf') {
      $('#idproof1').attr('src', 'img/pdf.png');
    } else  if(fileExt=='doc' || fileExt=='docx')  {
      $('#idproof1').attr('src', 'img/doc.png');
    }
  } else {
    readURLId1(this);
  }
  // alert('afd');
});


  var date = new Date();
 date.setDate(date.getDate());

$('.valid-till_edit').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: date,
        format: 'yyyy-mm-dd'
      });
      //19march2020
      var maxdate = new Date(); //get current date
      maxdate.setDate(maxdate.getDate() + 30);
      

     var visitor_sub_type_id_edit = $("#visitor_sub_type_id_edit option:selected").html();

     if (visitor_sub_type_id_edit=='Other') {
        if (visitor_sub_type_id_edit=='Other') {
          $("#otherCompanyEdit").show();
          // $("#visit_from_otherName").val('');
        }else {
          $("#otherCompanyEdit").hide();
        }

      } else {
        $("#otherCompanyEdit").hide();
      }


      function  otherDailyCheck11() {
        var visitor_sub_type_id_edit = $("#visitor_sub_type_id_edit option:selected").html();
        // alert(visitor_sub_type_id);
        if (visitor_sub_type_id_edit=='Other') {
          if (visitor_sub_type_id_edit=='Other') {
            $("#otherCompanyEdit").show();
            $("#visit_from_otherName").val('');
          }else {
            $("#otherCompanyEdit").hide();
          }

        }else {
          $("#otherCompanyEdit").hide();
        }

      }
  
</script>
<?php }  else {
   $_SESSION['msg']="Something Wrong";
    header("Location: ../dailyVisitors");
} ?>