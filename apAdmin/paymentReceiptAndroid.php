<?php
if (filter_var($_GET['id'], FILTER_VALIDATE_INT) == true ) {
  error_reporting(0);
  $url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
  $activePage = basename($_SERVER['PHP_SELF'], ".php");
  include_once 'lib/dao.php';
  include 'lib/model.php';
  $d = new dao();
  $m = new model();
  $con=$d->dbCon();
  include 'common/checkLanguage.php';
  if ($_COOKIE['country_id']==101) { 
    $cgstName = $xml->string->cgst;
    $sgstName = $xml->string->sgst;
    $igstName = $xml->string->igst;
  } else if ($_COOKIE['country_id']==161) {
    $igstName = $xml->string->igst;
  }
  include 'common/checkLanguage.php';

  extract(array_map("test_input" , $_GET));
  $societyid =  (int)$societyid;
  $user_id =  (int)$user_id;
  $id =  (int)$id;
  $event_attend_id =  (int)$event_attend_id;
  $facility_id =  (int)$facility_id;
  ?>
  <!-- simplebar CSS-->
  <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="assets/css/sidebar-menu2.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="assets/css/app-stylfe8.css" rel="stylesheet"/>
  <link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
  <!--Lightbox Css-->
  <link href="assets/plugins/fancybox/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <?php
  
  $sp=$d->select("society_master","society_id='$societyid'");
  $socData=mysqli_fetch_array($sp);
  $currency = $socData['currency'];
   $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                "getCountriesSingle=getCountriesSingle&country_id=$socData[country_id]&language_id=1");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'key: bmsapikey'
    ));

    $server_output = curl_exec($ch);

    curl_close ($ch);
    $server_output=json_decode($server_output,true);

 $country_name= $server_output['name'];

   $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/main_api/location_controller_new.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                "getStateSingle=getStateSingle&state_id=$socData[state_id]&language_id=1");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'key: bmsapikey'
    ));

    $server_output = curl_exec($ch);

    curl_close ($ch);
    $server_output=json_decode($server_output,true);

   $state_name= $server_output['name'];


  if($type=="exp"){
    $qb=$d->select("expenses_balance_sheet_master","expenses_balance_sheet_id ='$id'","");
    $bData=mysqli_fetch_array($qb);
    $ref_no='REF_'.$id;
    $Ramount=$bData['expenses_amount'];
    $amount_without_gst=$bData['expenses_amount'];
    $gst=$bData['gst'];
    $tax_slab=$bData['tax_slab'];
    $taxble_type='0';
    $bill_type="";
    $bill_type_data = ""; 
    $traDate =$bData['expenses_add_date'];
//
// $payType="Paid";
    $pType="";
    $TypeName = "Expenses for ".$bData['expenses_title'];

     

  }
  else{
  }
  ?>
  <div class="">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body" id="printableArea">
              <!-- Content Header (Page header) -->
              <!-- <section class="content-header">
                <h3>
                  Ref.No
                  <small>#<?php echo $ref_no;  ?></small>
                </h3>
              </section> -->
              <!-- Main content -->
              <section class="invoice">
                <!-- title row -->
                <div class="row mt-3">
                  <div class="col-lg-6 col-6">
                    <h4><i class="fa fa-building"></i> <?php echo $socData['society_name']; ?></h4>
                    <address>
                      <?php echo $socData['society_address']; ?>
                      <br>
                      <?php echo $socData['city_name']; ?>, <?php echo $state_name; ?> - <?php echo $country_name; ?>
                    </address>
                    <p class="text-uppercase">
                      <?php if($socData['gst_no']!='') {
                        echo $xml->string->tax." :".$socData['gst_no'];
                      } ?>
                    </p>
                  </div>
                  <div class="col-lg-6 col-6 text-right">
                    <?php if($type=="P"){
                      $penalty_id=$id;
                      $paneltyDataQry=$d->select("penalty_master,unit_master,users_master,expenses_balance_sheet_master","
                        expenses_balance_sheet_master.society_id=penalty_master.society_id AND
                        expenses_balance_sheet_master.balancesheet_id=penalty_master.balancesheet_id AND
                        penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$societyid' AND penalty_master.penalty_id='$id'  AND penalty_master.penalty_amount =expenses_balance_sheet_master.income_amount   ","");

                      $paneltyDataQData=mysqli_fetch_array($paneltyDataQry);
                      // print_r($paneltyDataQData);
                      ?>
                      <b>Invoice Type:</b> Penalty Payment<br>
                      <b>Received Date:</b> <?php if($bData['penalty_receive_date'] !="0000-00-00 00:00:00") { echo date("d-m-Y h:i A", strtotime($bData['penalty_receive_date'])); } else {  echo date("d-m-Y h:i A", strtotime($bData['penalty_receive_date'])); } ?><br>
                      <?php //IS_846?>
                      <b>Payment Method:</b> <?php echo $pType; ?><br>
                  <?php //IS_846
                  
                      echo "<b>Invoice Number: </b><span style='color:red;'> INVPN".$penalty_id.'<span>';
                 
                  ?>
                  <?php //IS_846 ?>
                  <?php //IS_582?>
                <?php }  else if($type=="B"){
                  $bill_qry=$d->select("receive_bill_master,bill_master,bill_category_master","bill_category_master.bill_category_id=bill_master.bill_category AND receive_bill_id=$id AND receive_bill_master.bill_master_id=bill_master.bill_master_id");
                  $bill_data=mysqli_fetch_array($bill_qry);
                  $no_of_unit= $bill_data['no_of_unit'];
                  $unit_price= $bill_data['unit_price'];
                  $bill_type= $bill_data['bill_category_name'];
                  ?>
                  <b>Invoice Category: </b> Bill Payment(<?php echo $bill_type;?>)<br>
                  <b> Payment Date:</b> <?php echo date("d-m-Y h:i:s A", strtotime($bill_data['bill_payment_date'])); ?><br>
                  
                  <?php //IS_846?>
                  <b>Payment Method:</b> <?php echo $pType; ?><br>
                  <?php //IS_846
                     echo "<b>Invoice Number: </b><span style='color:red;'> INVBL".$bill_data['receive_bill_id'].'<span>';
                 
                   ?>
                <?php } else if($type=="E") {
                  $event_qry=$d->select("event_master,event_attend_list","event_master.event_id=$id AND event_master.event_id=event_attend_list.event_id AND event_attend_list.event_attend_id='$event_attend_id'");
                  $event_data=mysqli_fetch_array($event_qry);
                  ?>
                  <b>Invoice Type:</b> Event Booking<br>
                  <b>Payment Date:</b> <?php echo date("d-m-Y h:i:s A", strtotime($event_data['payment_received_date'])); ?><br>
                  <?php //IS_846?>
                  <b>Payment Method:</b> <?php echo $pType; ?><br>
                  <?php //IS_846
                  
                      echo "<b>Invoice Number:<span style='color:red;'>  INVEV".$event_data['event_attend_id'].'</span>';
                
                  ?>
                   <?php } else if($type=="Fac") {
                 
                  ?>
                  <b>Invoice Type:</b> Facility Booking<br>
                  <b>Payment Date:</b> <?php echo date("d-m-Y h:i:s A", strtotime($traDate)); ?><br>
                  <?php //IS_846?>
                  <b>Payment Method:</b> <?php echo $pType; ?><br>
                  <?php //IS_846
                  
                      echo "<b>Invoice Number:<span style='color:red;'>  INVFAC".$id.'</span>';
                
                  ?>
                <?php }   else if($type=="M"){
                  $man_qry=$d->select("receive_maintenance_master,maintenance_master","receive_maintenance_id=$id AND receive_maintenance_master.maintenance_id=maintenance_master.maintenance_id");
                  $man_data=mysqli_fetch_array($man_qry); ?>
                  <b>Invoice Category:</b> <?php echo $xml->string->maintenance; ?><br>
                  <b> Received Date:</b> <?php echo date("d-m-Y h:i:s A", strtotime($man_data['receive_maintenance_date'])); ?><br>
                  <?php //IS_846?>
                  <b>Payment Method:</b> <?php echo $pType; ?><br>
                  <?php //IS_846
                   echo "<b>Invoice Number:<span style='color:red;'>  INVMN".$man_data['receive_maintenance_id'].'</span>';
                  ?>
                  <?php
                }  else if($type=="exp"){
                   ?>
                  <b>Invoice Category:</b> Expense<br>
                  <b> Created Date:</b> <?php echo date("d-m-Y", strtotime($traDate)); ?><br>
                  <?php //IS_846
                   echo "<b>Receipt Number:<span style='color:red;'>  INVEX".$id.'</span>';
                  ?><br>
                  <?php
                }  else {
                  
                 
                } ?>
              </div>
            </div>
            <?php if($type!="exp"){ ?>
            <hr>
            <div class="row invoice-info">
              <div class="col-sm-12 invoice-col">
                <h4><i class="fa fa-user"></i> Billing Address</h4>
                <address>
                  <strong><?php echo $userData['block_name']; ?> -
                    <?php echo $userData['unit_name']; ?>,
                    <?php echo $userData['user_full_name'];?></strong><br>
                     <?php echo $socData['society_address']; ?><br>
                    <STRONG>Mobile:</STRONG>
                    <?php echo $userData['user_mobile']; ?><br>
                    <STRONG>Email:</STRONG>
                    <?php echo $userData['user_email']; ?><br>
                    
                  </address>
                </div>
              </div>
            <?php } ?>
              <!-- Table row -->
              <?php $total_var = 0 ; 
              $subtotal_var = 0 ;  
              $today = strtotime("today midnight");
              ?>
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th class="txt-center">Sr.No</th>
                        <th class="txt-center">Description</th>
                        <th class="text-right">Price</th>
                        
                        
                        
                        <th class="text-right">Total Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php   $gst_amount  = 0 ; ?>
                      <?php $cnt = 1 ; ?>
                      <tr>
                        <td class="txt-center">1</td>
                        <td class="txt-center"><?php echo $TypeName; ?></td>
                        <?php
                        if(($type=="E" || $type=="exp" || $type=="Fac") ){
                         $new_amouunt = $amount_without_gst ;                  
                         ?>
                         <td class="text-right"><?php echo sprintf("%.2f",$new_amouunt);

                         ?></td>
                         <?php

                       } else { ?>
                        <td class="text-right"><?php echo sprintf("%.2f",$Ramount);

                        ?></td>
                      <?php }
                      if(( $type=="E" || $type=="exp" || $type=="Fac") ){
                        ?>
                        <td class="text-right"><?php echo $currency; ?> <?php
                        echo sprintf("%.2f",$new_amouunt);
                        $total_var += $new_amouunt ; 
                        $subtotal_var += $new_amouunt ;
                        ?></td>
                        <?php
                      } else {
                        ?>



                        <td class="text-right"><?php echo $currency; ?> <?php

                        echo sprintf("%.2f",$Ramount);
                        $total_var += $Ramount ; 
                        $subtotal_var += $Ramount ;
                        ?></td>
                      <?php } ?>
                    </tr>
                   
                  

                    <?php
                    if(( $type=="E" || $type=="Fac") ){
 
                      if($gst==1){
                        $gst_amount = (($amount_without_gst * $tax_slab) / 100);
                      } else {
                      
                        $gst_amount = $Ramount - ($Ramount * (100/(100+$tax_slab))); 
                         }
                        // echo $gst_amount;

                      if( $gst==0 &&  $type=="B"){
                         $gst_amount = $bill_amount - ($bill_amount * (100/(100+$tax_slab)));


                      // $gst_amount = (($bill_amount * $tax_slab) /100);
                     } else if($type=="B"){

                        $gst_amount = (($amount_without_gst * $tax_slab) /100);
                     }
                     if($gst_amount > 0 ){
                      $gst_amount  = $gst_amount ;
                    } else {
                      $gst_amount  = 0 ;
                    }
                    if($gst==1 &&  $type=="B"){
                      $Ramount =$bill_amount+ $gst_amount;

                    }

 
                    if($is_taxble=="1"){
                      if($taxble_type=="0"){
                        $per = 0;
                        $tax_slab_val = explode(".", $tax_slab);
                        if($tax_slab_val[1] !="00"){
                          $per= (($tax_slab)/2);
                        } else {
                          $per= ($tax_slab_val[0]/2);
                        }

                         
                        $gst_amount_half= ($gst_amount/2);
                        ?>
                        <tr>
                          <th style="text-align: left;" colspan="3"><?php echo $cgstName;?>  ( <?php
                            echo $per."%";
                            ?> ):</th>
                          <td class="text-right"><?php echo $currency; ?> <?php echo sprintf("%.2f",$gst_amount_half); 
                          
                           
                            $total_var += $gst_amount_half ; 
                            $subtotal_var += $gst_amount_half ;
                          
                          
                          ?></td>
                        </tr>
                        <tr>
                          <th style="text-align: left;"  colspan="3"><?php echo $sgstName;?>  ( <?php

                            echo $per."%";
                            ?> ):</th>
                          <td class="text-right"><?php echo $currency; ?> <?php echo sprintf("%.2f",$gst_amount_half); 
                          
                            $total_var += $gst_amount_half ; 
                            $subtotal_var += $gst_amount_half ;
                          
                          ?></td>
                        </tr>
                      <?php } else { ?>
                        <tr>
                          <th style="text-align: left;"  colspan="3"><?php echo $igstName;?> ( <?php
                            $tax_slab_val = explode(".", $tax_slab);
                            if($tax_slab_val[1] !="00"){
                              echo $tax_slab ."%";
                            } else {
                              echo $tax_slab_val[0]."%";
                            }
                            ?> ):</th>
                          <td style="text-align: left;"  class="text-right"><?php echo $currency; ?> <?php echo sprintf("%.2f",$gst_amount); 
                        
                            $total_var += $gst_amount ; 
                            $subtotal_var += $gst_amount ;
                        
                          ?></td>
                        </tr>
                      <?php } }
                    }?>
                    <tr>
                      <th style="text-align: right;"  colspan="3">Subtotal :</th>
                      <td class="text-right"><?php echo $currency; ?> <?php  
                      echo sprintf("%.2f",$subtotal_var); ?></td>
                    </tr>
                     <?php if($type=='B' && $bData['late_fees_amount']!='0.00' ){
                      $Ramount += $bData['bill_late_fees'];
                      $total_var += $bData['bill_late_fees'] ; 
                      $subtotal_var += $bData['late_fees_amount'] ;
                      $cnt++ ;
                      ?>
                      <tr class="">
                        <td class="text-right" colspan="3"><b><?php echo "Late Fees :"; ?></b></td>
                        <td class="text-right"><?php echo $currency; ?> <?php echo $bData['late_fees_amount']; ?></td>
                      </tr>
                    <?php } ?>

                     <?php 
                    if($type=='M' && $mData['maintenance_late_fees']!='0.00' ){
                      $Ramount += $mData['maintenance_late_fees'];
                      $total_var += $mData['maintenance_late_fees'] ; 
                      $subtotal_var += $mData['maintenance_late_fees'] ;
                      $cnt++ ;
                      ?>
                      <tr>
                        <td class="text-right" colspan="3"><b><?php echo "Late Fees :"; ?></b></td>
                        <!-- <td class="txt-center"></td> -->
                        <!-- <td class="text-right"></td> -->
                        <td class="text-right"><?php echo $currency; ?> <?php echo $mData['maintenance_late_fees']; ?></td>
                      </tr>
                    <?php } ?>
                    <?php if($transaction_charges>0) { ?>
                    <tr>
                        <td class="txt-center" ></td>
                        <td class="txt-center" colspan="2">Transaction Charges</td>
                     
                        <td class="text-right"><?php echo $currency; ?> <?php
                        echo sprintf("%.2f",$transaction_charges);
                        
                        ?></td>
                       
                    </tr>
                    <?php } ?>
                    <tr>
                      <th style="text-align: right;"  colspan="3">Total :</th>
                      <td class="text-right"><?php echo $currency; ?> <?php  
                      echo sprintf("%.2f",$total_var+$transaction_charges); ?></td>
                    </tr>
                  </tbody>
                </table>
              </div><!-- /.col -->
            </div>
             <?php if ($bData['wallet_amount_type']==0 && $bData['wallet_amount']>0) { 
              echo  $currency.' '.$bData['wallet_amount']. " Amount credited in wallet for Overpaid";
            } else if ($bData['wallet_amount_type']==1 && $bData['wallet_amount']>0) { 
              echo  $currency.' '.$bData['wallet_amount']. " Amount debited from wallet";
            }?>
            <?php

            $invoice_term_conditions_master_qry=$d->select("invoice_term_conditions_master","society_id='$societyid' and status= '0'  "," ORDER BY invoice_term_condition_id DESC limit 0,1");
            if(mysqli_num_rows($invoice_term_conditions_master_qry) > 0 ){
              $invoice_term_conditions_master_data=mysqli_fetch_array( $invoice_term_conditions_master_qry);
              echo "<br> <h4><span  class='card-title' ><u>Terms & Conditions</u></span></h4>";
              echo  $invoice_term_conditions_master_data['condition_desc'];
            }
            ?>
          </section>
        </div>
        <!-- this row will not appear when printing -->
        <hr>
        <div class="row no-print" id="printPageButton">
          <div class="col-lg-3">
            <a href="#" onclick="printDiv('printableArea')"  class="btn btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
          </div>
          <div class="col-lg-9">
            <div class="float-sm-right">
              <!-- <button class="btn btn-primary m-1"><i class="fa fa-envelope"></i> Receive on Mail</button> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }
</script>
<style type="text/css">
  @media print {
    #printPageButton {
      display: none;
    }
  }
  @media screen and (max-device-width: 480px) and (orientation: portrait){
    .text-right {
      text-align: right !important;
    }
    .txt-center{
      text-align: left !important;
    }
  }
  @media only screen
  and (min-device-width : 375px)
  and (max-device-width : 667px) {
    .text-right {
      text-align: right !important;
    }
    .txt-center{
      text-align: left !important;
    }
  }
</style>
<!-- Bootstrap core JavaScript-->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- simplebar js -->
<script src="assets/plugins/simplebar/js/simplebar.js"></script>
<!-- waves effect js -->
<script src="assets/js/waves.js"></script>
<!-- sidebar-menu js -->
<script src="assets/js/sidebar-menu.js"></script>
<!-- Custom scripts -->
<script src="assets/js/app-script.js"></script>
<script src="assets/js/sweetDelete6.js"></script>
<!--Data Tables js-->
<script src="assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/jszip.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/pdfmake.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/vfs_fonts.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.print.min.js"></script>
<script src="assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"></script>
<!--notification js -->
<script src="assets/plugins/notifications/js/lobibox.min.js"></script>
<script src="assets/plugins/notifications/js/notifications.min.js"></script>
<script src="assets/plugins/notifications/js/notification-custom-script.js"></script>
<!--Peity Chart -->
<script src="assets/plugins/peity/jquery.peity.min.js"></script>
<!-- Index js -->
<!-- <script src="assets/js/index.js"></script> -->
<!--Lightbox-->
<script src="assets/plugins/fancybox/js/jquery.fancybox.min.js"></script>
<script src="assets/js/custom.js"></script>
<script>
  $(document).ready(function() {
  //Default data table
  $('#default-datatable').DataTable();
  $('#default-datatable1').DataTable();
  $('#default-datatable2').DataTable();
  var table = $('#example').DataTable( {
    lengthChange: false,
    buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
  } );
  var table1 = $('#exampleReport').DataTable( {
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false
  } );
  table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
  table1.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
  
} );
</script>
</body>
</html>
<?php } else{
  echo "Invalid Request!!!";
}?>