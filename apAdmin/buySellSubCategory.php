<?php
error_reporting(0);
extract(array_map("test_input" , $_POST));
?>

<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
      <div class="col-sm-6">
        <h4 class="page-title">Buy / Sell Sub Category</h4>
     </div>
      <div class="col-sm-3">
       
      </div>
     <div class="col-sm-3">
        <div class="btn-group float-sm-right">
          <a href="#" data-toggle="modal" data-target="#addCategory" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Sub Category</a>
          <!-- </button> -->
        </div>
      </div>
     </div>
     <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="col-md-4">
              <form id="categoryForm" method="get">
                <?php
                  $c = $d->select("classified_category","classified_category_status='0'","ORDER BY classified_category_name ASC"); 
                ?>
                <select class="form-control single-select" name="category" onchange="$('#categoryForm').submit()">
                  <option value="All">All</option>
                  <?php while($cate=mysqli_fetch_array($c)){ ?>
                    <option value="<?=$cate['classified_category_id'] ?>" <?=($_GET) ? ($_GET['category']==$cate['classified_category_id']) ? 'selected' : '' : ''?>><?=$cate['classified_category_name'] ?></option>
                  <?php } ?>
                </select>
              </form>
            </div>
          </div>
        </div>
      </div>
     </div>
    <!-- End Breadcrumb-->
     <div class="row">
        <?php 
          $where = "classified_sub_category.classified_category_id=classified_category.classified_category_id";
          if(isset($_GET['category']) && $_GET['category']!='All'){
            $catid = (int)$_GET['category'];
            $where .= " AND classified_sub_category.classified_category_id = '$catid'";
          }
          // echo $where;
          $q=$d->select("classified_category,classified_sub_category",$where);
         if(mysqli_num_rows($q)>0) {
          while($row=mysqli_fetch_array($q)){ ?> 
          <div class="col-lg-3">
           <div class="card">
              <img height="150" class="card-img-top" src="../img/classified/classified_cat/<?php echo $row['classified_sub_category_image']; ?>" alt="Card image cap">
              <div class="card-body">
                 <h5 class="card-title text-primary"><?php echo $row['classified_sub_category_name'].' ('.$row[classified_category_name].')'; ?></h5>
                 <hr>
                 <a data-toggle="modal" data-target="#editCategory" href="javascript:void();" onclick="editSubCategory('<?php echo $row['classified_sub_category_id']; ?>','<?php echo $row['classified_sub_category_name']; ?>','<?php echo $row['classified_sub_category_image']; ?>');" class="btn btn-sm btn-primary shadow-primary">Edit</a>
                 <a href="javascript:void();" onclick="deleteClassifiedSubCategory('<?php echo $row['classified_sub_category_id']; ?>');" class="btn btn-sm btn-danger shadow-primary">Delete</a>
              </div>
           </div>
        </div>
        <?php } } else {
      echo "<img src='img/no_data_found.png'>";
    } ?>
      </div>
      </div>

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
  
<div class="modal fade" id="addCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Buy / Sell Sub Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="classifiedSubCategory" action="controller/classifiedController.php" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label"> Category  *</label>
                    <div class="col-sm-8">
                      <select required="" type="text" name="classified_category_id"  class="single-select form-control">
                        <option value="">-- Select --</option>
                         <?php $q=$d->select("classified_category","");
                        while($row=mysqli_fetch_array($q)){ ?> 
                        <option value="<?php echo $row['classified_category_id']; ?>"><?php echo $row['classified_category_name']; ?></option>
                        <?php }?>
                      </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Sub Category Name *</label>
                    <div class="col-sm-8">
                      <input required="" type="text" name="classified_sub_category_name"  class="form-control text-capitalize">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Image *</label>
                    <div class="col-sm-8">
                      <input required="" accept="image/*" type="file" name="classified_sub_category_image"  class="form-control-file border photoOnly">
                    </div>
                </div>
         
                <div class="form-footer text-center">
                  <input type="hidden" name="addSubCategory" value="addSubCategory">
                  <button type="submit" name="" value="" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->


<div class="modal fade" id="editCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Buy / Sell Sub Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="controller/classifiedController.php" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Sub Category Name *</label>
                    <div class="col-sm-8">
                      <input type="hidden" name="classified_sub_category_id" id="local_service_provider_sub_id">
                      <input required="" id="service_provider_sub_category_name" type="text" name="classified_sub_category_name"  class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Image </label>
                    <div class="col-sm-8">
                      <input type="hidden" name="classified_sub_category_image_old" id="service_provider_sub_category_image">
                      <input accept="image/*" type="file" name="classified_sub_category_image"  class="form-control-file border photoOnly">
                    </div>
                </div>
         
                <div class="form-footer text-center">
                  <input type="hidden" name="editSubCategory" value="editSubCategory">
                  <button type="submit" name="" value="" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->