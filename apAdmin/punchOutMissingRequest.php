<?php error_reporting(0);
  
  error_reporting(0);
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year']= $_REQUEST['month_year'];
  $status = $_REQUEST['status'];
  $dId = (int)$_REQUEST['dId'];
  $bId = (int)$_REQUEST['bId'];
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row ">
        <div class="col-md-12  col-12">
          <h4 class="page-title">Punch Out Missing Request</h4>
        </div>
       
     </div>
     <form action="" method="get" class="branchDeptFilter">
        <div class="row">
          <?php include('selectBranchDeptForFilterAll.php'); ?>
          <div class="col-md-2 col-6 form-group">
          <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){echo $from= $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-31 days'));} ?>">  
          </div>
          <div class="col-md-2 col-6 form-group">
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate = $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
          </div>
          <div class="col-md-2 form-group">
            <select  name="status" id="status" class="form-control single-select">
              <option value="">Select Status </option> 
              <option  <?php if($status=='0') { echo 'selected';} ?> value="0" >Pending</option>
              <option  <?php if($status=='1') { echo 'selected';} ?> value="1" >Accept</option>
              <option  <?php if($status=='2') { echo 'selected';} ?> value="2" >Reject</option>
              </select>
          </div>
          <div class="col-md-12 text-center ">

              <input class="btn btn-success btn-sm mb-3" type="submit" name="getReport"  value="Get Data">
          </div>
        </div>
    </form> 
      <div class="row mt-2">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                  <?php 
                        $i=1;

                        if (isset($bId) && $bId> 0) {
                          $BranchFilterQuery = " AND users_master.block_id='$bId]'";
                        }

                        if(isset($dId) && $dId>0) {
                          $deptFilterQuery = " AND users_master.floor_id='$dId'";
                        }
                        else
                        {
                          $limitSql = 'LIMIT 1000';
                        }

                        if(isset($from) && isset($from) && $toDate != '' && $toDate) {
                          $dateFilterQuery = " AND attendance_punch_out_missing_request.attendance_date BETWEEN '$from' AND '$toDate'";
                        }

                        if($status!=''){
                          $appendQuery = " AND attendance_punch_out_missing_request.attendance_punch_out_missing_status = '$_GET[status]'";
                        }
                        $q=$d->selectRow("floors_master.floor_name,block_master.block_name,users_master.user_designation,users_master.block_id,users_master.floor_id,users_master.user_full_name,users_master.user_id,userStatus.user_full_name AS status_change_by_user,adminStatus.admin_name,attendance_punch_out_missing_request.*,attendance_master.attendance_id,attendance_master.punch_in_time,attendance_master.attendance_date_start","attendance_punch_out_missing_request 
                        LEFT JOIN users_master AS userStatus ON userStatus.user_id =   attendance_punch_out_missing_request.attendance_punch_out_missing_status_changed_by
                        LEFT JOIN bms_admin_master AS adminStatus ON adminStatus.admin_id =   attendance_punch_out_missing_request.attendance_punch_out_missing_status_changed_by
                        ,attendance_master,users_master LEFT JOIN floors_master ON floors_master.floor_id=users_master.floor_id LEFT JOIN block_master ON block_master.block_id=users_master.block_id","attendance_punch_out_missing_request.attendance_id=attendance_master.attendance_id AND attendance_punch_out_missing_request.user_id=users_master.user_id $dateFilterQuery $BranchFilterQuery $appendQuery $deptFilterQuery $blockAppendQueryUser"," ORDER BY attendance_punch_out_missing_request.attendance_date DESC $limitSql");
                        $counter = 1;
                        
                        //if (isset($status) && isset($status)) {
                 ?>
                  <table id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>                        
                            <th>Status</th>                      
                            <th>Action</th>
                            <th>Employee Name</th>
                            <th>Department</th>
                            <th>Punch In Date</th>  
                            <th>Punch In</th>
                            <th>Out Date</th>                      
                            <th>Request Out Time</th>
                            <th>Hours</th>
                            <th>Changed By</th>                      
                            <th>Reason</th>
                            <th>In Branch</th>

                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      while ($data=mysqli_fetch_array($q)) {
                        if($data['attendance_punch_out_missing_status_changed_by'] !=null && $data['attendance_punch_out_missing_status_changed_by']>0)
                        {
                          if($data['status_change_by_type']==1){
                            $status_change_by_user = $data['admin_name'];
                          }
                          else
                          {
                            $status_change_by_user = $data['status_change_by_user'];
                          }
                        }
                        else
                        {
                          $status_change_by_user ="";
                        }
                      ?>
                        <tr>
                          <td><?php echo $counter++; ?></td>
                          <td> <?php if($data['attendance_punch_out_missing_status'] == 0){ ?>
                          


                            <?php if($data['attendance_punch_out_missing_status'] == 0){ ?>
                          <button title="Approve Attendance" type="button" class="btn btn-sm btn-primary ml-1" onclick="updateUserAttendace(1, '<?php echo $data['attendance_id']; ?>','<?php echo $data['attendance_punch_out_missing_id']; ?>','<?php echo $data['attendance_date']; ?>','<?php echo $data['attendance_time']; ?>')"><i class="fa fa-check"></i></button>
                          <button title="Decline Attendance" type="button" class="btn btn-sm btn-danger ml-1" onclick="updateUserAttendace(2, '<?php echo $data['attendance_id']; ?>','<?php echo $data['attendance_punch_out_missing_id']; ?>','<?php echo $data['attendance_date']; ?>','<?php echo $data['attendance_time']; ?>')"><i class="fa fa-times"></i></button>
                            <?php } ?>
                          
                            <?php } else if($data['attendance_punch_out_missing_status'] == 1){ ?>
                              <span class="badge badge-pill badge-success m-1">Accepted </span>
                              <?php }else{ ?>
                                <span class="badge badge-pill badge-danger m-1">Rejected </span>
                              <?php } ?>
                          </td>
                          <td>
                            <button type="button" class="btn btn-sm btn-primary ml-2" onclick="viewUserCheckInOutImage('<?php echo $data['attendance_id']; ?>')"> <i class="fa fa-picture-o"></i> <i class="fa fa-map-marker"></i></button>

                            <button type="submit" class="btn btn-sm btn-primary" onclick="punchOutMissingRequestModal(<?php echo $data['attendance_punch_out_missing_id']; ?>)"> <i class="fa fa-eye"></i></button>
                          </td>
                          <td><?php echo $data['user_full_name'].' ( '.$data['user_designation'].' )'; ?></td>
                          <td><?php echo $data['floor_name']." (".$data['block_name']." )"; ?></td>
                          <td><?php if($data['attendance_date_start']!=="0000-00-00" && $data['attendance_date_start']!='null') { echo date("d M Y", strtotime($data['attendance_date_start'])); }?></td>
                          <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_in_time'] != 'null') {
                                echo date("h:i A", strtotime($data['punch_in_time']));
                              } ?></td>
                           <td><?php 
                          if($data['attendance_date']!=="0000-00-00" && $data['attendance_date']!='null') 
                          { 
                            if(($data['attendance_date']==$data['updated_attendance_date'] ) ){
                              echo date("d M Y", strtotime($data['attendance_date'])); 
                            }
                            else
                            {
                             
                              if($data['updated_attendance_date'] !="0000-00-00" && $data['updated_attendance_date'] !=null){

                                echo date("h:i A", strtotime($data['attendance_date']))."( Updated Date: ". date("h:i A", strtotime($data['updated_attendance_date']))." )";
                              }
                              else
                              {
                                echo date("d M Y", strtotime($data['attendance_date'])); 
                              }
                            }
                          }
                            ?>
                            </td>
                          <td><?php if ($data['attendance_time'] != '00:00:00' && $data['attendance_time'] != 'null') {
                          if(($data['attendance_time']==$data['updated_attendance_time'] ) ){
                            echo date("h:i A", strtotime($data['attendance_time']));
                          }
                          else
                          {
                            
                            if($data['updated_attendance_time'] !="00:00:00" && $data['updated_attendance_time'] !=null){

                              echo date("h:i A", strtotime($data['attendance_time']))."( Updated Time: ". date("h:i A", strtotime($data['updated_attendance_time']))." )";
                            }
                            else
                            {
                              echo date("h:i A", strtotime($data['attendance_time']));
                            }
                          }
                              } ?></td>
                          <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_out_time'] != '00:00:00' && $data['total_working_hours']=='') {
                            echo $d->getTotalHours($data['attendance_date_start'],$data['attendance_date'],$data['punch_in_time'],$data['attendance_time']);
                              } else {
                                echo $data['total_working_hours'];
                              } ?>
                          </td>
                            <td><?php echo $status_change_by_user ;?></td>
                         
                          <td><?php custom_echo($data['punch_out_missing_reason'], 30); ?></td>
                          <td><?php  echo $data['punch_in_branch'];  ?></td>
                          
                          
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
               <!--  <?php ///} else {  ?>
              <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select Department And Branch !!!!!</span>
                </div>
            <?php //} ?> -->
            </div>
            </div>
          </div>
        </div>
     </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>

<div class="modal fade" id="updateUserAttendace">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Update Punch Out Time</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
          <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form class="updateAttendancePunchOutMissingRequest" action="../residentApiNew/commonController.php" name="changeAttendance"  enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-4 col-form-label">Reason :</label>
                        <div class="col-lg-8 col-md-8" id="">
                            <p class="resonId"></p>
                        </div>                   
                    </div> 
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-4 col-form-label">Total Hours :</label>
                        <div class="col-lg-8 col-md-8" id="">
                            <p class="totalWorkingHours"></p>
                        </div>                   
                    </div> 

                    <div class="form-group row">
                        <label for="input-10" class="col-sm-4 col-form-label">Punch Out Date <span class="text-danger">*</span></label>
                        <div class="col-lg-8 col-md-8" id="">
                            <input required type="text" class="form-control " readonly id="attendance_date_end" name="attendance_date_end" >
                        </div>                   
                    </div>    
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-4 col-form-label">Punch Out Time <span class="text-danger">*</span></label>
                        <div class="col-lg-8 col-md-8" id="">
                            <input required type="text" class="form-control punch_out_time_change " readonly id="punch_out_time" name="punch_out_time" >
                        </div>                   
                    </div> 
                                 
                    <div class="form-footer text-center">
                    <input type="hidden" name="updateUserAttendance"  value="updateUserAttendance">
                    <input type="hidden" class="attendance_punch_out_missing_status" name="attendance_punch_out_missing_status"  value="">
                    <input type="hidden" name="attendance_id"  class="attendance_id">
                    <input type="hidden" name="redirect_floor_id" value="<?php echo $dId; ?>" >
                    <input type="hidden" name="redirect_block_id" value="<?php echo $bId; ?>" >
                    <input type="hidden" name="redirect_status" value="<?php echo $status;?>" >
                    <input type="hidden" name="attendance_punch_out_missing_id"  class="attendance_punch_out_missing_id">
                        <button id="addHrDocumentBtn" type="submit" class="btn btn-success addUserBank hideupdate"><i class="fa fa-check-square-o"></i> Update </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="rejectReasonModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Reject Reason</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="updateAttendancePunchOutMissingRequest" action="../residentApiNew/commonController.php" name="changeAttendance"  enctype="multipart/form-data" method="post">
                <div class="form-group row">
                        <label for="input-10" class="col-sm-5 col-form-label">Attendance Reason :</label>
                        <div class="col-lg-6 col-md-6" id="">
                            <p class="resonId"></p>
                        </div>                   
                    </div>
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-4 col-form-label">Reject Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-8 col-md-8" id="">
                          <textarea  class="form-control" name="punch_out_missing_reject_reason" id="punch_out_missing_reject_reason"></textarea>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                        <input type="hidden" name="updateUserAttendance"  value="updateUserAttendance">
                        <input type="hidden" class="attendance_punch_out_missing_status" name="attendance_punch_out_missing_status"  value="">
                        <input type="hidden" name="attendance_id"  class="attendance_id">
                        <input type="hidden" name="attendance_punch_out_missing_id"  class="attendance_punch_out_missing_id">
                        <button id="addHrDocumentBtn" type="submit" class="btn btn-success addUserBank hideupdate"><i class="fa fa-check-square-o"></i> Update </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="detail_view">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Punch Out Missing Request</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="punch_out_missing_request">

        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="UserCheckInOutImage">
    <div class="modal-dialog " style="max-width: 75%">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Attendance Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-md-6">
                <div><b>Punch In :</b> <span id="in_time"></span></div>
                <div ><b>Distance:</b> <span class="Inkm"></span></div>
                <div ><b>Address:</b> <span class="InAddress"></span></div>
              </div>
              <div class="col-md-6" >
                <div><b>Punch Out :</b> <span id="out_time"></span></div>
                <div class="d-none"><b>Distance:</b> <span class="Outkm"></span></div>
                <div class="d-none"><b>Address:</b> <span class="OutAddress"></span></div>
               
              </div>
            </div>
             <div class="row">
                <div class="col-md-6 col-6 text-center hideInData">
                  <div class="userCheckInOutImageData " id="userCheckInOutImageData">
                  </div>
                </div>
                <div class="col-md-6 col-6 text-center hideOutData">
                  <div class="userCheckOutImageData " id="userCheckOutImageData">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-12 float-left mapDiv ">
                  <div class="map mr-1" id="map3" style="width: 100%; height: 280px;">
                  </div>
                </div>
                
              </div>
            </div>

        </div>
      </div>
    </div>
  </div>

<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>


<script type="text/javascript">

  function initializeCommonMap(in_lat, in_long,out_lat,out_long) {

      var latlng = new google.maps.LatLng(in_lat, in_long);
      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize3(d_lat, d_long) {
       
      var latlng = new google.maps.LatLng(d_lat, d_long);
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
   

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize2(out_lat, out_long) {

      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlngOut,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));


      var infowindow = new google.maps.InfoWindow();
      
    }

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
      }
      return false;
    }

</script>
