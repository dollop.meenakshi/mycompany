<?php 
error_reporting(0);
extract($_REQUEST);
if (isset($_GET['id']) && $_GET['id']!='') {
  $user_mobile  =(int)$_GET['id'];
$count5=$d->sum_data("credit_amount-debit_amount","user_wallet_master","user_mobile='$user_mobile' AND active_status=0  AND user_mobile!=0");
            $row11=mysqli_fetch_array($count5);
            $totalDebitAmount=$row11['SUM(credit_amount-debit_amount)'];
}

if ($virtual_wallet==0) {
  $vu= $d->selectRow("user_mobile","users_master","user_mobile='$user_mobile' AND delete_status=0  AND user_mobile!=0");

 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-6">
        <h4 class="page-title">User Wallet (<?php echo $currency;?> <?php $count6=$d->sum_data("credit_amount-debit_amount","user_wallet_master","active_status=0  AND user_mobile!=0");
            $row=mysqli_fetch_array($count6);
            $totalDebitAmount1=$row['SUM(credit_amount-debit_amount)'];
             echo number_format($totalDebitAmount1,2);
             ?>)</h4>
        </div>
        <div class="col-sm-6 text-right">
          <a href="walletReport" class=" btn btn-sm btn-warning waves-effect waves-light "  href="javascript:void();"><i class="fa fa-file mr-1"></i> View Report</a>
        <?php if (isset($_GET['id']) && $_GET['id']!='' && $user_mobile!=0 && mysqli_num_rows($vu)>0) { ?>
          <?php if ($totalDebitAmount>0): ?>
           <a href="#" class=" btn btn-sm btn-danger waves-effect waves-light " data-toggle="modal" data-target="#debitModal" href="javascript:void();"><i class="fa fa-minus mr-1"></i> Debit Amount</a>
          <?php endif ?>
          <a href="#" class=" btn btn-sm btn-success waves-effect waves-light " data-toggle="modal" data-target="#creditModal" href="javascript:void();"><i class="fa fa-plus mr-1"></i> Credit Amount </a>
      <?php } ?>
        </div>
     </div>

    <form action="" id="personal-info" method="get">
     <div class="row pt-2 pb-2">
            
          <div class="col-lg-6 col-6">
             <select onchange="this.form.submit();" type="text" required="" class="form-control single-select" name="id">
                <option value=""> Select User</option>
                 <?php 
                   error_reporting(0);
                   $q3=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4  AND users_master.user_status!=0 AND users_master.user_mobile!=0 $blockAppendQuery","GROUP BY users_master.user_mobile ORDER BY block_master.block_sort ASC");
                   while ($row11=mysqli_fetch_array($q3)) {
                 ?>
                  <option <?php if($user_mobile==$row11['user_mobile']) { echo 'selected';} ?> value="<?php echo $row11['user_mobile'];?>"><?php echo $row11['user_full_name'];?>-<?php  if($row11['user_type']=="0" && $row11['member_status']=="0" ){
                            echo "Owner";
                        } else  if($row11['user_type']=="0" && $row11['member_status']=="1" ){
                            echo "Owner Family";
                        } else  if($row11['user_type']=="1" && $row11['member_status']=="0"  ){
                            echo "Tenant";
                        }else  if($row11['user_type']=="1" && $row11['member_status']=="1"   ){
                            echo "Tenant Family";
                        } else   {
                          echo "Owner";
                        }  ?> (<?php echo $row11['user_mobile'];?>)</option>
                  <?php }  ?>
            </select>
          </div>
          <?php if (isset($_GET['id']) && $_GET['id']!='') { ?>
           <div class="col-lg-6 col-6 text-danger">
            <b>
            Available Balance:  <?php 
             echo  $currency.' '. number_format($totalDebitAmount,2);
             ?>
           </b>
          </div>
          <?php } ?>
     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <?php 
                extract(array_map("test_input" , $_GET));


                  $i=1;
                if (isset($_GET['id']) && $_GET['id']!='') {
                  
               ?>
                
              <div class="table-responsive">

                <table id="" class="table table-bordered auto-index">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Description</th>
                            <th>Credit</th>
                            <th>Debit</th>
                            <th>Avl. Balance</th>
                            <th>Date</th>
                            <!-- <th>Action</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $i=1;


                          $q5=$d->select("user_wallet_master","user_mobile='$user_mobile' AND user_mobile!=0","ORDER BY wallet_id DESC");
        
                          $i=1;
                          if (mysqli_num_rows($q5) > 0) {
                          while ($eventData=mysqli_fetch_array($q5)) {
                           
                          ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $eventData['remark']; ?> </td>
                            <td><?php if($eventData['credit_amount']>0) { echo $currency.' '. number_format($eventData['credit_amount'],2); } ?></td>
                            <td><?php if($eventData['debit_amount']>0) { echo $currency.' '.number_format($eventData['debit_amount'],2); } ?></td>
                             <td><?php echo $currency.' '.$eventData['avl_balance']; ?></td>
                            <td><?php echo $eventData['created_date']; ?></td>
                          
                          </tr>
                          <?php  }}
                          ?>

                         
                           
                        </tbody>
                        
                      </table>
            
              </div>

            <?php } else {  ?>
             
                 <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select User</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->


   <div class="modal fade" id="creditModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Credit </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="creditFrom" action="controller/walletController.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="addCredit" value="addCredit">
          <input type="hidden" name="user_mobile" value="<?php echo $user_mobile; ?>">
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Credit Title <span class="required">*</span></label>
            <div class="col-sm-7">
              <input maxlength="100" required="" type="text" class="form-control" name="remark">
            </div>
            
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Credit Amount <span class="required">*</span></label>
            <div class="col-sm-7">
              <input required="" type="text" maxlength="10" id="credit_amount" min="1"  class="form-control onlyNumber" inputmode="numeric" name="credit_amount">
            </div>
            
          </div>
     
         
            
          
          <div class="form-footer text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o" name="addIncome"></i> Submit </button>
            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  </div><!--End Modal -->

   <div class="modal fade" id="debitModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"> Debit Amount</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="debitFrom" action="controller/walletController.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="addDebit" value="addDebit">
           <input type="hidden" name="user_mobile" value="<?php echo $user_mobile; ?>">
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Debit Title <span class="required">*</span></label>
            <div class="col-sm-7">
              <input maxlength="100" required="" type="text" class="form-control" name="remark">
            </div>
            
          </div>
          <div class="form-group row">
            <label for="input-10" class="col-sm-5 col-form-label">Debit Amount <span class="required">*</span></label>
            <div class="col-sm-7">
              <input required="" type="text" maxlength="10" id="debit_amount" min="1"  class="form-control onlyNumber" inputmode="numeric" name="debit_amount">
            </div>
            
          </div>
     
         
            
          
          <div class="form-footer text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o" name="addIncome"></i> Submit </button>
            <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  </div><!--End Modal -->
 <?php } else { ?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Your <?php echo $xml->string->society; ?> Wallet Service is Deactivate</h4>
        </div>
        <div class="col-sm-12">
          <p class="text-danger"><b>For Active Go To Building Setting -> App Access -> App Wallet <b></p>
        </div>
       
     </div>

  </div>
</div>


<?php } ?>