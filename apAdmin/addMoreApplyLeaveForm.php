<?php
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

$currentYear = date('Y');
$block_id = $_POST['block_id'];
$floor_id = $_POST['floor_id'];
$user_id = $_POST['user_id'];
//$no_of_leave = $_POST['no_of_leave'];
$society_id=$_COOKIE['society_id'];
?>

<div class="row">
    <div class="col-md-4">
        <div class="form-group row w-100 mx-0">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Leave Date <span class="required">*</span></label>
            <div class="col-lg-12 col-md-12 col-12" id="">
                <input type="text" placeholder="Leave Date" class="form-control default-datepicker-leave" name="leave_start_date" id="leave_start_date" readonly>
            </div>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="form-group row w-100 mx-0">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Leave Type <span class="required">*</span></label>
            <div class="col-lg-12 col-md-12 col-12" id="">
                <select name="leave_type_id" id="leave_type_id" class="form-control single-select" required >
                    <option value="">-- Leave Type --</option> 
                    
                </select>
            </div>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="form-group row w-100 mx-0">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Total Remaining Leave (Year) </label>
            <div class="col-lg-12 col-md-12 col-12" id="">
                <input type="text" placeholder="Remaining Leave" class="form-control" id="remaining_leave" value="" disabled>
            </div>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="form-group row w-100 mx-0">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Applicable Leave Till This Month </label>
            <div class="col-lg-12 col-md-12 col-12" id="">
            <input type="text" placeholder="Available Leave" class="form-control" id="available_paid_leave" value="" disabled>
            </div>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="form-group row w-100 mx-0">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Leave Day Type <span class="required">*</span></label>
            <div class="col-lg-12 col-md-12 col-12" id="">
                <select name="leave_day_type" id="leave_day_type" class="form-control" required>
                    <option value="">-- Day Type --</option> 
                    <option value="0">Full Day</option> 
                    <option value="1">Half Day</option> 
                </select>
            </div>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="form-group row w-100 mx-0">
            <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Paid Or Unpaid <span class="required">*</span></label>
            <div class="col-lg-12 col-md-12 col-12" id="">
                <select name="paid_unpaid" id="paid_unpaid" class="form-control paid_unpaid" required>
                    <option value="">-- Mode --</option> 
                </select>
            </div>
        </div> 
    </div>
</div> 

<script type="text/javascript" src="assets/js/select.js"></script>
<script src="assets/js/datepicker.js"></script>

<?php
    $user=$d->selectRow("users_master.shift_time_id,user_employment_details.joining_date","users_master,user_employment_details","users_master.user_id=user_employment_details.user_id AND users_master.user_id='$user_id'");
    $userData = mysqli_fetch_array($user);
    $shift=$d->selectRow("week_off_days","shift_timing_master","shift_time_id='$userData[shift_time_id]'");
    $shiftData = mysqli_fetch_array($shift);
    $array = explode(',', $shiftData['week_off_days']);
    
    $dateHideArry = array();
    $q1=$d->select("holiday_master","society_id='$society_id' AND floor_id=0 OR floor_id='$floor_id' AND holiday_status=0 AND DATE_FORMAT(holiday_start_date,'%Y') = '$currentYear'"); 
    while ($dData = mysqli_fetch_array($q1)){
        array_push($dateHideArry, $dData['holiday_start_date']);
    }
    
    $q2=$d->select("leave_master","society_id='$society_id' AND user_id='$user_id' AND DATE_FORMAT(leave_start_date,'%Y') = '$currentYear'"); 
    while ($dlData = mysqli_fetch_array($q2)){
        
        array_push($dateHideArry, $dlData['leave_start_date']);
    }
    ?>
    
    <script type="text/javascript">
      var date = new Date();
      firstDay = new Date(new Date().getFullYear(), 0, 1);
      //firstDay = new Date(2022-04-01);
      <?php if($currentYear == date('Y', strtotime($userData['joining_date']))){ ?>
        firstDay = new Date('<?php echo $userData['joining_date']; ?>');
      <?php } ?>
      
      lastDay = new Date(date.getFullYear(), date.getMonth() + 12, 0);
      date.setDate(date.getDate());
      var edate = new Date();
      edate.setDate(edate.getDate()+90);
      $('.default-datepicker-leave').datepicker({
        //todayHighlight: true,
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: firstDay,
        endDate: lastDay,
        datesDisabled: [<?php for($i=0; $i < count($dateHideArry); $i++){
          echo "'".$dateHideArry[$i]."'";
          echo ",";
        } ?>],
        daysOfWeekDisabled:[<?php for($j=0; $j < count($array); $j++){
          echo "'".$array[$j]."'";
          echo ",";
        } ?>],
      });

    $(function() {
        $("#leave_start_date").change(function(){
            var leave_date = new Date($(this).val());
            year = leave_date.getFullYear();
            var user_id = <?php echo $user_id;?>;
            $.ajax({
                url: "../residentApiNew/commonController.php",
                cache: false,
                type: "POST",
                data: {
                    action:'getLeaveTypeByUserId',
                    user_id:user_id,
                    year:year,
                },
                success: function(response){
                $(".ajax-loader").hide();
                var html ="";
                html=`<option value="">-- Leave Type --</option>`;
                
                $.each(response.leave_type, function( index, value ) {
                    html +=`<option value="`+value.leave_type_id+`">`+value.leave_type_name+`</option>`;
                });
                
                $('#leave_type_id').html(html);
                
                }
            })
        });
        $("#leave_type_id").change(function(){
            var leave_type_id = $(this).val();
            var leave_date = $('#leave_start_date').val();
            var user_id = <?php echo $user_id;?>;
            $.ajax({
                url: "../residentApiNew/commonController.php",
                cache: false,
                type: "POST",
                data: {
                    action:'getLeaveBalanceForAutoLeave',
                    leave_type_id:leave_type_id,
                    leave_date:leave_date,
                    user_id:user_id,
                },
                success: function(response){
                    $(".ajax-loader").hide();
                    $('#available_paid_leave').val(response.leave.available_paid_leave);
                    $('#remaining_leave').val(response.leave.remaining_leave);
                    if(response.leave.available_paid_leave >=1){
                        var paidUnpaidValue = `<option value="">-- Mode --</option>
                                <option value="0">Paid Leave</option>
                                <option value="1">Unpaid Leave</option> `;
                        $('#paid_unpaid').html(paidUnpaidValue);  
                    }
                    else{
                        var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
                        $('#paid_unpaid').html(paidUnpaidValue);
                    }
                }
            })
        });
        $("#leave_day_type").change(function(){
            var leave_day_type = $('#leave_day_type').val();
            var available_paid_leave = parseFloat($('#available_paid_leave').val());
            if(available_paid_leave == 0.5 && leave_day_type == 1){
                var paidUnpaidValue = `<option value="">-- Mode --</option>
                        <option value="0">Paid Leave</option>
                        <option value="1">Unpaid Leave</option> `;
                $('#paid_unpaid').html(paidUnpaidValue);  
            }else if(available_paid_leave >=1){
                var paidUnpaidValue = `<option value="">-- Mode --</option>
                        <option value="0">Paid Leave</option>
                        <option value="1">Unpaid Leave</option> `;
                $('#paid_unpaid').html(paidUnpaidValue);  
            }
            else{
                var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
                $('#paid_unpaid').html(paidUnpaidValue);
            }
        });
    });

    </script>