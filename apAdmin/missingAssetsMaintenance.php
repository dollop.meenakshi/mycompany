<?php
$id = (int)$_GET['id'];
$mt = $_GET['mt'];
$currentDate = date('Y-m-d');
$q = $d->selectRow("assets_category_master.assets_category,assets_item_detail_master.assets_name,assets_maintenance_master.*,assets_maintenance_last_sync.last_sync_date","assets_category_master,assets_item_detail_master,assets_maintenance_master LEFT JOIN assets_maintenance_last_sync ON assets_maintenance_last_sync.assets_maintenance_id = assets_maintenance_master.assets_maintenance_id AND assets_maintenance_last_sync.assets_id=assets_maintenance_master.assets_id","assets_category_master.assets_category_id=assets_maintenance_master.assets_category_id AND assets_item_detail_master.assets_id=assets_maintenance_master.assets_id AND assets_maintenance_master.society_id='$society_id'");
while ($row = mysqli_fetch_array($q)) {
    if($row['maintenance_type'] == 0){ 
        $custom_date = date("d F Y", strtotime($row['custom_date'])); 
        $to_date = date("d F Y", strtotime($row['last_sync_date'])); 
        $end_date = new DateTime($to_date);
        $start_date = new DateTime($currentDate);
        $diff=date_diff($start_date,$end_date);
        $diff_in_days = $diff->format("%a"); 
        if($row['last_sync_date'] < $currentDate){ 
            $maintenance_schedule_date = date("Y-m-d", strtotime($custom_date)); 
            $addDataArray = array('society_id'=>$society_id,
                                'assets_category_id'=>$row['assets_category_id'],
                                'assets_id'=>$row['assets_id'],
                                'assets_maintenance_id'=>$row['assets_maintenance_id'],
                                'is_maintenance_complete'=>0,
                                'maintenance_schedule_date'=>$maintenance_schedule_date,
                                );
            $q10 = $d->insert("assets_maintenance_complete", $addDataArray);
            $syncDate = array('last_sync_date'=>date("Y-m-d H:i:s"));
            $q11 = $d->update("assets_maintenance_last_sync", $syncDate, "assets_maintenance_id ='$row[assets_maintenance_id]' AND assets_id='$row[assets_id]'");
        } 
    }
    elseif($row['maintenance_type'] == 1){
        $week_day = explode(',', $row['days']);
        $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
        $to_date=date("Y-m-d", strtotime($row['last_sync_date']));
        $begin = new DateTime($to_date);
        $end = new DateTime($currentDate);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
            $day_name = $attDate =$dt->format("w");
            if(in_array($day_name ,$week_day)){
                $weekly_date = $attDate =$dt->format("d F Y");
                $maintenance_schedule_date = date("Y-m-d", strtotime($weekly_date)); 
                $addDataArray = array('society_id'=>$society_id,
                        'assets_category_id'=>$row['assets_category_id'],
                        'assets_id'=>$row['assets_id'],
                        'assets_maintenance_id'=>$row['assets_maintenance_id'],
                        'is_maintenance_complete'=>0,
                        'maintenance_schedule_date'=>$maintenance_schedule_date,
                        );
                $q10 = $d->insert("assets_maintenance_complete", $addDataArray);
                $syncDate = array('last_sync_date'=>date("Y-m-d H:i:s"));
                $q11 = $d->update("assets_maintenance_last_sync", $syncDate, "assets_maintenance_id ='$row[assets_maintenance_id]' AND assets_id='$row[assets_id]'");
            } 
        } 
    }
    elseif($row['maintenance_type'] == 2){
        $month = date('F Y');
        $month_days = explode(',',$row['days']);
        $to_date=date("Y-m-d", strtotime($row['last_sync_date']));
        $begin = new DateTime($to_date);
        $end = new DateTime($currentDate);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
            $month_date = $attDate =$dt->format("d");
            if(in_array($month_date ,$month_days)){ 
                $show_date = $attDate =$dt->format("d F Y");
                $maintenance_schedule_date = date("Y-m-d", strtotime($show_date)); 
                $addDataArray = array('society_id'=>$society_id,
                                    'assets_category_id'=>$row['assets_category_id'],
                                    'assets_id'=>$row['assets_id'],
                                    'assets_maintenance_id'=>$row['assets_maintenance_id'],
                                    'is_maintenance_complete'=>0,
                                    'maintenance_schedule_date'=>$maintenance_schedule_date,
                                    );
                $q10 = $d->insert("assets_maintenance_complete", $addDataArray);
                $syncDate = array('last_sync_date'=>date("Y-m-d H:i:s"));
                $q11 = $d->update("assets_maintenance_last_sync", $syncDate, "assets_maintenance_id ='$row[assets_maintenance_id]' AND assets_id='$row[assets_id]'");
            }
        }
    }
    elseif($row['maintenance_type'] == 3){ 
        $to_date=date("Y-m-d", strtotime($row['last_sync_date']));
        $begin = new DateTime($to_date);
        $end = new DateTime($currentDate);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
            $month_day = $attDate =$dt->format("d");
            if($month_day == $row['days']){ 
                $month_date = $attDate =$dt->format("d F Y");
                $maintenance_schedule_date = date("Y-m-d", strtotime($month_date)); 
                $addDataArray = array('society_id'=>$society_id,
                        'assets_category_id'=>$row['assets_category_id'],
                        'assets_id'=>$row['assets_id'],
                        'assets_maintenance_id'=>$row['assets_maintenance_id'],
                        'is_maintenance_complete'=>0,
                        'maintenance_schedule_date'=>$maintenance_schedule_date,
                        );
                $q10 = $d->insert("assets_maintenance_complete", $addDataArray);
                $syncDate = array('last_sync_date'=>date("Y-m-d H:i:s"));
                $q11 = $d->update("assets_maintenance_last_sync", $syncDate, "assets_maintenance_id ='$row[assets_maintenance_id]' AND assets_id='$row[assets_id]'");
            }
        }
    }
    elseif($row['maintenance_type'] == 4){ 
        $created_year = date("Y", strtotime($row['last_sync_date']));
        $to_date=$created_year.'-'.$row['start_month'].'-'.$row['days'];
        $begin = new DateTime($to_date);
        $end = new DateTime($currentDate);
        $interval = DateInterval::createFromDateString('3 month');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
            $month_day = $dt->format("d");
            if($month_day == $row['days']){ 
                $quarterly_date = $dt->format("d F Y");
                if(date("Y-m-d", strtotime($row['last_sync_date'])) <= date('Y-m-d', strtotime($quarterly_date))){
                    $maintenance_schedule_date = date("Y-m-d", strtotime($quarterly_date)); 
                    $addDataArray = array('society_id'=>$society_id,
                            'assets_category_id'=>$row['assets_category_id'],
                            'assets_id'=>$row['assets_id'],
                            'assets_maintenance_id'=>$row['assets_maintenance_id'],
                            'is_maintenance_complete'=>0,
                            'maintenance_schedule_date'=>$maintenance_schedule_date,
                            );
                    $q10 = $d->insert("assets_maintenance_complete", $addDataArray);
                    $syncDate = array('last_sync_date'=>date("Y-m-d H:i:s"));
                    $q11 = $d->update("assets_maintenance_last_sync", $syncDate, "assets_maintenance_id ='$row[assets_maintenance_id]' AND assets_id='$row[assets_id]'");
                } 
            } 
        }
    }
    elseif($row['maintenance_type'] == 5){ 
        $created_year = date("Y", strtotime($row['last_sync_date']));
        $to_date=$created_year.'-'.$row['start_month'].'-'.$row['days'];
        $begin = new DateTime($to_date);
        $end = new DateTime($currentDate);
        $interval = DateInterval::createFromDateString('6 month');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
            $month_day = $dt->format("d");
            if($month_day == $row['days']){ 
                $half_year_date = $dt->format("d F Y");
                if(date("Y-m-d", strtotime($row['last_sync_date'])) <= date('Y-m-d', strtotime($half_year_date))){
                    $maintenance_schedule_date = date("Y-m-d", strtotime($half_year_date)); 
                    $addDataArray = array('society_id'=>$society_id,
                                'assets_category_id'=>$row['assets_category_id'],
                                'assets_id'=>$row['assets_id'],
                                'assets_maintenance_id'=>$row['assets_maintenance_id'],
                                'is_maintenance_complete'=>0,
                                'maintenance_schedule_date'=>$maintenance_schedule_date,
                                );
                    $q10 = $d->insert("assets_maintenance_complete", $addDataArray);
                    $syncDate = array('last_sync_date'=>date("Y-m-d H:i:s"));
                    $q11 = $d->update("assets_maintenance_last_sync", $syncDate, "assets_maintenance_id ='$row[assets_maintenance_id]' AND assets_id='$row[assets_id]'");
                }
            } 
        }
    }
    elseif($row['maintenance_type'] == 6){ 
        $created_year = date("Y", strtotime($row['last_sync_date']));
        $to_date=$created_year.'-'.$row['start_month'].'-'.$row['days'];
        $begin = new DateTime($to_date);
        $end = new DateTime($currentDate);
        $interval = DateInterval::createFromDateString('1 year');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
            $month_day = $dt->format("d");
            if($month_day == $row['days']){ 
                $year_date = $dt->format("d F Y");
                if(date("Y-m-d", strtotime($row['last_sync_date'])) <= date('Y-m-d', strtotime($year_date))){
                    $addDataArray = array('society_id'=>$society_id,
                                'assets_category_id'=>$row['assets_category_id'],
                                'assets_id'=>$row['assets_id'],
                                'assets_maintenance_id'=>$row['assets_maintenance_id'],
                                'is_maintenance_complete'=>0,
                                'maintenance_schedule_date'=>$maintenance_schedule_date,
                                );
                    $q10 = $d->insert("assets_maintenance_complete", $addDataArray);
                    $syncDate = array('last_sync_date'=>date("Y-m-d H:i:s"));
                    $q11 = $d->update("assets_maintenance_last_sync", $syncDate, "assets_maintenance_id ='$row[assets_maintenance_id]' AND assets_id='$row[assets_id]'");
                }
            }
        }
    }
}
?>

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<h4 class="page-title">Missing Assets Maintenance</h4>
			</div>
		</div>

		<form class="branchDeptFilter" action="" method="get">
			<div class="row pt-2 pb-2">
				<div class="col-md-3 form-group" >
					<select type="text" name="id" id="id" class="form-control single-select" style="width: 100%">
						<option value="">All Category</option>
						<?php $q12 = $d->select("assets_category_master", "", "order by assets_category ASC");
						while ($row12 = mysqli_fetch_array($q12)) { ?>
						<option value="<?php echo $row12['assets_category_id']; ?>" <?php if ($id == $row12['assets_category_id']) {echo "selected";} ?>><?php echo $row12['assets_category']; ?> </option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3 form-group" >
					<select class="form-control single-select" name="mt">
						<option <?php if(isset($mt) && $mt == 'all'){echo 'selected';} ?> value="all">All Maintenance Type</option>
						<option <?php if(isset($mt) && $mt == '0'){echo 'selected';} ?> value="0">Custom Date</option>
						<option <?php if(isset($mt) && $mt == '1'){echo 'selected';} ?> value="1">Weekly</option>
						<option <?php if(isset($mt) && $mt == '2'){echo 'selected';} ?> value="2">Month Days</option>
						<option <?php if(isset($mt) && $mt == '3'){echo 'selected';} ?> value="3">Monthly</option>
						<option <?php if(isset($mt) && $mt == '4'){echo 'selected';} ?> value="4">Quarterly</option>
						<option <?php if(isset($mt) && $mt == '5'){echo 'selected';} ?> value="5">Half Yearly</option>
						<option <?php if(isset($mt) && $mt == '6'){echo 'selected';} ?> value="6">Yearly</option>
					</select>
				</div>
				<div class="col-md-3 form-group">
					<input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-bordered">
								<thead>
									<tr>
										<th>S. No</th>
										<th>Category</th>
										<th>Item Name</th>
										<th>Maintenance Type</th>
										<th>Maintenance Date</th>
										<th>Vendor Name</th>
										<th>Vendor Mobile</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$counter = 1;
									if (isset($id) && $id > 0) {
										$appendQery = " AND assets_item_detail_master.assets_category_id='$id'";
									}
									if (isset($mt) && $mt != 'all') {
										$typeAppendQery = " AND assets_maintenance_master.maintenance_type='$mt'";
									}
									
									$q = $d->select("assets_category_master,assets_item_detail_master,assets_maintenance_master,assets_maintenance_complete","assets_category_master.assets_category_id=assets_maintenance_complete.assets_category_id AND assets_item_detail_master.assets_id=assets_maintenance_complete.assets_id AND assets_maintenance_complete.assets_maintenance_id=assets_maintenance_master.assets_maintenance_id AND assets_maintenance_complete.society_id='$society_id' AND assets_maintenance_complete.is_maintenance_complete=0  $appendQery $typeAppendQery");
									while ($row = mysqli_fetch_array($q)) { ?>
										<tr>
											<td><?php echo $counter++; ?></td>
											<td><?php echo $row['assets_category']; ?></td>
											<td><?php echo $row['assets_name']; ?></td>
											<td><?php if($row['maintenance_type'] == 0){
													echo 'Custom Date';
												}
												elseif($row['maintenance_type'] == 1){
													echo 'Weekly';
												}
												elseif($row['maintenance_type'] == 2){
													echo 'Month Days';
												}
												elseif($row['maintenance_type'] == 3){
													echo 'Monthly';
												}
												elseif($row['maintenance_type'] == 4){
													echo 'Quarterly';
												}
												elseif($row['maintenance_type'] == 5){
													echo 'Half Yearly';
												}
												elseif($row['maintenance_type'] == 6){
													echo 'Yearly';
												}
										 	?></td>
                                            <td><?php echo $row['maintenance_schedule_date']; ?></td>
											<td><?php echo $row['vendor_name']; ?></td>
											<td><?php echo $row['vendor_mobile_no']; ?></td>
											<td>
                                                <div class="d-flex align-items-center">
                                                    <button class="btn btn-primary btn-sm" title="Complete Maintenance" onclick="markAsMaintenanceCompleted('<?php echo $row['assets_maintenance_complete_id'] ?>');"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                </div>
                                            </td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="markAsMaintenanceCompletedModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Complete Assets Maintenance</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                    <form id="completeAssetsMaintenanceForm" action="controller/assetsItemDetailsController.php" name="changeAttendance"  enctype="multipart/form-data" method="post">
                        <div class="form-group row">
                            <label for="input-10" class="col-sm-12 col-form-label">Amount <span class="text-danger">*</span></label>
                            <div class="col-lg-12 col-md-12" id="">
                            <input type="text" required class="form-control onlyNumber" name="maintenance_amount" id="maintenance_amount" placeholder="Maintenance Amount">
                            </div>                   
                        </div>
                        <div class="form-group row">
                            <label for="input-10" class="col-sm-12 col-form-label">Remark </label>
                            <div class="col-lg-12 col-md-12" id="">
                            <textarea class="form-control" name="remark"></textarea>
                            </div>                   
                        </div>            
                        <div class="form-footer text-center">
                            <input type="hidden" name="assets_maintenance_complete_id" id="assets_maintenance_complete_id"  value="">
                            <input type="hidden" name="markAsMaintenanceCompleted"  value="markAsMaintenanceCompleted">
                            <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>