<?php error_reporting(0);
  
  error_reporting(0);
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $_GET['month_year']= $_REQUEST['month_year'];
  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-12">
          <h4 class="page-title">Pending Attendance (Out Of Geo Facing Range)</h4>
        </div>
        <div class="col-sm-3 col-9">
          <div class="btn-group float-sm-right">
              <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
            <!--  <a href="addAttendanceType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
              
              <a href="javascript:void(0)" onclick="DeleteAll('deleteAttendanceType');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
            -->
          </div>
        </div>
     </div>
     <form action="" class="branchDeptFilter" >
      <div class="row pt-2 pb-2">
        <?php include('selectBranchDeptEmpForFilterAll.php'); ?>
        <div class="col-md-2 col-6 form-group">
          <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-31 days'));} ?>">  
        </div>
        <div class="col-md-2 col-6 form-group">
          <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d');} ?>">  
        </div>
          <div class="col-md-3 form-group mt-auto">
              <input class="btn btn-success btn-sm " type="submit" name="getReport"  value="Get Data">
          </div>    
      </div>
      </form>
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                  <?php 
                        $i=1;
                        if (isset($bId) && $bId> 0) {
                          $BranchFilterQuery = " AND users_master.block_id='$bId]'";
                        }
                        if(isset($_GET['dId']) && $_GET['dId']>0) {
                          $deptFilterQuery = " AND users_master.floor_id='$_GET[dId]'";
                        }
                        else
                        {
                          $limitSql = 'LIMIT 100';
                        }
                       
                        if(isset($from) && isset($from) && $toDate != '' && $toDate) {
                          $dateFilterQuery = " AND attendance_master.attendance_date_start BETWEEN '$from' AND '$toDate'";
                        }
                        if(isset($uId) && $uId>0) {
                          $userFilterQuery = "AND attendance_master.user_id='$uId'";
                        }
                        if(isset($_GET['key'])  && $_GET['key'] == 'pending') {
                          $todayDate = date('Y-m-d');
                          $todayDateFilterQuery = " AND attendance_master.attendance_date_start= '$todayDate'";
                        }
                        $q=$d->select("attendance_master LEFT JOIN shift_timing_master ON shift_timing_master.shift_time_id = attendance_master.shift_time_id,users_master LEFT JOIN floors_master ON floors_master.floor_id=users_master.floor_id LEFT JOIN block_master ON block_master.block_id=users_master.block_id","users_master.user_id=attendance_master.user_id AND attendance_master.society_id='$society_id' AND attendance_master.attendance_status=0 AND users_master.delete_status=0 AND attendance_master.punch_in_request!=1  $BranchFilterQuery $deptFilterQuery $dateFilterQuery $userFilterQuery $todayDateFilterQuery $blockAppendQueryUser","ORDER BY attendance_master.attendance_id DESC $limitSql");
                        $counter = 1;
                        //if(isset($_GET['dId'])){
                        ?>
                  <table id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>                        
                            <th>Status</th>                      
                            <th>Action</th>
                            <th>Employee Name</th>
                            <th>Department</th>
                            <th>Punch In</th>
                            <th>Start Date</th>                      
                            <th>Punch Out</th>
                            <th>End Date</th>   
                            <th>Hours</th>
                            <th>In Branch</th>
                            <th>Out Branch</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      while ($data=mysqli_fetch_array($q)) {
                      ?>
                        <tr>
                          <td><?php echo $counter++; ?></td>
                          <td>
                              
                              <?php if($data['attendance_status'] == 0){ 
                               
                                  if($data['attendance_range_in_km'] !=""){
                                    $data['attendance_range_in_km']=$data['attendance_range_in_km'];
                                  }
                                  else
                                  {
                                    $data['attendance_range_in_km']=0;
                                  }
                                  if($data['attendance_range_in_meter'] !="0.00"){
                                    $data['attendance_range_in_meter']=$data['attendance_range_in_meter'];
                                  }
                                  else
                                  {
                                    $data['attendance_range_in_meter']=0;
                                  }
                                  if($data['punch_out_latitude'] !="")
                                  {
                                    $out_lat=$data['punch_out_latitude'];
                                  }else
                                  {
                                    $out_lat=0;

                                  }
                                  if($data['punch_out_longitude'] !="")
                                  {
                                    $out_lng=$data['punch_out_longitude'];
                                  }else
                                  {
                                    $out_lng=0;

                                  }
                                  ?>
                                <button title="Approve Attendance" type="button" class="btn btn-sm btn-primary ml-1" onclick="pendingAttendanceStatusChange(1, '<?php echo $data['attendance_id']; ?>', 'attendaceStatus','<?php echo $data['punch_in_latitude']; ?>', '<?php echo $data['punch_in_longitude']; ?>','<?php echo $data['block_id']; ?>','<?php echo $data['attendance_range_in_meter']?>','<?php echo $data['attendance_range_in_km']?>','<?php echo $data['punch_in_image']?>','<?php echo $out_lat; ?>','<?php echo $out_lng?>','<?php echo $data['punch_out_image'];?>')"><i class="fa fa-check"></i></button>
                                <button title="Decline Attendance" type="button" class="btn btn-sm btn-danger ml-1" onclick="pendingAttendanceStatusChange(2, '<?php echo $data['attendance_id']; ?>', 'attendaceStatus','<?php echo $data['punch_in_latitude']; ?>', '<?php echo $data['punch_in_longitude']; ?>','<?php echo $data['block_id']; ?>','<?php echo $data['attendance_range_in_meter']?>','<?php echo $data['attendance_range_in_km']?>','<?php echo $data['punch_in_image']?>','<?php echo $out_lat; ?>','<?php echo $out_lng?>','<?php echo $data['punch_out_image'];?>')"><i class="fa fa-times"></i></button>
                              <?php } else if($data['attendance_status'] == 1){ ?>
                              <span class="badge badge-pill badge-success m-1">Approved </span>
                              <?php }else{ ?>
                                <span class="badge badge-pill badge-danger m-1">Declined </span>
                              <?php }  ?>
                          </td>
                          <td>
                          <div class="d-flex align-items-center">
                          <button type="button" class="btn btn-sm btn-primary ml-2" onclick="viewUserCheckInOutImage('<?php echo $data['attendance_id']; ?>')"> <i class="fa fa-picture-o"></i> <i class="fa fa-map-marker"></i></button>

                              <button type="button" class="btn btn-sm btn-primary ml-2" onclick="allAttendaceSet(<?php echo $data['attendance_id']; ?>)"> <i class="fa fa-eye"></i></button>
                                <input type="hidden" name="user_id_attendace" value="<?php if($data['user_id']!=""){ echo $data['user_id'];}   ?>">
                                 <!--  <a   href="attendanceCalendar?uid=<?php if($data['user_id']!=""){ echo $data['user_id'];} ?>">
                                    <button type="button" class="btn btn-sm btn-primary ml-2" > 
                                      <i class="fa fa-calendar"></i>
                                    </button>
                                  </a> -->
                              </div>
                          </td>
                          <td><?php echo $data['user_full_name']." (".$data['user_designation']." )"; ?></td>
                          <td><?php echo $data['floor_name']." (".$data['block_name']." )"; ?></td>
                          <td><?php if($data['punch_in_time']!='00:00:00' && $data['punch_in_time']!='null') { echo date("h:i A", strtotime($data['punch_in_time'])); }?>
                          <?php if ($data['punch_in_image']!='') { ?>
                         <?php } ?>
                          <?php if($data['punch_in_latitude']!='' & $data['punch_in_longitude']!=''){ ?>
                          <?php } ?>
                          </td>
                          
                          <td><?php if($data['attendance_date_start']!='0000-00-00' && $data['attendance_date_start']!='null') { echo date("d M Y", strtotime($data['attendance_date_start'])); }?></td>
                          
                          <td><?php if($data['punch_out_time']!='00:00:00' && $data['punch_out_time']!='null') { echo date("h:i A", strtotime($data['punch_out_time'])); }?>
                          <?php if ($data['punch_out_image']!='') { ?>
                          <?php } ?>
                          <?php if($data['punch_out_latitude']!='' & $data['punch_out_longitude']!=''){ ?>
                         <?php } ?>
                          </td>
                          
                          <td><?php if($data['attendance_date_end']!='0000-00-00' && $data['attendance_date_end']!='null') { echo date("d M Y", strtotime($data['attendance_date_end'])); }?></td>
                          <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_out_time'] != '00:00:00' && $data['total_working_hours']=='') {
                            echo $d->getTotalHours($data['attendance_date_start'],$data['attendance_date_end'],$data['punch_in_time'],$data['punch_out_time']);
                              } else {
                                echo $data['total_working_hours'];
                              } ?>
                          </td>
                          <td><?php  echo $data['punch_in_branch'];  ?></td>
                          <td><?php  echo $data['punch_out_branch'];  ?></td>
                          
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
               <!--  <?php //} else {  ?>
                <div class="" role="alert">
                  <span><strong>Note :</strong> Please Select Department</span>
                  </div>
                <?php //} ?> -->
            </div>
            </div>
          </div>
        </div>
     </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>

<div class="modal fade" id="ChanngeAttendaceDataModal">
    <div class="modal-dialog ">
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Update Attendance</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body">
            <form id="changeAttendance" name="changeAttendance" enctype="multipart/form-data" method="post">
              <div class="form-group row hidePunchDateForShift" id="hidePunchDateForShift">
                <label for="input-10" class="col-sm-6 col-form-label">Punch Out Date <span class="text-danger">*</span></label>
                <div class="col-lg-6 col-md-6" id="">
                  <select type="text" readonly maxlength="10" style="text-transform:uppercase;" name="punch_out_date" id="punch_out_date" class="form-control punch_out_date">
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-6 col-form-label">Punch Out Time <span class="text-danger">*</span></label>
                <div class="col-lg-6 col-md-6" id="">
                  <input type="text" class="form-control time-picker-shft-Out_update" readonly id="punch_out_time" name="punch_out_time">
                </div>
              </div>
              <div class="form-footer text-center">
                <input type="hidden" name="updateUserAttendance" value="updateUserAttendance">
                <input type="hidden" name="attendance_id" id="attendance_id">
                <input type="hidden" class="indate" name="indate" id="indate">
                <button id="addHrDocumentBtn" type="submit" onclick="updateUserAttendanceData()" class="btn btn-success addUserBank hideupdate"><i class="fa fa-check-square-o"></i> Update & Approve</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="addAttendaceModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Attendance Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
          <div class="card-body">
            <div class="row col-md-12 ">
              <div class="table-responsive">
                <table  class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody id="attendanceData">
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row col-md-12 multiPunchInTable" style="display: none;">
              <table class="table table-bordered">
                <thead>
                  <th>Punch In</th>
                  <th>Punch Out</th>
                  <th>Hours</th>
                </thead>
                <tbody id="MulitPunchIn">
                </tbody>
              </table>
            </div>
            <div class="row col-md-12 wrkRpt">

            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="UserCheckInLocation">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Attendance Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
        <div class="modal-body" style="align-content: center;">
            <div class="" > 
                <div class="row">
                <div class="col-md-6 col-12 out_reason_hide">
                  <div><b>Punch In :</b> <span id="approvInTime"></span></div>
                  <div><b>Distance :</b> <span class="InawayShow"></span></div>
                  <div class="in_reason_div"><b>In Reason :</b> <span id="in_reason"></span></div>
                </div>
                <div class="col-md-6 col-12 out_reason_hide">
                  <div><b>Punch out :</b> <span id="approvOutTime"></span></div>
                  <div><b>Distance :</b> <span class="away"></span></div>
                  <div class="out_reason_div"><b>Out Reason :</b> <span id="out_reason"></span></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-12 text-center">
                  <div class="punchInImage" id=""></div>
                </div>
                <div class="col-md-6 col-12  text-center">
                  <div class="punchOutImage" id=""></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-12 text-center mapDiv">
                  <div class="map" id="aprovePunchOut" style="width: 100%; height: 300px;"></div>
                </div>
                
              </div>
              <div class="col-md-12">
                <form id="attendanceDeclineReasonForm">
                    <div class="col-md-12 attendanceDeclineReason">
                      <label  class="form-control-label">Decline Reason <span class="text-danger">*</span> </label>
                        <textarea class="form-control" required name="attendance_declined_reason" id="attendance_declined_reason"></textarea>
                    </div>
                    <div class="form-footer text-center">
                      <input type="hidden" name="value" id="value">
                      <input type="hidden" name="id" id="id">
                      <input type="hidden" name="status" id="status">
                        <button type="submit" class="btn btn-primary declineCLass attendanceDeclineReasonClass "  >Change Status</button>
                    </div>
                </form>
              </div>
            </div>
        </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="UserCheckInOutImage">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white">Attendance Details</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body p-0">
               <div class="row">
              <div class="col-md-6">
                <div><b>Punch In :</b> <span id="in_time"></span></div>
                <div ><b>Distance:</b> <span class="Inkm"></span></div>
                <div ><b>Address:</b> <span class="InAddress"></span></div>
              </div>
              <div class="col-md-6">
                <div><b>Punch Out :</b> <span id="out_time"></span></div>
                <div ><b>Distance:</b> <span class="Outkm"></span></div>
                <div ><b>Address:</b> <span class="OutAddress"></span></div>
               
              </div>
            </div>
             <div class="row">
                <div class="col-md-6 col-6 text-center hideInData">
                  <div class="userCheckInOutImageData " id="userCheckInOutImageData">
                  </div>
                </div>
                <div class="col-md-6 col-6 text-center hideOutData">
                  <div class="userCheckOutImageData " id="userCheckOutImageData">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-12 float-left mapDiv ">
                  <div class="map mr-1" id="map3" style="width: 100%; height: 280px;">
                  </div>
                </div>
                
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
<!-- <div class="modal fade" id="UserCheckInOutImage">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Attendance Data</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" style="align-content: center;">
          <div class="card-body" > 
          <div class="row col-md-12  punchInImage" id="userCheckInOutImageData"></div>
          </div>
        </div>
    </div>
  </div>
</div> -->

<div class="modal fade" id="attendanceDeclineReasons">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Declined Reason</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="" style="align-content: center;">
<!--         <textarea class="form-control" name="attendance_declined_reason" id="attendance_declined_reason"></textarea>
 -->      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary attendanceDeclineReasonClass "  data-dismiss="modal" aria-label="Close">Save changes</button>
      </div>

    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>

<script type="text/javascript">
function initialize(d_lat,d_long,blockRange,blockLat,blockLOng,out_lat,out_lng)
{
    
    var latlng = new google.maps.LatLng(d_lat,d_long);
    var Blatlng = {lat: parseFloat(blockLat), lng: parseFloat(blockLOng)};
    
    var latitute = d_lat;
    var longitute = d_long;
   
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
     ////User marker
    var marker = new google.maps.Marker({
      map: map,
      icon:"assets/images/placeholder.png",
      position: latlng,
     // draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });
    /////////////////out marker
    if(out_lat !=0 && out_lng !=0){
      var outLatLng = {lat: parseFloat(out_lat), lng: parseFloat(out_lng)};
      var Outmarker = new google.maps.Marker({
      map: map,
      icon:"assets/images/blue-dot.png",
      position: outLatLng,
     // draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });

    }
   ////Company marker
    var marker2 = new google.maps.Marker({
      map: map,
      
      position: Blatlng,
    //  draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });
     ////Company circle
    var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange),    // 10 miles in metres
        fillColor: '#AA0000'
      });
    circle.bindTo('center', marker2, 'position');
    var parkingRadition = 5;
    var citymap = {
        newyork: {
          center: {lat: latitute, lng: longitute},
          population: parkingRadition
        }
    };
   
  
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function()
    {
      geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
      {
        if (status == google.maps.GeocoderStatus.OK)
        {
          if (results[0])
          {
            var places = results[0];
            var pincode="";
            var serviceable_area_locality= places.address_components[4].long_name;
            for (var i = 0; i < places.address_components.length; i++)
            {
              for (var j = 0; j < places.address_components[i].types.length; j++)
              {
                if (places.address_components[i].types[j] == "postal_code")
                {
                  pincode = places.address_components[i].long_name;
                }
              }
            }
            bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
          }
        }
      });
    });
  }


    function initializeCommonMap(in_lat, in_long,out_lat,out_long) {

      var latlng = new google.maps.LatLng(in_lat, in_long);
      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize3(d_lat, d_long) {
       
      var latlng = new google.maps.LatLng(d_lat, d_long);
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlng,
        zoom: 13
      });
      ////out marker
   

       ////in marker
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function initialize2(out_lat, out_long) {

      var latlngOut = new google.maps.LatLng(out_lat, out_long);
     
      var map = new google.maps.Map(document.getElementById('map3'), {
        center: latlngOut,
        zoom: 13
      });
      ////out marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlngOut,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));


      var infowindow = new google.maps.InfoWindow();
      
    }

    function mapViewCommon(d_lat, d_long, blockRange, blockLat, blockLOng,in_latitude,out_longitude) {

      var labels = 'C';
      var labelIndex = 0;

      var latlngIn = new google.maps.LatLng(in_latitude, out_longitude);
      var latlng = new google.maps.LatLng(d_lat, d_long);
      var Blatlng = {
        lat: parseFloat(blockLat),
        lng: parseFloat(blockLOng)
      };
      var latitute = d_lat;
      var longitute = d_long;

      var map = new google.maps.Map(document.getElementById('aprovePunchOut'), {
        center: latlng,
        zoom: 13
      });
      ////User marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlng,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));

      var marker1 = new google.maps.Marker({
        map: map,
        position: latlngIn,
        label: labels['0'],
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));

      ////Company marker
      var marker2 = new google.maps.Marker({
        map: map,
        position: Blatlng,
        label: labels['0'],
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker2, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Work Location");
            infowindow.open(map, marker2);
          }
      })(marker2));

      ////Company circle
      var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange), // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker2, 'position');
      var parkingRadition = 5;
      var citymap = {
        newyork: {
          center: {
            lat: blockLat,
            lng: blockLOng
          },
          population: parkingRadition
        }
      };
      
      var infowindow = new google.maps.InfoWindow();

    }

    function mapViewIn(in_latitude,in_longitude,blockRange,blockLat, blockLOng) {

      var labels = 'C';
      var labelIndex = 0;

      var latlngIn = new google.maps.LatLng(in_latitude, in_longitude);
      
      var Blatlng = {
        lat: parseFloat(blockLat),
        lng: parseFloat(blockLOng)
      };
      
      var map = new google.maps.Map(document.getElementById('aprovePunchOut'), {
        center: latlngIn,
        zoom: 13
      });
      ////User marker

      var marker1 = new google.maps.Marker({
        map: map,
        position: latlngIn,
        label: labels['0'],
        icon: "../img/green-dot.png",
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker1, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch In Location");
            infowindow.open(map, marker1);
          }
      })(marker1));

      ////Company marker
      var marker2 = new google.maps.Marker({
        map: map,
        position: Blatlng,
        label: labels['0'],
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker2, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Work Location");
            infowindow.open(map, marker2);
          }
      })(marker2));

      ////Company circle
      var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange), // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker2, 'position');
      var parkingRadition = 5;
      var citymap = {
        newyork: {
          center: {
            lat: blockLat,
            lng: blockLOng
          },
          population: parkingRadition
        }
      };
      
      var infowindow = new google.maps.InfoWindow();

    }

    function mapViewOut(d_lat, d_long, blockRange, blockLat, blockLOng,in_latitude,out_longitude) {

      var labels = 'C';
      var labelIndex = 0;

      var latlng = new google.maps.LatLng(d_lat, d_long);
      var Blatlng = {
        lat: parseFloat(blockLat),
        lng: parseFloat(blockLOng)
      };
     
      var map = new google.maps.Map(document.getElementById('aprovePunchOut'), {
        center: latlng,
        zoom: 13
      });
      ////User marker
      var marker = new google.maps.Marker({
        map: map,
        icon: "../img/red-dot.png",
        position: latlng,
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Punch Out Location");
            infowindow.open(map, marker);
          }
      })(marker));

      ////Company marker
      var marker2 = new google.maps.Marker({
        map: map,
        position: Blatlng,
        label: labels['0'],
        anchorPoint: new google.maps.Point(0, -29)
      });

      google.maps.event.addListener(marker2, 'click', (function(marker) {
          return function() {
            infowindow.setContent("Work Location");
            infowindow.open(map, marker2);
          }
      })(marker2));

      ////Company circle
      var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(blockRange), // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker2, 'position');
      var parkingRadition = 5;
      var citymap = {
        newyork: {
          center: {
            lat: blockLat,
            lng: blockLOng
          },
          population: parkingRadition
        }
      };
      
      var infowindow = new google.maps.InfoWindow();

    }

<?php
 if(!isset($_GET['month_year']))
 { ?>
    $('#month_year').val(<?php echo $currentMonth;?>)
<?php }

?>

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
      }
      return false;
    }

</script>
<style>
.attendance_status
{  
  min-width: 113px;
}
</style>
