<?php
$floor_id = (int)$_GET['floor_id'];
$dId = (int)$_REQUEST['dId'];
$bId = (int)$_REQUEST['bId'];
error_reporting(0);
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_GET['toDate']) != 1) {
    $_SESSION['msg1'] = "Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employeeExpenseReport';
        </script>");
  }
}
$q2 = $d->selectRow("users_master.app_version_code,users_master.device", "users_master", "delete_status=0 AND  users_master.app_version_code !='' GROUP BY users_master.app_version_code $blockAppendQueryUser");
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">App version Report</h4>
      </div>
    </div>
    <!-- End Breadcrumb-->
   
    <div class="row mt-1">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <?php
              extract(array_map("test_input", $_GET));
             
              $i = 1;
              ?>
              <table id="<?php if ($adminData['report_download_access'] == 0) {
                            echo 'exampleReportWithoutBtn';
                          } else {
                            echo 'exampleReport';
                          } ?>" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Device</th>
                    <th>Version</th>
                    <th>Employee</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  while ($data = mysqli_fetch_array($q2)) {
                    $totalShift = $d->count_data_direct("user_id", "users_master", "delete_status=0 AND app_version_code='$data[app_version_code]' $blockAppendQueryUser");
                  ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $data['device']; ?> </td>
                      <td><?php echo $data['app_version_code']; ?> </td>
                      <td> <a href="employeeAppReportDetail?vrId=<?php echo $data['app_version_code']; ?>"><?php echo $totalShift;  ?></a></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->
  </div>
  <!-- End container-fluid-->
</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->