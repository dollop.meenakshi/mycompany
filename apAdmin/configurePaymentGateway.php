<?php
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI'];
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
if (isset($_COOKIE['bms_admin_id'])) {
  $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
  $society_id = $_COOKIE['society_id'];
}

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

extract(array_map("test_input" , $_POST));


 $ch = curl_init();
       
curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/contact_fincasysteam_controller.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "getPaymentGetwatRequestDetails=getPaymentGetwatRequestDetails&society_id=$society_id&requiest_id=$requiest_id  ");

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'key: bmsapikey'
));

$server_output = curl_exec($ch);
curl_close ($ch);
$server_output=json_decode($server_output,true);
$totalRq = count($server_output['payment_gateway_requiest']);


?>
    <form id="paymentGatewayFrmAdd" action="controller/buildingController.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
      
       <div class="form-group row">
        <label for="payment_getway_master_id" class="col-sm-4 col-form-label">Payment Gateway Company <i class="text-danger">*</i></label>
        <div class="col-sm-8">
          <select required="" id="paymentCompanyChange" name="payment_getway_master_id" class="form-control">
           <?php 
           $i=1;
           $q=$d->select("payment_getway_master","payment_getway_name='$server_output[payment_getway_name]'");
           while ($data=mysqli_fetch_array($q)) {
             ?>
            <option value="<?php echo $data['payment_getway_master_id']; ?>"><?php echo $data['payment_getway_name']; ?></option>
          <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        
        <label for="name" class="col-sm-4 col-form-label">Name  <i class="text-danger">*</i></label>
        <div class="col-sm-8">
         <input maxlength="60" type="text" required="" class="form-control" id="name"  value="<?php echo $server_output['payment_getway_name'];?> Payment Gateway"  name="name">
        </div>
      </div>
      <?php  if($server_output['payment_getway_name']=='Payumoney') { ?>
        <div class="form-group row">
          
          <label for="merchant_id" class="col-sm-4 col-form-label">Merchant Id <i class="text-danger">*</i></label>
          <div class="col-sm-8">
           <input readonly maxlength="60" type="text" required=""  minlength="3" class="form-control" id="merchant_id" value="<?php echo $server_output['merchant_id'];?>"  name="merchant_id">
          </div>
        </div>
         <div class="form-group row">
          <label for="merchant_key" class="col-sm-4 col-form-label">Merchant Key <i class="text-danger">*</i></label>
          <div class="col-sm-8">
            <input readonly maxlength="250"  minlength="3" type="text" required="" class="form-control" id="merchant_key" value="<?php echo $server_output['merchant_key'];?>"  name="merchant_key"  >
          </div>
        </div>
        

        <div class="form-group row">
         
      
          <label for="salt_key" class="col-sm-4 col-form-label">Salt Key <i class="text-danger">*</i></label>
          <div class="col-sm-8">
            <input readonly maxlength="250" type="text" required=""  minlength="3" class="form-control" value="<?php echo $server_output['salt_key'];?>"  id="salt_key" name="salt_key"   >
          </div>
           
        </div>
        <div class="form-group row">
          <label for="merchant_key" class="col-sm-4 col-form-label">Transaction Charges % </label>
          <div class="col-sm-8">
            <input  maxlength="5" type="text" max="10"   class="form-control onlyNumber" inputmode="numeric" id="transaction_charges" name="transaction_charges"  >
          </div>
        </div>
      <?php } else  if($server_output['payment_getway_name']=='Razorpay') { ?>
        <div class="form-group row">
          
          <label for="merchant_id" class="col-sm-4 col-form-label">Razor Pay keyId <i class="text-danger">*</i></label>
          <div class="col-sm-8">
           <input readonly maxlength="250" minlength="3" type="text" required="" class="form-control" id="merchant_id" value="<?php echo $server_output['merchant_id'];?>"  name="RozerPaykeyId">
          </div>
        </div>
         <div class="form-group row">
          <label for="merchant_key" class="col-sm-4 col-form-label">Razor Pay Secret key <i class="text-danger">*</i></label>
          <div class="col-sm-8">
            <input readonly maxlength="250" type="text" required=""  minlength="3" class="form-control" id="merchant_key" value="<?php echo $server_output['merchant_key'];?>"  name="SecretKey"  >
          </div>
        </div>
        <div class="form-group row">
          <label for="merchant_key" class="col-sm-4 col-form-label">Transaction Charges % </label>
          <div class="col-sm-8">
            <input maxlength="5" type="text" max="10"  class="form-control onlyNumber" inputmode="numeric" id="transaction_charges" name="transaction_charges_razer"  >
          </div>
        </div>
        
       <?php } else { ?>
        <div class="form-group row">
          
          <label for="merchant_id" class="col-sm-4 col-form-label">UPI Id <i class="text-danger">*</i></label>
          <div class="col-sm-8">
           <input readonly maxlength="250" type="text"  minlength="3" required="" class="form-control" value="<?php echo $server_output['merchant_id'];?>"  id="merchant_id"   name="UpiId">
          </div>
        </div>
        <div class="form-group row">
          <label for="merchant_key" class="col-sm-4 col-form-label">Merchant Account Name <i class="text-danger">*</i></label>
          <div class="col-sm-8">
            <input  readonly maxlength="250" type="text"   minlength="3" required="" class="form-control" value="<?php echo $server_output['merchant_key'];?>"  id="merchant_key" name="MerchantAccount"  >
          </div>
        </div>

      <?php } ?>
      

      <div class="form-footer text-center">
        <input type="hidden" name="addPaymentGetwat" value="addPaymentGetwat">
        
        <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i>Add</button>
      </div>
    </form>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  jQuery(document).ready(function($){
    
    $.validator.addMethod("noSpace", function(value, element) {
      return value == '' || value.trim().length != 0;
    }, "No space please and don't leave it empty");
      
      $("#paymentGatewayFrmAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            payment_getway_master_id: {
                required: true
            },
            name: {
                required: true,
                noSpace:true
            },
            merchant_id: {
                required: true,
                noSpace:true
            },
            merchant_key:{
                required: true,
                noSpace:true
            },
            salt_key:{
                required: true,
                noSpace:true
            }       
          },
        messages: {
            payment_getway_master_id: {
                required : "Please select payment gateway"
            },
             name: {
                required : "Please enter payment gateway name"
            },
            merchant_id: {
                required : "Please enter merchant id"
            },
            merchant_key: {
                required : "Please enter merchant key"
            },
            salt_key: {
                required : "Please enter salt key"
            }   
        }
        , submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
        }
    });


  });

  $(".photoOnly").change(function () {
     // alert(this.files[0].size);
     var fileExtension = ['jpeg', 'jpg', 'png'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
        swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
        $('.photoOnly').val('');
    }
});
//IS_1130
  $(".onlyNumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function calcNewPriceFinal() {
  var currency =$('#currency').val();
  var penalty_amount = parseFloat($('#penalty_amount').val());
  var org_penalty_amount = parseFloat($('#org_penalty_amount').val());
  if(org_penalty_amount<penalty_amount) {
    var difAmount = parseFloat(penalty_amount-org_penalty_amount).toFixed(2);
    $('#walletMsg').html(currency+' '+difAmount +' Credited in user wallet for overpaid');
  } else {
      $('#walletMsg').html('');
  }
}
</script>
