

<!-- Start wrapper-->
<div id="wrapper" >
 
 <div class="container">
         <div class="row">
             <div class="col-md-12">
                 <div class="text-center error-pages">
                     <h1 class="error-title  <?php if ($_SESSION['error_code']==403) { echo 'text-primary'; } else  { echo 'text-danger';} ?>"> <?php echo $_SESSION['error_code']; ?></h1>
                     <h2 class="error-sub-title  <?php if ($_SESSION['error_code']==403) { echo 'text-primary'; } else  { echo 'text-danger';} ?>">
                     <?php if ($_SESSION['error_code']==403) {
                         echo "Forbidden";
                     } else {
                         echo "page not found";
                     } ?>
                     </h2>

                     <p class="error-message  text-uppercase"><?php echo $_SESSION['errorMsg']; ?></p>
                     
                     <div class="mt-4">
                       <a href="welcome" class="btn btn-primary btn-round shadow-primary m-1">Go To Home </a>
                       
                     </div>

                     
                        <hr class="w-50 border-light-2">
                     <br><br><br><br><br><br><br><br><br><br>
                 </div>
             </div>
         </div>
     </div>

</div><!--wrapper-->

