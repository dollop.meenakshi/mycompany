<style>
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
-webkit-appearance: none;
margin: 0;
}
</style>
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" /> -->
<?php
extract(array_map("test_input", $_POST));
if(isset($edit_expense))
{
    $q = $d->selectRow("ue.*,um.block_id","user_expenses AS ue JOIN users_master AS um ON um.user_id = ue.user_id","user_expense_id = '$user_expense_id'");
    $data = $q->fetch_assoc();
    extract($data);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row  ">
            <div class="col-sm-6">
                <?php
                if(isset($edit_expense))
                {
                    $a = 4;
                    $t = 4;
                ?>
                <h4 class="page-title">Edit Expense</h4>
                <?php
                }
                else
                {
                    $a = 3;
                    $t = 4;
                ?>
                <h4 class="page-title">Add Multiple Expenses</h4>
                <?php
                }
                ?>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="addExpenseForm" action="controller/ExpenseSettingController.php" enctype="multipart/form-data" method="post">
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Branch<span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <select id="block_id" name="block_id" class="form-control inputSl single-select" onchange="getFloorByBlockIdAddSalary(this.value)" required="">
                                        <option value="">Select Branch</option>
                                        <?php
                                        $qd = $d->select("block_master", "block_master.society_id='$society_id' AND block_master.block_status=0 $blockAppendQueryOnly ");
                                        while ($depaData = mysqli_fetch_array($qd)) {
                                        ?>
                                        <option <?php if ($data['block_id'] == $depaData['block_id']) { echo 'selected';} ?> <?php if ($block_id == $depaData['block_id']) {echo 'selected';} ?> value="<?php echo  $depaData['block_id']; ?>"><?php echo $depaData['block_name']; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label for="input-10" class="col-sm-2 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <select type="text" id="floor_id" required="" onchange="getUserByFloorId(this.value)" class="form-control single-select floor_id inputSl" name="floor_id">
                                        <option value="">-- Select --</option>
                                        <?php
                                        if (isset($data)) {
                                        $floors = $d->select("floors_master,block_master", "block_master.block_id =floors_master.block_id AND  floors_master.society_id='$society_id' AND floors_master.block_id = '$data[block_id]' $blockAppendQuery");
                                        while ($floorsData = mysqli_fetch_array($floors)) {
                                        ?>
                                        <option <?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] == $data['floor_id']) {
                                            echo "selected";
                                            } ?> value="<?php if (isset($floorsData['floor_id']) && $floorsData['floor_id'] != "") {
                                            echo $floorsData['floor_id'];
                                            } ?>"> <?php if (isset($floorsData['floor_name']) && $floorsData['floor_name'] != "") {
                                            echo $floorsData['floor_name'] . "(" . $floorsData['block_name'] . ")";
                                        } ?></option>
                                        <?php } }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input-10" class="col-sm-2 col-form-label">Employee <span class="required">*</span></label>
                                <div class="col-lg-4 col-md-4">
                                    <select name="user_id" id="user_id" class="inputSl form-control single-select">
                                        <option value="">-- Select Employee --</option>
                                        <?php
                                        if (isset($data)) {
                                        $user = $d->select("users_master", "society_id='$society_id' AND delete_status=0 AND user_status !=0 AND floor_id = '$data[floor_id]' $blockAppendQueryUser");
                                        while ($userdata = mysqli_fetch_array($user)) {
                                        ?>
                                        <option <?php if ($data['user_id'] == $userdata['user_id']) {
                                            echo 'selected';
                                            } ?> value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name']; ?>
                                        </option>
                                        <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="list_wrapper border">
                                        <?php
                                        if(isset($edit_expense))
                                        {
                                        ?>
                                        <h5 class="text-center">Update Expense</h5>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                        <h5 class="text-center">Add Multiple Expenses</h5>
                                        <?php
                                        }
                                        ?>
                                        <div class="row justify-content-md-center p-3">
                                            <div class="col-xs-<?php echo $t; ?> col-sm-<?php echo $t; ?> col-md-<?php echo $t; ?>">
                                                <div class="form-group">
                                                    Title<span class="text-danger">*</span>
                                                    <input type="text" autocomplete="off" maxlength="80" name="expense_title[]" id="expense_title" required class="form-control" value="<?php if(isset($edit_expense)){ echo $expense_title; } ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-<?php echo $a; ?> col-sm-<?php echo $a; ?> col-md-<?php echo $a; ?>">
                                                <div class="form-group">
                                                    Date<span class="text-danger">*</span>
                                                    <input type="text" readonly autocomplete="off" name="date[]" id="date" class="form-control past-dates-datepicker" value="<?php if(isset($edit_expense)){ echo $date; } ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-<?php echo $t; ?> col-sm-<?php echo $t; ?> col-md-<?php echo $t; ?>">
                                                <div class="form-group">
                                                    Category Type<span class="text-danger">*</span>
                                                    <select class="form-control single-select ect" required name="expense_category_id[]" id="expense_category_id">
                                                        <option value="">--SELECT--</option>
                                                        <?php
                                                        $q = $d->selectRow("expense_category_id,expense_category_name","expense_category_master");
                                                        while($data1 = $q->fetch_assoc())
                                                        {
                                                        ?>
                                                        <option <?php if(isset($edit_expense) && $expense_category_id == $data1['expense_category_id']){ echo "selected"; } ?> value="<?php echo $data1['expense_category_id']; ?>"><?php echo $data1['expense_category_name']; ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php
                                                    if(isset($edit_expense))
                                                    {
                                                    ?>
                                                        <input type="hidden" value="<?php echo $expense_category_type; ?>" name="expense_category_type" id="expense_category_type"/>
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                    ?>
                                                        <input type="hidden" name="expense_category_type[]" id="expense_category_type"/>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                            if(!isset($edit_expense))
                                            {
                                            ?>
                                            <div class="col-xs-1 col-sm-1 col-md-1"></div>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <!-- <div class="col-xs-2 col-sm-2 col-md-2"></div> -->
                                            <?php
                                            }
                                            ?>
                                            <div class="col-xs-<?php echo $t; ?> col-sm-<?php echo $t; ?> col-md-<?php echo $t; ?>">
                                                <div class="form-group">
                                                    Description
                                                    <input type="text" autocomplete="off" name="description[]" id="description" maxlength="200" class="form-control" value="<?php if(isset($edit_expense)){ echo $description; } ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-<?php echo $a; ?> col-sm-<?php echo $a; ?> col-md-<?php echo $a; ?>">
                                                <div class="form-group">
                                                    Attachment
                                                    <input type="file" name="expense_document[]" id="expense_document" accept="image/jpeg,image/png,application/pdf,image/jpg" class="form-control"/>
                                                </div>
                                                <?php
                                                if(isset($edit_expense) && !empty($expense_document))
                                                {
                                                ?>
                                                <input type="hidden" name="old_document" value="<?php echo $expense_document; ?>"/>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="col-xs-<?php echo $t; ?> col-sm-<?php echo $t; ?> col-md-<?php echo $t; ?>">
                                                <div class="form-group">
                                                    <?php
                                                    if(isset($edit_expense) && $expense_category_type == 1)
                                                    {
                                                    ?>
                                                    <span id="unit-amount">Unit</span><span class="text-danger">*</span>
                                                    <input type="text" required autocomplete="off" name="unit[]" id="unit" class="form-control decimal unit-amount" value="<?php if(isset($edit_expense)){ echo $unit; } ?>"/>
                                                    <?php
                                                    }
                                                    elseif(isset($edit_expense) && $expense_category_type == 0)
                                                    {
                                                    ?>
                                                    <span id="unit-amount">Amount</span><span class="text-danger">*</span>
                                                    <input type="text" required autocomplete="off" name="amount[]" id="amount" class="form-control decimal unit-amount" value="<?php if(isset($edit_expense)){ echo $amount; } ?>"/>
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                    ?>
                                                    <span id="unit-amount">Amount</span><span class="text-danger">*</span>
                                                    <input type="text" required autocomplete="off" name="amount[]" id="amount" class="form-control decimal unit-amount unitKeyup"/>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                            if(!isset($edit_expense))
                                            {
                                            ?>
                                            <div class="col-xs-1 col-sm-1 col-md-1"></div>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <!-- <div class="col-xs-2 col-sm-2 col-md-2"></div> -->
                                            <?php
                                            }
                                            ?>
                                            <?php
                                            if(isset($edit_expense) && $expense_category_type == 1)
                                            {
                                            ?>
                                            <div class="col-xs-<?php echo $t; ?> col-sm-<?php echo $t; ?> col-md-<?php echo $t; ?> unitDiv">
                                                <div class="form-group">
                                                    Unit Name<span class="text-danger">*</span>
                                                    <input type="text" class="form-control" maxlength="140" autocomplete="off" required name="expense_category_unit_name[]" readonly id="expense_category_unit_name" value="<?php echo $expense_category_unit_name; ?>">
                                                </div>
                                            </div>
                                            <div class="col-xs-<?php echo $a; ?> col-sm-<?php echo $a; ?> col-md-<?php echo $a; ?> unitDiv">
                                                <div class="form-group">
                                                    Unit Price<span class="text-danger">*</span>
                                                    <input type="number" class="form-control decimal" autocomplete="off" readonly required name="expense_category_unit_price[]" id="expense_category_unit_price" value="<?php echo $expense_category_unit_price; ?>">
                                                </div>
                                            </div>
                                            <div class="col-xs-<?php echo $t; ?> col-sm-<?php echo $t; ?> col-md-<?php echo $t; ?> unitDiv">
                                                <div class="form-group">
                                                    Final Amount
                                                    <input type="number" class="form-control finalAmountUnitWise" id="unit_amount_calc" disabled readonly value="<?php echo $amount; ?>">
                                                </div>
                                            </div>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <div class="col-xs-<?php echo $t; ?> col-sm-<?php echo $t; ?> col-md-<?php echo $t; ?> unitDiv d-none">
                                                <div class="form-group">
                                                    Unit Name<span class="text-danger">*</span>
                                                    <input type="text" class="form-control" maxlength="140" autocomplete="off" required name="expense_category_unit_name[]" readonly id="expense_category_unit_name">
                                                </div>
                                            </div>
                                            <div class="col-xs-<?php echo $a; ?> col-sm-<?php echo $a; ?> col-md-<?php echo $a; ?> unitDiv d-none">
                                                <div class="form-group">
                                                    Unit Price<span class="text-danger">*</span>
                                                    <input type="number" class="form-control decimal" autocomplete="off" readonly required name="expense_category_unit_price[]" id="expense_category_unit_price">
                                                </div>
                                            </div>
                                            <div class="col-xs-<?php echo $t; ?> col-sm-<?php echo $t; ?> col-md-<?php echo $t; ?> unitDiv d-none">
                                                <div class="form-group">
                                                    Final Amount
                                                    <input type="number" class="form-control finalAmountUnitWise" id="unit_amount_calc" disabled readonly>
                                                </div>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                            <div class="col-xs-1 col-sm-1 col-md-1"></div>
                                        </div>
                                    </div>
                                        <div class="row justify-content-md-center p-3">
                                            <button class="btn btn-primary list_add_button" type="button"><span class="fa fa-plus-circle" aria-hidden="true"></span> Add More Expense</button>
                                        </div>
                                </div>
                            </div>
                            <div class="form-footer text-center">
                                <input type="hidden" name="csrf" value='<?php echo $_SESSION["token"]; ?>'>
                                <?php
                                if(isset($edit_expense))
                                {
                                ?>
                                    <input type="hidden" name="bId" value="<?php echo $_POST['bId']; ?>"/>
                                    <input type="hidden" name="dId" value="<?php echo $_POST['dId']; ?>"/>
                                    <input type="hidden" name="uId" value="<?php echo $_POST['uId']; ?>"/>
                                    <input type="hidden" name="ueMonth" value="<?php echo $_POST['ueMonth']; ?>"/>
                                    <input type="hidden" name="ueYear" value="<?php echo $_POST['ueYear']; ?>"/>
                                    <input type="hidden" name="getReport" value="<?php echo $_POST['getReport']; ?>"/>
                                    <input type="hidden" name="f" value="<?php echo $_POST['f']; ?>"/>
                                    <input type="hidden" id="user_expense_id" name="user_expense_id" value="<?php echo $data['user_expense_id']; ?>">
                                    <input type="hidden" name="updateExpense" value="updateExpense">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <input type="hidden" name="addExpense" value="addExpense">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save</button>
                                <?php
                                }
                                ?>
                                <button type="button" value="add" class="btn btn-danger resetBtn"><i class="fa fa-check-square-o"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--End Row-->
    </div>
</div>
<!--End content-wrapper