<?php
extract(array_map("test_input", $_POST));
if (isset($edit_leave_type)) {
    $q = $d->select("leave_type_master","leave_type_id='$leave_type_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Leave Type</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
           
                <form id="addLeaveTypeAdd" action="controller/LeaveTypeController.php" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                    <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Leave Type Name <span class="required">*</span></label>
                    <div class="col-lg-10 col-md-10" id="">
                        <input type="text" class="form-control" placeholder="Leave Type Name" name="leave_type_name" value="<?php if($data['leave_type_name'] !=""){ echo $data['leave_type_name']; } ?>">
                    </div>
                    
                </div>                 
                <div class="form-footer text-center">
                  <?php //IS_1019 addPenaltiesBtn 
                  if (isset($edit_leave_type)) {                    
                  ?>
                  <input type="hidden" id="leave_type_id" name="leave_type_id" value="<?php if($data['leave_type_id'] !=""){ echo $data['leave_type_id']; } ?>" >
                  <button id="addLeaveTypeBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                 <input type="hidden" name="addLeaveType"  value="addLeaveType">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addLeaveTypeAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php }
                    else {
                      ?>
                 
                  <button id="addLeaveTypeBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                 <input type="hidden" name="addLeaveType"  value="addLeaveType">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addLeaveTypeAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                  <?php } ?>
                </div>

              </form>
           
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->

    <script type="text/javascript">
       function removePhoto2() {
        $('#facility_photo_2').remove();
        $('#penalty_photo_old_2').val('');
      }

       function removePhoto3() {
        $('#facility_photo_3').remove();
        $('#penalty_photo_old_3').val('');
      }

    </script>