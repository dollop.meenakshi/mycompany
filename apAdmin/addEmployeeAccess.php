<?php 

extract(array_map("test_input", $_POST));
if (isset($edit_app_access)) {
    $q = $d->selectRow('app_access_master.*,access_type_master.is_access_type_changeable,access_type_master.access_type_for',"app_access_master,access_type_master","app_access_master.access_type=access_type_master.access_type_id AND app_access_master.app_access_id='$app_access_id' AND app_access_master.society_id='$society_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
    $access_for_id = array();
    $blockIds = array();
    $floorIds = array();
    if($data['access_for']=="1"){
        $access_for_id = explode(',', $data['access_for_id']);
    }
    else if($data['access_for']=="2"){
        $access_for_id = explode(',', $data['access_for_id']);
        $dept_ids = implode("','", $access_for_id);
        //$dept_ids = explode("','", $dept_ids);
        $q1 = $d->selectRow('*',"floors_master","floor_id IN ('$dept_ids') AND society_id='$society_id'");	
        
        while($data1 = mysqli_fetch_array($q1)){
            array_push($blockIds,$data1['block_id']);
        }
        $blockIds = array_unique($blockIds);
        $blockIds = array_values($blockIds);
        $block_ids = implode("','", $blockIds);
    }
    else if($data['access_for']=="3"){
        $access_for_id = explode(',', $data['access_for_id']);
        $user_ids = implode("','", $access_for_id);
        //$dept_ids = explode("','", $dept_ids);
        $q2 = $d->selectRow('*',"users_master","user_id IN ('$user_ids') AND society_id='$society_id'");
        while($data2 = mysqli_fetch_array($q2)){
            array_push($blockIds,$data2['block_id']);
            array_push($floorIds,$data2['floor_id']);
        }
        $blockIds = array_unique($blockIds);
        $blockIds = array_values($blockIds);
        $block_ids = implode("','", $blockIds);
        $floorIds = array_unique($floorIds);
        $floorIds = array_values($floorIds);
        $floor_ids = implode("','", $floorIds);
    }
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Add Employee App Access</h4>
     </div>
     </div>
    <!-- End Breadcrumb-->

    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addEmployeeAccessFrom" action="controller/EmployeeAccessController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Branch <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select name="block_id" class="form-control single-select" onchange="getVisitorFloorByBlockId(this.value)" required <?php if (isset($edit_app_access)) { echo 'disabled'; }?>  >
                                        <option value="">-- Select Branch --</option> 
                                        <?php 
                                        $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                                        while ($blockData=mysqli_fetch_array($qb)) {
                                        ?>
                                        <option  <?php if($block_id==$blockData['block_id']) { echo 'selected';} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                                        <?php } ?>

                                    </select>                
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Department <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select name="floor_id" id="dId" class="form-control single-select" onchange="getUserByFloorId(this.value);" required <?php if (isset($edit_app_access)) { echo 'disabled'; }?>>
                                    <option value="">-- Select Department --</option> 
                                    <?php 
                                        $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id='$block_id' $blockAppendQuery");  
                                        while ($depaData=mysqli_fetch_array($qd)) {
                                    ?>
                                    <option  <?php if($floor_id==$depaData['floor_id']) { echo 'selected';} ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?></option>
                                    <?php } ?>
                                    
                                    </select>                 
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Access By Employee <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select name="access_by_id" id="uId" class="form-control single-select" <?php if (isset($edit_app_access)) { echo 'disabled'; }?> onchange="getEmployeeAccessType(this.value)">
                                        <option value="">-- Select Employee --</option> 
                                        <?php 
                                            $user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND user_status=1 AND floor_id = '$floor_id' $blockAppendQueryUser");  
                                            while ($userdata=mysqli_fetch_array($user)) {
                                        ?>
                                        <option <?php if($access_by_id==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
                                        <?php } ?>
                                    </select>               
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Access Type <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text" required="" class="form-control single-select" name="access_type" id="access_type" onchange="getAccessType(this.value);" <?php if (isset($edit_app_access)) { echo 'disabled'; }?>>
                                        <option value="">-- Select Access Type --</option> 
                                        <?php 
                                            $acq=$d->select("access_type_master","access_type_id='$access_type'");
                                            while ($accessTypeData=mysqli_fetch_array($acq)) {
                                        ?>
                                        <option <?php if($access_type==$accessTypeData['access_type_id']) { echo 'selected';} ?> value="<?php echo $accessTypeData['access_type_id'];?>" ><?php echo $accessTypeData['access_type'];?></option>
                                        <?php } ?>
                                    </select>          
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4 <?php if(isset($edit_app_access) && $is_access_type_changeable == 1){echo 'd-none';} ?>" id="access_mode">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Access Mode <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text" required="" class="form-control" name="access_mode">
                                        <option value="">-- Select Access Mode --</option> 
                                        <option <?php if(isset($edit_app_access) && $access_mode == 0){echo 'selected';} ?> value="0">Modification</option> 
                                        <option <?php if(isset($edit_app_access) && $access_mode == 1){echo 'selected';} ?> value="1">Read Only</option> 
                                    </select>          
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Access For <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text" required="" class="form-control" name="access_for" id="access_for" onchange="accessType(this.value);">
                                        <option value="">-- Select Access For --</option> 
                                        <option <?php if(isset($edit_app_access) && $access_for == 0){echo 'selected';} ?> value="0">All </option> 
                                        <option <?php if(isset($edit_app_access) && $access_for == 1){echo 'selected';} ?> value="1">Branch Wise</option> 
                                        <option <?php if(isset($edit_app_access) && $access_for == 2){echo 'selected';} ?> value="2">Department Wise</option> 
                                        <?php if(isset($edit_app_access) && $access_type_for == 0){ ?>
                                        <option <?php if(isset($edit_app_access) && $access_for == 3){echo 'selected';} ?> value="3">Employee Wise</option> 
                                        <?php } ?>
                                    </select>          
                                </div>
                            </div> 
                        </div>
                        
                    </div>    
                    <div class="row all-access branch-wise <?php if($data['access_for']==1){}else{echo "d-none";} ?>">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" >
                                    <select type="text" required="" id="access_for_id" class="form-control multiple-select taskAccessForBranchMultiSelectCls access_for_id" name="access_for_id[]" multiple>
                                        <option value="0">All Branch</option>
                                        <?php
                                        $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                                        while ($blockData=mysqli_fetch_array($bq)) {
                                        ?>
                                        <option <?php if(isset($access_for_id) && in_array($blockData['block_id'],$access_for_id)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                    </div>
                    <div class="row all-access department-wise <?php if($data['access_for']==2){}else{echo "d-none";} ?>">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select dwblockId taskAccessForBranchMultiSelectCls" multiple name="blockId[]" id="dwblockId" onchange="getTaskAccessForDepartment();">
                                        <!-- <option value="0">All Branch</option> -->
                                        <?php
                                        $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                                        while ($blockData=mysqli_fetch_array($bq)) {
                                        ?>
                                        <option <?php if(isset($access_for_id) && in_array($blockData['block_id'],$blockIds)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" id="dwfloorId" class="form-control multiple-select dwfloorId taskAccessForDepartmentMultiSelectCls access_for_id" multiple name="access_for_id[]">
                                        <option value="0">All Department</option>
                                        <?php 
                                            $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') $blockAppendQuery");  
                                            while ($depaData=mysqli_fetch_array($qd)) {
                                        ?>
                                        <option <?php if(in_array($depaData['floor_id'],$access_for_id)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
                                        <?php } ?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                    </div>
                    <div class="row all-access employee-wise <?php if($data['access_for']==3){}else{echo "d-none";} ?>">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Branch <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select ewblockId taskAccessForBranchMultiSelectCls" multiple name="blockId[]" id="ewblockId" onchange="getTaskAccessForDepartment();">
                                        <!-- <option value="0">All Branch</option> -->
                                        <?php
                                        $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                                        while ($blockData=mysqli_fetch_array($bq)) {
                                        ?>
                                        <option <?php if(isset($blockIds) && in_array($blockData['block_id'],$blockIds)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Department <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select ewfloorId taskAccessForDepartmentMultiSelectCls" multiple onchange="getTaskAccessForEmployee();" name="floorId[]">
                                        <!-- <option value="0">All Department</option> -->
                                        <?php 
                                            $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') $blockAppendQuery");  
                                            while ($depaData=mysqli_fetch_array($qd)) {
                                        ?>
                                        <option <?php if(in_array($depaData['floor_id'],$floorIds)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
                                        <?php } ?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Employee <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select type="text" required="" class="form-control multiple-select ewempId taskAccessForEmployeeMultiSelectCls access_for_id" multiple name="access_for_id[]" id="ewempId"> 
                                        <option value="0">All Employee</option>
                                        <?php 
                                            $user=$d->select("users_master","society_id='$society_id' AND delete_status=0 AND user_status=1 AND floor_id IN ('$floor_ids')$blockAppendQueryUser");  
                                            while ($userdata=mysqli_fetch_array($user)) {
                                        ?>
                                        <option <?php if(in_array($userdata['user_id'],$access_for_id)){ echo "selected"; } ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
                                        <?php } ?>
                                    </select>
                                </div>                   
                            </div> 
                        </div>
                    </div>
                <div class="row">
                <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Allow Notification <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="radio" name="allow_notification" <?php if (isset($edit_app_access)) { if($data['allow_notification']==1){ echo "checked"; } }else { echo "checked"; }?>  value="1"><label class="col-form-label ml-2">Yes</label>
                                    <input type="radio" <?php if (isset($edit_app_access)) { if($data['allow_notification']==0){ echo "checked"; } }else { echo ""; }?>   name="allow_notification" value="0"><label  class="col-form-label ml-2">No</label>
                                </div>                   
                            </div> 
                        </div>
                        <div class="col-md-4 hideNotiSound">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Notification Sound <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="radio" <?php if (isset($edit_app_access)) { if($data['notification_sound']==1){ echo "checked"; } }else { echo "checked"; }?> name="notification_sound" checked value="1"><label class="col-form-label ml-2">Yes</label>
                                    <input type="radio" <?php if (isset($edit_app_access)) { if($data['notification_sound']==0){ echo "checked"; } }else { echo ""; }?>   name="notification_sound" value="0"><label  class="col-form-label ml-2">No</label>
                                </div>                   
                            </div> 
                        </div>
                </div>
                    <div class="form-footer text-center">              
                        <?php if (isset($edit_app_access)) { ?>      
                        <button id="addCanteenProductVariantBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                        <input type="hidden" name="updateEmployeeAccess"  value="updateEmployeeAccess">
                        <input type="hidden" name="app_access_id" id="app_access_id"  value="<?php echo $app_access_id;?>">
                        <?php }else{ ?>
                        <button id="addCanteenProductVariantBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                        <input type="hidden" name="addEmployeeAccess"  value="addEmployeeAccess">
                        <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    function accessType(value) {
		$('.multiple-select').val('').trigger('change');
		if(value == "1"){
			$('.blockId').val('').trigger('change');
			$('.all-access').addClass('d-none');
			$('.branch-wise').removeClass('d-none');
		}
		else if(value == "2"){
			$('.blockId').val('');
			$('.all-access').addClass('d-none');
			$('.department-wise').removeClass('d-none');
		}
		else if(value == "3"){
			$('.blockId').val('');
			$('.all-access').addClass('d-none');
			$('.employee-wise').removeClass('d-none');
		}
        else{
			$('.blockId').val('');
			$('.all-access').addClass('d-none');
		}

        $('#dwblockId').rules("add", {
                required: true,
                messages: {
                    required: "Please select Branch",
                }
            });
        $('#ewblockId').rules("add", {
                required: true,
                messages: {
                    required: "Please select Branch",
                }
            });
        $('#access_for_id').rules("add", {
                required: true,
                messages: {
                    required: "Please select Branch",
                }
            });
        $('#ewempId').rules("add", {
                required: true,
                messages: {
                    required: "Please select Employee",
                }
            });
        $('#dwfloorId').rules("add", {
                required: true,
                messages: {
                    required: "Please select Department",
                }
            });
    }

    $('input[type=radio][name=allow_notification]').change(function() {
    if (this.value == 1) {
        $('.hideNotiSound').show();

    }
    else {
        $('.hideNotiSound').hide();
    }
    });
    <?php if (isset($edit_app_access)) { 
        if($data['allow_notification']==1){  ?>
           $('.hideNotiSound').show();
           <?php } else{
              ?>$('.hideNotiSound').hide(); <?php 
           }
           
        }else { ?>$('.hideNotiSound').show(); <?php }?>
</script>  
  