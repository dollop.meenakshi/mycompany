<?php
$qSociety = $d->select("society_master", "society_id ='$society_id'");
$user_data_society = mysqli_fetch_array($qSociety);
extract($user_data_society);

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-5">
        <h4 class="page-title"> <?php echo $xml->string->app_access; ?></h4>

      </div>



    </div>
    <!-- End Breadcrumb-->


    <div class="row">
      <div class="card-body">
        <ul class="list-group row">
          <li class="list-group-item d-flex justify-content-between align-items-center bg-primary text-white">
            App <?php echo $xml->string->settings; ?>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->ss_capture_timeline; ?>
            <span class="">
              <?php
              if ($screen_sort_capture_in_timeline == "0") {
              ?>
                <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $screen_sort_capture_in_timeline; ?>','screensortDeactive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $screen_sort_capture_in_timeline; ?>','screensortActive');" data-size="small" />
              <?php } ?>
            </span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->group_chat_app; ?>
            <span class="">
              <?php
              if ($group_chat_status == "0") {
              ?>
                <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $group_chat_status; ?>','GroupChatDeactive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $group_chat_status; ?>','GroupChatActive');" data-size="small" />
              <?php } ?>
            </span>
          </li>
          <?php if ($society_type == 0) {  ?>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $xml->string->visitor_approval; ?>
              <span>
                <?php if ($visitor_on_off == "0") { ?>
                  <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $visitor_on_off; ?>','VisitorApprovalDeactive');" data-size="small" />
                <?php } else { ?>
                  <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $visitor_on_off; ?>','VisitorApprovalActive');" data-size="small" />
                <?php } ?>
              </span>
            </li>

            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $xml->string->resident_in_out_security_guard_app; ?>
              <span>
                <?php
                if ($resident_in_out == "1") {
                ?>
                  <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $resident_in_out; ?>','residentinoutActive');" data-size="small" />
                <?php } else { ?>
                  <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $resident_in_out; ?>','residentinoutDeactive');" data-size="small" />
                <?php } ?>
              </span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $xml->string->show_visitor_mo_no_security_guard_app; ?>
              <span>
                <?php
                if ($visitor_mobile_number_show_gatekeeper == "1") {
                ?>
                  <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $visitor_mobile_number_show_gatekeeper; ?>','visitorMobileActive');" data-size="small" />
                <?php } else { ?>
                  <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $visitor_mobile_number_show_gatekeeper; ?>','visitorMobiletDeactive');" data-size="small" />
                <?php } ?>
              </span>
            </li>
          <?php } ?>

          <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->create_group_app; ?>
            <span>
              <?php
              if ($create_group == "1") {
              ?>
                <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $create_group; ?>','CreateGroupActive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $create_group; ?>','CreateGroupDeactive');" data-size="small" />
              <?php } ?>
            </span>
          </li>

          <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->registration_request_app; ?>
            <span>
              <?php
              if ($registration_request_from_app == "0") {
              ?>
                <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $registration_request_from_app; ?>','registrationRequestDeactive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $registration_request_from_app; ?>','registrationRequestActive');" data-size="small" />
              <?php } ?>
            </span>
          </li>



          <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->cab_courier_boy_approval; ?>
            <span>
              <?php
              if ($entry_all_visitor_group == "1") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $entry_all_visitor_group; ?>','entryAllVisitorGroupActive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $entry_all_visitor_group; ?>','entryAllVisitorGroupDeactive');" data-size="small" />
              <?php } ?>
            </span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->event_pass_scan_security_guard_app; ?>
            <span>
              <?php
              if ($event_scanner_for_gatekeeper == "0") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $event_scanner_for_gatekeeper; ?>','scannerDeactive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $event_scanner_for_gatekeeper; ?>','scannerActive');" data-size="small" />
              <?php } ?>
            </span>
          </li>

          <?php
          if ($adminData['role_id'] == "2") { ?>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?php echo $xml->string->opt_verification_all_visitors; ?>
              <span>
                <?php
                if ($visitor_otp_verify == "1") {
                ?>
                  <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $society_id; ?>','otpVerifyActive');" data-size="small" />
                <?php } else { ?>
                  <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $society_id; ?>','otpVerifyDeactive');" data-size="small" />
                <?php } ?>
              </span>
            </li>
          <?php } //14july2020 
          ?>

          <!-- <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->hide_resident_parking_app; ?>
            <span>
              <?php
              if ($hide_member_parking == "1") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $society_id; ?>','parkingtabActive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $society_id; ?>','parkingtabDeactive');" data-size="small" />
              <?php } ?>
            </span>
          </li> -->
          <?php // IS_298 
          ?>





          <?php //14july2020 
          ?>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->resident_timeline_access; ?>
            <span>
              <?php
              if ($timeline_aceess_for_society == "0") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $timeline_aceess_for_society; ?>','timelineActive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $timeline_aceess_for_society; ?>','timelineDeactive');" data-size="small" />
              <?php } ?>
            </span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->resident_chat_access; ?>
            <span>
              <?php
              if ($chat_aceess_for_society == "0") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $chat_aceess_for_society; ?>','chatActive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $chat_aceess_for_society; ?>','chatDeactive');" data-size="small" />
              <?php } ?>
            </span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <?php echo $xml->string->resident_my_activity_access; ?>
            <span>
              <?php
              if ($myactivity_aceess_for_society == "0") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $myactivity_aceess_for_society; ?>','activityActive');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $myactivity_aceess_for_society; ?>','activityDeactive');" data-size="small" />
              <?php } ?>
            </span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Bind Mac Address with mobile app
            <span>
              <?php
              if ($bind_mac_address == "1") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $bind_mac_address; ?>','bindMacAddressOff');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $bind_mac_address; ?>','bindMacAddressOn');" data-size="small" />
              <?php } ?>
            </span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Hide Working hours in mobile app
            <span>
              <?php
              if ($hide_working_hours == "1") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $hide_working_hours; ?>','ShowWorkingHoursinApp');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $hide_working_hours; ?>','hideWorkingHoursinApp');" data-size="small" />
              <?php } ?>
            </span>
          </li>

          <li class="list-group-item d-flex justify-content-between align-items-center">
            Hide Upcoming Birthday in mobile app dashboard
            <span>
              <?php
              if ($hide_birthday_view == "1") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $hide_birthday_view; ?>','ShowUpcomingBirthday');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $hide_birthday_view; ?>','HideUpcomingBirthday');" data-size="small" />
              <?php } ?>
            </span>
          </li>

          <li class="list-group-item d-flex justify-content-between align-items-center">
            Hide My Department in mobile app dashboard
            <span>
              <?php
              if ($hide_department_employee_view == "1") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $hide_department_employee_view; ?>','showMyDepartment');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $hide_department_employee_view; ?>','HidesMyDepartment');" data-size="small" />
              <?php } ?>
            </span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Hide Photo Gallary in mobile app dashboard
            <span>
              <?php
              if ($hide_gallery_view == "1") {
              ?>
                <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $hide_gallery_view; ?>','ShowPhotoGallary');" data-size="small" />
              <?php } else { ?>
                <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $hide_gallery_view; ?>','hidePhotoGallary');" data-size="small" />
              <?php } ?>
            </span>
          </li>

          <li class="list-group-item d-flex justify-content-between align-items-center">
            Daily Birthday Notification
            <span>
              <form>
                <select name="birthday_notification_type" onchange="changeBirthdayNotificationSetting(this.value);" id="birthday_notification_type" class="form-control">
                  <option <?php if ($birthday_notification_type == 0) {
                            echo 'selected';
                          } ?> value="0"> OFF </option>
                  <option <?php if ($birthday_notification_type == 1) {
                            echo 'selected';
                          } ?> value="1">Branch Wise</option>
                  <option <?php if ($birthday_notification_type == 2) {
                            echo 'selected';
                          } ?> value="2">Department Wise</option>
                  <option <?php if ($birthday_notification_type == 3) {
                            echo 'selected';
                          } ?> value="3">Company Wise</option>
                </select>
              </form>

            </span>
          </li>

        </ul>
      </div>




    </div><!-- End Row-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header"><i class="fa fa-table"></i> Profile Menu Access</div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>Sr.No</th>
                    <th>Menu Name</th>
                    <th>Menu Photo</th>
                    <th>Access Type</th>
                    <th>Menu Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i = 1;
                  $q = $d->select("employee_profile_menu_access", "");
                  $counter = 1;
                  while ($data = mysqli_fetch_array($q)) {
                  ?>
                    <tr>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['profile_menu_name']; ?></td>
                      <td>
                        <a data-fancybox="images" data-caption="Photo Name : <?php echo $data['profile_menu_name'] ?>" href="../img/app_icon/<?php echo $data['profile_menu_photo'] ?>" target="_blank">
                          <img style="height:70px; width:70px; object-fit: cover;" class="lazyload" id="<?php echo $data['profile_menu_id']; ?>" data-src="../img/app_icon/<?php echo $data['profile_menu_photo'] ?>">
                        </a>
                      </td>
                      <td>
                        <?php if($data['is_chanable'] == 0){ ?>
                        <form>
                          <select name="access_type" onchange="changeMenuAccessTypeSetting(this.value, '<?php echo $data['profile_menu_id']; ?>');" id="access_type" class="form-control">
                            <option <?php if ($data['access_type'] == 0) {
                                      echo 'selected';
                                    } ?> value="0"> Edit </option>
                            <option <?php if ($data['access_type'] == 1) {
                                      echo 'selected';
                                    } ?> value="1">View Only</option>
                            <option <?php if ($data['access_type'] == 2) {
                                      echo 'selected';
                                    } ?> value="2">Change Request</option>
                          </select>
                        </form>
                        <?php } ?>
                      </td>
                      <td>
                        <?php
                        if ($data['profile_menu_status'] == "0") {
                        ?>
                          <input type="checkbox" checked="" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['profile_menu_id']; ?>','profileMenuStatusDeactive');" data-size="small" />
                        <?php } else { ?>
                          <input type="checkbox" class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['profile_menu_id']; ?>','profileMenuStatusActive');" data-size="small" />
                        <?php } ?>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>