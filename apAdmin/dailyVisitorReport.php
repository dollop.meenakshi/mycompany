<?php //IS_1218
error_reporting(0);
if (isset($_GET['from'])  ) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1  ){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='dailyVisitorReport';
        </script>");
  }
} else if(isset($_GET['from']) ){
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='dailyVisitorReport';
        </script>");
  } 
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title"> Daily Visitor  Report</h4>
        </div>
     
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
          
           
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
          <div class="col-lg-2">
          

          </div>

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                extract(array_map("test_input" , $_GET));
                  // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                       $q=$d->select("visitors_master","visitors_master.visitor_type=4 and  visitors_master.society_id='$society_id' AND visitors_master.visit_date BETWEEN '$from' AND '$toDate'");
                    

                  $i=1;
                if (isset($_GET['from'])) {
               ?>
                
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Visit From</th>
                        <th>Visitor</th>
                        <th>Visitor Mobile</th>
                       
                        
                        <th>Visit In Time</th>
                        <th>Visit Out Time</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q)) {
                    // echo '<pre>';
                    // print_r($data);
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                         

                         <td>
                          <?php echo $data['visit_from']; ?>
                         </td>
                        <td><?php echo $data['visitor_name']; ?></td>
                        <td><?php echo $data['country_code'] . " " . substr($data['visitor_mobile'], 0, 2) . '*****' . substr($data['visitor_mobile'], -3); ?></td>
                        
                        <td><?php echo  date ("d-m-Y h:i A", strtotime($data['visit_date'].' '.$data['visit_time']));   ?></td>
                        <td><?php if ($data['visitor_status']==3) { echo   date ("d-m-Y h:i A", strtotime($data['exit_date'].' '.$data['exit_time']));  }   ?></td>
                        
                        <td>
                         <?php if ($data['visitor_status']==0) {
                          echo "Pending";
                         } elseif ($data['visitor_status']==1) {
                          echo "Approved ";
                         } elseif ($data['visitor_status']==2) {
                          echo "Entered ";
                         } elseif ($data['visitor_status']==3) {
                          echo "Exit ";
                         } elseif ($data['visitor_status']==4) {
                          echo "Rejected ";
                         } elseif ($data['visitor_status']==6) {
                          echo "Hold";
                         } 
                         ?>
                        </td>
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            <?php } else {  ?>
             
                <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->