
<?php
extract(array_map("test_input", $_POST));
if (isset($edit_company_home_slider)) {
    $q = $d->select("company_home_slider_master","company_home_slider_id='$company_home_slider_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Company Home Slider</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addCompanyHomeSliderFrom" action="controller/CompanyHomeSliderController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Top Image <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="file" accept="image/png, image/jpeg" class="form-control" name="company_home_slider_image" value="">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Slider Title <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Slider Title" name="company_home_slider_title" value="<?php if($data['company_home_slider_title'] !=""){ echo $data['company_home_slider_title']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Description <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                <textarea id="company_home_slider_description" name="company_home_slider_description" placeholder="Slider Description" class="form-control "><?php if($data['company_home_slider_description'] !=""){ echo $data['company_home_slider_description']; } ?></textarea>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Slider URL <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Slider URL" name="company_home_slider_url" value="<?php if($data['company_home_slider_url'] !=""){ echo $data['company_home_slider_url']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Slider Mobile No <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Slider Mobile No" name="company_home_slider_mobile" value="<?php if($data['company_home_slider_mobile'] !=""){ echo $data['company_home_slider_mobile']; } ?>">
                                </div>
                            </div> 
                        </div>
                    </div>    
                    <input type="hidden" id="company_home_slider_id" name="company_home_slider_id" value="<?php if($data['company_home_slider_id'] !=""){ echo $data['company_home_slider_id']; } ?>" >          
                    <div class="form-footer text-center">
                    <?php //IS_1019 addPenaltiesBtn 
                    if (isset($edit_company_home_slider)) {                    
                    ?>
                    
                    <button id="addCompanyHomeSliderBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addCompanyHomeSlider"  value="addCompanyHomeSlider">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyHomeSliderFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                        <?php }
                        else {
                        ?>
                    
                    <button id="addCompanyHomeSliderBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addCompanyHomeSlider"  value="addCompanyHomeSlider">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyHomeSliderFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //$('#blah').attr('src', e.target.result);
            $('#blah').html('<img src="'+e.target.result+'" width="75%" height="50%">');
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
