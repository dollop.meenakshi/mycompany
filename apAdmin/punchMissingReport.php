<?php
error_reporting(0);
//$month_year= $_REQUEST['month_year'];
//$laYear = $_REQUEST['laYear'];
$bId = $_GET['bId'];
$dId = $_GET['dId'];
$uId = $_GET['uId'];
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_GET['toDate']) != 1) {
    $_SESSION['msg1'] = "Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employeeExpenseReport';
        </script>");
  }
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Punch Out Missing Report</h4>
      </div>

    </div>

    <form action="" method="get" class="branchDeptFilter">
      <div class="row pb-2">
        <?php include('selectBranchDeptEmpForFilterAll.php');?>
        
        <div class="col-md-3 col-6">
          <select name="laYear" class="form-control" >
            <option value="">Year</option>
            <option <?php if ($_GET['laYear'] == $twoPreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $twoPreviousYear ?>"><?php echo $twoPreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $onePreviousYear) {
                      echo 'selected';
                    } ?> value="<?php echo $onePreviousYear ?>"><?php echo $onePreviousYear; ?></option>
            <option <?php if ($_GET['laYear'] == $currentYear) {
                      echo 'selected';
                    } ?> <?php if ($_GET['laYear'] == '') { echo 'selected';} ?> value="<?php echo $currentYear ?>"><?php echo $currentYear ?></option>
            <option <?php if ($_GET['laYear'] == $nextYear) {
                      echo 'selected';
                    } ?> value="<?php echo $nextYear ?>"><?php echo $nextYear; ?></option>
          </select>
        </div>
        <div class="col-md-3 col-6">
          <select class="form-control month_year single-select" name="month_year" required>
                <option value="">--Select Month --</option>
                <option <?php if($_GET['month_year']=="01") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '01') { echo 'selected';}?> value="01">January</option>
                <option <?php if($_GET['month_year']=="02") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '02') { echo 'selected';}?> value="02">February</option>
                <option <?php if($_GET['month_year']=="03") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '03') { echo 'selected';}?> value="03">March</option>
                <option <?php if($_GET['month_year']=="04") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '04') { echo 'selected';}?> value="04">April</option>
                <option <?php if($_GET['month_year']=="05") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '05') { echo 'selected';}?> value="05">May</option>
                <option <?php if($_GET['month_year']=="06") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '06') { echo 'selected';}?> value="06">June</option>
                <option <?php if($_GET['month_year']=="07") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '07') { echo 'selected';}?> value="07">July</option>
                <option <?php if($_GET['month_year']=="08") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '08') { echo 'selected';}?> value="08">August</option>
                <option <?php if($_GET['month_year']=="09") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '09') { echo 'selected';}?> value="09">September</option>
                <option <?php if($_GET['month_year']=="10") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '10') { echo 'selected';}?> value="10">October</option>
                <option <?php if($_GET['month_year']=="11") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '11') { echo 'selected';}?> value="11">November</option>
                <option <?php if($_GET['month_year']=="12") { echo 'selected';} ?> <?php if($_GET['month_year'] == '' && $currentMonth == '12') { echo 'selected';}?> value="12">December</option> 
            </select> 
        </div>
        

        
        <input type="hidden" name="">
        
        <div class="col-lg-2 from-group col-6">
          <input  class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Report">
        </div>


      </div>
    </form>
    <!-- End Breadcrumb-->
    <input type="hidden" name="floor_id_old" id="floor_id_old" value="<?php if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                                                                              echo $_GET['dId'];
                                                                            } ?>">

    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
          <div class="card-body">

            <div class="table-responsive">
              <?php
              extract(array_map("test_input", $_GET));


              if (isset($_GET['dId']) && $_GET['dId'] > 0) {
                $deptFilterQuery = " AND users_master.floor_id='$_GET[dId]'";
              }
              if (isset($uId) && $uId > 0) {
                $userFilterQuery = " AND attendance_master.user_id='$uId'";
              }
              if(isset($_GET['laYear']) && $_GET['laYear']>0) {
                $YearFilterQuery = " AND DATE_FORMAT(attendance_master.attendance_date_start,'%Y')='$_GET[laYear]'";
              }
              if(isset($_GET['month_year']) && (int)$_GET['month_year']>0 ) {
                $MonthFilterQuery = " AND DATE_FORMAT(attendance_master.attendance_date_start,'%m')='$_GET[month_year]'";
              }

              $q = $d->selectRow("floors_master.floor_name,block_master.block_name,attendance_master.*,leave_master.*,users_master.user_full_name,users_master.user_designation","attendance_master 
              LEFT JOIN leave_master ON leave_master.user_id = attendance_master.user_id 
              AND leave_master.leave_start_date =attendance_master.attendance_date_start,users_master,floors_master,block_master", "users_master.user_id=attendance_master.user_id  AND attendance_master.society_id='$society_id' AND attendance_master.attendance_status !=0 AND floors_master.floor_id = users_master.floor_id AND block_master.block_id = users_master.block_id AND attendance_master.punch_out_time ='00:00:00' $YearFilterQuery $MonthFilterQuery $deptFilterQuery $userFilterQuery $blockAppendQueryUser","ORDER BY attendance_master.attendance_date_start ASC , users_master.user_full_name ASC");

              $i = 1;
              if (isset($_GET['dId']) && isset($_GET['bId'])) {
              ?>

                <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Employee</th>
                      <th>Designation</th>
                      <th>Branch/Departement</th>
                      <th>Punch In Date</th>
                      <th>Punch In Time</th>
                      
                      <th>On Leave</th>
                      <th>Late In</th>
                      <th>Out of Range (In)</th>
                      <th>Is Modified</th>
                    <!--   <th>View</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    while ($data = mysqli_fetch_array($q)) {
                    ?>
                      <tr>

                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php echo $data['user_designation']; ?></td>
                        <td><?php echo $data['block_name'].'-'.$data['floor_name']; ?></td>
                        <td><?php if ($data['attendance_date_start'] != '0000-00-00' && $data['attendance_date_start'] != 'null') {
                              echo date("d M Y", strtotime($data['attendance_date_start']))." (".date("D", strtotime($data['attendance_date_start'])).")";
                            } ?></td>
                        <td><?php if ($data['punch_in_time'] != '00:00:00' && $data['punch_in_time'] != 'null') {
                              echo date("h:i A", strtotime($data['punch_in_time']));
                            } ?></td>
                        
                      
                        
                        <td>
                        <?php if($data['leave_id']!=''){ 
                          if($data['leave_day_type']==0) { $leaveType= "Full Day"; } else {  $leaveType= "Half Day"; }
                            if($data['auto_leave'] !=1 ) { echo $leaveType;}else{ echo $leaveType.' <br>(Auto Leave)'; } 
                          } else { echo "No"; } ?>
                        </td>
                         <td>
                          <?php if($data['late_in']=='1'){ 
                           echo "Yes";
                          } else { echo "No"; } ?>
                        </td>
                        
                        <td>
                          <?php if($data['punch_in_in_range']=='1'){ 
                           echo "Yes";
                          } else { echo "No"; } ?>
                        </td>
                       
                        <td>
                          <?php if($data['is_modified']=='1'){ 
                           echo "Yes";
                          } else { echo "No"; } ?>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>

                </table>


              <?php } else {  ?>
                <div class="" role="alert">
                  <span><strong>Note :</strong> Please Select Date</span>
                </div>
              <?php } ?>

            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-    fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->

<div class="modal fade" id="addAttendaceReportModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Attendance Data</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">
          <div class="row col-md-12 " id="attendanceReportData">

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>
