<?php error_reporting(0);
 $plId = (int)$_REQUEST['plId'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Employee Level</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
       <div class="btn-group float-sm-right">
        <!-- <a href="penaltyReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <a href="addEmployeeLevel" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
        
         <a href="javascript:void(0)" onclick="DeleteAll('deleteEmployeeLevels');" class="btn  btn-sm btn-danger pull-right mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
     </div>
     </div>

      <form class="branchDeptFilterWithUser" action="" >
        <div class="row pt-2 pb-2">
          <div class="col-md-6 col-12 form-group">
            <select name="plId" class="form-control single-select" onchange="form.submit();">
              <option value="">-- Select Parent --</option> 
                <?php 
                  $lq=$d->select("employee_level_master","society_id='$society_id' AND level_status=0");  
                  while ($levelData=mysqli_fetch_array($lq)) {
                ?>
              <option  <?php if($plId==$levelData['level_id']) { echo 'selected';} ?> value="<?php echo  $levelData['level_id'];?>" ><?php echo $levelData['level_name'];?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      </form>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>#</th>
                        <th>Level</th>
                        <th>Parent Level</th>
                        <th>Date</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;

                        if(isset($plId) && $plId > 0){
                          $appendQuery = " AND employee_level_master.parent_level_id = '$plId'";
                        }

                        $q=$d->selectRow("employee_level_master.*, parent_level.level_name AS parent_level_name","employee_level_master LEFT JOIN employee_level_master parent_level ON parent_level.level_id=employee_level_master.parent_level_id","employee_level_master.society_id='$society_id' $appendQuery","ORDER BY employee_level_master.level_id DESC");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                        
                      <td><?php echo $counter++; ?></td>
                       <td class="text-center">
                          <?php $totalLevel = $d->count_data_direct("level_id","users_master","level_id='$data[level_id]'");
                            if( $totalLevel==0) {
                          ?>
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['level_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['level_id']; ?>">                      
                          <?php } ?>
                        </td>
                       <td><?php echo $data['level_name']; ?></td>
                       <td><?php echo $data['parent_level_name']; ?></td>
                       <td><?php echo date("d M Y h:i A", strtotime($data['level_created_date'])); ?></td>
                       <td>
                        <div class="d-flex align-items-center">
                          <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="employeeLevelDataSet(<?php echo $data['level_id']; ?>)"> <i class="fa fa-pencil"></i></button>
                          <button type="submit" class="btn btn-sm btn-primary mr-2" onclick="addLevel(<?php echo $data['level_id']; ?>)">Add Level</button>
                            <!-- <?php if($data['level_status']=="0"){ ?>
                            <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['level_id']; ?>','levelDeactive');" data-size="small"/>
                            <?php } else { ?>
                            <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['level_id']; ?>','levelActive');" data-size="small"/>
                            <?php } ?> -->
                        </div>
                        </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>

<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Employee Level</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           
          <form id="employeeLevelAdd" action="controller/EmployeeLevelController.php" enctype="multipart/form-data" method="post"> 
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Level Name <span class="required">*</span></label>
                <div class="col-lg-8 col-md-8" id="">
                    <input type="text" class="form-control" placeholder="Employee Level Name" id="level_name" name="level_name" value="">
                </div>
            </div>                    
            <div class="form-group row">
                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Parent Level </label>
                <div class="col-lg-8 col-md-8" id="">
                    <select name="parent_level_id" id="parent_level_id" class="form-control single-select">
                        <option value="">-- Select Level --</option> 
                        <?php 
                            $lq=$d->select("employee_level_master","society_id='$society_id'");  
                            while ($levelData=mysqli_fetch_array($lq)) {
                        ?>
                        <option value="<?php echo $levelData['level_id'];?>" ><?php echo $levelData['level_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>                    
           <div class="form-footer text-center">
             <input type="hidden" id="level_id" name="level_id" value="" >
             <button id="addLevelBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addLevel"  value="addLevel">
             <button id="addLevelBtn" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('employeeLevelAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
           </div>
         </form>
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
