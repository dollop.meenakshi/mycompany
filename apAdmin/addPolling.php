<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<?php
error_reporting(0);
extract($_REQUEST);
//echo "<pre>";print_r($_REQUEST);

if($action =="Edit"){
       
        extract(array_map("test_input" , $_POST));
        if (isset($voting_id)) {
          $q=$d->select("voting_master","society_id='$society_id' AND voting_id='$voting_id'");
          $row=mysqli_fetch_array($q);
        //  echo "<pre>";print_r( $row);
          extract($row);
        }
}

?>

<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="poll" action="controller/pullingController.php" method="post" enctype="multipart/form-data">
               <?php if(isset($voting_id) ){  ?>
                <input type="hidden" name="voting_attachment_old" value="<?php echo $voting_attachment; ?>">
                  <input type="hidden" name="voting_id" value="<?php echo $voting_id; ?>">
                  <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
                  <input type="hidden" name="editPullingQue" value="editPullingQue">
                <?php }  else {?>


              <input type="hidden" name="addPullingQue" value="addPullingQue">
            <?php } ?> 
              <h4 class="form-header text-uppercase">
                <i class="fa fa-bullhorn"></i>
                <?php echo $xml->string->society; ?> Poll
              </h4>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label">Poll Question <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <?php //IS_1260 alphanumeric ?>
                  <input required="" type="text" class="form-control" id="input-10" name="pulling_question" maxlength="100" value="<?php if(isset($voting_id) ){ echo $voting_question;} ?>" >
                </div>

                <div class="col-sm-4">

                </div>
              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Poll Description</label>
                <div class="col-sm-10">
                  <!-- <input type="text" class="form-control" id="input-12" name="pulling_description" maxlength="200" value="<?php if(isset($voting_id) ){ echo $voting_description;} ?>" > -->
                  <textarea required="" id="summernoteImgage" name="pulling_description"><?php if(isset($voting_id) ){ echo $voting_description;} ?></textarea>
                </div>

              </div>
              <div class="form-group row">
                <label for="input-12" class="col-sm-2 col-form-label">Attachment</label>
                <div class="col-sm-4">
                  <!-- <input type="text" class="form-control" id="input-12" name="pulling_description" maxlength="200" value="<?php if(isset($voting_id) ){ echo $voting_description;} ?>" > -->
                  <input  class="fform-control-file border " type="file" name="voting_attachment">
                    <?php  if ($voting_attachment!="") 
                    { 
                        $ext = pathinfo($voting_attachment, PATHINFO_EXTENSION);
                       
                        if ($ext == 'pdf' || $ext == 'PDF') {
                          $imgIcon = 'img/pdf.png';
                        } elseif ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                          $imgIcon = 'img/jpg.png';
                        } elseif($ext == 'png'){
                          $imgIcon = 'img/png.png';
                        }elseif ($ext == 'doc' || $ext == 'docx') {
                          $imgIcon = 'img/doc.png';
                        }
                         elseif ($ext == 'MP4' || $ext == 'mp4') {
                          $imgIcon = '../img/video.png';
                        } 
                         else{
                          $imgIcon = 'img/doc.png';
                        }
                        ?>
                        <a title="View" class="btn btn-link btn-sm" target="_blank" href="../img/voting_doc/<?php echo $voting_attachment; ?>"><div style="height: 70px;width: 70px;background-image: url(<?php echo $imgIcon ?>);background-repeat: no-repeat;background-size: contain;" class="d-block text-break"></div></a>
                    <?php } ?>
                </div>

              </div>
              <div class="form-group row">
                <label for="input-14" class="col-sm-2 col-form-label">Poll Start Date<span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text"  readonly="" class="form-control" id="poll-start-date"  name="polling_start_time"  value="<?php if(isset($voting_id) ){ echo $voting_start_date;} ?>" >
                </div>
                <label for="input-14" class="col-sm-2 col-form-label">Poll End Date<span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" type="text" readonly="" class="form-control" id="poll-end-date"  name="polling_end_time"  value="<?php if(isset($voting_id) ){ echo $voting_end_date;} ?>" >
                </div>
              </div>

            <?php if(isset($voting_id)){
              $voting_option_master_qry=$d->select("voting_option_master","society_id='$society_id' AND voting_id='$voting_id'");

              $no_of_option_selected = mysqli_num_rows($voting_option_master_qry);
                     // $voting_option_master_data=mysqli_fetch_array($voting_option_master_qry);

            } ?>
              <div class="form-group row">
                <input type="hidden" name="isOptionAdded" id="isOptionAdded" value="Yes">
                <label for="input-12" class="col-sm-2 col-form-label">No Of Options <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                  <select required="" class="form-control" id="no_of_option" name="no_of_option" onchange="getOptionList();">
                    <option value="">-- Select --</option>
                    <?php 
                    for ($i=2; $i <= 10; $i++) { ?>
                      <option <?php if(isset($voting_id) && $no_of_option_selected==$i ){ echo "selected";} ?> value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?php }
                    ?>
                  </select>
                  <!--  <input required="" autocomplete="off" maxlength="2" onkeyup ="getOptionList();" type="text" id="no_of_option" class="form-control no_of_option" id="input-10" name="no_of_option"> -->
                </div>
                <label for="input-13333" class="col-sm-2 col-form-label">Poll For <span class="text-danger">*</span></label>
                <div class="col-sm-4">
                 
                  <select required=""  class="form-control"  name="poll_for" id="poll_for">
                    <option value="0"> <?php echo $xml->string->all; ?>  <?php echo $xml->string->floors; ?></option>
                    <?php 
                    $fq=$d->select("floors_master,block_master","block_master.block_id=floors_master.block_id AND floors_master.society_id='$society_id' ");
                      while ($floorData=mysqli_fetch_array($fq)) { ?>
                      <option <?php if(isset($voting_id) && $poll_for==$floorData['floor_id']  ) { echo "selected";} ?> value="<?php echo $floorData['floor_id'];?>"><?php echo $floorData['floor_name'].' ('.$floorData['block_name'].')';?></option>
                    <?php  }?>
                    
                  </select>
                </div>

              </div>

              <div id="OptionResp" class="form-group row">
                <?php if(isset($voting_id)){
                 
                  $cnt= 1;
                  while ($voting_option_master_data = mysqli_fetch_array($voting_option_master_qry)) {
                  // echo "<pre>";print_r($voting_option_master_data);echo "</pre>";
                    if(trim($voting_option_master_data['option_name']) !="")
                       
                  ?>
                  <label for="option_name<?php echo $cnt;?>"  style="padding-bottom: 15px;" class="col-sm-2 col-form-label">Option <?php echo $cnt; ?> <span class="text-danger">*</span></label>
                  <div class="col-sm-4" style="padding-bottom: 15px;">
                 
                    <input  maxlength="30" onblur="blurtxt();"   type="text" id="option_name<?php echo $cnt;?>" class="form-control  option_name-cls"   name="option_name[]"   value="<?php echo $voting_option_master_data['option_name'];?>">

                     <label id="option_name<?php echo $cnt;?>_div"  style="display: none;"  >Please enter option name <?php echo $cnt;?> </label>
                     
                  </div>
                  <?php $cnt++; }

                  
                }?>
              </div>

              <div class="form-group row">

              </div> 
             
              <div class="form-footer text-center">
                 <?php   if(isset($voting_id)) { ?> 
                    <input type="hidden" name="editPoll" value="editPoll">
                     <button type="submit" class="btn btn-success" name=""><i class="fa fa-check-square-o"></i> Update</button>
                  <?php } else {?>
                      
                <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-check-square-o"></i> Add</button>
              <?php } ?>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!--End Row-->

  </div>
  <!-- End container-fluid-->
</div>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">

/* 
  $('input').change(function(){
    this.value = $.trim(this.value);
  }); */

   $('textarea').change(function(){
    this.value = $.trim(this.value);
  });

  function getOptionList() {
    var data= $('#no_of_option').val();
    if (data>1) {
     $.ajax({
      url: "getOptionList.php",
      cache: false,
      type: "POST",
      data: {no_of_option : data},
      success: function(response){
        $('#OptionResp').html(response);
                    // /IS_853

                    $('#isOptionAdded').val('Yes');

                  }
                });
   } else {
     $('#isOptionAdded').val('No');
     swal("Minimum 2 Option Required !", {
      icon: "error",
    });
     $('#OptionResp').html(' ');
   }
  }

  $( "#poll" ).submit(function( event ) {

    var error = 0;
    $('.option_name-cls').each(function() {
      var clsVal =  $(this).val();
      var eleId = $(this).attr('id');

      if($.trim(clsVal) ==""){
        error++;
          /*$(eleId+'_div').removeClass('valid');
          $(eleId+'_div').addClass('error');*/

          $('#'+eleId+'_div').css('display','inline-block');
          $('#'+eleId+'_div').css('color','#ff0000');
          $('#'+eleId+'_div').text("Please enter Option Value "); //Dollop Infotech 27-Nov-2021
          $(".ajax-loader").hide();
        } else {
          /*$(eleId).addClass('valid');
          $(eleId).removeClass('error');*/
          $(".ajax-loader").show();
          $('#'+eleId+'_div').css('display','none');
        }
      });

//IS_1309
var valOption = [];

 
          $('.option_name-cls').each(function(i){
            valOption[i] = $(this).val();
          });
        
$('.option_name-cls').each(function() {
      var clsVal =  $(this).val();
      var eleId = $(this).attr('id');
var numOccurences =1;

 if($.trim(clsVal) !=""){  
      numOccurences = $.grep(valOption, function (elem) {
        return elem.toLowerCase() === clsVal.toLowerCase();
    }).length;

       if(numOccurences>1){
        error++;
              $('#'+eleId+'_div').css('display','inline-block');
              $('#'+eleId+'_div').css('color','#ff0000');
              $('#'+eleId+'_div').text("Duplicate Option");
      } else {
           $('#'+eleId+'_div').css('display','none');
        }
}
  });
//IS_1309
//alert(error);

    if($('#isOptionAdded').val()=="No"){
      error++;
      $('#no_of_option-error').text("Please select No Of Options");
      $('#no_of_option').addClass('error');
    }

    



    if(error > 0 ){  
      event.preventDefault();
    }  else {  
      $(".ajax-loader").hide();
      $( "#poll" ).submit();
    } 
  });


  function blurtxt(){


var valOption = [];
      
          $('.option_name-cls').each(function(i){
          //  alert($(this).val());
            valOption[i] = $(this).val();
          });
      
var error = 0;



    
    $('.option_name-cls').each(function() {
    var clsVal =  $(this).val();
    var eleId = $(this).attr('id');
    
      if($.trim(clsVal) ==""){
        error++;
        $('#'+eleId+'_div').css('display','inline-block');
        $('#'+eleId+'_div').css('color','#ff0000');
        $('#'+eleId+'_div').text("Please enter Option Value");

      } else {
        $('#'+eleId+'_div').css('display','none');
      }
    });

//IS_1309
$('.option_name-cls').each(function() {
      var clsVal =  $(this).val();
      var eleId = $(this).attr('id');
var numOccurences =1;

 if($.trim(clsVal) !=""){  
      numOccurences = $.grep(valOption, function (elem) {
        return elem.toLowerCase() === clsVal.toLowerCase();
    }).length;
 
       if(numOccurences>1){
        error++;
              $('#'+eleId+'_div').css('display','inline-block');
              $('#'+eleId+'_div').css('color','#ff0000');
              $('#'+eleId+'_div').text("Duplicate Option");
      } else {
           $('#'+eleId+'_div').css('display','none');
        }
}
  });
//IS_1309


 
if(error>0){
  $(':input[type="submit"]').prop('disabled', true);
} else {
  $(':input[type="submit"]').prop('disabled', false);
}


    if(error > 0   ){
    event.preventDefault();
    } /*else {
      $( "#poll" ).submit();
    }*/
  }
</script>
<script src="assets/plugins/material-datepicker/js/moment.min.js"></script>
<script src="assets/plugins/material-datepicker/js/bootstrap-material-datetimepicker.min.js"></script>
<script src="assets/plugins/material-datepicker/js/ja.js"></script>
<script type="text/javascript">
  var FromEndDate = new Date();

  $('#poll-start-date').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD HH:mm',
    minDate: FromEndDate,
  });

  $('#poll-end-date').focus(function() {
    var startDate = $('#poll-start-date').val();
    if (startDate=='') {
      swal('Please select Start Date first');
    } 
  });
  $('#poll-start-date').change(function() {
    var startDate = $('#poll-start-date').val();
    $('#poll-end-date').bootstrapMaterialDatePicker({
      format: 'YYYY-MM-DD HH:mm',
      minDate: startDate,
    });
  });
  
</script>