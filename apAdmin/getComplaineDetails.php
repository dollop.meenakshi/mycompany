<?php 
session_start();
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$society_id=$_COOKIE['society_id'];
extract(array_map("test_input" , $_POST));
if (isset($complain_id)) {
    $q=$d->select("complains_master,unit_master","complains_master.unit_id=unit_master.unit_id AND complains_master.society_id='$society_id' AND complains_master.complain_id='$complain_id'");
    $data=mysqli_fetch_array($q);
    $qData = $d->selectRow("category_name","complaint_category","complaint_category_id='$data[complaint_category]'");
    $catData=mysqli_fetch_array($qData);

      $qc=$d->select("complains_track_master","complain_id='$data[complain_id]'","ORDER BY complains_track_id DESC");
      $atData= mysqli_fetch_array($qc);
      $technician = $atData['technician'];

    if ($data['complaint_new_status']==0) {
        $aComplaint =array(
            'complaint_new_status'=> 1
         );
      
      $d->update("complains_master",$aComplaint,"complain_id='$complain_id'");
    }
?>


  <?php //IS_723 
 if(isset($onlyInfo) && $onlyInfo =="yes" ){ ?>
     <div class="row" >
            <label for="input-10" class="col-sm-4 col-form-label">Message: </label>
            <div class="col-sm-8" id="">
                 <?php if(trim($data['complain_review_msg'])==""){ echo "No Message";} echo $data['complain_review_msg']; ?> 
            </div>
        </div>

         <div class="row" >
            <label for="input-10" class="col-sm-4 col-form-label">Feedback: </label>
            <div class="col-sm-8" id="">
                 <?php if(trim($data['feedback_msg'])==""){ echo "No Feedback";} echo $data['feedback_msg']; ?> 
            </div>
        </div>
        <?php 
            if ($data['complain_closed_by']!=0) { ?>
            <div class="row" >
                <label for="input-10" class="col-sm-4 col-form-label">Closed By: </label>
                <div class="col-sm-8" id="">
                 <?php $adminData = $d->selectArray("bms_admin_master","admin_id='$data[complain_closed_by]' AND society_id='$society_id'"); 
                    echo $adminData['admin_name'];
                 ?> 
                </div>
            </div>   
        <?php  }
        ?>
 <?php }  else {?>


<form id="complainValidation" action="controller/compalainController.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
    <input type="hidden"  value="<?php echo $complain_id; ?>" name="complain_id">
    <input type="hidden"  value="<?php echo $data['unit_id']; ?>" name="unit_id">
    <input type="hidden"  value="<?php echo $data['user_id']; ?>" name="user_id">
    <input type="hidden"  value="<?php echo $data['compalain_title']; ?>" name="compalain_title">
    <input type="hidden"  value="CN<?php echo $data['complain_id']; ?>" name="complain_no">
        <div class="row">
            <label for="input-10" class="col-sm-4 col-form-label">Complain Number </label>
            <div class="col-sm-8" id="">
                <span class="badge badge-pill badge-primary m-1">CN<?php echo $data['complain_id']; ?></span>
            </div>
        </div>
        <div class="row">
            <label for="input-10" class="col-sm-4 col-form-label">From </label>
            <div class="col-sm-8" id="">
                 <?php
                          $qu=$d->select("users_master","user_id='$data[user_id]'");
                          $userData=mysqli_fetch_array($qu);
                          echo $userData['user_full_name']." (".$userData['user_designation'].")"; ?>
            </div>
        </div>
        
        <div class="row">
            <label for="input-10" class="col-sm-4 col-form-label">Complain </label>
            <div class="col-sm-8" id="">
                 <?php echo custom_echo($data['compalain_title'],200) ?>
            </div>
        </div>
         <div class="row">
            <label for="input-10" class="col-sm-4 col-form-label">Description </label>
            <div class="col-sm-8" id="">
                <p style="word-wrap: break-word;"> <?php echo $data['complain_description']; ?></p>
            </div>
        </div>
        <?php if($data['complain_photo']!='')  {?>
        <div class="row">
            <label for="input-10" class="col-sm-4 col-form-label">Image  </label>
            <div class="col-sm-8" id="">
                <a target="_blank" href="../img/complain/<?php echo $data['complain_photo']; ?>"><img style="width: 100%; height: 300px;"  src="../img/complain/<?php echo $data['complain_photo']; ?>" ></a>
            </div>
        </div>
        <?php }?>
        <?php if($data['complaint_voice']!='')  {?>
        <div class="row">
            <label for="input-10" class="col-sm-4 col-form-label">Audio File </label>
            <div class="col-sm-8" id="">
                <audio controls>
                  <source src="../img/complain/<?php echo $data['complaint_voice']; ?>" type="audio/mp3">
                </audio> 
            </div>
        </div>
        <?php }?>
        <div class="row">
            <label for="input-10" class="col-sm-4 col-form-label">Category </label>
            <div class="col-sm-8" id="">
                 <?php echo $catData['category_name']; ?>
            </div>
        </div>
        <div class="row">
            <label for="input-10" class="col-sm-4 col-form-label">Date </label>
                <div class="col-sm-8" id="">
                <?php echo date("d M Y h:i A", strtotime($data['complain_date'])); ?>
                </div>
            </div>
        </div>
      <?php  if ($data['flag_delete']==0) { ?>
        <div class="form-group row" >
            <label for="input-10" class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
            <div class="col-sm-8" id="">
                <input type="hidden" name="complain_status_old" value="<?php echo $data['complain_status']; ?>">
                <select required="" id="complain_statusChange" name="complain_status" class="form-control">
                    <?php  if($data['complain_status']!=1) {   ?>
                    <option <?php if($data['complain_status']==0) {echo 'selected';} ?> value="0">Open</option>
                    <?php  } ?>
                    <?php  if($data['complain_status']!=1) {   ?>
                    <option <?php if($data['complain_status']==3) {echo 'selected';} ?> value="3">In Progress</option>
                    <?php  } ?>
                    <option <?php if($data['complain_status']==1) {echo 'selected';} ?> value="1">Close</option>
                     <?php  if($data['complain_status']!=0) {   ?>
                    <option <?php if($data['complain_status']==2) {echo 'selected';} ?> value="2">Reopen</option>
                    <?php  } ?>

                </select>
            </div>
        </div>
        <div class="form-group row" >
            <label for="input-10" class="col-sm-4 col-form-label">Message </label>
            <div class="col-sm-8" id="">
                <textarea rows="4" maxlength="1000" class="form-control" name="complain_review_msg"></textarea>
            </div>
        </div>
        <div class="form-group row" >
            <label for="input-10" class="col-sm-4 col-form-label">Technician </label>
            <div class="col-sm-8" id="">
                <input type="text" value="<?php echo $technician; ?>" maxlength="50" class="form-control" name="technician">
            </div>
        </div>
        <div class="form-group  row" >
            <label for="input-10" class="col-sm-4 col-form-label">Image / Attachment  </label>
            <div class="col-sm-8" id="">
                <input type="file" id="complains_track_img" name="complains_track_img" class="form-control-file border " accept=".mp4,.MP4,.pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" >
            </div>
        </div>
        <div class="row" >
            <label for="input-10" class="col-sm-4 col-form-label">Audio </label>
            <div class="col-sm-8" id="">
                <input type="file" name="complains_track_voice" class="form-control-file border mp3Only" accept=".mp3">
                (Max file size 3MB)
            </div>
        </div>
        <!-- <div id="sericeTypeDiv" <?php if($data['complain_status']==1) {echo '';} else { echo "style='display: none'";} ?>  >
            <div class="row form-group" >

                <label for="input-sp" class="col-sm-4 col-form-label">Service Type <span class="text-danger">*</span></label>
                <div class="col-sm-8" id="">
                    <select required="" id="service_type" name="service_type" class="form-control">
                        <option <?php if($data['service_type']==0) {echo 'selected';} ?> value="0">Free</option>
                        <option <?php if($data['service_type']==1) {echo 'selected';} ?> value="1">Paid</option>
                        <option <?php if($data['service_type']==2) {echo 'selected';} ?> value="2">Complimentary</option>
                    </select>
                </div>
            </div>
        </div> -->
        <div id="padiServiceDiv" <?php if($data['complain_status']==1 && $data['service_type']==1) {echo '';} else { echo "style='display: none'";} ?> >
            <div class="row form-group" >
                <label for="input-10" class="col-sm-4 col-form-label">Amount <span class="text-danger">*</span></label>
                <div class="col-sm-8" id="">
                    <input required="" type="text" value="<?php if($data['amount']>1) { echo $data['amount']; } ?>"  maxlength="50" class="form-control onlyNumber" inputmode="numeric" name="amount">
                </div>
            </div>
            <div class="row form-group" >
                <label class="col-sm-4 col-form-label"> Payment Status</label>
                  <div class="col-lg-8">
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" <?php if($data['amount_status']=='0'){echo "checked";} ?>  class="form-check-input" value="0" name="amount_status"> Received
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio"  <?php if($data['amount_status']=='1'){echo "checked";} ?>  class="form-check-input" value="1" name="amount_status"> Not Received
                      </label>
                    </div>
                  </div>
            </div>
        </div>
        <div id="ComplimentaryDiv" <?php if($data['complain_status']==1 && $data['service_type']==2) {echo '';} else { echo "style='display: none'";} ?> >
            <div class="row form-group" >
                <label for="input-ComplimentaryDiv" class="col-sm-4 col-form-label">Complimentary Message </label>
                <div class="col-sm-8" id="">
                    <textarea maxlength="150" class="form-control" name="complimentary_msg"><?php echo $data['complimentary_msg'];?></textarea>
                </div>
            </div>
        </div>
        <div class="form-footer text-center">
           <input type="hidden" name="changeStausComp" value="changeStausComp">
          <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
            <?php } else {
                echo  "<br>" . " This complaint is deleted by Employee";
            } ?>
        </div>

    
</form> 
​
<?php } } ?>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$().ready(function() {

     $.validator.addMethod('filesize5mb', function (value, element, arg) {
        var size =3000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }

    }, $.validator.format("file size must be less than or equal to 5MB."));

     $("#complainValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules:{
            complain_status:{
                required:true,
            },
            
            complains_track_voice:{
                 
                filesize:true
            },
            complains_track_img: {
                filesize5mb:true,
            }
             
        },
        messages:{
            complains_track_img:{
                fileExtension: 'Only Valid files are allowed',
            },
            complains_track_voice:{
                extension:'Only MP3 files are allowed',
            },submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                form.submit(); 
          }
        }
    });
});

</script>
<script type="text/javascript">
    $(".photoOnly").change(function () {
        var fileExtension = ['jpeg', 'jpg', 'png'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
            $('.photoOnly').val('');
        }
    });
    $(".docOnly").change(function () {
        var fileExtension = ['jpeg', 'jpg', 'png', 'doc','docx', 'pdf', 'csv', 'xls' , 'xlsx'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            // alert("Only formats are allowed : "+fileExtension.join(', '));
            swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
            $('.idProof').val('');
        }
    });
    $(".mp3Only").change(function () {
        var fileExtension = ['mp3','wav','flac','ogg','m3u','acc','wma','midi','aif','m4a','mpa','pls'];
        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
            $('.mp3Only').val('');
        }
    });

    $.validator.addMethod('filesize', function (value, element, arg) {
        var size =3000000;
        if(element.files.length){
             
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }

    }, $.validator.format("file size must be less than or equal to 3MB."));


    $('#complain_statusChange').on('change', function() {
        if(this.value=="1"){
          $('#sericeTypeDiv').css('display','block');
        } else {
          $('#sericeTypeDiv').css('display','none');
        }
        
    });

    $('#service_type').on('change', function() {
        if(this.value=="1"){
          $('#padiServiceDiv').css('display','block');
          $('#ComplimentaryDiv').css('display','none');
        }else if(this.value=="2"){
          $('#padiServiceDiv').css('display','none');
          $('#ComplimentaryDiv').css('display','block');
        } else {
          $('#padiServiceDiv').css('display','none');
          $('#ComplimentaryDiv').css('display','none');
        }
        
    });

    $(".onlyNumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

</script>