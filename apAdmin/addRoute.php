<?php
if(isset($_POST['addRouteBtn']) && $_POST['addRouteBtn'] == "addRouteBtn")
{
    extract($_POST);
}
$all = [];
$route_retailer_id_arr = [];
if(isset($_POST['editRoute']) && isset($_POST['route_id']))
{
    $route_id = $_POST['route_id'];
    $q = $d->select("route_master","route_id = '$route_id'");
    $data = $q->fetch_assoc();
    extract($data);

    $q1 = $d->selectRow("route_retailer_id,retailer_id","route_retailer_master","route_id = '$route_id'");
    while($data1 = $q1->fetch_assoc())
    {
        $all[] = $data1;
        $route_retailer_id_arr[] = $data1['retailer_id'];
    }
    $form_id = "routeUpdateForm";
}
else
{
    $form_id = "routeAddForm";
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="<?php echo $form_id; ?>" action="controller/routeController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <?php
                            if(isset($_POST['editRoute']) && isset($_POST['route_id']))
                            {
                            ?>
                            <i class="fa fa-edit"></i>
                            Edit Route
                            <?php
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-plus"></i>
                            Add Route
                            <?php
                            }
                            ?>
                            </h4>
                            <div class="form-group row">
                                <label for="country_id" class="col-sm-2 col-form-label">Country <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="country_id" name="country_id" required onchange="getStateCity(this.value);">
                                        <option value="">-- Select --</option>
                                        <?php
                                        $cq = $d->select("countries");
                                        while ($cd = $cq->fetch_assoc())
                                        {
                                        ?>
                                        <option <?php if($country_id == $cd['country_id']) { echo 'selected'; } ?> value="<?php echo $cd['country_id'];?>"><?php echo $cd['country_name'];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label for="state_city_id" class="col-sm-2 col-form-label">State-City <span class="required">*</span></label>
                                <div class="col-sm-4">
                                    <select class="form-control single-select" id="state_city_id" name="state_city_id" required onchange="getRetailer(this.value);">
                                        <option value="">-- Select --</option>
                                        <?php
                                        if(isset($editRoute) || isset($addRouteBtn) && (isset($city_id) && $city_id != ""))
                                        {
                                            $qt = $d->selectRow("s.state_id,s.state_name,c.city_id,c.city_name","states AS s JOIN cities AS c ON s.state_id = c.state_id","s.country_id = '$country_id' AND c.flag = 1");
                                            while ($qd = $qt->fetch_assoc())
                                            {
                                            ?>
                                        <option <?php if($city_id == $qd['city_id']) { echo 'selected'; } ?> value="<?php echo $qd['city_id']."-".$qd['state_id'];?>"><?php echo $qd['city_name'];?>-<?php echo $qd['state_name'];?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="route_name" class="col-sm-2 col-form-label">Route Name <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input value="<?php if(isset($editRoute)){ echo $route_name; } ?>" required type="text" class="form-control" name="route_name" id="route_name" maxlength="100">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="retailer_id" class="col-sm-2 col-form-label">Retailer </label>
                                <div class="col-sm-10">
                                    <select class="form-control single-select" multiple id="retailer_id" name="retailer_id[]" >
                                        <?php
                                        if(isset($editRoute) || isset($addRouteBtn))
                                        {
                                            $qr = $d->selectRow("rm.retailer_id,rm.retailer_name,amn.area_name","retailer_master AS rm JOIN area_master_new AS amn ON amn.area_id = rm.area_id","rm.country_id = '$country_id' AND rm.state_id = '$state_id' AND rm.city_id = '$city_id'");
                                            while ($qdr = $qr->fetch_assoc())
                                            {
                                            ?>
                                        <option <?php if(in_array($qdr['retailer_id'],$route_retailer_id_arr)) { echo 'selected'; } ?> value="<?php echo $qdr['retailer_id']; ?>"><?php echo $qdr['retailer_name'] . " (" . $qdr['area_name'] . ")";?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="route_description" class="col-sm-2 col-form-label"><?php echo $xml->string->description; ?></label>
                                <div class="col-sm-10">
                                    <textarea id="route_description" rows="5" name="route_description" class="form-control"><?php if($route_description != "") { echo $route_description; } ?></textarea>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                            <div class="form-footer text-center">
                                <input type="hidden" name="addRoute" value="addRoute">
                                <?php
                                if(isset($editRoute))
                                {
                                    foreach($route_retailer_id_arr AS $key => $value)
                                    {
                                    ?>
                                <input type="hidden" class="form-control" name="old_retailer_id[<?php echo $key; ?>]" value="<?php echo $value; ?>">
                                    <?php
                                    }
                                ?>
                                <input type="hidden" class="form-control" name="route_id" id="route_id" value="<?php echo $route_id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script>
function getStateCity(country_id)
{
    $.ajax({
        url: 'controller/routeController.php',
        data: {getStateCityTag:"getStateCityTag",country_id:country_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#state_city_id').empty();
            response = JSON.parse(response);
            $('#state_city_id').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#state_city_id').append("<option value='"+value.city_id+"-"+value.state_id+"'>"+value.city_name+" - "+value.state_name+"</option>");
            });
        }
    });
}

function getRetailer(city_state_id)
{
    var country_id = $('#country_id').val();
    const city_id = city_state_id.substring(0, city_state_id.indexOf('-'));
    const state_id = city_state_id.substring(city_state_id.indexOf('-') + 1);
    $.ajax({
        url: 'controller/routeController.php',
        data: {getRetailerTag:"getRetailerTag",country_id:country_id,state_id:state_id,city_id:city_id,csrf:csrf},
        type: "post",
        success: function(response)
        {
            $('#retailer_id').empty();
            response = JSON.parse(response);
            $('#retailer_id').append("<option value=''>--Select--</option>");
            $.each(response, function(key, value)
            {
                $('#retailer_id').append("<option value='"+value.retailer_id+"'>"+value.retailer_name+" ("+value.retailer_contact_person+" - "+value.area_name+")</option>");
            });
        }
    });
}
</script>