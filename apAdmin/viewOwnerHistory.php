<?php
extract($_REQUEST);
$id = (int)$id;
$qq=$d->select("users_master,unit_master,block_master,floors_master","users_master.user_id='$id' AND unit_master.unit_id=users_master.unit_id AND block_master.block_id=floors_master.block_id AND floors_master.floor_id=unit_master.floor_id AND users_master.society_id='$society_id'  $blockAppendQuery ");
$userData=mysqli_fetch_array($qq);
extract($userData);
$society_id = $userData['society_id'];
$user_id = $userData['user_id'];
$unit_id = $userData['unit_id'];
error_reporting(0);
$userType =$unit_status;
$user_type =$user_type;
if ($user_status==0) {
  echo ("<script LANGUAGE='JavaScript'>
    window.location.href='pendingUser?id=$id';
    </script>");
  exit();
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <?php if(mysqli_num_rows($qq)>0) { ?>
      <div class="row pt-2 pb-2">
        <div class="col-sm-12">
          <h4 class="page-title"><?php echo $user_full_name; ?> (<?php echo $block_name; ?>-<?php echo $floor_name; ?>)</h4>
        </div>
      </div>
      <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified">
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#notifications" data-toggle="pill" class="nav-link active"><i class="fa fa-bell-o"></i> <span class="hidden-xs"><?php echo $xml->string->notifications; ?></span></a>
                </li>
                 <li class="nav-item">
                  <a href="javascript:void();" data-target="#event" data-toggle="pill" class="nav-link "><i class="fa fa-calendar"></i> <span class="hidden-xs"><?php echo $xml->string->event; ?> <?php echo $xml->string->booking; ?></span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#penalty" data-toggle="pill" class="nav-link "><i class="fa fa-ticket"></i> <span class="hidden-xs"><?php echo $xml->string->penalty; ?> </span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#complain" data-toggle="pill" class="nav-link "><i class="fa fa-bullhorn"></i> <span class="hidden-xs"><?php echo $xml->string->complaint; ?>  </span></a>
                </li>
                <li class="nav-item">
                  <a href="javascript:void();" data-target="#lostfound" data-toggle="pill" class="nav-link "><i class="fa fa-cubes"></i> <span class="hidden-xs"><?php echo $xml->string->lost_found; ?></span></a>
                </li>
               
              </ul>
              <div class="tab-content p-3">
                <div class="tab-pane active" id="notifications">
                  <div class="table-responsive">
                    <table class="table table-bordered exampleReport">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th> <?php echo $xml->string->title; ?></th>
                              <th> <?php echo $xml->string->description; ?></th>
                              <th><?php echo $xml->string->date; ?></th>
                              <!-- <th>Action</th> -->
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        $q=$d->select("user_notification","society_id='$society_id' AND user_id='$user_id' ","ORDER BY user_notification_id DESC LIMIT 1000");
                        while($row=mysqli_fetch_array($q))
                        {
                          extract($row);
                        ?>
                          <tr>
                              <td><?php echo $i++; ?></td>
                              <td><?php echo $notification_title; ?></td>
                              <td><?php echo $notification_desc;  ?></td>
                              <td><?php echo $notification_date; ?></td>
                              <!-- <td><?php echo $notification_action; ?></td> -->
                          </tr>
                          <?php }?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane " id="event">
                  <table class="table table-bordered exampleReport">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo $xml->string->event; ?></th>
                            <th><?php echo $xml->string->amount; ?></th>
                            <th><?php echo $xml->string->adult; ?></th>
                            <th><?php echo $xml->string->child; ?></th>
                            <th><?php echo $xml->string->guest; ?></th>
                            <th><?php echo $xml->string->event; ?> <?php echo $xml->string->date; ?></th>
                            <th><?php echo $xml->string->paid; ?> <?php echo $xml->string->date; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $q=$d->select("event_master,event_attend_list,event_days_master,unit_master,block_master,users_master","users_master.user_id=event_attend_list.user_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id=event_attend_list.unit_id  AND event_days_master.events_day_id=event_attend_list.events_day_id AND event_attend_list.event_id=event_master.event_id AND  event_master.society_id = '$society_id' AND  event_attend_list.unit_id='$unit_id' AND event_attend_list.user_id='$user_id' AND event_attend_list.book_status=1","order by event_attend_list.event_attend_id DESC LIMIT 100");
                      $i=1;
                      while ($data=mysqli_fetch_array($q)) {
                       ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['event_title']; ?>-<?php echo $data['event_day_name']; ?></td>
                            <td><?php echo number_format($data['recived_amount']- $data['transaction_charges'],2); ?></td>
                            <td><?php echo $data['going_person']; ?></td>
                            <td><?php echo $data['going_child']; ?></td>
                            <td><?php echo $data['going_guest']; ?></td>
                            <td><?php  echo $data['event_date']; ?></td>
                            <td><?php  echo $data['payment_received_date']; ?></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                </table>
                </div>
                <div class="tab-pane " id="penalty">
                  <div class="table-responsive">
                    <table class="table table-bordered exampleReport">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th><?php echo $xml->string->status; ?> </th>
                              <th><?php echo $xml->string->amount; ?></th>
                              <th><?php echo $xml->string->penalty; ?></th>
                              <th><?php echo $xml->string->penalty; ?> <?php echo $xml->string->date; ?></th>
                              <?php //IS_595?>
                              <th><?php echo $xml->string->paid; ?> <?php echo $xml->string->date; ?></th>
                              <th><?php echo $xml->string->created_on; ?></th>
                              <th><?php echo $xml->string->photo; ?> </th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        $q=$d->select("penalty_master","user_id='$user_id' AND society_id='$society_id'","ORDER BY created_at DESC");
                        $counter = 1;
                        while ($data=mysqli_fetch_array($q)){ ?>
                        <tr>
                         <td><?php echo $counter;  $counter++; ?></td>
                        <td><?php
                         if ($data['paid_status']==0) { ?>
                           <a href="javascript:void();"><span class="badge badge-danger m-1 pointerCursor">Unpaid</span></a>
                        <?php } elseif ($data['paid_status']==1) { ?>
                          <span class="badge badge-success  m-1 pointerCursor">Paid</span>
                          <a href="#" class="badge badge-warning"></a>
                        <?php  }  ?></td>
                        <td><?php echo number_format($data['penalty_amount'],2);?></td>
                        <td><?php echo $data['penalty_name']; ?></td>
                        <td><?php echo date("d M Y h:i A", strtotime($data['penalty_datetime'])); ?></td>
                        <td><?php if($data['paid_status']==1) {
                          if($data['penalty_receive_date']=="0000-00-00 00:00:00") {
                             $paneltyDataQry=$d->select("penalty_master,unit_master,users_master,expenses_balance_sheet_master","
                      expenses_balance_sheet_master.society_id=penalty_master.society_id AND expenses_balance_sheet_master.balancesheet_id=penalty_master.balancesheet_id AND penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id=".$society_id ." AND penalty_master.penalty_id=".$data['penalty_id']."  AND penalty_master.penalty_amount =expenses_balance_sheet_master.income_amount   ","");

                      if(mysqli_num_rows($paneltyDataQry) > 0 ){
                         $paneltyDataQData=mysqli_fetch_array($paneltyDataQry);
                         if( trim($paneltyDataQData['expenses_add_date'])==""){
                          echo "Not Available";
                         }else{
                          echo date("d-m-Y h:i A", strtotime($paneltyDataQData['expenses_add_date']));
                         }
                       } else {
                          echo "Not Available";
                       }
                          } else {
                             echo date("d M Y h:i A", strtotime($data['penalty_receive_date']));
                          }
                           } else { echo "Not Available";} ?></td>
                           <td  data-order="<?php echo date("U",strtotime($data['created_at'])); ?>" ><?php echo date("d M Y h:i A", strtotime($data['created_at'])); ?></td>

                        <td><?php if ($data['penalty_photo']!='') { ?>
                           <a data-caption="Photo Name : <?php echo $data["penalty_photo"]; ?>" target="_blank" href="../img/billReceipt/<?php echo $data['penalty_photo']; ?>"><img width="50" height="50"  src="../img/billReceipt/<?php echo $data['penalty_photo']; ?>" href="#divFormPenalty<?php echo $data['penalty_id'];?>" class="btnForm"></a>
                        <?php } else {
                          echo "Not Uploaded";
                        } ?>
                          <div id="divFormPenalty<?php echo $data['penalty_id'];?>" style="display:none">
                            <picture>
                              <source srcset="../img/billReceipt/<?php echo $data['penalty_photo']; ?>" media="(max-width: 800px)">
                              <img style="max-height: 500px; max-width: 500px;"   src="../img/billReceipt/<?php echo $data['penalty_photo']; ?>" />
                            </picture>
                          </div>
                        </td>
                    </tr>
                    <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane " id="complain">
                  <div class="table-responsive">
                    <table class="table table-bordered exampleReport">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo $xml->string->status; ?></th>
                          <th>#</th>
                          <th><?php echo $xml->string->title; ?></th>
                          <th><?php echo $xml->string->photo; ?></th>
                          <th><?php echo $xml->string->date; ?></th>
                          <th><?php echo $xml->string->status; ?></th>
                          <th><?php echo $xml->string->category; ?></th>
                          <th><?php echo $xml->string->feedback; ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        $where = "society_id='$society_id' AND user_id='$user_id'";
                        $q=$d->select("complains_master",$where,"ORDER BY complain_id DESC");
                        while ($data=mysqli_fetch_array($q)) {?>
                        <tr>
                             <td><?php echo $i; ?></td>
                            <td><?php
                            if ($data['flag_delete']==0) {
                             if ($data['complain_status']==0) {  ?>
                              <span class="badge badge-warning m-1 ">OPEN</span>
                            <?php } elseif ($data['complain_status']==1) { ?>
                              <span class="badge badge-success  m-1 ">CLOSE</span>
                            <?php  } elseif ($data['complain_status']==2) { ?>
                              <span class="badge badge-info  m-1 ">REOPEN</span>
                            <?php } elseif ($data['complain_status']==3) { ?>
                              <span class="badge badge-primary  m-1 ">In Progress</span>
                            <?php } } else {
                              echo "Deleted by User";
                            }?></td>
                            <td>
                              <form action="complaintHistory" method="POST">
                                <input type="hidden" name="complain_id" value="<?php echo $data['complain_id']; ?>">
                                <button title="View History" class="btn btn-link p-0">CN<?php echo $data['complain_id']; ?> <i class="fa  fa-history" aria-hidden="true"></i></button>
                              </form>
                            </td>
                            <td class="tableWidth"><?php echo $data['compalain_title']; ?></td>
                            <td><?php if ($data['complain_photo']!='') { ?>
                               <a data-fancybox="images" data-caption="Photo Name : <?php echo $data["complain_photo"]; ?>" target="_blank" href="../img/complain/<?php echo $data['complain_photo']; ?>"><img width="50" height="50"  src="../img/complain/<?php echo $data['complain_photo']; ?>" ></a>
                            <?php } else {
                              echo "Not Uploaded";
                            } ?></td>
                            <td><?php echo $data['complain_date']; ?></td>
                            <td class="tableWidth"><?php echo $data['complain_review_msg']; ?></td>
                            <td><?php
                                $q11=$d->selectRow("category_name","complaint_category","complaint_category_id='$data[complaint_category]'");
                                $catData=mysqli_fetch_array($q11);
                                echo $catData['category_name']; ?></td>
                            <td><?php echo $data['feedback_msg']; ?></td>
                        </tr>
                        <?php $i++; } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="tab-pane " id="lostfound">
                  <div class="row">
                    <?php
                      $q=$d->select("lost_found_master,users_master,block_master,unit_master","lost_found_master.society_id='$society_id'  AND lost_found_master.active_status='0' AND  lost_found_master.user_id=users_master.user_id AND lost_found_master.user_id='$user_id' AND block_master.block_id=unit_master.block_id AND block_master.block_id=lost_found_master.block_id AND unit_master.unit_id=lost_found_master.unit_id" );
                      if(mysqli_num_rows($q)>0) {
                        while($row=mysqli_fetch_array($q)){
                          $path = '../img/lostFound/'.$row['lost_found_image'];
                        ?>
                        <div class="col-lg-3">
                         <div class="card">
                          <a>
                            <img   onerror="this.src='../img/app_icon/Lost-foundxxxhdpi.png'"  style="width: 100%;height: 200px;" <?php if(file_exists($path)){ ?> class="btnForm" <?php } ?> href="#divForm<?php echo $row['lost_found_master_id'];?>" src="../img/lostFound/<?php echo $row['lost_found_image']; ?>" alt="<?php echo $row['lost_found_title']; ?>"></a>
                            <div id="divForm<?php echo $row['lost_found_master_id'];?>" style="display:none">
                              <picture>
                                <source srcset="../img/lostFound/<?php echo $row['lost_found_image']; ?>" media="(max-width: 800px)">
                                <img style="max-height: 500px; max-width: 500px;"   src="../img/lostFound/<?php echo $row['lost_found_image']; ?>" />
                              </picture>
                              <br>
                              <span><center><b><?php echo $row['lost_found_title']; ?></b></center></span>
                            </div>
                            <div class="p-2">
                               <h5 class="card-title text-primary"><?php echo $row['lost_found_title']; ?>
                                 <?php if($row['lost_found_type']==0)  { ?>
                                 <span   data-toggle="modal" data-target="#userDetails" onclick="viewLostFound('<?php echo $row['lost_found_master_id']; ?>');" class="badge badge-warning m-1 pointerCursor">Found by</span>
                                <?php } else { ?>
                                 <span   data-toggle="modal" data-target="#userDetails" onclick="viewLostFound('<?php echo $row['lost_found_master_id']; ?>');" class="badge badge-danger m-1 pointerCursor">Lost by</span>
                                <?php } ?>
                               </h5><span   data-toggle="modal" data-target="#userDetails" onclick="viewLostFound('<?php echo $row['lost_found_master_id']; ?>');" class="pointerCursor"><?php echo $row['user_full_name'] .'('.$row['user_designation'].')'; ?></span>
                               <hr>
                                <?php echo custom_echo($row['lost_found_description'],200); ?>
                                <Br>
                                <span>Date: <?php echo $row['lost_found_date']; ?></span><br>
                                <!--  <form action="controller/buildingController.php" method="post">
                                  <input type="hidden" name="lost_found_master_id"  value="<?php echo $row['lost_found_master_id']; ?>">
                                   <button type="submit" class="form-btn btn btn-sm btn-danger waves-effect waves-light m-1" title="Delete"> <i class="fa fa-trash-o"></i> Delete</button>
                                 </form> -->
                            </div>
                         </div>
                        </div>
                      <?php }
                      } else {
                      echo "<img src='img/no_data_found.png'>";
                    } ?>
                  </div>
                </div>
                
              </div>
            </div>
        </div>
      </div>
      <!-- Agreement Section -->
<?php  } else {
  echo "<img width='250' src='img/no_data_found.png'>";
} ?>
</div>
</div>



<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#imgInp").change(function() {
    readURL(this);
  });
</script>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  function viewLostFound(lost_id) {
    $.ajax({
      url: "getLostFoundUserDetails.php",
      cache: false,
      type: "POST",
      data: {lost_id : lost_id},
      success: function(response){
        $('#userDetailsModal').html(response);
      }
    });
  }
</script>
<script type="text/javascript">
  $(function(){
    $(".btnForm").fancybox();
  });
</script>
<div class="modal fade" id="userDetails">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"><?php echo $xml->string->view; ?> <?php echo $xml->string->details; ?> </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="userDetailsModal">
      </div>
    </div>
  </div>
</div>