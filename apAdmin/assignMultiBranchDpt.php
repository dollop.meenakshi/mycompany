<?php error_reporting(0);
  
 
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-5 col-3">
          <h4 class="page-title">Absent Employee Attendance</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6 text-right">
        <a href="addEmpMultiBranchDepartment" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteMultiAssignDptBlkUser');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
     </div>
     <form action="" class="branchDeptFilter">
        <!-- <div class="row pt-2 pb-2">
          <div class="col-md-3 form-group">
              <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){echo $from =  $_GET['from'];}else{echo  $from = date('Y-m-d');} ?>">  
          </div>
          <div class="col-md-3 form-group ">
              <input class="btn btn-success btn-sm " type="submit" name="getReport" class="form-control" value="Get Data">
          </div>    
      </div> -->
     </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table id="example" class="table table-bordered">
                    <thead>
                        <tr>
                        <th>#</th>                        
                        <th>Sr.No</th>                        
                        <th>Employee </th>
                        <th>Branch</th>
                        <th>Department</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 1;
                            $q = $d->selectRow('employee_multiple_department_branch.*,users_master.user_full_name,block_master.block_name,floors_master.floor_name',"employee_multiple_department_branch
                            LEFT JOIN  users_master ON employee_multiple_department_branch.user_id=users_master.user_id
                            LEFT JOIN  block_master ON employee_multiple_department_branch.branch_id=block_master.block_id
                            LEFT JOIN  floors_master ON employee_multiple_department_branch.floor_id=floors_master.floor_id
                            ");
                            while ($data = mysqli_fetch_assoc($q)) {
                        ?>
                            <tr>
                            <td><input type="hidden" name="id" id="id"  value="<?php echo $data['employee_multiple_department_branch_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['employee_multiple_department_branch_id']; ?>"> </td>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['user_full_name']; ?></td>
                            <td><?php echo $data['block_name']; ?></td>
                            <td><?php if($data['floor_id']=="0" ||  $data['floor_id']<=0){
                                echo "All Department";
                            }else{
                               echo $data['floor_name'];
                            } ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="" role="alert">
                    <!-- <span><strong>Note :</strong> No Data !!!!!</span>
                    <span><strong>Note :</strong> Please Select Department !!!</span> -->
                </div>
                </div>
            </div>
          </div>
        </div>
     </div><!-- End Row-->
    </div>
  </div>
 
<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">

</script>

