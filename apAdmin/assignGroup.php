<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Assign Group List</h4>
            </div>
            <div class="col-sm-3">
                <div class="btn-group float-sm-right">
                    <a href="addAssignGroup" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add</a>
                </div>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Holiday Group Name</th>
                                        <th>Common Assign Name</th>
                                        <th>Common Assign Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $q = $d->selectRow("hgam.holiday_group_assign_id,hgam.common_assign_type,hgm.holiday_group_name,GROUP_CONCAT(bm.block_name) AS block_name,GROUP_CONCAT(fm.floor_name) AS department_name,GROUP_CONCAT(elm.level_name) AS level_name,GROUP_CONCAT(um.user_full_name) AS user_name","holiday_group_assign_master AS hgam JOIN holiday_group_master AS hgm ON hgm.holiday_group_id = hgam.holiday_group_id LEFT OUTER JOIN block_master AS bm ON (FIND_IN_SET(bm.block_id, hgam.common_assign_id) > 0 AND hgam.common_assign_type = 0) LEFT OUTER JOIN floors_master AS fm ON (FIND_IN_SET(fm.floor_id, hgam.common_assign_id) > 0 AND hgam.common_assign_type = 1) LEFT OUTER JOIN employee_level_master AS elm ON (FIND_IN_SET(elm.level_id, hgam.common_assign_id) > 0 AND hgam.common_assign_type = 2) LEFT OUTER JOIN users_master AS um ON (FIND_IN_SET(um.user_id, hgam.common_assign_id) > 0 AND hgam.common_assign_type = 3)","","GROUP BY hgam.holiday_group_assign_id");
                                    while ($data = $q->fetch_assoc())
                                    {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $data['holiday_group_name']; ?></td>
                                        <td>
                                            <?php
                                            if($data['common_assign_type'] == 0)
                                            {
                                                echo $data['block_name'];
                                            }
                                            elseif($data['common_assign_type'] == 1)
                                            {
                                                echo $data['department_name'];
                                            }
                                            elseif($data['common_assign_type'] == 2)
                                            {
                                                echo $data['level_name'];
                                            }
                                            else
                                            {
                                                echo $data['user_name'];
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($data['common_assign_type'] == 0)
                                            {
                                                echo "Branch Wise";
                                            }
                                            elseif($data['common_assign_type'] == 1)
                                            {
                                                echo "Department Wise";
                                            }
                                            elseif($data['common_assign_type'] == 2)
                                            {
                                                echo "Level Wise";
                                            }
                                            else
                                            {
                                                echo "User Wise";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <form action="addAssignGroup" method="post">
                                                    <input type="hidden" name="holiday_group_assign_id" value="<?php echo $data['holiday_group_assign_id']; ?>">
                                                    <input type="hidden" name="editGroupAssign" value="editGroupAssign"/>
                                                    <button type="submit" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit Group Assign"><i class="fa fa-pencil"></i></button>
                                                </form>
                                                <form class="deleteForm<?php echo $data['holiday_group_assign_id']; ?> ml-2" action="controller/HolidayController.php" method="post">
                                                    <input type="hidden" name="holiday_group_assign_id" value="<?php echo $data['holiday_group_assign_id']; ?>">
                                                    <input type="hidden" name="deleteGroupAssign" value="deleteGroupAssign"/>
                                                    <button type="button" class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $data['holiday_group_assign_id']; ?>');" data-toggle="tooltip" title="Delete Group Assign"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->