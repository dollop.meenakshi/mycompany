<?php
error_reporting(0);
//$month_year= $_REQUEST['month_year'];
//$laYear = $_REQUEST['laYear'];
$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
if (isset($_GET['year'])) {
  $year = $_GET['year'];
} else {
  $year = date('Y');

}
$ltId = (int)$_REQUEST['ltId'];

?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-12">
        <h4 class="page-title">Leave Balance Report</h4>
      </div>

    </div>

    <form action="" method="get" class="branchDeptFilter">
      <div class="row pb-2">
        <?php include('selectBranchDeptEmpForFilterAll.php');?>
        <div class="col-md-3 form-group">
            <select name="ltId" class="form-control single-select">
              <option value="">All Leave Type</option> 
                <?php 
                  $qLeaveType=$d->select("leave_type_master","society_id='$society_id' AND leave_type_delete=0");  
                  while ($qLeaveTypeData=mysqli_fetch_array($qLeaveType)) {
                ?>
              <option  <?php if($ltId==$qLeaveTypeData['leave_type_id']) { echo 'selected';} ?> value="<?php echo  $qLeaveTypeData['leave_type_id'];?>" ><?php echo $qLeaveTypeData['leave_type_name'];?></option>
              <?php } ?>
            </select>
        </div>
        <?php include('selectYearForFilter.php');?>
        
        <div class="col-lg-2 from-group col-6">
          <input  class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Report">
        </div>
      </div>
    </form>
    <!-- End Breadcrumb-->
    
    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-body">

            <div class="table-responsive">
              <?php
              if(isset($bId) && $bId>0) {
                $blockFilterQuery = " AND users_master.block_id='$bId'";
              }
              if (isset($dId) && $dId > 0) {
                $deptFilterQuery = " AND users_master.floor_id='$dId'";
              }
              if (isset($uId) && $uId > 0) {
                $userFilterQuery = " AND users_master.user_id='$uId'";
              }
              if(isset($ltId) && $ltId>0) {
                $leaveTypeFilterQuery = " AND leave_assign_master.leave_type_id='$ltId'";
              }
              $q = $d->selectRow("*,
                (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
                      AND leave_status=1 AND paid_unpaid=0 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=0 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year') AS total_full_day_leave, 
                (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
                      AND leave_status=1 AND paid_unpaid=0 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=1 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year') AS total_half_day_leave, 
                (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
                      AND leave_status=1 AND paid_unpaid=1 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=0 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year') AS unpaid_full_day_leave, 
                (SELECT COUNT(*) FROM leave_master WHERE leave_master.user_id = users_master.user_id 
                      AND leave_status=1 AND paid_unpaid=1 AND leave_type_id=leave_assign_master.leave_type_id AND leave_day_type=1 AND DATE_FORMAT(leave_master.leave_start_date,'%Y')='$year') AS unpaid_half_day_leave,
                (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = users_master.user_id AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year='$year' AND is_leave_carry_forward=0) AS pay_out_leave,
                (SELECT SUM(no_of_payout_leaves) FROM leave_payout_master WHERE leave_payout_master.user_id = users_master.user_id AND leave_type_id=leave_assign_master.leave_type_id AND leave_payout_year='$year' AND is_leave_carry_forward=1) AS carry_forward_leave
                ", 
                "block_master,floors_master,users_master,leave_assign_master,leave_type_master"," users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND leave_assign_master.leave_type_id=leave_type_master.leave_type_id AND leave_assign_master.user_id=users_master.user_id AND users_master.user_status=1 AND users_master.active_status =0 AND users_master.delete_status=0 AND leave_assign_master.assign_leave_year='$year' $blockFilterQuery $deptFilterQuery $userFilterQuery $leaveTypeFilterQuery $blockAppendQueryUser");

              $i = 1;

              ?>

                <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Branch </th>
                      <th>Department</th>
                      <th>Leave Type</th>
                      <th>Total Leave</th>
                      <th>Balance Leave</th>
                      <th>Paid Leave</th>
                      <th>Unpaid Leave</th>
                      <th>Pay Out Leave</th>
                      <th>Carry Forward Leave</th>
                      <th>Year</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    while ($data = mysqli_fetch_array($q)) { ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['user_full_name']; ?></td>
                        <td><?php echo $data['block_name']; ?></td>
                        <td><?php echo $data['floor_name']; ?></td>
                        <td><?php echo $data['leave_type_name']; ?></td>
                        <td><?php echo $data['user_total_leave']; ?></td>
                        <td><?php echo $data['user_total_leave']-($data['total_full_day_leave']+($data['total_half_day_leave']/2))-$data['pay_out_leave']-$data['carry_forward_leave']; ?></td>
                        <td><?php echo ($data['total_full_day_leave']+($data['total_half_day_leave']/2)); ?></td>
                        <td><?php echo ($data['unpaid_full_day_leave']+($data['unpaid_half_day_leave']/2)); ?></td>
                        <td><?php echo $data['pay_out_leave']; ?></td>
                        <td><?php echo $data['carry_forward_leave']; ?></td>
                        <td><?php echo $data['assign_leave_year']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>

                </table>


              

            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-    fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->

