<?php 
extract(array_map("test_input" , $_POST));
$result = $d->select("classified_master,classified_category,classified_sub_category,society_master,users_master","users_master.user_id=classified_master.user_id AND society_master.society_id=classified_master.society_id AND classified_master.classified_status='0' AND classified_master.classified_category_id=classified_category.classified_category_id AND classified_master.classified_sub_category_id=classified_sub_category.classified_sub_category_id $countryAppendQuerySociety ");
$results_per_page = 15;  
$number_of_result = mysqli_num_rows($result);
$number_of_page = ceil ($number_of_result / $results_per_page);
if (!isset ($_POST['page']) ) {  
  $page = 1;  
} else {  
  $page = $_POST['page'];  
}
$page_first_result = ($page-1) * $results_per_page; 
?>

<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9">
        <h4 class="page-title">Buy / Sell Items</h4>
        
      </div>
    </div>
    <!-- End Breadcrumb-->
      <?php $q = $d->select("classified_master,classified_category,classified_sub_category,society_master,users_master","users_master.user_id=classified_master.user_id AND society_master.society_id=classified_master.society_id AND classified_master.classified_status='0' AND classified_master.classified_category_id=classified_category.classified_category_id AND classified_master.classified_sub_category_id=classified_sub_category.classified_sub_category_id $countryAppendQuerySociety ","ORDER BY classified_master.classified_master_id DESC LIMIT $page_first_result, $results_per_page");


        // print_r($q);
      if(mysqli_num_rows($q)>0) { ?>
        <div class="row">
          <?php $i = 0;
            while($row=mysqli_fetch_array($q)){
            $image = explode("~",$row['classified_image']);
          ?> 
          <div class="col-12 col-lg-6 col-xl-4">
            <div class="card profile-card-2">
              <form style="position: absolute;z-index: 900;right: 0;padding: 5px;" action="controller/classifiedController.php" method="post" accept-charset="utf-8">
                <input type="hidden" name="iteamName" value="<?= $row['classified_add_title'] ?>">
                <input type="hidden" name="classified_master_id" value="<?= $row['classified_master_id'] ?>">
                <button style="padding: 0;" class="btn form-btn btn-sm btn-danger" type="submit"><i class="fa fa-trash-o"></i></button>
              </form>
              <div class="card-img-block">
                <div id="carousel-<?=$i?>" class="carousel slide" data-ride="carousel">
                  <ul class="carousel-indicators">
                    <?php for ($j=0; $j < count($image) ; $j++) { ?>
                      <li data-target="#demo" data-slide-to="<?=$j?>" class="<?= ($j==0) ? 'active' : ''?>"></li>
                    <?php } ?>
                  </ul>
                  <div class="carousel-inner">
                    <?php for ($j=0; $j < count($image) ; $j++) { ?>
                      <div style="background-color: white;" class="carousel-item <?= ($j==0) ? 'active' : ''?>">
                        <a href="../img/classified/classified_items_img/<?php echo $image[$j]; ?>" data-fancybox="images" data-caption="<?php echo $row['classified_add_title']; ?>">
                          <img style="height:150px;" onerror="this.src='img/classified.jpg'" class="d-block w-100" src="../img/classified/classified_items_img/<?php echo $image[$j]; ?>" alt="<?php echo $row['classified_add_title']; ?>">
                        </a>
                      </div>
                    <?php } ?>
                  </div>
                  <a class="carousel-control-prev" href="#carousel-<?=$i?>" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                  </a>
                  <a class="carousel-control-next" href="#carousel-<?=$i?>" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                  </a>
                </div>
              </div>
              <div class="card-body pt-5">
                <img onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $row['user_profile_pic'];?>" alt="profile-image" class="profile">

                <h5 class="card-title"><?php echo $row['classified_add_title']; ?> 
                <?php if ($row['product_type']==1) { ?>
                <span class="badge badge-secondary m-1">NEW</span>

                <?php } else { ?>
                <span class="badge badge-warning m-1">OLD</span>
                <?php } ?>
                </h5>
                <h6 class="text-primary"><?= $row['user_name'].' - '. $row['user_mobile'] ?></h6>
                <p class="card-text"> <?php echo $row['society_address'];?></p>
                <div class="icon-block">
                  <h5>Category : <small><?php echo $row['classified_category_name']; ?>-<?php echo $row['classified_sub_category_name']; ?></small></h5>
                  <h5>Price : <small><?php echo number_format($row['classified_expected_price'],2); ?></small></h5>
                  <hr>
                  <h5>Brand : <small><?php echo $row['classified_brand_name']; ?></small></h5>
                  <h5>Purchase Year : <small><?php echo $row['classified_manufacturing_year']; ?></small></h5>
                  <h6>Added On : <?php 
               
                            echo date('d M Y h:i A', strtotime($row['item_added_date']));  ?></h6>
                  <span data-target="#getClassifiedDetails" onclick="classifiedDetails(<?php echo $row['classified_master_id'] ?>)" data-toggle="modal" title="View Details" class="badge badge-info"><i class="fa fa-info-circle"></i> View More</span>
                </div>
              </div>
            </div>
          </div>
          <?php $i++; }  ?>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="bg-white p-3 float-right">
              <?php if($page!=1){ ?>
                <form class="btn-group" method="POST">
                  <input type="hidden" name="page" value="<?php echo $page-1 ?>">
                  <input type="hidden" name="filter_type" value="<?php echo $filter_type; ?>">
                  <button class="btn btn-light">Prev</button>
                </form>
              <?php } ?>
              <?php 
                $outOfRange = false;
                for($page1 = 1; $page1<= $number_of_page; $page1++) {
                    if ($page1 <= 2 || $page1 >= $number_of_page - 2 || abs($page1 - $page) <= 2) {
                        $outOfRange = false; ?>
                        <form class="btn-group" method="POST">
                          <input type="hidden" name="page" value="<?php echo $page1 ?>">
                          <input type="hidden" name="filter_type" value="<?php echo $filter_type; ?>">
                          <button class="btn <?php if($page==$page1){ echo 'btn-dark'; } else{ echo 'btn-light'; } ?>"><?php echo $page1 ?></button>
                        </form>
                    <?php } else {
                        if (!$outOfRange) {
                            echo ' ... ';
                        }
                        $outOfRange = true;
                    }
                } 
                
              ?>
              <?php if($number_of_page-$page!=0){ ?>
                <form class="btn-group" method="POST">
                  <input type="hidden" name="page" value="<?php echo $page+1 ?>">
                  <input type="hidden" name="filter_type" value="<?php echo $filter_type; ?>">
                  <button class="btn btn-light">Next</button>
                </form>
              <?php } ?>
            </div>
          </div>
        </div>
        
      <?php } else {
          echo "<img src='img/no_data_found.png'>";
      } ?>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  function classifiedDetails(argument) {
    $.ajax({
      url:'classifiedDetails.php',
      type:'POST',
      data:{classified_id:argument,getClassified:"getClassified"}
    })
    .done(function(response){
      $('#setClassifiedDetails').html(response);
    });
  }
</script>


<div class="modal fade" id="getClassifiedDetails">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Buy / Sell Item Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="setClassifiedDetails">
      </div>
    </div>
  </div>
</div>