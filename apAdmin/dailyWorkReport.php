<?php error_reporting(0);
  
  error_reporting(0);
  $currentYear = date('Y');
  $currentMonth = date('m');
  $nextYear = date('Y', strtotime('+1 year'));
  $onePreviousYear = date('Y', strtotime('-1 year'));
  $twoPreviousYear = date('Y', strtotime('-2 year'));
  $previous15DaysDate = date('Y-m-d', strtotime('-31 days'));
  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row ">
        <div class="col-sm-3 col-md-12 col-6">
          <h4 class="page-title">Daily Work Report (Without Template)</h4>
        </div>
     </div>
     <form class="branchDeptFilterWithUser" action="" >
      <div class="row ">
          <?php include 'selectBranchDeptEmpForFilterAll.php' ?>

          <div class="col-md-3 col-6 form-group">
            <input type="text" required="" readonly="" class="form-control" autocomplete="off" id="autoclose-datepickerFrom" name="from" value="<?php if (isset($_GET['from']) && $_GET['from'] != '') { echo $fromDate = $_GET['from']; } else { echo $fromDate= $previous15DaysDate; } ?>">
          </div>
          <div class="col-md-3 col-6 form-group">
            <input type="text" required="" readonly="" class="form-control" autocomplete="off" id="autoclose-datepickerTo" name="toDate" value="<?php if (isset($_GET['toDate']) && $_GET['toDate'] != '') { echo $toDate = $_GET['toDate']; } else { echo $toDate = date('Y-m-d'); } ?>">
          </div>
          <div class="col-md-1 mb-auto">
              <input class="btn btn-success btn-sm" type="submit" name="getReport"  value="Get Data">
          </div>    
      </div>
    
      </form>
     
      <div class="row mt-2">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                  <?php 
                        $i=1;
                        if (isset($bId) && $bId > 0) {
                            $branchFilterQuery = " AND work_report_master.block_id='$bId'";
                        }
                        if(isset($dId) && $dId>0) {
                          $deptFilterQuery = " AND work_report_master.floor_id='$dId'";
                        }
                        if (isset($fromDate) && isset($fromDate) && $fromDate != ''  && $toDate != '' ) {
                          $dateFilterQuery = " AND work_report_master.work_report_date BETWEEN '$fromDate' AND '$toDate'";
                        }
                        
                        if(isset($uId) && $uId>0) {
                          // $UserFilterQuery = "AND work_report_master.user_id='$uId'";
                        }

                        $q=$d->select("work_report_master,users_master,floors_master,block_master","block_master.block_id=floors_master.block_id AND work_report_master.user_id=users_master.user_id AND work_report_master.society_id='$society_id' AND  work_report_master.work_report_type=0 AND work_report_master.floor_id=floors_master.floor_id $branchFilterQuery $deptFilterQuery $dateFilterQuery $UserFilterQuery $blockAppendQueryUser","ORDER BY work_report_master.work_report_date DESC");
                        $counter = 1;
                       
                        ?>
                  <table  id="<?php if ($adminData['report_download_access'] == 0) {
                                            echo 'exampleReportWithoutBtn';
                                        } else {
                                            echo 'exampleReport';
                                        } ?>" class="table table-bordered" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>                        
                            <th>Name</th>
                            <th>Department</th>
                            <th>Date</th>                      
                            <th>Day</th>                      
                            <th>Report</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      while ($data=mysqli_fetch_array($q)) {
                      ?>
                        <tr>
                          <td><?php echo $counter++; ?></td>
                          <td><?php echo $data['user_full_name']; ?> (<?php echo $data['user_designation']; ?>)</td>
                          <td><?php echo $data['floor_name'].'-'.$data['block_name']; ?></td>
                          <td><?php if($data['work_report_date']!=="0000-00-00" && $data['work_report_date']!='null') { echo date("d M Y", strtotime($data['work_report_date'])); }?></td>
                          <td><?php if($data['work_report_date']!=="0000-00-00" && $data['work_report_date']!='null') { echo date("l", strtotime($data['work_report_date'])); }?></td>
                          <td><?php echo $data['work_report']; ?></td>
                          <td><button type="submit" class="btn btn-sm btn-primary" onclick="workReportModal(<?php echo $data['work_report_id']; ?>)"> <i class="fa fa-eye"></i></button></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                
            </div>
            </div>
          </div>
        </div>
     </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>

<div class="modal fade" id="detail_view">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Work Report Detail</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="work_report">

        </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/js/jquery.min.js"></script>


<script type="text/javascript">

<?php
 if(!isset($_GET['month_year']))
 { ?>
    $('#month_year').val(<?php echo $currentMonth;?>)
<?php }

?>

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
      }
      return false;
    }

</script>
<style>
.attendance_status
{  
  min-width: 113px;
}
</style>
