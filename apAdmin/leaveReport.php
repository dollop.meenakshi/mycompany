<?php 
error_reporting(0);
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employeeExpenseReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">Leave Report</h4>
        </div>
     
     </div>

    <form action="" method="get" class="fromToDateFilter">
     <div class="row pt-2 pb-2">
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">From Date </label>
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php echo $_GET['from']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">To Date </label>
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php echo $_GET['toDate']; ?>" readonly>  
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Employee </label>
            <select name="user_id"  class="form-control single-select">
                <option value="">All Employer</option> 
                <?php 
                    $user=$d->select("users_master","society_id='$society_id'");  
                    while ($userdata=mysqli_fetch_array($user)) {
                ?>
                <option <?php if($_GET['user_id']==$userdata['user_id']) { echo 'selected';} ?>  value="<?php echo $userdata['user_id']; ?>"><?php echo $userdata['user_full_name'];?></option> 
                <?php } ?>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label">Leave Status </label>
            <select name="leave_status"  class="form-control">
                <option value="">All</option> 
                <option <?php if($_GET['leave_status']=='0') { echo 'selected';} ?> value="0"  >Awaiting</option>
                <option <?php if($_GET['leave_status']=='1') { echo 'selected';} ?>  value="1" >Approved</option>
                <option <?php if($_GET['leave_status']=='2') { echo 'selected';} ?>  value="2" >Declined</option>
            </select>
          </div>
          <div class="col-lg-2 col-6">
            <label  class="form-control-label"> </label>
              <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
          

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                extract(array_map("test_input" , $_GET));
                // $from = date('Y-m-d', strtotime($from . ' -1 day'));
                // $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));
                 $date=date_create($from);
                $dateTo=date_create($toDate);
               /*  $nFrom= date_format($date,"Y-m-d 00:00:00");
                $nTo= date_format($dateTo,"Y-m-d 23:59:59"); */
                $nFrom= date_format($date,"Y-m-d");
                $nTo= date_format($dateTo,"Y-m-d");
                if(isset($_GET['leave_status']) && $_GET['leave_status'] != '') {
                    $statusFilterQuery = " AND leave_master.leave_status='$_GET[leave_status]'";
                }
                if(isset($_GET['user_id']) && $_GET['user_id'] > 0) {
                    $userFilterQuery = " AND leave_master.user_id='$_GET[user_id]'";
                }
                $q = $d->select("leave_master,leave_type_master,users_master", "users_master.user_id=leave_master.user_id AND leave_type_master.leave_type_id=leave_master.leave_type_id AND leave_master.society_id='$society_id' AND leave_master.leave_end_date BETWEEN '$nFrom' AND '$nTo' $blockAppendQueryUser $userFilterQuery $statusFilterQuery ", "ORDER BY leave_master.leave_id DESC");
                  $i=1;
                if (isset($_GET['from'])) {
               ?>
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Employee</th>
                        <th>Leave Type</th>
                        <th>Leave Day Type</th>
                        <th>Leave Date</th>
                        <th>Leave Status</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 


                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $data['user_full_name']; ?> (<?php echo $data['user_designation']; ?>)</td>
                        <td><?php echo $data['leave_type_name']; ?></td>
                        <td><?php 
                          if($data['leave_day_type'] == 0){
                              echo "Full Day";
                          }else{
                              if ($data['half_day_session'] == "1") {
                                 echo "Half Day <br>(First Half)";
                              } else if ($data['half_day_session'] == "2") {
                                echo "Half Day <br>(Second Half)";
                              } else {
                                echo "Half Day";
                              }
                          }
                        ?></td>
                        <td><?php echo date("d M Y", strtotime($data['leave_start_date'])); ?> (<?php echo date("D", strtotime($data['leave_start_date'])); ?>)</td>
                        <td><?php
                            if ($data['leave_status']==0) { 
                                echo "Awaiting";
                            } else if($data['leave_status']==1){
                                echo "Approved";
                            } else{
                                echo "Declined";
                            }
                        ?></td>
                       
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            <?php } else {  ?>
              <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->