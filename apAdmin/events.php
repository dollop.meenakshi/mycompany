<?php
extract($_REQUEST);
error_reporting(0);
$society_id = $_COOKIE['society_id'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-6 col-8">
        <h4 class="page-title"><?php if($_GET['t']=='completed') {echo "Completed";} else { echo "Upcoming";}?> Events</h4>
      </div>
      <div class="col-sm-6 col-12">
        <div class="float-sm-right">
          <a href="event" class="btn btn-primary btn-sm waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New</a>
          <?php if($t!='completed') { ?>
            <a href="events?t=completed" class="btn btn-warning btn-sm waves-effect waves-light"><i class="fa fa-calendar mr-1"></i> View Completed Events</a>
          <?php } else {?>
            <a href="events" class="btn btn-warning btn-sm waves-effect waves-light"><i class="fa fa-calendar mr-1"></i> View Upcoming Events</a>
          <?php }?>
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->
    <?php if($t!='completed') { ?>
      <div class="row">
        <?php 
          $noOfEvents =0;
          
          $today=date('Y-m-d');
          $blockidsForEvent = join("|",$blockAryAccess); 
          if($blockidsForEvent != ''){
            $appendQuery = " AND (block_id='' OR block_id=0 OR CONCAT(',', block_id, ',') REGEXP ',($blockidsForEvent)') ";
          }
          $q=$d->select("event_master","society_id='$society_id' $appendQuery","ORDER BY event_id DESC LIMIT 100");
          if(mysqli_num_rows($q)>0) {
            $i = 0;
            while($row=mysqli_fetch_array($q))
            {
              extract($row);
              $eData= date('Y-m-d',strtotime($row['event_end_date']));
              $i++;

              if ($today<=$eData) {
                $noOfEvents++;
              ?>
                <div class="col-lg-4">
                  <div class="card">
                    <?php if(file_exists("../img/event_image/$event_image")) { ?>
                    <img height="250"  src="../img/ajax-loader.gif" data-src="../img/event_image/<?php echo $event_image; ?>" class="card-img-top lazyload" alt="Event Image">
                    <?php } else { ?>
                      <img height="250"  src="../img/eventDefault.png"  class="card-img-top" alt="Event Image">
                    <?php } ?>
                    <div class="card-body">
                      <h5 class="card-title text-dark"><?php echo $event_title; ?>
                      <!-- <?php if($event_type=="0"){   ?> 
                        <span class="badge badge-pill badge-success m-1">Free</span>
                      <?php } else { ?>
                        <span class="badge badge-pill badge-danger m-1">Paid</span>
                      <?php }?> -->
                    </h5>
                    <address>
                      <b>Address</b> : <?php echo $eventMom;?>  
                    </address>
                    
                  </div>
                  <ul class="list-group list-group-flush list shadow-none">
                    <li class="list-group-item d-flex justify-content-between align-items-center"><b>Start Date</b> : <?php echo $event_start_date; ?> </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center"><b>End Date</b> : <?php echo $event_end_date; ?></li>
                   
                    </ul>
                    <div class="card-body text-center">
                      <form style="float: left;" class="mr-2" action="event" method="post">
                        <input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
                        <input type="hidden" name="editEvent" value="editEvent">
                        <button type="submit" name=""  value="editEvent" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></button>
                      </form>
                      <?php $totalBooking= $d->count_data_direct("event_attend_id","event_attend_list","event_id = '$event_id'"); 
                      if ($totalBooking==0) {
                      ?>
                      <form  style="float: left;" class="mr-2"  action="controller/eventController.php" method="post">
                        <input type="hidden" name="event_id_delete" value="<?php echo $event_id; ?>">
                        <input type="hidden" name="t" name="<?php echo $_GET['t'];?>">
                        <button type="submit"  class="btn form-btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></button>
                      </form>
                      <?php } else { ?>
                        <form  style="float: left;" class="mr-2"  action="#" method="post">
                        <button type="button"  onclick=" swal('Error! Someone Already Booked this Event!', { icon: 'error',});" class="btn  btn-sm btn-danger"><i class="fa fa-trash-o"></i></button>
                        </form>
                      <?php } ?>
                      <form  style="float: left;" class="mr-2"  action="viewEvents" method="get">
                        <input type="hidden" name="id" value="<?php echo $event_id; ?>">
                        <button type="submit"  value="View" class="btn btn-sm btn-warning"><i class="fa fa-ticket"></i> View Booking</button>
                      </form>

                      <!--  <form style="float: left;" action="eventBook" method="GET" enctype="multipart/form-data">
                <input type="hidden" name="event_id" value="<?php echo $event_id;?>">
                <button style="margin-right: 5px;" type="submit" class="btn   btn-success btn-sm shadow-primary" name="bookEvent">Book Now</button>
              </form> -->

                    </div>
                  </div>
                </div>
              <?php } 
            } 

          } else {
            echo "<img src='img/no_data_found.png'>";
          }
          if($noOfEvents==0 && mysqli_num_rows($q)>0){
            echo "<img src='img/no_data_found.png'>";
          }?>
      </div>
    <?php } else { $noOfComplitedEvents = 0 ; ?>

      <div class="row">
        <?php
          $today=date('Y-m-d');
          $q=$d->select("event_master","society_id='$society_id'","ORDER BY event_id DESC LIMIT 100");
          if(mysqli_num_rows($q)>0) {
            $i = 0;
            while($row=mysqli_fetch_array($q))
            {
              extract($row);
              $eData= date('Y-m-d',strtotime($row['event_end_date']));
              $i++;
              if ($today>$eData) { 
                $noOfComplitedEvents++ ; 
        ?>
          <div class="col-lg-4">
            <div class="card">
              <?php if(file_exists("../img/event_image/$event_image")) { ?>
              <img height="250"  src="../img/ajax-loader.gif" data-src="../img/event_image/<?php echo $event_image; ?>" class="card-img-top lazyload" alt="Event Image">
              <?php } else { ?>
                <img height="250"  src="../img/eventDefault.png"  class="card-img-top" alt="Event Image">
              <?php } ?>
              <div class="card-body ">
                <h5 class="card-title text-dark"><?php echo $event_title; ?>
                  <!-- <?php if($event_type=="0"){   ?> 
                    <span class="badge badge-pill badge-success m-1">Free</span>
                  <?php } else { ?>
                    <span class="badge badge-pill badge-danger m-1">Paid</span>
                  <?php }?>
                  <br> -->
                </h5>
                <address>
                  <b>Address</b> : <?php echo $eventMom;?>  
                </address>
              </div>
              <ul class="list-group list-group-flush list shadow-none">
                <li class="list-group-item d-flex justify-content-between align-items-center"><b>Start Date</b> : <?php echo $event_start_date; ?> </li>
                <li class="list-group-item d-flex justify-content-between align-items-center"><b>End Date</b> : <?php echo $event_end_date; ?></li>
                <li class="list-group-item d-flex justify-content-between align-items-center"><b>Status</b> : <span class="badge badge-pill badge-warning m-1">Completed</span>
                </li>
              </ul>
              <div class="card-body text-center">
                <!-- <form style="float: left;" class="mr-2" action="event" method="post">
                  <input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
                  <button type="submit" name="editEvent"  value="editEvent" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></button>
                </form>
                <form  style="float: left;" class="mr-2"  action="controller/eventController.php" method="post">
                  <input type="hidden" name="event_id_delete" value="<?php echo $event_id; ?>">
                  <input type="hidden" name="t" value="<?php echo $_GET['t'];?>">
                  <button type="submit" name="deleteEvent"  value="deleteEvent" class="btn form-btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></button>
                </form> -->
                 <form  style="float: left;" class="mr-2"  action="viewEvents" method="get">
                    <input type="hidden" name="id" value="<?php echo $event_id; ?>">
                    <button type="submit"  value="View" class="btn btn-sm btn-warning"><i class="fa fa-ticket"></i> View Booking</button>
                  </form>

              </div>
            </div>
          </div>
        <?php } } } else {
          echo "<img src='img/no_data_found.png'>";
        }
        if($noOfComplitedEvents==0 && mysqli_num_rows($q)>0 ){
          echo "<img src='img/no_data_found.png'>";
        } ?>
      </div>
    <?php }?>
  </div>
</div>