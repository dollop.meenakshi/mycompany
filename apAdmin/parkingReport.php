<?php //IS_3243 whole file 
error_reporting(0); ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-7">
        <h4 class="page-title">Parking Report</h4>
        </div>
        <div class="col-sm-3 col-5 text-right">
           
        </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <?php
                 $q3=$d->select("society_parking_master,parking_master ","parking_master.society_parking_id=society_parking_master.society_parking_id ","ORDER BY parking_master.parking_id ASC");
                  $i=1;
               ?>
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Parking</th>
                        <th>Type</th>
                        <th><?php echo $xml->string->block; ?></th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Vehicle</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                  <?php   while ($data=mysqli_fetch_array($q3)) {
                    $q4=$d->select("unit_master,block_master,users_master,floors_master","users_master.delete_status=0 AND block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND users_master.user_type = '0' and unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND  users_master.member_status=0  AND unit_master.unit_id='$data[unit_id]' AND users_master.user_status!=0  $blockAppendQuery","ORDER BY unit_master.unit_id ASC");
                    $userData=mysqli_fetch_array($q4);
                   ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td> <?php echo $data['socieaty_parking_name'].'-'.$data['parking_name']; ?></td>
                        <td> <?php  if($data['parking_type']==0) { echo 'Car'; } else { echo 'Bike';} ?></td>
                        <td><?php if($userData['block_name']!='') { echo $userData['block_name']; } ?></td>
                        <td><?php echo $userData['user_full_name'];  ?></td>
                        <td><?php if(!empty($userData['user_mobile']) && $userData['user_mobile'] != ""){ echo $userData['country_code'] . " " . substr($userData['user_mobile'], 0, 2) . '*****' . substr($userData['user_mobile'], -3); } ?></td>
                        <td> <?php echo $data['vehicle_no']; ?></td>
                        <td> <?php if ($data['parking_status']==2) {
                          echo "Pending";
                        } else if ($data['parking_status']==0) {
                          echo "Not Allocated";
                        }else {
                          echo "Allocated";
                        }; ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->