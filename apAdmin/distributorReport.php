<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title">Distributor Report</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $q = $d->select("distributor_master");
                            $i = 1;
                            ?>
                            <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Distributor</th>
                                        <th>Order Email</th>
                                        <th>Email</th>
                                        <th>Contact Person</th>
                                        <th>Contact Person Number</th>
                                        <th>Address</th>
                                        <th>Pincode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($data = $q->fetch_assoc())
                                    {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $data['distributor_name']; ?></td>
                                        <td><?php echo $data['distributor_email']; ?></td>
                                        <td><?php echo $data['distributor_email_main']; ?></td>
                                        <td><?php echo $data['distributor_contact_person']; ?></td>
                                        <td><?php echo $data['distributor_contact_person_country_code']. " " .$data['distributor_contact_person_number']; ?></td>
                                        <td><?php echo chunk_split($data['distributor_address'],50,"<br>"); ?></td>
                                        <td><?php echo $data['distributor_pincode']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div>