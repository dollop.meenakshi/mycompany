<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-10">
        <h4 class="page-title">Requests</h4>
        <ol class="breadcrumb">
          <li>
            <a href ="requests"><span class="badge badge-pill badge-primary m-1">All (<?php echo $d->count_data_direct("request_id","request_master,users_master","users_master.user_id=request_master.request_user_id AND request_master.society_id='$society_id'   $blockAppendQueryUser  "); ?>)</span></a>
            <a href ="requests?type=1"><span class="badge badge-pill badge-success m-1">Close (<?php echo $d->count_data_direct("request_id","request_master,users_master","users_master.user_id=request_master.request_user_id AND request_master.society_id='$society_id' AND request_master.request_status=1  $blockAppendQueryUser "); ?>)</span></a>
           
            <a href ="requests?type=0"><span class="badge badge-pill badge-warning m-1">Open (<?php echo $d->count_data_direct("request_id","request_master,users_master","users_master.user_id=request_master.request_user_id AND request_master.society_id='$society_id' AND request_status=0 $blockAppendQueryUser OR users_master.user_id=request_master.request_user_id AND request_master.society_id='$society_id' AND request_master.request_status=3  $blockAppendQueryUser "); ?>)</span></a>
            <a href ="requests?type=2"><span class="badge badge-pill badge-primary m-1">In Progress (<?php echo $d->count_data_direct("request_id","request_master,users_master","users_master.user_id=request_master.request_user_id AND request_master.society_id='$society_id' AND request_master.request_status=2  $blockAppendQueryUser "); ?>)</span></a>
          </li>  
        </ol>
      </div>
      <div class="col-sm-3 col-2 text-right">
        <div class="btn-group">
          <a href="javascript:void(0)" onclick="DeleteAll('deleteRequest');" class="btn  btn-sm btn-danger float-right"><i class="fa fa-trash-o fa-lg"></i>  </a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                     <th>#</th>
                      <th>Status</th>
                      <th>No.</th>
                      <th><?php echo $xml->string->unit; ?></th>
                      <th>Title</th>
                      <th>Date</th>
                  </tr>
                </thead>

                <tbody>
                  <?php 
                    $sr=1;
                    if (isset($_GET['type'])) {
                      if ($_GET['type']==0) {
                        $q=$d->select("request_master,users_master","users_master.user_id=request_master.request_user_id AND  request_master.society_id='$society_id' AND  request_master.request_status=0 $blockAppendQueryUser OR users_master.user_id=request_master.request_user_id AND request_master.society_id='$society_id' AND  request_master.request_status=3 $blockAppendQueryUser ","ORDER BY request_master.request_id DESC");
                      }elseif ($_GET['type']==1) { 
                        $q=$d->select("request_master,users_master","users_master.user_id=request_master.request_user_id AND  request_master.society_id='$society_id'  AND request_master.request_status=1 $blockAppendQueryUser","ORDER BY request_master.request_id DESC");
                      } elseif ($_GET['type']==2) {
                        $q=$d->select("request_master,users_master","users_master.user_id=request_master.request_user_id AND  request_master.society_id='$society_id'  AND request_master.request_status=2  $blockAppendQueryUser","ORDER BY request_master.request_id DESC");
                      } 
                    }  
                    else {
                      $q=$d->select("request_master,users_master","users_master.user_id=request_master.request_user_id AND request_master.society_id='$society_id' $blockAppendQueryUser ","ORDER BY request_master.request_id DESC");
                    }
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td class="text-center">
                        <?php if ($data['request_status']==1): ?>
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['request_id']; ?>">
                        <?php endif ?>
                        </td>
                        <td><?php
                         if ($data['request_status']==0) {  ?>
                          <span   data-toggle="modal" data-target="#reqModal" onclick="requestUpdate('<?php echo $data['request_id']; ?>');" class="badge badge-warning m-1 pointerCursor">OPEN <i class="fa fa-pencil"></i></span>
                        <?php } elseif ($data['request_status']==1) { ?>
                          <span   data-toggle="modal" data-target="#reqModal" onclick="requestUpdate('<?php echo $data['request_id']; ?>');" class="badge badge-success  m-1 pointerCursor">CLOSE</span>
                        <?php } elseif ($data['request_status']==2) { ?>
                          <span  data-toggle="modal" data-target="#reqModal" onclick="requestUpdate('<?php echo $data['request_id']; ?>');" class="badge badge-primary  m-1 pointerCursor">In Progress <i class="fa fa-pencil"></i></span>
                        <?php }elseif ($data['request_status']==3) { ?>
                          <span   data-toggle="modal" data-target="#reqModal" onclick="requestUpdate('<?php echo $data['request_id']; ?>');" class="badge badge-warning m-1 pointerCursor">OPEN <i class="fa fa-pencil"></i></span>
                        <?php } ?></td>
                         <td>
                          <form action="requestHistory" method="POST">
                            <input type="hidden" name="request_id" value="<?php echo $data['request_id']; ?>">
                            <button class="btn btn-link p-0"><?php echo 'REQ'.$data['request_id']; ?> <i class="fa fa-history"></i></button>
                          </form>
                        </td>
                        <td><?php 
                          $userDa=$d->selectArray("users_master","user_id='$data[request_user_id]'");
                         
                          $blockDa=$d->selectArray("block_master","block_id='$userDa[block_id]'");
                         
                          echo $userDa['user_full_name']." - ".$userDa['user_designation']." (".$blockDa['block_name'].")";
                         ?></td>
                        <td class="tableWidth"><?php echo $data['request_title']; ?></td>
                        
                        <td><?php echo  date("d M Y h:i A", strtotime($data['request_date'])); ?></td>
                    </tr>
                    <?php
                  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  
<script type="text/javascript">
  function requestUpdate(request_id) {
      $.ajax({
          url: "getRequestDetails.php",
          cache: false,
          type: "POST",
          data: {request_id : request_id},
          success: function(response){
              $('#reqData').html(response);
            
              
          }
       });
  } 

   function request_details(request_id) {
      $.ajax({
          url: "getRequestDetails.php",
          cache: false,
          type: "POST",
          data: {request_id : request_id, onlyInfo:"yes"},
          success: function(response){
              $('#reqDataInfo').html(response);
            
              
          }
       });
  } 

</script>

<div class="modal fade" id="viewReqInfo">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Request Info </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="reqDataInfo">
          
      </div>
     
    </div>
  </div>
</div>


<div class="modal fade" id="reqModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Request</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="reqData">
          
      </div>
     
    </div>
  </div>
</div>