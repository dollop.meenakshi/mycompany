
<style>
  input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	  -webkit-appearance: none;
	  margin: 0;
	}
</style>
<?php
$q1 = $d->selectRow('*', "society_master", "society_id='$society_id'");
$data2 = mysqli_fetch_assoc($q1);

$user_latitude = $data2['society_latitude'];
$user_longitude = $data2['society_longitude'];
extract(array_map("test_input", $_POST));
if (isset($edit_stationary_vendor)) {
    $q = $d->select("vendor_master","vendor_id='$vendor_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Stationery Vendor</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addStationaryVendorFrom" action="controller/StationaryVendorController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Category <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="hidden" class="form-control" placeholder="Vendor Name" name="vendor_category_id" value="2">
                                    <input type="text" class="form-control" value="Stationary" readonly>                 
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Vendor Name <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Vendor Name" name="vendor_name" value="<?php if($data['vendor_name'] !=""){ echo $data['vendor_name']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Vendor Email </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="email" class="form-control" placeholder="Vendor Email" name="vendor_email" value="<?php if($data['vendor_email'] !=""){ echo $data['vendor_email']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Mobile Country Code <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  name="vendor_mobile_country_code" class="form-control single-select" id="vendor_mobile_country_code" required="">  
                                        <?php include 'country_code_option_list.php'; ?>
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Vendor Mobile <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control  onlyNumber numberEClass" placeholder="Vendor Mobile" name="vendor_mobile" value="<?php if($data['vendor_mobile'] !=""){ echo $data['vendor_mobile']; } ?>">
                                </div>
                            </div> 
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Vendor Address</label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Vendor Address" id="vendor_address" name="vendor_address" value="<?php if($data['vendor_address'] !=""){ echo $data['vendor_address']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Vendor Latitude </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Vendor Latitude" id="vendor_latitude" name="vendor_latitude" value="<?php if($data['vendor_latitude'] !=""){ echo $data['vendor_latitude']; }else{ echo $user_latitude; } ?>" readonly>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Vendor Longitude </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Vendor Longitude" id="vendor_longitude" name="vendor_longitude" value="<?php if($data['vendor_longitude'] !=""){ echo $data['vendor_longitude']; }else{ echo $user_longitude; } ?>" readonly>
                                </div>
                            </div> 
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Vendor Logo</label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="file" class="form-control" name="vendor_logo" onchange="readURL(this);" value="">
                                    <input type="hidden" name="vendor_logo_old" id="vendor_logo_old" value="<?php if($data['vendor_logo'] !=""){ echo $data['vendor_logo']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row w-100 mx-0">
                                <input id="searchInput5" name="location_name" class="form-control" type="text" placeholder="Enter location" >
                                <div class="map" id="map" style="width: 100%; height: 300px;"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4" id="blah">
                            <?php if (isset($data['vendor_logo']) && $data['vendor_logo'] != null && $data['vendor_logo'] != '') { ?>
                                <img src="../img/vendor_logo/<?php echo $data['vendor_logo']; ?>" width="75%" height="50%">
                            <?php } ?>
                            </div>
                        </div>
                    </div>    
                                
                    <div class="form-footer text-center">
                    <?php //IS_1019 addPenaltiesBtn 
                    if (isset($edit_stationary_vendor)) {                    
                    ?>
                    <input type="hidden" id="vendor_id" name="vendor_id" value="<?php if($data['vendor_id'] !=""){ echo $data['vendor_id']; } ?>" >
                    <button id="addStationaryVendorBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addStationaryVendor"  value="addStationaryVendor">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addStationaryVendorFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                        <?php }
                        else {
                        ?>
                    
                    <button id="addStationaryVendorBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addStationaryVendor"  value="addStationaryVendor">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addStationaryVendorFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>
<script type="text/javascript">

$().ready(function()
  {
    var latitude = '<?php echo $user_latitude; ?>';
    var longitude = '<?php echo $user_longitude; ?>';

    initialize(latitude,longitude);
  });

  function initialize(d_lat,d_long)
  {
    var latlng = new google.maps.LatLng(d_lat,d_long);
    var latitute = 23.037786;
    var longitute = 72.512043;

    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });
    var parkingRadition = 5;
    var citymap = {
        newyork: {
          center: {lat: latitute, lng: longitute},
          population: parkingRadition
        }
    };

    var input = document.getElementById('searchInput5');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    autocomplete10.addListener('place_changed', function()
    {
      infowindow.close();
      marker.setVisible(false);
      var place5 = autocomplete10.getPlace();
      if (!place5.geometry) {
          window.alert("Autocomplete's returned place5 contains no geometry");
          return;
      }

      // If the place5 has a geometry, then present it on a map.
      if (place5.geometry.viewport) {
          map.fitBounds(place5.geometry.viewport);
      } else {
          map.setCenter(place5.geometry.location);
          map.setZoom(17);
      }

      marker.setPosition(place5.geometry.location);
      marker.setVisible(true);

      var pincode="";
      for (var i = 0; i < place5.address_components.length; i++) {
        for (var j = 0; j < place5.address_components[i].types.length; j++) {
          if (place5.address_components[i].types[j] == "postal_code") {
            pincode = place5.address_components[i].long_name;
          }
        }
      }
      bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
      infowindow.setContent(place5.formatted_address);
      infowindow.open(map, marker);

    });
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function()
    {
      geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
      {
        if (status == google.maps.GeocoderStatus.OK)
        {
          if (results[0])
          {
            var places = results[0];
            var pincode="";
            var serviceable_area_locality= places.address_components[4].long_name;
            for (var i = 0; i < places.address_components.length; i++)
            {
              for (var j = 0; j < places.address_components[i].types.length; j++)
              {
                if (places.address_components[i].types[j] == "postal_code")
                {
                  pincode = places.address_components[i].long_name;
                }
              }
            }
            bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
          }
        }
      });
    });
  }

  function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality)
  {
    document.getElementById('vendor_address').value = address+' '+pin_code+' '+serviceable_area_locality ;
    document.getElementById('vendor_latitude').value = lat;
    document.getElementById('vendor_longitude').value = lng;
  }



function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //$('#blah').attr('src', e.target.result);
            $('#blah').html('<img src="'+e.target.result+'" width="75%" height="50%">');
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>