<?php
extract(array_map("test_input" , $_POST));
$searchCategory = $_REQUEST['searchCategory'];
?>

<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title">Vendor Category</h4>
       
     </div>
     <div class="col-sm-3">
        <div class="btn-group float-sm-right">
          <a href="#" data-toggle="modal" data-target="#addCategory" class="btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Category</a>
          <!-- </button> -->
        </div>
      </div>
     </div>
      <form action="" id="searchVendorCategoryFilter">
        <div class="row pt-2 pb-2">
          <div class="col-md-3 form-group">
            <input type="text" class="form-control" autocomplete="off" name="searchCategory" placeholder="Search Vendor Category" value="<?php echo $searchCategory; ?>">
          </div>
          <div class="col-md-3 form-group">
            <button type="submit" style="padding: 10px 12px;margin-left: 5px;" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </form>
    <!-- End Breadcrumb-->
     <div class="row">

        <?php 
        if(isset($searchCategory) && $searchCategory !=''){
          $searchQuery = "service_provider_category_name LIKE '%$searchCategory%'";
        }
        $q=$d->select("local_service_provider_master","$searchQuery","ORDER BY service_provider_category_name ASC");
          while($row=mysqli_fetch_array($q)){ ?> 
          <div class="col-lg-3">
           <div class="card">
            <a href="vendorSubcategory?searchcatid=<?php echo $row['local_service_provider_id']; ?>">
              <img height="150" class="card-img-top" src="../img/local_service_provider/local_service_cat/<?php echo $row['service_provider_category_image']; ?>" alt="<?php echo $row['service_provider_category_name']; ?>"></a>
              <div class="p-2 text-center">
                 <h5 class="card-title text-primary"><?php custom_echo($row['service_provider_category_name'],29); ?></h5>
                 <hr>
                 <a data-toggle="modal" data-target="#editCategory" href="javascript:void();" onclick="editCategory('<?php echo $row['local_service_provider_id']; ?>','<?php echo $row['service_provider_category_name']; ?>','<?php echo $row['service_provider_category_image']; ?>');" class="btn btn-sm btn-primary shadow-primary">Edit</a>
                 <?php
                  $totalAsing = $d->count_data_direct("users_category_id","local_service_provider_users_category","category_id='$row[local_service_provider_id]'");
                  if($totalAsing<=0){
                 ?>
                 <a href="javascript:void();" onclick="deleteSpCategory('<?php echo $row['local_service_provider_id']; ?>');" class="btn btn-sm btn-danger shadow-primary">Delete</a>
               <?php } ?>
              </div>
           </div>
        </div>
        <?php } ?>
      </div>

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
  
<div class="modal fade" id="addCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="controller/serviceProviderController.php" id="addVndCateogry" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Category Name <span class="required">*</span></label>
                    <div class="col-sm-8">
                      <input required="" type="text" name="service_provider_category_name"  class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Image <span class="required">*</span></label>
                    <div class="col-sm-8">
                      <input required="" type="file" name="service_provider_category_image"  class="form-control-file border">
                    </div>
                </div>
         
                <div class="form-footer text-center">
                  <button type="submit" name="addCategory" value="importParking" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->


<div class="modal fade" id="editCategory">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="controller/serviceProviderController.php" method="post" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Category Name *</label>
                    <div class="col-sm-8">
                      <input type="hidden" name="local_service_provider_id" id="local_service_provider_id">
                      <input required="" id="service_provider_category_name" type="text" name="service_provider_category_name"  class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Image </label>
                    <div class="col-sm-8">
                      <input type="hidden" name="service_provider_category_image_old" id="service_provider_category_image">
                      <input  type="file" name="service_provider_category_image"  class="form-control-file border">
                    </div>
                </div>
         
                <div class="form-footer text-center">
                  <button type="submit" name="editCategory" value="editCategory" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                </div>

          </form> 
      </div>
     
    </div>
  </div>
</div><!--End Modal -->