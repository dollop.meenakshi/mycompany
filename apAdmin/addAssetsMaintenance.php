<?php
extract(array_map("test_input", $_POST));
if (isset($edit_assets_maintenance)) {
    $q = $d->select("assets_maintenance_master","assets_maintenance_id='$assets_maintenance_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12">
                <h4 class="page-title"> Add Assets Maintenance</h4>
            </div>
        </div>
        <!-- End Breadcrumb-->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <?php
                        $week_days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                        ?>

                        <form id="addAssetsMaintenanceForm" action="controller/assetsItemDetailsController.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Assets Category <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select required="" class="form-control single-select" name="assets_category_id" id="assets_category_id" onchange="getAssetsItemByCategoryId(this.value);" <?php if (isset($edit_assets_maintenance)) { echo 'disabled'; }?>> 
                                                <option value="">-- Select Assets Category --</option>
                                                <?php
                                                $acq = $d->select("assets_category_master", "society_id='$society_id'");
                                                while ($AssetsCatData = mysqli_fetch_array($acq)) {
                                                ?>
                                                    <option <?php if($assets_category_id == $AssetsCatData['assets_category_id']){echo 'selected';} ?> value="<?php echo $AssetsCatData['assets_category_id'];?>"><?php echo $AssetsCatData['assets_category'];?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Assets Item <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select required="" class="form-control single-select" name="assets_id" id="assets_id" <?php if (isset($edit_assets_maintenance)) { echo 'disabled'; }?>>
                                                <option value="">-- Select Assets Item --</option>
                                                <?php
                                                $aiq = $d->select("assets_item_detail_master", "society_id='$society_id' AND assets_category_id='$assets_category_id'");
                                                while ($AssetsItemData = mysqli_fetch_array($aiq)) {
                                                ?>
                                                    <option <?php if($assets_id == $AssetsItemData['assets_id']){echo 'selected';} ?> value="<?php echo $AssetsItemData['assets_id'];?>"><?php echo $AssetsItemData['assets_name'];?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Assets Maintenance Type <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select required="" class="form-control single-select" name="maintenance_type" id="maintenance_type" onchange="maintenanceType(this.value)" <?php if (isset($edit_assets_maintenance)) { echo 'disabled'; }?>>
                                                <option value="">-- Select Maintenance Type --</option>
                                                <option <?php if(isset($data['maintenance_type']) && $data['maintenance_type'] == 0){echo 'selected';} ?> value="0">Custom Date</option>
                                                <option <?php if(isset($data['maintenance_type']) && $data['maintenance_type'] == 1){echo 'selected';} ?> value="1">Weekly</option>
                                                <option <?php if(isset($data['maintenance_type']) && $data['maintenance_type'] == 2){echo 'selected';} ?> value="2">Month Days</option>
                                                <option <?php if(isset($data['maintenance_type']) && $data['maintenance_type'] == 3){echo 'selected';} ?> value="3">Monthly</option>
                                                <option <?php if(isset($data['maintenance_type']) && $data['maintenance_type'] == 4){echo 'selected';} ?> value="4">Quarterly</option>
                                                <option <?php if(isset($data['maintenance_type']) && $data['maintenance_type'] == 5){echo 'selected';} ?> value="5">Half Yearly</option>
                                                <option <?php if(isset($data['maintenance_type']) && $data['maintenance_type'] == 6){echo 'selected';} ?> value="6">Yearly</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 common-hide <?php if($maintenance_type != '0'){echo 'd-none';}else{} ?> custom-date">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Custom Date <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" class="form-control autoclose-datepicker-item-maintenance" readonly placeholder="Custom Date" name="custom_date" id="custom_date" required="" value="<?= $custom_date ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 common-hide <?php if($maintenance_type != '1'){echo 'd-none';}else{} ?> weekly-days">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Week Days <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select id="week_days" type="text" required="" class="form-control multiple-select" multiple name="week_days[]">
                                                <?php
                                                $week_day = explode(',', $data['days']);
                                                for ($i = 0; $i < count($week_days); $i++) {
                                                ?>
                                                    <option <?php if (isset($data['days']) && in_array($i, $week_day)) {
                                                                echo "selected";
                                                            } ?> value="<?php echo $i; ?>"><?php echo $week_days[$i]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 common-hide <?php if($maintenance_type != '2'){echo 'd-none';}else{} ?> month-days">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Month Days <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select id="month_days" type="text" required="" class="form-control multiple-select" multiple name="month_days[]">
                                                <?php
                                                $month_days = explode(',', $data['days']);
                                                for ($i = 1; $i <= 31; $i++) {
                                                ?>
                                                    <option <?php if (isset($data['days']) && in_array($i, $month_days)) {
                                                                echo "selected";
                                                            }?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 common-hide start-month <?php if($maintenance_type == '4'|| $maintenance_type == '5' || $maintenance_type == '6'){}else{echo 'd-none';} ?>">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Start Month <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select id="start_month" type="text" required="" class="form-control single-select" name="start_month">
                                                <option value="">-- Select Start Month --</option>
                                                <option <?php if($data['start_month']=='01'){echo 'selected';} ?> value="01">January</option>
                                                <option <?php if($data['start_month']=='02'){echo 'selected';} ?> value="02">February</option>
                                                <option <?php if($data['start_month']=='03'){echo 'selected';} ?> value="03">March</option>
                                                <option <?php if($data['start_month']=='04'){echo 'selected';} ?> value="04">April</option>
                                                <option <?php if($data['start_month']=='05'){echo 'selected';} ?> value="05">May</option>
                                                <option <?php if($data['start_month']=='06'){echo 'selected';} ?> value="06">June</option>
                                                <option <?php if($data['start_month']=='07'){echo 'selected';} ?> value="07">July</option>
                                                <option <?php if($data['start_month']=='08'){echo 'selected';} ?> value="08">August</option>
                                                <option <?php if($data['start_month']=='09'){echo 'selected';} ?> value="09">September</option>
                                                <option <?php if($data['start_month']=='10'){echo 'selected';} ?> value="10">October</option>
                                                <option <?php if($data['start_month']=='11'){echo 'selected';} ?> value="11">November</option>
                                                <option <?php if($data['start_month']=='12'){echo 'selected';} ?> value="12">December</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 common-hide <?php if($maintenance_type == '3' || $maintenance_type == '4'|| $maintenance_type == '5' || $maintenance_type == '6'){}else{echo 'd-none';} ?> month-date">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Month Date <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <select id="month_date" type="text" required="" class="form-control single-select" name="month_date">
                                                <option value="">-- Select Month Date --</option>
                                                <?php
                                                for ($i = 1; $i <= 31; $i++) {
                                                ?>
                                                    <option <?php if($data['days']==$i){echo 'selected';} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Vendor Name <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" class="form-control" placeholder="Vendor Name" name="vendor_name" id="vendor_name" required="" value="<?php echo $vendor_name; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row w-100 mx-0">
                                        <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Vendor Mobile No <span class="required">*</span></label>
                                        <div class="col-lg-12 col-md-12 col-12" id="">
                                            <input type="text" class="form-control" placeholder="Vendor Mobile No" name="vendor_mobile_no" id="vendor_mobile_no" required="" value="<?php echo $vendor_mobile_no; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-footer text-center">
                                <?php 
                                if (isset($edit_assets_maintenance)) {
                                ?>
                                    <input type="hidden" id="assets_maintenance_id" name="assets_maintenance_id" value="<?php if ($data['assets_maintenance_id'] != "") {echo $data['assets_maintenance_id'];} ?>">
                                    <input type="hidden" id="maintenance_type" name="maintenance_type" value="<?php if ($data['maintenance_type'] != "") {echo $data['maintenance_type'];} ?>">
                                    <button id="editAssetsMaintenanceBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                                    <input type="hidden" name="editAssetsMaintenance" value="editAssetsMaintenance">
                                <?php } else {
                                ?>
                                    <button id="addAssetsMaintenanceBtn" type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                    <input type="hidden" name="addAssetsMaintenance" value="addAssetsMaintenance">

                                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" 
                                    ?>
                                    <button type="reset" value="add" class="btn btn-danger cancel" onclick="resetFrm('addShiftTiming');"><i class="fa fa-check-square-o"></i> Reset</button>
                                <?php } ?>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!--End Row-->

    </div>

</div>
<!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    function maintenanceType(value){
        if(value == 0){
            $('.common-hide').addClass('d-none');
            $('.custom-date').removeClass('d-none');
        }else if(value == 1){
            $('.common-hide').addClass('d-none');
            $('.weekly-days').removeClass('d-none');
        }else if(value == 2){
            $('.common-hide').addClass('d-none');
            $('.month-days').removeClass('d-none');
        }else if(value == 3){
            $('.common-hide').addClass('d-none');
            $('.month-date').removeClass('d-none');
        }else if(value == 4){
            $('.common-hide').addClass('d-none');
            $('.start-month').removeClass('d-none');
            $('.month-date').removeClass('d-none');
        }else if(value == 5){
            $('.common-hide').addClass('d-none');
            $('.start-month').removeClass('d-none');
            $('.month-date').removeClass('d-none');
        }else if(value == 6){
            $('.common-hide').addClass('d-none');
            $('.start-month').removeClass('d-none');
            $('.month-date').removeClass('d-none');
        }else{
            $('.common-hide').addClass('d-none');
        }
    }
</script>
