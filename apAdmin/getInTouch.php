<?php error_reporting(0);
  ?>
  
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Get In Touch</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <a href="javascript:void(0)" onclick="DeleteAll('deleteGetInTouch');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
     </div>
     </div>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>    
                        <th>User Name</th>                      
                        <th>Mobile No</th>                      
                        <th>Email</th>                                               
                        <th>Message</th>
                        <th>Date</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                      $i=1;
                      $q=$d->select("company_get_in_touch_master","society_id='$society_id'");
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td class="text-center">
                         
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['company_get_in_touch_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['company_get_in_touch_id']; ?>">                      
                          
                        </td>
                        <td><?php echo $counter++; ?></td>
                        <td><?php echo $data['company_get_in_touch_user_name']; ?></td>
                        <td><?php echo $data['company_get_in_touch_user_mobile_number']; ?></td>
                        <td><?php echo $data['company_get_in_touch_user_email_id']; ?></td>
                        <td title='<?php echo $data['company_get_in_touch_user_message']; ?>'><?php custom_echo($data['company_get_in_touch_user_message'], 30); ?></td>
                        <td><?php echo (date("d M Y h:i A", strtotime($data['company_get_in_touch_created_at']))); ?></td>
                       <td><button type="submit" class="btn btn-sm btn-primary mr-2" onclick="getInTouch(<?php echo $data['company_get_in_touch_id']; ?>)"> <i class="fa fa-eye"></i></button></td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
  </div>

  <div class="modal fade" id="getInTouchModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Get In Touch Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="align-content: center;">
        <div class="card-body">

          <div class="row col-md-12"  id="showGetInTouch">
          </div>

        </div>
      </div>

    </div>
  </div>
</div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
