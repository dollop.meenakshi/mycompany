
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-9">
        <h4 class="page-title">Assets Category Management</h4>
      </div>
      <div class="col-sm-3">
        <button type="button" class="btn btn-sm ml-1 btn-primary float-right" data-toggle="modal" data-target="#assetsModal">
          +ADD
        </button>    
      </div>  
    </div>   

    <div class="row mt-3">   
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Category</th>
                    <th>Total Items</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                  $q=$d->select("assets_category_master","","");
                
                  while ($row=mysqli_fetch_array($q)) {

                    $assets_category_id=$row['assets_category_id'];

                    ?>
                    <tr>
           
                      <td><?php echo $i++;?></td>
                      <td><?php echo $row['assets_category'];?></td>
                      <td>  
                       <?php 

                       echo $totalAssetsitems= $d->count_data_direct("assets_category_id","assets_item_detail_master","assets_category_id='$assets_category_id'"); ?>
                    </td>

                      <td>

                    <button type="button" class="btn btn-sm btn-primary ml-2 mr-2 editAssetscategory" data-toggle="modal" data-target="#editAssetscategory" onclick="editAssetscategory(<?php echo $row['assets_category_id'] ?>)">
                        <i class="fa fa-edit"></i>
                      </button>
                       <div style="display: inline-block;">
                          <form method="POST" class="form-horizontal" action="controller/assetsCategoryController.php">
                          
                            <input type="hidden" name="assets_category_id" id="assets_category_id" value="<?php echo $row['assets_category_id'] ?>" />
                            <input type="hidden" name="delete_assets" id="delete_assets" value="<?php $row['assets_category_id'] ?>" />

                            <?php if ($totalAssetsitems < 1 )   {  ?>
                            <button class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash-o"></i></button>
                          
                          <?php }?>
                          </form>
                        </div>

                      </td>

                       </tr>
                  <?php } ?>

                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
 

  </div>
</div>

<!-- -------- -->

  <div class="modal fade" id="assetsModal" tabindex="-1" role="dialog" aria-labelledby="assetsCategoryModal" aria-hidden="true">
        <div class="modal-dialog" role="document">


          <div class="modal-content">

            <div class="modal-header bg-primary">
              <h5 class="modal-title text-white" id="assetsCategoryModal">Add Assets Category</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>


            <form id="addAssets" method="POST" enctype="multipart/form-data" class="form-horizontal" action="controller/assetsCategoryController.php">


              <div class="modal-body">

                <div class="form-group row">
                  <label for="input-10" class="col-sm-4 col-form-label">Assets Category<span class="text-danger">*</span></label>
                  <div class="col-sm-8">

                    <input type="text" class="form-control" name="assets_category" id="assets_category" required="" />
                  </div>
                </div> 
              </div>

              <div class="modal-footer">

                <div class="col-md-12 text-center">
                  <input type="hidden" name="submit_assets" id="submit_assets" value="submit_assets" />
                  <button type="submit" class="btn btn-success"  id="submit_assets"  value="submit_assets" >Add</button>
                </div>
              </div>
            </form>
          </div>

        </div>

      </div>
<!-- -------- -->

<div style="display: inline-block;">
    <div class="modal fade" id="editAssetscategory" tabindex="-1" role="dialog" aria-labelledby="assetsCategoryModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header bg-primary">
            <h5 class="modal-title text-white" id="assetsCategoryModal">Edit Assets Category</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <span id="edit_Assets_category"></span>
 
        </div>
      </div>
    </div>
</div>