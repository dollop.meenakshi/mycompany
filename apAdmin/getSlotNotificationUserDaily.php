<?php
session_start();
error_reporting(0);
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$society_id = $_COOKIE['society_id'];
extract(array_map("test_input" , $_POST));
$daily_visitor_unit_master_Added = $d->select("daily_visitor_unit_master,daily_visitors_master","daily_visitors_master.visitor_id=daily_visitor_unit_master.daily_visitor_id AND daily_visitor_unit_master.daily_visitor_id ='$daily_visitor_id' AND daily_visitor_unit_master.unit_id='$unit_id'");
 $data=mysqli_fetch_array($daily_visitor_unit_master_Added);
 ?>
 <style type="text/css">
   .def-padding{
    padding-left: 5px !important;
    padding-right: 5px !important;
   }
   .pb-2, .py-2 {
    padding-bottom: 0px !important;
   }
.pt-2, .py-2 {
   padding-top: 0px !important;
 }
 .form-group {
    margin-bottom: 0px !important;
}
 </style>
 <?php
if(isset($EditFlg) && $EditFlg=="Yes"){  ?>
<div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>User</th>
              <th>Days</th>
              <th>Notification</th>
              <!-- <th>Action</th> -->
            </tr>
          </thead>
          <tbody>
          <?php   
          $i3=1;
          $alradyAddeduserAray = array();

           $uq=$d->select("unit_master,block_master,users_master,daily_visitor_unit_master ","users_master.delete_status=0 AND daily_visitor_unit_master .unit_id=unit_master.unit_id  AND
            daily_visitor_unit_master .user_id=users_master.user_id AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  daily_visitor_unit_master .daily_visitor_id ='$daily_visitor_id' AND daily_visitor_unit_master.unit_id='$unit_id'","ORDER BY users_master.user_id ASC");
              while ($unitData=mysqli_fetch_array($uq)) { 
                array_push($alradyAddeduserAray, $unitData['user_id']);

                $inew= $i3++;
            ?>
            <tr>
              <td><?php echo $unitData['user_full_name'];  ?> (<?php echo $unitData['user_designation']; 
                       ?>)</td>
              <td><?php echo $unitData['week_days_user'];  ?></td>
              <td>
                <?php if($unitData['active_status']=="0"){  ?>
                 <a href="javascript:void(0)"  onclick="muteDailyVisitorNotification('<?php echo $unitData[unit_id];?>','<?php echo $unitData[user_id];?>','<?php echo $unitData[daily_visitor_id];?>');" > <i class="fa fa-bell" aria-hidden="true"></i> On </a>
                <?php } else { ?>
                  <a href="javascript:void(0)"  onclick="unmuteDailyVisitoreNotification('<?php echo $unitData[unit_id];?>','<?php echo $unitData[user_id];?>','<?php echo $unitData[daily_visitor_id];?>');" > <i class="fa fa-bell-slash" aria-hidden="true"></i> Off</a>
                <?php } ?>
              </td>
             <!--  <td>
                <?php if (mysqli_num_rows($uq)>1) { ?>
                  <button style="margin-top: 0px !important;" type="button" onclick="removeSingleUserDailyVisitor('<?php echo $unitData[unit_id];?>','<?php echo $unitData[user_id];?>','<?php echo $unitData[daily_visitor_id];?>');" class="form-btn btn btn-sm btn-danger waves-effect waves-light m-1" title="Delete"> <i class="fa fa-trash-o"></i> </button>
                  <?php  }?>
              </td> -->
            </tr>
          <?php } ?>
          </tbody>
        </table>
        <?php 
        $ids = join("','",$alradyAddeduserAray);   
             
              $q3=$d->select("unit_master,block_master,users_master","users_master.user_status=1 AND users_master.delete_status=0 AND  users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 and  users_master.user_id  NOT IN ('$ids') $blockAppendQueryUser  AND users_master.unit_id='$unit_id'  and   users_master.user_id  NOT IN ('$ids')"," ORDER BY block_master.block_sort ASC");
            if (mysqli_num_rows($q3)>0) {
              ?>
        <form  id="personal-info" action="controller/dailyVisitorController.php" method="post" enctype="multipart/form-data">
         <input type="hidden" name="daily_visitor_id" value="<?php echo $daily_visitor_id;?>">
         <input type="hidden" name="visitor_name" value="<?php echo $data['visitor_name'];?>">
         <input type="hidden" name="valid_till" value="<?php echo $data['valid_till_user'];?>">
         <input type="hidden" name="in_time" value="<?php echo $data['in_time_user'];?>">
         <input type="hidden" name="out_time" value="<?php echo $data['out_time_user'];?>">
         <input type="hidden" name="week_days" value="<?php echo $data['week_days_user'];?>">
         <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
         
        <div class="form-group">
          <div class="col-sm-12">
            <label  class="col-form-label">Manage Units/User For This Visitor</label>
            <select  name="user_data[]" id="" class="form-control " required="" >
              <option value="">-- Select User --</option>
               <?php 
              
            while ($blockRow=mysqli_fetch_array($q3)) {
              if($blockRow['user_type']=="0" && $blockRow['member_status']=="0" ){
                    $uType= " (Owner)";
                } else  if($blockRow['user_type']=="0" && $blockRow['member_status']=="1" ){
                    $uType= " (Owner ".$blockRow['member_relation_name'].')';
                } else  if($blockRow['user_type']=="1" && $blockRow['member_status']=="0"  ){
                    $uType= " (Tenant)";
                }else  if($blockRow['user_type']=="1" && $blockRow['member_status']=="1"   ){
                    $uType= " (Tenant ".$blockRow['member_relation_name'].')';
                } else   {
                  $uType= " (Owner)";
                } 
              ?>
              <option value="<?php echo $blockRow['unit_id'].'~'.$blockRow['user_id'].'~'.$blockRow['user_type'];?>"><?php echo $blockRow['block_name'];?>-<?php echo $blockRow['unit_name'];?>  <?php echo $blockRow['user_full_name'];?> <?php echo $uType;?></option>
            <?php }?>


              ?>
            </select>
          </div>
          <div class="col-sm-2">
            <input type="hidden" name="dailyVisitiorUserBtnAll" value="dailyVisitiorUserBtnAll">
            <button type="submit" id="" class="btn btn-success mt-4" ><i class="fa fa-check-square-o"></i> Add</button>
          </div>
        </div>
        </form>
      <?php } ?>
      </div>
      </div>
    </div>
 
  <?php

}
?>
<!-- <script type="text/javascript">
  
$('.form-btn').on('click',function(e){
    e.preventDefault();
    var form = $(this).parents('form');
     swal({
        title: "Are you sure?",
        text: "",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ['Cancel', 'Yes, I am sure !'],
      })
     .then((willDelete) => {
        if (willDelete) {
          form.submit();
        }
      });

});

</script> -->