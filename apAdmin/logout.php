<?php 
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$con=$d->dbCon();
$base_url=$m->base_url();
$urlArya = explode("/", $base_url);
$cookieUrl = $urlArya[3];
$admin_id = $_COOKIE['bms_admin_id'];
$bq= $d->select("bms_admin_master","admin_id='$admin_id'");
$adminData= mysqli_fetch_array($bq);
$phone_model = $adminData['phone_model'];
$phone_brand = $adminData['phone_brand'];

$ip_address=$d->get_client_ip(); # Save The IP Address
$browser=$browser=$_SERVER['HTTP_USER_AGENT']; # Save The User Agent
$loginTime=date("d M,Y h:i:sa");//Login Time
$m->set_data('admin_id',$admin_id);
$m->set_data('name',$adminData['admin_name']);
$m->set_data('role_name','Admin');
$m->set_data('ip_address',$ip_address);
$m->set_data('browser',$browser);
$m->set_data('loginTime',$loginTime);

$a1= array ('admin_id'=> $m->get_data('admin_id'),
    'name'=> $m->get_data('name'),
    'role_name'=> $m->get_data('role_name'),
    'ip_address'=> $m->get_data('ip_address'),
    'browser'=> $m->get_data('browser'),
    'loginTime'=> $m->get_data('loginTime'),
    'login_type'=> 1,
);

$insert=$d->insert('session_log',$a1);

session_start();
session_destroy();
if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time()-1000);
        setcookie($name, '', time()-1000, '/');
        setcookie($name, '', time()-1000, "/$cookieUrl");
    }
}

header("location:./index.php");
 ?>