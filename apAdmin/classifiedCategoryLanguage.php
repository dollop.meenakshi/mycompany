<?php 
  extract($_GET);
  if(isset($_GET['lId'])) { 
    $lId = (int)$lId;
  }
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-6">
        <h4 class="page-title">Classified Category Language </h4>
        
      </div>
      <div class="col-sm-6">
              <form method="get">
                <select class="form-control single-select" name="lId" onchange="this.form.submit()">
                  <option value="">  Select <?php echo $xml->string->society; ?> </option>
                  <?php $qs = $d->select("language_master" ,"","order by language_id  ASC");
                      while ($sdata=mysqli_fetch_array($qs)) { ?>
                  <option <?php if(isset($_GET['lId']) && $sdata['language_id']==$_GET['lId']) { echo "selected";} ?> value="<?php echo $sdata['language_id']; ?>"><?php echo $sdata['language_name']; ?> (<?php echo $sdata['language_name_1']; ?>)</option>
                  <?php } ?>
                </select>
              </form>

      </div>
    </div>
    <!-- End Breadcrumb-->


    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <?php if(isset($_GET['lId'])) { ?>
                <div class="table-responsive">
                  <table  class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Icon</th>
                        <th>Language Name</th>
                      </tr>
                    </thead>
                    <tbody>
                       <form action="controller/appMenuController.php" id="adMultiUnit" method="post" >
                        <input type="hidden" name="setClasCategoryLanguage" value="<?php echo 'setClasCategoryLanguage';?>">
                        <input type="hidden" name="language_id" value="<?php echo $lId;?>">
                      <?php 
                        $i=1;
                        $q=$d->select("classified_category","","");
                        
                        while ($data=mysqli_fetch_array($q)) {
                        extract($data);
                        $qt=$d->select("classified_category_language","classified_category_id='$classified_category_id' AND language_id='$lId' AND cat_type=0");
                        $lngData=mysqli_fetch_array($qt);

                      ?>
                        <tr>
                          
                          <td><?php echo $i++; ?></td>
                            <td ><?php echo $classified_category_name; ?></td>
                            <td><img src="../img/classified/classified_cat/<?php echo $classified_category_image; ?>" width='50' alt=""></td>
                           
                          <td>
                              <input value="<?php echo $classified_category_id; ?>" type="hidden" name="classified_category_id[]">

                              <input class="form-control" value="<?php echo $lngData['language_category_name']; ?>" type="text" name="language_category_name[]">
                          </td>
                          
                       
                         
                        </tr>

                      <?php } if (mysqli_num_rows($q)>0) {  ?>
                        <tr>
                          <td colspan="7" class="text-center">
                            <button type="submit"  class="btn ml-3 btn-primary btn-sm">Update Name</button>
                          </td>

                        </tr>
                       <?php } ?>
                       </form>
                    </tbody>

                  </table>
                </div>
             <?php } else {
              echo "Please Select Society";
             } ?>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
</div>

