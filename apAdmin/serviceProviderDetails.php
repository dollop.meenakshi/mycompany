<?php
if($_GET){
	extract($_GET);
}
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2">
			<div class="col-sm-6">
				<h4 class="page-title">Vendor Details</h4>
				
			</div>
			<div class="col-sm-6">
				<form action="" method="get">
					<select name="sp_id" class="form-control single-select" onchange="this.form.submit();">
						<option value="">-- Select Vendor --</option>
						<?php
						$serviceProviders = $d->select("local_service_provider_users","");
						while ($details = mysqli_fetch_array($serviceProviders)) { ?>
							<option <?=($sp_id) ? ($sp_id==$details['service_provider_users_id'] ? 'selected' : '') : ''?> value="<?=$details['service_provider_users_id']?>"><?=$details['service_provider_name'].'( '.$details['service_provider_phone'].' - '.$details['category_name'].' )'?></option>
						<?php }
						?>
					</select>
				</form>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<?php
						$sp_id= (int)$sp_id;
						$q = $d->select("local_service_provider_users","service_provider_users_id='$sp_id'");
						$sp = $q->fetch_assoc();
						if (mysqli_num_rows($q)>0) {

						$country_id = $sp['country_id'];
					    $state_id = $sp['state_id'];
					    $city_id = $sp['city_id'];
					  
					?>
					<div class="card-header">
						<div class="row">
							<div class="col-sm-10">

								<?php echo $sp['service_provider_name'];  if ($sp['kyc_status']==1) {
                    echo " <img src='../img/check.png' width='15'>";
                  } ?> 
							</div>

						</div>
					</div>
					<div class="card-body">
						<div class="row m-1">
							<div class="col-lg-6">
								<div class="form-group row">
									<label for="secretary_mobile" class="col-sm-4"> Contact Person </label>
									<div class="col-sm-8">
										<?php echo $sp['contact_person_name'] ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="secretary_mobile" class="col-sm-4"> Mobile </label>
									<div class="col-sm-8">
										<?php echo $sp['service_provider_phone'] ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="input-13" class="col-sm-4"> Email </label>
									<div class="col-sm-8">
										<?php echo $sp['service_provider_email'] ?>
									</div>
								</div>
								
								<?php if($sp['open_time']!==''){ ?>
									<div class="form-group row">
										<label for="secretary_mobile" class="col-sm-4"> Timings </label>
										<div class="col-sm-8">
											<?php echo $sp['open_time'] ?> - <?php echo $sp['close_time'] ?>
										</div>
									</div>
								<?php } ?>
								<div class="form-group row" >
									<label for="input-12" class="col-sm-4">Category </label>
									<div class="col-sm-8">
										<?php $i1=1;
										 $qcd=$d->select("local_service_provider_users_category,local_service_provider_master","local_service_provider_master.local_service_provider_id=local_service_provider_users_category.category_id AND  local_service_provider_users_category.service_provider_users_id='$sp_id'");
									      while ($catData=mysqli_fetch_array($qcd)) {
									      	echo $i1++;
									      echo	'. '.$catData['service_provider_category_name'];
									      echo "<br>";
									      }
										
										?>
									</div>
								</div>
								<div class="form-group row" >
									<label for="input-12" class="col-sm-4">Sub Category </label>
									<div class="col-sm-8">
										<?php
										$get_all_sub_id = $d->selectRow("service_provider_sub_category_name,sub_category_id","local_service_provider_users_category JOIN local_service_provider_sub_master ON local_service_provider_users_category.sub_category_id = local_service_provider_sub_master.local_service_provider_sub_id","service_provider_users_id = '$sp_id'");
										$i2=1;
										while ($blockRow=mysqli_fetch_array($get_all_sub_id)) 
										{
											echo $i2++;
											echo ". " . $blockRow['service_provider_sub_category_name'];
											echo "<br>";
										}
										?>
									</div>
								</div>
								<div class="form-group row">
									<label for="input-12" class="col-sm-4">Rating </label>
										<div class="col-sm-8">
										<?php
											$data = $d->select('local_service_providers_ratings',"local_service_provider_id='$sp_id' AND status=0");
											$average = 0;
											while ( $avg = mysqli_fetch_array($data)) {
												$average = $average + $avg['rating_point'];
											}
											$totalRating = mysqli_num_rows($data);
											if($totalRating>0){
												$averagerating = $average/$totalRating;
												$total = number_format($totalRating);
												$average = number_format($averagerating,1);
											}else{
												$total = '0';
												$average = '0';          
											}
											$rating = $average;
											$star = '';
											if($rating>0){
												(2.8*2)%2;
												$fullStars = floor( $rating );
												$halfStar = round(( $rating * 2 ) % 2);
												$t=0;
												for( $i=0; $i<5; $i++ ){
													if($i<$fullStars){
														$star .= "<img src='../img/star-full.jpg' alt='Full Star' style='width:15px; height:15px;'>";
														$t = $t+1;
													}
												}
												if( $halfStar ){
													$star .= "<img src='../img/star-half.jpg' alt='Half Star' style='width:15px; height:15px;'>";
													$t = $t+1;
												}
												for($i=5;$i>$t;$i--){
													$star .= "<img src='../img/star-not.jpg' alt='Half Star' style='width:15px; height:15px;'>";
												}
											}else{
												for( $i=0; $i<5; $i++ ){
													$star .= "<img src='../img/star-not.jpg' alt='Half Star' style='width:15px; height:15px;'>";
												}
											}
											echo $star." (".$rating.")";
										?>
									</div>
									
								</div>
								
								<div class="form-group row">
									<label for="input-101" class="col-sm-4"> Address </label>
									<div class="col-sm-8">
										<p><?php echo $sp['service_provider_address'] ?></p>
									</div>
								</div>	
								<div class="form-group row">
									<label for="input-101" class="col-sm-4"> Zipcode </label>
									<div class="col-sm-8">
										<?php echo $sp['service_provider_zipcode'] ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="input-101" class="col-sm-4"> GST No. </label>
									<div class="col-sm-8">
										<?php echo $sp['gst_no'] ?>
									</div>
								</div>
								<div class="form-group row">
									<label for="input-101" class="col-sm-4"> Website </label>
									<div class="col-sm-8">
										<a target="_blank" href="<?php echo $sp['sp_webiste'] ?>"><?php echo $sp['sp_webiste'] ?></a>
									</div>
								</div>
								<div class="form-group row">
									<label for="input-101" class="col-sm-4"> Work Description </label>
									<div class="col-sm-8">
										<?php echo $sp['work_description'] ?>
									</div>
								</div>								
							</div>
							<div class="col-lg-6 text-center overflow-hidden px-md-3">
								<?php if ($sp['service_provider_user_image']!="") { ?>
								<img style="width: 200px;"   src="../img/local_service_provider/local_service_users/<?php echo $sp['service_provider_user_image']; ?>" />
								<?php } ?>

								<div class="form-group row" id="map" style="width: 100%; height: 400px;margin:0px;"></div>
							</div>
						</div>
						<div class="row m-auto">
							
							<?php if ($sp['id_proof']!='') { ?>
								<button class="btnForm btn btn-primary ml-3 mr-3" href="#idProof<?php echo $sp['service_provider_users_id'];?>">View ID Proof</button>
								<div id="idProof<?php echo $sp['service_provider_users_id'];?>" style="display:none">
									<picture>
										<source srcset="../img/local_service_provider/local_service_id_proof/<?php echo $sp['id_proof']; ?>" media="(max-width: 800px)">
										<img style="max-height: 500px; max-width: 500px;"   src="../img/local_service_provider/local_service_id_proof/<?php echo $sp['id_proof']; ?>" />
									</picture>
									<br>
									<span><center><b><?php echo $sp['service_provider_name']; ?></b></center></span>
								</div>
							<?php } 
							$location_proof_file_name = $sp['location_proof'];		
							$location_proof_file_ext = pathinfo($sp['location_proof'], PATHINFO_EXTENSION);							
							if ($sp['location_proof']!='') 
							{ 
								if($location_proof_file_ext == "jpg" || $location_proof_file_ext == "jpeg" || $location_proof_file_ext == "png" || $location_proof_file_ext == "gif")
								{
								?>
									<button class="btn btn-primary btnForm" href="#locationProof<?php echo $sp['service_provider_users_id'];?>">Location Proof</button>
									<div id="locationProof<?php echo $sp['service_provider_users_id'];?>" style="display:none">
										<picture>
											<source srcset="../img/local_service_provider/local_service_location_proof/<?php echo $sp['location_proof']; ?>" media="(max-width: 800px)">
											<img style="max-height: 500px; max-width: 500px;"   src="../img/local_service_provider/local_service_location_proof/<?php echo $sp['location_proof']; ?>" />
										</picture>
										<br>
										<span><center><b><?php echo $sp['service_provider_name']; ?></b></center></span>
									</div>
							<?php 
								}
								elseif($location_proof_file_ext == "pdf" || $location_proof_file_ext == "PDF")
								{
								?>
									<button class="btn btn-primary" onclick="window.open('../img/local_service_provider/local_service_location_proof/<?php echo $location_proof_file_name; ?>','_blank')">Location Proof</button>
								<?php
								}
							}
							if ($sp['brochure_profile']!='') { ?>

							<a target="_blank" href="../img/local_service_provider/local_service_location_proof/<?php echo $sp['brochure_profile']; ?>" class="btn ml-3 btn-primary" >Profile/Brochure</a>
							<?php } ?>

								</div>
						</div>
					</div>
					<?php } else { ?>
						<img src="img/no_data_found.png">
					<?php } ?>
				</div>
			</div>
		</div>
		<?php if(mysqli_num_rows($q)>0){ ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-body">
							<h4>Rating</h4>
							<div class="table-responsive">
								<table  class="table table-bordered example">
									<thead>
										<tr>
											<th>#</th>
											<th>Employee</th>
											<th>Mobile</th>
											<th>Rating</th>
											<th>Comment</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$data = $d->select('society_master,local_service_providers_ratings',"society_master.society_id=local_service_providers_ratings.society_id AND  local_service_providers_ratings.local_service_provider_id='$sp_id' AND local_service_providers_ratings.status=0","ORDER BY rating_id DESC");
										$i1=1;
										while ( $data1 = mysqli_fetch_array($data)) { ?>
											<tr>
												<td><?=$i1++;?></td>
												<td><?=$data1['user_name']?></td>
												<td><?=$data1['user_mobile']?></td>
												<?php
												$rating = $data1['rating_point'];
												$star1 = '';
												$fullStars = floor( $rating );
												$halfStar = round( $rating * 2 ) % 2;
												$t=0;
												for( $i=0; $i<5; $i++ ){
													if($i<$fullStars){
														$star1 .= "<img src='../img/star-full.jpg' alt='Full Star' style='width:15px; height:15px;'>";
														$t = $t+1;
													}
												}
												if( $halfStar ){
													$star1 .= "<img src='../img/star-half.jpg' alt='Half Star' style='width:15px; height:15px;'>";
													$t = $t+1;
												}
												for($i=5;$i>$t;$i--){
													$star1 .= "<img src='../img/star-not.jpg' alt='Half Star' style='width:15px; height:15px;'>";
												}
												?>
												<td><?=$star1?></td>
												<td><?=$data1['rating_comment']?></td>
												
												<td>
													<form method="POST" action="controller/serviceProviderController.php">
														<input type="hidden" name="sp_id" value="<?php echo $sp_id ?>">
														<input type="hidden" name="delete_rating" value="<?php echo $data1['rating_id'] ?>">
														<button class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash-o"></i></button>
													</form>
												</td>
											</tr>
										<?php  }?>									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-body">
							<h4>Complains</h4>
							<div class="table-responsive">
								<table  class="table table-bordered example">
									<thead>
										<tr>
											<th>#</th>
											<th>Employee</th>
											<th>Mobile</th>
											<th>Comment</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$data = $d->select('local_service_provider_complains,society_master',"local_service_provider_complains.local_service_provider_id='$sp_id' AND local_service_provider_complains.society_id=society_master.society_id AND local_service_provider_complains.status=0","ORDER BY local_service_provider_complains.complain_id DESC");
											$i1=1;
											while ( $data1 = mysqli_fetch_array($data)) { 
										?>
										<tr>
											<td><?=$i1++;?></td>
											<td><?=$data1['user_name']?></td>
											<td><?=$data1['user_mobile']?></td>
											<td><?=$data1['comment']?></td>
											
											<td>
												<form method="POST" action="controller/serviceProviderController.php">
													<input type="hidden" name="sp_id" value="<?php echo $sp_id ?>">
													<input type="hidden" name="delete_complain" value="<?php echo $data1['complain_id'] ?>">
													<button class="btn btn-sm btn-danger form-btn"><i class="fa fa-trash-o"></i></button>
												</form>
											</td>
										</tr>
										<?php } ?>									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--  -->
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-body">
							<div class="row pt-2 pb-2">
								<div class="col-sm-9 col-3">
									<h4>Bank</h4>
								</div>
								<div class="col-sm-3 col-9">
          							<div class="btn-group float-sm-right">
										<button class="btn btn-sm btn-primary" data-target="#vendorBankModal" data-toggle="modal" ><i class="fa fa-plus mr-1"></i> Add</button>
										<a href="javascript:void(0)" onclick="DeleteAll('deleteBankDetail');" class="btn btn-sm pull-right btn-danger "><i class="fa fa-trash-o fa-lg"></i> Delete </a>
									</div>
								</div>
							</div>
							<div class="table-responsive">
								<table id="example" class="table table-bordered ">
									<thead>
										<tr>
											<th>#</th>
											<th>Sr.No</th>
											<th>Bank Name</th>
											<th>Account No.</th>
											<th>IFSC Code</th>
											<th>Account Holder Name</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$data = $d->select('service_provider_bank_detail',"service_provider_id='$sp_id' AND sp_bank_deleted=0");
											$i1=1;
											while ( $data1 = mysqli_fetch_array($data)) { 
										?>
										<tr>
											<td class="text-center">
											<input type="hidden" name="id" id="id" value="<?php echo $data1['service_provider_bank_id']; ?>"> 
											<input type="checkbox" name="" class="multiDelteCheckbox multiple" value="<?php echo $data1['service_provider_bank_id']; ?>">
											</td>									
											<td><?=$i1++;?></td>
											<td><?=$data1['service_provider_account_no']?></td>
											<td><?=$data1['service_provider_bank_name']?></td>
											<td><?=$data1['service_provider_ifsc']?></td>
											<td><?=$data1['account_holdar_name']?></td>
											<td>
												<form method="POST" action="controller/serviceProviderController.php">
													<input type="hidden" name="sp_id" value="<?php echo $sp_id ?>">
													<button type="button" class="btn btn-sm btn-primary mr-1" onclick="serviceProviderSetData(<?php echo $data1['service_provider_bank_id']; ?>)"  > <i class="fa fa-pencil"></i></button> 
													
												</form>
											</td>
										</tr>
										<?php } ?>									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--  -->

		<?php } ?>



<div class="modal fade" id="vendorBankModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Bank</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="" style="align-content: center;">
      <div class="card-body">

           <form id="addServiceProviderDetail" action="controller/VendorBankController.php" enctype="multipart/form-data" method="post">
           <div class="row">
               <div class="col-md-12">
			   <div class="form-group row">
						<label for="input-10" class="col-sm-4 col-form-label">Bank Name<span class="required">*</span></label>
						<div class="col-lg-8 col-md-8" id="">
							<input type="text" name="service_provider_bank_name" id="service_provider_bank_name" class="form-control "></input>
						</div>                   
					</div>
					<div class="form-group row">
						<label for="input-10" class="col-sm-4 col-form-label">Account No <span class="required">*</span></label>
						<div class="col-lg-8 col-md-8" id="">
							<input type="text" name="service_provider_account_no" id="service_provider_account_no" class="form-control">
						</div>                   
					</div> 
					<div class="form-group row">
						<label for="input-10" class="col-sm-4 col-form-label">IFSC Code<span class="required">*</span></label>
						<div class="col-lg-8 col-md-8" id="">
							<input type="text" name="service_provider_ifsc" id="service_provider_ifsc" class="form-control "></input>
						</div>                   
					</div> 
					
                   <div class="form-group row">
                       <label for="input-10" class="col-lg-4 col-md-4 col-form-label col-12">Account Holder Name <span class="required">*</span></label>
                       <div class="col-lg-8 col-md-8 col-12" id="">
					   		<input type="text" name="account_holdar_name" id="account_holdar_name" class="form-control"> 
                       </div>
                   </div>
					
					<div class="form-group row">
						<label for="input-10" class="col-sm-4 col-form-label">Customer Id/CRN No.</label>
						<div class="col-lg-8 col-md-8" id="">
							<input type="text" name="crn_no" id="crn_no" class="form-control "></input>
						</div>                   
					</div>
							
					<div class="form-footer text-center">
						<input type="hidden" id="service_provider_bank_id" name="service_provider_bank_id" value="" >
						<input type="hidden" id="service_provider_id" name="service_provider_id" value="<?php echo $sp_id ?>" >
						<button id="addServiceProviderBtn" name="addServiceProviderBtn" type="submit"  class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
						<input type="hidden" name="addServiceProvider" value="addServiceProvider">
						<button id="addServiceProviderBtn" type="submit"  class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
								
						<button type="button" onclick="resetForm()"  value="add" class="btn btn-danger" ><i class="fa fa-check-square-o"></i> Reset</button>
									
					</div>
				</div>
			</div>
         </form>

       </div>
      </div>

    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script>

function resetForm()
{
	$('#addServiceProviderDetail').trigger("reset");
}

function buttonSetting()
{
  $('.hideupdate').hide();
  $('.hideAdd').show();
}
    function initMap() {
      var myLatLng = {lat: <?php echo $sp['service_provider_latitude'] ?>, lng: <?php echo $sp['service_provider_logitude'] ?>};

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: myLatLng
      });

      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: '<?php echo $sp['service_provider_name'] ?>'
      });
    }
</script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=<?php echo $d->map_key();?>&callback=initMap">
</script>
<script type="text/javascript">
  $(function(){
    $(".btnForm").fancybox();
  });


  $(document).ready( function () {
    $('.example').DataTable();
} );
</script>
<style>
.hideupdate{
  display:none;
}

</style>