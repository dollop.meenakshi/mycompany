
<div class="content-wrapper">
    <div class="container-fluid">
      
	  	<div class="row pt-2 pb-2">
        	<div class="col-sm-3 col-md-6 col-6">
          		<h4 class="page-title">Scrap Assets Item</h4>
     		</div>
     		<div class="col-sm-3 col-md-6 col-6">
       			<div class="btn-group float-sm-right">
        		</div>
     		</div>
     	</div>
	  	<form action="" method="get" id="searchbycatform">
			<div class="row pt-2 pb-2">
				<div class="col-md-3 form-group">
					<select type="text" onchange="this.form.submit();" name="id" id="id" class="form-control single-select" style="width: 100%">
					<option value="">All Category</option>
					<?php $q12 = $d->select("assets_category_master", "", "order by assets_category ASC");
					while ($row12 = mysqli_fetch_array($q12)) { ?>
						<option value="<?php echo $row12['assets_category_id']; ?>" <?php if (isset($_REQUEST) && isset($_REQUEST['id'])) {
																					if ((int)$_REQUEST['id'] == $row12['assets_category_id']) {
																						echo "selected";
																					}
																					} ?>><?php echo $row12['assets_category']; ?> </option>
					<?php } ?>
					</select>
				</div>
			</div>
        </form>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-bordered">
								<thead>
									<tr>
										<th>#</th>
										<th>Category</th>
										<th>Item Name</th>
										<th>Brand </th>
										<th>Item Image</th>
										<th>Reason</th>
										<th>Sold Out Price</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i1 = 1;
									if (isset($_GET['id']) && $_GET['id'] > 0 && filter_var($_GET['id'], FILTER_VALIDATE_INT) == true) {
									$appendQery = " AND assets_item_detail_master.assets_category_id='$_GET[id]'";
									}
									$q = $d->select1("assets_item_detail_master LEFT JOIN assets_detail_inventory_master ON assets_detail_inventory_master.assets_id = assets_item_detail_master.assets_id AND assets_detail_inventory_master.end_date='0000-00-00' LEFT JOIN users_master ON users_master.user_id =assets_detail_inventory_master.user_id ,assets_category_master","assets_item_detail_master.*,assets_category_master.*,users_master.user_full_name,assets_detail_inventory_master.user_id" , "assets_item_detail_master.assets_category_id=assets_category_master.assets_category_id AND assets_item_detail_master.move_to_scrap=1  $appendQery ", "ORDER BY assets_item_detail_master.assets_id DESC");
									while ($row = mysqli_fetch_array($q)) {
									?>
									<tr>
										<td><?php echo $i1++; ?> </td>
										<td><?php echo $row['assets_category']; ?> </td>
										<td><?php echo $row['assets_name']; ?> </td>
										<td><?php echo $row['assets_brand_name']; ?> </td>
										<td class="text-center align-middle">
										<?php if($row['assets_file'] != ''){ 
											if(file_exists("../img/society/$row[assets_file]")) {
											?>
										<a data-fancybox="images" data-caption="Photo Name : <?php echo $row['assets_file'] ?>" href="../img/society/<?php echo $row['assets_file'] ?>" target="_blank">
											<img style="max-height:100px; max-width:100px;" class="lazyload" onerror="this.src='../img/banner_placeholder.png'" src='../img/banner_placeholder.png' id="<?php echo $row['assets_id']; ?>" data-src="../img/society/<?php echo $row['assets_file']; ?>"></a>
										<?php } } ?>
										</td>
										<td><?php echo $row['move_to_scrap_reason']; ?></td>
										<td><?php if($row['is_assets_item_sold_out'] == 1){ echo $row['sold_out_price']; } ?></td>
										<td>
											<div class="d-flex align-items-center">
											<?php if($row['is_assets_item_sold_out'] == 0){ ?>
												<button class="btn btn-sm btn-success" title="Sold Out" onclick="itemSoldOut(<?php echo $row['assets_id']; ?>)"><i class="fa fa-tags"></i></button>
											<?php }else{ ?>
											<p class="card-title badge badge-success">Sold Out</p>
											<?php } ?>
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

    </div>
</div>


<div class="modal fade" id="soldOutReasonAndPriceModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Sold Out</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="assetsSoldOutForm" action="controller/assetsItemDetailsController.php"  enctype="multipart/form-data" method="post">      
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Sold Out Price <span class="required">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <input class="form-control onlyNumber" placeholder="Sold Out Price" name="sold_out_price">
                        </div>                   
                    </div>     
                    <div class="form-footer text-center">
                      <input type="hidden" name="sold_out_assets" value="sold_out_assets">
                      <input type="hidden" name="assets_id"  class="assets_id">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>