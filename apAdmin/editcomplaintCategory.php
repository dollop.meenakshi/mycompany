<?php 
extract($_POST); 
session_start();
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
  $data = $d->selectArray("complaint_category","complaint_category_id='$complaint_category_id'");
?>

<form id="editComplainCategoryValidation" method="POST" action="controller/complaintCategoryController.php" enctype="multipart/form-data">
  <input type="hidden" name="complaint_category_id" value="<?php echo $_POST['complaint_category_id'] ?>">
  <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
  <div class="row form-group">
    <label class="col-sm-3 form-control-label">Name <span class="text-danger">*</span></label>
    <div class="col-sm-9">
      <input type="text" class="form-control" required="" name="category_name" value="<?php echo html_entity_decode($data['category_name']); ?>">
    </div>
  </div>
  <div class="form-footer text-center">
    <button class="btn btn-sm btn-success"><i class="fa fa-check"></i> Submit</button>
  </div>
</form>

<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$().ready(function() {
  $.validator.addMethod("noSpace", function(value, element) { 
        return value == '' || value.trim().length != 0;  
    }, "No space please and don't leave it empty");

   $.validator.addMethod("alphaRestSpeChartor", function(value, element) {
        return this.optional(element) || value == value.match(/^[A-Za-z 0-9\d=!,\n\-@&()/?%._*]*$/);
    }, jQuery.validator.format("special characters not allowed"));


  $("#editComplainCategoryValidation").validate({
    rules: {
      category_name: {
          required: true,
          maxlength: 25,
          noSpace: true,
          alphaRestSpeChartor:true,
      },
    },
  });
});
</script>