<?php

session_start();

include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$assets_id = $_POST['assets_id'];
$society_id = $_POST['society_id'];
$q=$d->select("assets_item_detail_master","assets_id='$assets_id'");
$row = mysqli_fetch_array($q);
?>

<form id="additemDetails1" enctype="multipart/form-data" method="POST" class="form-horizontal" action="controller/assetsItemDetailsController.php">
    <div class="modal-body">
        <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>">
        <input type="hidden" name="assets_id" value="<?php echo $assets_id; ?>" id="assets_id">
      
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Assets Category<span class="required">*</span></label>
            <div class="col-lg-8">
                <select class="form-control multiple-select" required name="assets_category_id" id="assets_category_id">
                    <option  value="<?php echo $row['assets_category_id']; ?>">Select Category</option>
                    <?php $sql=$d -> select("assets_category_master","","");
                    while($data=mysqli_fetch_array($sql)){ ?>
                        <option <?php if($row['assets_category_id'] == $data['assets_category_id']) { echo "selected"; } ?> value="<?php echo $data['assets_category_id']; ?>" ><?php echo $data['assets_category']; ?></option>
                <?php  } ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Item Name<span class="text-danger">*</span></label>
            <div class="col-lg-8">
                <input type="text" required class="form-control" name="assets_name" id="assets_name"  value="<?php echo $row['assets_name'] ?>" >
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Brand Name</label>
            <div class="col-lg-8">
                <input type="text" class="form-control" name="assets_brand_name" id="assets_brand_name"  value="<?php echo $row['assets_brand_name'] ?>" >
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Image</label>
            <div class="col-lg-8">
                <input type="hidden" name="image_old" id="assets_file" value="<?php echo $row['assets_file']?>">
                <input type="file" accept="image/*" class="form-control" name="assets_file" id="assets_file"    value="<?php echo $row['assets_file'] ?>" 
                />
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Location</label>
            <div class="col-lg-8">
                <input type="text" class="form-control" name="assets_location" id="assets_location" value="<?php echo $row['assets_location'] ?>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Item Code</label>
            <div class="col-lg-8">
                <input type="text" class="form-control" name="assets_item_code" id="assets_item_code" value="<?php echo $row['assets_item_code'] ?>" />
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Description </label>
            <div class="col-lg-8">
                <textarea class="form-control" name="assets_description" id="assets_description"><?php echo $row['assets_description'] ?></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Sr. No./MAC/Sim</label>
            <div class="col-lg-8">

                <input type="text" maxlength="200" class="form-control" name="sr_no" id="sr_no" value="<?php echo $row['sr_no'] ?>"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Price</label>
            <div class="col-lg-8">

                <input type="text" maxlength="200" class="form-control" name="item_price" id="item_price"  value="<?php echo $row['item_price'] ?>"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Purchase Date</label>
            <div class="col-lg-8">

            <input type="text" readonly="" value="<?php if($row['item_purchase_date'] != '0000-00-00' && $row['item_purchase_date'] != 'null'){ echo $row['item_purchase_date']; }?>" class="form-control autoclose-datepicker-item" id="item_purchase_date" name="item_purchase_date">
            </div>
        </div>
        <div class="form-group row">
            <label for="input-10" class="col-lg-4 col-form-label">Invoice</label>
            <div class="col-lg-8">
                <input type="hidden" name="old_pdf" id="assets_invoice" value="<?php echo $row['assets_invoice']?>">
                <input type="file" accept="image/*,.pdf" class="form-control" name="assets_invoice" id="assets_invoice"    value="<?php echo $row['assets_invoice'] ?>" 
                />
            </div>
        </div>     
    </div>
    <div class="modal-footer">
        <div class="col-md-12 text-center">
            <input type="hidden" name="assets_id" id="assets_id_update" value="<?php echo $row['assets_id'] ?>" />
            <input type="hidden" name="update_assets" value="<?php $row['assets_id'] ?>" />
            <button type="submit" class="btn btn-success" name="update_assets">Update</button>
        </div>  
    </div>
</form>
<script src="assets/plugins/jquery-multi-select/jquery.multi-select.js"></script>
<script src="assets/plugins/jquery-multi-select/jquery.quicksearch.js"></script>
<script type="text/javascript" src="assets/js/select.js"></script>
<script src="assets/plugins/material-datepicker/js/moment.min.js"></script>
<script src="assets/plugins/material-datepicker/js/bootstrap-material-datetimepicker.min.js"></script>
<script src="assets/plugins/material-datepicker/js/ja.js"></script>
<script src="assets/js/datepicker.js"></script>
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>


<script type="text/javascript">

    $("#additemDetails1").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            assets_name: {
                required: true,
                noSpace:true
            },
            assets_category_id:{
                required: true,
                noSpace:true
            },  
        },
        messages: {
            assets_name:{
                required: "Please Enter Item Name"
            },
            assets_category_id:{
                required: "Please Select Assets Category"
            },
        },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            form.submit(); 
        } 
    });

</script>

