<?php error_reporting(0);
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Purchase Orders</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <a href="addPurchase" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
           <!--  <a href="javascript:void(0)" onclick="DeleteAll('deletePurchase0');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
      </div>
     </div>
      <form action="" method="get">
          <div class="row pt-2 pb-2">
            <div class="col-lg-2 col-6">
              <label  class="form-control-label">From Date </label>
              <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){echo $_GET['from'];}else{echo date('Y-m-01');} ?>">  
            </div>
            <div class="col-lg-2 col-6">
              <label  class="form-control-label">To Date </label>
              <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){echo $_GET['toDate'];}else{echo date('Y-m-t');} ?>">  
            </div>
            <div class="col-lg-2 col-6">
              <label  class="form-control-label">Site </label>
              <select name="site_id"  class="form-control single-select">
                  <option value="">All Site</option> 
                  <?php 
                      $site=$d->select("site_master","society_id='$society_id' ");  
                      while ($siteData=mysqli_fetch_array($site)) {
                  ?>
                  <option <?php if(isset($_GET['site_id']) && $_GET['site_id'] ==$siteData['site_id']){ echo "selected"; } ?> value="<?php if(isset($siteData['site_id']) && $siteData['site_id'] !=""){ echo $siteData['site_id']; } ?>"><?php if(isset($siteData['site_name']) && $siteData['site_name'] !=""){ echo $siteData['site_name']; } ?></option> 
                  <?php } ?>
              </select>
            </div>
            <div class="col-lg-2 col-6">
              <label  class="form-control-label">Status </label>
              <select name="purchase_status"  class="form-control">
                  <option value="">All</option> 
                  <option <?php if($_GET['purchase_status']=='0') { echo 'selected';} ?> value="0"  >Pending</option>
                  <option <?php if($_GET['purchase_status']=='1') { echo 'selected';} ?>  value="1" >Accepted</option>
                  <option <?php if($_GET['purchase_status']=='2') { echo 'selected';} ?>  value="2" >Rejected</option>
              </select>
            </div>
            <div class="col-lg-2 col-6">
              <label  class="form-control-label"> </label>
                <input style="margin-top:30px;" class="btn btn-success" type="submit" name="getReport" class="form-control" value="Search">
            </div>
          </div>
      </form>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
               
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <!-- <th>#</th> -->
                        <th>Sr.No</th>
                        <th>Site Name</th>
                        <th>Vandor Name</th>
                        <th>Created By</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="showFilterData">
                   <?php 
                    $i=1;
                      $monthStart = date('Y-m-01');
                      $monthEnd = date('Y-m-t');
                      $dateFilterQuery = " AND purchase_vendor_order_master.purchase_date BETWEEN '$monthStart' AND '$monthEnd'";                     
                      if(isset($_GET['from']) && isset($_GET['from']) && $_GET['toDate'] != '' && $_GET['toDate']) {
                        $dateFilterQuery = " AND purchase_vendor_order_master.purchase_date BETWEEN '$_GET[from]' AND '$_GET[toDate]'";
                      }
                      if(isset($_GET['site_id']) && $_GET['site_id'] >0) {
                        $siteFilterQuery = " AND purchase_vendor_order_master.site_id = '$_GET[site_id]'";
                      }
                      if(isset($_GET['purchase_status']) && $_GET['purchase_status'] != '') {
                        $statusFilterQuery = " AND purchase_vendor_order_master.purchase_status = '$_GET[purchase_status]'";
                      }
                      $q=$d->selectRow("purchase_vendor_order_master.*,users_master.user_full_name,site_master.site_name,created_by_user.user_full_name AS created_by_name,local_service_provider_users.service_provider_name","purchase_vendor_order_master LEFT JOIN users_master ON users_master.user_id = purchase_vendor_order_master.user_id LEFT JOIN site_master ON site_master.site_id = purchase_vendor_order_master.site_id LEFT JOIN local_service_provider_users ON  local_service_provider_users.service_provider_users_id = purchase_vendor_order_master.vendor_id LEFT JOIN users_master AS created_by_user ON created_by_user.user_id = purchase_vendor_order_master.created_by","purchase_vendor_order_master.society_id = $society_id $dateFilterQuery $statusFilterQuery $siteFilterQuery ");
                        $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                     <!--  <td class="text-center">
                          <?php 
                            if($data['purchase_status']==0) {
                          ?>
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['purchase_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['purchase_id']; ?>">                      
                          <?php } ?>
                      </td> -->
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['site_name']; ?></td>
                      <td><?php  echo $data['service_provider_name']; ?></td>
                      <td><?php  echo $data['created_by_name']; ?></td>
                      <td><?php echo date("d M Y h:i A", strtotime($data['purchase_date'])); ?></td>
                      <?php if($data['purchase_status'] == '0'){ 
                          $status = "Pending";
                          $statusClass = "badge-primary";
                        } else if($data['purchase_status'] == '1'){
                          $status = "Accepted";
                          $statusClass = "badge-success";
                        }else{
                          $status = "Rejected";
                          $statusClass = "badge-danger ";
                        }
                      ?>

                      <td><b><span class="badge badge-pill m-1 <?php echo $statusClass; ?>"><?php echo $status; ?></span></b></td>
                      <!-- <td><?php if(isset($data['purchase_invoice'])){ ?><a target="_blank" href="../img/purchase_invoice/<?php echo $data['purchase_invoice'];?>"><?php  echo $data['purchase_invoice'];  ?></a><?php }?></td>
                        -->
                       
                      <td>
                      <div class="d-flex align-items-center">
                        <form action="addPurchase" method="GET" accept-charset="utf-8">
                          <input type="hidden" name="purchase_id" value="<?php echo $data['purchase_id']; ?>">
                          <input type="hidden" name="edit_purchase" value="edit_purchase">
                         <!--  <button type="submit" class="btn btn-sm btn-primary mr-1" > <i class="fa fa-pencil"></i></button>  -->
                        </form>
                          <a href="purchaseOrders?id=<?php echo $data['vendor_order_id']; ?>" class="btn btn-sm btn-primary ml-1" ><i class="fa fa-eye"></i></a> 
                        </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
<script type="text/javascript">
  function vieComplain(complain_id) {
      $.ajax({
          url: "getComplaineDetails.php",
          cache: false,
          type: "POST",
          data: {complain_id : complain_id},
          success: function(response){
              $('#comResp').html(response);
            
              
          }
       });
  } 
</script>




<div class="modal fade" id="siteDetailModel">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Site Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="siteDetailModelDiv" style="align-content: center;">

      </div>
      
    </div>
  </div>
</div>
<!-- <div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Site</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           <form id="addSiteForm" action="controller/siteController.php" enctype="multipart/form-data" method="post">
           
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">Site Name <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
               <input type="text"  required="" name="site_name" id="site_name" class="form-control">
               </div>                   
           </div> 

           <div class="form-group row">
               <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Manager <span class="required">*</span></label>
                   <div class="col-lg-8 col-md-8" id="">
                      <select name="site_manager_id" id="site_manager_id" class="form-control single-select">
                          <option value="">All Users</option> 
                            <?php 
                              $qd=$d->select("users_master","society_id='$society_id'");  
                              while ($userData=mysqli_fetch_array($qd)) {
                            ?>
                          <option value="<?php echo  $userData['user_id'];?>" ><?php echo $userData['user_full_name'];?></option>
                          <?php } ?>
                  
                        </select>                 
                    </div>
           </div> 
         
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">Site Address <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
               <textarea  required="" name="site_address" id="site_address" class="form-control "></textarea>
               </div>                   
           </div> 
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">Site Discription</label>
               <div class="col-lg-8 col-md-8" id="">
               <textarea  name="site_description" id="site_description" class="form-control"></textarea>
               </div>                   
           </div> 
                             
           <div class="form-footer text-center">
            
             <input type="hidden" id="site_id" name="site_id" value="" >
             <button id="addSiteBtn" name="addSiteBtn" type="submit"  class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addSite" value="addSite">
             
             <button id="addSiteBtn" type="submit"  class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
           
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addSiteForm');"><i class="fa fa-check-square-o"></i> Reset</button>
            
           </div>

         </form>
      
       </div>
      </div>
      
    </div>
  </div>
</div> -->

<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
.hideupdate{
  display:none;
}

</style>