<?php
if(isset($_GET['bId']))
{
    if(!ctype_digit($_GET['bId']))
    {
        $_SESSION['msg1'] = "Invalid Block Id!";
    ?>
    <script>
        window.location = "manageLetterTemplate";
    </script>
    <?php
        exit;
    }
    $block_id = $_GET['bId'];
}
if(isset($_GET['letter_id']))
{
    if(!ctype_digit($_GET['letter_id']))
    {
        $_SESSION['msg1'] = "Invalid Request!";
    ?>
    <script>
        window.location = "manageLetterTemplate";
    </script>
    <?php
        exit;
    }
    $letter_id = $_GET['letter_id'];
    $letter_name = $_GET['letter_name'];
    $q = $d->select("letter_master","letter_id = '$letter_id' AND letter_name = '$letter_name'");
    if(mysqli_num_rows($q) > 0)
    {
        $data = $q->fetch_assoc();
        extract($data);
    }
    else
    {
        $_SESSION['msg1'] = "Invalid Letter Id Or Name!";
    ?>
    <script>
        window.location = "manageLetterTemplate";
    </script>
    <?php
        exit;
    }

    $get_dc = $d->selectRow("hdcm.hr_document_category_id,hdcm.hr_document_category_name","hr_document_category_master AS hdcm","hdcm.hr_document_category_name = 'Employee Documents'");
    if(mysqli_num_rows($get_dc) > 0)
    {
        $get_dc_data = $get_dc->fetch_assoc();
        $hr_document_category_id = $get_dc_data['hr_document_category_id'];
    }
    else
    {
        $a = [
            'society_id' => $society_id,
            'hr_document_category_name' => 'Employee Documents',
            'hr_document_category_created_by' => $_COOKIE['bms_admin_id'],
            'hr_document_category_status' => 0,
            'hr_document_category_created_date' => date("Y-m-d H:i:s"),
            'hr_document_category_deleted' => 0
        ];
        $hdcm = $d->insert("hr_document_category_master",$a);
        $hr_document_category_id = $con->insert_id;
    }

    $get_dsc = $d->selectRow("hdscm.hr_document_sub_category_id,hdscm.hr_document_sub_category_name","hr_document_sub_category_master AS hdscm","hdscm.hr_document_sub_category_name = '$letter_name'");
    if(mysqli_num_rows($get_dsc) > 0)
    {
        $get_dc_data = $get_dsc->fetch_assoc();
        $hr_document_sub_category_id = $get_dc_data['hr_document_sub_category_id'];
    }
    else
    {
        $a1 = [
            'society_id' => $society_id,
            'hr_document_category_id' => $hr_document_category_id,
            'hr_document_sub_category_name' => $letter_name,
            'hr_document_sub_category_created_by' => $_COOKIE['bms_admin_id'],
            'hr_document_sub_category_status' => 0,
            'hr_document_sub_category_created_date' => date("Y-m-d H:i:s"),
            'hr_document_sub_category_deleted' => 0
        ];
        $hdscm = $d->insert("hr_document_sub_category_master",$a1);
        $hr_document_sub_category_id = $con->insert_id;
    }
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title">Generate Documents </h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
                <div class="btn-group float-sm-right">
                </div>
            </div>
        </div>
        <form id="generateDocumentsFilterForm">
            <div class="row">
                <div class="col-lg-3 col-6 pb-2">
                    <label  class="form-control-label">Branch </label>
                    <select onchange="submitFilterForm();" name="bId" id="bId" class="form-control single-select" required>
                        <option value="">--Select--</option>
                        <?php
                        $cq = $d->selectRow("block_id,block_name","block_master","block_status = 0");
                        while ($cd = $cq->fetch_assoc())
                        {
                        ?>
                        <option <?php if($block_id == $cd['block_id']) { echo 'selected';} ?> value="<?php echo $cd['block_id']; ?>"><?php echo $cd['block_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
               
            </div>
        </form>
        <!-- End Breadcrumb-->
        <?php
        if(isset($_GET['letter_id']) && !empty($_GET['letter_id']) && isset($_GET['bId']) && !empty($_GET['bId']))
        {
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="page-title">Generate Documents For - <?php echo $letter_name; ?></h4>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="btn btn-sm btn-warning waves-effect waves-light" onClick="generateDocuments();" href="javascript:void(0);">Generate</a>
                            </div>
                        </div>
                        <label><input type="checkbox" name="sample" checked class="selectall"/> Select/Unselect all</label>
                        <div class="table-responsive">
                            <table id="sampleTable" class="table table-bordered auto-index">
                                <thead>
                                    <tr>
                                        <th class='deleteTh'>Check To Generate Document</th>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Document Status</th>
                                        <th>Document Link</th>
                                    </tr>
                                </thead>
                                <tbody id="checkboxlist">
                                <?php
                                    $i = 1;
                                    $append = "";
                                    if(isset($block_id) && $block_id != 0)
                                    {
                                        $append = " AND um.block_id = '$block_id'";
                                    }
                                    $q = $d->selectRow("hdm.hr_document_id,hdm.hr_document_file,um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id LEFT JOIN hr_document_master AS hdm ON hdm.user_id = um.user_id AND hdm.hr_document_category_id = '$hr_document_category_id' AND hdm.hr_document_sub_category_id = '$hr_document_sub_category_id' AND hdm.letter_id='$letter_id'","um.user_status = 1 AND um.active_status = 0 AND um.delete_status = 0 $append","GROUP BY um.user_id");
                                    if(mysqli_num_rows($q) > 0)
                                    {
                                        while ($row = $q->fetch_assoc())
                                        {
                                            extract($row);
                                    ?>
                                    <tr>
                                        <td class="text-center">
                                        <?php
                                            if(empty($hr_document_id))
                                            {
                                        ?>
                                            <input type="checkbox" id="user_id_check<?php echo $user_id; ?>" checked class="payCheckBox" value="<?php echo $user_id; ?>">
                                        <?php
                                            }
                                        ?>
                                        </td>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $user_full_name . " (" . $block_name . "-" . $floor_name . ")"; ?></td>
                                        <td>
                                        <?php
                                            if($hr_document_file != "" && file_exists("../img/hrdoc/".$hr_document_file))
                                            {
                                            ?>
                                            <span id="user_id_status<?php echo $user_id; ?>" class="badge badge-pill badge-success">Document Generated</span></td>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <span id="user_id_status<?php echo $user_id; ?>" class="badge badge-pill"></span></td>
                                            <?php
                                            }
                                        ?>
                                        <td id="user_id_link<?php echo $user_id; ?>" class="text-center">
                                        <?php
                                            if($hr_document_file != "" && file_exists("../img/hrdoc/".$hr_document_file))
                                            {
                                            ?>
                                            <a target="_blank" href="../img/hrdoc/<?php echo $hr_document_file; ?>" class="btn btn-sm btn-primary btn-round shadow-primary"><i class="fa fa-download"></i></a>
                                            <?php
                                            }
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                    <input type="hidden" name="hr_document_category_id" id="hr_document_category_id" value="<?php echo $hr_document_category_id; ?>"/>
                                    <input type="hidden" name="hr_document_sub_category_id" id="hr_document_sub_category_id" value="<?php echo $hr_document_sub_category_id; ?>"/>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
        <?php
        }
        else
        {
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <label class="text-danger">Please select branch</label>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/custom17.js"></script>
<script>
$('.selectall').on('change', function(e)
{
    var $inputs = $('#checkboxlist input[type=checkbox]');
    if(e.originalEvent === undefined)
    {
        var allChecked = true;
        $inputs.each(function(){
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    }
    else
    {
        $inputs.prop('checked', this.checked);
    }
});

$('#checkboxlist input[type=checkbox]').on('change', function()
{
    $('.selectall').trigger('change');
    if(this.checked)
    {
      $(this).attr("checked",'true');
    }
    else
    {
      $(this).removeAttr("checked");
    }
});


var elementArray;
function generateDocuments()
{
    $(".ajax-loader").show();
    elementArray = new Array();
    $(".payCheckBox:checked").each(function ()
    {
        elementArray.push($(this).val());
    });
    if(elementArray.length > 0)
    {
        doAjax(0);
    }
    else
    {
        $(".ajax-loader").hide();
        swal("Please select atleast one user", {icon: "error",});
    }
}

var hr_document_category_id = '<?php echo $hr_document_category_id; ?>';
var hr_document_sub_category_id = '<?php echo $hr_document_sub_category_id; ?>';
var letter_id = '<?php echo $letter_id; ?>';
var socieaty_logo = '<?php echo $socieaty_logo; ?>';
function doAjax(arrCount)
{
    var user_id = elementArray[arrCount];
    $.ajax({
        url: 'controller/letterController.php',
        data: {generateDocuments : "generateDocuments",user_id:user_id,hr_document_category_id:hr_document_category_id,hr_document_sub_category_id:hr_document_sub_category_id,letter_id:letter_id,socieaty_logo:socieaty_logo,csrf:csrf},
        type: "post",
        success: function(response)
        {
            response = JSON.parse(response);
            if(response.status == 1)
            {
                $('#user_id_check'+user_id).remove();
                $('#user_id_status'+user_id).text('Document Generated');
                $('#user_id_status'+user_id).addClass('badge-success');
                $('#user_id_link'+user_id).append('<a target="_blank" href="../img/hrdoc/'+response.file_name+'" class="btn btn-sm btn-primary btn-round shadow-primary"><i class="fa fa-download"></i></a>');
            }
            else if(response.status == 2)
            {
                $('#user_id_status'+user_id).text('Document already Generated');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 3)
            {
                $('#user_id_status'+user_id).text('Employee joining date not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 4)
            {
                $('#user_id_status'+user_id).text('Employee full name not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 5)
            {
                $('#user_id_status'+user_id).text('Employee first name not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 6)
            {
                $('#user_id_status'+user_id).text('Employee address not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 7)
            {
                $('#user_id_status'+user_id).text('Employee designation not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 8)
            {
                $('#user_id_status'+user_id).text('Employee mobile number not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 9)
            {
                $('#user_id_status'+user_id).text('Employee CTC not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 10)
            {
                $('#user_id_status'+user_id).text('Company address not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 11)
            {
                $('#user_id_status'+user_id).text('Company email not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else if(response.status == 12)
            {
                $('#user_id_status'+user_id).text('Company website not added');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
            else
            {
                $('#user_id_status'+user_id).text('Something went wrong');
                $('#user_id_status'+user_id).addClass('badge-danger');
            }
        },
        complete : function (response)
        {
            arrCount++;
            if (arrCount < elementArray.length)
            {
                setTimeout( function(){
                doAjax(arrCount);
                },100);
            }
            else
            {
                $(".ajax-loader").hide();
            }
        }
    });
}

function submitFilterForm()
{
    var letter_id = <?php echo $_GET['letter_id']; ?>;
    var letter_name = "<?php echo $_GET['letter_name']; ?>";
    var block_id = $('#bId').val();
    if($('#generateDocumentsFilterForm').valid())
    {
        window.location.href = "generateDocuments?letter_id="+letter_id+"&letter_name="+letter_name+"&bId="+block_id;
    }
    else
    {
        $('#generateDocumentsFilterForm').validate();
    }
}

$(document).ready(function()
{
    $("#sampleTable tbody tr").click(function(event)
    {
        var $checkbox = $(this).find("input");
        if (event.target.type != "checkbox")
        {
            $checkbox.prop("checked", !$checkbox.prop("checked"));
        }
    });
});
</script>