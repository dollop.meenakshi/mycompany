  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"><?php echo $xml->string->employees; ?> Salary</h4>
     </div>
     <div class="col-sm-3">
       
     </div>
     </div>
    <!-- End Breadcrumb-->
     

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      <th>#</th>
                      <!-- <th>#</th> -->
                        <th> Name</th>
                        <th> Type</th>
                        <th>Mobile</th>
                        <th>Salary</th>
                        <th>Joining Date</th>
                        <th>Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                     <?php 
                  $i=1;
                  $q=$d->select("employee_master","society_id='$society_id' AND emp_type=0","");
                  while($row=mysqli_fetch_array($q))
                  {

                    extract($row);
                    $EW=$d->select("emp_type_master","emp_type_id='$row[emp_type_id]'");
                     $empTypeData=mysqli_fetch_array($EW);
                  ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $emp_name; ?></td>
                        <td><?php if($emp_type_id==0) { echo 'Security Guard'; } else { echo $empTypeData['emp_type_name']; } ?></td>
                        <td><?php echo $country_code.' '.$emp_mobile;  ?></td>
                        <td><?php echo $emp_sallary; ?></td>
                        <td><?php echo $emp_date_of_joing; ?></td>
                        <td>
                        <form action="viewSalary" method="get">
                            <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                            <button class="btn btn-sm btn-primary" name="viewSallary" value="View">View</button>
                          </form>
                        </td>
                    </tr>
                    <?php }?>
                    
                </tbody>
                                
                </div>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->



