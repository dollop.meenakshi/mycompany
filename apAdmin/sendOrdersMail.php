<?php
error_reporting(0);
// ini_set('display_errors', '1');
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$con=$d->dbCon();
$base_url=$m->base_url();

date_default_timezone_set("Asia/Calcutta");
$soc = $d->selectRow("society_id","society_master");
$soc_data = $soc->fetch_assoc();
$society_id = $soc_data['society_id'];
$filter_data = date("Y-m-d");
$filter_data_view = date("d M Y");

$level_str = "";

$appendQuery = "";
$appendQuery2 = "";
$total_orders = "";
$total_sales = "";
$total_quantity = "";
$total_visits = "";
$total_visit_duration = "";
$productivity = "";
if(!isset($filter_date))
{
    $filter_date = date('Y-m-d');
}
$appendQuery = " AND DATE_FORMAT(rom.order_date,'%Y-%m-%d') = '$filter_date'";
$appendQuery2 = " AND DATE_FORMAT(retailer_visit_created_date,'%Y-%m-%d') = '$filter_date'";

$totalAmount = $d->sum_data("order_total_amount","retailer_order_master AS rom","rom.order_status = '1' AND rom.society_id = '$society_id' $appendQuery");
$totalDuration = $d->sum_data("retailer_visit_duration","retailer_daily_visit_timeline_master","1=1" . $appendQuery2);
$totalQuantity = $d->sum_data("total_order_product_qty","retailer_order_master AS rom","rom.order_status = 1 AND rom.society_id = '$society_id' $appendQuery");
$totalVisits = $d->count_data_direct("retailer_daily_visit_timeline_id","retailer_daily_visit_timeline_master","retailer_visit_start_datetime IS NOT NULL AND order_taken != 0 $appendQuery2");
$totalOrders = $d->count_data_direct("retailer_order_id","retailer_order_master AS rom","1=1".$appendQuery);

$total_orders = $totalOrders.'';
if ($totalOrders > 0 && $totalVisits > 0)
{
    $totalProductivity = ($totalOrders * 100) / $totalVisits;
}
else
{
    $totalProductivity = 0;
}

$totalAmountData = mysqli_fetch_array($totalAmount);
if ($totalAmountData['SUM(order_total_amount)'] != '')
{
    $total_sales = $totalAmountData['SUM(order_total_amount)'].'';
}
else
{
    $total_sales = "0";
}

$totalQuantityData = mysqli_fetch_array($totalQuantity);
if ($totalQuantityData['SUM(total_order_product_qty)'] != '')
{
    $total_quantity = $totalQuantityData['SUM(total_order_product_qty)'].'';
}
else
{
    $total_quantity = "0";
}

$totalDurationData = mysqli_fetch_array($totalDuration);
$totalSecounds = $totalDurationData['SUM(retailer_visit_duration)'].'';
if($totalSecounds == "" || empty($totalSecounds))
{
    $totalVisitDuration = gmdate("H:i:s", 0);
}
else
{
    $totalVisitDuration = gmdate("H:i:s", $totalSecounds);
}
$total_visits = $totalVisits.'';
$total_visit_duration = $totalVisitDuration.'';
$productivity = number_format($totalProductivity,2,'.','').'';

$dt = date("Y-m-d");
$fd = date("Y-m-01", strtotime($dt));
$ld = date("Y-m-t", strtotime($dt));
$total_days = date('t');
$total_days_arr = [];
$total_days_str = "";
$total_amount_mon_arr = [];
$total_amount_mon_str = "";
$total_qty_mon_arr = [];
$total_qty_mon_str = "";
$mwc = [];
$get_mcq = $d->selectRow("DATE_FORMAT(rom.order_date,'%e') AS date,SUM(rom.order_total_amount) AS total_amount,SUM(rom.total_order_product_qty) AS total_qty","retailer_order_master AS rom","rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59' AND rom.order_status = 1","GROUP BY DATE(rom.order_date)");
while($mcd = $get_mcq->fetch_assoc())
{
    $mwc[$mcd['date']] = $mcd;
}
for($i = 1;$i <= $total_days;$i++ )
{
    $total_days_arr[] = $i;
    if(!isset($mwc[$i]))
    {
        $mwc[$i]['date'] = $i;
        $mwc[$i]['total_amount'] = 0;
        $mwc[$i]['total_qty'] = 0;
        $total_amount_mon_arr[] = 0;
        $total_qty_mon_arr[] = 0;
    }
    else
    {
        $total_amount_mon_arr[] = $mwc[$i]['total_amount'];
        $total_qty_mon_arr[] = $mwc[$i]['total_qty'];
    }
}
$total_days_str = "'" .  implode("','",$total_days_arr) . "'";
$total_amount_mon_str = implode(",",$total_amount_mon_arr);
$total_qty_mon_str = implode(",",$total_qty_mon_arr);

$first_date_year = date("Y")."-01-01 00:00:01";
$last_date_year = date("Y")."-12-31 23:59:59";
$total_amount_year_arr = [];
$total_qty_year_arr = [];
$total_amount_year_str = "";
$total_qty_year_str = "";
$ywc = [];
$append3 = "";

$get_mcq = $d->selectRow("DATE_FORMAT(rom.order_date,'%c') AS month,SUM(rom.order_total_amount) AS total_amount,SUM(rom.total_order_product_qty) AS total_qty","retailer_order_master AS rom","rom.order_date BETWEEN '$first_date_year' AND '$last_date_year' AND rom.order_status = 1","GROUP BY MONTH(rom.order_date)");
while($ycd = $get_mcq->fetch_assoc())
{
    $ywc[$ycd['month']] = $ycd;
}
for($i = 1;$i <= 12;$i++)
{
    if(!isset($ywc[$i]))
    {
        $ywc[$i]['month'] = $i;
        $ywc[$i]['total_amount'] = 0;
        $ywc[$i]['total_qty'] = 0;
        $total_amount_year_arr[] = 0;
        $total_qty_year_arr[] = 0;
    }
    else
    {
        $total_amount_year_arr[] = $ywc[$i]['total_amount'];
        $total_qty_year_arr[] = $ywc[$i]['total_qty'];
    }
}
$total_amount_year_str = implode(",",$total_amount_year_arr);
$total_qty_year_str = implode(",",$total_qty_year_arr);

$today_date = date("Y-m-d");
$dt_min = new DateTime("last saturday");
$dt_min->modify('+2 day');
$dt_max = clone($dt_min);
$dt_max->modify('+6 days');
$start_date = $dt_min->format('Y-m-d');
$end_date = $dt_max->format('Y-m-d');
$osf = $d->selectRow("um.user_id,um.user_full_name,SUM(Case When DATE(rom.order_date) = '$today_date' Then rom.order_total_amount Else 0 End) AS today_total_amount,SUM(Case When DATE(rom.order_date) = '$today_date' Then rom.total_order_product_qty Else 0 End) AS today_total_qtyt1,SUM(Case When rom.order_date BETWEEN '$start_date 00:00:01' AND '$end_date 23:59:59' Then rom.order_total_amount Else 0 End) AS week_total_amount , SUM(Case When rom.order_date BETWEEN '$start_date 00:00:01' AND '$end_date 23:59:59' Then rom.total_order_product_qty Else 0 End) AS week_total_qtyt1,SUM(Case When rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59' Then rom.order_total_amount Else 0 End) AS month_total_amount,
    SUM(Case When rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59' Then rom.total_order_product_qty Else 0 End) AS month_total_qtyt1
    ,COUNT(if(DATE_FORMAT(rom.order_date,'%Y-%m-%d') = '$today_date', rom.retailer_order_id, NULL)) AS today_total_orders,COUNT(if(rom.order_date BETWEEN '$start_date 00:00:01' AND '$end_date 23:59:59', rom.retailer_order_id, NULL)) AS week_total_orders,COUNT(if(rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59', rom.retailer_order_id, NULL)) AS month_total_orders","retailer_order_master AS rom JOIN users_master AS um ON rom.order_by_user_id = um.user_id","rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59'","GROUP BY rom.order_by_user_id ORDER BY today_total_amount DESC");
$osda = [];
$today_amount_sum = 0;
$today_order_sum = 0;
$today_order_quntiyt1 = 0;
$week_amount_sum = 0;
$week_order_sum = 0;
$week_order_quntiyt1 = 0;
$month_amount_sum = 0;
$month_order_sum = 0;
$month_order_quntiyt1 = 0;
$today_cp_sum = 0;
$week_cp_sum = 0;
$month_cp_sum = 0;
while($ord_d = $osf->fetch_assoc())
{
    $today_amount_sum += $ord_d['today_total_amount'];
    $week_amount_sum += $ord_d['week_total_amount'];
    $month_amount_sum += $ord_d['month_total_amount'];
    $today_order_sum += $ord_d['today_total_orders'];
    $today_order_quntiyt1 += $ord_d['today_total_qtyt1'];
    $week_order_sum += $ord_d['week_total_orders'];
    $week_order_quntiyt1 += $ord_d['week_total_qtyt1'];
    $month_order_sum += $ord_d['month_total_orders'];
    $month_order_quntiyt1 += $ord_d['month_total_qtyt1'];
    $user_id = $ord_d['user_id'];
    $cpq = $d->selectRow("COUNT(if(DATE_FORMAT(rdvtm.retailer_visit_created_date,'%Y-%m-%d') = '$today_date', rdvtm.retailer_daily_visit_timeline_id, NULL)) AS today_tv,COUNT(if(DATE_FORMAT(rdvtm.retailer_visit_created_date,'%Y-%m-%d') BETWEEN '$start_date' AND '$end_date', rdvtm.retailer_daily_visit_timeline_id, NULL)) AS week_tv,COUNT(if(DATE_FORMAT(rdvtm.retailer_visit_created_date,'%Y-%m-%d') BETWEEN '$fd' AND '$ld', rdvtm.retailer_daily_visit_timeline_id, NULL)) AS month_tv","retailer_daily_visit_timeline_master AS rdvtm","rdvtm.retailer_visit_start_datetime IS NOT NULL AND rdvtm.retailer_visit_by_id = '$user_id'");
    $fetch_pd = $cpq->fetch_assoc();

    // $ord_d['today_total_orders'] = $fetch_pd['today_total_orders'];
    // $ord_d['week_total_orders'] = $fetch_pd['week_total_orders'];
    // $ord_d['month_total_orders'] = $fetch_pd['month_total_orders'];

    // $today_order_sum += $fetch_pd['today_total_orders'];
    // $week_order_sum += $fetch_pd['week_total_orders'];
    // $month_order_sum += $fetch_pd['month_total_orders'];
    if($fetch_pd['today_tv'] > 0 && $ord_d['today_total_orders'] > 0)
    {
        $val = ($ord_d['today_total_orders'] * 100) / $fetch_pd['today_tv'];
        if($val > 100)
        {
            $val = 100;
        }
        $ord_d['today_cp'] = number_format((float)$val,2,'.','').'';
    }
    else
    {
        $ord_d['today_cp'] = "0.00";
    }
    if($fetch_pd['week_tv'] > 0 && $ord_d['week_total_orders'] > 0)
    {
        $val1 = ($ord_d['week_total_orders'] * 100) / $fetch_pd['week_tv'];
        if($val1 > 100)
        {
            $val1 = 100;
        }
        $ord_d['week_cp'] = number_format((float)$val1,2,'.','').'';
    }
    else
    {
        $ord_d['week_cp'] = "0.00";
    }
    if($fetch_pd['month_tv'] > 0 && $ord_d['month_total_orders'] > 0)
    {
        $val2 = ($ord_d['month_total_orders'] * 100) / $fetch_pd['month_tv'];
        if($val2 > 100)
        {
            $val2 = 100;
        }
        $ord_d['month_cp'] = number_format((float)$val2,2,'.','').'';
    }
    else
    {
        $ord_d['month_cp'] = "0.00";
    }
    $today_cp_sum += $ord_d['today_cp'];
    $week_cp_sum += $ord_d['week_cp'];
    $month_cp_sum += $ord_d['month_cp'];
    $osda[] = $ord_d;
}
$total_cnt = count($osda);
if ($total_cnt>0) {
    // code...
    $today_cp_sum = $today_cp_sum / $total_cnt;
    $today_cp_sum = number_format((float)$today_cp_sum, 2, '.', '');
    $week_cp_sum = $week_cp_sum / $total_cnt;
    $week_cp_sum = number_format((float)$week_cp_sum, 2, '.', '');
    $month_cp_sum = $month_cp_sum / $total_cnt;
    $month_cp_sum = number_format((float)$month_cp_sum, 2, '.', '');
}
$total = [
    'user_full_name' => 'Total',
    'today_total_amount' => ($today_amount_sum == 0) ? "0.00" : number_format((float)$today_amount_sum, 2, '.', ''),
    'today_total_orders' => $today_order_sum,
    'today_total_qtyt1' => $today_order_quntiyt1,
    'today_cp' => $today_cp_sum,
    'week_total_amount' => ($week_amount_sum == 0) ? "0.00" : number_format((float)$week_amount_sum, 2, '.', ''),
    'week_total_orders' => $week_order_sum,
    'week_total_qtyt1' => $week_order_quntiyt1,
    'week_cp' => $week_cp_sum,
    'month_total_amount' => ($month_amount_sum == 0) ? "0.00" : number_format((float)$month_amount_sum, 2, '.', ''),
    'month_total_orders' => $month_order_sum,
    'month_total_qtyt1' => $month_order_quntiyt1,
    'month_cp' => $month_cp_sum
];
array_push($osda,$total);

$get_product_c = $d->selectRow("pcm.category_name,SUM(Case When DATE(rom.order_date) = '$today_date' Then ropm.product_total_price Else 0 End) AS today_total_amount,SUM(Case When DATE(rom.order_date) = '$today_date' Then ropm.product_quantity Else 0 End) AS today_total_qty,SUM(Case When rom.order_date BETWEEN '$start_date 00:00:01' AND '$end_date 23:59:59' Then ropm.product_total_price Else 0 End) AS week_total_amount,SUM(Case When rom.order_date BETWEEN '$start_date 00:00:01' AND '$end_date 23:59:59' Then ropm.product_quantity Else 0 End) AS week_total_qty,SUM(Case When rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59' Then ropm.product_total_price Else 0 End) AS month_total_amount,SUM(Case When rom.order_date BETWEEN '$fd 00:00:01' AND '$ld 23:59:59' Then ropm.product_quantity Else 0 End) AS month_total_qty","retailer_order_product_master AS ropm JOIN retailer_order_master AS rom ON rom.retailer_order_id = ropm.retailer_order_id JOIN retailer_product_master AS rpm ON rpm.product_id = ropm.product_id RIGHT JOIN product_category_master AS pcm ON pcm.product_category_id = rpm.product_category_id","","GROUP BY pcm.product_category_id");
$pcda = [];
$category_value_month = [];
$category_name_month = [];
$category_value_month_str = "";
$category_name_month_str = "";
while($r = $get_product_c->fetch_assoc())
{
    $category_value_month[] = $r['month_total_amount'];
    $category_name_month[] = $r['category_name'] . " (" . $r['month_total_amount'] . ") ";
    $pcda[] = $r;
}
$category_value_month_str = implode(",",$category_value_month);
$category_name_month_str = "'" .  implode("','",$category_name_month) . "'";

$table3 = $d->selectRow("um.user_id,um.user_full_name,SUM(Case When DATE(rom.order_date) = '$today_date' Then rom.order_total_amount Else 0 End) AS today_total_amount, SUM(Case When DATE(rom.order_date) = '$today_date' Then rom.total_order_product_qty Else 0 End) AS today_total_qtyt3 ,COUNT(if(DATE(rom.order_date) = '$today_date', rom.retailer_order_id, NULL)) AS today_total_orders_master,COUNT(if(DATE_FORMAT(rom.order_date,'%Y-%m-%d') = '$today_date', rom.retailer_order_id, NULL)) AS today_total_orders","retailer_order_master AS rom JOIN users_master AS um ON rom.order_by_user_id = um.user_id","DATE(rom.order_date) = '$today_date'","GROUP BY rom.order_by_user_id ORDER BY today_total_amount DESC");
$td3_arr = [];
while($td3 = $table3->fetch_assoc())
{
    $user_id = $td3['user_id'];
    $cpq = $d->selectRow("COUNT(if(DATE_FORMAT(rdvtm.retailer_visit_created_date,'%Y-%m-%d') = '$today_date', rdvtm.retailer_id, NULL)) AS total_retailers,COUNT(if(DATE_FORMAT(rdvtm.retailer_visit_created_date,'%Y-%m-%d') = '$today_date', rdvtm.retailer_daily_visit_timeline_id, NULL)) AS today_tv,COUNT(if(DATE_FORMAT(rnom.no_order_created_date,'%Y-%m-%d') = '$today_date', rnom.retailer_no_order_id, NULL)) AS today_no_orders","retailer_daily_visit_timeline_master AS rdvtm LEFT JOIN retailer_no_order_master AS rnom ON rnom.retailer_visit_id = rdvtm.retailer_daily_visit_timeline_id","rdvtm.retailer_visit_start_datetime IS NOT NULL AND rdvtm.retailer_visit_by_id = '$user_id'");
    $tv_tab3 = $cpq->fetch_assoc();
    $td3['total_retailers'] = $tv_tab3['total_retailers'];
    $td3['today_tv'] = $tv_tab3['today_tv'];
    $td3['today_no_orders'] = $tv_tab3['today_no_orders'];
    // $td3['today_total_orders'] = $tv_tab3['today_total_orders'];
    if($tv_tab3['today_tv'] > 0 && $td3['today_total_orders'] > 0)
    {
        $cpu = $td3['today_total_orders'] * 100 / $tv_tab3['today_tv'];
        if($cpu > 100)
        {
            $cpu = 100;
        }
    }
    else
    {
        $cpu = 0;
    }
    $td3['user_call_produ'] = number_format((float)$cpu, 2, '.', '');
    $td3_arr[] = $td3;
}

$PPCO = array(
    'chart' => array(
        'type' => 'pie',
        'width' => 700
    ),
    'labels' => $category_name_month,
    'responsive' => [array(
        'breakpoint' => 480,
        'options' => array(
            'chart' => array(
                'width' => 200
            ),
            'legend' => array(
                'position' => 'bottom'
            )
        )
    )],
    'series' => $category_value_month,
    'title' => array(
        'text' => 'Monthly '.date("M-Y")
    )
);
$PPCO_j = json_encode($PPCO,JSON_NUMERIC_CHECK);
$img1_url = "https://quickchart.io/apex-charts/render?config=".$PPCO_j;


$MCO = array(
    'chart' => array(
        'height' => 350,
        'type' => 'bar',
        'width' => 750
    ),
    'dataLabels' => array(
        'enabled' => true,
        'offsetY' => 15,
        'style' => array(
            'colors' => array('black')
        )
    ),
    'fill' => array(
        'opacity' => 1
    ),
    'plotOptions' => array(
        'bar' => array(
            'dataLabels' => array(
                'orientation' => 'vertical',
                'position' => 'top'
            )
        )
    ),
    'series' => array(array(
        'data' => $total_amount_mon_arr,
        'name' => 'Order Amount'
    )),
    'stroke' => array(
        'colors' =>  array('transparent'),
        'show' => true,
        'width' => 2
    ),
    'xaxis' => array(
        'categories' => $total_days_arr,
    ),
    'yaxis' => array(
        'title' => array(
            'text' => 'Daily Sales '.date("M-Y")
        )
    )
);
$MCO_j = json_encode($MCO,JSON_NUMERIC_CHECK);
$img2_url = "https://quickchart.io/apex-charts/render?config=".$MCO_j;


$YCO = array(
    'chart' => array(
        'height' => 350,
        'type' => 'bar',
        'width' => 750
    ),
    'dataLabels' => array(
        'enabled' => true,
        'offsetY' => 15,
        'style' => array(
            'colors' => array('black')
        )
    ),
    'fill' => array(
        'opacity' => 1
    ),
    'plotOptions' => array(
        'bar' => array(
            'dataLabels' => array(
                'orientation' => 'vertical',
                'position' => 'top'
            )
        )
    ),
    'series' => array(array(
        'data' => $total_amount_year_arr,
        'name' => 'Order Amount'
    )),
    'stroke' => array(
        'colors' =>  array('transparent'),
        'show' => true,
        'width' => 2
    ),
    'xaxis' => array(
        'categories' => array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'),
    ),
    'yaxis' => array(
        'title' => array(
            'text' => 'Monthly Sales '.date("Y")
        )
    )
);
$YCO_j = json_encode($YCO,JSON_NUMERIC_CHECK);
$img3_url = "https://quickchart.io/apex-charts/render?config=".$YCO_j;


$filter_data_view = date("d M Y");
$this_month = date("F-Y");
$weekView = $start_date.' to '.$end_date;

$get_soc = $d->selectRow("sm.socieaty_logo","society_master AS sm");
$soc_logo = "";
if(mysqli_num_rows($get_soc))
{
    $fetch_cl = $get_soc->fetch_assoc();
    $soc_logo = $fetch_cl['socieaty_logo'];
}
$get_user = $d->selectRow("um.user_email","users_master AS um","um.active_status = 0 AND um.delete_status = 0 AND um.receive_daily_sales_summary_mail = 0 AND um.user_email IS NOT NULL AND um.user_email != ''");
$to = [];
if(mysqli_num_rows($get_user) > 0)
{
    while($uData = $get_user->fetch_assoc())
    {
        $to[] = $uData['user_email'];
        // $to = array("shailesh.valand@nkproteins.com", "alpeshpatel@nkproteins.com");
    }
    $bcc = "";
    $subject = "Sales Summary Till " . date("Y-m-d H:i:s");
    include 'mail/orderDetailsTemplate.php';
    // $message;exit;
    $neww = include 'mail.php';
}

$txt = "Cron Run ".date("Y-m-d h:i A");
$myfile = file_put_contents('../img/cronlogs.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);

?>