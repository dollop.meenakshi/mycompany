<?php
error_reporting(0);
$currentYear = date('Y');
$currentMonth = date('m');
$nextYear = date('Y', strtotime('+1 year'));
$onePreviousYear = date('Y', strtotime('-1 year'));
$twoPreviousYear = date('Y', strtotime('-2 year'));
$bId = (int)$_REQUEST['bId'];
$dId = (int)$_REQUEST['dId'];
$uId = (int)$_REQUEST['uId'];
$ueMonth = $_REQUEST['ueMonth'];
$ueYear = $_REQUEST['ueYear'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-md-6">
                <h4 class="page-title">Manage Advance Expense</h4>
            </div>
            <div class="col-sm-6 col-md-6 col-6">
                <div class="btn-group float-sm-right">
                    <a href="addAdvanceExpense" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteGiveExpense');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <form class="branchDeptFilter" action="" >
            <div class="row pt-2 pb-2">
                <?php include 'selectBranchDeptEmpForFilterAll.php' ?>
                
                <div class="col-md-2 form-group">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php
                            $i = 1;
                             if(isset($bId) && $bId>0) {
                                $BranchFilterQuery = " AND users_master.block_id='$bId'";
                              }

                            if(isset($dId) && $dId > 0)
                            {
                                $deptFilterQuery = " AND user_expenses.floor_id='$dId'";
                            }
                            if(isset($uId) && $uId > 0)
                            {
                                $userFilterQuery = " AND user_expenses.user_id='$uId'";
                            }
                            $q = $d->select("user_expenses JOIN floors_master ON user_expenses.floor_id=floors_master.floor_id JOIN users_master ON users_master.user_id = user_expenses.user_id","user_expenses.society_id='$society_id' AND users_master.delete_status = 0 AND user_expenses.expense_type = 1 $deptFilterQuery $BranchFilterQuery $blockAppendQueryUser $userFilterQuery", "ORDER BY user_expenses.date DESC");
                            $counter = 1;
                            
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <th>Sr.No</th>
                                        <th>Action</th>
                                        <th>Employee</th>
                                        <th>Department</th>
                                        <th>Title</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php while ($data = mysqli_fetch_array($q)) { ?>
                                    <tr>
                                        <td class="text-center">
                                            <input type="hidden" name="id" id="id"  value="<?php echo $data['user_expense_id']; ?>">                  
                                            <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['user_expense_id']; ?>">
                                        </td>
                                        <td><?php echo $counter++; ?></td>
                                        <td>
                                            <?php if($data['expense_approved_by_id'] == $bms_admin_id){ ?>
                                            <form action="giveExpense" method="post">
                                                <input type="hidden" name="user_expense_id" value="<?php echo $data['user_expense_id']; ?>">
                                                <input type="hidden" name="bId" value="<?php echo $_GET['bId']; ?>"/>
                                                <input type="hidden" name="dId" value="<?php echo $_GET['dId']; ?>"/>
                                                <input type="hidden" name="uId" value="<?php echo $_GET['uId']; ?>"/>
                                                <input type="hidden" name="getReport" value="<?php echo $_GET['getReport']; ?>"/>
                                                <input type="hidden" name="f" value="<?php echo $_GET['f']; ?>"/>
                                                <input type="hidden" name="csrf" value="<?php echo $_GET['csrf']; ?>"/>
                                                <input type="hidden" name="edit_give_expense" value="edit_give_expense"/>
                                                <button type="submit" title="Edit Expense" class="btn btn-sm btn-warning mr-1 pd-1"><i class="fa fa-pencil"></i></button>
                                            </form>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $data['user_full_name']; ?><br>
                                            <span>(<?php echo $data['user_designation']; ?>)</span>
                                        </td>
                                        <td><?php echo $data['floor_name']; ?> </td>
                                        <td><?php echo $data['expense_title']; ?></td>
                                        <td><?php echo trim($data['amount'], "-"); ?></td>
                                        
                                        <td><?php echo date("d M Y", strtotime($data['date'])); ?></td>
                                        
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<div class="modal fade" id="employeeExpensesModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Employee Expenses</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="employeeExpensesData" style="align-content: center;">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expenesDeclineReason">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Declined Reason</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <div class="card-body" >
                    <div class="row " >
                        <div class="col-md-12">
                            <form id="expenesStatusReject" action="controller/statusController.php" name="changeAttendance"  enctype="multipart/form-data" method="post">
                                <div class="form-group row">
                                    <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                                    <div class="col-lg-12 col-md-12" id="">
                                        <textarea required maxlength="250" class="form-control" name="expense_reject_reason"></textarea>
                                    </div>
                                </div>
                                <div class="form-footer text-center">
                                    <input type="hidden" name="status"  value="declinedEmployeeExpenses">
                                    <input type="hidden" name="user_expense_id"  class="user_expense_id">
                                    <input type="hidden" name="bId" value="<?php echo $bId; ?>">
                                    <input type="hidden" name="dId" value="<?php echo $dId; ?>">
                                    <input type="hidden" name="ueMonth" value="<?php echo $ueMonth; ?>">
                                    <input type="hidden" name="ueYear" value="<?php echo $ueYear; ?>">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expenesDeclineReasonNew">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Declined Reason</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <div class="card-body" >
                    <div class="row " >
                        <div class="col-md-12">
                            <form id="expenesStatusRejectNew" action="controller/ExpenseSettingController.php" name="changeAttendance" enctype="multipart/form-data" method="post">
                                <div class="form-group row">
                                    <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                                    <div class="col-lg-12 col-md-12" id="">
                                        <textarea required maxlength="250" class="form-control" name="expense_reject_reason_new"></textarea>
                                    </div>
                                </div>
                                <div class="form-footer text-center">
                                    <input type="hidden" name="rejectExpenses" value="rejectExpenses">
                                    <input type="hidden" name="bId" value="<?php echo $bId; ?>">
                                    <input type="hidden" name="dId" value="<?php echo $dId; ?>">
                                    <input type="hidden" name="ueMonth" value="<?php echo $ueMonth; ?>">
                                    <input type="hidden" name="ueYear" value="<?php echo $ueYear; ?>">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expensePayModal">
    <div class="modal-dialog ">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Expense Pay Detail</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="billPayDiv" style="align-content: center;">
                <div class="card-body">
                    <form id="payExpenseAmountForm" action="controller/ExpenseSettingController.php" enctype="multipart/form-data" method="post">
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Expense Payment Mode <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" class="form-control" placeholder="Expense Payment Mode" id="expense_payment_mode" name="expense_payment_mode" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Reference Number <span class="required">*</span></label>
                            <div class="col-lg-8 col-md-8" id="">
                                <input type="text" class="form-control" placeholder="Reference Number" id="reference_no" name="reference_no" value="">
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" id="user_expense_id" name="user_expense_id" value="" >
                            <input type="hidden" name="payExpenseAmount"  value="payExpenseAmount">
                            <button id="payExpenseAmountBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                            <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('payExpenseAmountForm');"><i class="fa fa-check-square-o"></i> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>