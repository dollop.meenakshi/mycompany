<?php error_reporting(0);
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-9 col-6">
          <h4 class="page-title">Earning Deduction Type</h4>
        </div>
        <div class="col-sm-3 col-6 text-right">
        <a href="addSiteBtn" data-toggle="modal" data-target="#EarningDeductionModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
        <a href="javascript:void(0)" onclick="deleteErnDedct('deleteEarnDeduction');" class="btn  btn-sm btn-danger pull-right mr-1" ><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
      </div>
    

      <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table  class="table table-bordered example">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Earning Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="showFilterDatas">
                   <?php 
                    $i=1;
                     
                      $q=$d->select("salary_earning_deduction_type_master","society_id='$society_id' AND earning_deduction_type=0 AND earn_deduct_is_delete=0");
                        $counter = 1;
                      while ($data=mysqli_fetch_array($q)) {
                    ?>
                    <tr>
                      <td class="text-center">
                      <?php $totalShift = $d->count_data_direct("salary_earning_deduction_id","salary_common_value_master","salary_earning_deduction_id='$data[salary_earning_deduction_id]'");
                            if( $totalShift==0) {
                          ?>
                       
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['salary_earning_deduction_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['salary_earning_deduction_id']; ?>">                      
                        <?php } ?>
                        </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['earning_deduction_name']; ?></td>
                      <td>
                      <div class="d-flex align-items-center">
                        <form  method="post" accept-charset="utf-8">
                          <input type="hidden" name="salary_earning_deduction_id" value="<?php echo $data['salary_earning_deduction_id']; ?>">
                         
                          <button type="button" class="btn btn-sm btn-primary mr-1" onclick="EarningDeductionTypeData(<?php echo $data['salary_earning_deduction_id']; ?>)" data-toggle="modal" data-target="#addModal" > <i class="fa fa-pencil"></i></button> 
                        </form>
                          <?php if($data['salary_earning_deduction_status']=="0"){
                          ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['salary_earning_deduction_id']; ?>','EarningDeductionStatusDeactive');" data-size="small"/>
                          <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['salary_earning_deduction_id']; ?>','EarningDeductionStatusActive');" data-size="small"/>
                          <?php } ?>
                          <!-- <button type="button" class="btn btn-sm btn-primary ml-1" onclick="siteMasterShowDetails(<?php echo $data['site_id']; ?>)" data-toggle="modal" data-target="#siteDetailModel" ><i class="fa fa-eye"></i></button>  -->
                        </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
               
              <table class="table table-bordered examples">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th> Deduction Name</th>
                       
                        <!-- <th>Employee Name</th> -->
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody id="showFilterData">
                   <?php 
                    $i=1;
                                           
                      
                     
                      $q=$d->select("salary_earning_deduction_type_master","society_id='$society_id' AND earning_deduction_type=1 AND earn_deduct_is_delete=0");
                        $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>

                    <tr>
                        
                      <td class="text-center">
                      <?php $totalShift = $d->count_data_direct("salary_earning_deduction_id","salary_common_value_master","salary_earning_deduction_id='$data[salary_earning_deduction_id]'");
                            if( $totalShift==0) {
                          ?>
                         <input type="hidden" name="id" id="ids"  value="<?php echo $data['salary_earning_deduction_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['salary_earning_deduction_id']; ?>">                      
                        <?php } ?>
                        </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['earning_deduction_name']; ?></td>
                     <td>
                      <div class="d-flex align-items-center">
                        <form  method="post" accept-charset="utf-8">
                          <input type="hidden" name="salary_earning_deduction_id" value="<?php echo $data['salary_earning_deduction_id']; ?>">
                          <input type="hidden" name="edit_hr_doc" value="edit_hr_doc">
                         
                          <button type="button" class="btn btn-sm btn-primary mr-1" onclick="EarningDeductionTypeData(<?php echo $data['salary_earning_deduction_id']; ?>)" data-toggle="modal" data-target="#addModal" > <i class="fa fa-pencil"></i></button> 
                        </form>

                        
                          <?php if($data['salary_earning_deduction_status']=="0"){
                          ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['salary_earning_deduction_id']; ?>','EarningDeductionStatusDeactive');" data-size="small"/>
                          <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['salary_earning_deduction_id']; ?>','EarningDeductionStatusActive');" data-size="small"/>
                          <?php } ?>
                          <!-- <button type="button" class="btn btn-sm btn-primary ml-1" onclick="siteMasterShowDetails(<?php echo $data['site_id']; ?>)" data-toggle="modal" data-target="#siteDetailModel" ><i class="fa fa-eye"></i></button>  -->
                        </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->






<div class="modal fade" id="EarningDeductionModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white"> Earning Deduction Type </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           <form id="earningDeductionForm" action="controller/earningDeductionControlller.php" enctype="multipart/form-data" method="post">
           
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">Earning Deduction name <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
               <input type="text"  required="" name="earning_deduction_name" id="earning_deduction_name" class="form-control">
               </div>                   
           </div> 

           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">Earning Deduction Type <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="" >
                <select name="earning_deduction_type" id="earning_deduction_type" class="form-control single-select">
                            <option value="">Select Type</option>
                            <option value="0">Earning</option>
                            <option value="1">Deduction</option>
                </select>   
                </div>                   
           </div> 
           
                             
           <div class="form-footer text-center">
           <input type="hidden" name="addEarnDeductType" value="addEarnDeductType">

             <input type="hidden" id="earning_deduction_id" name="earning_deduction_id" value="" >
             <button id="updateEarnDeductType" name="addSiteBtn" type="submit"  class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
             
             <button name="addEarnDeductType" id="addEarnDeductType" type="submit"  class="btn btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
           
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addSiteForm');"><i class="fa fa-check-square-o"></i> Reset</button>
            
           </div>

         </form>
      
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
.hideupdate{
  display:none;
}

</style>