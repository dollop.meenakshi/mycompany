<?php 
session_start();	
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
include_once 'fcm_file/admin_fcm.php';
include_once 'fcm_file/gaurd_fcm.php';
include_once 'fcm_file/resident_fcm.php';
$d = new dao();
$m = new model();
$nAdmin = new firebase_admin();
$nResident = new firebase_resident();
$nGaurd = new firebase_gaurd();
extract($_REQUEST);
 $ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://master.my-company.app/commonApi/questionsController.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "getHouseQuestion=getHouseQuestion");


curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);

curl_close ($ch);

$server_output=json_decode($server_output,true);

 ?>
<form enctype="multipart/form-data" id="addHousieGame" action="addHousieGame" method="get">

    <input type="hidden" name="id" value="<?php echo $room_id;?>">
    <input type="hidden" name="que" value="<?php echo $que;?>">
    <input type="hidden" name="importFromServer" value="<?php echo 'importFromServer';?>">
     <div class="form-group row">
        <label for="input-<?php echo $a; ?>" class="col-sm-4 col-form-label">Topic <span class="text-danger">*</span></label>
        <div class="col-sm-8">
          <select required="" class="form-control" name="game_id">
            <option value="">-- Select --</option>
          <?php for ($i=0; $i <count($server_output['game']) ; $i++) { 
            ?>
          <option value="<?php echo $server_output['game'][$i]['housie_game_id']; ?>"><?php echo $server_output['game'][$i]['game_topic']; ?> (<?php echo count($server_output['game'][$i]['question']);?> Questions )</option>
          <?php } ?>
          </select>
        </div>

       
     </div>
     
    
      <div class="form-footer text-center">
                 
                <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-check-square-o"></i> Next</button>
        </div>

</form>