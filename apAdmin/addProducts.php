<style>
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

</style>


<?php
extract(array_map("test_input", $_POST));
if (isset($edit_product)) {
    $q = $d->select("product_master","product_id='$product_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Product</h4>
     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addProductFrom" action="controller/ProductController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <!-- <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Product Code </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" maxlength="100" class="form-control" placeholder="Product Code" name="product_code" value="<?php if($data['product_code'] !=""){ echo $data['product_code']; } ?>">
                                </div>
                            </div> 
                        </div> -->
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Product Name <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" maxlength="100" class="form-control" placeholder="Product Name" name="product_name" value="<?php if($data['product_name'] !=""){ echo $data['product_name']; } ?>">
                                </div>
                            </div> 
                        </div>
                       <!--  <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Alias Name </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" maxlength="100"  class="form-control" placeholder="Alias Name"  name="product_alias_name" value="<?php if($data['product_alias_name'] !=""){ echo $data['product_alias_name']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Short Name </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" maxlength="100" class="form-control" placeholder="Short Name" name="product_short_name" value="<?php if($data['product_short_name'] !=""){ echo $data['product_short_name']; } ?>">
                                </div>
                            </div> 
                        </div> -->
                       
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12"> Category <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text" required="" class="form-control single-select" name="product_category_id" id="product_category_id" onchange="getSubCategoryByIdInAddProduct(this.value);">
                                        <option value="">-- Select --</option> 
                                        <?php 
                                           $floor=$d->select("product_category_master","society_id='$society_id' AND product_category_status=0 AND product_category_delete=0 ");  
                                            while ($cData=mysqli_fetch_array($floor)) {
                                                print_r($cData);
                                                $totalVariant = $d->count_data_direct("product_category_vendor_id","product_category_vendor_master","product_category_id='$cData[product_category_id]'");
                                                //if( $totalVariant!=0) {    
                                        ?>
                                        <option <?php if(isset($data['product_category_id']) && $data['product_category_id'] ==$cData['product_category_id']){ echo "selected"; } ?> value="<?php if(isset($cData['product_category_id']) && $cData['product_category_id'] !=""){ echo $cData['product_category_id']; } ?>"><?php if(isset($cData['category_name']) && $cData['category_name'] !=""){ echo $cData['category_name']; } ?></option> 
                                        <?php }
                                       // } ?>
                                    </select>                   
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Sub Category </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text"     class="form-control single-select" id="product_sub_category_id" name="product_sub_category_id">
                                        <option value="">-- Select --</option> 
                                        <?php 
                                        if(isset($data)&& $data['product_category_id']!=""){
                                            $dq = $d->selectRow('product_sub_category_master.*',"product_sub_category_master","product_sub_category_master.product_sub_category_status=0 AND product_sub_category_master.product_sub_category_delete=0 AND product_sub_category_master.product_category_id = '$data[product_category_id]'");
                                            while($SubCatdata = mysqli_fetch_array($dq)){
                                                if($SubCatdata){?>
                                                    <option <?php if(isset($data['product_sub_category_id']) && $data['product_sub_category_id']==$SubCatdata['product_sub_category_id']){ echo "selected"; } ?> value="<?php echo $SubCatdata['product_sub_category_id']; ?>"><?php echo $SubCatdata['sub_category_name']; ?></option>
                                            <?php }
                                            }
                                        }?>
                                    </select>                   
                                </div>
                            </div> 
                        </div>
                        <!-- <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Vendor <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text"class="form-control single-select frmInputSl" onchange="getCateByVendorId(this.value)" id="vendor_id" name="vendor_id">
                                        <option value="">-- Select --</option> 
                                        <?php 
                                            $floor=$d->select("local_service_provider_users"," service_provider_status=0 AND service_provider_delete_status=0 AND society_id = $society_id ");  
                                            while ($floorData=mysqli_fetch_array($floor)) {
                                        ?>
                                        <option <?php if(isset($data['vendor_id']) && $data['vendor_id'] ==$floorData['service_provider_users_id']){ echo "selected"; } ?> value="<?php if(isset($floorData['service_provider_users_id']) && $floorData['service_provider_users_id'] !=""){ echo $floorData['service_provider_users_id']; } ?>"><?php if(isset($floorData['service_provider_name']) && $floorData['service_provider_name'] !=""){ echo $floorData['service_provider_name'] ; } ?></option> 
                                        <?php } ?>
                                    </select>                 
                                    </div>
                            </div> 
                        </div> -->
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Brand</label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" maxlength="100" class="form-control" placeholder="Brand" name="product_brand" value="<?php if($data['product_brand'] !=""){ echo $data['product_brand']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <!-- <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Minimum Order Quantity<span class="required">*</span> </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" maxlength="10" min="0" class="form-control onlyNumber" placeholder="Minimum Order Quantity" name="minimum_order_quantity" value="<?php if($data['minimum_order_quantity'] !=""){ echo $data['minimum_order_quantity']; } ?>">
                                </div>
                            </div> 
                        </div> -->
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Unit of Measure <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text" required="" class="form-control single-select" name="unit_measurement_id">
                                        <option value="">-- Select --</option> 
                                        <?php 
                                            $floor=$d->select("unit_measurement_master","society_id='$society_id' AND unit_measurement_delete=0 AND unit_measurement_status =0 ");  
                                            while ($floorData=mysqli_fetch_array($floor)) {
                                        ?>
                                        <option <?php if(isset($data['unit_measurement_id']) && $data['unit_measurement_id'] ==$floorData['unit_measurement_id']){ echo "selected"; } ?> value="<?php if(isset($floorData['unit_measurement_id']) && $floorData['unit_measurement_id'] !=""){ echo $floorData['unit_measurement_id']; } ?>"><?php if(isset($floorData['unit_measurement_name']) && $floorData['unit_measurement_name'] !=""){ echo $floorData['unit_measurement_name']; } ?></option> 
                                        <?php } ?>
                                    </select>                   
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Product Image</label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="file" class="form-control" name="product_image" onchange="readURL(this);" value="">
                                    <input type="hidden" name="product_image_old" id="product_image_old" value="<?php if($data['product_image'] !=""){ echo $data['product_image']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Description</label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <textarea  maxlength="100" class="form-control" placeholder="Other Remark" name="other" value="<?php if($data['other'] !=""){ echo $data['other']; } ?>"><?php if($data['other'] !=""){ echo $data['other']; } ?></textarea>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4" id="blah">
                            <?php if (isset($data['product_image']) && $data['product_image'] != null && $data['product_image'] != '') { ?>
                                <img src="../img/product/<?php echo $data['product_image']; ?>" width="75%" height="50%">
                            <?php } ?>
                            </div>
                        </div>
                        <?php if(!isset($product_id) || $product_id==0){ ?>
                        <div class="col-md-6">
                            <input type="text" placeholder="Variant Name" name="product_variant_name[]" id="product_variant_name_0" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <span class="btn btn-sm btn-danger addMoreVariant" ><i class="fa fa-plus"></i></span>
                        </div>
                           <?php } ?>
                        <div class="addNewVariant col-md-12">

                        </div>
                           
                       
                    </div>    
                                
                    <div class="form-footer text-center">
                    <?php //IS_1019 addPenaltiesBtn 
                    if (isset($edit_product)) {                    
                    ?>
                    <input type="hidden" id="product_id" name="product_id" value="<?php if($data['product_id'] !=""){ echo $data['product_id']; } ?>" >
                    <button  type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addProduct"  value="addProduct">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addProduct');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php }
                    else {
                    ?>
                    <button id="addProductBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addProduct"  value="addProduct">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addProductFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
<?php  if (isset($edit_product)) { ?>
  //  getSubCategoryByIdInAddProduct(<?php echo $data['product_category_id']; ?>, <?php echo $data['product_sub_category_id']; ?>);

<?php } ?>



function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').html('<img src="'+e.target.result+'" width="75%" height="50%">');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

(function () {
  var count = 0;

  $('.addMoreVariant').click(function () {
    count += 1;
    addVarnt = "";
    addVarnt += `<div class="row mt-1 " style="margin-left:-4px;">
                    <div class="col-md-6 col-6 remvCls_`+count+`">
                  <input type="text" placeholder="Variant Name" id="product_variant_name_`+count+`" name="product_variant_name[]" class="form-control">
                </div>
                <div class="col-md-6 col-6 remvCls_`+count+`">
                  <span class="btn btn-sm btn-danger  " onclick="removeVariant(`+count+`)" data-id="`+count+`"><i class="fa fa-trash"></i></span>
                </div>
                <div/>`;
    $('.addNewVariant').append(addVarnt);
    addRule() 
  });
  
   
})();
addRule() 

function addRule() {
    $("[name^=product_variant_name]").each(function () {
        $('#'+this.id).rules("add", {
            required: true,
            messages: {
                    required: "Please Enter Variant Name ",
                }
        });
    });
}

  function removeVariant(id) {
    $('.remvCls_' + id).remove();  
  }
</script>