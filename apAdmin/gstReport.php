<?php 
error_reporting(0);
if (isset($_GET['from'])) {
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['from']) != 1 ||  preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_GET['toDate']) != 1){
    $_SESSION['msg1']="Invalid Report Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='gstReport';
        </script>");
  }
}
 ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-12">
        <h4 class="page-title">GST Payment Report</h4>
        </div>
     
     </div>

    <form action="" method="get">
     <div class="row pt-2 pb-2">
          <div class="col-lg-6 col-9">
            <div id="dateragne-pickerNew">
                <div class="input-daterange input-group">
                <input readonly="" type="text" class="form-control" autocomplete="off" required="" placeholder="Start Date" name="from" value="<?php echo $_GET['from']; ?>" />
                <div class="input-group-prepend">
                 <span class="input-group-text">to</span>
                </div>
                <input readonly="" type="text" class="form-control" autocomplete="off" required="" placeholder="End Date" name="toDate" value="<?php echo $_GET['toDate']; ?>" />
               </div>
              </div>
          </div>
          <div class="col-lg-2 col-3">
            <select name="type"  class="form-control">
              <option value="" >All</option>
              <option <?php if($_GET['type']==1) { echo 'selected';} ?>  value="1" >Events</option>
              <option <?php if($_GET['type']==2) { echo 'selected';} ?>  value="2" >Facilities</option>
              <option <?php if($_GET['type']==5) { echo 'selected';} ?> value="5" >Penalty</option>
              <option <?php if($_GET['type']==6) { echo 'selected';} ?>  value="6" >Other Income</option>
            </select>
          </div>
           
          <div class="col-lg-2 col-3">
            <label  class="form-control-label"> </label>
              <input  class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Report">
          </div>
          <div class="col-lg-2 col-6">
          

          </div>

     </div>
    </form>
    <!-- End Breadcrumb-->
     
      
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              
              <div class="table-responsive">
              <?php 
                extract(array_map("test_input" , $_GET));
                $from = date('Y-m-d', strtotime($from));
                $toDate = date('Y-m-d', strtotime($toDate));
                $date=date_create($from);
                $dateTo=date_create($toDate);
                $nFrom= date_format($date,"Y-m-d 00:00:00");
                $nTo= date_format($dateTo,"Y-m-d 23:59:59");

                switch ($type) {
                  case '1':
                   $q=$d->select("event_attend_list,event_master,users_master","users_master.user_id=event_attend_list.user_id AND event_attend_list.event_id=event_master.event_id AND  event_attend_list.society_id='$society_id' AND event_attend_list.book_status=1 AND event_attend_list.recived_amount>0 AND event_master.is_taxble='1' AND  event_attend_list.payment_received_date BETWEEN '$nFrom' AND '$nTo'","");
                    break;
                   case '2':
                   $q1=$d->select("facilitybooking_master,facilities_master","facilitybooking_master.book_status=1 AND facilitybooking_master.facility_id=facilities_master.facility_id AND facilitybooking_master.society_id='$society_id'  AND facilities_master.is_taxble='1' AND   facilitybooking_master.receive_amount>0 AND facilitybooking_master.payment_received_date BETWEEN '$nFrom' AND '$nTo'","");
                    break;
                  case '5':
                    $q4=$d->select("penalty_master,unit_master,users_master","penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id'  AND penalty_master.paid_status=1 AND penalty_master.is_taxble='1' AND   penalty_master.penalty_amount>0 AND penalty_master.penalty_receive_date BETWEEN '$nFrom' AND '$nTo'","");
                    break;
                  case '6':
                   $q5=$d->select("expenses_balance_sheet_master","society_id='$society_id' AND income_amount!=0 AND is_taxble='1' AND  income_amount>0 AND expenses_add_date BETWEEN '$nFrom' AND '$nTo'","");
                    break;
                  default:
                     // event data
                    $q=$d->select("event_attend_list,event_master,users_master","users_master.user_id=event_attend_list.user_id AND event_attend_list.event_id=event_master.event_id AND  event_attend_list.society_id='$society_id' AND event_attend_list.book_status=1 AND event_attend_list.recived_amount>0 AND event_master.is_taxble='1' AND  event_attend_list.payment_received_date BETWEEN '$nFrom' AND '$nTo'","");

                 

                   
                    // penalty
                     $q4=$d->select("penalty_master,unit_master,users_master","penalty_master.user_id=users_master.user_id AND unit_master.unit_id=penalty_master.unit_id AND penalty_master.society_id='$society_id'  AND penalty_master.paid_status=1 AND penalty_master.is_taxble='1' AND   penalty_master.penalty_amount>0 AND penalty_master.penalty_receive_date BETWEEN '$nFrom' AND '$nTo'","");

                     // income
                     $q5=$d->select("expenses_balance_sheet_master","society_id='$society_id' AND income_amount!=0 AND is_taxble='1' AND  income_amount>0 AND expenses_add_date BETWEEN '$nFrom' AND '$nTo'","");
                    break;
                }
                       
               

                  $i=1;
                if (isset($_GET['from'])) {
               ?>
                
              <table id="<?php if($adminData['report_download_access']==0) { echo 'exampleReportWithoutBtn'; } else { echo 'exampleReport'; }?>" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Type</th>
                        <th><?php echo $xml->string->payment_for; ?> </th>
                        <th>Received Date</th>
                        <th>Received Amount</th>
                        <th><?php echo $xml->string->tax; ?> Amount</th>
                        <th><?php echo $xml->string->tax_type; ?></th>
                        <th>Slab</th>
                        <th>Employee/Branch</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  while ($data=mysqli_fetch_array($q)) {
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo "Event"; ?></td>
                        <td><?php echo custom_echo($data['event_title'],70); ?></td>
                        <td><?php if($data['payment_received_date']!="") { echo date('Y-m-d h:i A', strtotime($data['payment_received_date'])); } ?></td>
                         <td>
                          <?php
                          $recived_amount=number_format($data['recived_amount']-$data['transaction_charges'],2,'.','');  
                           echo $recived_amount; ?>
                         </td>
                         <td>
                          <?php 
                          $tax_slab=$data['tax_slab'];
                          $gstValueAdult =  $recived_amount - ($recived_amount  * (100/(100+$data['tax_slab']))); 
                           
                          echo   number_format($gstValueAdult,2,'.','');
                          ?>
                         </td>
                         <td><?php 
                            if ($_COOKIE['country_id']==101) { 
                              if ($data['taxble_type']==0) {
                                echo $xml->string->cgst_sgst;
                              }  else {
                                echo $xml->string->igst;
                              }
                            } else if ($_COOKIE['country_id']==161) { 
                                echo $xml->string->igst;
                            }
                              ?></td>
                         <td><?php echo $data['tax_slab']; ?> %</td>
                         <td><?php
                          $bq = $d->selectRow("block_master.block_name,unit_master.unit_name","block_master,unit_master","unit_master.unit_id='$data[unit_id]' AND unit_master.block_id = block_master.block_id");
                          $bData = mysqli_fetch_array($bq);
                      echo $data['user_full_name'].'-'.$data['user_designation'] ." (".$bData['block_name'].')';?></td>
                    </tr>
                  <?php } 
                  while ($data44=mysqli_fetch_array($q4)) {
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo "Penalty"; ?></td>
                        <td><?php echo custom_echo($data44['penalty_name'],70); ?></td>
                        <td><?php if($data44['penalty_receive_date']!="") { echo date('Y-m-d h:i A', strtotime($data44['penalty_receive_date'])); } ?></td>
                         <td>
                          <?php echo $data44['penalty_amount']; ?>
                         </td>
                         <td>
                          <?php 
                          $tax_slab=$data44['tax_slab'];
                          $gstValueAdult =  $data44['penalty_amount'] - ($data44['penalty_amount']  * (100/(100+$data44['tax_slab']))); 
                          echo   number_format($gstValueAdult,2,'.','');
                          ?>
                         </td>
                         <td><?php 
                            if ($_COOKIE['country_id']==101) { 
                              if ($data44['taxble_type']==0) {
                                echo $xml->string->cgst_sgst;
                              }  else {
                                echo $xml->string->igst;
                              }
                            } else if ($_COOKIE['country_id']==161) { 
                                echo $xml->string->igst;
                            }
                         ?></td>
                         <td><?php echo $data44['tax_slab']; ?> %</td>
                         <td><?php
                          $bq = $d->selectRow("block_master.block_name,unit_master.unit_name","block_master,unit_master","unit_master.unit_id='$data44[unit_id]' AND unit_master.block_id = block_master.block_id");
                          $bData = mysqli_fetch_array($bq);
                      echo $data44['user_full_name'].'-'.$data44['user_designation'] ." (".$bData['block_name'].')';?></td>
                    </tr>
                   <?php } 
                  while ($data55=mysqli_fetch_array($q5)) {
                   ?>
                    <tr>
                       
                        <td><?php echo $i++; ?></td>
                        <td><?php echo "Income"; ?></td>
                        <td><?php echo custom_echo($data55['expenses_title'],70); ?></td>
                        <td><?php if($data55['expenses_add_date']!="") { echo date('Y-m-d h:i A', strtotime($data55['expenses_add_date'])); } ?></td>
                         <td>
                          <?php echo $data55['income_amount']; ?>
                         </td>
                         <td>
                          <?php 
                          $tax_slab=$data55['tax_slab'];
                          $gstValueAdult =  $data55['income_amount'] - ($data55['income_amount']  * (100/(100+$data55['tax_slab']))); 
                          echo   number_format($gstValueAdult,2,'.','');
                          ?>
                         </td>
                         <td><?php 
                            if ($_COOKIE['country_id']==101) { 
                              if ($data55['taxble_type']==0) {
                                echo $xml->string->cgst_sgst;
                              }  else {
                                echo $xml->string->igst;
                              }
                            } else if ($_COOKIE['country_id']==161) { 
                                echo $xml->string->igst;
                            }
                          ?></td>
                         <td><?php echo $data55['tax_slab']; ?> %</td>
                         <td>-</td>
                    </tr>
                  <?php } ?>
                </tbody>  
                
            </table>
            

            <?php } else {  ?>
              <div class="" role="alert">
                 <span><strong>Note :</strong> Please Select date</span>
                </div>
            <?php } ?>

            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->