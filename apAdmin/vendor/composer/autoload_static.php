<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd2d227b00fe7ac6d3f497a8ee046cf45
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd2d227b00fe7ac6d3f497a8ee046cf45::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd2d227b00fe7ac6d3f497a8ee046cf45::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
