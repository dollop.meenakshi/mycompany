<?php
session_start();
$society_id=$_COOKIE['society_id'];
extract($_POST);
include_once 'lib/dao.php';
include 'lib/model.php';
include 'common/checkLogin.php';
$d = new dao();
$m = new model();
$base_url=$m->base_url();
$master_url=$d->master_url();
$keydb = $m->api_key();

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);

$today = date("Y-m-d");

$q=$d->select("bms_admin_master","admin_id='$_COOKIE[bms_admin_id]'");
$data=mysqli_fetch_array($q);
extract($data);

$q=$d->select("society_master","society_id='$society_id'");
$bData=mysqli_fetch_array($q);
$package_id = $bData['package_id'];
$plan_expire_date = $bData['plan_expire_date'];

$qx=$d->selectRow("no_of_month,merchant_id,merchant_key","transection_master","order_id='$_SESSION[razorpay_order_id]'");
$oldTraData=mysqli_fetch_array($qx);
$no_of_month = $oldTraData['no_of_month'];
$merchant_id = $oldTraData['merchant_id'];
$merchant_key = $oldTraData['merchant_key'];


$keyId = $merchant_id;
$keySecret = $merchant_key;
$displayCurrency = 'INR';

require('razorpay-php/Razorpay.php');
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;

$success = true;

$error = "Payment Failed";

if (empty($_POST['razorpay_payment_id']) === false)
{
    $api = new Api($keyId, $keySecret);

    try
    {
        // Please note that the razorpay order ID must
        // come from a trusted source (session here, but
        // could be database or something else)
        $attributes = array(
            'razorpay_order_id' => $_SESSION['razorpay_order_id'],
            'razorpay_payment_id' => $_POST['razorpay_payment_id'],
            'razorpay_signature' => $_POST['razorpay_signature']
        );

        $api->utility->verifyPaymentSignature($attributes);
    }
    catch(SignatureVerificationError $e)
    {
        $success = false;
        $error = 'Razorpay Error : ' . $e->getMessage();
        $_SESSION['msg1']= "Razorpay Error ".$e->getMessage();
        header("location:buyPlan");
        exit();
    }
}

if ($success === true)
{
    // $html = "<p>Your payment was successful</p>
    //          <p>Payment ID: {$_POST['razorpay_payment_id']}</p>";


    $paymentAry = array(
        'payment_status' => 'successful',
        'payment_txnid' => $_POST['razorpay_payment_id'],
        // 'razorpay_signature' => $_POST['razorpay_signature'],
        'transection_date' => date("Y-m-d H:i:s"),
        'order_status' => 0,
    );

         
    $q3=$d->update("transection_master",$paymentAry,"order_id='$_SESSION[razorpay_order_id]'");

    // update plan exprie date
      
        $qx=$d->selectRow("no_of_month","transection_master","order_id='$_SESSION[razorpay_order_id]'");
        $oldTraData=mysqli_fetch_array($qx);
        $no_of_month = $oldTraData['no_of_month'];

        if ($today>$plan_expire_date) {
           $start_date =$today;
        } else {
           $start_date =$plan_expire_date;
        }
        
        $plan_expire_date = date('Y-m-d', strtotime("+".$no_of_month." months", strtotime($start_date)));
        $last_renew_date = $today;

        $m->set_data('package_id',$package_id);
        $m->set_data('plan_expire_date',$plan_expire_date);
        $m->set_data('last_renew_date',$last_renew_date);

        $a =array(
          'package_id'=>$m->get_data('package_id'),
          'plan_expire_date'=>$m->get_data('plan_expire_date'),
          'last_renew_date'=>$m->get_data('last_renew_date'),
        );
       $q=$d->update("society_master",$a,"society_id='$society_id'");

    
       $_SESSION['msg']= "Payment received successfully";
    header("location:buyPlan");
    exit();
}
else
{   
    $paymentAry = array(
        'payment_status' => 'failed',
        'payment_txnid' => $_POST['razorpay_payment_id'],
        // 'razorpay_signature' => $_POST['razorpay_signature'],
        'transection_date' => date("Y-m-d H:i:s"),
        'order_status' => 2,
    );
             
    $q3=$d->update("transection_master",$paymentAry,"order_id='$_SESSION[razorpay_order_id]'");
     $_SESSION['msg1']= "Your payment failed ({$error})";
     header("location:index.php");
    exit();
    // $html = "<p>Your payment failed</p>
    //          <p>{$error}</p>";
}

?>