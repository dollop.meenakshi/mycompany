<?php
if(isset($_POST['editCategory']) && isset($_POST['product_category_id']))
{
    $product_category_id = $_POST['product_category_id'];
    $q = $d->select("product_category_master","product_category_id = '$product_category_id'");
    $data = $q->fetch_assoc();
    extract($data);
    $form_id = "categoryUpdateForm";
}
else
{
    $form_id = "categoryAddForm";
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form id="<?php echo $form_id; ?>" action="controller/categorySubCategoryController.php" method="post" enctype="multipart/form-data">
                            <h4 class="form-header text-uppercase">
                            <?php
                            if(isset($_POST['editCategory']) && isset($_POST['product_category_id']))
                            {
                            ?>
                            <i class="fa fa-edit"></i>
                            Edit Category
                            <?php
                            }
                            else
                            {
                            ?>
                            <i class="fa fa-plus"></i>
                            Add Category
                            <?php
                            }
                            ?>
                            </h4>
                            <div class="form-group row">
                                <label for="category_name" class="col-sm-2 col-form-label">Category Name <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input value="<?php if(isset($editCategory)){ echo $category_name; } ?>" required type="text" class="form-control" name="category_name" id="category_name" maxlength="100">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category_description" class="col-sm-2 col-form-label">Category Description</label>
                                <div class="col-sm-10">
                                    <textarea id="category_description" name="category_description" class="form-control"><?php if(isset($editCategory) && $category_description != "") { echo $category_description; } ?></textarea>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="csrf" value="<?php echo $_SESSION['token']; ?>">
                            <div class="form-footer text-center">
                                <input type="hidden" name="addCategory" value="addCategory">
                                <?php
                                if(isset($editCategory))
                                {
                                ?>
                                <input type="hidden" class="form-control" name="product_category_id" id="product_category_id" value="<?php echo $product_category_id; ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <?php
                                }
                                else
                                {
                                ?>
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                                <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->