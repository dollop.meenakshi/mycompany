<?php
error_reporting(0);
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
$leave_group_id = $_POST['leave_group_id'];
$user_id = $_POST['user_id'];
$floor_id = $_POST['floor_id'];
$currentYear = $_POST['currentYear'];
$nextYear = $_POST['nextYear'];
$previousYear = $_POST['previousYear'];
$leave_year = $_POST['leave_year'];
$society_id = $_COOKIE['society_id'];

if ($leave_group_id != '' && $user_id != '' && $floor_id != '' && $currentYear != '' && $nextYear != '' && $previousYear != '') { ?>

    <div class="row">
        <div class="col-md-3">
            <label for="input-10" class="col-form-label">Leave Type</label>
        </div>

        <div class="col-md-3">
            <label for="input-10" class="col-form-label">Carry Forward From <?php echo $previousYear ?> to <?php echo $leave_year ?></label>
        </div>

        <div class="col-md-3">
            <label for="input-10" class="col-form-label">Paid Leave</label>
        </div>

        <div class="col-md-3">
            <label for="input-10" class="col-form-label">Maximum Paid Leave Use In a Single Month</label>
        </div>
    </div>
    <div class="row">
        <?php
        $i = 0;
        $q = $d->select("leave_default_count_master,leave_type_master", "leave_type_master.leave_type_id=leave_default_count_master.leave_type_id AND leave_type_master.society_id='$society_id' AND leave_type_master.leave_type_delete=0 AND leave_default_count_master.leave_group_id='$leave_group_id'");
        while ($data = mysqli_fetch_array($q)) {
            $userLeaveq = $d->selectRow('user_total_leave', "leave_assign_master", "user_id='$user_id' AND leave_type_id='$data[leave_type_id]' AND assign_leave_year= '$previousYear'");
            $userLeaveData = mysqli_fetch_assoc($userLeaveq);
            $approvedFullDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$user_id' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=0 AND DATE_FORMAT(leave_created_date,'%Y') = '$previousYear'");
            $approvedFullDayLeaveData = mysqli_fetch_assoc($approvedFullDayLeave);
            $approvedHalfDayLeave = $d->selectRow('COUNT(*) AS approved_leave', "leave_master", "user_id='$user_id' AND leave_type_id='$data[leave_type_id]' AND leave_status=1 AND paid_unpaid=0 AND leave_day_type=1 AND DATE_FORMAT(leave_created_date,'%Y') = '$previousYear'");
            $approvedHalfDayLeaveData = mysqli_fetch_assoc($approvedHalfDayLeave);
            $noOfHalfDayLeave = $approvedHalfDayLeaveData['approved_leave'] / 2;
            $payOutLeave = $d->selectRow('SUM(no_of_payout_leaves) AS no_of_payout_leaves', "leave_payout_master", "user_id='$user_id' AND leave_type_id='$data[leave_type_id]' AND leave_payout_year = '$previousYear'");
            $payOutLeaveData = mysqli_fetch_assoc($payOutLeave);
            $remaining_leave = $userLeaveData['user_total_leave'] - ($approvedFullDayLeaveData['approved_leave'] + $noOfHalfDayLeave) - $payOutLeaveData['no_of_payout_leaves'];
        ?>
            <div class="col-md-3">
                <div class="form-group row w-100 mx-0">
                    <?php
                    $ldcq = $d->select("leave_default_count_master", "leave_type_id='$data[leave_type_id]' AND leave_group_id='$leave_group_id'");
                    $ldcqData = mysqli_fetch_array($ldcq);
                    ?>
                    <input type="text" class="form-control" disabled value="<?php echo $data['leave_type_name']; ?>">
                    <input type="hidden" class="form-control" name="leave_type_id[<?php echo $i; ?>]" id="leave_type_id_<?php echo $i; ?>" value="<?php echo $data['leave_type_id']; ?>">
                    <input type="hidden" class="form-control" name="user_leave_id[<?php echo $i; ?>]" id="user_leave_id_<?php echo $i; ?>" value="<?php echo $data['user_leave_id']; ?>">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group row w-100 mx-0">
                    <?php $leaveRuleq = $d->select("leave_default_count_master", "leave_type_id='$data[leave_type_id]' AND leave_group_id='$leave_group_id' AND carry_forward_to_next_year=0");
                    $leaveRuleData = mysqli_fetch_array($leaveRuleq);
                    //print_r($remaining_leave);
                    if (mysqli_num_rows($leaveRuleq) > 0) {
                        if ($remaining_leave > 0) {
                    ?>
                            <?php if ($remaining_leave >= $leaveRuleData['min_carry_forward']) { ?>
                                <select name="carry_forward_leaves[<?php echo $i; ?>]" id="carry_forward_leaves_<?php echo $i; ?>" class="form-control">
                                    <option value="">--Select Leave--</option>
                                    <?php
                                    $j = $leaveRuleData['max_carry_forward'];
                                    if ($leaveRuleData['max_carry_forward'] >= $remaining_leave) {
                                        $j = $remaining_leave;
                                    }
                                    for ($ai = 1; $ai <= $j; $ai++) { ?>
                                        <option value="<?php echo $ai; ?>"><?php echo $ai; ?></option>
                                    <?php } ?>
                                </select>
                            <?php } else { ?>
                                <span class="required small">Remaining Leave (<?php echo $remaining_leave; ?>) is less then minimum carry forward leave count (<?php echo $leaveRuleData['min_carry_forward']; ?>)</span>
                            <?php } ?>
                        <?php } else { ?>
                            <span class="required small">No Remaining Leave available in <?php echo $previousYear ?> for <?php echo $data['leave_type_name']; ?></span>
                        <?php } ?>
                    <?php } else { ?>
                        <span class="required small">No Carry Forward Leave found in <?php echo $previousYear ?> </span>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group row w-100 mx-0">
                    <input step="0.5" min="0" type="text" class="form-control user_total_leave onlyNumber" name="user_total_leave[<?php echo $i; ?>]" id="user_total_leave_<?php echo $i; ?>" max="365" value="<?php echo $data['number_of_leaves']; ?>" placeholder="User Total Leave" onkeyup="setUseInMonthLeaves(<?php echo $i; ?>)">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group row w-100 mx-0">
                    <select name="applicable_leaves_in_month[<?php echo $i; ?>]" id="applicable_leaves_in_month_<?php echo $i; ?>" class="form-control single-select">
                        <?php $x = 1;
                        for ($a = ($x - 0.5); $a <= $data['number_of_leaves']; $a += 0.5) { ?>
                            <option <?php if ($data['leaves_use_in_month'] == $a) {
                                        echo 'selected';
                                    } ?> value="<?php echo $a; ?>"><?php echo $a; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        <?php $i++;
        } ?>

    </div>
<?php } ?>

<script type="text/javascript" src="assets/js/select.js"></script>