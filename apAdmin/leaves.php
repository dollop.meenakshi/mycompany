  <?php error_reporting(0);
  $currentYear = date('Y');

  $bId = (int)$_REQUEST['bId'];
  $dId = (int)$_REQUEST['dId'];
  $uId = (int)$_REQUEST['uId'];
  $year = (int)$_REQUEST['year'];
  $sortby = (int)$_REQUEST['sortby'];
  $month = (int)$_REQUEST['month'];
  $key = $_REQUEST['key'];
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-3">
          <h4 class="page-title">Leave Request</h4>
        </div>
        <div class="col-sm-3 col-md-6">
          <form class="" method="get" action="">
            <label>Sort By : </label>
            <input type="hidden" name="bId" value="<?php echo $bId; ?>">
            <input type="hidden" name="dId" value="<?php echo $dId; ?>">
            <input type="hidden" name="uId" value="<?php echo $uId; ?>">
            <input type="hidden" name="year" value="<?php echo $year; ?>">
            <select name="sortby" onchange="this.form.submit()">
              <option <?php if($sortby==0) { echo 'selected'; } ?> value="0">Applied Date (DESC)</option>
              <option <?php if($sortby==1) { echo 'selected'; } ?> value="1">Leave Date (DESC)</option>
            </select>
          </form>
        </div>
        <div class="col-sm-3 col-md-3 col-3">
       <div class="btn-group float-sm-right">
        <a href="addLeave" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
        <?php if($_COOKIE['role_name']=="Super Admin") {?>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteLeaves');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
         <?php } ?>
      </div>
     </div>
     </div>
     <form class="branchDeptFilter" action="" method="get">
        <div class="row pt-2 pb-2">
          <?php include 'selectBranchDeptEmpForFilterAll.php' ?>
          <div class="col-md-2 col-6 form-group">
            <input type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerFrom"  name="from" value="<?php if(isset($_GET['from']) && $_GET['from'] != ''){ echo $from = $_GET['from'];}else{echo $from=  date('Y-m-d', strtotime('-60 days'));} ?>">  
          </div>
          <div class="col-md-2 col-6 form-group">
            <input  type="text" required=""  class="form-control" autocomplete="off" id="autoclose-datepickerTo"  name="toDate" value="<?php if(isset($_GET['toDate']) && $_GET['toDate'] != ''){ echo $toDate= $_GET['toDate'];}else{ echo $toDate= date('Y-m-d', strtotime('+30 days'));} ?>">  
          </div>
          <div class="col-md-3 form-group">
              <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
            </div>
        </div>
    </form>
    
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <?php
                      $i = 1;
                      $yearFilterQuery = " AND DATE_FORMAT(leave_start_date,'%Y') = '$currentYear'";
                      $orderByQuery = "ORDER BY leave_master.leave_id DESC";
                      $limitQuery = " LIMIT 100";
                      if(isset($bId) && $bId>0) {
                        $blockFilterQuery = " AND users_master.block_id='$bId'";
                      }
                      if(isset($dId) && $dId>0) {
                          $deptFilterQuery = " AND users_master.floor_id='$dId'";
                      }
                      if(isset($uId) && $uId>0) {
                          $userFilterQuery = " AND users_master.user_id='$uId'";
                      }
                      if(isset($sortby) && $sortby==1){
                        $orderByQuery = "ORDER BY leave_master.leave_start_date DESC";
                      }
                      if(isset($from) && isset($from) && $toDate != '' && $toDate) {
                          $dateFilterQuery = " AND leave_master.leave_start_date BETWEEN '$from' AND '$toDate'";
                      }
                      // if(isset($month) && $month>0) {
                      //   $monthFilterQuery = " AND DATE_FORMAT(leave_start_date,'%c') = '$month'";
                      // }
                      // if(isset($year) && $year>0) {
                      //   $yearFilterQuery = " AND DATE_FORMAT(leave_start_date,'%Y') = '$year'";
                      //   $limitQuery = '';
                      // }
                      if(isset($key) && $key="on_leave"){
                        $todayDate = date('Y-m-d');
                        $todayLeavesQuery = " AND leave_master.leave_start_date='$todayDate'";
                      }
                      $q = $d->selectRow("leave_master.*,users_master.floor_id,users_master.block_id,leave_type_master.leave_type_name,users_master.user_full_name, AAB.admin_name AS admin_approve_by, UAB.user_full_name AS user_approve_by, ADB.admin_name AS admin_declined_by, UDB.user_full_name AS user_declined_by,block_master.block_name,floors_master.floor_name,users_master.user_designation,
                      (SELECT COUNT(*) FROM `attendance_master` WHERE user_id=leave_master.user_id AND attendance_date_start=leave_master.leave_start_date) AS attendance_available",
                      "block_master,floors_master,leave_type_master,users_master,leave_master LEFT JOIN bms_admin_master AAB ON AAB.admin_id=leave_master.leave_approved_by LEFT JOIN users_master UAB ON UAB.user_id=leave_master.leave_approved_by LEFT JOIN bms_admin_master ADB ON ADB.admin_id=leave_master.leave_declined_by LEFT JOIN users_master UDB ON UDB.user_id=leave_master.leave_declined_by", 
                      "users_master.user_id=leave_master.user_id AND leave_type_master.leave_type_id=leave_master.leave_type_id AND users_master.delete_status=0 AND users_master.block_id=block_master.block_id AND users_master.floor_id=floors_master.floor_id AND leave_master.society_id='$society_id' $blockFilterQuery $deptFilterQuery $userFilterQuery $dateFilterQuery  $blockAppendQueryUser $todayLeavesQuery", "$orderByQuery $limitQuery");
                      $counter = 1;
                      
                    ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <?php if($_COOKIE['role_name']=="Super Admin") {?>
                        <th>#</th>
                        <?php } ?>
                        <th>Sr.No</th>
                        <th>Action</th>
                        <th>Status</th>
                        <th>Employee</th>
                        <th>Branch (Department)</th>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Day Type</th>
                        <th>Paid/Unpaid</th>
                        <th>Performed By</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                      while ($data = mysqli_fetch_array($q)) {

                        if($data['leave_day_type'] == 0){
                              $leave_day_type=  "Full Day";
                          }else{
                              if ($data['half_day_session'] == "1") {
                                 $leave_day_type=  "Half Day <br>(First Half)";
                              } else if ($data['half_day_session'] == "2") {
                                $leave_day_type=  "Half Day <br>(Second Half)";
                              } else {
                                $leave_day_type=  "Half Day";
                              }
                          }

                        if($data['paid_unpaid'] == 0){
                              $paid_unpaid =  "Paid";
                          }else{
                              $paid_unpaid=  "Unpaid";
                          }
                    ?>
                    <tr>
                        <?php if($_COOKIE['role_name']=="Super Admin") {?>
                        <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['leave_id']; ?>">
                         <?php  if ($data['leave_status'] == 0 ) { ?>
                           <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['leave_id']; ?>">
                          <?php }?>
                        </td>
                        <?php }?>
                       <td><?php echo $counter++; ?></td>
                       <td>
                         <div class="d-flex align-items-center">
                         <?php //echo $data['attendance_available'] ?>
                            <a data-toggle="tooltip" title="Leave Balance" href="leaveAssign?bId=<?php echo $data['block_id'];?>&dId=<?php echo $data['floor_id'];?>&uId=<?php echo $data['user_id'];?>&ltId=&year=<?php echo date("Y", strtotime($data['leave_start_date']));?>" class="btn btn-sm btn-primary ml-1"><i class="fa fa-angellist"></i></a>
                            <button type="button" class="btn btn-sm btn-primary ml-1" onclick="LeaveModal(<?php echo $data['leave_id']; ?>, <?php echo $data['society_id']; ?>)" >
                            <i class="fa fa-eye"></i>
                            </button>
                            <?php 
                            if($data['is_salary_generated'] == 0){ ?>
                              <button type="button" class="btn btn-sm btn-primary ml-1" onclick="updateLeaveDayType(<?php echo $data['leave_id']; ?>, <?php echo $data['leave_day_type']; ?>, <?php echo $data['paid_unpaid']; ?>, <?php echo $data['leave_type_id']; ?>, <?php echo $data['user_id']; ?>, '<?php echo $data['leave_start_date']; ?>','<?php echo date('d M Y', strtotime($data['leave_start_date'])); ?>','<?php echo $data['attendance_available'] ?>');"><i class="fa fa-pencil"></i></button>
                            <?php } ?>
                         </div>
                        </td>
                        <td>
                         <?php if($data['leave_status'] == 0){ 
                           
                           ?>
                          <button title="Approve Leave" type="button" class="btn btn-sm btn-primary ml-1" onclick="leaveStatusChange('1','<?php echo $data['leave_id']; ?>','<?php echo $data['leave_day_type']; ?>', '<?php echo $data['paid_unpaid'];?>',<?php echo $data['leave_type_id']; ?>, <?php echo $data['user_id']; ?>, '<?php echo $data['leave_start_date']; ?>','<?php echo date('d M Y', strtotime($data['leave_start_date'])); ?>',`<?php echo $data['leave_reason']; ?>`,`<?php echo $paid_unpaid; ?>`,`<?php echo $data['leave_type_name']; ?>`)"><i class="fa fa-check"></i></button>
                          <button title="Decline Leave" type="button" class="btn btn-sm btn-danger ml-1" onclick="leaveStatusChange('2', '<?php echo $data['leave_id']; ?>','<?php echo $data['paid_unpaid'];?>')"><i class="fa fa-times"></i></button>
                        <?php }elseif($data['leave_status'] == 1){ ?>
                          <b><span class="badge badge-pill m-1 badge-success">Approved</span></b>
                        <?php }else{ ?>
                          <b><span class="badge badge-pill m-1 badge-danger">Rejected</span></b>
                        <?php } ?>
                        </td>
                       <td><?php echo $data['user_full_name']; ?> (<?php custom_echo($data['user_designation'],20); ?>)</td>
                       <td><?php echo $data['block_name']; ?> (<?php echo $data['floor_name']; ?>)</td>
                       <td><?php echo date("d M Y", strtotime($data['leave_start_date'])); ?> (<?php echo date("D", strtotime($data['leave_start_date'])); ?>)</td>
                       <td><?php echo $data['leave_type_name']; ?></td>
                       <td><?php  echo $leave_day_type; ?></td>
                          <td><?php 
                         echo $paid_unpaid;
                        ?></td>
                       
                        <td><?php if($data['leave_status'] == 1){ 
                            if($data['leave_approved_by_type'] == 1){
                                echo $data['admin_approve_by'];
                            }else{
                                echo $data['user_approve_by'];
                            } 
                          }elseif($data['leave_status'] == 2){ 
                            if($data['leave_declined_by_type'] == 1){
                                echo $data['admin_declined_by'];
                            }else{
                                echo $data['user_declined_by'];
                            } 
                          } ?>
                        </td>
                        <input type="hidden" name="leave_id" value="<?php echo $data['leave_id']; ?>">
                    </tr>
                    <?php } ?>
                </tbody>

            </table>
            
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div>

<div class="modal fade" id="leaveStatusApprovedModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Leave Request</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
          <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="leaveStatusApproved" action="controller/leaveController.php" name="leaveStatusApproved"  enctype="multipart/form-data" method="post">
                <div class="form-group row">
                <p class="col-sm-12"><b>Leave Type: </b> <span id="leave_type_view"></p>
                <p class="col-sm-12"><b>Day Type : </b> <span id="leave_day_type_view"></p>
                <p class="col-sm-12"><b>Leave Reason: </b> <span id="leave_reason"></p>
                <p class="col-sm-12 text-danger">Total Leave Balance (Year): <span id="balance_leave"></span></p>
                <p class="col-sm-12 text-danger">Applicable Leave Till Date <span class="leave_date"></span>: <span id="applicable_leave"></span></p>
                </div>  
                  <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Mode <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <select name="paid_unpaid" class="form-control paid_unpaid">
                              <option value="">-- Mode --</option> 
                              <option value="0">Paid Leave</option> 
                              <option value="1">Unpaid Leave</option> 
                          </select>
                        </div>                   
                    </div>                 
                    <div class="form-footer text-center">
                      <input type="hidden" name="leaveStatusApproved"  value="leaveStatusApproved">
                      <input type="hidden" class="leave_status" name="leave_status"  value="">
                      <input type="hidden" name="leave_id"  class="leave_id">
                      <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Approve </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="leaveStatusRejectModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Reject Reason</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="leaveStatusReject" action="controller/leaveController.php" name="changeAttendance"  enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <textarea required class="form-control" name="leave_admin_reason"></textarea>
                        </div>                   
                    </div>            
                    <div class="form-footer text-center">
                    <input type="hidden" name="leaveStatusReject"  value="leaveStatusReject">
                      <input type="hidden" class="leave_status" name="leave_status"  value="">
                      <input type="hidden" name="leave_id"  class="leave_id">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Reject </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="leaveDetailModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Leave Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="leaveData" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="leaveDayTypeModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Update Leave Day Type</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" >
          <div class="card-body" > 
            <div class="row " >
                <div class="col-md-12">
                <form id="leaveDayTypeForm" action="controller/leaveController.php" name="leaveStatusApproved"  enctype="multipart/form-data" method="post">
                    <div class="form-group row">
                      <p class="col-sm-12 text-danger">Total Leave Balance (Year): <span class="balance_leave"></span></p>
                      <p class="col-sm-12 text-danger">Applicable Leave Till Date <span class="leave_date"></span>: <span class="applicable_leave"></span></p>
                        <label for="input-10" class="col-sm-12 col-form-label">Leave Day Type <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <select name="leave_day_type" id="leave_day_type" class="form-control" required>
                              <option value="0">Full Day Leave</option> 
                              <option value="1">Half Day Leave</option> 
                          </select>
                        </div>                   
                    </div>  
                    <div class="form-group row">
                        <label for="input-10" class="col-sm-12 col-form-label">Mode <span class="text-danger">*</span></label>
                        <div class="col-lg-12 col-md-12" id="">
                          <select name="paid_unpaid" id="paid_unpaid" class="form-control">
                              <option value="0">Paid Leave</option> 
                              <option value="1">Unpaid Leave</option> 
                          </select>
                        </div>                   
                    </div>
                    <div class="form-footer text-center">
                      <input type="hidden" name="updateLeaveDayType"  value="updateLeaveDayType">
                      <input type="hidden" name="leave_id"  class="leave_id">
                      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> Update </button> 
                      <button type="button" class="btn btn-sm btn-danger leave_id" onclick="removeUserApprovedLeave()"><i class="fa fa-trash-o fa-lg"></i> Remove Leave </button> 
                    </div>
                </form>
                </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remainingLeaveModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Total & Remaining Leave</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="remainingLeaveData" style="align-content: center;">
        
      </div>
      
    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
  $("#leave_day_type").change(function(){
      var leave_day_type = $('#leave_day_type').val();
      var applicable_leave = parseFloat($('.applicable_leave').text());
      if(applicable_leave == 0.5 && leave_day_type == 1){
          var paidUnpaidValue = `<option value="">-- Mode --</option>
                  <option value="0">Paid Leave</option>
                  <option value="1">Unpaid Leave</option> `;
          $('.paid_unpaid').html(paidUnpaidValue);  
      }else if(applicable_leave >=1){
          var paidUnpaidValue = `<option value="">-- Mode --</option>
                  <option value="0">Paid Leave</option>
                  <option value="1">Unpaid Leave</option> `;
          $('.paid_unpaid').html(paidUnpaidValue);  
      }
      else{
          var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
          $('.paid_unpaid').html(paidUnpaidValue);
      }
  });
</script>
