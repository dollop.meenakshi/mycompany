<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css" />

<?php
error_reporting(0);
extract($_REQUEST);
//echo "<pre>";print_r($_REQUEST);
$society_id = $_COOKIE['society_id'];
$survey_id = (int)$survey_id;
if ($action == "Edit") {
  extract(array_map("test_input", $_POST));
  if (isset($survey_id)) {
    $q = $d->select("survey_master", "society_id='$society_id' AND survey_id='$survey_id'");
    $row = mysqli_fetch_array($q);
    extract($row);
  }
}
if ($addQuestion == "true" || $editQuestion == "true") {
  if (isset($survey_question_id)) {
    $survey_question_master_qry = $d->select("survey_question_master", "society_id='$society_id' AND survey_question_id='$survey_question_id'");
    $survey_question_master_result = mysqli_fetch_array($survey_question_master_qry);
//  echo "<pre>";print_r( $row);
    extract($survey_question_master_result);
  }
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="addSurveyQueFrm" action="controller/surveyController.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="survey_id" value="<?php echo $survey_id; ?>">
                <?php if (isset($survey_question_id)) {  ?>
                  <input type="hidden" name="survey_question_id" value="<?php echo $survey_question_id; ?>">
                  <input type="hidden" name="editSurveyQue" value="editSurveyQue">
                <?php } else { ?>
                  <input type="hidden" name="addSurveyQue" value="addSurveyQue">
                <?php } ?>
                <h4 class="form-header text-uppercase">
                  Survey Question
                </h4>
                <div class="form-group row">
                  <label for="survey_question" class="col-sm-2 col-form-label">Survey Question <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <input required="" type="text" class="form-control " id="survey_question" name="survey_question" maxlength="150" value="<?php if (isset($survey_question_id)) {echo $survey_question;} ?>">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <?php if (isset($survey_question_id)) {
                  $survey_option_master_qry = $d->select("survey_option_master", "society_id='$society_id' AND survey_question_id='$survey_question_id'");
                  $no_of_option_selected = mysqli_num_rows($survey_option_master_qry);
                } ?>
                <div class="form-group row">
                  <label for="question_image" class="col-sm-2 col-form-label">Image</label>
                  <div class="col-sm-4">
                    <input class="form-control-file border photoOnly" accept="image/*" type="file" name="question_image" accept="image/x-png,image/gif,image/jpeg">
                    <?php if (isset($survey_question_id)) { ?>
                      <input class="form-control-file border photoOnly" accept="image/*" type="hidden" name="question_image_old" value="<?php echo $question_image; ?>">
                    <?php } ?>
                  </div>
                  <input type="hidden" name="isOptionAdded" id="isOptionAdded" value="Yes">
                  <label for="input-12" class="col-sm-2 col-form-label">No Of Options <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <select required="" class="form-control" id="no_of_option" name="no_of_option" onchange="getOptionList();">
                      <option value="">-- Select --</option>
                      <?php
                      for ($i = 2; $i < 10; $i++) { ?>
                        <option <?php if (isset($survey_question_id) && $no_of_option_selected == $i) {echo "selected";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php }
                      ?>
                    </select>
                  </div>
                </div>
                <div id="OptionResp" class="form-group row">
                  <?php if (isset($survey_question_id)) {
                    $cnt = 1;
                    while ($survey_option_master_data = mysqli_fetch_array($survey_option_master_qry)) {
//  echo "<pre>";print_r($survey_option_master_data);echo "</pre>";
                      if (trim($survey_option_master_data['survey_option_name']) != "")
                        ?>
                      <label for="option_name<?php echo $cnt; ?>" style="padding-bottom: 15px;" class="col-sm-2 col-form-label">Option <?php echo $cnt; ?> <span class="text-danger">*</span></label>
                      <div class="col-sm-4" style="padding-bottom: 15px;">

                        <input onblur="blurtxt();" type="text" id="option_name<?php echo $cnt; ?>" class="form-control option_name-cls" name="option_name[]" value="<?php echo $survey_option_master_data['survey_option_name']; ?>">
                        <label id="option_name<?php echo $cnt; ?>_div" style="display: none;">Please enter option name <?php echo $cnt; ?> </label>
                      </div>
                      <?php $cnt++;
                    }
                  } ?>
                </div>
                <div class="form-group row">
                </div>
                <?php if (!isset($survey_question_id)) {  ?>
                  <div class="form-group row">
                    <label class="col-lg-2 col-form-label form-control-label">Next Step</label>
                    <div class="col-lg-9">
                      <div class="form-check-inline">
                        <label class="form-check-label">
                          <input type="radio" checked="" class="form-check-input" value="0" name="btnType"> Save & Add More Question
                        </label>
                      </div>
                      <div class="form-check-inline">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" value="1" name="btnType"> Save & Finish
                        </label>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                <div class="form-footer text-center">
                  <?php if (isset($survey_question_id)) { ?>
                    <input type="hidden" name="editSurveyQueBtn" value="editSurveyQueBtn">
                    <button type="submit" class="btn btn-primary" name=""><i class="fa fa-check-square-o"></i> Edit</button>
                  <?php } else { ?>
                    <input type="hidden" name="adMoreQuer" value="adMoreQuer">
                    <input type="submit" class="btn btn-primary" value="Save" name="">
                  <?php } ?>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!--End Row-->
    </div>
    <!-- End container-fluid-->
  </div>
  <script src="assets/js/jquery.min.js"></script>
  <script type="text/javascript">
    $('input').change(function() {
      this.value = $.trim(this.value);
    });
    $('textarea').change(function() {
      this.value = $.trim(this.value);
    });

    function getOptionList() {
      var data = $('#no_of_option').val();
      if (data > 1) {
        $.ajax({
          url: "getOptionList.php",
          cache: false,
          type: "POST",
          data: {
            no_of_option: data
          },
          success: function(response) {
            $('#OptionResp').html(response);
// /IS_853
            $('#isOptionAdded').val('Yes');
          }
        });
      } else {
        $('#isOptionAdded').val('No');
        swal("Minimum 2 Option Required !", {
          icon: "error",
        });
        $('#OptionResp').html(' ');
      }
    }
    $("#addSurveyQueFrm").submit(function(event) {
      var error = 0;
      $('.option_name-cls').each(function() {
        var clsVal = $(this).val();
        var eleId = $(this).attr('id');
        if ($.trim(clsVal) == "") {
          error++;
/*$(eleId+'_div').removeClass('valid');
$(eleId+'_div').addClass('error');*/
          $('#' + eleId + '_div').css('display', 'inline-block');
          $('#' + eleId + '_div').css('color', '#ff0000');
          $('#' + eleId + '_div').text("Please enter Option Value ");
        } else {
/*$(eleId).addClass('valid');
$(eleId).removeClass('error');*/
          $('#' + eleId + '_div').css('display', 'none');
        }
      });
//IS_1309
      var valOption = [];

      $('.option_name-cls').each(function(i) {
        valOption[i] = $(this).val();
      });

      $('.option_name-cls').each(function() {
        var clsVal = $(this).val();
        var eleId = $(this).attr('id');
        var numOccurences = 1;
        if ($.trim(clsVal) != "") {
          numOccurences = $.grep(valOption, function(elem) {
            return elem.toLowerCase() === clsVal.toLowerCase();
          }).length;
          if (numOccurences > 1) {
            error++;
            $('#' + eleId + '_div').css('display', 'inline-block');
            $('#' + eleId + '_div').css('color', '#ff0000');
            $('#' + eleId + '_div').text("Duplicate Option");
          } else {
            $('#' + eleId + '_div').css('display', 'none');
          }
        }
      });
//IS_1309
//alert(error);
      if ($('#isOptionAdded').val() == "No") {
        error++;
        $('#no_of_option-error').text("Please select No Of Options");
        $('#no_of_option').addClass('error');
      }

      if (error > 0) {
        event.preventDefault();
      } else {
        $("#addSurveyQueFrm").submit();
      }
    });

    function blurtxt() {
      var valOption = [];

      $('.option_name-cls').each(function(i) {
//  alert($(this).val());
        valOption[i] = $(this).val();
      });

      var error = 0;

      $('.option_name-cls').each(function() {
        var clsVal = $(this).val();
        var eleId = $(this).attr('id');

        if ($.trim(clsVal) == "") {
          error++;
          $('#' + eleId + '_div').css('display', 'inline-block');
          $('#' + eleId + '_div').css('color', '#ff0000');
          $('#' + eleId + '_div').text("Please enter " + eleId);
        } else {
          $('#' + eleId + '_div').css('display', 'none');
        }
      });

      $('.option_name-cls').each(function() {
        var clsVal = $(this).val();
        var eleId = $(this).attr('id');
        var numOccurences = 1;
        if ($.trim(clsVal) != "") {
          numOccurences = $.grep(valOption, function(elem) {
            return elem.toLowerCase() === clsVal.toLowerCase();
          }).length;

          if (numOccurences > 1) {
            error++;
            $('#' + eleId + '_div').css('display', 'inline-block');
            $('#' + eleId + '_div').css('color', '#ff0000');
            $('#' + eleId + '_div').text("Duplicate Option");
          } else {
            $('#' + eleId + '_div').css('display', 'none');
          }
        }
      });


      if (error > 0) {
        $(':input[type="submit"]').prop('disabled', true);
      } else {
        $(':input[type="submit"]').prop('disabled', false);
      }
      if (error > 0) {
        event.preventDefault();
      }
    }
  </script>
  <script src="assets/plugins/material-datepicker/js/moment.min.js"></script>
  <script src="assets/plugins/material-datepicker/js/bootstrap-material-datetimepicker.min.js"></script>

  <?php
} else {
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="surveyFrm" action="controller/surveyController.php" method="post">
                <?php if (isset($survey_id) && $action == "Edit") {  ?>
                  <input type="hidden" name="survey_id" value="<?php echo $survey_id; ?>">
                  <input type="hidden" name="society_id" value="<?php echo $society_id; ?>">
                  <input type="hidden" name="editSurvey" value="editSurvey">
                <?php } else { ?>
                  <input type="hidden" name="addSurvey" value="addSurvey">
                <?php } ?>
                <h4 class="form-header text-uppercase">
                  Survey
                </h4>
                <div class="form-group row">
                  <label for="survey_title" class="col-sm-2 col-form-label">Survey Title <span class="text-danger">*</span></label>
                  <div class="col-sm-10">

                    <input required="" type="text" class="form-control " id="survey_title" name="survey_title" maxlength="150" value="<?php if (isset($survey_id)  && $action == "Edit") {
                      echo $survey_title;
                    } ?>">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label"> Description </label>
                  <div class="col-sm-10">
                    <textarea required="" class="form-control" id="summernoteImgage" name="survey_desciption" maxlength="250"><?php if (isset($survey_id) && $action == "Edit") {
                      echo $survey_desciption;
                    } ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="survey_date" class="col-sm-2 col-form-label">Start Date<span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input required="" type="text" readonly="" class="form-control" id="autoclose-datepicker2" name="survey_date" value="<?php if (isset($survey_id) && $action == "Edit") {echo $survey_date;} ?>">
                  </div>


                  <label for="input-13333" class="col-sm-2 col-form-label">Survey For <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <select required="" class="form-control single-select" name="survey_for" id="survey_for">
                      <?php if ($access_branchs=="") { ?>
                      <option value="0"> <?php echo $xml->string->all; ?> <?php echo $xml->string->floors; ?></option>
                      <?php }
                      $fq = $d->select("floors_master,block_master", "floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' $blockAppendQueryFloor");
                      while ($floorData = mysqli_fetch_array($fq)) { ?>
                        <option <?php if (isset($survey_id) && $survey_for == $floorData['floor_id']) {
                          echo "selected";
                        } ?> value="<?php echo $floorData['floor_id']; ?>"><?php echo $floorData['floor_name']; ?> (<?php echo $floorData['block_name']; ?>)</option>
                      <?php  } ?>

                    </select>
                  </div>
                  <label for="input-13333" class="col-sm-2 col-form-label">Is User name Required <span class="text-danger">*</span></label>
                  <div class="col-sm-4 mt-2">
                    <input type="radio" <?php if(isset($is_username_required) && $is_username_required==1){
                      echo "checked"; 
                    }else { echo "checked"; } ?>  value="1" name="is_username_required" id="">
                    <label>Yes</label>
                    <input type="radio" <?php if(isset($is_username_required) && $is_username_required==0){
                      echo "checked"; 
                    }else { echo ""; } ?> value="0" name="is_username_required" id="">
                    <label>No</label>
                  </div>
                </div>

                <div class="form-footer text-center">
                  <?php if (isset($survey_id) && $action == "Edit") { ?>
                    <input type="hidden" name="editSurveyBtn" value="editSurveyBtn">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Edit</button>
                  <?php } else { ?>

                    <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-check-square-o"></i> Add</button>
                  <?php } ?>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!--End Row-->
    </div>
    <!-- End container-fluid-->
  </div>
  <?php } ?>