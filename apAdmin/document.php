 <?php 
    if(isset($_POST['document_id_edit'])) {
    $btnName="Update";
    extract(array_map("test_input" , $_POST));
    $q=$d->select("document_master","document_id='$document_id_edit'");
    $data=mysqli_fetch_array($q);
    } else {
    $btnName="Add";
    }
     ?>
     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        
           <?php  $totalDocument= $d->count_data_direct("document_type_id","document_type_master","society_id='$society_id'");
           if ($totalDocument<1) {  ?>
             <div class="alert alert-danger alert-dismissible" role="alert">
               <button type="button" class="close" data-dismiss="alert">×</button>
            
              <div class="alert-message">
                <span><strong>Error!</strong> Please Add Document Type First  <a href="documentType">Click Here</a></span>
              </div>
              </div>
          <?php }
              ?>
     </div>
     
     </div>
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form method="post" id="addDocument" action="controller/documentController.php" enctype="multipart/form-data">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-file-pdf-o"></i>
                 <?php echo $xml->string->society; ?> Document
                </h4>

                <div class="form-group row">
                  <label for="input-10" class="col-sm-2 col-form-label">Document Name <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <?php  if(isset($_POST['document_id_edit'])) { ?>
                    <input type="text" required="" value="<?php echo $data['ducument_name']; ?>" maxlength="50" class="form-control" id="input-10" name="ducument_name_edit">
                    <input type="hidden" id="input-14" name="document_file_old" required="" value="<?php echo $data['document_file']; ?>" >
                    <input type="hidden" id="input-14" name="document_id_update" required="" value="<?php echo $data['document_id']; ?>" >
                    <?php } else { ?>
                    <input type="text" required=""  maxlength="50" class="form-control " id="input-10" name="ducument_name_add">
                    <?php } ?>
                 
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label">Document Description </label>
                  <div class="col-sm-10">
                    <textarea class="form-control" maxlength="300" id="input-12" name="ducument_description"><?php echo $data['ducument_description']; ?></textarea>
                  </div>
                 
                </div>
                <div class="form-group row">
                  <label for="input-14" class="col-sm-2 col-form-label">Document File <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <?php  if(isset($_POST['document_id_edit'])) { ?>
                    <input class="form-control-file border docOnly" type="file" id="input-14" name="document_file" > (JPG,PNG,Doc,CSV,XLS & PDF are allowed)
                  <?php } else  { ?>

                    <input class="form-control-file border docOnly" type="file" id="input-14" name="document_file" required=""> (JPG,PNG,Doc,CSV,XLS & PDF are allowed)
                  <?php } ?>
                  </div>
                 
                </div>
                <div class="form-group row">
                  <label for="input-33" class="col-sm-2 col-form-label">Document Type <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <select  class="form-control" id="input-33" required="" name="document_type_id">
                      <option value="">-- Select --</option>
                       <?php 
                        $i=1;
                        $q=$d->select("document_type_master","society_id='$society_id' ");
                        while ($empData=mysqli_fetch_array($q)) {
                         ?>
                         <option <?php if($empData['document_type_id']==$data['document_type_id']) { echo 'selected';} ?> value="<?php echo $empData['document_type_id']; ?>"><?php echo $empData['document_type_name']; ?></option>
                       <?php } ?>
                    </select>
                  </div>
                  
                </div>
                
                <div class="form-footer text-center">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->