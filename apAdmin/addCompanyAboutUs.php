
<?php
extract(array_map("test_input", $_POST));
if (isset($edit_company_about_us)) {
    $q = $d->select("company_about_us_master","company_about_us_id='$company_about_us_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Add/Edit Company About Us</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addCompanyAboutUsFrom" action="controller/CompanyAboutUsController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Top Image <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="file" accept="image/png, image/jpeg" class="form-control" name="company_about_us_top_image" value="">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Bottom Title <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" class="form-control" placeholder="Bottom Title" name="company_about_us_bottom_title" value="<?php if($data['company_about_us_bottom_title'] !=""){ echo $data['company_about_us_bottom_title']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Top Description <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                <textarea id="company_about_us_top_description" name="company_about_us_top_description" class="form-control "><?php if($data['company_about_us_top_description'] !=""){ echo $data['company_about_us_top_description']; } ?></textarea>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Bottom Description <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                <textarea id="company_about_us_bottom_description" name="company_about_us_bottom_description" class="form-control "><?php if($data['company_about_us_bottom_description'] !=""){ echo $data['company_about_us_bottom_description']; } ?></textarea>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Image 1 <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="file" accept="image/png, image/jpeg" class="form-control" name="company_about_us_image_one" value="" >
                                </div>
                            </div> 
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Image 2 <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="file" accept="image/png, image/jpeg" class="form-control" name="company_about_us_image_two" value="" >
                                </div>
                            </div> 
                        </div>
                    </div>    
                    <input type="hidden" id="company_about_us_id" name="company_about_us_id" value="<?php if($data['company_about_us_id'] !=""){ echo $data['company_about_us_id']; } ?>" >            
                    <div class="form-footer text-center">
                    <?php //IS_1019 addPenaltiesBtn 
                    if (isset($edit_company_about_us)) {                    
                    ?>
                    <button id="addCompanyAboutUsBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addCompanyAboutUs"  value="addCompanyAboutUs">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyAboutUsFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                        <?php }
                        else {
                        ?>
                    
                    <button id="addCompanyAboutUsBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addCompanyAboutUs"  value="addCompanyAboutUs">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addCompanyAboutUsFrom');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //$('#blah').attr('src', e.target.result);
            $('#blah').html('<img src="'+e.target.result+'" width="75%" height="50%">');
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
