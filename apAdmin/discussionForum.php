<link rel="stylesheet" href="assets/plugins/summernote/dist/summernote-bs4.css"/>

<?php
extract($_REQUEST);
//echo "<pre>";print_r($_REQUEST);

if($editDiscussion =="editDiscussion"){
       
        extract(array_map("test_input" , $_POST));
        if (isset($discussion_forum_id)) {
          $q=$d->select("discussion_forum_master","society_id='$society_id' AND discussion_forum_id='$discussion_forum_id'");
          $row=mysqli_fetch_array($q);
          extract($row);
          
          $q1=$d->select("discussion_forum_users","society_id='$society_id' AND discussion_forum_id='$discussion_forum_id'");
          $user_ids = array();
          while ($data = mysqli_fetch_array($q1)) {
            array_push($user_ids, $data['user_id']);
          }
        }
}

?>
<input type="hidden" id="user_ids" value="<?php if(isset($user_ids)) { echo implode(',', $user_ids); } ?>">

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form enctype="multipart/form-data" id="addDiscussion" action="controller/discussionForumController.php" method="post">
                <?php if(isset($discussion_forum_id) ){  ?>
                  <input type="hidden" name="discussion_forum_id" value="<?php echo $discussion_forum_id; ?>">
                <?php } ?>
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-bullhorn"></i>
                   Discussion Forum 
                </h4>
                <div class="form-group row">
                  <label for="input-122" class="col-sm-2 col-form-label">Discussion Title <span class="text-danger">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" required="" maxlength="200" class="form-control" id="input-122" name="discussion_forum_title" value="<?php if(isset($discussion_forum_id) ){ echo $discussion_forum_title;} ?>">
                  </div>
                  
                  <div class="col-sm-4">
                   
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-12" class="col-sm-2 col-form-label">Discussion Description </label>
                  <div class="col-sm-10">
                    <textarea maxlength="800" class="form-control" rows="10" id="summernoteImgage" required="" name="discussion_forum_description"><?php if(isset($discussion_forum_id) ){ echo $discussion_forum_description;} ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-photo" class="col-sm-2 col-form-label">Photo </label>
                  <div class="col-sm-10">
                    <input type="hidden" class="form-control" rows="10" id="input-photo" value="<?php if(isset($discussion_forum_id) ){ echo $discussion_photo;} ?>" required="" name="discussion_photo_old">
                    <input accept="image/*" type="file" class="form-control-file border photoOnly"  id="input-photo"  name="discussion_photo">
                  </div>
                
                 
                
                 
                </div>
                <div class="form-group row">
                  
                  <label for="input-doucment" class="col-sm-2 col-form-label">Document  </label>
                  <div class="col-sm-10">
                    <input type="hidden" class="form-control" rows="10" id="input-doucment" value="<?php if(isset($discussion_forum_id) ){ echo $discussion_file;} ?>" required="" name="discussion_file_old">
                    <input accept=".pdf,.PDF,.DOC,.doc,.csv,.CSV,.xlsx,.XLSX,.jpg,.png" type="file" class="form-control-file border docOnly"  id="input-doucment"  name="discussion_file">
                  </div>
                
                 
                </div>
                 <div class="form-group row">
                
                 <label for="input-13333" class="col-sm-2 col-form-label">Branch <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                  <select name="discussion_block_id" class="form-control single-select" onchange="getDiscussionForumFloorByBlockId(this.value)" required>
                  <option value="">-- Select Branch --</option> 
                    <?php 
                      $qb=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");  
                      while ($blockData=mysqli_fetch_array($qb)) {
                    ?>
                  <option <?php if(isset($discussion_block_id) && $discussion_block_id==$blockData['block_id']  ) { echo "selected";} ?> value="<?php echo  $blockData['block_id'];?>" ><?php echo $blockData['block_name'];?></option>
                  <?php } ?>
                  
                  </select>
                  </div>
                  <label for="input-13333" class="col-sm-2 col-form-label">Discussion For <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <select required=""  class="form-control single-select" id="discussion_forum_for"  name="discussion_forum_for" onchange="getDiscussionUserByFloor(this.value)">
                      <option value="0"> <?php echo $xml->string->all; ?>  <?php echo $xml->string->floors; ?></option>
                    <?php 
                    if(isset($discussion_forum_id) && $discussion_forum_id>0){
                    $fq=$d->select("floors_master,block_master","floors_master.society_id='$society_id' AND floors_master.block_id=block_master.block_id AND floors_master.block_id='$discussion_block_id'");
                      while ($floorData=mysqli_fetch_array($fq)) { ?>
                      <option <?php if(isset($discussion_forum_id) && $discussion_forum_for==$floorData['floor_id']  ) { echo "selected";} ?> value="<?php echo $floorData['floor_id'];?>"><?php echo $floorData['floor_name'];?></option>
                    <?php  } }?>
                   
                    </select>
                    
                  </div>
                 </div>
                  <div class="form-group row">
                  <label for="input-13333" class="col-sm-2 col-form-label">Employees </label>
                  <div class="col-sm-4">
                  <select  multiple="multiple" name="user_id[]" id="user_id" class="form-control multiple-select" >
                      <option value="">-- Select User --</option>
                      ?>
                    </select>
                  </div>
                 
                </div>
                
               
                <div class="form-footer text-center">
                  <?php   if(isset($discussion_forum_id)) { ?> 
                    <input type="hidden" name="editDiscussion" value="editDiscussion">
                     <button type="submit" class="btn btn-success" name="editElectionBtn"><i class="fa fa-check-square-o"></i> Update</button>
                  <?php } else {?>
                    <input type="hidden" name="addDiscussion" value="addDiscussion">
                    <button type="submit" class="btn btn-success" name="addElectionBtn"><i class="fa fa-check-square-o"></i> SAVE</button>
                  <?php } ?>
                    <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
<?php //IS_933?>
   <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript">
getDiscussionUserByFloor(<?php echo $discussion_forum_for; ?>);
  $( "#event" ).submit(function( event ) {
   $("#myButton").attr("disabled", true);


   window.setTimeout(setDisabled, 10000);
 
  });
function getDiscussionUserByFloor(floor_id)
{
  var user_id_old = $('#user_ids').val();
  if(user_id_old != ''){
    user_id_old = $('#user_ids').val().split(",");
  }
  
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUserByFloor",
                floor_id:floor_id,
              },
            success: function(response){
              var userHtml ="";
              userHtml=`<option value="">--Select User--</option>`;
              
              $.each(response.users, function( index, value ) {
                var j = user_id_old[index];
                
                if(jQuery.inArray(value.user_id, user_id_old)!= -1)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                }
                
                userHtml +=`<option `+selected+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });
              
             $('#user_id').html(userHtml);
            }
    })
}

function getDiscussionForumFloorByBlockId(id)
{
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getFloorByBlockId",
          block_id:id,
        },
      success: function(response){
        console.log(response);
        if(access_branchs_id=='') {
          optionContent = `<option value="0">All Departments</option>`;
        }
        $.each(response.floor, function( index, value ) {
          optionContent += `<option value="`+value.floor_id+`" >`+value.floor_name+`</option>`;
        });
        $('#discussion_forum_for').html(optionContent);
      }
  });
}

 function setDisabled() {
    document.getElementById('myButton').disabled = false;
}
 
    </script>