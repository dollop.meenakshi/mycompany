<?php
extract($_REQUEST);
if(isset($emp_id)){
  $emp_id = (int)$emp_id;
}
$q1=$d->select("employee_master","society_id='$society_id' AND emp_id='$emp_id'","");
$row=mysqli_fetch_array($q1);
extract($row);

if (mysqli_num_rows($q1)==0){
    $_SESSION['msg1']="Invalid Request";
    echo ("<script LANGUAGE='JavaScript'>
        window.location.href='employeeSalary';
        </script>");
  }

$count5=$d->sum_data("rating_star","employee_rating_master","emp_id='$emp_id'");
    while($row111=mysqli_fetch_array($count5))
   {
        $asif=$row111['SUM(rating_star)'];
      $totalStar=number_format($asif,2,'.','');
    }
    $totalUser=$d->count_data_direct("rating_id","employee_rating_master","emp_id='$emp_id'");
    if ($totalStar>0) {
        $average_rating= $totalStar/$totalUser;
        $average_rating= number_format($average_rating,1,'.','');
    } else {
        $average_rating='0';

    }

 $EW=$d->select("emp_type_master","emp_type_id='$row[emp_type_id]'");
    $empTypeData=mysqli_fetch_array($EW);
      if ($empTypeData['emp_type_name']!='') {
      $type= $empTypeData['emp_type_name'];
      } else {
      $type= "Security Guard";
      }
 $empType= $data['emp_type'];

// find last blancesheet & Categoro
$qlast=$d->select("emp_salary_master,expenses_balance_sheet_master","emp_salary_master.expenses_balance_sheet_id=expenses_balance_sheet_master.expenses_balance_sheet_id AND emp_salary_master.expenses_balance_sheet_id!=0 AND emp_salary_master.society_id='$society_id' AND emp_salary_master.emp_id='$emp_id'","ORDER BY emp_salary_master.emp_salary_master DESC");
$lastData=mysqli_fetch_array($qlast);
 ?>
 <style type="text/css" media="screen">
  .checked {
  color: orange;
}
</style>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"><?php echo $row['emp_name'] ?> Salary</h4>
     </div>
     <div class="col-sm-3">
       <div class="btn-group float-sm-right">
        <?php
        if($row['emp_sallary'] == 0)
        {
        ?>
        <button type="button" class="btn btn-primary btn-sm btnResourceSalary"><i class="fa fa-plus mr-1"></i>Add Salary</button>
        <?php
        }
        else
        {
        ?>
        <a href="#" data-toggle="modal" data-target="#addSalary" onclick="addSalary('<?php echo $emp_id; ?>','<?php echo $emp_name; ?>')" class="btn btn-primary waves-effect waves-light  btn-sm"><i class="fa fa-plus mr-1"></i> Add Salary</a>
        <?php
        }
        ?>
      </div>
     </div>
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
               <div class="form-group row">
                  <div class="col-sm-3 text-center">
                    <?php if($row['emp_profile']!="user.png") { ?> <a href="../img/emp/<?php echo $row['emp_profile'];?>" data-fancybox="images" data-caption="Photo Name : Profile Photo">
                    <img  onerror="this.src='img/user.png'" height="180" width="180" src="../img/emp/<?php echo $row['emp_profile'];?>"  alt="">
                    </a>
                  <?php } else { ?>
                     <img  onerror="this.src='img/user.png'" height="180" width="180" src="../img/emp/<?php echo $row['emp_profile'];?>"  alt="">
                    <?php } ?>

                    <ul  class="list-inline text-center mt-3">

                       <li class="list-inline-item">
                        <!-- <form title="Edit" action="employee" method="post">
                          <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                          <input type="hidden" name="updateemployee" value="updateemployee">
                          <button type="submit" name="" value="" class="btn btn-warning btn-sm"><i class="fa fa-pencil-square-o"></i> Edit</button>
                        </form> -->
                      </li>
                      <li class="list-inline-item">
                        <!-- <form title="Delete" action="controller/employeeController.php" method="post"  >
                          <input type="hidden" name="emp_profile" value="<?php if($data['emp_profile']!="user.png") { echo $data['emp_profile']; } ?>">
                          <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">

                          <input type="hidden" value="deleteemployee" name="deleteemployee">
                          <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i> Delete</button>
                        </form> -->
                      </li>
                    </ul>
                  <?php for ($i=1; $i <=5 ; $i++) {
                    if ($i<=$average_rating) {
                    echo "<span class='fa fa-star checked'></span>";
                    } else {
                    echo "<span class='fa fa-star'></span>";
                    }
                    ?>
                    <?php } ?>
                    (<?php echo  $average_rating;?>)
                  </div>
                  <div  class="col-sm-9 ">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <th>Name</th>
                          <td><?php echo $row['emp_name'];?></td>
                        </tr>
                        <tr>
                          <th>Type</th>
                          <td><?php echo $type;?></td>
                        </tr>
                        <tr>
                          <th>Mobile</th>
                          <td><?php echo $row['country_code'];?> <?php echo $row['emp_mobile'];?></td>
                        </tr>
                        <?php  if( $row['emp_id_proof']!='') { ?>
                         <tr>
                          <th>View ID Proof</th>
                          <td><a title="View ID Proof" target="_blank" href="../img/emp_id/<?php echo $row['emp_id_proof'];?>" name="Id Proof" class="btn btn-warning btn-sm">VIEW</a></td>
                        </tr>
                      <?php  } ?>
                         <tr>
                          <th>Joining Date</th>
                          <td><?php echo $row['emp_date_of_joing'] ?> </td>
                        </tr>
                        <tr>
                          <th>Salary</th>
                          <td> <?php if($row['emp_sallary']>0) { ?> <?php echo $currency; ?> <?php echo $row['emp_sallary'] ?> Per Month <?php  }?></td>
                        </tr>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div><!--End Row-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                      <th>#</th>
                      <!-- <th>#</th> -->
                        <th> Month</th>
                        <th>Total Days</th>
                        <th> Working Days</th>
                        <th>Leave Days</th>
                        <th>Salary Amount</th>
                        <th>Created Date</th>
                        <th>Created By</th>
                        <th>Mode</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                  $q=$d->select("emp_salary_master","society_id='$society_id' AND emp_id='$emp_id'","ORDER BY emp_salary_master DESC");
                  while($row=mysqli_fetch_array($q))
                  {
                    extract($row);
                  ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $month_year; ?></td>
                        <td><?php if($month_working_days>0) { echo $month_working_days; } else {
                       echo   $working_days+$leave_days;
                        } ?></td>
                        <td><?php echo $working_days; ?></td>
                        <td><?php echo $leave_days;  ?></td>
                        <td><?php echo $salary_amount; ?></td>
                        <td><?php echo date("Y-m-d", strtotime($created_date)); ?></td>
                        <td><?php  if($created_by!=0) {
                             $qad=$d->select("bms_admin_master","admin_id='$created_by'");
                            $adminData=mysqli_fetch_array($qad);
                           echo  $adminData['admin_name'];
                        }  ?></td>
                        <td>
                          <?php if($payment_type==1) {
                            echo "Cheque" .'('.$bank_name.'-'.$payment_ref_no.')';
                          } else if($payment_type==2) {
                            echo "Online" .'('.$bank_name.'-'.$payment_ref_no.')';
                          } else {
                            echo "Cash";
                          }
                          ?>
                        </td>
                        <td>
                          <?php
                              if($created_date!="" && $created_by==$_COOKIE['bms_admin_id'] || $created_date!="" && $adminData['admin_type'] ==1) {
                               $cTime= date("Y-m-d H:i:s");
                              $d1= new DateTime($created_date);
                              $d2= new DateTime($cTime);
                              $interval= $d1->diff($d2);
                              $hoursDifrent=  ($interval->days * 24) + $interval->h;
                              if($hoursDifrent<=48) {
                              ?>
                              <form  action="controller/employeeController.php" method="post" >
                                <input type="hidden" name="expenses_balance_sheet_id_delete" value="<?php echo $expenses_balance_sheet_id; ?>">
                                <input type="hidden" name="emp_salary_master_delete" value="<?php echo $emp_salary_master; ?>">
                                <input type="hidden" name="month_year" value="<?php echo $month_year; ?>">
                                <input type="hidden" name="emp_name" value="<?php echo $row['emp_name']; ?>">
                                <input type="hidden" name="salary_amount_deleted" value="<?php echo $salary_amount; ?>">
                                <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>">
                                <button type="submit" class="btn form-btn btn-danger btn-sm waves-effect waves-light m-1 "><i class="fa fa-trash-o"></i></button>
                                 <a data-toggle="modal" data-target="#updateSalary"  onclick="editSalary('<?php echo $emp_salary_master;?>');" href="#" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> </a>
                              </form>
                            <?php  } } ?>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
                </div>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->


<div class="modal fade" id="addSalary">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Add Salary for <span id="empName"></span></h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php //IS_1686 personal-info TO addSalaryFrm ?>
          <form id="addSalaryFrm" action="controller/employeeController.php" method="post">
            <?php //IS_1750 ?>
            <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
            <input type="hidden" id="emp_id" name="emp_id">
            <input type="hidden" id="empNameForm" name="emp_name">
            <input type="hidden" id="" name="emp_mobile" value="<?php echo $emp_mobile; ?>">
                <div class="form-group row">
                   <label for="input-14" class="col-sm-4 col-form-label">Balance Sheet</label>
                  <div class="col-sm-8">
                    <select type="text" required="" class="form-control" name="balancesheet_id">
                        <option value="">-- Select --</option>
                         <?php
                          error_reporting(0);
                          $q=$d->select("balancesheet_master","society_id='$society_id'","");
                           while ($row11=mysqli_fetch_array($q)) {
                             if ($adminData['admin_type']==1 || $row11['block_id']==0 ||  count($blockAryAccess)==0 ||  in_array($row11['block_id'], $blockAryAccess)) {
                            /*$count=$d->sum_data("received_amount","society_id='$society_id' AND balancesheet_id='$row11[balancesheet_id]' AND society_id='$society_id' AND balancesheet_id='$row11[balancesheet_id]'");
                            while($row=mysqli_fetch_array($count))
                           {
                                $asif=$row['SUM(received_amount)'];
                              $totalMain=number_format($asif,2,'.','');
                            }*/

                            /*$count1=$d->sum_data("received_amount","receive_bill_master","society_id='$society_id' AND balancesheet_id='$row11[balancesheet_id]'");
                            while($row1=mysqli_fetch_array($count1))
                           {
                                $asif1=$row1['SUM(received_amount)'];
                              $totalBill=number_format($asif1,2,'.','');
                            }*/

                            $count2=$d->sum_data("receive_amount","facilitybooking_master","payment_status=0 AND society_id='$society_id' AND balancesheet_id='$row11[balancesheet_id]'");
                            while($row2=mysqli_fetch_array($count2))
                           {
                                $asif2=$row2['SUM(receive_amount)'];
                              $totalFac=number_format($asif2,2,'.','');
                            }
                            $countev=$d->sum_data("recived_amount","event_attend_list","society_id='$society_id' AND balancesheet_id='$row11[balancesheet_id]'");
                                while($rowev=mysqli_fetch_array($countev))
                               {
                                    $asif=$rowev['SUM(recived_amount)'];
                                  $totalEventBooking=number_format($asif,2,'.','');
                                }

                            $count7=$d->sum_data("penalty_amount","penalty_master","paid_status='1' AND society_id='$society_id' AND balancesheet_id='$row11[balancesheet_id]'");
                              while($row=mysqli_fetch_array($count7))
                             {
                                  $asif=$row['SUM(penalty_amount)'];
                                 $totalPenalaty=number_format($asif,2,'.','');
                              }

                             $icnome=$d->sum_data("income_amount","expenses_balance_sheet_master","income_amount!=0 AND society_id='$society_id' AND balancesheet_id='$row11[balancesheet_id]'");
                            while($row2=mysqli_fetch_array($icnome))
                            {
                                $asif5=$row2['SUM(income_amount)'];
                              $totalIncome=number_format($asif5,2,'.','');
                            }
                             $totalIncome= $totalMain+$totalBill+ $totalFac+$totalIncome+$totalEventBooking+$totalPenalaty;

                            $count4=$d->sum_data("expenses_amount","expenses_balance_sheet_master","society_id='$society_id' AND balancesheet_id='$row11[balancesheet_id]'");
                              while($row5=mysqli_fetch_array($count4))
                             {
                                  $asif5=$row5['SUM(expenses_amount)'];
                                $totalExp=number_format($asif5,2,'.','');
                              }

                         ?>
                          <option <?php if($lastData['balancesheet_id']==$row11['balancesheet_id']) { echo 'selected';} ?> value="<?php echo $row11['balancesheet_id'];?>"><?php echo $row11['balancesheet_name'];?> (<?php echo $currency; ?> <?php echo $totalBlance= number_format($totalIncome-$totalExp,2);?>)</option>
                          <?php } }?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <?php
                    $months = array('','January','February','March','April','May','June','July ','August','September','October','November','December');
                    $current = date('m');
                    $prevMonth= $current-1;
                   ?>
                    <label for="input-10" class="col-sm-4 col-form-label">Month <span class="text-danger">*</span></label>
                    <div class="col-sm-8" >
                      <select required="" onchange="getNewDays()" id="month" class="form-control" name="month">
                        <?php for ($j=1; $j <13 ; $j++) {  ?>
                        <option <?php if($j==$prevMonth) { echo 'selected';} ?> value="<?php echo $months[$j]; ?>"><?php echo $months[$j]; ?></option>
                       <?php } ?>
                      </select>
                    </div>
                </div>

                <div class="form-group row">
                  <?php
                    $current = date('m');
                   ?>
                    <label for="input-10" class="col-sm-4 col-form-label">Year <span class="text-danger">*</span></label>
                    <div class="col-sm-8" >
                      <select required="" onchange="getNewDays()" class="form-control" id="year" name="year">
                        <?php for ($j1=0; $j1 <=3 ; $j1++) {  ?>
                        <option  value="<?php echo date('Y')-$j1; ?>"><?php echo date('Y')-$j1; ?></option>
                       <?php } ?>
                      </select>
                    </div>
                </div>
                 <div class="form-group row">
                   <label for="input-10" class="col-sm-4 col-form-label">Month Working Days <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <?php $noOfDays=cal_days_in_month(CAL_GREGORIAN,$prevMonth,date('Y'));?>
                      <input maxlength="10" required="" onkeyup="calcSalary()" value="<?php echo $noOfDays; ?>" type="text" min="1" max="31" id="month_working_days" name="month_working_days"  class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                   <label for="input-10" class="col-sm-4 col-form-label">Working Days <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input  maxlength="10" required="" type="text" onkeyup="getNewSalary()" min="1" max="31" id="working_days" name="working_days"  class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                   <label for="input-10" class="col-sm-4 col-form-label">Leave Days <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input maxlength="10" readonly="" required="" type="text" min="0" max="31" id="leave_days" name="leave_days" id="leave_days" class="form-control">
                      <i id="extraDayNote"></i>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-10" class="col-sm-4 col-form-label">Salary Amount <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input maxlength="20" required="" type="text" id="salary_amount" value="<?php echo $emp_sallary; ?>"  name="salary_amount"  class="form-control">
                      <span>Per Day Salary: <i id="perDaySalary"></i></span>
                      <?php  $perDay= round($emp_sallary/$noOfDays,2);  ?>

                      <input maxlength="20" required="" type="hidden" id="perDayCharge" value="<?php echo $perDay; ?>" id="perDayCharge" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                  <label for="input-10" class="col-sm-4 col-form-label">Expense Category </label>
                  <div class="col-sm-8">
                    <select type="text" class="form-control single-select"  id="expense_category_id" name="expense_category_id">
                      <option value=""> -- Select --</option>
                      <?php
                       $qcc=$d->select("expense_category_master","active_status=0","");
                       while($row=mysqli_fetch_array($qcc))
                       {?>
                      <option <?php if($lastData['expense_category_id']==$row['expense_category_id']) { echo 'selected';}  ?> value="<?php echo $row['expense_category_id']; ?>"><?php echo $row['expense_category_name']; ?></option>
                      <?php  }?>
                    </select>
                  </div>
                </div>
                <input type="hidden" name="is_taxble" value="0">

                <div class="form-group row">
                  <label for="payment_type" class="col-sm-4 col-form-label">Payment Method <span class="required">*</span></label>
                  <div class="col-sm-8">
                    <?php //IS_846  onchange="getBankDetails(this.value)" ?>
                    <select required="" type="text" class="form-control check" id="payment_type" name="payment_type" onchange="getBankDetailsSalary(this.value)" >
                      <option value="">-- Select --</option>
                      <option value="0">Cash</option>
                      <option value="1">Cheque</option>
                      <option value="2">Online Payment</option>
                    </select>
                  </div>
                </div>
                <?php //IS_846 ?>
                <div id="addBankDetails"></div>
          <?php //IS_1686 ?>
               <i>Note:Salary Amount automatically added as a expense in selected balance sheet</i>
                <div class="form-footer text-center">
                  <button type="submit" name="" value="" class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                </div>
          </form>
      </div>
    </div>
  </div>
</div><!--End Modal -->

<div class="modal fade" id="updateSalary">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Update Salary </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="editcomplaintCatDetailsSalary">
      </div>
    </div>
  </div>
</div><!--End Modal -->

<script type="text/javascript">
  function addSalary(emp_id,emp_name) {
     $('#empName').html(emp_name);
     $('#empNameForm').val(emp_name);
     $('#emp_id').val(emp_id);
  }
 function getNewDays() {
     var month= $('#month').val();
     var year= $('#year').val();
     var emp_id= $('#emp_id').val();
      var now = new Date();
      var  newMonth= new Date(Date.parse(month +" 1,"+year)).getMonth()+1;
      var newDays= new Date(year, newMonth, 0).getDate();
      
      $('#month_working_days').val(newDays);
       calcSalary();
 }

 function calcSalary() {
      var month= $('#month').val();
      var year= $('#year').val();
      var emp_id= $('#emp_id').val();
      var motnhWrokingDays= $('#month_working_days').val();
      var now = new Date();
      var newMonth= new Date(Date.parse(month +" 1,"+year)).getMonth()+1;
      var newDays= new Date(year, newMonth, 0).getDate();
      // /IS_1750
      var csrf =$('input[name="csrf"]').val();

       $.ajax({
        url: "calculateSalary.php",
        cache: false,
        type: "POST",
        //IS_1750 ,csrf:csrf
        data: {month : month,year:year,motnhWrokingDays:motnhWrokingDays,emp_id:emp_id,calSalary:'calSalary',csrf:csrf},
        success: function(response){
           var array = response.split("~");
           $('#working_days').val(array[0]);
           $('#leave_days').val(array[1]);
           $('#salary_amount').val(parseFloat(array[2]));
           $('#perDayCharge').val(parseFloat(array[3]));
           $('#perDaySalary').html(parseFloat(array[3]));
        }
      });
 }

 function getNewSalary() {
     var motnhWrokingDays= parseInt($('#month_working_days').val());
     var working_days= parseInt($('#working_days').val());
     var perDayCharge= parseFloat($('#perDayCharge').val());
    if (working_days!=0) {
      if (true) {
         var newsalary = perDayCharge*working_days;
         var leave_days = motnhWrokingDays-working_days;
         if (working_days>motnhWrokingDays) {
         //       var newsalary = perDayCharge*motnhWrokingDays;
               var extraDays = working_days-motnhWrokingDays;
               $('#extraDayNote').html(extraDays+' extra working days');
         //       $('#leave_days').val(0);
         //       var newsalary =Math.round(newsalary);
         //       $('#salary_amount').val(parseFloat(newsalary));
               $('#leave_days').val(0);
         //    var alertmasg ='Set Working Below '+ motnhWrokingDays+' Days';
         //    swal(alertmasg, {icon: "warning",});
          } else {
               $('#leave_days').val(leave_days);
          }
             $('#perDaySalary').html(parseFloat(perDayCharge));
               var newsalary =Math.round(newsalary);
              $('#salary_amount').val(parseFloat(newsalary));
       }
     } else {
      swal('Need Minimum 1 Working Day', {icon: "warning",});
     }
}
</script>