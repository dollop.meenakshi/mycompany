<?php error_reporting(0);

$bId = (int)$_GET['bId'];
$dId = (int)$_GET['dId'];
$uId = (int)$_GET['uId'];
$allow_wfh = $_GET['allow_wfh'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-6 col-6">
                <h4 class="page-title"> Work From Home Setting</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
            <div class="btn-group float-sm-right">
            <?php if($allow_wfh == '0'){ ?>
                <a href="javascript:void(0)" onclick="allowMultiWFH(1)" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-check-square-o mr-1"></i> WFH ON</a>
            <?php }elseif($allow_wfh == '1'){ ?>
                <a href="javascript:void(0)" onclick="allowMultiWFH(0)" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-check-square-o mr-1"></i> WFH OFF</a>
            <?php } ?>
            </div>
            </div>
        </div>   
        <form class="branchDeptFilter" action="" method="get">
            <div class="row pt-2 pb-2">
				<?php include 'selectBranchDeptEmpForFilter.php'; ?>
                <div class="col-md-2 form-group">
                    <select name="allow_wfh" class="form-control">
                        <option <?php if($allow_wfh == 'all'){echo 'selected';} ?> value="all">All</option>
                        <option <?php if($allow_wfh == '0'){echo 'selected';} ?> value="0">WFH OFF</option>
                        <option <?php if($allow_wfh == '1'){echo 'selected';} ?> value="1">WFH ON</option>
                    </select>         
                </div>
				<div class="col-md-1 form-group">
				<button class="btn btn-sm btn-success" type="submit" name="getReport" class="form-control"><i class="fa fa-search" aria-hidden="true"></i></button>
				</div>
            </div>
        </form>
    

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <?php if((isset($bId) && $bId >0) && (isset($dId) && $dId >0) )
                            {
                            ?>
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <?php if($allow_wfh == '0' || $allow_wfh == '1'){ ?>
                                        <th><input type="checkbox" name="" class="selectAll" value=""></th>
                                        <?php } ?>
                                        <th>User Name</th>
                                        <th>Department</th>
                                        <th>Allow WFH</th>
                                    
                                    </tr>
                                </thead>
                                <tbody id="showFilterData">
                                    <?php
                                        if(isset($uId) && $uId > 0) {
                                            $userFilterQuery = " AND users_master.user_id=$uId";
                                        }
                                        if(isset($dId) && $dId > 0) {
                                            $dIdFilterQuery = " AND floors_master.floor_id=$dId";
                                        }
                                        if(isset($allow_wfh) && $allow_wfh != 'all') {
                                            $wfhAllowQuery = " AND users_master.allow_wfh=$allow_wfh";
                                        }
                                    $q = $d->select("users_master,block_master,floors_master", "users_master.floor_id = floors_master.floor_id AND  block_master.block_id=floors_master.block_id AND users_master.society_id='$society_id' AND users_master.delete_status= 0 $wfhAllowQuery $dIdFilterQuery $userFilterQuery $blockAppendQuery");
                                    $counter = 1;
                                    while ($data = mysqli_fetch_array($q)) {
                                    ?>
                                        <tr>
                                            <td><?php echo $counter++; ?></td>
                                            <?php if($allow_wfh == '0' || $allow_wfh == '1'){ ?>
                                            <td>
                                                <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['user_id']; ?>">    
                                            </td>
                                            <?php } ?>
                                            <td><?php echo $data['user_full_name']; ?></td>
                                            <td><?php echo $data['floor_name']; ?></td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <?php if ($data['allow_wfh'] == "0") {
                                                        $status = "";
                                                        $activeDeactive="wfhOn";
                                                    ?>
                                                    <?php } else { 
                                                        $status = "checked";
                                                        $activeDeactive="wfhOff";

                                                    } ?>
                                                    <input type="checkbox" <?php echo $status; ?> class="js-switch" data-color="#15ca20" onchange="changeStatus('<?php echo $data['user_id']; ?>','<?php echo $activeDeactive;  ?>');" data-size="small" />
                                                </div>
                                            </td>
                                        

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            
                            </table>
                            <?php }else
                            { ?>
                            <div class="" role="alert">
                                <span><strong>Note :</strong> Please Select Department</span>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->