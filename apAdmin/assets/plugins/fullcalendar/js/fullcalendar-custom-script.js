
/* eventArray = [];
 $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {user_id :$('#user_attendance_id').val(),
                  action:"get_attendance_by_user",
                  society_id: $('#society_id').val()},
            success: function(response){
              var jasonObj = "";
              var jasonObj2 = "";
              var jasonObj3 = "";
              $('#total_month_hours').text(response.total_hours);
              $.each(response.attendance, function( index, value ) {
                if(value.work_report !="")
                {
                  var work_report = value.work_report;
                }
                else
                {
                  var work_report = "No Report";

                }
                jasonObj3 = {
                            "title":"Working Day("+work_report+")",
                            "start":value.attendance_date_start,
                            "end":value.attendance_date_start,
                            "eventBackgroundColor":"#fff41c",
                          

                          }
               eventArray.push(jasonObj3);
              
              });
              $.each(response.holiday, function( index, value ) {
                jasonObj = {"title":value.holiday_name,
                            "start":value.holiday_start_date,
                            "end":value.holiday_end_date,
                            "color":"Yellow",
                            "BackgroundColor":"red",
                          }
               eventArray.push(jasonObj);
              
              });
              $.each(response.leave, function( index, value ) {
                if(value.leave_day_type==0)
                {
                  value.leave_day_type = "Full-Day"
                }
                else
                {
                  value.leave_day_type = "Half-Day"

                }
                jasonObj2 = {"title":value.leave_type_name+ '('+(value.leave_day_type)+')',
                            "start":value.leave_start_date,
                            "end":value.leave_end_date,
                          }
               eventArray.push(jasonObj2);
              });

              main(eventArray);
            }

            
  });
 */
  function main(event)
  {
    $(document).ready(function() {


      var crntData=$('#crntData').val();
     
        $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
         // defaultDate: '2018-03-12',
          defaultDate: crntData,
          navLinks: true, // can click day/week names to navigate views
          selectable: true,
          selectHelper: true,
          /* select: function(start, end) {
            var title = prompt('Event Title:');
            var eventData;
            if (title) {
              eventData = {
                title: title,
                start: start,
                end: end
              };
              $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
            }
            $('#calendar').fullCalendar('unselect');
          }, */
          dayClick: function(date, jsEvent, view){
           // console.log(date._d); //day of the week: 1 for monday
           // console.log(jsEvent); //day of the week: 1 for monday
           // console.log(view); //day of the week: 1 for monday
           // var date = calendar.getDate();
           let dates = new Date(date._d);

          let dd = dates.getDate();
          let mm = dates.getMonth() + 1;
          let yyyy = dates.getFullYear();

       //   console.log(yyyy + "-" + mm + "-" + dd);

            getTodayData(yyyy + "-" + mm + "-" + dd)
          },
          editable: false,
          eventLimit: false, // allow "more" link when too many events
          events: event /* [
            {
              title: 'All Day Event',
              start: '2021-10-01'
            },
            {
              title: 'Long Event',
              start: '2021-10-01',
              end: '2021-10-10'
            },
            {
              id: 999,
              title: 'Repeating Event',
              start: '2018-03-09T16:00:00'
            },
            {
              id: 999,
              title: 'Repeating Event',
              start: '2018-03-16T16:00:00'
            },
            {
              title: 'Conference',
              start: '2018-03-11',
              end: '2018-03-13'
            },
            {
              title: 'Meeting',
              start: '2018-03-12T10:30:00',
              end: '2018-03-12T12:30:00'
            },
            {
              title: 'Lunch',
              start: '2018-03-12T12:00:00'
            },
            {
              title: 'Meeting',
              start: '2018-03-12T14:30:00'
            },
            {
              title: 'Happy Hour',
              start: '2018-03-12T17:30:00'
            },
            {
              title: 'Dinner',
              start: '2018-03-12T20:00:00'
            },
            {
              title: 'Birthday Party',
              start: '2018-03-13T07:00:00'
            },
            {
              title: 'Click for Google',
              url: 'http://google.com/',
              start: '2018-03-28'
            }
          ] */
        });
    
      });
  }

var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = yyyy + '-' + mm + '-' + dd;
//getTodayData(today)

  /* function getTodayData(date){
    var showData ="";
    $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {user_id :$('#user_attendance_id').val(),
            date:date,
            action:"get_attendance_today_attendance_data",
            society_id: $('#society_id').val()},
      success: function(response){
        //console.log(response);
        var date2 = new Date(date);
        date = date2.toDateString();
       if(response.status ==200)
       {
           showData += `<div style="width: 60px; position: relative;z-index: 950;left: 0;margin: 5px;" class="badge "></div>             
                <div class="card-body pt-2">
                  <h5 class="card-title">`+date+`</h5>
                  <h6 class="card-title">Total Hours :`+response.data.total_working+`</h6>
                  <h5 class="card-title">Report:`+response.data.work_report_title+`</h5>
                  <h6 class="">`+response.data.work_report+`</h6>
                  <h5 class="card-title">Punch In:`+response.data.punch_in_time+`</h5>
                  <h5 class="card-title">Punch Out:`+response.data.punch_out_time+`</h5>`;
                  if(response.data.attedance.length>0)
                  {
                      $.each(response.data.attedance, function( index, value ) {
                      showData += `<h5 class="card-title">Break:`+value.attendance_type_name+`</h5>
                      <h6 class="">Break In:`+value.break_in_time+`</h6>
                      <h6 class="">Break Out:`+value.break_out_time+`</h6>
                      <h6 class="">`+value.total_break+`</h6>`;
                      });

                  }

                  showData +=  `</div>
                  </div>
                  </div>`;
                  //console.log(showData);
       }
       else
       {
        showData = `<div style="width: 60px; position: relative;z-index: 950;left: 0;margin: 5px;" class="badge "></div>             
                        <div class="card-body pt-2">
                            Todays Report !
                        </div>
                     </div>`;
       }
        
      $('#todayData').html(showData);

      }
    });
  } */

