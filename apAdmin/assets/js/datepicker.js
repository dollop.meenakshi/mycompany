var date = new Date();
 date.setDate(date.getDate());

  $('input.timepicker').timepicker({
    startTime: '09:00',
  });
  $('.maximum_halfday_hours').timepicker({
    startTime: '01:00',
    endTime: '13:00',
    timeFormat: 'h:mm',
    interval : 15,
  });
  $('.minimum_hours_for_full_day').timepicker({
    startTime: '01:00',
    endTime: '13:00',
    timeFormat: 'h:mm',
    interval : 15,
    //change: changesMinimumFullDayHours(this.value)
  });
  $('.changeShiftInTIme').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
  })
  
  
  $('.changeShiftOutTIme').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
    
  })

  $('.max_punch_out_time').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
    
  })
  
  $('.halfday_before_time').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
    
  })
  $('.half_day_time_starts').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
    
  })
  $('.lunch_break_start_time').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
    
  })
  $('.tea_break_start_time').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
    
  })
  $('.tea_break_end_time').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
    
  })
  $('.lunch_break_end_time').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
  })
$('.tea_break_end_time').change(function () {
  tea_break_end_time = this.value;
  shift_start_time =   $('#shift_start_time').val();
  shift_end_time = $('#shift_end_time').val();
  tea_break_start_time = $('.tea_break_start_time').val();
  if (shift_end_time != "") {
    if (shift_start_time < shift_end_time) {
      if (tea_break_end_time < shift_start_time) {
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.tea_break_end_time').val('');
      }
      if (tea_break_end_time > shift_end_time) {
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.tea_break_end_time').val('');
  
      }
      if (tea_break_start_time != "") {
       
        if(tea_break_start_time>tea_break_end_time)
        {
          swal("Please set time between Greater Then Tea Break Start time", {
            icon: "warning",
          });
          $('.tea_break_end_time').val('');
        }
      }
    }
    else
    {
      if(shift_end_time<half_day_time_starts && shift_start_time>half_day_time_starts){
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.tea_break_end_time').val('');
      }
      if(tea_break_start_time > tea_break_end_time && tea_break_start_time >"00:00" && shift_end_time > tea_break_end_time )
        {
          swal("Please set time between Greater Then Lunch Start time", {
            icon: "warning",
          });
          $('.tea_break_end_time').val('');
        }
    }
    
  }
  else {
    swal("Please Select Shift End Time Or Start Time", {
      icon: "warning",
    });
    $('.lunch_break_start_time').val('');

  }
});
$('.tea_break_start_time').change(function () {
  half_day_time_starts = this.value;
  shift_start_time =   $('#shift_start_time').val();
  shift_end_time = $('#shift_end_time').val();
  if (shift_end_time != "") {
    
    if (shift_start_time < shift_end_time) {
      if (half_day_time_starts < shift_start_time) {
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.tea_break_start_time').val('');
      }
      if (half_day_time_starts > shift_end_time) {
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.tea_break_start_time').val('');
  
      }
    }
    else
    {
      if(shift_end_time<half_day_time_starts && shift_start_time>half_day_time_starts){
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.tea_break_start_time').val('');
      }
    }
  }
  else {
    swal("Please Select Shift End Time Or Start Time", {
      icon: "warning",
    });
    $('.tea_break_start_time').val('');
  }
});
$('.lunch_break_start_time').change(function () {
  half_day_time_starts = this.value;
  shift_start_time =   $('#shift_start_time').val();
  shift_end_time = $('#shift_end_time').val();
  lunch_break_end_time = $('#lunch_break_end_time').val();
  if (shift_end_time != "") {
    
    if (shift_start_time < shift_end_time) {
      if (half_day_time_starts < shift_start_time) {
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.lunch_break_start_time').val('');
      }
      if (half_day_time_starts > shift_end_time) {
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.lunch_break_start_time').val('');
  
      }
      
    }
    else
    {
      if(shift_end_time<half_day_time_starts && shift_start_time>half_day_time_starts){
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.lunch_break_start_time').val('');
      }
    }
  } else {
    swal("Please Select Shift End Time Or Start Time", {
      icon: "warning",
    });
    $('.lunch_break_start_time').val('');

  }
});
$('.lunch_break_end_time').change(function () {
  lunch_break_end_time = this.value;
  shift_start_time =   $('#shift_start_time').val();
  lunch_break_start_time =   $('.lunch_break_start_time').val();
  shift_end_time = $('#shift_end_time').val();
  if (shift_end_time != "") {
    if (shift_start_time < shift_end_time) {
      if (lunch_break_end_time < shift_start_time) {
        swal("Please set time between shift start and end time", {
          icon: "warning",
        });
        $('.lunch_break_end_time').val('');
      }
      if (lunch_break_end_time > shift_end_time) {
        swal("Please set time between shift start and end time", {
          icon: "warning",
        });
        $('.lunch_break_end_time').val('');
      }
      if (lunch_break_start_time != "") {
        if(lunch_break_start_time>lunch_break_end_time)
        {
          swal("Please set time between Greater Then Lunch Start time", {
            icon: "warning",
          });
          $('.lunch_break_end_time').val('');
        }
      }
    }
    else
    {
      if(shift_end_time<lunch_break_end_time && shift_start_time>lunch_break_end_time){
        swal("Please set time between shift start and end time", {
          icon: "warning",
        });
        $('.lunch_break_end_time').val('');
      }
      if(lunch_break_start_time > lunch_break_end_time && lunch_break_start_time >"00:00" && shift_end_time > lunch_break_end_time )
        {
          swal("Please set time between Greater Then Lunch Start time", {
            icon: "warning",
          });
          $('.lunch_break_end_time').val('');
        }
      
    }
  }else {
    swal("Please Select Shift End Time Or Start Time", {
      icon: "warning",
    });
    $('.lunch_break_start_time').val('');

  }
  
});
  
$('.half_day_time_starts').change(function () {
  half_day_time_starts = this.value;
    shift_start_time =   $('#shift_start_time').val();
    shift_end_time =   $('#shift_end_time').val();
    var startTime = moment(shift_start_time , 'hh:mm:ss');
    var endTime = moment(shift_end_time, 'hh:mm:ss');
    var totalMinutes = endTime.diff(startTime, 'minutes');
 ////////minutes to hours calculation 
      var num = totalMinutes;
      var hours = (num / 60);
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
  if (shift_start_time < shift_end_time) {
    if (half_day_time_starts < shift_start_time) {
          swal("Please set  time between shift start and end time", {
            icon: "warning",
          });
      $('.half_day_time_starts').val('');
    }
    if (half_day_time_starts > shift_end_time) {
      swal("Please set  time between shift start and end time", {
        icon: "warning",
      });
      
      $('.half_day_time_starts').val('');

  }
     
  }
  else
    {
      if(shift_end_time<half_day_time_starts && shift_start_time>half_day_time_starts){
        swal("Please set  time between shift start and end time", {
          icon: "warning",
        });
        $('.half_day_time_starts').val('');
      }
    }
    
  })
  $('.minimum_hours_for_full_day').timepicker('option', 'change', function(time) {
    shift_start_time =   $('#shift_start_time').val();
    shift_end_time =   $('#shift_end_time').val();
    var startTime = moment(shift_start_time , 'hh:mm:ss');
    var endTime = moment(shift_end_time, 'hh:mm:ss');
    if (shift_start_time > shift_end_time) { 
      var timeStart = new Date("01/01/2022 " + shift_start_time).getHours();
      var timeEnd = new Date("02/01/2022 " + shift_end_time).getHours();
      var timeStartMi = new Date("01/01/2022 " + shift_start_time).getMinutes();
      var timeEndMin = new Date("02/01/2022 " + shift_end_time).getMinutes();
      var hourDiff = timeEnd - timeStart;  
      var MinDiff = timeEndMin - timeStartMi;  
      hourDiff = parseInt(hourDiff);
      MinDiff = parseInt(MinDiff);
      console.log(MinDiff);
      if (MinDiff == 0) {
        MinDiff = "00";
      }
      else
      {
        if (MinDiff < 10) {
          MinDiff = "0" + MinDiff;
        }
        else
        {
          MinDiff = MinDiff;
          }
      }
      shift_diffrance_hours = hourDiff + parseInt(24)+":" + MinDiff;;
    }
    else
    {
      var totalMinutes = endTime.diff(startTime, 'minutes');
      var num = totalMinutes;
      var hours = (num / 60);
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      if (rminutes == 0) {
        rminutes = "00";
      } else
      {
        rminutes = parseInt(rminutes);
      }
      if (rminutes < 10) {
        rminutes = "0" + rminutes;
      }
      shift_diffrance_hours = (rhours + ":" + rminutes);
    }
    
   /////////////////////minutes to hours calculation 
   minimum_hours = $('.minimum_hours_for_full_day').val();
   
   if(parseFloat(shift_diffrance_hours)<=parseFloat(minimum_hours))
   {
     if (parseFloat(minimum_hours) == parseFloat(shift_diffrance_hours)) {
       shift_diffrance_hours = shift_diffrance_hours.split(':');
       minimum_hours = minimum_hours.split(':');
       console.log(shift_diffrance_hours);
       console.log(minimum_hours);
       if (minimum_hours[1] > shift_diffrance_hours[1])
       {
         $('.minimum_hours_for_full_day').val("");
         swal("No More Then Shift Total Hours ", {
           icon: "warning",
         });
       }
     }
     else {
       $('.minimum_hours_for_full_day').val("");
       swal("No More Then Shift Total Hours ", {
         icon: "warning",
       });
     }
   }
    
  });
  $('.maximum_halfday_hours').timepicker('option', 'change', function(time) {
    shift_start_time =   $('#shift_start_time').val();
    shift_end_time =   $('#shift_end_time').val();
    minimum_hours = $('.maximum_halfday_hours').val();
    var startTime = moment(shift_start_time , 'HH:mm:ss');
    var endTime = moment(shift_end_time, 'HH:mm:ss');
     
    if (shift_start_time > shift_end_time) { 
      var timeStart = new Date("01/01/2022 " + shift_start_time).getHours();
      var timeEnd = new Date("02/01/2022 " + shift_end_time).getHours();
      var timeStartMi = new Date("01/01/2022 " + shift_start_time).getMinutes();
      var timeEndMin = new Date("02/01/2022 " + shift_end_time).getMinutes();
      var hourDiff = timeEnd - timeStart;  
      var MinDiff = timeEndMin - timeStartMi;  
      hourDiff = parseInt(hourDiff);
      MinDiff = parseInt(MinDiff);
     
      if (MinDiff == 0) {
        MinDiff = "00";
      }
      else
      {
        if (MinDiff < 10) {
          MinDiff = "0" + MinDiff;
        }
        else
        {
          MinDiff = MinDiff;
        }
      }
      shift_diffrance_hours = hourDiff + parseInt(24) + ":" + MinDiff;;
     
    }
    else
    {
      var totalMinutes = endTime.diff(startTime, 'minutes');
      var num = totalMinutes;
      var hours = (num / 60);
     
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      if (rminutes == 0) {
        rminutes = "00";
      } else
      {
        rminutes = parseInt(rminutes);
      }
      if (rminutes < 10) {
        rminutes = "0" + rminutes;
      }
      shift_diffrance_hours = (rhours + ":" + rminutes);
    }
  
    if(parseFloat(shift_diffrance_hours)<=parseFloat(minimum_hours))
    {
      if (parseFloat(minimum_hours) == parseFloat(shift_diffrance_hours)) {
        shift_diffrance_hours = shift_diffrance_hours.split(':');
        minimum_hours = minimum_hours.split(':');
        console.log(shift_diffrance_hours);
        console.log(minimum_hours);
        if (minimum_hours[1] > shift_diffrance_hours[1])
        {
          $('.maximum_halfday_hours').val("");
          swal("No More Then Shift Total Hours ", {
            icon: "warning",
          });
        }
      }
      else {
        $('.maximum_halfday_hours').val("");
        swal("No More Then Shift Total Hours ", {
          icon: "warning",
        });
      }
    }
    
  });
  $('.halfday_before_time').change(function () {
    halfday_before_time = this.value;
    shift_start_time =   $('#shift_start_time').val();
    shift_end_time =   $('#shift_end_time').val();
   
  if (shift_start_time < shift_end_time) {
    if (halfday_before_time < shift_start_time) {
      swal("Please set  time between shift start and end time", {
        icon: "warning",
      });
      $('.halfday_before_time').val('');
    }
    if (halfday_before_time > shift_end_time) {
      swal("Please set  time between shift start and end time", {
        icon: "warning",
      });
      $('.halfday_before_time').val('');
    }
  }else
  {
    if(shift_end_time<halfday_before_time && shift_start_time>halfday_before_time){
      swal("Please set  time between shift start and end time", {
        icon: "warning",
      });
      $('.halfday_before_time').val('');
    }
  }
    
  });
  $('.sun,.mon,.tue,.wed,.thu,.fri,.sat').timepicker({
      // timeFormat : 'hh:mm a',
      interval : 30,
      minTime : '00:00 AM',
      maxTime : '11:00 PM',
      startTime : '04:00 AM',
      dynamic : false,
      dropdown : true,
      scrollbar : true
  });

  $('.Sunday,.Monday,.Tuesday,.Wednesday,.Thursday,.Friday,.Saturday').timepicker({
      // timeFormat : 'hh:mm a',
      interval : 30,
      minTime : '00:00 AM',
      maxTime : '11:00 PM',
      startTime : '04:00 AM',
      dynamic : false,
      dropdown : true,
      scrollbar : true
  });

  $('.sun').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 60 * 60 * 1000));
    $('.sun1').timepicker('option', 'minTime', later);
    $('.sun1').timepicker('setTime', later);
  });

  $('.mon').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 60 * 60 * 1000));
    $('.mon1').timepicker('option', 'minTime', later);
    $('.mon1').timepicker('setTime', later);
  });
   $('.tue').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 60 * 60 * 1000));
    $('.tue1').timepicker('option', 'minTime', later);
    $('.tue1').timepicker('setTime', later);
  });
    $('.wed').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 60 * 60 * 1000));
    $('.wed1').timepicker('option', 'minTime', later);
    $('.wed1').timepicker('setTime', later);
  });
     $('.thu').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 60 * 60 * 1000));
    $('.thu1').timepicker('option', 'minTime', later);
    $('.thu1').timepicker('setTime', later);
  });
      $('.fri').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 60 * 60 * 1000));
    $('.fri1').timepicker('option', 'minTime', later);
    $('.fri1').timepicker('setTime', later);
  });
       $('.sat').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 60 * 60 * 1000));
    $('.sat1').timepicker('option', 'minTime', later);
    $('.sat1').timepicker('setTime', later);
  });

  $('.sun1,.mon1,.tue1,.wed1,.thu1,.fri1,.sat1').timepicker({
      // timeFormat : 'hh:mm a',s
      interval : 30,
      minTime : '04:00 AM',
      maxTime : '11:30 PM',
      startTime : '04:00 AM',
      dynamic : false,
      dropdown : true,
      scrollbar : true
  });

   $('.Sunday1,.Monday1,.Tuesday1,.Wednesday1,.Thursday1,.Friday1,.Saturday1').timepicker({
      // timeFormat : 'hh:mm a',
      interval : 30,
      minTime : '00:00 AM',
      maxTime : '11:00 PM',
      startTime : '04:00 AM',
      dynamic : false,
      dropdown : true,
      scrollbar : true
  });

  $('.Sunday1,.Monday1,.Tuesday1,.Wednesday1,.Thursday1,.Friday1,.Saturday1').timepicker('option', 'change', function(time) {
     $('.fullDivSunday').remove();
  });

  $('.Monday1').timepicker('option', 'change', function(time) {
     $('.fullDivMonday').remove();
  });
  $('.Tuesday1').timepicker('option', 'change', function(time) {
     $('.fullDivTuesday').remove();
  });
  $('.Wednesday1').timepicker('option', 'change', function(time) {
     $('.fullDivWednesday').remove();
  });
  $('.Thursday1').timepicker('option', 'change', function(time) {
     $('.fullDivThursday').remove();
  });
  $('.Friday1').timepicker('option', 'change', function(time) {
     $('.fullDivFriday').remove();
  });
  $('.Saturday1').timepicker('option', 'change', function(time) {
     $('.fullDivSaturday').remove();
  });

   $('.Sunday').timepicker('option', 'change', function(time) {
     $('.fullDivSunday').remove();
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
    $('.Sunday1').timepicker('option', 'minTime', later);
    $('.Sunday1').timepicker('setTime', later);
  });

  $('.Monday').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
    $('.Monday1').timepicker('option', 'minTime', later);
    $('.Monday1').timepicker('setTime', later);
  });
   $('.Tuesday').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
    $('.Tuesday1').timepicker('option', 'minTime', later);
    $('.Tuesday1').timepicker('setTime', later);
  });
    $('.Wednesday').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
    $('.Wednesday1').timepicker('option', 'minTime', later);
    $('.Wednesday1').timepicker('setTime', later);
  });
     $('.Thursday').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
    $('.Thursday1').timepicker('option', 'minTime', later);
    $('.Thursday1').timepicker('setTime', later);
  });
      $('.Friday').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
    $('.Friday1').timepicker('option', 'minTime', later);
    $('.Friday1').timepicker('setTime', later);
  });
       $('.Saturday').timepicker('option', 'change', function(time) {
    var selectedaty = $(this).attr("name");
    var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
    $('.Saturday1').timepicker('option', 'minTime', later);
    $('.Saturday1').timepicker('setTime', later);
  });

 $('#default-datepicker').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true
});

 $(".date-picker-month-year").datepicker( {
    autoclose: true,
    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months"
  });

$('#default-datepicker_salary_start').datepicker({
  format: 'yyyy-mm-dd',
  autoclose: true
  //minDate: $('#salary_prv_date').val()
});

$('#salary_min_date').datepicker({
  format: 'yyyy-mm-dd',
  autoclose: true,
  startDate: $('#lastSalaryEndDate').val(),
});

$('#default-datepicker_salary_Increment').datepicker({
  format: 'yyyy-mm-dd',
  autoclose: true
});
 $('#default-datepicker-1').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true
 // minDate: new Date(2021, 12 - 1, 25),
  // minDate:  $('.startDateHoliday').val()
});

 $('.facility_datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    todayHighlight: true,
    // startDate: date,
  });

 $('.facility_datepicker').change(function() {
     var no_of_person= parseInt($('#no_of_person').val());
      var facility_id= $('#facility_id').val();
      var facility_type= $('#facility_type').val();
      var book_date= $('.facility_datepicker').val();
      var user_id= $('#user_id').val();
      if (user_id!='' &&  no_of_person>0) {
      $('.mothListDiv').html(' ');
        $.ajax({ 
        url: "getFacilityTimeSlot.php",
        cache: false,
        type: "POST",
          data: {user_id:user_id,no_of_person:no_of_person,facility_id : facility_id, facility_type : facility_type,book_date:book_date},
          success: function(response){
           $('#facilityTimeSlotCheck').html(response);
           $('.facility_amount').val('');

           }
        });
      } else {
        swal("Please Select Booking For & No Of Person!", {
                              icon: "warning",
                            });
        $('.facility_datepicker').val('');
      }

 });
 $('#autoclose-datepicker,#autoclose-datepicker1').datepicker({
  autoclose: true,
  todayHighlight: true,
  format: 'yyyy-mm-dd'
});

$('.housie_start_date').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    todayHighlight: true,
    startDate: date,
  });

      //IS_579
      $('.expense-income-cls').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
      });
      $('.expense-income-cls').datepicker('setEndDate', date);
      //IS_579
      

      $('#autoclose-datepicker,#autoclose-datepicker2').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: date,
        format: 'yyyy-mm-dd'
      });

      $('#autoclose-datepicker-dob').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: date,
      });
      $('#wedding_anniversary_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: date,
      });

      $('#autoclose-datepicker-doj').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: date,
      });

      $('#datepicker-doj').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: date,
      });

      $('#datepicker-doa').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: date,
      });

      $('#datepicker-wf').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: date,
       
      });
      $('#new_datepicker-wf').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate: $('#joining_date_validate').val(),
      });
      $('#new_datepicker-wf-2').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: $('#joining_date_validate').val(),
      });

      
      $('#datepicker-wt').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: $('#joining_date_validate').val(),
      });
      $('#new_datepicker-wt').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: $('#joining_date_v').val(),
      });


      $('.valid-till').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: date,
        format: 'yyyy-mm-dd'
      });

      $('.autoclose-datepicker-item-maintenance').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: date,
        format: 'yyyy-mm-dd'
      });
      //19march2020
      var maxdate = new Date(); //get current date
      maxdate.setDate(maxdate.getDate() + 30);
      $('.valid-till').datepicker("setDate", maxdate );

      $('#autoclose-datepickerFrom,#autoclose-datepickerTo').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: '+365d',
      });

      $('#autoclose-datepickerFromMen').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: date,
      });

      $('#inline-datepicker').datepicker({
       todayHighlight: true
     });

      $('#dateragne-picker .input-daterange').datepicker({
       autoclose: true,
       /*startDate: date,*/
       format: 'yyyy-mm-dd',
         //IS_805 28fe2020
         todayHighlight: true,
       });

      $('#dateragne-pickerNew .input-daterange').datepicker({
        //IS_805 28feb2020
        todayHighlight: true,
        autoclose: true,
        endDate: date,
        format: 'yyyy-mm-dd',
        minDate: '-30d',
      });

      //11march2020 IS_1131
      var mindate = new Date();  
      mindate.setDate(mindate.getDate() - 1095);
      $('#dateragne-pickerBill .input-daterange').datepicker({
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        autoclose: true,
        
        startDate: mindate,
        maxDate: '0'

      });
      //11march2020 IS_1131

      /// 05 Jan 2022 For Issue IS_4035
      $('.autoclose-datepicker-item').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        endDate: date,
      });
      $('#loan_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        
      });
      $('#received_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        
      });




// Material Datekpicer code

var FromEndDate = new Date();

 $('#event-start-date').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD',
    minDate: FromEndDate,
    time: false,
  });
  $('#event-end-date').focus(function() {
    var startDate = $('#event-start-date').val();
    if (startDate=='') {
       swal("Please select Start Date first.!", {
                            icon: "warning",
                          });
    } 
  });

 //28march2020
  //replace this code with below code
  $('#event-start-date').change(function() {
    var startDate = $('#event-start-date').val();
    var endDate = $('#event-end-date').val();
    // if (endDate=='') {
      $('#event-end-date').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        minDate: startDate,
        time: false,
      });
    // }
  });
    
    /*var eventStartDate = $('#event-start-date').val();
    if (eventStartDate !='') {
        $('#event-end-date').bootstrapMaterialDatePicker({
          format: 'YYYY-MM-DD',
          minDate: eventStartDate, 
          time: false,
        });
    }*/

  

    // $('#event-start-date').change(function() {
    //   var startDate = $('#event-start-date').val();
    //   // $('#event-end-date').val(startDate);
    // });
 //28march2020

// facility

  $('.facility-start-date').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
  });

  $('.facility-end-date').focus(function() {
    var startDate = $('.facility-start-date').val();
    // alert(startDate);
    if (startDate=='') {
      swal("Please select Start Time first", {
        icon: "warning",
      });
    } 
  });
  $('.facility-start-date').change(function() {
    var startDate = $('.facility-start-date').val();
    $('.facility-end-date').val(startDate);
    $('.facility-end-date').bootstrapMaterialDatePicker({
      date: false,
      format: 'HH:mm',
      minDate: startDate,
    });
  });


  // dat time picker
        $('#date-time-picker').bootstrapMaterialDatePicker({
          format: 'YYYY-MM-DD HH:mm',
          maxDate: FromEndDate
        });

        

       // only date picker
       $('#date-picker').bootstrapMaterialDatePicker({
        time: false
      });

//19march2020
       // only time picker
       $('.time-picker').bootstrapMaterialDatePicker({
        date: false,
        format: 'hh:mm A'
      });

        // only time picker
        $('.time-picker1').bootstrapMaterialDatePicker({
          date: false,
          format: 'hh:mm A'
        });

    $('.time-picker-end').bootstrapMaterialDatePicker({
          date: false,
          format: 'hh:mm A',
        });

    /* Dollop Infotect date 22/oct/2021 */
        $('.time-picker-shft').bootstrapMaterialDatePicker({
          date: false,
          format: 'HH:mm',
          
        })
        $('.time-picker-shft-Out_update').bootstrapMaterialDatePicker({
          date: false,
          format: 'HH:mm',
          
        })
        
        
        $('.punch_out_time_change').bootstrapMaterialDatePicker({
          date: false,
          shortTime: true,
          twelvehour: false,
          format: 'HH:mm'
          
        })
        
        
     /* Trigger on load or any where */
     $('#time-picker-shft').trigger('changeDate');
        $('#half_day_time_start').bootstrapMaterialDatePicker({
          date: false,
          format: 'HH:mm'
        });
/* Dollop Infotect date 22/oct/2021 */

     $('.FromDate').datepicker({
  weekStart: 1,
/*startDate: firstDay,*/
  format: 'yyyy-mm-dd', 
  autoclose: true,
  endDate: $('#ToDate').val()
})
.on('changeDate', function (selected) {
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('.ToDate').val();
        $('.ToDate').datepicker('setStartDate', startDate);
    });
$('.ToDate')
    .datepicker({
        weekStart: 1,
        /*startDate: firstDay,*/
         format: 'yyyy-mm-dd',
        autoclose: true
    })
    .on('changeDate', function (selected) {
        FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('.FromDate').datepicker('setEndDate', FromEndDate);
    });


$('.unit_reading-datepicker').datepicker({
  format: 'dd M yyyy',
   endDate: date,
  autoclose: true,


});

  //1oct2020
    $('#date-time-picker-rem').bootstrapMaterialDatePicker({
          format: 'YYYY-MM-DD HH:mm:ss',
          minDate: FromEndDate
        });
    //1oct2020

 /*    $('#default-datepicker-holiday_start_date').datepicker({
      format: 'yyyy-mm-dd',
    
    });
    $('#default-datepicker-holiday_end_date').datepicker({
      format: 'yyyy-mm-dd',
      maxDate: "+2m +1w"
    }); */

    $( "#default-datepicker-holiday_start_date" ).datepicker({
     // startDate: '-3d',
     format: 'yyyy-mm-dd',
      
    })
    $( ".default-datepicker-holiday_start_date" ).datepicker({
     // startDate: '-3d',
     format: 'yyyy-mm-dd',
      
    })
    $( "#default-datepicker-holiday_end_date" ).datepicker({
      // startDate: '-3d',
      format: 'yyyy-mm-dd',
       
     })

    $('.datepicker_task_due_date').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
      startDate: date,
    });

    $('#advance_salary_date').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
     
    });

    $('.datepicker_task_step_due_date').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
      startDate: date,
      endDate: $('.datepicker_task_due_date').val()
    });

    $('.autoclose-datepicker-item-custodian').datepicker({
      autoclose: true,
      todayHighlight: true,
      format: 'yyyy-mm-dd',
      startDate: $('#iteam_created_date').val(),
      endDate: date,
    });

    $('.autoclose-datepicker-item-custodian-takeover').datepicker({
      autoclose: true,
      todayHighlight: true,
      format: 'yyyy-mm-dd',
      startDate: $('#handover_date').val(),
      endDate: date,
    });
    $('.take_handover_date').datepicker({
      autoclose: true,
      todayHighlight: true,
      format: 'yyyy-mm-dd',
      //startDate: $('#handover_date').val(),
      endDate: date,
    });
    
    $('.autoclose-datepicker-add-item-handover-date').datepicker({
      autoclose: true,
      todayHighlight: true,
      format: 'yyyy-mm-dd',
      endDate: date,
    });

    $('#item_purchase_date').datepicker({
      format: 'yyyy-mm-dd', 
      autoclose: true,
    })
    .on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('.autoclose-datepicker-add-item-handover-date').val();
            $('.autoclose-datepicker-add-item-handover-date').datepicker('setStartDate', startDate);
            $('.autoclose-datepicker-add-item-handover-date').datepicker('setEndDate', date);
        });

  
  $("#attendance_start_date").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    }).on("changeDate", function (e)
    {
      $('#attendance_end_date').val('');
      var dt = new Date(e.format());
      $("#attendance_end_date").datepicker('destroy').datepicker({
          format: 'yyyy-mm-dd',
          autoclose: true,
          startDate: dt,
          todayHighlight: true,
      });
    });

    $('.past-dates-datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
      endDate: date,
    });


  /* 18 July 2022 */
$('.template-datepicker').bootstrapMaterialDatePicker({
  time: false,
});

$('.template-timepicker').bootstrapMaterialDatePicker({
  date: false,
  format: 'hh:mm A'
});

$('.template-datetimepicker').bootstrapMaterialDatePicker({
  format: 'yyyy-mm-dd HH:mm:ss',
});

    $('#punch_in_time').bootstrapMaterialDatePicker({
      date: false,
      format: 'HH:mm'
    }).on('change', function(in_time)
    {
    });

    $('#edit_punch_out_time').bootstrapMaterialDatePicker({
      date: false,
      format: 'HH:mm'
    })
    .on('change', function(out_time)
    {
    });

    $('.past-dates-datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
      endDate: date,
    });

    function tConvert(time)
    {
      time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
      if (time.length > 1)
      {
        time = time.slice (1);
        time[5] = +time[0] < 12 ? ' AM' : ' PM';
        time[0] = +time[0] % 12 || 12;
      }
      return time.join ('');
    }

  var date = new Date();
  var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

  $("#date_from_new").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
      defaultDate: new Date()
  }).on("changeDate", function (e)
  {
    $('#date_to_new').val('');
    var dt = new Date(e.format());
    $("#date_to_new").datepicker('destroy').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        startDate: dt,
        todayHighlight: true,
    });
  });

$('.mhdhp').timepicker({
    startTime: '01:00',
    endTime: '13:00',
    timeFormat: 'h:mm',
    interval : 15
});

$('.mhffdp').timepicker({
    startTime: '01:00',
    endTime: '13:00',
    timeFormat: 'h:mm',
    interval : 15
});

$('.hdbtp').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm'
});

$('.hdtsp').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm'
});

$('.lbstp').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm'
});

$('.tbstp').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm'
});

$('.tbetp').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm'
});

$('.lbetp').bootstrapMaterialDatePicker({
    date: false,
    format: 'HH:mm',
});