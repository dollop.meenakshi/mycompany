/* /////////////Dollop Infotech(October 21 2021) //////////////  */

/* remove e for number  */
var access_branchs_id = $("#access_branchs_id").val();
function buttonSetting()
{
  $('.hideupdate').hide();
  $('.hideAdd').show();
  //$('#addHrDocumentAdd')[0].reset();
  // $('#hrDocAdd')[0].reset();
  // $('#addAttendanceType')[0].reset();
}

function changeBirthdayNotificationSetting(value) {
    $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {updateBirthdaySetting:"updateBirthdaySetting",birthday_notification_type : value,csrf:csrf},
          success: function(response){
              document.location.reload(true);
          }
       });
  }

function ideaCategoryDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getIdeaCategory",
                idea_category_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#idea_category_id').val(response.idea_category.idea_category_id); 
              $('#idea_category_name').val(response.idea_category.idea_category_name); 
            }
    });
}

function ideaStatusChange(value, id, key){
    var csrf =$('input[name="csrf"]').val();
    $(".ajax-loader").show();
    $.ajax({
      url: "controller/statusController.php",
      cache: false,
      type: "POST",
      data: {value: value,
              id : id,
              status:key, 
              csrf:csrf},
      success: function(response){
        $(".ajax-loader").hide();
        
        if(response==1) {
          document.location.reload(true);
          swal("Status Changed", {
                    icon: "success",
                  });
        } else {
          document.location.reload(true);
          swal("Something Wrong!", {
                    icon: "error",
                  });

        }
      }
    });
}

function DeleteIdeaMaster(val, deleteValue) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this data!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
              url: "controller/deleteController.php",
              cache: false,
              type: "POST",
              data: {id : val,deleteValue
                :deleteValue},
              success: function(response){
                // 
                if(response==1) {
                  document.location.reload(true);
                  // history.go(0);
                } else {
                  document.location.reload(true);
                  /// history.go(0);
                }
              }
            });
        }else{
            swal("Your data is safe!");
        }
    });
}


function courseDataSet(id)
{
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getCourseById",
                course_id:id,
              },
            success: function(response){
                
              
              $('#course_id').val(response.course.course_id);
              $('#course_title').val(response.course.course_title);
              $('#course_description').val(response.course.course_description);
              $('#course_image_old').val(response.course.course_image);
              $('#blah').attr('src','../img/course/'+response.course.course_image);
              $('#blah').attr('width','75px');
              $('#blah').attr('height','75px');

            }
    });
}

function courseDetailFunction(id)
{
    var main_content = "";
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getCourseById",
                course_id:id,
              },
            success: function(response){
                main_content += `<div class="col-md-12 mb-3">
                                <b>Course Title </b>
                                <p class="d-block" >`+response.course.course_title+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Course Description </b>
                                <p class="d-block" >`+response.course.course_description+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Image </b>
                                <img class="myimage3" width="75px" height="75px" src="../img/course/`+response.course.course_image+`" >
                              </div>`;
                              $('#viewModal').html(main_content);
                              $('#mainModal').modal();
                              $(".myimage3").on('error', function() {
                                $(this).prop('src', '../img/default.jpg');
                            });
            }
    })
}


function courseLessonsDataSet(id)
{
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        data: {
            action:"getCourseLesson",
            lessons_id:id,
            },
        success: function(response){
          
          type = parseInt(response.course_lesson.lesson_type_id);

          if (type == 5 || type == 6 || type == 10 || type == 7) {
            $('.showYoutubeinput').hide();
            $('.inoutForText').hide();
            $('.lessonInput').show();
           
            if (type == "5") {
              $('#lesson_video').addClass('pdfOnly');
              $('#lesson_video').attr("accept", "image/*,.pdf");
            } else if (type == "10") {
              $('#lesson_video').attr("accept", "audio/*");
            } else if (type == "7") {
              $('#lesson_video').attr("accept", "video/*");
            } else {
              $('#lesson_video').attr("accept", ".txt,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            }
    
          } else if (type == 11) {
            $('.showYoutubeinput').hide();
            $('.inoutForText').show();
            $('.lessonInput').hide();
          } else {
            $('.showYoutubeinput').show();
            $('.inoutForText').hide();
            $('.lessonInput').hide();
          }
            $('#addLessonModal').modal();
            $('#lessons_id').val(response.course_lesson.lessons_id);
            $('#lesson_type_id').val(response.course_lesson.lesson_type_id);
            $('#lesson_type_id').select2();
            $('#lesson_title').val(response.course_lesson.lesson_title);
          $('.cpId').val(response.course_lesson.course_chapter_id);
          $(".lesson_text").summernote("code", response.course_lesson.lesson_text);
            $('#lesson_video_link').val(response.course_lesson.lesson_video_link);
            $('#lesson_video_old').val(response.course_lesson.lesson_video);
           // $('#lesson_video').val(response.course_lesson.lesson_video);
        }
    })
}

function courseLessonsModal(id)
{
  var main_content="";
  $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {
              action:"getCourseLesson",
              lessons_id:id
            },
          success: function(response){
            $(".ajax-loader").hide();
            if(response.status==200) {
              $('#detail_view').modal();
              main_content += `<div class="col-md-6 mb-3">
                                <b>Lesson Title </b>
                                <p class="d-block" >`+response.course_lesson.lesson_title+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Lesson Type </b>
                                <p class="d-block" >`+response.course_lesson.lesson_type+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Date </b>
                                <p class="d-block" >`+response.course_lesson.lesson_created_date+`</p>
                              </div>`;
              
              if (response.course_lesson.lesson_video != "") {
               
                if (response.course_lesson.lesson_type == "Video") {
                  
                  main_content += `<div class="col-md-6  mb-3">
                                      <b>Lesson Video </b>
                                      <a target="_blank" class="badge badge-primary" href="../img/course/`+response.course_lesson.lesson_video+`"> View</a>
                                      </video>
                                    </div>`;
                }
                else
                {
                  main_content += `<div class="col-md-6 mb-3">
                                      <b>Lesson Link </b>
                                      <a target="_blank" class="badge badge-primary" href="../img/course/`+response.course_lesson.lesson_video+`"> View</a>
                                    </div>`;
                }
              }
              /* if (response.course_lesson.lesson_video !="") {
                main_content += `<div class="col-md-12 mb-3">
                                    <b>Lesson Video </b>
                                    <video width="320" height="240" controls>
                                      <source src="`+ '../img/courseLessons/' + response.course_lesson.lesson_video + `" type="video/mp4">
                                    </video>
                                  </div>`;
              } */
              if (response.course_lesson.lesson_video_link !="") {
                main_content += `<div class="col-md-6 mb-3">
                                    <b>Lesson Link </b>
                                    <a target="_blank" class="badge badge-primary" href="`+response.course_lesson.lesson_video_link+`">View</a>
                                  </div>`;
              }
              if (response.course_lesson.lesson_text !="") {
                main_content += `<div class="col-md-12 mb-3">
                                    <b>Lesson Text </b>
                                   <p>`+response.course_lesson.lesson_text+`</p>
                                  </div>`;
              }

              $('#course_lesson').html(main_content);
            }
          }
        });
}

function CourseFunction(id,chapter_id="")
{
var optionContet ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getCourseChapterByCourse",
          course_id:id,
        },
      success: function(response){
        
        optionContet = `<option>Select Chapter</option>`;
        $.each(response.course_chapter, function( index, value ) {
          if(chapter_id ==value.course_chapter_id)
          {
            var selected="selected";
          }
          else
          {
            var selected="";

          }
          optionContet += `<option `+selected+` value="`+value.course_chapter_id+`" >`+value.chapter_title+`</option>`;
        });
        $('#course_chapter_id').html(optionContet);
      }
  });
}

function courseChapterDataSet(id)
{  
  
  $('.frmRst ').removeClass('error');
  $("form#courseChapterAdd :input").each(function(){
    var input = $(this.id); 
    if (input.selector != "") {
      $('#' + input.selector + '-error').remove();
    }
   });
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getcourseChapterByChapterId",
                course_chapter_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#course_chapter_id').val(response.course_chapter.course_chapter_id); 
              $('#chapter_title').val(response.course_chapter.chapter_title); 
              $('#chapter_description').val(response.course_chapter.chapter_description); 
              $('#course_id').val(response.course_chapter.course_id);
              $('#course_id').select2();
            }
    })
}

function courseChapterDetailModal(id)
{
    var main_content = "";
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getcourseChapterByChapterId",
                course_chapter_id:id,
              },
            success: function(response){
              $('#detail_view').modal();
                main_content += `<div class="col-md-6 mb-3">
                                  <b>Course </b>
                                  <p class="d-block" >`+response.course_chapter.course_title+`</p>
                                </div>
                                <div class="col-md-6 mb-3">
                                  <b>Chapter Title </b>
                                  <p class="d-block" >`+response.course_chapter.chapter_title+`</p>
                                </div>
                                <div class="col-md-6 mb-3">
                                  <b>Description </b>
                                  <p class="d-block" >`+response.course_chapter.chapter_description+`</p>
                                </div>
                                <div class="col-md-6 mb-3">
                                  <b>Date </b>
                                  <p class="d-block" >`+response.course_chapter.course_chapter_created_date+`</p>
                                </div>`;
              $('#course_chapter').html(main_content);
            }
    })
}

function tConvert (time) {
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) { 
      time = time.slice (1);
      var main_time = '';
      var hour = +time[0] % 12 || 12;
      if(time[0]> 12){
        main_time = hour+time[1]+time[2]+" PM"
      }
      else{
        main_time = hour+time[1]+time[2]+" AM"
  
      }
    }
    return main_time;
  }
/* Dollop Infotech Date 22/oct/2021 */
  
function shiftTimingModal(id)
{
  var main_content="";
  $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {
              action:"getShiftTiming",
              shift_time_id:id
            },
          success: function(response){
            $(".ajax-loader").hide();
            if(response.status==200) {  
              $('#detail_view').modal();   
              main_content += `<div class="col-md-6 mb-3">
                                <b>Shift Name </b>
                                <p class="d-block" >`+response.shift_timing.floor_name+` (`+response.shift_timing.shift_type+`)</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Shift Start Time </b>
                                <p class="d-block" >`+(response.shift_timing.shift_start_time)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Shift End Time </b>
                                <p class="d-block" >`+(response.shift_timing.shift_end_time)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Lunch Start Time </b>
                                <p class="d-block" >`+(response.shift_timing.lunch_break_start_time)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Lunch End Time </b>
                                <p class="d-block" >`+(response.shift_timing.lunch_break_end_time)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Tea Start Time </b>
                                <p class="d-block" >`+(response.shift_timing.tea_break_start_time)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Tea End Time </b>
                                <p class="d-block" >`+(response.shift_timing.tea_break_end_time)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Per Day Hour </b>
                                <p class="d-block" >`+response.shift_timing.per_day_hour+` Hours</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Maximum Half Day Hours </b>
                                <p class="d-block" >`+response.shift_timing.maximum_halfday_hours+` Hours</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Minimum Full Day Hours </b>
                                <p class="d-block" >`+response.shift_timing.minimum_hours_for_full_day+` Hours</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Half Day Start(After)</b>
                                <p class="d-block" >`+response.shift_timing.half_day_time_start+` </p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Half Day (Before)</b>
                                <p class="d-block" >`+response.shift_timing.halfday_before_time+` </p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Week Off Days </b>
                                <p class="d-block" >`+response.shift_timing.week_off_days+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Late Time Start </b>
                                <p class="d-block" >`+response.shift_timing.late_time_start+` Minute</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Early Out Time </b>
                                <p class="d-block" >`+response.shift_timing.early_out_time+` Minute</p>
                              </div>`;
                              if(response.shift_timing.has_altenate_week_off == 1){
                                var arrayS = response.shift_timing.alternate_week_off.split(',');
                                main_content += `<div class="col-md-6 mb-3">
                                                  <b>Alternate Week off</b>`;
                              $.each(arrayS, function( index, value ) {                
                              main_content += `<p class="d-block" >Week `+value+` </p>`;
                              })
                              main_content += ` </div>
                                                <div class="col-md-6 mb-3">
                                                  <b>Alternate Weekoff Days </b>
                                                  <p class="d-block" >`+response.shift_timing.alternate_weekoff_days+`</p>
                                                </div>`;
                              }
                              
                            
              $('#shift_timing').html(main_content);
            } 
          }
        });
}

/* Dollop Infotech Date 22/oct/2021 */
function workTimeDataSet(id)
{
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getWorkTIme",
                working_day_id:id,
              },
            success: function(response){
              
              $('#working_day_id').val(response.work_time.working_day_id);
              $('#floor_id').val(response.work_time.floor_id);
              $('#floor_id').select2();
              getShiftByDeptId(response.work_time.floor_id, response.work_time.shift_time_id);

              $('#month_year').val(response.work_time.month_year);
              $('#month_days').val(response.work_time.month_days);
            }
    })
}



function getShiftByDeptId(id, shift_id='')
{
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getShiftByDeptId",
        floor_id:id,
      },
    success: function(response){
      var main_content = '<option value="">-- Select --</option>';
      $.each(response.shift, function(i, value) {
        var select = '';
          if(value.shift_time_id ==  shift_id){
            select = 'selected';
          }
          main_content += `<option value="`+value.shift_time_id+`" `+select+`>`+value.shift_start_time+` - `+value.shift_end_time+`</option>`;
      });
      $('#shift_time_id').html(main_content);
    }
  })
}


function leaveTypeDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getLeaveType",
                leave_type_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#leave_type_id').val(response.leave_type.leave_type_id); 
              $('#leave_type_name').val(response.leave_type.leave_type_name); 
              //$('input[name=type]').prop("checked", true);
              if(response.leave_type.leave_apply_on_date == 1){
                  $('#leaveApplyOnDateYes').prop("checked", true);
              }else{
                  $('#leaveApplyOnDateNo').prop("checked", true);
              }
            }
    })
}

function leaveDefaultCountDataSet(id)
{  
  $(".ajax-loader").show();
  $('.hideupdate').show();
  $('.carry_forward').removeClass('d-none'); 
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getLeaveDefaultCount",
                leave_default_count_id:id,
              },
            success: function(response){
              $(".ajax-loader").hide();
              $('.hideAdd').hide();
              $('#addModal').modal(); 
              use_in_month_content = ``;
              max_carry_forward_content = ``;
              min_carry_forward_content = ``;
              x=1;
              month_user = response.leave_count.number_of_leaves>=31?31:response.leave_count.number_of_leaves;
              for(var i = (x-0.5); i <=month_user; i += 0.5) {
                selected = '';
                if(response.leave_count.leaves_use_in_month == i){
                  selected = 'selected';
                }
                use_in_month_content += `<option `+selected+` value="`+i+`">`+i+`</option>`;
              }
              for (var j = response.leave_count.number_of_leaves; j>=0.5; j-=0.5) {
                selected = '';
                if(response.leave_count.max_carry_forward == j){
                  selected = 'selected';
                }
                max_carry_forward_content += `<option `+selected+` value="`+j+`">`+j+`</option>`;
              }
              for (var k = (x-0.5); k <response.leave_count.max_carry_forward; k+=0.5) {
                selected = '';
                if(response.leave_count.min_carry_forward == k){
                  selected = 'selected';
                }
                  min_carry_forward_content += `<option `+selected+` value="`+k+`">`+k+`</option>`;
              }
              $('#leaves_use_in_month').html(use_in_month_content);
              $('#max_carry_forward').html(max_carry_forward_content);
              $('#min_carry_forward').html(min_carry_forward_content);

              $('#leave_default_count_id').val(response.leave_count.leave_default_count_id); 
              $('#number_of_leaves').val(response.leave_count.number_of_leaves);
              $('#leave_year').val(response.leave_count.leave_year);
              $('#dId').val(response.leave_count.floor_id); 
              $('#floor_id').select2();
              $('#floor_id').attr("disabled", true);                          
              $('#leave_type_id').val(response.leave_count.leave_type_id); 
              $('#leave_type_id').select2();
              $('#leave_type_id').attr("disabled", true);
              $('#pay_out_remark').val(response.leave_count.pay_out_remark); 
              $('#pay_out').val(response.leave_count.pay_out); 
              $('#carry_forward_to_next_year').val(response.leave_count.carry_forward_to_next_year); 
              if(response.leave_count.carry_forward_to_next_year == 1){
                $('.carry_forward').addClass('d-none'); 
              } else {
                $('.carry_forward').removeClass('d-none'); 
              }
              $('#leave_calculation').val(response.leave_count.leave_calculation); 
              if(response.leave_count.number_of_leaves < 12){
                $('.leave_calculation').addClass('d-none'); 
              } else {
                 $('.leave_calculation').removeClass('d-none'); 
              }
            }
    })
}


function leaveAssignDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getLeaveAssign",
                user_leave_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 

              used_leave = parseFloat(response.leave_assign.total_full_day_leave)+(parseFloat(response.leave_assign.total_half_day_leave)/2);

              pay_out_leave = response.leave_assign.pay_out_leave==null || response.leave_assign.pay_out_leave==''?0:response.leave_assign.pay_out_leave;

              carry_forward_leave = response.leave_assign.carry_forward_leave==null || response.leave_assign.carry_forward_leave==''?0:response.leave_assign.carry_forward_leave;

              total_used_leave = parseFloat(used_leave) + parseFloat(pay_out_leave) + parseFloat(carry_forward_leave);
              
              //console.log(total_used_leave);
              $('#user_leave_id').val(response.leave_assign.user_leave_id); 
              $('#leave_type_id').val(response.leave_assign.leave_type_id); 
              $('#total_used_leave').text(total_used_leave); 
              $("#user_total_leave").attr({     
                "min" : total_used_leave         
             });
              $('#leave_type_id').select2();
              $('#user_id').val(response.leave_assign.user_id); 
              $('#user_id').select2();
              $('#user_total_leave').val(response.leave_assign.user_total_leave);

              use_in_month_content = ``;
              x=1;
              for(var i = (x-0.5); i <=response.leave_assign.user_total_leave; i += 0.5) {
                selected = '';
                if(response.leave_assign.applicable_leaves_in_month == i){
                  selected = 'selected';
                }
                use_in_month_content += `<option `+selected+` value="`+i+`">`+i+`</option>`;
              }
              $('#applicable_leaves_in_month').html(use_in_month_content);

              $('#assign_leave_year').val(response.leave_assign.assign_leave_year);
            }
    })
}

function LeaveModal(id, society_id)
{
  $(".ajax-loader").hide();
  var holidayData="";
  $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {
              action:"getLeaveDetails",
              society_id:society_id,
              leave_id:id
            },
          success: function(response){
            $(".ajax-loader").hide();
            if(response.status==200) {
              $('#leaveDetailModal').modal();
            if(response.leave.leave_day_type == "0")
            {
                var leaveType="Full Day";
            }
            else
            {
                if (response.leave.half_day_session == "1") {
                  var leaveType="Half Day (First Half)";
                } else if (response.leave.half_day_session == "2") {
                  var leaveType="Half Day (Second Half)";
                } else {
                  var leaveType="Half Day";
                }
            }
            
              holidayData = `<div class="row mx-0">
              <div class="col-md-6 form-group ">
                <label for="exampleInputEmail1">Employee Name : </label>
                <span class="d-block">`+response.leave.user_full_name+`</span>
              </div>
              <div class="col-md-6 form-group ">
                <label for="exampleInputEmail1">Leave Alternate Number : </label>`;
                if(response.leave.leave_alternate_number > 0){
                 holidayData += `<span class="d-block">` +response.leave.leave_alternate_number+ `</span>`;
                }
               holidayData += `</div>
              <div class="col-md-6 form-group ">
                <label for="exampleInputEmail1">Leave Date : </label>
                <span class="d-block">`+response.leave.leave_start_date+`</span>
              </div>
              <div class="col-md-6 form-group ">
                <label for="exampleInputEmail1">Leaves Attachment : </label>
                <a href="../img/leave_attchment/`+response.leave.leave_attachment+`" target="_blank"  >`+response.leave.leave_attachment+`</a></span>
              </div>
              <div class="col-md-6 form-group ">
                <label for="exampleInputEmail1">Leaves Type : </label>
                <span class="d-block">`+leaveType+`</span>
              </div>`;
              if(response.leave.leave_reason !=''){
                holidayData += `<div class="col-md-12 form-group ">
                <label for="exampleInputEmail1">Leave Reason : </label>
                <span class="d-block">`+response.leave.leave_reason+`</span>
              </div>`;
              }
              if(response.leave.leave_handle_dependency !=''){
                holidayData += `<div class="col-md-12 form-group ">
                <label for="exampleInputEmail1">Dependency : </label>
                <span class="d-block">`+response.leave.leave_handle_dependency+`</span>
              </div>`;
              }
              if(response.leave.leave_alternate_number >0){
                holidayData += `<div class="col-md-12 form-group ">
                <label for="exampleInputEmail1">Alternate Contact Number : </label>
                <span class="d-block">`+response.leave.leave_alternate_number+`</span>
              </div>`;
              }
              if(response.leave.auto_leave_reason !='' && response.leave.leave_type_id =='0'){
                holidayData += `<div class="col-md-12 form-group ">
                <label for="exampleInputEmail1">Auto Leave Reason : </label>
                <span class="d-block">`+response.leave.auto_leave_reason+`</span>
              </div>`;
              }
              if(response.leave.leave_task_dependency == 1){
                holidayData += `<div class="col-md-12 form-group ">
                                  <label for="exampleInputEmail1">Leave Handle : </label>
                                  <span class="d-block">`+response.leave.leave_handle_dependency+`</span>
                                </div>`;
              }
              if(response.leave.leave_admin_reason != ''){
                holidayData += `<div class="col-md-12 form-group ">
                                  <label for="exampleInputEmail1">Leave Reject Reason : </label>
                                  <span class="d-block">`+response.leave.leave_admin_reason+`</span>
                                </div>`;
              }
              if(response.leave.punch_in_time !=null){
                holidayData += `<div class="col-md-6 form-group ">
                                  <label for="exampleInputEmail1">Punch In Time : </label>
                                  <span class="d-block">`+response.leave.attendance_date_start+ ' '+response.leave.punch_in_time+`</span>
                                </div>`;
              }
              if(response.leave.punch_out_time !=null){
                holidayData += `<div class="col-md-6 form-group ">
                                  <label for="exampleInputEmail1">Punch Out Time : </label>
                                  <span class="d-block">`+response.leave.attendance_date_end+ ' '+response.leave.punch_out_time+`</span>
                                </div>`;
              }
              holidayData += `
            </div>`;
              $('#leaveData').html(holidayData);
            }
          }
        });
}

$('.salary_Modal').click(function() {
id= $(this).attr('data-id');
  var holidayData="";
  $.ajax({
          url: "../residentApiNew/commonController.php",
          //cache: false,
          type: "POST",
          data: {
              action:"getsalaryById",
              salary_id:id
            },
          success: function(response){
            $("#salaryModal").modal();
            $(".ajax-loader").hide();
            
            if (response.status == 200) {
             
              DeductData ="";
              earnDatas = "";
              if (response.salary.salary_increment_date != "0000-00-00") {
                salary_increment_date = response.salary.salary_increment_date;
              }
              else
              {
                salary_increment_date = "";

              }
              $('#netSalary').text(response.salary.net_salary);
              holidayData += '<div class="row mx-0">'+
                              '<div class="col-md-6 form-group ">'+
                                '<label for="exampleInputEmail1">Employee Name : </label>'+
                                '<span  >'+response.salary.user_full_name+' ('+response.salary.user_designation+')</span>'+
                              '</div>'+
                              '<div class="col-md-6 form-group ">'+
                                '<label for="exampleInputEmail1">Increment Remark: </label>'+
                                '<span  >'+response.salary.salary_increment_remark+'</span>'+
                              '</div>'+
                              '<div class="col-md-6 form-group ">'+
                                '<label for="exampleInputEmail1">Next Increment Date: </label>'+
                                '<span  >'+salary_increment_date+'</span>'+
                              '</div>'+
                              '<div class="col-md-6 form-group ">'+
                                '<label for="exampleInputEmail1">Start Date : </label>'+
                                '<span  >'+response.salary.salary_start_date+'</span>'+
                              '</div>';
            
              j=1;
              k=1;
              earnData = "";
                DeductData = "";
                earnAmnt = 0;
                deductAmnt = 0;
              $.each(response.salary.earn_deduct, function( index, value ) {
                if (parseFloat(value.amount_percentage) > 0 && value.amount_type==0) {
                    
                  showPrcnt = " ("+parseFloat(value.amount_percentage)+"%)";
                }else if (value.amount_type==1) {
                    
                  showPrcnt = " (Flat)";
                }else if (value.amount_type==2) {
                    
                  showPrcnt = " (Slab)";
                }
                else
                {
                  showPrcnt = "";
                }          
                if(value.earning_deduction_type==0) 
                {
                  earnAmnt += parseFloat(value.salary_earning_deduction_value);
                  
                earnData +=` <tr>
                              
                              <td>`+ value.earning_deduction_name + showPrcnt+`</td>
                              <td>`+ value.salary_earning_deduction_value + `</td>
                              
                            <tr>`;
                } 
                else if(parseFloat(value.salary_earning_deduction_value)>0){
                deductAmnt +=parseFloat(value.salary_earning_deduction_value);
                DeductData +=` <tr>
                                <td>`+ value.earning_deduction_name + showPrcnt;
                                if (value.earning_deduction_type==1 && value.amount_type==0 && value.deduct_view!='') {
                                   DeductData +=` <i title='`+ value.deduct_view + `' class='fa fa-info-circle'></i>`;
                                }
                DeductData +=`</td>
                                <td>`+ value.salary_earning_deduction_value + `</td>
                              <tr>`;
                }
              });
              if (earnAmnt > 0) {
                earnData +=` <tr>
                              
                <td class='text-success'><b>Gross Salary</b></td>
                <td class='text-success'><b>`+ earnAmnt.toFixed(2); + `</b></td>
                
              <tr>`;
              }
              if (deductAmnt > 0) {
                DeductData +=` <tr>
                              
                <td class='text-danger'><b>Total Deduction</b></td>
                <td class='text-danger'><b>`+ deductAmnt.toFixed(2) + `</b></td>
                
              <tr>`;
              }
              holidayData +='</div>';
                $('#salaryData').html(holidayData);
              $('#deductData').html(DeductData);
              $('#earnData').html(earnData);
              
            }
          }
        });
})
  

function leaveStatusChange(value, id, leave_day_type, paid_unpaid, leave_type_id, user_id, leave_date,show_leave_date,leave_reason,leave_day_type_view,leave_type_name)
{
  $('.leave_status').val(value);
  $('.leave_id').val(id);
  if(value == 1){
    $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:'getLeaveBalanceForAutoLeave',
          leave_type_id:leave_type_id,
          leave_date:leave_date,
          user_id:user_id,
      },
      success: function(response){
          $(".ajax-loader").hide();
          console.log(leave_day_type);
          
          $('#applicable_leave').html(response.leave.available_paid_leave);
          $('#balance_leave').html(response.leave.remaining_leave);
          $('.leave_date').html(show_leave_date); 
          
          if(response.leave.available_paid_leave >=1 && leave_day_type==0){
            var paidUnpaidValue = `<option value="">-- Mode --</option>
            <option value="0">Paid Leave</option>
            <option value="1">Unpaid Leave</option> `;
            $('.paid_unpaid').html(paidUnpaidValue);
            $('.paid_unpaid').val(paid_unpaid);
          } else if(response.leave.available_paid_leave >=0.5 && leave_day_type==1){
            var paidUnpaidValue = `<option value="">-- Mode --</option>
            <option value="0">Paid Leave</option>
            <option value="1">Unpaid Leave</option> `;
            $('.paid_unpaid').html(paidUnpaidValue);
            $('.paid_unpaid').val(paid_unpaid);
          } else {
             var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
            $('.paid_unpaid').html(paidUnpaidValue);  
          }
      }
    })
    $('#leaveStatusApprovedModal').modal();
    $('#leave_reason').html(leave_reason);
    $('#leave_type_view').html(leave_type_name);
    $('#leave_day_type_view').html(leave_day_type_view);
  }else{
    $('#leaveStatusRejectModal').modal();
  }
   

}



function HrDocDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getHrDoc",
                hr_document_id:id,
              },
            success: function(response){
              if(response.hr_document.user_id ==0){
                $('.employee_select').addClass('d-none');
              }else{
                $('.employee_select').removeClass('d-none');
              }
              $('#hr_document_id').val(response.hr_document.hr_document_id);
              $('#hr_document_file_old').val(response.hr_document.hr_document_file); 
              $('#hr_document_category_id').val(response.hr_document.hr_document_category_id); 
              $('#hr_document_sub_category_id').val(response.hr_document.hr_document_sub_category_id); 
              $('#hr_document_name').val(response.hr_document.hr_document_name); 
              $('#hr_document_category_id').attr("disabled", true);                          
              $('#hr_document_sub_category_id').attr("disabled", true);                          
              $('#floor_id').val(response.hr_document.floor_id); 
              $('#floor_id').select2();
              $('#hr_document_category_id').select2();
              $('#hr_document_sub_category_id').select2();
              $('#floor_id').attr("disabled", true);                          
              $('#user_id').val(response.hr_document.user_id); 
              $('#user_id').select2();
              $('#user_id').attr("disabled", true);                          
              $('#user_id_old').val(response.hr_document.user_id); 
              $('#floor_id_old').val(response.hr_document.floor_id); 
              getHrDocUser(response.hr_document.floor_id)
            }
    })
}


function changeHrCategoryStatus(id,key)
{
    $.ajax({
          url: "controller/statusController.php",
         // cache: false,
          type: "POST",
          data: {id : id,status:key},
          success: function(response){          
            
              
          }
       });
}


////////////////////active deactive hr category
function HrDocCatDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getHrDocCategory",
                hr_document_category_id:id,
              },
            success: function(response){
              $('#hr_document_category_id').val(response.hr_document_category.hr_document_category_id);
              $('#hr_document_category_name').val(response.hr_document_category.hr_document_category_name);

            }
    })
}




function getUserForSalary(floor_id)
{
  var user_id_old = $('#user_id_old').val();

  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUserByFloor",
                floor_id:floor_id,
              },
            success: function(response){
              var userHtml ="";

              userHtml=`<option value=''> Select Employee </option>`;
              $.each(response.users, function( index, value ) {

                if(user_id_old==value.user_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                }
                userHtml +=`<option `+selected+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });

             $('#user_id').html(userHtml);
             $('.user_id01').html(userHtml);
            }
    })
}


/**  chnage on 27 oct 2021 */
function salaryCalculation(){
    var salary = $('#basic_salary').val();
    if(salary<5999)
    {
      $('#professional_tex').val(0)
    }
    else if(salary>5999 && salary<=8999)
    {
      $('#professional_tex').val(80);

    }
    else if(salary<=11999)
    {
      $('#professional_tex').val(150);
    }
    else 
    {
      $('#professional_tex').val(200);
    }
}
/* dollop 22/10/2021 changes done for the is multiple */

function attendanceTypeDataSet(id)
{
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getAttendanceType",
                attendance_type_id:id,
              },
            success: function(response){
              $('#addModal').modal();
              $('#attendance_type_id').val(response.attendance_type.attendance_type_id);
              $('#attendance_type_name').val(response.attendance_type.attendance_type_name);
              $('#is_multipletime_use').val(response.attendance_type.is_multipletime_use);
              $('#is_multipletime_use').select2();
            }
    })
}


function allAttendaceSet(id)
{  
  $(".ajax-loader").show();
    var main_content = "";
    var main_content2 = "";
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"get_attendace_by_id",
                attendance_id:id,
              },
      success: function (response) {
        $(".ajax-loader").hide();
              $('#addAttendaceModal').modal();
               
             if(response.status==200){
                if(response.data.attendance_status==0)
             {
                var status="Pending";
             }
             else if(response.data.attendance_status==1)
             {
                var status="Approve";
             }
             else
             {
                var status="Declined";
             }
             if(response.data.attendance_date_end !="0000-00-00")
             {
              response.data.attendance_date_end = response.data.attendance_date_end;
             }
             else
             {
              response.data.attendance_date_end = "";
             }
             if(response.data.punch_out_time !="00:00:00")
             {
              response.data.punch_out_time = response.data.punch_out_time;
             }
             else
             {
              response.data.punch_out_time = "";
             }
               var punch_out_time = "";
               var wfh = "";
               var late_in = "";
               var punch_in_in_range = "";
               var attendance_out_from = "";
               var punch_out_in_range = "";
               var early_out = "";
               var attendance_range_in_km = "";
               var punch_out_km = "";
               var attendance_date_end = "";
               if (response.data.punch_out_time != "") {
                punch_out_time = `(`+response.data.punch_out_time+`)`;
               }
               if (response.data.wfh_attendance != 0) {
                wfh = "Yes";
               } else {
                wfh = "No";
               }
               if (response.data.late_in != 0) {
                late_in = "Yes";
               } else {
                late_in = "No";
               }
               if (response.data.early_out != 0) {
                early_out = "Yes ";
               } else {
                early_out = "No ";
               }

               if (response.data.punch_in_in_range != 0) {
                if (response.data.attendance_range_in_km) {
                  dstnc =   parseFloat(response.data.attendance_range_in_km);
                 dstnc =   dstnc.toFixed(2);
                  attendance_range_in_km = `(` + dstnc + `)`;
                } else
                {
                  attendance_range_in_km = "";
                 }
                punch_in_in_range = "No "+attendance_range_in_km;
               } else {
                 
                punch_in_in_range = "Yes ";
               }
               if (response.data.punch_out_in_range != 0) {
                if (response.data.punch_out_km) {
                  dstnc =   parseFloat(response.data.punch_out_km);
                 dstnc =   dstnc.toFixed(2);
                  punch_out_km = `(` + dstnc + `)`;
                } else
                {
                  punch_out_km = "";
                }
                punch_out_in_range = "No "+punch_out_km;
               } else {
                 punch_out_in_range = "Yes ";
               }
               
               if (response.data.attendance_date_end != "") {
                
                attendance_date_end = `<tr>
                                      <td>Punch Out </td>
                                      <td>`+ response.data.attendance_date_end+punch_out_time + `-`+ response.data.attendance_out_from + `
                                      </td>
                                    </tr>`;
               }
               main_content2 = `<tr>
                                    <td>Punch In </td>
                                    <td>`+ response.data.attendance_date_start + `(` + response.data.punch_in_time +`)-`+ response.data.attendance_in_from +`
                                    </td>
                                </tr>
                                <tr>
                                    <td>Punch In Location </td>
                                    <td>`+ response.data.punch_in_branch +`
                                    </td>
                                </tr>
                                <tr>
                                    <td>Punch Out Location </td>
                                    <td>`+ response.data.punch_out_branch +`
                                    </td>
                                </tr>
                               `+attendance_date_end+attendance_out_from+`
                                <tr>
                                    <td>Work From Home </td>
                                    <td>`+wfh+`
                                    </td>
                                </tr>
                                <tr>
                                    <td>Late In </td>
                                    <td>`+late_in+`
                                    </td>
                                </tr>
                               
                               
                                 `; 
                if (response.data.punch_out_latitude != null && response.data.punch_out_latitude != "") {
                main_content2 += ` <tr>
                                      <td>Punch In (in range)</td>
                                      <td>`+punch_in_in_range+`
                                      </td>
                                  </tr>`;
               }
               if(response.data.attendance_date_end !="") {
                main_content2 += `  <tr>
                                      <td>Punch Out (in range)</td>
                                      <td>`+punch_out_in_range+`
                                      </td>
                                  </tr>`;
               }
               if (response.data.punch_in_request != null && response.data.punch_in_request == 1) {
                 punch_in_request_day_type = "";
                 if (response.data.punch_in_request_day_type == 1) {
                   punch_in_request_day_type = " ( Half-Day )";
                 }
                punch_in_request_day_type = 
                main_content2 += `  <tr>
                                      <td>Punch In Request</td>
                                      <td>Yes`+punch_in_request_day_type+`
                                      </td>
                                  </tr>`;
               }
               
               if (response.data.late_in_reason != null && response.data.late_in_reason != "") {
                main_content2 += `<tr>
                                    <td>Late In Reason</td>
                                    <td>`+ response.data.late_in_reason+`
                                    </td>
                                   </tr>`;
               }
              
               
               if (response.data.early_out_reason != null && response.data.early_out_reason != "") {
                main_content2 += `<tr>
                                    <td>Out Reason</td>
                                    <td>`+ response.data.early_out_reason+`
                                    </td>
                                   </tr>`;
               }
               if(response.data.attendance_reason != null && response.data.attendance_reason != ""){
                main_content2 += `<tr>
                                    <td>Punch In Reason</td>
                                    <td>`+ response.data.attendance_reason+`
                                    </td>
                                   </tr>`;
                 }

                if(response.data.punch_out_reason != null && response.data.punch_out_reason != ""){
                main_content2 += `<tr>
                                    <td>Punch Out Reason</td>
                                    <td>`+ response.data.punch_out_reason+`
                                    </td>
                                   </tr>`;
                 }
              if( response.data.changed_by != ""){
                  main_content2 += `<tr>
                                      <td>Status Changed By</td>
                                      <td>`+ response.data.changed_by+`
                                      </td>
                                    </tr>`;
              }
              if (response.data.attendance_declined_reason != null && response.data.attendance_declined_reason != "") {
                  main_content2 += `<tr>
                                      <td>Attendance Rejected Reason</td>
                                        <td>`+ response.data.attendance_declined_reason+`
                                      </td>
                                    </tr>`;
                                }
              if (response.data.auto_leave == 1 && response.data.auto_leave != "") {
                  main_content2 += `<tr>
                                      <td>Auto Leave</td>
                                        <td> Yes `+ response.data.auto_leave_reason +`
                                      </td>
                                    </tr>`;
                                }
             if(response.data.is_modified == 1 ){
                    main_content  +=`<div class="col-md-12 mb-3 float-left">
                                      <b>This Attendance is Modified </b>
                                      <p class="d-block" style="white-space: pre-line"> `+response.data.modified_data+`</p>
                                    </div>`;
              }
              
              $.each(response.data.attedance, function (index, value) {
                 
                 main_content2 += `<tr>
                                      <td>`+ value.attendance_type_name + `</td>
                                      <td>`+ value.break_in_time + `-` + value.break_out_time +` (` + value.total_break +`)
                                      </td>
                                  </tr>`;
               })
                
              $('#attendanceData').html(main_content2);
              var  main_content3 ;
              if (response.data.total_punch>1) {
                $('.multiPunchInTable').show();
                $.each(response.data.punch_in_data, function (index, value) {
                   
                   main_content3 += `<tr>
                                        <td>`+ value.punch_in_date +`-`+ value.punch_in_branch +`</td>
                                        <td>`+ value.punch_out_date  +`-`+ value.punch_out_branch +`</td>
                                        <td>`+ value.working_hour  +`</td>
                                    </tr>`;
                 })
                $('#MulitPunchIn').html(main_content3);
              } else {
                $('#MulitPunchIn').html('');
                $('.multiPunchInTable').hide();
              }

              $('.wrkRpt').html(main_content);
             }
            
            }
    });
}

function attendanceStatusChange(value, id, key){
    if(value != 2){
        var csrf =$('input[name="csrf"]').val();
        $(".ajax-loader").show();
        $.ajax({
        url: "controller/statusController.php",
        cache: false,
        type: "POST",
        data: {value: value,
                id : id,
                status:key, 
                csrf:csrf},
        success: function(response){
            $(".ajax-loader").hide();
            
            if(response==1) {
            document.location.reload(true);
            swal("Status Changed", {
                        icon: "success",
                    });
            } else {
            document.location.reload(true);
            swal("Something Wrong!", {
                        icon: "error",
                    });
            }
        }
        });
    }
    else
  {
      $("#attendanceDeclineReason").modal('show');
      $('.attendanceDeclineReasonClass').click(function() {
        var attendance_declined_reason =  $('#attendance_declined_reason').val();
       
        var csrf =$('input[name="csrf"]').val();
        if(attendance_declined_reason.trim() != ''){
            $(".ajax-loader").show();
            $.ajax({
              url: "controller/statusController.php",
              cache: false,
              type: "POST",
              data: {
                      value: value,
                      id : id,
                      status:key, 
                      attendance_declined_reason:attendance_declined_reason,
                      csrf:csrf},
              success: function(response){
                  $(".ajax-loader").hide();
                  if(response==1) {
                  document.location.reload(true);
                  swal("Status Changed", {
                            icon: "success",
                          });
                  } else {
                  document.location.reload(true);
                  swal("Something Wrong!", {
                              icon: "error",
                          });
                  }
              }
              });
        }else{
          //alert(id);
          
          swal("Please Enter Decline Reason!", {
            icon: "error",
          });
          $('.attendance_status_'+id).val(0);
        }
      });

  }
}


function UserBankDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
  $('#account_no-error').remove(); 
  $('#account_no-error').remove(); 
  $('#ifsc_code-error').remove(); 
  $('#bank_name-error').remove(); 
  $('#user_id-error').remove(); 
  $('#floor_id-error').remove(); 
  //$(this).prev("label").toggleClass("error", (this.value==""))
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getbankById",
                bank_id:id,
              },
            success: function(response){
              
              //getVisitorFloorByBlockId(response.bank.block_id);
               $('#bank_id').val(response.bank.bank_id);
                $('#bank_name').val(response.bank.bank_name);
                $('#account_holders_name').val(response.bank.account_holders_name);
                $('#bank_name').removeClass('error');
                $('#account_no').val(response.bank.account_no);
                $('#account_no').removeClass('error');
                $('#ifsc_code').val(response.bank.ifsc_code);
                $('#ifsc_code').removeClass('error');
                $('#crn_no').val(response.bank.crn_no);
                $('#esic_no').val(response.bank.esic_no);
                $('#pan_card_no').val(response.bank.pan_card_no);
                $('.floor_id').val(response.bank.floor_id);
                //$('.floor_id').val(response.bank.floor_id).trigger("change", true);
                $('.floor_id').select2("enable", false);
                 $('.floor_id').select2();
                $('.floor_id').removeClass('error');
                $('#user_id').removeClass('error');
                $('#block_id').val(response.bank.block_id);
                $('#block_id').removeClass('error');
                $('#block_id').select2("enable", false);
                $('#block_id').select2();
              $('#user_id').val(response.bank.user_id); 
              $('#user_id_old').val(response.bank.user_id); 
              $('#bank_branch_name').val(response.bank.bank_branch_name); 
              $('#account_type').val(response.bank.account_type); 
              $('#floor_id_old').val(response.bank.floor_id); 
              $('#user_id').select2("enable", false);
              $('#user_id').select2();
              getUserForBank(response.bank.floor_id);
            }
    })
}


function getUserForBank(floor_id)
{
  var user_id_old = $('#user_id_old').val();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUserByFloor",
                floor_id:floor_id,
              },
            success: function(response){
              var userHtml ="";
              
              userHtml=`<option value=''> Select Employee </option>`;
              $.each(response.users, function( index, value ) {
               
                if(user_id_old==value.user_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                }
                userHtml +=`<option `+selected+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });
              
             $('#user_id').html(userHtml);
            }
    })
}

$(document).on('change', '.radio_week_off', function(){

  let protype = $(this).val();
  if (protype == 0) {
      $(".has_altenate_week_off").hide();
  }
  if (protype == 1) {
      var optiondata = "";
      changeAltrDays();
      $(".has_altenate_week_off").show();
  } 
});    
  $('.week_off_days').on('change', function() {
      changeAltrDays();
  });
if($('#checkChnageAltFunction').val()==1)
{ 
  changeAltrDays();
} 
if($('#is_Edit').val()==1)
{
    changeAltrDays();
}
function changeAltrDays()
{
  if($('#is_Edit').val()==1){
    var alrtDaysEdit = $('#hasAlrtWeekDays').val();
    
    var  idarry = $('.week_off_days').val();
    var optiondata = "";
    var dataArr = $("#week_off_days").val()
    alrtDaysEditAr = [];
                
    if (alrtDaysEdit == 0) {
      
      alrtDaysEditAr.push("0");
    } else {
      
      alrtDaysEditAr = alrtDaysEdit.split(',');
    }
    $.each(dataArr, function (index, value) {
     
                var showVal = "";
                if(value==0)
                {
                    showVal = "Sunday"
                }
                if(value==1)
                {
                    showVal = "Monday"
                }
                if(value==2)
                {
                    showVal = "Tuesday"
                }
                if(value==3)
                {
                    showVal = "Wednesday"
                }
                if(value==4)
                {
                    showVal = "Thursday"
                }
                if(value==5)
                {
                    showVal = "Friday"
                }
                if(value==6)
                {
                    showVal = "Saturday"
              }
              if (alrtDaysEdit != "") {
                value = value.toString();
               
                if ($.inArray(value, alrtDaysEditAr) >  -1)
               
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";
                }
              }
              else
              {
                var selected = "";
              }
                
                optiondata +=`<option `+selected+`  value="`+value+`">`+showVal+`</option>`;
                $('#has_altenate_week_off_opt').html(optiondata);
            });
  }
  else
  {
    var  idarry = $('.week_off_days').val();
    var optiondata = "";
    var dataArr = $("#week_off_days").val()
    $.each(dataArr, function( index, value ) {
          var showVal = "";
          if(value==0)
          {
              showVal = "Sunday"
          }
          if(value==1)
          {
              showVal = "Monday"
          }
          if(value==2)
          {
              showVal = "Tuesday"
          }
          if(value==3)
          {
              showVal = "Wednesday"
          }
          if(value==4)
          {
              showVal = "Thursday"
          }
          if(value==5)
          {
              showVal = "Friday"
          }
          if(value==6)
          {
              showVal = "Saturday"
          }
          optiondata +=`<option value="`+value+`">`+showVal+`</option>`;
            $('#has_altenate_week_off_opt').html(optiondata);
       });
  }

}


/* Dollop Infotech Date 22/Oct/2021 -  */
function AddExpenseAmountFunction(id)
{  
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getFloorById",
                floor_id:id,
              },
            success: function(response){
              $('#addExpenseAmountModal').modal();
              $('#expense_floor_id').val(response.floor.floor_id);
              $('#max_expense_amount').val(response.floor.max_expense_amount);
            }
    })
}


function employeeExpensesDetail(id)
{
  var main_content = '';
  $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {
            action:"getemployeeExpenses",
              user_expense_id:id
            },
          success: function(response){
            $(".ajax-loader").hide();
            main_content = "";
            if(response.status==200) 
            {
              
              $('#employeeExpensesModal').modal();
              main_content += `<div class="row mx-0">
                                <div class="col-md-6 mb-3">
                                  <b>Department Name : </b>
                                  <p class="d-block">`+response.expenses.floor_name+`</p>
                                </div>
                                <div class="col-md-6 mb-3">
                                  <b>Employee Name : </b>
                                  <p class="d-block">`+response.expenses.user_full_name+` (`+response.expenses.user_designation+`)</p>
                                </div>
                                <div class="col-md-6 mb-3">
                                  <b>Expense Title : </b>
                                  <p class="d-block">`+response.expenses.expense_title+`</p>
                                </div>
                                <div class="col-md-6 mb-3">
                                  <b>Amount : </b>
                                  <p class="d-block">`+response.expenses.amount+`</p>
                                </div>
                                <div class="col-md-6 mb-3">
                                  <b>Date : </b>
                                  <p class="d-block">`+response.expenses.date+`</p>
                                </div>
                                <div class="col-md-6 mb-3">
                                  <b>Expense Document : </b>`;
                                  if(response.expenses.expense_document !== null){
                                  main_content += `<p class="d-block"><a href="../img/expense_document/`+response.expenses.expense_document+`" target="_blank">`+response.expenses.expense_document+`</a></p>`;
                                }else{
                                  main_content += `<p class="d-block">No expense document found!</p>`;
                                }
                                main_content +=
                                `</div>
                                <div class="col-md-12 mb-3">
                                  <b>Description : </b>
                                  <p class="d-block">`+response.expenses.description+`</p>
                                </div>`;
                                if(response.expenses.expense_reject_reason != ''){
                                  main_content += `<div class="col-md-12 mb-3">
                                                    <b>Reject Reason : </b>
                                                    <p class="d-block">`+response.expenses.expense_reject_reason+`</p>
                                                  </div>`;
                                }
                                main_content +=
                `</div>`;
              if (response.expenses.expense_approved_by_id > 0) {
                if (response.expenses.expense_approved_by_type==0) {
                  approved_name = response.expenses.approved_user_name;
                }
                else
                {
                  approved_name = response.expenses.approved_admin_name;
                }
                main_content += `<div class="col-md-6 mb-3">
                                <b>Approved By : </b>
                                <p class="d-block">`+approved_name+ `</p>
                              </div>`;
                $('#employeeExpensesData').html(main_content);
              }
              if (response.expenses.expense_reject_by_id > 0) {
                if (response.expenses.expense_reject_by_type==0) {
                  reject_name = response.expenses.reject_user_name;
                }
                else
                {
                  reject_name = response.expenses.reject_admin_name;
                }
                main_content += `<div class="col-md-6 mb-3">
                                <b>Rejected By : </b>
                                <p class="d-block">`+ reject_name + `</p>
                              </div>`;
              }
              if (response.expenses.expense_paid_by_id > 0) {
                if (response.expenses.expense_paid_by_type==0) {
                  paid_name = response.expenses.paid_by_user_name;
                }
                else
                {
                  paid_name = response.expenses.paid_by_admin_name;
                }
                main_content += `<div class="col-md-6 mb-3">
                                <b>Paid By : </b>
                                <p class="d-block">`+ paid_name + `</p>
                              </div>`;
              }
              $('#employeeExpensesData').html(main_content);

            }
          }
        });
}


function employeeExpensesStatusChange(id)
{
  $('.user_expense_id').val(id);
  $('#expenesDeclineReason').modal();
}

function salarySlipGeneration(salary_slip_id)
{
  holidayData = "";
  DeductData ="";
              earnD ="";
   var csrf =$('input[name="csrf"]').val();
      $(".ajax-loader").show();
      $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
            //  value: value,
            salary_slip_id : salary_slip_id,
              action:"getSalarySlipById",
              csrf:csrf},
        success: function (response) {
         
          
          $(".ajax-loader").hide();
          var main_content = '';
          $('#salarySLipModal').modal();
          if(response.status==200) {
            if(response.salary.salary_mode==0)
            {
              response.salary.salary_mode = "Bank Transaction";
            }
            else if(response.salary.salary_mode == 1)
            {
              response.salary.salary_mode = "Cash";

            }
            else
            {
              response.salary.salary_mode = "Cheque";

            }
            $('#netSalary').text(response.salary.total_net_salary);
            $('#totalCtc').text(response.salary.total_ctc_cost);
            main_content += '<div class="row mx-0">' +
              '<div class="col-md-6  ">' +
              '<label for="exampleInputEmail1">Employee Name : </label>' +
              '<span  >' + response.salary.user_full_name + '</span>' +
              '</div>';
            if (parseInt(response.salary.salary_slip_status)==0 ||parseInt(response.salary.salary_slip_status)>0) {
              main_content += '<div class="col-md-6  ">' +
                '<label for="exampleInputEmail1">Generated By : </label>' +
                '<span  >' + response.salary.generated_user_by + '</span>' +
                '</div>';
            }
            if (parseInt(response.salary.salary_slip_status) == 2 ) {
              main_content += '<div class="col-md-6  ">' +
                '<label for="exampleInputEmail1">Published By : </label>' +
                '<span  >' + response.salary.published_user_by + '</span>' +
                '</div>';
            }
            if (parseInt(response.salary.salary_slip_status) == 1 || parseInt(response.salary.salary_slip_status) > 1) {
              main_content += '<div class="col-md-6  ">' +
                '<label for="exampleInputEmail1">Checked By : </label>' +
                '<span  >' + response.salary.checked_user_by + '</span>' +
                '</div>';
            }
            main_content += '<div class="col-md-6  ">'+
                              '<label for="exampleInputEmail1">Start Date: </label>'+
                              '<span  >'+response.salary.salary_start_date+'</span>'+
                            '</div>'+
                            '<div class="col-md-6  ">'+
                              '<label for="exampleInputEmail1">End Date: </label>'+
                              '<span  >'+response.salary.salary_end_date+'</span>'+
                            '</div>'+
                            '<div class="col-md-6 ">'+
                              '<label for="exampleInputEmail1">Salary Month  : </label>'+
                              '<span  >'+response.salary.month+'</span>'+
                            '</div>'+
                            '<div class="col-md-6  ">'+
                              '<label for="exampleInputEmail1">Total Working Days : </label>'+
                              '<span  >'+response.salary.total_month_days+'</span>'+
                            '</div>'+
                            
                            '<div class="col-md-6  ">'+
                              '<label for="exampleInputEmail1">Total Leaves Days : </label>'+
                              '<span  >'+response.salary.leave_days+'</span>'+
                            '</div>'+
                            '<div class="col-md-6 ">'+
                              '<label for="exampleInputEmail1">Salary Mode : </label>'+
                              '<span  >'+response.salary.salary_mode+'</span>'+
                            '</div>'+
                            '<div class="col-md-6  ">'+
                              '<label for="exampleInputEmail1">Salary Mode Description : </label>'+
                              '<span  >'+response.salary.salary_mode_description+'</span>'+
                            '</div>'+
                            '<div class="col-md-6 ">'+
                              '<label for="exampleInputEmail1">Extra Days : </label>'+
                              '<span  >'+response.salary.extra_days+'</span>'+
                            '</div>'+
                            '</div>';
            $('#salarySlipData').html(main_content);
              j=1;
              k=1;
            $.each(response.salary.earn_deduct, function( index, value ) {
                if(value.earning_deduction_type_current==0) 
               {
                earnD +=` <tr>
                              <td>`+ value.earning_deduction_name_current + `</td>
                              <td>`+ value.earning_deduction_amount + `</td>
                            <tr>`;
                  
               } 
               else{
                DeductData +=` <tr>
                                <td>`+ value.earning_deduction_name_current + `</td>
                                <td>`+ value.earning_deduction_amount + `</td>
                              <tr>`;
                  
              }
              
               
            });
            if (parseInt(response.salary.advance_salary_paid_amount) > 0)
              {
                DeductData +=` <tr>
                           
                <td>Advace Salary Deduction</td>
                <td>`+ response.salary.advance_salary_paid_amount + `</td>
                
              <tr>`;       
             }
             if (parseInt(response.salary.emi_deduction) > 0)
              {
                DeductData +=` <tr>
                           
                <td>Loan EMI Deduction</td>
                <td>`+ response.salary.emi_deduction + `</td>
                
              <tr>`;       
             }
            if (parseInt(response.salary.other_deduction) > 0)
              {
                DeductData +=` <tr>
                           
                <td>Other Deduction</td>
                <td>`+ response.salary.other_deduction + `</td>
                
              <tr>`;       
             }
            
            if (parseInt(response.salary.other_earning) > 0)
              {
                earnD +=` <tr>
                           
                <td>Other Earning</td>
                <td>`+ response.salary.other_earning + `</td>
                
              <tr>`;       
              }
            if (response.salary.total_earning_salary) {
              earnD +=` <tr>
                          <th> Total Earning</th>
                          <th>`+ response.salary.total_earning_salary + `</th>
                        <tr>`;
            }
             if (response.salary.expense_amount) {
              earnD +=` <tr>
                          <td> Reimbursement/Expense </td>
                          <td>`+ response.salary.expense_amount + `</td>
                        <tr>`;
            }
            if (response.salary.total_deduction_salary) {
              DeductData +=` <tr>
                              <th> Total Deduction</th>
                              <th>`+ response.salary.total_deduction_salary + `</th>
                            <tr>`;
            }
            var contribution = "";
            //response.salary.contribution.length = 0;
            if (response.salary.contribution.length > 0) {
              $('.contributionClass').removeClass('d-none');
              $.each(response.salary.contribution, function (i, v) {
                contribution = `<tr>
                                  <td>`+v.earning_deduction_name_current+`</td>
                                  <td>`+v.employer_contribution_amount+`</td>
                                </tr>`;
              });

            } else {
              $('.contributionClass').addClass('d-none');
           }
            
          }
          $('#deductData').html(DeductData);
          $('#contributionData').html(contribution);
              $('#earnData').html(earnD);
          $('#salarySlipData').html(main_content); 
      }
      });
}

$('#user_id01').on('change', function() {
 var month = $('#month').val();
 var year = $('#year').val();
  //var csrf =$('input[name="csrf"]').val();
 // $(".ajax-loader").show();
    $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
          //  value: value,
            user_id : $(this).val(),
            action:"getUserSalarySlip",
            month:month,
            year:year,
          },
    success: function(response){
     
    $('#basic_salary').val(response.salary.basic);
    $('#hra').val(response.salary.HRA);
    $('#conveyance').val(response.salary.conveyance);
    $('#medical').val(response.salary.Medical);
    $('#pf').val(response.salary.PF);
    $('#esic').val(response.salary.esic);
    $('#others_deduction').val(response.salary.Others);
    $('#net_salary').val(response.salary.net_salary);
    $('#holiday').val(response.salary.holiday);
    $('#leaves').val(response.salary.leaves);
    $('#month_wokring_days').val(response.salary.month_wokring_days);
    $('#pf_amount').val(response.salary.pf_amount);
    $('#professional_tax').val(response.salary.professional_tax);
    $('#totalworkingdays').val(response.salary.totalworkingdays);
    $('#user_expanse').val(response.salary.user_expanse);
    /*

    
     */
    }
  });
});



// paid expense to user

function PaidAll(PaidValue) {
  var oTable = $("#example").dataTable();
  var id = [];
  var amount =0;
  var user_id = '';
        $(".multiPaidCheckbox:checked", oTable.fnGetNodes()).each(function(i) {
          id[i] = $(this).val();
          amount += +$(this).attr('amount');
          user_id = $(this).attr('user_id');
        });
  
  /* if (user_id == "") {
    user_id = $('#uId').val();
  } */
  if(id=="" && amount==0 && user_id=='') {
    swal(
      'Warning !',
      'Please Select at least 1 Expense !',
      'warning'
    );
  } 
  else {
    $('#user_expense_id').val(id);
    $('#amount').val(amount);
    $('#user_id').val(user_id);
    $('#expensePayModal').modal();
  }
}

function singleExpensePaid(user_id,user_expense_id,amount) {
  
  $('#user_expense_id').val(user_expense_id);
  $('#amount').val(amount);
  $('#user_id').val(user_id);
  $('#expensePayModal').modal();
}
function getRemainingLeave(user_id, leave_type_id, year){
  var csrf =$('input[name="csrf"]').val();
      $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        data: {
                user_id: user_id,
                leave_type_id : leave_type_id,
                year : year,
                action:"getAssignAndRemainingLeave",
                csrf:csrf},
        success: function(response){
            var main_content = '';
            $('#remainingLeaveModal').modal();
            main_content = `<div class="row mx-0">
                              <div class="col-md-12 mb-3">
                                <b>Leave Type : </b>
                                <p class="d-block">`+response.leave.leave_type_name+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Total Leave : </b>
                                <p class="d-block">`+response.leave.user_total_leave+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Remaining Leave : </b>
                                <p class="d-block">`+response.leave.remaining_leave+`</p>
                              </div>
                            </div>`;
            $('#remainingLeaveData').html(main_content);
        }
      });
}


function siteMasterSetData(id)

{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getSiteData",
                site_id :id,
              },
            success: function(response){
              $('#site_id').val(response.site.site_id );
              $('#site_name').val(response.site.site_name);  
              $('#user_id_old').val(response.site.site_manager_id); 
              $('#floor_id_old').val(response.site.floor_id); 
             // $('#site_manager_id').select2();
              $('#br_id').val(response.site.block_id); 
              $('#br_id').select2();
              getSiteManagerFloorByBLock(response.site.block_id);
              $("#floor_id").val(response.site.floor_id).trigger('change');
             /*  $('#floor_id').val(response.site.floor_id); 
              $('#floor_id').select2(); */
              getSiteManagerByFloorBLock(response.site.floor_id);
              $('#site_manager_id').val(response.site.site_manager_id); 
              $('#site_manager_id').select2();
              $('#site_description').val(response.site.site_description); 
              $('#site_address').val(response.site.site_address); 
              
            }
    })
}
function siteMasterShowDetails(id)

{  
  var detail = '';
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getSiteData",
                site_id :id,
              },
            success: function(response){
              detail = `<div class="row mx-0">
                         
                          <div class="col-md-6 mb-3">
                            <b>Site Name : </b>
                            <p class="d-block">`+ response.site.site_name +`</p>
                          </div>
                         
                          <div class="col-md-6 mb-3">
                            <b>Site Created Date : </b>
                            <p class="d-block">`+ response.site.site_created_date +`</p>
                          </div>
                          <div class="col-md-6 mb-3">
                            <b>Site Address : </b>
                            <p class="d-block">`+ response.site.site_address +`</p>
                          </div>
                          <div class="col-md-6 mb-3">
                            <b>Site Description : </b>
                            <p class="d-block">`+ response.site.site_description +`</p>
                          </div>
                          
                  </div>`;
              managerData = "";
              if (response.finance.length > 0) {
                $.each(response.finance, function (index, value) {
                  if (value.manager_type == 2) {
                    type = "Finance";
                  } else if (value.manager_type == 1) {
                    type = "Procurement";
                  } else
                  {
                    type = "Site Manager";
                  }
                  managerData += `<tr>
                                    <td>`+value.user_full_name+`</td>
                                    <td>`+type+`</td>
                                  </tr>`;
                });
              }
              if (response.site_manager.length > 0) {
                $.each(response.site_manager, function (index, value) {
                  if (value.manager_type == 2) {
                    type = "Finance";
                  } else if (value.manager_type == 1) {
                    type = "Procurement";
                  } else
                  {
                    type = "Site Manager";
                  }
                  managerData += `<tr>
                                    <td>`+value.user_full_name+`</td>
                                    <td>`+type+`</td>
                                  </tr>`;
                });
              }
              if (response.procurement.length > 0) {
                $.each(response.procurement, function (index, value) {
                  if (value.manager_type == 2) {
                    type = "Finance";
                  } else if (value.manager_type == 1) {
                    type = "Procurement";
                  } else
                  {
                    type = "Site Manager";
                  }
                  managerData += `<tr>
                                    <td>`+value.user_full_name+`</td>
                                    <td>`+type+`</td>
                                  </tr>`;
                });
              }
                  $('#siteDetailModelDiv').html(detail);
                  $('#siteMasterData').html(managerData);
            }
            
    })
}
function UnitMeasurementSetData(id)

{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUnitMeasurementData",
                unit_measurement_id :id,
              },
            success: function(response){
              $('#unit_measurement_id').val(response.unit.unit_measurement_id);
              $('#vendor_id').val(response.unit.vendor_id);
              $('#vendor_id').select2();
              $('#unit_measurement_name').val(response.unit.unit_measurement_name);  
              $('#unit_measurement_description').val(response.unit.unit_measurement_description); 
            }
    })
}

function unitMeasurementrShowDetails(id)

{  
  var detail = '';
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUnitMeasurementData",
                unit_measurement_id :id,
              },
            success: function(response){
              if(response.status == 200){
              detail = `<div class="row mx-0">
                          <div class="col-md-12 mb-3">
                            <b>Unit Measurement Name : </b>
                            <p class="d-block">`+ response.unit.unit_measurement_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Unit Measurement Created Date : </b>
                            <p class="d-block">`+ response.unit.unit_measurement_created_date +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Unit Measurement Details : </b>
                            <p class="d-block">`+ response.unit.unit_measurement_description +`</p>
                          </div>
                       </div>`;
                  $('#unitMeasurementDetailModelDiv').html(detail);
              }
            }
            
    })
}


function categorySetData(id)

{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getCategorytData",
                product_category_id :id,
              },
      success: function (response) {
              $('#product_category_id').val(response.category.product_category_id);
              $('#vendor_id').val(response.category.vendor_id);
              $('#vendor_id').select2();
              $('#category_name').val(response.category.category_name);  
              $('#category_description').val(response.category.category_description); 
            }
    })
}

function categoryShowDetails(id)
{  
  var detail = '';
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getCategorytData",
                product_category_id :id,
              },
            success: function(response){
              if(response.status == 200){
              detail = `<div class="row mx-0">
                          <div class="col-md-12 mb-3">
                            <b>Category Name : </b>
                            <p class="d-block">`+ response.category.category_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Created Date : </b>
                            <p class="d-block">`+ response.category.product_category_created_date +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Category Description : </b>
                            <p class="d-block">`+ response.category.category_description +`</p>
                          </div>
                  </div>`;
                  $('#productCategoryModelDiv').html(detail);
              }
            }
            
    })
}

 function getSubCategoryByCatId(product_category_id, product_sub_category_id='')
{
  
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getSubCategory",
                product_category_id:product_category_id,
              },
            success: function(response){
              var main_content ="";
              main_content=`<option value="">-- Select --</option>`;
              $.each(response.sub_category, function( index, value ) {
                var selected = "";
                if(product_sub_category_id==value.product_sub_category_id)
                {
                   selected = "selected";
                }
                main_content +=`<option `+selected+` value="`+value.product_sub_category_id+`">`+value.sub_category_name+`</option>`;
              });
              
             $('.product_sub_category_id').html(main_content);
             $('#product_sub_category_id').html(main_content);
            }
    })
} 

function subCategorySetData(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getSubCategoryData",
                product_sub_category_id :id,
              },
            success: function(response){
              $('#product_sub_category_id').val(response.subCategory.product_sub_category_id );
              $('#sub_category_name').val(response.subCategory.sub_category_name);  
              $('#cat_id').val(response.subCategory.product_category_id);  
              $('#product_category_id').val(response.subCategory.product_category_id); 
              $('#product_category_id').select2();
              $('#product_category_id').attr('disabled',true);
              $('#sub_category_description').val(response.subCategory.sub_category_description); 
               
            }
    })
}

function subCategoryShowDetails(id)
{  
  var detail = '';
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getSubCategoryData",
                product_sub_category_id :id,
              },
            success: function(response){
              detail = `<div class="row mx-0">
                          <div class="col-md-6 mb-3">
                            <b>Sub Category Name : </b>
                            <p class="d-block">`+ response.subCategory.sub_category_name +`</p>
                          </div>
                          <div class="col-md-6 mb-3">
                            <b>Category Name : </b>
                            <p class="d-block">`+ response.subCategory.category_name +`</p>
                          </div>
                          <div class="col-md-6 mb-3">
                            <b>Date : </b>
                            <p class="d-block">`+ response.subCategory.product_sub_category_created_date +`</p>
                          </div>
                          <div class="col-md-6 mb-3">
                            <b>Sub-Category Description : </b>
                            <p class="d-block">`+ response.subCategory.sub_category_description +`</p>
                          </div>
                  </div>`;
                  $('#subCategoryDetailModelDiv').html(detail);
            }
            
    })
}

function serviceProviderSetData(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getServiceProviderDetails",
                service_provider_bank_id :id,
              },
            success: function(response){
              $('#vendorBankModal').modal();
              $('#service_provider_bank_id').val(response.bank_details.service_provider_bank_id);
              $('#service_provider_account_no').val(response.bank_details.service_provider_account_no); 
              $('#service_provider_ifsc').val(response.bank_details.service_provider_ifsc); 
              $('#service_provider_bank_name').val(response.bank_details.service_provider_bank_name); 
              $('#account_holdar_name').val(response.bank_details.account_holdar_name);
              $('#crn_no').val(response.bank_details.crn_no); 
            }
    })
}

function productDetailModal(id)
{
  var main_content="";
  $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {
              action:"getProduct",
              product_id:id
            },
          success: function(response){
            $(".ajax-loader").hide();
            if(response.status==200) {  
             // 
              $('#productDetailModal').modal();   
             
              main_content += `<div class="col-md-6 mb-3">
                                <b>Product Code </b>
                                <p class="d-block" >`+response.product.product_code+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Product Name </b>
                                <p class="d-block" >`+response.product.product_name+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Alias Name </b>
                                <p class="d-block" >`+response.product.product_alias_name+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Short Name </b>
                                <p class="d-block" >`+(response.product.product_short_name)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Category </b>
                                <p class="d-block" >`+(response.product.category_name)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Sub Category </b>
                                <p class="d-block" >`+(response.product.sub_category_name)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Brand </b>
                                <p class="d-block" >`+(response.product.product_brand)+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Unit of Measure </b>
                                <p class="d-block" >`+response.product.unit_measurement_name+`</p>
                              </div>
                              <div class="col-md-12 mb-3">
                                <b>Other Remark </b>
                                <p class="d-block" >`+response.product.other+`</p>
                              </div>`;
                              if(response.product.product_image != '' && response.product.product_image != null){
                                main_content += `<div class="col-md-12 mb-3">
                                                    <b>Product Image </b>
                                                      <p class="d-block" >
                                                         <img src="../img/product/`+ response.product.product_image + `" class="onErrorDetetctClass_` + response.product.product_id + `" onerror="imgError(` + response.product.product_id +`)" width="75%" height="50%">
                                                      </p>
                                                </div>`
                              }else{
                                main_content += `<div class="col-md-12 mb-3">
                                                    <b>Product Image </b>
                                                      <p class="d-block" >No Product Image Found!</p>
                                                </div>`
                              }  
              $('#product_detail').html(main_content);
            } 
          }
        });
}
function imgError(id) {

  src = "../img/ajax-loader.gif";
  $('.onErrorDetetctClass_'+id).attr('src',src);
  $('.onErrorDetetctClass_'+id).attr('width',"75px");
  $('.onErrorDetetctClass_'+id).attr('height',"50px");
}
function paperSizeSetData(id)

{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getXeroxPaperSizeData",
                xerox_paper_size_id :id,
              },
            success: function(response){
              $('#xerox_paper_size_id').val(response.xerox.xerox_paper_size_id);
              $('#xerox_paper_size_name').val(response.xerox.xerox_paper_size_name);  
              $('#xerox_paper_size_desc').val(response.xerox.xerox_paper_size_desc); 
            }
    })
}

function xeroxSizeShowDetails(id)
{  
  var detail = '';
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getXeroxPaperSizeData",
                xerox_paper_size_id :id,
              },
            success: function(response){
              if(response.status == 200){
              detail = `<div class="row mx-0">
                          <div class="col-md-12 mb-3">
                            <b>Paper Size : </b>
                            <p class="d-block">`+ response.xerox.xerox_paper_size_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Description : </b>
                            <p class="d-block">`+ response.xerox.xerox_paper_size_desc +`</p>
                          </div>
                  </div>`;
                  $('#xeroxPaperModelDiv').html(detail);
              }
            }
            
    })
}

function xeroxCategorySetData(id)

{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getXeroxCategoryData",
                xerox_type_id :id,
              },
            success: function(response){
              $('#xerox_type_id').val(response.category.xerox_type_id);
              $('#xerox_type_name').val(response.category.xerox_type_name);  
              $('#xerox_type_desc').val(response.category.xerox_type_desc); 
            }
    })
}

function xeroxTypeShowDetails(id)
{  
  var detail = '';
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getXeroxCategoryData",
                xerox_type_id :id,
              },
            success: function(response){
              if(response.status == 200){
              detail = `<div class="row mx-0">
                          <div class="col-md-12 mb-3">
                            <b>Paper Size : </b>
                            <p class="d-block">`+ response.category.xerox_type_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Description : </b>
                            <p class="d-block">`+ response.category.xerox_type_desc +`</p>
                          </div>
                  </div>`;
                  $('#xeroxCategoryModelDiv').html(detail);
              }
            }
            
    })
}

function xeroxDocShowDetails(id)
{  
  var detail = '';
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getXeroxDocData",
                xerox_doc_id :id,
              },
            success: function(response){
              if(response.status == 200){
              detail = `<div class="row mx-0">
                          <div class="col-md-12 mb-3">
                            <b>User Name : </b>
                            <p class="d-block">`+ response.doc.user_full_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Xerox Document Name : </b>
                            <p class="d-block"><a href = "`+'../img/documents/'+response.doc.xerox_doc_name+`">`+ response.doc.xerox_doc_name +`</a></p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Xerox Type : </b>
                            <p class="d-block">`+ response.doc.xerox_type_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Paper Size : </b>
                            <p class="d-block">`+ response.doc.xerox_paper_size_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Notes : </b>
                            <p class="d-block">`+ response.doc.xerox_notes +`</p>
                          </div>
                  </div>`;
                  $('#xeroxDocModelDiv').html(detail);
              }
            }
            
    })
}

function orderStatusChange(value, id){
  var csrf =$('input[name="csrf"]').val();
  $(".ajax-loader").show();
  $.ajax({
      url: "controller/statusController.php",
      cache: false,
      type: "POST",
      data: {
              value: value,
              id : id,
              status:"orderStatus",
              csrf:csrf},
      success: function(response){
          $(".ajax-loader").hide();
          
          if(response==1) {
          document.location.reload(true);
          swal("Status Changed", {
                      icon: "success",
                  });
          } else {
          document.location.reload(true);
          swal("Something Wrong!", {
                      icon: "error",
                  });

          }
      }
  });
}
/* Dollop Infotech Date 22/Oct/2021 -  */

/* Dollop Infotech Date 15-Nov-2021 -  */
function vendorProductCategoryMasterShowDetails(id)
{  
  var detail = '';
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getVendorProductCategoryData",
                vendor_product_category_id :id,
              },
            success: function(response){
              if(response.status == 200){
              detail = `<div class="row mx-0">
                          <div class="col-md-12 mb-3">
                            <b>Vendor Name : </b>
                            <p class="d-block">`+ response.product.vendor_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Vendor Category Name : </b>
                            <p class="d-block">`+ response.product.vendor_category_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Vendor Product Category Name : </b>
                            <p class="d-block">`+ response.product.vendor_product_category_name +`</p>
                          </div>
                          <div class="col-md-12 mb-3">
                            <b>Product Category Image : </b>
                            <p class="d-block"><a href = "`+'../img/vendor_category/'+response.product.vendor_product_category_image+`" target="_blank">`+ response.product.vendor_product_category_image +`</a></p>
                          </div>
                  </div>`;
                  $('#vendorProductDetailModelDiv').html(detail);
              }
            }
            
    })
}

function vendorProductCategorySetData(id)

{ 
   
  $('#removeImageValid').hide();
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getVendorProductCategoryData",
                vendor_product_category_id :id,
              },
            success: function(response){
              $('#vendor_product_category_id').val(response.product.vendor_product_category_id);
              $('#vendor_product_category_name').val(response.product.vendor_product_category_name);
              $('#vendor_id').val(response.product.vendor_id); 
              $('#vendor_id').select2(); 
              $('#vendor_product_category_image_old').val(response.product.vendor_product_category_image); 
            }
    })
}

function vendorProductSetData(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getCanteenAndStationaryProductData",
                vendor_product_id :id,
              },
            success: function(response){
              $('#vendor_product_category_id').val(response.product.vendor_product_category_id);
              $('#vendor_product_name').val(response.product.vendor_product_name);
              $('#vendor_product_id').val(response.product.vendor_product_id); 
              $('#vendor_product_category_id').select2(); 
            }
    })
}

function vendorProductImageSetData(vendor_product_image_id,vendor_product_variant_id,vendor_product_image_name)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
  $('#vendor_product_image_id').val(vendor_product_image_id);
  $('#vendor_product_variant_id').val(vendor_product_variant_id);
  $('#vendor_product_image_name_old').val(vendor_product_image_name);
  $('#productVariantImageModal').modal();  
}

function ChangeCompnayAttendace(type)
{
  
  if(type == 1 || type =="")
  {
    if(type=="")
    {
      $('.UserlocationRangeDiv').show();
      $('.branchDiv').hide();
    }
    else
    {
      $('.UserlocationRangeDiv').hide();
      $('.branchDiv').show();
      $('.blockList').show();

    }
    $('.locationRangeDiv').show();
    $('#take_attendance_selfie').removeClass('d-none');
    $('#attendance_with_matching_face').removeClass('d-none');
    $('#work_report_on').removeClass('d-none');
    $('#branch_geo_fencing').removeClass('d-none');
  }else if(type == 0 || type ==5){
    $('.blockList').hide();
    $('#take_attendance_selfie').addClass('d-none');
    $('#attendance_with_matching_face').addClass('d-none');
    $('#work_report_on').addClass('d-none');
    $('.locationRangeDiv').hide();
    $('#branch_geo_fencing').hide();
  }
  else
  {
    $('.blockList').show();
    $('#take_attendance_selfie').removeClass('d-none');
    $('#attendance_with_matching_face').removeClass('d-none');
    $('#work_report_on').removeClass('d-none');
    $('.locationRangeDiv').hide();
    $('#branch_geo_fencing').removeClass('d-none');
  }
}

function DeleteProductVariantImage(deleteValue, id, image) {
 
  swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            // $('form.deleteForm'+id).submit();
            $(".ajax-loader").show();
        $.ajax({
            url: "controller/deleteController.php",
            cache: false,
            type: "POST",
            data: {id : id,deleteValue
              :deleteValue, image:image},
            success: function(response){
              $(".ajax-loader").hide();
              // 
              if(response==1) {
                document.location.reload(true);
                // history.go(-1);
              } else {
                document.location.reload(true);
                // history.go(0);
              }
            }
          });

           
          } else {
            // swal("Your data is safe!");
          }
        });
}


function adminChangePin(admin_id){
  $('#admin_id').val(admin_id);
  $('#addPinModal').modal();  
}

function updateRangeValue(val) {
  document.getElementById('textInput').value=val; 
/*  /// $('#showRange').text(val+'KM'); 
 */}

function productVendorDetailModal(id)
{  
  var detail = '';
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getProductVendorDetail",
                vendor_id :id,
              },
            success: function(response){
              if(response.status == 200){
                $('#productVendorDetailModal').modal();
                detail = `<div class="row mx-0">
                            <div class="col-md-12 mb-3">
                              <b>Vendor Name : </b>
                              <p class="d-block">`+ response.vendor.vendor_name +`</p>
                            </div>
                            <div class="col-md-12 mb-3">
                              <b>Vendor Email : </b>
                              <p class="d-block">`+ response.vendor.vendor_email +`</p>
                            </div>
                            <div class="col-md-12 mb-3">
                              <b>Mobile Number : </b>
                              <p class="d-block">`+ response.vendor.vendor_mobile_country_code +` - `+ response.vendor.vendor_mobile +`</p>
                            </div>
                            <div class="col-md-12 mb-3">
                              <b>Vendor Address : </b>
                              <p class="d-block">`+ response.vendor.vendor_address +`</p>
                            </div>

                            <div class="col-md-12 mb-3">
                              <b>Vendor Latitude : </b>
                              <p class="d-block">`+ response.vendor.vendor_latitude +`</p>
                            </div>

                            <div class="col-md-12 mb-3">
                              <b>Vendor Longitude : </b>
                              <p class="d-block">`+ response.vendor.vendor_longitude +`</p>
                            </div>`;
                            if(response.vendor.vendor_logo != ''){
                              detail += `<div class="col-md-12 mb-3">
                                              <b>Vendor Logo : </b>
                                              <a target="_blank" href="../img/vendor_logo/`+ response.vendor.vendor_logo +`" class="d-block"><img width="50" height="50" src="../img/vendor_logo/`+ response.vendor.vendor_logo +`"></a>
                                            </div>
                                          </div>`;
                            }
                            
                    $('#product_vendor_detail').html(detail);
              }
            }
            
    })
}

function ideaBoxLike(id){
  var detail = '';
  $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {
              action:"getLikedIdeaBoxUsers",
              idea_id :id,
            },
          success: function(response){
            var main_content ='';
            if(response.status == 200){
              $('#likedUserModal').modal();
              
              $.each( response.like_users, function( key, value ) {
                main_content += `
                                    <div class="col-md-6 mb-3">
                                      <div class="user-like-list d-inline-block w-100 align-items-center border p-3 position-relative">
                                       <span class="liked_user-avtar"> 
                                       <img onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/`+value.user_profile_pic+`" alt="your image" class="rounded-circle p-1 lazyload">
                                       </span>
                                        <p class="d-block mb-0 ml-3">`+ value.user_full_name +`</p>
                                      </div>
                                    </div>
                                   
                                  `;
              });
              $('#like_user_detail').html(main_content);
            }
          }
          
  })
}

function ideaBoxComment(id){
  var detail = '';
  $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {
              action:"getCommentIdeaBoxUsers",
              idea_id :id,
            },
          success: function(response){
            var main_content ='';
            if(response.status == 200){
              $('#usersCommentModal').modal();
              $.each( response.like_users, function( key, value ) {
                main_content += ` 
                                    <div class="col-md-12 mb-3">
                                      <div class="user-like-list d-inline-block w-100 align-items-center border p-3 position-relative">
                                      
                                      <span class="liked_user-avtar"> <img onerror="this.src='img/user.png'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/`+value.user_profile_pic+`" alt="your image" class="rounded-circle p-1 lazyload"></span>
                                        <p class="d-block mb-0 ml-3">`+ value.user_full_name +`</p>
                                        <span class="d-block mb-0 ml-3">`+ value.idea_comment +`</span>`;
                                        if(value.sub_comment.length > 0){
                                          $.each( value.sub_comment, function( key, value01 ) {
                                            main_content += `<div class="col-md-12">
                                                              <div class="user-like-list d-inline-block w-100 position-relative user-sub-comment-list p-3 w-100">
                                                              <span class="liked_user-avtar"> <img class="" src="../img/users/recident_profile/`+value01.user_profile_pic+`"></span>
                                                                <p class="d-block mb-0 ml-3">`+ value01.user_full_name +`</p>
                                                                <span class="d-block mb-0 ml-3">`+ value01.idea_comment +`</span>
                                                              </div>
                                                            </div>`;
                                          });
                                        }
                                        main_content += `
                                      </div>
                                    </div>
                                  `;
              });
              $('#comment_detail').html(main_content);
            }
          }
          
  })
}


function updateBlockGeoLocation(id,latitude,longitude)
{
  $('#block_geofence_range').val("");

  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getBlockById",
        block_id :id,
      },
    success: function(response){
      
     $('#blockModal').modal();
     $('#block_geofence_range').val(response.block.block_geofence_range);
     $('#block_id').val(response.block.block_id);
     if(response.block.block_geofence_latitude !="")
     {
      $('#block_latitude').val(response.block.block_geofence_latitude);

     }
     if(response.block.block_geofence_longitude)
     {
      $('#block_longitude').val(response.block.block_geofence_longitude);

     }
     if(response.block.block_geofence_latitude !="" && response.block.block_geofence_longitude !=''){
      initialize(response.block.block_geofence_latitude,response.block.block_geofence_longitude);
     }else{
      initialize(latitude,longitude);
     }
    
     $('#showRange').text(response.block.block_geofence_range+'M');
     
    }
  });
  
 
}
function updateUserGeoRange(userId)
{
  $('#user_geofence_range').val("");
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getDeleteEmployeedata",
        user_id :userId,
      },
    success: function(response){
     $('#user_latitude').val(response.employee.user_geofence_latitude);
     $('#floor_id').val(response.employee.floor_id);
     $('#floor_id').select2();
     $('#user_longitude').val(response.employee.user_geofence_longitude);
     $('#user_geofence_range').val(response.employee.user_geofence_range);
     //$('#showRange2').text(response.employee.user_geofence_range+'M'); 
     $('#user_id_old').val(response.employee.user_id);
     $('.userDiv').hide();
     $('.blockDiv').hide();
     if(response.employee.user_geofence_longitude !="" && response.employee.user_geofence_latitude !=''){
      initialize2(response.employee.user_geofence_latitude,response.employee.user_geofence_longitude);
     }else{
      initialize2(latitude,longitude);
     }
     $('#UserRangeModal').modal();
     
    }
  });
  
}
function removeBlockGeoLocation(id)
{
  $.ajax({
    url: "controller/deleteController.php",
    cache: false,
    type: "POST",
    data: {
        deleteValue:"removeBLockGeoLocation",
        block_id :id,
        csrf:csrf
      },
    success: function(response){
      $(".ajax-loader").hide();
      
      if(response!=0) {
        document.location.reload(true);
      } else {
        document.location.reload(true);
        swal("Something Wrong!", {
                  icon: "error",
                });

      }
    }
  });
  
}
function removeUserGeoLocation(id)
{
  swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this data!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  }).then((willDelete) => {
      if (willDelete) {
        $.ajax({
          url: "controller/deleteController.php",
          cache: false,
          type: "POST",
          data: {
            deleteValue: "removUserGeoLocation",
            geo_fance_id: id,
          },
          success: function (response) {
            location.reload(0);
          }
        });
      }
    })
  
}
function updateUserRangeValue(val) {
  document.getElementById('textInput2').value=val; 
 /*  $('#showRange2').text(val+'KM');  */
}

/* Dollop Infotech Date 15-Nov-2021 - */

/* Dollop Infotech Date 25-Nov-2021 - */

function getShiftByFloorId(id)
{
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getShiftByFloorId",
          floor_id:id,
        },
      success: function(response){
        
        optionContent = `<option>-- Select Shift --</option>`;
        $.each(response.shift, function( index, value ) {
          optionContent += `<option value="`+value.shift_time_id+`" >`+value.shift_name+` (`+value.shift_time_view+`) `+value.show_off_name+`</option>`;
        });
        $('#shift_time_id').html(optionContent);
      }
  });
}
function UserDeleteActive(id)
{
  $.ajax({
    url: "controller/statusController.php",
    cache: false,
    type: "POST",
    data: {
        status:"UserDeleteActive",
        id :id,
        csrf:csrf,
      },
    success: function(response){
      $(".ajax-loader").hide();
      
      if(response==1) {
        document.location.reload(true);
        swal("Status Changed", {
                  icon: "success",
                });
      } else {
        document.location.reload(true);
        swal("Something Wrong!", {
                  icon: "error",
                });

      }
     
    }
  });
}
function getUserShiftByFloorId(id,user_id,shiftid="")
{
  if (shiftid != "" && shiftid > 0) {
    $('.HideNoteClass').show();
  }
  else {
    $('.HideNoteClass').hide();

  }
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getShiftByFloorId",
          floor_id:id,
        },
      success: function(response){
        $('#userIdForShift').val(user_id);
        $('#update_user_shift').modal();
        optionContent = `<option value=""> Select Shift</option>`;
        $.each(response.shift, function( index, value ) {
          if(shiftid==value.shift_time_id)
          {
            selected = "selected";
          }
          else
          {
            selected = "";

          }
          optionContent += `<option `+selected+` value="`+value.shift_time_id+`" >`+value.shift_name+` (`+value.shift_time_view+`) `+value.show_off_name+`</option>`;
        });
        $('#shift_id').html(optionContent);
      }
  });
}
function UpdateUserShift()
{
  var shift_time_id = $('#shift_id').val();
  var id =  $('#userIdForShift').val();
  var apply_from = $('#apply_from').val();
  if(shift_time_id !="")
  {
    $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"updateUserById",
          user_id:id,
          shift_time_id:shift_time_id,
          apply_from:apply_from,
        },
      success: function(response){
        location.reload(0);
      
      }
  });
  }
  
}
/* Dollop Infotech Date 25-Nov-2021 - */

/* Dollop Infotech Date 29-Nov-2021 */
function viewUserCheckInOutLocation(latitude,longitude,id,image){
  $("#attendanceDeclineReasonForm").hide();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getBlockById",
        block_id:id,
      },
    success: function(response){
      block_geofence_range=response.block.block_geofence_range;
      block_geofence_latitude=response.block.block_geofence_latitude;
      block_geofence_longitude=response.block.block_geofence_longitude;
      $('#UserCheckInLocation').modal();
      if(image !="")
      {
        $('.punchInImage').html('<img style="max-height:175px" width="100%" onerror="imgPunchError()" src="../img/attendance/'+image+'" alt=""></img>');
      }
      else
      {
        $('.punchInImage').html('<img style="max-height:175px" width="60%" onerror="imgPunchError()" src="img/user.png" alt=""></img>');

      }

      initialize(latitude,longitude,block_geofence_range,block_geofence_latitude,block_geofence_longitude);
    }
    });
  
}
function viewUserLocation(latitude,longitude){
      $('#UserCheckInLocation').modal();
      initialize(latitude,longitude);
}
function getFloorByBlockId(id)
{
  floor_id_old = $('#floor_id_old').val();
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getFloorByBlockId",
          block_id:id,
        },
      success: function(response){
        
        if(access_branchs_id=='') {
          optionContent = `<option value="0">All Departments</option>`;
        }
        $.each(response.floor, function( index, value ) {
          if(floor_id_old==value.floor_id)
          {
            var selected="selected";
          }
          else
          {
            var selected="";

          }
          optionContent += `<option `+selected+` value="`+value.floor_id+`" >`+value.floor_name+` (`+value.block_name+`)</option>`;
        });
        $('#floor_id').html(optionContent);
        $('.floor_id').html(optionContent);
      }
  });
}
/* Dollop Infotech Date 29-Nov-2021 */
/* Dollop Infotech Date 1-Dec-2021 */
function changeXeroxStatus(status,id,user_id){
  $(".ajax-loader").show();

  $.ajax({
    url: "controller/statusController.php",
    cache: false,
    type: "POST",
    data: {
          xerox_doc_master : status,
          id : id,
          user_id : user_id,
          status:"UpdatexeroxStatus", 
          csrf:csrf},
    success: function(response){
      $(".ajax-loader").hide();
      
      if(response==1) {
        document.location.reload(true);
        swal("Status Changed", {
                  icon: "success",
                });
      } else {
        document.location.reload(true);
        swal("Something Wrong!", {
                  icon: "error",
                });

      }
    }
  });
}

function getUser(floor_id)
{
  var user_id_old = $('#user_id_old').val();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUserByFloorForBank",
                floor_id:floor_id,
              },
            success: function(response){
              var userHtml ="";
              
              userHtml=`<option value=''> Select Employee </option>`;
              $.each(response.users, function( index, value ) {
                if(user_id_old==value.user_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                }
                userHtml +=`<option `+selected+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });
              
             $('#user_id').html(userHtml);
            }
    })
}

function editBlockLatLong(id)
{
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getBlockById",
        block_id :id,
      },
    success: function(response){
     $('#blockModal2').modal();

     
     $('#update_block_id').val(response.block.block_id);
     if(response.block.block_geofence_latitude !="")
     {
      $('#block_latitude').val(response.block.block_geofence_latitude);

     }
     if(response.block.block_geofence_longitude)
     {
      $('#block_longitude').val(response.block.block_geofence_longitude);

     }
     if(response.block.block_geofence_latitude !="")
     {
      initialize(response.block.block_geofence_latitude,response.block.block_geofence_longitude);

     }
     $('#block_geofence_range').val(response.block.block_geofence_range);
     /* $('#showRange').text(response.block.block_geofence_range+'KM'); */
     
    }
  });
}
/* Dollop Infotech Date 1-Dec-2021 */


/* Dollop Infotech Date 02-Dec-2021 */


function deleteUserFaceData(id) {
  swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            // $('form.deleteForm'+id).submit();
        $.ajax({
            url: "controller/deleteController.php",
            cache: false,
            type: "POST",
            data: {id : id,deleteValue
              :'deleteUserFaceData'},
            success: function(response){
              // 
              if(response==1) {
                document.location.reload(true);
                // history.go(-1);
              } else {
                document.location.reload(true);
                // history.go(0);
              }
            }
          });
          
          } else {
            // swal("Your data is safe!");
          }
        });
}
/* Dollop Infotech Date 06 Dec-2021 */
function changeHolidayStartEnd(date)
{
  if(date !="")
  {
    changes(date);
  }
}
function changes(date)
{
  $('#default-datepicker-holiday_end_date').datepicker('destroy');
 
  $( "#default-datepicker-holiday_end_date" ).datepicker({
     startDate: date,
    format: 'yyyy-mm-dd',
     
   })
  
}

function changeWfDate(date) {
 
  $('#datepicker-wt').datepicker('destroy');
 
  $( "#datepicker-wt" ).datepicker({
      startDate: date,
      format: 'yyyy-mm-dd',
      endDate: $('#joining_date_validate').val(),
     
   })
  
}

function courseLessonTypeDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getLessonTypeById",
                lesson_type_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#lesson_type').val(response.lesson_type.lesson_type); 
              $('#lesson_type_id').val(response.lesson_type.lesson_type_id); 
              
            }
    })
}

function AttendaceReport(user_id,society_id,date)
{
  var main_content = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {user_id :user_id,
          date:date,
          action:"get_attendance_today_attendance_data",
          society_id: society_id,
    },
    success: function(response){
      $('#addAttendaceReportModal').modal();
       
     if(response.status==200){
        if(response.data.attendance_status==0)
     {
        var status="Pending";
     }
     else if(response.data.attendance_status==1)
     {
        var status="Approve";
     }
     else
     {
        var status="Declined";
     }
     if(response.data.attendance_date_end !="0000-00-00")
     {
      response.data.attendance_date_end = response.data.attendance_date_end;
     }
     else
     {
      response.data.attendance_date_end = "";
     }
     if(response.data.punch_out_time !="00:00:00")
     {
      response.data.punch_out_time = response.data.punch_out_time;
     }
     else
     {
      response.data.punch_out_time = "";
     }
     main_content += `<div class="col-md-6 mb-3">
                        <b>Attendance Status </b>
                        <p class="d-block" >`+status+`</p>
                      </div>
                      <div class="col-md-6 mb-3 ">
                        <b>Attendance Start Date  </b>
                        <p class="d-block " >`+response.data.attendance_date_start+`</p>
                      </div>
                      <div class="col-md-6 mb-3 ">
                        <b>Attendance End Date  </b>
                        <p class="d-block" >`+response.data.attendance_date_end+`</p>
                      </div>
                      <div class="col-md-6 mb-3 ">
                        <b>Punch In </b>
                        <p class="d-block" >`+response.data.punch_in_time+`</p>
                      </div>
                      <div class="col-md-6 mb-3">
                        <b>Punch Out </b>
                        <p class="d-block" >`+response.data.punch_out_time+`</p>
                      </div>
                      <div class="col-md-6 mb-3">
                        <b>Work Report Title</b>
                        <p class="d-block" >`+response.data.work_report_title+`</p>
                      </div>
                      <div class="col-md-6 mb-3">
                        <b>Work Report </b>
                        <p class="d-block" >`+response.data.work_report+`</p>
                      </div>
                      <div class="col-md-6 mb-3">
                        <b>Attendance Reason</b>
                        <p class="d-block" >`+response.data.attendance_reason+`</p>
                      </div>`;
                      $.each(response.data.attedance, function( index, value ) {  
        main_content  += `<div class="col-md-6 mb-3">
                            <b>Break:`+value.attendance_type_name+`</b>
                            <p class="d-block" >`+value.total_break+`</p>
                         </div>`; 
                        });
                      
                        
      $('#attendanceReportData').html(main_content);
     }
    
    }
  });
}


function changeUserAttendace(id,floor_id,shiftType,date,time="")
{
  $('#ChanngeAttendaceDataModal').modal();
  $('#attendance_id').val(id);
  if(shiftType=="Day"){
  // $('#hidePunchDateForShift').hide();
    $('.indate').val(date);
    $('.time-picker-shft-Out_update').change(function () {
     
     val = date+" "+this.value;
     val2 = date+" "+time;
     var date1 = new Date(val);
      var date2 = new Date(val2);
      if(date1.getTime() < date2.getTime()){
        swal("Please Select Less Time From Shift Start");
         $('#punch_out_time').val('');
      }
    });

    nextDate =  new Date(new Date(date).setDate(new Date(date).getDate() + 1));
    nextDate =  nextDate.toISOString().substr(0, 10);
    $('#hidePunchDateForShift').show();
    showDate = "";
    showDate +=`<option value="`+date+`">`+date+`</option>`;
    $('#punch_out_date').html(showDate);
  }
  else
  {
      $('#punch_out_date').change(function () {
        $('#punch_out_time').val('');
      });
      $('.time-picker-shft-Out_update').change(function () {
      
        punch_out_date = $('#punch_out_date').val();
        //alert(punch_out_date);
        val = punch_out_date+" "+this.value;
        val2 = date+" "+time;
        var date1 = new Date(val);
        var date2 = new Date(val2);
        if(date1.getTime() < date2.getTime()){
          swal("Please Select Less Time From Shift Start");
            $('#punch_out_time').val('');
        }

      });
    
      // add a day
        nextDate =  new Date(new Date(date).setDate(new Date(date).getDate() + 1));
        nextDate =  nextDate.toISOString().substr(0, 10);
        $('#hidePunchDateForShift').show();
        showDate = "";
        showDate=`<option> Select Date </option>`;
        showDate +=`<option value="`+date+`">`+date+`</option>`+
                  `<option  value="`+nextDate+`">`+nextDate+`</option>`;
        $('#punch_out_date').html(showDate);
             
  }
  
 
}
if($('#punchTypeDid').val()==1)
{
  floor_id = $('#floor_id_old').val();
 // alert(floor_id);
  getUserForSalary(floor_id)
}

function updateUserAttendanceData()
{
  var punch_out_time = $('#punch_out_time').val();
  var  punch_out_date = $('.punch_out_date').val();
  var  indate = $('.indate').val();
  if(punch_out_date==null){
    punch_out_date = indate;
  }
  var  attendance_id = $('#attendance_id').val();
 
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"updateAttendanceById",
        attendance_id:attendance_id,
        punch_out_time:punch_out_time,
        punch_out_date:punch_out_date,
      },
    success: function(response){
      
    }
});
}

function getUserForSalarySlip(floor_id)
{
  var user_id_old = $('#user_id_old').val();
  var year = $('#year').val();
  var month = $('#month').val();

  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUserByFloorForSalarySlip",
                floor_id:floor_id,
                year:year,
                month:month,
              },
            success: function(response){
              var userHtml ="";

              userHtml=`<option value=''> Select Employee </option>`;
              $.each(response.users, function( index, value ) {

                if(user_id_old==value.user_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                }
                userHtml +=`<option `+selected+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });

             $('#user_id').html(userHtml);
             $('.user_id01').html(userHtml);
            }
    })
}

function getVisitorFloorByBlockId(id)
{
  $(".ajax-loader").show();
  $('#user_id').html('<option value="">-- Select Employee --</option> ');
  $('#uId').html('<option value="">-- Select Employee --</option> ');
  var optionContent = "";
  if (id != "All") {
    
    $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        data: {
            action:"getFloorByBlockId",
            block_id:id,
          },
        success: function(response){
          $(".ajax-loader").hide();
          optionContent = `<option value=""> Select Department </option>`;
          $.each(response.floor, function( index, value ) {
            optionContent += `<option value="`+value.floor_id+`" >`+value.floor_name+`</option>`;
          });
          $('#floor_id').html(optionContent);
          $('.floor_id').html(optionContent);
          $('#dId').html(optionContent);
        }
    });
  }
  else
  {
    $(".ajax-loader").hide();
    optionContent = `<option value="All">All</option>`;
    $('#floor_id').html(optionContent);
    $('.floor_id').html(optionContent);
    }
}



function getVisitorFloorByBlockIdAll(id,na="")
{
  $(".ajax-loader").show();
  $('#user_id').html('<option value="">-- Select Employee --</option> ');
  $('#uId').html('<option value="">-- Select Employee --</option> ');
  var optionContent = "";
  if (id != "All") {
    
    $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        data: {
            action:"getFloorByBlockId",
            block_id:id,
          },
        success: function(response){
          $(".ajax-loader").hide();
          if(na == '')
          {
            optionContent = `<option value="0"> All Department </option>`;
          }
          else
          {
            optionContent = `<option value="">--Select--</option>`;
          }
          $.each(response.floor, function( index, value ) {
            optionContent += `<option value="`+value.floor_id+`" >`+value.floor_name+`</option>`;
          });
          $('#floor_id').html(optionContent);
          $('.floor_id').html(optionContent);
          $('#dId').html(optionContent);
        }
    });
  }
  else
  {
    $(".ajax-loader").hide();
    optionContent = `<option value="All">All</option>`;
    $('#floor_id').html(optionContent);
    $('.floor_id').html(optionContent);
    }
}


function getEvntFltrFloorByBlockId(id)
{

  $(".ajax-loader").show();
 
  var optionContent = "";
  if (id != "All") {
    
    $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        data: {
            action:"getFloorByBlockId",
            block_id:id,
          },
      success: function (response) {
       
          $(".ajax-loader").hide();
          optionContent = `<option value=""> Select Department </option>`;
          $.each(response.floor, function( index, value ) {
            optionContent += `<option value="`+value.floor_id+`" >`+value.floor_name+`</option>`;
          });
          $('.dpt_id').html(optionContent);
       
        }
    });
  }
  else
  {
    $(".ajax-loader").hide();
   
    optionContent = `<option value="">Select Department</option>`;
    if(access_branchs_id=='') {
      optionContent = `<option value="">All Department</option>`;
    }
    $('.dpt_id').html(optionContent);
    }
}


function getVisitorUserByFloor(floor_id)
{
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUserByFloor",
                floor_id:floor_id,
              },
            success: function(response){
              var userHtml ="";
              userHtml=`<option value="">Select User </option>`;
              
              $.each(response.users, function( index, value ) {
                userHtml +=`<option value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });
              
             $('#user_id').html(userHtml);
            }
    })
} 

function birthdayNotification(user_id)
{
  $('.modal-title-noti').text('Send Birthday Notification');
  $('#title').val('Happy Birthday');
  $('#celebrationType').val('birthday');
  $('#birthNotiModal').modal();
  $('#user_id').val(user_id);
}
/* Dollop Infotech Date 06-Dec-2021 */


function zoneDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getZone",
                zone_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#zone_id').val(response.zone.zone_id); 
              $('#zone_name').val(response.zone.zone_name); 
            }
    });
}

function employeeLevelDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getEmployeeLevel",
                level_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#level_id').val(response.level.level_id); 
              $('#level_name').val(response.level.level_name); 
              if(response.level.parent_level_id != 0){
                $('#parent_level_id').val(response.level.parent_level_id); 
                $('#parent_level_id').select2();
              }else{
                $('#parent_level_id').val(''); 
                $('#parent_level_id').select2();
              }
            }
    });
}


function getSubDepartmentByFloorId(id)
{
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getSubDepartmentByFloorId",
          floor_id:id,
        },
      success: function(response){
        
        optionContent = `<option>-- Select Sub Department --</option>`;
        $.each(response.sub_department, function( index, value ) {
          optionContent += `<option value="`+value.sub_department_id+`" >`+value.sub_department_name+`</option>`;
        });
        $('#sub_department_id').html(optionContent);
      }
  });
}

function getVendorProductCategoryByVendorId(id)
{
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getVendorProductCategoryByVendorId",
          vendor_id:id,
        },
      success: function(response){
        
        optionContent = `<option value="">All Category</option>`;
        $.each(response.product_category, function( index, value ) {
          optionContent += `<option value="`+value.vendor_product_category_id+`" >`+value.vendor_product_category_name+`</option>`;
        });
        $('#cId').html(optionContent);
      }
  });
}

function getVendorProductByVendorId(id)
{
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getVendorProductByVendorId",
          vendor_id:id,
        },
      success: function(response){
        
        optionContent = `<option value="">All Product</option>`;
        $.each(response.vendor_product, function( index, value ) {
          optionContent += `<option value="`+value.vendor_product_id+`" >`+value.vendor_product_name+`</option>`;
        });
        $('#pId').html(optionContent);
      }
  });
}
function bulkHolidayAdd(society_id)
{
  $.ajax({
    url: "controller/BulkHolidayController.php",
    cache: false,
    type: "POST",
    data: {
      society_id: society_id,
      token: "token",
      csrf:"dfjsdfjdkfsdkfjks"
    },
    success: function (response) {
     // location.reload();
    }
  });
}

function updateUserAttendace(value, attendance_id, attendance_punch_out_missing_id, attendance_date_end, punch_out_time) {
 
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "getPunchOutMissingRequest",
      attendance_punch_out_missing_id: attendance_punch_out_missing_id,
    },
    success: function (response) {
      $('.resonId').text(response.missing_request.punch_out_missing_reason);

      if (value == 1) {
        if (attendance_date_end != "0000-00-00") {
          $('#attendance_date_end').val(attendance_date_end);
        }
        $('.totalWorkingHours').html(response.missing_request.total_hours);
        $('#punch_out_time').val(punch_out_time);
        $('.attendance_id').val(attendance_id);
        $('.attendance_punch_out_missing_status').val(value);
        $('.attendance_punch_out_missing_id').val(attendance_punch_out_missing_id);
        $('#updateUserAttendace').modal(); 
      }else if(value == 2){
        $('.attendance_id').val(attendance_id);
        $('.attendance_punch_out_missing_status').val(value);
        $('.attendance_punch_out_missing_id').val(attendance_punch_out_missing_id);
        $('#rejectReasonModal').modal(); 
      }
    }
  });

  
}

function acceptUserPunchOutRequest()
{
  var attendance_date_end = $('#attendance_date_end').val();
  var punch_out_time = $('#punch_out_time').val();
  var attendance_id = $('#attendance_id').val();
  var attendance_punch_out_missing_id = $('#attendance_punch_out_missing_id').val();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"acceptUserPunchOutRequest",
        attendance_date_end:attendance_date_end,
        punch_out_time:punch_out_time,
        attendance_id:attendance_id,
        attendance_punch_out_missing_id:attendance_punch_out_missing_id,
      },
    success: function(response){
      if(response.status==200) {
        // document.location.reload(true);
         swal("Updated Successfully", {
                   icon: "success",
                 });
          document.location.reload(true);
       } else {
         document.location.reload(true);
         swal("Something Wrong!", {
                   icon: "error",
                 });

       }
    }
});
}

function workReportModal(id)
{
  $.ajax({
    url: "workReportDetail.php",
    cache: false,
    type: "POST",
    data: {
        work_report_id:id
      },
    success: function(response){
      $('#detail_view').modal();   
      $('#work_report').html(response);
    }
  });
}


function punchOutMissingRequestModal(id)
{
 
  var main_content="";
  $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {
              action:"getPunchOutMissingRequest",
              attendance_punch_out_missing_id:id,
              status:status,
              dId:status,
              status:status,
            },
          success: function(response){
            $(".ajax-loader").hide();
            if(response.status==200) {  
              $('#detail_view').modal();   
             
              main_content += `<div class="col-md-6 mb-3">
                                <b>Name </b>
                                <p class="d-block" >`+response.missing_request.user_full_name+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Punch In </b>
                                <p class="d-block" >`+response.missing_request.punch_in_time+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Status Changed By </b>
                                <p class="d-block" >`+response.missing_request.changed_by+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Start Date</b>
                                <p class="d-block" >`+response.missing_request.attendance_date_start+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Request Out Time </b>
                                <p class="d-block" >`+response.missing_request.attendance_time+`</p>
                              </div>
                              <div class="col-md-6 mb-3">
                                <b>Request Out Date</b>
                                <p class="d-block" >`+response.missing_request.attendance_date+`</p>
                              </div>
                              <div class="col-md-12 mb-3">
                                <b>Reason </b>
                                <p class="d-block" >`+response.missing_request.punch_out_missing_reason+`</p>
                              </div>`;
                              
                            
              $('#punch_out_missing_request').html(main_content);
            } 
          }
        });
}

function getAccessForUser()
{
  $(".ajax-loader").show();
  var access_type = $('#access_type').val();
  var access_by_id = $('#access_by_id').val();
  var block_id = $('#block_id').val();
  var floor_id = $('.floor_id').val();
  if(floor_id != null){
    var floorIds = floor_id.join("','");
  }
  $.ajax({
      url: "getAccessForUserList.php",
      cache: false,
      type: "POST",
      data: {
          block_id:block_id,
          floor_id:floorIds,
          access_type:access_type,
          access_by_id:access_by_id,
        },
      success: function(response){
        $(".ajax-loader").hide();
        $('#access_for_id').html(response);
      }
  });
}

function openLeaveModal(leave_id,attendance_id,id,date,Toatlhour) {
  
  if(leave_id>0)
  {
    $('#leaveModal2').modal();
    $('#leave_user_id').val(id);
    $('#leave_attendance_id').val(attendance_id);
    $('#leave_id').val(leave_id);
    $('#leave_date').val(date);
    $('#Toatlhour').val(Toatlhour);
    $('#DeleteLeave').show();
    $.ajax({
      url: "addLeaveFromAttendance.php",
      cache: false,
      type: "POST",
      data: {
          leave_id:leave_id,
          user_id:id,
          date:date,
         // action:'getUserLeaveById'
         
        },
      success: function(response){
        $('#leaveBoday').html(response);
        /* if(response.status="200")
        {
          $('#leave_type_id').val(response.leave.leave_type_id);
          $('#updateKey').text("Update");
          $('#leave_type_id').select2();
          $('#leave_day_type').val(response.leave.leave_day_type);
          $('#leave_day_type').select2();
        } */
      }
  });
  }else{
    
    $('#leaveModal2').modal();
    $.ajax({
      url: "addLeaveFromAttendance.php",
      cache: false,
      type: "POST",
      data: {
          user_id:id,
          leave_id:leave_id,
          date:date,
         // action:'getUserLeaveById'
         
        },
      success: function(response){
        $('#leave_user_id').val(id);
        $('#leave_attendance_id').val(attendance_id);
        $('#leave_id').val(leave_id);
        $('#leave_date').val(date);
        
        $('#leaveBoday').html(response);
      }
      });
    $('#updateKey').text("Add");
    
    $('#leave_day_type').val('').trigger('change');
    $('#leave_type_id').val('').trigger('change');
    $('#leave_type_id').select2();
    $('#leave_day_type').select2();
    $('#DeleteLeave').hide();

  }
}


function removeUserLeave()
{
  swal({
      title: "Are you sure to remove this leave ?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })
  
  .then((willDelete) => {
      if (willDelete) {
          leave_id = $('#leave_id').val();
            $.ajax({
              url: "controller/leaveController.php",
              cache: false,
              type: "POST",
              data: {
                  leave_id:leave_id,
                  action:'deleteUserLeave',
                  csrf:csrf,
                },
              success: function(response){
                location.reload();
               
              }
          });
              

      }else{
          swal("Your data is safe!");
      }

  });

}
function companyHomeDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getCompanyHome",
                company_home_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#company_home_id').val(response.company_home.company_home_id); 
              $('#company_home_title').val(response.company_home.company_home_title); 
              $('#company_home_description').val(response.company_home.company_home_description); 
            }
    })
}

function showEscalationData(escaltion_id)
{
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getEscalationById",
        escaltion_id:escaltion_id,
      },
    success: function(response){
      
      main_content="";
      $('#EscalationData').modal();
      escalation_replay_date = '';
      if(response.data.escalation_replay_date != null){
        escalation_replay_date = response.data.escalation_replay_date;
      }
      main_content += `<div class="col-md-6 mb-3">
              <b>Escalation By </b>
              <p class="d-block" >`+response.data.sender_name+`</p>
            </div>
            <div class="col-md-6 mb-3">
              <b>Escalation To </b>
              <p class="d-block" >`+response.data.reciver_name+`</p>
            </div>
            <div class="col-md-6 mb-3">
              <b>Escalation Title</b>
              <p class="d-block" >`+response.data.escalation_title+`</p>
            </div>
            <div class="col-md-6 mb-3">
              <b>Escalation Date </b>
              <p class="d-block" >`+response.data.escalation_created_date+`</p>
            </div>
            <div class="col-md-6 mb-3">
              <b>Reply Date</b>
              <p class="d-block" >`+escalation_replay_date+`</p>
            </div>
            <div class="col-md-6 mb-3">
              <b>Escalation Description </b>
              <p class="d-block" >`+response.data.escalation_description+`</p>
            </div>
            <div class="col-md-6 mb-3">
              <b>Escalation File </b>
              <a href="../img/escalation/`+response.data.escalation_file+`" target="_blank">`+response.data.escalation_file+`</a>
            </div>
            <div class="col-md-6 mb-3">
              <b>Escalation Reply</b>
              <p class="d-block" >`+response.data.escalation_replay+`</p>
            </div>`;
        $('#escalationView').html(main_content);
    }
})
  
}







//////////////////17-01-2021 (SHUBHAM)///////////////////////////////////////////////////
function companyServiceDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getCompanyService",
                company_service_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#company_service_id').val(response.company_service.company_service_id); 
              $('#company_service_name').val(response.company_service.company_service_name); 
              $('#company_service_image_old').val(response.company_service.company_service_image); 
            }
    })
}


function CompanyServiceDetailDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getCompanyServiceDetail",
                company_service_details_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#company_service_details_id').val(response.company_service_detail.company_service_details_id); 
              $('#company_service_details_title').val(response.company_service_detail.company_service_details_title); 
              $('#company_service_details_description').val(response.company_service_detail.company_service_details_description); 
              $('#company_service_id').val(response.company_service_detail.company_service_id); 
              $('#company_service_id').select2(); 
            }
    })
}


function EarningDeductionTypeData(id)
{
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getEarningDeductionById",
        salary_earning_deduction_type_id:id,
      },
    success: function(response){
      
      $('#EarningDeductionModal').modal(); 
      $('#addEarnDeductType').hide();
      $('#updateEarnDeductType').show();
      $('#earning_deduction_id').val(response.salary_earning_deduction_type_master.salary_earning_deduction_id); 
      $('#earning_deduction_name').val(response.salary_earning_deduction_type_master.earning_deduction_name); 
       $('#earning_deduction_type').val(response.salary_earning_deduction_type_master.earning_deduction_type); 
      $('#earning_deduction_type').select2(); 
    }
})
}

function getDepartmentByFloorIdForSalaryValue(id)
{
  var salary_earning_deduction_id = $('#salary_earning_deduction_id').val();
  //if(salary_earning_deduction_id > 0){
    var optionContent ="";
    $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        data: {
            action:"getDepartmentByFloorIdForSalaryValue",
            block_id:id,
            salary_earning_deduction_id:salary_earning_deduction_id,
          },
        success: function(response){
          if(response.status == 200){
            optionContent = `<option value="">--Select---</option>`;
            $.each(response.floor, function( index, value ) {
              optionContent += `<option value="`+value.floor_id+`" >`+value.floor_name+`</option>`;
            });
          }
          $('#floor_id').html(optionContent);
        }
    });
  
}


function getCompanyCurrentOpening(id)
{
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getCompanyCurrentOpening",
        company_current_opening_id:id,
      },
    success: function(response){
      main_content="";
      $('#companyCurrentOpeningModal').modal();
      main_content += `<div class="col-md-12 mb-3">
              <b>Title</b>
              <p class="d-block" >`+response.current_opening.company_current_opening_title+`</p>
            </div>
            <div class="col-md-12 mb-3">
              <b>Position</b>
              <p class="d-block" >`+response.current_opening.company_current_opening_position+`</p>
            </div>
            <div class="col-md-12 mb-3">
              <b>Timing</b>
              <p class="d-block" >`+response.current_opening.company_current_opening_timing+`</p>
            </div>
            <div class="col-md-12 mb-3">
              <b>Description </b>
              <p class="d-block" >`+response.current_opening.company_current_opening_description+`</p>
            </div>
            <div class="col-md-12 mb-3">
              <b>Address</b>
              <p class="d-block" >`+response.current_opening.company_current_opening_address+`</p>
            </div>`;
        $('#showCompanyCurrentOpening').html(main_content);
    }
})
  
}


function getUserForAddSalary(floor_id)
{
  
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUserByFloorForSalary",
                floor_id:floor_id,
              },
            success: function(response){
              var userHtml ="";

              userHtml=`<option value=''> Select Employee </option>`;
              $.each(response.users, function( index, value ) {

                /* if(user_id_old==value.user_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                } */
                userHtml +=`<option  value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });

             $('#user_id').html(userHtml);
           //  $('.user_id01').html(userHtml);
            }
    })
}
function getFloorByBlockIdAddSalary(id)
{
  floor_id_old = $('#floor_id_old').val();
  
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
    type: "POST",
      async:false,
      data: {
          action:"getFloorByBlockId",
          block_id:id,
        },
    success: function (response) {
       
        optionContent = `<option value="">Select Department</option>`;
        $.each(response.floor, function( index, value ) {
          if(floor_id_old==value.floor_id)
          {
            var selected="selected";
          }
          else
          {
            var selected="";
          }
          optionContent += `<option `+selected+` value="`+value.floor_id+`" >`+value.floor_name+` (`+value.block_name+`)</option>`;
        });
        $('#floor_id').html(optionContent);
        $('.floor_id').html(optionContent);
      }
  });
}
function editCommonValue(id)
{
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getSalaryCommonValueById",
        salary_common_value_id:id,
      },
    success: function(response){
      if(response.status==200)
      {
        $('#editSalaryCommonValue').modal();
        $('#amount_type').val(response.salary.amount_type);
        $('#salary_common_value_id').val(id);
        $('#amount_type').select2();
        $('#amount_value').val(response.salary.amount_value);
      }
    }
  });
  
}

function ShowUserFaceRequest(id)
 {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "getUserFaceRequestById",
      user_face_data_request_id: id,
    },
    success: function (response) {
      
      main_content = "";
      if (response.status == 200) {
        $('#UserFaceRequest').modal();
        $('#user_face_data_id').val(id)

;
        main_content += `<div class="col-md-12 text-center mb-3">
                           <a href="../img/users/recident_profile/`+ response.user_face_data_request.user_profile_pic + `" data-fancybox="images" data-caption="Photo Name : Profile Photo"> 
                                <img class="lazyload checkImage" style='width:200px;max-height:300px;' src="../img/users/recident_profile/`+ response.user_face_data_request.user_profile_pic + `"/>
                            </a>
                          </div>
                    <div class="col-md-6 mb-3">
                    <a href="../img/attendance_face_image/`+ response.user_face_data_request.face_data_image + `" data-fancybox="images" data-caption="Photo Name : Face App Photo 1"> 
                      <img class="lazyload checkImage"  width="100%"  height="245px" src="../img/attendance_face_image/`+ response.user_face_data_request.face_data_image + `"/>
                    </a>
                      </div>
                    <div class="col-md-6 mb-3">
                    <a href="../img/attendance_face_image/`+ response.user_face_data_request.face_data_image_two + `" data-fancybox="images" data-caption="Photo Name : Face App Photo 2"> 
                      <img class="lazyload checkImage"  width="100%"  height="245px" src="../img/attendance_face_image/`+ response.user_face_data_request.face_data_image_two + `"/>
                    </a>
                      </div>`;
        $('#UserFaceRequestData').html(main_content);
      }
      
    $('.checkImage').error(function(){
      $(this).attr('src','img/user.png');
    });
    }
  });
}

function viewUserCheckInOutImage(id)
{
  $(".ajax-loader").show();


  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"get_attendace_by_id",
        attendance_id:id,
      },
    success: function (response) {
     
      $('#UserCheckInOutImage').modal();
    
     if(response.status==200){
     
     if(response.data.attendance_range_in_km !="" && response.data.punch_in_in_range==1)
     {
          dstnc =   parseFloat(response.data.attendance_range_in_km);
          dstnc =   dstnc.toFixed(2);
          if (dstnc>0) {
            $('.Inkm').html(`<lable> : </lable><span>`+dstnc+` KM Away From Work Location</span>`);
          }
     } else if(response.data.attendance_range_in_km =="" && response.data.punch_in_in_range==1)
     {
          dstnc =   parseFloat(response.data.attendance_range_in_km);
          dstnc =   dstnc.toFixed(2);
          if (dstnc>0) {
            $('.Inkm').html(`<lable> : </lable>Out Of Range</span>`);
          }
     }
     else
     {
       $('.Inkm').html('In Geofencing range');
     }

     if(response.data.punch_in_address !="")
     {
      $('.InAddress').html(response.data.punch_in_address);
     } else {
      $('.InAddress').html('');
     }

     if(response.data.punch_out_address !="")
     {
      $('.OutAddress').html(response.data.punch_out_address);
     } else {
      $('.OutAddress').html('');
     }

     if(response.data.punch_out_km !="" && response.data.punch_out_in_range==1)
     {
        dstnc =   parseFloat(response.data.punch_out_km);
          dstnc =   dstnc.toFixed(2);
        if (dstnc>0) {
          $('.Outkm').html(`<lable> </lable><span>`+dstnc+` KM Away From Work Location</span>`);
        }
     }else if(response.data.punch_out_km =="" && response.data.punch_out_in_range==1 && response.data.attendance_date_end!='')
     {
        dstnc =   parseFloat(response.data.punch_out_km);
          dstnc =   dstnc.toFixed(2);
        if (dstnc>0) {
          $('.Outkm').html(`<lable> </lable>Out Of Range</span>`);
        }
     }else if(response.data.attendance_date_end!='' && response.data.punch_out_in_range==0) {
       $('.Outkm').html('In Geofencing range');
     } else {
       $('.Outkm').html('');
     }
     if(response.data.out_time !="00:00:00")
     {
      response.data.out_time = response.data.out_time;
     }
     else
     {
      response.data.out_time = "";
     }
     if(response.data.in_time !="00:00:00")
     {
      response.data.in_time = response.data.in_time;
     }
     else
     {
      response.data.in_time = "";
     }
     if(response.data.punch_in_image !="")
     {
      image =  response.data.punch_in_image ;
     }
     else
     {
      image= "user.png";
     }
     if(response.data.punch_out_image !="")
     {
      image2 =  response.data.punch_out_image;
     }
     else
     {
      image2= "user.png";
     }
     imageData="";
     imageOutData="";
     if(response.data.punch_in_image !="")
     {
      var imageData = `<a href="../img/attendance/`+image+`" data-fancybox="images" data-caption="Photo Name : `+image+`"><img style="max-height:300px;width:200px;"  src="../img/attendance/`+image+`"  href="#divForm`+id+`" class="btnForm checkCheckInOutImage lazyload" ></a>`
      $('#userCheckInOutImageData').html(imageData);
     }else {
        var imageData = "";
        $('#userCheckInOutImageData').html(imageData);
     } 

     $('.mapDiv').show();
     if(response.data.punch_in_latitude !="" && response.data.punch_in_longitude !="" && response.data.punch_out_latitude !="" && response.data.punch_out_longitude !="")
     {
       initializeCommonMap(response.data.punch_in_latitude,response.data.punch_in_longitude,response.data.punch_out_latitude,response.data.punch_out_longitude);
     } else if(response.data.punch_in_latitude !="" && response.data.punch_in_longitude !="") {
         initialize3(response.data.punch_in_latitude,response.data.punch_in_longitude);
     } else if(response.data.punch_out_latitude !="" && response.data.punch_out_longitude !=""){
         initialize2(response.data.punch_out_latitude,response.data.punch_out_longitude);
     } else {
        // initializeCommonMap("","","","");
        $('.mapDiv').hide();
     }

     if(response.data.punch_out_image !="")
      {
        var imageOutData = `<a href="../img/attendance/`+image2+`" data-fancybox="images" data-caption="Photo Name : `+image2+`"><img style="max-height:300px;width:200px;" src="../img/attendance/`+image2+`"  href="#divForm`+id+`" class="btnForm checkCheckInOutImage lazyload" ></a>`
        $('#userCheckOutImageData').html(imageOutData)
      }else {
        var imageData = "";
        $('#userCheckOutImageData').html(imageData);
      } 
      
      if(response.data.in_time =="")
      {
        $('.hideInData').hide();
      }
      else
      {
        $('.hideInData').show()
      }
      if(response.data.out_time =="")
      {
        $('.hideOutData').hide()
      }
      else
      {
        $('.hideOutData').show()
      }
      $('#in_time').text(response.data.in_time)
      $('#out_time').text(response.data.out_time)
      $('.checkCheckInOutImage').error(function(){
        $(this).attr('src','img/user.png');
       // $(this).attr('width','100%');
      });
    }
      $(".ajax-loader").hide();
    }
  });
  
}

function SalarySlipStatusChange(val,salary_slip_id,bms_admin) {
  if(val !=0)
  {
    $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"ChangeSalarySlipStatus",
          bms_id:bms_admin,
          status:val,
          is_notify:0,
          salary_slip_id:salary_slip_id,
        },
      success: function(response){
        if(response.status==200)
        {
         location.reload(1);
        }
      }
    });
  }
  else
  {
    $('#askStatusModal').modal();
    $('#salary_slip_id').val(salary_slip_id);
  }
}
function changeSalaryPublishStatus()
{
  salary_slip_id =  $('#salary_slip_id').val();
  bms_admin = $('#bms_admin').val();
  is_notify = $('input[name="is_notify"]:checked').val();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"ChangeSalarySlipStatus",
        bms_id:bms_admin,
        status:2,
        is_notify:is_notify,
        salary_slip_id:salary_slip_id,
      },
    success: function(response){
      if(response.status==200)
      {
       location.reload(1);
      }
    }
  });
  
}

function pendingAttendanceStatusChange(value, id, key,latitude,longitude,block_id,meter_range,km_range,in_image,out_lat,out_lng,out_image){
  $("#attendanceDeclineReasonForm").show();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "get_attendace_by_id",
      attendance_id: id,
    },
    success: function (response) {
    
      $("#approvInTime").html(response.data.in_time);
      $("#approvOutTime").html(response.data.out_time);
      if (response.data.attendance_reason != "") {
        $("#in_reason").html(response.data.attendance_reason);
      } else {
        $("#in_reason").html('');
      }
      if (response.data.punch_out_reason != "") {
        $("#out_reason").html(response.data.punch_out_reason);
      } else {
        $("#out_reason").html('');
      }
      if (response.status == 200) {
        if (response.data.punch_in_in_range == 1) {
          $('.in_reason_div').show();
            $('.Inaway').show();
            if(response.data.attendance_range_in_meter !="")
            {
            dstnc =   parseFloat(response.data.attendance_range_in_meter);
            dstnc =   dstnc.toFixed(2);
              $('.InawayShow').text(dstnc+" Meter Away From Work Location");
            }
            if(response.data.attendance_range_in_km !=0)
            {
              dstnc =   parseFloat(response.data.attendance_range_in_km);
              dstnc =   dstnc.toFixed(2);
              $('.InawayShow').text(dstnc+" Km Away From Work Location");
            }
        } else {
          $('.in_reason_div').hide();
          $('.InawayShow').text("In Geofencing range");
        }
        
        if (response.data.punch_out_in_range == 1) {
          $('.out_reason_div').show();
          $('.Outaway').show();
              if (response.data.punch_out_km != "") {
                dstnc =   parseFloat(response.data.punch_out_km);
                 dstnc =   dstnc.toFixed(2);
                $('.away').text(dstnc + " Away From Work Location");
              }
        }else {
          $('.out_reason_div').hide();
          $('.away').text("In Geofencing range");
        }
        
      }
    }
  });
  var csrf =$('input[name="csrf"]').val();
  $(".ajax-loader").show();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getBlockById",
        block_id:block_id,
      },
    success: function(response){
      block_geofence_range=response.block.block_geofence_range;
      block_geofence_latitude=response.block.block_geofence_latitude;
      block_geofence_longitude=response.block.block_geofence_longitude;
      $('#UserCheckInLocation').modal();
      $(".map").show();
      
      $('.punchInImage').show();
      $(".ajax-loader").hide();
     // in_image ="";
      if(in_image !=""){
        var imageData = `<a href="../img/attendance/`+in_image+`" data-fancybox="images" data-caption="Photo Name : `+in_image+`"><img onerror="imgOutPunchError()" style="max-height:300px;width:200px;"  src="../img/attendance/`+in_image+`"  href="#divForm`+id+`" class="btnForm checkCheckInOutImage lazyload" ></a>`
        $('.punchInImage').html(imageData);
        // $('.punchInImage').html('<img style="max-height:300px;width:200px;"   onerror="imgPunchError()" src="../img/attendance/'+in_image+'" alt=""></img>');
      }
      else{
        $('.punchInImage').html("");
      }
      if(out_image !=""){
        var imageData1 = `<a href="../img/attendance/`+out_image+`" data-fancybox="images" data-caption="Photo Name : `+out_image+`"><img onerror="imgOutPunchError()" style="max-height:300px;width:200px;"  src="../img/attendance/`+out_image+`"  href="#divForm`+id+`" class="btnForm checkCheckInOutImage lazyload" ></a>`
        $('.punchOutImage').html(imageData1);
        // $('.punchOutImage').html('<img  style="max-height:300px;width:200px;"  onerror="imgOutPunchError()" src="../img/attendance/'+out_image+'" alt=""></img>');
      }
      else{
        $('.punchOutImage').html("");
      }
      $(".mapDiv").show();
      if(longitude >0 && out_lat<1){
        // pucnh in marker only
        mapViewIn(latitude,longitude,block_geofence_range,block_geofence_latitude,block_geofence_longitude);
      } else if(longitude <1 && out_lat>0){
        // punch out marker only
        mapViewOut(out_lat, out_lng, block_geofence_range, block_geofence_latitude, block_geofence_longitude)
      }else if (out_lat != "" && out_lng != "" && out_lat >0 && out_lng >0 && longitude !="" && latitude !="") {
        // both marker
        mapViewCommon(out_lat, out_lng, block_geofence_range, block_geofence_latitude, block_geofence_longitude,latitude,longitude)
      } else  {
        $(".mapDiv").hide();
      }
      
    }
    });
  if(value != 2){
    $('#value').val(value);
    $('#id').val(id);
    $('#status').val(key);
    $('#csrf').val(csrf);
    $('.attendanceDeclineReasonClass').text('Approve');
    $(".attendanceDeclineReason").hide();
    // $('.attendanceDeclineReasonClass').click(function() {
    // $.ajax({
    //   url: "controller/statusController.php",
    //   cache: false,
    //   type: "POST",
    //   data: {value: value,
    //           id : id,
    //           status:key, 
    //           csrf:csrf},
    //   success: function(response){
    //       $(".ajax-loader").hide();
          
    //       if(response==1) {
    //     document.location.reload(true);
    //       swal("Status Changed", {
    //                   icon: "success",
    //               });
    //       } else {
    //         document.location.reload(true);
    //         swal("Something Wrong!", {
    //                     icon: "error",
    //                 });
    //         }
    //   }
    //   });
    //   });
  }
  else
  {
    $('.attendanceDeclineReasonClass').text('Decline');
    $(".attendanceDeclineReason").show();
    $('#value').val(value);
    $('#id').val(id);
    $('#status').val(key);
    $('#csrf').val(csrf);
   
  }
}
$('.declineCLass').click(function() {
  value =  $('#value').val();
  id = $('#id').val();
  key = $('#status').val();
  csrf =  $('#csrf').val();
  var csrf =$('input[name="csrf"]').val();
   var attendance_declined_reason =  $('#attendance_declined_reason').val();
 var error = 0;
  var csrf =$('input[name="csrf"]').val();
  if(value==2){
    if(attendance_declined_reason.trim() != ''){
        $(".ajax-loader").show();
          error  = 0;
        }
        else
        {
          error = 1;
        }
  }
      if(error==0){
        $.ajax({
          url: "controller/statusController.php",
          cache: false,
          type: "POST",
          data: {value: value,
                  id : id,
                  status:key, 
                  attendance_declined_reason:attendance_declined_reason,
                  csrf:csrf},
          success: function(response){
              $(".ajax-loader").hide();
              
              if(response==1) {
            document.location.reload(true);
              swal("Status Changed", {
                          icon: "success",
                      });
              } else {
                document.location.reload(true);
                swal("Something Wrong!", {
                            icon: "error",
                        });
                }
          }
          });
      }
  
});


function viewUserChecksImage(punch_in_image,id)
{
  $('#UserCheckInOutImage').modal();
  main_content = `<img width="100%"   onerror="imgPunchError()" src="../img/attendance/`+punch_in_image+`" alt=""></img>`;
  $('#userCheckInOutImageData').html(main_content);
}
function imgPunchError(){
  src = "img/user.png";
  $('.punchInImage').html('<img style="max-height:175px" width="200px"  src="'+src+'" alt=""></img>');
}
function imgOutPunchError(){
  src = "img/user.png";
  $('.punchOutImage').html('<img style="max-height:175px" width="200px"  src="'+src+'" alt=""></img>');
}
function ShowCommonValue(salary_common_value_id) {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "getSalaryCommonValueById",
      salary_common_value_id: salary_common_value_id,
    },
    success: function (response) {
      main_content = "";
      if (response.status = 200) {
        if (response.salary.amount_type == '0') {
          type = "Percentage";
        } else if (response.salary.amount_type == '2') {
          type = "Slab";
        } else {
          type = "Flat";
        }
        $('#salaryCommonValueModal').modal();
        main_content += '<div class="row mx-0">' +
          '<div class="col-md-6 form-group ">' +
          '<label for="exampleInputEmail1">Amount Type : </label>' +
          '<span  >' + type + '</span>' +
          '</div>';
        if (response.salary.amount_type == '0' && response.salary.earning_deduction_type == '1' && response.salary.percent_deduction_max_amount > '0') {
          main_content +=
            '<div class="col-md-6 form-group ">' +
            '<label for="exampleInputEmail1">Maximum Amount : </label>' +
            '<span> ' + response.salary.percent_deduction_max_amount + '</span>' +
            '</div>';
        }
        if (response.salary.earning_deduction_type == '1' && response.salary.amount_type == '0') {
          main_content +=
            '<div class="col-md-6 form-group ">' +
            '<label for="exampleInputEmail1">Percentage : </label>' +
            '<span> ' + response.salary.amount_value + ' %</span>' +
            '</div>';
        }
        if (response.salary.amount_type == '1') {
          main_content +=
            '<div class="col-md-6 form-group ">' +
            '<label for="exampleInputEmail1">Amount : </label>' +
            '<span> ' + response.salary.amount_value + '</span>' +
            '</div>';
        }
        if (response.salary.percent_min_max !="") {
          percent_min_max = JSON.parse(response.salary.percent_min_max);
          main_content +=
            '<div class="col-md-6 form-group ">' +
            '<label for="exampleInputEmail1">Percent min  : </label>' +
            '<span> ' + percent_min_max.percent_min + '</span>' +
            '</div><div class="col-md-6 form-group ">' +
            '<label for="exampleInputEmail1">Percent Max  : </label>' +
            '<span> ' + percent_min_max.percent_max + '</span>' +
            '</div>';
        }
        if (response.salary.amount_value_employeer > 0) {
          if (response.salary.amount_type == 0) {
            typeKey = '%';
          } else {
            typeKey = '';
          }
          main_content +=
            '<div class="col-md-6 form-group ">' +
            '<label for="exampleInputEmail1">Employer Contribution : </label>' +
            '<span> ' + response.salary.amount_value_employeer +' '+typeKey
            + '</span>' +
            '</div>';
        }
        main_content +=
          '<div class="col-md-6 form-group ">' +
          '<label for="exampleInputEmail1"> Name : </label>' +
          '<span  >' + response.salary.earning_deduction_name + '</span>' +
          '</div>' +
          '<div class="col-md-6 form-group ">' +
          '<label for="exampleInputEmail1">Remark: </label>' +
          '<span  >' + response.salary.salary_common_value_remark + '</span>' +
          '</div>' +
          '</div>';
        $('#salaryCommonValue').html(main_content);
        j = 1;
        k = 1;
        if (response.salary.amount_type != '1' && response.salary.earning_deduction_type == '1') {
          $('.ernDedData').show();
          // $('.SlabData').hide();
          earnDataDeductFrom = "";
          if (response.salary.salary_common_value_earn_deduction != "") {
            salary_common_value_deduction = response.salary.ern_deduct_name.split(',');
            $.each(salary_common_value_deduction, function (index, value) {
              earnDataDeductFrom += ` <tr>
                              <td>`+ value + `</td>
                            <tr>`;
            });
            $('#SalaryCommonValueEarnDedcutData').html(earnDataDeductFrom);
          }
        } else {
          $('.ernDedData').hide();
        }

        if (response.salary.amount_type == '2' && response.salary.earning_deduction_type == '1') {
          earnData = "";
          $('.SlabData').show();
          salarySlab = response.salary.slab_json.split("~");

          if (salarySlab.length > 0) {
            $.each(salarySlab, function (index, value) {

              maxMinValue = JSON.parse(value);
              earnData += ` <tr>
                              <td>`+ maxMinValue.min + `</td>
                              <td>`+ maxMinValue.max + `</td>
                              <td>`+ maxMinValue.value + `</td>
                            <tr>`;

            });
            $('#SalarySlabMinMaxValue').html(earnData);

          }
        } else {
          $('.SlabData').hide();
        }
      }
    }
  });

}

function autoCalculate(block_id, floor_id, salary_group_id, earnCLick = 0, salary_earning_deduction_id = '') {
  if (salary_earning_deduction_id != "") {
    earnFixed = $('.earning_' + salary_earning_deduction_id).val();
  } else {
    earnFixed = 0;
  }
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "autoCalculateCommonEarnDeductForSalarySlip",
      block_id: block_id,
      floor_id: floor_id,
      salary_group_id: salary_group_id,
      salary_earning_deduction_id: salary_earning_deduction_id,
      earnFixed: earnFixed,
    },
    success: function (response) {
     
      if (response.status = 200) {
        flatAmount=response.flatAmount
        finalAmnt = 0;
        amount_value_employeer = 0;
        $('#flatAmount').val(flatAmount);
        showCtcContribution = [];
        $.each(response.earn_deduct_calculation, function (index, value) {
          gross_salary = $('#gross_salary').val();
          if (parseFloat(value.amount_value_employeer) > 0) {
            showCtcAmnt = 0;
            var showCtc = [];
            if (value.salary_common_value_earn_deduction != "") {
                var ernDeDctNew2 = '';
                ernDeDctNew2 = value.salary_common_value_earn_deduction.split(',');
                employeer_percent = (value.amount_value_employeer);
                var  tAmountEmployer = 0;
                  $.each(ernDeDctNew2, function (p, j) {
                    var earnAmountEmployer = 0;
                    earnAmountEmployer = $(".earning_" + j).val();
  
                    if (earnAmountEmployer == NaN) {
                      earnAmountEmployer = 0;
                    }
                    if (earnAmountEmployer == "") {
                      earnAmountEmployer = 0;
                    }
                    

                    tAmountEmployer += parseFloat(earnAmountEmployer);
                  });
              showCtcAmnt = (tAmountEmployer) * (parseFloat(employeer_percent) / 100);
              
              if (parseFloat(value.percent_deduction_max_amount) > 0) {
                console.log('showCtcAmnt');
                console.log(showCtcAmnt);
               
                console.log(parseFloat(value.percent_deduction_max_amount));
                    if (parseFloat(value.percent_deduction_max_amount) < showCtcAmnt) {
                      showCtcAmnt = parseFloat(value.percent_deduction_max_amount);
                    } else {
                      showCtcAmnt = showCtcAmnt;
                    }
                  } else {
                    showCtcAmnt = showCtcAmnt;
                  }
                  amount_value_employeer = parseFloat(amount_value_employeer) + parseFloat(showCtcAmnt);
            } else {
              
              if (value.amount_type == "0") {
                tempEmployeer = (gross_salary) * (value.amount_value_employeer / 100);
                showCtcAmnt = tempEmployeer;
                amount_value_employeer = parseFloat(amount_value_employeer) + parseFloat(tempEmployeer);
               
              } else if (value.amount_type == "1") {
                showCtcAmnt = value.amount_value_employeer;
                amount_value_employeer = parseFloat(amount_value_employeer) + parseFloat(value.amount_value_employeer);
              }
            }
           
           
            showCtc = {'name':value.earning_deduction_name,'amount':showCtcAmnt};
            showCtcContribution.push(showCtc );
            
          }
          
        //  console.log(amount_value_employeer);
          $('#ctcPart').text(amount_value_employeer);
          $('#ctcPart').val(amount_value_employeer);
          gross_salary = $('#gross_salary').val();
          ctcPart =  $('#ctcPart').val();
          total_ctc = parseFloat(gross_salary)+parseFloat(ctcPart)+parseFloat(flatAmount);
           $('#total_ctc').val(total_ctc);
           $('#ctc').text(total_ctc);
          if (value.earning_deduction_type == "0") {
            if (value.amount_type == "0") {
             
              gross_salary = $('#gross_salary').val();
              eAmnt = $('.earning_' + value.salary_earning_deduction_id).val();
             
                error = "0";
              
              if (value.salary_common_value_earn_deduction != "") {
                
                var tAmount = 0;
                var ernDeDctNew = '';
                ernDeDctNew = value.salary_common_value_earn_deduction.split(',');
                
                if (salary_earning_deduction_id == "") {
                    error = "0";
                } else {
                  // if(jQuery.inArray(salary_earning_deduction_id, ernDeDctNew) !== -1)
                  
                 
                  if (parseInt(salary_earning_deduction_id )== parseInt(value.salary_earning_deduction_id) && value.salary_common_value_earn_deduction != "") {
                  
                    error = "1";
                  }
                }
                
                if (error == "0") {
                  percent = (value.amount_value);
                  $.each(ernDeDctNew, function (i, val) {
                    var earnAmount = 0;
                    earnAmount = $(".earning_" + val).val();
  
                    if (earnAmount == NaN) {
                      earnAmount = 0;
                    }
                    if (earnAmount == "") {
                      earnAmount = 0;
                    }
                    tAmount += parseFloat(earnAmount);
                  });
                  EfinalAmnt = (tAmount) * (percent / 100);
                } else {
                  
                  if (earnCLick == 1) {
                    EfinalAmnt = eAmnt;
                  } else {
                    Epercent = value.amount_value;
                    EfinalAmnt = (gross_salary) * (Epercent / 100);
                  //  EfinalAmnt = (gross_salary-flatAmount) * (Epercent / 100);
                  }
                }
               
              } else {
               // alert(value.earning_deduction_name);
               
                if (eAmnt == "" || eAmnt <= 0) {
                 /// alert(eAmnt);
                  Epercent = value.amount_value;
                  
                  EfinalAmnt = (gross_salary) * (Epercent / 100);
                }
              else {
                  sidValid = $('#sidValid').val();
                  if (earnCLick == 1) {
                    EfinalAmnt = eAmnt;
                  } else {
                    Epercent = value.amount_value;
                    EfinalAmnt = (gross_salary) * (Epercent / 100);
                   // EfinalAmnt = (gross_salary-flatAmount) * (Epercent / 100);
                  }
                }
              }
              


              if (value.percent_min_max != "") {
                percent_max = jQuery.parseJSON(value.percent_min_max).percent_max;
                percent_min = jQuery.parseJSON(value.percent_min_max).percent_min;
                if ((parseFloat(percent_max) >= parseFloat(gross_salary)) && (parseFloat(percent_min) <= parseFloat(gross_salary))) {
                  EfinalAmnt = EfinalAmnt;
                } else {
                  EfinalAmnt = 0;
                }
              } else {
                EfinalAmnt = EfinalAmnt;
              }
              $('.earning_' + value.salary_earning_deduction_id).val(EfinalAmnt);
              var totalEarn = 0;
              for (let index = 0; index < earnCount; index++) {
                earnAmount = $('#earning_' + index).val();
                if (earnAmount == NaN) {
                  earnAmount = 0;
                }
                if (earnAmount == "") {
                  earnAmount = 0;
                }
                totalEarn += +parseFloat(earnAmount);
              }
              earn = totalEarn;
              $('#total_earn').text(totalEarn);
              finalAmount = earn - (deduct);
              if (finalAmount != NaN) {
                $('#finalAmount').text((finalAmount));
                $('#net_salary').val((finalAmount));
              }
              checkCal();
              deductCalculation();
            } else {

              eAmnt2 = $('.earning_' + value.salary_earning_deduction_id).val();
              EfinalAmnt = 0;
              if (eAmnt2 == "" || eAmnt2 == 0) {
                gross_salary = $('#gross_salary').val();
                if (salary_earning_deduction_id != "") {
                  
                  EfinalAmnt = eAmnt2;
                } else {
                  EfinalAmnt = value.amount_value;
                }
              }
              else {
                sidValid = $('#sidValid').val();
                if (earnCLick == 1) {
                  EfinalAmnt = eAmnt2;
                } else {
                  gross_salary = $('#gross_salary').val();
                  Epercent = value.amount_value;
                  EfinalAmnt = value.amount_value;
                }
              }
              $('.earning_' + value.salary_earning_deduction_id).val(EfinalAmnt);
            }
          } else {
            if (value.amount_type == 2) {
              slabArray = value.slab_json.split("~");
              $('.d_' + value.salary_earning_deduction_id).val(0);
              for (let index = 0; index < slabArray.length; index++) {

                singleSlabe = jQuery.parseJSON(slabArray[index]);
                gross_salary = $('#gross_salary').val();
                var tAmount = 0;
                if (value.salary_common_value_earn_deduction != "") {
                  ernDeDct = value.salary_common_value_earn_deduction.split(',');
                  $.each(ernDeDct, function (i, val) {
                    var earnAmount = 0;
                    earnAmount = $(".earning_" + val).val();
                    if (earnAmount == NaN) {
                      earnAmount = 0;
                    }
                    if (earnAmount == "") {
                      earnAmount = 0;
                    }
                    tAmount += parseFloat(earnAmount);
                  });
                  //finalAmnt = (tAmount) * (percent / 100);
                }
                if (parseFloat(tAmount) >= parseFloat(singleSlabe.min) && parseFloat(tAmount) <= parseFloat(singleSlabe.max)) {
                  $('.d_' + value.salary_earning_deduction_id).val(singleSlabe.value);
                }
              }

            }
            else if (value.amount_type == 0) {
              if (value.salary_common_value_earn_deduction != "") {
                var tAmount = 0;
                ernDeDct = value.salary_common_value_earn_deduction.split(',');
                percent = (value.amount_value);
                $.each(ernDeDct, function (i, val) {
                  var earnAmount = 0;
                  earnAmount = $(".earning_" + val).val();

                  if (earnAmount == NaN) {
                    earnAmount = 0;
                  }
                  if (earnAmount == "") {
                    earnAmount = 0;
                  }
                  tAmount += parseFloat(earnAmount);
                });
                finalAmnt = (tAmount) * (percent / 100);
              }

              if (value.percent_min_max != "") {
                gross_salary = $('#gross_salary').val();
                percent_max = jQuery.parseJSON(value.percent_min_max).percent_max;
                percent_min = jQuery.parseJSON(value.percent_min_max).percent_min;
                if (parseFloat(percent_max) >= parseFloat(gross_salary) && parseFloat(percent_min) <= parseFloat(gross_salary)) {
                  if (parseFloat(value.percent_deduction_max_amount) > 0) {
                    if (parseFloat(value.percent_deduction_max_amount) >= finalAmnt) {
                      $('.d_' + value.salary_earning_deduction_id).val(finalAmnt);
                      $('.d_remark_' + value.salary_earning_deduction_id).html('');

                    } else {
                      $('.d_' + value.salary_earning_deduction_id).val(parseFloat(value.percent_deduction_max_amount));
                      $('.d_remark_' + value.salary_earning_deduction_id).html('Maximum Value ' + parseFloat(value.percent_deduction_max_amount) + ' Applicable');

                    }
                  } else {
                    $('.d_' + value.salary_earning_deduction_id).val(finalAmnt);
                  }
                } else {
                  $('.d_' + value.salary_earning_deduction_id).val('');
                }
              } else {
                if (parseFloat(value.percent_deduction_max_amount) > 0) {
                  if (parseFloat(value.percent_deduction_max_amount) >= finalAmnt) {
                    $('.d_' + value.salary_earning_deduction_id).val(finalAmnt);
                    $('.d_remark_' + value.salary_earning_deduction_id).html('');

                  } else {
                    $('.d_' + value.salary_earning_deduction_id).val(parseFloat(value.percent_deduction_max_amount));
                    $('.d_remark_' + value.salary_earning_deduction_id).html('Maximum Value ' + parseFloat(value.percent_deduction_max_amount) + ' Applicable');

                  }
                } else {
                  $('.d_' + value.salary_earning_deduction_id).val(finalAmnt);
                }
              }


              deductCalculation();
            }
          }
          
          var showContribution = ``;
          console.log(totalEarn);
          actualEarn = $('#total_earn').text();
          actualCtc = $('#total_ctc').val();
          if (showCtcContribution.length > 0) {
            showContribution += `<tr>
                                      <td>Employee Earning</td>
                                      <td>`+actualEarn+`</td>
                                    </tr> `;
            $.each(showCtcContribution, function (j, vl) {
              showContribution += `<tr>
                                      <td>`+vl.name+` Employer Contribution</td>
                                      <td>`+vl.amount+`</td>
                                    </tr> `;
            })
          
            showContribution += `<tr>
                                      <td></b>Total CTC </b></td>
                                      <td></b>`+actualCtc+`</b></td>
                                    </tr> `;
          }
         
          $('.showContribution').html(showContribution);
          

        });
      }

    }
  });

}

function showCOntributionModal() {
  $('#contributionModal').modal('show');
}


function getCompnayAttendaceUser(floor_id)
{
  var user_id_old = $('#user_id_old').val();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUserByFloorForGeo",
                floor_id:floor_id,
              },
            success: function(response){
              var userHtml ="";
              
              userHtml=`<option value=''> Select Employee </option>`;
              $.each(response.users, function( index, value ) {
                if(user_id_old==value.user_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";
                }
                userHtml +=`<option `+selected+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });
             $('#user_id').html(userHtml);
            }
    })
}

function PunchRequestAttendanceStatusChange(value, id, key,leaveKey,user_id,attendance_date_start,attendance_date_end,punch_in_time,punch_out_time,punch_in_request_day_type)
{
 // punch_in_request_day_type = "";
   $("#attendanceDeclineReasonForm").show();
  $("#UserCheckInLocation").modal();
  attendance_reason = $('#attendance_reason_' + id).val();
  $(".attendance_reason").html('<p>'+attendance_reason+'</p>');
  $(".workingHours").html('<p>'+leaveKey+'</p>');
  $(".punch_in_div").html('<p>'+attendance_date_start+' '+punch_in_time+'</p>');
  $(".punch_out_div").html('<p>'+attendance_date_end+' '+punch_out_time+'</p>');
  $("#punch_in_request_day_type").val(punch_in_request_day_type);
  $("#user_id").val(user_id);
  $("#csrf").val(user_id);
  $("#value").val(value);
  $("#id").val(id);
  $("#status").val(key);
  if(value != 2){
   
    $('.attendanceDeclineReasonClass').text('Approve');
    $(".attendanceDeclineReason").hide();
    
 /// $('.attendanceDeclineReasonClass').click(function() {
    // $.ajax({
    //   url: "controller/statusController.php",
    //   cache: false,
    //   type: "POST",
    //   data: {value: value,
    //           id : id,
    //           status:key, 
    //           punch_in_request_day_type:punch_in_request_day_type,user_id:user_id,
    //           csrf:csrf},
    //   success: function(response){
    //       $(".ajax-loader").hide();
         
    //       if(response==1) {
    //    document.location.reload(true);
    //       swal("Status Changed", {
    //                   icon: "success",
    //               });
    //       } else {
    //        document.location.reload(true);
    //         swal("Something Wrong!", {
    //                     icon: "error",
    //                 });
    //         }
    //   }
    //   });
     /// });
  }
  else
  {
  $('.attendanceDeclineReasonClass').text('Decline');
  $(".attendanceDeclineReason").show();
  /// $('.attendanceDeclineReasonClass').click(function() {
   
      /* var attendance_declined_reason =  $('#attendance_declined_reason').val();
     
      var csrf =$('input[name="csrf"]').val();
      if(attendance_declined_reason.trim() != ''){
          $(".ajax-loader").show();
          $.ajax({
            url: "controller/statusController.php",
            cache: false,
            type: "POST",
            data: {value: value,
                    id : id,
                    status:key, 
                    attendance_declined_reason:attendance_declined_reason,user_id,
                    csrf:csrf},
            success: function(response){
                $(".ajax-loader").hide();
                
                if(response==1) {
              document.location.reload(true);
                swal("Status Changed", {
                            icon: "success",
                        });
                } else {
                  document.location.reload(true);
                  swal("Something Wrong!", {
                              icon: "error",
                          });
                  }
            }
            });
          }
          else
          {
            swal("Please Enter Decline Reason !", {
              icon: "error",
            });
          } */
      
   /// }); 
  }

}

$('.declinePunchMissCLass').click(function() {
  value =  $('#value').val();
  id = $('#id').val();
  key = $('#status').val();
  csrf = $('#csrf').val();
  punch_in_request_day_type=  $("#punch_in_request_day_type").val();
  user_id= $("#user_id").val();
  var csrf =$('input[name="csrf"]').val();
   var attendance_declined_reason =  $('#attendance_declined_reason').val();
 var error = 0;
  var csrf =$('input[name="csrf"]').val();
  if(value==2){
    if(attendance_declined_reason.trim() != ''){
        $(".ajax-loader").show();
          error  = 0;
        }
        else
        {
          error = 1;
        }
  }
  if (error == 0) {
        $.ajax({
          url: "controller/statusController.php",
          cache: false,
          type: "POST",
          data: {value: value,
                  id : id,
                  status: key, 
                  punch_in_request_day_type:punch_in_request_day_type,
                  attendance_declined_reason:attendance_declined_reason,user_id:user_id,
                  csrf:csrf},
          success: function(response){
              $(".ajax-loader").hide();
              
              if(response==1) {
             document.location.reload(true);
              swal("Status Changed", {
                          icon: "success",
                      });
              } else {
                document.location.reload(true);
                swal("Something Wrong!", {
                            icon: "error",
                        });
                }
          }
          });
      }
});

function showDateDetailsModal(month,user_id,key){
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getAttendanceSummary",
        key:key,
        month:month,
        user_id:user_id,
      },
    success: function(response){
      data = "";
      if(response.status !='201'){
        $('#attnSmryDateDataMdl').modal();
        $.each(response.attendance_summary, function( index, value ) {
          var indexTemp = index+1;
          data +=`<tr> <td>`+indexTemp+`</td><td>`+value+`</td></tr>`;
        });
      }
      $('#attnSmryDateData').html(data)
    }
  });
  
}
function getSiteManagerFloorByBLock()
{
  $(".ajax-loader").show();
  $('#user_id').html('<option value="">-- Select Employee --</option> ');
  $('#uId').html('<option value="">-- Select Employee --</option> ');
  var floor_id_old = $('#floor_id_old').val();
  var block_id = $('#br_id').val();
  
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getFloorByBlockIdForHr",
          block_id:block_id,
        },
      success: function(response){
        $(".ajax-loader").hide();

        optionContent = `<option value=""> Select Department </option>`;
        $.each(response.floor, function (index, value) {
          pr_floor_id = $('#floor_id').val();
          
          if(floor_id_old==value.floor_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                }
                optionContent += `<option `+selected+` value="`+value.floor_id+`" >`+value.floor_name+`(`+value.block_name+`)</option>`;
        });
        $('#floor_id').html(optionContent);
        $('#floor_id').val(pr_floor_id);
        $('#floor_id').select2();
        $('#dId').html(optionContent);
      }
  });
}
function getSiteManagerFloorByBLockNew()
{
  $(".ajax-loader").show();
  
  var floor_id_old = $('#floor_id_old').val();
  var block_id = $('#br_id').val();
  
  var optionContent ="";
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getFloorByBlockIdForHr",
          block_id:block_id,
        },
      success: function(response){
        $(".ajax-loader").hide();
        optionContent = `<option value=""> Select Department </option>`;
        $.each(response.floor, function (index, value) {
          pr_floor_id = $('#floor_id').val();
          
          if(floor_id_old==value.floor_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                }
                optionContent += `<option `+selected+` value="`+value.floor_id+`" >`+value.floor_name+`(`+value.block_name+`)</option>`;
        });
        $('#floor_id').html(optionContent);
        $('#floor_id').val(pr_floor_id);
        $('#floor_id').select2();
        $('#dId').html(optionContent);
      }
  });
}
function getSiteManagerByFloorBLock(floor_id)
{
  var user_id_old = $('#user_id_old').val();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getUserByFloorForGeo",
                floor_id:floor_id,
              },
            success: function(response){
              var userHtml ="";
            
              userHtml=`<option value=''> Select Employee </option>`;
              $.each(response.users, function( index, value ) {
                if(user_id_old==value.user_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                }
                userHtml +=`<option `+selected+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });

              $('#user_id').html(userHtml);
             
            }
    })
}
function getCateByVendorId(vendor_id)
{
  cat_id = $('#cat_id').val();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getCateogryByServiceProvider",
                vendor_id:vendor_id,
              },
            success: function(response){
              var userHtml ="";
              
              userHtml=`<option> Select Cateogry </option>`;
              $.each(response.category, function( index, value ) {
                if(cat_id==value.product_category_id)
                {
                  var selected = "selected";
                }
                else
                {
                  var selected = "";

                }
                userHtml +=`<option `+selected+` value="`+value.product_category_id+`">`+value.category_name+`</option>`;
              });

             $('.product_category_id').html(userHtml);
             $('#product_category_id').html(userHtml);
            }
    })
}
function geDeptByAssignBlock(id)
{
  $(".ajax-loader").show();
 
  var optionContent = "";
 
  floor_id = $('#floor_id').val();
  user_id = $('#user_id').val();
  
    $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        data: {
            action:"getFloorByBlockIdForAssignMultiDptBranch",
            block_id:id,
            user_id:user_id,
          },
        success: function(response){
          $(".ajax-loader").hide();
          if(access_branchs_id=='') {
            optionContent = `<option value="0"> All Department </option>`;
          }
          $.each(response.floor, function (index, value) {
            if (value.floor_id != floor_id) {
              optionContent += `<option value="`+value.floor_id+`" >`+value.floor_name+`</option>`;
            }
          });
          $('#emp_multi_floor').html(optionContent);
         
        }
    });
  
}
function changeUserForAssign(user_id){
  $('#emp_multi_branch').val('');
  $('#emp_multi_branch').select2();
  $('#emp_multi_floor').html('<option>--Select Department--</option>');
  br_id = $('#br_id').val();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getBlockByUserForAssignMultiDptBranch",
        user_id:user_id,
        br_id:br_id,
      },
    success: function(response){
      $(".ajax-loader").hide();
      optionContent = `<option value="0"> Select Branch </option>`;
      $.each(response.floor, function (index, value) {
          optionContent += `<option value="`+value.block_id+`" >`+value.block_name+`</option>`;
      });
      $('#emp_multi_branch').html(optionContent);
     
    }
});
}
$('.empAccessForBranchMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all Department, please removed selected branch!");
      $('#emp_multi_floor').val(0).trigger('change');
      //$('#emp_multi_floor').select2();
     // $('option:contains(All Branch)').prop("selected", false);
    }else  {

    }
  }
});

function getDateValidation(user_id) {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getJoiningDateByUserId",
        user_id:user_id,
        
      },
    success: function(response){
      $(".ajax-loader").hide();
      if (response.status == 200) {
        
        loadDatepicker(response.data.joining_date);
      }
     
    }
});
}
function loadDatepicker(sDate) {
  $('#default-datepicker_salary_start').datepicker('destroy').datepicker({
    format: 'yyyy-mm-dd'
    , autoclose: true
    , startDate: sDate
    
});
}
function getSubCategoryByIdInAddProduct(product_category_id, product_sub_category_id='')
{
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getSubCategory",
                product_category_id:product_category_id,
              },
            success: function(response){
              var main_content ="";
              var pr_content ="";
              main_content=`<option value="">-- Select --</option>`;
              $.each(response.sub_category, function( index, value ) {
                var selected = "";
                if(product_sub_category_id==value.product_sub_category_id)
                {
                  var selected = "selected";
                }
                main_content +=`<option `+selected+` value="`+value.product_sub_category_id+`">`+value.sub_category_name+`</option>`;
              });
              pr_content +=`<option  value="">-- Select Product---</option>`;
              $.each(response.products, function( index, val ) {
                var selected = "";
                /* if(product_sub_category_id==value.product_sub_category_id)
                {
                  var selected = "selected";
                } */
               
                pr_content +=`<option  value="`+val.product_id+`">`+val.product_name+`</option>`;
              });
              
             $('#product_sub_category_id').html(main_content);
             $('#product_id').html(pr_content);
            }
    })
}

function lessonDelete(lesson_id) {
  swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })
  
  .then((willDelete) => {
      if (willDelete) {
          $.ajax({
            url: "controller/deleteController.php",
            cache: false,
            type: "POST",
            data: {id : lesson_id,deleteValue
              :"lesson_delete"},
            success: function(response){
              // 
              if(response==1) {
                document.location.reload(true);
                // history.go(0);
              } else {
                document.location.reload(true);
            //    /// history.go(0);
              }
            }
          });
      }else{
          swal("Your data is safe!");
      }
  });
}
function deletCourseChapter(chapter_id) {
  swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })
  
  .then((willDelete) => {
      if (willDelete) {
          $.ajax({
            url: "controller/deleteController.php",
            cache: false,
            type: "POST",
            data: {id : chapter_id,deleteValue
              :"course_chapter_delete"},
            success: function(response){
              // 
              if(response==1) {
                document.location.reload(true);
                // history.go(0);
              } else {
                document.location.reload(true);
                /// history.go(0);
              }
            }
          });
      }else{
          swal("Your data is safe!");
      }
  });
}

function deleteBankAccount(id,user_id)
{ 
    swal({
      title: "Are you sure to delete this data ?",
      text: "Bank Account !!!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })

      .then((willDelete) => {
        if (willDelete) { 
          $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            async: false,
            data: {
              action: "deleteBankAccuount",
              user_id: user_id,
              bank_id: id,
            },
            success: function (response) {
              window.location = "employeeDetails?id="+user_id;
            }
          });
      }
    });
}
function employeeBankDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
  $('#account_no-error').remove(); 
  $('#account_no-error').remove(); 
  $('#ifsc_code-error').remove(); 
  $('#bank_name-error').remove(); 
  $('#user_id-error').remove(); 
  $('#floor_id-error').remove(); 
  //$(this).prev("label").toggleClass("error", (this.value==""))
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getbankById",
                bank_id:id,
              },
            success: function(response){
              $('#addBankModal').modal();
              
              //getVisitorFloorByBlockId(response.bank.block_id);
               $('#bank_id').val(response.bank.bank_id);
                $('#bank_name').val(response.bank.bank_name);
                $('#bank_name').removeClass('error');
                $('#account_no').val(response.bank.account_no);
                $('#bank_branch_name').val(response.bank.bank_branch_name);
                $('#account_type').val(response.bank.account_type);
                $('#account_no').removeClass('error');
                $('#ifsc_code').val(response.bank.ifsc_code);
                $('#ifsc_code').removeClass('error');
                $('#crn_no').val(response.bank.crn_no);
                $('#esic_no').val(response.bank.esic_no);
                $('#pan_card_no').val(response.bank.pan_card_no);
                $('.floor_id').val(response.bank.floor_id);
                $('#block_id').val(response.bank.block_id);
              $('#user_id').val(response.bank.user_id); 
              $('#user_id_old').val(response.bank.user_id); 
              $('#floor_id_old').val(response.bank.floor_id); 
             
              getUserForBank(response.bank.floor_id);
            }
    })
}

function editProductVariant(product_variant_id) {
  
  $('.btnUpAdd').text('Update');
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getProductVariantById",
        product_variant_id:product_variant_id,
      },
    success: function(response){
     
      if (response.status == "200") {
        $('#product_variant_name').val(response.product_variant.product_variant_name);
        $('#product_variant_id').val(response.product_variant.product_variant_id);
        $('#addVerientModal').modal();
      
      }
    }
});
}


function getProductByCategoryAndSubCategoryId(sId) {
 
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getProductByCategoryAndSubCategoryId",
        product_sub_category_id:sId,
      },
    success: function (response) {
    pr_content +=`<option  value="">-- Select Product---</option>`;
    $.each(response.products, function( index, val ) {
      var selected = "";
      /* if(product_sub_category_id==value.product_sub_category_id)
      {
        var selected = "selected";
      } */
      
      pr_content +=`<option  value="`+val.product_id+`">`+val.product_name+`</option>`;
    });
      $('#product_id').html(pr_content);

    }
});
}

function getProductVariant(pId) {
  pr_content = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getProductVariant",
        product_id:pId,
      },
    success: function (response) {
    pr_content +=`<option  value="">-- Select variant---</option>`;
    $.each(response.data, function( index, val ) {
      var selected = "";
      /* if(product_sub_category_id==value.product_sub_category_id)
      {
        var selected = "selected";
      } */
      
      pr_content +=`<option  value="`+val.product_variant_id+`">`+val.product_variant_name+`</option>`;
    });
      $('#product_variant_id').html(pr_content);

    }
});
}


function getProductPrice(product_price_id) {

  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getProductPriceData",
        product_price_id:product_price_id,
        
      },
    success: function(response){
      if (response.status == "200") {
       
        $('#productPriceDetailModal').modal();
        data = '<div class="row mx-0">'+
                '<div class="col-md-6 form-group ">'+
                  '<label for="exampleInputEmail1">Product Name </label>'+
                  '<span  >'+response.data.product_name+'</span>'+
                '</div>'+
                '<div class="col-md-6 form-group ">'+
                  '<label for="exampleInputEmail1">Category </label>'+
                  '<span  >'+response.data.category_name+'</span>'+
                '</div>'+
                '<div class="col-md-6 form-grouuser_full_namep ">'+
                  '<label for="exampleInputEmail1">Sub category: </label>'+
                  '<span  >'+response.data.sub_category_name+'</span>'+
                '</div>'+
                '<div class="col-md-6 form-group ">'+
                  '<label for="exampleInputEmail1">Variant : </label>'+
                  '<span  >'+response.data.product_variant_name+'</span>'+
                '</div>'+
                '<div class="col-md-6 form-group ">'+
                  '<label for="exampleInputEmail1">Product Price : </label>'+
                  '<span  >'+response.data.product_price+'</span>'+
                '</div>'+
                '<div class="col-md-6 form-group ">'+
                  '<label for="exampleInputEmail1">Maximum Order Quantity : </label>'+
                  '<span  >'+response.data.minimum_order_quantity+'</span>'+
                '</div>'+
                '</div>'
                ;
        $('#product_price_detail').html(data);
      }
    }
  });

}
/* function addCommentOnPost(feed_id,society_id,bms_admin_id) {
  msg = $('textarea#comment_'+feed_id).val();
  if (msg != "") {
    $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"addCommentOnPost",
          feed_id:feed_id,
          bms_admin_id:bms_admin_id,
          society_id:society_id,
          msg:msg,
        },
      success: function(response){
        if (response.status == "200") {
          location.reload();
        }
      }
    });
  } else {
    swal("Please Enter Comment");
  }
  
}
function addSubcomment(comment_id,feed_id, society_id, bms_admin_id) {
  msg = $('textarea#sub_comment_'+comment_id).val();
  if (msg != "") {
    $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"addSubComment",
          feed_id:feed_id,
          comment_id:comment_id,
          bms_admin_id:bms_admin_id,
          society_id:society_id,
          msg:msg,
        },
      success: function(response){
        if (response.status == "200") {
          location.reload();
        }
      }
    });
  } else {
    swal("Please Enter Comment");
  }
  
} */

function addCatvendorModal(cId) {
  $.ajax({
    url: "addVendorModalFromCateogry.php",
    cache: false,
    type: "POST",
    data: {
      cId: cId,
    },
    success: function (response) {
      
      $('#addCatvendor').html(response);
    }
  })

}
function addSubCatvendorModal(scId) {
  $.ajax({
    url: "addVendorModalFromSubCateogry.php",
    cache: false,
    type: "POST",
    data: {
      scId: scId,
    },
    success: function (response) {
      $('#addVendorSubCatModal').modal('show');
      $('.addSubCatvendor').html(response);
    }
  })

}

function editAdvaceSalary(advance_salary_id) {
    $.ajax({
    url: "../residentApiNew/commonController.php",
      cache: false,
    async:false,
    type: "POST",
    data: {
      advance_salary_id: advance_salary_id,action:"advanceSalaryById"
    },
    success: function (response) {
     
      if (response.status = 200) {
        $('.hideAdd').text('Update');
        getFloorByBlockIdAddSalary(response.data.block_id);
        getUserByFloorId(response.data.floor_id);
        $('#block_id').val(response.data.block_id);
        $('#block_id').select2();
        $('#floor_id').val(response.data.floor_id);
        $('#floor_id').select2();
        $('#advance_salary_id').val(response.data.advance_salary_id);
        $('#advance_salary_amount').val(response.data.advance_salary_amount);
        $('#advance_salary_date').val(response.data.advance_salary_date);
        $('#advance_salary_mode').val(response.data.advance_salary_mode);
        $('textarea#advance_salary_remark').val(response.data.advance_salary_remark);
        $('#advance_salary_mode').select2();
        $('#user_id').val(response.data.user_id);
        $('#user_id').select2();
        $('#addAdvanceSalaryModal').modal('show');
      }
    }
  })
}

function getAdvaceSalaryDetail(advance_salary_id) {
 // alert(advance_salary_id);
  $.ajax({
    url: "../residentApiNew/commonController.php",
      cache: false,
   
    type: "POST",
    data: {
      advance_salary_id: advance_salary_id,action:"advanceSalaryById"
    },
    success: function (response) {
      showData = "";
      if (response.status = 200) {
        $('#advanceSalaryModal').modal('show');
        if(response.data.advance_salary_mode==0)
            {
              response.data.advance_salary_mode = "Bank Transaction";
            }
            else if(response.data.advance_salary_mode == 1)
            {
              response.data.advance_salary_mode = "Cash";

            }
            else
            {
              response.data.advance_salary_mode = "Cheque";

            }
            showData += `<div class="col-md-6 form-group ">
                        <label for="exampleInputEmail1">Employee : </label>
                        <span>`+response.data.user_full_name+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                      <label for="exampleInputEmail1">Department : </label>
                      <span>`+response.data.floor_name+`</span>
                    </div>
                      <div class="col-md-6 form-group ">
                      <label for="exampleInputEmail1">Amount : </label>
                      <span>`+response.data.advance_salary_amount+`</span>
                    </div>
                      <div class="col-md-6 form-group ">
                      <label for="exampleInputEmail1">Given Date : </label>
                      <span>`+response.data.cr_date+`</span>
                    </div>
                      <div class="col-md-6 form-group ">
                      <label for="exampleInputEmail1">Remark : </label>
                      <span>`+response.data.advance_salary_remark+`</span>
                    </div>
                      <div class="col-md-6 form-group ">
                      <label for="exampleInputEmail1">Mode : </label>
                      <span>`+response.data.advance_salary_mode+`</span>
                    </div>
                      `;
                    if (response.data.is_paid == 1) {
                       showData += `<div class="col-md-12 form-group ">
                        <label for="exampleInputEmail1">Return Date : </label>
                        <span>`+response.data.paid_date+`</span>
                      </div>
                      <div class="col-md-12 form-group ">
                        <label for="exampleInputEmail1">Received By : </label>
                        <span>`+response.data.paid_by_admin_name+`</span>
                      </div>
                      <div class="col-md-12 form-group ">
                        <label for="exampleInputEmail1">Receive Remark : </label>
                        <span>`+response.data.advance_salary_paid_remark+`</span>
                      </div>`;

                    }
        $('#advanceSalaryData').html(showData);
      }
    }
  })
}

function getPromotionDetail(history_id) {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      history_id: history_id,action:"employee_history"
    },
    success: function (response) {
      showData = "";
      if (response.status = 200) {
        $('#PromotionModal').modal('show');
        if(response.data.salary_incrment==0)
            {
              response.data.salary_incrment = "Yes";
            }
            else
            {
              response.data.salary_incrment = "No";
            }
        showData += `<div class="row">
                      <div class="col-md-6 form-group ">
                        <label for="exampleInputEmail1">Employee </label>
                        <span>`+response.data.user_full_name+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                        <label for="exampleInputEmail1">Department </label>
                        <span>`+response.data.floor_name+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                        <label for="exampleInputEmail1">Salary Increment </label>
                        <span>`+response.data.salary_incrment+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                        <label for="exampleInputEmail1">Joining Date </label>
                        <span>`+response.data.joining_date+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                        <label for="exampleInputEmail1">End Date </label>
                        <span>`+response.data.end_date+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                        <label for="exampleInputEmail1">Remark </label>
                        <span>`+response.data.remark+`</span>
                      </div>
                    </div>
                     `;
        $('.PromotionModalData').html(showData);
      }
    }
  })
}
function UserBankDetail(id) {
  showData = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getbankById",
        bank_id:id,
      },
    success: function (response) {
      if (response.status == 200) {
        $('#BankDetailsModal').modal();
        showData += `<div class="row">
                        <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Employee </label>
                          <span>`+response.bank.user_full_name+`</span>
                        </div>
                        <div class="col-md-6 form-group ">
                        <label for="exampleInputEmail1">Department </label>
                        <span>`+response.bank.floor_name+`(`+response.bank.block_name+`)</span>
                        </div>
                        <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Account </label>
                          <span>`+response.bank.account_type+`</span>
                        </div>
                        <div class="col-md-6 form-group ">
                            <label for="exampleInputEmail1">Pan Card No </label>
                            <span>`+response.bank.pan_card_no+`</span>
                        </div>
                        <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Account Number </label>
                          <span>`+response.bank.account_no+`</span>
                        </div>
                        <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Bank Name</label>
                          <span>`+response.bank.bank_name+`(`+response.bank.bank_branch_name+`)</span>
                        </div>
                        <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Ifsc Code </label>
                          <span>`+response.bank.ifsc_code+`</span>
                        </div>
                        <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Customer Id/CRN No. </label>
                          <span>`+response.bank.crn_no+`</span>
                        </div>
                        <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Esic No </label>
                          <span>`+response.bank.esic_no+`</span>
                        </div>
                      </div>`;
                      $('#bankDetails').html(showData);
      }
    }
  })
}




  $('.HrFloor').change(function() {  

    $('#user_id').html('<option value="">-- Select Employee --</option> ');
  var item=$(this);
    var abc = item.val();
   
    if (abc != null) {
      
      if (abc.length > 0) {
        if (abc.length > 1) {
          if (jQuery.inArray("all_branch", abc) !== -1) {
            swal("if you want to all Branch, please removed selected branch!");
            $('.HrFloor').val("all_branch").trigger('change');
          }
        } 
      }

      id =$('.HrFloor').val();
      
      var optionContent = "";
      if (jQuery.inArray("all_branch", id) !== -1) {
        $(".ajax-loader").hide();
        if(access_branchs_id=='') {
          optionContent = `<option value="All"> All Department </option>`;
        }
        $('#floor_id').html(optionContent);
        $('.floor_id').html(optionContent);
        optionUContent = `<option value="all_employee" > All Employee </option>`;
        $('#user_id').html(optionUContent);
       
      }
      else
      {
          $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getFloorByBlockIdForHr",
                block_id:id,
              },
            success: function(response){
              $(".ajax-loader").hide();
              if(access_branchs_id=='') {
                optionContent = ` <option value="All"> All Department </option>`;
              }
              $.each(response.floor, function( index, value ) {
                optionContent += `<option value="`+value.floor_id+`" >`+value.floor_name+`(`+value.block_name+`)</option>`;
              });
              $('#floor_id').html(optionContent);
              $('.floor_id').html(optionContent);
              $('#dId').html(optionContent);
            }
        });
        }
    }
 
  })
  /* function getHrDocUser(floor_id)
  {
   
    console.log(user_id_old);
    if(floor_id == "All"){
      $('.employee_select').addClass('d-none');
    }else{
      $('.employee_select').removeClass('d-none');
    }
    $.ajax({
              url: "../residentApiNew/commonController.php",
              cache: false,
              type: "POST",
              data: {
                  action:"getUserByFloorForHrdoc",
                  floor_id:floor_id,
                },
              success: function(response){
                var userHtml ="";
                
                userHtml=`<option value="all_employee"> All Employee </option>`;
                $.each(response.users, function( index, value ) {
                  if(user_id_old==value.user_id)
                  {
                    var selected = "selected";
                  }
                  else
                  {
                    var selected = "";
                  }
                  userHtml +=`<option `+selected+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
                });
                
               $('#user_id').html(userHtml);
              }
      })
  } */

$('.HrDocUser').change(function () {
  var item = $(this);
  var abc = item.val();
 
  if (abc != null) {
    if (abc.length > 0) {
      if (abc.length > 1) {
        if (jQuery.inArray("All", abc) !== -1) {
          swal("if you want to all Department, please removed selected Department!");
          $('.HrDocUser').val("All").trigger('change');
        }
      }
    }
    floor_id =$('#floor_id').val();
    if (jQuery.inArray("All", abc) !== -1) {
      userHtml=`<option value="all_employee"> All Employee </option>`;
      $('#user_id').html(userHtml);
    } else {
      $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        data: {
            action:"getUserByFloorForHrdoc",
            floor_id:floor_id,
          },
        success: function(response){
          var userHtml ="";
          var user_id_old = $('#user_id_old').val();
          userHtml=`<option value="all_employee"> All Employee </option>`;
          $.each(response.users, function( index, value ) {
            if(user_id_old==value.user_id)
            {
              var selected = "selected";
            }
            else
            {
              var selected = "";
            }
            userHtml +=`<option `+selected+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
          });
          
         $('#user_id').html(userHtml);
        }
      })
    }
  }

});
  
$('.employees').change(function () {
  var item = $(this);
  var abc = item.val();
 
  if (abc != null) {
    if (abc.length > 0) {
      if (abc.length > 1) {
        if (jQuery.inArray("all_employee", abc) !== -1) {
          swal("if you want to all User, please removed selected Users!");
          $('#user_id').val("all_employee").trigger('change');
        }
      }
    }
  }
})
function getFloorByBlockIdForTrack(id) {
    $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      async: false,
      type: "POST",
      data: {
          action:"getFloorByBlockId",
          block_id:id,
        },
      success: function(response){
        $(".ajax-loader").hide();
       
        optionContent = `<option value=""> Select Department </option>`;
        $.each(response.floor, function( index, value ) {
          optionContent += `<option value="`+value.floor_id+`" >`+value.floor_name+`</option>`;
        });
        $('#track_floor_id').html(optionContent);
      
      }
  });
}
function getUserForTrack(floor_id,user_id="") {
  $(".ajax-loader").show();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            async: false,
            data: {
                action:"getUserByFloorForTrack",
                floor_id:floor_id,
              },
            success: function(response){
              $(".ajax-loader").hide();
              var userHtml ="";
              userHtml=`<option value=''> Select Employee </option>`;
              
              $.each(response.users, function (index, value) {
                if (user_id != "" && user_id == value.user_id) {
                  sel = "selected";
                } else
                {
                  sel = "";

                }
                userHtml +=`<option `+sel+` value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });
              
             $('#track_user_id').html(userHtml);
             
            }
    })
}
function editTrackTime(user_id) {
  $('.btnSpan').text("Update");
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    
    data: {
      action:"getUserById",
      user_id:user_id,
    },
    success: function(response){
      $('#EditTrackUser').modal();
      if (response.status == 200) {
        $('.track_user_time').val(response.user.track_user_time);
        $('.trackUserId').val(response.user.user_id);
      }
    }
})
}
function trackedUser(user_id) {
  $('#EditTrackUser').modal();
  $('.btnSpan').text("Add");
  $('.trackUserId').val(user_id);
  $('#editTrackerUser').val('addSingleTrackUser');
  $('#editTrackerUser').attr('name','addSingleTrackUser');
}

function untrackedUser(user_id,track_user_location) {
  if (track_user_location==0) {
    var msg= "Utrack Employee Location ?";
  } else {
    var msg= "Track Employee Location ?";
  }
  swal({
    title: "Are you sure?",
    text: msg,
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })

    .then((willDelete) => {
      if (willDelete) { 
        $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          async: false,
          data: {
            action: "untrackeUser",
            user_id: user_id,
            track_user_location: track_user_location,
          },
          success: function (response) {
            swal("Changed Successfully", {
              icon: "success",
            });
            location.reload();
          }
        });
    }
  });
}
function userTrackData(user_track_id) {
  showData = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "GetUserTrackDataById",
      user_track_id: user_track_id,
    },
    success: function (response) {
      $('#userTrackDataModal').modal();
       showData += `<div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Employee :</label>
                          <span>`+response.user.user_full_name+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Department :</label>
                          <span>`+response.user.floor_name+`(`+response.user.block_name+`)</span>
                      </div>
                     
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Locality :</label>
                          <span>`+response.user.locality+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Area :</label>
                          <span>`+response.user.area+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Area :</label>
                          <span>`+response.user.remark+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Address :</label>
                          <span>`+response.user.address+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Privious Entry Distance :</label>
                          <span>`+response.user.distance_from_previous_entry+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                      <label for="exampleInputEmail1">Track Date :</label>
                      <span>`+response.user.user_track_date+`</span>
                  </div>
                      `;
      $('#userTrackDetail').html(showData);
      trackUser(response.user.user_lat, response.user.user_long);
    }
  });
}

function userBackTrackDataLastData(user_id) {
  ///
  showData = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "GetUserbackTrackDataByIdLastData",
      user_id: user_id,
    },
    success: function (response) {
      $('#userTrackDataModal').modal();
       showData += `<div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Employee :</label>
                          <span>`+response.user.user_full_name+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Department :</label>
                          <span>`+response.user.floor_name+`(`+response.user.block_name+`)</span>
                      </div>
                     
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Locality :</label>
                          <span>`+response.user.last_tracking_locality+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Area :</label>
                          <span>`+response.user.last_tracking_area+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Address :</label>
                          <span>`+response.user.last_tracking_address+`</span>
                      </div>
                    
                      <div class="col-md-6 form-group ">
                      <label for="exampleInputEmail1">Track Date :</label>
                      <span>`+response.user.last_tracking_location_time+`</span>
                  </div>
                      `;
      $('#userTrackDetail').html(showData);
      trackUserDetails(response.user.last_tracking_latitude, response.user.last_tracking_longitude)
    }
  });
}

function userBackTrackData(user_back_track_id) {
  ///
  showData = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "GetUserbackTrackDataById",
      user_back_track_id: user_back_track_id,
    },
    success: function (response) {
      $('#userTrackDataModal').modal();
       showData += `<div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Employee :</label>
                          <span>`+response.user.user_full_name+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Department :</label>
                          <span>`+response.user.floor_name+`(`+response.user.block_name+`)</span>
                      </div>
                     
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Locality :</label>
                          <span>`+response.user.back_locality+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Area :</label>
                          <span>`+response.user.back_area+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Address :</label>
                          <span>`+response.user.back_address+`</span>
                      </div>
                    
                      <div class="col-md-6 form-group ">
                      <label for="exampleInputEmail1">Track Date :</label>
                      <span>`+response.user.user_back_track_date+`</span>
                  </div>
                      `;
      $('#userTrackDetail').html(showData);
      trackUserDetails(response.user.back_user_lat, response.user.back_user_long)
    }
  });
}


function userGPSData(user_gps_on_off_id) {
  ///
  showData = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "getGpsDataUser",
      user_gps_on_off_id: user_gps_on_off_id,
    },
    success: function (response) {
      $('#userTrackDataModal').modal();
       showData += `<div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Status :</label>
                          <span>`+response.user.gps_status+`</span>
                      </div>
                    
                      <div class="col-md-6 form-group ">
                      <label for="exampleInputEmail1">Date :</label>
                      <span>`+response.user.created_date+`</span>
                  </div>
                      `;
      $('#userTrackDetail').html(showData);
      trackUserDetails(response.user.latitude, response.user.longitude)
    }
  });
}

floor_idss = [];
function getUserForSiteManager()
{
  site_mn_ids = "";
  floor_id = $('#floor_id').val();
  site_mn_ids = $('#site_mn_ids').val();
  procurement_ids = $('#procurement_ids').val();
  finance_manager_ids = $('#finance_manager_ids').val();
  
  $(".ajax-loader").show();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            async: false,
            data: {
                action:"getUserForSiteManager",
                floor_id:floor_id,
                site_mn_ids:site_mn_ids,
                finance_manager_ids:finance_manager_ids,
                procurement_ids:procurement_ids,
              },
    success: function (response) {
     
              $(".ajax-loader").hide();
              pr_user_id = $('#user_id').val();
              finance_manager_id = $('#finance_manager_id').val();
              procurement_id = $('#procurement_id').val();
              var SiteuserHtml = "";
              SiteuserHtml=`<option value=''> Select Employee </option>`;
            $.each(response.users, function (index, value) {
                
                if (jQuery.inArray(value.user_id, response.site_mn_idss) !== -1) {
                  sl = "selected";
                } else {
                  sl = "";
                }
                SiteuserHtml +=`<option `+sl+` data-date="`+value.joining_date+`" value="`+value.user_id+`">`+value.user_full_name+`</option>`;
            });
            $('#user_id').html(SiteuserHtml);
              procuremenuserHtml = "";
             procuremenuserHtml=`<option value=''> Select Employee </option>`;
              $.each(response.users, function (index, value) {
                 
                  if (jQuery.inArray(value.user_id, response.procurement_ids) !== -1) {
                    sl2 = "selected";
                  } else {
                    sl2 = "";
                  }
                  procuremenuserHtml +=`<option `+sl2+` data-date="`+value.joining_date+`" value="`+value.user_id+`">`+value.user_full_name+`</option>`;
                });
              $('#procurement_id').html(procuremenuserHtml);
              financeManagerHtml = "";
              financeManagerHtml=`<option value=''> Select Employee </option>`;
              $.each(response.users, function (index, value) {
                 
                  if (jQuery.inArray(value.user_id, response.finance_manager_ids) !== -1) {
                    sl3 = "selected";
                  } else {
                    sl3 = "";
                  }
                  financeManagerHtml +=`<option `+sl3+` data-date="`+value.joining_date+`" value="`+value.user_id+`">`+value.user_full_name+`</option>`;
                });
               $('#finance_manager_id').html(financeManagerHtml);
               
            }
    })
}

$('.muliDptHoliday').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all Department, please removed selected Department!");
      $('#flr_id').val(0).trigger('change');
      //$('#emp_multi_floor').select2();
     // $('option:contains(All Branch)').prop("selected", false);
    }else  {

    }
  }
});


function paidAdvanceSalary(advance_salary_id) {
  swal({
    title: "Are you sure?",
    text: " Mark As Paid",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  }) .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          async: false,
          data: {
            action: "paidAdvanceSalary",
            advance_salary_id: advance_salary_id,
          },
          success: function (response) {
            swal("Status Changed", {
              icon: "success",
            });
            document.location.reload(true);
          }
        });
      }
    });
}


function getEmployeeLoanDetail(loan_id) {
  showData = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "getLoanById",
      loan_id: loan_id,
    },
    success: function (response) {
     
      
      showData += `<div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Employee :</label>
                          <span>`+response.data.user_full_name+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Department :</label>
                          <span>`+response.data.floor_name+`(`+response.data.block_name+`)</span>
                      </div>
                     
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Loan Amount :</label>
                          <span>`+response.data.loan_amount+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Date :</label>
                          <span>`+response.data.loan_date+`</span>
                      </div>
                      <div class="col-md-6 form-group ">
                          <label for="exampleInputEmail1">Emi's :</label>
                          <span>`+response.data.loan_emi+`</span>
                      </div>`;
      var i = 1;
      if (response.data.loan_emis.length > 0) {
        emisData = "";
        $.each(response.data.loan_emis, function (index, value) {
          if (value.is_paid=="0") {
            is_paid = "Unpaid";
          } else
          {
            is_paid = "Paid";
          }
          if (value.emi_paid_date!="0000-00-00") {
            emi_paid_date = value.emi_paid_date;
          } else
          {
            emi_paid_date = "";
          }
          emisData +=` <tr>
                        <td>`+ i++ + `</td>
                        <td>`+ value.emi_amount + `</td>
                        <td><span class="badge badge-info">`+ is_paid + `</span></td>
                        <td>`+ emi_paid_date + `</td>
                      <tr>`;
          });     
          $('#showLoanEmi').html(emisData);
      }
      $('#showLoanData').html(showData);
     
      $('#showLoanModal').modal();
    }
  });
}

function editFaceAppDevice(face_app_device_id) {
      $('#block_id').val('')
      $('#device_location').val('')
      $('#face_app_device_id').val('')
      $('#block_id').select2();
      $('#floor_id').val('')
      $('#floor_id').select2();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "getFaceAppDeviceById",
      face_app_device_id: face_app_device_id,
    },
    success: function (response) {
      $('#editFaceAppDeviceModal').modal();
      block_ids = response.data.block_ids;
      blkIds = block_ids.split(',');
      floor_ids = response.data.floor_ids;
      flrds = floor_ids.split(',');
      getFLoorByMultiBLockIds(blkIds,floor_ids);
      $('#block_id').val(blkIds)
      $('#device_location').val(response.data.device_location)
      $('#face_app_device_id').val(face_app_device_id)
      $('#block_id').select2();
      $('#floor_id').val(flrds)
      $('#floor_id').select2();
    }
  });
}

function getFLoorByMultiBLockIds(blkIds = "", floor_ids = "") {
  if (blkIds != "") {
    block_id = blkIds;
  } else {
    block_id = $('#block_id').val();
  }
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async:false,
    data: {
        action:"getFloorByBlockIdForHr",
        block_id:block_id,
      },
    success: function (response) {
      sl = "";
      $(".ajax-loader").hide();
      if (floor_ids != "") {
        flrds = floor_ids.split(',');
      } else
      {
        flrds = $('#floor_id').val();
      }
      optionContent = ` <option > Select Floor </option>`;
      $.each(response.floor, function (index, value) {
        if (jQuery.inArray(value.floor_id, flrds) !== -1) {
          sl = "selected";
        } else {
          sl = "";
        }
        optionContent += `<option `+sl+` value="`+value.floor_id+`" >`+value.floor_name+`(`+value.block_name+`)</option>`;
      });
      $('#floor_id').html(optionContent);
      $('.floor_id').html(optionContent);
      $('#dId').html(optionContent);
    }
});
}

function showFaceAppDevice(face_app_device_id) {
  set_data = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "getFaceAppDeviceById",
      face_app_device_id: face_app_device_id,
    },
    success: function (response) {
      $('#DetailFaceAppDeviceModal').modal();
      set_data = `<div class="col-md-6 form-group ">
                    <label for="exampleInputEmail1">Device Model :</label>
                    <span>`+response.data.device_model+`</span>
                </div>
                <div class="col-md-6 form-group ">
                    <label for="exampleInputEmail1">Location :</label>
                    <span>`+response.data.device_location+`</span>
                </div>
                <div class="col-md-6 form-group ">
                    <label for="exampleInputEmail1">App Version :</label>
                    <span>`+response.data.app_version+`</span>
                </div>
                <div class="col-md-6 form-group ">
                    <label for="exampleInputEmail1">Device Mac :</label>
                    <span>`+response.data.device_mac+`</span>
                </div>
                <div class="col-md-6 form-group ">
                    <label for="exampleInputEmail1">Last Sync Date :</label>
                    <span>`+response.data.last_syc_date+`</span>
                </div>
                `;
        if (response.data.admin_name != "") {
          set_data += `<div class="col-md-6 form-group ">
                    <label for="exampleInputEmail1">Updated By :</label>
                    <span>`+ response.data.admin_name + `</span>
                </div>`;
      }
      $('.showDetail').html(set_data);
      getFaceAppDevice(response.data.device_latitude, response.data.device_longitude)
    }
  });
}

function deleteFaceAppDevice(id, status) {
  swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this data!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })

    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          url: "controller/statusController.php",
          cache: false,
          type: "POST",
          data: {
            id: id, status
              : status, csrf: csrf
          },
          success: function (response) {
            $(".ajax-loader").hide();
            if (response == 1) {
              // document.location.reload(true);
              swal("Status Changed", {
                icon: "success",
              });
              document.location.reload(true);
            } else {
              document.location.reload(true);
              swal("Something Wrong!", {
                icon: "error",
              });

            }
          }
        });
      }
    });
}

function logoutConfirm() {
  swal({
    title: "Are you sure?",
    text: " App Will logout",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })

    .then((willDelete) => {
      if (willDelete) { 
        $('#logoutFrm').submit();
      }
    });
}
function payEmi(loan_emi_id,user_id,loan_id) {
    $('#markAsPaid').modal();
    $('#loan_emi_id').val(loan_emi_id);
    $('#user_id').val(user_id);
    $('#loan_id').val(loan_id);
}

function editEmployeeLoan(loan_id) {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "getLoanById",
      loan_id: loan_id,
    },
    success: function (response) {
     
      
    }
  });
}
function removeStampAndSign(settingId) {
  swal({
    title: "Are you sure?",
    text: " Remove Salary Stamp Signature",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
     
      $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        async: false,
        data: {
          action: "removeSalaryStamp",
          salary_setting_id: settingId,
        },
        success: function (response) {
          
          swal("Removed Successfully", {
                    icon: "success",
          });
          document.location.reload(true);
        }
      });
    }
  });
}
function getPendingDues(user_id) {
  dueData = "";
    $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      async: false,
      data: {
        action: "getPendingDues",
        user_id: user_id,
      },
      success: function (response) {
        
        if (response.getLoan.penalty_count > 0) {
          dueData += `<tr>
                      <td>Penalty</td>
                      <td>`+response.getLoan.penalty_amount+`</td>
                    </tr>`;
        }
        if (response.getLoan.emi_count > 0) {
          dueData += `<tr>
                      <td>Emi Due Amount</td>
                      <td>`+response.getLoan.emi_amount+`</td>
                    </tr>`;
        }
        if (response.getLoan.advance_salary_count > 0) {
          dueData += `<tr>
                      <td>Advance Salary Due Amount</td>
                      <td>`+response.getLoan.advance_salary_amount+`</td>
                    </tr>`;
        }
        if ((response.getLoan.penalty_count > 0) || (response.getLoan.emi_count > 0) || (response.getLoan.advance_salary_count > 0)) {
          $('.pendiAndDues').show();
          $('#pendiAndDuesData').html(dueData);
        } else
        {
          $('.pendiAndDues').html('<img width="250" src="img/no_data_found.png">');
        }
      }
    });
   
}

function getEducationAndAchivements(user_id) {
  data = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "getAachievementsEducation",
      user_id: user_id,
    },
    success: function (response) {
      i = 1;
      /*  <form title="Delete" action="controller/userController.php" method="post">
                    <input type="hidden" name="deleteEducation" value="deleteEducation">
                    <input type="hidden" name="employee_achievement_id" value=" `educatio.employee_achievement_id'];">
                    <input type="hidden" name="user_id" value=" user_id;">
                    <input type="hidden" name="society_id" value=" `educatio.society_id'];">
                    <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i> </button>
                  </form> */
      if (response.education.length > 0) {
        $.each(response.education, function (index, value) {
          data += `<tr>
                  <td>`+ i++ +`</td>
                  <td> `+ value.achievement_name + `</td>
                  <td> `+ value.university_board_name + `</td>
                  <td>`+ value.achievement_date + `</td>
                  <td>
                      <button type="submit" onclick="deleteEducation('deleteEducation',`+ value.employee_achievement_id + `,`+ value.user_id + `,`+ value.society_id + `)" class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i> </button>
                  </td>
                </tr>`;
        });
        $('#educationData').html(data);
      } else
      {
        $('.education_details').html(`<img width="250" src="img/no_data_found.png">`);
      }
      
    }
  });
}
function getNotes(user_id,society_id) {
  data = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "getUserNots",
      user_id: user_id,
      society_id: society_id,
    },
    success: function (response) {
      i = 1;
      /*  <form title="Delete" action="controller/userController.php" method="post">
                    <input type="hidden" name="deleteEducation" value="deleteEducation">
                    <input type="hidden" name="employee_achievement_id" value=" `educatio.employee_achievement_id'];">
                    <input type="hidden" name="user_id" value=" user_id;">
                    <input type="hidden" name="society_id" value=" `educatio.society_id'];">
                    <button type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i> </button>
                  </form> */
      if (response.user_notes.length > 0) {
        $.each(response.user_notes, function (index, value) {
          data += `<tr>
                    <td>`+ i++ +`</td>
                    <td>`+ value.note_title +`</td>
                    <td> `+ value.note_description+`</td>
                    <td>`+ value.created_date+`</td>
                    <td>
                      <button type="submit" onclick="deleteNoteData('deleteNote',`+ value.note_id + `,`+ value.user_id + `,`+ value.society_id + `)" class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i> </button>
                  </td>
                </tr>`;
        });
        $('#notesTableData').html(data);
      } else
      {
        $('.notsData').html(`<img width="250" src="img/no_data_found.png">`);
      }
      
    }
  });
}
function getUserAssets(user_id,society_id) {
  data = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "UserAssetsData",
      user_id: user_id,
      society_id: society_id,
    },
    success: function (response) {
      i = 1;
      if (response.user_assets.length > 0) {
        $.each(response.user_assets, function (index, value) {
          if (value.start_date !="0000-00") {
            value.start_date = value.start_date;
          } else {
            value.start_date = "";
          }
          data += ` <tr>
          <td> `+ i++ + `</td>
          <td> `+ value.assets_category + `</td>
          <td> `+ value.assets_name + `</td>
          <td> `+ value.assets_brand_name + `</td>
          <td class="text-center align-middle">`;
          if (value.assets_file != '') {
            data += `<a data-fancybox="images" data-caption="Photo Name :  ` + value.assets_file + `" href="../img/society/` + value.assets_file +`" target="_blank">
              <img style="max-height:100px; max-width:100px;" class="" onerror="this.src='../img/banner_placeholder.png'"  id="`+ value.assets_id +`" src="../img/society/` + value.assets_file + `"></a>`;
          }
            
          data += `</td>
                    <td>`+value.start_date+`                           
                    </td>
                    <td class="text-center align-middle">`;
                    if (value.handover_image != '') {
                    data += `<a data-fancybox="images" data-caption="Photo Name :  `+value.handover_image+`" href="../img/society/ `+value.handover_image+`" target='_blank'>
                        <img style="max-height:100px; max-width:100px;" class="lazyload" onerror="this.src='../img/banner_placeholder.png'" src='../img/banner_placeholder.png' id=" `+value.assets_id+`" data-src="../img/society/`+value.handover_image+`"></a>`;
                    }
          data += `</td>
                    <td>
                      <a href="assetsInventory?id= `+value.assets_id+`&inventory_id= `+value.inventory_id+`&custodian=takeover" class="btn btn-sm btn-success mt-2">Takeover Assets</a>
                    </td>
                  </tr>`;
        });
        $('#assetsData').html(data);
      } else
      {
        $('.assets').html(`<img width="250" src="img/no_data_found.png">`);
      }
      
    }
  });
}

function getUserBankData(user_id,society_id) {
  data = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "getEmployeeBankData",
      user_id: user_id,
      society_id: society_id,
    },
    success: function (response) {
      i = 1;
      if (response.user_bank.length > 0) {
        $.each(response.user_bank, function (index, value) {
          data += `<tr>
                    <td> `+ i++ +`</td>
                    <td>
                      <div class="d-flex align-items-center">
                        <form method="post" accept-charset="utf-8">
                          <input type="hidden" name="bank_id" value=" `+value.bank_id+`">
                          <input type="hidden" name="edit_hr_doc" value="edit_hr_doc">
                          <button type="button" class="btn btn-sm btn-primary mr-1" onclick="employeeBankDataSet( `+value.bank_id+`)" data-toggle="modal" > <i class="fa fa-pencil"></i></button>
                        </form>
                         <form method="post" accept-charset="utf-8">
                          <input type="hidden" name="bank_id" value=" `+value.bank_id+`">
                          <input type="hidden" name="deleteBankServer" value="deleteBankServer">
                          <button type="button" class="btn btn-sm btn-danger mr-1" onclick="deleteBankAccount( `+value.bank_id+`,`+value.user_id+`)" > <i class="fa fa-trash-o"></i></button>
                        </form>
                      </div>
                    </td>
                    <td> `+value.account_holders_name+`</td>
                    <td> `+value.bank_name+`</td>
                    <td> `+value.account_no+`</td>
                    <td> `+value.ifsc_code+`</td>
                    <td> `+value.crn_no+`</td>
                    <td> `+value.pan_card_no+`</td>
                    <td> `+value.bank_branch_name+`</td>
                    <td> `+value.account_type+`</td>
                    <td> `+value.bank_created_at+`</td>
                    
                  </tr>`;
        });
        $('#userBankData').html(data);
        
      } else
      {
        $('.bank').html(`<img width="250" src="img/no_data_found.png">`);
      }
      
    }
  });
}

function getVisitingCardData(user_id,society_id) {
  data = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "getEmployeeVisitingCard",
      user_id: user_id,
      society_id: society_id,
    },
    success: function (response) {
      i = 1;
      if (response.visiting_card.length > 0) {
        $.each(response.visiting_card, function (index, value) {
          data += `<div class='col-md-12 mt-2'>
                    <div class='row'>`;
                     if (value.user_visiting_card_back != '') {
                      data += `<div class='col-md-6'>
                        <a href="../img/users/recident_profile/`+value.user_visiting_card_front+`" data-fancybox="images" data-caption="Photo Name : Visiting Card Front">
                        <img style='height:245px;  width:100%;' src=../img/users/recident_profile/`+value.user_visiting_card_front+`>
                        </a>
                      </div>
                      <div class='col-md-6'>
                        <a href="../img/users/recident_profile/`+value.user_visiting_card_back+`" data-fancybox="images" data-caption="Photo Name : Visiting Card Back">
                        <img style='height:245px; width:100%;' src=../img/users/recident_profile/`+value.user_visiting_card_back+`>
                        </a>
                      </div>`;
                    } else {
                      data += `<div class='col-md-6 '>
                       <a href="../img/users/recident_profile/`+value.user_visiting_card_front+`" data-fancybox="images" data-caption="Photo Name : Visiting Card">
                        <img style='height:245px;  width:100%;' src=../img/users/recident_profile/`+value.user_visiting_card_front+`>
                        </a>
                      </div>`;
                    }
            data += `</div>
                  </div>`;
        });
        $('.vistingCardMainDiv').html(data);
        
      } else
      {
        $('.vistingCardMainDiv').html(`<img width="250" src="img/no_data_found.png">`);
      }
      
    }
  });
}


function deleteEducation(key, employee_achievement_id, user_id, society_id) {
 
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })

  .then((willDelete) => {
      if (willDelete) {
          $.ajax({
            url: "controller/userController.php",
            cache: false,
            type: "POST",
            data: {deleteEducation : key,employee_achievement_id
              :employee_achievement_id,user_id:user_id,society_id:society_id},
            success: function(response){
              // 
              if (response == 1) {
                swal("Deleted Successfully");
                document.location.reload(true);
                // history.go(0);
              } else {
                swal("Not Deleted");
                document.location.reload(true);
                /// history.go(0);
              }
            }
          });
      }else{
          swal("Your data is safe!");
      }
  });
}
function deleteNoteData(key, note_id, user_id, society_id) {
 
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })

  .then((willDelete) => {
      if (willDelete) {
          $.ajax({
            url: "controller/userController.php",
            cache: false,
            type: "POST",
            data: {deleteNote:key,note_id
              :note_id,user_id:user_id,society_id:society_id,csrf:csrf},
            success: function(response){
              
              if (response == 1) {
                swal("Deleted Successfully");
                document.location.reload(true);
                // history.go(0);
              } else {
                swal("Not Deleted");
                document.location.reload(true);
                /// history.go(0);
              }
            }
          });
      }else{
          swal("Your data is safe!");
      }
  });
}

    function getEmployeeHistory(user_id,society_id) {
      data = "";
      data2 = "";
      $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        async: false,
        data: {
          action: "getEmployeeHistory",
          user_id: user_id,
          society_id: society_id,
        },
        success: function (response) {
          i = 1;
         
          if (response.employee_history.length > 0) {
            $.each(response.employee_history, function (index, value) {
              if (value.salary_incrment == 0) {
                salary_incrment = "Yes";
              } else {
                salary_incrment = "No";
              }
              data += `<tr>
                          <td>`+i++ +`</td>
                          <td>`+value.designation+`</td>
                          <td>`+value.joining_date+`</td>
                          <td>`+value.end_date+`</td>
                          <td>`+value.remark+`</td>
                          <td>`+ salary_incrment+`</td>
                          <td>
                            <button onclick="deleteExperienceHistoryById(`+value.history_id+`,`+value.user_id+`,`+value.society_id+`)" type="button" class="btn btn-danger btn-sm "><i class="fa fa-trash-o"></i> </button>
                          </td>
                        </tr>`;
            });
            $('#expHistoryData').html(data);
          } else
          {
            $('.expHistory').html(`<img width="250" src="img/no_data_found.png">`);
          }
          if (response.employee_experience.length > 0) {
            $.each(response.employee_experience, function (index, value) {
              if (value.salary_incrment == 0) {
                salary_incrment = "Yes";
              } else {
                salary_incrment = "No";
              }
              data2 += `<tr>
                        <td>`+i++ +`</td>
                        <td>`+value.exp_company_name+`</td>
                        <td>`+value.designation+`</td>
                        <td>`+value.work_from+`</td>
                        <td>`+value.work_to+`</td>
                        <td>`+value.company_location+`</td>
                        <td>
                          <button onclick="deleteExperience(`+value.employee_experience_id+`,`+value.user_id+`,`+value.society_id+`)" type="submit"  class="btn btn-danger btn-sm form-btn"><i class="fa fa-trash-o"></i> </button>
                        </td>
                      </tr>`;
            });
            $('#empe_exp_data').html(data2);
          } else
          {
            $('.empe_exp').html(`<img width="250" src="img/no_data_found.png">`);
          }
          
        }
      });
    }
   
function deleteExperienceHistoryById(history_id, user_id, society_id) {
  
      swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this data!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
  
    .then((willDelete) => {
      if (willDelete) {
            $.ajax({
              url: "controller/userController.php",
              cache: false,
              type: "POST",
              data: {
                deleteExperienceHistory: "deleteExperienceHistory",
                history_id:history_id,user_id:user_id,society_id:society_id,csrf:csrf},
              success: function(response){
                if (response == 1) {
                  swal("Deleted Successfully");
                  document.location.reload(true);
                  // history.go(0);
                } else {
                  swal("Not Deleted");
                  document.location.reload(true);
                  /// history.go(0);
                }
              }
            });
        }else{
            swal("Your data is safe!");
        }
    });
}

function deleteExperience(employee_experience_id,user_id,society_id) {
  swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this data!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
})

.then((willDelete) => {
    if (willDelete) {
        $.ajax({
          url: "controller/userController.php",
          cache: false,
          type: "POST",
          data: {
            deleteExperience: "deleteExperience",
            employee_experience_id:employee_experience_id,user_id:user_id,society_id:society_id,csrf:csrf},
          success: function(response){
            
            if (response == 1) {
              swal("Deleted Successfully");
              document.location.reload(true);
              // history.go(0);
            } else {
              swal("Not Deleted");
              document.location.reload(true);
              /// history.go(0);
            }
          }
        });
    }else{
        swal("Your data is safe!");
    }
});

}

function payOutLeave(leave_payout_id) {
  
  $('#leave_payout_id').val(leave_payout_id);
  $('#leavePayoutModel').modal();
 
}

function approveRejectExpense(PaidValue)
{
  var oTable = $("#example").dataTable();
  var id = [];
  $(".multipleCheckbox:checked", oTable.fnGetNodes()).each(function(i)
  {
    id[i] = $(this).attr('user_expense_id');
  });
  if(id == "")
  {
    swal(
      'Warning !',
      'Please Select at least 1 Expense !',
      'warning'
    );
  }
  else if(id != "")
  {
    if(PaidValue == "approve")
    {
      swal({
        title: "Are you sure?",
        text: "You want to approve selected expenses!",
        icon: "warning",
        buttons: true,
        dangerMode: true
      })
      .then((willDelete) =>
      {
        if(willDelete)
        {
          $.ajax({
            url: "controller/ExpenseSettingController.php",
            type: "POST",
            data: {user_expense_ids : id,approveExpenses:"approveExpenses",csrf:csrf},
            success: function(response)
            {
              document.location.reload(true);
            }
          });
        }
      });
    }
    else
    {
      swal({
        title: "Are you sure?",
        text: "You want to reject selected expenses!",
        icon: "warning",
        buttons: true,
        dangerMode: true
      })
      .then((willDelete) =>
      {
        if(willDelete)
        {
          $.each(id, function (key, val)
          {
            $('<input>').attr({
                type: 'hidden',
                id: 'exids'+key,
                name: 'user_expense_ids['+key+']',
                class: 'expenseIdsDynamic',
                value: val
            }).appendTo('#expenesStatusRejectNew');
          });
          $('#expenesDeclineReasonNew').modal();
        }
      });
    }
  }
}

$('#expense_category_type').change(function ()
{
  var val = $(this).val();
  if(val == 1)
  {
    $('.unitDiv').removeClass('d-none');
    $('.amountDiv').addClass('d-none');
  }
  else
  {
    $('.unitDiv').addClass('d-none');
    $('.amountDiv').removeClass('d-none');
  }
});

$('#edit_expense_category_type').change(function ()
{
  var val = $(this).val();
  if(val == 1)
  {
    $('.unitDivEdit').removeClass('d-none');
    $('.amountDivEdit').addClass('d-none');
  }
  else
  {
    $('.unitDivEdit').addClass('d-none');
    $('.amountDivEdit').removeClass('d-none');
  }
});

$('.decimal').keypress(function (e) {
    var character = String.fromCharCode(e.keyCode)
    var newValue = this.value + character;
    if (isNaN(newValue) || parseFloat(newValue) * 100 % 1 > 0) {
        e.preventDefault();
        return false;
    }
});

function editExpenseCategoryNew(argument)
{
    $.ajax({
      url: 'controller/ExpenseSettingController.php',
      type: 'POST',
      data: {expense_category_id: argument,getExpenseInfo:'getExpenseInfo',csrf,csrf},
    })
    .done(function(response)
    {
      response = JSON.parse(response);
      $('#edit_csrf').val(csrf);
      $('#edit_expense_category_id').val(response.expense_category_id);
      $('#edit_expense_category_name').val(response.expense_category_name);
      $("#edit_expense_category_type").select2("val",response.expense_category_type);
      $("#edit_expense_category_type").trigger('change');
      $('#edit_expense_category_amount').val(response.expense_category_amount);
      $('#edit_expense_category_unit_name').val(response.expense_category_unit_name);
      $('#edit_expense_category_unit_price').val(response.expense_category_unit_price);
    });
}

$('#complainCategoryModal').on('hidden.bs.modal', function ()
{
  $('#addExpenseCategoryForm').trigger("reset");
  $("#addExpenseCategoryForm").validate().resetForm();
});

$('#editExpenseCategoryModal').on('hidden.bs.modal', function ()
{
  $('#editExpenseCategoryForm').trigger("reset");
  $("#editExpenseCategoryForm").validate().resetForm();
});


/* PMS Module 07/07/2022 */

function dimensionalDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getDimensional",
                dimensional_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#dimensional_name').val(response.dimensional.dimensional_name); 
              $('#dimensional_id').val(response.dimensional.dimensional_id); 
            }
    })
}

function attributeDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getAttribute",
                attribute_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              $('#attribute_name').val(response.attribute.attribute_name); 
              $('#dimensional_id').val(response.attribute.dimensional_id); 
              $('#attribute_type').val(response.attribute.attribute_type); 
              $('#attribute_id').val(response.attribute.attribute_id); 
              $('#dimensional_id').select2();
              $('#attribute_type').select2();
            }
    })
}

/* DAR Module 11/07/2022 */
function templateDataSet(id)
{ 
  $('.hideupdate').show();
  $('.hideAdd').hide();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getTemplate",
                template_id:id,
              },
            success: function(response){
              $('#addModal').modal(); 
              if (response.template.allow_muliple_time==1) {
                $(".allow_muliple_time_yes").prop("checked", true);
              } else {
                $(".allow_muliple_time_no").prop("checked", true);
              }

              $('#template_name').val(response.template.template_name); 
              $('#template_description').val(response.template.template_description); 
              $('#template_id').val(response.template.template_id); 
            }
    })
}

function companyOpeningReferStatusChange(id){
  $('#referStatusRejectModal').modal();
  $('#company_opening_refer_id').val(id);
}

/* 13 July 2022 */
function addLevel(level_id){
  $('.hideupdate').hide();
  $('.hideAdd').show();
  $('#addModal').modal(); 
  $('#parent_level_id').val(level_id); 
  $('#parent_level_id').select2();
}


function updateAttendance(id)
{
  $(".ajax-loader").show();
  $.ajax({
    url: "controller/AttendanceTypeController.php",
    cache: false,
    type: "POST",
    data:
    {
      getAttendanceinfo:"getAttendanceinfo",
      attendance_id:id,
      csrf:csrf
    },
    success: function (response)
    {
      if (response=='000') {
         $(".ajax-loader").hide();
        swal("Muliple Punch In Edit Not Allowed", {icon: "warning",});
      } else {
        response = JSON.parse(response);
        if(response.shift_type == "Night")
        {
          $('.attendance-end-date-div').removeClass("d-none");
          $('.asd').addClass("asd-night");
          $('.asd-night').removeClass("asd");
          var tomorrow = new Date(response.attendance_date_start);
          var sameDate = tomorrow.toLocaleDateString('en-CA');
          tomorrow.setDate(tomorrow.getDate()+1);
          var f = tomorrow.toLocaleDateString('en-CA');

          var strFirstThree = response.punch_in_time.substring(0,2);
          var ampm = (strFirstThree >= 12) ? "PM" : "AM";
          $('#attendance_end_date').append($("<option></option>").attr("value",sameDate).text(sameDate));
          $('#attendance_end_date').append($("<option></option>").attr("value",f).text(f));
          if(response.attendance_date_end == sameDate)
          {
            $('#attendance_end_date').val(sameDate);
            $('#attendance_end_date').trigger("change");
          }
          if(response.attendance_date_end == f)
          {
            $('#attendance_end_date').val(f);
            $('#attendance_end_date').trigger("change");
          }
        }
        else
        {
            $('#attendance_end_date').append($("<option></option>").attr("value",response.attendance_date_start).text(response.attendance_date_start));
        }
        $(".shift_type").val(response.shift_type);
        $("#modified_remark").val(response.modified_remark);
        $('#update_user_id').val(response.user_id);
        $('#update_unit_id').val(response.unit_id);
        $('#update_user_name').val(response.user_full_name);
        $('#shift_time_id').val(response.shift_time_id);
        $('#update_attendance_id').val(response.attendance_id);
        $('#attendance_start_date_View').val(response.attendance_date_start);
        $('#punch_in_time').val(response.punch_in_time);
        if(response.punch_out_time != "00:00:00")
        {
          $('#edit_punch_out_time').val(response.punch_out_time);
        }
        $("#late_in").select2("val", response.late_in);
        $("#early_out").select2("val", response.early_out);
        $('#updateAttendanceModal').modal();
        $(".ajax-loader").hide();
      }
    }
  });
}

function editAttendanceForm()
{
  
  var startDate = $("#attendance_start_date_View").val();
  var endDate = $("#attendance_end_date").val();
  var st1 = $('#punch_in_time').val();
  var et1 = $('#edit_punch_out_time').val();
  // var st = tConvert(st1);
  // var et = tConvert(et1);
  var fullStart = startDate + " " + st1;
  var fullEnd = endDate + " " + et1;
  var startTime = new Date(fullStart);
  var endTime = new Date(fullEnd);

  // console.log(startTime);

  if(startTime > endTime)
  {
    swal("Punch out time must be greater than punch in time", {icon: "warning",});
  }else if(et1=='') {
    swal("Please set punch out time", {icon: "warning",});
  }
  else
  {
    if($('#updateAttendanceForm').valid())
    {
      $.ajax({
        url: "controller/AttendanceTypeController.php",
        cache: false,
        type: "POST",
        data: $("#updateAttendanceForm").serialize(),
        success: function(response)
        {
            location.reload();
        }
      });
    }
    else
    {
      $('#updateAttendanceForm').validate()
    }
  }
}

$('#updateAttendanceModal').on('hidden.bs.modal', function ()
{
  $('#updateAttendanceForm').trigger("reset");
  $("#updateAttendanceForm").validate().resetForm();
  $("#attendance_end_date").empty();
  $('#attendance_end_date').append($("<option></option>").attr("value","").text("--SELECT--"));
});


function approveBirthdateRequest(birthdate_change_request_id,user_id,new_birthdate)
{
  swal({
    title: "Are you sure?",
    text: "You want to approve birthdate change request!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  })
  .then((willDelete) =>
  {
    if (willDelete)
    {
      $.ajax({
        url: "controller/userController.php",
        cache: false,
        type: "POST",
        data: {birthdate_change_request_id : birthdate_change_request_id,user_id:user_id,approveBirthdateRequest:"approveBirthdateRequest",new_birthdate:new_birthdate,csrf:csrf},
        success: function(response)
        {
          location.reload();
        }
      });
    }
  });
}

function rejectBirthdateRequest(birthdate_change_request_id)
{
  swal({
    title: "Are you sure?",
    text: "You want to reject birthdate change request!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  })
  .then((willDelete) =>
  {
    if (willDelete)
    {
      $('#birthdate_change_request_id').val(birthdate_change_request_id);
      $('#rejectRequestModal').modal();
    }
  });
}


function approveRequest()
{
  var oTable = $("#example").dataTable();
  var val = [];
  var user_id = [];
  var birth_dates = [];
  $(".multiApproveRejectCheckbox:checked", oTable.fnGetNodes()).each(function(i)
  {
    val[i] = $(this).val();
    user_id[i] = $(this).attr("user-id");
    birth_dates[i] = $(this).attr("birth-date");
  });
  if(val == "")
  {
    swal(
      'Warning !',
      'Please Select at least 1 request !',
      'warning'
    );
  }
  else
  {
    swal({
      title: "Are you sure?",
      text: "You want to approve birthdate change request!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) =>
    {
      if(willDelete)
      {
        $.ajax({
          url: "controller/userController.php",
          cache: false,
          type: "POST",
          data: {ids : val,approveRequestMulti:"approveRequestMulti",user_id:user_id,birth_dates:birth_dates,csrf:csrf},
          success: function(response)
          {
            location.reload();
          }
        });
      }
    });
  }
}

function rejectRequest()
{
  var oTable = $("#example").dataTable();
  var val = [];
  $(".multiApproveRejectCheckbox:checked", oTable.fnGetNodes()).each(function(i)
  {
    val[i] = $(this).val();
  });
  if(val == "")
  {
    swal(
      'Warning !',
      'Please Select at least 1 request !',
      'warning'
    );
  }
  else
  {
    swal({
      title: "Are you sure?",
      text: "You want to reject birthdate change request!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) =>
    {
      if(willDelete)
      {
        $.each(val, function (key, value)
        {
          if(key == 0)
          {
            $('<input>').attr({
                type: 'hidden',
                id: 'birthdate_change_request_id',
                name: 'birthdate_change_request_id[]',
                value: value
            }).appendTo('#rejectBirthdateRequestForm');
          }
          else
          {
            $('<input>').attr({
                type: 'hidden',
                id: 'birthdate_change_request_id'+key,
                name: 'birthdate_change_request_id['+key+']',
                value: value
            }).appendTo('#rejectBirthdateRequestForm');
          }
        });
        $('#rejectRequestModal').modal();
      }
    });
  }
}

function getCelebrationList()
{
  $.ajax({
    url: "controller/userController.php",
    cache: false,
    type: "POST",
    data: {getCelebrationList:"getCelebrationList",csrf:csrf},
    success: function(response)
    {
      response = JSON.parse(response);
      if(response.birthdays.length > 0)
      {
        var add = "";
        $.each(response.birthdays, function (key, val)
        {
          var add3 = "";
          let length = val.user_designation;
          if(length > 17)
          {
            add3 = "..";
          }
          add = '<div class="col-lg-6 col-xl-3"><div class="col"><div class="card radius-10 border-1px"><div class="p-2"><div class="d-flex align-items-start "><div class="text-success"><img class="myIcon lazyload rounded-circle" onerror="this.onerror=null;this.src=\'../img/users/recident_profile/user_default.png\'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/'+val.user_profile_pic+'"><img class="myIcon lazyload rounded-circle mt-2" src="img/icons/cake.png"></div><div class="w-100 pl-1"><p class="mb-0 text-primary"><b>'+val.user_full_name+'</b></p><span class="mb-0 text-primary" style="font-size: 14px;">'+val.user_designation.substring(0, 17)+add3+'</span><p class="my-1 text-primary"><b>'+val.member_date_of_birth+'</b></p></div></div></div></div></div></div>';
          $('.birthCelebration').append(add);
        });
      }
      else
      {
        add = '<div class="col text-danger ml-5"><b>No Upcoming Birthdays</b></div>';
          $('.birthCelebration').append(add);
      }

      if(response.wedding.length > 0)
      {
        var add1 = "";
        $.each(response.wedding, function (key, val)
        {
          var add4 = "";
          let length = val.user_designation;
          if(length > 17)
          {
            add4 = "..";
          }
          add1 = '<div class="col-lg-6 col-xl-3"><div class="col"><div class="card radius-10 border-1px"><div class="p-2"><div class="d-flex align-items-start "><div class="text-success"><img class="myIcon lazyload rounded-circle" onerror="this.onerror=null;this.src=\'../img/users/recident_profile/user_default.png\'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/'+val.user_profile_pic+'"><img class="myIcon lazyload rounded-circle mt-2" src="img/icons/wedding-ring.png"></div><div class="w-100 pl-1"><p class="mb-0 text-primary"><b>'+val.user_full_name+'</b></p><span class="mb-0 text-primary" style="font-size: 14px;">'+val.user_designation.substring(0, 17)+add4+'</span><p class="my-1 text-primary"><b>'+val.wedding_anniversary_date+'</b></p></div></div></div></div></div></div>';
          $('.weddingCelebration').append(add1);
        });
      }
      else
      {
        add1 = '<div class="col text-danger ml-5"><b>No Upcoming Wedding Anniversary</b></div>';
          $('.weddingCelebration').append(add1);
      }

      if(response.joining.length > 0)
      {
        var add2 = "";
        $.each(response.joining, function (key, val)
        {
          var add5 = "";
          let length = val.user_designation;
          if(length > 17)
          {
            add5 = "..";
          }
          add2 = '<div class="col-lg-6 col-xl-3"><div class="col"><div class="card radius-10 border-1px"><div class="p-2"><div class="d-flex align-items-start "><div class="text-success"><img class="myIcon lazyload rounded-circle" onerror="this.onerror=null;this.src=\'../img/users/recident_profile/user_default.png\'" src="../img/users/recident_profile/user_default.png" data-src="../img/users/recident_profile/'+val.user_profile_pic+'"><img class="myIcon lazyload rounded-circle mt-2" src="img/icons/schedule.png"></div><div class="w-100 pl-1"><p class="mb-0 text-primary"><b>'+val.user_full_name+'</b></p><span class="mb-0 text-primary" style="font-size: 14px;">'+val.user_designation.substring(0, 17)+add5+'</span><p class="my-1 text-primary"><b>'+val.joining_date+'</b></p></div></div></div></div></div></div>';
          $('.workCelebration').append(add2);
        });
      }
      else
      {
        add2 = '<div class="col text-danger ml-5"><b>No Upcoming Wedding Anniversary</b></div>';
          $('.workCelebration').append(add2);
      }
      $('#birthWeddingJoinModal').modal();
    }
  });
}


$('#birthWeddingJoinModal').on('hidden.bs.modal', function ()
{
  $('.birthCelebration').empty();
  $('.weddingCelebration').empty();
  $('.workCelebration').empty();
});

var x = 0;
var list_maxField = 9;
var numberIncr = 1;
$('.list_add_button').click(function()
{
  if(x < list_maxField)
  {
    if($('#expense_title1').length == 0)
    {
        numberIncr = 1;
    }
    else if($('#expense_title2').length == 0)
    {
        numberIncr = 2;
    }
    else if($('#expense_title3').length == 0)
    {
        numberIncr = 3;
    }
    else if($('#expense_title4').length == 0)
    {
        numberIncr = 4;
    }
    else if($('#expense_title5').length == 0)
    {
        numberIncr = 5;
    }
    else if($('#expense_title6').length == 0)
    {
        numberIncr = 6;
    }
    else if($('#expense_title7').length == 0)
    {
        numberIncr = 7;
    }
    else if($('#expense_title8').length == 0)
    {
        numberIncr = 8;
    }
    else if($('#expense_title9').length == 0)
    {
        numberIncr = 9;
    }
    else if($('#expense_title10').length == 0)
    {
        numberIncr = 10;
    }
    else
    {
        numberIncr = 1;
    }
    $.ajax({
      url: "controller/ExpenseSettingController.php",
      cache: false,
      type: "POST",
      data: {getExpenseCategoryList:"getExpenseCategoryList",csrf:csrf},
      success: function(response)
      {
        response = JSON.parse(response);
        var add = "";
        $.each(response, function (key, val)
        {
          $('.appendNew').empty();
          $('.appendNew').append('<option value="">--SELECT--</option>');
          add += '<option value="'+val.expense_category_id+'">'+val.expense_category_name+'</option>';
          $('.appendNew').append(add);
        });
      }
    });
    x++;
    var list_fieldHTML = '<div class="row p-3 justify-content-md-center pb-3"><div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group">Title<span class="text-danger">*</span><input name="expense_title[' + numberIncr + ']" maxlength="80" autocomplete="off" required type="text" class="form-control" id="expense_title'+numberIncr+'"/></div></div><div class="col-xs-3 col-sm-3 col-md-3"><div class="form-group">Date<span class="text-danger">*</span><input type="text" name="date[' + numberIncr + ']" class="form-control past-dates-datepicker" readonly required autocomplete="off" id="date'+numberIncr+'"/></div></div><div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group">Category Type<span class="text-danger">*</span><select class="form-control single-select ect appendNew" required name="expense_category_id[' + numberIncr + ']" id="expense_category_id'+numberIncr+'"><option value="">--SELECT--</option></select><input type="hidden" name="expense_category_type['+numberIncr+']" id="expense_category_type'+numberIncr+'"/></div></div><div class="col-xs-1 col-sm-1 col-md-1"><button class="btn btn-danger list_remove_button btn-sm" type="button"><span class="fa fa-minus-circle" aria-hidden="true"></span></button></div><div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group">Description<input name="description[' + numberIncr + ']" maxlength="200" type="text" class="form-control" autocomplete="off" id="description'+numberIncr+'"/></div></div><div class="col-xs-3 col-sm-3 col-md-3"><div class="form-group">Attachment<input name="expense_document[' + numberIncr + ']" type="file" class="form-control" required accept="image/jpeg,image/gif,image/png,application/pdf,image/jpg"/></div></div><div class="col-xs-4 col-sm-4 col-md-4"><div class="form-group"><span id="unit-amount'+numberIncr+'">Amount</span><span class="text-danger">*</span><input type="text" required name="amount[' + numberIncr + ']" class="form-control decimal unit-amount'+numberIncr+' unitKeyup" autocomplete="off" id="amount'+numberIncr+'"/></div></div><div class="col-xs-1 col-sm-1 col-md-1"></div><div class="col-xs-4 col-sm-4 col-md-4 unitDiv'+numberIncr+' d-none"><div class="form-group">Unit Name<span class="text-danger">*</span><input type="text" class="form-control" maxlength="140" autocomplete="off" readonly required name="expense_category_unit_name[' + numberIncr + ']" id="expense_category_unit_name'+numberIncr+'"></div></div><div class="col-xs-3 col-sm-3 col-md-3 unitDiv'+numberIncr+' d-none"><div class="form-group">Unit Price<span class="text-danger">*</span><input type="text" class="form-control decimal" autocomplete="off" required readonly name="expense_category_unit_price[' + numberIncr + ']" id="expense_category_unit_price'+numberIncr+'"></div></div><div class="col-xs-4 col-sm-4 col-md-4 unitDiv'+numberIncr+' d-none"><div class="form-group">Final Amount<input type="text" class="form-control decimal finalAmountUnitWise" id="unit_amount_calc'+numberIncr+'" disabled readonly></div></div><div class="col-xs-1 col-sm-1 col-md-1"></div></div>';
      $('.list_wrapper').append(list_fieldHTML);
      numberIncr++;
      $('.single-select').select2({
        placeholder: "--SELECT--",
      });
      $('.past-dates-datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        endDate: date,
      });
  }
  $('form.check-validate').validate();
  $('[name^="expense_title"]').each(function() {
      $(this).rules('add', {
          required: true,
          noSpace: true,
          messages: {
              required: "Please enter expense title",
              noSpace: "No Space"
          }
      });
  });
  $('[name^="date"]').each(function() {
      $(this).rules('add', {
          required: true,
          messages: {
              required: "Please select date"
          }
      });
  });
  $('[name^="amount"]').each(function() {
      $(this).rules('add', {
          required: true,
          noSpace: true,
          messages: {
              required: "Please enter amount",
              noSpace: "No Space"
          }
      });
  });
  $('[name^="unit"]').each(function() {
      $(this).rules('add', {
          required: true,
          noSpace: true,
          messages: {
              required: "Please enter unit",
              noSpace: "No Space"
          }
      });
  });
  $('[name^="description"]').each(function() {
      $(this).rules('add', {
          required: false,
          noSpace: true,
          messages: {
              noSpace: "No Space"
          }
      });
  });
  $('[name^="expense_document"]').each(function() {
      $(this).rules('add', {
          required: false,
          extensionDocumentNew: true,
          filesize2MB: true
      });
  });
  $('[name^="expense_category_id"]').each(function() {
      $(this).rules('add', {
          required: true,
          messages: {
              required: "Please select type"
          }
      });
  });
  $('[name^="expense_category_unit_name"]').each(function() {
      $(this).rules('add', {
          required: true,
          noSpace: true,
          messages: {
              required: "Please enter unit name",
              noSpace: "No Space"
          }
      });
  });
  $('[name^="expense_category_unit_price"]').each(function() {
      $(this).rules('add', {
          required: true,
          noSpace: true,
          messages: {
              required: "Please enter unit price",
              noSpace: "No Space"
          }
      });
  });
});


$('.list_wrapper').on('click', '.list_remove_button', function()
{
    swal({
        title: "Are you sure?",
        text: "Once removed, you will not be able to recover this data !",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) =>
    {
        if (willDelete)
        {
            $(this).closest('div.row').remove();
            x--;
            swal("Successfully Removed.", {
            icon: "success",
            timer: 5000
            });
        }
    });
});

$('.ect').change(function ()
{
  var val = $(this).val();
  $.ajax({
    url: "controller/ExpenseSettingController.php",
    cache: false,
    type: "POST",
    data: {id : val,getCategoryDetails:"getCategoryDetails",csrf:csrf},
    success: function(response)
    {
      response = JSON.parse(response);
      $('#expense_category_type').val(response.expense_category_type);
      $('#unit_amount_calc').val('');
      $('.unit-amount').val('');
      if(response.expense_category_type == 1)
      {
        $('#expense_category_unit_name').val(response.expense_category_name);
        $('#expense_category_unit_price').val(response.expense_category_unit_price);
        $('#unit-amount').text('Unit');
        $('.unit-amount').attr('name','unit[]');
        $('.unit-amount').attr('id','unit');
        $('.unitDiv').removeClass('d-none');
      }
      else
      {
        $('#expense_category_amount').val(response.expense_category_amount);
        $('#unit-amount').text('Amount');
        $('.unit-amount').attr('name','amount[]');
        $('.unit-amount').attr('id','amount');
        $('.unitDiv').addClass('d-none');
      }
    }
  });
});

$(".list_wrapper").on("change", ".ect", function()
{
  myString = this.id;
  var lastChar = myString[myString.length -1];
  var val = $(this).val();
  $.ajax({
    url: "controller/ExpenseSettingController.php",
    cache: false,
    type: "POST",
    data: {id : val,getCategoryDetails:"getCategoryDetails",csrf:csrf},
    success: function(response)
    {
      response = JSON.parse(response);
      $('#expense_category_type'+lastChar).val(response.expense_category_type);
      $('#unit_amount_calc'+lastChar).val('');
      $('.unit-amount'+lastChar).val('');
      if(response.expense_category_type == 1)
      {
        $('#expense_category_unit_name'+lastChar).val(response.expense_category_name);
        $('#expense_category_unit_price'+lastChar).val(response.expense_category_unit_price);
        $('#unit-amount'+lastChar).text('Unit');
        $('.unit-amount'+lastChar).attr('name','unit['+lastChar+']');
        $('.unit-amount'+lastChar).attr('id','unit'+lastChar);
        $('.unitDiv'+lastChar).removeClass('d-none');
      }
      else
      {
        $('#expense_category_amount'+lastChar).val(response.expense_category_amount);
        $('#unit-amount'+lastChar).text('Amount');
        $('.unit-amount'+lastChar).attr('name','amount['+lastChar+']');
        $('.unit-amount'+lastChar).attr('id','amount'+lastChar);
        $('.unitDiv'+lastChar).addClass('d-none');
      }
    }
  });
});


$(".resetBtn").click(function()
{
  $(".single-select").select2('val', 'All');
});

function changeSalaryStatus(salary_issue_id)
{
  $('#salary_issue_id').val(salary_issue_id);
  $('#csrf').val(csrf);
}

$('#changeSalaryIssueStatusModal').on('hidden.bs.modal', function ()
{
  $('#salaryIssueStatusChangeForm').trigger("reset");
  $("#salaryIssueStatusChangeForm").validate().resetForm();
});


$(".list_wrapper").on("keyup", ".unitKeyup", function()
{
  myString = this.id;
  var lastChar = myString[myString.length -1];
  var unit = $(this).val();
  if(lastChar.match(/^-?\d+$/))
  {
    var unitPrice = $('#expense_category_unit_price'+lastChar).val();
    var amount = unit * unitPrice;
    $('#unit_amount_calc'+lastChar).val(amount);
  }
  else
  {
    var unitPrice = $('#expense_category_unit_price').val();
    var amount = unit * unitPrice;
    $('#unit_amount_calc').val(amount);
  }
});

$('#unit').keyup(function ()
{
  var unit = $(this).val();
  var unitPrice = $('#expense_category_unit_price').val();
  var amount = unit * unitPrice;
  $('#unit_amount_calc').val(amount);
});


function approveSalaryIssue(salary_issue_id)
{
  swal({
    title: "Are you sure?",
    text: "Yoy want to approve salary issue!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) =>
  {
    if(willDelete)
    {
      $(".ajax-loader").show();
      $.ajax({
        url: "controller/SalaryController.php",
        cache: false,
        type: "POST",
        data: {salary_issue_id : salary_issue_id,approveSalaryIssue:"approveSalaryIssue",csrf:csrf},
        success: function(response)
        {
          $(".ajax-loader").hide();
          location.reload();
        }
      });
    }
  });
}

function celebrationsFilter(val)
{
  if(val == 1)
  {
    $('.bdayDiv').removeClass("d-none");
    $('.weddDiv').addClass("d-none");
    $('.workDiv').addClass("d-none");
  }
  else if(val == 2)
  {
    $('.bdayDiv').addClass("d-none");
    $('.weddDiv').removeClass("d-none");
    $('.workDiv').addClass("d-none");
  }
  else if(val == 3)
  {
    $('.bdayDiv').addClass("d-none");
    $('.weddDiv').addClass("d-none");
    $('.workDiv').removeClass("d-none");
  }
  else
  {
    $('.bdayDiv').removeClass("d-none");
    $('.weddDiv').removeClass("d-none");
    $('.workDiv').removeClass("d-none");
  }
}


function weddingNotification(user_id)
{
  $('.modal-title-noti').text('Send Wedding Anniversary Notification');
  $('#title').val('Happy Wedding Anniversary');
  $('#celebrationType').val('wedding');
  $('#birthNotiModal').modal();
  $('#user_id').val(user_id);
}

function workNotification(user_id)
{
  $('.modal-title-noti').text('Send Work Anniversary Notification');
  $('#title').val('Happy Work Anniversary');
  $('#celebrationType').val('work');
  $('#birthNotiModal').modal();
  $('#user_id').val(user_id);
}

$('#birthNotiModal').on('hidden.bs.modal', function ()
{
  $('#birthdayNotificationForm').trigger("reset");
  $("#birthdayNotificationForm").validate().resetForm();
  $("#description").val();
});


/*25 June 2022*/
function getDepartmentForPMS()
{
  $(".ajax-loader").show();
  var block_ids = $('#block_ids').val();
  if(block_ids != null){
    var block_ids = block_ids.join("','");
  }
  $.ajax({
      url: "getDepartmentListForPMS.php",
      cache: false,
      type: "POST",
      data: {
        blockIds:block_ids,
        },
      success: function(response){
        $(".ajax-loader").hide();
        $('#floor_ids').html(response);
      }
  });
}

$('.pmsAccessForDepartmentMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all department, please removed selected department!");
      $('option:contains(All Department)').prop("selected", false);
    }else  {

    }
  }
});

$('.pmsAccessForBranchMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all branch, please removed selected branch!");
      $('option:contains(All Branch)').prop("selected", false);
    }else  {

    }
  }
});

$('.pmsAccessForLevelMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all level, please removed selected level!");
      $('option:contains(All Level)').prop("selected", false);
    }else  {

    }
  }
});

function getEmployeeByLeaveForPMS()
{
  var level_ids = $('#level_ids').val();
  var floor_ids = $('#floor_ids').val();
  var block_ids = $('#block_ids').val();
  if(block_ids != null){
    var block_ids = block_ids.join("','");
  }
  if(floor_ids != null){
      var floor_ids = floor_ids.join("','");
    }
  if(level_ids != null){
    var level_ids = level_ids.join("','");
  }
  if(level_ids && floor_ids && block_ids){
    $(".ajax-loader").show();
    $.ajax({
        url: "getEmployeeListByLevelForPMS.php",
        cache: false,
        type: "POST",
        data: {
          level_ids:level_ids,
          floor_ids:floor_ids,
          block_ids:block_ids,
          },
        success: function(response){
          $(".ajax-loader").hide();
          $('#users_ids').html(response);
        }
    });
  }else if(block_ids == null){
    swal('Please select branch');
  }else if(floor_ids == null){
    swal('Please select department');
  } else{
    swal('Please select level');
  }
}

function getUserTeamMembers(user_id,level_id,society_id) {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "UserTeamMembers",
      user_id: user_id,
      society_id: society_id,
      level_id: level_id,
    },
    success: function (response) {
      i = 1;
      var main_content = '';
      if (response.team_members.length > 0) {
        $.each(response.team_members, function (index, value) {
          main_content += 
          `<div class="col-6 col-12 col-lg-6 col-xl-6 pt-3">
            <div class="card gradient-scooter  no-bottom-margin">
              <div class="card-body text-center pb-0">
                <a href="employeeDetails?id=`+value.user_id+`">
                <div class="row">
                  <div class="col-3">
                    <img id="blah" onerror="this.src='img/user.png'" src="img/user.png" data-src="../img/users/recident_profile/`+value.user_profile_pic+`" width="60" height="60" alt="your image" class="rounded-circle p-1 lazyload">
                  </div>
                  <div class="col-9 text-left">
                    <h6 class="text-white mt-2 text-capitalize"><b>`+value.user_full_name+`</b> </h6>
                    <i class="text-white" style="font-size: 11px;font-style: normal;"><i class="fa fa-arrow-circle-up"></i> `+value.user_designation+` </i><br>
                    <i class="text-white" style="font-size: 11px;font-style: normal;"><i class="fa fa-arrow-circle-up"></i> `+value.level_name+` </i>
                    <i class="text-white" style="font-size: 11px;font-style: normal;"><i class="fa fa-arrow-circle-up"></i> `+value.floor_name+` - `+value.block_name+` </i>

                  </div>
                  
                </div>
              </a>
              </div>
            </div>
          </div>`;
        });
        $('#teamMembersData').html(main_content);
      } else
      {
        $('#teamMembersData').html(`<img width="250" src="img/no_data_found.png">`);
      }
      
    }
  });
}

function idProofDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getIdProof",
        id_proof_id:id,
      },
    success: function(response){
      $('#addModal').modal(); 
      $('#id_proof_id').val(response.id_proof.id_proof_id); 
      $('#id_proof_name').val(response.id_proof.id_proof_name);     
      $('#id_proof_pages').val(response.id_proof.id_proof_pages);     
    }
  })
}

function getUserIdProof(user_id,society_id) {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    async: false,
    data: {
      action: "UserIdProof",
      user_id: user_id,
      society_id: society_id,
    },
    success: function (response) {
      console.log(response);
      i = 1;
      var main_content = '';
      if (response.id_proof.length > 0) {
        
           main_content += 
          `<div class="col-md-12 col-12 text-center">
              <div class ="table-responsive"" >
              <table class="table table-bordered" >
                <tr>
                  <th>Name</th>
                  <th>File</th>
                  <th>Action</th>
                <tr>`;
              $.each(response.id_proof, function (index, value) { 
                var imgIcon = 'img/doc.png';
                if (value.id_proof_ext == 'pdf' || value.id_proof_ext == 'PDF') {
                  imgIcon = 'img/pdf.png';
                } else if (value.id_proof_ext == 'jpg' || value.id_proof_ext == 'jpeg' || value.id_proof_ext == 'png') {
                  imgIcon = 'img/jpg.png';
                } else if(value.id_proof_ext == 'png'){
                  imgIcon = 'img/png.png';
                }else if (value.id_proof_ext == 'doc' || value.id_proof_ext == 'docx') {
                  imgIcon = 'img/doc.png';
                } else{
                  imgIcon = 'img/doc.png';
                }

                if (value.id_proof_back!='') {
                    if (value.id_proof_ext_back == 'pdf' || value.id_proof_ext_back == 'PDF') {
                      imgIconBack = 'img/pdf.png';
                    } else if (value.id_proof_ext_back == 'jpg' || value.id_proof_ext_back == 'jpeg' || value.id_proof_ext_back == 'png') {
                      imgIconBack = 'img/jpg.png';
                    } else if(value.id_proof_ext_back == 'png'){
                      imgIconBack = 'img/png.png';
                    }else if (value.id_proof_ext_back == 'doc' || value.id_proof_ext_back == 'docx') {
                      imgIconBack = 'img/doc.png';
                    } else{
                      imgIconBack = 'img/doc.png';
                    }
                  }

                main_content +=
                `<tr>
                  <td>`+value.id_proof_name+` </td>
                  <td><a  data-fancybox="images" data-caption="Name : `+value.id_proof_name+`" target="_blank" href="../img/id_proof/`+value.id_proof+`">
                      <img width="70" height="70" src="`+imgIcon+`" alt=""></a>`;
                  if (value.id_proof_back!='') { 
                     main_content +=`<a  data-fancybox="images" data-caption="Name : `+value.id_proof_name+`" target="_blank" href="../img/id_proof/`+value.id_proof_back+`">
                      <img width="70" height="70" src="`+imgIcon+`" alt=""></a>`;
                  }
                  main_content += `</td>
                  <td><button type="submit" onclick="deleteIdProofData(`+ value.id_proof_id + `, `+ value.user_id + `)" class="btn btn-link text-danger btn-sm " ><i class="fa fa-trash-o"></i> Delete </button>
                  </td>
                <tr>`;
              });
             main_content +=`</table>
            </div>`;
             
        $('.idProofData').html(main_content);
      } else
      {
        $('.idProofData').html(`<img width="250" src="img/no_data_found.png">`);
      }
      
    }
  });
}

function deleteIdProofData(id_proof_id,user_id) {
 
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })

  .then((willDelete) => {
      if (willDelete) {
          $.ajax({
            url: "controller/userController.php",
            cache: false,
            type: "POST",
            data: {deleteIdProof:'deleteIdProof',
                  id_proof_id:id_proof_id,
                  user_id:user_id,
                  csrf:csrf},
            success: function(response){
              
              if (response == 1) {
                swal("Deleted Successfully");
                document.location.reload(true);
                // history.go(0);
              } else {
                swal("Not Deleted");
                document.location.reload(true);
                /// history.go(0);
              }
            }
          });
      }else{
          swal("Your data is safe!");
      }
  });
}


function editHolidayGroup(id)
{
  $.ajax({
    url: "controller/HolidayController.php",
    cache: false,
    type: "POST",
    data: {
        getHolidayGroupDetails : "getHolidayGroupDetails",
        holiday_group_id : id,
        csrf : csrf
    },
    success: function(response)
    {
      response = JSON.parse(response);
      $('#holiday_group_id').val(id);
      $('#holiday_group_name_edit').val(response.holiday_group_name);
      $('#modalHolidayGroupEdit').modal();
    }
  });
}

$('#modalHolidayGroup').on('hidden.bs.modal', function ()
{
  $('#holidayGroupAddForm').trigger("reset");
  $("#holidayGroupAddForm").validate().resetForm();
});

$('#modalHolidayGroupEdit').on('hidden.bs.modal', function ()
{
  $('#holidayGroupEditForm').trigger("reset");
  $("#holidayGroupEditForm").validate().resetForm();
});

function assignHolidayGroupId(holiday_group_id,holiday_id)
{
  $.ajax({
    url: "controller/HolidayController.php",
    cache: false,
    type: "POST",
    data: {
        updateHolidayGroupIdInMaster : "updateHolidayGroupIdInMaster",
        holiday_group_id : holiday_group_id,
        holiday_id : holiday_id,
        csrf : csrf
    },
    success: function(response)
    {
      location.reload();
    }
  });
}

function getFloorByBlockIdAddGroup()
{
  var holiday_group_id = $('#holiday_group_id').val();
  var flag = 0;
  var id_check = $('#block_id_assign').val()+'';
  var id = $('#block_id_assign').val();
  var splitString = id_check.split(',');
  if(id != 0)
  {
    if(splitString.includes('0'))
    {
      flag = 1;
    }
  }
  if(id == null)
  {
    $('.departmentDiv').addClass("d-none");
    $('.levelDiv').addClass("d-none");
    $('.empDiv').addClass("d-none");
  }
  else if(flag == 1)
  {
    $('.select2-container--below').removeClass("error");
    $("#block_id_assign").val('').change();
    $("#block_id_assign").val('0').trigger('change');
  }
  else if(id != 0)
  {
    $('.select2-container--below').removeClass("error");
    var optionContent = "";
    $.ajax({
      url: "controller/HolidayController.php",
      cache: false,
      type: "POST",
      data: {
        getFloorByBlockId:"getFloorByBlockId",
        block_ids:id,
        holiday_group_id:holiday_group_id,
        csrf:csrf
      },
      success: function (response)
      {
        response = JSON.parse(response);
        optionContent = `<option value="">Select Department</option>`;
        if(access_branchs_id=='') {
          optionContent = `<option value="0">All Department</option>`;
        }
        $.each(response, function( index, value )
        {
          optionContent += `<option value="`+value.floor_id+`" >`+value.floor_name+` (`+value.block_name+`)</option>`;
        });
        $('#floor_id').html(optionContent);
        $('.departmentDiv').removeClass("d-none");
        $('.levelDiv').addClass("d-none");
        $('.empDiv').addClass("d-none");
      }
    });
  }
}

function getBranches()
{
  var holiday_group_id = $('#holiday_group_id').val();
  $("#floor_id").empty();
  $("#level_id").empty();
  $("#user_id").empty();
  $('.departmentDiv').addClass("d-none");
  $('.levelDiv').addClass("d-none");
  $('.empDiv').addClass("d-none");
  $.ajax({
      url: "controller/HolidayController.php",
      cache: false,
      type: "POST",
      data: {
        getBranchByGroup:"getBranchByGroup",
        holiday_group_id:holiday_group_id,
        csrf:csrf
      },
      success: function (response)
      {
        response = JSON.parse(response);
        optionContent = `<option value="">Select Branch</option>`;
        if(access_branchs_id=='') {
          optionContent = `<option value="0">All Branch</option>`;
        }
        $.each(response, function( index, value )
        {
          optionContent += `<option value="`+value.block_id+`" >`+value.block_name+`</option>`;
        });
        $('#block_id_assign').html(optionContent);
      }
    });
}

function getAllLevel()
{
  var holiday_group_id = $('#holiday_group_id').val();
  var flag = 0;
  var id_check = $('#floor_id').val()+'';
  var id = $('#floor_id').val();
  var splitString = id_check.split(',');
  if(id != 0)
  {
    if(splitString.includes('0'))
    {
      flag = 1;
    }
  }
  if(id == null)
  {
    $('.levelDiv').addClass("d-none");
    $('.empDiv').addClass("d-none");
  }
  else if(flag == 1)
  {
    $('.select2-container--below').removeClass("error");
    $("#floor_id").val('').change();
    $("#floor_id").val('0').trigger('change');
  }
  else if(id != 0)
  {
    $('.select2-container--below').removeClass("error");
    var optionContent = "";
    $.ajax({
      url: "controller/HolidayController.php",
      cache: false,
      type: "POST",
      data: {
        getLevelByDepartmentId:"getLevelByDepartmentId",
        floor_ids:id,
        holiday_group_id:holiday_group_id,
        csrf:csrf
      },
      success: function (response)
      {
        response = JSON.parse(response);
        optionContent = `<option value="">Select Level</option>`;
        optionContent = `<option value="0">All Level</option>`;
        $.each(response, function( index, value )
        {
          optionContent += `<option value="`+value.level_id+`" >`+value.level_name+`</option>`;
        });
        $('#level_id').html(optionContent);
        $('.levelDiv').removeClass("d-none");
        $('.empDiv').addClass("d-none");
      }
    });
  }
}

function getEmp()
{
  var holiday_group_id = $('#holiday_group_id').val();
  var flag = 0;
  var id_check = $('#level_id').val()+'';
  var splitString = id_check.split(',');
  var dep = $('#floor_id').val();
  var level_id = $('#level_id').val();
  if(level_id != 0)
  {
    if(splitString.includes('0'))
    {
      flag = 1;
    }
  }
  if(level_id == null)
  {
    $('.empDiv').addClass("d-none");
  }
  else if(flag == 1)
  {
    $('.select2-container--below').removeClass("error");
    $("#level_id").val('').change();
    $("#level_id").val('0').trigger('change');
  }
  else if(level_id != 0)
  {
    $('.select2-container--below').removeClass("error");
    var optionContent = "";
    $.ajax({
      url: "controller/HolidayController.php",
      cache: false,
      type: "POST",
      data: {
        getEmpLevel:"getEmpLevel",
        level_id:level_id,
        floor_id:dep,
        holiday_group_id:holiday_group_id,
        csrf:csrf
      },
      success: function (response)
      {
        response = JSON.parse(response);
        optionContent = `<option value="">Select Employee</option>`;
        optionContent = `<option value="0">All Employees</option>`;
        $.each(response, function(index, value)
        {
          optionContent += `<option value="`+value.user_id+`">`+value.user_full_name+`</option>`;
        });
        $('#user_id').html(optionContent);
        $('.empDiv').removeClass("d-none");
      }
    });
  }
}

function validateEmpDrop()
{
  var flag = 0;
  var id_check = $('#user_id').val()+'';
  var splitString = id_check.split(',');
  var user_id = $('#user_id').val();
  if(user_id != 0)
  {
    if(splitString.includes('0'))
    {
      flag = 1;
    }
  }
  if(flag == 1)
  {
    $('.select2-container--below').removeClass("error");
    $("#user_id").val('').change();
    $("#user_id").val('0').trigger('change');
  }
}


  $('#getVisitorReport').click(function () {
      if($("#getVisitorReportForm").valid()){   // test for validity
        $('#visitorReport').css("display","block");
        $('#visitorReportNoData').css("display","none");

        var from = $(".from").val();
        var toDate = $(".toDate").val();
        var block_id = $("#block_id").val();
        //var csrf =$('input[name="csrf"]').val();
        var report_download_access =$('input[name="report_download_access"]').val();
        var role_id =$('input[name="role_id"]').val();
        

        if(report_download_access=="0"){
            maintable = $('#visitorReportWithBtn').DataTable( {
              "destroy": true,
              "bPaginate": false,
              "bLengthChange": false,
              "bFilter": true,
              "bInfo": false,
              "bAutoWidth": false,
              "processing": true,
              "serverSide": true,
              "ajax": {
                      url: "visitorReport_server_processing.php",
                      type: 'POST',
                            data: {from:from,toDate:toDate,block_id:block_id,report_download_access:report_download_access,role_id:role_id,csrf:csrf}
                    },
               } );
        }else{
          maintable = $('#visitorReportWithBtn').DataTable({
            "destroy": true,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false,
            "dom": 'Blfrtip',
            //"order": [[0, 'desc']],
            "processing": true,
            "serverSide": true,
            "paging": false,
            "buttons": [
            {
              extend: 'copyHtml5',
              title: $("#reportName").val(),
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'csv',
              title: $("#reportName").val(),
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'excelHtml5',
              title: $("#reportName").val(),
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend : 'pdfHtml5',
              title: $("#reportName").val(),
              orientation : 'landscape',
              pageSize : 'LEGAL',
              titleAttr : 'PDF',
              exportOptions: {
                columns: ':visible'
              }
            },'colvis'],
            "ajax": {
              url: "visitorReport_server_processing.php",
              type: 'POST',
                    data: {from:from,toDate:toDate,block_id:block_id,report_download_access:report_download_access,role_id:role_id,csrf:csrf}
            },

            
          });
        }
        
        
        
      }
      
  });
  //20220422

$('#addAttendaceModalNew').on('hidden.bs.modal', function ()
{
    $('#addAttendanceFormNew').trigger("reset");
    $("#addAttendanceFormNew").validate().resetForm();
});

// 06 DEC 2022 SHUBHAM
function ShowCommonValue(leave_default_count_id)
{
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getLeaveDefaultCountById",
        leave_default_count_id:leave_default_count_id,
      },
    success: function(response){
      main_content = "";
      if(response.status=200){
        $('#leaveGroupModal').modal();
        $('#leave_group_name').html(response.Leave.leave_group_name);
        $('#leave_type').html(response.Leave.leave_type_name);
        $('#number_of_leaves').html(response.Leave.number_of_leaves);
        $('#getLeaves_use_in_month').html(response.Leave.leaves_use_in_month);
        $('#carry_forward_to_next_year').html(response.Leave.carry_forward_to_next_year);
        $('#max_carry_forward').html(response.Leave.max_carry_forward);
        $('#min_carry_forward').html(response.Leave.min_carry_forward);
        $('#pay_out').html(response.Leave.pay_out);
        $('#leave_calculation').html(response.Leave.leave_calculation);
        $('#pay_out_remark').html(response.Leave.pay_out_remark);
        }
    }
  });
  
}


$('.sortableMainMenu').sortable({
  opacity: 1,
  cursor:         'move',
  placeholder:    'sortable-placeholder',
  handle:         '.block-title',
  cursorAt:       { left: 150, top: 17 },
  tolerance:      'pointer',
  scroll:         false,
  zIndex:         9999,
  update  : function(event, ui)
  {
    var menu_id = new Array();
    $('.mainMenuId').each(function()
    {
      menu_id.push($(this).data("post-id"));
    });
    $.ajax({
      url:"controller/menuController.php",
      method:"POST",
      data:{menu_id:menu_id,admin_menu_sorting:'admin_menu_sorting',csrf:csrf},
      success:function(response)
      {
        if(response == 1)
        {
          Lobibox.notify('success', {
            pauseDelayOnHover: true,
            continueDelayOnInactiveTab: false,
            position: 'top right',
            icon: 'fa fa-times-circle',
            msg: 'Menu order successfully changed'
          });
        }
        else
        {
          Lobibox.notify('error', {
            pauseDelayOnHover: true,
            continueDelayOnInactiveTab: false,
            position: 'top right',
            icon: 'fa fa-times-circle',
            msg: 'Something went wrong'
          });
        }
      }
    });
  }
});

$('.sortableSub').sortable({
  opacity: 0.5,
  cursor:         'move',
  placeholder:    'sortable-placeholder',
  handle:         '.block-title',
  cursorAt:       { left: 150, top: 17 },
  tolerance:      'pointer',
  scroll:         false,
  zIndex:         9999,
  update  : function(event, ui)
  {
    id = this.id;
    var sub_menu_id = new Array();
    $('#'+id+' .subMenuId').each(function()
    {
      sub_menu_id.push($(this).data("post-id"));
    });
    $.ajax({
      url:"controller/menuController.php",
      method:"POST",
      data:{sub_menu_id:sub_menu_id,admin_sub_menu_sorting:'admin_sub_menu_sorting',csrf:csrf},
      success:function(response)
      {
        if(response == 1)
        {
          Lobibox.notify('success', {
            pauseDelayOnHover: true,
            continueDelayOnInactiveTab: false,
            position: 'top right',
            icon: 'fa fa-times-circle',
            msg: 'Menu order successfully changed'
          });
        }
        else
        {
          Lobibox.notify('error', {
            pauseDelayOnHover: true,
            continueDelayOnInactiveTab: false,
            position: 'top right',
            icon: 'fa fa-times-circle',
            msg: 'Something went wrong'
          });
        }
      }
    });
  }
});

$('#sisterCompanyDetailsModal').on('hidden.bs.modal', function ()
{
    $('.sisComDetails').empty();
});