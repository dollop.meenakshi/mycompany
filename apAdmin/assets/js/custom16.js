jQuery(document).ready(function($){

  var country_code = $('#country_code_get').val();
  $("#country_code").val(country_code);
  var country_code_alt = $('#country_code_get_alt').val();
  $("#country_code_alt").val(country_code_alt);
  var country_code_emergency_get = $('#country_code_emergency_get').val();
  $("#country_code_emergency").val(country_code_emergency_get);
  var country_code_whatsapp_get = $('#country_code_whatsapp_get').val();
  $("#country_code_whatsapp").val(country_code_whatsapp_get);
  
  $('#is_taxble').on('change', function() {
    if(this.value=="1"){
      $('#gst_detail_div').css('display','block');
    } else {
      $('#gst_detail_div').css('display','none');
    }
    
});


  $('#member_relation_name').on('change', function() {
    if(this.value=="Other"){
      $('.otherRelationDiv').css('display','block');
      $('#member_relation_name_other').val('');
    } else {
      $('.otherRelationDiv').css('display','none');
    }
    
});


  $('#paymentCompanyChange').on('change', function() {
    // alert(this.value);
    if(this.value=="2"){
      $('#payUmoneyDiv').css('display','none');
      $('#upiDiv').css('display','none');
      $('#razorpayDiv').css('display','block');
      $('#flutterwaveDiv').css('display','none');
    }else if(this.value=="3"){
      $('#payUmoneyDiv').css('display','none');
      $('#upiDiv').css('display','block');
      $('#razorpayDiv').css('display','none');
      $('#flutterwaveDiv').css('display','none');
    } else if(this.value=="4"){
      $('#payUmoneyDiv').css('display','none');
      $('#upiDiv').css('display','none');
      $('#razorpayDiv').css('display','none');
      $('#flutterwaveDiv').css('display','block');
    } else {
      $('#payUmoneyDiv').css('display','block');
      $('#razorpayDiv').css('display','none');
      $('#upiDiv').css('display','none');
      $('#flutterwaveDiv').css('display','none');
    }
    
});

  
  $('#member_relation_add').on('change', function() {
    if(this.value=="Other"){
      $('.otherRelationDivAdd').css('display','block');
      $('#member_relation_name_other').val('');
    } else {
      $('.otherRelationDivAdd').css('display','none');
    }
    
});

$('#member_relation_primary').on('change', function() {
    if(this.value=="Other"){
      $('.otherRelationDivPrimary').css('display','block');
      $('#member_relation_name_other_primary').val('');
    } else {
      $('.otherRelationDivPrimary').css('display','none');
    }
    
});
//IS_1686 27march2020
$('#is_taxble_bill_type').on('change', function() {
    if(this.value=="1"){
      $('#income_gst_detail_div').css('display','block');
    } else {
      $('#income_gst_detail_div').css('display','none');
    }
    
});
//IS_1686 27march2020

    // Notice Board Start
   $('#summernoteEditor').summernote({
  height: 400,
  tabsize: 2,
  toolbar: [
  [ 'style', [ 'style' ] ],
  [ 'font', [ 'bold', 'italic', 'underline', 'clear'] ],
  [ 'fontname', [ 'fontname' ] ],
  [ 'fontsize', [ 'fontsize' ] ],
  [ 'color', [ 'color' ] ],
  [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
  [ 'table', [ 'table' ] ],
  [ 'insert', [ 'link','picture'] ],
  [ 'view', [ 'undo', 'redo', 'codeview', 'help' ] ]
  ]
});

 $('#summernoteImgage').summernote({
  height: 400,
  tabsize: 2,
  toolbar: [
    [ 'style', [ 'style' ] ],
    [ 'font', [ 'bold', 'italic', 'underline', 'clear'] ],
    [ 'fontname', [ 'fontname' ] ],
    [ 'fontsize', [ 'fontsize' ] ],
    [ 'color', [ 'color' ] ],
    [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
    [ 'table', [ 'table' ] ],
    [ 'insert', [ 'link','picture','video'] ],
    [ 'view', [ 'undo', 'redo', 'codeview', 'help' ] ],
  ]
  ,
  callbacks: {
    onImageUpload: function(image) {
      uploadImage(image[0]);
    }
  }
});
 function uploadImage(image) {
  var data = new FormData();
  data.append("image", image);
  $.ajax({
    url: 'controller/noticeBoardImage.php',
    cache: false,
    contentType: false,
    processData: false,
    data: data,
    type: "post",
    success: function(url) {
      if (url==0) {
        swal("Error! Please Upload Only Photo !", {icon: "error",});

      } else {

      var image = $('<img width=320>').attr('src', url);
      $('#summernoteImgage').summernote("insertNode", image[0]);
      }
    },
    error: function(data) {
    }
  });
}

$('#CoursesummernoteImgage').summernote({
  height: 400,
  tabsize: 2,
  toolbar: [
  [ 'style', [ 'style' ] ],
  [ 'font', [ 'bold', 'italic', 'underline', 'clear'] ],
  [ 'fontname', [ 'fontname' ] ],
  [ 'fontsize', [ 'fontsize' ] ],
  [ 'color', [ 'color' ] ],
  [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
  [ 'table', [ 'table' ] ],
  [ 'insert', [ 'link','picture'] ],
  [ 'view', [ 'undo', 'redo', 'codeview', 'help' ] ]
  ],
  callbacks: {
    onImageUpload: function(image) {
      CourseuploadImage(image[0]);
    }
  }
});
  
function CourseuploadImage(image) {
  var data = new FormData();
  data.append("image", image);
  $.ajax({
    url: 'controller/CourseLessonImage.php',
    cache: false,
    contentType: false,
    processData: false,
    data: data,
    type: "post",
    success: function(url) {
      if (url==0) {
        swal("Error! Please Upload Only Photo !", {icon: "error",});
      } else {
        var image = $('<img width=320>').attr('src', url);
        $('#CoursesummernoteImgage').summernote("insertNode", image[0]);
      }
    },
    error: function(data) {
    }
  });
}
  
$(".selectAll").on("change", function(){
  table.$("input[type='checkbox']").attr('checked', $(this.checked));  
});


    //IS_986
    $( "#noticeBoard" ).submit(function( event ) {
     if ($('#summernoteImgage').summernote('codeview.isActivated')) {
       $('#summernoteImgage').summernote('codeview.deactivate');
     }
   });

$('#balancesheetIncome').dataTable({
  aaSorting: [[4, 'desc']],
  "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
               return nRow;
            },
});



$('#balancesheetExpence').dataTable({
  aaSorting: [[3, 'desc']]
});

$('#balancesheetExpencePublic').dataTable({
  aaSorting: [[2, 'desc']],
  lengthChange: false,
  bPaginate: false,
   bInfo : false,
   bFilter :false,
   "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
               return nRow;
            },
            

});

$('#balancesheetExpencePublicSumary').dataTable({
  lengthChange: false,
  bPaginate: false,
   bInfo : false,
   bFilter :false,
});
$('#bulkSalary').dataTable({
  lengthChange: false,
  bPaginate: false,
   bFilter :true,
});
// data table start
      $('#default-datatable').DataTable({
        drawCallback: function(){
          $("img.lazyload").lazyload();
       }
      });
      $('#default-datatable1').DataTable({
        drawCallback: function(){
          $("img.lazyload").lazyload();
       }
      });
      $('#default-datatable2').DataTable({
        drawCallback: function(){
          $("img.lazyload").lazyload();
       }
      });
      $('#default-datatable3').DataTable({
        drawCallback: function(){
          $("img.lazyload").lazyload();
       }
      });
      //IS_1217 13march2020
      $('#default-datatable4').DataTable({
        drawCallback: function(){
          $("img.lazyload").lazyload();
       }
      });
      $('#default-datatable5').DataTable({
        drawCallback: function(){
          $("img.lazyload").lazyload();
       }
      });
      $('#default-datatable6').DataTable({
        drawCallback: function(){
          $("img.lazyload").lazyload();
       }
      });


//IS_675 //26feb2020
var table = $('#examplePenaltiesTbl').DataTable( {
  lengthChange: false,
  order: [[ 8, 'desc' ]] 
});
table.buttons().container().appendTo( '#examplePenaltiesTbl_wrapper .col-md-6:eq(0)' );
//IS_675 //26feb2020

var table = $('#example,.exampleReport').DataTable( {
  drawCallback: function(){
          $("img.lazyload").lazyload();
       },
  lengthChange: true,
  pageLength: 25,
  lengthMenu: [[5,10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        // buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );


var table = $('#viewOrderDetails').DataTable( {
  drawCallback: function(){
          $("img.lazyload").lazyload();
       },
    lengthChange: false,
    bPaginate : false,
    bInfo: false,
    bFilter: false,
  } );

var table1 = $('#exampleReportBackup').DataTable( {
  "bPaginate": false,
  "bLengthChange": false,
  "bFilter": true,
  "bInfo": false,
  "bAutoWidth": false 

} );

var table1 = $('#exampleReport').DataTable( {
  "bPaginate": false,
  "bLengthChange": false,
  "bFilter": true,
  "bInfo": false,
  "bAutoWidth": false,
  "dom": 'Blfrtip',
  "buttons": [
  {
    extend: 'copyHtml5',
    title: $("#reportName").val(),
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend: 'csv',
    title: $("#reportName").val(),
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend: 'excelHtml5',
    title: $("#reportName").val(),
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend : 'pdfHtml5',
    title: $("#reportName").val(),
    orientation : 'landscape',
    pageSize : 'LEGAL',
    titleAttr : 'PDF',
    exportOptions: {
      columns: ':visible'
    }
  },'colvis']
} );

var table1 = $('#exampleReportWithoutBtn').DataTable( {
  "bPaginate": false,
  "bLengthChange": false,
  "bFilter": true,
  "bInfo": false,
  "bAutoWidth": false,
   } );

var tableBala = $('#exampleReportBalancesheet').DataTable( {
   aaSorting: [[2, 'desc']],
  "bPaginate": false,
  "bLengthChange": false,
  "bFilter": true,
  "bInfo": false,
  "bAutoWidth": false,
  "dom": 'Blfrtip',
   "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
               return nRow;
            },
  "buttons": [
  {
    extend: 'copyHtml5',
    title: $("#reportName").val(),
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend: 'csv',
    title: $("#reportName").val(),
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend: 'excelHtml5',
    title: $("#reportName").val(),
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend: 'pdfHtml5',
    title: $("#reportName").val(),
    exportOptions: {
      columns: ':visible'
    }
  },'colvis']
} );

var tableBala = $('#exampleReportBalancesheetWithoutBtn').DataTable( {
  aaSorting: [[2, 'desc']],
 "bPaginate": false,
 "bLengthChange": false,
 "bFilter": true,
 "bInfo": false,
 "bAutoWidth": false,
} );

table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
table1.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
tableBala.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );

// Data Table End

$('#loginOTP').on('click',function(e){

    e.preventDefault();


   var adminMobile= $('#mobile').val();

   if($.trim(adminMobile) ==''){
     swal("Error! Please Provide Mobile Number!", {icon: "error",});
   } else {

    var csrf =$('input[name="csrf"]').val();
      $.ajax({
      url: 'controller/loginController.php',
      cache: false,
      data: {adminMobile : adminMobile,SendOPT:'yes',csrf:csrf},
      type: "post",
      success: function(data) {

        if(data==0){
          swal("Error! Please Enter Registered Mobile Number!", { icon: "error", });
        } else if(data==2){
          swal("Error! Something Went Wrong, Please Try Again!", { icon: "error",  });
        } else {

           
            $('#success_info').text('OTP Sent to '+adminMobile+', Please Provide in below textbox.');
           
          $('#verifyOTPFrm :input[name="verify_mobile"]').val(adminMobile);
            $('#verifyOTP').click();
        }
          
      },
      error: function(data) {
         swal("Error! Something Went Wrong, Please Try Again!", { icon: "error",  });
      }
    });
   }

   });

  

});

/* $('#status_type_select').on('change', function(){
    if(this.value == 1)
    {
      $('#class_name').text('Class *');
      $('#uni_achi_name').text('University Name *');
      $('#pass_achi_date').text('Passing Date *');
      var span = $("#class_name");
      span.html(span.html().replace('*', '<span style="color: red">$&</span>'));
      var span1 = $("#uni_achi_name");
      span1.html(span1.html().replace('*', '<span style="color: red">$&</span>'));
      var span2 = $("#pass_achi_date");
      span2.html(span2.html().replace('*', '<span style="color: red">$&</span>'));
    }
    else if(this.value == 2)
    {
      
      $('#class_name').text('Achievement Name *');
      $('#uni_achi_name').text('Achievement from *');
      $('#pass_achi_date').text('Achievement Date *');
      var span3 = $("#class_name");
      span3.html(span3.html().replace('*', '<span style="color: red">$&</span>'));
      var span4 = $("#uni_achi_name");
      span4.html(span4.html().replace('*', '<span style="color: red">$&</span>'));
      var span5 = $("#pass_achi_date");
      span5.html(span5.html().replace('*', '<span style="color: red">$&</span>'));
    }
    else
    { 
      $('#class_name').text('Class *');
      $('#uni_achi_name').text('University Name *');
      $('#pass_achi_date').text('Passing Date *');
      var span = $("#class_name");
      span.html(span.html().replace('*', '<span style="color: red">$&</span>'));
      var span1 = $("#uni_achi_name");
      span1.html(span1.html().replace('*', '<span style="color: red">$&</span>'));
      var span2 = $("#pass_achi_date");
      span2.html(span2.html().replace('*', '<span style="color: red">$&</span>'));
    }
}).trigger('change'); */
// multiselect delete function

function deleteData(id) {
              swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data !",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
              $('form.deleteForm'+id).submit();
                  swal("Successfully Deleted !", {
                    icon: "success",
                  });
                } else {
                  // swal("Your imaginary file is safe!");
                }
              });
  }

$(document).on("click", "button[type='reset']", function(){
       // $("select").trigger("change");
       // $(".single-select").select2('val', 'All');
       $('.single-select').val('').trigger('change');
});

// multiple delete
// function DeleteAllTem(deleteValue) {
//     var myArray = [];
//     var id = "";
//     var oTable = $("#example").dataTable();
//     $(".multiDelteCheckbox:checked", oTable.fnGetNodes()).each(function() {
//         if (id != "") {
//             id = id + "," + $(this).val();
//         } else {
//             id = $(this).val();
//         }
//     });
//     alert(id);
// }

function DeleteAll(deleteValue) {
    // var myArray = [];
    // var val = [];
    var oTable = $("#example").dataTable();
    // $(".multiDelteCheckbox:checked", oTable.fnGetNodes()).each(function(i) {
    //     if (val != "") {
    //         val[i] = val + "," + $(this).val();
    //     } else {
    //         val = $(this).val();
    //     }
    // });

    var val = [];
          $(".multiDelteCheckbox:checked", oTable.fnGetNodes()).each(function(i) {
            val[i] = $(this).val();
          });
    if(val=="") {
      swal(
        'Warning !',
        'Please Select at least 1 item !',
        'warning'
      );
    } else {
       
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              // $('form.deleteForm'+id).submit();
          $.ajax({
              url: "controller/deleteController.php",
              cache: false,
              type: "POST",
              data: {ids : val,deleteValue
                :deleteValue},
              success: function(response){
                if(response==1) {
                  document.location.reload(true);
                  // history.go(-1);
                } else {
                  document.location.reload(true);
                  // history.go(0);
                }
              }
            });

             
            } else {
              // swal("Your data is safe!");
            }
          });
    }
}
  // change status 

  $(function() {

    $('.selectAll').click(function() {
        $('.multiDelteCheckbox').prop('checked', this.checked);
    });

});

  

function changeStatus(id,status) {
  swal({
    title: "Are you sure?",
    text: "You want to change status!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  })
  .then((willDelete) =>
  {
    if(willDelete)
    {
      var csrf =$('input[name="csrf"]').val();
      $(".ajax-loader").show();
      $.ajax({
        url: "controller/statusController.php",
        cache: false,
        type: "POST",
        data: {id : id,status
          :status, csrf:csrf},
        success: function(response)
        {
          $(".ajax-loader").hide();
          if(response==1)
          {
            swal("Status Changed",{
              icon: "success",
            });
            document.location.reload(true);
          }
          else
          {
            document.location.reload(true);
            swal("Something Wrong!",{
              icon: "error",
            });
          }
        }
      });
    }
  });
}


$('#new_primary_id').on('change', function() {
  var user_type =$('#userType').val();
  var user_id =$('#user_id').val();
  var unit_id =$('input[name="unit_id"]').val();
  var csrf =$('input[name="csrf"]').val();
  var new_primary_id =this.value;
  $(".ajax-loader").show();
  $.ajax({
        url: "getOtherFamilyMember.php",
        cache: false,
        type: "POST",
        data: {user_type:user_type,new_primary_id : new_primary_id,unit_id
          :unit_id,user_id:user_id, csrf:csrf},
        success: function(response){
          $(".ajax-loader").hide();
          $('#familyLoadDiv').html(response);
         
        }
      });
});


  // var csrf =$('input[name="csrf"]').val();   Aleredu imclude in footer.php
//IS_1045 7march2020
$('.block-id-cls').change(function() {
      var elem = $(this);
      var  id = elem.closest('tr').attr('id');

      var detail = id.split('~');

      var block_id= detail[0];
      var society_id= detail[1];
      var block_sort =  $(this).val();
      $.ajax({
        url: "controller/blockController.php",
        cache: false,
        type: "POST",
        data: {block_id : block_id,society_id:society_id,block_sort:block_sort, updateBlocksAjax:'yes',csrf:csrf},
        success: function(response){
          var obj =response.split('~');
          if(obj[0]=="error"){
             swal("Error! Duplicate Order Number!", {
                            icon: "error",
                          });
            // $('#block_id_'+block_id).html('<center><label   style="color: #ff0000;"  >'+obj[1]+'</label></center>');
          } else {
            swal("Success ! Branch Order Number Changed!", {
                            icon: "success",
                          });
            // $('#block_id_'+block_id).html('<center><label   style="color: #2dce89;"  >'+obj[1]+'</label></center>');
            // location.reload();
          }


        }
      });  

    });
//IS_1045 7march2020




function  getBlockList() {
  var no_of_blocks = $("#no_of_blocks").val();
  var block_type = $("#block_type").val();
  $.ajax({
        url: "getBlockList.php",
        cache: false,
        type: "POST",
        data: {no_of_blocks : no_of_blocks,block_type:block_type,csrf:csrf},
        success: function(response){
            $('#BlockResp').html(response);
          
            
        }
     });
}

function  getSubCategory() {
  var business_categories = $("#business_categories").val();
  if (business_categories=='Other') {
    $("#ProfessionalOther").show();
  }else {
    $("#ProfessionalOther").hide();
    $("#ProfessionalTypeOther").hide();
  }
  $.ajax({
        url: "getBusSubCategory.php",
        cache: false,
        type: "POST",
        data: {business_categories : business_categories,csrf:csrf},
        success: function(response){
            $('#business_categories_sub').html(response);
        }
     });
}



function  otherCheck() {
  var business_categories_sub = $("#business_categories_sub").val();
  if (business_categories_sub=='Other') {
    if (business_categories_sub=='Other') {
      $("#ProfessionalTypeOther").show();
    }else {
      $("#ProfessionalTypeOther").hide();
    }

  }

}


function  otherDailyCheck() {
  var visitor_sub_type_id = $("#visitor_sub_type_id option:selected").html();
    if (visitor_sub_type_id=='Other') {
      $("#otherCompany").show();
    }else {
      $("#otherCompany").hide();
    }


}



function  getFloorList() {
  var no_of_floor = $("#no_of_floor").val();
  var floor_type = $("#floor_type").val();
  $.ajax({
        url: "getFloorList.php",
        cache: false,
        type: "POST",
        data: {no_of_floor : no_of_floor,floor_type:floor_type,csrf:csrf},
        success: function(response){
            $('#floorResp').html(response);
          
            
        }
     });
}

function  getSubDepartmentList() {
  var no_of_sub_dept = $("#no_of_sub_dept").val();
  var floor_type = $("#floor_type").val();
  $.ajax({
        url: "getSubDepartmentList.php",
        cache: false,
        type: "POST",
        data: {no_of_sub_dept : no_of_sub_dept,floor_type:floor_type,csrf:csrf},
        success: function(response){
            $('#subDeptResp').html(response);
          
            
        }
     });
}

function  editAgent(agent_id) {
  
  $.ajax({
        url: "editAgent.php",
        cache: false,
        type: "POST",
        data: {agent_id : agent_id,csrf:csrf},
        success: function(response){
            $('#agentEditDiv').html(response);
        }
     });
}

function DeleteBlock(block_id) {
    // alert(block_id);
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/blockController.php",
                cache: false,
                type: "POST",
                data: {deleteBlock : block_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                    
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  } else {
                         swal("Token Mismatch", {
                            icon: "error",
                          });
                  }
                }
              });
             
            } else {
              //swal("Your data is safe!");
            }
          });

}

function DeleteFloor(floor_id) {
    // alert(floor_id);
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/floorController.php",
                cache: false,
                type: "POST",
                data: {deleteFloor : floor_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  } else {
                         swal("Success ! Your data has been deleted!", {
                            icon: "error",
                          });
                  }
                }
              });

             
            } else {
              //swal("Your data is safe!");
            }
          });



}

function DeleteSubDepartment(sub_department_id) {
  // alert(floor_id);
  swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              // $('form.deleteForm'+id).submit();
              $.ajax({
              url: "controller/subDepartmentController.php",
              cache: false,
              type: "POST",
              data: {deleteSubDept : sub_department_id,csrf:csrf},
              success: function(response){
                if(response==1) {
                       swal("Success ! Your data has been deleted!", {
                          icon: "success",
                        });
                  document.location.reload(true);
                } else {
                       swal("Success ! Your data has been deleted!", {
                          icon: "error",
                        });
                }
              }
            });

           
          } else {
            //swal("Your data is safe!");
          }
        });



}

function DeleteUnit(unit_id) {
    // alert(unit_id);
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/unitController.php",
                cache: false,
                type: "POST",
                data: {deleteUnit : unit_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  } else {
                         swal("Success ! Your data has been deleted!", {
                            icon: "error",
                          });
                  }
                }
              });

             
            } else {
              //swal("Your data is safe!");
            }
          });



}


function DeleteParking(parking_id) {
    // alert(parking_id);
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/parkingController.php",
                cache: false,
                type: "POST",
                data: {deleteParking_id : parking_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  } else {
                         swal("Success ! Your data has been deleted!", {
                            icon: "error",
                          });
                  }
                }
              });

             
            } else {
              //swal("Your data is safe!");
            }
          });



}


function editFloor (floor_id,floor_name) {
    $('#floorId').val(floor_id);
    $('#oldFloorname').val(floor_name);

}

function editSubDepartment (sub_department_id,sub_department_name) {
  $('#subDepartmentId').val(sub_department_id);
  $('#oldSubDepartmentName').val(sub_department_name);

}

function editBlock (block_id,block_name,block_sort) {
    $('#floorId').val(block_id);
    $('#oldFloorname').val(block_name);
    $('#block_sort').val(block_sort);

}


function editParking (parking_id,parking_name,society_parking_id) {
    $('#parking_id').val(parking_id);
    $('#oldParkingName').val(parking_name);
    $('#society_parking_id').val(society_parking_id);

}
 function editKbgCategory (kbg_category_id,category_name) {
    $('#kbg_category_id').val(kbg_category_id);
    $('#category_name').val(category_name);

}
function addParking(parking_id, Type, parking_name, society_parking_id) {
  $('#P_id').val(parking_id);
  $('#sParking_id').val(society_parking_id);
  $('#pType').html(Type);
  //IS_607
  $('#CappType').html(Type);
  $('#parkingName').html(parking_name);
  $('#alocateParkingName').val(parking_name);

}

function updateParking(parking_id,Type,parking_name,society_parking_id,unit_id) {
  $('.P_id11').val(parking_id);
  $('.sParking_id1').val(society_parking_id);
  $('.unitId').val(unit_id);
  $('#pType1').html(Type);
   //IS_607
  $('#UpCappType').html(Type);
  $('#parkingName1').html(parking_name);
  $('#alocateParkingNameEdit').val(parking_name);
   $.ajax({
        url: "getParkingDetails.php",
        cache: false,
        type: "POST",
        data: {unit_id : unit_id,UpCappType:Type,parking_id:parking_id,csrf:csrf},
        success: function(response){
            $('#getParkingDetails').html(response);
        }
      });
}

function approveParking(parking_id,Type,parking_name,society_parking_id,unit_id) {
 
   $.ajax({
        url: "getParkingDetailsPending.php",
        cache: false,
        type: "POST",
        data: {unit_id : unit_id,Type:Type,parking_name:parking_name,society_parking_id:society_parking_id,parking_id:parking_id,csrf:csrf},
        success: function(response){
            $('#pendingParkingDiv').html(response);
        }
      });
}

function checkMobileSociety() {
  var secretary_mobile= $('#secretary_mobile').val();
  
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {secretary_mobile : secretary_mobile,checkSocietyMobile:'checkSocietyMobile',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This mobile number is Already Used.'
              //   });

              swal("This Mobile Number is Already Used.", {
                            icon: "error",
                          });
                  
           } else {
               document.getElementById("socAddBtn").disabled=false;
           }
        }
      });
}


function checkMobileUser() {
  var userMobile= $('#userMobile').val();
  
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {userMobile : userMobile,checkUserMobile:'checkUserMobile',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This mobile number is Already Used.'
              //   });
                  
                swal("This Mobile Number is Already Used.", {
                            icon: "error",
                          });
           } else {
               document.getElementById("socAddBtn").disabled=false;
           }
        }
      });
}


function checkMobileUserTenant() {
  var userMobile= $('#userMobileTenant').val();
   if (userMobile.length>0) {

     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {userMobile : userMobile,checkUserMobile:'checkUserMobile',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtnTenat").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This mobile number is Already Used.'
              //   });
              swal("This mobile number is Already Used.", {
                            icon: "error",
                          });

           } else {
               document.getElementById("socAddBtnTenat").disabled=false;
           }
        }
      });
  }
}



function checkEmailUser() {
  var userEmail= $('#userEmail').val();
    if (userEmail!='') {
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {userEmail : userEmail,checkEmailMobile:'checkEmailMobile',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This Email is Already Used.'
              //   });
                  
               swal("This Email is Already Used.", {
                            icon: "error",
                          });
           } else {
               document.getElementById("socAddBtn").disabled=false;
           }
        }
      });
    } else {
      document.getElementById("socAddBtn").disabled=false;
    }
}

function checkEmailUserTenant() {
  var userEmail= $('#userEmailTenant').val();
    if (userEmail!='') {
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {userEmail : userEmail,checkEmailMobile:'checkEmailMobile',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtnTenat").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This Email is Already Used.'
              //   });
              
               swal("This Email is Already Used.", {
                            icon: "error",
                          });
           } else {
               document.getElementById("socAddBtnTenat").disabled=false;
           }
        }
      });
    } else {
       document.getElementById("socAddBtnTenat").disabled=false;
    }
}

function checkMobileUser1() {
  var userMobile= $('#userMobile1').val();
  
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {userMobile : userMobile,checkUserMobile:'checkUserMobile',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn1").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This mobile number is Already Used.'
              //   });
                  
                 swal("This mobile number is Already Used.", {
                            icon: "error",
                          });
           } else {
               document.getElementById("socAddBtn1").disabled=false;
           }
        }
      });
}



function checkEmailUser1() {
  var userEmail= $('#userEmail1').val();
 
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {userEmail : userEmail,checkEmailMobile:'checkEmailMobile',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn1").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This Email is Already Used.'
              //   });
              
              swal("This Email is Already Used.", {
                            icon: "error",
                          });
           } else {
               document.getElementById("socAddBtn1").disabled=false;
           }
        }
      });
}


function checkMobileSocietyEdit() {

  var secretary_mobile= $('#secretary_mobile').val();
  var secretary_mobile_old= $('#secretary_mobile_old').val();
  if (secretary_mobile_old!=secretary_mobile) {
    // alert("call");
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {secretary_mobile : secretary_mobile,secretary_mobile_old:secretary_mobile_old,checkSocietyMobileEdit:'checkSocietyMobileEdit',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This Mobile Number  is Already Used.'
              //   });
              
              swal("This Mobile Number is Already Used.", {
                            icon: "error",
                          });
           } else {
               document.getElementById("socAddBtn").disabled=false;
           }
        }
      });
    } else {
         document.getElementById("socAddBtn").disabled=false;
    }
}

function checkemailSociety() {
  var secretary_email= $('#secretary_email').val();
  
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {secretary_email : secretary_email,checkSocietyEmail:'checkSocietyEmail',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'Email already used'
              //   });
                
                swal("This Email is Already Used.", {
                            icon: "error",
                          });
           } else {
               document.getElementById("socAddBtn").disabled=false;
           }
        }
      });
}


function checkemailSocietyEdit() {

  var secretary_email= $('#secretary_email').val();
  var secretary_email_old= $('#secretary_email_old').val();
  if (secretary_email_old!=secretary_email) {
    // alert("call");
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {secretary_email : secretary_email,secretary_email_old:secretary_email_old,checkSocietyEmailEdit:'checkSocietyEmailEdit',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'Email already used'
              //   });

              swal("This Email is Already Used.", {
                            icon: "error",
                          });
                  
           } else {
               document.getElementById("socAddBtn").disabled=false;
           }
        }
      });
    } else {
         document.getElementById("socAddBtn").disabled=false;
    }
}


function checkMobileEmp() {
  var emp_mobile= $('#empNumber').val();
  
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {emp_mobile : emp_mobile,checkUserEmp:'checkUserEmp',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This mobile number is Already Used.'
              //   });
              
              swal("This Mobile Number is Already Used.", {
                            icon: "error",
                          });
           } else {
               document.getElementById("socAddBtn").disabled=false;
           }
        }
      });
}


function checkMobileEmpEdit() {
  var emp_mobile= $('#empNumber').val();
  var empNumberOld= $('#empNumberOld').val();
  if (empNumberOld!=emp_mobile) {
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {emp_mobile : emp_mobile,checkUserEmp:'checkUserEmp',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This mobile number is Already Used.'
              //   });
                
                swal("This Mobile Number is Already Used.", {
                            icon: "error",
                          });
           } else {
               document.getElementById("socAddBtn").disabled=false;
           }
        }
      });
    }
}



function checkMobileUserEdit() {
  var userMobile= $('#userMobile').val();
  var userMobileOld= $('#userMobileOld').val();
  if (userMobile!='') {
    if (userMobile!=userMobileOld) {
     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {userMobile : userMobile,checkUserMobile:'checkUserMobile',csrf:csrf},
        success: function(response){
           if (response==1) {
                document.getElementById("socAddBtn").disabled=true;
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This mobile number is Already Used.'
              //   });

            swal("This Mobile Number is Already Used.", {
                            icon: "error",
                          });
                  
           } else {
               document.getElementById("socAddBtn").disabled=false;
           }
        }
      });
    } else {
    document.getElementById("socAddBtn").disabled=false;
    }
  } else {
    document.getElementById("socAddBtn").disabled=false;
  }
}



function checkEmailUserEdit() {
  var userEmail= $('#userEmail').val();
  var userEmailOld= $('#userEmailOld').val();
  if (userEmail!='') {
    if (userEmail!=userEmailOld) {
       $.ajax({
          url: "controller/uniqueController.php",
          cache: false,
          type: "POST",
          data: {userEmail : userEmail,checkEmailMobile:'checkEmailMobile',csrf:csrf},
          success: function(response){
             if (response==1) {
                  document.getElementById("socAddBtn").disabled=true;
                // Lobibox.notify('error', {
                //   pauseDelayOnHover: true,
                //   continueDelayOnInactiveTab: false,
                //   position: 'top right',
                //   icon: 'fa fa-times-circle',
                //   msg: 'This Email is Already Used.'
                //   });

                swal("This Email is Already Used.", {
                            icon: "error",
                          });
                    
             } else {
                 document.getElementById("socAddBtn").disabled=false;
             }
          }
        });
      }
     } else {
       document.getElementById("socAddBtn").disabled=false;
    }
}


 function changePlan(society_id) {
   $.ajax({
      url: "getPlan.php",
      cache: false,
      type: "POST",
      data: {society_id : society_id,csrf:csrf},
      success: function(response){
          document.getElementById("planFormRes").innerHTML=response;
      }
    });


}


function  getStates() {
  var country_id = $("#country_id").val();
  $.ajax({
        url: "getStates.php",
        cache: false,
        type: "POST",
        data: {country_id : country_id,getStates:'getStates',csrf:csrf},
        success: function(response){
            $('#state_id').html(response);
          
            
        }
     });
}

function  getCity() {
  var state_id = $("#state_id").val();
  $.ajax({
        url: "getCities.php",
        cache: false,
        type: "POST",
        data: {state_id : state_id,getCity:'getCity',csrf:csrf},
        success: function(response){
            $('#city_id').html(response);
          
            
        }
     });
}

function  getSubCategorySp() {
  var local_service_master_id = $("#local_service_master_id").val();
  $.ajax({
        url: "getSubCategory.php",
        cache: false,
        type: "POST",
        data: {local_service_master_id : local_service_master_id,getSubCategory:'getSubCategory',csrf:csrf},
        success: function(response){
            $('#local_service_provider_sub_id').html(response);
          
            
        }
     });
}


function deletePost(feed_id) {
    // alert(block_id);
    swal({
            title: "Are you sure to Delete this Post ?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/newsFeedController.php",
                cache: false,
                type: "POST",
                data: {feed_id_delete : feed_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                    
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  } else {
                         swal("Success ! Your data has been deleted!", {
                            icon: "error",
                          });
                  }
                }
              });
             
            } else {
              //swal("Your data is safe!");
            }
          });

}


 function getEmerEditNumber(emergency_id) {
 
  $.ajax({
    url: 'editEmergencyNumber.php',
    cache: false,
    data: {emergency_id : emergency_id,editEmer:'editEmer',csrf:csrf},
    type: "post",
    success: function(response) {
          $('#editemerDiv').html(response);
    },
    error: function(data) {
    }
  });
}

function deleteComment(comments_id) {
    // alert(block_id);
    swal({
            title: "Are you sure delete this Comment ?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/newsFeedController.php",
                cache: false,
                type: "POST",
                data: {comments_id_delete : comments_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                    
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  } else {
                         swal("Success ! Your data has been deleted!", {
                            icon: "error",
                          });
                  }
                }
              });
             
            } else {
              //swal("Your data is safe!");
            }
          });

}



function deleteSpCategory(local_service_provider_id) {
    // alert(block_id);
    swal({
            title: "Are you sure to Delete this Category ?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/serviceProviderController.php",
                cache: false,
                type: "POST",
                data: {local_service_provider_id_delete : local_service_provider_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                    
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  }
                }
              });
             
            } else {
            }
          });

}

function deleteSpSubCategory(local_service_provider_sub_id) {
    // alert(block_id);
    swal({
            title: "Are you sure to Delete this Sub Category ?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/serviceProviderController.php",
                cache: false,
                type: "POST",
                data: {local_service_provider_sub_id_delete : local_service_provider_sub_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  } 
                }
              });
             
            } else {
            }
          });

}


function editCategory(local_service_provider_id,service_provider_category_name,service_provider_category_image) {
   $("#local_service_provider_id").val(local_service_provider_id);
   $("#service_provider_category_name").val(service_provider_category_name);
   $("#service_provider_category_image").val(service_provider_category_image);
    

}

function editSubCategory(local_service_provider_sub_id,service_provider_sub_category_name,service_provider_sub_category_image) {
   $("#local_service_provider_sub_id").val(local_service_provider_sub_id);
   $("#service_provider_sub_category_name").val(service_provider_sub_category_name);
   $("#service_provider_sub_category_image").val(service_provider_sub_category_image);
    

}



function  getCategorySp() {

  $.ajax({
        url: "https://master.my-company.app/main_api/local_service_provider_controller.php",
        cache: false,
        type: "POST",
         dataType: 'JSON',
        data: {getLocalServiceProviders : 'getLocalServiceProviders',csrf:csrf},
        success: function(response){
            
          $.each(response.local_service_provider, function (key, value) {
              $("#dropDownDest").append($('<option></option>').val(value.local_service_provider_id+'~'+value.service_provider_category_name).html(value.service_provider_category_name));
          });

          $('#dropDownDest').change(function () {
              // alert($(this).val());
              //Code to select image based on selected car id
          })
            
        }
     });
}

function showError(msg) {
  swal(msg, {
    icon: "warning",
  });
}
$('.eventPrice').hide();

$('input[type=radio][name=event_type]').change(function() {
    if (this.value == '1') {
      alert('asfdf');
      $('.eventPrice').show();

    }
    else if (this.value == '0') {
      $('.eventPrice').hide();
      alert("Asdff");
      
    }
});

function hideData() {
  var empType = $('.employment_type').val();
  if (empType=='Unemployed' || empType=='Student' || empType=='Others' || empType=='Homemaker') {
    $('.proExtDiv').hide();
  } else {
    $('.proExtDiv').show();
  }

}

 $(".onlyNumber,#secretary_mobile,#trlDays,#emp_sallary,#no_of_option,#month_working_days,#working_days,#leave_days,#no_of_person,#no_of_month,#expAmoint,#no_of_unit_bill,#no_of_unit,#no_of_blocks,#no_of_floor,#emrNumber,#userMobile,#ownerMobile,#empNumber,#cMobile,#editMobile1,#noofCar,#noofBike,#person_limit_day,#person_limit").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

//IS_250 21FEB2020
$("#no_of_person,#no_of_month").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    //110 for dot(.)
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress

    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

 

//IS_250 21FEB2020


//24feb2020 IS_387
function hideNewsFeedInfo(id){
  if($("#collapse"+id).hasClass("show")){
    $("#showNewsFeedIfo"+id).css("display","block");
  } else {
    //20220728
    $.ajax({
        url: "getTimeline.php",
        cache: false,
        type: "POST",
        data: {feed_id : id,getComment:"yes",csrf:csrf},
        success: function(response){
          $(".collapse_"+id).html(response);
            
        }
    });
    //20220728
    $("#showNewsFeedIfo"+id).css("display","none");
  }
}
//24feb2020 IS_387

//28feb2020 IS_837
function resetFrm(id){
  $('#'+id).validate().resetForm();
  
}

//28feb2020 IS_837

//2march2020
/*//28feb2020-new  IS_853
 $('input').change(function(){
        this.value = $.trim(this.value);
    });

     $('textarea').change(function(){
        this.value = $.trim(this.value);
    });
//28feb2020-new  IS_853*/
//2march2020


$('.form-btn').on('click',function(e){
    e.preventDefault();
    var form = $(this).parents('form');
     swal({
        title: "Are you sure?",
        text: "",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ['Cancel', 'Yes, I am sure !'],
      })
     .then((willDelete) => {
        if (willDelete) {
          form.submit();
        }
      });

});

if (window.innerWidth < 720) {

  $('.content-wrapper').click(function(e) {
     if($("#wrapper").hasClass("toggled")){
        $('#wrapper').removeClass("toggled"); 
           e.preventDefault();
        } 
  });
   $('.toggle-menu').click(function(event){
       event.stopPropagation();
   });

}



function  getSocietyData(society_id) {
 
  $.ajax({
        url: "controller/cronGetData.php",
        cache: false,
        type: "POST",
        data: {society_id : society_id,csrf:csrf},
        success: function(response){
            $('#BlockResp').html(response);
            
        }
     });
}

function getPenalty(penalty_id) {
      // alert(receive_bill_id);
      $.ajax({
      url: "getPenalty.php",
      cache: false,
      type: "POST",
      data: {penalty_id : penalty_id,csrf:csrf},
      success: function(response){
      $('#billPayDiv').html(response);
      
      
      }
      });
}

function getPenaltyRequest(penalty_id) {
      // alert(receive_bill_id);
      $.ajax({
      url: "getPenaltyRequest.php",
      cache: false,
      type: "POST",
      data: {penalty_id : penalty_id,csrf:csrf},
      success: function(response){
      $('#billPayDiv').html(response);
      
      
      }
      });
}

function mainPayRequest(receive_maintenance_id,mId,unit_id,bId) {
    $.ajax({
      url: "getPaymentRequest.php",
      cache: false,
      type: "POST",
      data: {receive_maintenance_id : receive_maintenance_id,mId:mId,unit_id:unit_id,bId:bId,csrf:csrf},
      success: function(response){
        $('#paidReqDiv').html(response);
      }
    });
  }

 function generateBIllRequestNew(receive_bill_id,block_id,balancesheet_id) {
    $.ajax({
      url: "getPayBillRequest.php",
      cache: false,
      type: "POST",
      data: {receive_bill_id : receive_bill_id,block_id:block_id,balancesheet_id:balancesheet_id},
      success: function(response){
        $('#billPayDiv').html(response);


      }
    });
  }

//IS_605 26feb2020
function getPenaltyEdit(penalty_id) {
      // alert(receive_bill_id);
      $.ajax({
      url: "getPenalty.php",
      cache: false,
      type: "POST",
      data: {penalty_id : penalty_id,EditFlg:"Yes",csrf:csrf},
      success: function(response){
      $('#EditPanDiv').html(response);
      
      
      }
      });
}
//IS_605 26feb2020


//12march2020
function getCommEntry(common_entry_id) {
  $.ajax({
 url: "getCommEntry.php",
 cache: false,
 type: "POST",
 data: {common_entry_id : common_entry_id,csrf:csrf},
 success: function(response){
 $('#editCMDiv').html(response);
 
 
 }
 });
}
//12march2020



//13march2020
function editMainType(visitor_main_type_id) {
  $.ajax({
 url: "getVisitorDetails.php",
 cache: false,
 type: "POST",
 data: {visitor_main_type_id : visitor_main_type_id,csrf:csrf},
 success: function(response){
 $('#updateModalDiv').html(response);
 
 
 }
 });
}

function editSubcategory(visitor_sub_type_id) {
  $.ajax({
 url: "getVisitorDetails.php",
 cache: false,
 type: "POST",
 data: {visitor_sub_type_id : visitor_sub_type_id,csrf:csrf},
 success: function(response){
 $('#updateSubModalDiv').html(response);
 
 
 }
 });
}
function DeleteVisitorMain(visitor_main_type_id) {
 

    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              // $('form.deleteForm'+id).submit();

 
        $.ajax({
            url: 'controller/addMainTypeVisitorsController.php',
            cache: false,
            type: "POST",
            data: {visitor_main_type_id : visitor_main_type_id,deleteData:'deleteData',csrf:csrf  },
            success: function(response){
              
               if(response == 1217) {

                  swal("Not Deleted", "Visitor sub type has records with this Main Type, Please Delete Those records before you delete main visitor type...!", "error"); 
                } else if(response==1) {
                
                swal("Success ! Your data has been deleted!", {
                      icon: "success",
                    });
               location.reload();
              } else {
                    swal("Success ! Your data has been deleted!", {
                      icon: "error",
                    });
              }
            }
          });
            } else {
              swal("Your data is safe!");
            }
          });
      
}

//13march2020

function deleteClassifiedCategory(classified_cat_id) {
    // alert(block_id);
    swal({
            title: "Are you sure to Delete this Category ?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/classifiedController.php",
                cache: false,
                type: "POST",
                data: {classified_cat_id_delete : classified_cat_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  }
                }
              });
            } else {
            }
          });
}
function deleteClassifiedSubCategory(classified_sub_id) {
    // alert(block_id);
    swal({
            title: "Are you sure to Delete this Sub Category ?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // $('form.deleteForm'+id).submit();
                $.ajax({
                url: "controller/classifiedController.php",
                cache: false,
                type: "POST",
                data: {classified_sub_id_delete : classified_sub_id,csrf:csrf},
                success: function(response){
                  if(response==1) {
                         swal("Success ! Your data has been deleted!", {
                            icon: "success",
                          });
                    document.location.reload(true);
                  } 
                }
              });
            } else {
            }
          });
}


function editinstallationteam(societyid,memberid,membername,membermobile){
    $('#member_society_id option[value='+societyid+']').attr('selected','selected');
    $('#member_id').val(memberid);
    $('#member_full_name').val(membername);
    $('#member_mobile_number').val(membermobile);
  }
  


$(".docOnly").change(function () {
    var fileExtension = ['jpeg', 'jpg', 'png', 'doc','docx', 'pdf', 'csv', 'xls' , 'xlsx','ods','xlsb'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
        swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
        $('.docOnly').val('');
    }
});

$(".pdfOnly").change(function () {
    var fileExtension = ['pdf'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
        swal("Only PDF formats are allowed", {icon: "error", });
        $('.docOnly').val('');
    }
});

$(".idProof").change(function () {
     var fileExtension = ['jpeg', 'jpg', 'png', 'doc','docx', 'pdf'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
        swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
        $('.idProof').val('');
    }
});

$(".photoOnly").change(function () {
     // alert(this.files[0].size);
     var fileExtension = ['jpeg', 'jpg', 'png','webp','HEIC'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        // alert("Only formats are allowed : "+fileExtension.join(', '));
        swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
        $('.photoOnly').val('');
    }
});

$(".imageOnly").change(function () {
     var fileExtension = ['jpeg', 'jpg', 'png','webp','HEIC'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
        $('.photoOnly').val('');
    }
});

$(".mp3Only").change(function () {
  // alert(this.files[0].size);
  var fileExtension = ['mp3','wav','flac','ogg','m3u','acc','wma','midi','aif','m4a','mpa','pls'];
 if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
     // alert("Only formats are allowed : "+fileExtension.join(', '));
     swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
     $('.mp3Only').val('');
 }
});

$('.csvupload').change(function(){
      var fileExtension = ['csv'];
      if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
          // alert("Only formats are allowed : "+fileExtension.join(', '));
          swal("Only formats are allowed : "+fileExtension.join(', '), {icon: "error", });
          $('.csvupload').val('');
        }
      });
      

$(".alphanumeric").keypress(function (e) {
  var keyCode = e.which;
  if ( !( (keyCode >= 48 && keyCode <= 57) 
   ||(keyCode >= 65 && keyCode <= 90) 
   || (keyCode >= 97 && keyCode <= 122) ) 
   && keyCode != 8 && keyCode != 32) {
   e.preventDefault();
  }
});


//14march2020
/*$('.txtNumeric').keydown(function (e) { //alert();
   
  var key = e.keyCode;
  
    if (!(  (key == 8) ||   (key == 189) ||  (key == 32)  || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) )) {
    e.preventDefault();
    } 
});*/
$('.txtNumeric').keypress(function(event)
{
    var kcode = event.keyCode;

    if (kcode == 8 ||
        kcode == 9 ||
       
        kcode == 95 ||
         kcode == 45 ||
         kcode == 32 ||
        (kcode > 47 && kcode < 58) ||
        (kcode > 64 && kcode < 91) ||
        (kcode > 96 && kcode < 123))
    {
        return true;
    }
    else
    {
        return false;
    }
});
// $('.txtNumeric').bind("cut copy paste",function(e) {
//      e.preventDefault();
//  });
//14march2020

$('.number').keypress(function(event) {
  if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});



$('.space').on('keypress', function(e) {
    if (e.which == 32){
        return false;
    }
});


function getAccessMenuDetail(app_menu_id) {
  $.ajax({
 url: "getAccessMenuDetail.php",
 cache: false,
 type: "POST",
 data: {app_menu_id : app_menu_id,csrf:csrf},
 success: function(response){
 $('#editAMDiv').html(response);
 
 
 }
 });
}

 
// $('.noticeBoardMultiSelectCls').on('change', function(){  

  // const selected = $('.noticeBoardMultiSelectCls :selected').map(function() {return $(this).val()}).get()
  
  // if (selected.includes('0')) {
  //   $('option:not(:contains("0"))').prop('selected', false);
  //   $('option:contains(All Block)').prop('selected', true);
  // } 
// })

$('.noticeBoardMultiSelectCls').change(function() {    
    var item=$(this);
    var abc= item.val();
    if (abc==0) {
      
    } else {
     var x = abc+' ';
      if(x.charAt(0)=='0'){
        // alert("all select");
        swal("if you choose custom branch all branch auto removed");
        $('option:contains(All Blocks)').prop("selected", false);
      }else  {

      }
    }
});

$('.noticeBoardZoneMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      // alert("all select");
      swal("if you choose any zone, all zone auto removed");
      $('option:contains(All Zone)').prop("selected", false);
    }else  {

    }
  }
});

$('.noticeBoardLevelMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      // alert("all select");
      swal("if you choose any level, all employee level auto removed");
      $('option:contains(All Employee Level)').prop("selected", false);
    }else  {

    }
  }
});

$('.accessForEmployeeMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      // alert("all select");
      swal("if you want to all employee, please remove selected employee!");
      $('option:contains(All Employee)').prop("selected", false);
    }else  {

    }
  }
});

$('.accessForDepartmentMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      // alert("all select");
      swal("if you want to all department, please removed selected department!");
      $('option:contains(All Department)').prop("selected", false);
    }else  {

    }
  }
});

$('.salaryValueDepartmentMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      // alert("all select");
      swal("if you want to all department, please removed selected department!");
      $('option:contains(All Department)').prop("selected", false);
    }else  {

    }
  }
});

//18march2020
 function checkMobileUserDailyVisitor() {

  
  var userMobile= $('#visitor_mobile').val();
   if (userMobile.length>0) {

     $.ajax({
        url: "controller/uniqueController.php",
        cache: false,
        type: "POST",
        data: {userMobile : userMobile,checkDailyUserMobile:'checkDailyUserMobile',csrf:csrf},
        success: function(response){
           if (response==1) {
            $(':input[type="submit"]').prop('disabled', true);
                
              // Lobibox.notify('error', {
              //   pauseDelayOnHover: true,
              //   continueDelayOnInactiveTab: false,
              //   position: 'top right',
              //   icon: 'fa fa-times-circle',
              //   msg: 'This mobile number is Already Used.'
              //   });

              swal("This Mobile Number is Already Used.", {
                            icon: "error",
                          });
                  
           } else {
               $(':input[type="submit"]').prop('disabled', false);
           }
        }
      });
  }
}


function getDailyVisitorEdit(visitor_id) {
 
        
      $.ajax({
      url: "getDailyVisitor.php",
      cache: false,
      type: "POST",
      data: {visitor_id : visitor_id,EditFlg:"Yes",csrf:csrf},
      success: function(response){
        
      $('#updateDailyVisitorModalDiv').html(response);
      
      
      }
      });
}


//IS_1498 20march2020
function getEmpUnitEdit(unit_id,user_id) {
   var emp_id = $('#emp_id').val(); 
     $.ajax({
      url: "getSlotDetail.php",
      cache: false,
      type: "POST",
      data: {emp_id:emp_id, unit_id : unit_id,user_id:user_id,EditFlg:"Yes",csrf:csrf},
      success: function(response){ 
      $('#updateTimeUnitModalDiv').html(response);
      
      
      }
      });
}
//IS_1498 20march2020


//IS_1498 20march2020
function getNotificationData(unit_id,user_id) {
   var emp_id = $('#emp_id').val(); 
     $.ajax({
      url: "getSlotNotificationUser.php",
      cache: false,
      type: "POST",
      data: {emp_id:emp_id, unit_id : unit_id,user_id:user_id,EditFlg:"Yes",csrf:csrf},
      success: function(response){ 
      $('#NotificationListDiv').html(response);
      
      
      }
      });
}

function getNotificationDataDaily(unit_id,user_id,daily_visitor_id) {
     $.ajax({
      url: "getSlotNotificationUserDaily.php",
      cache: false,
      type: "POST",
      data: {daily_visitor_id:daily_visitor_id, unit_id : unit_id,user_id:user_id,EditFlg:"Yes",csrf:csrf},
      success: function(response){ 
      $('#NotificationListDiv').html(response);
      
      
      }
      });
}

function removeSingleUser(unit_id,user_id,emp_id) {
  $.ajax({
      url: "controller/employeeController.php",
      cache: false,
      type: "POST",
      data: {emp_id:emp_id, unit_id : unit_id,user_id:user_id,deleteSingleUser:"deleteSingleUser",csrf:csrf},
      success: function(response){ 
      // $('#NotificationListDiv').html(response);
        if (response==0) {
          getNotificationData(unit_id,user_id);
        }
      
      }
      });
}

function removeSingleUserDailyVisitor(unit_id,user_id,daily_visitor_id) {
  $.ajax({
      url: "controller/dailyVisitorController.php",
      cache: false,
      type: "POST",
      data: {daily_visitor_id:daily_visitor_id, unit_id : unit_id,user_id:user_id,deleteSingleUser:"deleteSingleUser",csrf:csrf},
      success: function(response){ 
      // $('#NotificationListDiv').html(response);
        if (response==0) {
          getNotificationDataDaily(unit_id,user_id,daily_visitor_id);
        }
      
      }
      });
}


function muteResourceNotification(unit_id,user_id,emp_id) {
  $.ajax({
      url: "controller/employeeController.php",
      cache: false,
      type: "POST",
      data: {emp_id:emp_id, unit_id : unit_id,user_id:user_id,muteResourceNotification:"muteResourceNotification",csrf:csrf},
      success: function(response){ 
      // $('#NotificationListDiv').html(response);
        if (response==0) {
          getNotificationData(unit_id,user_id);
        }
      
      }
      });
}

function unmuteResourceNotification(unit_id,user_id,emp_id) {
  $.ajax({
      url: "controller/employeeController.php",
      cache: false,
      type: "POST",
      data: {emp_id:emp_id, unit_id : unit_id,user_id:user_id,unmuteResourceNotification:"unmuteResourceNotification",csrf:csrf},
      success: function(response){ 
      // $('#NotificationListDiv').html(response);
        if (response==0) {
          getNotificationData(unit_id,user_id);
        }
      
      }
      });
}


function getEmpNotification(emp_id) {
  $.ajax({
      url: "controller/employeeController.php",
      cache: false,
      type: "POST",
      data: {emp_id:emp_id,getEmpNotification:"getEmpNotification",csrf:csrf},
      success: function(response){ 
        $('.empSpan'+emp_id).html(response);
        swal("Success ! In/Out Notification Enable !", {icon: "success",});
      
      }
      });
}

function notGetEmpNotification(emp_id) {
  $.ajax({
      url: "controller/employeeController.php",
      cache: false,
      type: "POST",
      data: {emp_id:emp_id,NotgetEmpNotification:"NotgetEmpNotification",csrf:csrf},
      success: function(response){ 
        $('.empSpan'+emp_id).html(response);
        swal("Success ! In/Out Notification Disabled !", {icon: "success",});
      }
      });
}

function getEmpNotificationDaily(emp_id) {
  $.ajax({
      url: "controller/dailyVisitorController.php",
      cache: false,
      type: "POST",
      data: {emp_id:emp_id,getEmpNotification:"getEmpNotification",csrf:csrf},
      success: function(response){ 
        $('.empSpan'+emp_id).html(response);
        swal("Success ! In/Out Notification Enable !", {icon: "success",});
      
      }
      });
}

function notGetEmpNotificationDaily(emp_id) {
  $.ajax({
      url: "controller/dailyVisitorController.php",
      cache: false,
      type: "POST",
      data: {emp_id:emp_id,NotgetEmpNotification:"NotgetEmpNotification",csrf:csrf},
      success: function(response){ 
        $('.empSpan'+emp_id).html(response);
        swal("Success ! In/Out Notification Disabled !", {icon: "success",});
      }
      });
}



function muteDailyVisitorNotification(unit_id,user_id,daily_visitor_id) {
  $.ajax({
      url: "controller/dailyVisitorController.php",
      cache: false,
      type: "POST",
      data: {daily_visitor_id:daily_visitor_id, unit_id : unit_id,user_id:user_id,muteDailyVisitorNotification:"muteDailyVisitorNotification",csrf:csrf},
      success: function(response){ 
      // $('#NotificationListDiv').html(response);
        if (response==0) {
          getNotificationDataDaily(unit_id,user_id,daily_visitor_id);
        }
      
      }
      });
}

function unmuteDailyVisitoreNotification(unit_id,user_id,daily_visitor_id) {
  $.ajax({
      url: "controller/dailyVisitorController.php",
      cache: false,
      type: "POST",
      data: {daily_visitor_id:daily_visitor_id, unit_id : unit_id,user_id:user_id,unmuteDailyVisitoreNotification:"unmuteDailyVisitoreNotification",csrf:csrf},
      success: function(response){ 
      // $('#NotificationListDiv').html(response);
        if (response==0) {
          getNotificationDataDaily(unit_id,user_id,daily_visitor_id);
        }
      
      }
      });
}

//IS_1498 20march2020
$("#user_data").change(function () {
        var end = this.value;
        var firstDropVal = $('#user_data').val();
        var detail = firstDropVal.split('~');
        var unit_id = detail[0];
        var user_id = detail[1];
        var emp_id = $('#emp_id').val();
        $.ajax({
            url: 'getSlotDetail.php',
            type: 'POST',
             data: {emp_id:emp_id, unit_id : unit_id, user_id:user_id },
            success: function(data) {
                $('#time_slot_detail_div').html(data); 
            } 
        });
  });
//IS_1498 20march2020


function getslabEdit(slab_id) {
      $.ajax({
      url: "getSlabData.php",
      cache: false,
      type: "POST",
      data: {slab_id : slab_id,EditFlg:"Yes",csrf:csrf},
      success: function(response){
      $('#editGSTModalDiv').html(response);
      
      
      }
      });
}

function getGSTDetails(){
    if($('#penaltyEdit :input[name="is_taxble"]').val()=="1"){
      $('#gst_detail_div_edit').css('display','block');
    } else {
      $('#gst_detail_div_edit').css('display','none');
    }
  }


function showPaidInfo(flg,id){
  if (flg == 'paid') {
     $.ajax({ 
      url: "getEventData.php",
      cache: false,
      type: "POST",
      data: {id : id,paidinfo:"yes",csrf:csrf},
      success: function(response){
        $('.eventPrice-days'+id).css("display", "block");
        var abc= $(':radio[value="1"]:checked').length;
        if (abc>0) {
          $('#billTypeLble').show();
          $('#is_taxble').show();
          $('#bill_type').prop('selectedIndex', 0);
        }else {
          $('#billTypeLble').hide();
          $('#is_taxble').hide();
          $('#gst_detail_div').hide();
        }
      //$('#event_days_detail').html(response);
      $('.eventPrice-days'+id).html(response);
      
      }
      });


      

    }
    else if (flg == 'Unpaid')  {
      $('.eventPrice-days'+id).html('');
      var abc= $(':radio[value="1"]:checked').length;
      if (abc>0) {
          $('#billTypeLble').show();
          $('#is_taxble').show();
          $('#bill_type').prop('selectedIndex', 0);
          $('#gst_detail_div').hide();
          
        }else {
          $('#billTypeLble').hide();
          $('#is_taxble').hide();
          $('#gst_detail_div').hide();
        }
    }
 }
  
$('#total_days').change(function() {
  var total_days = $('#total_days').val();
  //var event_end_time = $('input[type=text][name=event_end_time]').val();
  if(total_days !=""){
   $.ajax({ 
      url: "getEventData.php",
      cache: false,
      type: "POST",
      data: {total_days : total_days,csrf:csrf},
      success: function(response){
      $('#event_days_detail').html(response);
      $('#billTypeLble').hide();
      $('#bill_type').hide();
      $('#is_taxble').hide();
      $('#gst_detail_div').hide();
      
      
      }
      });
  }
});
//27march2020 new


//28march2020
 
$('#type').on('change', function() {
    if(this.value=="Block" || this.value=="BalanceSheet" ){
      var type = this.value;
       $.ajax({ 
      url: "getPaymentTypeData.php",
      cache: false,
      type: "POST",
      data: {type : type,csrf:csrf},
      success: function(response){
      

      if(type=="Block"){
        $('#block_id').html(response);
        $('#payment_type_block_details').css('display','block');
        $('#payment_type_balancesheet_details').css('display','none');

      } else {
        $('#balancesheet_id').html(response);
        $('#payment_type_balancesheet_details').css('display','block');
         $('#payment_type_block_details').css('display','none');
      }
      
      
      }
      });

      
    } else {
      $('#payment_type_balancesheet_details').css('display','none');
      $('#payment_type_block_details').css('display','none');
       
    }
    
});

 function getPaymentInfo(society_payment_getway_id){
   $.ajax({ 
      url: "getPaymentTypeData.php",
      cache: false,
      type: "POST",
      data: {society_payment_getway_id : society_payment_getway_id,csrf:csrf, getInfo:"yes"},
      success: function(response){
       $('#infoModalDiv').html(response);
         
      
      
      }
      });
 }
//28march2020


//30march2020 , #sos_duration IS_1751
$("#no_of_person,#no_of_month, #sos_duration").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    //110 for dot(.)
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress

    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});


//$('#events_day_id').on('change', function() {
function getBookEventDetail() {
  var events_day_id =  $('#events_day_id').val();
  var user_id = $('#user_id').val();
  if(user_id !== null && user_id !== '') {

       $.ajax({ 
      url: "getEventData.php",
      cache: false,
      type: "POST",
      data: {user_id:user_id,events_day_id : events_day_id,bookEventDetail:"Yes",csrf:csrf},
      success: function(response){
       $('#eventDetailsDiv').html(response);
       }
      });
  } else {
      $('#events_day_id').prop('selectedIndex', 0);
    swal("Please Select Booking For!");

  }

    
}

function resetBookingForm() {
  $('#events_day_id').prop('selectedIndex', 0);
  $('#eventDetailsDiv').html(" ");

}

function getAmountDetail(person='', child='', guest=''){
  if(person != '' && person != 0){
    var going_person= person;
    var going_child= child;
    var going_guest= guest;
  }else{
    var going_person= $('#going_person').val();
    var going_child= $('#going_child').val();
    var going_guest= $('#going_guest').val();
  }
  var events_day_id= $('#events_day_id').val();
  var user_id= $('#user_id').val();
  var event_id= $('#event_id').val();  
       $.ajax({ 
      url: "getEventData.php",
      cache: false,
      type: "POST",
      data: {user_id:user_id,going_person : going_person, going_child : going_child, going_guest : going_guest, events_day_id : events_day_id,event_id : event_id,calculatePrice:"Yes",csrf:csrf},
      success: function(response){
       $('#payementDetailsDiv').html(response);
       }
      });
}
$('#going_person, #going_child, #going_guest').on('change', function() {
  var going_person= $('#going_person').val();
  var going_child= $('#going_child').val();
  var going_guest= $('#going_guest').val();
  var events_day_id= $('#events_day_id').val();

  var going_person =$('#going_person').val();
  var going_child =$('#going_child').val();
  var going_guest =$('#going_guest').val();
  var totalGuests = parseInt(going_person)+parseInt(going_child)+parseInt(going_guest);

  $( "#eventPerson" ).val(totalGuests);
 
  var event_id= $('#event_id').val(); 
       $.ajax({ 
      url: "getEventData.php",
      cache: false,
      type: "POST",
      data: {going_person : going_person, going_child : going_child, going_guest : going_guest, events_day_id : events_day_id,event_id : event_id,calculatePrice:"Yes",csrf:csrf},
      success: function(response){
       $('#payementDetailsDiv').html(response);
       }
      });
});

//30march2020


function getPayAmount(receive_amount,event_attend_id,balancesheet_id,events_day_id,event_id) {
    $('#payEventAmountFrm :input[name="receive_amount"]').val(receive_amount);
    $('#payEventAmountFrm :input[name="event_attend_id"]').val(event_attend_id);
    $('#payEventAmountFrm :input[name="balancesheet_id"]').val(balancesheet_id);
    $('#payEventAmountFrm :input[name="events_day_id"]').val(events_day_id);
    $('#payEventAmountFrm :input[name="event_id"]').val(event_id);
    document.getElementById('PaybleAmount').innerHTML=receive_amount; 
  }



  function getBankDetails(value){

    if(value != ''){
      $.ajax({
        url: 'ajaxPaymentData.php',
        type: 'POST',
        data: {payment_type_value:value},
      })
      .done(function(response) {
        $('#addBankDetails').html(response);
        $('#isCash').val('1');

      });
    }else{
      $('#addBankDetails').html('');
      $('#isCash').val('');

    }
  }

  function getBankDetailsSalary(value){

    if(value != ''){
      $.ajax({
        url: 'ajaxPaymentDataSalary.php',
        type: 'POST',
        data: {payment_type_value:value},
      })
      .done(function(response) {
        $('#addBankDetails').html(response);
        $('#isCash').val('1');

      });
    }else{
      $('#addBankDetails').html('');
      $('#isCash').val('');

    }
  }
  function getBankDetailsEvent(value){

    if(value != ''){
      $.ajax({
        url: 'ajaxPaymentData.php',
        type: 'POST',
        data: {payment_type_value:value,eventBook:'eventBook'},
      })
      .done(function(response) {
        $('#addBankDetails').html(response);
        $('#isCash').val('1');

      });
    }else{
      $('#addBankDetails').html('');
      $('#isCash').val('');

    }
  }

   function getBankDetailsFacility(value){

    if(value != ''){
      $.ajax({
        url: 'ajaxPaymentDataFacility.php',
        type: 'POST',
        data: {payment_type_value:value},
      })
      .done(function(response) {
        $('#addBankDetails').html(response);
        $('#isCash').val('1');

      });
    }else{
      $('#addBankDetails').html('');
      $('#isCash').val('');

    }
  }


  function cancelEvent(id) {
    $.ajax({
        url: 'getCancelEventData.php',
        type: 'POST',
        data: {event_attend_id:id},
      })
      .done(function(response) {
        $('#cancelPaidEvent').html(response);

      });
  }

  function cancelFacility(id) {
    $.ajax({
        url: 'getCancelFacilityData.php',
        type: 'POST',
        data: {booking_id:id},
      })
      .done(function(response) {
        $('#cancelPaidEvent').html(response);

      });
  }


  function importquestion(room_id,que) {
    $.ajax({
        url: 'getHousieQuestions.php',
        type: 'POST',
        data: {room_id:room_id,que:que},
      })
      .done(function(response) {
        $('#queDiv').html(response);

      });
  }



function eidtMainParking (society_parking_id) {
    $('#society_parking_id_edit').val(society_parking_id);
    // $('#oldFloorname').val(floor_name);
    var conceptName = $('#society_parking_name_view').find(":selected").text();
    $('#socieaty_parking_name').val(conceptName);

}


//15july2020
$('input[name="maintenance_type_id"]').click(function(){

 var maintenance_type_id = $("input[name='maintenance_type_id']:checked").val();
 if(maintenance_type_id==0) {
  $('#auto_gen_date_div').css('display','block');
  $('#auto_gen_date_lbl').css('display','block');
} else {
 $('#auto_gen_date_div').css('display','none');
 $('#auto_gen_date_lbl').css('display','none');
}
});



$('#mentainance_price_type').on('change', function() {

 var amountType = $("input[name='amountType']:checked").val();
 var mentainance_price_type = $("#mentainance_price_type").val();
  // alert(mentainance_price_type);

if(amountType==1 && mentainance_price_type==1  ) {
  $('#amt_lbl').text('Monthly Amount'); 
} else if(amountType==1 && mentainance_price_type==0  ) {
  $('#amt_lbl').text('Fixed Amount'); 
}else if(amountType==0 && mentainance_price_type==1  ) {
  $('#amt_lbl').text('Monthly Amount'); 
} else   {
  $('#amt_lbl').text('Amount'); 
}


if(this.value=="0"){

 $('#month_div').css('display','none');
 $('#fixed_div').css('display','block');

} else if(this.value=="1"){ 
  $('#month_div').css('display','block');


  $('#fixed_div').css('display','none');

} else  { 

 $('#fixed_div').css('display','none');
 $('#month_div').css('display','none');

}

});


$('input[name="accept_custom_amount"]').click(function(){
 var accept_custom_amount = $("input[name='accept_custom_amount']:checked").val();
 if(accept_custom_amount==0) {

  $('#cut_per_lbl').css('display','none');
  $('#cut_per_div').css('display','none');


} else {
  $('#cut_per_lbl').css('display','block');
  $('#cut_per_div').css('display','block');

}

var late_fees_type = $("#late_fees_type").val();
var accept_custom_amount = $("input[name='accept_custom_amount']:checked").val();
var late_fee_applicable = $("input[name='late_fee_applicable']:checked").val();
if(late_fees_type!=0 && accept_custom_amount==1) {
  $('#late_fee_everytime').css('display','block');
}else if(late_fees_type==0 && accept_custom_amount==1 && late_fee_applicable==0) {
  $('#late_fee_everytime').css('display','block');
} else {
  $('#late_fee_everytime').css('display','none'); 
}



});



$('#late_fees_type').on('change', function() {

  var accept_custom_amount = $("input[name='accept_custom_amount']:checked").val();
  var amountType = $("input[name='amountType']:checked").val();
  var  sqftt ='';
  if(amountType == "1"){
    sqftt ='';
  }
  if(this.value=="1"){
    $('#late_fee_lbl').text('Late Fees Per Day'+sqftt); 
  }  else   if(this.value=="2"){
    $('#late_fee_lbl').text('Late Fees Per Month'+sqftt); 
  }    else { 
    $('#late_fee_lbl').text('Late Fees'); 
  }

});

$('input[name="late_fee_applicable"]').click(function(){

  var accept_custom_amount = $("input[name='accept_custom_amount']:checked").val();
  var amountType = $("input[name='amountType']:checked").val();

 var late_fee_applicable = $("input[name='late_fee_applicable']:checked").val();
 if(late_fee_applicable==0) {

  $('#late_fes_max_amount_div').css('display','block');
  $('#late_fee_type_cap').css('display','block');
  $('#late_fee_type_div').css('display','block');
  $('#late_fee_data_div').css('display','block');
  if (accept_custom_amount==1) {
    $('#late_fee_everytime').css('display','block'); 
  }
  if (amountType==1) {
    $('#LateFeesAmountTypeLbl').css('display','block'); 
    $('#LateFeesAmountTypeLblDiv').css('display','block'); 
  } else {
    $('#LateFeesAmountTypeLbl').css('display','none'); 
    $('#LateFeesAmountTypeLblDiv').css('display','none'); 
  }
} else {
  $('#late_fes_max_amount_div').css('display','none');
  $('#late_fee_type_cap').css('display','none');
  $('#late_fee_type_div').css('display','none');
  $('#late_fee_data_div').css('display','none'); 
  $('#late_fee_everytime').css('display','none'); 
  $('#LateFeesAmountTypeLbl').css('display','none'); 
  $('#LateFeesAmountTypeLblDiv').css('display','none'); 
}
});


$('#late_fees_type').change(function(){
  var late_fees_type = $("#late_fees_type").val();
  var accept_custom_amount = $("input[name='accept_custom_amount']:checked").val();
  if(accept_custom_amount==1) {
    $('#late_fee_everytime').css('display','block');
  } else {
    $('#late_fee_everytime').css('display','none'); 
  }
});



$('input[name="amountType"]').click(function(){

 var amountType = $("input[name='amountType']:checked").val();
 var mentainance_price_type = $("#mentainance_price_type").val();

 if (amountType==1) {
    $('#LateFeesAmountTypeLbl').css('display','block'); 
    $('#LateFeesAmountTypeLblDiv').css('display','block'); 
  } else {
    $('#LateFeesAmountTypeLbl').css('display','none'); 
    $('#LateFeesAmountTypeLblDiv').css('display','none'); 
  }


 if(amountType==1 && mentainance_price_type==1  ) {
  $('#amt_lbl').text('Monthly Amount Per Square Feet'); 
} else if(amountType==1 && mentainance_price_type==0  ) {
  $('#amt_lbl').text('Fixed Amount Per Square Feet'); 
}else if(amountType==0 && mentainance_price_type==1  ) {
  $('#amt_lbl').text('Monthly Amount'); 
} else   {
  $('#amt_lbl').text('Amount'); 
}

 var accept_custom_amount = $("input[name='accept_custom_amount']:checked").val();
  var late_fees_type = $("#late_fees_type").val();
   var amountType = $("input[name='amountType']:checked").val();
  var  sqftt ='';

  if(amountType == "1"){
    sqftt =' /Per Square Feet';
  }
  if(late_fees_type=="1"){
    $('#late_fee_lbl').text('Late Fees Per Day'+sqftt); 
  }  else   if(late_fees_type=="2"){
    $('#late_fee_lbl').text('Late Fees Per Month'+sqftt); 
  }    else { 
    $('#late_fee_lbl').text('Late Fees'); 
  }



});

function selUnsel(block_id){
 var isChecked = $("#selectSAll"+block_id).is(":checked");
 if (isChecked) {
   $('.selectSAll'+block_id).prop('checked', true);
 } else {
  $('.selectSAll'+block_id).prop('checked', false);
}


}

$('#month_from').on('change', function() {
  var month_from = $('#month_from').val();


  $.ajax({
    url: "ajaxMonthDetails.php",
    cache: false,
    type: "POST",
    data: {month_from : month_from},
    success: function(response){
     $('#year_from').html(response);
     $('#month_to').html('');
     $('#year_to').html('');
   }
 });

});

$('#year_from').on('change', function() {
  var year_from = $('#year_from').val();
  var month_from = $('#month_from').val();  

  $.ajax({
    url: "ajaxMonthDetails.php",
    cache: false,
    type: "POST",
    data: {year_from : year_from,month_from:month_from},
    success: function(response){
     $('#month_to').html(response);
     $('#year_to').html('');
   }
 });

});

$('input[name="parkingType"]').click(function(){
    var parkingType = $("input[name='parkingType']:checked").val();
    if (parkingType ==1) {
      $("#parkingNameDiv").hide();
    } else {
      $("#parkingNameDiv").show();
    }

});



$('#month_to').on('change', function() {
  var year_from = $('#year_from').val();
  var month_from = $('#month_from').val(); 
  var month_to = $('#month_to').val();  

  $.ajax({
    url: "ajaxMonthDetails.php",
    cache: false,
    type: "POST",
    data: {year_from : year_from,month_from:month_from,month_to:month_to},
    success: function(response){
     $('#year_to').html(response);
   }
 });

});

function men_edit(receive_maintenance_id,block_id){
 $.ajax({ 
  url: "ajaxEditMentainance.php",
  cache: false,
  type: "POST",
  data: {receive_maintenance_id : receive_maintenance_id,bId:block_id,csrf:csrf},
  success: function(response){
    $('#ediFormDetails').html(response);


  }
});
}

function men_history(receive_maintenance_id){
 $.ajax({ 
  url: "ajaxMenHistory.php",
  cache: false,
  type: "POST",
  data: {receive_maintenance_id : receive_maintenance_id,csrf:csrf},
  success: function(response){
    $('#payHistoryDetails').html(response);


  }
});
}
//15july2020


function autoNumberGenerate() {
  var emp_type_id =$('.emp_type_id').val();
  var emp_type_id11 =$('.emp_type_id11').val();


  if(emp_type_id == '' && emp_type_id11 == ''){
   swal("Error! Please Select Type First !", {icon: "error",});
    $('#empNumber').val('');
   
  } else if(emp_type_id == '1'){
   swal("Error! Security Guard Need Valid Phone Number !", {icon: "error",});
    $('#empNumber').val('');
  } else  {
    // alert(emp_type_id);
     $.ajax({ 
      url: "getUniqueNumber.php",
      cache: false,
      type: "POST",
      data: {emp_type_id : emp_type_id,csrf:csrf},
      success: function(response){
        $('#empNumber').val(response);
      }
    });
  }

}


 // $('.weekday').val(this.checked);

 //    $('.weekday').change(function() {
 //        if(this.checked) {
 //         $(':input[type="submit"]').prop('disabled', false);
 //        }
               
 //    });


function editExpenseCategory(argument) {
    $.ajax({
      url: 'editExpenseCategory.php',
      type: 'POST',
      data: {expense_category_id: argument},
    })
    .done(function(response) {
      $('#editcomplaintCatDetails').html(response);
    });
  }

function editExpense(argument) {
    $.ajax({
      url: 'editExpense.php',
      type: 'POST',
      data: {expenses_balance_sheet_id: argument},
    })
    .done(function(response) {
      $('#editcomplaintCatDetails').html(response);
    });
  }


function editIncome(argument) {
    $.ajax({
      url: 'editIncome.php',
      type: 'POST',
      data: {expenses_balance_sheet_id: argument},
    })
    .done(function(response) {
      $('#editcomplaintCatDetailsIncome').html(response);
    });
  }

function editSalary(argument) {
    $.ajax({
      url: 'editSalary.php',
      type: 'POST',
      data: {emp_salary_master: argument},
    })
    .done(function(response) {
      $('#editcomplaintCatDetailsSalary').html(response);
    });
  }

function getnewUnitlist() {
  var blockIds= $("#block_id").val();
  $.ajax({
      url: 'getUnitlistNoticeboard.php',
      type: 'POST',
      data: {blockIds: blockIds},
    })
    .done(function(response) {
      $('#unitList').html(response);
    });
}

function getnewFloorlist() {
  var blockIds= $("#block_id").val();
  $.ajax({
      url: 'getFloorlistNoticeboard.php',
      type: 'POST',
      data: {blockIds: blockIds},
    })
    .done(function(response) {
      $('#floorList').html(response);
    });
}

 function readURLProfile(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#profileView').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#imgInpProfile").change(function() {
  readURLProfile(this);
});


function readURLSoceityLogo(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#societyLogoView').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#soceityLogo").change(function() {
  readURLSoceityLogo(this);
});

function readURLSoceityLogoCover(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#societyCoverView').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}


function configurePaymentGateway(requiest_id) {
   $('#congigureDiv').html('');
  // var blockIds= $("#block_id").val();
  $.ajax({
      url: 'configurePaymentGateway.php',
      type: 'POST',
      data: {requiest_id: requiest_id},
    })
    .done(function(response) {
      $('#congigureDiv').html(response);
    });
}


function getQrBox() {
  var no_of_qr = $("#no_of_qr").val();

  $.ajax({
      url: 'getQrAddOption.php',
      type: 'POST',
      data: {no_of_qr: no_of_qr},
    })
    .done(function(response) {
      $('.qrDiv').html(response);
    });

}

$("#soceityCover").change(function() {
  readURLSoceityLogoCover(this);
});

function resetSubDiv(dayname,dayNumber) {
    $('.addMoreTimeslot_'+dayname).html(' ');
}

function addMoreTimeslot(dayname,dayNumber) {
   var Lastval = $('.'+dayname+1).val();
   var timeSlotNumber = $('.countDiv'+dayname).length;
  
        if (timeSlotNumber>0) {
          vamptimeSlotNumber = parseInt(timeSlotNumber-1);
          var LastvalNew = $('.appenddayCheckEnd'+dayname+vamptimeSlotNumber).val();
        } else {
          LastvalNew = Lastval;
        }


  if (LastvalNew=='11:30 PM') {
     swal("Error! No More Time Slot Available !", {icon: "error",});
  }else if (LastvalNew!='') {
    $.ajax({
        url: 'addMoreTimeslotFacility.php',
        type: 'POST',
        data: {dayname: dayname,lastTime:Lastval,timeSlotNumber:timeSlotNumber },
      })
      .done(function(response) {

        $('.addMoreTimeslot_'+dayname).append(response);
       
          $('.appenddayCheck'+dayname+timeSlotNumber).timepicker({
        // timeFormat : 'hh:mm a',
              interval : 30,
              minTime : LastvalNew,
              maxTime : '11:00 PM',
              startTime : '04:00 AM',
              dynamic : false,
              dropdown : true,
              scrollbar : true
          });
          $('.appenddayCheckEnd'+dayname+timeSlotNumber).timepicker({
                // timeFormat : 'hh:mm a',
                interval : 30,
                minTime : '00:00 AM',
                maxTime : '11:30 PM',
                startTime : '04:00 AM',
                dynamic : false,
                dropdown : true,
                scrollbar : true
            });

          $('.appenddayCheck'+dayname+timeSlotNumber).timepicker('option', 'change', function(time) {
            var selectedaty = $(this).attr("name");
            var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
            $('.appenddayCheckEnd'+dayname+timeSlotNumber).timepicker('option', 'minTime', later);
            $('.appenddayCheckEnd'+dayname+timeSlotNumber).timepicker('setTime', later);
          });

         

      });
    } else {
        swal("Error! Please Enter Time Slot of the "+dayname, {icon: "error",});

    }
}

function deleteTimeSlot(divName) {
  $('.'+divName).remove();
}

function addTimeSlotEdit(facility_id,facility_day_id,dayname,timeSlotNumber,next_start_time,facility_end_time) {
    $("#edit_time").addClass("d-none");
    if (facility_end_time!='00:00:00') {
      var LastvalNew =facility_end_time;
    } else  {
      var LastvalNew = '04:00 AM';
    }

    if (next_start_time!='00:00:00') {
      var next_start_time =next_start_time;
    } else  {
      var next_start_time = '11:30 PM';
    }
    // alert(next_start_time);
    $.ajax({
        url: 'addMoreTimeslotFacilityEdit.php',
        type: 'POST',
        data: {facility_id: facility_id,facility_day_id:facility_day_id,dayname,timeSlotNumber:timeSlotNumber },
      })
      .done(function(response) {
        $("#add_time").removeClass("d-none");
        $('#AddTimeSLotDiv').html(response);
       
          $('.appenddayCheck'+dayname+timeSlotNumber).timepicker({
        // timeFormat : 'hh:mm a',
              interval : 30,
              minTime : LastvalNew,
              maxTime : next_start_time,
              startTime : '04:00 AM',
              dynamic : false,
              dropdown : true,
              scrollbar : true
          });
          $('.appenddayCheckEnd'+dayname+timeSlotNumber).timepicker({
                // timeFormat : 'hh:mm a',
                interval : 30,
                minTime : '00:00 AM',
                maxTime : next_start_time,
                startTime : '04:00 AM',
                dynamic : false,
                dropdown : true,
                scrollbar : true
            });

          $('.appenddayCheck'+dayname+timeSlotNumber).timepicker('option', 'change', function(time) {
            var selectedaty = $(this).attr("name");
            var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
            $('.appenddayCheckEnd'+dayname+timeSlotNumber).timepicker('option', 'minTime', later);
            $('.appenddayCheckEnd'+dayname+timeSlotNumber).timepicker('setTime', later);
          });

         

      });
}


function singleTimeSlotEdit(facility_schedule_id,c_start_time,c_end_time,facility_id,facility_day_id,dayname,timeSlotNumber,next_start_time,current_start_time) {

  let min = 30;
  let times=c_start_time.split(":");
  //clear here more than 24 hours
  min=min%(24*60);
  times[0]=(parseInt(times[0]))+parseInt(min/60) ;
  times[1]=parseInt(times[1])+min%60;
  //here control if hour and minutes reach max
  if(times[1]>=60) { times[1]=0 ;times[0]++} ;
  times[0]>=24 ?  times[0]-=24  :null;
  
  //here control if less than 10 then put 0 frond them
  times[0]<10 ? times[0]= "0" + times[0] : null ;
  times[1]<10 ? times[1]= "0" + times[1] : null ;
  var new_end_time =  times.join(":");

    $("#add_time").addClass("d-none");
    if (current_start_time!='00:00:00') {
      var LastvalNew =current_start_time;
    } else  {
      var LastvalNew = '04:00 AM';
    }

    if (next_start_time!='00:00:00') {
      var next_start_time =next_start_time;
    } else  {
      var next_start_time = '11:30 PM';
    }
    $.ajax({
        url: 'addMoreTimeslotFacilityEdit.php',
        type: 'POST',
        data: {facility_schedule_id:facility_schedule_id,c_start_time:c_start_time,c_end_time:c_end_time,facility_id: facility_id,facility_day_id:facility_day_id,dayname,timeSlotNumber:timeSlotNumber,editTimeSlotSIngle:'editTimeSlotSIngle' },
      })
      .done(function(response) {
        $("#edit_time").removeClass("d-none");

        $('#AddTimeSLotDiv').html(response);
       
          $('.appenddayCheck'+dayname+timeSlotNumber).timepicker({
        // timeFormat : 'hh:mm a',
              interval : 30,
              minTime : LastvalNew,
              maxTime : next_start_time,
              startTime : '04:00 AM',
              dynamic : false,
              dropdown : true,
              scrollbar : true
          });
          $('.appenddayCheckEnd'+dayname+timeSlotNumber).timepicker({
                // timeFormat : 'hh:mm a',
                interval : 30,
                minTime : new_end_time,
                maxTime : next_start_time,
                startTime : '04:00 AM',
                dynamic : false,
                dropdown : true,
                scrollbar : true
            });

          $('.appenddayCheck'+dayname+timeSlotNumber).timepicker('option', 'change', function(time) {
            var selectedaty = $(this).attr("name");
            var later = new Date(time.getTime() + (1 * 30 * 60 * 1000));
            $('.appenddayCheckEnd'+dayname+timeSlotNumber).timepicker('option', 'minTime', later);
            $('.appenddayCheckEnd'+dayname+timeSlotNumber).timepicker('setTime', later);
          });

         

      });
}

function ResetFormBooking() {
   $('#facilityTimeSlotCheck').html('');
   $('.facility_datepicker').val('');
}

function copyReminder() {
  var reminderText = $("#quick_reminder").val();
  $("#reminder_text").val(reminderText);
}


$('input[type=radio][name=qr_attendace_self]').change(function() {
    if (this.value == '1') {
      // alert('asfdf');
      $('.alertDurationLblDiv').show();
      $('.alertDurationLbl').show();
      $('.eyeAttendance1').show();
      $('.eyeAttendance').show();

    }
    else if (this.value == '0') {
      // $('.eventPrice').hide();
      $('.alertDurationLblDiv').hide();
      $('.alertDurationLbl').hide();
      $('.eyeAttendance1').hide();
      
    }
});

$('.no_copy').bind("cut copy paste",function(e) {
     e.preventDefault();
 });


$('.floor-id-cls').change(function() {
      var floor_id = this.id;
      var floor_sort =  $(this).val();
      $.ajax({
        url: "controller/blockController.php",
        cache: false,
        type: "POST",
        data: {floor_id : floor_id, floor_sort : floor_sort, updateFloorSort : 'updateFloorSort', csrf:csrf},
        success: function(response)
        {
          var obj =response.split('~');
          if(obj[0]=="error")
          {
             swal("Error! Duplicate Order Number!", {
                            icon: "error",
                          });
          } 
          else 
          {
            swal("Success ! Department Order Number Changed!", {
                            icon: "success",
                          });
            // location.reload();
          }
        }
      });  

    });

$('.users-order-change').change(function() {
      var user_id = this.id;
      var user_sort =  $(this).val();
      $.ajax({
        url: "controller/userController.php",
        cache: false,
        type: "POST",
        data: {user_id : user_id, user_sort : user_sort, updateUserSort : 'updateUserSort', csrf:csrf},
        success: function(response)
        {
          response = JSON.parse(response);
          if(response.status == "error")
          {
             swal("Error! Something Went Wrong!", {
                            icon: "error",
                          });
          }
          else if(response.status == "duplicate")
          {
            swal("Error! Duplicate Order Number!, Already Assigned to "+response.user, {
                            icon: "error",
                          }); 
          }
          else 
          {
            swal("Success ! User Order Number Changed!", {
                            icon: "success",
                          });
            // location.reload();
          }
        }
      });  

    });

$('#nationality_drop').on('change', function() 
{
  if ( this.value == 'other')
  {
    $('#nationality_div').removeClass('d-none');
  }
  else
  {
    $('#nationality_div').addClass('d-none');
  }
});

function updateLanguage() {
    $(".ajax-loader").show();
     $.ajax({
      url: "controller/updateLng.php",
      cache: false,
      type: "POST",
      data: {udpateLng : 'udpateLng'},
      success: function(response){
        $(".ajax-loader").hide();
      }
    });
  }

  $( function() {
      $( "#searchMenu" ).autocomplete({
      source: 'menu-search.php',
      select: function (e, ui) {
        var menuUrl = ui['item']['id'];

           window.location.href=menuUrl;
      }  
      });
  });

  $('#block_id').on('select2:select', function (e)
  {
    var data = e.params.data;
    if (data.id == 0) 
    {
      $("#block_id option[value='30']").removeAttr("selected");
      $("#block_id option[value='31']").removeAttr("selected");
      $("#block_id option[value='40']").removeAttr("selected");
      $("#block_id option[value='44']").removeAttr("selected");
      $('#block_id').trigger("change");
    }
    if (data.id == 30 || data.id == 31 || data.id == 40 || data.id == 44) 
    {
      $("#block_id option[value='0']").removeAttr("selected");
      $('#block_id').trigger("change");
    }
  });

  function  editItemsDetails(assets_id,society_id) {
    //alert(assets_id);
    $.ajax({

      url: "editItemsDetails.php",
      cache: false,
      type: "POST",
      data: {assets_id : assets_id,csrf:csrf,society_id:society_id},
      success: function(response){
        $('#edit_items').html(response);
      }
    });
  }


  function  getCustodian() {
     var floor_id = $("#floor_id").val();
     var floor_id_old = $("#floor_id_old").val();
     var assets_id = $("#assets_id").val();
     var cstId = $("#cstId").val();
     
     if(floor_id>0){
      //  $('#user_id').attr('required','required');
       // $('#start_date').attr('required','required');
        floor_id_old=floor_id
     }
     
    
    //$(".ajax-loader").show();
    $.ajax({
      url: "getCustodian.php",
      cache: false,
      type: "POST",
      data: {floor_id : floor_id_old,getCustodian:'getCustodian',csrf:csrf,assets_id:assets_id,cstId:cstId},
      success: function(response){
        $('#user_id').html(response);
        $('.user_id').html(response);
        //$(".ajax-loader").hide();

      }
    });
  }

function  getCustodianReport() {
     var floor_id = $("#floor_id").val();
     
     
    $(".ajax-loader").show();
    $.ajax({
      url: "getCustodianReport.php",
      cache: false,
      type: "POST",
      data: {floor_id : floor_id,getCustodianReport:'getCustodianReport',csrf:csrf},
      success: function(response){
        $('#user_id').html(response);
        $(".ajax-loader").hide();

      }
    });
  }

  function  editAssetscategory(assets_category_id) {

    $.ajax({
      url: "editassetsCategory.php",
      cache: false,
      type: "POST",
      data: {assets_category_id : assets_category_id,csrf:csrf},
      success: function(response){
        $('#edit_Assets_category').html(response);
      }
    });
  }


  function  editAssets(assets_id) {
   /* alert(assets_id);*/
    $.ajax({

      url: "editAssets.php",
      cache: false,
      type: "POST",
      data: {assets_id : assets_id,csrf:csrf},
      success: function(response){
        $('#edit_assets').html(response);
      }
    });
  }


  function  editInventory(inventory_id) {
   /* alert(inventory_id);*/
    $.ajax({

      url: "editInventory.php",
      cache: false,
      type: "POST",
      data: {inventory_id : inventory_id,csrf:csrf},
      success: function(response){
        $('#edit_Inventory').html(response);
      }
    });
  }


  function  editOutofStock(out_of_stock_id) {
   /* alert(out_of_stock_id);*/
    $.ajax({

      url: "editOutofStock.php",
      cache: false,
      type: "POST",
      data: {out_of_stock_id : out_of_stock_id,csrf:csrf},
      success: function(response){
        $('#edit_Out_stock').html(response);
      }
    });
  }

  $(".btnErrorBalance").click(function()
  {
    swal("Error! This balance sheet is Used.", {icon: "error",});
  });

  $(".btnResourceSalary").click(function()
  {
    swal("Error! Salary is not added.", {icon: "error",});
  });

  $('.preventStartWithZero').keypress(function(e){
    if (this.value.length == 0 && e.which == 48 ){
      return false;
    }
  });

  $( "#floor-list" ).sortable({
      placeholder : "ui-state-highlight",
      update  : function(event, ui)
      {
        var imageIdsArray = new Array();
        var csrf =$('input[name="csrf"]').val();
        $('#floor-list .floorBox').each(function(){
          imageIdsArray.push($(this).data("post-id"));
        });
        $.ajax({
          url:"controller/changeOrderController.php",
          method:"POST",
          data:{order_change_id:imageIdsArray,changeFloorOrder:'changeFloorOrder',csrf:csrf},
          success:function(data)
          {
           if(data==1){
            swal("Order Changed Successfull", {icon: "success",});
           }else{
             swal("Something went wrong!", {icon: "error",});
           }
          }
        });
      }
    });


    $( "#sub-dept-list" ).sortable({
      placeholder : "ui-state-highlight",
      update  : function(event, ui)
      {
        var imageIdsArray = new Array();
        var csrf =$('input[name="csrf"]').val();
        $('#sub-dept-list .subDeptBox').each(function(){
          imageIdsArray.push($(this).data("post-id"));
        });
        $.ajax({
          url:"controller/changeOrderController.php",
          method:"POST",
          data:{order_change_id:imageIdsArray,changeSubDepartmentOrder:'changeSubDepartmentOrder',csrf:csrf},
          success:function(data)
          {
           if(data==1){
            swal("Order Changed Successfull", {icon: "success",});
           }else{
             swal("Something went wrong!", {icon: "error",});
           }
          }
        });
      }
    });

  $( "#users-list" ).sortable({
      placeholder : "ui-state-highlight",
      update  : function(event, ui)
      {
        var imageIdsArray = new Array();
        var csrf =$('input[name="csrf"]').val();
        $('#users-list .userBox').each(function(){
          imageIdsArray.push($(this).data("post-id"));
        });
        $.ajax({
          url:"controller/changeOrderController.php",
          method:"POST",
          data:{order_change_id:imageIdsArray,changeEmployeeOrder:'changeEmployeeOrder',csrf:csrf},
          success:function(data)
          {
           if(data==1){
            swal("Order Changed Successfull", {icon: "success",});
           }else{
             swal("Something went wrong!", {icon: "error",});
           }
          }
        });
      }
    });

///////20-01-2022 (SHUBHAM)/////////////
function getLeaveTypeByFloorid(id)
{
  var leave_year = $('#leave_year').val();
  $.ajax({
      url: "getLeaveTypeList.php",
      cache: false,
      type: "POST",
      data: {
          floor_id:id,
          leave_year:leave_year,
        },
      success: function(response){
        $('#all-leave-type').html(response);
      }
  });
}

function getUserByFloorId(floor_id)
{
  $(".ajax-loader").show();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            async: false,
            data: {
                action:"getUserByFloor",
                floor_id:floor_id,
              },
            success: function(response){
              $(".ajax-loader").hide();
              var userHtml ="";
              userHtml=`<option value="" > Select Employee </option>`;
              
              $.each(response.users, function( index, value ) {
                userHtml +=`<option data-date="`+value.joining_date+`" value="`+value.user_id+`">`+value.user_full_name+`</option>`;
              });
              
             $('#user_id').html(userHtml);
             $('#uId').html(userHtml);
             $('.uId').html(userHtml);
            }
    })
}

function getUserByLeaveType(id)
{
  $(".ajax-loader").show();
  var leave_type_id = $('#leave_type_id').val();
  var assign_leave_year = $('#assign_leave_year').val();
  if(leave_type_id != '' && assign_leave_year != ''){
    $.ajax({
        url: "getUserListByLeaveType.php",
        cache: false,
        type: "POST",
        data: {
            floor_id:id,
            leave_type_id:leave_type_id,
            assign_leave_year:assign_leave_year,
          },
        success: function(response){
          $(".ajax-loader").hide();
          $('#user_id').html(response);
        }
    });
  }else{
    $(".ajax-loader").hide();
    //swal("Please Select Leave Type!");
  }
}

function addMoreApplyLeaveForm(){

  var block_id = $('#block_id').val();
  var floor_id = $('#floor_id').val();
  var user_id = $('#user_id').val();
  if(user_id != ''){
    $(".ajax-loader").show();
    $.ajax({
      url: "addMoreApplyLeaveForm.php",
      cache: false,
      type: "POST",
      data: {block_id:block_id,
        floor_id:floor_id,
        user_id:user_id
      },
      success: function(response){
        $('.add-more-leave').html(response);
        $(".ajax-loader").hide();
      }
    });
  }else{
    $('.add-more-leave').html('');
  }
}

function leaveHistory(user_id, leave_type_id, year) {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {action:'userLeaveHistory', 
      user_id:user_id,
      leave_type_id:leave_type_id,
      year:year,
    },
    success: function(response){
      var mainContent = '';
      if(response.status == 200){
        $('#userLeaveHistoryModal').modal();
        $.each(response.leave, function( index, value ) {
            mainContent += `<tr>
                              <td>`+(index+1)+`</td>
                              <td>`+value.leave_date+` (`+value.leave_day+`)</td>
                              <td>`+value.leave_day_type+`</td>
                            </tr>`;
        });
        $('#showLeaveHistoryData').html(mainContent);
      }
      //$('.add-more-leave').append(response);
    }
  });
}

function updateLeaveDayType(leave_id, leave_day_type, paid_unpaid, leave_type_id, user_id, leave_date,show_leave_date,attendance_available) {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:'getLeaveBalanceForAutoLeave',
        leave_type_id:leave_type_id,
        leave_date:leave_date,
        user_id:user_id,
    },
    success: function(response){
        $(".ajax-loader").hide();
        $('.applicable_leave').html(response.leave.available_paid_leave);
        $('.balance_leave').html(response.leave.remaining_leave);
        $('.leave_date').html(show_leave_date);
        if(response.leave.available_paid_leave <= 0){
          var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
          $('#paid_unpaid').html(paidUnpaidValue);  
        }
        else{
          var paidUnpaidValue = `<option value="">-- Mode --</option>
          <option value="0">Paid Leave</option>
          <option value="1">Unpaid Leave</option> `;
          $('#paid_unpaid').html(paidUnpaidValue);
          $('#paid_unpaid').val(paid_unpaid);
        }
        
        $('.leave_id').val(leave_id);
        $('#leave_day_type').val(leave_day_type);
        if(leave_day_type == 1 && attendance_available > 0){
          var leaveDayType = `<option value="1">Half Day Leave</option> `;
          $('#leave_day_type').html(leaveDayType);
        }
        else if(leave_day_type == 0 && attendance_available == 0){
            var leaveDayType = `<option value="1">Full Day Leave</option> `;
            $('#leave_day_type').html(leaveDayType);
        }
        $('#leaveDayTypeModal').modal();
    }
  })
}

function removeUserApprovedLeave()
{
  swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this data!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      leave_id = $('.leave_id').val();
      $.ajax({
        url: "controller/leaveController.php",
        cache: false,
        type: "POST",
        data: {
            leave_id:leave_id,
            action:'deleteUserLeave',
            csrf:csrf,
          },
        success: function(response){
          location.reload();
        }
      });
    } else {
      //swal("Your data is safe!");
    }
  });
}

function removeLeaveForm() {
  $('.add-more-leave').html('');
}


function assignLeaveToAllUsers(year) {
  var oTable = $("#example").dataTable();
  var val = [];
  $(".multiDelteCheckbox:checked", oTable.fnGetNodes()).each(function(i) {
    val[i] = $(this).val();
  });
  if(val=="") {
    swal(
      'Warning !',
      'Please Select at least 1 user !',
      'warning'
    );
  } else { 
    $('#userIds').val(val.toString())
    $('#bulkAssignLeaveGroupModal').modal();    
  }
}

function getAccessForDepartment(id)
{
  $(".ajax-loader").show();
  var access_type = $('#access_type').val();
  var access_by_id = $('#access_by_id').val();
  var access_type_for = $('#access_type_for').val();
  $.ajax({
      url: "getAccessForDepartmentList.php",
      cache: false,
      type: "POST",
      data: {
          block_id:id,
          access_type:access_type,
          access_by_id:access_by_id,
          access_type_for:access_type_for,
        },
      success: function(response){
        $(".ajax-loader").hide();
        $('.floor_id').html(response);
      }
  });
}

function userTaskPauseHistoryDetail(id)
{
  var main_content="";
  $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {
              action:"getUserTaskPauseHistoryDetail",
              task_pause_history_id:id
            },
          success: function(response){
            $(".ajax-loader").hide();
            if(response.status==200) {  
              $('#PauseHistoryDetailModal').modal();   
             
              main_content += `<div class="col-md-12 mb-3">
                                <b>User Name </b>
                                <p class="d-block" >`+response.pause_history.user_full_name+`</p>
                              </div>
                              <div class="col-md-12 mb-3">
                                <b>Pause Start Date</b>
                                <p class="d-block" >`+response.pause_history.task_pause_date+`</p>
                              </div>
                              <div class="col-md-12 mb-3">
                                <b>Pause Resume Date</b>
                                <p class="d-block" >`+response.pause_history.task_resume_date+`</p>
                              </div>
                              <div class="col-md-12 mb-3">
                                <b>Pause Time</b>
                                <p class="d-block" >`+response.pause_history.pause_time+`</p>
                              </div>
                              <div class="col-md-12 mb-3">
                                <b>Reason</b>
                                <p class="d-block" >`+response.pause_history.task_pause_reason+`</p>
                              </div>`;
                              
                            
              $('#pauseHistoryDetailData').html(main_content);
            } 
          }
        });
}

function chatGroupDataSet(id,group_type)
{  
  $(".ajax-loader").show();
  $.ajax({
    url: "chatGroupDataSet.php",
    cache: false,
    type: "POST",
    data: {
        group_id:id,group_type:group_type,
        csrf:csrf
      },
    success: function(response){
      $('#chatGroupModal').modal(); 
      $('.chatGroupData').html(response);
      $(".ajax-loader").hide();
    }
  })
}

function wfhStatusChange(value,id,lat,long,range,take_selfie,key) {
  if(value == 1){
    initialize(lat,long);
    $('.wfh_status').val(value);
    $('.wfh_id').val(id);
    $('.latitude').val(lat);
    $('.longitude').val(long);
    $('#wfh_take_selfie').val(take_selfie);
    if(range == 0){
      range = 10.00;
    }
    $('#wfh_attendance_range').val(range);
    $('#action_key').val(key);
    $('#wfhStatusApprovedModal').modal();
  }else if(value == 2){
    $('.wfh_status').val(value);
    $('.wfh_id').val(id);
    $('#wfhStatusRejectModal').modal();
  }
}

function showWFHDetail(id)
{
  var main_content="";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getWFHDetailById",
        wfh_id:id
      },
    success: function(response){
      $(".ajax-loader").hide();
      if(response.status==200) {  
        $('#wfhDetailModal').modal();   
        initialize01(response.wfh.wfh_latitude,response.wfh.wfh_longitude,response.wfh.wfh_attendance_range);
        main_content += `<div class="col-md-6 mb-1">
                          <b>User Name </b>
                          <p class="d-block" >`+response.wfh.user_full_name+`</p>
                        </div>
                        <div class="col-md-6 mb-1">
                          <b>Designation</b>
                          <p class="d-block" >`+response.wfh.user_designation+`</p>
                        </div>
                        <div class="col-md-6 mb-1">
                          <b>Start Date</b>
                          <p class="d-block" >`+response.wfh.wfh_start_date+`</p>
                        </div>
                        <div class="col-md-6 mb-1">
                          <b>End Date</b>
                          <p class="d-block" >`+response.wfh.wfh_end_date+`</p>
                        </div>
                        <div class="col-md-6 mb-1">
                          <b>Day Type</b>
                          <p class="d-block" >`+response.wfh.wfh_day_type+`</p>
                        </div>
                        <div class="col-md-6 mb-1">
                          <b>Need Selfie</b>
                          <p class="d-block" >`+response.wfh.wfh_take_selfie+`</p>
                        </div>
                        <div class="col-md-6 mb-1">
                           <b>Range</b>
                          <p class="d-block" >`+response.wfh.wfh_attendance_range+` Meter</p>
                        </div>
                        <div class="col-md-6 mb-1">
                          <b>Attachment</b>
                          <p class="d-block" >`;
                          if(response.wfh.wfh_attachment != '' && response.wfh.wfh_attachment != null){
                            main_content += `<a href="../img/wfh_attachment/`+response.wfh.wfh_attachment+`" target="_blank"  >`+response.wfh.wfh_attachment+`</a>`
                          }else{
                            main_content +=
                          `No Attachment Found!`;
                          }
                          main_content +=
                          `</p>
                        </div>
                        <div class="col-md-12 mb-1">
                          <b>Reason</b>
                          <p class="d-block" >`+response.wfh.wfh_reason+`</p>
                        </div>`;
                        if(response.wfh.wfh_declined_reason != '' && response.wfh.wfh_declined_reason != null){
                          main_content +=
                          `<div class="col-md-12 mb-1">
                          <b>Declined Reason</b>
                          <p class="d-block" >`+response.wfh.wfh_declined_reason+`</p>
                        </div>`;
                        }
                        
                      
        $('#showWFHData').html(main_content);
      } 
    }
  });
}

function  getTaskStepList() {
  var no_of_task_step = $("#no_of_task_step").val();
  var floor_type = $("#floor_type").val();
  $.ajax({
        url: "getTaskStepList.php",
        cache: false,
        type: "POST",
        data: {no_of_task_step : no_of_task_step,csrf:csrf},
        success: function(response){
            $('#taskStepForm').html(response);
          
            
        }
     });
}

function taskStepSetData(id) {
  $('.hideupdate').show();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getTaskStepById",
                task_step_id:id,
              },
            success: function(response){
              $('.hideAdd').hide();
              $('#taskStepModal').modal(); 
              $('#task_step_id').val(response.task_step.task_step_id);  
              $('#task_step').val(response.task_step.task_step);  
              $('#task_step_due_date').val(response.task_step.task_step_due_date);  
              $('#task_step_note').val(response.task_step.task_step_note);  
            }
    })
}

$('.task_complete').on('change', function(){
  $(':input[type="submit"]').prop('disabled', true);
  $(".ajax-loader").show();
  $('#taskCompleteStatus').submit();
})

function allowMultiWFH(value) {
  var oTable = $("#example").dataTable();
  var val = [];
  $(".multiDelteCheckbox:checked", oTable.fnGetNodes()).each(function(i) {
    val[i] = $(this).val();
  });
  if(val=="") {
    swal(
      'Warning !',
      'Please Select at least 1 employee !',
      'warning'
    );
  } else {
      var csrf =$('input[name="csrf"]').val();
      $(".ajax-loader").show();
      $.ajax({
        url: "controller/statusController.php",
        cache: false,
        type: "POST",
        data: {ids : val,status
          :'allowMultiWFH',value:value, csrf:csrf},
        success: function(response){
          $(".ajax-loader").hide();
          if(response==1) {
            swal("Status Changed", {
              icon: "success",
            });
            document.location.reload(true);
          } else {
            document.location.reload(true);
            swal("Something Wrong!", {
                      icon: "error",
                    });
          }
        }
      });  
  }
}


function taskStepDetail(id) {
  var main_content="";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getTaskStepDetailById",
        task_step_id:id
      },
    success: function(response){
      $(".ajax-loader").hide();
      if(response.status==200) {  
        $('#taskStepDetailModal').modal();   
        
        main_content += `<div class="col-md-12 mb-3">
                          <b>Task Step </b>
                          <p class="d-block" >`+response.step.task_step+`</p>
                        </div>
                        <div class="col-md-12 mb-3">
                          <b>Due Date</b>
                          <p class="d-block" >`+response.step.task_step_due_date+`</p>
                        </div>
                        <div class="col-md-12 mb-3">
                          <b>Created Date</b>
                          <p class="d-block" >`+response.step.task_step_created_date+`</p>
                        </div>
                        <div class="col-md-12 mb-3">
                          <b>Complete Status</b>
                          <p class="d-block" >`+response.step.task_step_complete+`</p>
                        </div>
                        <div class="col-md-12 mb-3">
                          <b>Step Note</b>
                          <p class="d-block" >`+response.step.task_step_note+`</p>
                        </div>`;       
        $('#taskStepDetailData').html(main_content);
      } 
    }
  });
}

function getUserByFloorIdForGroup(floor_id)
{
  group_id = $('#group_id').val();
  if(group_id != '' && group_id>0){
    $(".ajax-loader").show();
    $.ajax({
              url: "getUserListForGroup.php",
              cache: false,
              type: "POST",
              data: {
                  floor_id:floor_id,
                  group_id:group_id,
                },
              success: function(response){
                $(".ajax-loader").hide();
                $('#user_id').html(response);
              }
    })
  }else{
    swal("Please Select Group!");
  }
}

function macAddressRequestStatusChange(status,id){
  $('#mac_address_change_status').val(status);
  $('#user_mac_address_id').val(id);
  $('#macAddreeStatusRejectModal').modal();
}

function userMACAddressDetail(id) {
  var main_content="";
  $(".ajax-loader").show();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getUserMACAddressDetailById",
        user_mac_address_id:id
      },
    success: function(response){
      $(".ajax-loader").hide();
      if(response.status==200) {  
        $('#userMACAddressDetailModal').modal();   
        main_content += `<div class="col-md-12 mb-3">
                          <b>User </b>
                          <p class="d-block" >`+response.step.user_full_name+`</p>
                        </div>
                        <div class="col-md-12 mb-3">
                          <b>MAC Address</b>
                          <p class="d-block" >`+response.step.mac_address+`</p>
                        </div>
                        <div class="col-md-12 mb-3">
                          <b>MAC Address Change Reason</b>
                          <p class="d-block" >`+response.step.mac_address_change_reason+`</p>
                        </div>
                        <div class="col-md-12 mb-3">
                          <b>Change Status</b>
                          <p class="d-block" >`+response.step.mac_address_change_status+`</p>
                        </div>
                        <div class="col-md-12 mb-3">
                          <b>Status Change By</b>
                          <p class="d-block" >`+response.step.status_change_by+`</p>
                        </div>`;       
        $('#userMACAddressDetailData').html(main_content);
      } 
    }
  });
}

$('.employee-access').on('change', function() {
  var user_id = $('#user_id').val();
  var access_type = $('#access_type').val();
  if(user_id != '' && access_type !=''){
    $(".ajax-loader").show();
    $('#employeeAccess').submit();
  }else{
    //swal("Please select Access by employee and access type!");
  }
});

$('.employeeAccessfrom').on('change', function() {
  $("#employeeAccess").validate({
  errorPlacement: function (error, element) {
    if (element.parent('.input-group').length) { 
        error.insertAfter(element.parent());      // radio/checkbox?
    } else if (element.hasClass('select2-hidden-accessible')) {     
        error.insertAfter(element.next('span'));  // select2
        element.next('span').addClass('error').removeClass('valid');
    } else {                                      
        error.insertAfter(element);               // default
    }
  }
})
});

function updateAutoLeave(user_full_name,leave_id, user_id, leave_date, paid_unpaid, year,show_leave_date,leave_day_type) {
  $('#applicable_leave').html('');
  $('#use_leave').html('');
  $('#balance_leave').html('');
  $('.leave_date').html('');
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:'getLeaveTypeByUserId',
        user_id:user_id,
        year:year,
      },
    success: function(response){
      $(".ajax-loader").hide();
      var html ="";
      html=`<option value="">-- Leave Type --</option>`;
      
      $.each(response.leave_type, function( index, value ) {
        html +=`<option value="`+value.leave_type_id+`">`+value.leave_type_name+`</option>`;
      });
      
     $('#leave_type_id').html(html);
      
    }
  })
  $('.leave_id').val(leave_id);
  $('.user_full_name').val(user_full_name);
  $('#leave_date').val(leave_date);
  $('#show_leave_date').val(show_leave_date);
  $('#user_id').val(user_id);
  $('.paid_unpaid').val(paid_unpaid);
  $('.leave_day_type').val(leave_day_type);
  $('#updateAutoLeaveModal').modal();
}

function getLeaveBalanceForAutoLeave(id) {
  var leave_date = $('#leave_date').val();
  var show_leave_date = $('#show_leave_date').val();
  var leave_day_type = $('.leave_day_type').val();
  var user_id = $('#user_id').val();
  $(".ajax-loader").show();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:'getLeaveBalanceForAutoLeave',
        leave_type_id:id,
        leave_date:leave_date,
        user_id:user_id,
      },
    success: function(response){
      console.log(leave_day_type);
      $(".ajax-loader").hide();
      $('#applicable_leave').html(response.leave.available_paid_leave);
      $('#balance_leave').html(response.leave.remaining_leave);
      $('.leave_date').html(show_leave_date);
      
      if(response.leave.available_paid_leave <= 0 && leave_day_type==0){
        var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
          $('.paid_unpaid').html(paidUnpaidValue);
      } else if(response.leave.available_paid_leave >=1 && leave_day_type==0){
        var paidUnpaidValue = `<option value="">-- Mode --</option>
                <option value="0">Paid Leave</option>
                <option value="1">Unpaid Leave</option> `;
        $('.paid_unpaid').html(paidUnpaidValue);  
      }  else if(response.leave.available_paid_leave >=0.5 && leave_day_type==1){
        var paidUnpaidValue = `<option value="">-- Mode --</option>
                <option value="0">Paid Leave</option>
                <option value="1">Unpaid Leave</option> `;
        $('.paid_unpaid').html(paidUnpaidValue);  
      } else {
          var paidUnpaidValue = `<option value="1">Unpaid Leave</option> `;
          $('.paid_unpaid').html(paidUnpaidValue);
      }
    }
  })
}

function takeAttendanceSelfieAccess(value) {
  var csrf =$('input[name="csrf"]').val();
  $(".ajax-loader").show();
  $.ajax({
        url: "controller/statusController.php",
        cache: false,
        type: "POST",
        data: {value : value,status
          :'takeAttendanceSelfieAccess', csrf:csrf},
        success: function(response){
          $(".ajax-loader").hide();
          if(response==1) {
           // document.location.reload(true);
            swal("Status Changed", {
                      icon: "success",
                    });
                    document.location.reload(true);
          } else {
            document.location.reload(true);
            swal("Something Wrong!", {
                      icon: "error",
                    });

          }
        }
      });
}

function getWorkfolioEmployees(id)
{
  $(".ajax-loader").show();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getWorkfolioEmployeesById",
                workfolio_team_id:id,
              },
            success: function(response){
              $(".ajax-loader").hide();
              var userHtml ="";
              userHtml=`<option value="">-- Select Team Employee --</option>`;
              
              $.each(response.team_employee, function( index, value ) {
                userHtml +=`<option value="`+value.workfolio_employee_id+`">`+value.displayName+`</option>`;
              });
              
             $('#eId').html(userHtml);
            }
    })
}

function taskPriorityDataSet(id)
{  
  $(".ajax-loader").show();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getTaskPriority",
        task_priority_id:id,
      },
    success: function(response){
      $(".ajax-loader").hide();
      $('#editModal').modal(); 
      $('#task_priority_id').val(response.task_priority.task_priority_id); 
      $('#task_priority_name').val(response.task_priority.task_priority_name); 
    }
  })
}

function getTaskUserHistory(task_id) {
  $(".ajax-loader").show();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {action:'getTaskUserHistory', 
      task_id:task_id,
    },
    success: function(response){
      var mainContent = '';
      $(".ajax-loader").hide();
      if(response.status == 200){
        $('#taskUserHistoryModal').modal();
        $.each(response.task_history, function( index, value ) {
            var actionBtn = '';
            var current_assign = '';
            if(value.total_pause > 0){
              var actionBtn = `<a href="userTaskPauseHistory?id=`+value.task_user_history_id+`" title="Pause History" class="btn btn-sm btn-warning mr-2"> <i class="fa fa-history"></i></a>`;
            }
            if(value.task_pull_back_date == null || value.task_pull_back_date == '0000-00-00 00:00:00'){
              var current_assign = `<i data-toggle="tooltip" title="Assign User" class="fa fa-check-circle text-success" aria-hidden="true"></i>`;
            }
            mainContent += `<tr>
                              <td>`+(index+1)+`</td>
                              <td>`+value.user_full_name+` `+current_assign+`</td>
                              <td>`+value.task_assign_date+`</td>
                              <td>`+value.status_change_date+`</td>
                              <td>`+actionBtn+`</td>
                            </tr>`;
        });
        $('#showTaskUserHistoryData').html(mainContent);
      }
      //$('.add-more-leave').append(response);
    }
  });
}

function moveToScrap(id){
  $('.assets_id').val(id); 
  $('#moveToScrapReasonModal').modal();
}

function itemSoldOut(id){
  $('.assets_id').val(id); 
  $('#soldOutReasonAndPriceModal').modal();
}

$('.taskAccessForBranchMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all branch, please removed selected branch!");
      $('option:contains(All Branch)').prop("selected", false);
    }else  {

    }
  }
});

$('.taskAccessForDepartmentMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all department, please removed selected department!");
      $('option:contains(All Department)').prop("selected", false);
    }else  {

    }
  }
});

$('.galleryForBranchMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all branch, please removed selected branch!");
      $('option:contains(All Branch)').prop("selected", false);
    }else  {

    }
  }
});

$('.galleryForDepartmentMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all department, please removed selected department!");
      $('option:contains(All Department)').prop("selected", false);
    }else  {

    }
  }
});

$('.eventForBranchMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all branch, please removed selected branch!");
      $('option:contains(All Branch)').prop("selected", false);
    }else  {

    }
  }
});

$('.eventForDepartmentMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all department, please removed selected department!");
      $('option:contains(All Department)').prop("selected", false);
    }else  {

    }
  }
});

$('.taskAccessForEmployeeMultiSelectCls').change(function() {    
  var item=$(this);
  var abc= item.val();
  if (abc==0) {
    
  } else {
   var x = abc+' ';
    if(x.charAt(0)=='0'){
      swal("if you want to all employee, please removed selected employee!");
      $('option:contains(All Employee)').prop("selected", false);
    }else  {

    }
  }
});

function getTaskAccessForDepartment()
{
  $(".ajax-loader").show();
  var access_for = $('#access_for').val();
  var dwblockId = $('.dwblockId').val();
  var ewblockId = $('.ewblockId').val();
  console.log(dwblockId);
  if(access_for==2 && dwblockId != null){
    var blockIds = dwblockId.join("','");
  }
  if(access_for==3 &&  ewblockId != null){
    var blockIds = ewblockId.join("','");
  }
  console.log(blockIds);
  $.ajax({
      url: "getTaskAccessForDepartmentList.php",
      cache: false,
      type: "POST",
      data: {
        blockIds:blockIds,
        access_for:access_for,
        },
      success: function(response){
        $(".ajax-loader").hide();
        $('.dwfloorId').html(response);
        $('.ewfloorId').html(response);
      }
  });
}

function getTaskAccessForDepartmentWorkReport()
{
  $(".ajax-loader").show();
  var work_report_on = $('.work_report_on').val();
  var dwblockId = $('.dwblockId').val();
  var ewblockId = $('.ewblockId').val();
  console.log(work_report_on);
  if(work_report_on==3 && dwblockId != null){
    var blockIds = dwblockId.join("','");
  }
 
  console.log(blockIds);
  $.ajax({
      url: "getTaskAccessForDepartmentListWorkReport.php",
      cache: false,
      type: "POST",
      data: {
        blockIds:blockIds,
        work_report_on:work_report_on,
        },
      success: function(response){
        $(".ajax-loader").hide();
        $('.dwfloorId').html(response);
        $('.ewfloorId').html(response);
      }
  });
}

function getDepartmentForAdmin()
{
  $(".ajax-loader").show();
  var accessAdminBlocks = $('.accessAdminBlocks').val();
  if(accessAdminBlocks != null){
    var blockIds = accessAdminBlocks.join("','");
  }
 
  console.log(blockIds);
  $.ajax({
      url: "getAccessForDepartmentListAdmin.php",
      cache: false,
      type: "POST",
      data: {
        blockIds:blockIds
        },
      success: function(response){
        $(".ajax-loader").hide();
        $('.dwfloorId').html(response);
      }
  });
}



function getTaskAccessForEmployee()
{
  $(".ajax-loader").show();
  var ewfloorId = $('.ewfloorId').val();
  if(ewfloorId != null){
    var floorIds = ewfloorId.join("','");
  }
  $.ajax({
      url: "getTaskAccessForEmployeeList.php",
      cache: false,
      type: "POST",
      data: {
        floorIds:floorIds,
        },
      success: function(response){
        $(".ajax-loader").hide();
        $('.ewempId').html(response);
      }
  });
}

function changeAccessMode(value, user_id, access_type){
  var csrf =$('input[name="csrf"]').val();
  $(".ajax-loader").show();
  $.ajax({
    url: "controller/statusController.php",
    cache: false,
    type: "POST",
    data: {value : value,
          user_id:user_id, 
          access_type:access_type, 
          status:'changeAccessMode', 
          csrf:csrf},
    success: function(response){
      $(".ajax-loader").hide();
      if(response==1) {
        // document.location.reload(true);
        swal("Status Changed", {
                  icon: "success",
                });
                document.location.reload(true);
      } else {
        document.location.reload(true);
        swal("Something Wrong!", {
                  icon: "error",
                });
      }
    }
  });
}

function HrDocSubCategoryDataSet(id)
{  
  $('.hideupdate').show();
  $('.hideAdd').hide();
  $(".ajax-loader").show();
    $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getHrDocSubCategory",
                hr_document_sub_category_id:id,
              },
            success: function(response){
              $(".ajax-loader").hide();
              if(response.status==200){
                $('#addModal').modal();
                $('#hr_document_sub_category_id').val(response.hr_document_sub_category.hr_document_sub_category_id);
                $('#hr_document_category_id').val(response.hr_document_sub_category.hr_document_category_id);
                $('#hr_document_sub_category_name').val(response.hr_document_sub_category.hr_document_sub_category_name);
                $('#hr_document_category_id').select2();
              }
            }
    })
}

function getHRDocSubCategory(id)
{
  $(".ajax-loader").show();
  $.ajax({
            url: "../residentApiNew/commonController.php",
            cache: false,
            type: "POST",
            data: {
                action:"getHRDocSubCategoryById",
                hr_document_category_id:id,
              },
            success: function(response){
              $(".ajax-loader").hide();
              var userHtml ="";
              userHtml=`<option value="">-- Select --</option>`;
              
              $.each(response.sub_category, function( index, value ) {
                userHtml +=`<option value="`+value.hr_document_sub_category_id+`">`+value.hr_document_sub_category_name+`</option>`;
              });
              
             $('#hr_document_sub_category_id').html(userHtml);
            }
    })
}

function getAccessType(id) {
  $(".ajax-loader").show();
  $.ajax({
      url: "../residentApiNew/commonController.php",
      cache: false,
      type: "POST",
      data: {
          action:"getAccessTypeById",
          access_type_id:id,
        },
      success: function(response){
        $(".ajax-loader").hide();
        console.log(response.access_type);
        $('#access_mode').removeClass('d-none');
        accessForValue = `<option value="">-- Select Access For --</option> 
                                <option value="0">All </option> 
                                <option value="1">Branch Wise</option> 
                                <option value="2">Department Wise</option>
                                <option value="3">Employee Wise</option>`;
        if(response.access_type.is_access_type_changeable == 1){
          $('#access_mode').addClass('d-none');
        }
        if(response.access_type.access_type_for == 1){
          accessForValue = `<option value="">-- Select Access For --</option> 
                                <option value="0">All </option> 
                                <option value="1">Branch Wise</option> 
                                <option value="2">Department Wise</option>`;
        }
        $('#access_for').html(accessForValue);
      }
  })
}

$(".only5DecimalPoint").keyup(function (e) {

  value = $(this).val(); 
  var splitVal = value.split('.'); 
  var number = splitVal[0];
  var decimal = splitVal[1];
  if(decimal){
    if(decimal!=5){
      next_num = parseInt(number) + 1;
      swal("Please enter valid leave (Ex. " +number+", "+number+".5, or "+next_num+ ")");
      $(this).val(splitVal[0]+'.0');
    }
  }
  e.preventDefault();
});

function getEmployeeAccessType(id){
  //$(".ajax-loader").show();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getEmployeeAccessTypeById",
        user_id:id,
      },
    success: function(response){
      //$(".ajax-loader").hide();
      var main_content ="";
      main_content=`<option value="">-- Select Access Type --</option>`;
      
      $.each(response.access_type, function( index, value ) {
        main_content +=`<option value="`+value.access_type_id+`">`+value.access_type+`</option>`;
      });
      
      $('#access_type').html(main_content);
    }
  })
}

function deleteErnDedct(deleteValue) {
  var oTable = $(".example").dataTable();
  var oTable2 = $(".examples").dataTable();
  var val = [];
        $(".multiDelteCheckbox:checked", oTable.fnGetNodes()).each(function(i) {
          val[i] = $(this).val();
        });
        $(".multiDelteCheckbox:checked", oTable2.fnGetNodes()).each(function(j) {
          d = $(this).val();
          val.push(d);
        });
       
  if(val=="") {
    swal(
      'Warning !',
      'Please Select at least 1 item !',
      'warning'
    );
  } else {
    // alert(val);
  swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            // $('form.deleteForm'+id).submit();
        $.ajax({
            url: "controller/deleteController.php",
            cache: false,
            type: "POST",
            data: {ids : val,deleteValue
              :deleteValue},
            success: function(response){
              if(response==1) {
                document.location.reload(true);
                // history.go(-1);
              } else {
                document.location.reload(true);
                // history.go(0);
              }
            }
          });

           
          } else {
            // swal("Your data is safe!");
          }
        });
  }
}

function cancelEventDayBooking(events_day_id,event_day_name,event_date,event_id){
  $('#free_events_day_id').val(events_day_id);
  $('#free_event_day_name').val(event_day_name);
  $('#free_event_date').val(event_date);
  $('.free_event_day_name').html(event_day_name);
  $('.free_event_date').html(event_date);
  $('#free_event_id').val(event_id);
  $('#cancelEventDayBookingModal').modal();
}

function cancelPaidEventDayBooking(events_day_id,event_day_name,event_date,event_id){
  $('#paid_events_day_id').val(events_day_id);
  $('#paid_event_day_name').val(event_day_name);
  $('#paid_event_date').val(event_date);
  $('.paid_event_day_name').html(event_day_name);
  $('.paid_event_date').html(event_date);
  $('#paid_event_id').val(event_id);
  $('#cancelPaidEventDayBookingModal').modal();
}

function rescheduleEvent(event_date, event_time, events_day_id, event_id, event_day_name,event_title){
  $('.event_day_name').html(event_day_name);
  $('#event-start-date').val(event_date);
  $('#event_time').val(event_time);
  $('#rs_events_day_id').val(events_day_id);
  $('#rs_event_id').val(event_id);
  $('#rs_event_title').val(event_title);
  $('#rs_event_day_name').val(event_day_name);
  $('#rescheduleEventModal').modal();
}

function getFloorListGallery() {
  var blockIds= $("#block_id").val();
  $.ajax({
      url: 'getFloorListGallery.php',
      type: 'POST',
      data: {blockIds: blockIds},
    })
    .done(function(response) {
      $('#floor_id').html(response);
    });
}

function getAssetsItemByCategoryId(id){
  $(".ajax-loader").show();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
        action:"getAssetsItemByCategoryId",
        assets_category_id:id,
      },
    success: function(response){
      $(".ajax-loader").hide();
      var main_content ="";
      main_content=`<option value="">-- Select Assets Item --</option>`;
      
      $.each(response.assets_item, function( index, value ) {
        main_content +=`<option value="`+value.assets_id+`">`+value.assets_name+`</option>`;
      });
      
      $('#assets_id').html(main_content);
    }
  })
}

function assetsMaintenanceCompleted(assets_category_id,assets_id,assets_maintenance_id,date){
    $('#assetsMaintenanceCompletedModal').modal();
    $('#assets_category_id').val(assets_category_id);
    $('#assets_id').val(assets_id);
    $('#assets_maintenance_id').val(assets_maintenance_id);
    $('#date').val(date);
}

function markAsMaintenanceCompleted(assets_maintenance_complete_id){
  $('#markAsMaintenanceCompletedModal').modal();
  $('#assets_maintenance_complete_id').val(assets_maintenance_complete_id);
}

function getFloorListEvent() {
  var blockIds= $("#block_id").val();
  $.ajax({
      url: 'getFloorListGallery.php',
      type: 'POST',
      data: {blockIds: blockIds},
    })
    .done(function(response) {
      $('#floor_id').html(response);
    });
}

function getBookingANDAmountEventDetail(){
  var option = $('#events_day_id option:selected').attr('data-id');
  var user_id = $('#user_id').val();
  if(user_id !== null && user_id !== '') {
    if(option == 0){
      $('#payementDetailsDiv').html('');
      $('#addBankDetails').html('');
      $('#eventDetailsDiv').html('');
      getAmountDetail(1,0,0);
    }else{
      getBookEventDetail();
    }
  } else {
    $('#events_day_id').prop('selectedIndex', 0);
    swal("Please Select Booking For!");
  }
}



$('.group_type_change').on('change', function() {
  var group_type =$('.group_type_change').val();
  
  var csrf =$('input[name="csrf"]').val();
  $(".ajax-loader").show();
  $.ajax({
        url: "getAutoCreateGrouplist.php",
        cache: false,
        type: "POST",
        data: {group_type:group_type, csrf:csrf},
        success: function(response){
          $(".ajax-loader").hide();
          $('#getAutoGroupDiv').html(response);
         
        }
      });
});




function checkTransactionStatus(merchant_id,merchant_key,order_id,payment_mode,transection_id,no_of_month,package_id,logo,token,payment_txnid){
  
  $("#order_id").val(order_id);
  $("#transection_id").val(transection_id);
  $("#no_of_month").val(no_of_month);
  $("#package_id").val(package_id);
  $("#status").val("");
  $("#paymentStatus").text("");
  $("#paymentStatusBtn").hide();
  if(logo!="")
  {
    $("#paymentGatewayLogo").attr("src",logo);
  }else{
    $("#paymentGatewayLogo").css("display","none");
  }
  
  var payment_mode = payment_mode.toLowerCase();
  $(".ajax-loader").show();
  $.ajax({
    url: "controller/checkPaymentStatusController.php",
    cache: false,
    type: "POST",
    data: {merchant_id:merchant_id,merchant_key:merchant_key,order_id:order_id,payment_mode:payment_mode,transection_id:transection_id,no_of_month:no_of_month,package_id:package_id,payment_txnid:payment_txnid,token:token,csrf:csrf},
    success: function(response){
      $(".ajax-loader").hide();
      if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').
      replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
      replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
        // console.log(response);
        var obj = JSON.parse(response);
        if(payment_mode=="paytm" || payment_mode=="upi" || payment_mode=="upi_intent")
        {

          var status = obj.body.resultInfo.resultStatus;
          if(status!==undefined){
            if(status=="TXN_FAILURE"){
              paymentStatus="<i class='text-danger'>Invalid Order ID</i>";
              status="failed";
            }else if(status=="TXN_SUCCESS"){
               var receive_amount = obj.body['txnAmount'];
              paymentStatus="<i class='text-success'>Success (Received Amount : "+receive_amount+")</i>";
              status="success";
            }else if(status=="TXN_PENDING"){
              paymentStatus="<i class='text-warning'>Pending</i>";
              status="pending";
            } else if(status=="PENDING"){
              paymentStatus="<i class='text-warning'>Pending</i>";
              status="pending";
            } 
            $("#status").val(status);
            $("#paymentStatus").html(paymentStatus);
            $("#paymentStatusBtn").show();
          }
        }else if(payment_mode=="razorpay"){
         
            var status = obj.items[0]['status'];
            if(status=="failed"){
              paymentStatus="<i class='text-danger'>Failed</i>";
              status="failed";
            }else if(status=="captured"){
              var receive_amount = parseFloat(obj.items[0]['amount'])/100;
              paymentStatus="<i class='text-success'>Success (Received Amount : "+receive_amount+")</i>";
              status="success";
            }else {
              paymentStatus="<i class='text-warning'>Pending</i>";
              status="pending";
            } 
            $("#status").val(status);
            $("#paymentStatus").html(paymentStatus);
            $("#paymentStatusBtn").show();
            
          
        }else if(payment_mode=="payumoney"){
          
          if(status!='' && obj.status==0){
            var status = obj.result[0].postBackParam.status;
            if(status=="failure"){
              paymentStatus="<i class='text-danger'>Failed</i>";
              status="failed";
            }else if(status=="success"){
              var receive_amount = obj.result[0].postBackParam['amount'];
              paymentStatus="<i class='text-success'>Success (Received Amount : "+receive_amount+")</i>";;
              status="success";
            }else {
              paymentStatus="<i class='text-warning'>Pending</i>";
              status="pending";
            } 
            $("#status").val(status);
            $("#paymentStatus").html(paymentStatus);
            $("#paymentStatusBtn").show();
          } else if(obj.status==-1) {
            $("#paymentStatus").html("merchantTransactionIds does not exist");
          } else {
            $("#paymentStatus").html("Server Not Responding");
          }
        }
      //window.location.reload();
      }else{

        //the json is not ok
         $("#paymentStatus").html(response);
      }
    }
  });  
      
}

function updateTransactionStatus(){
  swal({
    title: "Are you sure?",
    text: "Update Transaction Status !",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $("#updateTransactionStatus").modal("hide");

      var order_id = $("#order_id").val();
      var transection_id = $("#transection_id").val();
      var no_of_month = $("#no_of_month").val();
      var package_id = $("#package_id").val();
      var status = $("#status").val();

      $.ajax({
        url: "controller/checkPaymentStatusController.php",
        cache: false,
        type: "POST",
        data: {order_id:order_id,status:status,transection_id:transection_id,no_of_month:no_of_month,package_id:package_id,updateStatus:"yes",csrf:csrf},
        success: function(response){
          var obj = JSON.parse(response);
          if(obj.status=="200")
          {
            swal("Successfully Updated !", {
              icon: "success",
            });
          }else{
            swal("Something Wrong !", {
              icon: "error",
            });
          }
          setTimeout(() => {
            window.location.reload();
          }, "2000");
          
        }
      });  
      
    } else {
      // swal("Your imaginary file is safe!");
    }
  });
}
//20220802

$("#admin-image-list" ).sortable({
    placeholder : "ui-state-highlight",
    update  : function(event, ui)
    {
      var imageIdsArray = new Array();
      var csrf =$('input[name="csrf"]').val();
      $('#admin-image-list .adminBox').each(function(){
        imageIdsArray.push($(this).data("post-id"));
        console.log(imageIdsArray);
      });
      $.ajax({
        url:"controller/changeOrderController.php",
        method:"POST",
        data:{order_change_id:imageIdsArray,changeOrderAdmin:'changeOrderAdmin',csrf:csrf},
        success:function(data)
        {
         if(data){
          swal("Order Changed Successfully", {
            icon: "success",
          });
         }else{
          swal("Order Changed Successfully", {
            icon: "error",
          });
         }
        }
      });
    }
  });

$("#branch-image-list" ).sortable({
    placeholder : "ui-state-highlight",
    update  : function(event, ui)
    {
      var imageIdsArray = new Array();
      var csrf =$('input[name="csrf"]').val();
      $('#branch-image-list .branchBox').each(function(){
        imageIdsArray.push($(this).data("post-id"));
        console.log(imageIdsArray);
      });
      $.ajax({
        url:"controller/changeOrderController.php",
        method:"POST",
        data:{order_change_id:imageIdsArray,changeOrderBranch:'changeOrderBranch',csrf:csrf},
        success:function(data)
        {
         if(data){
          swal("Order Changed Successfully", {
            icon: "success",
          });
         }else{
          swal("Order Changed Successfully", {
            icon: "error",
          });
         }
        }
      });
    }
  });

$(".department-image-list" ).sortable({
    placeholder : "ui-state-highlight",
    update  : function(event, ui)
    {
      var imageIdsArray = new Array();
      var csrf =$('input[name="csrf"]').val();
      $('.department-image-list .departmentBox').each(function(){
        imageIdsArray.push($(this).data("post-id"));
        console.log(imageIdsArray);
      });
      $.ajax({
        url:"controller/changeOrderController.php",
        method:"POST",
        data:{order_change_id:imageIdsArray,changeOrderDepartment:'changeOrderDepartment',csrf:csrf},
        success:function(data)
        {
         if(data){
          swal("Order Changed Successfully", {
            icon: "success",
          });
         }else{
          swal("Order Changed Successfully", {
            icon: "error",
          });
         }
        }
      });
    }
  });

 /* 28 Nov 2022 Shubham */
  function viewPersonalInfo(newData, oldData, user_id){
    main_content = 
    `<tr>`;
        var chnageMark = newData.blood_group==oldData.blood_group?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Blood Group</td>
        <td>`+oldData.blood_group+`</td>
        <td>`+newData.blood_group+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.gender==oldData.gender?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Gender</td>
        <td>`+oldData.gender+`</td>
        <td>`+newData.gender+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.intrest_hobbies==oldData.intrest_hobbies?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Intrest/Hobbies</td>
        <td>`+oldData.intrest_hobbies+`</td>
        <td>`+newData.intrest_hobbies+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.blood_group==oldData.blood_group?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Language Known</td>
        <td>`+oldData.language_known+`</td>
        <td>`+newData.language_known+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.marital_status==oldData.marital_status?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Marital Status</td>
        <td>`+oldData.marital_status+`</td>
        <td>`+newData.marital_status+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.member_date_of_birth==oldData.member_date_of_birth?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>DOB</td>
        <td>`+oldData.member_date_of_birth+`</td>
        <td>`+newData.member_date_of_birth+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.nationality==oldData.nationality?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Nationality</td>
        <td>`+oldData.nationality+`</td>
        <td>`+newData.nationality+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.professional_skills==oldData.professional_skills?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Professional Skills</td>
        <td>`+oldData.professional_skills+`</td>
        <td>`+newData.professional_skills+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.special_skills==oldData.special_skills?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Special Skills</td>
        <td>`+oldData.special_skills+`</td>
        <td>`+newData.special_skills+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.total_family_members==oldData.total_family_members?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Family Members</td>
        <td>`+oldData.total_family_members+`</td>
        <td>`+newData.total_family_members+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.wedding_anniversary_date==oldData.wedding_anniversary_date?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Anniversary Date</td>
        <td>`+oldData.wedding_anniversary_date+`</td>
        <td>`+newData.wedding_anniversary_date+`</td>
        <td>`+chnageMark+`</td>
    </tr>`
    $('.user_details_menu_type').val('1');
    $('.user_id').val(user_id);
    $('#new_data').val(JSON.stringify(oldData));
    $('#old_data').val(JSON.stringify(newData));
    $('#modalHeadName').html('Personal Info');
    $('#personalInfoData').html(main_content);
    $('#personalInfoModal').modal();
  }

  function viewContactDetail(newData, oldData, user_id){
    main_content = 
    `<tr>`;
        var chnageMark = newData.country_code_alt==oldData.country_code_alt?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Country Code For Alt. Mobile</td>
        <td>`+oldData.country_code_alt+`</td>
        <td>`+newData.country_code_alt+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.alt_mobile==oldData.alt_mobile?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Alt. Mobile</td>
        <td>`+oldData.alt_mobile+`</td>
        <td>`+newData.alt_mobile+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.country_code_emergency==oldData.country_code_emergency?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Country Code For Emergency</td>
        <td>`+oldData.country_code_emergency+`</td>
        <td>`+newData.country_code_emergency+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.emergency_number==oldData.emergency_number?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Emergency Number</td>
        <td>`+oldData.emergency_number+`</td>
        <td>`+newData.emergency_number+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.country_code_whatsapp==oldData.country_code_whatsapp?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Country Code For Whatsapp</td>
        <td>`+oldData.country_code_whatsapp+`</td>
        <td>`+newData.country_code_whatsapp+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.whatsapp_number==oldData.whatsapp_number?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Whatsapp Number</td>
        <td>`+oldData.whatsapp_number+`</td>
        <td>`+newData.whatsapp_number+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.last_address==oldData.last_address?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Last Address</td>
        <td>`+oldData.last_address+`</td>
        <td>`+newData.last_address+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.permanent_address==oldData.permanent_address?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Permanent Address</td>
        <td>`+oldData.permanent_address+`</td>
        <td>`+newData.permanent_address+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.personal_email==oldData.personal_email?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>Personal Email</td>
        <td>`+oldData.personal_email+`</td>
        <td>`+newData.personal_email+`</td>
        <td>`+chnageMark+`</td>
    </tr>
    <tr>`;
        var chnageMark = newData.user_email==oldData.user_email?'<span class="text-success">No Chnage</span>':'<span class="text-danger">Chnage</span>';
        main_content += 
        `<td>User Email</td>
        <td>`+oldData.user_email+`</td>
        <td>`+newData.user_email+`</td>
        <td>`+chnageMark+`</td>
    </tr>`;
    $('.user_details_menu_type').val('0');
    $('.user_id').val(user_id);
    $('#new_data').val(JSON.stringify(oldData));
    $('#old_data').val(JSON.stringify(newData));
    $('#modalHeadName').html('Contact Detail');
    $('#personalInfoData').html(main_content);
    $('#personalInfoModal').modal();
  }
  
  function changeMenuAccessTypeSetting(value, id) {
    $(".ajax-loader").show();
    $.ajax({
          url: "../residentApiNew/commonController.php",
          cache: false,
          type: "POST",
          data: {action:"menuAccessTypeSetting",
          access_type : value,
          profile_menu_id  : id,
          csrf:csrf},
          success: function(response){
            $(".ajax-loader").hide();
            if(response==1)
            {
              swal("Access Type Changed",{
                icon: "success",
              });
              document.location.reload(true);
            }
            else
            {
              document.location.reload(true);
              swal("Something Wrong!",{
                icon: "error",
              });
            }
          }
       });
  }

  function checkedDelete(dimensional_id){
    if($("#checkbox_"+dimensional_id).prop("checked") == true){
      $("#removeDimensional_"+dimensional_id).removeClass('onStepPercent');
      $("#removeDimensional_"+dimensional_id).prop('disabled',true);
      $("#removeDimensionalName_"+dimensional_id).prop('disabled',true);
      $("#removeDimensionalImage_"+dimensional_id).prop('disabled',true);
      $("#removeDimensionalImageOld_"+dimensional_id).prop('disabled',true);
      $("#removeDimensionalId_"+dimensional_id).prop('disabled',true);
    }else{
      $("#removeDimensional_"+dimensional_id).addClass('onStepPercent');
      $("#removeDimensional_"+dimensional_id).prop('disabled',false);
      $("#removeDimensionalName_"+dimensional_id).prop('disabled',false);
      $("#removeDimensionalImage_"+dimensional_id).prop('disabled',false);
      $("#removeDimensionalImageOld_"+dimensional_id).prop('disabled',false);
      $("#removeDimensionalId_"+dimensional_id).prop('disabled',false);
    }

    var val2 = 0;
    
     $('.onStepPercent').each(function(){
          val2+=(parseFloat($(this).val()) || 0);
      });
    $('#totalDimensionalWeightage').val(val2);

    if(val2>100 || val2<100){
      $(".totalDimensionalWeightageMsg").html('Total Dimensional Weightage Should Be 100 % ');
    }else{
      $(".totalDimensionalWeightageMsg").html('');
    }
  }

  $(document).on('keyup',".onStepPercent", function(e){
        
    var val2 = 0;
    $('.onStepPercent').each(function(){
        val2+=(parseFloat($(this).val()) || 0);
    });
    $('#totalDimensionalWeightage').val(val2);

    if(val2>100 || val2<100){
      
      $(".totalDimensionalWeightageMsg").html('Total Dimensional Weightage Should Be 100 % ..');
    }else{
      $(".totalDimensionalWeightageMsg").html('');
    }
    
  });

  /* 07 DEC 2022 SHUBHAM */
  function getAllGroupLeaveTypeOption(leave_group_id, user_id, floor_id, currentYear, nextYear, previousYear, leave_year){
    $.ajax({
      url: "getAllGroupLeaveTypeList.php",
      cache: false,
      type: "POST",
      data: {
          leave_group_id:leave_group_id,
          user_id:user_id,
          floor_id:floor_id,
          currentYear:currentYear,
          nextYear:nextYear,
          previousYear:previousYear,
          leave_year:leave_year,
        },
      success: function(response){
        $('#show-all-group-leave').html(response);
      }
  });
  }

function changeStatusLetterSetting(cs,status)
{
  swal({
    title: "Are you sure?",
    text: "You want to change status!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  })
  .then((willDelete) =>
  {
    if(willDelete)
    {
      var csrf =$('input[name="csrf"]').val();
      $(".ajax-loader").show();
      $.ajax({
        url: "controller/letterController.php",
        cache: false,
        type: "POST",
        data: {status:status,changeLetterSettingStatus:"changeLetterSettingStatus",csrf:csrf},
        success: function(response)
        {
          $(".ajax-loader").hide();
          if(response == 1)
          {
            swal("Status Changed",{
              icon: "success",
            });
          }
          else
          {
            swal("Something Wrong!",{
              icon: "error",
            });
          }
          document.location.reload(true);
        }
      });
    }
  });
}

function deleteHeaderImage(header_image)
{
  swal({
    title: "Are you sure?",
    text: "You want to delete header image!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  })
  .then((willDelete) =>
  {
    if(willDelete)
    {
      var csrf =$('input[name="csrf"]').val();
      $(".ajax-loader").show();
      $.ajax({
        url: "controller/letterController.php",
        cache: false,
        type: "POST",
        data: {deleteHeaderImage:"deleteHeaderImage",header_image:header_image,csrf:csrf},
        success: function(response)
        {
          $(".ajax-loader").hide();
          if(response == 1)
          {
            swal("Header Image Deleted",{
              icon: "success",
            });
          }
          else
          {
            swal("Something Wrong!",{
              icon: "error",
            });
          }
          document.location.reload(true);
        }
      });
    }
  });
}

function deleteFooterImage(footer_image)
{
  swal({
    title: "Are you sure?",
    text: "You want to delete footer image!",
    icon: "warning",
    buttons: true,
    dangerMode: true
  })
  .then((willDelete) =>
  {
    if(willDelete)
    {
      var csrf =$('input[name="csrf"]').val();
      $(".ajax-loader").show();
      $.ajax({
        url: "controller/letterController.php",
        cache: false,
        type: "POST",
        data: {deleteFooterImage:"deleteFooterImage",footer_image:footer_image,csrf:csrf},
        success: function(response)
        {
          $(".ajax-loader").hide();
          if(response == 1)
          {
            swal("Footer Image Deleted",{
              icon: "success",
            });
          }
          else
          {
            swal("Something Wrong!",{
              icon: "error",
            });
          }
          document.location.reload(true);
        }
      });
    }
  });
}