




var product_id_arr = [];
$("#addRequirements").validate({
  rules: {
    product_category_id: {
      required: true,
    },
   /*  product_sub_category_id: {
      required: true,
    }, */
    product_id: {
      required: true,
    },
    quantity: {
      required: true,
    }
  },
  messages: {
    product_category_id: {
      required: "Please select category"
    },
   /*  product_sub_category_id: {
      required: "Please select sub category"
    }, */
    product_id: {
      required: "Please select product"
    },
    quantity: {
      required: "Please select quantity"
    }
  },
  errorPlacement: function (error, element) {
    if (element.parent('.input-group').length) {
      error.insertAfter(element.parent());      // radio/checkbox?
    } else if (element.hasClass('select2-hidden-accessible')) {
      error.insertAfter(element.next('span'));  // select2
      element.next('span').addClass('error').removeClass('valid');
    } else {
      error.insertAfter(element);               // default
    }
  },
  submitHandler: function (form) {
    var product_id = $('#product_id').val();
    var purchase_product_id = $('#purchase_product_id').val();
    var quantity = $('#quantity').val();
    var product_old_id = $('#product_old_id').val();
    var key = $('#key').val();
    console.log(key);
    //product_id_arr[product_id]=quantity; 
    if (key == "add") {
      product_id_arr[product_id] = quantity;
      showPurchaseRequirements(product_id_arr);
      $('#requirements').val(product_id_arr);
      console.log(product_id_arr);
    } else if (key == "editPurchaseProduct") {
      $.ajax({
        url: "../residentApiNew/commonController.php",
        cache: false,
        type: "POST",
        data: {
          action: "updatePrchaseProductById",
          purchase_product_id: purchase_product_id,
          quantity: quantity,
        },
        success: function (response) {
          $('#addRequirementsModal').modal('toggle');
          location.reload(0);
        }
      });
    }
    else if (key == "edit") {
     
      if (product_old_id != product_id) {
        delete product_id_arr[product_old_id];
      }
      product_id_arr[product_id] = quantity;
      showPurchaseRequirements(product_id_arr);
      $('#requirements').val(product_id_arr);
    }
    else {
      
      if (product_old_id != product_id) {
        delete product_id_arr[product_old_id];
      }
      product_id_arr[product_id] = quantity;
      showPurchaseRequirements(product_id_arr);
      $('#requirements').val(product_id_arr);
      console.log(product_id_arr);
    }

    $('#addRequirements')[0].reset();
    $("#product_category_id").val('').trigger('change.select2');
    $("#product_sub_category_id").val('').trigger('change.select2');
    $("#product_id").val('').trigger('change.select2');
  }
});

function showPurchaseRequirements(products) {
  main_content = "";
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "getProductByIds",
      product_ids: products
    },
    success: function (response) {

      if (response.status == 200) {
        $.each(response.product, function (index, value) {
          main_content += `<tr>
                            <td>`+ value.product_name + `</td>
                            <td>`+ value.category_name + `</td>
                            <td>`+ value.sub_category_name + `</td>
                            <td>`+ value.unit_measurement_name + `</td>
                            <td>`+ value.quantity + `</td>
                            <td>
                              <button type="button" class="btn btn-sm btn-primary ml-1" onclick="editRequirements('`+ value.product_id + `', '` + value.quantity + `')"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-sm btn-danger form-btn deleteRequirements" data_id="`+ value.product_id + `"><i class="fa fa-trash-o"></i></button>
                            </td>
                        </tr>`;
        });

        $('.purchase_requirements').html(main_content);
        $('#addRequirementsModal').modal('hide');

      }

    }

  });
}

function addRequirements() {
  var site_id = $('#site_id').val();
  var vendor_id = $('#vendor_id').val();
  var po_number = $('#po_number').val();
  if (site_id != '' && vendor_id != '' ) {
    $('#key').val('add');
    $('#addRequirements')[0].reset();
    //$('#product_category_id').select2('refresh');
    $("#product_category_id").val('').trigger('change.select2');
    $("#product_sub_category_id").val('').trigger('change.select2');
    $("#product_id").val('').trigger('change.select2');
    $('#addRequirementsModal').modal();
  } else {
    if(site_id != ''){
      swal(
        'Warning !',
        'Please Select Vendor',
        'warning'
      );
    }
    else if(vendor_id !="")
    {
      swal(
        'Warning !',
        'Please Select  Site !',
        'warning'
      );
    }
    else
    {
      swal(
        'Warning !',
        'Please Select Site, Vendor !',
        'warning'
      );
    }
    
  }
}

function getSubCategoryById(product_category_id, product_sub_category_id = '') {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "getSubCategory",
      product_category_id: product_category_id,
    },
    success: function (response) {
      if(response.status !=201)
      {
        var main_content = "";
        main_content = `<option value="">-- Select --</option>`;
        $.each(response.sub_category, function (index, value) {
          var selected = "";
          if (product_sub_category_id == value.product_sub_category_id) {
            var selected = "selected";
          }
          main_content += `<option ` + selected + ` value="` + value.product_sub_category_id + `">` + value.sub_category_name + `</option>`;
        });
  
        $('#product_sub_category_id').html(main_content);
        if(response.products.length>0){
          var main_content = "";
          main_content = `<option value="">-- Select --</option>`;
          $.each(response.products, function (index, value) {
            var selected = "";
            if (value.product_id == product_id) {
              var selected = "selected";
            }
            main_content += `<option ` + selected + ` value="` + value.product_id + `">` + value.product_name + `</option>`;
          });
  
          $('#product_id').html(main_content);
        }
      }
      
       
    }
  })
}


function getProductByCategoryAndSubCategoryId(product_sub_category_id, product_id = '') {
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "getProductByCategoryAndSubCategoryId",
      //product_category_id:product_category_id,
      product_sub_category_id: product_sub_category_id,
    },
    success: function (response) {
      var main_content = "";
      main_content = `<option value="">-- Select --</option>`;
      $.each(response.product, function (index, value) {
        var selected = "";
        if (value.product_id == product_id) {
          var selected = "selected";
        }
        main_content += `<option ` + selected + ` value="` + value.product_id + `">` + value.product_name + `</option>`;
      });

      $('#product_id').html(main_content);
    }
  })
}

function editRequirements(id, quantity) {
  $('.hideupdate').show();
  $('.hideAdd').hide();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "getProduct",
      product_id: id,
    },
    success: function (response) {
      $('#addRequirementsModal').modal();
      $('#quantity').val(quantity);
      $('#product_category_id').val(response.product.product_category_id);
      $('#product_category_id').select2();
      getSubCategoryById(response.product.product_category_id, response.product.product_sub_category_id);
      $('#product_sub_category_id').select2();
      getProductByCategoryAndSubCategoryId(response.product.product_sub_category_id, response.product.product_id);
      $('#product_id').select2();
      $('#key').val('edit');
      $('#product_old_id').val(id);
    }
  });

}

$(document).on('click', '.deleteRequirements', function (e) {
  console.log("before");
  console.log(product_id_arr);
  var product_id = $(this).attr('data_id');
  delete product_id_arr[product_id];
  console.log("after");

  console.log(product_id_arr);
  showPurchaseRequirements(product_id_arr);
  document.getElementById("showFilterData").deleteRow(this);
  e.preventDefault();


});

function DeletePurchaseProduct(id) {
purchase_id =$('#purchase_id').val();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      puchase_product_id: id,
      action: "deletepurchaseProduct"
    },
    success: function (response) {
      console.log(response);
     // alert("sdfdfj");
      window.location.replace("addPurchase?purchase_id="+purchase_id);
      /* if (response == 1) {
        ///location.reload(0);
      } */
    }
  });
}

function editPurchaseProduct(id) {
  $('.hideupdate').show();
  $('.hideAdd').hide();
  $.ajax({
    url: "../residentApiNew/commonController.php",
    cache: false,
    type: "POST",
    data: {
      action: "getPurhaseProductById",
      purchase_id: id,
    },
    success: function (response) {
      $('#addRequirementsModal').modal();
      $('#quantity').val(response.PurchaseProduct.quantity);

      $('#product_category_id').val(response.PurchaseProduct.product_category_id);
      $('#purchase_product_id').val(response.PurchaseProduct.puchase_product_id);
      $('#product_category_id').select2();
      getSubCategoryById(response.PurchaseProduct.product_category_id, response.PurchaseProduct.product_sub_category_id);
      $('#product_sub_category_id').select2();
      getProductByCategoryAndSubCategoryId(response.PurchaseProduct.product_sub_category_id, response.PurchaseProduct.product_id);
      $('#product_id').select2();
      $('#key').val('editPurchaseProduct');
      $('#product_old_id').val(id);
    }
  });

}