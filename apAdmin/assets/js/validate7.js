$().ready(function() {

    $.validator.addMethod("customemail", function(value, element) {
        return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
        }, 
        "Sorry, I've enabled very strict email validation"
    );

    $.validator.addMethod("pwcheck", function(value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
           && /[A-Z]/.test(value) // has a lowercase letter
           && /\d/.test(value) // has a digit
    });

    $.validator.addMethod("addressCheck", function(value) {
        /*alert(value);*/
        /*alert((/^[A-Za-z ]+/.test(value) || value==""  ))*/
         return /^[A-Za-z 0-9\d=!,\n\-@._*]*$/.test(value) // consists of only these
            && (/\w*[a-zA-Z ]\w*/.test(value) || value==''  )// has a lowercase letter
           // && /\d/.test(value) // has a digit
    });

    jQuery.validator.addMethod("acceptName", function(value, element, param) {
        return value.match(new RegExp("." + param + "$"));
    });


    $.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z ]*$/);
    }, jQuery.validator.format("Please enter a Valid Name."));

    $.validator.addMethod("alphaRestSpeChartor", function(value, element) {
        return this.optional(element) || value == value.match(/^[A-Za-z 0-9\d=!,:\n\-@&()/?%._*]*$/);
    }, jQuery.validator.format("special characters not allowed"));


    $.validator.addMethod("noSpace", function(value, element) { 
        return value == '' || value.trim().length != 0;  
    }, "No space please and don't leave it empty");

    $.validator.addMethod('minStrict', function (value, el, param) {
        return value >= param;
    });

    $.validator.addMethod('regexp', function(value, element, param) {
        return this.optional(element) || value.match(param);
    },'Please enter a valid GST no.');

    jQuery.validator.addMethod("greaterThan", 
        function(value, element, params) {

            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) > new Date($(params).val());
            }

            return isNaN(value) && isNaN($(params).val()) 
            || (Number(value) > Number($(params).val())); 
    },'Must be greater than {0}.');

    jQuery.validator.addMethod("image", function (value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    }, jQuery.validator.format("Please add a valid image file."));


    $('input[type="file"]').change(function(){
        $(this).valid()
    });

    //IS_639 divya
    $.validator.addMethod('filesize', function (value, element, arg) {
        var size =3000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }

    }, $.validator.format("file size must be less than or equal to 3MB."));

    //21FEB2020
    $.validator.addMethod('filesize1MB', function (value, element, arg) {
        var size =1097152;
        if(element.files.length){
            if(element.files[0].size<size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than  1MB."));

    $.validator.addMethod('MaxUploadFile', function (value, element, arg) {
        var FIlelength = 20;

        if(element.files.length <= FIlelength){

            return true;
        }else{
            return false;
        }
    }, $.validator.format("You are allowed to upload only maximum 20 files at a time"));

    //21FEB2020


    //24feb2020
    $.validator.addMethod("extension", function(value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    },$.validator.format("Please select a file with a valid extension (png,jpeg,jpg,gif)."));

    $.validator.addMethod("extensionDoc", function(value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif|pdf";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    }, $.validator.format("Please select a file with a valid extension (png,jpeg,jpg,gif,pdf)."));

    $.validator.addMethod("extensionDocumentNew", function(value, element, param) {
    param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|pdf";
    return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    },$.validator.format("Please select a file with a valid extension (png,jpeg,jpg,pdf)."));


    $.validator.addMethod('filesize4MB', function (value, element, arg) {
        var size =4000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 4MB."));
    //24feb2020
    $.validator.addMethod('filesize2MB', function (value, element, arg) {
        var size =2000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 2MB."));

    /////2022-03-25
    $.validator.addMethod('filesize8MB', function (value, element, arg) {
        var size =8388608;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 8MB."));

    $.validator.addMethod("extensionDocument", function(value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|txt|xls|csv|pdf|docx|odt|ods|xlsx";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    },$.validator.format("Please select a file with a valid extension (png,jpeg,jpg,txt,xls,csv,pdf,docx,odt,ods,xlsx)."));

    $.validator.addMethod('compare', function (value, element, param) {
        return this.optional(element) || parseInt(value) > 0 || parseInt($(param).val()) > 0;
    }, $.validator.format('Invalid value'));


    //19march2020 eve
    $.validator.addMethod('filesize12MB', function (value, element, arg) {
        var size =12000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 12MB."));

    //19march2020 eve
    $.validator.addMethod('filesize32MB', function (value, element, arg) {
        var size =32000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 32MB."));
 //19march2020

    $("#personal-info").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        }
    });
    $("#personal-info1").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            parking_name:
            {
                required: true,
                noSpace: true/*,
                remote:
                {
                    url: 'UniqueParkingName.php',
                    type: "post",
                    data:
                    {
                        oldParkingName: function()
                        {
                            return $('#personal-info1 :input[name="parking_name"]').val();
                        },
                        society_parking_id: function()
                        {
                            return $('#personal-info1 :input[name="society_parking_id"]').val();
                        },
                        society_id: function()
                        {
                            return $('#personal-info1 :input[name="society_id"]').val();
                        }
                        ,
                        parking_id: function()
                        {
                            return $('#personal-info1 :input[name="parking_id"]').val();
                        }
                    }
                }*/
            }
        },
        messages: {
            parking_name: {
            //2march2020 -new
            required: "Please enter Parking Name"
            // remote: "Parking Name is already exists, please use another parking name to avoid confusion"
            },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit();
          }
     }
    //28feb2020
    });




    //IS_914  //2march2020 -new
    $("#bookFacilityFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            payment_type: {
                required: true,
                noSpace:true 
            },
            unit_id_main:{
                required:true
            }, 
            booked_date:{
                required:true
            },
            payment_bank:{
                required:{
                    depends: function(element) {
                      return $("#payment_type").val()=="1"  || $("#payment_type").val()=="2" ;
                    }
                },
                 noSpace:true 
            },
            payment_number:{
                required:{
                    depends: function(element) {
                      return $("#payment_type").val()=="1"  || $("#payment_type").val()=="2" ;
                    }
                },
                 noSpace:true 
            }
        },
        messages: {
            payment_type: {
                required: "Please select Payment Type", 
                },
            unit_id_main:{
                required: "Please Select booking for"
            },
            booked_date:{
                required:"please select booking date"
            },
            payment_bank:{
                required:"Please enter bank name"
            },
            payment_number:{
                required:"Please enter reference number"
            }
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
         
    });
    $("#payFacilityFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            payment_type: {
                required: true,
                noSpace:true 
            } ,
            payment_bank:{
                required:{
                    depends: function(element) { 
                      return $('#payFacilityFrm :input[name="payment_type"]').val() =="1"  || $('#payFacilityFrm :input[name="payment_type"]')=="2" ;
                    }
                },
                 noSpace:true 
            },
            payment_number:{
                required:{
                    depends: function(element) {
                       return $('#payFacilityFrm :input[name="payment_type"]').val() =="1"  || $('#payFacilityFrm :input[name="payment_type"]')=="2" ;
                  }
                },
                 noSpace:true 
            }
        },
        messages: {
            payment_type: {
                required: "Please select Payment Type", 
            },
            
            payment_bank:{
                required:"Please enter bank name"
            },
            payment_number:{
                required:"Please enter reference number"
            }
         },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
     
    });
    
    //IS_914 //2march2020 -new




    
    $("#personal-info2").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        }
    });
    $("#personal-info3").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules:{
            employment_type:{
                required:false,
            },
            business_categories:{
                required:false,
            },
            business_categories_sub:{
                required:false,
            },
            company_name:{
                required:true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            designation:{
                required:true,
                noSpace:true,
            },
            company_address:{
                noSpace:true,
            },
            company_contact_number:{
                required:false,
                digits: true,
                maxlength: 15,
                minlength: 8,
            },
            employment_type:{
                noSpace:true,
            },
            joining_date:{
                noSpace:true,
            },
            total_experience:{
                noSpace:true,
            },
            shift_timing:{
                noSpace:true,
            },
            job_location:{
                noSpace:true,
            },
            intrest_hobbies:{
                noSpace:true,
            },
            professional_skills:{
                noSpace:true,
            },
            special_skills:{
                noSpace:true,
            },
            language_known:{
                noSpace:true,
            },
            /* shift_time_id:{
                required:true,
                noSpace:true,
            } */
        },  
        messages: {
            designation: "Please enter designation"
           
        },
        /* shift_time_id: {
            required: "Please Select Timing"
        }, */
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $(".common-form").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        }
    });
    $("#signupForm").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            firstname: "required",
            lastname: "required",
            username: {
                required: true,
                minlength: 2,
                alpha:true
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true,
                customemail:true,
            },
            contactnumber: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            topic: {
                required: "#newsletter:checked",
                minlength: 2
            },
            agree: "required"
        },
        messages: {
            firstname: "Please enter your firstname",
            lastname: "Please enter your lastname",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: {
                customemail: "Please enter a valid email address",
                required: "Please enter  email address",
            },            contactnumber: "Please enter valid number",
            agree: "Please accept our policy",
            topic: "Please select at least 2 topics"
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                // $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#addUser").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            user_first_name_add: {
                required: true,
                alpha: true,
                noSpace:true,
            },
             company_name: {
                required: true,
                noSpace:true,
            },
            user_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
             designation: {
                required: true,
                noSpace:true,
            },
            user_email: {
                email:true
            },
            user_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            family_user_mobile: {
                minlength: 8,
                maxlength:15

            },
            member_relation_name_other: {
                minlength: 2,
                maxlength:15,
                required: true,
                noSpace:true,

            },
            owner_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            tenant_doc: {
               filesize4MB: true

            },
            prv_doc: {
               filesize4MB: true
            },
            company_employee_id: {
                noSpace:true
            },
            facebook: {
                noSpace:true
            },
            linkedin: {
                noSpace:true
            },
            twitter: {
                noSpace:true
            },
            instagram: {
                noSpace:true
            },
            floor_id: {
                required:true
            },
            designation: {
                required:true
            },
            nationality: {
                required: false,
                alpha: true,
                noSpace:true,
            },
            
        },
        messages: {
            company_name: {
                required : "Please enter company name",
                noSpace: "Space Not Allowed",
            },
            designation: {
                required : "Please enter designation",
            },
             user_first_name_add: {
                required : "Please enter first name",
                noSpace: "Space Not Allowed",
            },
            user_first_name: {
                required : "Please enter first name",
                noSpace: "Space Not Allowed",
            },
            floor_id: {
                required : "Please Select  Department",
                
            },
            user_last_name: {
                required : "Please enter last name",
                noSpace: "Space Not Allowed",
            },
            member_relation_name_other: {
                required : "Please enter other relation name",
                noSpace: "Space Not Allowed",
            },
            owner_first_name: {
                required : "Please enter first name",
                noSpace: "Space Not Allowed",
            },
            owner_last_name: {
                required : "Please enter last name",
                noSpace: "Space Not Allowed",
            },
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            user_email: {
                email: "Please enter a valid email address",
                
                
            },
            user_mobile: "Please enter valid mobile number",
            family_user_mobile: "Please enter valid mobile number",
            owner_mobile: "Please enter valid mobile number",
            nationality: {
                alpha: "Please enter characters only"
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    $("#addUserTransfer").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            user_first_name_new_owner: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_email: {
                required: true,
                customemail: true,
            },
            user_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            family_user_mobile: {
                minlength: 8,
                maxlength:15

            },
            member_relation_name_other: {
                minlength: 2,
                maxlength:15,
                required: true,
                noSpace:true,

            },
            owner_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            user_profile_pic: {
               filesize2MB: true

            },
             tenant_doc: {
               filesize4MB: true

            },
            prv_doc: {
               filesize4MB: true

            }
            
        },
        messages: {
            user_first_name_new_owner: {
                required : "Please enter firstname",
                noSpace: "Space Not Allowed",
            },
            user_first_name: {
                required : "Please enter firstname",
                noSpace: "Space Not Allowed",
            },
            user_last_name: {
                required : "Please enter lastname",
                noSpace: "Space Not Allowed",
            },
            member_relation_name_other: {
                required : "Please enter other relation name",
                noSpace: "Space Not Allowed",
            },
            owner_first_name: {
                required : "Please enter firstname",
                noSpace: "Space Not Allowed",
            },
            owner_last_name: {
                required : "Please enter lastname",
                noSpace: "Space Not Allowed",
            },
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            
            user_email: {
                customemail: "Please enter a valid email address",
                required: "Please enter  email address",
            },            user_mobile: "Please enter valid mobile number",
            family_user_mobile: "Please enter valid mobile number",
            owner_mobile: "Please enter valid mobile number"

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    $("#addUserCommercial").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            phone: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            password: {
                minlength: 5,
                maxlength:15

            },
           
            
        },
        messages: {
            name: {
                required : "Please enter name",
                noSpace: "Space Not Allowed",
            },
            password: {
                required : "Please enter firstname",
                noSpace: "Space Not Allowed",
            },
            phone: "Please enter valid mobile number",

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    $("#addNoteForm").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            note_title: {
                required: true,
                noSpace:true,
            },
            note_description: {
                required: true,
                noSpace:true,
                minlength: 1,
                maxlength:500

            }
           
            
        },
        messages: {
            note_title: {
                required : "Please enter title",
                noSpace: "Space Not Allowed",
            },
            note_description: {
                required : "Please enter description",
                noSpace: "Space Not Allowed",
            },
            phone: "Please enter valid mobile number",

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


     $("#addEducationForm").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            status_type: {
                required: true
            },
            class_name: {
                required: true,
                noSpace:true,
                minlength: 3,
                maxlength:50
            },
            university_name: {
                required: true,
                noSpace:true,
                minlength: 3
            },
            pass_year: {
                required: true
            }
            
        },
        messages: {
            status_type: {
                required : "Please select type"
            },
            class_name: {
                noSpace: "Space Not Allowed"
            },
            university_name: {
                noSpace: "Space Not Allowed"
            }
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    $("#addExperienceForm").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            company_name: {
                required: true,
                noSpace:true
            },
            designation: {
                required: true,
                noSpace:true
            },
            work_from: {
                required: true
            },
            work_to: {
                required: true
            },
            company_location: {
                required: true,
                noSpace:true
            }
           
        },
        messages: {
            company_name: {
                required : "Please enter company name"
            },
            designation: {
                required : "Please enter designation"
            },
            work_from: {
                required : "Please select from date"
            },
            work_to: {
                required : "Please select to date"
            },
            company_location: {
                required : "Please enter company location"
            }
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#addNewExperienceForm").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            
            designation: {
                required: true,
                noSpace:true
            },
            joining_date: {
                required: true
            },
            end_date: {
                required: true
            },
            block_id: {
                required: true
            },
            floor_id: {
                required: true
            },
            user_id: {
                required: true
            },
          
        },
        messages: {
            
            designation: {
                required : "Please enter designation"
            },
            joining_date: {
                required : "Please select from date"
            },
            end_date: {
                required : "Please select to date"
            },
            block_id: {
                required : "Please select Branch"
            },
            floor_id: {
                required : "Please select Department"
            },
            user_id: {
                required : "Please select Employee"
            },
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    
    $("#addUser").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            user_first_name_add: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_email: {
                required: true,
                customemail: true,
            },
            user_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            owner_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            tenant_doc: {
               filesize4MB: true

            },
            prv_doc: {
               filesize4MB: true

            }
            
        },
        messages: {
            user_first_name_add: {
                required : "Please enter first name",
                noSpace: "Space Not Allowed",
            },
            user_first_name: {
                required : "Please enter first name",
                noSpace: "Space Not Allowed",
            },
            user_last_name: {
                required : "Please enter last name",
                noSpace: "Space Not Allowed",
            },
            owner_first_name: {
                required : "Please enter first name",
                noSpace: "Space Not Allowed",
            },
            owner_last_name: {
                required : "Please enter last name",
                noSpace: "Space Not Allowed",
            },
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            
            user_email: {
                customemail: "Please enter a valid email address",
                required: "Please enter  email address",
            },
            user_mobile: "Please enter valid mobile number",
            owner_mobile: "Please enter valid mobile number"

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    $("#addFamilyForm").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            family_user_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
             member_relation_name_other: {
                required: true,
                noSpace:true,
            },
            owner_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_email: {
                required: true,
                customemail: true,
            },
            user_mobile: {
                required: false,
                minlength: 8,
                maxlength:15

            },
            owner_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            tenant_doc: {
               filesize4MB: true

            },
            prv_doc: {
               filesize4MB: true

            }
            
        },
        messages: {
            user_first_name_add: {
                required : "Please enter first name",
                noSpace: "Space Not Allowed",
            },
            user_first_name: {
                required : "Please enter first name",
                noSpace: "Space Not Allowed",
            },
            member_relation_name_other: {
                required : "Please enter valid name",
                noSpace: "Space Not Allowed",
            },
            user_last_name: {
                required : "Please enter last name",
                noSpace: "Space Not Allowed",
            },
            owner_first_name: {
                required : "Please enter first name",
                noSpace: "Space Not Allowed",
            },
            owner_last_name: {
                required : "Please enter last name",
                noSpace: "Space Not Allowed",
            },
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            
            user_email: {
                customemail: "Please enter a valid email address",
                required: "Please enter  email address",
            },
            user_mobile: "Please enter valid mobile number",
            owner_mobile: "Please enter valid mobile number"

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    $("#tenantForm").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            company_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
             user_first_name_owner: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
             last_addresss: {
                // alpha: true,
                noSpace:true,
            },
            user_email: {
                required: true,
                customemail:true,
            },
            user_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            owner_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            tenant_doc: {
               filesize4MB: true

            },
            prv_doc: {
               filesize4MB: true

            }
            
        },
        messages: {
            company_name: {
                required : "Please enter company name",
                noSpace: "Space Not Allowed",
            },
             user_first_name_owner: {
                required : "Please enter firstname",
                noSpace: "Space Not Allowed",
            },
            user_last_name: {
                required : "Please enter lastname",
                noSpace: "Space Not Allowed",
            },
            owner_first_name: {
                required : "Please enter firstname",
                noSpace: "Space Not Allowed",
            },
            owner_last_name: {
                required : "Please enter lastname",
                noSpace: "Space Not Allowed",
            },
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            
            user_email: {
                customemail:"Please enter a valid email address",
                required:"Please enter email address",
            },
            user_mobile: {
                required: "Please enter valid mobile number",
            },
            owner_mobile: "Please enter valid mobile number"

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#ownerAgain").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            user_first_name_owner_again: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_first_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            owner_last_name: {
                required: true,
                alpha: true,
                noSpace:true,
            },
            user_email: {
                minlength: 2,
                required: true,
                customemail:true,
            },
            user_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            owner_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            }
            
        },
        messages: {
            user_first_name_owner_again: {
                required : "Please enter firstname",
                noSpace: "Space Not Allowed",
            },
            user_last_name: {
                required : "Please enter lastname",
                noSpace: "Space Not Allowed",
            },
            owner_first_name: {
                required : "Please enter firstname",
                noSpace: "Space Not Allowed",
            },
            owner_last_name: {
                required : "Please enter lastname",
                noSpace: "Space Not Allowed",
            },
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            
            user_email: {
                required: "Please enter  email",
                customemail:"Please enter a valid email address"
            },
            user_mobile: "Please enter valid mobile number",
            owner_mobile: "Please enter valid mobile number"

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#addEmp").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            emp_name: {
                required: true,
                noSpace:true,
                // Bug - 25 Feb
                alpha:true,
            },
            emp_type_id: {
                required: true,
            },
            country_code: {
                required: true,
            },
            user_email: {
                required: true,
                customemail:true,
                minlength: 2
            },
            emp_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            },
            owner_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            } //16MARCH2020 IS_1279
            ,emp_id_proof:{
                filesize4MB:true
            }
            ,emp_id_proof1:{
                filesize4MB:true
            }
            ,emp_profile:{
                filesize4MB:true
            }
            
        },
        messages: {
            emp_name: "Please enter resource name",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            emp_type_id: {
                required: "Please Select Resource Type",
            },
            country_code: {
                required: "Please Select Country Code",
            },
            user_email: {
                required: "Please Enter Email",
                customemail:"Please Enter Valid Email",
            },
            
            emp_email: "Please enter a valid email address",
            emp_mobile: "Please enter valid mobile number",
            owner_mobile: "Please enter valid mobile number"

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#addAdmin").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            admin_name_add: {
                required:true,
                noSpace:true,
                alpha: true,
            },
            role_name:{
                required: true,
                noSpace:true,
                alpha:true,
            },
            admin_email: {
                required:true,
                customemail:true,
                minlength: 2
            },
            admin_mobile: {
                required: true,
                minlength: 8,
                maxlength:15

            }
            
        },
        messages: {
            admin_name_add: "Please enter full name",
            role_name: "Please role name",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            admin_email: {
                customemail :  "Please enter a valid email address",
                required :  "Please enter email address",
               
            },
            admin_mobile: "Please enter valid mobile number"

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit();
          }
    });
    $("#buildingDetails").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            society_name_update: {
                required: true,
                noSpace:true
            },
            society_address_edit: {
                required: true,
                noSpace:true
            },
            company_website: {
                required: false,
                noSpace:true
            },
            society_pincode: {
                required: true,
                noSpace:true
            },
            currency: {
                required: true,
                noSpace:true
            },
            auto_reject_vistor_minutes: {
                required: true,
                noSpace:true
            },
            complaint_reopen_minutes: {
                required: true,
                noSpace:true
            },
            secretary_email: {
                required: true,
                noSpace: true,
                email:true
            },
            default_time_zone: {
                required: true
            },
            secretary_mobile: {
                required: true
            }
        },
        messages: {
            secretary_mobile: {
                required : "Please enter Company Contact"
            },
            society_name_update: {
                required : "Please enter Company name"
            },
            secretary_email: {
                required : "Please enter Company Email"
            },
            society_address_edit: {
                required : "Please enter Company address"
            },
            company_website: {
                required : "Please enter Company website"
            },
            society_pincode: {
                required : "Please enter Company pincode"
            },
            currency: {
                required : "Please enter currency"
            },
            auto_reject_vistor_minutes: {
                required : "Please enter visitor auto reject time"
            },
            complaint_reopen_minutes: {
                required : "Please enter complaint reopen max time"
            },
            default_time_zone: {
                required : "Please select timezone"
            }
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit();
          }
    });
    $("#blockAdd").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            block_name: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
                //IS_159 28feb2020
                    //IS_159 28feb2020
                 },
                 block_sort: {
                    required: true,
                    noSpace:true,
              },

                 blockNameSingle: {
                     required: true,
                     noSpace:true,
                     alphaRestSpeChartor: true,
                
                     //IS_159 28feb2020
                 },
                 unit_name_single: {
                     required: true,
                     noSpace:true,
                     alphaRestSpeChartor: true,
                 }

             },
             messages: {
                 block_name: {
                     required : "Please enter branch name",
                     noSpace: "No space please and don't leave it empty",
               //IS_159 28feb2020
               remote: "branch Name is already exists, please use another branch name to avoid confusion"
               //IS_159  28feb2020
           },

           //7march2020
            block_sort: {
                     required : "Please enter branch order",
                     noSpace: "No space please and don't leave it empty",
                     remote: "branch order is already exists, please use another branch order to avoid confusion"
               
           },
           //7march2020

           blockNameSingle: {
               required : "Please enter branch name",
               noSpace: "No space please and don't leave it empty",
               //IS_159 28feb2020
               remote: "branch Name is already exists, please use another branch name to avoid confusion"
               //IS_159 28feb2020
           },
           unit_name_single: {
               required : "Please enter unit name",
               noSpace: "No space please and don't leave it empty",
           },
           
       },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#floorAdd").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            "floor_name[]": {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            floor_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            blockNameSingle: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            
        },
        messages: {
            "floor_name[]": {
                required : "Please enter department name",
                noSpace: "No space please and don't leave it empty",
            },
            floor_name: {
                required : "Please enter department name",
                noSpace: "No space please and don't leave it empty",
            },
            block_sort: {
                required : "Please enter branch name",
                noSpace: "No space please and don't leave it empty",
                remote: "branch order is already exists, please use another branch order to avoid confusion"
            },
            blockNameSingle: {
                required : "Please enter branch name",
                noSpace: "No space please and don't leave it empty",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#noticeBoard").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            notice_title: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            blockNameSingle: {
                required: true,
                noSpace:true,
            },
           " block_id[]": {
                required: true
            },
            notice_attachment: {
                 filesize32MB:true 
            } ,
            
        },
        messages: {
            notice_title: {
                required : "Please enter Circular title",
                noSpace: "No space please and don't leave it empty",
            },
            blockNameSingle: {
                required : "Please enter branch name",
                noSpace: "No space please and don't leave it empty",
            },
            "block_id[]": {
                required: "Please select Branch"
            }

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#emergencyNumberEdit").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            name_edit: {
                required: true,
                noSpace:true,
                alpha:true,
            },
            designation : {
                required: true,
                noSpace:true,
                // alpha: true,
            },
            mobile: {
                required: true,
                noSpace:true,
                minlength: 3,
                maxlength:15,
            }
            
        },
        messages: {
            name_edit: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },
            designation: {
                required : "Please enter designation",
                noSpace: "No space please and don't leave it empty",
            },
            mobile: {
                minlength : "Please enter valid number",
                maxlength : "Please enter valid number",
                required : "Please enter valid number",
                noSpace: "No space please and don't leave it empty",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#emergencyNumberAdd").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            name: {
                required: true,
                noSpace:true,
                alpha:true,
            },
            designation : {
                required: true,
                noSpace:true,
                // alpha: true,
            },
            mobile: {
                noSpace:true,
                required: true,
                minlength: 3,
                maxlength:15,
            }
            
        },
        messages: {
            name: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },
            designation: {
                required : "Please enter Description",
                noSpace: "No space please and don't leave it empty",
            },
            mobile: {
                minlength : "Please enter valid number",
                maxlength : "Please enter valid number",
                required : "Please enter valid number",
                noSpace: "No space please and don't leave it empty",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#facility").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            facility_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            facility_type: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            designation : {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            person_limit : {
                required: true,
                noSpace:true,
                minStrict: 1,
            },
            facility_amount : {
                required: true,
                noSpace:true,
                minStrict: 1,
            },
            facility_amount_day : {
                required: true,
                noSpace:true,
                minStrict: 1,
            },
                
            facility_photo : {
                required: true,
               
            },
            is_taxble : {
                required: true,
               
            },
            tax_slab : {
                required: true,
               
            },
            balancesheet_id: {
                required: true,
           },
           amount_type: {
            required: true,
           },
                
            
        },
        messages: {
            facility_name: {
                required : "Please enter facility name",
                noSpace: "No space please and don't leave it empty",
            },
            facility_type: {
                required : "Please enter facility type",
                noSpace: "No space please and don't leave it empty",
            },
            designation: {
                required : "Please enter designation",
                noSpace: "No space please and don't leave it empty",
            },
            person_limit: {
                required : "Please enter person limit",
                noSpace: "No space please and don't leave it empty",
                minStrict : "Minimum 1 person required"
            },
            facility_amount: {
                required : "Please enter amount",
                noSpace: "No space please and don't leave it empty",
                minStrict : "Minimum 1 rs required"
            },
            facility_photo: {
                required : "Please Select Photo",
               
               
            },
            facility_amount_day: {
                required : "Please enter amount",
                noSpace: "No space please and don't leave it empty",
                minStrict : "Minimum 1 rs required"
            },
            is_taxble: {
                required : "Please Select Bill Type",
           },
           tax_slab: {
                required : "Please Select Tax Type",
           },
           balancesheet_id: {
                required : "Please Select Balance Sheet",
           },
           amount_type: {
                required : "Please Select Amount Type",
           },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    $("#balancesheet").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            balancesheet_name_add: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            balancesheet_name : {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            
        },
        messages: {
            facility_name: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },
            balancesheet_name: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#incomeAdd").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            expenses_title: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            balancesheet_name : {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
             //24feb2020
             ,expAmoint:{
                 required:true
             }
             , income_amount:{
                 required:true
             }
             ,expenses_add_date:{
                 required: true
             },expenses_photo:{
                 required: false,
                 filesize4MB: true 
             },is_taxble:{
                required:true
            }
            ,taxble_type:{
                required: {
                 depends: function(element) {    
                      return $('#incomeAdd :input[name="is_taxble"]').val()=="1" ;
                    }
                }
            }

            //24feb2020
            
        },
        messages: {
            expenses_title: {
                required : "Please enter title",
                noSpace: "No space please and don't leave it empty",
            },
            balancesheet_name: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },

            //24feb2020
            expenses_add_date:{
                required : "Please select date"
            } ,
            income_amount:{
                required : "Please enter amount"
            },
            expAmoint:{
                required : "Please enter amount"
            } ,is_taxble:{
                required: "Please select Bill Type"
            }
            ,taxble_type:{
                required:  "Please select Tax Type"
            }
            //24feb2020

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


      $("#creditFrom").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            remark: {
                required: true,
                noSpace:true,
            }, credit_amount:{
                 required:true
             }
            
        },
        messages: {
            remark: {
                required : "Please enter remark",
                noSpace: "No space please and don't leave it empty",
            },
            
            credit_amount:{
                required : "Please enter credit amount"
            }
            //24feb2020

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


         $("#debitFrom").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            remark: {
                required: true,
                noSpace:true,
            }, debit_amount:{
                 required:true
             }
            
        },
        messages: {
            remark: {
                required : "Please enter remark",
                noSpace: "No space please and don't leave it empty",
            },
            
            debit_amount:{
                required : "Please enter debit amount"
            }
            //24feb2020

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    $("#chand").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            expenses_title: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            balancesheet_name : {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
             //24feb2020
             ,expAmoint:{
                 required:true
             }
             , income_amount:{
                 required:true
             }
             ,expenses_add_date:{
                 required: true
             },expenses_photo:{
                 required: false,
                 filesize4MB: true 
             },is_taxble:{
                required:true
            }
            ,taxble_type:{
                required: {
                 depends: function(element) {    
                      return $('#incomeAdd :input[name="is_taxble"]').val()=="1" ;
                    }
                }
            }

            //24feb2020
            
        },
        messages: {
            expenses_title: {
                required : "Please enter title",
                noSpace: "No space please and don't leave it empty",
            },
            balancesheet_name: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },

            //24feb2020
            expenses_add_date:{
                required : "Please select date"
            } ,
            income_amount:{
                required : "Please enter amount"
            },
            expAmoint:{
                required : "Please enter amount"
            } ,is_taxble:{
                required: "Please select Bill Type"
            }
            ,taxble_type:{
                required:  "Please select Tax Type"
            }
            //24feb2020

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#expense").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            expenses_title: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            balancesheet_name : {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            //24feb2020
            ,expAmoint:{
                required:true
            }
            , income_amount:{
                required:true
            }
            ,expenses_add_date:{
                required: true
            },expenses_photo:{
                required: false,
                filesize4MB: true 
            } //27march2020 #expense
            ,is_taxble:{
                required:true
            }
            ,taxble_type:{
                required: {
                 depends: function(element) {    
                      return $('#expense :input[name="is_taxble"]').val()=="1" ;
                    }
                }
            }

            //24feb2020
            
        },
        messages: {
            expenses_title: {
                required : "Please enter title",
                noSpace: "No space please and don't leave it empty",
            },
            balancesheet_name: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },

            //24feb2020
            expenses_add_date:{
                required : "Please select expense date"
            } ,
            income_amount:{
                required : "Please enter income amount"
            },
            expAmoint:{
                required : "Please enter expense amount"
            },is_taxble:{
                required: "Please select Bill Type"
            }
            ,taxble_type:{
                required:  "Please select Tax Type"
            }
            //24feb2020

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#bill").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }

        },
        rules: {
            bill_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            ,
            bill_category:{
                required: true,
                // alphaRestSpeChartor: true,
            },
            bill_genrate_date:{
                required: true
            } ,
            bill_end_date:{
                required: true
            }  ,
            owner_unit_price:{
                required: true
            } ,
            rent_unit_price:{
                required: true
            },
            close_unit_price:{
                required: true
            },
            balancesheet_id:{
                required:true
            },
            wing_id:{
                required:true
            }
            ,is_taxble:{
                required:true
            }
            ,taxble_type:{
                required: {
                 depends: function(element) {    
                      return $('#is_taxble').val()=="1" ;
                    }
                }
            }
        },
        messages: {
            bill_name: {
                required : "Please enter bill name",
                noSpace: "No space please and don't leave it empty",
            },

            bill_genrate_date:{
                required : "Please select bill date"
            } ,
            bill_end_date:{
                required : ""
            }  ,
            owner_unit_price:{
                required : "Please enter bill owner unit price"
            } ,
            rent_unit_price:{
                required : "Please enter bill rent unit price"
            },
            close_unit_price:{
                required : "Please enter bill close unit price"
            },
            balancesheet_id:{
                required : "Please select balancesheet"
            },
            wing_id:{
                required : "Please select Unit"
            }
            //24feb2020 
            ,is_taxble:{
                required:"Please select Bill Type" 
            }
            ,taxble_type:{
                required:"Please select Tax Type" 
            }

        },
          //   submitHandler: function(form) {
          //       $(':input[type="submit"]').prop('disabled', true);
          //$(".ajax-loader").show();
          //       form.submit(); 
          // }
            submitHandler: function(form) {
                var error = 0;
                  $( "#unit_id_main" ).each(function( index ) {
                     var clsVal =  $(this).val();
                     if($.trim(clsVal) ==""){
                        error++;
                    }
                });
                if (error > 0 ) {
              swal("Please select unit.!");
              } else {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
            }
          }
    });
    $("#maintenance").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            maintenance_name_add: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            maintenance_name_auto: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            created_date:{
                required:true,
            },
            end_date:{
                required:true,
            },is_taxble:{
                required:true
            }
            ,unit_id_main:{
                required:true
            }
            ,taxble_type:{
                required: {
                 depends: function(element) {    
                      return $('#is_taxble').val()=="1" ;
                    }
                }
            },

            //15july2020
            auto_gen_date:{
                required:{
                    depends: function(element) {
                      return $('#maintenance :input[name="maintenance_type_id"]:checked').val() =="0" ;
                    }
                },
            },
            mentainance_price_type:{
                 required: true
            },
             month_from:{
                required:{
                    depends: function(element) {
                      return $('#maintenance :input[name="mentainance_price_type"]').val() =="1" ;
                    }
                },
            },
             year_from:{
                required:{
                    depends: function(element) {
                      return $('#maintenance :input[name="mentainance_price_type"]').val() =="1" ;
                    }
                },
            },
             month_to:{
                required:{
                    depends: function(element) {
                      return $('#maintenance :input[name="mentainance_price_type"]').val() =="1" ;
                    }
                },
            },
             year_to:{
                required:{
                    depends: function(element) {
                      return $('#maintenance :input[name="mentainance_price_type"]').val() =="1" ;
                    }
                },
            },
            //15july2020
            
        },
        messages: {
            maintenance_name_add: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },
            maintenance_name_auto: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },
            created_date:{
                required : "Please enter maintenance date",
            },
            //15july2020
             auto_gen_date:{
                required : "Please select auto generate date",
            },
            mentainance_price_type:{
                required:"Please select maintenance price type"
            },
            month_from:{
                required:"select from month"
            },
            year_from:{
                required:"select to month"
            },
            month_to:{
                required:"select from month"
            },
            year_to:{
                required:"select to month"
            },
            end_date:{
                required:"",
            },is_taxble:{
                required:"Please select Bill Type" 
            }
            ,taxble_type:{
                required:"Please select Tax Type" 
            },unit_id_main:{
                required:"Please select Unit" 
            }

        },
            submitHandler: function(form) {
                var error = 0;
                  $( "#unit_id_main" ).each(function( index ) {
                     var clsVal =  $(this).val();
                     if($.trim(clsVal) ==""){
                        error++;
                    }
                });
                if (error > 0 ) {
              swal("Please select unit.!");
              } else {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
            }
          }
    });

    $.validator.addMethod("pollExtensionDoc", function(value, element, param) {
        param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|pdf|doc|docx|mp4|csv";
        return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    }, $.validator.format("Please select a file with a valid extension (png,jpeg,jpg,pdf,doc,docx,mp4,csv)."));  

    $("#poll").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            pulling_question: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            option_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            //24feb2020 IS_205
            ,poll_for:{
                required:true
            },
            no_of_option: {
                required:true
            },
            polling_start_time: {
                required:true
            },
            polling_end_time: {
                required:true
            },
            voting_attachment: {
                filesize12MB: true,
                pollExtensionDoc: true,
            }

            /*,option_name1: {
                required: true,
                noSpace:true,
            },option_name2: {
                required: true,
                noSpace:true,
            },option_name3: {
                required: true,
                noSpace:true,
            },option_name4: {
                required: true,
                noSpace:true,
            },option_name5: {
                required: true,
                noSpace:true,
            },option_name6: {
                required: true,
                noSpace:true,
            },option_name7: {
                required: true,
                noSpace:true,
            },option_name8: {
                required: true,
                noSpace:true,
            },option_name9: {
                required: true,
                noSpace:true,
            },option_name10: {
                required: true,
                noSpace:true,
            }*/

             //24feb2020 IS_205

         },
         messages: {
             pulling_question: {
                 required : "Please enter question",
                 noSpace: "No space please and don't leave it empty",
             },
             option_name    : {
                 required : "Please enter option",
                 noSpace: "No space please and don't leave it empty",
             },
             polling_start_time: {
                required:"Please Select Start Date"
            },
            polling_end_time: {
                required:"Please Select End Date"
            },
            //24feb2020 IS_205
            poll_for:{
                required : "Please select Poll For"
            },no_of_option:{
                required : "Please select No Of Options"
            },
            option_name1    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            },
            option_name2    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            },
            option_name3    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            },
            option_name4    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            },
            option_name5    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            },
            option_name6    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            },
            option_name7    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            },
            option_name8    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            },
            option_name9    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            },
            option_name10    : {
                required : "Please enter option",
                noSpace: "No space please and don't leave it empty",
            } 
            //24feb2020 IS_205

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                // $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#election").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            election_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            election_description: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            // /IS_641 26feb2020
            ,penalty_photo:{
                filesize:true
            }
             // /IS_641 26feb2020
         },
         messages: {
             election_name: {
                 required : "Please enter name",
                 noSpace: "No space please and don't leave it empty",
             },
             election_description: {
                 required : "Please enter description",
                 noSpace: "No space please and don't leave it empty",
             },

         },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

     $("#addDiscussion").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            discussion_forum_title: {
                required: true,
                noSpace:true,
            },
            discussion_forum_description: {
                required: true,
                noSpace:true,
            }
            // /IS_641 26feb2020
            ,penalty_photo:{
                filesize:true
            }
             // /IS_641 26feb2020
         },
         messages: {
             discussion_forum_title: {
                 required : "Please enter title",
                 noSpace: "No space please and don't leave it empty",
             },
             discussion_forum_description: {
                 required : "Please enter description",
                 noSpace: "No space please and don't leave it empty",
             },

         },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

      $("#commentAddDiscusstion").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            comment_messaage: {
                required: true,
                noSpace:true,
            },
           
             // /IS_641 26feb2020
         },
         messages: {
             comment_messaage: {
                 required : "Please enter message",
                 noSpace: "No space please and don't leave it empty",
             },
             

         },
             submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    $("#penaltyAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            penalty_name: {
                required: true,
                noSpace:true,
            },
            election_description: {
                required: true,
                noSpace:true,
            }
            //2march2020
            ,
            block_id: {
                required: true,
                noSpace:true,
            },
            floor_id: {
                required: true,
                noSpace:true,
            },
            penalty_photo:{
                filesize2MB: true
            },
            penalty_amount:{
                required:true,
                noSpace:true,
                min:1
            },
            balancesheet_id:{
                 required:true
            },
           "user_id[]":{
                 required:true
            },
            penalty_date:{
                required: true
            }, tax_slab:{
                required: true
            } 
            //2march2020
            ,is_taxble:{
                required:true
            }
            ,taxble_type:{
                required: {
                 depends: function(element) {    
                      return $("#is_taxble").val()=="1" ;
                    }
                }
            }
        },
        messages: {
            penalty_name: {
                required : "Please enter description",
                noSpace: "No space please and don't leave it empty",
            },
            block_id: {
                required : "Please Select Branch",
                noSpace: "No space please and don't leave it empty",
            },
            "user_id[]":{
                required : "Please Select Employee",
           },
            floor_id: {
                required : "Please Select Department",
                noSpace: "No space please and don't leave it empty",
            },
            election_description: {
                required : "Please enter description",
                noSpace: "No space please and don't leave it empty",
            },
            //2march2020
            penalty_amount: {
               required : "Please enter penalty amount",
               noSpace: "No space please and don't leave it empty",
               min: "penalty Amount should be more then 0"
            },
            balancesheet_id:{
                required : "Please select balancesheet"
            },
            penalty_date:{
                 required : "Please select penalty date"    
            } , tax_slab:{
                required: "Please select Tax Slab"
            } 
            //2march2020
             ,is_taxble:{
                required:"Please select Bill Type" 
            }
            ,taxble_type:{
                required:"Please select Tax Type" 
            }
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


     $("#contactCompany").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            subject: {
                required: true,
                noSpace:true,
            },
            feedback_msg: {
                required: true,
                noSpace:true,
            }
        },
        messages: {
            subject: {
                required : "Please enter subject",
                noSpace: "No space please and don't leave it empty",
            },
            feedback_msg: {
                required : "Please enter message",
                noSpace: "No space please and don't leave it empty",
            }
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit();
          }
    });


    $("#serviceProviderFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            service_provider_name: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            },
            contact_person_name: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            },
            service_provider_name_edit:{
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            },
            /* service_provider_address: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            }, */
            service_provider_phone:{
                required: true,
                noSpace:true,
                minlength: 8,
                maxlength: 12,
            },
            society_id:{
                required:true
            },
            /* end_time:{
                required:true
            }, */
           /*  start_time:{
                required:true
            }, */
            country_id:{
                required:true
            },
            state_id:{
                required:true
            },
            city_id:{
                required:true
            },
          /*   service_provider_email:{
                email:true,
                customemail:true,
            }, */
            local_service_master_id:{
                required:true
            },
            local_service_provider_sub_id:{
                required:true
            },
           /*  service_provider_user_image:{
                required: function(element){
                    return $("#service_provider_user_image_old").val()=="";
                },
                            //21FEB2020
               //filesize: true
               filesize1MB:true
           }, */
            id_proof:{
                required:false,
                filesize:true,
                extensionDoc:true
            },
           
            location_proof:{
                required:false,
                filesize:true,
                extensionDoc:true
            }
            //24feb2020
        },
        messages: {
            service_provider_name: {
                required : "Please enter Vendor",
                noSpace: "No space please and don't leave it empty",
            },
            contact_person_name: {
                required : "Please Contact Person Name ",
                noSpace: "No space please and don't leave it empty",
            },
            /* start_time: {
                required : "Please Enter Opening Time ",
                noSpace: "No space please and don't leave it empty",
            }, */
            /* end_time: {
                required : "Please Enter Closing Time ",
                noSpace: "No space please and don't leave it empty",
            }, */
            service_provider_name_edit:{
                required : "Please enter Vendor",
                noSpace: "No space please and don't leave it empty",
            },
            /* service_provider_address: {
                required : "Please enter address",
                noSpace: "No space please and don't leave it empty",
            } , */
            service_provider_phone:{
                required : "Please enter Mobile",
                noSpace: "No space please and don't leave it empty",
                minlength: "PLEASE ENTER AT LEAST 10 DIGITS."
            },
            society_id:{
                required:"Please select society"
            },
            country_id:{
                required:"Please select country"
            },
            state_id:{
                required:"Please select state"
            },
            city_id:{
                required:"Please select city"
            },
           /*  service_provider_email:{
                required:"Please enter email",
                email: "Please enter valid email address.",
                customemail: "Please enter valid email address.",
                noSpace: "No space please and don't leave it empty",
            }, */
            local_service_master_id:{
                required : "Please select category",
            },
            local_service_provider_sub_id:{
                required : "Please select sub category",
            },
           /*  service_provider_user_image: {
                required : "Please Select logo"
            } */
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit();
          }
    });
    $("#BalancesheetFileFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            balancesheet_name: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            },
            file_name:{
                required: true,
                filesize4MB: true
            } 
            
        },
        messages: {
            balancesheet_name: {
                required : "Please enter balancesheet name",
                noSpace: "No space please and don't leave it empty",
            },
            file_name: {
                required : "Please select pdf",
                noSpace: "No space please and don't leave it empty",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#addGallary").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            event_id: {
                required: true,
                noSpace:true,
            },
            "block_id[]":{
                required: true,
            },
            "floor_id[]":{
                required: true,
            },
            "gallery_photo[]":{
                required: true,
                image: true,
                filesize8MB: true,
            },

       },
       messages: {
            event_id: {
                required : "Please select event",
                noSpace: "No space please and don't leave it empty",
            },
            "block_id[]": {
                required : "Please select Branch",
            },
            "floor_id[]": {
                required : "Please select Department",
            },
            "gallery_photo[]": {
                required : "Please select image",
                image : "Please add a valid image file.",
                filesize8MB : "file size must be less than or equal to 8MB.",
           },
           
       },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#editProfileFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            admin_name: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            },
            admin_email:{
                required:true,
                email:true,
                customemail:true,
                noSpace: true,
            },
             admin_mobile:{
                required:true,
                minlength: 8,
                maxlength: 15,
                noSpace: true,
            },
            profile_image:{
                required: function(element){
                    return $("#profile_image_old").val()=="";
                },
                filesize: true
            } 
            
        },
        messages: {
            admin_name: {
                required : "Please enter admin name",
                noSpace: "No space please and don't leave it empty",
            },
            admin_email: {
                required : "Please enter email",
                email: "Please enter valid email",
                customemail: "Please enter valid email",
                noSpace: "No space please and don't leave it empty",
            },
            profile_image: {
                required : "Please select image",
                noSpace: "No space please and don't leave it empty",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
     $("#event").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            event_title: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            eventMom: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            //IS_171 21FEB2020
            "block_id[]":{
                required: true
            },
            "floor_id[]":{
                required: true
            },
            event_start_date:{
                required: true
            },
            event_end_date:{
                required: true
            },
            event_start_time:{
                required: true
            },
            event_end_time:{
                required: true
            },
            event_image: {
                // required: true,
                filesize: true,
            },
            adult_charge:{
                required: function(element){
                    return  $('#event_typePaid').is(':checked');
                },
                min: 1
            },
            child_charge:{
                required: function(element){
                    return  $('#event_typePaid').is(':checked');
                },
                min: 1
            },
            maximum_pass_adult:{
                min: function() {
                    return parseInt($('#minAdult').val());
                }
            },
            maximum_pass_children:{
                min: function() {
                    return parseInt($('#minChildren').val());
                }
            },
            maximum_pass_guests:{
                min: function() {
                    return parseInt($('#minGuests').val());
                }
            }
             //27march2020 new #event
             ,balancesheet_id:{
                required:true
            }
            ,is_taxble:{
                required:true
            }
            ,taxble_type:{
                required: {
                   depends: function(element) {    
                      return $('#event :input[name="is_taxble"]').val()=="1" ;
                  }
              }
          }
            //27march2020 new #event
        },
        messages: {
            event_title: {
                required : "Please enter event name",
                noSpace: "No space please and don't leave it empty",
            },
            eventMom: {
                required : "Please enter event location",
                noSpace: "No space please and don't leave it empty",
            },
             //IS_171 21FEB2020
            "block_id[]": {
                required : "Please select Branch"
            },
            "floor_id[]": {
                required : "Please select Department"
            },
            event_start_date: {
               required : "Please select start date"
            },
           event_end_date: {
               required : "Please select end date"
           },
           event_start_time: {
               required : "Please select start time"
           },
           event_end_time: {
               required : "Please select end time"
           },
             //IS_171 21FEB2020
            //IS_639 divya
            event_image: {
                required : "Please select image" 
            }
            //IS_639 divya
            //27march2020 new #event 
            ,balancesheet_id:{
                required:"Please select balancesheet" 
            }
            ,is_taxble:{
                required: "Please select Bill Type"
            }
            ,taxble_type:{
                required:  "Please select Tax Type"
            }
            //27march2020 new #event
        },
        submitHandler: function(form) {
          //27march2020 new #event  
          var error = 0;
          $( ".paid-price" ).each(function( index ) {
             var clsVal =  $(this).val();
             if($.trim(clsVal) ==""){
                error++;
            }
        });
          $( ".max-allow" ).each(function( index ) {
             var clsVal =  $(this).val();
             if($.trim(clsVal) ==""){
                error++;
            }
        });
          $( ".event-day-name" ).each(function( index ) {
             var clsVal =  $(this).val();
             if($.trim(clsVal) ==""){
                error++;
            }
        });
          if (error > 0 ) {
              swal("Please provide value for every textbox...!");
          } else {
           $(':input[type="submit"]').prop('disabled', true);
           $(".ajax-loader").show();
           form.submit();
       }
        //27march2020 new #event
    }
});
     
    $("#parkingAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            socieaty_parking_name_add: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            },
            total_car_parking: {
                required: true,
                digits:true,
                compare: '#noofBike',
            },
            total_bike_parking: {
                required: true,
                digits:true,
                compare: '#noofCar',
            },
            car_parking_name: {
                required: true,
                noSpace:true,
            },
            bike_parking_name: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            socieaty_parking_name_add: {
                required : "Please enter parking area name",
                noSpace: "No space please and don't leave it empty",
            },
            car_parking_name: {
                required : "Please enter car parking name",
                noSpace: "No space please and don't leave it empty",
            },
            bike_parking_name: {
                required : "Please enter bike parking name",
                noSpace: "No space please and don't leave it empty",
            },
            total_car_parking: {
                required : "Please enter number of car parking",
                compare : "Please add Positive Number of Parkings",
            },
            total_bike_parking: {
                required : "Please enter number of bike parking",
                compare : "Please add Positive Number of Parkings",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

     $("#addMoreParkingForm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            
            parking_name: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
           
            parking_name: {
                compare : "Please Parking Name",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#documentTypeAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            document_type_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            employee_type_name_edit: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            
            
        },
        messages: {
            document_type_name: {
                required : "Please enter type name",
                noSpace: "No space please and don't leave it empty",
            },
            employee_type_name_edit: {
                required : "Please enter type name",
                noSpace: "No space please and don't leave it empty",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#empTypeAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            employee_type_name_add: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            employee_type_name_edit: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            ,emp_type_icon:{
                filesize2MB: true,
                required: $('#emp_type_id_edit').val()!="0"  ? false :true ,
            }
            
            
        },
        messages: {
            employee_type_name_add: {
                required : "Please enter resource type name",
                noSpace: "No space please and don't leave it empty",
            },
            employee_type_name_edit: {
                required : "Please enter resource type name",
                noSpace: "No space please and don't leave it empty",
            },
            emp_type_icon: {
                required : "Please Select Icon",
               
            },

        },
        submitHandler: function (form) {
           
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#addDocument").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            ducument_name_add: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            ducument_name_edit: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            //IS_738 26feb2020
            ,document_file:{
                extensionDocument:true,
                filesize: true
            }
            //IS_738 26feb2020
            
        },
        messages: {
            ducument_name_add: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },
            ducument_name_edit: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#sosAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            event_name_edit: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            event_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            sos_image:{
                required: {
                    depends: function(element) {    
                      return $("#img_req").val()=="0" ;
                    }
                },
                filesize:true
            },
            sos_duration:{
                required: true,
                min:0,
                max:120
            }
            
            
        },
        messages: {
            event_name_edit: {
                required : "Please enter sos name",
                noSpace: "No space please and don't leave it empty",
            },
            event_name: {
                required : "Please enter sos name",
                noSpace: "No space please and don't leave it empty",
            },
            sos_image:{
                required:"Please Select SOS Image"
            },
            sos_duration:{
                required: "Please enter SOS minutes",
                min:"Please enter minutes in Positive number",
                max:"Max SOS duration can be 2 hours, i.e. 120 minutes"
            }

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#classifiedCategory").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            classified_category_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            
        },
        messages: {
            classified_category_name: {
                required : "Please enter category name",
                noSpace: "No space please and don't leave it empty",
            }

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#classifiedCategoryCateEdit").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            classified_category_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            
        },
        messages: {
            classified_category_name: {
                required : "Please enter category name",
                noSpace: "No space please and don't leave it empty",
            }

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#classifiedSubCategory").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            classified_sub_category_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            
        },
        messages: {
            classified_sub_category_name: {
                required : "Please enter sub category name",
                noSpace: "No space please and don't leave it empty",
            }

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#classifiedSubCateEdit").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            classified_sub_category_image: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            
        },
        messages: {
            classified_sub_category_image: {
                required : "Please enter sub category name",
                noSpace: "No space please and don't leave it empty",
            }

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#unitEdit").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            unit_name: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            },
            car_parking_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            bike_parking_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            
        },
        messages: {
            unit_name: {
                required : "Please enter unit name",
                noSpace: "No space please and don't leave it empty",
            },
            car_parking_name: {
                required : "Please enter car parking name",
                noSpace: "No space please and don't leave it empty",
            },
            bike_parking_name: {
                required : "Please enter bike parking name",
                noSpace: "No space please and don't leave it empty",
            },

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                // $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#insTeam").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            full_name: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            },
            mobile_number: {
                required: true,
                noSpace:true,
                minlength: 8,
                maxlength: 13,
            }
            
        },
        messages: {
            full_name: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },
            mobile_number: {
                required : "Please enter mobile number",
                noSpace: "No space please and don't leave it empty",
            },


        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#editinsTeam").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            member_full_name: {
                required: true,
                noSpace:true,
                alphaRestSpeChartor: true,
            },
            member_mobile_number: {
                required: true,
                noSpace:true,
                minlength: 8,
                maxlength: 13,
            }
            
        },
        messages: {
            member_full_name: {
                required : "Please enter name",
                noSpace: "No space please and don't leave it empty",
            },
            member_mobile_number: {
                required : "Please enter mobile number",
                noSpace: "No space please and don't leave it empty",
            },


        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#floorValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            block_id: {
                required: true,
            },
            no_of_floor: {
                required: true,
                digits: true,
            },
            /* no_of_unit: {
                required: true,
            },
            unit_type: {
                required: true,
            }, */
        },
        messages: {
            block_id: {
                required: "Please Select Branch",
            },
            no_of_floor: {
                required: "Please Enter No Of Floor",
                digits: "Please Enter number only",
            }, 
            /* no_of_unit: {
                required: "Please Select Branch",
            },
            unit_type: {
                required: "Please Enter No Of Floor",
                digits: "Please Enter number only",
            }, */ 
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#SubfloorValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            floor_id: {
                required: true,
            },
            
            no_of_sub_dept: {
                required: true,
            },
            "sub_department_name[]": {
                required: true,
            },
        },
        messages: {
            floor_id: {
                required: "Please select Department",
            },
            no_of_sub_dept: {
                required: "Please Enter Sub Deparment Count",
            },
            "sub_department_name[]": {
                required: "Please Enter Sub Name",
            }
       },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#billCategoryValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            category_name: {
                required: true,
                maxlength: 25,
                noSpace: true,
                // alphaRestSpeChartor: true,
            },
            category_description: {
                maxlength: 200,
                noSpace: true,
            },
            category_image:{
              required:false,
                filesize2MB: true
            },
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#importValidation").validate({
        rules: {
            file: {
                required: true,
                extension: "csv",
            }
        },
        messages:{
            file:{
                extension: "Please upload CSV file only"
            }
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#notificationValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            title: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
            description: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
            dId: {
                required: true,
            },
            bId: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please Enter Title",
                
            },
            description: {
                required: "Please Enter description",
               
            },
            dId: {
                required: "Please select Department",
            },
            bId: {
                required: "Please select Branch",
            },  
        },submitHandler: function (form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

     $("#notificationValidationSingle").validate({
        rules: {
            title: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
            description: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
        },submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#pendingValidation").validate({
        rules: {
            user_first_name: {
                required: true,
                noSpace: true,
                alpha: true,
            },
            user_last_name: {
                required: true,
                noSpace: true,
                alpha: true,
            },
            user_mobile: {
                required: true,
                noSpace: true,
                digits: true,
                maxlength: 15,
                minlength: 8,
            },
            user_email: {
                required: true,
                noSpace: true,
                email: true,
                customemail: true,
            },
        },
    });
    $("#complainCategoryValidation").validate({
        rules: {
            category_name: {
                required: true,
                noSpace: true,
                maxlength: 25,
                // alphaRestSpeChartor: true,
            },
             user_mobile: "Please enter valid mobile number",
        },
        messages: {
            category_name: {
                required: "Please Enter Cateogry Name",
               
                // alphaRestSpeChartor: true,
            },
        },
    });
    //3 March
    $("#complaintValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            user_id: {
                required: true,
            },
            floor_id: {
                required: true,
            },
            block_id: {
                required: true,
            },
            complaint_category: {
                required: true,
                // alphaRestSpeChartor: true,
            },
            compalain_title: {
                required: true,
                noSpace: true,
                maxlength: 100,
                // alphaRestSpeChartor: true,
            },
            complain_photo: {
                image: true,
            },
            complain_description: {
                noSpace: true,
                // alphaRestSpeChartor: true,
            },
        },
        messages: {
            floor_id: {
                required: "Please Select Department",
            },
            block_id: {
                required: "Please Select Branch",
            },
            user_id: {
                required: "Please Select Employee",
            },
            complaint_category: {
                required: "Please Select Cateogry",
               
            },
            compalain_title: {
                required: "Please Enter title",

            },
            complain_description: {
                noSpace: "Please remove Space",
                // alphaRestSpeChartor: true,
            },
            complain_photo: {
                image: "Please Select Image",
            },
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    $("#complaintBlockValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            
            unit_id: {
               required:true,
            },
            block_message: {
                noSpace: true,
                // alphaRestSpeChartor: true,
            },
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    $("#addexplenseCategory").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
           
            expense_category_name: {
                required: true,
                noSpace: true,
                maxlength: 250,
                // alphaRestSpeChartor: true,
            },
            category_type: {
                required: true,
                
                // alphaRestSpeChartor: true,
            },
           
        },
         messages: {
            expense_category_name: {
                required: "Please enter category name",
            },
            category_type: {
                required: "Please Select Type",
            }
            },

            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    //12march2020
    $("#CommEntryFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            common_entry_name:
            {
                required: true,
                noSpace: true,
                // alphaRestSpeChartor: true,
            },
            common_entry_image:
            {
                required: false,
                filesize4MB: true
            },
        },
        messages: {
            common_entry_name: {
                required: "Please enter common entry name",
    }
    },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    //12march2020



//13march2020
$("#addMainVisitorFrm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
        error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        main_type_name:
        {
            required: true,
            noSpace: true,
            // alphaRestSpeChartor: true, 
        },
        visitor_type:{
            required: true,
            noSpace: true 
        },
        main_type_image:
        {
            required: true,
            filesize4MB: true 
        },
         visitor_main_full_img:
        {
            required: true,
            filesize4MB: true 
        },
    },
    messages: {
        main_type_name: {
            required: "Please enter visitor type name", 
   },
   visitor_type:{
    required: "Please enter visitor type", 
   },
   main_type_image: {
            required: "Please select main type image", 
   },
   visitor_main_full_img: {
            required: "Please select full image", 
   }
  },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
});
$("#visitorSubTypeFrm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
        error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        visitor_main_type_id:
        {
            required: true,
            noSpace: true 
        },
        visitor_sub_type_name:{
            required: true,
            noSpace: true ,
            // alphaRestSpeChartor: true,
        },
        visitor_sub_image:
        {
            required: true,
            filesize4MB: true 
        } 
    },
    messages: {
        visitor_main_type_id: {
            required: "Please select main type", 
   },
   visitor_sub_type_name:{
    required: "Please enter sub type name", 
   },
   visitor_sub_image: {
            required: "Please select image", 
   } 
  },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
});

//13march2020

    $("#payMaintenanceFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            payment_type: {
                required: true,
                noSpace:true
            },
            receive_maintenance_date: {
                required: true,
                noSpace:true 
            },
           
            receive_maintenance_receipt_photo:{
              required:false,
                filesize2MB: true
            },
            maintence_amount_new:{
                required:true,
                noSpace:true,
                //15july2020
                /*,
                min:1*/
            }
            
            ,bank_name:{
                required:{
                    depends: function(element) {  
                      return $("#payment_type").val()=="1"  || $("#payment_type").val()=="2" ;
                    }
                },
                 noSpace:true 
            }
             
            
        },
        messages: {
            payment_type: {
               required : "Please select Payment Type",
               noSpace: "No space please and don't leave it empty"
            },
             receive_maintenance_date: {
               required : "Please select receive maintenance date",
               noSpace: "No space please and don't leave it empty" 
            },
             maintence_amount_new: {
               required : "Please enter maintenance amount",
               noSpace: "No space please and don't leave it empty",
               // min: "penalty maintence should be more then 0"
            }
           
             ,
            bank_name:{
                required:"Please enter bank name"
            }
           
            

           
        } ,
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    $("#chngProfileFrm").validate({
    rules: {
      old_password:
      {
        required: true,
      },
      password: {
        required: true,
        pwcheck: true,
        minlength: 5
      },
      password2: {
        required: true,
        pwcheck: true,
        minlength: 5,
        equalTo: "#password"
      },

    } ,
    messages: {

      old_password: {
        required: "Please provide a old password",
        remote: "The current password is incorrect."
      },

      password: {
        required: "Please provide a new password",
         pwcheck: "Please enter at least 1 Uppercase letter & 1 number value",
        minlength: "Your password must be at least 5 characters long"
      },
      password2: {
        required: "Please confirm your new password",
         pwcheck: "Please enter at least 1 Uppercase letter & 1 number value",
        minlength: "Your password must be at least 5 characters long",
        equalTo: "Please enter the same password"
      }   

    },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
  });


$("#mpinForm").validate({
    rules: {

      mpin: {
        required: true,
        minlength: 4,
        noSpace:true,
      },
      cmpin: {
        required: true,
        minlength: 4,
        noSpace:true,
        equalTo: "#mpin"
      },

    } ,
    messages: {

      mpin: {
        required: "Please provide a new mpin",
        minlength: "mpin must be at least 4 digits long",
        noSpace: "No space please and don't leave it empty"
      },
      cmpin: {
        required: "Please confirm new mpin",
        minlength: "mpin must be at least 4 digits long",
        equalTo: "Please enter the same mpin",
        noSpace: "No space please and don't leave it empty"
      }

    },
    submitHandler: function(form) {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
  });


 $("#replyFeedbackFrm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        reply: {
            required: true,
            noSpace:true,
            // alphaRestSpeChartor: true,
        } 
        
    },
    messages: {
        reply: {
            required : "Please enter reply",
            noSpace: "No space please and don't leave it empty",
        } 

    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});


 $("#addDailyVisitorFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            visit_from: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            } ,
             visitor_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
             visitor_mobile: {
                required: true,
                noSpace:true,
                minlength: 8,
                maxlength : 15,
                remote:
                {
                    url: 'getDailyVisitor.php',
                    type: "post",
                    data:
                    {
                        
                        visitor_mobile: function()
                        {
                            return $('#addDailyVisitorFrm :input[name="visitor_mobile"]').val();
                        }, 
                        validatemobileFirst:'yes' 
                          
                    } 
                }
            },
             visitor_profile: {
                 filesize12MB:true 
            } ,
            id_proof:{
                filesize4MB:true
            },
            id_proof1:{
                filesize4MB:true
            },
            valid_till:{
                required:true
            } ,
            in_time:{
                required:true
            },
            out_time:{
                required:true
            } ,
             
            visitor_sub_type_id:{
                required:true
            }
            
        },
        messages: {
            visit_from: {
                required : "Please enter visit from",
                noSpace: "No space please and don't leave it empty",
            },
            visitor_name: {
                required : "Please enter visitor name",
                noSpace: "No space please and don't leave it empty",
            },
            visitor_mobile: {
                minlength: "Please enter valid mobile number",
                maxlength: "Please enter valid mobile number",
                required : "Please enter visitor mobile",
                noSpace: "No space please and don't leave it empty",
                remote:"Please Provide another number, this mobile number is already registered"
            },
            valid_till:{
                required : "Please select date",
            } ,
            in_time:{
                required : "Please select in time",
            },
            out_time:{
                required : "Please select out time",
            } ,
             
            visitor_sub_type_id:{
               required : "Please select type",
            } 

        },
            submitHandler: function(form) {
                if (($("input[name*='week_days']:checked").length)<=0) {
                    swal("Please select at least 1 week day!");
                }else {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                    form.submit(); 

                }
          }
    });


  $("#editDailyVisitorFrm").validate({
        errorPlacement: function (error, element) {
          if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
              } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
              } else {                                      
                error.insertAfter(element);               // default
              }
            },
            rules: {
              visit_from: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
              } ,
              visitor_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
              },
              visitor_mobile: {
                required: true,
                noSpace:true,
                minlength: 8,
                maxlength : 15,
                remote:
                {
                    url: 'getDailyVisitor.php',
                    type: "post",
                    data:
                    {
                        visitor_id: function()
                        {
                            return $('#editDailyVisitorFrm :input[name="visitor_id"]').val();
                        },
                        visitor_mobile: function()
                        {
                            return $('#editDailyVisitorFrm :input[name="visitor_mobile"]').val();
                        }, 
                        validatemobile:'yes' 
                          
                    } 
                }
              },
              visitor_profile: {
               filesize12MB:true 
             }   ,
         id_proof:{
            
          filesize4MB:true
        } ,
             valid_till:{
              required:true
            } ,
            in_time:{
              required:true
            },
            out_time:{
              required:true
            } ,
            
          visitor_sub_type_id:{
            required:true
          }

        },
        messages: {
          visit_from: {
            required : "Please enter visit from",
            noSpace: "No space please and don't leave it empty",
          },
          visitor_name: {
            required : "Please enter visitor name",
            noSpace: "No space please and don't leave it empty",
          },
          visitor_mobile: {
            required : "Please enter visitor mobile",
            minlength: "Please enter valid mobile number",
            maxlength: "Please enter valid mobile number",
            noSpace: "No space please and don't leave it empty",
            remote:"Please Provide another number, this mobile number is already registered"
          } ,
          valid_till:{
            required : "Please select date",
          } ,
          in_time:{
            required : "Please select in time",
          },
          out_time:{
            required : "Please select out time",
          } ,
           
          visitor_sub_type_id:{
           required : "Please select type",
         }

      },
      submitHandler: function(form) {

        if (($("input[name*='week_days']:checked").length)<=0) {
          swal("Please select at least 1 week day!");

        }else {
          $(':input[type="submit"]').prop('disabled', true);
          $(".ajax-loader").show();
          form.submit(); 

        }

      }
    });

  $("#GenerateBill").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            previous_unit_read: {
                required: true,
                noSpace:true,
            } ,
             current_unit_read: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            previous_unit_read: {
                required : "Please enter Previous Reading",
                noSpace: "No space please and don't leave it empty",
            },
            current_unit_read: {
                required : "Please enter Current Reading",
                noSpace: "No space please and don't leave it empty",
            }

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


   $("#ParkingUpdateFrmNew").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        }
         , submitHandler: function(form) {
          $(':input[type="submit"]').prop('disabled', true);
          $(".ajax-loader").show();
          form.submit();
        }
    });

    $("#ParkingUpdateFrmNewMain").validate({
         errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            socieaty_parking_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            }
            
        },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
        }
    });


   $("#festivalAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            festival_name: {
                required: true,
                noSpace:true,
                // alphaRestSpeChartor: true,
            },
            festival_date: {
                required: true,
            },
            festival_image:{
                image: true,
                filesize2MB: true,
            }
            
        },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
        }
    });


   //20march2020
   $("#EmpUnitFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        }
         , submitHandler: function(form) {
          $(':input[type="submit"]').prop('disabled', true);
          $(".ajax-loader").show();
          form.submit();
        }
    });
   //20march2020

   //1418
   $("#personalInfo2").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
       },
        rules: {
            block_id: {
                required: true,
            }, 
            floor_id: {
                required: true,
            }, 
            unit_id: {
                required: true,
            }, 
        },
        messages: {
            block_id: {
                required : "Please Select Branch ",
            } ,
            floor_id: {
                required : "Please Select Department ",
            } ,
            unit_id: {
                required : "Please Select Employee ",
            } ,

        }
        , submitHandler: function(form) {
          $(':input[type="submit"]').prop('disabled', true);
          $(".ajax-loader").show();
          form.submit();
        }
    });


   //IS_1546 23march2020
   $("#slabFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        } ,
        rules: {
            slab_percentage: {
                required: true,
                noSpace:true,
            }  
          },
        messages: {
            slab_percentage: {
                required : "Please enter slab value",
                noSpace: "No space please and don't leave it empty",
            } 

        }
        , submitHandler: function(form) {
             
          $(':input[type="submit"]').prop('disabled', true);
          $(".ajax-loader").show();
          form.submit();
       
        }
    });
   
   //IS_1546 23march2020

   //24march2020 IS_1581
   $("#addTermFrm, #editTermFrm").validate({
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            condition_desc: {
                required: true,
                noSpace:true 
            } 
            
        },
        messages: {
            condition_desc: {
                required : "Please enter terms and conditions",
                noSpace: "No space please and don't leave it empty",
            } 

        },
            submitHandler: function(form) {
                 var error = 0;
               var clsVal =  $('#summernoteImgage').val();
               
                   if($.trim(clsVal) =="<p><br></p>" ){
                    error++;
                  } 
              
         if (error > 0 ) {
          swal("Please provide terms and conditions!");
         }else {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
            }
          }
    });
   

   $("#penaltyEdit").validate({
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
        error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
        error.insertAfter(element.next('span'));  // select2
        element.next('span').addClass('error').removeClass('valid');
        } else {
        error.insertAfter(element);               // default
        }
        },
        rules: {
          penalty_name: {
            required: true,
            noSpace:true,
          },
          block_id: {
            required: true,
            noSpace:true,
          },
          election_description: {
            required: true,
            noSpace:true,
          },

          penalty_photo:{
            required:false,
            filesize2MB: true
          },
          penalty_amount:{
            required:true,
            noSpace:true,
            min:1
          },
          balancesheet_id:{
            required:true
          },
          penalty_date:{
            required: true
          }
        //24march2020
        ,is_taxble:{
          required:true
        }
        ,taxble_type:{
          required: {
            depends: function(element) {
              return $('#penaltyEdit :input[name="is_taxble"]').val()=="1" ;
            }
          }
        }
        //24march2020

        },
        messages: {
          penalty_name: {
            required : "Please enter description",
            noSpace: "No space please and don't leave it empty",
          },
          block_id: {
            required : "Please Select BLock",
            noSpace: "No space please and don't leave it empty",
          },
          election_description: {
            required : "Please enter description",
            noSpace: "No space please and don't leave it empty",
          },
          penalty_amount: {
            required : "Please enter penalty amount",
            noSpace: "No space please and don't leave it empty",
            min: "penalty Amount should be more then 0"
          },
          balancesheet_id:{
            required : "Please select balancesheet"
          },
          penalty_date:{
            required : "Please select penalty date"
          }

        //24march2020
        ,is_taxble:{
          required:"Please select Bill Type"
        }
        ,taxble_type:{
          required:"Please select Tax Type"
        }
        //24march2020

        }
    });




    //26march2020 IS_1652
   $("#surveyFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        } ,
        rules: {
            survey_title: {
                required: true,
                noSpace:true,
            } ,
            survey_desciption:{
                 required: false,
                noSpace:true,
            },
            survey_date:{
                 required: true,
                noSpace:true,
            }
          },
        messages: {
            survey_title: {
                required : "Please enter survey title",
                noSpace: "No space please and don't leave it empty",
            } ,
            survey_desciption: {
                required : "Please enter description",
                noSpace: "No space please and don't leave it empty",
            } ,
            survey_date: {
                required : "Please select survey date",
                noSpace: "No space please and don't leave it empty",
            } 
        }
        , submitHandler: function(form) {
          $(':input[type="submit"]').prop('disabled', true);
          $(".ajax-loader").show();
          form.submit();
        }
    });
   $("#addSurveyQueFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        } ,
        rules: {
            survey_question: {
                required: true,
                noSpace:true,
            }  
          },
        messages: {
            survey_question: {
                required : "Please enter survey title",
                noSpace: "No space please and don't leave it empty",
            }  
        }
        , submitHandler: function(form) {
              var error = 0;
            $( ".option_name-cls" ).each(function( index ) {
               var clsVal =  $(this).val();
                  if($.trim(clsVal) ==""){
                    error++;
                  }
            });
         if (error > 0 ) {
          swal("Please provide value for every option...!");
         } else {
             $(':input[type="submit"]').prop('disabled', true);
             $(".ajax-loader").show();
          form.submit();
         }
        }
    });
   //26march2020  IS_1652
   
    //27march2020 1686
    $("#addSalaryFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            balancesheet_id: {
                required: true
            },
            month: {
                required: true
            },
            year:{
                required: true
            },
            month_working_days:{
                required: true
            },
            working_days:{
                required: true
            },
            leave_days:{
                required: true
            },
            start_date:{
                required: true
            },
            is_taxble:{
                required:true
            } ,
            taxble_type:{
                required: {
                 depends: function(element) {    
                      return $('#addSalaryFrm :input[name="is_taxble"]').val() =="1" ;
                    }
                }
            }    
          },
        messages: {
            balancesheet_id: {
                required : "Please select balance sheet"
            },
            month: {
                required : "Please select month"
            },
            year: {
                required : "Please select year"
            },
            month_working_days: {
                required : "Please enter month working days"
            },
            working_days:{
                required: "Please enter working days"
            }  ,
            leave_days:{
                required: "Please enter leave days"
            },
            start_date:{
                required: "Please select Start Date"
            },
            is_taxble:{
                required: "Please select Bill Type"
            },
            taxble_type:{
                required:  "Please select Tax Type"
            }    
        }
        , submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
        }
    });
   //27march2020 1686
    //28march2020
    $("#paymentGatewayFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            payment_getway_master_id: {
                required: true
            },
            name: {
                required: true,
                noSpace:true
            },
            merchant_id: {
                required: true,
                noSpace:true
            },
            merchant_key:{
                required: true,
                noSpace:true
            },
            salt_key:{
                required: true,
                noSpace:true
            },
            type:{
                required: true
            } ,
            block_id:{
                required: {
                 depends: function(element) {    
                      return $('#paymentGatewayFrm :input[name="type"]').val() =="Block" ;
                    }
                }
            },
            balancesheet_id:{
                required: {
                 depends: function(element) {    
                      return $('#paymentGatewayFrm :input[name="type"]').val() =="BalanceSheet" ;
                    }
                }
            }        
          },
        messages: {
            payment_getway_master_id: {
                required : "Please select payment gateway"
            },
             name: {
                required : "Please enter payment gateway name"
            },
            merchant_id: {
                required : "Please enter merchant id"
            },
            merchant_key: {
                required : "Please enter merchant key"
            },
            salt_key: {
                required : "Please enter salt key"
            },
            type:{
                required: "Please select gateway for"
            }  ,
            block_id:{
                required: "Please select block"
            },
            balancesheet_id:{
                required: "Please select balancesheet"
            }    
        }
        , submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
        }
    });


     $("#paymentGatewayApplyFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            payment_getway_master_id: {
                required: true
            },
            name: {
                required: true,
                noSpace:true,
                 alpha: true,
            },
            mobile: {
                required: true,
                noSpace:true,
                number: true
            },
            email:{
                required: true,
                customemail:true,
                noSpace:true
            },
            registrationProof: {
               required: true,
               filesize4MB: true

            },
            panCard: {
               filesize4MB: true,
               required: true

            } ,
            gstCerty: {
               filesize4MB: true

            } ,
            cancelledCheque: {
               required: true,
               filesize4MB: true

            } ,
            authorizedAddress: {
               required: true,
               filesize4MB: true

            } ,
            authorizedPancard: {
               required: true,
               filesize4MB: true

            }        
          },
        messages: {
            payment_getway_master_id: {
                required : "Please select payment gateway"
            },
            name: {
                required : "Please enter contact person name"
            },
            mobile: {
                required : "Please enter mobile number"
            },
            email: {
                required: "Please enter email id",
                customemail:"Please Enter Valid Email"
            }
             
        }
        , submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
        }
    });


    $("#editpaymentGatewayFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            payment_getway_master_id: {
                required: true
            },
            merchant_id: {
                required: true,
                noSpace:true
            },
            name: {
                required: true,
                noSpace:true
            },
            merchant_key:{
                required: true,
                noSpace:true
            },
            salt_key:{
                required: true,
                noSpace:true
            },
            type:{
                required: true
            } ,
            block_id:{
                required: {
                 depends: function(element) {    
                      return $('#editpaymentGatewayFrm :input[name="type"]').val() =="Block" ;
                    }
                }
            },
            balancesheet_id:{
                required: {
                 depends: function(element) {    
                      return $('#editpaymentGatewayFrm :input[name="type"]').val() =="BalanceSheet" ;
                    }
                }
            }        
          },
        messages: {
            payment_getway_master_id: {
                required : "Please select payment gateway"
            },
             name: {
                required : "Please enter payment gateway name"
            },
            merchant_id: {
                required : "Please enter merchant id"
            },
            merchant_key: {
                required : "Please enter merchant key"
            },
            salt_key: {
                required : "Please enter salt key"
            },
            type:{
                required: "Please select gateway for"
            }  ,
            block_id:{
                required: "Please select block"
            },
            balancesheet_id:{
                required: "Please select balancesheet"
            }    
        }
        , submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
        }
    });
    //28march2020

    //30march2020
 $("#eventBookFrm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            user_id: {
                required: true
            },
            bId: {
                required: true
            },
            dId: {
                required: true
            },
            events_day_id: {
                required: true 
            },
             bank_name: {
                required: true,
                noSpace:true, 
            }       
        },
        messages: {
            user_id: {
                required : "Please select Employee"
            },
            bId: {
                required : "Please select Branch"
            },
            dId: {
                required : "Please select Department"
            },
            events_day_id: {
                required : "Please select event date"
            },
            bank_name: {
                required : "Please enter bank name"
            } 
        }
        , submitHandler: function(form) {
           var going_person =$('#going_person').val();
              var going_child =$('#going_child').val();
              var going_guest =$('#going_guest').val();
              var totalGuests = parseInt(going_person)+parseInt(going_child)+parseInt(going_guest);
           if (totalGuests <= 0 || totalGuests=="" ) {
              swal("Please Select Number of Person (Adult/Child/guest) for event..!");
          } else {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
          }
        }
    });

$("#payEventAmountFrm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            payment_type: {
                required: true,
                noSpace:true 
            },
            payment_bank:{
                required:{
                    depends: function(element) {
                      return $('#payEventAmountFrm :input[name="payment_type"]').val() !="" ;
                  }
              },
              noSpace:true ,
          alphaRestSpeChartor:true
          },
          payment_ref_no:{
            required:{
                depends: function(element) {
                  return $('#payEventAmountFrm :input[name="payment_type"]').val() !="" ;
              }
          },
          noSpace:true ,
          alphaRestSpeChartor:true
      }     
      },
      messages: {
        payment_type: {
            required : "Please select payment type"
        },
        payment_bank: {
            required : "Please enter payment bank"
        } ,
        payment_ref_no: {
            required : "Please enter cheque/ref. no"
        } 
    }
    , submitHandler: function(form) {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});



 $("#addHousie").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            game_name: {
                required: true,
                noSpace:true
            },
            game_date:{
                required: true,
                noSpace:true
            },
            game_time:{
                required: true,
            },
            que_id:{
                required: true,
            },
            housie_type:{
                required: true,
            },
           
             
          },
        messages: {
            game_name: {
                required : "Please enter name"
            },
            game_date: {
                required : "Please enter date"
            } ,
            que_id: {
                required : "Please enter No of Questions"
            } ,
            housie_type: {
                required : "Please Select Housie Type"
            } ,
            game_time: {
                required : "Please enter time"
            } 
        }
        , submitHandler: function(form) {
            form.submit();
        }
    });


 $("#changeOwner").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            new_primary_id: {
                required: true,
                noSpace:true
            },
            member_relation_name_other_primary:{
                required: true,
                noSpace:true
            },
           
             
          },
        messages: {
            new_primary_id: {
                required : "Please select member"
            },
            member_relation_name_other_primary: {
                required : "Please enter here"
            } 
        }
        , submitHandler: function(form) {
            var error = 0;
            $( ".member_relation" ).each(function( index ) {
               var clsVal =  $(this).val();
                  if($.trim(clsVal) ==""){
                    error++;
                  }
            });
         if (error > 0 ) {
          swal("Please provide value for every Relation...!");
         } else {
             $(':input[type="submit"]').prop('disabled', true);
             $(".ajax-loader").show();
          form.submit();
         }
        }
    });


  $("#addKbg").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            kbg_game_name: {
                required: true,
                noSpace:true
            },
            kbg_category_id:{
                required: true,
                noSpace:true
            },
           
             
          },
        messages: {
            kbg_game_name: {
                required : "Please enter name"
            },
            kbg_category_id: {
                required : "Please select category"
            } 
        }
        , submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
        }
    });



 $("#addHousieQuestion").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            "question_name[]": "required",
            "answer[]": "required"
        },
        messages: {
            "question_name[]": "Please enter question",
            "answer[]": "Please enter answer",
        }
    });


 $("#addKbgQuestion").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            "kbg_question[]": "required",
            "kbg_option_1[]": "required"
        },
        messages: {
            "kbg_question[]": "Please enter question",
            "kbg_option_1[]": "Please enter option a",
        }
    });


//30march2020

   
   $("[name^=opening_time]").each(function () {
        $(this).rules("add", {
            required: true,
        });
    });

    $("[name^=closing_time]").each(function () {
        $(this).rules("add", {
            required: true,
        });
    });

    //1oct2020
     $("#reminder").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            reminder_text:{ required: true, noSpace:true},
            reminder_date:{ required: true, noSpace:true}
        }, messages: {
             reminder_text:{ required: "Please enter reminder text."},
            reminder_date:{ required: "Please select reminder date."},
        } 
    });
    //1oct2020
   $("#reqFoprmValidation").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules:{
            log_name:{
                required:true,
                noSpace:true
            },file_name: {
               filesize4MB: true,
               required: false

            }
             
        },
        messages:{
            log_name:{
                required : "Please enter your message"
            },submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                form.submit(); 
          }
        }
    });

   $("#addAssets").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            
            assets_category: {
                 required: true,
                noSpace:true
            },
            
        },

        messages: {
    assets_category:{
                required: "Please Enter Assets Category Name"
                    }
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                form.submit(); 
          } 
    });


$("#addAssetsDetails").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            assets_name: {
                required: true,
                noSpace:true
            },

           

            assets_category_id:{
                required: true
                
            },
              stock_measurement: {
                required: true,
                noSpace:true
            },
              stock_measurement_other_name: {
                required: true,
                noSpace:true
            }
                       
        },
        messages: {

                assets_name:{
                required: "Please Enter Item Name"
                },

                 assets_category_id:{
                required: "Please Select Assets Category"
                },

                stock_measurement:{
                required: "Please Select Measurement"
                },
                 stock_measurement_other_name:{
                    required: "Please Enter Other Name"
                }

        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                form.submit(); 
          } 

    });
     

     $("#addAssetsInventory").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            
            floor_id:{
                required: true,   
            },

            start_date:{
                required: true,
                noSpace:true,
            },

            condition_type:{
                required: true,
                noSpace:true,
            },
            
             user_id:{
                required: true,
                noSpace:true,
            },
                         
        },
        messages: {

            floor_id:{
                required: "Please Select Department"
                },
                start_date:{
                    required: "Please Enter Date"
                },
                condition_type:{
                    required: "Please Select Condition Type"
                },
                user_id:{
                    required: "Please select Custodian"
                }
               
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                form.submit(); 
          } 
    });     

     $("#OutAssetsInventory").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            inventory_stock_details:{
                required: true,
                noSpace:true,
            },
            stock_unit:{
                required: true,
                 digits:true,
                noSpace:true,
               
            },
        },
        messages: {
                 inventory_stock_details:{
                    required: "Please Enter Stock Details"
                },
                stock_unit:{
                    required: "Please Enter Stock Quantity"
                },
                
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                form.submit(); 
          } 

    });


    $("#additemDetails").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            assets_name: {
                required: true,
                noSpace:true
            },
            assets_category_id:{
                required: true,
                noSpace:true
            },
            start_date:{
                required:{
                    depends: function(element) {
                        return $('#additemDetails :input[name="user_id"]').val() !="" ;
                    }
                } 
            },
        },
        messages: {
            assets_name:{
                required: "Please Enter Item Name"
            },
            assets_category_id:{
                required: "Please Select Assets Category"
            },
            start_date:{
                required: "Please Select Handover Date"
            },
        },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            form.submit(); 
        }
    });


   ///////////////holiday validation\

    $("#holidayAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            holiday_name: {
                required: true,
                noSpace:true,
            },
            holiday_start_date: {
                required: true,
                noSpace:true,
            },
            holiday_end_date: {
                required: true,
                noSpace:true,
                
            },
           /*  holiday_description: {
                required: true,
                noSpace:true,
            }, */
            
        },
        messages: {
            holiday_name: {
                required : "Please enter holiday name",
                noSpace: "No space please and don't leave it empty",
            },
            holiday_start_date: {
                required : "Please enter holiday start date",
                noSpace: "No space please and don't leave it empty",
            },
            holiday_end_date: {
                required : "Please enter holiday end date",
                noSpace: "No space please and don't leave it empty",
            },
            /* holiday_description: {
                required : "Please enter holiday description",
                noSpace: "No space please and don't leave it empty",
            }, */
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
  

  ///////////////idea box validation//////////////

    $("#ideaCategoryAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            idea_category_name: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            idea_category_name: {
                required : "Please enter idea category name",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    ///////////////hr doc category validation//////////////

    $("#hrDocAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            hr_document_category_name: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            hr_document_category_name: {
                required : "Please enter hr document category name",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
//////////////////////hr doc validation

//$(".hrDocSubmitbtn").click(function(){
    $("#addHrDocumentAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
        
            /* hr_document_file: {
                required: $('#hr_document_file_old').val()==""  ? true :false  ,
            }, */
            hr_document_file: {
                required: {
                    depends: function(element) {
                        return $("#hr_document_file_old").is(":blank");
                    }
                },
            },
            hr_document_category_id: {
                required: true,
            },
            floor_id: {
                    required: true,
            },
            hr_document_name: {
                required: true,
                noSpace:true,
            },
            document_type: {
                required: true,
            },
            hr_document_url: {
                required: true,
                noSpace:true,
            },
        },
        messages: {
            hr_document_file: {
                required : "Please Select Document",
            },
            hr_document_category_id: {
                required : "Please Select hr document category ",
            },
            floor_id: {
                required : "Please Select Department ",
            },
            hr_document_name: {
                required : "Please Enter Document Name",
                noSpace: "No space please and don't leave it empty",
            },
            document_type: {
                required : "Please Select Document Type",
            },
            hr_document_url: {
                required : "Please Enter Document URL",
                noSpace: "No space please and don't leave it empty",
            },
            
            
        },
            submitHandler: function(form) {
                console.log($('#hr_document_file_old').val());
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
        }
    });
//});

    ///////////////leave type validation//////////////

    $("#addLeaveTypeAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            leave_type_name: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            leave_type_name: {
                required : "Please enter leave type name",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    ///////////////leave assign validation//////////////

    $("#leaveAssignAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            leave_type_id: {
                required: true,
                noSpace:true,
            },
            user_id: {
                required: true,
                noSpace:true,
            },
            user_total_leave: {
                required: true,
                noSpace:true,
            },
            assign_leave_year: {
                required: true,
                noSpace:true,
            },
            applicable_leaves_in_month: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            leave_type_id: {
                required : "Please select leave type",
                noSpace: "No space please and don't leave it empty",
            },
            user_id: {
                required : "Please select employer",
                noSpace: "No space please and don't leave it empty",
            },
            user_total_leave: {
                required : "Please enter user total leave",
                noSpace: "No space please and don't leave it empty",
            },
            assign_leave_year: {
                required : "Please enter user total leave",
                noSpace: "No space please and don't leave it empty",
            },
            applicable_leaves_in_month: {
                required : "Please enter user Applicable leaves",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    ///////////////leave count validation//////////////

    $("#addLeaveDefaultCount").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            floor_id: {
                required: true,
                noSpace:true,
            },
            leave_type_id: {
                required: true,
                noSpace:true,
            },
            number_of_leaves: {
                required: true,
                noSpace:true,
                max:1000
            },
            leave_year: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            floor_id: {
                required : "Please select department",
                noSpace: "No space please and don't leave it empty",
            },
            leave_type_id: {
                required : "Please select leave type",
                noSpace: "No space please and don't leave it empty",
            },
            number_of_leaves: {
                required : "Please enter number of leave",
                noSpace: "No space please and don't leave it empty",
            },
            leave_year: {
                required : "Please select year",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    
    ///////////////addWorkTime validation//////////////

    $("#addWorkTime").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            floor_id: {
                required: true,
                noSpace:true,
            },
            shift_time_id: {
                required: true,
                noSpace:true,
            },
            month_year: {
                required: true,
                noSpace:true,
            },
            month_days: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            floor_id: {
                required : "Please select Departement type",
                noSpace: "No space please and don't leave it empty",
            },
            shift_time_id: {
                required : "Please select shift",
                noSpace: "No space please and don't leave it empty",
            },
            month_year: {
                required : "Please enter Month",
                noSpace: "No space please and don't leave it empty",
            },
            month_days: {
                required : "Please enter days",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    ///////////////Shift Timing validation//////////////
    $("#addShiftTiming").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            shift_name: {
                required: true,
                noSpace:true,
            },
            shift_start_time: {
                required: true,
                noSpace:true,
            },
            shift_end_time: {
                required: true,
                noSpace: true,
                //notEqualTo: "#shift_start_time"
            },
           
            /* tea_break_start_time: {
                required: true,
                noSpace:true,
            },
            tea_break_end_time: {
                required: true,
                noSpace:true,
            }, */
            
            "week_off_days[]": {
                required: false,
              //  maximumSelectionLength:3
            },
            half_day_time_start: {
                required: false,
               // noSpace:true,
            },
            'alternate_weekoff_days[]': {
                required: true,
             
            },
            'alternate_week_off[]': {
                required: true,
               
            },
            late_time_start: {
                noSpace:true,
            },
            alternate_week_off: {
                required: true,
                noSpace:true,
            },
            alternate_weekoff_days: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            shift_name: {
                required : "Please enter shift name",
               
            },
            'alternate_weekoff_days[]': {
                required : "Please select Alternate Week Off Days",
                
            },
            'alternate_week_off[]': {
                required : "Please select Alternate Week Off",
             
            },
            shift_start_time: {
                required : "Please select shift start time",
                noSpace: "No space please and don't leave it empty",
            },
            shift_end_time: {
                required : "Please enter shift end time",
                noSpace: "No space please and don't leave it empty",
            },
            
            /* tea_break_start_time: {
                required : "Please enter tea break start time",
                noSpace: "No space please and don't leave it empty",
            },
            tea_break_end_time: {
                required : "Please select tea break end time",
                noSpace: "No space please and don't leave it empty",
            }, */
            
            "week_off_days[]": {
                required : "Please enter week off days",
              
            },
            half_day_time_start: {
                required : "Please enter half day time start",
                noSpace: "No space please and don't leave it empty",
            },
            late_time_start: {
                noSpace: "No space please and don't leave it empty",
            },
            alternate_week_off: {
                required : "Please select alternate week off",
                noSpace: "No space please and don't leave it empty",
            },
            alternate_weekoff_days: {
                required : "Please enter alternate weekoff days",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    

    $("#addCourseForm").validate({
  
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
         
            course_title: {
                required: true,
                    noSpace:true,
            },
           
            course_description: {
                    required: true,
                    noSpace:true,
            }  
        },
        messages: {
            
            course_title: {
                required : "Please Enter Title ",
                noSpace: "No space please and don't leave it empty",
            },
            
            course_description: {
                required : "Please Enter Description",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    
    $("#addUserBank").validate({
  
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            block_id: {
                required: true,
                    noSpace:true,
            },
            floor_id: {
                required: true,
                    noSpace:true,
            },

            user_id: {
                required: true,
                    noSpace:true,
            },           
            account_holders_name: {
                    required: true,
                    noSpace:true,
            },  
            bank_name: {
                    required: true,
                    noSpace:true,
            },  
            bank_branch_name: {
                    required: true,
                    noSpace:true,
            },  
            account_type: {
                    required: true,
            },  
            account_no: {
                required: true,
                noSpace:true,
            },
           
            ifsc_code: {
                    required: true,
                    noSpace:true,
            },
            /* pan_card_no: {
                required: true,
                    noSpace:true,
            },
             
            crn_no: {
                    required: true,
                    noSpace:true,
            } */  
        },
        messages: {
            floor_id: {
                required : "Please Select Department",
                noSpace: "No space please and don't leave it empty",
            },
            block_id: {
                required : "Please Select Branch",
                noSpace: "No space please and don't leave it empty",
            },
            user_id: {
                required : "Please Select Employee",
                noSpace: "No space please and don't leave it empty",
            },
            
            account_holders_name: {
                required : "Please Enter Account Holder's Name ",
                noSpace: "No space please and don't leave it empty",
            },
            bank_name: {
                required : "Please Enter Bank Name ",
                noSpace: "No space please and don't leave it empty",
            },
            bank_branch_name: {
                required : "Please Enter Bank Branch Name ",
                noSpace: "No space please and don't leave it empty",
            },
            
            account_no: {
                required : "Please Enter Account",
                noSpace: "No space please and don't leave it empty",
            },
            account_type: {
                required : "Please Enter Account",
            },
            ifsc_code: {
                required : "Please Enter IFSC",
                noSpace: "No space please and don't leave it empty",
            },
           /*  pan_card_no: {
                required : "Please Enter PAn Card Number",
                noSpace: "No space please and don't leave it empty",
            },
            crn_no: {
                required : "Please Enter CRN  Number",
                noSpace: "No space please and don't leave it empty",
            } */
            
        },
            submitHandler: function(form) {
                
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });  
    
    ///////////////course lesson validation//////////////
    
        $("#addCourseLesson").validate({
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) { 
                    error.insertAfter(element.parent());      // radio/checkbox?
                } else if (element.hasClass('select2-hidden-accessible')) {     
                    error.insertAfter(element.next('span'));  // select2
                    element.next('span').addClass('error').removeClass('valid');
                } else {                                      
                    error.insertAfter(element);               // default
                }
            },
            rules: {
                course_id: {
                    required: true,
                    noSpace:true,
                },
                lesson_type: {
                        required: true,
                        noSpace:true,
                },
                lesson_type_id: {
                        required: true,
                        noSpace:true,
                },
                
                course_chapter_id: {
                        required: true,
                        noSpace:true,
                },
                lesson_text: {
                        required: true,
                        noSpace:true,
                },
                lesson_video_link: {
                        required: true,
                        noSpace:true,

                },
                lesson_title: {
                        required: true,
                        noSpace:true,
                },
                lesson_description: {
                    required: true,
                    noSpace:true,
                }, 
                lesson_video: {
                    filesize32MB:true,
                    required:  $('#lesson_video_old').val()==""  ? true :false,
                }, 
            },
            messages: {
                course_id: {
                    required : "Please Select Course",
                    noSpace: "No space please and don't leave it empty",
                },
                
                lesson_type: {
                    required : "Please Select Lesson Type ",
                    noSpace: "No space please and don't leave it empty",
                },
                lesson_video_link: {
                    required : "Please Select Lesson Link ",
                    noSpace: "No space please and don't leave it empty",
                },
                lesson_text: {
                    required : "Please Enter Lesson Text ",
                    noSpace: "No space please and don't leave it empty",
                },
                lesson_title: {
                    required : "Please Enter Lesson Title ",
                    noSpace: "No space please and don't leave it empty",
                },
                course_chapter_id: {
                    required : "Please Select Chapter ",
                    noSpace: "No space please and don't leave it empty",
                },
                
                lesson_type_id: {
                    required : "Please Select Lesson Type ",
                    noSpace: "No space please and don't leave it empty",
                },
                
                lesson_video: {
                    required : "Please select lesson File",
                }
                
            },
            submitHandler: function (form) {
                error = 0;
                var clsVal = $('#summernoteImgage').val();
                var lesson_type_id = $('#lesson_type_id').val();
                if (lesson_type_id == "11") {
                    
                    if (clsVal != "") {
                        $(':input[type="submit"]').prop('disabled', true);
                        $(".ajax-loader").show();
                        form.submit(); 
                    }
                    else {
                        swal('Please Enter Some Text')
                    }
                }
                else
                {
                    $(':input[type="submit"]').prop('disabled', true);
                        $(".ajax-loader").show();
                        form.submit(); 
                }
            //     if($.trim(clsVal) =="<p><br></p>" ){
            //      error++;
            //    }
                   
            }
        });
    


    ///////////////Attendance Type validation//////////////

    $("#addAttendanceType").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            attendance_type_name: {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            attendance_type_name: {
                required : "Please enter attendance type name",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    $("#addLeaveAssignBulk").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            user_id: {
                required: true,
                noSpace:true,
            },
            "user_total_leave[]": {
                required: true,
                noSpace:true,
            }
            
        },
        messages: {
            user_id: {
                required : "Please select employee",
                noSpace: "No space please and don't leave it empty",
            },
            "user_total_leave[]": {
                required : "Please enter user total leave",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    $("#courseChapterAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            course_id: {
                required: true,
                noSpace:true,
            },
            chapter_title: {
                required: true,
                noSpace:true,
            },
            chapter_description: {
                noSpace:true,
            }
            
        },
        messages: {
            course_id: {
                required : "Please select course",
                noSpace: "No space please and don't leave it empty",
            },
            chapter_title: {
                required : "Please enter chapter title",
                noSpace: "No space please and don't leave it empty",
            },
            chapter_description: {
                required : "Please enter chapter description",
                noSpace: "No space please and don't leave it empty",
            }
            
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });


    $(".salaryAdd").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            floor_id: {
                required: true,
                noSpace:true,
            },
            user_id: {
                required: true,
                noSpace:true,
            },
            gross_salary: {
                required: true,
                noSpace:true,
            },
            salary_type: {
                required: true,
                noSpace:true,
            },
            salary_mode: {
                required: true,
                noSpace:true,
            },
            employee_net_salary: {
                required: true,
               
            },
            salary_amount: {
                required: true,
                noSpace:true,
            },
            salary_increment_date : {
                required: true,
                noSpace:true,
            },
            salary_start_date : {
                required: true,
                noSpace:true,
            },
            
            
            salary_increment_remark : {
                required: true,
                noSpace:true,
            },
            total_deduction_salary : {
                required: true,
                noSpace:true,
            },
            total_earning_salary: {
                required: true,
                noSpace:true,
            },
            total_net_salary: {
                required: true,
                noSpace:true,
            },
            
            
        },
        messages: {
            floor_id: {
                required : "Please select department",
                noSpace: "No space please and don't leave it empty",
            },
            user_id: {
                required : "Please select Employee",
                noSpace: "No space please and don't leave it empty",
            },
            gross_salary: {
                required : "Please Enter Gross Salary",
                noSpace: "No space please and don't leave it empty",
            },
            employee_net_salary: {
                required : "Net Salary Not Avaliable",
            },
            salary_mode: {
                required : "Please select salary Mode",
                noSpace: "No space please and don't leave it empty",
            },
           
            salary_type: {
                required : "Please enter salary type",
                noSpace: "No space please and don't leave it empty",
            },
           
            net_salary: {
                required : "Please enter salary amount",
                noSpace: "No space please and don't leave it empty",
            },
            salary_increment_date: {
                required : "provide increment date",
                noSpace: "No space please and don't leave it empty",
            },
            total_deduction_salary: {
                required : "No Deduction Applied",
                noSpace: "No space please and don't leave it empty",
            },
            total_earning_salary: {
                required : "No Deduction Applied",
                noSpace: "No space please and don't leave it empty",
            },
            total_net_salary: {
                required : "No Net Salary calculation",
                noSpace: "No space please and don't leave it empty",
            },
            
           
            salary_start_date: {
                required : " Please Select Start Date",
                noSpace: "No space please and don't leave it empty",
            },
            
            salary_increment_remark: {
                required : "provide increment amount",
                noSpace: "No space please and don't leave it empty",
            },
         


        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
    $("#salaryAddFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            floor_id: {
                required: true,
                noSpace:true,
            },
            user_id: {
                required: true,
                noSpace:true,
            },
            gross_salary: {
                required: true,
                
            },
            
            
            
        },
        messages: {
            floor_id: {
                required : "Please select department",
                noSpace: "No space please and don't leave it empty",
            },
            user_id: {
                required : "Please select Employee",
                noSpace: "No space please and don't leave it empty",
            },
            gross_salary: {
                required : "Please Enter Gross Salary",
                
            },
        },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });

    /* Dollop Infotech Date 22/Oct/2021 -  */
$("#addExpenseAmountForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
            max_expense_amount: {
            required: true,
            noSpace:true,
        }
        
    },
    messages: {
        max_expense_amount: {
            required : "Please enter max expense amount",
            noSpace: "No space please and don't leave it empty",
        }
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});


$("#payExpenseAmountForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        expense_payment_mode: {
            required: true
        }
    },
    messages: {
        expense_payment_mode: {
            required : "Please select expense payment mode"
        }
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
      }
});


$("#addSiteForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        site_name: {
            required: true,
            noSpace:true,
        },
        'br_id[]': {
            required: true,
            
        },
        'floor_id[]': {
            required: true,
           
        },
        "site_manager_id[]": {
            required: true,
        },
        "procurement_id[]": {
            required: true,
        },
        "finance_manager_id[]": {
            required: true,
        },
        site_address: {
            required: true,
            noSpace:true,
        }
        
    },
    messages: {
        site_name: {
            required : "Please Enter Site name",
            noSpace: "No space please and don't leave it empty",
        },
        "site_manager_id[]": {
            required : "Please select manager",
            noSpace: "No space please and don't leave it empty",
        },
        "procurement_id[]": {
            required : "Please select procurement manager",
            noSpace: "No space please and don't leave it empty",
        },
        "finance_manager_id[]": {
            required : "Please select finance manager",
            noSpace: "No space please and don't leave it empty",
        },
        'br_id[]': {
            required : "Plese Select Branch",
            noSpace: "No space please and don't leave it empty",
        },
        'floor_id[]': {
            required : "Plese Select Department",
            noSpace: "No space please and don't leave it empty",
        },
        site_address: {
            required : "Please enter address",
            noSpace: "No space please and don't leave it empty",
        }
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addUnitMeasurementForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        unit_measurement_name: {
            required: true,
            noSpace:true,
        },
       vendor_id: {
            required: true,
            noSpace:true,
        },
        
        
    },
    messages: {
        unit_measurement_name: {
            required : "Please Enter Unit name",
            noSpace: "No space please and don't leave it empty",
        },
        vendor_id: {
            required : "Please Select Vendor",
            noSpace: "No space please and don't leave it empty",
        },
       
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addproductCategoryForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        category_name: {
            required: true,
            noSpace:true,
        },
       /*  vendor_id: {
            required: true,
            noSpace:true,
        }, */
        
        
    },
    messages: {
        category_name: {
            required : "Please Enter Category name",
            noSpace: "No space please and don't leave it empty",
        },
        /* vendor_id: {
            required : "Please Select Vendor",
            noSpace: "No space please and don't leave it empty",
        }, */
       
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});


$("#addsubCategoryForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        sub_category_name: {
            required: true,
            noSpace:true,
        },
        product_category_id: {
            required: true,
            noSpace:true,
        },
        /* vendor_id: {
            required: true,
            noSpace:true,
        }, */
        
    },
    messages: {
        sub_category_name: {
            required : "Please Enter Sub Category name",
            noSpace: "No space please and don't leave it empty",
        },
        product_category_id: {
            required : "Please Select Category",
            noSpace: "No space please and don't leave it empty",
        },
        /* vendor_id: {
            required : "Please Select Vendor",
            noSpace: "No space please and don't leave it empty",
        }, */
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addProductFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        product_name: {
            required: true,
            noSpace:true,
        },
        product_category_id: {
            required: true,
            noSpace:true,
        },
        "product_variant_name[]": {
            required: true,
            noSpace:true,
        },
        vendor_id: {
            required: true,
            noSpace:true,
        },
        unit_measurement_id: {
            required: true,
            noSpace:true,
        },
        minimum_order_quantity: {
            required: true,
            noSpace:true,
        },
        product_image: {
            image: true,
        },
        
    },
    messages: {
        product_name: {
            required : "Please Enter product name",
            noSpace: "No space please and don't leave it empty",
        },
        product_category_id: {
            required : "Please Select category",
            noSpace: "No space please and don't leave it empty",
        },
        'product_variant_name[]': {
            required : "Please Enter Variant Name",
            noSpace: "No space please and don't leave it empty",
        },
        minimum_order_quantity: {
            required : "Please Enter Minimum Quantity",
            noSpace: "No space please and don't leave it empty",
        },
        /* product_sub_category_id: {
            required : "Please Select Sub category",
            noSpace: "No space please and don't leave it empty",
        }, */
        vendor_id: {
            required : "Please Select Vendor",
            noSpace: "No space please and don't leave it empty",
        },
        unit_measurement_id: {
            required : "Please Select Unit of Measure",
            noSpace: "No space please and don't leave it empty",
        },
        product_image: {
            image : "Please add a valid image file.",
        },
       
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#addProductPriceFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        product_id: {
            required: true,
            
        },
        product_category_id: {
            required: true,
            
        },
       /*  product_sub_category_id: {
            required: true,
            
        }, */
        vendor_id: {
            required: true,
            
        },
        product_variant_id: {
            required: true,
            
        },
        minimum_order_quantity: {
            required: true,
            noSpace:true,
        },
        product_price: {
            required: true,
            noSpace:true,
        },
        
        
    },
    messages: {
        product_price: {
            required : "Please Enter product price",
            noSpace: "No space please and don't leave it empty",
        },
        product_category_id: {
            required : "Please Select category",
        },
        
        minimum_order_quantity: {
            required : "Please Enter Minimum Quantity",
            noSpace: "No space please and don't leave it empty",
        },
        // product_sub_category_id: {
        //     required : "Please Select Sub category",
        // },
        vendor_id: {
            required : "Please Select Vendor",
        },
        product_variant_id: {
            required : "Please Select Product Variant",
        },
        product_id: {
            required : "Please Select Product",
        },
       
       
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addServiceProviderDetail").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        service_provider_bank_name: {
            required: true,
            noSpace:true,
        },
        service_provider_account_no: {
            required: true,
            noSpace:true,
        },
        service_provider_ifsc: {
            required: true,
            noSpace:true,
        },
        account_holdar_name: {
            required: true,
            noSpace:true,
        },
    },
    messages: {
        service_provider_bank_name: {
            required : "Please Enter Service Provider Bank Name",
            noSpace: "No space please and don't leave it empty",
        },
        service_provider_account_no: {
            required : "Please Enter Service Provider Acount No.",
            noSpace: "No space please and don't leave it empty",
        },
        service_provider_ifsc: {
            required : "Please Enter IFSC code",
            noSpace: "No space please and don't leave it empty",
        },
        account_holdar_name: {
            required : "Please Enter account holder name",
            noSpace: "No space please and don't leave it empty",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addXeroxCategoryForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        xerox_type_name: {
            required: true,
            noSpace:true,
        },
    },
    messages: {
        xerox_type_name: {
            required : "Please Enter xerox Category name",
            noSpace: "No space please and don't leave it empty",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addXeroxPaperForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        xerox_paper_size_name: {
            required: true,
            noSpace:true,
        },
    },
    messages: {
        xerox_paper_size_name: {
            required : "Please Enter Paper size",
            noSpace: "No space please and don't leave it empty",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});


/* Dollop Infotech Date 22/Oct/2021 -  */
    /* Dollop Infotech Date 15-Nov-2021 -  */
$("#addCanteenVendorFrom").validate({
  rules: {

        vendor_category_id: {
            required: true,
            noSpace:true,
        },
        vendor_name: {
            required: true,
            noSpace:true,
        },
        vendor_email: {
            noSpace: true,
            customemail:true,
            email: true,
            remote:
                {
                    url: 'UniqueVendorEmail.php',
                    type: "post",
                    data:
                    {
                        oldEmail: function()
                        {
                            return $('#addCanteenVendorFrom :input[name="vendor_email"]').val();
                        },
                        vendor_id: function()
                        {
                            return $('#addCanteenVendorFrom :input[name="vendor_id"]').val();
                        },
                    } 
                }
        },
        vendor_mobile_country_code: {
            required: true,
            noSpace:true,
        },
        vendor_mobile: {
            required: true,
            noSpace:true,
            minlength: 10,
            maxlength: 10,
        },
        vendor_logo: {
            image: true,
            filesize12MB: true,
        },
    },
    messages: {
        vendor_category_id: {
            required : "Please Select Vendor Category",
            noSpace: "No space please and don't leave it empty",
        },
        vendor_name: {
            required : "Please Enter vendor name",
            noSpace: "No space please and don't leave it empty",
        },
        vendor_email: {
            noSpace: "No space please and don't leave it empty",
            remote: "Vendor Email is already exists",
            customemail:"Please Enter Valid Email"
        },
        vendor_mobile_country_code: {
            required : "Please Select vendor mobile country code",
            noSpace: "No space please and don't leave it empty",
        },
        vendor_mobile: {
            required : "Please Enter vendor mobile",
            noSpace: "No space please and don't leave it empty",
            minlength: "Please enter valid mobile number",
            maxlength: "Please enter valid mobile number",
        },
    },
          
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
  
      submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
  
});   

$("#addStationaryVendorFrom").validate({
    rules: {
  
          vendor_category_id: {
              required: true,
              noSpace:true,
          },
          vendor_name: {
              required: true,
              noSpace:true,
          },
          vendor_email: {
              noSpace:true,
              email: true,
              customemail:true,
              remote:
                  {
                      url: 'UniqueVendorEmail.php',
                      type: "post",
                      data:
                      {
                          oldEmail: function()
                          {
                              return $('#addStationaryVendorFrom :input[name="vendor_email"]').val();
                          },
                          vendor_id: function()
                            {
                                return $('#addStationaryVendorFrom :input[name="vendor_id"]').val();
                            },
                      } 
                  }
          },
          vendor_mobile_country_code: {
              required: true,
              noSpace:true,
          },
          vendor_mobile: {
              required: true,
              noSpace:true,
              minlength: 10,
              maxlength: 10,
          },
          vendor_logo: {
              image: true,
              filesize12MB: true,
          },
      },
      messages: {
          vendor_category_id: {
              required : "Please Select Vendor Category",
              noSpace: "No space please and don't leave it empty",
          },
          vendor_name: {
              required : "Please Enter vendor name",
              noSpace: "No space please and don't leave it empty",
          },
          vendor_email: {
              noSpace: "No space please and don't leave it empty",
              remote: "Vendor Email is already exists",
              customemail: "Please Enter Valid Email"
          },
          vendor_mobile_country_code: {
              required : "Please Select vendor mobile country code",
              noSpace: "No space please and don't leave it empty",
          },
          vendor_mobile: {
              required : "Please Enter vendor mobile",
              noSpace: "No space please and don't leave it empty",
              minlength: "Please enter valid mobile number",
              maxlength: "Please enter valid mobile number",
          },
      },
            
          errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) { 
                  error.insertAfter(element.parent());      // radio/checkbox?
              } else if (element.hasClass('select2-hidden-accessible')) {     
                  error.insertAfter(element.next('span'));  // select2
                  element.next('span').addClass('error').removeClass('valid');
              } else {                                      
                  error.insertAfter(element);               // default
              }
          },
    
        submitHandler: function(form) {
              $(':input[type="submit"]').prop('disabled', true);
              $(".ajax-loader").show();
              form.submit(); 
        }
    
  });
  $(".sbmitbtn").click(function(){ 
    $("#addVendorProductCategoryForm").validate({
            rules: {  
            vendor_product_category_name: {
                required: true,
                noSpace:true,
            },
            vendor_id: {
                required: true,
                noSpace:true,
            },
            vendor_product_category_image: {
                required: $('#vendor_product_category_id').val()==""  ? true :false  ,
            }, 

        },
        messages: {
            vendor_product_category_name: {
                required : "Please Enter Category Name",
                noSpace: "No space please and don't leave it empty",
            },
            vendor_id: {
                required : "Please Select Vendor",
            },
            vendor_product_category_image: {
                required : "Please Select an image",

            },
            
        },
            
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) { 
                    error.insertAfter(element.parent());      // radio/checkbox?
                } else if (element.hasClass('select2-hidden-accessible')) {     
                    error.insertAfter(element.next('span'));  // select2
                    element.next('span').addClass('error').removeClass('valid');
                } else {                                      
                    error.insertAfter(element);               // default
                }
            },
            

            
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
        }
    });
  });

$("#addCanteenProductForm").validate({
rules: {  
        vendor_product_category_id: {
        required: true,
        noSpace:true,
    },
    vendor_product_name: {
        required: true,
        noSpace:true,
    },

},
messages: {
    vendor_product_category_id: {
        required : "Please Select Product Category",
        noSpace: "No space please and don't leave it empty",
    },
    vendor_product_name: {
        required : "Please Enter Product Name",
        noSpace: "No space please and don't leave it empty",
    },
    
},
      
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
      

    
    submitHandler: function(form) {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit(); 
  }
});

$("#addStationaryProductForm").validate({
    rules: {  
            vendor_product_category_id: {
            required: true,
            noSpace:true,
        },
        vendor_product_name: {
            required: true,
            noSpace:true,
        },
    
    },
    messages: {
        vendor_product_category_id: {
            required : "Please Select Product Category",
            noSpace: "No space please and don't leave it empty",
        },
        vendor_product_name: {
            required : "Please Enter Product Name",
            noSpace: "No space please and don't leave it empty",
        },
        
    },
          
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
          
    
        
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
    });


$.validator.addMethod('UploadFiles5', function (value, element, arg) {
    var FIlelength = 5;

    if(element.files.length <= FIlelength){

        return true;
    }else{
        return false;
    }
}, $.validator.format("You are allowed to upload only maximum 5 files at a time"));


$("#addStationaryProductVariantFrom").validate({
        rules: {
              vendor_product_id: {
                  required: true,
                  noSpace:true,
              },
              vendor_product_variant_name: {
                  required: true,
                  noSpace:true,
              },
              vendor_product_variant_price: {
                  required: true,
                  noSpace:true,
              },
              "vendor_product_image_name[]": {
                  required: true,
                  image: true,
                  UploadFiles5: true,
              },
          },
          messages: {
              vendor_product_id: {
                  required : "Please Select Product",
                  noSpace: "No space please and don't leave it empty",
              },
              vendor_product_variant_name: {
                  required : "Please Enter variant name",
                  noSpace: "No space please and don't leave it empty",
              },
              vendor_product_variant_price: {
                  required : "Please Enter price",
                  noSpace: "No space please and don't leave it empty",
              },
              "vendor_product_image_name[]": {
                  required : "Please Select image",
                  
              },
          },
                
              errorPlacement: function (error, element) {
                  if (element.parent('.input-group').length) { 
                      error.insertAfter(element.parent());      // radio/checkbox?
                  } else if (element.hasClass('select2-hidden-accessible')) {     
                      error.insertAfter(element.next('span'));  // select2
                      element.next('span').addClass('error').removeClass('valid');
                  } else {                                      
                      error.insertAfter(element);               // default
                  }
              },
        
            submitHandler: function(form) {
                  $(':input[type="submit"]').prop('disabled', true);
                  $(".ajax-loader").show();
                  form.submit(); 
            }
        
}); 

$("#addCanteenProductVariantFrom").validate({
    rules: {
          vendor_product_id: {
              required: true,
              noSpace:true,
          },
          vendor_product_variant_name: {
              required: true,
              noSpace:true,
          },
          vendor_product_variant_price: {
              required: true,
              noSpace:true,
          },
          "vendor_product_image_name[]": {
              required: true,
              image: true,
              UploadFiles5: true,
          },
      },
      messages: {
          vendor_product_id: {
              required : "Please Select Product",
              noSpace: "No space please and don't leave it empty",
          },
          vendor_product_variant_name: {
              required : "Please Enter variant name",
              noSpace: "No space please and don't leave it empty",
          },
          vendor_product_variant_price: {
              required : "Please Enter price",
              noSpace: "No space please and don't leave it empty",
          },
          "vendor_product_image_name[]": {
              required : "Please Select image",
              
          },
      },
            
          errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) { 
                  error.insertAfter(element.parent());      // radio/checkbox?
              } else if (element.hasClass('select2-hidden-accessible')) {     
                  error.insertAfter(element.next('span'));  // select2
                  element.next('span').addClass('error').removeClass('valid');
              } else {                                      
                  error.insertAfter(element);               // default
              }
          },
    
        submitHandler: function(form) {
              $(':input[type="submit"]').prop('disabled', true);
              $(".ajax-loader").show();
              form.submit(); 
        }
    
}); 

$("#addProductVariantImageForm").validate({
    rules: {
          vendor_product_image_name: {
              required: true,
              image: true,
              UploadFiles5: true,
          },
      },
      messages: {
          vendor_product_image_name: {
              required : "Please Select image",
              
          },
      },
            
          errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) { 
                  error.insertAfter(element.parent());      // radio/checkbox?
              } else if (element.hasClass('select2-hidden-accessible')) {     
                  error.insertAfter(element.next('span'));  // select2
                  element.next('span').addClass('error').removeClass('valid');
              } else {                                      
                  error.insertAfter(element);               // default
              }
          },
    
        submitHandler: function(form) {
              $(':input[type="submit"]').prop('disabled', true);
              $(".ajax-loader").show();
              form.submit(); 
        }
    
}); 

$("#addAdminPinForm").validate({
    rules: {
        admin_pin: {
              required: true,
              digits: true,
              remote:
                {
                    url: 'UniqueAdminPin.php',
                    type: "post",
                    data:
                    {
                        oldPin: function()
                        {
                            return $('#addAdminPinForm :input[name="admin_pin"]').val();
                        },
                        admin_id: function()
                        {
                            return $('#addAdminPinForm :input[name="admin_id"]').val();
                        },
                    } 
                }
          },
          confirm_admin_pin: {
              required: true,
              digits: true,
              equalTo: "#admin_pin"
          },
      },
      messages: {
        admin_pin: {
              required : "Please enter Pin",
              remote: "Entered Pin is already exists"
          },
          confirm_admin_pin: {
              required : "Please enter  confirm Pin ",
              equalTo : "Please enter the same pin as above"
              
          },
      },
            
          errorPlacement: function (error, element) {
              if (element.parent('.input-group').length) { 
                  error.insertAfter(element.parent());      // radio/checkbox?
              } else if (element.hasClass('select2-hidden-accessible')) {     
                  error.insertAfter(element.next('span'));  // select2
                  element.next('span').addClass('error').removeClass('valid');
              } else {                                      
                  error.insertAfter(element);               // default
              }
          },
    
        submitHandler: function(form) {
              $(':input[type="submit"]').prop('disabled', true);
              $(".ajax-loader").show();
              form.submit(); 
        }
    
});
/* Dollop Infotech Date 15-Nov-2021 -  */
/* Dollop Infotech Date 25-Nov-2021 -  */

//$(".sbmitbtnUserCmpAtnd").click(function(){
    $("#UsercmpAtnType").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            
           
            user_geofence_range: {
                required: true,
                noSpace:true,
            },
            floor_id: {
                required: true,
                noSpace:true,
            },
           
            user_id: {
                required: true,
                noSpace:true,
            },
            br_id: {
                required: true,
                noSpace:true,
            },
           
            user_latitude: {
                required: true,
                    noSpace:true,
            },
           
            
            user_longitude: {
                    required: true,
                    noSpace:true,
            }  
        },
        messages: {
            user_geofence_range: {
                required : "Please Select Range",
                noSpace: "No space please and don't leave it empty",
            },
            floor_id: {
                required : "Please Select Department",
                noSpace: "No space please and don't leave it empty",
            },
            
            user_id: {
                required : "Please Select user",
                noSpace: "No space please and don't leave it empty",
            },
            
            br_id: {
                required : "Please Select Branch",
                noSpace: "No space please and don't leave it empty",
            },
            
            user_latitude: {
                required : "Please Select Latittude",
                noSpace: "No space ",
            },
            
            user_longitude: {
                required : "Please Select Longitude ",
                noSpace: "No space ",
            },
            
            
            
        },
            submitHandler: function(form) {
                
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
//});

$('.updateBloCkGeo').click(function(){
    $("#updateBloCkGeoFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            
           
            block_geofence_range: {
                required: true,
                noSpace:true,
            },
           
            block_latitude: {
                required: true,
                    noSpace:true,
            },
           
            
            block_longitude: {
                    required: true,
                    noSpace:true,
            }  
        },
        messages: {
            block_geofence_range: {
                required : "Please Select Range",
                noSpace: "No space please and don't leave it empty",
            },
            
            block_latitude: {
                required : "Please Select Latittude",
                noSpace: "No space ",
            },
            
            block_longitude: {
                required : "Please Select Longitude ",
                noSpace: "No space ",
            },
            
            
            
        },
            submitHandler: function(form) {
                
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
          }
    });
})
$("#addRequirementsForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        
       
        site_id: {
            required: true,
            noSpace:true,
        },
       
        vendor_id: {
            required: true,
                noSpace:true,
        },
       
        
       /*  po_number: {
                required: true,
                noSpace:true,
        }  */ 
    },
    messages: {
        site_id: {
            required : "Please Select site",
            noSpace: "No space please and don't leave it empty",
        },
        
        vendor_id: {
            required : "Please Select vendor",
            noSpace: "No space ",
        },
        
        /* po_number: {
            required : "Please Select Enter PO Number ",
            noSpace: "No space ",
        }, */
        
        
        
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

/* Dollop Infotech Date 25-Nov-2021 -  */
  $("#changeAttendance").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        
       
        punch_out_date: {
            required: true,
            noSpace:true,
        },
       
        punch_out_time: {
            required: true,
                noSpace:true,
        },
       
    },
    messages: {
        punch_out_date: {
            required : "Please Select Date",
            noSpace: "No space please and don't leave it empty",
        },
        
        punch_out_time: {
            required : "Please Select Time",
            noSpace: "No space ",
        },
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});  


$("#zoneAdd").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        zone_name: {
            required: true,
            noSpace:true,
        }
        
    },
    messages: {
        zone_name: {
            required : "Please enter zone name",
            noSpace: "No space please and don't leave it empty",
        }
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#employeeLevelAdd").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        level_name: {
            required: true,
            noSpace:true,
        }
        
    },
    messages: {
        level_name: {
            required : "Please enter Employee level name",
            noSpace: "No space please and don't leave it empty",
        }
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#updateAttendance").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
      
        attendance_date_end: {
            required: true,
            noSpace:true,
        },
       
        punch_out_time: {
            required: true,
                noSpace:true,
        },
       
    },
    messages: {
        attendance_date_end: {
            required : "Please Select Date",
            noSpace: "No space please and don't leave it empty",
        },
        
        punch_out_time: {
            required : "Please Select Time",
            noSpace: "No space ",
        },
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});  

$("#addEmployeeAccessFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
      
        access_type: {
            required: true,
        },
        access_by_id: {
            required: true,
        },
        block_id: {
            required: true,
        },
        floor_id: {
            required: true,
        },
        access_for: {
            required: true,
        },
        access_mode: {
            required: true,
        },
        "access_for_id[]": {
            required: true,
        },
        "floorId[]": {
            required: true,
        },
        "blockId[]": {
            required: true,
        },
        
       
    },
    messages: {
        access_type: {
            required : "Please Select Access type",
        },
        access_mode: {
            required : "Please Select Access mode",
        },
        access_for: {
            required : "Please Select Access For",
        },
        access_by_id: {
            required : "Please Select Access by Employee ",
        },
        block_id: {
            required : "Please Select Branch",
        },
        floor_id: {
            required : "Please Select Department",
        },
        "access_for_id[]": {
            required : "Please Select Access for Employee",
        },
        "floorId[]": {
            required : "Please Select Department for access",
        },
        "blockId[]": {
            required : "Please Select Branch for access",
        },
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
}); 


//////////////14-01-2022 (Shubham)/////////////////////////////////////////////
$("#addCompanyAboutUsFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        company_about_us_top_image: {
            required: $('#company_about_us_id').val()==""  ? true :false  ,
        },
        company_about_us_bottom_title: {
            required: true,
        },
        company_about_us_top_description: {
            required: true,
        },
        company_about_us_bottom_description: {
            required: true,
        },
        company_about_us_image_one: {
            required: $('#company_about_us_id').val()==""  ? true :false  ,
        },
        company_about_us_image_two: {
            required: $('#company_about_us_id').val()==""  ? true :false  ,
        },
    },
    messages: {
        company_about_us_top_image: {
            required : "Please Select top image",
        },
        company_about_us_bottom_title: {
            required : "Please enter bottom title ",
        },
        company_about_us_top_description: {
            required : "Please enter top description",
        },
        company_about_us_bottom_description: {
            required : "Please enter bottom description",
        },
        company_about_us_image_one: {
            required : "Please Select image 1",
        },
        company_about_us_image_two: {
            required : "Please Select image 2",
        },
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
}); 

$("#addCompanyHomeMasterForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        company_home_title: {
            required: true,
        },
        company_home_description: {
            required: true,
        },
    },
    messages: {
        company_home_title: {
            required : "Please enter company title",
        },
        company_home_description: {
            required : "Please enter company description ",
        },
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
}); 

$("#addCompanyHomeSliderFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        company_home_slider_image: {
            required: $('#company_home_slider_id').val()==""  ? true :false  ,
        },
        company_home_slider_title: {
            required: true,
        },
        company_home_slider_description: {
            required: true,
        },
        company_home_slider_url: {
            required: true,
        },
        company_home_slider_mobile: {
            required: true,
        },
    },
    messages: {
        company_home_slider_image: {
            required : "Please Select slider image",
        },
        company_home_slider_title: {
            required : "Please enter slider title ",
        },
        company_home_slider_description: {
            required : "Please enter slider description",
        },
        company_home_slider_url: {
            required : "Please enter slider url",
        },
        company_home_slider_mobile: {
            required : "Please enter slider mobile",
        },
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
}); 


$("#addCompanyCurrentOpeningFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        company_current_opening_title: {
            required: true,
        },
        company_current_opening_position: {
            required: true,
        },
        company_current_opening_timing: {
            required: true,
        },
        "company_current_opening_schedule[]": {
            required: true,
        },
        company_current_opening_address: {
            required: true,
        },
        company_current_opening_pay_scale: {
            required: true,
        },
        company_current_opening_salary_period: {
            required: true,
        },
        company_current_opening_experience: {
            required: true,
        },
        company_current_opening_description: {
            required: true,
        },
    },
    messages: {
        company_current_opening_title: {
            required : "Please enter opening title",
        },
        company_current_opening_position: {
            required : "Please select opening position ",
        },
        company_current_opening_timing: {
            required : "Please select opening timing",
        },
        "company_current_opening_schedule[]": {
            required : "Please select opening schedule",
        },
        company_current_opening_address: {
            required : "Please enter opening address ",
        },
        company_current_opening_pay_scale: {
            required : "Please enter pay scale ",
        },
        company_current_opening_salary_period: {
            required : "Please select salary period ",
        },
        company_current_opening_experience: {
            required : "Please enter experience ",
        },
        company_current_opening_description: {
            required : "Please enter opening description ",
        },
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
}); 

//////////////14-01-2022 (Shubham)/////////////////////////////////////////////


//////////////17-01-2022 (Shubham)/////////////////////////////////////////////
$("#addCompanyServiceForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        company_service_name: {
            required: true,
            noSpace:true,
        },
        company_service_image: {
            required: {
                depends: function(element) {
                    return $("#company_service_image_old").is(":blank");
                }
            },
        },
    },
    messages: {
        company_service_name: {
            required : "Please enter service name",
            noSpace: "No space please and don't leave it empty",
        },
        company_service_image: {
            required : "Please select service image",
        },
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
}); 


$("#addCompanyServiceDetailForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        company_service_id: {
            required: true,
        },
        company_service_details_title: {
            required: true,
        },
        company_service_details_description: {
            required: true,
        },
        no_of_list: {
            required: $('#company_service_details_id').val()==""  ? true :false  ,
        },
    },
    messages: {
        company_service_id: {
            required : "Please select service",
        },
        company_service_details_title: {
            required : "Please enter service detail tilte",
        },
        company_service_details_description: {
            required : "Please enter service detail description",
        },
        no_of_list: {
            required : "Please enter No of Service Detail List",
        },
    },
        submitHandler: function(form) {
            
            //$(':input[type="submit"]').prop('disabled', true);
            //$(".ajax-loader").show();
            //form.submit(); 
      }
});


$("#addCompanyServiceMoreDetailForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        company_service_details_id: {
            required: true,
        },
        company_service_more_details_name: {
            required: true,
        },
        company_service_more_details_image: {
            required: $('#company_service_more_details_id').val()==""  ? true :false  ,
        },
        company_service_more_details_title: {
            required: true,
        },
        company_service_more_details_description: {
            required: true,
        },
        no_of_list: {
            required: $('#company_service_more_details_id').val()==""  ? true :false  ,
        },
    },
    messages: {
        company_service_details_id: {
            required : "Please select service detail",
        },
        company_service_more_details_name: {
            required : "Please enter service more detail tilte",
        },
        company_service_more_details_image: {
            required : "Please select service more detail image",
        },
        company_service_more_details_title: {
            required : "Please select service more detail title",
        },
        company_service_more_details_description: {
            required : "Please enter service more detail description",
        },
        no_of_list: {
            required : "Please enter No of Service More Detail List",
        },
    },
        submitHandler: function(form) {
            
            //$(':input[type="submit"]').prop('disabled', true);
            //$(".ajax-loader").show();
            //form.submit(); 
      }
});
//////////////17-01-2022 (Shubham)/////////////////////////////////////////////



//////////////18-01-2022 (Shubham)/////////////////////////////////////////////

$("#subDepartmentForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        sub_department_name: {
            required: true,
            noSpace:true 
        },
    },
    messages: {
        sub_department_name: {
            required : "Please enter Sub Department Name",
            noSpace: "No space please and don't leave it empty",
        },
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});


$("#faceRequest").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
    decline_reason: {
    required: true,
    noSpace:true
},
face_reuqest_status:{
    required: true,
    noSpace:true
},

  
    },
    messages: {
        decline_reason:{
    required: "Please Enter Reason "
},

face_reuqest_status:{
    required: "Please Select Status"
},
    
   
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            form.submit(); 
      } 
    },

});

   
    function validateCOmmonValueform() {
        $("#addSalaryCommonValueFrom").validate({
    
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass('select2-hidden-accessible')) {
                        error.insertAfter(element.next('span'));  // select2
                        element.next('span').addClass('error').removeClass('valid');
                    } else {
                        error.insertAfter(element);               // default
                    }
                },
                rules: {
                    salary_earning_deduction_id: {
                        required: true,
                    },
                    block_id:{
                        required: true,
                    },
                    "floor_id[]": {
                        required: true,
                    },
                    "multiEarning[][]": {
                        required: true,
                    },
                    /* "amount_value[]": {
                        required: true,
                    }, */
                    amount_type:{
                        required: true,
                    },
                   /*  amount_value:{
                        required: true,
                        noSpace:true
                    }, */
                },
                messages: {
                    salary_earning_deduction_id:{
                        required: "Please select salary earning deduction type",
                    },
                    block_id:{
                        required: "Please Select Branch",
                    },
                    "floor_id[]":{
                        required: "Please Enter Department ",
                    },
                    "multiEarning[]":{
                        required: "Please Select Earnings ",
                    },
                    amount_type:{
                        required: "Please Select amount type",
                    },
                    /* "amount_value[]":{
                        required: "Please Select amount",
                    }, */
            
                    
                },
            
            });
    }
/* $("#addSalaryCommonValueFrom").submit(function(e){
   
    alert("sdfkfl");
    var v = validateCOmmonValueform();
    alert(v);
    alert();
}); */
$("#earningDeductionForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        earning_deduction_name: {
            required: true,
        },
        earning_deduction_type: {
            required: true,
        },
        
    },
    messages: {
        earning_deduction_name: {
            required : "Please Enter Earning Deduction Name",
        },
        earning_deduction_type: {
            required : "Please Select Type",
        },
        
    },
        submitHandler: function(form) {
            
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addLeaveDefaultCountForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        leave_group_name: {
            required: true,
            noSpace:true,
        },
        "number_of_leaves[]": {
            required: true,
            noSpace:true,
        }
        
    },
    messages: {
        leave_group_name: {
            required : "Please Enter Group name",
        },
        "number_of_leaves[]": {
            required : "Please enter no of leave",
            noSpace: "No space please and don't leave it empty",
        }
        
    },
        submitHandler: function(form) {
            var lgId = $('#lgId').val();
            if(lgId > 0){
                $(':input[type="submit"]').prop('disabled', true);
                $(".ajax-loader").show();
                form.submit(); 
            }else{
                if($('.leaveTypeCheck').is(":checked")){
                    var isNumberOfLeaveBlank = false;
                    $('.number_of_leaves').each(function () {
                        if($(this).val() != ''){
                            isNumberOfLeaveBlank = true;
                        }
                    });
                    if(isNumberOfLeaveBlank){
                        $(':input[type="submit"]').prop('disabled', true);
                        $(".ajax-loader").show();
                        form.submit(); 
                    }else{
                        swal("Error! Please enter number of leaves!", {
                            icon: "error",
                        });
                    }
                }else{
                    swal("Error! Please select leave type!", {
                        icon: "error",
                    });
                }
            }
      }
});


$("#leaveAssignUpdate").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        user_total_leave: {
            required: true,
            noSpace:true,
        },  
    },
    messages: {
        user_total_leave: {
            required : "Please enter user total leave",
            noSpace: "No space please and don't leave it empty",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#leaveStatusApproved").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        paid_unpaid: {
            required: true,
        },  
    },
    messages: {
        paid_unpaid: {
            required : "Please select Mode",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#leaveStatusReject").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        leave_admin_reason: {
            required: true,
            noSpace:true,
        },  
    },
    messages: {
        leave_admin_reason: {
            required : "Please enter reason",
            noSpace: "No space please and don't leave it empty",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addLeaveForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true,
        },  
        floor_id: {
            required: true,
        },  
        user_id: {
            required: true,
        }, 
        leave_type_id: {
            required: true,
        }, 
        leave_start_date: {
            required: true,
        }, 
        leave_day_type: {
            required: true,
        }, 
        paid_unpaid: {
            required: true,
        }, 
    },
    messages: {
        block_id: {
            required : "Please select branch",
        },   
        floor_id: {
            required : "Please select department",
        },   
        user_id: {
            required : "Please select employee",
        },  
        leave_type_id: {
            required : "Please select leave type",
        },  
        leave_start_date: {
            required : "Please select leave date",
        },  
        leave_day_type: {
            required : "Please select leave day type",
        },  
        paid_unpaid: {
            required : "Please select leave mode",
        },  
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#editCommonValue").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        amount_value: {
            required: true,
        },
        amount_type: {
            required: true,
        },
                
    },
    messages: {
        amount_value: {
            required: "Please Enter Value",
        },
        amount_type: {
            required: "Plese Select Type",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#changeDepartmentForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true,
        },
        floor_id: {
            required: true,
        },
                
    },
    messages: {
        block_id: {
            required: "Please Select Branch",
        },
        floor_id: {
            required: "Plese Select Department",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#wfhStatusApprovedFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        wfh_attendance_range: {
            required: true,
            number:true
        },
        wfh_take_selfie: {
            required: true,
        },
                
    },
    messages: {
        wfh_attendance_range: {
            required: "Please Enter Range",
            noSpace: "No space please and don't leave it empty",
            digits: true,
        },
        wfh_take_selfie: {
            required: "Please Select WFH Take Selfie",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$(".branchDeptFilterWithUser").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        bId: {
            required: true,
        },
        dId: {
            required: true,
        },
        uId: {
            required: false,
        },
                
    },
    messages: {
        bId: {
            required: "Please Select Branch",
        },
        dId: {
            required: "Please Select Department",
        },
        uId: {
            required: "Please Select Employee",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$(".branchDeptFilter").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        bId: {
            required: true,
        },
        dId: {
            required: true,
        },         
    },
    messages: {
        bId: {
            required: "Please Select Branch",
        },
        dId: {
            required: "Please Select Department",
        },
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#birthdayNotificationForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        title: {
            required: true,
            noSpace: true,
        },
        description: {
            required: true,
            noSpace: true,
        },         
    },
    messages: {
        title: {
            required: "Please Enter title",
            noSpace: "No space please and don't leave it empty",
        },
        description: {
            required: "Please Enter description",
            noSpace: "No space please and don't leave it empty",
        },
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addTaskFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        task_name: {
            required: true,
            noSpace: true,
        },
        task_due_date: {
            required: true,
        },         
        task_note: {
            noSpace: true,
            maxlength: 1500,
        },         
        block_id: {
            required: true,
        },
        floor_id: {
            required: true,
        },
        task_assign_to: {
            required: true,
        },
        no_of_task_step: {
            digits: true,
        },
    },
    messages: {
        task_name: {
            required: "Please Enter task name",
            noSpace: "No space please and don't leave it empty",
        },
        task_due_date: {
            required: "Please Enter task due date",
        },
        task_note: {
            noSpace: "No space please and don't leave it empty",
        },
        block_id: {
            required: "Please Select Branch",
        },
        floor_id: {
            required: "Please Select Department",
        },
        task_assign_to: {
            required: "Please Select Employee",
        },
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addTaskStepForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        task_step: {
            required: true,
            noSpace: true,
        },
        task_step_due_date: {
            required: true,
        },         
        task_step_note: {
            noSpace: true,
            maxlength: 1000,
        },
    },
    messages: {
        task_step: {
            required: "Please Enter step name",
            noSpace: "No space please and don't leave it empty",
        },
        task_step_due_date: {
            required: "Please Enter step due date",
        },
        task_step_note: {
            noSpace: "No space please and don't leave it empty",
        },
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#taskAssignForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true,
        },
        floor_id: {
            required: true,
        },
        task_assign_to: {
            required: true,
        },
                
    },
    messages: {
        block_id: {
            required: "Please Select Branch",
        },
        floor_id: {
            required: "Please Select Department",
        },
        task_assign_to: {
            required: "Please Select Employee",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
//$('.attendanceDeclineReasonClass').click(function(e) {
$("#attendanceDeclineReasonForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        attendance_declined_reason: {
            required: true,
            noSpace: true,
        },
                
    },
    messages: {
        attendance_declined_reason: {
            required: "Please Enter Reason",
        },
                
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
           // form.submit(); 
      }
});
//});


$("#getLeavesFromPreviousYearForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        from_year: {
            required: true,
        },
        to_year: {
            required: true,
        },
        block_id: {
            required: true,
        },
        floor_id: {
            required: true,
        },
                
    },
    messages: {
        from_year: {
            required: "Please Select from year",
        },
        to_year: {
            required: "Please Select to year",
        },
        block_id: {
            required: "Please Select branch",
        },
        floor_id: {
            required: "Please Select department",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addGroupMemberFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        group_id: {
            required: true,
        }, 
        block_id: {
            required: true,
        },  
        floor_id: {
            required: true,
        },  
        "user_id[]": {
            required: true,
        },
    },
    messages: {
        group_id: {
            required : "Please select group",
        },
        block_id: {
            required : "Please select branch",
        },   
        floor_id: {
            required : "Please select department",
        },   
        "user_id[]": {
            required : "Please select employee",
        },  
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#chatGroupForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        group_name: {
            required: true,
        },
    },
    messages: {
        group_name: {
            required : "Please enter group name",
        },  
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#macAddressRejectForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        mac_address_reject_reason: {
            required: true,
            noSpace:true,
        },  
    },
    messages: {
        mac_address_reject_reason: {
            required : "Please enter reason",
            noSpace: "No space please and don't leave it empty",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});


});
$("#salaryBasicAdd").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true,
            noSpace:true,
        },
        floor_id: {
            required: true,
            noSpace:true,
        },
        user_id01: {
            required: true,
            noSpace:true,
        },
        year: {
            required: true,
            noSpace:true,
        },
        month: {
                required: true,
                noSpace:true,
                },  
            
        },
        messages: {
            mac_address_reject_reason: {
                required : "Please enter reason",
                noSpace: "No space please and don't leave it empty",
            },   
            block_id: {
                required : "Please select Branch ",
                noSpace: "No space please and don't leave it empty",
            },
            floor_id: {
                required : "Please select department ",
                noSpace: "No space please and don't leave it empty",
            },
            user_id01: {
                required : "Please select user",
                noSpace: "No space please and don't leave it empty",
            },
            year: {
                required : "Please select year",
                noSpace: "No space please and don't leave it empty",
            },
            month: {
                required : "Please select month",
                noSpace: "No space please and don't leave it empty",
                    },  
                
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#salarySlip").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true,
            noSpace:true,
        },
        floor_id: {
            required: true,
            noSpace:true,
        },
        user_id01: {
            required: true,
            noSpace:true,
        },
        year: {
            required: true,
            noSpace:true,
        },
        month: {
                required: true,
                noSpace:true,
                },  
            
        },
        messages: {
            mac_address_reject_reason: {
                required : "Please enter reason",
                noSpace: "No space please and don't leave it empty",
            },   
            block_id: {
                required : "Please select Branch ",
                noSpace: "No space please and don't leave it empty",
            },
            floor_id: {
                required : "Please select department ",
                noSpace: "No space please and don't leave it empty",
            },
            user_id01: {
                required : "Please select user",
                noSpace: "No space please and don't leave it empty",
            },
            year: {
                required : "Please select year",
                noSpace: "No space please and don't leave it empty",
            },
            month: {
                required : "Please select month",
                noSpace: "No space please and don't leave it empty",
                    },  
                
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#updateAutoLeaveForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        leave_type_id: {
            required: true,
        },  
        paid_unpaid: {
            required: true,
        },  
    },
    messages: {
        leave_type_id: {
            required : "Please select leave type",
        },   
        paid_unpaid: {
            required : "Please select leave mode",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});


$(".workfolioFilterForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        tId: {
            required: true,
        },  
        eId: {
            required: true,
        },  
    },
    messages: {
        tId: {
            required : "Please select Team",
        },   
        eId: {
            required : "Please select Team Employee",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addTaskPriorityFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        task_priority_name: {
            required: true,
            noSpace:true,
        },   
    },
    messages: {
        task_priority_name: {
            required : "Please Enter task priority",
            noSpace: "No space please and don't leave it empty",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#editTaskPriorityFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        task_priority_name: {
            required: true,
            noSpace:true,
        },   
    },
    messages: {
        task_priority_name: {
            required : "Please Enter task priority",
            noSpace: "No space please and don't leave it empty",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#assetsSoldOutForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        sold_out_price: {
            required: true,
            noSpace:true,
        },   
    },
    messages: {
        sold_out_price: {
            required : "Please Enter price",
            noSpace: "No space please and don't leave it empty",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});


$("#wfhStatusRejectForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        wfh_declined_reason: {
            required: true,
            noSpace:true,
        },   
    },
    messages: {
        wfh_declined_reason: {
            required : "Please Enter Reason",
            noSpace: "No space please and don't leave it empty",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#addVndCateogry").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        service_provider_category_name: {
            required: true,
            noSpace:true,
        },   
        service_provider_category_image: {
            required: true,
            noSpace:true,
        },   
    },
    messages: {
        service_provider_category_name: {
            required : "Please Enter Category Name",
            noSpace: "No space please and don't leave it empty",
        },   
        service_provider_category_image: {
            required : "Please Select Image",
            noSpace: "No space please and don't leave it empty",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#expenesStatusReject").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        expense_reject_reason: {
            required: true,
            noSpace:true,
        },  
    },
    messages: {
        expense_reject_reason: {
            required : "Please enter reason",
            noSpace: "No space please and don't leave it empty",
        },   
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addTaskAccessForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        user_id: {
            required: true,
        }, 
        task_access_type: {
            required: true,
        },  
        "task_access_ids[]": {
            required: true,
        },  
    },
    messages: {
        user_id: {
            required : "Please Select Employee",
        },   
        task_access_type: {
            required : "Please Select Access Type",
        }, 
        "task_access_ids[]": {
            required : "Please Select Task Access",
        },  
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#hrDocSubCategoryForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        hr_document_category_id: {
            required: true,
        }, 
        hr_document_sub_category_name: {
            required: true,
            noSpace:true,
        },  
    },
    messages: {
        hr_document_category_id: {
            required : "Please Select Category",
        },   
        hr_document_sub_category_name: {
            required : "Please Enter Sub Category Name",
            noSpace: "No space please and don't leave it empty",
        }, 
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$(".productFltr").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        cId: {
            required: true,
        }, 
        vId: {
            required: true,
            noSpace:true,
        },  
        /* scId: {
            required: true,
            noSpace:true,
        }, */  
    },
    messages: {
        cId: {
            required : "Please Select Category",
        },   
       /*  scId: {
            required : "Please Select Sub Category",
        },  */  
        vId: {
            required : "Please Select Vendor",
            noSpace: "No space please and don't leave it empty",
        }, 
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addGoogleDriveSettingForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        google_client_id: {
            required: true,
            noSpace:true,
        }, 
        google_client_secret_key: {
            required: true,
            noSpace:true,
        },  
    },
    messages: {
        google_client_id: {
            required : "Please Enter Google Client ID",
            noSpace: "No space please and don't leave it empty",
        },   
        google_client_secret_key: {
            required : "Please Enter Google Client Secret Key",
            noSpace: "No space please and don't leave it empty",
        }, 
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#bulkModalForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        salary_mode: {
            required: true,
        }, 
        status_action: {
            required: true,
        }, 
        
       
    },
    messages: {
        salary_mode: {
            required : "Please Select salary mode",
        },   
        status_action: {
            required : "Please Select Status Action",
        },   
       
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#updateUserShift").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        apply_from: {
            required: true,
        }, 
        shift_id: {
            required: true,
        }, 
       
    },
    messages: {
        apply_from: {
            required : "Please Select From When You Want To apply",
        },   
        shift_id: {
            required : "Please Select Shift",
        },   
           
       
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#personal-info2").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        local_service_provider_id: {
            required: true,
        }, 
        service_provider_sub_category_name: {
            required: true,
        }, 
        service_provider_sub_category_image: {
            required: $('#local_service_provider_sub_id').val() =="" ? true : false,
        }, 
       
    },
    messages: {
        local_service_provider_id: {
            required : "Please Select Category",
        },   
        service_provider_sub_category_name: {
            required : "Please Select Subcategory",
        },   
        service_provider_sub_category_image: {
            required : "Please Select Image",
        },   
           
       
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#complaintsEmailRcFrm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        email_id: {
            required: true,
            customemail:true,
        }, 
       
       
    },
    messages: {
        email_id: {
            required : "Please Enter Email",
            customemail : "Please Enter Valid Email",
        },   
       
           
       
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$(".fromToDateFilter").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        from: {
            required: true,
        },
        toDate: {
            required: true,
        },         
    },
    messages: {
        from: {
            required: "Please Select From Date",
        },
        toDate: {
            required: "Please Select To Date",
        },
    },
    submitHandler: function(form) {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit(); 
    }
});

$("#eventReportFilter").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        eId: {
            required: true,
        },        
    },
    messages: {
        eId: {
            required: "Please Select Event",
        },
    },
    submitHandler: function(form) {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit(); 
    }
});

$("#userAdd").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        user_name: {
            required: true,
            noSpace:true,
        },  
        user_mobile: {
            required: true,
            noSpace:true,
            maxlength:10,
            minlength:10
        }, 
        user_password: {
            required: true,
            noSpace:true,
        }, 
        branch_id: {
            required: true,
            noSpace:true,
        }, 
    },
    messages: {
        user_name: {
            required:"Please Enter Name",
        },  
        user_mobile: {
            required: "Please Enter Mobile Number",
        }, 
        user_password: {
            required: "Please Enter Password",
        }, 
        branch_id: {
            required: "Plese Select Branch",
        },    
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#courseLessonTYpeAdd").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        lesson_type: {
            required: true,
            noSpace:true,
        },  
        
    },
    messages: {
        lesson_type: {
            required:"Please Enter Lesson type",
        },  
          
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#assignMultiDptBrnch").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        floor_id: {
            required: true,
        },  
        br_id: {
            required: true,
        },  
        user_id: {
            required: true,
        },  
        emp_multi_branch: {
            required: true,
        },  

    },
    messages: {
        floor_id: {
            required:"Please Select Department",
        },  
        br_id: {
            required:"Please Select Branch",
        },  
        user_id: {
            required:"Please Select Employee",
        },  

        emp_multi_branch: {
            required:"Please Select Branch For assign",
        },  
          
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#addproductVendorCategoryForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        vendor_id: {
            required: true,
        },  
          

    },
    messages: {
        vendor_id: {
            required:"Please Select Vendor",
        },  
         
          
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});


$("#addAssetsMaintenanceForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        assets_category_id: {
            required: true,
        },  
        assets_id: {
            required: true,
        },    
        maintenance_type: {
            required: true,
        },  
        custom_date: {
            required: true,
        },
        "week_days[]": {
            required: true,
        },
        "month_days[]": {
            required: true,
        },
        start_month: {
            required: true,
        },
        month_date: {
            required: true,
        },
        vendor_name: {
            required: true,
            noSpace:true,
        },
        vendor_mobile_no: {
            required: true,
            noSpace:true,
            minlength: 10,
            maxlength:10,
        },
    },
    messages: {
        assets_category_id: {
            required:"Please Select Assets Category",
        },  
        assets_id: {
            required:"Please Select Assets Item",
        },  
        maintenance_type: {
            required:"Please Select Maintenance Type",
        },  
        custom_date: {
            required:"Please Select Date",
        }, 
        "week_days[]": {
            required:"Please Select Week Days",
        },
        "month_days[]": {
            required:"Please Select Month Days",
        },
        start_month: {
            required:"Please Select Start Month",
        },
        month_date: {
            required:"Please Select Month Date",
        },
        vendor_name: {
            required:"Please Enter Vendor Name",
            noSpace: "No space please and don't leave it empty",
        },
        vendor_mobile_no: {
            required:"Please Enter Vendor Mobile No",
            noSpace: "No space please and don't leave it empty",
            minlength: "Please enter valid mobile number",
            maxlength: "Please enter valid mobile number",
        },
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#completeAssetsMaintenanceForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        maintenance_amount: {
            required: true,
            noSpace:true,
        },
    },
    messages: {
        maintenance_amount: {
            required:"Please Enter Amount",
            noSpace: "No space please and don't leave it empty",
        },
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#rescheduleEventForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        event_start_time: {
            required: true,
        },
        event_time: {
            required: true,
        },
    },
    messages: {
        event_start_time: {
            required:"Please Select Event Date",
        },
        event_time: {
            required:"Please Select Event Time",
        },
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#addProductVerientFrm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        product_variant_name: {
            required: true,
        },
       
    },
    messages: {
        product_variant_name: {
            required:"Please Enter Varient Name",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#addAdvanceSalary").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true,
        },
        floor_id: {
            required: true,
        },
        user_id: {
            required: true,
        },
        advance_salary_amount: {
            required: true,
        },
        advance_salary_mode: {
            required: true,
        },
        advance_salary_date: {
            required: true,
        },
    },
    messages: {
        block_id: {
            required:"Please select Branch",
        },
        floor_id: {
            required:"Please select Department",
        },
        user_id: {
            required:"Please select Employee",
        },
        advance_salary_amount: {
            required:"Please Enter Advance Salary Amount",
        },
        advance_salary_date: {
            required:"Please Enter Advance Salary Amount",
        },
        advance_salary_mode: {
            required:"Please Select Advance Salary Mode",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#trackUser").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true,
        },
        floor_id: {
            required: true,
        },
        user_id: {
            required: true,
        },
        track_user_time: {
            required: true,
        },
       
    },
    messages: {
        block_id: {
            required:"Please select Branch",
        },
        floor_id: {
            required:"Please select Department",
        },
        user_id: {
            required:"Please select Employee",
        },
        track_user_time: {
            required:"Please Enter Tracking Duration",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});
$("#updateTrackUser").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        
        track_user_time: {
            required: true,
        },
       
    },
    messages: {
       
        track_user_time: {
            required:"Please Enter Tracking Duration",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#routeForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        
        startLocation: {
            required: true,
        },
       
    },
    messages: {
       
        startLocation: {
            required:"Start location required",
        },
        
    },
        submitHandler: function(form) {
            // form.submit(); 
      }
});

$("#addEmployeeLoanFrm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true,
        },
        floor_id: {
            required: true,
        },
        user_id: {
            required: true,
        },
        loan_amount: {
            required: true,
        },
        loan_emi: {
            required: true,
        },
       
    },
    messages: {
        block_id: {
            required:"Please select Branch",
        },
        floor_id: {
            required:"Please select Department",
        },
        user_id: {
            required:"Please select Employee",
        },
        loan_amount: {
            required:"Please Enter Loan Amount",
        },
        loan_emi: {
            required:"Please Enter Loan Amount",
        },
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addAdvanceSalary").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true,
        },
        floor_id: {
            required: true,
        },
        user_id: {
            required: true,
        },
        advance_salary_amount: {
            required: true,
        },
        advance_salary_date: {
            required: true,
        },
        advance_salary_mode: {
            required: true,
        },
        advance_salary_remark: {
            required: true,
        },
        
       
    },
    messages: {
        block_id: {
            required:"Please select Branch",
        },
        floor_id: {
            required:"Please select Department",
        },
        user_id: {
            required:"Please select Employee",
        },
        advance_salary_amount: {
            required:"Please Enter Amount",
        },
        advance_salary_date: {
            required:"Please select Date",
        },
        advance_salary_mode: {
            required:"Please select Mode",
        },
        advance_salary_remark: {
            required:"Please Enter Remark",
        },
        
    },


});
$("#editFaceApppDevice").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        device_location: {
            required: true,
        },
      
    },
    messages: {
       device_location: {
            required:"Please Enter Device Location",
        },
       
        
    },

});
$("#payEmiForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        emi_paid_mode: {
            required: true,
        },
        received_date: {
            required: true,
        },
      
    },
    messages: {
        emi_paid_mode: {
            required:"Please Select Mode",
        },
        received_date: {
            required:"Please select Received Date",
        },
       
        
    },

});
$("#leavePayout").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        payment_mode: {
            required: true,
        },
        payment_reference_number: {
            required: true,
        },
        payment_remark: {
            required: true,
        },
      
    },
    messages: {
        payment_mode: {
            required:"Please Select Mode",
        },
        payment_reference_number: {
            required:"Please Enter Reference",
        },
        payment_remark: {
            required:"Please Enter Remark",
        },
       
        
    },

});



$("#expenesStatusRejectNew").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        expense_reject_reason_new: {
            required: true,
            noSpace:true,
        },
    },
    messages: {
        expense_reject_reason_new: {
            required : "Please enter reason",
            noSpace: "No space please and don't leave it empty",
        },
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
      }
});

$("#addExpenseCategoryForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        expense_category_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/ExpenseSettingController.php',
                type: "post",
                data:
                {
                    getCategoryName:'getCategoryName',csrf:csrf
                }
            }
        },
        expense_category_type: {
            required: true
        },
        expense_category_unit_name: {
            required: true,
            noSpace:true
        },
        expense_category_unit_price: {
            required: true,
            noSpace:true
        }
    },
    messages: {
        expense_category_name: {
            required : "Please enter category name",
            noSpace: "No space please and don't leave it empty",
            remote: "Category name already added."
        },
        expense_category_type: {
            required : "Please select type"
        },
        expense_category_unit_name: {
            required : "Please enter unit name",
            noSpace: "No space please and don't leave it empty"
        },
        expense_category_unit_price: {
            required : "Please enter price",
            noSpace: "No space please and don't leave it empty"
        }
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
      }
});

$("#editExpenseCategoryForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        edit_expense_category_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/ExpenseSettingController.php',
                type: "post",
                data:
                {
                    getCategoryName:'getCategoryName',csrf:csrf,
                    category_id: function()
                    {
                        return $('#edit_expense_category_id').val();
                    }
                }
            }
        },
        edit_expense_category_type: {
            required: true
        },
        edit_expense_category_unit_name: {
            required: true,
            noSpace:true
        },
        edit_expense_category_unit_price: {
            required: true,
            noSpace:true
        }
    },
    messages: {
        edit_expense_category_name: {
            required : "Please enter category name",
            noSpace: "No space please and don't leave it empty",
            remote: "Category name already added."
        },
        edit_expense_category_type: {
            required : "Please select type"
        },
        edit_expense_category_unit_name: {
            required : "Please enter unit name",
            noSpace: "No space please and don't leave it empty"
        },
        edit_expense_category_unit_price: {
            required : "Please enter price",
            noSpace: "No space please and don't leave it empty"
        }
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
      }
});

/* 07/07/2022 PMS Module */

$("#addDimensionalForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        "dimensional_name[]": {
            required: true,
            noSpace:true
        },
        "dimensional_weightage[]": {
            required: true,
            noSpace:true
        },
    },
    messages: {
        "dimensional_name[]": {
            required:"Please Enter Dimensional Name",
        },
        "dimensional_weightage[]": {
            required:"Please Enter Dimensional Weightage",
        },
    },

    submitHandler: function(form) {
                
        var totalDimensionalWeightage = $("#totalDimensionalWeightage").val();
        if(totalDimensionalWeightage>100 || totalDimensionalWeightage<100){
            $(".totalDimensionalWeightageMsg").html('Total Dimensional Weightage Should Be 100 %');
            swal("Total Dimensional Weightage Should Be 100 % !", {
              icon: "error",
            });
        }else{
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
        }
        
}

});

$("#addAttributesForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        dimensional_id: {
            required: true,
        },
        attribute_name: {
            required: true,
             noSpace:true
        },
        attribute_type: {
            required: true,
        },
    },
    messages: {
        dimensional_id: {
            required:"Please Select dimensional",
        },
        attribute_name: {
            required:"Please Enter attribute Name",
        },
        attribute_type: {
            required:"Please Enter attribute type",
        },
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }

});

/* DAR Module 11/07/2022 */
$("#addTemplateForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        template_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/WorkReportTemplateController.php',
                type: "post",
                data:
                {
                    template_id: function()
                    {
                        return $('#template_id').val();
                    },
                    checkTemplateName:'checkTemplateName',
                    csrf:csrf
                }
            }
        },
    },
    messages: {
        template_name: {
            required:"Please Enter template Name",
            noSpace: "No space please and don't leave it empty",
            remote: "Template name already used"
        },
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }

});

$("#addTemplateQuestionFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else if (element.hasClass('template-option-error-placement')) {
            error.insertAfter(element.parent('div'));
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        template_id: {
            required: true,
        },
        template_question: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/WorkReportTemplateController.php',
                type: "post",
                data:
                {
                    template_id: function()
                    {
                        return $('#template_id').val();
                    },
                    template_question_id: function()
                    {
                        return $('#template_question_id').val();
                    },
                    checkTemplateQuestion:'checkTemplateQuestion',
                    csrf:csrf
                }
            }
        },
        question_type: {
            required: true,
        },
        "question_type_value[]": {
            required: true,
            noSpace:true,
        },
    },
    messages: {
        template_id: {
            required:"Please Select template",
        },
        template_question: {
            required:"Please Enter Template Question",
            noSpace: "No space please and don't leave it empty",
            remote: "Question already added in this template"
        },
        question_type: {
            required:"Please Enter Template Type",
        },
        "question_type_value[]": {
            required:"Please Enter Template Type",
            noSpace: "No space please and don't leave it empty",
        },
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }

});

$("#referStatusReject").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        company_opening_refer_admin_note: {
            required: true,
            noSpace:true,
        },
    },
    messages: {
        company_opening_refer_admin_note: {
            required:"Please Enter Reason",
            noSpace: "No space please and don't leave it empty",
        },
    },

});

$("#updateAttendanceForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        attendance_start_date: {
            required: true
        },
        attendance_end_date: {
            required: true
        },
        punch_in_time: {
            required: true
        },
        edit_punch_out_time: {
            required: true
        },
        late_in: {
            required: true
        },
        early_out: {
            required: true
        },
        modified_remark: {
            required: false,
            noSpace: true
        }
    },
    messages:
    {
        attendance_start_date: {
            required : "Please select attendance start date"
        },
        attendance_end_date: {
            required : "Please select attendance end date"
        },
        punch_in_time: {
            required : "Please select punch in time"
        },
        edit_punch_out_time: {
            required : "Please select punch out time"
        },
        late_in: {
            required : "Please select late in"
        },
        early_out: {
            required : "Please select early out"
        },
        modified_remark: {
            noSpace: "No space please",
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#rejectBirthdateRequestForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        reject_reason: {
            required: true,
            noSpace:true
        }
    },
    messages:
    {
        reject_reason: {
            required : "Please enter reject reason",
            noSpace: "No space please and don't leave it empty",
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#addExpenseForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        block_id: {
            required: true
        },
        floor_id: {
            required: true
        },
        user_id: {
            required: true
        },
        "expense_title[]": {
            required: true,
            noSpace:true
        },
        "date[]": {
            required: true
        },
        "amount[]": {
            required: true,
            noSpace:true
        },
        "unit[]": {
            required: true,
            noSpace:true
        },
        "description[]": {
            required: false,
            noSpace:true
        },
        "expense_document[]": {
            required: false,
            extensionDocumentNew: true,
            filesize2MB: true
        },
        "expense_category_id[]": {
            required: true,
            noSpace:true
        },
        "expense_category_amount[]": {
            required: true,
            noSpace:true
        },
        "expense_category_unit_name[]": {
            required: true,
            noSpace:true
        },
        "expense_category_unit_price[]": {
            required: true,
            noSpace:true
        }
    },
    messages:
    {
        block_id: {
            required : "Please select branch"
        },
        floor_id: {
            required : "Please select department"
        },
        user_id: {
            required : "Please select user"
        },
        "expense_title[]": {
            required : "Please enter expense title",
            noSpace: "No space please and don't leave it empty"
        },
        "date[]": {
            required : "Please select date"
        },
        "amount[]": {
            required : "Please enter amount",
            noSpace: "No space please and don't leave it empty"
        },
        "unit[]": {
            required : "Please enter unit",
            noSpace: "No space please and don't leave it empty"
        },
        "description[]": {
            noSpace: "No space please"
        },
        "expense_category_id[]": {
            required : "Please select type",
            noSpace: "No space please and don't leave it empty"
        },
        "expense_category_amount[]": {
            required : "Please enter amount",
            noSpace: "No space please and don't leave it empty"
        },
        "expense_category_unit_name[]": {
            required : "Please enter unit name",
            noSpace: "No space please and don't leave it empty"
        },
        "expense_category_unit_price[]": {
            required : "Please enter unit price",
            noSpace: "No space please and don't leave it empty"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#salaryIssueStatusChangeForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        reject_reason: {
            required: true,
            noSpace:true
        }
    },
    messages:
    {
        reject_reason: {
            required : "Please enter reason",
            noSpace: "No space please and don't leave it empty"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});


/*25 July 2022*/
$("#assignDimensionalFrom").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        dimensional_id: {
            required: true,
        },
        "block_ids[]": {
            required: true,
        },
        "floor_ids[]": {
            required: true,
        },
        "level_ids[]": {
            required: true,
        },
        "users_ids[]": {
            required: true,
        },
        pms_type: {
            required: true,
        },
        day: {
            required: true,
        },
        start_month: {
            required: true,
        },
    },
    messages:
    {
        dimensional_id: {
            required : "Please select dimensional",
        },
        "block_ids[]": {
            required: "Please select branch",
        },
        "floor_ids[]": {
            required: "Please select department",
        },
        "level_ids[]": {
            required: "Please select level",
        },
        "users_ids[]": {
            required: "Please select employee",
        },
        pms_type: {
            required: "Please select type",
        },
        day: {
            required: "Please select days",
        },
        start_month: {
            required: "Please select start month",
        },
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});


/*02 August 2022*/

$("#addIdProofForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        id_proof_id: {
            required: true,
        },
        "id_proof[]": {
            required: true,
        },
    },
    messages:
    {
        id_proof_id: {
            required : "Please Select Id Proof Type",
        },
        "id_proof[]": {
            required: "Please Select File",
        },
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#holidayGroupAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        holiday_group_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/HolidayController.php',
                type: "post",
                data:
                {
                    getHolidayGroupName:'getHolidayGroupName',csrf:csrf
                }
            }
        }
    },
    messages:
    {
        holiday_group_name: {
            required : "Please enter holiday group name",
            noSpace: "No space please and don't leave it empty",
            remote: "Holiday group name already added"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#holidayGroupEditForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        holiday_group_name_edit: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/HolidayController.php',
                type: "post",
                data:
                {
                    holiday_group_id: function()
                    {
                        return $('#holiday_group_id').val();
                    },
                    getHolidayGroupName:'getHolidayGroupName',
                    csrf:csrf
                }
            }
        }
    },
    messages:
    {
        holiday_group_name_edit: {
            required : "Please enter holiday group name",
            noSpace: "No space please and don't leave it empty",
            remote: "Holiday group name already added"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#assignGroupAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        holiday_group_id: {
            required: true
        },
        "block_id_assign[]": {
            required: true
        }
    },
    messages:
    {
        holiday_group_id: {
            required : "Please select holiday group"
        },
        "block_id_assign[]": {
            required : "Please select branch"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#areaAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        country_id: {
            required: true
        },
        state_city_id: {
            required: true
        },
        area_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/areaController.php',
                type: "post",
                data:
                {
                    checkAreaName:'checkAreaName',
                    csrf:csrf
                }
            }
        },
        pincode: {
            required: false,
            noSpace:true
        }
    },
    messages:
    {
        country_id: {
            required : "Please select country"
        },
        state_city_id: {
            required : "Please select city state"
        },
        area_name: {
            required : "Please enter area name",
            remote : "Area name already used"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#areaUpdateForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        country_id: {
            required: true
        },
        state_city_id: {
            required: true
        },
        area_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/areaController.php',
                type: "post",
                data:
                {
                    area_id: function()
                    {
                        return $('#area_id').val();
                    },
                    checkAreaName:'checkAreaName',
                    csrf:csrf
                }
            }
        },
        pincode: {
            required: false,
            noSpace:true
        }
    },
    messages:
    {
        country_id: {
            required : "Please select country"
        },
        state_city_id: {
            required : "Please select city state"
        },
        area_name: {
            required : "Please enter area name",
            remote : "Area name already used"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#routeAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        country_id: {
            required: true
        },
        state_city_id: {
            required: true
        },
        route_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/routeController.php',
                type: "post",
                data:
                {
                    checkRouteName:'checkRouteName',
                    csrf:csrf
                }
            }
        },
        "retailer_id[]": {
            required: false
        },
        route_description: {
            required: false,
            noSpace:true
        }
    },
    messages:
    {
        country_id: {
            required : "Please select country"
        },
        state_city_id: {
            required : "Please select city state"
        },
        route_name: {
            required : "Please enter route name",
            remote : "Route name already used"
        },
        "retailer_id[]": {
            required : "Please select retailer"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#routeUpdateForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        country_id: {
            required: true
        },
        state_city_id: {
            required: true
        },
        route_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/routeController.php',
                type: "post",
                data:
                {
                    route_id: function()
                    {
                        return $('#route_id').val();
                    },
                    checkRouteName:'checkRouteName',
                    csrf:csrf
                }
            }
        },
        "retailer_id[]": {
            required: false
        },
        route_description: {
            required: false,
            noSpace:true
        }
    },
    messages:
    {
        country_id: {
            required : "Please select country"
        },
        state_city_id: {
            required : "Please select city state"
        },
        route_name: {
            required : "Please enter route name",
            remote : "Route name already used"
        },
        "retailer_id[]": {
            required : "Please select retailer"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#retailerAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        country_id: {
            required: true
        },
        state_city_id: {
            required: true
        },
        area_id: {
            required: true
        },
        retailer_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/retailerController.php',
                type: "post",
                data:
                {
                    retailer_contact_person_country_code: function()
                    {
                        return $('#retailer_contact_person_country_code').val();
                    },
                    retailer_contact_person_number: function()
                    {
                        return $('#retailer_contact_person_number').val();
                    },
                    retailer_id: function()
                    {
                        return $('#retailer_id').val();
                    },
                    checkRetailerName:'checkRetailerName',
                    csrf:csrf
                }
            }
        },
        retailer_code: {
            required: false,
            noSpace:true
        },
        retailer_contact_person: {
            required: true,
            noSpace:true
        },
        retailer_contact_person_country_code: {
            required: true,
            remote:
            {
                url: 'controller/retailerController.php',
                type: "post",
                data:
                {
                    retailer_name: function()
                    {
                        return $('#retailer_name').val();
                    },
                    retailer_contact_person_number: function()
                    {
                        return $('#retailer_contact_person_number').val();
                    },
                    retailer_id: function()
                    {
                        return $('#retailer_id').val();
                    },
                    checkRetailerName:'checkRetailerName',
                    csrf:csrf
                }
            }
        },
        retailer_contact_person_number: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/retailerController.php',
                type: "post",
                data:
                {
                    retailer_name: function()
                    {
                        return $('#retailer_name').val();
                    },
                    retailer_contact_person_country_code: function()
                    {
                        return $('#retailer_contact_person_country_code').val();
                    },
                    retailer_id: function()
                    {
                        return $('#retailer_id').val();
                    },
                    checkRetailerName:'checkRetailerName',
                    csrf:csrf
                }
            }
        },
        retailer_alt_contact_number: {
            required: false,
            noSpace:true
        },
        "distributor_id[]": {
            required: true
        },
        retailer_address: {
            required: true,
            noSpace:true
        },
        retailer_geofence_range: {
            required: true,
            noSpace:true
        },
        retailer_pincode: {
            required: false,
            noSpace:true
        },
        retailer_gst_no: {
            required: false,
            noSpace:true,
            regexp: /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
        },
        retailer_type: {
            required: false,
            noSpace:true
        },
        retailer_credit_limit: {
            required: false,
            noSpace:true
        },
        retailer_credit_days: {
            required: false,
            noSpace:true
        },
        retailer_photo: {
            required: false
        }
    },
    messages:
    {
        country_id: {
            required : "Please select country"
        },
        state_city_id: {
            required : "Please select city-state"
        },
        area_id: {
            required : "Please select area"
        },
        retailer_name: {
            required : "Please enter retailer name",
            remote : "Same name and number already used"
        },
        retailer_contact_person: {
            required : "Please enter contact person name"
        },
        retailer_contact_person_country_code: {
            required : "Please select country code",
            remote : "Same name and number already used"
        },
        retailer_contact_person_number: {
            required : "Please enter contact person number",
            remote : "Same name and number already used"
        },
        "distributor_id[]": {
            required : "Please select distributor"
        },
        retailer_address: {
            required : "Please enter address"
        },
        retailer_geofence_range: {
            required : "Please enter geofence range"
        },
        retailer_pincode: {
            required : "Please enter pincode"
        },
        retailer_gst_no: {
            required : "Please enter GST no"
        },
        retailer_type: {
            required : "Please enter type"
        },
        retailer_credit_limit: {
            required : "Please enter credit limit"
        },
        retailer_credit_days: {
            required : "Please enter credit days"
        },
        retailer_photo: {
            required : "Please select photo"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#distributorAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        country_id: {
            required: true
        },
        distributor_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/distributorController.php',
                type: "post",
                data:
                {
                    distributor_contact_person_country_code: function()
                    {
                        return $('#distributor_contact_person_country_code').val();
                    },
                    distributor_contact_person_number: function()
                    {
                        return $('#distributor_contact_person_number').val();
                    },
                    distributor_id: function()
                    {
                        return $('#distributor_id').val();
                    },
                    checkDistributorName:'checkDistributorName',
                    csrf:csrf
                }
            }
        },
        distributor_code: {
            required: false,
            noSpace:true
        },
        distributor_contact_person: {
            required: true,
            noSpace:true
        },
        distributor_contact_person_country_code: {
            required: true,
            remote:
            {
                url: 'controller/distributorController.php',
                type: "post",
                data:
                {
                    distributor_name: function()
                    {
                        return $('#distributor_name').val();
                    },
                    distributor_contact_person_number: function()
                    {
                        return $('#distributor_contact_person_number').val();
                    },
                    distributor_id: function()
                    {
                        return $('#distributor_id').val();
                    },
                    checkDistributorName:'checkDistributorName',
                    csrf:csrf
                }
            }
        },
        distributor_contact_person_number: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/distributorController.php',
                type: "post",
                data:
                {
                    distributor_name: function()
                    {
                        return $('#distributor_name').val();
                    },
                    distributor_contact_person_country_code: function()
                    {
                        return $('#distributor_contact_person_country_code').val();
                    },
                    distributor_id: function()
                    {
                        return $('#distributor_id').val();
                    },
                    checkDistributorName:'checkDistributorName',
                    csrf:csrf
                }
            }
        },
        distributor_alt_contact_number: {
            required: false,
            noSpace:true
        },
        distributor_email: {
            required: false,
            noSpace:true
        },
        distributor_address: {
            required: false,
            noSpace:true
        },
        distributor_pincode: {
            required: false,
            noSpace:true
        },
        distributor_gst_no: {
            required: false,
            noSpace:true,
            regexp: /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
        },
        distributor_photo: {
            required: false
        }
    },
    messages:
    {
        country_id: {
            required : "Please select country"
        },
        distributor_name: {
            required : "Please enter distributor name",
            remote : "Same name and number already used"
        },
        distributor_contact_person: {
            required : "Please enter contact person name"
        },
        distributor_contact_person_country_code: {
            required : "Please select country code",
            remote : "Same name and number already used"
        },
        distributor_contact_person_number: {
            required : "Please enter contact person number",
            remote : "Same name and number already used"
        },
        distributor_address: {
            required : "Please enter address"
        },
        distributor_pincode: {
            required : "Please enter pincode"
        },
        distributor_gst_no: {
            required : "Please enter GST no"
        },
        distributor_photo: {
            required : "Please select photo"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#employeeRouteAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        route_id: {
            required: true
        },
        user_id: {
            required: true
        },
        "route_assign_week_days[]": {
            required: true
        }
    },
    messages:
    {
        route_id: {
            required : "Please select route"
        },
        user_id: {
            required : "Please select user"
        },
        "route_assign_week_days[]": {
            required : "Please select week days"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#categoryAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        category_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/categorySubCategoryController.php',
                type: "post",
                data:
                {
                    checkCategoryName:'checkCategoryName',
                    csrf:csrf
                }
            }
        },
        category_description: {
            required: false,
            noSpace:true
        }
    },
    messages:
    {
        category_name: {
            required : "Please enter category name",
            remote : "Category name already used"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#categoryUpdateForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        category_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/categorySubCategoryController.php',
                type: "post",
                data:
                {
                    product_category_id: function()
                    {
                        return $('#product_category_id').val();
                    },
                    checkCategoryName:'checkCategoryName',
                    csrf:csrf
                }
            }
        },
        category_description: {
            required: false,
            noSpace:true
        }
    },
    messages:
    {
        category_name: {
            required : "Please enter category name",
            remote : "Category name already used"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#subCategoryAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        product_category_id: {
            required: true
        },
        sub_category_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/categorySubCategoryController.php',
                type: "post",
                data:
                {
                    checkSubCategoryName:'checkSubCategoryName',
                    csrf:csrf
                }
            }
        },
        sub_category_description: {
            required: false,
            noSpace:true
        }
    },
    messages:
    {
        product_category_id: {
            required : "Please select category"
        },
        sub_category_name: {
            required : "Please enter sub category name",
            remote : "Sub category name already used"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#subCategoryUpdateForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        product_category_id: {
            required: true
        },
        sub_category_name: {
            required: true,
            noSpace:true,
            remote:
            {
                url: 'controller/categorySubCategoryController.php',
                type: "post",
                data:
                {
                    product_sub_category_id: function()
                    {
                        return $('#product_sub_category_id').val();
                    },
                    checkSubCategoryName:'checkSubCategoryName',
                    csrf:csrf
                }
            }
        },
        sub_category_description: {
            required: false,
            noSpace:true
        }
    },
    messages:
    {
        product_category_id: {
            required : "Please select category"
        },
        sub_category_name: {
            required : "Please enter sub category name",
            remote : "Sub category name already used"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#productAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        product_category_id: {
            required: true
        },
        product_sub_category_id: {
            required: false
        },
        product_name: {
            required: true,
            noSpace:true
        },
        product_description: {
            required: false,
            noSpace:true
        }
    },
    messages:
    {
        product_category_id: {
            required : "Please select category"
        },
        product_sub_category_id: {
            required : "Please select sub category"
        },
        product_name: {
            required : "Please enter name"
        },
        product_description: {
            required : "Please enter description"
        },
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#productVariantAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        product_id: {
            required: true
        },
        product_variant_name: {
            required: true,
            noSpace:true
        },
        variant_bulk_type: {
            required: true
        },
        variant_per_box_piece: {
            required: true,
            noSpace:true
        },
        retailer_selling_price: {
            required: true,
            noSpace:true
        },
        maximum_retail_price: {
            required: false,
            noSpace:true
        },
        menufacturing_cost: {
            required: false,
            noSpace:true
        },
        variant_photo: {
            required: false
        },
        sku: {
            required: false,
            noSpace:true
        },
        weight_in_kg: {
            required: false,
            noSpace:true
        },
        variant_description: {
            required: false,
            noSpace:true
        },
        unit_measurement_id: {
            required: true
        }
    },
    messages:
    {
        product_id: {
            required : "Please select product"
        },
        product_variant_name: {
            required : "Please enter name"
        },
        variant_bulk_type: {
            required : "Please select bulk type"
        },
        variant_per_box_piece: {
            required : "Please enter per box piece"
        },
        retailer_selling_price: {
            required : "Please enter selling price"
        },
        maximum_retail_price: {
            required : "Please enter MRP"
        },
        menufacturing_cost: {
            required : "Please enter manufacturing cost"
        },
        variant_photo: {
            required : "Please select image"
        },
        variant_description: {
            required : "Please enter description"
        },
        unit_measurement_id: {
            required : "Please select unit"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#changeOrderStatusForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules:
    {
        order_status: {
            required: true
        }
    },
    messages:
    {
        order_status: {
            required : "Please select status"
        }
    },
    submitHandler: function(form)
    {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
    }
});

$("#addAttendanceFormNew").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        punch_in_time: {
            required: true
        },
        punch_out_time: {
            required: true
        },
        punch_out_date: {
            required: true
        },
        attendance_type: {
            required: true
        }
    },
    messages: {
        punch_in_time: {
            required : "Please select punch in time"
        },
        punch_out_time: {
            required : "Please select punch out time"
        },
        punch_out_date: {
            required : "Please select punch out date"
        },
        attendance_type: {
            required : "Please select attendance type"
        }
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
      }
});

$("#letterTemplateAddForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        letter_id: {
            required: true
        },
        letter_text: {
            required: true
        }
    },
    messages: {
        letter_id: {
            required : "Please select letter"
        },
        letter_text: {
            required : "Please enter letter text"
        }
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
      }
});

$("#generateDocumentsFilterForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        bId: {
            required: true
        }
    },
    messages: {
        bId: {
            required : "Please select branch"
        }
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
      }
});

$("#groupExportForm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {
                error.insertAfter(element);               // default
            }
        },
        rules: {
            salary_group_id: {
                required: true
            }
        },
        messages: {
            salary_group_id: {
                required : "Please select salary group"
            }
        },
        submitHandler: function(form) {
            $(".ajax-loader").show();
            form.submit();
            $(".ajax-loader").hide();
        }
    });


/* 28 Nov 2022 */
$("#giveExpenseForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        block_id: {
            required: true
        },
        floor_id: {
            required: true
        },
        user_id: {
            required: true
        },
        expense_title: {
            required: true,
            noSpace: true,
        },
        description: {
            required: true,
            noSpace: true,
        },
        amount: {
            required: true,
            noSpace: true,
        },
        date: {
            required: true,
        }
    },
    messages: {
        block_id: {
            required : "Please select branch"
        },
        floor_id: {
            required : "Please select department"
        },
        user_id: {
            required : "Please select employee"
        },
        expense_title: {
            required : "Please select title",
        },
        description: {
            required : "Please select description",
        },
        amount: {
            required : "Please select amount",
        },
        date: {
            required : "Please select date",
        }
    },
    submitHandler: function(form) {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#letterSettingFormAdd").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        header_image: {
            required: true,
            image: true,
            filesize2MB: true
        },
        footer_image: {
            required: true,
            image: true,
            filesize2MB: true
        }
    },
    messages: {
        header_image: {
            required : "Please select header image"
        },
        footer_image: {
            required : "Please select footer image"
        }
    },
    submitHandler: function(form) {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#letterSettingFormUpdate").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        header_image: {
            required: false,
            image: true,
            filesize2MB: true
        },
        footer_image: {
            required: false,
            image: true,
            filesize2MB: true
        }
    },
    messages: {
        header_image: {
            required : "Please select header image"
        },
        footer_image: {
            required : "Please select footer image"
        }
    },
    submitHandler: function(form) {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#updateAttendancePunchOutMissingRequest").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {     
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {                                      
            error.insertAfter(element);               // default
        }
    },
    rules: {
        punch_out_missing_reject_reason: {
            required: true,
            noSpace:true,
        },
        
        
    },
    messages: {
        punch_out_missing_reject_reason: {
            required : "Please enter reject reason ",
            noSpace : "Can not leave empty space",
        },
        
        
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit(); 
      }
});

$("#addShiftForm").validate({
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        shift_name: {
            required: true,
            noSpace:true
        },
        "week_off_days[]": {
            required: false
        },
        has_altenate_week_off: {
            required: true
        },
        'alternate_week_off[]': {
            required: true
        },
        'alternate_weekoff_days[]': {
            required: true
        },
        maximum_in_out: {
            required: false
        },
        late_in_reason: {
            required: false
        },
        early_out_reason: {
            required: false
        },
        is_multiple_punch_in: {
            required: false
        },
        take_out_of_range_reason: {
            required: false
        }
    },
    messages: {
        shift_name: {
            required : "Please enter shift name"
        },
        has_altenate_week_off: {
            required : "Please select alternate week off"
        },
        'alternate_week_off[]': {
            required : "Please select alternate weeks"
        },
        'alternate_weekoff_days[]': {
            required : "Please select alternate week off days"
        }
    },
    submitHandler: function(form) {
        $(':input[type="submit"]').prop('disabled', true);
        $(".ajax-loader").show();
        form.submit();
    }
});

$("#sisterCompanyAddForm").validate({
    errorPlacement: function (error, element) {
       if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());      // radio/checkbox?
        } else if (element.hasClass('select2-hidden-accessible')) {
            error.insertAfter(element.next('span'));  // select2
            element.next('span').addClass('error').removeClass('valid');
        } else {
            error.insertAfter(element);               // default
        }
    },
    rules: {
        sister_company_name: {
            required: true,
            noSpace:true
        },
        sister_company_address: {
            required: true,
            noSpace:true
        },
        sister_company_pincode: {
            required: true,
            noSpace:true
        },
        sister_company_phone: {
            required: true,
            noSpace:true
        },
        sister_company_email: {
            required: true,
            noSpace:true
        },
        sister_company_website: {
            required: false,
            noSpace:true
        },
        sister_company_gst_no: {
            required: false,
            noSpace:true,
            regexp: /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
        },
        sister_company_pan: {
            required: false,
            noSpace:true,
            regexp: /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/
        }
    },
    messages: {
        sister_company_name: {
            required : "Please enter sister company name"
        },
        sister_company_address: {
            required : "Please enter address"
        },
        sister_company_pincode: {
            required : "Please enter pincode"
        },
        sister_company_phone: {
            required : "Please enter contact number"
        },
        sister_company_email: {
            required : "Please enter company email"
        },
        sister_company_pan: {
            regexp : "Please enter valid pan no"
        }
    },
        submitHandler: function(form) {
            $(':input[type="submit"]').prop('disabled', true);
            $(".ajax-loader").show();
            form.submit();
      }
});