$(document).ready(function() {
  $('select').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});

$('.datepicker-simple,.datepicker-simple-end').on('change', function() {  // when the value changes
    $(this).valid(); // trigger validation on this element
});

$(".single-select").select2({
    placeholder: "--Select--"
});

$(".single-salary-group").select2({
    placeholder: "--Select Salary Group--"
});


$(".max_shift_hour").select2({
    placeholder: "Select Max Shift Hours"
});

$('.single-select-new').select2({
   minimumInputLength: 3
 });

$('.multiple-select').select2({
  placeholder: "-- Select-- "
});

$('.multiple-select-sal_common').select2({
  placeholder: "Calculate From Gross Salary"
});
$('.multiple-select-sal_common-deduct').select2({
  placeholder: "Deduct From Total Earning "
});

$('.multiple-select-salary-common-value').select2({
  placeholder: " All "
});

$('.multiple-select-notice').select2({
  placeholder: "Select "
});


$('.multiple-select-unit_notice').select2({
  placeholder: "All "
});


$('.multiple-select-block').select2({
  placeholder: " All "
});

$('.multiple-select1').select2({
  placeholder: "-- Select-- "
});

$('.multiple-select2').select2({
  placeholder: "-- Select-- "
});

$('.multiple-select3').select2({
  placeholder: "-- Select-- "
});

$('.multiple-select-keyword').select2({
  placeholder: " Type Keyword ",
  tags: true
});

$('.multiple-select-map').select2({
  placeholder: " Type Midale City name ",
  tags: true
});

$('.select-map-from').select2({
  placeholder: "From City Name",
  tags: true
});

$('.select-map-to').select2({
  placeholder: "To City Name",
  tags: true
});

$('.complain-select').select2({
  placeholder: "--Select Category--",
});

//multiselect start
$('#my_multi_select1').multiSelect();


$('#my_multi_select2').multiSelect({
selectableOptgroup: true
});
$('#my_multi_select3').multiSelect({
selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
afterInit: function (ms) {
var that = this,
$selectableSearch = that.$selectableUl.prev(),
$selectionSearch = that.$selectionUl.prev(),
selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';
that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
.on('keydown', function (e) {
if (e.which === 40) {
that.$selectableUl.focus();
return false;
}
});
that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
.on('keydown', function (e) {
if (e.which == 40) {
that.$selectionUl.focus();
return false;
}
});
},
afterSelect: function () {
this.qs1.cache();
this.qs2.cache();
},
afterDeselect: function () {
this.qs1.cache();
this.qs2.cache();
}
});
$('.custom-header').multiSelect({
selectableHeader: "<div class='custom-header'>Selectable items</div>",
selectionHeader: "<div class='custom-header'>Selection items</div>",
selectableFooter: "<div class='custom-header'>Selectable footer</div>",
selectionFooter: "<div class='custom-header'>Selection footer</div>"
});
});

/*
Dollop= 21/oct/2021 
 */
$(".multiDpt").select2({
  maximumSelectionLength: 3
  });
  $(".week_off_days").select2({
    maximumSelectionLength: 6
    });
  
  $(".alternate_weekoff_days").select2({
    maximumSelectionLength: 6
    });
  $(".alternate_week_off").select2({
    maximumSelectionLength: 6
    });
  /*=======================end 21/oct/2021 
  */
  $('.multiEarning').select2({
     
  }).on('change', function (e) {
    checkedValue = this.id;
    selfId = $(this).attr('data-id');
    Mainvalue = $('#' + checkedValue).val();
    if (Mainvalue != "") {
      $.each(Mainvalue, function (index, value) {
        othersValue = $('#checkedDataId_' + value).val();
      
        if (othersValue != null) {
          if (jQuery.inArray(selfId, othersValue) !== -1) {
            Mainvalue = jQuery.grep(Mainvalue, function(v) {
                return v != value;
            });
            $.each(othersValue, function (i, v2) {
              newOthersValue = $('#checkedDataId_' + v2).val();
              if (newOthersValue != null) {
                if (jQuery.inArray(selfId, newOthersValue) !== -1) {
                  Mainvalue = jQuery.grep(Mainvalue, function(v) {
                    return v3 != v2;
                });
                }
              }
            });
          
            $('#' + checkedValue).val(Mainvalue).trigger("change");
            // $('#checkedDataId_' + checkedValue).val(Mainvalue);
            // $('#checkedDataId_' + checkedValue).select2();
          }
        }
      });
    }
    
    console.log(value);
    console.log(selfId);
});
  /*=======================end 21/oct/2021 
  */

