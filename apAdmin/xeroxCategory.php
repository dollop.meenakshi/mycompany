<?php error_reporting(0);
  
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Xerox Type</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <a href="xeroxCategoryBtn" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteXeroxType');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
        </div>
      </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
               
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>
                        <th>Category Name</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody id="showFilterData">
                   <?php 
                    $i=1;
                                           
                      if(isset($dId) && $dId>0) {
                        $departmentFilterQuery = " AND users_master.floor_id='$dId'";
                       }
                     
                      $q=$d->select("xerox_type_category","society_id='$society_id' AND is_delete= 0");
                        $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                        
                      <td class="text-center">
                      <?php $totalShift = $d->count_data_direct("xerox_doc_id","xerox_doc_master","xerox_type_id='$data[xerox_type_id]'");
                            if( $totalShift==0) {
                          ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['xerox_type_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['xerox_type_id']; ?>">                      
                        <?php }?>
                        </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['xerox_type_name']; ?></td>
                     
                      
                      <td>
                      <div class="d-flex align-items-center">
                        <form  method="post" accept-charset="utf-8">
                          <input type="hidden" name="xerox_type_id" value="<?php echo $data['xerox_type_id']; ?>">
                          <input type="hidden" name="edit_xerox_category" value="edit_xerox_category">
                         
                          <button type="button" class="btn btn-sm btn-primary mr-1" onclick="xeroxCategorySetData(<?php echo $data['xerox_type_id']; ?>)" data-toggle="modal" data-target="#addModal" > <i class="fa fa-pencil"></i></button> 
                        </form>

                        
                          <?php if($data['xerox_type_status']=="0"){
                          ?>
                          <input type="checkbox" checked class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['xerox_type_id']; ?>','xeroxTypeDeactive');" data-size="small"/>
                          <?php } else { ?>
                          <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['xerox_type_id']; ?>','xeroxTypeActive');" data-size="small"/>
                          <?php } ?>
                          <button type="button" class="btn btn-sm btn-primary ml-1" onclick="xeroxTypeShowDetails(<?php echo $data['xerox_type_id']; ?>)" data-toggle="modal" data-target="#xeroxCategoryModel" ><i class="fa fa-eye"></i></button> 
                        </div>
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
<script type="text/javascript">
  function vieComplain(complain_id) {
      $.ajax({
          url: "getComplaineDetails.php",
          cache: false,
          type: "POST",
          data: {complain_id : complain_id},
          success: function(response){
              $('#comResp').html(response);
            
              
          }
       });
  } 
</script>




<div class="modal fade" id="xeroxCategoryModel">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Paper Size Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="xeroxCategoryModelDiv" style="align-content: center;">

      </div>
      
    </div>
  </div>
</div>
<div class="modal fade" id="addModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Xerox </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
      <div class="card-body">  
           <form id="addXeroxCategoryForm" action="controller/XeroxCategoryCont.php" enctype="multipart/form-data" method="post">
           
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">Xerox Type <span class="required">*</span></label>
               <div class="col-lg-8 col-md-8" id="">
               <input type="text"  required="" name="xerox_type_name" id="xerox_type_name" class="form-control">
               </div>                   
           </div> 
           <div class="form-group row">
               <label for="input-10" class="col-sm-4 col-form-label">Description</label>
               <div class="col-lg-8 col-md-8" id="">
               <textarea  name="xerox_type_desc" id="xerox_type_desc" class="form-control"></textarea>
               </div>                   
           </div> 
                             
           <div class="form-footer text-center">
            
             <input type="hidden" id="xerox_type_id" name="xerox_type_id" value="" >
             <button id="addXeroxCategoryBtn" name="addXeroxCategoryBtn" type="submit"  class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
            <input type="hidden" name="addXeroxCategory" value="addXeroxCategory">
             
             <button id="addCategoryBtn" type="submit"  class="btn btn-success sbmitbtn hideAdd"><i class="fa fa-check-square-o"></i> Add</button>
           
             <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addXeroxCategoryForm');"><i class="fa fa-check-square-o"></i> Reset</button>
            
           </div>

         </form>
      
       </div>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
.hideupdate{
  display:none;
}

</style>