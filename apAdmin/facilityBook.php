<?php
extract($_REQUEST);
error_reporting(0);
$facility_id = $id ;
if (filter_var($_GET['id'], FILTER_VALIDATE_INT) != true || (isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) != true)) {
	$_SESSION['msg1']="Invalid  Request";
	echo ("<script LANGUAGE='JavaScript'>
		window.location.href='bookFacility';
		</script>");
	exit();
}
//IS_1219

$q=$d->select("facilities_master","society_id='$society_id' AND facility_id='$facility_id'","");
$row=mysqli_fetch_array($q);
extract($row);
 if ($amount_type==0) {
  $personText = " / Per Person";
}
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumb-->
		<div class="row pt-2 pb-2 ">
			<div class="col-sm-9 col-6">
				<h4 class="page-title">Book Facilities</h4>
				
			</div>
			<div class="col-sm-3 text-right  col-6">
				 <a href="facilityBookingReport?facility_id=<?php echo $_GET['id'];?>" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> View Report</a>	
			</div>
		</div>
		<!-- End Breadcrumb-->
<?php //IS_1026
if(mysqli_num_rows($q) <= 0  ){ ?>
	<div class="row">
		<div class="col-lg-12">

			<img src='img/no_data_found.png'>
		</div>
	</div>
<?php } else {  ?>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title text-primary"><?php echo $facility_name.' (' ;  if ($facility_type==2) {
			                    echo " Free";
			                  } else  if ($facility_type==0)  {
			                    echo " Per Day".$personText;
			                  } else  if ($facility_type==1)  {
			                    echo " Per Month".$personText;
			                  } else  if ($facility_type==3)  {
			                    echo " Per Hour".$personText;
			                  }else  if ($facility_type==4)  {
			                    echo " Per Time Slot".$personText;
			                  } ?>) </h5>
					<hr>
					<div class="form-group row">
						<div class="col-sm-8">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Price </th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?php echo $currency; ?> <?php
					
									 if ($facility_amount>0) { echo number_format($facility_amount,2); } else {
										echo " Not Availble ";
										} 
					                  ?>
					                </td>
					                
								</tr>
							</tbody>
						</table>
							
						<?php
						if ($facility_type == 1 ) {
							$todayDate=$chk;
							$count5=$d->sum_data("no_of_person","facilitybooking_master","facility_id='$facility_id' AND booking_expire_date>='$todayDate' and book_status!=3 ");
							$row11=mysqli_fetch_array($count5);
							$person_count=$row11['SUM(no_of_person)'];
							?>
						<label for="input-14" class="col-form-label">Maximum Capacity : <?php echo  $person_limit; ?></label>
						<?php }  else {?>
							<label for="input-14" class="col-form-label">Maximum Capacity : <?php echo $person_limit ?></label>
						<?php } ?>
						<div class="ml-2 mr-2">
						<form id="bookFacilityFrm" action="controller/facilitiesController.php" method="POST" enctype="multipart/form-data">
							<input type="hidden" id="facility_amount" name="facility_amount" value="<?php echo $facility_amount; ?> ">
							<input type="hidden" id="amount_type" name="amount_type" value="<?php echo $amount_type; ?> ">
							<input type="hidden" id="facility_amount_tenant" name="facility_amount_tenant" value="<?php echo $facility_amount_tenant; ?> ">
							<input type="hidden" id="user_type" name="user_type" value="<?php echo $user_type; ?> ">
							<input type="hidden" id="facility_name" name="facility_name" value="<?php echo $facility_name; ?> ">
							<input type="hidden" name="balancesheet_id" value="<?php echo $balancesheet_id; ?> ">
							<input type="hidden" id="facility_type" name="facility_type" value="<?php echo $facility_type; ?> ">
							<input type="hidden" id="facility_id" name="facility_id" value="<?php echo $id; ?> ">
							
							<div class="form-group row">
								<label for="unit_id_main" class="col-sm-4 col-form-label">Booking for <span class="text-danger">*</span> </label>
								<div class="col-sm-8">
									<select type="text" onchange="ResetFormBooking()" required="" class="form-control single-select" name="user_id" id="user_id">
										<option value="">-- Select --</option>
										<?php 
										$q3=$d->select("unit_master,block_master,users_master","users_master.delete_status=0 AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.user_status!=0 $blockAppendQueryUser","ORDER BY block_master.block_sort ASC");
										while ($blockRow=mysqli_fetch_array($q3)) {
											?>
											<option value="<?php echo $blockRow['user_id'];?>"><?php echo $blockRow['user_full_name'];?>-<?php echo $blockRow['user_designation'];?> (<?php echo $blockRow['block_name'];?>)</option>
										<?php }?>
									</select>
									
								</div>
								
							</div>
							<?php if ($facility_type==3 || $facility_type==4 || $facility_type==0) {
								// hourly booking (3) & // Day Wise Booking (0)
							 ?>
							<div class="form-group row">
								<label for="default-datepicker11" class=" col-sm-4 col-form-label"> No of Persons <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select  onchange="ResetFormBooking()" class="form-control noOfpersonDiv" id="no_of_person" name="no_of_person" required="">
										<option value="">-- Select --</option>
										<?php for ($i=1; $i <= $person_limit ; $i++) { ?> 
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									</select>
									
								</div>
							</div>
							<div class="form-group row" id="hourly">
								<label for="default-datepicker11" class=" col-sm-4 col-form-label"> Booking Date <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input readonly="" type="text" required="" autocomplete="off" placeholder="Date" name="booked_date" class="form-control facility_datepicker" >
								</div>
							</div>

							<?php }  if ($facility_type==1) { 
							// month wise booking
							?>
							<div class="form-group row">
								<label for="default-datepicker11" class=" col-sm-4 col-form-label"> No of Persons <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<select onchange="resetForm();" class="form-control" id="no_of_person" name="no_of_person" required="">
										<option value="">-- Select --</option>
										<?php for ($i=1; $i <= $person_limit ; $i++) { ?> 
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group row" id="monthly">
								<label for="no_of_month" class=" col-sm-4 col-form-label">  Booking Start Date  <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input readonly="" type="text" required="" autocomplete="off" placeholder="Date" name="booked_date" class="form-control facility_datepicker" >
								</div>
								
							</div>
							
						<?php } ?>
							<div class="form-group row" id="facilityTimeSlotCheck">
								
								<label for="no_of_month" class=" col-sm-4 col-form-label">Booking Time Slot<span class="text-danger">*</span></label>
								<div class="col-sm-8">
									
								</div>
								
							</div>
							<?php if ($facility_type==1) {  ?>

							<div class="form-group row" id="">
								<label for="no_of_month" class=" col-sm-4 col-form-label"> Booking End Month  <span class="text-danger">*</span></label>
								<div class="col-sm-8 mothListDiv" >
									<!-- <input readonly="" type="text" required="" autocomplete="off" placeholder="Date" name="booked_date" class="form-control facility_datepicker" > -->
								</div>

							</div>
							<?php }  ?>
							<div class="form-group row">
								<label for="payment_type" class="col-sm-4 col-form-label">Payment Type <span class="text-danger">*</span></label>
								<div class="col-sm-8" >
									<select id="payment_type" class="form-control" name="payment_type" onchange="getBankDetailsFacility(this.value)">
										<option value="">-- Select --</option>
										<option value="0">Cash</option>
										<option value="1">Cheque</option>
										<option value="2">Online</option>
										<!-- <option value="4">Un Paid</option> -->
									</select>
								</div>
							</div>
							
							<div id="addBankDetails"></div>
							<div class="form-group row">
								<label for="default-datepicker11" class=" col-sm-4 col-form-label"> Booking Amount <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input type="hidden" readonly="" required="" name="facility_amount_org" maxlength="1000" value="<?php echo $facility_amount; ?>" class="form-control onlyNumber facility_amount_org" >
									<input type="hidden" readonly="" required="" name="facility_amount_org_wallet" maxlength="1000" value="<?php echo $facility_amount; ?>" class="form-control onlyNumber facility_amount_org_wallet" >
									<input readonly  autocomplete="off" onchange="checkFinalePirce();" onkeyup="checkOrgAmount();" type="text" inputmode="numeric" required="" name="facility_amount" maxlength="1000" value="<?php echo $facility_amount; ?>" class="form-control onlyNumber facility_amount" >
								</div>
							</div>
							<div class="form-group row" >
							    <label for="" class="col-sm-4 col-form-label">  </label> 
							     <div class="col-sm-8 text-danger"  id="walletMsg">
							     </div>
							  </div>
							<?php //IS_910 ?>
							<input type="hidden" name="isCash" id="isCash" value="">
							<hr>
							<div class="text-center" style="display: none;" id="bookingButton">
								<!-- <span class="pull-left">Payble Amount: <i id="paybleAmountFacility"><?php echo $facility_amount; ?> </i></span> -->
								<input type="hidden" name="bookFacilityNew" value="bookFacilityNew">
								<button style="margin-right: 5px;" type="submit" class="btn btn-success btn-sm" name="">Confirm Booking</button>
							</div>
						</form>
					 </div>
					</div>
						<div class="col-sm-4">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Day</th>
										<th>Time</th>
									</tr>
								</thead>
								<tbody>
									<?php 
								   $dayName = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
									for ($i=0; $i < 7; $i++) { 
									$qt=$d->select("facility_schedule_master","facility_id='$facility_id' AND facility_day_id='$i'","");
									  ?>
									<tr>
										<td><?php  echo $dayName[$i]; ?></td>
										<td><?php
										while ($timData=mysqli_fetch_array($qt)) {
										 	if ($timData['facility_start_time']=="00:00:00" || $timData['facility_status']==1) {
											echo "Close";
											} else {
											echo date('h:i A',strtotime($timData['facility_start_time']));?> to <?php echo date('h:i A',strtotime($timData['facility_end_time'])).'<br>';
												
											} 
										} ?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
				</div>
				
					
					
			</div>
		</div>
	</div>

</div><!--End Row-->

<!-- User Table-->
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
			<div class="card-body">
				<div class="table-responsive">
					<table id="example" class="table table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th><?php echo $xml->string->unit; ?></th>
								<th>Booked Date</th>
								<th>End Date</th>
								<th>Person</th>
								<th>Amount</th>
								<th>Transaction Charges</th>
								<th>Payment</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=1;
							$q1=$d->select("facilitybooking_master,facilities_master,unit_master,block_master,users_master","users_master.user_id=facilitybooking_master.user_id AND block_master.block_id=unit_master.block_id AND unit_master.unit_id =facilitybooking_master.unit_id AND facilitybooking_master.facility_id=facilities_master.facility_id AND facilitybooking_master.society_id='$society_id' AND facilitybooking_master.facility_id = $facility_id","ORDER BY booking_id DESC");
							while ($fmData=mysqli_fetch_array($q1)) {
								
								?>
								<tr>
									<td><?php echo $i++; ?></td>
									<td><?php echo $fmData['user_full_name']; ?> 
									<?php
									if(!empty($fmData['user_designation']))
									{
										echo " - " . $fmData['user_designation'];
									}
									 ?>
									
									 </td>
									<td><?php echo $fmData['booked_date']; ?></td>
									<td><?php echo $fmData['booking_expire_date']; ?></td>
									<td><?php echo $fmData['no_of_person']; ?></td>
									<td><?php echo number_format($fmData['receive_amount']-$fmData['transaction_charges'],2); ?></td>
									<td><?php echo $fmData['transaction_charges']; ?></td>
									<td><?php if($fmData['payment_type']==0) {
										echo "Cash";
									} elseif ($fmData['payment_type']==1) {
										echo "Cheque";
										echo "<br>";
										echo "Bank: ".$fmData['bank_name'];
										echo "<br>";
										echo "Cheque No.: ".$fmData['payment_ref_no'];
									} elseif ($fmData['payment_type']==2) {
										echo "Online";
										echo "<br>";
										echo "Bank: ".$fmData['bank_name'];
										echo "<br>";
										echo "Ref. No.: ".$fmData['payment_ref_no'];
									} elseif ($fmData['payment_type']==4) {
										echo "Un Paid";
									} ?></td>
									<td>
										<?php if ($fmData['book_status'] == 0 ) { ?>
											<form action="controller/facilitiesController.php" method="post" id="statusForm" >
												<label>Pending</label>
												<input type="hidden" name="unit_id" value="<?php echo $fmData['unit_id']; ?>">
												<input type="hidden" name="facility_id" value="<?php echo $facility_id; ?>">
												<input type="hidden" name="book_status" value="1" >
												<input type="hidden" value="approved" name="approved">
												<input type="submit" name="" class="form-btn btn btn-primary btn-sm" value="Approved ?">
											</form>
											<?php
										} else if($fmData['book_status']==3){
											echo "Cancelled";
										}  else {
											echo "Confirmed";
										}?>
									</td>
									<td>
										<?php
												//IS_955 &&  $fmData['book_status'] !=3 added
										if ($fmData['book_status'] == 0) {
											?>
											<p>
												<div style="display: inline-block;">
													<form action="controller/facilitiesController.php" method="post" >
														<input type="hidden" name="unit_id" value="<?php echo $fmData['unit_id']; ?>">
														<input type="hidden" name="facility_id" value="<?php echo $facility_id; ?>">
														<input type="hidden" name="booking_id" value="<?php echo $fmData['booking_id']; ?>">
														<input type="hidden" value="deleteFacilityReq" name="deleteFacilityReq">
														<button type="submit" class="form-btn btn btn-danger btn-sm" name=""><i class="fa fa-trash"></i> Delete</button>
													</form>
												</div>
											</p>
										<?php } if($fmData['book_status'] == 1){ 
											if (in_array($fmData['block_id'], $blockAryAccess) && count($blockAryAccess)>0) { 
											?>
											<div style="display: inline-block;">
												<form action="controller/facilitiesController.php" method="post" >
													<a  href="invoice.php?user_id=<?php echo $fmData['user_id'] ?>&unit_id=<?php echo $fmData['unit_id'] ?>&type=Fac&societyid=<?php echo $fmData['society_id'] ?>&id=<?php echo $fmData['booking_id'] ?>&facility_id=<?php echo $fmData['facility_id'] ?>" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print</a>

													<input type="hidden" name="unit_id" value="<?php echo $fmData['unit_id']; ?>">
													<input type="hidden" name="facility_id" value="<?php echo $facility_id; ?>">
													<input type="hidden" name="booking_id" value="<?php echo $fmData['booking_id']; ?>">
													<input type="hidden" name="cancelFacilityReq">
													<!-- <button type="submit" class="form-btn btn btn-danger btn-sm" name=""><i class="fa fa-cross"></i> Cancel</button> -->
													<a href="#" onclick="cancelFacility('<?php echo $fmData['booking_id']; ?>')" class=" btn btn-sm btn-danger waves-effect waves-light " data-toggle="modal" data-target="#billPay" href="javascript:void();"> Cancel</a>
												</form>
											</div>
										 	<?php } else if(count($blockAryAccess)==0)  { ?>
										 	<div style="display: inline-block;">
												<form action="controller/facilitiesController.php" method="post" >
													<a   href="invoice.php?user_id=<?php echo $fmData['user_id'] ?>&unit_id=<?php echo $fmData['unit_id'] ?>&type=Fac&societyid=<?php echo $fmData['society_id'] ?>&id=<?php echo $fmData['booking_id'] ?>&facility_id=<?php echo $fmData['facility_id'] ?>" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print</a>

													<input type="hidden" name="unit_id" value="<?php echo $fmData['unit_id']; ?>">
													<input type="hidden" name="facility_id" value="<?php echo $facility_id; ?>">
													<input type="hidden" name="booking_id" value="<?php echo $fmData['booking_id']; ?>">
													<input type="hidden" name="cancelFacilityReq">
													<!-- <button type="submit" class="form-btn btn btn-danger btn-sm" name=""><i class="fa fa-cross"></i> Cancel</button> -->
													<a href="#" onclick="cancelFacility('<?php echo $fmData['booking_id']; ?>')" class=" btn btn-sm btn-danger waves-effect waves-light " data-toggle="modal" data-target="#billPay" href="javascript:void();"> Cancel</a>
												</form>
											</div>
										<?php } } else if($fmData['book_status'] == 3){
											echo "Cancelled";
											if ($fmData['refund_amount']>0) {
												echo "<br>";
												echo "Refund Amount: ".$fmData['refund_amount'];
											}
										} ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

</div>
</div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
//IS_910 
function checkOrgAmount() {
	 var facility_amount_org = parseFloat($('.facility_amount_org_wallet').val());
	 var facility_amount = parseFloat($('.facility_amount').val());
    if(facility_amount_org<facility_amount) {
        var difAmount = parseFloat(facility_amount-facility_amount_org).toFixed(2);
        $('#walletMsg').html(difAmount +' Credited in user wallet for overpaid');
    } else {
          $('#walletMsg').html('');
    } 

   
}

function checkFinalePirce() {
	 var facility_amount_org = parseFloat($('.facility_amount_org').val());
	 var facility_amount = parseFloat($('.facility_amount').val());

    if(facility_amount<facility_amount_org) {
    	swal("Error! Need Minimum "+facility_amount_org+"  !", {icon: "error",});
    	$('.facility_amount').val(facility_amount_org);
    }
}

function calNewPrice() {
	var no_of_person = parseFloat($('#no_of_person').val());
	var facility_amount = parseFloat($('.facility_amount').val());
	var amount_type = parseFloat($('#amount_type').val());
	var facility_amount_org = parseFloat($('.facility_amount_org').val());
	 // alert(facility_amount_org);
	$('#walletMsg').html('');
    if(amount_type==0) {
	   var new_price = parseFloat(facility_amount_org*no_of_person);
	    $('.facility_amount_org_wallet').val(new_price.toFixed(2));
	    $('.facility_amount').val(new_price.toFixed(2));
	    $(".facility_amount").attr({
	       "min" : new_price         // values (or variables) here
	    });
    }
    
}

function calNewPriceHours() {
	var no_of_person = parseFloat($('#no_of_person').val());
	var facility_amount = parseFloat($('.facility_amount').val());
	var amount_type = parseFloat($('#amount_type').val());
	var facility_amount_org = parseFloat($('.facility_amount_org').val());

	var booking_start_time_days= $('#booking_start_time_days').val();
    var booking_end_time_days= $('#booking_end_time_days').val();
    if(user_type==0) {
    var facility_amount= parseInt($('#facility_amount').val());

    } else if(user_type==1){
    var facility_amount= parseInt($('#facility_amount_tenant').val());

    }

    //    //create date format  
	  var time1 = booking_start_time_days.split(':'), time2 = booking_end_time_days.split(':');
	 	var hours1 = parseInt(time1[0], 10), 
	     hours2 = parseInt(time2[0], 10),
	     mins1 = parseInt(time1[1], 10),
	     mins2 = parseInt(time2[1], 10);
		var hours = hours2 - hours1, mins = 0;

	 // get hours
	 if(hours < 0) hours = 24 + hours;

	 // get minutes
	 if(mins2 >= mins1) {
	     mins = mins2 - mins1;
	 }
	 else {
	     mins = (mins2 + 60) - mins1;
	     hours--;
	 }

	 // convert to fraction of 60
	 mins = mins / 60; 

	 hours += mins;
	 hours = hours.toFixed(2);
	 hours = Math.ceil(hours);
	
	$('#walletMsg').html('');
    if(amount_type==0) {
       var facility_amount_org =  parseFloat(facility_amount_org*hours);
	   var new_price = parseFloat(facility_amount_org*no_of_person);
	    $('.facility_amount_org_wallet').val(new_price.toFixed(2));
	    $('.facility_amount').val(new_price.toFixed(2));
	    $(".facility_amount").attr({
	       "min" : new_price         // values (or variables) here
	    });
    } else {
    	var facility_amount_org =  parseFloat(facility_amount_org*hours);
	    var new_price = parseFloat(facility_amount_org);
	    $('.facility_amount_org_wallet').val(new_price.toFixed(2));
	    $('.facility_amount').val(new_price.toFixed(2));
	    $(".facility_amount").attr({
	       "min" : new_price         // values (or variables) here
	    });
    }


}

$( "#bookFacilityFrm" ).submit(function( event ) {
	var error = 0;
	
	
	if($('#isCash').val()=="" && ($('#payment_type').val() =="1" || $('#payment_type').val() =="2" )  ){
		error++;
		$('#addBankDetails').html('<center><label   style="color: #ff0000;" for="addBankDetails">Please Payment type details</label></center>');
	}
	if(error > 0 ){
		event.preventDefault();
	} else {
		$( "#bookFacilityFrm" ).submit();
	}
	
});


//IS_910

function getFloorIds() {
	document.getElementById('unit_id_main').value="";
	$('#MaintanceBox').modal('hide');
	var val = [];
	$(':radio:checked').each(function(i){
		val[i] = $(this).val();
		console.log(val[i]);
		document.getElementById('unit_id_main').value+=val[i];
	});
	getUnitName();
}
function getUnitName() {
	var unit_id = $("#unit_id_main").val();
	$.ajax({
		url: "getFloorName.php",
		cache: false,
		type: "POST",
		data: {unit_id : unit_id},
		success: function(response){
			$('#bookingResp').html(response);


		}
	});
}

function GetNewPrice() {
	var facility_id= $('#facility_id').val();
    var facility_type= $('#facility_type').val();
    var user_id= $('#user_id').val();

    $.ajax({
		url: "getFacilityNewPrice.php",
		cache: false,
		type: "POST",
		data: {facility_id : facility_id,user_id:user_id,facility_type:facility_type},
		success: function(response){
			$('.mothListDiv').html(' ');
			$('#booking_end_time_days').prop('selectedIndex', 0);
			$('#booking_start_time_days').prop('selectedIndex', 0);
			var fields = response.split('~');

			$('.facility_amount').val(fields[0]);
			$('.facility_amount_org').val(fields[0]);
			$('#user_type').val(fields[1]);


		}
	});
}

function getPayAmount(receive_amount,booking_id,balancesheet_id,facility_id) {
	document.getElementById('bookingId').value=booking_id;
	document.getElementById('blId').value=balancesheet_id;
	document.getElementById('blId').value=balancesheet_id;
	document.getElementById('fIddd').value=facility_id;
	document.getElementById('PaybleAmount').innerHTML=receive_amount;
	document.getElementById('rAmount').value=receive_amount;
}

function calFare() {
	var singleFare= parseInt(document.getElementById('facility_amount').value);
	var no_of_month= parseInt(document.getElementById('no_of_month').value);
	var no_of_person= parseInt(document.getElementById('no_of_person').value);

	var totalAmount= singleFare*no_of_month;
	var grandTotal= totalAmount * no_of_person;
		// alert(no_of_month);
		document.getElementById('paybleAmountFacility').innerHTML=grandTotal;
	}

	function getBankDetails(value){
		var client_id=$("#client_matter_id").val();
		var client_Matters=$("#client_Matters").val();


		if(value == '1'){
			$.ajax({
				url: 'getPaymentDetails.php',
				type: 'POST',
				data: {addBankDetails: 'Add Bank Details'},
			})
			.done(function(response) {
				$('#addBankDetails').html(response);
         //IS_910
         $('#isCash').val('0');
        //IS_910
      });
		}else if(value == '2'){
			$.ajax({
				url: 'getPaymentDetails.php',
				type: 'POST',
				data: {onlineBankDetails: 'Add Bank Details'},
			})
			.done(function(response) {
				$('#addBankDetails').html(response);
        //IS_910
        $('#isCash').val('1');
        //IS_910
      });
		}else{
			$('#addBankDetails').html('');
      //IS_910
      $('#isCash').val('');
        //IS_910
      }
    }

    function getBankDetailsModal(value){
    	var client_id=$("#client_matter_id").val();
    	var client_Matters=$("#client_Matters").val();
    	if(value == '1'){
    		$.ajax({
    			url: 'getPaymentDetails.php',
    			type: 'POST',
    			data: {addBankDetailsModal: 'Add Bank Details'},
    		})
    		.done(function(response) {
    			$('#getDetailsModal').html(response);
        //IS_910
        $('#isCashModal').val('0');
        
      });
    	}else if(value == '2'){
    		$.ajax({
    			url: 'getPaymentDetails.php',
    			type: 'POST',
    			data: {onlineBankDetailsModal: 'Add Bank Details'},
    		})
    		.done(function(response) {
    			$('#getDetailsModal').html(response);
        //IS_910
        $('#isCashModal').val('1');
      });
    	}else{
    		$('#getDetailsModal').html('');
      //IS_910
      $('#isCashModal').val('');
    }
  }
</script>




<div class="modal fade" id="payFaci">
	<div class="modal-dialog">
		<div class="modal-content border-primary">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white">Pay Facility Amount</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" >
				<form id="payFacilityFrm" action="controller/facilitiesController.php" method="post">
					<input type="hidden" id="bookingId" name="booking_id">
					<input type="hidden" id="blId" name="balancesheet_id">
					<input type="hidden" id="fIddd" name="facility_id">
					<input type="hidden" id="rAmount" name="receive_amount">
					<div class="form-group row">
						<label for="input-10" class="col-sm-4 col-form-label">Payble Amount</label>
						<div class="col-sm-8" id="PaybleAmount">

						</div>
					</div>
					<div class="form-group row">
						<label for="payment_type" class="col-sm-4 col-form-label">Payment Type</label>
						<div class="col-sm-8">
							<select class="form-control" name="payment_type" onchange="getBankDetailsModal(this.value)">
								<option value="0">Cash</option>
								<option value="1">Cheque</option>
								<option value="2">Online</option>
							</select>
						</div>
					</div>
					<div id="getDetailsModal"></div>
					<?php //IS_910?>
					<input type="hidden" name="isCashModal" id="isCashModal" value="">
					<div class="form-footer text-center">
						<button type="submit" name="payRemAmount" class="btn btn-success"><i class="fa fa-check-square-o"></i> Pay Now</button>
					</div>

				</form>
			</div>

		</div>
	</div>
</div>

<?php //IS_910 ?>
<script type="text/javascript">
	$( "#payFacilityFrm" ).submit(function( event ) {
		var error = 0;
		
		
		if($('#isCashModal').val()=="" && ($('#payFacilityFrm :input[name="payment_type"]').val() =="1" || $('#payFacilityFrm :input[name="payment_type"]').val() =="2" )  ){
			error++;
			$('#getDetailsModal').html('<center><label   style="color: #ff0000;" for="addBankDetails">Please Payment type details</label></center>');
		}
		if(error > 0 ){
			event.preventDefault();
		} else {
			$( "#payFacilityFrm" ).submit();
		}
		
	});
</script>
<script type="text/javascript">
	function resetForm() {
		$('.mothListDiv').html(' ');
		$('#booking_end_time_days').prop('selectedIndex', 0);
		$('#booking_start_time_days').prop('selectedIndex', 0);
		$('.facility_amount').val(' ');
		$('.facility_amount_org').val(' ');
	}
	// $(document).ready(function() {
	// 	$('#hourly').hide();
	// 	$('#daywise').hide();
	// 	$('#monthly').hide();
	// 	var amount_type = $("#amount_type").val();
	// 		if (amount_type == 0) {
	// 			$('#hourly').show();
	// 			$('#daywise').hide();
	// 			$('#monthly').hide();
	// 		} else if (amount_type == 1) {
	// 			$('#hourly').hide();
	// 			$('#daywise').show();
	// 			$('#monthly').hide();
	// 		} else if (amount_type == 2) {
	// 			$('#hourly').hide();
	// 			$('#daywise').hide();
	// 			$('#monthly').show();
	// 		}
		
	// });

  function popitup(url) {
  newwindow=window.open(url,'name','height=800,width=900, location=0');
  if (window.focus) {newwindow.focus()}
  return false;
  }
</script>

<div class="modal fade" id="billPay">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Cancel Paid Facility </h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="cancelPaidEvent">
        
      </div>
      
    </div>
  </div>
  </div><!--End Modal -->
