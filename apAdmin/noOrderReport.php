<?php
if(isset($_POST) && !empty($_POST))
{
    extract($_POST);
}
if(isset($_GET['Df']) && !empty($_GET['Df']) && $_GET['Df'] != "0000-00-00 00:00:00")
{
    $date_from = $_GET['Df'];
}
elseif(isset($_COOKIE['Df']) && !empty($_COOKIE['Df']) && $_COOKIE['Df'] != "0000-00-00 00:00:00")
{
    $date_from = $_COOKIE['Df'];
}
else
{
    $date_from = date("Y-m-d");
}

if(isset($_GET['Dt']) && !empty($_GET['Dt']) && $_GET['Dt'] != "0000-00-00 00:00:00")
{
    $date_to = $_GET['Dt'];
}
elseif(isset($_COOKIE['Dt']) && !empty($_COOKIE['Dt']) && $_COOKIE['Dt'] != "0000-00-00 00:00:00")
{
    $date_to = $_COOKIE['Dt'];
}
else
{
    $date_to = date("Y-m-d");
}

$format = 'Y-m-d';
if(isset($date_from) && !empty($date_from))
{
    $df = DateTime::createFromFormat($format, $date_from);
    $result = $df && $df->format($format) === $date_from;
    if($result != 1)
    {
        $_SESSION['msg1'] = "Invalid From Date!";
    ?>
        <script>
            window.location = "manageOrders";
        </script>
    <?php
        exit;
    }
}

if(isset($date_to) && !empty($date_to))
{
    $dt = DateTime::createFromFormat($format, $date_to);
    $result = $dt && $dt->format($format) === $date_to;
    if($result != 1)
    {
        $_SESSION['msg1'] = "Invalid To Date!";
    ?>
        <script>
            window.location = "manageOrders";
        </script>
    <?php
        exit;
    }
}

if(isset($_GET['UId']) && !empty($_GET['UId']))
{
    $user_id = $_GET['UId'];
}

if(isset($user_id) && !empty($user_id) && !ctype_digit($user_id))
{
    $_SESSION['msg1'] = "Invalid User Id!";
    ?>
    <script>
        window.location = "manageOrders";
    </script>
<?php
    exit;
}

if(isset($_GET['RId']) && !empty($_GET['RId']))
{
    $retailer_id = $_GET['RId'];
}

if(isset($retailer_id) && !empty($retailer_id) && !ctype_digit($retailer_id))
{
    $_SESSION['msg1'] = "Invalid Retailer Id!";
    ?>
    <script>
        window.location = "manageOrders";
    </script>
<?php
    exit;
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-6">
                <h4 class="page-title">View No Orders</h4>
            </div>
        </div>
        <form id="filterFormOrders">
            <div class="row pt-2 pb-2">
                <div class="col-lg-3 col-6">
                    <label  class="form-control-label">User </label>
                    <select class="form-control single-select-new" id="UId" name="UId">
                        <option value="">-- Select --</option>
                        <?php
                            $qu = $d->selectRow("um.user_id,um.user_full_name,bm.block_name,fm.floor_name","users_master AS um JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id","delete_status = 0 AND active_status = 0");
                            while ($ud = $qu->fetch_assoc())
                            {
                            ?>
                        <option <?php if($user_id == $ud['user_id']) { echo 'selected'; } ?> value="<?php echo $ud['user_id']; ?>"><?php echo $ud['user_full_name'] . " (" . $ud['block_name'] . "-" . $ud['floor_name'] . ")"; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Retailer </label>
                    <select class="form-control single-select" id="RId" name="RId">
                        <option value="">-- Select --</option>
                        <?php
                            $rt = $d->selectRow("rm.retailer_id,rm.retailer_name,c.city_name,amn.area_name","retailer_master AS rm JOIN cities AS c ON c.city_id = rm.city_id JOIN area_master_new AS amn ON amn.area_id = rm.area_id");
                            while ($rd = $rt->fetch_assoc())
                            {
                        ?>
                        <option <?php if(isset($retailer_id) && $retailer_id == $rd['retailer_id']) { echo 'selected'; } ?> value="<?php echo $rd['retailer_id'];?>"><?php echo $rd['retailer_name'] . " (" . $rd['area_name'] . "-" . $rd['city_name'] . ")";?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">Start Date </label>
                    <input type="text" class="form-control" autocomplete="off" id="date_from_new" name="Df" value="<?php if(isset($date_from) && $date_from != ""){ echo $date_from; } ?>">
                </div>
                <div class="col-lg-3 col-6">
                    <label class="form-control-label">End Date </label>
                    <input  type="text" class="form-control" autocomplete="off" id="date_to_new" name="Dt" value="<?php if(isset($date_to) && $date_to != ""){ echo $date_to; } ?>">
                </div>
                <div class="col-lg-2 col-6">
                    <input type="hidden" name="get_data" value="get_data"/>
                    <button class="btn btn-success" style="margin-top:30px;" type="button" onclick="submitFilterForm();">Submit</button>
                </div>
            </div>
        </form>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example10" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User</th>
                                        <th>Retailer</th>
                                        <th>Reason</th>
                                        <th>Date Time</th>
                                        <th>Area</th>
                                        <th>City</th>
                                        <th>Remarks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="assets/js/jquery.min.js"></script>
<script>
function submitFilterForm()
{
    var from_d = $('#date_from_new').val();
    var to_d = $('#date_to_new').val();
    if(from_d != "" && to_d == "")
    {
        swal("Please select end date to use filter!", {icon: "error",});
    }
    else
    {
        loaddata();
    }
}

$(document).ready(function()
{
    var user_id = '<?php echo $user_id ?>';
    var retailer_id = '<?php echo $retailer_id ?>';
    var date_from = '<?php echo $date_from ?>';
    var date_to = '<?php echo $date_to ?>';
    loaddata(user_id,retailer_id,date_from,date_to);
});

function loaddata(user_id,retailer_id,date_from,date_to)
{
    $('#spinner').fadeIn(1);
    if($.fn.DataTable.isDataTable('#example10'))
    {
        $('#example10').DataTable().destroy();
    }
    if(user_id == "" || user_id == null || user_id == 0)
    {
        user_id = $('#UId').val();
    }
    if(retailer_id == "" || retailer_id == null || retailer_id == 0)
    {
        retailer_id = $('#RId').val();
    }
    if(date_from == "" || date_from == null || date_from == "0000-00-00 00:00:00")
    {
        date_from = $('#date_from_new').val();
    }
    if(date_to == "" || date_to == null || date_to == "0000-00-00 00:00:00")
    {
        date_to = $('#date_to_new').val();
    }
    pintable = $('#example10').DataTable({
        <?php
        if($adminData['report_download_access'] != 0)
        {
        ?>
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false,
        "dom": 'Blfrtip',
        "buttons": [
        {
            extend: 'copyHtml5',
            title: $("#reportName").val(),
            exportOptions: {
              columns: ':visible'
            }
        },
        {
            extend: 'csv',
            title: $("#reportName").val(),
            exportOptions: {
              columns: ':visible'
            }
        },
        {
            extend: 'excelHtml5',
            title: $("#reportName").val(),
            exportOptions: {
              columns: ':visible'
            }
        },
        {
            extend : 'pdfHtml5',
            title: $("#reportName").val(),
            orientation : 'landscape',
            pageSize : 'LEGAL',
            titleAttr : 'PDF',
            exportOptions: {
              columns: ':visible'
            }
        },'colvis'],
        <?php
        }
        ?>
        "columnDefs": [{
            "defaultContent": "",
            "targets": "_all"
        }],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "ajax": {
            url: "controller/orderProductController.php",
            type: "post",
            data: {getNoOrderList:"getNoOrderList",user_id:user_id,retailer_id:retailer_id,date_from:date_from,date_to:date_to,csrf:csrf},
            dataSrc: ""
        },
        "columns": [
            { "data": "no" },
            { "data": "user_name" , render : function ( data, type, row, meta ) {
                return data.user_full_name + " (" + data.block_name +  " - " + data.floor_name + ")";
            }},
            { "data": "retailer_name" },
            { "data": "no_order_reason" },
            { "data": "no_order_created_date" },
            { "data": "area_name" },
            { "data": "city_name" },
            { "data": "no_order_remark" }
        ]
    });
    $('#spinner').fadeOut("slow");
}

function destroydata()
{
    $('#example10').html('');
    pintable.destroy();
}
</script>