<?php error_reporting(0);?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Admin Task</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
          <a href="addAdminTask" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>

            <a href="javascript:void(0)" onclick="DeleteAll('deleteAdminTask');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
          </div>
        </div>
     </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No</th>                        
                        <th>#</th>
                        <th>Name</th>
                        <th>Added By</th>
                        <th>Due Date</th>                      
                        <th>Assign To</th>                      
                        <th>Complete Status</th>                      
                        <th>Complete Date</th>                      
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;
                        $q=$d->select("bms_admin_master, task_master LEFT JOIN users_master ON users_master.user_id=task_master.task_assign_to","bms_admin_master.admin_id=task_master.task_add_by AND task_master.society_id='$society_id' AND task_master.task_add_by_type=1 AND task_master.task_delete=0","ORDER BY task_master.task_id DESC");
                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                        <td><?php echo $counter++; ?></td>
                        <td class="text-center">
                          <?php if($data['task_complete'] == 0){ ?>
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['task_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['task_id']; ?>">    
                          <?php } ?>                  
                        </td>
                       <td><?php echo $data['task_name']; ?></td>
                       <td><?php echo $data['admin_name']; ?></td>
                       <td><?php echo date("d M Y", strtotime($data['task_due_date'])); ?></td>
                       <td><?php echo $data['user_full_name']; ?></td>	
                       <?php 
                        if($data['task_complete'] == 0){
                            $status = "Pending";
                            $statusClass = "badge-primary";
                        }
                        else if($data['task_complete'] == 1){
                            $status = "In Progress";
                            $statusClass = "badge-info";
                        }else{
                            $status = "Completed";
                            $statusClass = "badge-success";
                        }
                        ?>
                        <!-- <b><span class="badge badge-pill m-1 <?php echo $statusClass; ?>"><?php echo $status; ?></span></b> -->
                        <td>
                        <?php if($data['task_complete'] != 2){ ?>
                        <form id="taskCompleteStatus" action="controller/TaskController.php" enctype="multipart/form-data" method="post">
                          <select class="form-control task_complete" name="task_complete" >
                            <?php if($data['task_complete'] == 0){ ?>
                              <option value="0" <?php if($data['task_complete'] == 0){echo "selected";} ?>>Pending</option>
                              <option value="1" <?php if($data['task_complete'] == 1){echo "selected";} ?>>In Progress</option>
                              <?php }else{ ?>
                                <option value="1" <?php if($data['task_complete'] == 1){echo "selected";} ?>>In Progress</option>
                                <option value="2" <?php if($data['task_complete'] == 2){echo "selected";} ?>>Completed</option>
                              <?php } ?>
                          </select>
                          <input type="hidden" name="task_id" value="<?php echo $data['task_id']; ?>">
                          <input type="hidden" name="taskCompleteStatus" value="taskCompleteStatus">
                        </form>
                        <?php }else{ ?>
                          <b><span class="badge badge-pill m-1 badge-success">Completed</span></b>
                        <?php } ?>
                        </td>
                        <td><?php if($data['task_complete_date'] != '0000-00-00 00:00:00' && $data['task_complete_date'] != null){ echo date("d M Y h:i A", strtotime($data['task_complete_date']));} ?></td>
                        <td>
                            <div class="d-flex align-items-center">
                                <a href="taskDetail?id=<?php echo $data['task_id']?>" title="View Detail" class="btn btn-sm btn-primary mr-2"> <i class="fa fa-eye"></i></a>
                                <?php if($data['task_complete'] == 0){ ?>
                                <form action="addAdminTask" method="post" accept-charset="utf-8" class="mr-2">
                                  <input type="hidden" name="task_id" value="<?php echo $data['task_id']; ?>">
                                  <input type="hidden" name="edit_task" value="edit_task">
                                  <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button> 
                                </form>
                                <?php if($data['task_status']=="0"){
                                    $status = "taskStatusDeactive";  
                                    $active="checked";                     
                                } else { 
                                    $status = "taskStatusActive";
                                    $active="";  
                                } ?>
                                <input type="checkbox" <?php echo $active; ?> class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $data['task_id']; ?>','<?php echo $status; ?>');" data-size="small"/>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>

<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
