<?php error_reporting(0);
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Company Opening Apply</h4>
     </div>
     <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">        
        <a href="javascript:void(0)" onclick="DeleteAll('deleteCompanyOpeningApply');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a>

      </div>
     </div>
     </div>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
                <?php //IS_675  example to examplePenaltiesTbl?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sr.No</th>    
                        <th>Position</th>                      
                        <th>Name</th>                      
                        <th>DOB</th>                                               
                        <th>Gender</th>                      
                        <th>State</th>                      
                        <th>City</th>                                               
                        <th>Contact No</th>                                               
                        <th>Email Id</th>                                               
                        <th>Education</th>                                               
                        <th>Experience</th>                                               
                        <th>Resume</th>  
                       
                    </tr>
                </thead>
                <tbody>
                   <?php 
                      $i=1;
                      if(isset($_GET) && $_GET['id']>0){
                        $appnedQuery = " AND company_current_opening_apply_master.company_current_opening_id='$_GET[id]'";
                      }
                      $q=$d->select("company_current_opening_apply_master, company_current_opening_master","company_current_opening_apply_master.society_id='$society_id' AND company_current_opening_apply_master.company_current_opening_id=company_current_opening_master.company_current_opening_id $appnedQuery", "ORDER BY company_current_opening_apply_master.company_current_opening_apply_id DESC");
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     ?>
                    <tr>
                       <td class="text-center">
                          <input type="hidden" name="id" id="id"  value="<?php echo $data['company_current_opening_apply_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['company_current_opening_apply_id']; ?>">                      
                        </td>
                        <td><?php echo $counter++; ?></td>
                       <td><?php echo $data['company_current_opening_title']; ?></td>
                       <td><?php echo $data['company_current_opening_name']; ?></td>
                       <td><?php if ($data['company_current_opening_dob'] != '0000-00-00' && $data['company_current_opening_dob'] != 'null') {
                                echo date("d M Y", strtotime($data['company_current_opening_dob']));
                              } ?></td>
                       <td><?php echo $data['company_current_opening_gender']; ?></td>
                       <td><?php echo $data['company_current_opening_state']; ?></td>
                       <td><?php echo $data['company_current_opening_city']; ?></td>
                       <td><?php echo $data['company_current_opening_contact_no']; ?></td>
                       <td><?php echo $data['company_current_opening_email_id']; ?></td>
                       <td><?php echo $data['company_current_opening_education_info']; ?></td>
                       <td><?php echo $data['company_current_opening_experience']; ?></td>
                       <td> <a href="../img/opening_resume/<?php echo $data['company_current_opening_resume']; ?>" target="_blank">Resume</a></td>
                       
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div>
<div class="modal fade" id="productVendorDetailModal">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Canteen Vendor Details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-2 "  style="align-content: center;">
        <div class="row mx-0" id="product_vendor_detail">

        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
