<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-5">
        <h4 class="page-title">Complaint Category</h4>
      </div>
      <div class="col-sm-3 col-7">
        <div class="btn-group float-sm-right">
          <button type="button" data-toggle="modal" data-target="#complainCategoryModal" class="float-right btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add</button>
          <a href="javascript:void(0)" onclick="DeleteAll('deleteComplaintCategory');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a> 
        </div>
      </div>
    </div>
    <!-- End Breadcrumb-->

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>Category</th>
                    <th>Action</th>

                  </tr>
                </thead>
                <tbody>
                 <?php 
                 $i=1;
                 $q=$d->select("complaint_category","active_status=0","");
                 while($row=mysqli_fetch_array($q))
                 {
                  extract($row);
                  ?>
                  <tr>
                    <td class='text-center'>
                      <?php 
                        $billExist = $d->selectArray("complains_master","complaint_category='$complaint_category_id'");
                        if(empty($billExist)){
                      ?>
                        <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['complaint_category_id']; ?>">
                      <?php } ?>
                    </td>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo html_entity_decode($category_name); ?></td>
                    <td>
                      <button type="button" data-toggle="modal" data-target="#editcomplainCategoryModal" class="btn btn-info btn-sm" onclick="editComplainCategory(<?php echo $complaint_category_id ?>)"><i class="fa fa-pencil"></i></button>
                    </td>
                  </tr>
                  <?php }?>

                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function editComplainCategory(argument) {
    $.ajax({
      url: 'editcomplaintCategory.php',
      type: 'POST',
      data: {complaint_category_id: argument},
    })
    .done(function(response) {
      $('#editcomplaintCatDetails').html(response);
    });
  }
</script>

<div class="modal fade" id="complainCategoryModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Complaint Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="complainCategoryValidation" method="POST" action="controller/complaintCategoryController.php" enctype="multipart/form-data">
            <div class="row form-group">
              <label class="col-sm-3 form-control-label">Name <span class="text-danger">*</span></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" required="" name="category_name">
              </div>
            </div>
              <div class="form-footer text-center">
              <button class="btn btn-sm btn-success"><i class="fa fa-check"></i> Submit</button>
            </div>
          </form>
      </div>
     
    </div>
  </div>
</div>

<div class="modal fade" id="editcomplainCategoryModal">
  <div class="modal-dialog">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Edit Complaint Category</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="editcomplaintCatDetails">
      </div>
     
    </div>
  </div>
</div>