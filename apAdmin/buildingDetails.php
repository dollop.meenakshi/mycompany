<?php
$q=$d->select("society_master","society_id='$society_id'");
$bData=mysqli_fetch_array($q);
extract($bData);
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
   
    
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form id="buildingDetails" action="controller/buildingController.php" method="post" enctype="multipart/form-data">
              <h4 class="form-header text-uppercase">
              <i class="fa fa-building"></i>
              <?php echo $xml->string->edit; ?> <?php echo $xml->string->BuildingDetails; ?>
              </h4>
             
              <div class="form-group row">
                <div class="col-sm-2">
                </div>
                <div class="col-sm-3 text-center mt-5">
                  <label for="soceityLogo">
                       <img  onerror="this.src='../img/defaultSocityLogo.jpg'"  id="societyLogoView" style="border: 1px solid gray; width: 250px; height: 100px; border-radius: 15px;cursor: pointer;" src="<?php if(file_exists("../img/society/$socieaty_logo")) { echo '../img/society/'.$socieaty_logo; }  else { echo '../img/defaultSocityLogo.jpg'; } ?>"  src="#" alt="your image" class='' />
                       <i style="position: absolute;font-size: 20px;margin-left: -16px;margin-top: 86px;" class="fa fa-camera"></i>
                     <input accept="image/*" class="photoInput photoOnly" id="soceityLogo" type="file" name="socieaty_logo">
                     <br>
                     <?php echo $xml->string->society; ?> <?php echo $xml->string->logo; ?> (300 PX * 200 PX)
                   </label>
                </div>
                <div class="col-sm-7 text-center pl-5 pr-5">
                   <label for="soceityCover">
                       <img  onerror="this.src='../img/coverPhoto.jpg'"  id="societyCoverView" style="border: 1px solid gray; width: 100%; border-radius: 15px; height: 250px;cursor: pointer;" src="<?php if(file_exists("../img/society/$socieaty_cover_photo")) { echo '../img/society/'.$socieaty_cover_photo; }  else { echo '../img/coverPhoto.jpg'; } ?>"  src="#" alt="your image" class='' />
                       <i style="position: absolute;font-size: 20px;margin-left: -16px;margin-top: 237px;" class="fa fa-camera"></i>
                     <input accept="image/*" class="photoInput photoOnly" id="soceityCover" type="file" name="socieaty_cover_photo">
                     <br>
                     <?php echo $xml->string->society; ?> <?php echo $xml->string->photo; ?> (500 PX & 300 PX) 
                   </label>
                </div>
              </div>
              <div class="form-group row">
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->society; ?> <?php echo $xml->string->name; ?> <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <input maxlength="50" required="" value="<?php echo $society_name; ?>" type="text" class="form-control" id="input-10" name="society_name_update">
                </div>
              </div>
             
              <div class="form-group row">
                <label for="input-17" class="col-sm-2 col-form-label"><?php echo $xml->string->address; ?>  <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <textarea maxlength="300" required=""  name="society_address_edit" class="form-control" rows="4" id="input-17"><?php echo $society_address; ?></textarea>
                </div>
                
                
              </div>
              
             <div class="form-group row">
                  <label for="plan_expire_date" class="col-sm-2 col-form-label"><?php echo $xml->string->pincode; ?> <span class="required">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" id="society_pincode"  maxlength="6" value="<?php if($society_pincode!='0') { echo $society_pincode; } ?>" required="" class="form-control onlyNumber" inputmode="numeric" name="society_pincode">
                  </div>
                
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->tax_number; ?> </label>
                <div class="col-sm-4">
                  <input minlength="4" maxlength="30" value="<?php echo $gst_no; ?>" type="text" class="form-control text-uppercase alphanumeric" id="input-10" name="gst_no">
                </div>
              </div>
               <div class="form-group row">
                  <label for="plan_expire_date" class="col-sm-2 col-form-label"><?php echo $xml->string->pan_number; ?></label>
                  <div class="col-sm-4">
                    <input type="text" id="pan_number"  maxlength="30" value="<?php  echo $pan_number; ?>" class="form-control text-uppercase" name="pan_number">
                  </div>
                <label for="input-10" class="col-sm-2 col-form-label"><?php echo $xml->string->currency_symbol; ?>  <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input required="" minlength="1" maxlength="4" value="<?php echo $currency; ?>" type="text" class="form-control" id="input-10" name="currency">
                </div>
              </div>
                <div class="form-group row">
                <label for="financial_year_start" class="col-sm-2 col-form-label"><?php echo $xml->string->financial_year_date; ?></label>
                <div class="col-sm-4">
                  <div id="dateragne-picker">
                    <div class="input-daterange input-group">
                    <input readonly="" type="text" class="form-control" autocomplete="off" placeholder="Start Date" name="financial_year_start" value="<?php if($financial_year_start!="0000-00-00") { echo $financial_year_start; } ?>" />
                      <div class="input-group-prepend">
                       <span class="input-group-text"><?php echo $xml->string->to; ?></span>
                      </div>
                      <input readonly="" type="text" class="form-control" autocomplete="off" placeholder="End Date" name="financial_year_end" value="<?php if($financial_year_end!="0000-00-00") { echo $financial_year_end; } ?>" />
                    </div>
                  </div>
                </div>
                <label for="financial_year_start" class="col-sm-2 col-form-label"><?php echo $xml->string->visitors; ?> <?php echo $xml->string->auto_rejected; ?> <span class="required">*</span></label>
                <div class="col-sm-4">
                  <input min="5" max="120" minlength="1" required="" maxlength="10" value="<?php echo $auto_reject_vistor_minutes; ?>" type="text" class="form-control onlyNumber" inputmode="numeric" id="input-10" name="auto_reject_vistor_minutes">
                  <i>In Minutes</i>
                </div>
              </div>
               <?php
                if ($adminData['admin_type']==1 || $adminData['role_id']==2) { ?>
              <div class="form-group row">
                
                <label for="financial_year_start" class="col-sm-2 col-form-label"><?php echo $xml->string->complaint; ?> <?php echo $xml->string->reopen; ?> <?php echo $xml->string->max_time; ?><span class="required">*</span></label>
                <div class="col-sm-4">
                  <input min="1" max="46400" minlength="1" required="" maxlength="10" value="<?php echo $complaint_reopen_minutes; ?>" type="text" class="form-control onlyNumber" inputmode="numeric" id="input-10" name="complaint_reopen_minutes">
                  <i>In Minutes</i>
                </div>
              
                 <label for="financial_year_start" class="col-sm-2 col-form-label"><?php echo $xml->string->default_timezone; ?> <span class="required">*</span></label>
                <div class="col-sm-4">
                  <select class="form-control single-select" name="default_time_zone" required="">
                    <option>-- Select --</option>
                    <?php $qt = $d->select("timezoneMaster","");
                    while ($timeData=mysqli_fetch_array($qt)) { ?>
                      <option <?php if($default_time_zone==$timeData['timezone_name']) { echo 'selected'; } ?> value="<?php echo $timeData['timezone_name'];?>"><?php echo $timeData['timezone_name'];?>-<?php echo $timeData['timezone_category'];?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            <?php  } else { ?>
               <input min="5" minlength="1" required="" maxlength="3" value="<?php echo $bill_cancel_minutes; ?>" type="hidden" class="form-control onlyNumber" inputmode="numeric" id="input-10" name="bill_cancel_minutes">
                <input min="1" minlength="1" required="" maxlength="3" value="<?php echo $complaint_reopen_minutes; ?>" type="hidden" class="form-control onlyNumber" inputmode="numeric" id="input-10" name="complaint_reopen_minutes">
            <?php } ?>
              <div class="form-footer text-center">
                 <input type="hidden" value="<?php echo $socieaty_logo; ?>" class="form-control" id="input-4"  name="socieaty_logo_old" required>
                 <input type="hidden" value="<?php echo $socieaty_cover_photo; ?>" class="form-control" id="input-4"  name="socieaty_cover_photo_old" required>
                <input type="hidden" name="updateBuildingSingle" value="updateBuildingSingle">
                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      </div><!--End Row-->


     
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->