<?php error_reporting(0);
$cId = (int)$_REQUEST['cId'];
$dId = (int)$_REQUEST['dId'];
$bId = (int)$_REQUEST['bId'];
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-3 col-md-6 col-6">
        <h4 class="page-title">Employee Bank Details</h4>
      </div>
      <div class="col-sm-3 col-md-6 col-6 text-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <a href="addHrDoc" data-toggle="modal" data-target="#addModal" onclick="buttonSettings()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
        <a href="javascript:void(0)" onclick="DeleteAll('deleteUserBank');" class="btn  btn-sm btn-danger  mr-1"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
      </div>
    </div>
    <form action="" class="branchDeptFilter">
      <div class="row pt-2 pb-2">
        <?php include('selectBranchDeptForFilterAll.php'); ?>
        <div class="col-md-1 form-group ">
          <input class="btn btn-success btn-sm" type="submit" name="getReport" class="form-control" value="Get Data">
        </div>
      </div>
    </form>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <?php 
              ?>
              <table id="example" class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Sr.No</th>
                    <th>Name</th>
                    <th>Branch</th>
                    <th>Bank Name</th>
                    <th>Account No</th>
                    <th>Account Type</th>
                    <th>IFSC</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="showFilterData">
                  <?php
                  $i = 1;
                  if(isset($bId) && $bId>0) {
                    $blockFilterQuery = " AND users_master.block_id='$bId'";
                  }
                        
                  if (isset($cId) && $cId > 0) {
                    $categoryFilterQuery = " AND hr_document_master.hr_document_category_id='$cId'";
                  }
                  if (isset($dId) && $dId > 0) {
                    $departmentFilterQuery = " AND users_master.floor_id='$dId'";
                  }
                  $q = $d->select("block_master,floors_master,user_bank_master,users_master", "block_master.block_id=floors_master.block_id AND users_master.floor_id=floors_master.floor_id AND users_master.user_id=user_bank_master.user_id AND users_master.delete_status=0 AND user_bank_master.society_id='$society_id' $blockFilterQuery $departmentFilterQuery $blockAppendQueryUser");

                  $counter = 1;
                  while ($data = mysqli_fetch_array($q)) {
                  ?>
                    <tr>
                      <td class="text-center">
                        <input type="hidden" name="id" id="id" value="<?php echo $data['bank_id']; ?>">
                        <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['bank_id']; ?>">
                      </td>
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['user_full_name']; ?> (<?php echo $data['user_designation']; ?>)</td>
                      <td><?php echo $data['block_name'].'-'. $data['floor_name']; ?></td>
                      <td><?php echo $data['bank_name']." (". $data['bank_branch_name'].")"; ?></td>
                      <td><?php echo $data['account_no']; ?></td>
                      <td><?php echo $data['account_type']; ?></td>
                      <td><?php echo $data['ifsc_code']; ?></td>
                      <td>
                        <div class="d-flex align-items-center">
                          <form method="post" accept-charset="utf-8">
                            <input type="hidden" name="bank_id" value="<?php echo $data['bank_id']; ?>">
                            <input type="hidden" name="edit_hr_doc" value="edit_hr_doc">
                            <button type="button" class="btn btn-sm btn-primary mr-1" onclick="UserBankDataSet(<?php echo $data['bank_id']; ?>)" data-toggle="modal" data-target="#addModal"> <i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-sm btn-primary mr-1" onclick="UserBankDetail(<?php echo $data['bank_id']; ?>)" ><i class="fa fa-eye"></i></button>
                          </form>
                        </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Row-->

  </div>
  <!-- End container-fluid-->

</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->

<div class="modal fade" id="addModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Update Employee bank details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body">

          <form id="addUserBank" name="addUserBank" action="controller/UserBankController.php" enctype="multipart/form-data" method="post">

            <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Branch <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <select name="block_id" id="block_id" class="form-control single-select addRest" onchange="getVisitorFloorByBlockId(this.value)" required>
                  <option value="">-- Select Branch --</option>
                  <?php
                  $qb = $d->select("block_master", "society_id='$society_id' $blockAppendQueryOnly");
                  while ($blockData = mysqli_fetch_array($qb)) {
                  ?>
                    <option <?php if ($bId == $blockData['block_id']) {
                              echo 'selected';
                            } ?> value="<?php echo  $blockData['block_id']; ?>"><?php echo $blockData['block_name']; ?></option>
                  <?php } ?>

                </select>
              </div>

            </div>
            <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Department <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <select name="floor_id" id="floor_id" class="form-control single-select floor_id addRest" onchange="getUser(this.value)">
                  <option value="">--Select Department--</option>
                  <?php
                  $qd = $d->select("block_master,floors_master", "floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id");
                  while ($depaData = mysqli_fetch_array($qd)) {
                  ?>
                    <option <?php if ($dId == $depaData['floor_id']) {
                              echo 'selected';
                            } ?> value="<?php echo  $depaData['floor_id']; ?>"><?php echo $depaData['floor_name'] . '-' . $depaData['block_name']; ?></option>
                  <?php } ?>

                </select>
              </div>

            </div>
            <div class="form-group row">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Employee<span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <select id="user_id" type="text" required="" class="form-control single-select addRest" name="user_id">
                  <option value="">-- Select --</option>

                </select>
              </div>
            </div>
            <input type="hidden" name="user_id_old" id="user_id_old">
            <input type="hidden" name="floor_id_old" id="floor_id_old">
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Bank Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="bank_name" id="bank_name" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Branch Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="bank_branch_name" id="bank_branch_name" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Account Type<span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
               <select name="account_type" id="account_type" class="form-control" >
                 <option value="Current">Current</option>
                 <option value="Saving">Saving</option>
               </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Account Holder's Name <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="account_holders_name" id="account_holders_name" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Account No <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="account_no" id="account_no" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">IFSC Code <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" maxlength="11" style="text-transform:uppercase;" name="ifsc_code" id="ifsc_code" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Customer Id/CRN No. </label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" name="crn_no" id="crn_no" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">ESIC No </label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" maxlength="10" name="esic_no" id="esic_no" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="input-10" class="col-sm-4 col-form-label">Pan Card No </label>
              <div class="col-lg-8 col-md-8" id="">
                <input type="text" maxlength="10" style="text-transform:uppercase;" name="pan_card_no" id="pan_card_no" class="form-control">
              </div>
            </div>
            <div class="form-footer text-center">
              <input type="hidden" id="bank_id" name="bank_id" value="<?php if ($data['bank_id'] != "") {echo $data['bank_id']; } ?>">
              <button id="addHrDocumentBtn" type="submit" class="btn btn-success addUserBank hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
              <input type="hidden" name="addUserBank" value="addUserBank">

              <button id="addHrDocumentBtn" type="submit" class="btn addUserBank btn-success hideAdd"><i class="fa fa-check-square-o"></i> Add</button>

              <button type="reset" value="add" class="btn btn-danger  cancel hideAdd" onclick="resetFrm('addHrDocumentAdd');"><i class="fa fa-check-square-o"></i> Reset</button>

            </div>

          </form>

        </div>
      </div>

    </div>
  </div>
</div>
<div class="modal fade" id="BankDetailsModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Employee bank details</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="billPayDiv" style="align-content: center;">
        <div class="card-body">
          <div id="bankDetails">
          </div>
        </div>
      </div>

    </div>
  </div>
</div>



<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
  function buttonSettings() {
    $('#addUserBank').trigger('reset');
    $('.hideupdate').hide();
    $('.hideAdd').show();
    $('.addRest').attr("disabled", false);
    $('.addRest').val("");
    //$('.floor_id').val(response.bank.floor_id).trigger("change", true);

    $('.addRest').select2();
    //$('.addRest').val("").trigger("change", true);

  }

  function popitup(url) {
    newwindow = window.open(url, 'name', 'height=800,width=900, location=0');
    if (window.focus) {
      newwindow.focus()
    }
    return false;
  }
</script>
<style>
  .hideupdate {
    display: none;
  }
</style>