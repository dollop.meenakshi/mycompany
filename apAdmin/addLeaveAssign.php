<?php
extract(array_map("test_input", $_POST));
if (isset($edit_leave_assign)) {
    $q = $d->select("leave_assign_master","user_leave_id='$user_leave_id'");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>

     <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
        <h4 class="page-title"> Leave Assign</h4>

     </div>
     <div class="col-sm-3">

     </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
           
                <form id="leaveAssignAdd" action="controller/LeaveAssignController.php" enctype="multipart/form-data" method="post">
                <div class="form-group row">
                    <label for="input-10" class="col-lg-2 col-md-2 col-form-label">Leave Type <span class="required">*</span></label>
                        <div class="col-lg-10 col-md-10" id="">
                             <select class="form-control" name="leave_type_id">
                             <option value="">-- Select --</option> 
                                <?php 
                                    $leaveType=$d->select("leave_type_master","society_id='$society_id' AND leave_type_active_status = 0");  
                                    while ($leaveTypeData=mysqli_fetch_array($leaveType)) {
                                ?>
                                <option <?php if(isset($data['leave_type_id']) && $data['leave_type_id'] ==$leaveTypeData['leave_type_id']){ echo "selected"; } ?> value="<?php if(isset($leaveTypeData['leave_type_id']) && $leaveTypeData['leave_type_id'] !=""){ echo $leaveTypeData['leave_type_id']; } ?>"><?php if(isset($leaveTypeData['leave_type_name']) && $leaveTypeData['leave_type_name'] !=""){ echo $leaveTypeData['leave_type_name']; } ?></option> 
                                <?php } ?>
                             </select>                   
                    </div> 
                </div> 
                <div class="form-group row">
                    <label for="input-10" class="col-sm-2 col-form-label">Employee <span class="required">*</span></label>
                    <div class="col-lg-10 col-md-10" id="">
                      <select  type="text" required="" class="form-control single-select" name="user_id">
                        <option value="">-- Select --</option>
                        <?php
                        $userData=$d->select("users_master","society_id='$society_id'");
                        while ($user=mysqli_fetch_array($userData)) {
                          ?>
                         <option <?php if(isset($data['user_id']) && $data['user_id'] == $user['user_id']){ echo "selected"; } ?> value="<?php if(isset($user['user_id']) && $user['user_id'] !=""){ echo $user['user_id']; } ?>"> <?php if(isset($user['user_full_name']) && $user['user_full_name'] !=""){ echo $user['user_full_name']; } ?></option>
                         <?php }?>
                      </select>
                    </div>                   
                </div> 
                <div class="form-group row">
                    <label for="input-10" class="col-lg-2 col-md-2 col-form-label">User Total Leave <span class="required">*</span></label>
                    <div class="col-lg-10 col-md-10" id="">
                        <input type="text" class="form-control" placeholder="User Total Leave" name="user_total_leave" value="<?php if($data['user_total_leave'] !=""){ echo $data['user_total_leave']; } ?>">
                    </div> 
                </div>                 
                <div class="form-footer text-center">
                  <?php //IS_1019 addPenaltiesBtn 
                  if (isset($edit_leave_assign)) {                    
                  ?>
                  <input type="hidden" id="user_leave_id" name="user_leave_id" value="<?php if($data['user_leave_id'] !=""){ echo $data['user_leave_id']; } ?>" >
                  <button id="addLeaveAssignBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                 <input type="hidden" name="addLeaveAssign"  value="addLeaveAssign">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('leaveAssignAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php }
                    else {
                      ?>
                 
                  <button id="addLeaveAssignBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                 <input type="hidden" name="addLeaveAssign"  value="addLeaveAssign">
                  <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('leaveAssignAdd');"><i class="fa fa-check-square-o"></i> Reset</button>
                  <?php } ?>
                </div>

              </form>
           
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->

    <script type="text/javascript">
       function removePhoto2() {
        $('#facility_photo_2').remove();
        $('#penalty_photo_old_2').val('');
      }

       function removePhoto3() {
        $('#facility_photo_3').remove();
        $('#penalty_photo_old_3').val('');
      }

    </script>