<?php error_reporting(0);

$q = $d->selectRow('*', "society_master", "society_id='$society_id'");
$data = mysqli_fetch_assoc($q);

$blockIdsReport = array();
$floorIdsReport = array();

$blockReportArray = explode(',', $data['work_report_on_ids']);
if ($data['work_report_on']==3) {
  $floorReportArray = explode(',', $data['work_report_on_ids']);

  $dept_ids = implode("','", $floorReportArray);
  //       //$dept_ids = explode("','", $dept_ids);
  $q1 = $d->selectRow('*',"floors_master","floor_id IN ('$dept_ids') AND society_id='$society_id'"); 
  
  while($data1 = mysqli_fetch_array($q1)){
      array_push($blockIdsReport,$data1['block_id']);
  }

  $block_ids = join("','",$blockIdsReport); 

}

$user_latitud = $data['society_latitude'];
$user_longitude = $data['society_longitude'];
$hide_working_hours = $data['hide_working_hours'];
/* print_r($user_latitud);
print_r($user_longitude); */
?>
  <div class="content-wrapper">
    <div class="container-fluid">
     <div class="row pt-2 pb-2">
        <div class="col-md-6">
          <h4 class="page-title">Company Attendance Type</h4>
        </div>
        <div class="col-md-6">
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Hide Working hours in mobile app attendance screen
            <span>
               <?php  
              if($hide_working_hours=="1"){
              ?>
                <input type="checkbox" checked=""  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $hide_working_hours; ?>','ShowWorkingHoursinApp');" data-size="small"/>
                <?php } else { ?>
               <input type="checkbox"  class="js-switch" data-color="#15ca20" onchange ="changeStatus('<?php echo $hide_working_hours; ?>','hideWorkingHoursinApp');" data-size="small"/>
              <?php } ?>
            </span>
          </li>
        </div>
     </div>
      <div class="row">
        <div class="col-lg-12">
            <div class="card">
                    <div class="card-body">
                        <form id="addAttendanceType" action="controller/CompnayAttendanceTypeController.php" enctype="multipart/form-data" method="post">
                            <div class="form-group row">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label "> Company Attendance Type <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                <select name="society_attendace_type" class="form-control single-select"  onchange="ChangeCompnayAttendace(this.value)" id="society_attendace_type">
                                    <option <?php if (isset($data['attendance_type']) && $data['attendance_type'] == 0) {echo "selected";}?>  value="0">None</option>
                                    <option <?php if (isset($data['attendance_type']) && $data['attendance_type'] == 1) {echo "selected";}?>  value="1">Employee App With Geofencing</option>
                                    <option <?php if (isset($data['attendance_type']) && $data['attendance_type'] == 2) {echo "selected";}?>  value="2"> Face Detection App and Employee App With Geofencing</option>
                                   <!--  <option <?php if (isset($data['attendance_type']) && $data['attendance_type'] == 3) {echo "selected";}?>  value="3">Gatekeeper App by Guard and Employee App With Geofencing</option> 
                                    <option <?php if (isset($data['attendance_type']) && $data['attendance_type'] == 4) {echo "selected";}?> value="4">Employee App With Geofencing & Gatekeeper App </option> -->
                                    <option <?php if (isset($data['attendance_type']) && $data['attendance_type'] == 5) {echo "selected";}?> value="5">Only face Detection App</option>
                                </select>
                                </div>

                            </div>
                            <div class="form-group row <?php if ($data['attendance_type'] == 0 || $data['attendance_type'] == 5) {echo "d-none";}?>" id="take_attendance_selfie" >
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label"> Take Attendance Selfie Access <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                  <select name="take_attendance_selfie" class="form-control">
                                    <option <?php if($data['take_attendance_selfie']=='0') {echo "selected";}?> value="0">OFF</option>
                                    <option <?php if($data['take_attendance_selfie']=='1') {echo "selected";}?> value="1">Always</option>
                                    <option <?php if($data['take_attendance_selfie']=='2') {echo "selected";}?> value="2">Out of Range</option>
                                  </select>
                                </div>
                            </div>
                            <div class="form-group row <?php if ($data['attendance_type'] == 0 || $data['attendance_type'] == 5) {echo "d-none";}?>" id="attendance_with_matching_face" >
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label"> Match Selfie with Registered Face (Android Only) <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                  <select name="attendance_with_matching_face" class="form-control">
                                    <option <?php if($data['attendance_with_matching_face']=='0') {echo "selected";}?> value="0">No</option>
                                    <option <?php if($data['attendance_with_matching_face']=='1') {echo "selected";}?> value="1">Yes</option>
                                    
                                  </select>
                                </div>
                            </div>
                            <div class="form-group row <?php if ($data['attendance_type'] == 0 || $data['attendance_type'] == 5) {echo "d-none";}?>" id="branch_geo_fencing" >
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label"> Geofencing Type <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                  <select name="branch_geo_fencing" class="form-control">
                                    <option <?php if($data['branch_geo_fencing']=='0') {echo "selected";}?> value="0">Employee Branch Only</option>
                                    <option <?php if($data['branch_geo_fencing']=='1') {echo "selected";}?> value="1">All Branches</option>
                                   
                                  </select>
                                </div>
                            </div>
                            <div class="form-group row <?php if ($data['attendance_type'] == 0 || $data['attendance_type'] == 5) {echo "d-none";}?>" id="work_report_on" >
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Get Work Report On Punch Out <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                  <select  name="work_report_on" class="form-control work_report_on" onchange="accessReportType(this.value);" >
                                    <option <?php if($data['work_report_on']=='0') {echo "selected";}?> value="0">OFF</option>
                                    <option <?php if($data['work_report_on']=='1') {echo "selected";}?> value="1">All Company Employees</option>
                                    <option <?php if($data['work_report_on']=='2') {echo "selected";}?> value="2">Branch Wise</option>
                                    <option <?php if($data['work_report_on']=='3') {echo "selected";}?> value="3">Department Wise</option>
                                  </select>
                                </div>

                            </div>

                            <div class="row all-access branch-wise <?php if($data['work_report_on']==2){}else{echo "d-none";} ?>">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Work Report Branch<span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" >
                                    <select type="text" required="" id="access_for_id" class="form-control multiple-select taskAccessForBranchMultiSelectCls access_for_id" name="access_for_id[]" multiple>
                                        <option value="0">All Branch</option>
                                        <?php
                                        $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                                        while ($blockData=mysqli_fetch_array($bq)) {
                                        ?>
                                        <option <?php if(isset($blockReportArray) && in_array($blockData['block_id'],$blockReportArray)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div>

                            <div class="form-group row all-access department-wise <?php if($data['work_report_on']==3){}else{echo "d-none";} ?>">
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Work Report Branch <span class="required">*</span></label>
                                <div class="col-lg-8 col-md-8" id="">
                                    <select type="text" required="" class="form-control multiple-select dwblockId taskAccessForBranchMultiSelectCls" multiple name="blockId[]" id="dwblockId" onchange="getTaskAccessForDepartmentWorkReport();">
                                        <!-- <option value="0">All Branch</option> -->
                                        <?php
                                        $bq=$d->select("block_master","society_id='$society_id' $blockAppendQueryOnly");
                                        while ($blockData=mysqli_fetch_array($bq)) {
                                        ?>
                                        <option <?php if(isset($blockIdsReport) && in_array($blockData['block_id'],$blockIdsReport)){ echo "selected"; } ?> value="<?php echo $blockData['block_id'];?>"> <?php echo $blockData['block_name']; ?></option>
                                        <?php }?>
                                    </select>
                                </div>                   
                            </div>
                            <div class="form-group row all-access department-wise <?php if($data['work_report_on']==3){}else{echo "d-none";} ?>">    
                                <label for="input-10" class="col-lg-4 col-md-4 col-form-label">Work Report Department <span class="required">*</span></label>
                                      <div class="col-lg-8 col-md-8" id="">
                                          <select type="text" required="" id="dwfloorId" class="form-control multiple-select dwfloorId taskAccessForDepartmentMultiSelectCls access_for_id" multiple name="access_for_id[]">
                                              <?php 
                                                  $qd=$d->select("block_master,floors_master","floors_master.society_id='$society_id' AND block_master.block_id=floors_master.block_id AND floors_master.block_id IN ('$block_ids') $blockAppendQuery");  
                                                  while ($depaData=mysqli_fetch_array($qd)) {
                                              ?>
                                              <option <?php if(in_array($depaData['floor_id'],$floorReportArray)){ echo "selected"; } ?> value="<?php echo  $depaData['floor_id'];?>" ><?php echo $depaData['floor_name'];?> (<?php echo $depaData['block_name'];?>)</option>
                                              <?php } ?>
                                          </select>
                                      </div>                   
                          </div>

                            <div class="form-footer text-center">
                                <input type="hidden" id="attendance_type_id" name="attendance_type_id" value="" >
                                <button id="addAttendanceTypeBtn" type="submit"  class="btn btn-success hideupdate"><i class="fa fa-check-square-o"></i> Update </button>
                                <input type="hidden" name="UpdateCompanyAttendanceType"  value="UpdateCompanyAttendanceType">
                               <!--  <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addAttendanceType');"><i class="fa fa-check-square-o"></i> Reset</button> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div><!-- End Row-->
        <div class="row blockList px-3 align-items-stretch">
          <?php
              $blockArray = array();
              $q = $d->select("block_master", "society_id='$society_id' $blockAppendQuery", "ORDER BY block_sort ASC");
              while ($data = mysqli_fetch_array($q)) {
              $j = $i++;
              extract($data);
              array_push($blockArray, $data);
              

          ?>
            <div class="col-lg-4 col-6 form-group">
              <?php
                if (isset($_GET['bId']) && filter_var($_GET['bId'], FILTER_VALIDATE_INT) == true) {
              ?>
                <div class="card h-100 ">
                  <div class="card-header">
                    <?php echo $block_name; ?> <?php if($data['block_geofence_range']>0) { echo '('.$data['block_geofence_range'].' Meters)'; } ?>
                  </div>
                  <div class="card-body text-center text-primary">
                    <span >Range :</span>
                    <h2 class="mt-2 text-white"><?php echo $data['block_geofence_range']; ?></h2>
                    <span>Latitude :</span>
                    <h6 class="mt-2 text-white"><?php echo $data['block_geofence_latitude']; ?></h6>
                    <span>Longitude :</span>
                    <h6 class="mt-2 text-white"><?php echo $data['block_geofence_longitude']; ?></h6>
                  </div>
                  <div class="card-foot-btns text-center bg-primary pb-1 pt-2">
                  <button id="" type="button" onclick="updateBlockGeoLocation('<?php echo $data['block_id']; ?>', '<?php echo $user_latitud; ?>','<?php echo $user_longitude; ?>')"   class="btn btn-info btn-sm mb-2"><i class="fa fa-check-square-o"></i><?php if($data['block_geofence_latitude'] !=""){ echo "Update"; } else { echo "Add Range"; }?>  </button>
                   <?php if($data['block_geofence_latitude'] !=""){?>
                    <button id="" type="button" onclick="removeBlockGeoLocation(<?php echo $data['block_id']; ?>)"  class="btn btn-secondary btn-sm mb-2 "><i class="fa fa-check-square-o"></i> Remove </button>
                    <?php } ?>
                  </div>
                </div>
              <?php
              } else {
              ?>
                <div class="card">
                  <div class="card-header bg-primary text-white">
                    <?php echo $block_name; ?> <?php if($data['block_geofence_range']>0) { echo '('.$data['block_geofence_range'].' Meters)'; } ?>
                  </div>
                  <div class="card-body text-center text-primary" style="padding: 0 !important;">
                   <!--  <span>Range :</span>
                    <h5 class="mt-2 text-white"><?php echo $data['block_geofence_range']; ?></h5>
                    <span>Latitude :</span>
                    <h6 class="mt-2 text-white"><?php echo $data['block_geofence_latitude']; ?></h6>
                    <span>Longitude :</span>
                    <h6 class="mt-2 text-white"><?php echo $data['block_geofence_longitude']; ?></h6> -->
                            <!-- <input id="searchInput_<?php echo $data['block_id']; ?>" name="location_name" class="form-control" type="text" placeholder="Enter location" > -->
                            <div data-lat="<?php echo $data['block_geofence_latitude'];?>" data-lng="<?php echo $data['block_geofence_longitude'];?>"
                            data-rng="<?php echo $data['block_geofence_range'];?>"
                            class="map <?php if($data['block_geofence_latitude'] !="" && $data['block_geofence_longitude'] !="" && $data['block_geofence_range'] !=""){ echo "mapCLass"; } ?> "   id="map_<?php echo $data['block_id']; ?>" style="width: 100%; height: 230px;"></div>
                        
                  </div>
                  <div class="card-footer text-center">
                    <button id="" type="button" onclick="updateBlockGeoLocation('<?php echo $data['block_id']; ?>', '<?php echo $user_latitud; ?>','<?php echo $user_longitude; ?>')"   class="btn btn-info btn-sm "><i class="fa fa-check-square-o"></i><?php if($data['block_geofence_latitude'] !=""){ echo "Update Range"; } else { echo "Add Range"; }?>  </button>
                    <?php if($data['block_geofence_latitude'] !=""){?>
                    <button id="" type="button" onclick="removeBlockGeoLocation(<?php echo $data['block_id']; ?>)"  class="btn btn-danger btn-sm "><i class="fa fa-check-square-o"></i> Remove </button>
                    <?php } ?>              </div>
                </div>
              <?php
                  }
                ?>
          </div>
        <?php
        
    }
?>
</div>
    <!-- End container-fluid-->
    
    </div>
  <div class="content-wrapper blockList empList">
      <div class="container-fluid">
        <div class="row pt-2 pb-2">
            <div class="col-md-9">
              <h4 class="page-title">Employee Attendance Geo Facing</h4>
            </div>
            <div class="col-md-3">
              <a href="addUserGeoRange">
                <button class="btn btn-primary btn-sm waves-effect waves-light float-right" data-toggle="modal" data-target="#UserRangeModal">Add User Geo Facing</button>
              </a>
            </div>
        </div>
      </div>
      <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered">
                            <thead>
                                <tr>
                                    <!-- <th>#</th> -->
                                    <th>Sr.No</th>
                                    <th>User Name</th>
                                    <th>Department</th>
                                    <th>Range</th>
                                    <th>Work Location</th>
                                    <th>Address</th>
                                    <th>Action </th>
                                </tr>
                            </thead>
                            <tbody id="showFilterData">
                                <?php
                                $q = $d->selectRow('user_geofence.*,users_master.user_full_name,users_master.user_id,users_master.floor_id,users_master.block_id,floors_master.floor_name',"user_geofence,users_master,block_master,floors_master", "users_master.floor_id = floors_master.floor_id AND users_master.user_id = user_geofence.user_id AND block_master.block_id=floors_master.block_id AND users_master.society_id='$society_id' AND users_master.delete_status= 0 AND user_geofence.user_geofence_longitude !='' $blockAppendQueryUser");
                                $counter = 1;
                                while ($data = mysqli_fetch_array($q)) {
                                ?>
                                    <tr>
                                       <!--  <td class="text-center">
                                            <input type="hidden" name="id" id="id" value="<?php echo $data['geo_fance_id']; ?>">
                                            <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['geo_fance_id']; ?>">
                                        </td> -->
                                        <td><?php echo $counter++; ?></td>
                                        <td><?php echo $data['user_full_name']; ?></td>
                                        <td><?php echo $data['floor_name']; ?></td>
                                        <td><?php echo $data['user_geofence_range']; ?></td>
                                        <td><?php echo $data['work_location_name']; ?></td>
                                        <td><?php custom_echo($data['user_geo_address'],50); ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                             <a href="addUserGeoRange?gId=<?php echo $data['geo_fance_id']; ?>"><button  class="btn btn-sm btn-primary mr-1"  ><i class="fa fa-pencil"></i></button></a>
                                              <button class="btn btn-sm btn-secondary mr-1 pd-1" onclick="removeUserGeoLocation(<?php echo $data['geo_fance_id']; ?>)"><i class="fa fa-delete"></i>Remove</button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                       </table>
                    </div>
                </div>
            </div>
        </div>
  </div>

<div class="modal fade" id="blockModal">
  <div class="modal-dialog " >
    <div class="modal-content border-primary">
        <div class="modal-header bg-primary">
            <h5 class="modal-title text-white">Range Modal</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="attendanceDatas" style="align-content: center;">
        <form id="updateBloCkGeoFrm" action="controller/updateBLockController.php" enctype="multipart/form-data" method="post">
          <div class="form-group row">
            <input type="hidden" name="block_id" id="block_id">
              <label for="input-10" class="col-lg-4 col-md-4 col-form-label"> Range(Meter) <span class="required">*</span></label>
              <div class="col-lg-8 col-md-8" id="">
                        <?php if (isset($_GET['uId']) && $_GET['uId']>0) 
                              {
                                $qd = $d->selectRow("*",'users_master',"user_id=".$_GET['uId']."" );
                                $ddata = mysqli_fetch_assoc($qd);
                                $rangeValue= $ddata['user_geofence_range'];
                              }
                              else
                              {
                                if (isset($_GET['bId']) && $_GET['bId']>0) 
                                {
                                  
                                  $qd = $d->selectRow("*",'block_master',"block_id=".$_GET['bId']."" );
                                  $ddata = mysqli_fetch_assoc($qd);
                                //  print_r($ddata);
                                  $rangeValue= $ddata['block_geofence_range'];
                                }
                                
                              }?>
                        <input autocomplete="off"  type ="text" value="<?php echo $rangeValue; ?>"   min="5" id="block_geofence_range" onkeyup="chnageBLockRange()"  name="block_geofence_range" class="form-control onlyNumber">
                        <input  type="hidden" id="textInput" value="<?php echo $rangeValue; ?>">
                      </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-8 col-md-8" id="">
                          <input type ="hidden"  readonly id="block_latitude" name="block_latitude" class="form-control" value="<?php echo $user_latitud; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                          <div class="col-lg-8 col-md-8" id="">
                            <input type ="hidden"    readonly id="block_longitude" name="block_longitude" class="form-control" value="<?php echo $user_longitude; ?>">
                          </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <input id="searchInput5" name="location_name" class="form-control" type="text" placeholder="Enter location" >
                            <div class="map" id="map" style="width: 100%; height: 300px;"></div>
                        </div>
                    </div>
                    <input type="hidden" name="updateBLock" value="updateBLock">
                    <div class=" text-center ">
                      <button id="" type="submit"   class="btn btn-secondary btn-sm updateBloCkGeo "><i class="fa fa-check-square-o"></i> Update </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
  </div>
</div>


<input type="hidden" name="zoomValue" id="zoomValue" value="17">
<input type="hidden" name="zoomValueBlk" id="zoomValueBlk" value="17">
<script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
    function accessReportType(value) {
    $('.multiple-select').val('').trigger('change');
    if(value == "2"){
      $('.blockId').val('').trigger('change');
      $('.all-access').addClass('d-none');
      $('.branch-wise').removeClass('d-none');
    }
    else if(value == "3"){
      $('.blockId').val('');
      $('.all-access').addClass('d-none');
      $('.department-wise').removeClass('d-none');
    }
    else if(value == "4"){
      $('.blockId').val('');
      $('.all-access').addClass('d-none');
      $('.employee-wise').removeClass('d-none');
    }
        else{
      $('.blockId').val('');
      $('.all-access').addClass('d-none');
    }

        $('#dwblockId').rules("add", {
                required: true,
                messages: {
                    required: "Please select ",
                }
            });
        $('#ewblockId').rules("add", {
                required: true,
                messages: {
                    required: "Please select ",
                }
            });
        $('#access_for_id').rules("add", {
                required: true,
                messages: {
                    required: "Please select ",
                }
            });
        $('#dwfloorId').rules("add", {
                required: true,
                messages: {
                    required: "Please select ",
                }
            });
    }

    
</script>  

<script src="assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="assets/js/select.js"></script>

<script src="assets/js/custom17.js"></script>


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>
<script type="text/javascript">

function openModalForAdd()
{
  $('.userDiv').show();
  $('#user_geofence_range').val("");
     $('.blockDiv').show();
}

 // $('.single-select').select2();
var society_attendace_type = $('#society_attendace_type').val();
if(society_attendace_type!=0 || society_attendace_type!=5)
{
  $('.blockList').show();
}
else{
  $('.blockList').hide();

}
if(society_attendace_type==2)
{
  $('.empList').show();
  $('#is_app_attendance_on').val(1);
}

function chnageBLockRange()
{
  var latitude = $('#block_latitude').val();
    var longitude = $('#block_longitude').val();
    initialize(latitude,longitude);
}
<?php if (isset($_GET['bId']) && $_GET['bId'] != "") {?>
  $('.locationRangeDiv').css('display','block');
  $('#block_id').prop('disabled', true);
<?php }
?>
<?php if (isset($_GET['uId']) && $_GET['uId'] != "") {?>

  $('.UserlocationRangeDiv').css('display','block');
  $('.branchDiv').css('display','none');
  $('.locationRangeDiv').css('display','block');
  getCompnayAttendaceUser(<?php echo $_GET['dId']; ?>);
<?php }
?>
$().ready(function()
  {
    var latitude = '<?php echo $user_latitud; ?>';
    var longitude = '<?php echo $user_longitude; ?>';
    initialize(latitude,longitude);
  });
  function initialize(d_lat,d_long)
  {
    var latlng = new google.maps.LatLng(d_lat,d_long);
    var latitute = 23.037786;
    var longitute = 72.512043;
    zoomVal = $('#zoomValueBlk').val();
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: parseInt(zoomVal)
    });
    
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
    });
    mapzoom=map.getZoom();
    /////////////////on zoom in out
    google.maps.event.addListener(map, 'zoom_changed', function() {

      var zoomLevel = map.getZoom();
      
      $('#zoomValueBlk').val(zoomLevel);

      mapzoom=map.getZoom(); //to stored the current zoom level of the map

      });
    block_geofence_range =$('#block_geofence_range').val();
    if(block_geofence_range =="")
    {
      block_geofence_range=10;
    }
      // Add circle overlay and bind to marker
      var circle = new google.maps.Circle({
        map: map,
        radius: parseInt(block_geofence_range),    // 10 miles in metres
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker, 'position');

    var parkingRadition = 5;
    var citymap = {
        newyork: {
          center: {lat: latitute, lng: longitute},
          population: parkingRadition
        }
    };

    var input = document.getElementById('searchInput5');
    var geocoder = new google.maps.Geocoder();
    var autocomplete10 = new google.maps.places.Autocomplete(input);
    autocomplete10.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    autocomplete10.addListener('place_changed', function()
    {
      infowindow.close();
      marker.setVisible(false);
      var place5 = autocomplete10.getPlace();
      if (!place5.geometry) {
          window.alert("Autocomplete's returned place5 contains no geometry");
          return;
      }

      // If the place5 has a geometry, then present it on a map.
      if (place5.geometry.viewport) {
          map.fitBounds(place5.geometry.viewport);
      } else {
          map.setCenter(place5.geometry.location);
          map.setZoom(17);
      }
      marker.setPosition(place5.geometry.location);
      marker.setVisible(true);
      var pincode="";
      for (var i = 0; i < place5.address_components.length; i++) {
        for (var j = 0; j < place5.address_components[i].types.length; j++) {
          if (place5.address_components[i].types[j] == "postal_code") {
            pincode = place5.address_components[i].long_name;
          }
        }
      }
      bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
      infowindow.setContent(place5.formatted_address);
      infowindow.open(map, marker);

    });
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function()
    {
      geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
      {
        if (status == google.maps.GeocoderStatus.OK)
        {
          if (results[0])
          {
           
            var places = results[0];
            var pincode="";
            var serviceable_area_locality= places.address_components[4].long_name;
            for (var i = 0; i < places.address_components.length; i++)
            {
              for (var j = 0; j < places.address_components[i].types.length; j++)
              {
                if (places.address_components[i].types[j] == "postal_code")
                {
                  pincode = places.address_components[i].long_name;
                }
              }
            }
            bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
          }
        }
      });
    });
  }

  function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality)
  {
   
    document.getElementById('block_latitude').value = lat;
    document.getElementById('block_longitude').value = lng;
  }

  ///////////////////////////////////user map
  function chnageUserRange()
  {
      var latitude = $('#user_latitude').val();
      var longitude = $('#user_longitude').val();
      initialize2(latitude,longitude);
  }
  $('#blockModal').on('hidden.bs.modal', function () {
   
    var latitude = '<?php echo $user_latitud; ?>';
    var longitude = '<?php echo $user_longitude; ?>';

    initialize2(latitude,longitude);
})
 
/////loop map
//var countCLass =  $('.mapCLass').length;
var countCLass = $('.mapCLass').map(function() {
            id = this.id;
            lat = $(this).attr('data-lat');
            lng = $(this).attr('data-lng');
            rng = $(this).attr('data-rng');
            var latlng = new google.maps.LatLng(lat,lng);
            var latitute = 23.037786;
            var longitute = 72.512043;
            zoomVal = $('#zoomValueBlk').val();
            var map = new google.maps.Map(document.getElementById(id), {
              center: latlng,
              zoom: parseInt(zoomVal)
            });
            
            var marker = new google.maps.Marker({
              map: map,
              position: latlng,
              draggable: false,
              anchorPoint: new google.maps.Point(0, -29)
            });
            var circle = new google.maps.Circle({
              map: map,
              radius: parseInt(rng),    // 10 miles in metres
              fillColor: '#AA0000'
            });
            circle.bindTo('center', marker, 'position');
           
        });


  /////////////////////////////////////

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }

</script>
<style>
    .locationRangeDiv{
        display:none;
    }
    .UserlocationRangeDiv{
      display:none;

    }
   
    </style>
