 <?php 
    if(isset($_POST['emp_type_id_edit'])) {
    $btnName="Update";
    extract(array_map("test_input" , $_POST));
    $q=$d->select("emp_type_master","emp_type_id='$emp_type_id_edit'");
    $data=mysqli_fetch_array($q);
    } else {
    $btnName="Add";
    }
     ?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
          <h4 class="page-title"> <?php echo $xml->string->employee; ?> Type</h4>
        </div>
     </div>
    <!-- End Breadcrumb-->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form  id="empTypeAdd" action="controller/employeeController.php" method="post" enctype="multipart/form-data">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                    <?php echo $xml->string->employee; ?> Type
                </h4>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-3 col-form-label"><?php echo $xml->string->employee; ?> Type  <span class="text-danger">*</span></label>
                  <div class="col-sm-9">
                    <?php //IS_1284 alphanumeric ?>
                     <?php if(isset($_POST['emp_type_id_edit'])) { ?>
                      <input maxlength="30" required="" required="" type="text" class="form-control text-capitalize alphanumeric" id="input-10" name="employee_type_name_edit" value="<?php echo $data['emp_type_name']; ?>" >
                      
                      <input value="<?php echo $data['emp_type_icon']; ?>"  type="hidden" class="form-control" id="input-15" name="emp_type_icon_old">
                    <?php } else { ?>
                    <input maxlength="30" required="" required="" type="text" class="form-control text-capitalize alphanumeric" id="input-10" name="employee_type_name_add">
                    <?php } ?>
                  </div>
                  <input type="hidden" id="emp_type_id_edit" name="emp_type_id_edit" value="<?php if(isset($_POST['emp_type_id_edit'])){ echo $data['emp_type_id']; }else{ echo "0";} ?>">
                </div>
                <div class="form-group row">
                  <label for="input-15" class="col-sm-3 col-form-label">Type Icon <span class="text-danger">*</span></label>
                  <div class="col-sm-9">
                      <input accept="image/*" required="" type="file" class="form-control-file border photoOnly" id="input-15" name="emp_type_icon">
                  </div>
                </div>
               <div class="form-footer text-center">
                    <button  type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>
              </form>
            </div>
            <?php if(!isset($_POST['emp_type_id_edit'])) { ?>
             <div class="card-body">
              <form  id="empTypeAdd" action="controller/employeeController.php" method="post" enctype="multipart/form-data">
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-address-book-o"></i>
                     Add From Common Type 
                </h4>
                <?php 
                $soEmpType = array();
                $q11=$d->select("emp_type_master","society_id='$society_id' ");
               while ($row=mysqli_fetch_array($q11)) {
                    array_push($soEmpType, $row['emp_type_name']);
                 }
                 // print_r($soEmpType);
                ?>
                <div class="form-group row">
                  <label for="input-10" class="col-sm-3 col-form-label"><?php echo $xml->string->employee; ?> Types </label>
                  <div class="col-sm-9">
                     <ul style="list-style: none;">
                    <?php
                      $q=$d->select("emp_type_common","");
                      $i1=1;
                        while ($row=mysqli_fetch_array($q)) {
                          extract($row);
                    ?>    
                      <li><label><?php if(!in_array($emp_type_name, $soEmpType)){ ?><input  class="emp_type_id" value="<?php echo $emp_type_id; ?>" id="unit_id<?php echo $i1++; ?>"  checked="" type="checkbox" name="emp_type_id[]"> <?php } ?> <img src="../img/emp_icon/<?php echo $emp_type_icon; ?>" alt="">  <?php echo $emp_type_name; ?>   <?php if(in_array($emp_type_name, $soEmpType)){ echo "(Already Added)"; } ?>
                      </label></li> 
                  <?php  } ?>
                  </ul>
                  </div>
                </div>
                <div class="form-footer text-center">
                    <input type="hidden" name="addBulk" value="addBulk">
                    <button  type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->add; ?></button>
                </div>
              </form>
            </div>
          <?php  }?>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->