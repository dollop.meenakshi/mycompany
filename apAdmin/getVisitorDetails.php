<?php 
// ini_set('session.cache_limiter','public');
// session_cache_limiter(false);
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

if (isset($_COOKIE['bms_admin_id'])) {
    $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
$society_id = $_COOKIE['society_id'];
}

$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);


extract(array_map("test_input" , $_POST));








//IS_605
if(isset($visitor_main_type_id)  ){

  $q=$d->select("visitorMainType","visitor_main_type_id='$visitor_main_type_id' and  active_status = 0 ");
$row=mysqli_fetch_array($q);
extract($row);
 ?>

 
 <form id="editMainVisitorFrm" action="controller/addMainTypeVisitorsController.php" method="post" enctype="multipart/form-data">
   <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>">
              <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Visitor Type Name <span class="required">*</span></label>
                <div class="col-sm-8" id="PaybleAmount">
                  <input type="hidden" name="visitor_main_type_id" id="visitor_main_type_id" value="<?php echo $visitor_main_type_id; ?>">
                  <input type="text" class="form-control" name="main_type_name" id="main_type_name" value="<?php echo $main_type_name; ?>">
                </div>
              </div>

               <div class="form-group row">
                <label for="visitor_type" class="col-sm-4 col-form-label">Visitor Type <span class="required">*</span></label>
                <div class="col-sm-8" id="">
                 
                  <input type="text" class="form-control onlyNumber" inputmode="numeric" name="visitor_type" id="visitor_type" value="<?php echo $visitor_type; ?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Main image</label>
                <div class="col-sm-6">
                  <input type="hidden" name="main_type_image_old" value="<?php echo $main_type_image;?>">
                  <input type="file" class="form-control-file photoOnly" name="main_type_image" id="main_type_image" accept="image/*">
                </div>
                <div class="col-sm-2"><?php echo '<img src="../img/visitor_company/'.$main_type_image.'" height="30px" width="30px">'; ?></div>
              </div>


              <div class="form-group row">
                <label for="input-10" class="col-sm-4 col-form-label">Full image</label>
                <div class="col-sm-6">
                  <input type="hidden" name="visitor_main_full_img_old" id="visitor_main_full_img_old" value="<?php echo $visitor_main_full_img; ?>">
                  <input type="file" class="form-control-file photoOnly" name="visitor_main_full_img"  accept="image/*">
                </div>
                <div class="col-sm-2">
                  <?php echo '<img src="../img/visitor_company/'.$visitor_main_full_img.'" height="30px" width="30px">'; ?>
                </div>
              </div>
              <div class="form-footer text-center">
                <button type="submit" value="updateData" name="updateData" class="btn btn-success"><i class="fa fa-check-square-o"></i>Update</button>
              </div>
            </form>
              <script src="assets/js/jquery.min.js"></script>
              <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
 <script type="text/javascript">
   //12march2020

    jQuery(document).ready(function($){
$.validator.addMethod('filesize4MB', function (value, element, arg) {
        var size =4000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 4MB."));

 $.validator.addMethod("noSpace", function(value, element) { 
   return value == '' || value.trim().length != 0;  
}, "No space please and don't leave it empty");

$(".onlyNumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

 
$("#editMainVisitorFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            main_type_name:
            {
                required: true,
                noSpace: true 
            },
            visitor_type:{
                required: true,
                noSpace: true 
            },
            main_type_image:
            {
                required: false,
                filesize4MB: true 
            },
             visitor_main_full_img:
            {
                required: false,
                filesize4MB: true 
            },
        },
        messages: {
            main_type_name: {
                required: "Please enter visitor type name", 
       },
         visitor_type:{
        required: "Please enter visitor type", 
       }
      }
});
});
//12march2020
 </script>
        
<?php }   else if($visitor_sub_type_id) {

  $q=$d->select("visitorSubType","visitor_sub_type_id='$visitor_sub_type_id' and  active_status = 0 ");
$row=mysqli_fetch_array($q);
extract($row);
 ?>

 
   <form id="editvisitorSubTypeFrm" action="controller/addMainTypeVisitorsController.php" method="post" enctype="multipart/form-data">
               <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>">
 <input type="hidden" name="visitor_sub_type_id" value="<?php echo $visitor_sub_type_id; ?>">
                  <div class="form-group row mt-3">
        <label for="visitor_main_type_id" class="col-sm-4 col-form-label">Main Type</label>
        <div class="col-sm-8">
           
          <select class="form-control" name="visitor_main_type_id" id="visitor_main_type_id">
                        <option value="">Select Main Type</option>
                          <?php 
                         
                            $data=$d->select("visitorMainType"," active_status =0  ","");
                            while ($row=mysqli_fetch_array($data)) { ?>
                        <option <?php if($visitor_main_type_id==$row['visitor_main_type_id']) { echo "selected";}?> value="<?php echo $row['visitor_main_type_id']; ?>"><?php echo $row['main_type_name'  ]; ?></option>
                          <?php }  ?>
                      </select>
           
         
       </div>
     </div>


                    
                
          <div class="form-group row mt-3">
              <label for="visitor_sub_type_name" class="col-sm-4 col-form-label"> Sub Type Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control " name="visitor_sub_type_name" id="visitor_sub_type_name" value="<?php echo $visitor_sub_type_name;?>">
            </div>
          </div>
          <div class="form-group row">
              <label for="visitor_sub_image" class="col-sm-4 col-form-label">Image</label>
            <div class="col-sm-8">
              <input type="hidden"   class="form-control-file" name="visitor_sub_image_old"  value="<?php echo $visitor_sub_image;?>">
              <input type="file" required="" class="form-control-file" name="visitor_sub_image" id="visitor_sub_image">
            </div>
          </div>
          <div class="form-footer text-center">
              <button type="submit" name="editsubvisitor" value="Edit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Edit</button>
          </div>
     </form>
              <script src="assets/js/jquery.min.js"></script>
              <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>

 
 


 <script type="text/javascript">
   //12march2020

    jQuery(document).ready(function($){
$.validator.addMethod('filesize4MB', function (value, element, arg) {
        var size =4000000;
        if(element.files.length){
            if(element.files[0].size<=size)
            {
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }, $.validator.format("file size must be less than or equal to 4MB."));

 $.validator.addMethod("noSpace", function(value, element) { 
   return value == '' || value.trim().length != 0;  
}, "No space please and don't leave it empty");

$(".onlyNumber").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

 $("#editvisitorSubTypeFrm").validate({
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) { 
            error.insertAfter(element.parent());      // radio/checkbox?
            } else if (element.hasClass('select2-hidden-accessible')) {     
                error.insertAfter(element.next('span'));  // select2
                element.next('span').addClass('error').removeClass('valid');
            } else {                                      
                error.insertAfter(element);               // default
            }
        },
        rules: {
            visitor_main_type_id:
            {
                required: true,
                noSpace: true 
            },
            visitor_sub_type_name:{
                required: true,
                noSpace: true 
            },
            visitor_sub_image:
            {
                required: false,
                filesize4MB: true 
            } 
        },
        messages: {
            visitor_main_type_id: {
                required: "Please select main type", 
       },
       visitor_sub_type_name:{
        required: "Please enter sub type name", 
       },
       visitor_sub_image: {
                required: "Please select image", 
       } 
      }
});
 
});
//12march2020
 </script>
        
<?php 
} ?>