<?php
if(isset($_POST['route_id']) && !empty($_POST) && isset($_POST['routeDetails']))
{
    $route_id = $_POST['route_id'];
    $q = $d->selectRow("rm.*","route_master AS rm","rm.route_id = '$route_id'");
    $data = $q->fetch_assoc();

    $retailer_arr = [];
    $q1 = $d->selectRow("rm.retailer_code,rrm.route_retailer_order_by,rm.retailer_name,rm.retailer_id,rm.retailer_latitude,rm.retailer_longitude,rm.retailer_address,rm.retailer_contact_person,rm.retailer_contact_person_number,rm.retailer_contact_person_country_code","route_retailer_master AS rrm JOIN retailer_master AS rm ON rm.retailer_id = rrm.retailer_id","rrm.route_id = '$route_id'","ORDER BY rrm.route_retailer_order_by ASC");
    while($data1 = $q1->fetch_assoc())
    {
        $retailer_arr[] = $data1;
    }

    $user_arr = [];
    $q2 = $d->selectRow("ream.route_assign_week_days,ream.route_assign_date,um.user_full_name,bam.admin_name,bm.block_name,fm.floor_name","route_employee_assign_master AS ream JOIN users_master AS um ON um.user_id = ream.user_id JOIN block_master AS bm ON bm.block_id = um.block_id JOIN floors_master AS fm ON fm.floor_id = um.floor_id LEFT JOIN bms_admin_master AS bam ON bam.admin_id = ream.route_assign_by","ream.route_id = '$route_id'");
    while($data2 = $q2->fetch_assoc())
    {
        $user_arr[] = $data2;
    }
    $week_arr = [
        0 => 'Sunday',
        1 => 'Monday',
        2 => 'Tuesday',
        3 => 'Wednesday',
        4 => 'Thursday',
        5 => 'Friday',
        6 => 'Saturday',
    ];
}
else
{
    $_SESSION['msg1'] = "Invalid Request!";
?>
<script type="text/javascript">
    window.location = "manageRoute";
</script>
<?php
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-10 col-6">
                <h4 class="page-title">Route Details</h4>
            </div>
            <div class="col-sm-2 col-6 text-right">
                <form action="manageRoute" method="post">
                    <?php
                    if(isset($_POST['country_id']) && !empty($_POST['country_id']))
                    {
                        $country_id = $_POST['country_id'];
                    ?>
                    <input type="hidden" name="country_id" value="<?php echo $country_id; ?>"/>
                    <?php
                    }
                    if(isset($_POST['state_id']) && !empty($_POST['state_id']))
                    {
                        $state_id = $_POST['state_id'];
                    ?>
                    <input type="hidden" name="state_id" value="<?php echo $state_id; ?>"/>
                    <?php
                    }
                    if(isset($_POST['city_id']) && !empty($_POST['city_id']))
                    {
                        $city_id = $_POST['city_id'];
                    ?>
                    <input type="hidden" name="city_id" value="<?php echo $city_id; ?>"/>
                    <?php
                    }
                    ?>
                    <button type="submit" class="btn btn-sm btn-primary">Back</button>
                </form>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card border-dark mb-3">
                            <div class="card-header">Route Name : <?php echo $data['route_name']; ?></div>
                        </div>

                        <div class="card border-dark mb-3">
                            <div class="card-header">Assigned Retailers</div>
                            <div class="card-body text-dark">
                            <?php
                            foreach($retailer_arr AS $key => $value)
                            {
                            ?>
                                <span class="badge badge-primary"><?php echo $value['retailer_name']; ?></span>
                            <?php
                            }
                            ?>
                            </div>
                        </div>

                        <div class="card border-dark mb-3">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-sm-11">
                                        Assigned Employees
                                    </div>
                                    <div class="col-sm-1 text-right">
                                        <form action="addEmployeeRoute" method="post" id="addRouteBtnForm">
                                            <input type="hidden" name="route_id" value="<?php echo $route_id; ?>">
                                            <input type="hidden" name="addEmployeeRouteBtn" value="addEmployeeRouteBtn">
                                            <button type="submit" class="btn btn-primary btn-sm waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body text-dark">
                                <div class="table-responsive">
                                    <table id="example" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>User Name</th>
                                                <th>Assign Date-Time</th>
                                                <th>Week Days</th>
                                                <th>Assign By</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $iNo = 1;
                                            foreach($user_arr AS $key1 => $value1)
                                            {
                                                $route_assign_week_days_arr = explode(",",$value1['route_assign_week_days']);
                                            ?>
                                            <tr>
                                                <td><?php echo $iNo++; ?></td>
                                                <td><?php echo $value1['user_full_name'] . " (" . $value1['block_name'] . "-" . $value1['floor_name'] . ")"; ?></td>
                                                <td><?php echo $value1['route_assign_date']; ?></td>
                                                <td>
                                                <?php
                                                    $wd = "";
                                                    foreach($route_assign_week_days_arr AS $key => $value)
                                                    {
                                                        if(isset($week_arr[$value]))
                                                        {
                                                            $wd .= $week_arr[$value].",";
                                                        }
                                                    }
                                                    echo rtrim($wd, ",");
                                                ?>
                                                </td>
                                                <td><?php echo $value1['admin_name']; ?></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card border-dark mb-3">
                            <div class="card-header">Retailers Location</div>
                            <div class="card-body text-dark">
                            <div class="map" id="map2" style="width: 100%; height: 300px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $d->map_key(); ?>"></script>
<script>
function initMap()
{
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };

    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("map2"), mapOptions);
    map.setTilt(50);

    // Multiple markers location, latitude, and longitude
    var markers = [
        <?php
        if(count($retailer_arr) > 0)
        {
            $cnt = 1;
            $cnt_2 = 1;
            foreach($retailer_arr AS $key2 => $value2)
            {
                if($value2['retailer_latitude'] != "" && !empty($value2['retailer_latitude']) && $value2['retailer_longitude'] != "" && !empty($value2['retailer_longitude']))
                {
                    echo '["'.$value2['retailer_name'].'", '.$value2['retailer_latitude'].', '.$value2['retailer_longitude'].'],';
                    $cnt_2++;
                }
                else
                {
                    if($cnt == 1 && $cnt_2 == 1)
                    {
                        echo '["MyCo",23.037786,72.512043]';
                    }
                    $cnt++;
                }
            }
        }
        else
        {
            echo '["MyCo",23.037786,72.512043]';
        }
        ?>
    ];

    // Info window content
    var infoWindowContent = [
        <?php
        if(count($retailer_arr) > 0)
        {
            $cnt1 = 1;
            $cnt2 = 1;
            foreach($retailer_arr AS $key3 => $value3)
            {
                if($value3['retailer_latitude'] != "" && !empty($value3['retailer_latitude']) && $value3['retailer_longitude'] != "" && !empty($value3['retailer_longitude']))
                {
            ?>
                ['<div class="info_content">' +
                '<h6><?php echo $value3['retailer_name']; ?></h6>' +
                '<p><?php echo $value3['retailer_address']; ?></p>' + '<p><?php echo $value3['retailer_contact_person']; ?> :' + ' <?php echo $value3['retailer_contact_person_country_code'] . $value3['retailer_contact_person_number'] ; ?></p>' + '</div>'],
                <?php
                $cnt2++;
                }
                else
                {
                    if($cnt1 == 1 && $cnt2 == 1)
                    {
                    ?>
                        ['<div class="info_content">' +'<h6>MyCo</h6>' +'<p>101,1st Floor, Parshwa Tower,, Above Kotak Mahindra Bank, SG Highway, Bodakdev, near Pakwan II, Ahmedabad, Gujarat 380054</p></div>'],
                    <?php
                    }
                    $cnt1++;
                }
            }
        }
        else
        {
            echo '["MyCo",23.037786,72.512043]';
        }
        ?>
    ];

    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Place each marker on the map
    for( i = 0; i < markers.length; i++ )
    {
        var position = new google.maps.LatLng(parseFloat(markers[i][1]),parseFloat(markers[i][2]));
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });

        // Add info window to marker
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event)
    {
        this.setZoom(12);
        google.maps.event.removeListener(boundsListener);
    });
}

// Load initialize function
google.maps.event.addDomListener(window, 'load', initMap);
</script>