<?php
error_reporting(0);
$dId  = (int)$_GET['dId'];
$bId  = (int)$_GET['bId'];
$week_days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-3 col-md-6 col-6">
                <h4 class="page-title">Manage Shift</h4>
            </div>
            <div class="col-sm-3 col-md-6 col-6">
                <div class="btn-group float-sm-right">
                    <a href="addShift" class="btn btn-sm btn-primary waves-effect waves-light mr-1"><i class="fa fa-plus mr-1"></i> Add </a>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteShiftTiming');" class="btn  btn-sm btn-danger pull-right mr-1" ><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sr.No</th>
                                        <th>Action</th>
                                        <th>Shift Code</th>
                                        <th>Shift Name</th>
                                        <th>Employees</th>
                                        <th>Week Off </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $i = 1;
                                    $q = $d->selectRow("COUNT(distinct um.user_id) AS total_users,stm.shift_time_id,stm.shift_name,stm.week_off_days,stm.has_altenate_week_off,stm.maximum_in_out,stm.late_in_reason,stm.early_out_reason,stm.is_multiple_punch_in,stm.take_out_of_range_reason","shift_timing_master AS stm LEFT JOIN users_master AS um ON um.shift_time_id = stm.shift_time_id AND um.delete_status = 0 JOIN shift_day_master AS sdm ON sdm.shift_time_id = stm.shift_time_id","stm.society_id = '$society_id' AND stm.is_deleted = 0 ","GROUP BY stm.shift_time_id ORDER BY stm.shift_name ASC");
                                    $counter = 1;
                                    $arData = array();
                                    $wod = [];
                                    while ($data = $q->fetch_assoc())
                                    {
                                        extract($data);
                                        $wod = explode(",",$week_off_days);
                                        $var = "";
                                        foreach($wod AS $key => $value)
                                        {
                                            $var .= $week_days[$value].",";
                                        }
                                    ?>
                                    <tr>
                                        <td class="text-center">
                                            <input <?php if($total_users > 0) { echo 'disabled'; } ?> type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $shift_time_id; ?>">
                                        </td>
                                        <td><?php echo $counter++; ?></td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <form action="addShift" method="post" accept-charset="utf-8" class="mr-2">
                                                    <input type="hidden" name="shift_time_id" value="<?php echo $shift_time_id; ?>">
                                                    <input type="hidden" name="edit_shift_timing" value="edit_shift_timing">
                                                    <button type="submit" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i></button>
                                                </form>
                                                <form action="shiftDayDetails" method="get" accept-charset="utf-8" class="mr-2">
                                                    <input type="hidden" name="sti" value="<?php echo $shift_time_id; ?>">
                                                    <input type="hidden" name="edit_shift_timing" value="edit_shift_timing">
                                                    <button type="submit" title="Shift Details" class="btn btn-sm btn-primary"> <i class="fa fa-eye"></i></button>
                                                </form>
                                                <!-- <button type="submit" class="btn mr-1 btn-sm btn-primary" onclick="shiftTimingModalNew(<?php echo $shift_time_id; ?>,'<?php echo $shift_name; ?>')"> <i class="fa fa-eye"></i></button> -->
                                                <!-- <a href="addShiftTiming?shftId=<?php echo $shift_time_id; ?>" class="btn  btn-sm btn-warning"><i class="fa fa-clone" aria-hidden="true"></i></a> -->
                                            </div>
                                        </td>
                                        <td><?php echo 'S'.$shift_time_id; ?></td>
                                        <td><?php echo $shift_name; ?></td>
                                        <td>
                                            <a href="userShift?sId=<?php echo $shift_time_id; ?>"><?php if($total_users > 0){ echo $total_users; }else{ echo 0; }?></a>
                                        </td>
                                        <td><?php echo rtrim($var,","); ?></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Row-->
    </div><!-- End container-fluid-->
</div>
<div class="modal fade" id="shiftDetails">
    <div class="modal-dialog modal-xl">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Shift Details Of -> <span id="shift_name"></span></h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-2" style="align-content: center;">
                <div class="row mx-0 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Day</th>
                                <th>Shift Start Time</th>
                                <th>Shift End Time</th>
                                <th>Lunch Break Start Time</th>
                                <th>Lunch Break End Time</th>
                                <th>Tea Break Start Time</th>
                                <th>Tea Break End Time</th>
                                <th>Per Day Hour</th>
                                <th>Half Day After Time</th>
                                <th>Half Day Before Time</th>
                                <th>Late In Time Start</th>
                                <th>Early Out Time Start</th>
                                <th>Shift Type</th>
                                <th>Minimum Half Day Hours</th>
                                <th>Minimum Full Day Hours</th>
                                <th>Max Punch Out Time</th>
                                <th>Max Tea Break</th>
                                <th>Max Lunch Break</th>
                                <th>Max Personal Break</th>
                            </tr>
                        </thead>
                        <tbody id="shift_timing_data">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function popitup(url)
    {
        newwindow=window.open(url,'name','height=800,width=900, location=0');
        if (window.focus) {newwindow.focus()}
        return false;
    }

    function shiftTimingModalNew(shift_time_id,shift_name)
    {
        $.ajax({
            url: "controller/ShiftTimingController.php",
            cache: false,
            type: "POST",
            data: {
                getShiftDetailsNew:"getShiftDetailsNew",
                shift_time_id:shift_time_id,
                csrf:csrf
              },
            success: function(response)
            {
                $('#shift_timing_data').empty();
                $('#shift_name').text(shift_name);
                response = JSON.parse(response);
                $.each(response, function (key, val)
                {
                    row = "<tr><td>"+val.shift_day_name+"</td><td>"+val.shift_start_time+"</td><td>"+val.shift_end_time+"</td><td>"+val.lunch_break_start_time+"</td><td>"+val.lunch_break_end_time+"</td><td>"+val.tea_break_start_time+"</td><td>"+val.tea_break_end_time+"</td><td>"+val.per_day_hour+"</td><td>"+val.half_day_time_start+"</td><td>"+val.halfday_before_time+"</td><td>"+val.late_time_start+"</td><td>"+val.early_out_time+"</td><td>"+val.shift_type+"</td><td>"+val.maximum_halfday_hours+"</td><td>"+val.minimum_hours_for_full_day+"</td><td>"+val.max_punch_out_time+"</td><td>"+val.max_tea_break+"</td><td>"+val.max_lunch_break+"</td><td>"+val.max_personal_break+"</td></tr>";
                    $('#shift_timing_data').append(row);
                });
                $('#shiftDetails').modal();
            }
        })
    }
</script>