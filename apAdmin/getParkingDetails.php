<?php 
session_start();
$url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();

if (isset($_COOKIE['bms_admin_id'])) {
  $bms_admin_id = $_COOKIE['bms_admin_id'];
}
if (isset($_COOKIE['society_id'])) {
  $society_id = $_COOKIE['society_id'];
}
$default_time_zone=$d->getTimezone($society_id);
date_default_timezone_set($default_time_zone);
extract(array_map("test_input" , $_POST));
    $bms_admin_id = $_COOKIE['bms_admin_id'];

$aq=$d->select("bms_admin_master","admin_id='$bms_admin_id' ");
$adminData= mysqli_fetch_array($aq);
$access_branchs=$adminData['access_branchs'];
$access_departments=$adminData['access_departments'];
$blockAryAccess = array();
$departmentAry = array();
$blockAryAccess = explode(",", $access_branchs);
$departmentAry = explode(",", $access_departments);

if ($access_branchs!='') {
    $blockids = join("','",$blockAryAccess); 
    if ($access_departments!='') {
      $departmentIds = join("','",$departmentAry); 
      $restictDepartment = " AND floors_master.floor_id IN ('$departmentIds')";
      $restictDepartmentUser = " AND users_master.floor_id IN ('$departmentIds')";
    } else {
      $restictDepartment = "";
      $restictDepartmentUser = "";
    }

    $blockAppendQuery = " AND block_master.block_id IN ('$blockids')";
    $blockAppendQueryUnit = " AND unit_master.block_id IN ('$blockids')";
    $blockAppendQueryFloor = " AND floors_master.block_id IN ('$blockids') $restictDepartment";
    $blockAppendQueryUser = " AND users_master.block_id IN ('$blockids') $restictDepartmentUser";
    $blockAppendQueryOnly = " AND block_id IN ('$blockids')";

}  else {
    $blockAppendQuery = "";
    $blockAppendQueryUnit = "";
    $blockAppendQueryUser = "";
    $blockAppendQueryOnly="";
}
  $q4 = $d->select("parking_master","parking_id='$parking_id'");
$pData = mysqli_fetch_array($q4);
?>
<div class="form-group row">
  <label for="input-10" class="col-sm-4 col-form-label">Employee <span class="text-danger">*</span></label>
  <div class="col-sm-8" id="PaybleAmount">
   <select type="text" class="form-control single-select" onchange="clearNumber()" name="unit_id">
    <option value="">-- Select --</option>
    <?php 
    $q3=$d->select("unit_master,block_master,users_master","users_master.delete_status=0  AND users_master.unit_id=unit_master.unit_id AND block_master.block_id=unit_master.block_id AND unit_master.society_id='$society_id' AND unit_master.unit_status!=0 AND unit_master.unit_status!=4 AND users_master.member_status=0 $blockAppendQueryUser ","");
    while ($blockRow=mysqli_fetch_array($q3)) {
     ?>
     <option <?php if($blockRow['unit_id']==$_POST['unit_id'])  {echo 'selected';}?> value="<?php echo $blockRow['unit_id'];?>"> <?php echo $blockRow['user_full_name'];?> (<?php echo $blockRow['user_designation'];?>)</option>
   <?php }?>
 </select>
</div>
</div>
<div id="extraParkingEdit">

  <div class="form-group row">
    <label for="input-10" class="col-sm-4 col-form-label">Vehicle No. </label>
    <div class="col-sm-8">
      <input maxlength="40" minlength="5" value="<?php echo $pData['vehicle_no'] ?>" type="text" name="vehicle_no" id="vehicle_no" class="form-control text-uppercase vehicle_number11">
    </div>
  </div>

</div>
