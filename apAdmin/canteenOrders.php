<?php error_reporting(0);
  ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
      <div class="row pt-2 pb-2">
        <div class="col-md-12">
          <h4 class="page-title"> Canteen Orders</h4>
        </div>
     </div>
    
      <div class="row pt-2 pb-2">
        <div class="col-sm-12 col-12">
          <ol class="breadcrumb">
            <li>
                <a href ="canteenOrders"><span class="badge badge-pill badge-primary m-1">All (<?php echo $d->count_data_direct("order_id","order_master","society_id='$society_id' AND vendor_category_id=1"); ?>)</span></a>
                <a href ="canteenOrders?type=0"><span class="badge badge-pill badge-success m-1">Placed (<?php echo $d->count_data_direct("order_id","order_master","society_id='$society_id' AND vendor_category_id=1 AND order_status=0"); ?>)</span></a>
                <a href ="canteenOrders?type=1" ><span class="badge badge-pill badge-info m-1">Preparing  (<?php echo $d->count_data_direct("order_id","order_master","society_id='$society_id' AND vendor_category_id=1 AND order_status=1"); ?>)</span></a>
                <a href ="canteenOrders?type=2"><span class="badge badge-pill badge-warning m-1">Dispatched (<?php echo $d->count_data_direct("order_id","order_master","society_id='$society_id' AND vendor_category_id=1 AND order_status=2"); ?>)</span></a>
                <a href ="canteenOrders?type=3"><span class="badge badge-pill badge-primary m-1">Delivered (<?php echo $d->count_data_direct("order_id","order_master","society_id='$society_id' AND vendor_category_id=1 AND order_status=3"); ?>)</span></a>
                <a href ="canteenOrders?type=4"><span class="badge badge-pill badge-danger m-1">Cancelled (<?php echo $d->count_data_direct("order_id","order_master","society_id='$society_id' AND vendor_category_id=1 AND order_status=4"); ?>)</span></a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <!-- <div class="card-header"><i class="fa fa-table"></i> Data Exporting</div> -->
            <div class="card-body">
              <div class="table-responsive">
               
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <!-- <th>#</th> -->
                        <th>Sr.No</th>
                        <th>User Name</th>
                        <th>Department Name</th>
                        <th>Branch Name</th>
                        <th>Order No</th>
                        <th>Total Amount</th>
                        <th>Order Date</th>
                        <th>Status</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <tbody id="showFilterData">
                   <?php 
                    $i=1;
                                           
                      if(isset($_GET['type']) && $_GET['type'] !='') {
                        $statusFilterQuery = " AND order_master.order_status='$_GET[type]'";
                       }
                     
                      $q=$d->select("order_master,users_master,floors_master,block_master,unit_master","users_master.unit_id=unit_master.unit_id AND users_master.floor_id=floors_master.floor_id AND block_master.block_id=users_master.block_id AND order_master.user_id=users_master.user_id AND order_master.society_id='$society_id'  AND users_master.delete_status=0 AND order_master.vendor_category_id = 1 $statusFilterQuery","ORDER BY order_master.order_id DESC LIMIT 1000");
                        $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                        
                      <!-- <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['xerox_paper_size_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['xerox_paper_size_id']; ?>">                      
                        </td> -->
                      <td><?php echo $counter++; ?></td>
                      <td><?php echo $data['user_full_name']; ?></td>
                      <td><?php echo $data['floor_name']; ?></td>
                      <td><?php echo $data['block_name']; ?> (<?php echo $data['unit_name']; ?>)</td>
                      <td><?php echo $data['order_no']; ?></td>
                      <td><?php echo $data['order_total_amount']; ?></td>
                      <td><?php echo date("d M Y h:i A", strtotime($data['order_date'])); ?></td>
                      <?php 
                        if($data['order_status'] == 0){
                            $status = "Placed";
                            $statusClass = "badge-success";
                        }
                        else if($data['order_status'] == 1){
                            $status = "Preparing";
                            $statusClass = "badge-info";
                        }
                        else if($data['order_status'] == 2){
                            $status = "Despatched";
                            $statusClass = "badge-warning";
                        }
                        else if($data['order_status'] == 3){
                            $status = "Delivered";
                            $statusClass = "badge-primary";
                        }
                        else{
                            $status = "Cancelled";
                            $statusClass = "badge-danger ";
                        }
                      ?>
                      <td><b><span class="badge badge-pill m-1 <?php echo $statusClass; ?>"><?php echo $status; ?></span></b></td>
                      
                      <td>
                       <a href="orderDetail?id=<?php echo $data['order_id']; ?>" class="btn btn-sm btn-primary ml-1" ><i class="fa fa-eye"></i> Order Detail</a> 
                      </td>
                        
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->

<script type="text/javascript">
  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
<style>
.hideupdate{
  display:none;
}

</style>