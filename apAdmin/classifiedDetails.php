<?php 
session_start();  
include_once 'lib/dao.php';
include 'lib/model.php';
$d = new dao();
$m = new model();
extract(array_map("test_input" , $_POST));
if (isset($getClassified)) { 
	$classData =$d->selectArray("classified_master,classified_category,classified_sub_category,society_master,users_master","users_master.user_id=classified_master.user_id AND society_master.society_id=classified_master.society_id AND classified_master.classified_status='0' AND classified_master.classified_category_id=classified_category.classified_category_id AND classified_master.classified_sub_category_id=classified_sub_category.classified_sub_category_id AND classified_master_id = '$classified_id'");
	$image = explode("~",$classData['classified_image']); 
?>
<div class="card profile-card-2">
    <div class="card-img-block">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ul class="carousel-indicators">
            	<?php for ($j=0; $j < count($image) ; $j++) { ?>
                	<li data-target="#demo" data-slide-to="<?=$j?>" class="<?= ($j==0) ? 'active' : ''?>"></li>
            	<?php } ?>
            </ul>
            <div class="carousel-inner">
            	<?php for ($j=0; $j < count($image) ; $j++) { ?>
                    <div style="background-color: white;" class="carousel-item <?= ($j==0) ? 'active' : ''?>">
                        <a href="../img/classified/classified_items_img/<?php echo $image[$j]; ?>" data-fancybox="images" data-caption="<?php echo $classData['classified_add_title']; ?>">
                            <img style="height:150px;" onerror="this.src='img/classified.jpg'" class="d-block w-100" src="../img/classified/classified_items_img/<?php echo $image[$j]; ?>" alt="<?php echo $classData['classified_add_title']; ?>">
                        </a>
                    </div>
                <?php } ?>
            </div>
            <a class="carousel-control-prev" href="#carousel" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#carousel" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    </div>
    <div class="card-body pt-5">
        <img onerror="this.src='img/user.png'" src="../img/users/recident_profile/<?php echo $classData['user_profile_pic'];?>" alt="profile-image" class="profile">

        <h5 class="card-title"><?php echo $classData['classified_add_title']; ?> 
        <?php if ($classData['product_type']==1) { ?>
                <span class="badge badge-secondary m-1">NEW</span>

                <?php } else { ?>
                <span class="badge badge-warning m-1">OLD</span>
                <?php } ?></h5>
        <h6 class="text-primary"><?= $classData['user_name'].' - '. $classData['user_mobile'] ?></h6>
        <p class="card-text"> <?php echo $classData['society_address'];?></p>
        <div class="icon-block">
            <h5>Category : <small><?php echo $classData['classified_category_name']; ?>-<?php echo $classData['classified_sub_category_name']; ?></small></h5>
            <h5>Price : <small><?php echo number_format($classData['classified_expected_price'],2); ?></small></h5>
            <h5>Features : <small><?php echo $classData['classified_features']; ?></small></h5>
            <hr>
            <h5>Brand : <small><?php echo $classData['classified_brand_name']; ?></small></h5>
            <h5>Purchase Year : <small><?php echo $classData['classified_manufacturing_year']; ?></small></h5>
            <h6>Added On : <?php 
                
                            echo date('d M Y h:i A', strtotime($classData['item_added_date'])); ?></h6>
            <hr>
            <h5><?php echo $classData['classified_describe_selling']; ?></h5>
        </div>
    </div>
</div>   
<?php } 
?>