<?php 
if (filter_var($_GET['id'], FILTER_VALIDATE_INT) == true && filter_var($_GET['societyid'], FILTER_VALIDATE_INT) == true) {
  $url=$_SESSION['url'] = $_SERVER['REQUEST_URI']; 
  $activePage = basename($_SERVER['PHP_SELF'], ".php");
  include_once 'lib/dao.php';
  include 'lib/model.php';
  $d = new dao();
  $m = new model();
?>
<!-- simplebar CSS-->
<link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
<!-- Bootstrap core CSS-->
<link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
<!-- animate CSS-->
<link href="assets/css/animate.css" rel="stylesheet" type="text/css"/>
<!-- Icons CSS-->
<link href="assets/css/icons.css" rel="stylesheet" type="text/css"/>
<!-- Sidebar CSS-->
<link href="assets/css/sidebar-menu2.css" rel="stylesheet"/>
<!-- Custom Style-->
<?php include 'common/colours.php'; ?>
<link href="assets/css/app-style9.css" rel="stylesheet"/>

<link href="assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
<link href="assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
<!--Lightbox Css-->
<link href="assets/plugins/fancybox/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style type="text/css">
  .lineHeight{
    line-height: 2;
  }
</style>



<?php 
extract(array_map("test_input" , $_GET));
$q=$d->selectArray("complains_master","complain_id='$id'");
$userData=$d->selectArray("users_master,unit_master,block_master","user_id='$q[user_id]' AND users_master.unit_id=unit_master.unit_id AND users_master.block_id=block_master.block_id");
$socData=$d->selectArray("society_master","society_id='$societyid'");
$catData=$d->selectArray("complaint_category","complaint_category_id='$q[complaint_category]'");


      $qc=$d->select("complains_track_master","complain_id='$id'","ORDER BY complains_track_id DESC");
      $atData= mysqli_fetch_array($qc);
      $technician = $atData['technician'];
      $lastMessage = $atData['complains_track_msg'];
?>
<div class="">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body" id="printableArea">

            <!-- Main content -->
            <section class="invoice p-4 border border-dark">
              <!-- title row -->
              <div class="row mt-3">
                <div class="col-sm-6">
                  <h4><i class="fa fa-building"></i> <?php echo $socData['society_name']; ?></h4>
                  <address>
                    <?php echo $socData['society_address']; ?>
                  </address>
                </div>
                <div class="col-sm-6 text-right">
                  <img src="../img/logo.png" height="100">
                </div>
              </div>

              <hr>
              <div class="row invoice-info lineHeight">
                <div class="col-sm-6 invoice-col">
                  <strong>Complaint No.</strong> : CN<?php echo $q['complain_id'] ?>
                  <br>
                  <strong>Date</strong> : <?php echo date('d M Y h:i A',strtotime($q['complain_date']));?>
                  <br>
                  <strong>Status</strong> : <?php if($q['flag_delete']==0){ if($q['complain_status']==2){ echo "Re Open";} else if($q['complain_status']==1){ echo "Closed";} else if($q['complain_status']==3){ echo "In Progress";} else{ echo "Open";} } else if($q['flag_delete']==1){ echo "Completed (deleted by user)";} ?>
                  
                </div>
                <div class="col-sm-6 invoice-col">
                 <h5><i class="fa fa-user"></i> Complaint By</h5>
                 <address>
                   <strong>
                    <?php echo $userData['user_full_name'].'-'.$userData['user_designation'];?></strong><br>
                    <STRONG>Mobile:</STRONG>
                    <?php echo $userData['user_mobile']; ?><br>
                    <STRONG>Email:</STRONG>
                    <?php echo $userData['user_email']; ?><br>
                  </address>
                </div>
              </div>
              <hr>
              <div class="row invoice-info lineHeight">
                <div class="col-sm-12 invoice-col">
                  <h5><i class="fa fa-edit"></i> Complaint Details</h5>
                  <strong>Complaint</strong> : <?php echo $q['compalain_title'] ?>
                  <br>
                  <strong>Description</strong> : <?php echo $q['complain_description'] ?> <br>
                  <strong>Last Status Message</strong> : <?php echo $lastMessage; ?> <br>
                  <strong>Technician</strong> : <?php echo $technician; ?>
                </div>
              </div>
            </section>
          </div>
          <!-- this row will not appear when printing -->
          <hr>
          <div class="row no-print" id="printPageButton">
            <div class="col-lg-3 ml-4">
              <a href="#" onclick="printDiv('printableArea')"  class="btn btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
            </div>
            <div class="col-lg-9">
              <div class="float-sm-right">
                <!-- <button class="btn btn-primary m-1"><i class="fa fa-envelope"></i> Receive on Mail</button> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function printDiv(divName) {
   var printContents = document.getElementById(divName).innerHTML;
   var originalContents = document.body.innerHTML;

   document.body.innerHTML = printContents;

   window.print();

   document.body.innerHTML = originalContents;
  }
</script>

<style type="text/css">
  @media print {
    #printPageButton {
      display: none;
    }
  }
</style>

<!-- Bootstrap core JavaScript-->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<!-- simplebar js -->
<script src="assets/plugins/simplebar/js/simplebar.js"></script>
<!-- waves effect js -->
<script src="assets/js/waves.js"></script>
<!-- sidebar-menu js -->
<script src="assets/js/sidebar-menu.js"></script>
<!-- Custom scripts -->
<script src="assets/js/app-script.js"></script>

</body>

</html>
<?php } else{
  echo "Invalid Request!!!";
}?>