<style>
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

</style>


<?php
extract(array_map("test_input", $_POST));
if (isset($edit_product)) {
    $q = $d->selectRow("product_price_master.*,product_master.product_category_id,product_master.product_sub_category_id,product_master.product_id","product_price_master,product_master","product_price_master.product_price_id='$product_price_id' AND product_master.product_id=product_price_master.product_id");
    $data = mysqli_fetch_array($q);
    extract($data);
}
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title"> Add Product Price</h4>
        </div>

        </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">  
                <form id="addProductPriceFrom" action="controller/ProductController.php" enctype="multipart/form-data" method="post">
                    <div class="row">
                          <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12"> Category <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text" required="" class="form-control single-select" name="product_category_id" id="product_category_id" onchange="getSubCategoryByIdInAddProduct(this.value);">
                                        <option value="">-- Select --</option> 
                                        <?php 
                                           $floor=$d->select("product_category_master","society_id='$society_id' AND product_category_status=0 AND product_category_delete=0 ");  
                                            while ($cData=mysqli_fetch_array($floor)) {
                                                $totalVariant = $d->count_data_direct("product_category_vendor_id","product_category_vendor_master","product_category_id='$cData[product_category_id]'");
                                                //if( $totalVariant!=0) {    
                                        ?>
                                        <option <?php if(isset($data['product_category_id']) && $data['product_category_id'] ==$cData['product_category_id']){ echo "selected"; } ?> value="<?php if(isset($cData['product_category_id']) && $cData['product_category_id'] !=""){ echo $cData['product_category_id']; } ?>"><?php if(isset($cData['category_name']) && $cData['category_name'] !=""){ echo $cData['category_name']; } ?></option> 
                                        <?php }
                                       // } ?>
                                    </select>                   
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Sub Category </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text"  onchange="getProductByCategoryAndSubCategoryId(this.value)"    class="form-control single-select" id="product_sub_category_id" name="product_sub_category_id">
                                        <option value="">-- Select --</option> 
                                        <?php 
                                        if(isset($data)&& $data['product_category_id']!=""){
                                            $dq = $d->selectRow('product_sub_category_master.*',"product_sub_category_master","product_sub_category_master.product_sub_category_status=0 AND product_sub_category_master.product_sub_category_delete=0 AND product_sub_category_master.product_category_id = '$data[product_category_id]'");
                                            while($SubCatdata = mysqli_fetch_array($dq)){
                                                if($SubCatdata){?>
                                                    <option <?php if(isset($data['product_sub_category_id']) && $data['product_sub_category_id']==$SubCatdata['product_sub_category_id']){ echo "selected"; } ?> value="<?php echo $SubCatdata['product_sub_category_id']; ?>"><?php echo $SubCatdata['sub_category_name']; ?></option>
                                            <?php }
                                            }
                                        }?>
                                    </select>                   
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Product <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text"class="form-control single-select frmInputSl" onchange="getProductVariant(this.value)"  id="product_id" name="product_id">
                                        <option value="">-- Select --</option> 
                                        <?php if (isset($edit_product)) {
                                            $product_master = $d->select("product_master","product_category_id='$data[product_category_id]' AND product_sub_category_id='$data[product_sub_category_id]'");
                                            
                                            while($Prdata = mysqli_fetch_array($product_master)){
                                                ?>
                                                <option <?php if(isset($data['product_id']) && $data['product_id']==$Prdata['product_id']){ echo "selected"; } ?> value="<?php echo $Prdata['product_id']; ?>"><?php echo $Prdata['product_name']; ?></option>
                                                <?php
                                            }
                                        } ?>
                                    </select>                 
                                    </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Product Variant <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text"class="form-control single-select frmInputSl"  id="product_variant_id" name="product_variant_id">
                                        <option value="">-- Select --</option> 
                                        <?php if (isset($edit_product)) {
                                            $product_vmaster = $d->select("product_variant_master","product_id='$data[product_id]' ");
                                            while($Prvdata = mysqli_fetch_array($product_vmaster)){
                                                ?>
                                                <option <?php if(isset($data['product_variant_id']) && $data['product_variant_id']==$Prvdata['product_variant_id']){ echo "selected"; } ?> value="<?php echo $Prvdata['product_variant_id']; ?>"><?php echo $Prvdata['product_variant_name']; ?></option>
                                                <?php
                                            }
                                        } ?>
                                    </select>                 
                                    </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                            <label for="input-10" class="col-lg-12 col-md-12 col-form-label col-12">Vendor <span class="required">*</span></label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <select  type="text"class="form-control single-select frmInputSl" id="vendor_id" name="vendor_id">
                                        <option value="">-- Select --</option> 
                                        <?php 
                                            $floor=$d->select("local_service_provider_users"," service_provider_status=0 AND service_provider_delete_status=0 AND society_id = $society_id ");  
                                            while ($floorData=mysqli_fetch_array($floor)) {
                                        ?>
                                        <option <?php if(isset($data['vendor_id']) && $data['vendor_id'] ==$floorData['service_provider_users_id']){ echo "selected"; } ?> value="<?php if(isset($floorData['service_provider_users_id']) && $floorData['service_provider_users_id'] !=""){ echo $floorData['service_provider_users_id']; } ?>"><?php if(isset($floorData['service_provider_name']) && $floorData['service_provider_name'] !=""){ echo $floorData['service_provider_name'] ; } ?></option> 
                                        <?php } ?>
                                    </select>                 
                                    </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Minimum Order Quantity<span class="required">*</span> </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" maxlength="10" min="1" class="form-control onlyNumber" placeholder="Minimum Order Quantity" name="minimum_order_quantity" value="<?php if($data['minimum_order_quantity'] !=""){ echo $data['minimum_order_quantity']; } ?>">
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row w-100 mx-0">
                                <label for="input-10" class="col-lg-12 col-md-12 col-form-label">Product Price <span class="required">*</span> </label>
                                <div class="col-lg-12 col-md-12 col-12" id="">
                                    <input type="text" maxlength="10" min="1" class="form-control onlyNumber" placeholder="Product Price" name="product_price" value="<?php if($data['product_price'] !=""){ echo $data['product_price']; } ?>">
                                </div>
                            </div> 
                        </div>
                    </div>    
                                
                    <div class="form-footer text-center">
                    <?php
                    if (isset($edit_product)) {                    
                    ?>
                    <input type="hidden" id="product_price_id" name="product_price_id" value="<?php if($data['product_price_id'] !=""){ echo $data['product_price_id']; } ?>" >
                    <button  type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Update </button>
                    <input type="hidden" name="addProductPrice"  value="addProductPrice">
                    <?php //IS_837 onclick="resetFrm('penaltyAdd');" ?>
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addProductPrice');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php }
                    else {
                    ?>
                    <button id="addProductBtn" type="submit"  class="btn btn-success"><i class="fa fa-check-square-o"></i> Add</button>
                    <input type="hidden" name="addProductPrice"  value="addProductPrice">
                    <button type="reset"  value="add" class="btn btn-danger cancel" onclick="resetFrm('addProductPrice');"><i class="fa fa-check-square-o"></i> Reset</button>
                    <?php } ?>
                    </div>

                </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    </div><!--End content-wrapper-->
    <script src="assets/js/jquery.min.js"></script>

<script type="text/javascript">
<?php  if (isset($edit_product)) { ?>
  //  getSubCategoryByIdInAddProduct(<?php echo $data['product_category_id']; ?>, <?php echo $data['product_sub_category_id']; ?>);

<?php } ?>



function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').html('<img src="'+e.target.result+'" width="75%" height="50%">');
        };
        reader.readAsDataURL(input.files[0]);
    }
}


</script>