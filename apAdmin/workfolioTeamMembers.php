<?php error_reporting(0);
$teamId = $_REQUEST['teamId'];
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-3 col-md-6 col-6">
          <h4 class="page-title">Workfolio Employess</h4>
        </div>
        <div class="col-sm-3 col-md-6 col-6">
          <div class="btn-group float-sm-right">
            <!-- <a href="addLeaveType" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()" class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
            <a href="javascript:void(0)" onclick="DeleteAll('deleteChatGroupMember');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
          </div>
        </div>
     </div>

        <!-- <form action="">
            <div class="row pt-2 pb-2">
                <div class="col-md-3 form-group">
                    <label  class="form-control-label">Group Name</label>
                    <select name="gId" class="form-control single-select" required="">
                        <option value="">-- Select Group --</option> 
                        <?php 
                            $qb=$d->select("chat_group_master","society_id='$society_id'");  
                            while ($groupData=mysqli_fetch_array($qb)) {
                        ?>
                        <option  <?php if($gId==$groupData['group_id']) { echo 'selected';} ?> value="<?php echo $groupData['group_id'];?>" ><?php echo $groupData['group_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class="col-md-3 form-group mt-auto">
                    <input class="btn btn-success" type="submit" name="getReport" class="form-control" value="Get Data">
                </div>
            </div>
        </form> -->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <?php 
                    if (isset($teamId) && $teamId != '') {
                        $teamFilterQuery = "workfolio_employees.teamId='$teamId'";
                    }
                    $q=$d->selectRow("workfolio_employees.*, users_master.user_full_name","workfolio_employees LEFT JOIN users_master ON workfolio_employees.email=users_master.user_email","$teamFilterQuery");

                ?>
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <!-- <th>#</th> -->
                        <th>Sr.No</th>                        
                        <th>Team ID</th>                      
                        <th>Email</th>
                        <th>Display Name</th>
                        <th>Employee Name</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                    $i=1;                   
                      $counter = 1;
                    while ($data=mysqli_fetch_array($q)) {
                     
                     ?>
                    <tr>
                       <!-- <td class="text-center">
                         <input type="hidden" name="id" id="id"  value="<?php echo $data['chat_group_member_id']; ?>">                  
                          <input type="checkbox" name="" class="multiDelteCheckbox" value="<?php echo $data['chat_group_member_id']; ?>">                         
                        </td> -->
                        <td><?php echo $counter++; ?></td>
                        <td><?php echo $data['teamId']; ?></td>
                        <td><?php echo $data['email']; ?></td>
                        <td><?php echo $data['displayName']; ?></td>
                        <td><?php echo $data['user_full_name']; ?></td> 
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php //} else {  ?>
                  <!-- <div class="" role="alert">
                    <span><strong>Note :</strong> Please Select Group</span>
                  </div> -->
                <?php //} ?>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->
    
</div>


<script type="text/javascript">

  function popitup(url) {
    newwindow=window.open(url,'name','height=800,width=900, location=0');
    if (window.focus) {newwindow.focus()}
      return false;
    }
</script>
