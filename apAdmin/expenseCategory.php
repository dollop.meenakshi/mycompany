<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-5">
                <h4 class="page-title"><?php echo $xml->string->expense_category; ?></h4>
            </div>
            <div class="col-sm-3 col-7">
                <div class="btn-group float-sm-right">
                    <button type="button" data-toggle="modal" data-target="#complainCategoryModal" class="float-right btn btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add</button>
                    <a href="javascript:void(0)" onclick="DeleteAll('deleteExpenseCategory');" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fa fa-trash-o fa-lg"></i> Delete </a>
                </div>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>#</th>
                                        <th>Category Name</th>
                                        <th>Type</th>
                                        <th>Unit Name</th>
                                        <th>Unit Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    $q=$d->selectRow("expense_category_master.*,(SELECT COUNT(*) FROM user_expenses WHERE user_expenses.expense_category_id = expense_category_master.expense_category_id ) AS total_expense","expense_category_master");
                                    while($row = $q->fetch_assoc())
                                    {
                                        extract($row);
                                    ?>
                                    <tr>
                                        <td class='text-center'>
                                            <?php if ($total_expense>0) { ?>
                                                <input disabled type="checkbox" class="multiDelteCheckbox"  value="">
                                            <?php } else { ?>
                                            <input type="checkbox" class="multiDelteCheckbox"  value="<?php echo $row['expense_category_id']; ?>">
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $expense_category_name; ?></td>
                                        <td>
                                            <?php
                                            if ($expense_category_type == 0)
                                            {
                                                echo "Amount Wise";
                                            }
                                            else
                                            {
                                                echo "Unit Wise";
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $expense_category_unit_name; ?></td>
                                        <td>
                                            <?php
                                            if($expense_category_unit_price != 0 && $expense_category_unit_price != "0" && $expense_category_unit_price != 0.0 && $expense_category_unit_price != "0.0" && $expense_category_unit_price != 0.00 && $expense_category_unit_price != "0.00")
                                            {
                                                echo (float)$expense_category_unit_price;
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <button type="button" data-toggle="modal" data-target="#editExpenseCategoryModal" class="btn btn-info btn-sm" onclick="editExpenseCategoryNew(<?php echo $expense_category_id ?>)"><i class="fa fa-pencil"></i></button>
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="complainCategoryModal">
        <div class="modal-dialog">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Add Expense Category</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="addExpenseCategoryForm" method="POST" action="controller/ExpenseSettingController.php" enctype="multipart/form-data">
                        <div class="row form-group">
                            <label class="col-sm-3 form-control-label">Name <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" maxlength="140" autocomplete="off" required name="expense_category_name" id="expense_category_name">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3 form-control-label">Type <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <select class="form-control single-select" required name="expense_category_type" id="expense_category_type">
                                    <option value="">--SELECT--</option>
                                    <option value="0">Amount Wise</option>
                                    <option value="1">Unit Wise</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group unitDiv d-none">
                            <label class="col-sm-3 form-control-label">Unit Name <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" maxlength="140" autocomplete="off" required name="expense_category_unit_name" id="expense_category_unit_name">
                            </div>
                        </div>
                        <div class="row form-group unitDiv d-none">
                            <label class="col-sm-3 form-control-label">Unit Price <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control decimal" autocomplete="off" required name="expense_category_unit_price" id="expense_category_unit_price">
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" name="addExpenseCategory" value="addExpenseCategory">
                            <button class="btn btn-sm btn-success"><i class="fa fa-check"></i> Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editExpenseCategoryModal">
        <div class="modal-dialog">
            <div class="modal-content border-primary">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white"><?php echo $xml->string->edit; ?> <?php echo $xml->string->expense_category; ?></h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editExpenseCategoryForm" method="POST" action="controller/ExpenseSettingController.php" enctype="multipart/form-data">
                        <div class="row form-group">
                            <label class="col-sm-3 form-control-label">Name <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" maxlength="140" autocomplete="off" required name="edit_expense_category_name" id="edit_expense_category_name">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3 form-control-label">Type <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <select class="form-control single-select" required name="edit_expense_category_type" id="edit_expense_category_type">
                                    <option value="">--SELECT--</option>
                                    <option value="0">Amount Wise</option>
                                    <option value="1">Unit Wise</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group unitDivEdit d-none">
                            <label class="col-sm-3 form-control-label">Unit Name <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" maxlength="140" autocomplete="off" required name="edit_expense_category_unit_name" id="edit_expense_category_unit_name">
                            </div>
                        </div>
                        <div class="row form-group unitDivEdit d-none">
                            <label class="col-sm-3 form-control-label">Unit Price <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control decimal" autocomplete="off" required name="edit_expense_category_unit_price" id="edit_expense_category_unit_price">
                            </div>
                        </div>
                        <div class="form-footer text-center">
                            <input type="hidden" name="csrf" id="edit_csrf">
                            <input type="hidden" name="edit_expense_category_id" id="edit_expense_category_id">
                            <input type="hidden" name="updateExpenseCategory" value="updateExpenseCategory">
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>