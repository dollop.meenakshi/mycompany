<style>
  .photoInput ,.idInput
  {
    display: none;
  }
  .pac-container { z-index: 100000 !important; }
</style>
<?php
$sister_company_que = "";
$sister_company_que_and = "";
if(isset($_REQUEST['scId']) && $_REQUEST['scId']>0)
{
   
    $sister_company_id = (int)$_GET['scId'];
    $sister_company_que_and = " AND sister_company_id = $sister_company_id";
    $sister_company_que = " sister_company_id = $sister_company_id";
}

if(isset($_GET['changeSort']) && $_GET['changeSort'] == "yes" && isset($_GET['block_id']) && $_GET['block_id'] != "" && isset($_GET['floor_id']) && $_GET['floor_id'] != "")
{
  $floor_id = $_GET['floor_id'];
  $block_id = $_GET['block_id'];
?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-6">
                <h4 class="page-title">Change Employee Order</h4>
            </div>
            <div class="col-sm-2">
            </div>
            <div class="col-sm-4">
                <div class="btn-group float-sm-right">
                    <a href="companyEmployees" class="btn btn-primary waves-effect waves-light btn-sm"><i class="fa fa-arrow-left"></i> Go Back</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumb-->
    <div class="row">
        <div class="col-12 col-lg-12">
            <div class="card border border-info floorList">
                <div class="card-body">
                    <div class="media align-items-center">
                        <div class="media-body text-left">
                            <div class="row m-0" id="users-list">
                                <?php
                                $fq=$d->selectRow("users_master.user_id,users_master.user_designation,users_master.user_full_name,users_master.user_type,users_master.is_defaulter,users_master.user_profile_pic,unit_master.unit_status","users_master LEFT JOIN unit_master ON unit_master.unit_id = users_master.unit_id","users_master.society_id='$society_id' AND users_master.block_id='$block_id' AND users_master.floor_id = '$floor_id' AND users_master.member_status = 0 AND users_master.delete_status = 0 AND users_master.user_status = 1 $blockAppendQueryUser","ORDER BY user_sort");
                                while ($unitData = $fq->fetch_assoc())
                                {
                                $user_designation= $unitData['user_designation'];
                                $user_full_name= $unitData['user_full_name'];
                                $shortChar=$d->get_sort_name($user_full_name);
                                ?>
                                <div class="col-6 col-md-6 col-lg-3 col-xl-3 pt-3">
                                    <div class="card  no-bottom-margin border-1px">
                                        <div class="card-body text-center ">
                                            <a href="employeeDetails?id=<?php echo $unitData['user_id']; ?>">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <?php if(file_exists('../img/users/recident_profile/'.$unitData['user_profile_pic']) && $unitData['user_profile_pic']!="user_default.png" && $unitData['user_profile_pic']!="user.png") { ?>
                                                        <img id="blah"  onerror="this.src='img/user.png'" src='img/user.png'  data-src="../img/users/recident_profile/<?php echo $unitData['user_profile_pic']; ?>"  style="width: 60px; height: 60px;"   src="#" alt="your image" class='rounded-circle p-1 lazyload img-thumbnail' />
                                                        <?php } else { ?>
                                                        <div style="line-height: 60px; margin-left: .25rem !important;font-size: 25px;width: 60px;height: 60px;" class="profile-image-name">
                                                            <?php echo trim($shortChar); ?>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-9">
                                                        <h6 class=" mt-2 text-capitalize"><b><?php echo custom_echo($user_full_name,20); ?></b> </h6>
                                                        <i class="text-dark" style="font-size: 11px;font-style: normal;"><i class="fa fa-building"></i> <?php custom_echo($user_designation,20); ?> </i>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End container-fluid-->
</div><!--End content-wrapper-->
<?php
}
else
{
?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-4">
                <h4 class="page-title"><?php if( $society_type ==1) { echo $xml->string->members; } else { echo $xml->string->members; } ?></h4>
            </div>
            <div class="col-sm-4">
            <?php
                $check_sc = $d->selectRow("sister_company_id,sister_company_name","sister_company_master","society_id = '$society_id' AND status = 1");
                if(mysqli_num_rows($check_sc) > 0)
                {
            ?>
                <form>
                    <input type="hidden" name="sisterCompanyFilter" value="sisterCompanyFilter"/>
                    <select class="form-control single-select" id="scId" name="scId" onchange="this.form.submit()">
                        <option value="0">All</option>
                        <?php
                        while($row = $check_sc->fetch_assoc())
                        {
                        ?>
                        <option <?php if($sister_company_id == $row['sister_company_id']){ echo "selected"; } ?> value="<?php echo $row['sister_company_id']; ?>"><?php echo $row['sister_company_name']; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </form>
            <?php
                }
            ?>
            </div>
            <div class="col-sm-4">
                <div class="btn-group float-sm-right">
                    <a href="#" class="btn btn-sm btn-info" data-toggle="modal" data-target="#addOwner"><?php echo $xml->string->add; ?>  </a>
                    <a href="#" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#bulkUpload"><?php echo $xml->string->import; ?> <?php echo $xml->string->bulk; ?></a>
                    <a href="printIcard.php?allUsers=allUsers&type=<?php echo $_GET['type'];?>&bId=<?php echo $_GET['bId'];?>" class="btn btn-success btn-sm"><i class="fa fa-address-card-o"></i> <?php echo $xml->string->print_i_card; ?></a>
                </div>
            </div>
        </div>
      <!-- End Breadcrumb-->
    <?php
        $i=1;
        $firstBlockIdArray = array();
        $q =  $d->selectRow("block_master.*,(SELECT COUNT(*) FROM users_master WHERE users_master.block_id = block_master.block_id AND user_status!=0 AND delete_status=0 $sister_company_que_and) AS total_employees","block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_sort ASC");
        if(mysqli_num_rows($q) <=0)
        {
            echo "<img src='img/no_data_found.png'>";
        }
        else
        {
      ?>
        <div class="row blockList">
        <?php
            while ($data=mysqli_fetch_array($q))
            {
                $j=$i++;
                if ($j==1)
                {
                    array_push($firstBlockIdArray,$data['block_id']);
                }
                extract($data);
        ?>
            <div class="col-lg-2 col-6">
                <a href="companyEmployees?bId=<?php echo $block_id; if(isset($_GET['type'])){?>&type=<?php echo $_GET['type']; }?>">
                    <?php
                    if (isset($_GET['bId']) && filter_var($_GET['bId'], FILTER_VALIDATE_INT) == true)
                    {
                    ?>
                      <div class="mb-3 card <?php if($block_id==$_GET['bId']) { echo 'bg-primary'; $bId = $block_id; } else { echo 'bg-google-plus'; }  ?> ">
                        <div class="card-body text-center text-white">
                          <h5 class="mt-2 text-white"><?php echo $block_name; ?> (<?php echo $total_employees; ?>)</h5>
                        </div>
                      </div>
                    <?php
                    }
                    else
                    {
                    ?>
                      <div class="mb-3 card <?php  if($j==1){echo 'bg-primary';} else { echo 'bg-google-plus'; }  ?> ">
                        <div class="card-body text-center text-white">
                          <h6 class="mt-2 text-white"><?php echo $block_name; ?> (<?php echo $total_employees; ?>)</h6>
                        </div>
                      </div>
                    <?php
                    }
                    ?>
                </a>
            </div>
        <?php
            }
        ?>
        </div>
        <?php
            $invalidDataAry = $_SESSION['invalidDataAry'];
            if(!empty($invalidDataAry))
            {
        ?>
        <div class="row">
            <div class="col-lg-12">
              <h6 class="text-danger">Below Data Is Invalid, Please Check And Upload CSV Again</h6>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Remark.</th>
                                        <th>Row No.</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    foreach ($invalidDataAry as $key => $value)
                                    {
                                ?>
                                    <tr>
                                        <td><?php echo ($key+1);?></td>
                                        <td><?php echo $value['full_name'];?></td>
                                        <td><?php echo $value['user_mobile'];?></td>
                                        <td><?php echo $value['remark'];?></td>
                                        <td><?php echo $value['rowNo'];?></td>
                                    </tr>
                                <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
            unset($_SESSION['invalidDataAry']);?>
        <div class="row">
        <?php
            //$q3 = $d->select("block_master" ,"society_id='$society_id' $blockAppendQuery","ORDER BY block_sort ASC LIMIT 1");
            //$fdata=mysqli_fetch_array($q3);
            $firstBlockId= $firstBlockIdArray[0];
            if(isset($_GET['bId'])  && filter_var($_GET['bId'], FILTER_VALIDATE_INT) == true)
            {
                $fq=$d->selectRow("floors_master.*,(SELECT COUNT(*) FROM users_master WHERE users_master.block_id = block_master.block_id AND user_status!=0 AND delete_status=0 AND users_master.floor_id=floors_master.floor_id $sister_company_que_and) AS total_employees_in_department","floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floors_master.block_id='$_GET[bId]' $blockAppendQuery","ORDER BY floors_master.floor_sort ASC");
            }
            else
            {
                $fq=$d->selectRow("floors_master.*,(SELECT COUNT(*) FROM users_master WHERE users_master.block_id = block_master.block_id AND user_status!=0 AND delete_status=0 AND users_master.floor_id=floors_master.floor_id $sister_company_que_and) AS total_employees_in_department","floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floors_master.block_id='$firstBlockId' $blockAppendQuery $blockAppendQueryFloor","ORDER BY floors_master.floor_sort ASC");
            }
            while ($floorData=mysqli_fetch_array($fq))
            {
                $floorId= $floorData['floor_id'];
                $no_of_unit= $floorData['no_of_unit'];
                $unit_type= $floorData['unit_type'];
                $block_id= $floorData['block_id'];
                $blockName= $floorData['block_name'];
                $floorName = $floorData['floor_name'];
        ?>
            <div class="col-12 col-lg-12">
                <div class="card border border-info floorList">
                    <div class="card-body">
                        <div class="floor text-left p-l row">
                            <div class="col pl-3">
                                <h6 class="mb-0"><?php echo $floorData['floor_name']; ?> (<?php echo $floorData['total_employees_in_department'];?>)</h6>
                            </div>
                            <div class="col pr-3">
                            <?php
                                if ($floorData['total_employees_in_department'] > 0)
                                {
                            ?>
                                <a class="btn btn-sm bg-warning text-white fa fa-sort float-sm-right" href="companyEmployees?changeSort=yes&block_name=<?php echo $blockName; ?>&block_id=<?php echo $block_id; ?>&floor_name=<?php echo $floorName; ?>&floor_id=<?php echo $floorId; ?>" title="Change Order"></a>
                            <?php
                                }
                            ?>
                            </div>
                        </div>
                        <div class="media align-items-center">
                            <div class="media-body text-left">
                                <div class="row m-0">
                                    <?php
                                        $uq=$d->selectRow("unit_master.*, users_master.user_full_name, users_master.user_designation,users_master.user_profile_pic, users_master.user_id, users_master.user_type","unit_master LEFT JOIN users_master ON users_master.unit_id = unit_master.unit_id","unit_master.floor_id='$floorId' AND unit_master.society_id='$society_id' AND users_master.delete_status=0 $sister_company_que_and","ORDER BY user_sort");
                                        while ($unitData=mysqli_fetch_array($uq))
                                        {
                                            $user_designation= $unitData['user_designation'];
                                            $user_full_name= $unitData['user_full_name'];
                                            $shortChar=$d->get_sort_name($user_full_name);
                                    ?>
                                    <div class="col-6 col-md-6 col-lg-3 col-xl-3 pt-3">
                                        <div class="card no-bottom-margin border-1px">
                                            <div class="card-body text-center ">
                                                <a href="employeeDetails?id=<?php echo $unitData['user_id']; ?>">
                                                    <div class="row">
                                                        <div class="col-3">
                                                        <?php
                                                            if(file_exists('../img/users/recident_profile/'.$unitData['user_profile_pic']) && $unitData['user_profile_pic']!="user_default.png" && $unitData['user_profile_pic']!="user.png")
                                                            {
                                                        ?>
                                                            <img id="blah"  onerror="this.src='img/user.png'" src='img/user.png'  data-src="../img/users/recident_profile/<?php echo $unitData['user_profile_pic']; ?>"  width="60" height="60" style="width: 60px; height: 60px;"  src="#" alt="your image" class='rounded-circle p-1 lazyload img-thumbnail' />
                                                        <?php
                                                            }
                                                            else
                                                            {
                                                        ?>
                                                            <div style="line-height: 60px; margin-left: .25rem !important;font-size: 25px;width: 60px;height: 60px;" class="profile-image-name"><?php echo trim($shortChar); ?></div>
                                                        <?php
                                                            }
                                                        ?>
                                                        </div>
                                                        <div class="col-9">
                                                            <h6 class=" mt-2 text-capitalize"><b><?php echo custom_echo($user_full_name,20); ?></b> </h6>
                                                            <i class="text-dark" style="font-size: 11px;font-style: normal;"><i class="fa fa-building"></i> <?php custom_echo($user_designation,20); ?> </i>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            }
        ?>
        </div>
    <?php
        }
    ?>
<!-- End container-fluid-->
    </div><!--End content-wrapper-->
</div><!--End content-wrapper-->
<?php
}
?>
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i></a>
<!--End Back To Top Button-->
<div class="modal fade" id="addOwner">
    <div class="modal-dialog modal-lg">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white"><?php echo $xml->string->add_new; ?> <?php echo $xml->string->user; ?></h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="addUserDiv">
                <form id="addUser" action="controller/userController.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="block_id" value="<?php if($bId!="") { echo $bId; } else { echo $firstBlockId; } ?>" id="block_id">
                    <input type="hidden" name="unit_status" value="1">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-sm-12 text-center">
                                    <label for="imgInpProfile">
                                        <img id="profileView" style="border: 1px solid gray;" src="img/user.png"  width="100" height="100"   src="#" alt="your image" class='rounded-circle' />
                                        <i style="position: absolute;font-size: 20px;margin-left: -16px;margin-top: 66px;" class="fa fa-camera"></i>
                                        <input accept="image/*" class="photoInput photoOnly" id="imgInpProfile" type="file" name="user_profile_pic">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <input value="<?php echo $_COOKIE['society_name']; ?>" required="" maxlength="100" type="hidden" class="form-control" name="company_name">
                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->employee_id; ?>  </label>
                                <div class="col-lg-12 col-md-12">
                                    <input maxlength="50" type="text" class="form-control" name="unit_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->designation; ?> <span class="text-danger">*</span>  </label>
                                <div class="col-lg-12 col-md-12">
                                    <input maxlength="50" required="" type="text" class="form-control" name="designation">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->floor; ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-12 col-md-12">
                                    <select required="" maxlength="100" type="text" class="form-control single-select" name="floor_id" onchange="getShiftByFloorId(this.value); getSubDepartmentByFloorId(this.value);">
                                        <option value="">-- <?php echo $xml->string->select; ?> <?php echo $xml->string->floor; ?> --</option>
                                        <?php
                                        if ($bId=="") {
                                        $bId = $firstBlockId;
                                        }
                                        $fqAd=$d->select("floors_master,block_master","floors_master.block_id=block_master.block_id AND floors_master.society_id='$society_id' AND floors_master.block_id='$bId' $blockAppendQuery");
                                        while ($fData=mysqli_fetch_array($fqAd)) { ?>
                                        <option value="<?php echo $fData['floor_id'];?>"> <?php echo $fData['floor_name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label">Sub <?php echo $xml->string->floor; ?></label>
                                <div class="col-lg-12 col-md-12">
                                    <select type="text" class="form-control single-select" name="sub_department_id" id="sub_department_id">
                                        <option value="">-- <?php echo $xml->string->select; ?> Sub <?php echo $xml->string->floor; ?> --</option>
                                    </select>
                                </div>
                            </div>
                            <?php $totalZone = $d->count_data_direct("zone_id","zone_master","society_id='$society_id' AND zone_status = 0");
                            if( $totalZone > 0) {
                            ?>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label">Zone</label>
                                <div class="col-lg-12 col-md-12">
                                    <select type="text" class="form-control single-select" name="zone_id" id="zone_id">
                                        <option value="">-- <?php echo $xml->string->select; ?> Zone --</option>
                                        <?php
                                        $zq=$d->select("zone_master","society_id='$society_id' AND zone_status='0'");
                                        while ($zqData=mysqli_fetch_array($zq)) { ?>
                                        <option value="<?php echo $zqData['zone_id'];?>"> <?php echo $zqData['zone_name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php } ?>
                            <?php $totalLevel = $d->count_data_direct("level_id","employee_level_master","society_id='$society_id' AND level_status = 0");
                            if( $totalLevel > 0) {
                            ?>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label">Employee Level</label>
                                <div class="col-lg-12 col-md-12">
                                    <select type="text" class="form-control single-select" name="level_id" id="level_id">
                                        <option value="">-- <?php echo $xml->string->select; ?> Employee Level --</option>
                                        <?php
                                        $lq=$d->select("employee_level_master","society_id='$society_id' AND level_status='0'");
                                        while ($lqData=mysqli_fetch_array($lq)) { ?>
                                        <option value="<?php echo $lqData['level_id'];?>"> <?php echo $lqData['level_name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->shift; ?></label>
                                <div class="col-lg-12 col-md-12">
                                    <select type="text" class="form-control single-select" name="shift_time_id" id="shift_time_id">
                                        <option value="">-- <?php echo $xml->string->select; ?> <?php echo $xml->string->shift; ?> --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->first_name; ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-12 col-md-12">
                                    <input autocomplete="off"  required="" maxlength="50" type="text" class="form-control text-capitalize onlyName" name="user_first_name_add">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="input-11" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->last_name; ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-12 col-md-12">
                                    <input required="" autocomplete="off"  maxlength="50" type="text" class="form-control text-capitalize onlyName" id="input-11" name="user_last_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="input-Country" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->country_code; ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-12 col-md-12">
                                    <input type="hidden" value="+91" id="country_code_get" name="">
                                    <select name="country_code" class="form-control single-select" id="country_code" required="">
                                        <?php include 'country_code_option_list.php'; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="input-12" class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->mobile_no; ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-12 col-md-12">
                                    <input required="" autocomplete="off"  min="1" type="text" onblur="checkMobileUser()"  maxlength="15" minlength="8" class="form-control" id="userMobile" inputmode="numeric" name="user_mobile">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->date_of_birth; ?> </label>
                                <div class="col-lg-12 col-md-12">
                                    <input class="form-control" readonly="" id="autoclose-datepicker-dob" name="member_date_of_birth" type="text" value="" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->email_id; ?> </label>
                                <div class="col-lg-12 col-md-12">
                                    <input  type="email" autocomplete="off" class="form-control"   id="userEmail" name="user_email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label"><?php echo $xml->string->date_of_join; ?> <span class="text-danger">*</span></label>
                                <div class="col-lg-12 col-md-12">
                                    <input class="form-control" required readonly="" id="autoclose-datepicker-doj" name="joining_date" type="text" value="" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label form-control-label"><?php echo $xml->string->gender; ?></label>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" checked="" class="form-check-input" value="Male" name="gender"> <?php echo $xml->string->male; ?>
                                        </label>
                                    </div>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio"  class="form-check-input" value="Female" name="gender"> <?php echo $xml->string->female; ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $check_sc = $d->selectRow("sister_company_id,sister_company_name","sister_company_master","society_id = '$society_id' AND status = 1");
                            if(mysqli_num_rows($check_sc) > 0)
                            {
                            ?>
                            <div class="col-md-6">
                                <label class="col-lg-12 col-md-12 col-form-label form-control-label">Sister Company</label>
                                <div class="col-lg-12 col-md-12">
                                    <select class="form-control single-select" id="sister_company_id" name="sister_company_id">
                                        <option>Select</option>
                                        <?php
                                        while($row = $check_sc->fetch_assoc())
                                        {
                                        ?>
                                        <option value="<?php echo $row['sister_company_id']; ?>"><?php echo $row['sister_company_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                            <div id="ownerDiv">
                                <div class="form-group row">
                                    <label for="tenant_agreement_start_date" class="col-lg-2 col-form-label form-control-label"><?php echo $xml->string->agreement_start_date; ?></label>
                                    <div class="col-lg-4">
                                        <input class="form-control FromDate"  maxlength="10" name="tenant_agreement_start_date" id="tenant_agreement_start_date"  type="text" >
                                    </div>
                                    <label for="tenant_agreement_end_date" class="col-lg-2 col-form-label form-control-label"><?php echo $xml->string->agreement_end_date; ?></label>
                                    <div class="col-lg-4">
                                        <input class="form-control ToDate"  maxlength="10" name="tenant_agreement_end_date" id="tenant_agreement_end_date"  type="text" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tenant_doc" class="col-lg-2 col-form-label form-control-label"><?php echo $xml->string->rent_agreement; ?></label>
                                    <div class="col-lg-4">
                                        <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"  class="form-control-file border idProof" id="tenant_doc" type="file" name="tenant_doc">
                                    </div>
                                    <label for="prv_doc" class="col-lg-2 col-form-label form-control-label"><?php echo $xml->string->police_verification_document; ?></label>
                                    <div class="col-lg-4">
                                        <input accept=".pdf,.png,.jpg,.jpeg,.PNG,.JPG,.JPEG,.PDF,.DOC,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"  class="form-control-file border idProof" id="prv_doc" type="file" name="prv_doc">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label form-control-label"><?php echo $xml->string->owner; ?> <?php echo $xml->string->first_name; ?> *  </label>
                                    <div class="col-lg-4">
                                        <input required="" autocomplete="off"  maxlength="30" class="form-control text-capitalize " name="owner_first_name" type="text" >
                                    </div>
                                    <label class="col-lg-2 col-form-label form-control-label"><?php echo $xml->string->owner; ?> <?php echo $xml->string->last_name; ?>e<span class="text-danger">*</span></label>
                                    <div class="col-lg-4">
                                        <input maxlength="30" autocomplete="off"  class="form-control text-capitalize" required="" maxlength="50" name="owner_last_name" id="owner_last_name"  type="text" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="input-country_code_owner" class="col-sm-2 col-form-label"><?php echo $xml->string->country_code; ?> <span class="text-danger">*</span></label>
                                    <div class="col-sm-4">
                                        <select name="country_code_owner" class="form-control single-select" id="country_code_owner" required="">
                                            <?php include 'country_code_option_list.php'; ?>
                                        </select>
                                    </div>
                                    <label class="col-lg-2 col-form-label form-control-label"><?php echo $xml->string->owner; ?> <?php echo $xml->string->mobile_no; ?> <span class="text-danger">*</span></label>
                                    <div class="col-lg-4">
                                        <input class="form-control" autocomplete="off"  required="" maxlength="15" minlength="8" name="owner_mobile" id="ownerMobile"  type="text" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label form-control-label">"><?php echo $xml->string->owner; ?> <?php echo $xml->string->email_id; ?> </label>
                                    <div class="col-lg-4">
                                        <input class="form-control" autocomplete="off"  autocomplete="off"  maxlength="60" minlength="4" name="owner_email" id="owner_email"  type="email" >
                                    </div>
                                    <label for="input-13" class="col-sm-2 col-form-label"><?php echo $xml->string->permanent_address; ?> </label>
                                    <div class="col-sm-4">
                                        <textarea maxlength="300" type="text" class="form-control" id="input-13" name="last_addresss"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-footer text-center">
                                    <button type="submit" id="socAddBtn" class="btn btn-success "><i class="fa fa-check-square-o"></i> <?php echo $xml->string->add; ?></button>
                                    <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> <?php echo $xml->string->cancel; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!--End Modal -->
<div class="modal fade" id="bulkUpload">
    <div class="modal-dialog">
        <div class="modal-content border-primary">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white"><?php echo $xml->string->import; ?> <?php echo $xml->string->bulk; ?> Employees</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $qb=$d->selectRow("block_name","block_master","block_id='$block_id'");
                $blockData=mysqli_fetch_array($qb); ?>
                <div class="form-group row">
                    <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 1 -> <?php echo $xml->string->formatted_csv; ?> <a href="controller/bulkUploadController.php?ExportUsers=ExportUsers&&csrf=<?php echo $_SESSION["token"];?>" name="ExportUsers" value="ExportUsers" class="btn btn-sm btn-primary"><i class="fa fa-check-square-o"></i> Download</a></label>
                    <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 2 -> <?php echo $xml->string->fill_your_data; ?> of <?php echo $blockData['block_name'];?> Branch Only</label>
                    <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 3 -> <?php echo $xml->string->import_file; ?></label>
                    <label class="col-sm-12 col-form-label"><?php echo $xml->string->step; ?> 4 -> <?php echo $xml->string->click_upload_btn; ?></label>
                    <label class="col-sm-12 col-form-label text-danger"><?php echo $xml->string->note; ?>: Enter Valid Department Name in Department column</label>
                    <label class="col-sm-12 col-form-label text-danger"><?php echo $xml->string->note; ?>: For view shift code please visit <a href="shiftTiming">Page</a></label>
                    <label class="col-sm-12 col-form-label text-danger"><?php echo $xml->string->note; ?>: For view Sister Company code please visit <a href="manageSisterCompanies">Page</a></label>
                </div>
                <form id="importValidation" action="controller/bulkUploadController.php" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><?php echo $xml->string->import; ?> CSV <?php echo $xml->string->file; ?> <span class="text-danger">*</span></label>
                        <div class="col-sm-8" id="PaybleAmount">
                            <input required="" type="file" name="file"  accept=".csv" class="form-control-file border">
                        </div>
                    </div>
                    <div class="form-footer text-center">
                        <input type="hidden" name="block_id" value="<?php echo $block_id; ?>">
                        <input type="hidden" name="importUser" value="importUser">
                        <input type="hidden" name="company_address" value="<?php echo  $sData['society_address']; ?>">
                        <button type="submit" name="" value="" class="btn btn-sm btn-success"><i class="fa fa-check-square-o"></i> <?php echo $xml->string->upload; ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!--End Modal -->

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $d->map_key();?>"></script>
<script type="text/javascript">

  function addUser(unit_id,block_id,floor_id)  {
      document.getElementById('unit_id').value=unit_id;
      document.getElementById('block_id').value=block_id;
      document.getElementById('floor_id').value=floor_id;
  }
</script>

<script src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $('#ownerDiv').hide();
    $('.check').change(function(){
      var data= $(this).val();
       if(data==3) {
        $('#ownerDiv').show();
       } else {
        $('#ownerDiv').hide();
       }
    });
});

/* function initialize()
{
  var latlng = new google.maps.LatLng(23.037786,72.512043);
  var latitute = 23.037786;
  var longitute = 72.512043;

  var map = new google.maps.Map(document.getElementById('map'), {
    center: latlng,
    zoom: 13
  });
  var marker = new google.maps.Marker({
    map: map,
    position: latlng,
    draggable: true,
    anchorPoint: new google.maps.Point(0, -29)
 });
  var parkingRadition = 5;
  var citymap = {
      newyork: {
        center: {lat: latitute, lng: longitute},
        population: parkingRadition
      }
  };

  var input = document.getElementById('searchInput5');
  var geocoder = new google.maps.Geocoder();
  var autocomplete10 = new google.maps.places.Autocomplete(input);
  autocomplete10.bindTo('bounds', map);
  var infowindow = new google.maps.InfoWindow();
  autocomplete10.addListener('place_changed', function()
  {
    infowindow.close();
    marker.setVisible(false);
    var place5 = autocomplete10.getPlace();
    if (!place5.geometry) {
        window.alert("Autocomplete's returned place5 contains no geometry");
        return;
    }

    // If the place5 has a geometry, then present it on a map.
    if (place5.geometry.viewport) {
        map.fitBounds(place5.geometry.viewport);
    } else {
        map.setCenter(place5.geometry.location);
        map.setZoom(17);
    }

    marker.setPosition(place5.geometry.location);
    marker.setVisible(true);
    var pincode="";
    for (var i = 0; i < place5.address_components.length; i++) {
      for (var j = 0; j < place5.address_components[i].types.length; j++) {
        if (place5.address_components[i].types[j] == "postal_code") {
          pincode = place5.address_components[i].long_name;
        }
      }
    }
    bindDataToForm(place5.formatted_address,place5.geometry.location.lat(),place5.geometry.location.lng(),pincode,place5.name);
    infowindow.setContent(place5.formatted_address);
    infowindow.open(map, marker);
  });

  // this function will work on marker move event into map
  google.maps.event.addListener(marker, 'dragend', function()
  {
    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
    {
      if (status == google.maps.GeocoderStatus.OK)
      {
        if (results[0])
        {
          var places = results[0];
          var pincode="";
          var serviceable_area_locality= places.address_components[4].long_name;
          for (var i = 0; i < places.address_components.length; i++)
          {
            for (var j = 0; j < places.address_components[i].types.length; j++)
            {
              if (places.address_components[i].types[j] == "postal_code")
              {
                pincode = places.address_components[i].long_name;
              }
            }
          }
          bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),pincode,serviceable_area_locality);
        }
      }
    });
  });
}

function bindDataToForm(address,lat,lng,pin_code,serviceable_area_locality)
{
  document.getElementById('user_latitude').value = lat;
  document.getElementById('user_longitude').value = lng;
}
google.maps.event.addDomListener(window, 'load', initialize); */
</script>