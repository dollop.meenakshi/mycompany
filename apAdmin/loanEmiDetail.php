<?php error_reporting(0);
$lId =(int)$_REQUEST['lId'];
if (isset($lId) && $lId>0) {

    $q = $d->selectRow('employee_loan_master.*,users_master.*,floors_master.floor_id,floors_master.floor_name,block_master.block_id,block_master.block_name,loan_created_by_admin.admin_name,(SELECT SUM(emi_amount) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=1) AS paid_emi_amount,(SELECT count(*) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=1) AS paidEmiCount ,(SELECT count(*) FROM loan_emi_master WHERE loan_emi_master.loan_id=employee_loan_master.loan_id AND loan_emi_master.is_emi_paid=0) AS RemainEmiCount',"employee_loan_master LEFT JOIN floors_master ON floors_master.floor_id = employee_loan_master.floor_id LEFT JOIN block_master ON block_master.block_id = employee_loan_master.block_id LEFT JOIN users_master ON users_master.user_id = employee_loan_master.user_id  LEFT JOIN bms_admin_master AS loan_created_by_admin ON loan_created_by_admin.admin_id = employee_loan_master.loan_created_by","employee_loan_master.loan_id = '$lId'");
  $data = mysqli_fetch_array($q);
  extract($data);
}
?>
<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
      <div class="col-sm-9 col-6">
        <h4 class="page-title">Loan E.M.I Detail</h4>
      </div>
      <div class="col-sm-3 col-6 text-right">
        <!-- <a href="hrDocReport" class=" btn btn-sm btn-warning waves-effect waves-light "  ><i class="fa fa-file mr-1"></i> Report</a> -->
        <!-- <a href="addSiteBtn" data-toggle="modal" data-target="#addModal"  onclick="buttonSetting()"  class="btn mr-1 btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add </a>
         <a href="javascript:void(0)" onclick="DeleteAll('deleteSite');" class="btn  btn-sm btn-danger pull-right"><i class="fa fa-trash-o fa-lg"></i> Delete </a> -->
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">

          <div class="row">
           
            <div class="col-md-4 pt-4">
              <span><B>Employee: </B> <?php echo $data['user_full_name'] .' ('.$data['user_designation'].')'; ?> </span>
            </div>
            <div class="col-md-4 pt-4">
              <span><B>Loan Amount:</B> <?php echo $data['loan_amount']; ?> </span>
            </div>
            <div class="col-md-4 pt-4">
              <span><B>Loan EMI:</B> <?php echo $data['loan_emi']; ?> </span>
            </div>
            <div class="col-md-4 pt-4">
              <span><B>Loan Created By:</B> <?php echo $data['admin_name']; ?> </span>
            </div>
            <div class="col-md-4 pt-4">
              <span><B>Total Amount:</B> <?php echo $data['paid_emi_amount']; ?> </span>
            </div>
            <div class="col-md-4 pt-4">
              <span><B>Total Emi's:</B> <?php echo $data['paidEmiCount']; ?> </span>
            </div>
            <div class="col-md-4 pt-4">
              <span><B>Remaining Emi:</B> <?php echo $data['RemainEmiCount']; ?> </span>
            </div>
            <div class="col-md-4 pt-4">
              <span><B>Loan Start Date:</B> <?php if (isset($data['loan_date']) &&  $data['loan_date'] != "0000-00-00 00:00:00") {
                            echo date("d M Y", strtotime($data['loan_date']));
                          } ?>
                          </span>
            </div>
           

          </div>
          <div class="table-responsive pt-4">
            <table id="example" class="table table-bordered">
              <thead>
                <tr>
                  <th>EMI No.</th>
                  <th>EMI Amount</th>
                  <th>EMI Date</th>
                  <th>Paid status</th>
                  <th>Collected By</th>
                  <th>Collected Date</th>
                </tr>
              </thead>
              <tbody id="showFilterData" class="">
                <?php
                $counter = 1;
                $LEs = $q = $d->selectRow('loan_emi_master.*,bms_admin_master.admin_id,bms_admin_master.admin_name','loan_emi_master LEFT JOIN bms_admin_master ON bms_admin_master.admin_id=loan_emi_master.emi_paid_by',"loan_emi_master.loan_id=$lId");
                while ($data2 = mysqli_fetch_assoc($LEs)) { ?>
                  <tr>
                    <td><?php echo $counter++; ?></td>
                    <td><?php echo (float)$data2['emi_amount']; ?></td>
                    <td><?php echo date('d M Y',strtotime($data2['emi_created_at'])); ?></td>
                    <td>
                    <?php if($data2['is_emi_paid']==0){ ?> 
                        <button onclick="payEmi(<?php echo $data2['loan_emi_id']; ?>,<?php echo $data['user_id']; ?>,<?php echo $data['loan_id']; ?>)" data-toggle="tollip" title="Mark As Paid" class="btn btn-sm btn-primary"><i class="fa fa-money"></i>
                      </button>
                       <span class="badge badge-warning">Unpaid</span> 
                      <?php }else{ ?>
                        <span class="badge badge-primary">Paid</span> 
                        <?php } ?></td>
                    <!-- <td><?php  if($data2['emi_paid_date'] !="0000-00-00") { echo date('Y-m-d',strtotime($data2['emi_paid_date'])); } ?></td> -->
                    <td><?php echo $data2['admin_name']; ?></td>
                    <td><?php  if($data2['received_date'] !="0000-00-00" && $data2['received_date'] !=null) { echo date('Y-m-d',strtotime($data2['received_date'])); } ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div><!-- End Row-->

</div>
<!-- End container-fluid-->

</div>
<!--End content-wrapper-->

<div class="modal fade" id="markAsPaid">
  <div class="modal-dialog ">
    <div class="modal-content border-primary">
      <div class="modal-header bg-primary">
        <h5 class="modal-title text-white">Collect This Emi</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="" style="align-content: center;">
       <form id="payEmiForm" action="controller/EmployeeLoanController.php" method="post">
            <div class="card-body">
                <div class="form-group row">
                    <label>Pay Mode<span class="required">*</span></label>
                    <select class="single-select form-control" name="emi_paid_mode" id="emi_paid_mode" >
                            <option  value="">-- Select --</option>
                            <option <?php if(isset($salary_slip_data) && $salary_slip_data['salary_mode']==0) { echo "selected"; }?> value="0">Bank Transaction</option>
                            <option <?php if(isset($salary_slip_data) && $salary_slip_data['salary_mode']==1){ echo "selected"; }?> value="1">Cash</option>
                            <option <?php if(isset($salary_slip_data) && $salary_slip_data['salary_mode']==2){ echo "selected"; }?> value="2">Cheque</option>
                    </select>
                </div>
                <div class="form-group row">
                    <label>Remark </label>
                    <textarea class="form-control"  name="emi_paid_remark" id="emi_paid_remark"></textarea>
                </div>
                <div class="form-group row">
                    <label>Received Date <span class="required">*</span></label>
                    <input type="text" autocomplete="off" class="form-control"  name="received_date" id="received_date"/>
                </div>
            </div>
            <div class="form-footer text-center">
                <input type="hidden" id="loan_emi_id" name="loan_emi_id" value="">
                <input type="hidden" id="loan_id" name="loan_id" value="">
                <input type="hidden" id="user_id" name="user_id" value="">
                <input type="hidden" id="payEmi" name="payEmi" value="payEmi">
                <button id="payEmiButton" name="payEmiButton" type="submit" class="btn btn-success sbmitbtn hideupdate"><i class="fa fa-check-square-o"></i>Pay </button>
                
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

</script>
<style>
 
</style>